IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Library.OnetSOC')
BEGIN
	CREATE TABLE [Library.OnetSOC]
	(
		Id BIGINT NOT NULL IDENTITY(1, 1) PRIMARY KEY,
		OnetId BIGINT NOT NULL,
		SOCId BIGINT NOT NULL,
		CONSTRAINT [FK_Library.OnetSOC_OnetId] FOREIGN KEY (OnetId) REFERENCES [Library.Onet] (Id),
		CONSTRAINT [FK_Library.OnetSOC_SOCId] FOREIGN KEY (SOCId) REFERENCES [Library.SOC] (Id)
	)
END
GO

DECLARE @OnetSOC TABLE
(
	OnetCode NVARCHAR(10),
	SOC2010Code NVARCHAR(10)
)
DECLARE @SOCXML XML

SET @SOCXML = 
'<onetsocs>
	<onetsoc onet="11-1011.00" soc="11-1011" />
	<onetsoc onet="11-1011.03" soc="11-1011" />
	<onetsoc onet="11-1021.00" soc="11-1021" />
	<onetsoc onet="11-1031.00" soc="11-1031" />
	<onetsoc onet="11-2011.00" soc="11-2011" />
	<onetsoc onet="11-2011.01" soc="11-2011" />
	<onetsoc onet="11-2021.00" soc="11-2021" />
	<onetsoc onet="11-2022.00" soc="11-2022" />
	<onetsoc onet="11-2031.00" soc="11-2031" />
	<onetsoc onet="11-3011.00" soc="11-3011" />
	<onetsoc onet="11-3021.00" soc="11-3021" />
	<onetsoc onet="11-3031.00" soc="11-3031" />
	<onetsoc onet="11-3031.01" soc="11-3031" />
	<onetsoc onet="11-3031.02" soc="11-3031" />
	<onetsoc onet="11-3051.00" soc="11-3051" />
	<onetsoc onet="11-3051.01" soc="11-3051" />
	<onetsoc onet="11-3051.02" soc="11-3051" />
	<onetsoc onet="11-3051.03" soc="11-3051" />
	<onetsoc onet="11-3051.04" soc="11-3051" />
	<onetsoc onet="11-3051.05" soc="11-3051" />
	<onetsoc onet="11-3051.06" soc="11-3051" />
	<onetsoc onet="11-3061.00" soc="11-3061" />
	<onetsoc onet="11-3071.00" soc="11-3071" />
	<onetsoc onet="11-3071.01" soc="11-3071" />
	<onetsoc onet="11-3071.02" soc="11-3071" />
	<onetsoc onet="11-3071.03" soc="11-3071" />
	<onetsoc onet="11-3111.00" soc="11-3111" />
	<onetsoc onet="11-3121.00" soc="11-3121" />
	<onetsoc onet="11-3131.00" soc="11-3131" />
	<onetsoc onet="11-9013.00" soc="11-9013" />
	<onetsoc onet="11-9013.01" soc="11-9013" />
	<onetsoc onet="11-9013.02" soc="11-9013" />
	<onetsoc onet="11-9013.03" soc="11-9013" />
	<onetsoc onet="11-9021.00" soc="11-9021" />
	<onetsoc onet="11-9031.00" soc="11-9031" />
	<onetsoc onet="11-9032.00" soc="11-9032" />
	<onetsoc onet="11-9033.00" soc="11-9033" />
	<onetsoc onet="11-9039.00" soc="11-9039" />
	<onetsoc onet="11-9039.01" soc="11-9039" />
	<onetsoc onet="11-9039.02" soc="11-9039" />
	<onetsoc onet="11-9041.00" soc="11-9041" />
	<onetsoc onet="11-9041.01" soc="11-9041" />
	<onetsoc onet="11-9051.00" soc="11-9051" />
	<onetsoc onet="11-9061.00" soc="11-9061" />
	<onetsoc onet="11-9071.00" soc="11-9071" />
	<onetsoc onet="11-9081.00" soc="11-9081" />
	<onetsoc onet="11-9111.00" soc="11-9111" />
	<onetsoc onet="11-9121.00" soc="11-9121" />
	<onetsoc onet="11-9121.01" soc="11-9121" />
	<onetsoc onet="11-9121.02" soc="11-9121" />
	<onetsoc onet="11-9131.00" soc="11-9131" />
	<onetsoc onet="11-9141.00" soc="11-9141" />
	<onetsoc onet="11-9151.00" soc="11-9151" />
	<onetsoc onet="11-9161.00" soc="11-9161" />
	<onetsoc onet="11-9199.00" soc="11-9199" />
	<onetsoc onet="11-9199.01" soc="11-9199" />
	<onetsoc onet="11-9199.02" soc="11-9199" />
	<onetsoc onet="11-9199.03" soc="11-9199" />
	<onetsoc onet="11-9199.04" soc="11-9199" />
	<onetsoc onet="11-9199.07" soc="11-9199" />
	<onetsoc onet="11-9199.08" soc="11-9199" />
	<onetsoc onet="11-9199.09" soc="11-9199" />
	<onetsoc onet="11-9199.10" soc="11-9199" />
	<onetsoc onet="11-9199.11" soc="11-9199" />
	<onetsoc onet="13-1011.00" soc="13-1011" />
	<onetsoc onet="13-1021.00" soc="13-1021" />
	<onetsoc onet="13-1022.00" soc="13-1022" />
	<onetsoc onet="13-1023.00" soc="13-1023" />
	<onetsoc onet="13-1031.00" soc="13-1031" />
	<onetsoc onet="13-1031.01" soc="13-1031" />
	<onetsoc onet="13-1031.02" soc="13-1031" />
	<onetsoc onet="13-1032.00" soc="13-1032" />
	<onetsoc onet="13-1041.00" soc="13-1041" />
	<onetsoc onet="13-1041.01" soc="13-1041" />
	<onetsoc onet="13-1041.02" soc="13-1041" />
	<onetsoc onet="13-1041.03" soc="13-1041" />
	<onetsoc onet="13-1041.04" soc="13-1041" />
	<onetsoc onet="13-1041.06" soc="13-1041" />
	<onetsoc onet="13-1041.07" soc="13-1041" />
	<onetsoc onet="13-1051.00" soc="13-1051" />
	<onetsoc onet="13-1071.00" soc="13-1071" />
	<onetsoc onet="13-1074.00" soc="13-1074" />
	<onetsoc onet="13-1075.00" soc="13-1075" />
	<onetsoc onet="13-1081.00" soc="13-1081" />
	<onetsoc onet="13-1081.01" soc="13-1081" />
	<onetsoc onet="13-1081.02" soc="13-1081" />
	<onetsoc onet="13-1111.00" soc="13-1111" />
	<onetsoc onet="13-1121.00" soc="13-1121" />
	<onetsoc onet="13-1131.00" soc="13-1131" />
	<onetsoc onet="13-1141.00" soc="13-1141" />
	<onetsoc onet="13-1151.00" soc="13-1151" />
	<onetsoc onet="13-1161.00" soc="13-1161" />
	<onetsoc onet="13-1199.00" soc="13-1199" />
	<onetsoc onet="13-1199.01" soc="13-1199" />
	<onetsoc onet="13-1199.02" soc="13-1199" />
	<onetsoc onet="13-1199.03" soc="13-1199" />
	<onetsoc onet="13-1199.04" soc="13-1199" />
	<onetsoc onet="13-1199.05" soc="13-1199" />
	<onetsoc onet="13-1199.06" soc="13-1199" />
	<onetsoc onet="13-2011.00" soc="13-2011" />
	<onetsoc onet="13-2011.01" soc="13-2011" />
	<onetsoc onet="13-2011.02" soc="13-2011" />
	<onetsoc onet="13-2021.00" soc="13-2021" />
	<onetsoc onet="13-2021.01" soc="13-2021" />
	<onetsoc onet="13-2021.02" soc="13-2021" />
	<onetsoc onet="13-2031.00" soc="13-2031" />
	<onetsoc onet="13-2041.00" soc="13-2041" />
	<onetsoc onet="13-2051.00" soc="13-2051" />
	<onetsoc onet="13-2052.00" soc="13-2052" />
	<onetsoc onet="13-2053.00" soc="13-2053" />
	<onetsoc onet="13-2061.00" soc="13-2061" />
	<onetsoc onet="13-2071.00" soc="13-2071" />
	<onetsoc onet="13-2071.01" soc="13-2071" />
	<onetsoc onet="13-2072.00" soc="13-2072" />
	<onetsoc onet="13-2081.00" soc="13-2081" />
	<onetsoc onet="13-2082.00" soc="13-2082" />
	<onetsoc onet="13-2099.00" soc="13-2099" />
	<onetsoc onet="13-2099.01" soc="13-2099" />
	<onetsoc onet="13-2099.02" soc="13-2099" />
	<onetsoc onet="13-2099.03" soc="13-2099" />
	<onetsoc onet="13-2099.04" soc="13-2099" />
	<onetsoc onet="15-1111.00" soc="15-1111" />
	<onetsoc onet="15-1121.00" soc="15-1121" />
	<onetsoc onet="15-1121.01" soc="15-1121" />
	<onetsoc onet="15-1122.00" soc="15-1122" />
	<onetsoc onet="15-1131.00" soc="15-1131" />
	<onetsoc onet="15-1132.00" soc="15-1132" />
	<onetsoc onet="15-1133.00" soc="15-1133" />
	<onetsoc onet="15-1134.00" soc="15-1134" />
	<onetsoc onet="15-1141.00" soc="15-1141" />
	<onetsoc onet="15-1142.00" soc="15-1142" />
	<onetsoc onet="15-1143.00" soc="15-1143" />
	<onetsoc onet="15-1143.01" soc="15-1143" />
	<onetsoc onet="15-1151.00" soc="15-1151" />
	<onetsoc onet="15-1152.00" soc="15-1152" />
	<onetsoc onet="15-1199.00" soc="15-1199" />
	<onetsoc onet="15-1199.01" soc="15-1199" />
	<onetsoc onet="15-1199.02" soc="15-1199" />
	<onetsoc onet="15-1199.03" soc="15-1199" />
	<onetsoc onet="15-1199.04" soc="15-1199" />
	<onetsoc onet="15-1199.05" soc="15-1199" />
	<onetsoc onet="15-1199.06" soc="15-1199" />
	<onetsoc onet="15-1199.07" soc="15-1199" />
	<onetsoc onet="15-1199.08" soc="15-1199" />
	<onetsoc onet="15-1199.09" soc="15-1199" />
	<onetsoc onet="15-1199.10" soc="15-1199" />
	<onetsoc onet="15-1199.11" soc="15-1199" />
	<onetsoc onet="15-1199.12" soc="15-1199" />
	<onetsoc onet="15-2011.00" soc="15-2011" />
	<onetsoc onet="15-2021.00" soc="15-2021" />
	<onetsoc onet="15-2031.00" soc="15-2031" />
	<onetsoc onet="15-2041.00" soc="15-2041" />
	<onetsoc onet="15-2041.01" soc="15-2041" />
	<onetsoc onet="15-2041.02" soc="15-2041" />
	<onetsoc onet="15-2091.00" soc="15-2091" />
	<onetsoc onet="15-2099.00" soc="15-2099" />
	<onetsoc onet="17-1011.00" soc="17-1011" />
	<onetsoc onet="17-1012.00" soc="17-1012" />
	<onetsoc onet="17-1021.00" soc="17-1021" />
	<onetsoc onet="17-1022.00" soc="17-1022" />
	<onetsoc onet="17-1022.01" soc="17-1022" />
	<onetsoc onet="17-2011.00" soc="17-2011" />
	<onetsoc onet="17-2021.00" soc="17-2021" />
	<onetsoc onet="17-2031.00" soc="17-2031" />
	<onetsoc onet="17-2041.00" soc="17-2041" />
	<onetsoc onet="17-2051.00" soc="17-2051" />
	<onetsoc onet="17-2051.01" soc="17-2051" />
	<onetsoc onet="17-2061.00" soc="17-2061" />
	<onetsoc onet="17-2071.00" soc="17-2071" />
	<onetsoc onet="17-2072.00" soc="17-2072" />
	<onetsoc onet="17-2072.01" soc="17-2072" />
	<onetsoc onet="17-2081.00" soc="17-2081" />
	<onetsoc onet="17-2081.01" soc="17-2081" />
	<onetsoc onet="17-2111.00" soc="17-2111" />
	<onetsoc onet="17-2111.01" soc="17-2111" />
	<onetsoc onet="17-2111.02" soc="17-2111" />
	<onetsoc onet="17-2111.03" soc="17-2111" />
	<onetsoc onet="17-2112.00" soc="17-2112" />
	<onetsoc onet="17-2112.01" soc="17-2112" />
	<onetsoc onet="17-2121.00" soc="17-2121" />
	<onetsoc onet="17-2121.01" soc="17-2121" />
	<onetsoc onet="17-2121.02" soc="17-2121" />
	<onetsoc onet="17-2131.00" soc="17-2131" />
	<onetsoc onet="17-2141.00" soc="17-2141" />
	<onetsoc onet="17-2141.01" soc="17-2141" />
	<onetsoc onet="17-2141.02" soc="17-2141" />
	<onetsoc onet="17-2151.00" soc="17-2151" />
	<onetsoc onet="17-2161.00" soc="17-2161" />
	<onetsoc onet="17-2171.00" soc="17-2171" />
	<onetsoc onet="17-2199.00" soc="17-2199" />
	<onetsoc onet="17-2199.01" soc="17-2199" />
	<onetsoc onet="17-2199.02" soc="17-2199" />
	<onetsoc onet="17-2199.03" soc="17-2199" />
	<onetsoc onet="17-2199.04" soc="17-2199" />
	<onetsoc onet="17-2199.05" soc="17-2199" />
	<onetsoc onet="17-2199.06" soc="17-2199" />
	<onetsoc onet="17-2199.07" soc="17-2199" />
	<onetsoc onet="17-2199.08" soc="17-2199" />
	<onetsoc onet="17-2199.09" soc="17-2199" />
	<onetsoc onet="17-2199.10" soc="17-2199" />
	<onetsoc onet="17-2199.11" soc="17-2199" />
	<onetsoc onet="17-3011.00" soc="17-3011" />
	<onetsoc onet="17-3011.01" soc="17-3011" />
	<onetsoc onet="17-3011.02" soc="17-3011" />
	<onetsoc onet="17-3012.00" soc="17-3012" />
	<onetsoc onet="17-3012.01" soc="17-3012" />
	<onetsoc onet="17-3012.02" soc="17-3012" />
	<onetsoc onet="17-3013.00" soc="17-3013" />
	<onetsoc onet="17-3019.00" soc="17-3019" />
	<onetsoc onet="17-3021.00" soc="17-3021" />
	<onetsoc onet="17-3022.00" soc="17-3022" />
	<onetsoc onet="17-3023.00" soc="17-3023" />
	<onetsoc onet="17-3023.01" soc="17-3023" />
	<onetsoc onet="17-3023.03" soc="17-3023" />
	<onetsoc onet="17-3024.00" soc="17-3024" />
	<onetsoc onet="17-3024.01" soc="17-3024" />
	<onetsoc onet="17-3025.00" soc="17-3025" />
	<onetsoc onet="17-3026.00" soc="17-3026" />
	<onetsoc onet="17-3027.00" soc="17-3027" />
	<onetsoc onet="17-3027.01" soc="17-3027" />
	<onetsoc onet="17-3029.00" soc="17-3029" />
	<onetsoc onet="17-3029.01" soc="17-3029" />
	<onetsoc onet="17-3029.02" soc="17-3029" />
	<onetsoc onet="17-3029.03" soc="17-3029" />
	<onetsoc onet="17-3029.04" soc="17-3029" />
	<onetsoc onet="17-3029.05" soc="17-3029" />
	<onetsoc onet="17-3029.06" soc="17-3029" />
	<onetsoc onet="17-3029.07" soc="17-3029" />
	<onetsoc onet="17-3029.08" soc="17-3029" />
	<onetsoc onet="17-3029.09" soc="17-3029" />
	<onetsoc onet="17-3029.10" soc="17-3029" />
	<onetsoc onet="17-3029.11" soc="17-3029" />
	<onetsoc onet="17-3029.12" soc="17-3029" />
	<onetsoc onet="17-3031.00" soc="17-3031" />
	<onetsoc onet="17-3031.01" soc="17-3031" />
	<onetsoc onet="17-3031.02" soc="17-3031" />
	<onetsoc onet="19-1011.00" soc="19-1011" />
	<onetsoc onet="19-1012.00" soc="19-1012" />
	<onetsoc onet="19-1013.00" soc="19-1013" />
	<onetsoc onet="19-1020.01" soc="19-1020" />
	<onetsoc onet="19-1021.00" soc="19-1021" />
	<onetsoc onet="19-1022.00" soc="19-1022" />
	<onetsoc onet="19-1023.00" soc="19-1023" />
	<onetsoc onet="19-1029.00" soc="19-1029" />
	<onetsoc onet="19-1029.01" soc="19-1029" />
	<onetsoc onet="19-1029.02" soc="19-1029" />
	<onetsoc onet="19-1029.03" soc="19-1029" />
	<onetsoc onet="19-1031.00" soc="19-1031" />
	<onetsoc onet="19-1031.01" soc="19-1031" />
	<onetsoc onet="19-1031.02" soc="19-1031" />
	<onetsoc onet="19-1031.03" soc="19-1031" />
	<onetsoc onet="19-1032.00" soc="19-1032" />
	<onetsoc onet="19-1041.00" soc="19-1041" />
	<onetsoc onet="19-1042.00" soc="19-1042" />
	<onetsoc onet="19-1099.00" soc="19-1099" />
	<onetsoc onet="19-2011.00" soc="19-2011" />
	<onetsoc onet="19-2012.00" soc="19-2012" />
	<onetsoc onet="19-2021.00" soc="19-2021" />
	<onetsoc onet="19-2031.00" soc="19-2031" />
	<onetsoc onet="19-2032.00" soc="19-2032" />
	<onetsoc onet="19-2041.00" soc="19-2041" />
	<onetsoc onet="19-2041.01" soc="19-2041" />
	<onetsoc onet="19-2041.02" soc="19-2041" />
	<onetsoc onet="19-2041.03" soc="19-2041" />
	<onetsoc onet="19-2042.00" soc="19-2042" />
	<onetsoc onet="19-2043.00" soc="19-2043" />
	<onetsoc onet="19-2099.00" soc="19-2099" />
	<onetsoc onet="19-2099.01" soc="19-2099" />
	<onetsoc onet="19-3011.00" soc="19-3011" />
	<onetsoc onet="19-3011.01" soc="19-3011" />
	<onetsoc onet="19-3022.00" soc="19-3022" />
	<onetsoc onet="19-3031.00" soc="19-3031" />
	<onetsoc onet="19-3031.01" soc="19-3031" />
	<onetsoc onet="19-3031.02" soc="19-3031" />
	<onetsoc onet="19-3031.03" soc="19-3031" />
	<onetsoc onet="19-3032.00" soc="19-3032" />
	<onetsoc onet="19-3039.00" soc="19-3039" />
	<onetsoc onet="19-3039.01" soc="19-3039" />
	<onetsoc onet="19-3041.00" soc="19-3041" />
	<onetsoc onet="19-3051.00" soc="19-3051" />
	<onetsoc onet="19-3091.00" soc="19-3091" />
	<onetsoc onet="19-3091.01" soc="19-3091" />
	<onetsoc onet="19-3091.02" soc="19-3091" />
	<onetsoc onet="19-3092.00" soc="19-3092" />
	<onetsoc onet="19-3093.00" soc="19-3093" />
	<onetsoc onet="19-3094.00" soc="19-3094" />
	<onetsoc onet="19-3099.00" soc="19-3099" />
	<onetsoc onet="19-3099.01" soc="19-3099" />
	<onetsoc onet="19-4011.00" soc="19-4011" />
	<onetsoc onet="19-4011.01" soc="19-4011" />
	<onetsoc onet="19-4011.02" soc="19-4011" />
	<onetsoc onet="19-4021.00" soc="19-4021" />
	<onetsoc onet="19-4031.00" soc="19-4031" />
	<onetsoc onet="19-4041.00" soc="19-4041" />
	<onetsoc onet="19-4041.01" soc="19-4041" />
	<onetsoc onet="19-4041.02" soc="19-4041" />
	<onetsoc onet="19-4051.00" soc="19-4051" />
	<onetsoc onet="19-4051.01" soc="19-4051" />
	<onetsoc onet="19-4051.02" soc="19-4051" />
	<onetsoc onet="19-4061.00" soc="19-4061" />
	<onetsoc onet="19-4061.01" soc="19-4061" />
	<onetsoc onet="19-4091.00" soc="19-4091" />
	<onetsoc onet="19-4092.00" soc="19-4092" />
	<onetsoc onet="19-4093.00" soc="19-4093" />
	<onetsoc onet="19-4099.00" soc="19-4099" />
	<onetsoc onet="19-4099.01" soc="19-4099" />
	<onetsoc onet="19-4099.02" soc="19-4099" />
	<onetsoc onet="19-4099.03" soc="19-4099" />
	<onetsoc onet="21-1011.00" soc="21-1011" />
	<onetsoc onet="21-1012.00" soc="21-1012" />
	<onetsoc onet="21-1013.00" soc="21-1013" />
	<onetsoc onet="21-1014.00" soc="21-1014" />
	<onetsoc onet="21-1015.00" soc="21-1015" />
	<onetsoc onet="21-1019.00" soc="21-1019" />
	<onetsoc onet="21-1021.00" soc="21-1021" />
	<onetsoc onet="21-1022.00" soc="21-1022" />
	<onetsoc onet="21-1023.00" soc="21-1023" />
	<onetsoc onet="21-1029.00" soc="21-1029" />
	<onetsoc onet="21-1091.00" soc="21-1091" />
	<onetsoc onet="21-1092.00" soc="21-1092" />
	<onetsoc onet="21-1093.00" soc="21-1093" />
	<onetsoc onet="21-1094.00" soc="21-1094" />
	<onetsoc onet="21-1099.00" soc="21-1099" />
	<onetsoc onet="21-2011.00" soc="21-2011" />
	<onetsoc onet="21-2021.00" soc="21-2021" />
	<onetsoc onet="21-2099.00" soc="21-2099" />
	<onetsoc onet="23-1011.00" soc="23-1011" />
	<onetsoc onet="23-1012.00" soc="23-1012" />
	<onetsoc onet="23-1021.00" soc="23-1021" />
	<onetsoc onet="23-1022.00" soc="23-1022" />
	<onetsoc onet="23-1023.00" soc="23-1023" />
	<onetsoc onet="23-2011.00" soc="23-2011" />
	<onetsoc onet="23-2091.00" soc="23-2091" />
	<onetsoc onet="23-2093.00" soc="23-2093" />
	<onetsoc onet="23-2099.00" soc="23-2099" />
	<onetsoc onet="25-1011.00" soc="25-1011" />
	<onetsoc onet="25-1021.00" soc="25-1021" />
	<onetsoc onet="25-1022.00" soc="25-1022" />
	<onetsoc onet="25-1031.00" soc="25-1031" />
	<onetsoc onet="25-1032.00" soc="25-1032" />
	<onetsoc onet="25-1041.00" soc="25-1041" />
	<onetsoc onet="25-1042.00" soc="25-1042" />
	<onetsoc onet="25-1043.00" soc="25-1043" />
	<onetsoc onet="25-1051.00" soc="25-1051" />
	<onetsoc onet="25-1052.00" soc="25-1052" />
	<onetsoc onet="25-1053.00" soc="25-1053" />
	<onetsoc onet="25-1054.00" soc="25-1054" />
	<onetsoc onet="25-1061.00" soc="25-1061" />
	<onetsoc onet="25-1062.00" soc="25-1062" />
	<onetsoc onet="25-1063.00" soc="25-1063" />
	<onetsoc onet="25-1064.00" soc="25-1064" />
	<onetsoc onet="25-1065.00" soc="25-1065" />
	<onetsoc onet="25-1066.00" soc="25-1066" />
	<onetsoc onet="25-1067.00" soc="25-1067" />
	<onetsoc onet="25-1069.00" soc="25-1069" />
	<onetsoc onet="25-1071.00" soc="25-1071" />
	<onetsoc onet="25-1072.00" soc="25-1072" />
	<onetsoc onet="25-1081.00" soc="25-1081" />
	<onetsoc onet="25-1082.00" soc="25-1082" />
	<onetsoc onet="25-1111.00" soc="25-1111" />
	<onetsoc onet="25-1112.00" soc="25-1112" />
	<onetsoc onet="25-1113.00" soc="25-1113" />
	<onetsoc onet="25-1121.00" soc="25-1121" />
	<onetsoc onet="25-1122.00" soc="25-1122" />
	<onetsoc onet="25-1123.00" soc="25-1123" />
	<onetsoc onet="25-1124.00" soc="25-1124" />
	<onetsoc onet="25-1125.00" soc="25-1125" />
	<onetsoc onet="25-1126.00" soc="25-1126" />
	<onetsoc onet="25-1191.00" soc="25-1191" />
	<onetsoc onet="25-1192.00" soc="25-1192" />
	<onetsoc onet="25-1193.00" soc="25-1193" />
	<onetsoc onet="25-1194.00" soc="25-1194" />
	<onetsoc onet="25-1199.00" soc="25-1199" />
	<onetsoc onet="25-2011.00" soc="25-2011" />
	<onetsoc onet="25-2012.00" soc="25-2012" />
	<onetsoc onet="25-2021.00" soc="25-2021" />
	<onetsoc onet="25-2022.00" soc="25-2022" />
	<onetsoc onet="25-2023.00" soc="25-2023" />
	<onetsoc onet="25-2031.00" soc="25-2031" />
	<onetsoc onet="25-2032.00" soc="25-2032" />
	<onetsoc onet="25-2051.00" soc="25-2051" />
	<onetsoc onet="25-2052.00" soc="25-2052" />
	<onetsoc onet="25-2053.00" soc="25-2053" />
	<onetsoc onet="25-2054.00" soc="25-2054" />
	<onetsoc onet="25-2059.00" soc="25-2059" />
	<onetsoc onet="25-2059.01" soc="25-2059" />
	<onetsoc onet="25-3011.00" soc="25-3011" />
	<onetsoc onet="25-3021.00" soc="25-3021" />
	<onetsoc onet="25-3099.00" soc="25-3099" />
	<onetsoc onet="25-3099.02" soc="25-3099" />
	<onetsoc onet="25-4011.00" soc="25-4011" />
	<onetsoc onet="25-4012.00" soc="25-4012" />
	<onetsoc onet="25-4013.00" soc="25-4013" />
	<onetsoc onet="25-4021.00" soc="25-4021" />
	<onetsoc onet="25-4031.00" soc="25-4031" />
	<onetsoc onet="25-9011.00" soc="25-9011" />
	<onetsoc onet="25-9021.00" soc="25-9021" />
	<onetsoc onet="25-9031.00" soc="25-9031" />
	<onetsoc onet="25-9031.01" soc="25-9031" />
	<onetsoc onet="25-9041.00" soc="25-9041" />
	<onetsoc onet="25-9099.00" soc="25-9099" />
	<onetsoc onet="27-1011.00" soc="27-1011" />
	<onetsoc onet="27-1012.00" soc="27-1012" />
	<onetsoc onet="27-1013.00" soc="27-1013" />
	<onetsoc onet="27-1014.00" soc="27-1014" />
	<onetsoc onet="27-1019.00" soc="27-1019" />
	<onetsoc onet="27-1021.00" soc="27-1021" />
	<onetsoc onet="27-1022.00" soc="27-1022" />
	<onetsoc onet="27-1023.00" soc="27-1023" />
	<onetsoc onet="27-1024.00" soc="27-1024" />
	<onetsoc onet="27-1025.00" soc="27-1025" />
	<onetsoc onet="27-1026.00" soc="27-1026" />
	<onetsoc onet="27-1027.00" soc="27-1027" />
	<onetsoc onet="27-1029.00" soc="27-1029" />
	<onetsoc onet="27-2011.00" soc="27-2011" />
	<onetsoc onet="27-2012.00" soc="27-2012" />
	<onetsoc onet="27-2012.01" soc="27-2012" />
	<onetsoc onet="27-2012.02" soc="27-2012" />
	<onetsoc onet="27-2012.03" soc="27-2012" />
	<onetsoc onet="27-2012.04" soc="27-2012" />
	<onetsoc onet="27-2012.05" soc="27-2012" />
	<onetsoc onet="27-2021.00" soc="27-2021" />
	<onetsoc onet="27-2022.00" soc="27-2022" />
	<onetsoc onet="27-2023.00" soc="27-2023" />
	<onetsoc onet="27-2031.00" soc="27-2031" />
	<onetsoc onet="27-2032.00" soc="27-2032" />
	<onetsoc onet="27-2041.00" soc="27-2041" />
	<onetsoc onet="27-2041.01" soc="27-2041" />
	<onetsoc onet="27-2041.04" soc="27-2041" />
	<onetsoc onet="27-2042.00" soc="27-2042" />
	<onetsoc onet="27-2042.01" soc="27-2042" />
	<onetsoc onet="27-2042.02" soc="27-2042" />
	<onetsoc onet="27-2099.00" soc="27-2099" />
	<onetsoc onet="27-3011.00" soc="27-3011" />
	<onetsoc onet="27-3012.00" soc="27-3012" />
	<onetsoc onet="27-3021.00" soc="27-3021" />
	<onetsoc onet="27-3022.00" soc="27-3022" />
	<onetsoc onet="27-3031.00" soc="27-3031" />
	<onetsoc onet="27-3041.00" soc="27-3041" />
	<onetsoc onet="27-3042.00" soc="27-3042" />
	<onetsoc onet="27-3043.00" soc="27-3043" />
	<onetsoc onet="27-3043.04" soc="27-3043" />
	<onetsoc onet="27-3043.05" soc="27-3043" />
	<onetsoc onet="27-3091.00" soc="27-3091" />
	<onetsoc onet="27-3099.00" soc="27-3099" />
	<onetsoc onet="27-4011.00" soc="27-4011" />
	<onetsoc onet="27-4012.00" soc="27-4012" />
	<onetsoc onet="27-4013.00" soc="27-4013" />
	<onetsoc onet="27-4014.00" soc="27-4014" />
	<onetsoc onet="27-4021.00" soc="27-4021" />
	<onetsoc onet="27-4031.00" soc="27-4031" />
	<onetsoc onet="27-4032.00" soc="27-4032" />
	<onetsoc onet="27-4099.00" soc="27-4099" />
	<onetsoc onet="29-1011.00" soc="29-1011" />
	<onetsoc onet="29-1021.00" soc="29-1021" />
	<onetsoc onet="29-1022.00" soc="29-1022" />
	<onetsoc onet="29-1023.00" soc="29-1023" />
	<onetsoc onet="29-1024.00" soc="29-1024" />
	<onetsoc onet="29-1029.00" soc="29-1029" />
	<onetsoc onet="29-1031.00" soc="29-1031" />
	<onetsoc onet="29-1041.00" soc="29-1041" />
	<onetsoc onet="29-1051.00" soc="29-1051" />
	<onetsoc onet="29-1061.00" soc="29-1061" />
	<onetsoc onet="29-1062.00" soc="29-1062" />
	<onetsoc onet="29-1063.00" soc="29-1063" />
	<onetsoc onet="29-1064.00" soc="29-1064" />
	<onetsoc onet="29-1065.00" soc="29-1065" />
	<onetsoc onet="29-1066.00" soc="29-1066" />
	<onetsoc onet="29-1067.00" soc="29-1067" />
	<onetsoc onet="29-1069.00" soc="29-1069" />
	<onetsoc onet="29-1069.01" soc="29-1069" />
	<onetsoc onet="29-1069.02" soc="29-1069" />
	<onetsoc onet="29-1069.03" soc="29-1069" />
	<onetsoc onet="29-1069.04" soc="29-1069" />
	<onetsoc onet="29-1069.05" soc="29-1069" />
	<onetsoc onet="29-1069.06" soc="29-1069" />
	<onetsoc onet="29-1069.07" soc="29-1069" />
	<onetsoc onet="29-1069.08" soc="29-1069" />
	<onetsoc onet="29-1069.09" soc="29-1069" />
	<onetsoc onet="29-1069.10" soc="29-1069" />
	<onetsoc onet="29-1069.11" soc="29-1069" />
	<onetsoc onet="29-1069.12" soc="29-1069" />
	<onetsoc onet="29-1071.00" soc="29-1071" />
	<onetsoc onet="29-1071.01" soc="29-1071" />
	<onetsoc onet="29-1081.00" soc="29-1081" />
	<onetsoc onet="29-1122.00" soc="29-1122" />
	<onetsoc onet="29-1122.01" soc="29-1122" />
	<onetsoc onet="29-1123.00" soc="29-1123" />
	<onetsoc onet="29-1124.00" soc="29-1124" />
	<onetsoc onet="29-1125.00" soc="29-1125" />
	<onetsoc onet="29-1125.01" soc="29-1125" />
	<onetsoc onet="29-1125.02" soc="29-1125" />
	<onetsoc onet="29-1126.00" soc="29-1126" />
	<onetsoc onet="29-1127.00" soc="29-1127" />
	<onetsoc onet="29-1128.00" soc="29-1128" />
	<onetsoc onet="29-1129.00" soc="29-1129" />
	<onetsoc onet="29-1131.00" soc="29-1131" />
	<onetsoc onet="29-1141.00" soc="29-1141" />
	<onetsoc onet="29-1141.01" soc="29-1141" />
	<onetsoc onet="29-1141.02" soc="29-1141" />
	<onetsoc onet="29-1141.03" soc="29-1141" />
	<onetsoc onet="29-1141.04" soc="29-1141" />
	<onetsoc onet="29-1151.00" soc="29-1151" />
	<onetsoc onet="29-1161.00" soc="29-1161" />
	<onetsoc onet="29-1171.00" soc="29-1171" />
	<onetsoc onet="29-1181.00" soc="29-1181" />
	<onetsoc onet="29-1199.00" soc="29-1199" />
	<onetsoc onet="29-1199.01" soc="29-1199" />
	<onetsoc onet="29-1199.04" soc="29-1199" />
	<onetsoc onet="29-1199.05" soc="29-1199" />
	<onetsoc onet="29-2011.00" soc="29-2011" />
	<onetsoc onet="29-2011.01" soc="29-2011" />
	<onetsoc onet="29-2011.02" soc="29-2011" />
	<onetsoc onet="29-2011.03" soc="29-2011" />
	<onetsoc onet="29-2012.00" soc="29-2012" />
	<onetsoc onet="29-2021.00" soc="29-2021" />
	<onetsoc onet="29-2031.00" soc="29-2031" />
	<onetsoc onet="29-2032.00" soc="29-2032" />
	<onetsoc onet="29-2033.00" soc="29-2033" />
	<onetsoc onet="29-2034.00" soc="29-2034" />
	<onetsoc onet="29-2035.00" soc="29-2035" />
	<onetsoc onet="29-2041.00" soc="29-2041" />
	<onetsoc onet="29-2051.00" soc="29-2051" />
	<onetsoc onet="29-2052.00" soc="29-2052" />
	<onetsoc onet="29-2053.00" soc="29-2053" />
	<onetsoc onet="29-2054.00" soc="29-2054" />
	<onetsoc onet="29-2055.00" soc="29-2055" />
	<onetsoc onet="29-2056.00" soc="29-2056" />
	<onetsoc onet="29-2057.00" soc="29-2057" />
	<onetsoc onet="29-2061.00" soc="29-2061" />
	<onetsoc onet="29-2071.00" soc="29-2071" />
	<onetsoc onet="29-2081.00" soc="29-2081" />
	<onetsoc onet="29-2091.00" soc="29-2091" />
	<onetsoc onet="29-2092.00" soc="29-2092" />
	<onetsoc onet="29-2099.00" soc="29-2099" />
	<onetsoc onet="29-2099.01" soc="29-2099" />
	<onetsoc onet="29-2099.05" soc="29-2099" />
	<onetsoc onet="29-2099.06" soc="29-2099" />
	<onetsoc onet="29-2099.07" soc="29-2099" />
	<onetsoc onet="29-9011.00" soc="29-9011" />
	<onetsoc onet="29-9012.00" soc="29-9012" />
	<onetsoc onet="29-9091.00" soc="29-9091" />
	<onetsoc onet="29-9092.00" soc="29-9092" />
	<onetsoc onet="29-9099.00" soc="29-9099" />
	<onetsoc onet="29-9099.01" soc="29-9099" />
	<onetsoc onet="31-1011.00" soc="31-1011" />
	<onetsoc onet="31-1013.00" soc="31-1013" />
	<onetsoc onet="31-1014.00" soc="31-1014" />
	<onetsoc onet="31-1015.00" soc="31-1015" />
	<onetsoc onet="31-2011.00" soc="31-2011" />
	<onetsoc onet="31-2012.00" soc="31-2012" />
	<onetsoc onet="31-2021.00" soc="31-2021" />
	<onetsoc onet="31-2022.00" soc="31-2022" />
	<onetsoc onet="31-9011.00" soc="31-9011" />
	<onetsoc onet="31-9091.00" soc="31-9091" />
	<onetsoc onet="31-9092.00" soc="31-9092" />
	<onetsoc onet="31-9093.00" soc="31-9093" />
	<onetsoc onet="31-9094.00" soc="31-9094" />
	<onetsoc onet="31-9095.00" soc="31-9095" />
	<onetsoc onet="31-9096.00" soc="31-9096" />
	<onetsoc onet="31-9097.00" soc="31-9097" />
	<onetsoc onet="31-9099.00" soc="31-9099" />
	<onetsoc onet="31-9099.01" soc="31-9099" />
	<onetsoc onet="31-9099.02" soc="31-9099" />
	<onetsoc onet="33-1011.00" soc="33-1011" />
	<onetsoc onet="33-1012.00" soc="33-1012" />
	<onetsoc onet="33-1021.00" soc="33-1021" />
	<onetsoc onet="33-1021.01" soc="33-1021" />
	<onetsoc onet="33-1021.02" soc="33-1021" />
	<onetsoc onet="33-1099.00" soc="33-1099" />
	<onetsoc onet="33-2011.00" soc="33-2011" />
	<onetsoc onet="33-2011.01" soc="33-2011" />
	<onetsoc onet="33-2011.02" soc="33-2011" />
	<onetsoc onet="33-2021.00" soc="33-2021" />
	<onetsoc onet="33-2021.01" soc="33-2021" />
	<onetsoc onet="33-2021.02" soc="33-2021" />
	<onetsoc onet="33-2022.00" soc="33-2022" />
	<onetsoc onet="33-3011.00" soc="33-3011" />
	<onetsoc onet="33-3012.00" soc="33-3012" />
	<onetsoc onet="33-3021.00" soc="33-3021" />
	<onetsoc onet="33-3021.01" soc="33-3021" />
	<onetsoc onet="33-3021.02" soc="33-3021" />
	<onetsoc onet="33-3021.03" soc="33-3021" />
	<onetsoc onet="33-3021.05" soc="33-3021" />
	<onetsoc onet="33-3021.06" soc="33-3021" />
	<onetsoc onet="33-3031.00" soc="33-3031" />
	<onetsoc onet="33-3041.00" soc="33-3041" />
	<onetsoc onet="33-3051.00" soc="33-3051" />
	<onetsoc onet="33-3051.01" soc="33-3051" />
	<onetsoc onet="33-3051.03" soc="33-3051" />
	<onetsoc onet="33-3052.00" soc="33-3052" />
	<onetsoc onet="33-9011.00" soc="33-9011" />
	<onetsoc onet="33-9021.00" soc="33-9021" />
	<onetsoc onet="33-9031.00" soc="33-9031" />
	<onetsoc onet="33-9032.00" soc="33-9032" />
	<onetsoc onet="33-9091.00" soc="33-9091" />
	<onetsoc onet="33-9092.00" soc="33-9092" />
	<onetsoc onet="33-9093.00" soc="33-9093" />
	<onetsoc onet="33-9099.00" soc="33-9099" />
	<onetsoc onet="33-9099.02" soc="33-9099" />
	<onetsoc onet="35-1011.00" soc="35-1011" />
	<onetsoc onet="35-1012.00" soc="35-1012" />
	<onetsoc onet="35-2011.00" soc="35-2011" />
	<onetsoc onet="35-2012.00" soc="35-2012" />
	<onetsoc onet="35-2013.00" soc="35-2013" />
	<onetsoc onet="35-2014.00" soc="35-2014" />
	<onetsoc onet="35-2015.00" soc="35-2015" />
	<onetsoc onet="35-2019.00" soc="35-2019" />
	<onetsoc onet="35-2021.00" soc="35-2021" />
	<onetsoc onet="35-3011.00" soc="35-3011" />
	<onetsoc onet="35-3021.00" soc="35-3021" />
	<onetsoc onet="35-3022.00" soc="35-3022" />
	<onetsoc onet="35-3022.01" soc="35-3022" />
	<onetsoc onet="35-3031.00" soc="35-3031" />
	<onetsoc onet="35-3041.00" soc="35-3041" />
	<onetsoc onet="35-9011.00" soc="35-9011" />
	<onetsoc onet="35-9021.00" soc="35-9021" />
	<onetsoc onet="35-9031.00" soc="35-9031" />
	<onetsoc onet="35-9099.00" soc="35-9099" />
	<onetsoc onet="37-1011.00" soc="37-1011" />
	<onetsoc onet="37-1012.00" soc="37-1012" />
	<onetsoc onet="37-2011.00" soc="37-2011" />
	<onetsoc onet="37-2012.00" soc="37-2012" />
	<onetsoc onet="37-2019.00" soc="37-2019" />
	<onetsoc onet="37-2021.00" soc="37-2021" />
	<onetsoc onet="37-3011.00" soc="37-3011" />
	<onetsoc onet="37-3012.00" soc="37-3012" />
	<onetsoc onet="37-3013.00" soc="37-3013" />
	<onetsoc onet="37-3019.00" soc="37-3019" />
	<onetsoc onet="39-1011.00" soc="39-1011" />
	<onetsoc onet="39-1012.00" soc="39-1012" />
	<onetsoc onet="39-1021.00" soc="39-1021" />
	<onetsoc onet="39-1021.01" soc="39-1021" />
	<onetsoc onet="39-2011.00" soc="39-2011" />
	<onetsoc onet="39-2021.00" soc="39-2021" />
	<onetsoc onet="39-3011.00" soc="39-3011" />
	<onetsoc onet="39-3012.00" soc="39-3012" />
	<onetsoc onet="39-3019.00" soc="39-3019" />
	<onetsoc onet="39-3021.00" soc="39-3021" />
	<onetsoc onet="39-3031.00" soc="39-3031" />
	<onetsoc onet="39-3091.00" soc="39-3091" />
	<onetsoc onet="39-3092.00" soc="39-3092" />
	<onetsoc onet="39-3093.00" soc="39-3093" />
	<onetsoc onet="39-3099.00" soc="39-3099" />
	<onetsoc onet="39-4011.00" soc="39-4011" />
	<onetsoc onet="39-4021.00" soc="39-4021" />
	<onetsoc onet="39-4031.00" soc="39-4031" />
	<onetsoc onet="39-5011.00" soc="39-5011" />
	<onetsoc onet="39-5012.00" soc="39-5012" />
	<onetsoc onet="39-5091.00" soc="39-5091" />
	<onetsoc onet="39-5092.00" soc="39-5092" />
	<onetsoc onet="39-5093.00" soc="39-5093" />
	<onetsoc onet="39-5094.00" soc="39-5094" />
	<onetsoc onet="39-6011.00" soc="39-6011" />
	<onetsoc onet="39-6012.00" soc="39-6012" />
	<onetsoc onet="39-7011.00" soc="39-7011" />
	<onetsoc onet="39-7012.00" soc="39-7012" />
	<onetsoc onet="39-9011.00" soc="39-9011" />
	<onetsoc onet="39-9011.01" soc="39-9011" />
	<onetsoc onet="39-9021.00" soc="39-9021" />
	<onetsoc onet="39-9031.00" soc="39-9031" />
	<onetsoc onet="39-9032.00" soc="39-9032" />
	<onetsoc onet="39-9041.00" soc="39-9041" />
	<onetsoc onet="39-9099.00" soc="39-9099" />
	<onetsoc onet="41-1011.00" soc="41-1011" />
	<onetsoc onet="41-1012.00" soc="41-1012" />
	<onetsoc onet="41-2011.00" soc="41-2011" />
	<onetsoc onet="41-2012.00" soc="41-2012" />
	<onetsoc onet="41-2021.00" soc="41-2021" />
	<onetsoc onet="41-2022.00" soc="41-2022" />
	<onetsoc onet="41-2031.00" soc="41-2031" />
	<onetsoc onet="41-3011.00" soc="41-3011" />
	<onetsoc onet="41-3021.00" soc="41-3021" />
	<onetsoc onet="41-3031.00" soc="41-3031" />
	<onetsoc onet="41-3031.01" soc="41-3031" />
	<onetsoc onet="41-3031.02" soc="41-3031" />
	<onetsoc onet="41-3031.03" soc="41-3031" />
	<onetsoc onet="41-3041.00" soc="41-3041" />
	<onetsoc onet="41-3099.00" soc="41-3099" />
	<onetsoc onet="41-3099.01" soc="41-3099" />
	<onetsoc onet="41-4011.00" soc="41-4011" />
	<onetsoc onet="41-4011.07" soc="41-4011" />
	<onetsoc onet="41-4012.00" soc="41-4012" />
	<onetsoc onet="41-9011.00" soc="41-9011" />
	<onetsoc onet="41-9012.00" soc="41-9012" />
	<onetsoc onet="41-9021.00" soc="41-9021" />
	<onetsoc onet="41-9022.00" soc="41-9022" />
	<onetsoc onet="41-9031.00" soc="41-9031" />
	<onetsoc onet="41-9041.00" soc="41-9041" />
	<onetsoc onet="41-9091.00" soc="41-9091" />
	<onetsoc onet="41-9099.00" soc="41-9099" />
	<onetsoc onet="43-1011.00" soc="43-1011" />
	<onetsoc onet="43-2011.00" soc="43-2011" />
	<onetsoc onet="43-2021.00" soc="43-2021" />
	<onetsoc onet="43-2099.00" soc="43-2099" />
	<onetsoc onet="43-3011.00" soc="43-3011" />
	<onetsoc onet="43-3021.00" soc="43-3021" />
	<onetsoc onet="43-3021.01" soc="43-3021" />
	<onetsoc onet="43-3021.02" soc="43-3021" />
	<onetsoc onet="43-3031.00" soc="43-3031" />
	<onetsoc onet="43-3041.00" soc="43-3041" />
	<onetsoc onet="43-3051.00" soc="43-3051" />
	<onetsoc onet="43-3061.00" soc="43-3061" />
	<onetsoc onet="43-3071.00" soc="43-3071" />
	<onetsoc onet="43-3099.00" soc="43-3099" />
	<onetsoc onet="43-4011.00" soc="43-4011" />
	<onetsoc onet="43-4021.00" soc="43-4021" />
	<onetsoc onet="43-4031.00" soc="43-4031" />
	<onetsoc onet="43-4031.01" soc="43-4031" />
	<onetsoc onet="43-4031.02" soc="43-4031" />
	<onetsoc onet="43-4031.03" soc="43-4031" />
	<onetsoc onet="43-4041.00" soc="43-4041" />
	<onetsoc onet="43-4041.01" soc="43-4041" />
	<onetsoc onet="43-4041.02" soc="43-4041" />
	<onetsoc onet="43-4051.00" soc="43-4051" />
	<onetsoc onet="43-4051.03" soc="43-4051" />
	<onetsoc onet="43-4061.00" soc="43-4061" />
	<onetsoc onet="43-4071.00" soc="43-4071" />
	<onetsoc onet="43-4081.00" soc="43-4081" />
	<onetsoc onet="43-4111.00" soc="43-4111" />
	<onetsoc onet="43-4121.00" soc="43-4121" />
	<onetsoc onet="43-4131.00" soc="43-4131" />
	<onetsoc onet="43-4141.00" soc="43-4141" />
	<onetsoc onet="43-4151.00" soc="43-4151" />
	<onetsoc onet="43-4161.00" soc="43-4161" />
	<onetsoc onet="43-4171.00" soc="43-4171" />
	<onetsoc onet="43-4181.00" soc="43-4181" />
	<onetsoc onet="43-4199.00" soc="43-4199" />
	<onetsoc onet="43-5011.00" soc="43-5011" />
	<onetsoc onet="43-5011.01" soc="43-5011" />
	<onetsoc onet="43-5021.00" soc="43-5021" />
	<onetsoc onet="43-5031.00" soc="43-5031" />
	<onetsoc onet="43-5032.00" soc="43-5032" />
	<onetsoc onet="43-5041.00" soc="43-5041" />
	<onetsoc onet="43-5051.00" soc="43-5051" />
	<onetsoc onet="43-5052.00" soc="43-5052" />
	<onetsoc onet="43-5053.00" soc="43-5053" />
	<onetsoc onet="43-5061.00" soc="43-5061" />
	<onetsoc onet="43-5071.00" soc="43-5071" />
	<onetsoc onet="43-5081.00" soc="43-5081" />
	<onetsoc onet="43-5081.01" soc="43-5081" />
	<onetsoc onet="43-5081.02" soc="43-5081" />
	<onetsoc onet="43-5081.03" soc="43-5081" />
	<onetsoc onet="43-5081.04" soc="43-5081" />
	<onetsoc onet="43-5111.00" soc="43-5111" />
	<onetsoc onet="43-6011.00" soc="43-6011" />
	<onetsoc onet="43-6012.00" soc="43-6012" />
	<onetsoc onet="43-6013.00" soc="43-6013" />
	<onetsoc onet="43-6014.00" soc="43-6014" />
	<onetsoc onet="43-9011.00" soc="43-9011" />
	<onetsoc onet="43-9021.00" soc="43-9021" />
	<onetsoc onet="43-9022.00" soc="43-9022" />
	<onetsoc onet="43-9031.00" soc="43-9031" />
	<onetsoc onet="43-9041.00" soc="43-9041" />
	<onetsoc onet="43-9041.01" soc="43-9041" />
	<onetsoc onet="43-9041.02" soc="43-9041" />
	<onetsoc onet="43-9051.00" soc="43-9051" />
	<onetsoc onet="43-9061.00" soc="43-9061" />
	<onetsoc onet="43-9071.00" soc="43-9071" />
	<onetsoc onet="43-9081.00" soc="43-9081" />
	<onetsoc onet="43-9111.00" soc="43-9111" />
	<onetsoc onet="43-9111.01" soc="43-9111" />
	<onetsoc onet="43-9199.00" soc="43-9199" />
	<onetsoc onet="45-1011.00" soc="45-1011" />
	<onetsoc onet="45-1011.05" soc="45-1011" />
	<onetsoc onet="45-1011.06" soc="45-1011" />
	<onetsoc onet="45-1011.07" soc="45-1011" />
	<onetsoc onet="45-1011.08" soc="45-1011" />
	<onetsoc onet="45-2011.00" soc="45-2011" />
	<onetsoc onet="45-2021.00" soc="45-2021" />
	<onetsoc onet="45-2041.00" soc="45-2041" />
	<onetsoc onet="45-2091.00" soc="45-2091" />
	<onetsoc onet="45-2092.00" soc="45-2092" />
	<onetsoc onet="45-2092.01" soc="45-2092" />
	<onetsoc onet="45-2092.02" soc="45-2092" />
	<onetsoc onet="45-2093.00" soc="45-2093" />
	<onetsoc onet="45-2099.00" soc="45-2099" />
	<onetsoc onet="45-3011.00" soc="45-3011" />
	<onetsoc onet="45-3021.00" soc="45-3021" />
	<onetsoc onet="45-4011.00" soc="45-4011" />
	<onetsoc onet="45-4021.00" soc="45-4021" />
	<onetsoc onet="45-4022.00" soc="45-4022" />
	<onetsoc onet="45-4023.00" soc="45-4023" />
	<onetsoc onet="45-4029.00" soc="45-4029" />
	<onetsoc onet="47-1011.00" soc="47-1011" />
	<onetsoc onet="47-1011.03" soc="47-1011" />
	<onetsoc onet="47-2011.00" soc="47-2011" />
	<onetsoc onet="47-2021.00" soc="47-2021" />
	<onetsoc onet="47-2022.00" soc="47-2022" />
	<onetsoc onet="47-2031.00" soc="47-2031" />
	<onetsoc onet="47-2031.01" soc="47-2031" />
	<onetsoc onet="47-2031.02" soc="47-2031" />
	<onetsoc onet="47-2041.00" soc="47-2041" />
	<onetsoc onet="47-2042.00" soc="47-2042" />
	<onetsoc onet="47-2043.00" soc="47-2043" />
	<onetsoc onet="47-2044.00" soc="47-2044" />
	<onetsoc onet="47-2051.00" soc="47-2051" />
	<onetsoc onet="47-2053.00" soc="47-2053" />
	<onetsoc onet="47-2061.00" soc="47-2061" />
	<onetsoc onet="47-2071.00" soc="47-2071" />
	<onetsoc onet="47-2072.00" soc="47-2072" />
	<onetsoc onet="47-2073.00" soc="47-2073" />
	<onetsoc onet="47-2081.00" soc="47-2081" />
	<onetsoc onet="47-2082.00" soc="47-2082" />
	<onetsoc onet="47-2111.00" soc="47-2111" />
	<onetsoc onet="47-2121.00" soc="47-2121" />
	<onetsoc onet="47-2131.00" soc="47-2131" />
	<onetsoc onet="47-2132.00" soc="47-2132" />
	<onetsoc onet="47-2141.00" soc="47-2141" />
	<onetsoc onet="47-2142.00" soc="47-2142" />
	<onetsoc onet="47-2151.00" soc="47-2151" />
	<onetsoc onet="47-2152.00" soc="47-2152" />
	<onetsoc onet="47-2152.01" soc="47-2152" />
	<onetsoc onet="47-2152.02" soc="47-2152" />
	<onetsoc onet="47-2161.00" soc="47-2161" />
	<onetsoc onet="47-2171.00" soc="47-2171" />
	<onetsoc onet="47-2181.00" soc="47-2181" />
	<onetsoc onet="47-2211.00" soc="47-2211" />
	<onetsoc onet="47-2221.00" soc="47-2221" />
	<onetsoc onet="47-2231.00" soc="47-2231" />
	<onetsoc onet="47-3011.00" soc="47-3011" />
	<onetsoc onet="47-3012.00" soc="47-3012" />
	<onetsoc onet="47-3013.00" soc="47-3013" />
	<onetsoc onet="47-3014.00" soc="47-3014" />
	<onetsoc onet="47-3015.00" soc="47-3015" />
	<onetsoc onet="47-3016.00" soc="47-3016" />
	<onetsoc onet="47-3019.00" soc="47-3019" />
	<onetsoc onet="47-4011.00" soc="47-4011" />
	<onetsoc onet="47-4021.00" soc="47-4021" />
	<onetsoc onet="47-4031.00" soc="47-4031" />
	<onetsoc onet="47-4041.00" soc="47-4041" />
	<onetsoc onet="47-4051.00" soc="47-4051" />
	<onetsoc onet="47-4061.00" soc="47-4061" />
	<onetsoc onet="47-4071.00" soc="47-4071" />
	<onetsoc onet="47-4091.00" soc="47-4091" />
	<onetsoc onet="47-4099.00" soc="47-4099" />
	<onetsoc onet="47-4099.02" soc="47-4099" />
	<onetsoc onet="47-4099.03" soc="47-4099" />
	<onetsoc onet="47-5011.00" soc="47-5011" />
	<onetsoc onet="47-5012.00" soc="47-5012" />
	<onetsoc onet="47-5013.00" soc="47-5013" />
	<onetsoc onet="47-5021.00" soc="47-5021" />
	<onetsoc onet="47-5031.00" soc="47-5031" />
	<onetsoc onet="47-5041.00" soc="47-5041" />
	<onetsoc onet="47-5042.00" soc="47-5042" />
	<onetsoc onet="47-5049.00" soc="47-5049" />
	<onetsoc onet="47-5051.00" soc="47-5051" />
	<onetsoc onet="47-5061.00" soc="47-5061" />
	<onetsoc onet="47-5071.00" soc="47-5071" />
	<onetsoc onet="47-5081.00" soc="47-5081" />
	<onetsoc onet="47-5099.00" soc="47-5099" />
	<onetsoc onet="49-1011.00" soc="49-1011" />
	<onetsoc onet="49-2011.00" soc="49-2011" />
	<onetsoc onet="49-2021.00" soc="49-2021" />
	<onetsoc onet="49-2021.01" soc="49-2021" />
	<onetsoc onet="49-2022.00" soc="49-2022" />
	<onetsoc onet="49-2091.00" soc="49-2091" />
	<onetsoc onet="49-2092.00" soc="49-2092" />
	<onetsoc onet="49-2093.00" soc="49-2093" />
	<onetsoc onet="49-2094.00" soc="49-2094" />
	<onetsoc onet="49-2095.00" soc="49-2095" />
	<onetsoc onet="49-2096.00" soc="49-2096" />
	<onetsoc onet="49-2097.00" soc="49-2097" />
	<onetsoc onet="49-2098.00" soc="49-2098" />
	<onetsoc onet="49-3011.00" soc="49-3011" />
	<onetsoc onet="49-3021.00" soc="49-3021" />
	<onetsoc onet="49-3022.00" soc="49-3022" />
	<onetsoc onet="49-3023.00" soc="49-3023" />
	<onetsoc onet="49-3023.01" soc="49-3023" />
	<onetsoc onet="49-3023.02" soc="49-3023" />
	<onetsoc onet="49-3031.00" soc="49-3031" />
	<onetsoc onet="49-3041.00" soc="49-3041" />
	<onetsoc onet="49-3042.00" soc="49-3042" />
	<onetsoc onet="49-3043.00" soc="49-3043" />
	<onetsoc onet="49-3051.00" soc="49-3051" />
	<onetsoc onet="49-3052.00" soc="49-3052" />
	<onetsoc onet="49-3053.00" soc="49-3053" />
	<onetsoc onet="49-3091.00" soc="49-3091" />
	<onetsoc onet="49-3092.00" soc="49-3092" />
	<onetsoc onet="49-3093.00" soc="49-3093" />
	<onetsoc onet="49-9011.00" soc="49-9011" />
	<onetsoc onet="49-9012.00" soc="49-9012" />
	<onetsoc onet="49-9021.00" soc="49-9021" />
	<onetsoc onet="49-9021.01" soc="49-9021" />
	<onetsoc onet="49-9021.02" soc="49-9021" />
	<onetsoc onet="49-9031.00" soc="49-9031" />
	<onetsoc onet="49-9041.00" soc="49-9041" />
	<onetsoc onet="49-9043.00" soc="49-9043" />
	<onetsoc onet="49-9044.00" soc="49-9044" />
	<onetsoc onet="49-9045.00" soc="49-9045" />
	<onetsoc onet="49-9051.00" soc="49-9051" />
	<onetsoc onet="49-9052.00" soc="49-9052" />
	<onetsoc onet="49-9061.00" soc="49-9061" />
	<onetsoc onet="49-9062.00" soc="49-9062" />
	<onetsoc onet="49-9063.00" soc="49-9063" />
	<onetsoc onet="49-9064.00" soc="49-9064" />
	<onetsoc onet="49-9069.00" soc="49-9069" />
	<onetsoc onet="49-9071.00" soc="49-9071" />
	<onetsoc onet="49-9081.00" soc="49-9081" />
	<onetsoc onet="49-9091.00" soc="49-9091" />
	<onetsoc onet="49-9092.00" soc="49-9092" />
	<onetsoc onet="49-9093.00" soc="49-9093" />
	<onetsoc onet="49-9094.00" soc="49-9094" />
	<onetsoc onet="49-9095.00" soc="49-9095" />
	<onetsoc onet="49-9096.00" soc="49-9096" />
	<onetsoc onet="49-9097.00" soc="49-9097" />
	<onetsoc onet="49-9098.00" soc="49-9098" />
	<onetsoc onet="49-9099.00" soc="49-9099" />
	<onetsoc onet="49-9099.01" soc="49-9099" />
	<onetsoc onet="51-1011.00" soc="51-1011" />
	<onetsoc onet="51-2011.00" soc="51-2011" />
	<onetsoc onet="51-2021.00" soc="51-2021" />
	<onetsoc onet="51-2022.00" soc="51-2022" />
	<onetsoc onet="51-2023.00" soc="51-2023" />
	<onetsoc onet="51-2031.00" soc="51-2031" />
	<onetsoc onet="51-2041.00" soc="51-2041" />
	<onetsoc onet="51-2091.00" soc="51-2091" />
	<onetsoc onet="51-2092.00" soc="51-2092" />
	<onetsoc onet="51-2093.00" soc="51-2093" />
	<onetsoc onet="51-2099.00" soc="51-2099" />
	<onetsoc onet="51-3011.00" soc="51-3011" />
	<onetsoc onet="51-3021.00" soc="51-3021" />
	<onetsoc onet="51-3022.00" soc="51-3022" />
	<onetsoc onet="51-3023.00" soc="51-3023" />
	<onetsoc onet="51-3091.00" soc="51-3091" />
	<onetsoc onet="51-3092.00" soc="51-3092" />
	<onetsoc onet="51-3093.00" soc="51-3093" />
	<onetsoc onet="51-3099.00" soc="51-3099" />
	<onetsoc onet="51-4011.00" soc="51-4011" />
	<onetsoc onet="51-4012.00" soc="51-4012" />
	<onetsoc onet="51-4021.00" soc="51-4021" />
	<onetsoc onet="51-4022.00" soc="51-4022" />
	<onetsoc onet="51-4023.00" soc="51-4023" />
	<onetsoc onet="51-4031.00" soc="51-4031" />
	<onetsoc onet="51-4032.00" soc="51-4032" />
	<onetsoc onet="51-4033.00" soc="51-4033" />
	<onetsoc onet="51-4034.00" soc="51-4034" />
	<onetsoc onet="51-4035.00" soc="51-4035" />
	<onetsoc onet="51-4041.00" soc="51-4041" />
	<onetsoc onet="51-4051.00" soc="51-4051" />
	<onetsoc onet="51-4052.00" soc="51-4052" />
	<onetsoc onet="51-4061.00" soc="51-4061" />
	<onetsoc onet="51-4062.00" soc="51-4062" />
	<onetsoc onet="51-4071.00" soc="51-4071" />
	<onetsoc onet="51-4072.00" soc="51-4072" />
	<onetsoc onet="51-4081.00" soc="51-4081" />
	<onetsoc onet="51-4111.00" soc="51-4111" />
	<onetsoc onet="51-4121.00" soc="51-4121" />
	<onetsoc onet="51-4121.06" soc="51-4121" />
	<onetsoc onet="51-4121.07" soc="51-4121" />
	<onetsoc onet="51-4122.00" soc="51-4122" />
	<onetsoc onet="51-4191.00" soc="51-4191" />
	<onetsoc onet="51-4192.00" soc="51-4192" />
	<onetsoc onet="51-4193.00" soc="51-4193" />
	<onetsoc onet="51-4194.00" soc="51-4194" />
	<onetsoc onet="51-4199.00" soc="51-4199" />
	<onetsoc onet="51-5111.00" soc="51-5111" />
	<onetsoc onet="51-5112.00" soc="51-5112" />
	<onetsoc onet="51-5113.00" soc="51-5113" />
	<onetsoc onet="51-6011.00" soc="51-6011" />
	<onetsoc onet="51-6021.00" soc="51-6021" />
	<onetsoc onet="51-6031.00" soc="51-6031" />
	<onetsoc onet="51-6041.00" soc="51-6041" />
	<onetsoc onet="51-6042.00" soc="51-6042" />
	<onetsoc onet="51-6051.00" soc="51-6051" />
	<onetsoc onet="51-6052.00" soc="51-6052" />
	<onetsoc onet="51-6061.00" soc="51-6061" />
	<onetsoc onet="51-6062.00" soc="51-6062" />
	<onetsoc onet="51-6063.00" soc="51-6063" />
	<onetsoc onet="51-6064.00" soc="51-6064" />
	<onetsoc onet="51-6091.00" soc="51-6091" />
	<onetsoc onet="51-6092.00" soc="51-6092" />
	<onetsoc onet="51-6093.00" soc="51-6093" />
	<onetsoc onet="51-6099.00" soc="51-6099" />
	<onetsoc onet="51-7011.00" soc="51-7011" />
	<onetsoc onet="51-7021.00" soc="51-7021" />
	<onetsoc onet="51-7031.00" soc="51-7031" />
	<onetsoc onet="51-7032.00" soc="51-7032" />
	<onetsoc onet="51-7041.00" soc="51-7041" />
	<onetsoc onet="51-7042.00" soc="51-7042" />
	<onetsoc onet="51-7099.00" soc="51-7099" />
	<onetsoc onet="51-8011.00" soc="51-8011" />
	<onetsoc onet="51-8012.00" soc="51-8012" />
	<onetsoc onet="51-8013.00" soc="51-8013" />
	<onetsoc onet="51-8021.00" soc="51-8021" />
	<onetsoc onet="51-8031.00" soc="51-8031" />
	<onetsoc onet="51-8091.00" soc="51-8091" />
	<onetsoc onet="51-8092.00" soc="51-8092" />
	<onetsoc onet="51-8093.00" soc="51-8093" />
	<onetsoc onet="51-8099.00" soc="51-8099" />
	<onetsoc onet="51-8099.01" soc="51-8099" />
	<onetsoc onet="51-8099.02" soc="51-8099" />
	<onetsoc onet="51-8099.03" soc="51-8099" />
	<onetsoc onet="51-8099.04" soc="51-8099" />
	<onetsoc onet="51-9011.00" soc="51-9011" />
	<onetsoc onet="51-9012.00" soc="51-9012" />
	<onetsoc onet="51-9021.00" soc="51-9021" />
	<onetsoc onet="51-9022.00" soc="51-9022" />
	<onetsoc onet="51-9023.00" soc="51-9023" />
	<onetsoc onet="51-9031.00" soc="51-9031" />
	<onetsoc onet="51-9032.00" soc="51-9032" />
	<onetsoc onet="51-9041.00" soc="51-9041" />
	<onetsoc onet="51-9051.00" soc="51-9051" />
	<onetsoc onet="51-9061.00" soc="51-9061" />
	<onetsoc onet="51-9071.00" soc="51-9071" />
	<onetsoc onet="51-9071.01" soc="51-9071" />
	<onetsoc onet="51-9071.06" soc="51-9071" />
	<onetsoc onet="51-9071.07" soc="51-9071" />
	<onetsoc onet="51-9081.00" soc="51-9081" />
	<onetsoc onet="51-9082.00" soc="51-9082" />
	<onetsoc onet="51-9083.00" soc="51-9083" />
	<onetsoc onet="51-9111.00" soc="51-9111" />
	<onetsoc onet="51-9121.00" soc="51-9121" />
	<onetsoc onet="51-9122.00" soc="51-9122" />
	<onetsoc onet="51-9123.00" soc="51-9123" />
	<onetsoc onet="51-9141.00" soc="51-9141" />
	<onetsoc onet="51-9151.00" soc="51-9151" />
	<onetsoc onet="51-9191.00" soc="51-9191" />
	<onetsoc onet="51-9192.00" soc="51-9192" />
	<onetsoc onet="51-9193.00" soc="51-9193" />
	<onetsoc onet="51-9194.00" soc="51-9194" />
	<onetsoc onet="51-9195.00" soc="51-9195" />
	<onetsoc onet="51-9195.03" soc="51-9195" />
	<onetsoc onet="51-9195.04" soc="51-9195" />
	<onetsoc onet="51-9195.05" soc="51-9195" />
	<onetsoc onet="51-9195.07" soc="51-9195" />
	<onetsoc onet="51-9196.00" soc="51-9196" />
	<onetsoc onet="51-9197.00" soc="51-9197" />
	<onetsoc onet="51-9198.00" soc="51-9198" />
	<onetsoc onet="51-9199.00" soc="51-9199" />
	<onetsoc onet="51-9199.01" soc="51-9199" />
	<onetsoc onet="53-1011.00" soc="53-1011" />
	<onetsoc onet="53-1021.00" soc="53-1021" />
	<onetsoc onet="53-1021.01" soc="53-1021" />
	<onetsoc onet="53-1031.00" soc="53-1031" />
	<onetsoc onet="53-2011.00" soc="53-2011" />
	<onetsoc onet="53-2012.00" soc="53-2012" />
	<onetsoc onet="53-2021.00" soc="53-2021" />
	<onetsoc onet="53-2022.00" soc="53-2022" />
	<onetsoc onet="53-2031.00" soc="53-2031" />
	<onetsoc onet="53-3011.00" soc="53-3011" />
	<onetsoc onet="53-3021.00" soc="53-3021" />
	<onetsoc onet="53-3022.00" soc="53-3022" />
	<onetsoc onet="53-3031.00" soc="53-3031" />
	<onetsoc onet="53-3032.00" soc="53-3032" />
	<onetsoc onet="53-3033.00" soc="53-3033" />
	<onetsoc onet="53-3041.00" soc="53-3041" />
	<onetsoc onet="53-3099.00" soc="53-3099" />
	<onetsoc onet="53-4011.00" soc="53-4011" />
	<onetsoc onet="53-4012.00" soc="53-4012" />
	<onetsoc onet="53-4013.00" soc="53-4013" />
	<onetsoc onet="53-4021.00" soc="53-4021" />
	<onetsoc onet="53-4031.00" soc="53-4031" />
	<onetsoc onet="53-4041.00" soc="53-4041" />
	<onetsoc onet="53-4099.00" soc="53-4099" />
	<onetsoc onet="53-5011.00" soc="53-5011" />
	<onetsoc onet="53-5021.00" soc="53-5021" />
	<onetsoc onet="53-5021.01" soc="53-5021" />
	<onetsoc onet="53-5021.02" soc="53-5021" />
	<onetsoc onet="53-5021.03" soc="53-5021" />
	<onetsoc onet="53-5022.00" soc="53-5022" />
	<onetsoc onet="53-5031.00" soc="53-5031" />
	<onetsoc onet="53-6011.00" soc="53-6011" />
	<onetsoc onet="53-6021.00" soc="53-6021" />
	<onetsoc onet="53-6031.00" soc="53-6031" />
	<onetsoc onet="53-6041.00" soc="53-6041" />
	<onetsoc onet="53-6051.00" soc="53-6051" />
	<onetsoc onet="53-6051.01" soc="53-6051" />
	<onetsoc onet="53-6051.07" soc="53-6051" />
	<onetsoc onet="53-6051.08" soc="53-6051" />
	<onetsoc onet="53-6061.00" soc="53-6061" />
	<onetsoc onet="53-6099.00" soc="53-6099" />
	<onetsoc onet="53-7011.00" soc="53-7011" />
	<onetsoc onet="53-7021.00" soc="53-7021" />
	<onetsoc onet="53-7031.00" soc="53-7031" />
	<onetsoc onet="53-7032.00" soc="53-7032" />
	<onetsoc onet="53-7033.00" soc="53-7033" />
	<onetsoc onet="53-7041.00" soc="53-7041" />
	<onetsoc onet="53-7051.00" soc="53-7051" />
	<onetsoc onet="53-7061.00" soc="53-7061" />
	<onetsoc onet="53-7062.00" soc="53-7062" />
	<onetsoc onet="53-7063.00" soc="53-7063" />
	<onetsoc onet="53-7064.00" soc="53-7064" />
	<onetsoc onet="53-7071.00" soc="53-7071" />
	<onetsoc onet="53-7072.00" soc="53-7072" />
	<onetsoc onet="53-7073.00" soc="53-7073" />
	<onetsoc onet="53-7081.00" soc="53-7081" />
	<onetsoc onet="53-7111.00" soc="53-7111" />
	<onetsoc onet="53-7121.00" soc="53-7121" />
	<onetsoc onet="53-7199.00" soc="53-7199" />
	<onetsoc onet="55-1011.00" soc="55-1011" />
	<onetsoc onet="55-1012.00" soc="55-1012" />
	<onetsoc onet="55-1013.00" soc="55-1013" />
	<onetsoc onet="55-1014.00" soc="55-1014" />
	<onetsoc onet="55-1015.00" soc="55-1015" />
	<onetsoc onet="55-1016.00" soc="55-1016" />
	<onetsoc onet="55-1017.00" soc="55-1017" />
	<onetsoc onet="55-1019.00" soc="55-1019" />
	<onetsoc onet="55-2011.00" soc="55-2011" />
	<onetsoc onet="55-2012.00" soc="55-2012" />
	<onetsoc onet="55-2013.00" soc="55-2013" />
	<onetsoc onet="55-3011.00" soc="55-3011" />
	<onetsoc onet="55-3012.00" soc="55-3012" />
	<onetsoc onet="55-3013.00" soc="55-3013" />
	<onetsoc onet="55-3014.00" soc="55-3014" />
	<onetsoc onet="55-3015.00" soc="55-3015" />
	<onetsoc onet="55-3016.00" soc="55-3016" />
	<onetsoc onet="55-3017.00" soc="55-3017" />
	<onetsoc onet="55-3018.00" soc="55-3018" />
	<onetsoc onet="55-3019.00" soc="55-3019" />
</onetsocs>'

INSERT INTO @OnetSOC 
(
	OnetCode,
	SOC2010Code
)
SELECT 
	S.c.value('@onet', 'NVARCHAR(10)'),
	S.c.value('@soc', 'NVARCHAR(10)')
FROM   
	@SOCXML.nodes('/onetsocs/onetsoc') S(c)
	
INSERT INTO [Library.OnetSOC]
(
	OnetId,
	SOCId
)
SELECT
	O.Id,
	S.Id
FROM 
	@OnetSOC TS
INNER JOIN [Library.Onet] O
	ON O.OnetCode = TS.OnetCode
INNER JOIN [Library.SOC] S
	ON S.SOC2010Code = TS.SOC2010Code
WHERE
	NOT EXISTS
	(
		SELECT
			1
		FROM
			[Library.OnetSOC] OS
		WHERE
			OS.OnetId = O.Id
			AND OS.SOCId = S.Id
	)
	
/* Following 92 onet codes do not exist....

11-3071.03
11-3111.00
11-3121.00
11-3131.00
11-9013.00
11-9013.01
11-9013.02
11-9013.03
11-9161.00
13-1074.00
13-1075.00
13-1131.00
13-1141.00
13-1151.00
13-1161.00
13-1199.06
13-2071.01
15-1111.00
15-1121.00
15-1121.01
15-1122.00
15-1131.00
15-1132.00
15-1133.00
15-1134.00
15-1141.00
15-1142.00
15-1143.00
15-1143.01
15-1151.00
15-1152.00
15-1199.00
15-1199.01
15-1199.02
15-1199.03
15-1199.04
15-1199.05
15-1199.06
15-1199.07
15-1199.08
15-1199.09
15-1199.10
15-1199.11
15-1199.12
17-2081.01
21-1094.00
23-1012.00
25-2051.00
25-2052.00
25-2053.00
25-2054.00
25-2059.00
25-2059.01
29-1125.01
29-1125.02
29-1128.00
29-1141.00
29-1141.01
29-1141.02
29-1141.03
29-1141.04
29-1151.00
29-1161.00
29-1171.00
29-1181.00
29-2035.00
29-2057.00
29-2092.00
29-2099.05
29-2099.06
29-2099.07
29-9092.00
31-1014.00
31-1015.00
31-9097.00
31-9099.02
33-9093.00
39-4031.00
39-7011.00
39-7012.00
43-3099.00
47-2231.00
49-2021.01
49-9071.00
49-9081.00
51-3099.00
51-5111.00
51-5112.00
51-5113.00
51-9151.00
53-2031.00
53-6061.00

*/