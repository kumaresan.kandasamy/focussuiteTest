BEGIN TRANSACTION

DELETE FROM [Config.EmailTemplate] WHERE EmailTemplateType = 29

INSERT INTO [Config.EmailTemplate]
(
	EmailTemplateType, 
	[Subject], 
	Body, 
	Salutation, 
	Recipient
)
VALUES 
(
	29, 
	'Your Veteran Priority of Service Job Alert', 
	'A new job has been posted that is a good match against your resume. 

To access the job posting, click on the following link:

#JOBLINKURL#

Ensure you are logged into #FOCUSCAREER# if you don''t see the posting straight away.', 
	'Dear #RECIPIENTNAME#', 
	NULL
)
--ROLLBACK TRANSACTION
COMMIT TRANSACTION