﻿IF NOT EXISTS(SELECT 1 FROM [Config.EmailTemplate] WHERE EmailTemplateType = 39)
BEGIN
	INSERT INTO [Config.EmailTemplate]
	(
		EmailTemplateType,
		[Subject],
		Body,
		Salutation,
		SenderEmailType
	)
	VALUES
	(
		39,
		'Pre-screening service information requested',
		'#HIRINGMANAGERNAME# at #EMPLOYERNAME# has requested more information about your job center’s pre-screening service in relation to the following job order that they have recently posted. 

Focus Job ID: #JOBID# 
Job title: #JOBTITLE# 
Expiry date: #JOBEXPIRYDATE#',
		'Dear #RECIPIENTNAME#',
		1
	)
END
