UPDATE
	[Config.ConfigurationItem] 
SET
	Value = REPLACE(Value, '{"key":"RequiredProgramOfStudyId","tag":"cf036"}', '{"key":"RequiredProgramOfStudyId","tag":"cf036"},{"key":"HomeBased","tag":"cf037"}')
WHERE [Key] = 'LensService'
	AND Value LIKE '%customFilters%'
	AND Value LIKE '%"serviceType":"Posting"%'
	AND Value NOT LIKE '%HomeBased%'
	
UPDATE
	[Config.ConfigurationItem] 
SET
	Value = REPLACE(Value, '{"key":"ResumeIndustries","tag":"cf026"}', '{"key":"ResumeIndustries","tag":"cf026"},{"key":"HomeBased","tag":"cf027"}')
WHERE [Key] = 'LensService'
	AND Value LIKE '%customFilters%'
	AND Value LIKE '%"serviceType":"Resume"%'
	AND Value NOT LIKE '%HomeBased%'