IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name='IX_Data.Application.PersonsCurrentOffice_PersonId' AND object_id = OBJECT_ID('[Data.Application.PersonsCurrentOffice]'))
BEGIN
	CREATE INDEX 
		[IX_Data.Application.PersonsCurrentOffice_PersonId] 
	ON 
		[Data.Application.PersonsCurrentOffice] (PersonId)
END
