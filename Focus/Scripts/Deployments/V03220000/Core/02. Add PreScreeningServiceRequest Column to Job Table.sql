IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Job' AND COLUMN_NAME = 'PreScreeningServiceRequest')
BEGIN
	ALTER TABLE
		[Data.Application.Job]
	ADD
		PreScreeningServiceRequest BIT NULL
END
