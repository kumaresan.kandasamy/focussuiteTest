
/****** Object:  View [dbo].[Data.Application.JobPostingReferralView]    Script Date: 11/19/2014 10:18:58 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Data.Application.JobPostingReferralView]'))
DROP VIEW [dbo].[Data.Application.JobPostingReferralView]
GO

/****** Object:  View [dbo].[Data.Application.JobPostingReferralView]    Script Date: 11/19/2014 10:18:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[Data.Application.JobPostingReferralView]
AS
SELECT
	Job.Id,
	Job.JobTitle,
	Job.EmployerId AS EmployerId,
	BusinessUnit.Name AS EmployerName,
	Job.AwaitingApprovalOn,
	[dbo].[Data.Application.GetBusinessDays](Job.AwaitingApprovalOn, GETDATE()) AS TimeInQueue,
	Person.FirstName AS EmployeeFirstName,
	Person.LastName AS EmployeeLastName,
	Employee.Id AS EmployeeId,
	Job.CourtOrderedAffirmativeAction,
	Job.FederalContractor,
	Job.ForeignLabourCertificationH2A,
	Job.ForeignLabourCertificationH2B,
	Job.ForeignLabourCertificationOther,
	Job.IsCommissionBased,
	Job.IsSalaryAndCommissionBased,
	Job.AssignedToId,
	Job.SuitableForHomeWorker
FROM
	[Data.Application.Job] AS Job WITH (NOLOCK)
	INNER JOIN [Data.Application.BusinessUnit] AS BusinessUnit WITH (NOLOCK) ON Job.BusinessUnitId = BusinessUnit.Id
	LEFT OUTER JOIN [Data.Application.Employee] AS Employee WITH (NOLOCK) ON Job.EmployeeId = Employee.Id
	LEFT OUTER JOIN [Data.Application.Person] AS Person WITH (NOLOCK) ON Employee.PersonId = Person.Id
WHERE 
	Job.ApprovalStatus = 1







GO


