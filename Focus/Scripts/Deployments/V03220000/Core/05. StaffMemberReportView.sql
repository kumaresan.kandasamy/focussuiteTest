﻿-- =============================================
-- Script Template
-- =============================================
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Data.Application.StaffMemberReportView]'))
DROP VIEW [dbo].[Data.Application.StaffMemberReportView]
GO

/****** Object:  View [dbo].[Data.Application.StaffMemberReportView]    Script Date: 11/20/2014 15:33:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[Data.Application.StaffMemberReportView]

AS
	SELECT 
		P.Id,
		P.FirstName,
		P.LastName,
		P.EmailAddress,
		PA.Line1,
		PA.CountyId,
		PA.StateId,
		PA.PostcodeZip,
		U.CreatedOn,
		U.Blocked
	FROM
		[Data.Application.Person] P
	INNER JOIN [Data.Application.User] U
		ON U.PersonId = P.Id
	LEFT OUTER JOIN [Data.Application.PersonAddress] PA
		ON PA.PersonId = P.Id
		AND PA.IsPrimary = 1
	WHERE
		U.UserType = 1


GO


