IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Report.JobSeekerActivityLog')
BEGIN
	CREATE TABLE [Report.JobSeekerActivityLog]
	(
		Id BIGINT NOT NULL PRIMARY KEY,
		JobSeekerId BIGINT NOT NULL,
		ActionTypeName NVARCHAR(200) NOT NULL,
		ActionDate DATETIME NOT NULL,
		FocusUserId BIGINT NOT NULL,
		StaffMemberID BIGINT,
		StaffExternalId NVARCHAR(255),
		OfficeId BIGINT,
		OfficeName NVARCHAR(100), 
		CONSTRAINT [FK_Report.JobSeekerActivityLog_JobSeekerId] FOREIGN KEY (JobSeekerId) REFERENCES [Report.JobSeeker] (Id),
		CONSTRAINT [FK_Report.JobSeekerActivityLog_StaffMemberID] FOREIGN KEY (StaffMemberID) REFERENCES [Report.StaffMember] (Id)
	)
END
GO

IF NOT EXISTS(SELECT 1 FROM [Report.JobSeekerActivityLog])
BEGIN
	DECLARE @NextId BIGINT
	
	SELECT @NextId = NextId FROM KeyTable
	
	INSERT INTO [Report.JobSeekerActivityLog] 
	(
		Id,
		JobSeekerId,
		ActionTypeName,
		ActionDate,
		FocusUserId,
		StaffMemberID,
		StaffExternalId,
		OfficeId,
		OfficeName
	)		
	SELECT 
		@NextId - 1 + ROW_NUMBER() OVER(ORDER BY AE.Id ASC),
		JS.Id,
		T.Name,
		AE.ActionedOn,
		U.Id,
		SM.Id,
		CASE U.UserType WHEN 1 THEN U.ExternalId ELSE NULL END,
		CASE U.UserType WHEN 1 THEN O.OfficeId ELSE NULL END,
		CASE U.UserType WHEN 1 THEN O.OfficeName ELSE NULL END
	FROM
		[Data.Application.JobSeekerActivityActionView] AEV
	INNER JOIN [Data.Core.ActionEvent] AE
		ON AE.Id = AEV.Id
	INNER JOIN [Data.Core.ActionType] T
		ON T.Id = AE.ActionTypeId
	INNER JOIN [Data.Application.User] U
		ON U.Id = AE.UserId
	INNER JOIN [Report.JobSeeker] JS
		ON JS.FocusPersonId = AEV.JobSeekerId
	LEFT JOIN [Report.StaffMember] SM
		ON SM.FocusPersonId = U.PersonId
	OUTER APPLY
	(
		SELECT TOP 1
			O.Id AS OfficeId,
			O.OfficeName
		FROM
			[Data.Application.PersonsCurrentOffice] PCO
		INNER JOIN [Data.Application.Office] O
			ON O.Id = PCO.OfficeId
		WHERE
			PCO.PersonId = U.PersonId
			AND PCO.StartTime <= AE.ActionedOn
		ORDER BY
			PCO.StartTime DESC
	) O
	
	UPDATE KeyTable SET NextId = NextId + (SELECT COUNT(1) FROM [Report.JobSeekerActivityLog])
END
