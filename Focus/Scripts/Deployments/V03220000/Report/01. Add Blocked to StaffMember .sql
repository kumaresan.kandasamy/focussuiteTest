﻿-- =============================================
-- Script Template
-- =============================================
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.StaffMember' AND COLUMN_NAME = 'Blocked')
BEGIN
	ALTER TABLE [Report.StaffMember] ADD [Blocked] BIT NOT NULL DEFAULT 0;
END
GO

IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.StaffMember')
BEGIN
  UPDATE [Report.StaffMember]
  SET [Report.StaffMember].Blocked = u.Blocked
  FROM [Report.StaffMember] sm
  JOIN [Data.Application.Person] p ON p.Id = sm.FocusPersonId
  JOIN [Data.Application.User] u on u.PersonId = p.Id
  END
GO