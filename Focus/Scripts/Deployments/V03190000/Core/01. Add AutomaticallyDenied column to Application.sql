﻿IF NOT EXISTS
(
	SELECT * FROM sys.columns
	INNER JOIN sys.tables on sys.columns.object_id = sys.tables.object_id
    WHERE sys.columns.[name] = 'AutomaticallyDenied'
	AND sys.tables.[name] = 'Data.Application.Application'
)
BEGIN
    ALTER TABLE [dbo].[Data.Application.Application]
	ADD [AutomaticallyDenied] [bit] NOT NULL DEFAULT 0
END