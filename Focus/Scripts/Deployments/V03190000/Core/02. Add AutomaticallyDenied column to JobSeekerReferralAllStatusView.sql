/****** Object:  View [dbo].[Data.Application.JobSeekerReferralAllStatusView]    Script Date: 10/10/2014 12:56:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER VIEW [dbo].[Data.Application.JobSeekerReferralAllStatusView]
AS
SELECT 
	[Application].Id, 
	[Resume].FirstName + ' ' + [Resume].LastName AS Name, 
	[Application].CreatedOn AS ReferralDate, 
    Job.JobTitle, 
    BusinessUnit.Name AS EmployerName, 
    Job.EmployerId AS EmployerId, 
    Job.PostingHtml AS Posting, 
    [Resume].PersonId AS CandidateId, 
    Job.Id AS JobId, 
    [Application].ApprovalRequiredReason, 
    [Resume].IsVeteran, 
    BusinessUnitAddress.TownCity AS Town, 
    BusinessUnitAddress.StateID, 
    [Resume].PersonId,
    [Application].ApplicationScore,
    [Person].AssignedToId,
    [Application].ApprovalStatus,
    [Application].ApplicationStatus,
    [Application].StatusLastChangedOn,
    [Application].StatusLastChangedBy,
    [Posting].LensPostingId,
	[Application].AutomaticallyDenied
FROM  
	dbo.[Data.Application.Application] AS [Application] WITH (NOLOCK) 
	INNER JOIN dbo.[Data.Application.Posting] AS Posting WITH (NOLOCK) ON [Application].PostingId = Posting.Id
	INNER JOIN dbo.[Data.Application.Job] AS Job WITH (NOLOCK) ON Posting.JobId = Job.Id 
	INNER JOIN dbo.[Data.Application.BusinessUnit] AS BusinessUnit WITH (NOLOCK) ON Job.BusinessUnitId = BusinessUnit.Id 
	INNER JOIN dbo.[Data.Application.BusinessUnitAddress] AS BusinessUnitAddress ON BusinessUnit.Id = BusinessUnitAddress.BusinessUnitId AND BusinessUnitAddress.IsPrimary = 1
	INNER JOIN dbo.[Data.Application.Resume] AS [Resume] WITH (NOLOCK) ON [Application].ResumeId = [Resume].Id
	INNER JOIN dbo.[Data.Application.Person] AS [Person] WITH (NOLOCK) ON [Resume].PersonId = [Person].Id




GO


