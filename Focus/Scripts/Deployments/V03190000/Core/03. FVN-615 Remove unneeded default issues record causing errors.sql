﻿/****** Object:  View [dbo].[DATA.APPLICATION.ISSUES]    Script Date: 13/10/2014 12:56:49 ******/

DELETE FROM [DBO].[DATA.APPLICATION.ISSUES]
WHERE EXISTS(
				SELECT 1 
				FROM [DBO].[DATA.APPLICATION.ISSUES] I2 
				WHERE [DBO].[DATA.APPLICATION.ISSUES].PERSONID = I2.PERSONID
				AND [DBO].[DATA.APPLICATION.ISSUES].ID <> I2.ID
				AND I2.InappropriateEmailAddress = 1
			)
				AND ([DBO].[DATA.APPLICATION.ISSUES].InappropriateEmailAddress IS NULL
				OR [DBO].[DATA.APPLICATION.ISSUES].InappropriateEmailAddress = 0)