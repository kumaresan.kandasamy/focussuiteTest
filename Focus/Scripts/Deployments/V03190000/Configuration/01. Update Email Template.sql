Update [Config.EmailTemplate]
Set Body = 
'#EMPLOYERNAME# has indicated an interest in you for the following position at their company:

#JOBTITLE#

To apply for this position, please submit a resume through Focus/Career by clicking the following link: #JOBLINKURL#
We ask that you reply to this request within three days.

Sincerely,

#SENDERNAME#'
WHERE [EmailTemplateType] = 2