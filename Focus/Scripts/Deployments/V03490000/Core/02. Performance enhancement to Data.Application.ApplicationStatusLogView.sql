IF EXISTS (SELECT * FROM sys.indexes i WHERE i.name = 'UQ_Data.Application.ResumeJob_Id')
	DROP INDEX [UQ_Data.Application.ResumeJob_Id] ON [dbo].[Data.Application.ResumeJob]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_Data.Application.ResumeJob_Id] ON [dbo].[Data.Application.ResumeJob] 
(
	[ResumeId] ASC,
	[EndYear] DESC,
	[StartYear] DESC,
	[Id] ASC
)
INCLUDE ( [JobTitle],
[Employer]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

IF EXISTS (SELECT * FROM sys.indexes i WHERE i.name = 'IX_Data.Application.Application_PostingId')
	DROP INDEX [IX_Data.Application.Application_PostingId] ON [dbo].[Data.Application.Application] 
GO
CREATE NONCLUSTERED INDEX [IX_Data.Application.Application_PostingId] ON [dbo].[Data.Application.Application] 
(
	[PostingId] ASC
)
INCLUDE
(
	[ApprovalStatus],
	[ApplicationScore],
	[ResumeId]
)
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'Data.Application.ApplicationStatusLogView')
	DROP VIEW [dbo].[Data.Application.ApplicationStatusLogView]
GO
CREATE VIEW [dbo].[Data.Application.ApplicationStatusLogView]
AS
WITH logs AS
(
	SELECT	StatusLog.Id AS StatusLogId, 
					StatusLog.ActionedOn AS ActionedOn, 
					StatusLog.NewStatus AS CandidateApplicationStatus, 
					StatusLog.OriginalStatus AS CandidateOriginalApplicationStatus, 
					StatusLog.EntityId AS ApplicationId,
					[Application].ApplicationScore AS CandidateApplicationScore,
					[Application].Id AS CandidateApplicationId,
					[Application].ApprovalStatus AS CandidateApprovalStatus,
					[Application].ResumeId,
					[Application].PostingId,
					(SELECT TOP 1 Id FROM dbo.[Data.Application.ResumeJob] WHERE ResumeId = [Application].ResumeId ORDER BY EndYear DESC, StartYear DESC, Id) AS ResumeJobId
	FROM  	dbo.[Data.Core.StatusLog] AS StatusLog WITH (NOLOCK)
	INNER JOIN dbo.[Data.Application.Application] AS [Application] WITH (NOLOCK) ON StatusLog.EntityId = [Application].Id 
)
SELECT	logs.StatusLogId AS Id,
				logs.ActionedOn AS ActionedOn,
				logs.CandidateApplicationScore,
				[Resume].FirstName AS CandidateFirstName,
				[Resume].LastName AS CandidateLastName,
				[Resume].YearsExperience AS CandidateYearsExperience,
				[Resume].IsVeteran AS CandidateIsVeteran,
				logs.CandidateApplicationStatus,
				logs.CandidateOriginalApplicationStatus,
				Job.Id AS JobId,
				LatestJob.JobTitle AS LatestJobTitle,
				LatestJob.Employer AS LatestJobEmployer,
				LatestJob.StartYear AS LatestJobStartYear,
				LatestJob.EndYear AS LatestJobEndYear,
				logs.CandidateApplicationId,
				Job.BusinessUnitId AS BusinessUnitId,
				Job.JobTitle AS JobTitle,
				[Resume].PersonId,
				logs.CandidateApprovalStatus,
				Job.VeteranPriorityEndDate,
				[Person].ProgramAreaId,
				[Person].DegreeId,
				[Resume].EducationCompletionDate
FROM  	logs
INNER JOIN dbo.[Data.Application.Resume] AS [Resume] WITH (NOLOCK) ON logs.ResumeId = [Resume].Id 
INNER JOIN dbo.[Data.Application.Person] AS [Person] WITH (NOLOCK) ON [Resume].PersonId = [Person].Id 
INNER JOIN dbo.[Data.Application.Posting] AS Posting WITH (NOLOCK) ON logs.PostingId = Posting.Id
INNER JOIN dbo.[Data.Application.Job] AS Job WITH (NOLOCK) ON Job.Id = [Posting].JobId 
LEFT OUTER JOIN dbo.[Data.Application.ResumeJob] LatestJob WITH (NOLOCK) ON logs.ResumeJobId = LatestJob.Id
GO
