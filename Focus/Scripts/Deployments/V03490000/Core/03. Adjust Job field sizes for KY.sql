ALTER TABLE [dbo].[Data.Application.Job] ALTER COLUMN OtherBenefitsDetails nvarchar(3000) NULL;
ALTER TABLE [dbo].[Data.Application.Job] ALTER COLUMN InterviewApplicationUrl nvarchar(4000) NULL;
ALTER TABLE [dbo].[Data.Application.Job] ALTER COLUMN InterviewDirectApplicationDetails nvarchar(2000) NULL;
ALTER TABLE [dbo].[Data.Application.Job] ALTER COLUMN InterviewOtherInstructions nvarchar(MAX) NULL;
ALTER TABLE [dbo].[Data.Application.Job] ALTER COLUMN MinimumAgeReason nvarchar(500) NULL;
ALTER TABLE [dbo].[Data.Application.JobPrevData] ALTER COLUMN RecruitmentInformation nvarchar (MAX) NULL;

EXEC sp_refreshview '[Data.Application.ReferralView]';

