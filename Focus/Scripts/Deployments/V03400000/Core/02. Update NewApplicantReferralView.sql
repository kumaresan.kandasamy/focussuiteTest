IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = '')
BEGIN
	DROP VIEW [Data.Application.NewApplicantReferralView]
END
GO

CREATE VIEW [dbo].[Data.Application.NewApplicantReferralView]
AS
	SELECT 
		A.Id,
		P.Id AS PersonId,
		PO.JobTitle,
		PO.EmployerName,
		ApprovalRequiredReason AS ActionType,
		A.ApplicationStatus AS CandidateApplicationStatus,
		A.Id AS ApplicationId,
		R.IsVeteran,
		J.VeteranPriorityEndDate,
		BU.Name AS ActualEmployerName,
		A.ApprovalStatus
	FROM 
		[Data.Application.Person] P
	INNER JOIN [Data.Application.Resume] R
		ON R.PersonId = P.Id
	INNER JOIN [Data.Application.Application] A
		ON A.ResumeId = R.Id
	INNER JOIN [Data.Application.Posting] PO
		ON PO.Id = A.PostingId
	INNER JOIN [Data.Application.Job] J
		ON J.Id = PO.JobId
	INNER JOIN [Data.Application.BusinessUnit] BU
		ON BU.Id = J.BusinessUnitId
	WHERE
		PO.JobId IS NOT NULL
		AND A.ApplicationStatus = 3
GO