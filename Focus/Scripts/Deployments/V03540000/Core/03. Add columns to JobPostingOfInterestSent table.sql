IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.JobPostingOfInterestSent' AND COLUMN_NAME = 'QueueRecommendation')
BEGIN
	ALTER TABLE
		[Data.Application.JobPostingOfInterestSent]
	ADD
		QueueRecommendation bit NULL
END
GO

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.JobPostingOfInterestSent' AND COLUMN_NAME = 'CreatedBy')
BEGIN
	ALTER TABLE
		[Data.Application.JobPostingOfInterestSent]
	ADD
		CreatedBy bigint NULL
END
GO