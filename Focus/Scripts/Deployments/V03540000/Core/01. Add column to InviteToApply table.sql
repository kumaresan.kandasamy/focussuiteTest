IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.InviteToApply' AND COLUMN_NAME = 'QueueInvitation')
BEGIN
	ALTER TABLE
		[Data.Application.InviteToApply]
	ADD
		QueueInvitation bit NULL
END
GO