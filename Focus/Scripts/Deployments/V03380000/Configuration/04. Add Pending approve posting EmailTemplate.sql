﻿IF NOT EXISTS(SELECT 1 FROM [dbo].[Config.EmailTemplate] WHERE EmailTemplateType = 46)
BEGIN
	INSERT INTO [dbo].[Config.EmailTemplate]
	(
		EmailTemplateType,
		[Subject],
		Body,
		Salutation,
		SenderEmailType
	)
	VALUES
	(
		46,
		'Posting feedback #JOBTITLE#',
		'The #JOBTITLE# posting that you recently created has been approved. However your new account request is still under review.
Subject to approval of your account, your job will be automatically posted.

If you have any questions, please feel free to contact me at #SENDERPHONENUMBER# or #SENDEREMAILADDRESS#

Regards,
#SENDERNAME#',
		'Dear #RECIPIENTNAME#',
		1
	)
END
GO