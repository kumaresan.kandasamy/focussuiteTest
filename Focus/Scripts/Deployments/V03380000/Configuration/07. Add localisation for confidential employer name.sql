IF (SELECT Value FROM [Config.ConfigurationItem] WHERE [Key] = 'DefaultStateKey') = 'State.KY'
AND NOT EXISTS(SELECT * FROM [Config.LocalisationItem] WHERE [Key] = 'Global.ConfidentialEmployerLabel')
BEGIN
	DECLARE @LocalisationId BIGINT

	SELECT
		@LocalisationId = Localisation.Id
	FROM
		[Config.Localisation] Localisation
	WHERE
		Localisation.Culture = '**-**';

	INSERT INTO [Config.LocalisationItem]
	(
		[Key],
		Value,
		LocalisationId
	)
	VALUES
	(
		'Global.ConfidentialEmployerLabel',
		'{0} Business',
		@LocalisationId
	)
END

