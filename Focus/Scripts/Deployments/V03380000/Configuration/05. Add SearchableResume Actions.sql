IF OBJECT_ID('tempdb..#Messages') IS NOT NULL
BEGIN
   DROP TABLE #Messages
END

CREATE TABLE #Messages
(
	PersonId NVARCHAR(50),
	ResumeId NVARCHAR(50),
	EnqueuedOn DATETIME
)

DECLARE @NextId BIGINT
DECLARE @RegisterActionTypeId BIGINT
DECLARE @EntityTypeId BIGINT
DECLARE @ActionName NVARCHAR(20)

SET @ActionName = N'SearchableResume'

IF NOT EXISTS(SELECT 1 FROM [Data.Core.ActionType] WHERE Name = @ActionName)
BEGIN
	SELECT @NextId = NextId FROM KeyTable
	SET @RegisterActionTypeId = @NextId

	UPDATE KeyTable SET NextId = NextId + 1

	INSERT INTO [Data.Core.ActionType]
	(
		Id,
		Name,
		ReportOn,
		AssistAction
	)
	VALUES
	(
		@RegisterActionTypeId,
		@ActionName,
		0,
		0
	)
	
	SELECT @EntityTypeId = Id FROM [Data.Core.EntityType] WHERE Name = 'Resume'
	
	IF @EntityTypeId IS NULL
	BEGIN
		SELECT @EntityTypeId = NextId FROM KeyTable
		UPDATE KeyTable SET NextId = NextId + 1

		INSERT INTO [Data.Core.EntityType]
		(
			Id,
			Name
		)
		VALUES
		(
			@EntityTypeId,
			'Resume'
		)
	END
	
	INSERT INTO #Messages
	(
		PersonId,
		ResumeId,
		EnqueuedOn
	)
	SELECT 
		SUBSTRING(
			MP.Data, 
			CHARINDEX('"PersonId":', MP.Data) + 11, 
			CHARINDEX(',', MP.Data, CHARINDEX('"PersonId":', MP.Data)) - CHARINDEX('"PersonId":', MP.Data) - 11),
		SUBSTRING(
			MP.Data, 
			CHARINDEX('"ResumeId":', MP.Data) + 11, 
			CHARINDEX(',', MP.Data, CHARINDEX('"ResumeId":', MP.Data)) - CHARINDEX('"ResumeId":', MP.Data) - 11),
		DATEADD(HOUR, DATEDIFF(HOUR, GETUTCDATE(), GETDATE()), M.EnqueuedOn)
	FROM 
		[Messaging.Message] M
	INNER JOIN [Messaging.MessageType] T
		ON T.Id = M.MessageTypeId
	INNER JOIN [Messaging.MessagePayload] MP
		ON MP.Id = M.MessagePayloadID
	WHERE
		T.TypeName = 'Focus.Services.Messages.RegisterResumeMessage, Focus.Services, Version=2.0.6.0, Culture=neutral, PublicKeyToken=null'
	
	SELECT @NextId = NextId FROM KeyTable
	
	INSERT INTO dbo.[Data.Core.ActionEvent]
	(
		Id,
		SessionId,
		RequestId,
		UserId,
		ActionedOn,
		EntityId,
		EntityIdAdditional01,
		EntityIdAdditional02,
		AdditionalDetails,
		ActionTypeId,
		EntityTypeId,
		CreatedOn
	)
	SELECT 
		@NextId + (ROW_NUMBER() OVER (ORDER BY M.EnqueuedOn ASC)) - 1 AS Id,
		'00000000-0000-0000-0000-000000000000' AS SessionId,
		'00000000-0000-0000-0000-000000000000' AS RequestId,
		U.Id AS UserId,
		M.EnqueuedOn AS ActionedOn,
		CAST(M.ResumeId AS BIGINT) AS EntityId,
		NULL AS EntityIdAdditional01,
		NULL AS EntityIdAdditional02,
		NULL AS AdditionalDetails,
		@RegisterActionTypeId AS ActionTypeId,
		@EntityTypeId AS EntityTypeId,
		M.EnqueuedOn AS CreatedOn
	FROM 
		#Messages M
	INNER JOIN [Data.Application.User] U
		ON U.PersonId = CAST(M.PersonId AS BIGINT)
		
	UPDATE [KeyTable] SET NextId = NextId + @@ROWCOUNT
END
/*
SELECT
	E.*
FROM
	[Data.Core.ActionEvent] E
INNER JOIN [Data.Core.ActionType] T
	ON T.Id = E.ActionTypeID
WHERE
	T.[Name] = 'SearchableResume'
*/