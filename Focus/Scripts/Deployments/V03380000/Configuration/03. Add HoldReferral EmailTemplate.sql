--DELETE FROM [Config.EmailTemplate] WHERE EmailTemplateType = 45

IF NOT EXISTS(SELECT * FROM dbo.[Config.EmailTemplate] WHERE EmailTemplateType = 45)
BEGIN
	INSERT INTO dbo.[Config.EmailTemplate]
	(
		EmailTemplateType,
		[Subject],
		Body,
		Salutation,
		Recipient,
		SenderEmailType,
		ClientSpecificEmailAddress
	)
	VALUES
	(
		45,
		'Thank you for your referral request',
		'We have reviewed your referral request against #JOBTITLE# at #EMPLOYERNAME# but must place your request on temporary hold for the following reasons:
		
If you have any questions, please feel free to contact me at #SENDERPHONENUMBER# or #SENDEREMAILADDRESS#.
		
Regards, 
		
#SENDERNAME#',
		'Dear #RECIPIENTNAME#',
		NULL,
		0,
		NULL
	)
END
