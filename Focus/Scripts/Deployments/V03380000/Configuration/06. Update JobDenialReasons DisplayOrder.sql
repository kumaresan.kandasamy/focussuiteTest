DECLARE @Reasons TABLE
(
	[Key] NVARCHAR(100),
	DisplayOrder INT
)
INSERT INTO @Reasons ([Key], DisplayOrder)
VALUES 
	('JobDenialReasons.JobDuplicate', 1),
	('JobDenialReasons.JobLanguage', 2),
	('JobDenialReasons.JobRequired', 3),
	('JobDenialReasons.JobVerify', 4),
	('JobDenialReasons.OtherReason', 5)

UPDATE
	CGI
SET
	DisplayOrder = R.DisplayOrder
FROM 
	@Reasons R
INNER JOIN [Config.CodeItem] CI
	ON CI.[Key] = R.[Key]
INNER JOIN [Config.CodeGroupItem] CGI
	ON CGI.CodeItemId = CI.Id
	
