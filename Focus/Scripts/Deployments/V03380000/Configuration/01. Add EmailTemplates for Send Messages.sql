IF NOT EXISTS(SELECT 1 FROM [dbo].[Config.EmailTemplate] WHERE EmailTemplateType = 43)
BEGIN
	INSERT INTO [dbo].[Config.EmailTemplate]
	(
		EmailTemplateType,
		[Subject],
		Body,
		Salutation,
		SenderEmailType
	)
	VALUES
	(
		43,
		'',
		'',
		'',
		1
	)
END
GO

IF NOT EXISTS(SELECT 1 FROM [dbo].[Config.EmailTemplate] WHERE EmailTemplateType = 44)
BEGIN
	INSERT INTO [dbo].[Config.EmailTemplate]
	(
		EmailTemplateType,
		[Subject],
		Body,
		Salutation,
		SenderEmailType
	)
	VALUES
	(
		44,
		'',
		'',
		'',
		1
	)
END
