﻿
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Data.Application.ReferralStatusReason')
BEGIN

  CREATE TABLE [dbo].[Data.Application.ReferralStatusReason](
	  [Id] [bigint] NOT NULL,
	  [ReasonId] [bigint] NOT NULL,
	  [OtherReasonText] [nvarchar](250) NULL,
	  [EmployerId] [bigint] NULL,
	  [BusinessUnitId] [bigint] NULL,
    [ApplicationId] bigint NULL,
    [JobId] bigint NULL,
    [ApprovalStatus] INT NOT NULL,
	  [EntityType] [int] NOT NULL,
   CONSTRAINT [PK_Data.Application.ReferralStatusReason] PRIMARY KEY CLUSTERED 
  (
	  [Id] ASC
  )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
  ) ON [PRIMARY]


  ALTER TABLE [dbo].[Data.Application.ReferralStatusReason]  WITH CHECK ADD  CONSTRAINT [FK_Data.Application.ReferralStatusReason_BusinessUnitId] FOREIGN KEY([BusinessUnitId])
  REFERENCES [dbo].[Data.Application.BusinessUnit] ([Id])

  ALTER TABLE [dbo].[Data.Application.ReferralStatusReason] CHECK CONSTRAINT [FK_Data.Application.ReferralStatusReason_BusinessUnitId]

  ALTER TABLE [dbo].[Data.Application.ReferralStatusReason]  WITH CHECK ADD  CONSTRAINT [FK_Data.Application.ReferralStatusReason_EmployerId] FOREIGN KEY([EmployerId])
  REFERENCES [dbo].[Data.Application.Employer] ([Id])

  ALTER TABLE [dbo].[Data.Application.ReferralStatusReason] CHECK CONSTRAINT [FK_Data.Application.ReferralStatusReason_EmployerId]

  ALTER TABLE [dbo].[Data.Application.ReferralStatusReason]  WITH CHECK ADD  CONSTRAINT [FK_Data.Application.ReferralStatusReason_ApplicationId] FOREIGN KEY([ApplicationId])
	REFERENCES [dbo].[Data.Application.Application] ([Id]);

	ALTER TABLE [dbo].[Data.Application.ReferralStatusReason] CHECK CONSTRAINT [FK_Data.Application.ReferralStatusReason_ApplicationId];

  ALTER TABLE [dbo].[Data.Application.ReferralStatusReason]  WITH CHECK ADD  CONSTRAINT [FK_Data.Application.ReferralStatusReason_JobId] FOREIGN KEY([JobId])
	REFERENCES [dbo].[Data.Application.Job] ([Id])

	ALTER TABLE [dbo].[Data.Application.ReferralStatusReason] CHECK CONSTRAINT [FK_Data.Application.ReferralStatusReason_JobId]

  ALTER TABLE [dbo].[Data.Application.ReferralStatusReason] ADD  CONSTRAINT [DF_Data.Application.ReferralStatusReason_ReasonId]  DEFAULT ((0)) FOR [ReasonId]

  ALTER TABLE [dbo].[Data.Application.ReferralStatusReason] ADD  CONSTRAINT [DF_Data.Application.ReferralStatusReason_EntityType]  DEFAULT ((0)) FOR [EntityType]

  ALTER TABLE [dbo].[Data.Application.ReferralStatusReason] ADD CONSTRAINT [DF_Data.Application.ReferralStatusReason_ApprovalStatus]  DEFAULT ((0)) FOR [ApprovalStatus]
END