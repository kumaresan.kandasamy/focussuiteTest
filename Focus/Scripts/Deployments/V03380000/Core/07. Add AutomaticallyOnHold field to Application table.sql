IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Application' AND COLUMN_NAME = 'AutomaticallyOnHold')
BEGIN
    ALTER TABLE 
		[dbo].[Data.Application.Application]
	ADD 
		AutomaticallyOnHold BIT NULL
END
GO