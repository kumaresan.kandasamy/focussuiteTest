IF  EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'Data.Application.JobSeekerActivityActionView')
BEGIN
	DROP VIEW [dbo].[Data.Application.JobSeekerActivityActionView]
END
GO

CREATE VIEW [dbo].[Data.Application.JobSeekerActivityActionView]
AS

SELECT 
	ae.Id, 
	ae.UserId,
	p.LastName + ', ' + p.FirstName AS 'UserName',
	ae.ActionedOn,
	at.Name AS 'ActionType',
	j.JobTitle,
	bu.Name AS 'BusinessUnitName',
	r1.PersonId AS 'JobSeekerId',
	ae.EntityIdAdditional01,
	ae.AdditionalDetails,
	ae.EntityIdAdditional02
FROM 
	[dbo].[Data.Core.ActionEvent] ae WITH (NOLOCK)
INNER JOIN [Data.Core.ActionType] at WITH (NOLOCK) 
	ON at.Id = ae.ActionTypeId
INNER JOIN [Data.Core.EntityType] et WITH (NOLOCK)
	ON et.Id = ae.EntityTypeId
INNER JOIN [Data.Application.User] u WITH (NOLOCK) 
	ON u.Id = ae.UserId
INNER JOIN [Data.Application.Person] p WITH (NOLOCK) 
	ON p.Id = u.PersonId
INNER JOIN [Data.Application.Application] a WITH (NOLOCK) 
	ON a.Id = ae.EntityId
INNER JOIN [Data.Application.Posting] po WITH (NOLOCK) 
	ON po.Id = a.PostingId
INNER JOIN [Data.Application.Job] j WITH (NOLOCK) 
	ON j.Id = po.JobId	
INNER JOIN [Data.Application.BusinessUnit] bu WITH (NOLOCK) 
	ON bu.Id = j.BusinessUnitId
INNER JOIN [Data.Application.Resume] r1 WITH (NOLOCK) 
	ON r1.Id = a.ResumeId
WHERE
	at.Name IN 
	(
		'ApproveCandidateReferral',
		'CreateCandidateApplication',
		'ReferralRequest',
		'UpdateApplicationStatusToRecommended',
		'UpdateApplicationStatusToFailedToShow',
		'UpdateApplicationStatusToHired',
		'UpdateApplicationStatusToInterviewScheduled',									
		'UpdateApplicationStatusToNewApplicant',
		'UpdateApplicationStatusToNotApplicable',
		'UpdateApplicationStatusToOfferMade',
		'UpdateApplicationStatusToNotHired',
		'UpdateApplicationStatusToUnderConsideration',
		'UpdateApplicationStatusToDidNotApply',
		'UpdateApplicationStatusToInterviewDenied',
		'UpdateApplicationStatusToRefusedOffer',
		'UpdateApplicationStatusToSelfReferred',
		'SelfReferral',
		'ReapplyReferralRequest',
		'AutoApprovedReferralBypass',
		'HoldCandidateReferral',
		'HoldCandidateReferral'
	)
	AND et.Name = 'Application'
UNION ALL
SELECT 
	ae.Id, 
	ae.UserId,
	p.LastName + ', ' + p.FirstName AS 'UserName',
	ae.ActionedOn,
	at.Name AS 'ActionType',
	NULL AS 'JobTitle',
	NULL AS 'BusinessUnitName',
	r2.PersonId AS 'JobSeekerId',
	ae.EntityIdAdditional01,
	ae.AdditionalDetails,
	ae.EntityIdAdditional02
FROM 
	[dbo].[Data.Core.ActionEvent] ae WITH (NOLOCK)
INNER JOIN [Data.Core.ActionType] at WITH (NOLOCK) 
	ON at.Id = ae.ActionTypeId
INNER JOIN [Data.Application.User] u WITH (NOLOCK) 
	ON u.Id = ae.UserId
INNER JOIN [Data.Application.Person] p WITH (NOLOCK) 
	ON p.Id = u.PersonId
INNER JOIN [Data.Application.Resume] r2 WITH (NOLOCK) 
	ON r2.Id = ae.EntityId
WHERE 
	at.Name IN 
	(
		'SaveResume',
		'CreateNewResume'
	)
UNION ALL
SELECT 
	ae.Id, 
	ae.UserId,
	p.LastName + ', ' + p.FirstName AS 'UserName',
	ae.ActionedOn,
	at.Name AS 'ActionType',
	NULL AS 'JobTitle',
	NULL AS 'BusinessUnitName',
	ae.EntityId AS 'JobSeekerId',
	ae.EntityIdAdditional01,
	ae.AdditionalDetails,
	ae.EntityIdAdditional02
FROM 
	[dbo].[Data.Core.ActionEvent] ae WITH (NOLOCK)
INNER JOIN [Data.Core.ActionType] at WITH (NOLOCK) 
	ON at.Id = ae.ActionTypeId
INNER JOIN [Data.Application.User] u WITH (NOLOCK) 
	ON u.Id = ae.UserId
INNER JOIN [Data.Application.Person] p WITH (NOLOCK) 
	ON p.Id = u.PersonId
WHERE
	at.Name IN
	(
		'AssignActivityToCandidate',
		'ChangeJobSeekerSsn',
		'MarkCandidateIssuesResolved',
		'UpdateJobSeekerAssignedStaffMember'
	)
UNION ALL
SELECT 
	ae.Id, 
	ae.UserId,
	p.LastName + ', ' + p.FirstName AS 'UserName',
	ae.ActionedOn,
	at.Name AS 'ActionType',
	j2.JobTitle,
	bu2.Name AS 'BusinessUnitName',
	ae.EntityIdAdditional01 AS 'JobSeekerId',
	ae.EntityIdAdditional01,
	ae.AdditionalDetails,
	ae.EntityIdAdditional02
FROM 
	[dbo].[Data.Core.ActionEvent] ae WITH (NOLOCK)
INNER JOIN [Data.Core.ActionType] at WITH (NOLOCK) 
	ON at.Id = ae.ActionTypeId
INNER JOIN [Data.Application.User] u WITH (NOLOCK) 
	ON u.Id = ae.UserId
INNER JOIN [Data.Application.Person] p WITH (NOLOCK) 
	ON p.Id = u.PersonId
INNER JOIN [Data.Application.Job] j2 WITH (NOLOCK) 
	ON j2.Id = ae.EntityId
INNER JOIN [Data.Application.BusinessUnit] bu2 WITH (NOLOCK) 
	ON bu2.Id = j2.BusinessUnitId
WHERE
	at.Name IN
	(
		'InviteJobSeekerToApply'
	)
UNION ALL
SELECT 
	ae.Id, 
	ae.UserId,
	p.LastName + ', ' + p.FirstName AS 'UserName',
	ae.ActionedOn,
	at.Name AS 'ActionType',
	pos.JobTitle,
	pos.EmployerName AS 'BusinessUnitName',
	ae.EntityIdAdditional01 AS 'JobSeekerId',
	ae.EntityIdAdditional01,
	ae.AdditionalDetails,
	ae.EntityIdAdditional02
FROM 
	[dbo].[Data.Core.ActionEvent] ae WITH (NOLOCK)
INNER JOIN [Data.Core.ActionType] at WITH (NOLOCK) 
	ON at.Id = ae.ActionTypeId
INNER JOIN [Data.Core.EntityType] et WITH (NOLOCK)
	ON et.Id = ae.EntityTypeId
INNER JOIN [Data.Application.User] u WITH (NOLOCK) 
	ON u.Id = ae.UserId
INNER JOIN [Data.Application.Person] p WITH (NOLOCK) 
	ON p.Id = u.PersonId
INNER JOIN [Data.Application.Posting] pos WITH (NOLOCK) 
	ON pos.Id = ae.EntityId
WHERE
	at.Name IN
	(
		'ExternalReferral',
		'SelfReferral'
	)
	AND et.Name = 'Posting'
UNION ALL
SELECT 
	ae.Id, 
	ae.UserId,
	p.LastName + ', ' + p.FirstName AS 'UserName',
	ae.ActionedOn,
	at.Name AS 'ActionType',
	j3.JobTitle,
	bu1.Name AS 'BusinessUnitName',
	ae.EntityId AS 'JobSeekerId',
	ae.EntityIdAdditional01,
	ae.AdditionalDetails,
	ae.EntityIdAdditional02
FROM 
	[dbo].[Data.Core.ActionEvent] ae WITH (NOLOCK)
INNER JOIN [Data.Core.ActionType] at WITH (NOLOCK) 
	ON at.Id = ae.ActionTypeId
INNER JOIN [Data.Application.User] u WITH (NOLOCK) 
	ON u.Id = ae.UserId
INNER JOIN [Data.Application.Person] p WITH (NOLOCK) 
	ON p.Id = u.PersonId
INNER JOIN [Data.Application.Job] j3 
	ON j3.Id = ae.EntityIdAdditional01
INNER JOIN [Data.Application.BusinessUnit] bu1 WITH (NOLOCK) 
	ON bu1.Id = j3.BusinessUnitId
WHERE
	at.Name IN
	(
		'StaffReferral'
	)


GO


