﻿DECLARE @Pattern NVARCHAR(250) = 'DENIED[_][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][_]'

WHILE EXISTS(SELECT 1 FROM [dbo].[Data.Application.User] WHERE PATINDEX('%' + @Pattern + '%', SUBSTRING(UserName, 2, LEN(UserName))) > 0)
BEGIN
      UPDATE 
            [dbo].[Data.Application.User]
      SET 
            UserName = SUBSTRING(UserName, 26, LEN(UserName))
      WHERE 
            PATINDEX('%' + @Pattern + '%', SUBSTRING(UserName, 2, LEN(UserName))) > 0
END
