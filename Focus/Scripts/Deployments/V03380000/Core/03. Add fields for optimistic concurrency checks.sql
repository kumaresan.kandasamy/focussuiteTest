SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.BusinessUnit' AND COLUMN_NAME = 'LockVersion')
BEGIN
	ALTER TABLE
		[dbo].[Data.Application.BusinessUnit]
	ADD
		LockVersion INT NOT NULL DEFAULT 0
END

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Employee' AND COLUMN_NAME = 'LockVersion')
BEGIN
	ALTER TABLE
		[dbo].[Data.Application.Employee]
	ADD
		LockVersion INT NOT NULL DEFAULT 0
END

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Employer' AND COLUMN_NAME = 'LockVersion')
BEGIN
	ALTER TABLE
		[dbo].[Data.Application.Employer]
	ADD
		LockVersion INT NOT NULL DEFAULT 0
END

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Application' AND COLUMN_NAME = 'LockVersion')
BEGIN
	ALTER TABLE
		[dbo].[Data.Application.Application]
	ADD
		LockVersion INT NOT NULL DEFAULT 0
END

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Job' AND COLUMN_NAME = 'LockVersion')
BEGIN
	ALTER TABLE
		[dbo].[Data.Application.Job]
	ADD
		LockVersion INT NOT NULL DEFAULT 0
END

GO

IF  EXISTS (SELECT 1 FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Data.Application.JobSeekerReferralView]'))
	DROP VIEW [dbo].[Data.Application.JobSeekerReferralView]
GO

CREATE VIEW [dbo].[Data.Application.JobSeekerReferralView]
AS
SELECT 
	[Application].Id, 
	[Resume].FirstName + ' ' + [Resume].LastName AS Name, 
	[Application].CreatedOn AS ReferralDate, 
    dbo.[Data.Application.GetBusinessDays]([Application].CreatedOn, GETDATE()) AS TimeInQueue, 
    Job.JobTitle, 
    BusinessUnit.Name AS EmployerName, 
    Job.EmployerId AS EmployerId, 
    Job.PostingHtml AS Posting, 
    [Resume].PersonId AS CandidateId, 
    Job.Id AS JobId, 
    [Application].ApprovalRequiredReason, 
    CASE WHEN [Resume].IsVeteran = 1 THEN 'Yes' WHEN [Resume].IsVeteran = 0 THEN 'No' ELSE '' END AS Veteran, 
    BusinessUnitAddress.TownCity AS Town, 
    '' AS [State],
    BusinessUnitAddress.StateID, 
    [Resume].PersonId,
    [Application].ApplicationScore,
    [Person].AssignedToId,
	[Application].LockVersion
FROM  
	dbo.[Data.Application.Application] AS [Application] WITH (NOLOCK) 
	INNER JOIN dbo.[Data.Application.Posting] AS Posting WITH (NOLOCK) ON [Application].PostingId = Posting.Id
	INNER JOIN dbo.[Data.Application.Job] AS Job WITH (NOLOCK) ON Posting.JobId = Job.Id 
	INNER JOIN dbo.[Data.Application.BusinessUnit] AS BusinessUnit WITH (NOLOCK) ON Job.BusinessUnitId = BusinessUnit.Id 
	INNER JOIN dbo.[Data.Application.BusinessUnitAddress] AS BusinessUnitAddress ON BusinessUnit.Id = BusinessUnitAddress.BusinessUnitId AND BusinessUnitAddress.IsPrimary = 1
	INNER JOIN dbo.[Data.Application.Resume] AS [Resume] WITH (NOLOCK) ON [Application].ResumeId = [Resume].Id
	INNER JOIN dbo.[Data.Application.Person] AS [Person] WITH (NOLOCK) ON [Resume].PersonId = [Person].Id
WHERE 
	([Application].ApplicationStatus = 1 AND [Application].ApprovalStatus = 1)


GO

IF  EXISTS (SELECT 1 FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Data.Application.JobSeekerReferralAllStatusView]'))
	DROP VIEW [dbo].[Data.Application.JobSeekerReferralAllStatusView]
GO

CREATE VIEW [dbo].[Data.Application.JobSeekerReferralAllStatusView]
AS
SELECT 
	[Application].Id, 
	[Resume].FirstName + ' ' + [Resume].LastName AS Name, 
	[Application].CreatedOn AS ReferralDate, 
    Job.JobTitle, 
    BusinessUnit.Name AS EmployerName, 
    Job.EmployerId AS EmployerId, 
    Job.PostingHtml AS Posting, 
    [Resume].PersonId AS CandidateId, 
    Job.Id AS JobId, 
    [Application].ApprovalRequiredReason, 
    [Resume].IsVeteran, 
    BusinessUnitAddress.TownCity AS Town, 
    BusinessUnitAddress.StateID, 
    [Resume].PersonId,
    [Application].ApplicationScore,
    [Person].AssignedToId,
    [Application].ApprovalStatus,
    [Application].ApplicationStatus,
    [Application].StatusLastChangedOn,
    [Application].StatusLastChangedBy,
    [Posting].LensPostingId,
	[Application].AutomaticallyDenied,
	Job.IsConfidential,
	Application.LockVersion
FROM  
	dbo.[Data.Application.Application] AS [Application] WITH (NOLOCK) 
	INNER JOIN dbo.[Data.Application.Posting] AS Posting WITH (NOLOCK) ON [Application].PostingId = Posting.Id
	INNER JOIN dbo.[Data.Application.Job] AS Job WITH (NOLOCK) ON Posting.JobId = Job.Id 
	INNER JOIN dbo.[Data.Application.BusinessUnit] AS BusinessUnit WITH (NOLOCK) ON Job.BusinessUnitId = BusinessUnit.Id 
	INNER JOIN dbo.[Data.Application.BusinessUnitAddress] AS BusinessUnitAddress ON BusinessUnit.Id = BusinessUnitAddress.BusinessUnitId AND BusinessUnitAddress.IsPrimary = 1
	INNER JOIN dbo.[Data.Application.Resume] AS [Resume] WITH (NOLOCK) ON [Application].ResumeId = [Resume].Id
	INNER JOIN dbo.[Data.Application.Person] AS [Person] WITH (NOLOCK) ON [Resume].PersonId = [Person].Id

GO

IF  EXISTS (SELECT 1 FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Data.Application.JobPostingReferralView]'))
	DROP VIEW [dbo].[Data.Application.JobPostingReferralView]
GO

CREATE VIEW [dbo].[Data.Application.JobPostingReferralView]
AS
SELECT
	Job.Id,
	Job.JobTitle,
	Job.EmployerId AS EmployerId,
	BusinessUnit.Name AS EmployerName,
	Job.AwaitingApprovalOn,
	[dbo].[Data.Application.GetBusinessDays](Job.AwaitingApprovalOn, GETDATE()) AS TimeInQueue,
	Person.FirstName AS EmployeeFirstName,
	Person.LastName AS EmployeeLastName,
	Employee.Id AS EmployeeId,
	Job.CourtOrderedAffirmativeAction,
	Job.FederalContractor,
	Job.ForeignLabourCertificationH2A,
	Job.ForeignLabourCertificationH2B,
	Job.ForeignLabourCertificationOther,
	Job.IsCommissionBased,
	Job.IsSalaryAndCommissionBased,
	Job.AssignedToId,
	Job.SuitableForHomeWorker,
	Job.ApprovalStatus,
    Job.JobLocationType,
    Job.CreditCheckRequired,
    Job.JobStatus,
	Job.LockVersion
FROM
	[Data.Application.Job] AS Job WITH (NOLOCK)
	INNER JOIN [Data.Application.BusinessUnit] AS BusinessUnit WITH (NOLOCK) ON Job.BusinessUnitId = BusinessUnit.Id
	LEFT OUTER JOIN [Data.Application.Employee] AS Employee WITH (NOLOCK) ON Job.EmployeeId = Employee.Id
	LEFT OUTER JOIN [Data.Application.Person] AS Person WITH (NOLOCK) ON Employee.PersonId = Person.Id
WHERE 
	Job.ApprovalStatus IN (1,3,5)

GO

IF  EXISTS (SELECT 1 FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Data.Application.EmployerAccountReferralView]'))
	DROP VIEW [dbo].[Data.Application.EmployerAccountReferralView]
GO

CREATE VIEW [dbo].[Data.Application.EmployerAccountReferralView] AS
WITH EmployersAndEmployees AS
(
      SELECT
            Employer.Id AS EmployerId, 
            Employer.Name AS EmployerName, 
            ROW_NUMBER() OVER (PARTITION BY Employer.Id ORDER BY Employee.Id) AS EmployerEmployeeNumber,
            Employee.Id AS EmployeeId, 
            Person.FirstName AS EmployeeFirstName, 
            Person.LastName AS EmployeeLastName, 
			[User].CreatedOn AS AccountCreationDate,
			dbo.[Data.Application.GetBusinessDays](ISNULL(Employee.AwaitingApprovalDate, [User].CreatedOn), GETDATE()) AS TimeInQueue, 
			dbo.[Data.Application.GetBusinessDays](BusinessUnit.AwaitingApprovalDate, GETDATE()) AS BusinessUnitTimeInQueue, 
			BusinessUnitAddress.Line1 AS EmployerAddressLine1, 
            BusinessUnitAddress.Line2 AS EmployerAddressLine2, 
			BusinessUnitAddress.TownCity AS EmployerAddressTownCity,
			BusinessUnitAddress.StateId AS EmployerAddressStateId, 
            BusinessUnitAddress.PostcodeZip AS EmployerAddressPostcodeZip, 
			BusinessUnitAddress.PublicTransitAccessible AS EmployerPublicTransitAccessible, 
			BusinessUnit.PrimaryPhone, 
            BusinessUnit.PrimaryPhoneType, 
			BusinessUnit.AlternatePhone1, 
			BusinessUnit.AlternatePhone1Type, 
			BusinessUnit.AlternatePhone2, 
			BusinessUnit.AlternatePhone2Type, 
            CASE WHEN BusinessUnit.PrimaryPhoneType = 'Phone' THEN BusinessUnit.PrimaryPhone WHEN BusinessUnit.PrimaryPhoneType = 'Mobile' THEN BusinessUnit.PrimaryPhone WHEN BusinessUnit.AlternatePhone1Type = 'Phone' THEN BusinessUnit.AlternatePhone1 WHEN BusinessUnit.AlternatePhone1Type = 'Mobile' THEN BusinessUnit.AlternatePhone1 WHEN BusinessUnit.AlternatePhone2Type = 'Phone' THEN BusinessUnit.AlternatePhone2 WHEN BusinessUnit.AlternatePhone2Type = 'Mobile' THEN BusinessUnit.AlternatePhone2 END AS EmployerPhoneNumber, 
            CASE WHEN BusinessUnit.PrimaryPhoneType = 'Fax' THEN BusinessUnit.PrimaryPhone WHEN BusinessUnit.AlternatePhone1Type = 'Fax' THEN BusinessUnit.AlternatePhone1 WHEN BusinessUnit.AlternatePhone2Type = 'Fax' THEN BusinessUnit.AlternatePhone2 END AS EmployerFaxNumber, 
			PhoneNumber.Number AS EmployeePhoneNumber, 
			FaxNumber.Number AS EmployeeFaxNumber, 
            BusinessUnit.Url AS EmployerUrl, 
			Employer.FederalEmployerIdentificationNumber AS EmployerFederalEmployerIdentificationNumber, 
            Employer.StateEmployerIdentificationNumber AS EmployerStateEmployerIdentificationNumber, 
			BusinessUnit.IndustrialClassification AS EmployerIndustrialClassification, 
            PersonAddress.Line1 AS EmployeeAddressLine1, 
			PersonAddress.Line2 AS EmployeeAddressLine2, 
			PersonAddress.TownCity AS EmployeeAddressTownCity, 
            PersonAddress.StateId AS EmployeeAddressStateId, 
			PersonAddress.PostcodeZip AS EmployeeAddressPostcodeZip, 
			Person.EmailAddress AS EmployeeEmailAddress, 
            Employee.ApprovalStatus AS EmployeeApprovalStatus,
			Employer.ApprovalStatus AS EmployerApprovalStatus, 
			BusinessUnit.ApprovalStatus As BusinessUnitApprovalStatus,
			BusinessUnit.OwnershipTypeId AS EmployerOwnershipTypeId, 
            Employer.AssignedToId AS EmployerAssignedToId, 
			Employee.RedProfanityWords, 
			Employee.YellowProfanityWords, 
			ISNULL(BusinessUnitDescription.Description, '') AS BusinessUnitDescription, 
            BusinessUnit.Name AS BusinessUnitName, 
			BusinessUnit.Id AS BusinessUnitId,
			 ROW_NUMBER() OVER (PARTITION BY BusinessUnit.Id ORDER BY Employee.Id) AS BusinessUnitEmployeeNumber,
			Employee.ApprovalStatusChangedBy, 
			Employee.ApprovalStatusChangedOn, 
            BusinessUnit.AccountTypeId AS EmployerAccountTypeId,
			BusinessUnit.IsPrimary AS EmployerIsPrimary,
			BusinessUnit.LegalName AS BusinessUnitLegalName,
			BusinessUnit.RedProfanityWords AS BusinessUnitRedProfanityWords,
			BusinessUnit.YellowProfanityWords AS BusinessUnitYellowProfanityWords,
			EmployeeBusinessUnit.[Default] AS BusinessUnitIsDefault,
			Person.SuffixId,
			CAST(Employer.LockVersion as nvarchar(10)) + '.' + CAST(BusinessUnit.LockVersion as nvarchar(10)) + '.' + CAST(Employee.LockVersion as nvarchar(10)) as AccountLockVersion
      FROM
      dbo.[Data.Application.Employer] AS Employer WITH (NOLOCK) 
      INNER JOIN dbo.[Data.Application.Employee] AS Employee WITH (NOLOCK) ON Employer.Id = Employee.EmployerId 
      INNER JOIN dbo.[Data.Application.Person] AS Person WITH (NOLOCK) ON Employee.PersonId = Person.Id    
	  INNER JOIN dbo.[Data.Application.User] AS [User] WITH (NOLOCK) ON Person.Id = [User].PersonId 
	  INNER JOIN dbo.[Data.Application.EmployeeBusinessUnit] AS EmployeeBusinessUnit WITH (NOLOCK) ON Employee.Id = EmployeeBusinessUnit.EmployeeId 
	  INNER JOIN dbo.[Data.Application.BusinessUnit] AS BusinessUnit WITH (NOLOCK) ON EmployeeBusinessUnit.BusinessUnitId = BusinessUnit.Id 
	  INNER JOIN dbo.[Data.Application.BusinessUnitAddress] AS BusinessUnitAddress WITH (NOLOCK) ON BusinessUnit.Id = BusinessUnitAddress.BusinessUnitId AND BusinessUnitAddress.IsPrimary = 1 
	  LEFT OUTER JOIN dbo.[Data.Application.BusinessUnitDescription] AS BusinessUnitDescription WITH (NOLOCK) ON BusinessUnit.Id = BusinessUnitDescription.BusinessUnitId AND BusinessUnitDescription.IsPrimary = 1 
	  LEFT OUTER JOIN dbo.[Data.Application.PersonAddress] AS PersonAddress WITH (NOLOCK) ON Person.Id = PersonAddress.PersonId 
	  LEFT OUTER JOIN dbo.[Data.Application.PhoneNumber] AS PhoneNumber WITH (NOLOCK) ON Person.Id = PhoneNumber.PersonId AND (PhoneNumber.PhoneType = 0 OR PhoneNumber.PhoneType = 1) AND PhoneNumber.IsPrimary = 1 
	  LEFT OUTER JOIN dbo.[Data.Application.PhoneNumber] AS FaxNumber WITH (NOLOCK) ON Person.Id = FaxNumber.PersonId AND FaxNumber.PhoneType = 4
)
SELECT 
	  Employer.EmployerId AS Id,
      Employer.EmployerId, 
      Employer.EmployerName, 
      Employer.EmployerApprovalStatus,
      1 AS TypeOfApproval,
      Employer.EmployeeId, 
      Employer.EmployeeFirstName, 
      Employer.EmployeeLastName, 
      Employer.EmployeeApprovalStatus,
	  Employer.BusinessUnitApprovalStatus,
	  Employer.TimeInQueue,
	  Employer.AccountCreationDate,
	  Employer.EmployerAddressLine1,
	  Employer.EmployerAddressLine2,
	  Employer.EmployerAddressTownCity,
	  Employer.EmployerAddressStateId,
	  Employer.EmployerAddressPostcodeZip,
	  Employer.EmployerPublicTransitAccessible,
	  Employer.PrimaryPhone,
	  Employer.PrimaryPhoneType,
	  Employer.AlternatePhone1,
	  Employer.AlternatePhone1Type,
	  Employer.AlternatePhone2,
	  Employer.AlternatePhone2Type,
	  Employer.EmployerPhoneNumber,
	  Employer.EmployerFaxNumber,
	  Employer.EmployeePhoneNumber,
	  Employer.EmployeeFaxNumber,
	  Employer.EmployerUrl,
	  Employer.EmployerFederalEmployerIdentificationNumber,
	  Employer.EmployerStateEmployerIdentificationNumber,
	  Employer.EmployerIndustrialClassification,
	  Employer.EmployeeAddressLine1,
	  Employer.EmployeeAddressLine2,
	  Employer.EmployeeAddressTownCity,
	  Employer.EmployeeAddressStateId,
	  Employer.EmployeeAddressPostcodeZip,
	  Employer.EmployeeEmailAddress,
	  Employer.EmployerOwnershipTypeId,
	  Employer.EmployerAssignedToId,
	  Employer.RedProfanityWords,
	  Employer.YellowProfanityWords,
	  Employer.BusinessUnitDescription,
	  Employer.BusinessUnitName,
	  Employer.BusinessUnitId,
	  Employer.ApprovalStatusChangedBy,
	  Employer.ApprovalStatusChangedOn,
	  Employer.EmployerAccountTypeId,
	  Employer.EmployerIsPrimary,
	  Employer.BusinessUnitLegalName,
	  Employer.BusinessUnitRedProfanityWords,
	  Employer.BusinessUnitYellowProfanityWords,
	  Employer.SuffixId,
	  Employer.AccountLockVersion
FROM
      EmployersAndEmployees AS Employer
WHERE 
      Employer.EmployerApprovalStatus IN (1, 4, 3)
      AND Employer.EmployerEmployeeNumber = 1
UNION 
SELECT
	  Employee.EmployeeId AS Id,
      Employee.EmployerId, 
      Employee.BusinessUnitName, 
      Employee.EmployerApprovalStatus,
      3 AS TypeOfApproval,
      Employee.EmployeeId, 
      Employee.EmployeeFirstName, 
      Employee.EmployeeLastName, 
      Employee.EmployeeApprovalStatus,
	  Employee.BusinessUnitApprovalStatus,
	  Employee.TimeInQueue,
	  Employee.AccountCreationDate,
	  Employee.EmployerAddressLine1,
	  Employee.EmployerAddressLine2,
	  Employee.EmployerAddressTownCity,
	  Employee.EmployerAddressStateId,
	  Employee.EmployerAddressPostcodeZip,
	  Employee.EmployerPublicTransitAccessible,
	  Employee.PrimaryPhone,
	  Employee.PrimaryPhoneType,
	  Employee.AlternatePhone1,
	  Employee.AlternatePhone1Type,
	  Employee.AlternatePhone2,
	  Employee.AlternatePhone2Type,
	  Employee.EmployerPhoneNumber,
	  Employee.EmployerFaxNumber,
	  Employee.EmployeePhoneNumber,
	  Employee.EmployeeFaxNumber,
	  Employee.EmployerUrl,
	  Employee.EmployerFederalEmployerIdentificationNumber,
	  Employee.EmployerStateEmployerIdentificationNumber,
	  Employee.EmployerIndustrialClassification,
	  Employee.EmployeeAddressLine1,
	  Employee.EmployeeAddressLine2,
	  Employee.EmployeeAddressTownCity,
	  Employee.EmployeeAddressStateId,
	  Employee.EmployeeAddressPostcodeZip,
	  Employee.EmployeeEmailAddress,
	  Employee.EmployerOwnershipTypeId,
	  Employee.EmployerAssignedToId,
	  Employee.RedProfanityWords,
	  Employee.YellowProfanityWords,
	  Employee.BusinessUnitDescription,
	  Employee.BusinessUnitName,
	  Employee.BusinessUnitId,
	  Employee.ApprovalStatusChangedBy,
	  Employee.ApprovalStatusChangedOn,
	  Employee.EmployerAccountTypeId,
	  Employee.EmployerIsPrimary,
	  Employee.BusinessUnitLegalName,
	  Employee.BusinessUnitRedProfanityWords,
	  Employee.BusinessUnitYellowProfanityWords,
	  Employee.SuffixId,
	  Employee.AccountLockVersion
FROM
      EmployersAndEmployees AS Employee
WHERE 
      Employee.EmployeeApprovalStatus IN (1, 4, 3)
      AND 
      (
		(Employee.BusinessUnitEmployeeNumber > 1 AND Employee.EmployerEmployeeNumber > 1)
		OR (Employee.EmployerApprovalStatus IN (0, 2) AND Employee.BusinessUnitApprovalStatus  IN (0, 2) AND Employee.BusinessUnitIsDefault = 1)
	  )
	  AND NOT EXISTS
	  (
		SELECT
			1
		FROM
			[Data.Application.EmployeeBusinessUnit] EBU
		INNER JOIN [Data.Application.BusinessUnit] BU
			ON BU.Id = EBU.BusinessUnitId
		WHERE
			EBU.EmployeeId = Employee.EmployeeId
			AND BU.Id <> Employee.BusinessUnitId
			AND BU.ApprovalStatus IN (1, 3, 4)
	  )
UNION
SELECT 
      BusinessUnit.BusinessUnitId AS Id,
	  BusinessUnit.EmployerId, 
      BusinessUnit.BusinessUnitName, 
      BusinessUnit.EmployerApprovalStatus,
      2 AS TypeOfApproval,
      BusinessUnit.EmployeeId, 
      BusinessUnit.EmployeeFirstName, 
      BusinessUnit.EmployeeLastName, 
      BusinessUnit.EmployeeApprovalStatus,
	  BusinessUnit.BusinessUnitApprovalStatus,
	  ISNULL(BusinessUnit.BusinessUnitTimeInQueue, BusinessUnit.TimeInQueue) AS TimeInQueue,
	  BusinessUnit.AccountCreationDate,
	  BusinessUnit.EmployerAddressLine1,
	  BusinessUnit.EmployerAddressLine2,
	  BusinessUnit.EmployerAddressTownCity,
	  BusinessUnit.EmployerAddressStateId,
	  BusinessUnit.EmployerAddressPostcodeZip,
	  BusinessUnit.EmployerPublicTransitAccessible,
	  BusinessUnit.PrimaryPhone,
	  BusinessUnit.PrimaryPhoneType,
	  BusinessUnit.AlternatePhone1,
	  BusinessUnit.AlternatePhone1Type,
	  BusinessUnit.AlternatePhone2,
	  BusinessUnit.AlternatePhone2Type,
	  BusinessUnit.EmployerPhoneNumber,
	  BusinessUnit.EmployerFaxNumber,
	  BusinessUnit.EmployeePhoneNumber,
	  BusinessUnit.EmployeeFaxNumber,
	  BusinessUnit.EmployerUrl,
	  BusinessUnit.EmployerFederalEmployerIdentificationNumber,
	  BusinessUnit.EmployerStateEmployerIdentificationNumber,
	  BusinessUnit.EmployerIndustrialClassification,
	  BusinessUnit.EmployeeAddressLine1,
	  BusinessUnit.EmployeeAddressLine2,
	  BusinessUnit.EmployeeAddressTownCity,
	  BusinessUnit.EmployeeAddressStateId,
	  BusinessUnit.EmployeeAddressPostcodeZip,
	  BusinessUnit.EmployeeEmailAddress,
	  BusinessUnit.EmployerOwnershipTypeId,
	  BusinessUnit.EmployerAssignedToId,
	  BusinessUnit.RedProfanityWords,
	  BusinessUnit.YellowProfanityWords,
	  BusinessUnit.BusinessUnitDescription,
	  BusinessUnit.BusinessUnitName,
	  BusinessUnit.BusinessUnitId,
	  BusinessUnit.ApprovalStatusChangedBy,
	  BusinessUnit.ApprovalStatusChangedOn,
	  BusinessUnit.EmployerAccountTypeId,
	  BusinessUnit.EmployerIsPrimary,
	  BusinessUnit.BusinessUnitLegalName,
	  BusinessUnit.BusinessUnitRedProfanityWords,
	  BusinessUnit.BusinessUnitYellowProfanityWords,
	  BusinessUnit.SuffixId,
	  BusinessUnit.AccountLockVersion
FROM
      EmployersAndEmployees AS BusinessUnit
WHERE 
      BusinessUnit.BusinessUnitApprovalStatus IN (1, 4, 3) 
      AND (BusinessUnit.EmployerIsPrimary = 0 OR BusinessUnit.EmployerApprovalStatus IN (0, 2))
	  AND BusinessUnit.BusinessUnitEmployeeNumber = 1

GO

IF  EXISTS (SELECT 1 FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Data.Application.ApplicationView]'))
	DROP VIEW [dbo].[Data.Application.ApplicationView]
GO

CREATE VIEW [dbo].[Data.Application.ApplicationView]
AS

SELECT
	[Application].Id AS Id,
	Job.Id AS JobId,
	Job.EmployerId AS EmployerId,
	Job.EmployeeId AS EmployeeId,
	Job.BusinessUnitId AS BusinessUnitId,
	[Resume].PersonId AS CandidateId,
	[Resume].FirstName AS CandidateFirstName,
	[Resume].LastName AS CandidateLastName,
	[Resume].YearsExperience AS CandidateYearsExperience,
	[Resume].IsVeteran AS CandidateIsVeteran,
	[Application].ApplicationStatus AS CandidateApplicationStatus,
	[Application].ApplicationScore AS CandidateApplicationScore,
	[Application].CreatedOn AS CandidateApplicationReceivedOn,
	[Application].ApprovalStatus AS CandidateApplicationApprovalStatus,
	[Resume].NcrcLevelId AS CandidateNcrcLevelId,
	[Application].StatusLastChangedOn AS ApplicationStatusLastChangedOn,
	[Application].Viewed AS Viewed,
	[Application].PostHireFollowUpStatus AS PostHireFollowUpStatus,
	[BusinessUnit].Name AS BusinuessUnitName,
	Person.FirstName AS EmployeeFirstName,
	Person.LastName AS EmployeeLastName,
	Person.EmailAddress AS EmployeeEmail,
	Job.ClosingOn AS JobClosingOn,
	Job.JobTitle,
	Job.VeteranPriorityEndDate,
	[Posting].LensPostingId,
	[PhoneNumber].Number AS EmployeePhoneNumber,
	[Application].AutomaticallyApproved AS CandidateApplicationAutomaticallyApproved,
	[Resume].IsContactInfoVisible AS CandidateIsContactInfoVisible,
	ISNULL([AddIn].[Description], '') AS Branding,
	Application.LockVersion
FROM [Data.Application.Application] AS [Application] WITH (NOLOCK)
	INNER JOIN [Data.Application.Posting] AS Posting WITH (NOLOCK) ON [Application].PostingId = Posting.Id
	INNER JOIN [Data.Application.Job] AS Job WITH (NOLOCK) ON Posting.JobId = Job.Id
	INNER JOIN [Data.Application.Resume] AS [Resume] WITH (NOLOCK) ON [Application].ResumeId = [Resume].Id
	INNER JOIN [Data.Application.BusinessUnit] AS [BusinessUnit] WITH (NOLOCK) ON Job.BusinessUnitId = [BusinessUnit].Id
	INNER JOIN [Data.Application.Employee] AS [Employee] WITH (NOLOCK) ON Job.EmployeeId = [Employee].Id
	INNER JOIN [Data.Application.Person] AS [Person] WITH (NOLOCK) ON Employee.PersonId = Person.Id
	LEFT OUTER JOIN [Data.Application.PhoneNumber] AS [PhoneNumber] WITH (NOLOCK) ON [Person].Id = [PhoneNumber].PersonId AND [PhoneNumber].IsPrimary = 1
	LEFT OUTER JOIN [Data.Application.ResumeAddIn] AS [AddIn] WITH (NOLOCK) ON [AddIn].ResumeId = [Resume].Id AND [AddIn].ResumeSectionType = 8192 -- Branding Section
GO