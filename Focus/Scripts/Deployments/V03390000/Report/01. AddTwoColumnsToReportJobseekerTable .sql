/****** Object:  Table [dbo].[Report.JobSeeker]    Script Date: 07/31/2015 11:28:12 ******/
/*
	script to add two additional columns:  
	
	HasAttendedTAP - Bit field defaulted to false
	AttendedTAPDate - datetime and can be null

*/


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'Report.JobSeeker' 
		AND (COLUMN_NAME = 'HasAttendedTAP' OR COLUMN_NAME = 'AttendedTAPDate'))
BEGIN

	ALTER TABLE [dbo].[Report.JobSeeker]
	ADD [HasAttendedTAP] bit NOT NULL DEFAULT 0,
			[AttendedTAPDate] datetime NULL

END

GO