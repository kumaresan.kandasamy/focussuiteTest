﻿/****** Object:  Table [dbo].[Report.JobSeeker]    Script Date: 06/08/2015 14:28:12 ******/
/*
	script to add additional column:  
	
	AccountType - int field

*/


SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'Report.JobSeeker' AND COLUMN_NAME = 'AccountType')
BEGIN

	ALTER TABLE [dbo].[Report.JobSeeker]
	ADD AccountType [int] NULL 

END

GO

/* Update Current Records */
UPDATE rjs
SET rjs.AccountType = p.AccountType
FROM [dbo].[Report.JobSeeker] rjs 
INNER JOIN [dbo].[Data.Application.Person] p ON p.Id = rjs.FocusPersonId