
/*********************************************
* FVN-2414 Addition of TAP date to reporting table for job seekers
* Script to update dbo.[Report.JobSeeker] HasAttendedTAP and AttendedTAPDate columns
* for the existing record 
*/


WITH  
	veterans AS(
	SELECT PersonId, CAST(ResumeXML AS XML) as convertedXml
	FROM dbo.[Data.Application.Resume] WHERE isVeteran = 1 AND IsDefault = 1
)

UPDATE dbo.[Report.JobSeeker] SET HasAttendedTAP = attendedFlag,  AttendedTAPDate = dateAttended
FROM dbo.[Report.JobSeeker] INNER JOIN 
(
	SELECT
		PersonId,
		N.C.value('resume[1]/statements[1]/personal[1]/veteran[1]/date_attended_tap[1]','datetime') AS dateAttended,
		N.C.value('resume[1]/statements[1]/personal[1]/veteran[1]/attended_tap_flag[1]','int') AS attendedFlag
	FROM veterans
	CROSS APPLY convertedXml.nodes('ResDoc') N(C)
	WHERE N.C.value('resume[1]/statements[1]/personal[1]/veteran[1]/attended_tap_flag[1]','int')  = 1 
	) AS veterans ON
dbo.[Report.JobSeeker].FocusPersonId = veterans.PersonId

GO