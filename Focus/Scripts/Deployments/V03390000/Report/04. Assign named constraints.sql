﻿-- Firstly remove the anonymous constraints

DECLARE @ContraintName sysname = null;

SELECT @ContraintName = dc.name
FROM sys.default_constraints dc
INNER JOIN sys.tables t ON dc.parent_object_id = t.object_id
WHERE t.name = 'Data.Application.BusinessUnit'
AND dc.is_system_named = 1;

IF @ContraintName IS NOT NULL
BEGIN
	PRINT 'Dropping Constraint: [' + @ContraintName + ']';
	EXEC ('ALTER TABLE [Data.Application.BusinessUnit] DROP CONSTRAINT [' + @ContraintName + ']');
END

-------------------------------------------------------------------------------------------------------
SET @ContraintName = null;

SELECT @ContraintName = dc.name
FROM sys.default_constraints dc
INNER JOIN sys.tables t ON dc.parent_object_id = t.object_id
WHERE t.name = 'Data.Application.Employee'
AND dc.is_system_named = 1;

IF @ContraintName IS NOT NULL
BEGIN
	PRINT 'Dropping Constraint: [' + @ContraintName + ']';
	EXEC ('ALTER TABLE [Data.Application.Employee] DROP CONSTRAINT [' + @ContraintName + ']');
END

-------------------------------------------------------------------------------------------------------
SET @ContraintName = null;

SELECT @ContraintName = dc.name
FROM sys.default_constraints dc
INNER JOIN sys.tables t ON dc.parent_object_id = t.object_id
WHERE t.name = 'Data.Application.Employer'
AND dc.is_system_named = 1;

IF @ContraintName IS NOT NULL
BEGIN
	PRINT 'Dropping Constraint: [' + @ContraintName + ']';
	EXEC ('ALTER TABLE [Data.Application.Employer] DROP CONSTRAINT [' + @ContraintName + ']');
END

-------------------------------------------------------------------------------------------------------
SET @ContraintName = null;

SELECT @ContraintName = dc.name
FROM sys.default_constraints dc
INNER JOIN sys.tables t ON dc.parent_object_id = t.object_id
WHERE t.name = 'Data.Application.Application'
AND dc.is_system_named = 1;

IF @ContraintName IS NOT NULL
BEGIN
	PRINT 'Dropping Constraint: [' + @ContraintName + ']';
	EXEC ('ALTER TABLE [Data.Application.Application] DROP CONSTRAINT [' + @ContraintName + ']');
END

-------------------------------------------------------------------------------------------------------
SET @ContraintName = null;

SELECT @ContraintName = dc.name
FROM sys.default_constraints dc
INNER JOIN sys.tables t ON dc.parent_object_id = t.object_id
WHERE t.name = 'Data.Application.Job'
AND dc.is_system_named = 1;

IF @ContraintName IS NOT NULL
BEGIN
	PRINT 'Dropping Constraint: [' + @ContraintName + ']';
	EXEC ('ALTER TABLE [Data.Application.Job] DROP CONSTRAINT [' + @ContraintName + ']');
END

-------------------------------------------------------------------------------------------------------
SET @ContraintName = null;

SELECT @ContraintName = dc.name
FROM sys.default_constraints dc
INNER JOIN sys.tables t ON dc.parent_object_id = t.object_id
WHERE t.name = 'Report.JobSeeker'
AND dc.is_system_named = 1;

IF @ContraintName IS NOT NULL
BEGIN
	PRINT 'Dropping Constraint: [' + @ContraintName + ']';
	EXEC ('ALTER TABLE [Report.JobSeeker] DROP CONSTRAINT [' + @ContraintName + ']');
END




--------------------------------------------------------------------------------------------------------------
-- NOW CREATE NAMED CONSTRAINTS
--------------------------------------------------------------------------------------------------------------


IF NOT EXISTS (SELECT * FROM sys.default_constraints WHERE name = 'DF_Data.Application.BusinessUnit_LockVersion')
BEGIN
	ALTER TABLE [dbo].[Data.Application.BusinessUnit]
	    ADD CONSTRAINT [DF_Data.Application.BusinessUnit_LockVersion] DEFAULT ((0)) FOR [LockVersion];
END

IF NOT EXISTS (SELECT * FROM sys.default_constraints WHERE name = 'DF_Data.Application.Employee_LockVersion')
BEGIN
	ALTER TABLE [dbo].[Data.Application.Employee]
	    ADD CONSTRAINT [DF_Data.Application.Employee_LockVersion] DEFAULT ((0)) FOR [LockVersion];
END

IF NOT EXISTS (SELECT * FROM sys.default_constraints WHERE name = 'DF_Data.Application.Employer_LockVersion')
BEGIN
	ALTER TABLE [dbo].[Data.Application.Employer]
			ADD CONSTRAINT [DF_Data.Application.Employer_LockVersion] DEFAULT ((0)) FOR [LockVersion];
END

IF NOT EXISTS (SELECT * FROM sys.default_constraints WHERE name = 'DF_Data.Application.Application_LockVersion')
BEGIN
	ALTER TABLE [dbo].[Data.Application.Application]
			ADD CONSTRAINT [DF_Data.Application.Application_LockVersion] DEFAULT ((0)) FOR [LockVersion];
END

IF NOT EXISTS (SELECT * FROM sys.default_constraints WHERE name = 'DF_Data.Application.Job_LockVersion')
BEGIN
	ALTER TABLE [dbo].[Data.Application.Job]
			ADD CONSTRAINT [DF_Data.Application.Job_LockVersion] DEFAULT ((0)) FOR [LockVersion];
END

IF NOT EXISTS (SELECT * FROM sys.default_constraints WHERE name = 'DF_Report.JobSeeker_HasAttendedTAP')
BEGIN

	ALTER TABLE [dbo].[Report.JobSeeker]
			ADD CONSTRAINT [DF_Report.JobSeeker_HasAttendedTAP] DEFAULT ((0)) FOR [HasAttendedTAP];
END


