﻿IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_Data.Application.ReferralStatusReason_JobId]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[Data.Application.ReferralStatusReason] DROP CONSTRAINT [DF_Data.Application.ReferralStatusReason_JobId]
END

GO