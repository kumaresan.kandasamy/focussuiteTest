IF (SELECT Value FROM [Config.ConfigurationItem] WHERE [Key] = 'DefaultStateKey') = 'State.KY'
AND (SELECT Value FROM [Config.ConfigurationItem] WHERE [Key] = 'IntegrationClient') = '3'
BEGIN
	PRINT 'Script should not be run for Kentucky'
	RETURN
END

DECLARE @Activities TABLE
(
	ActivityCategoryName NVARCHAR(250),
	ActivityType INT,
	OldActivityName NVARCHAR(250),
	NewActivityName NVARCHAR(250),
	NewActivityKey NVARCHAR(20)
)
INSERT INTO @Activities (ActivityCategoryName, ActivityType, OldActivityName, NewActivityName, NewActivityKey)
VALUES 
	('Contact / Information Provided', 0, '', 'Info: Job Fair Information', 'Activity.700'),
	('Contact / Information Provided', 0, '', 'Info: Veterans Priority of Service', 'Activity.701'),
	('Counseling / Case Management / Services (Non-Vets)', 0, '', 'Counseling:  Post-Placement', 'Activity.702'),
	('Counseling / Case Management / Services (Non-Vets)', 0, '', 'Service: Alternate Work Experience', 'Activity.703'),
	('Counseling / Case Management / Services (Vets)', 0, '', 'Service: Job Development Contact (DVOP)', 'Activity.704'),
	('Counseling / Case Management / Services (Vets)', 0, '', 'Service: Job Development Contact (LVER)', 'Activity.705'),
	('Counseling / Case Management / Services (Vets)', 0, '', 'Info: VRAP - Cannot contact customer', 'Activity.706'),
	('Counseling / Case Management / Services (Vets)', 0, '', 'Info: VRAP - Customer contact attempted', 'Activity.707'),
	('Counseling / Case Management / Services (Vets)', 0, '', 'Info: VRAP - Customer found employment', 'Activity.708'),
	('Counseling / Case Management / Services (Vets)', 0, '', 'Info: VRAP - Customer needs assistance', 'Activity.709'),
	('Counseling / Case Management / Services (Vets)', 0, '', 'Info: VRAP - Customer needs no assistance', 'Activity.710'),
	('Counseling / Case Management / Services (Vets)', 0, '', 'Service: Alternate Work Experience', 'Activity.711'),
	('Events', 0, '', 'Job Finding Club', 'Activity.712'),
	('Job Search', 0, '', 'Refused Referral (Talent feed)', 'Activity.713'),
	('Orientations & Workshops (Non-UI)', 0, '', 'Orientation: Trade', 'Activity.714'),
	('Program / Supported Service Referrals', 0, 'Bonding Assistance', 'Bonding Certificate Issued', 'Activity.422'),
	('Testing / Assessments', 0, '', 'Predictive Index Behavioral Assessment', 'Activity.715'),
	('Training Enrollments & Referrals', 0, '', 'Enrolled: TAA-CCCT', 'Activity.716'),
	('Training Enrollments & Referrals', 0, '', 'Referred: Dislocated Worker', 'Activity.717'),
	('Training Enrollments & Referrals', 0, '', 'Referred: TAA-CCCT', 'Activity.718'),
	('Training Enrollments & Referrals', 0, '', 'Referred: Training', 'Activity.719'),
	('Agreements / Services', 1, 'Service: Incumbant Worker Services', 'Service: Incumbent Worker Services', 'Activity.610'),
	('Contact / Information Provided', 1, '', 'Referred: Business Services', 'Activity.720'), 
	('Contact / Information Provided', 1, '', 'Info: Employer Company Profile', 'Activity.721'),
	('Contact / Information Provided', 1, '', 'Info: JobFit Employer Documentation', 'Activity.722'),
	('Contact / Information Provided', 1, '', 'Info: JobFit Surveys', 'Activity.723'),
	('Job & Applicant Management', 1, '', 'Employment / Work History', 'Activity.724'),
	('Testing', 1, '', 'TABE', 'Activity.725'),
	('Workshop / Seminars', 1, '', 'JobFit Employer Orientation', 'Activity.726'),
	('Workshop / Seminars', 1, '', 'JobFit Employer Training', 'Activity.727')

UPDATE
	A
SET
	Name = TA.NewActivityName
FROM
	@Activities TA
INNER JOIN [Config.Activity] A
	ON A.Name = TA.OldActivityName
INNER JOIN [Config.ActivityCategory] AC
	ON AC.Id = A.ActivityCategoryId
	AND AC.Name = TA.ActivityCategoryName
	AND AC.ActivityType = TA.ActivityType
WHERE
	TA.OldActivityName <> ''
	AND A.Name <> TA.NewActivityName
	
INSERT INTO [Config.Activity] (LocalisationKey, Name, Visible, Administrable, SortOrder, Used, ExternalId, ActivityCategoryId)
SELECT
	TA.NewActivityKey,
	TA.NewActivityName,
	1,
	1,
	0,
	0,
	0,
	AC.Id
FROM
	@Activities TA
INNER JOIN [Config.ActivityCategory] AC
	ON AC.Name = TA.ActivityCategoryName
	AND AC.ActivityType = TA.ActivityType
WHERE
	NOT EXISTS
	(
		SELECT
			1
		FROM
			[Config.Activity] A2
		INNER JOIN [Config.ActivityCategory] AC2
			ON AC2.Id = A2.ActivityCategoryId
		WHERE
			AC2.Name = TA.ActivityCategoryName
			AND AC2.ActivityType = TA.ActivityType
			AND A2.Name = TA.NewActivityName
	)
