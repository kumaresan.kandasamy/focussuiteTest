UPDATE
	A
SET
	Visible = 0,
	Administrable = 0,
	Name = 'Workshop: TAP (Not used)'
FROM
	[Config.Activity] A
INNER JOIN [Config.ActivityCategory] AC
	ON AC.Id = A.ActivityCategoryId
WHERE
	A.Name = 'Workshop: TAP'
	AND A.LocalisationKey <> 'Activity.135'
	AND EXISTS
	(
		SELECT
			1
		FROM
			[Config.Activity] A2
		WHERE
			A2.Name = A.Name
			AND A2.ActivityCategoryId = A2.ActivityCategoryId
			AND A2.LocalisationKey = 'Activity.135'
	)