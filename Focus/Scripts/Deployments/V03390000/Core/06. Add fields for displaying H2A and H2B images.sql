/****** Object:  View [dbo].[Data.Application.JobSeekerReferralView]    Script Date: 07/29/2015 09:36:37 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Data.Application.JobSeekerReferralView]'))
DROP VIEW [dbo].[Data.Application.JobSeekerReferralView]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[Data.Application.JobSeekerReferralView]
AS
SELECT
	[Application].Id,
	[Resume].FirstName + ' ' + [Resume].LastName AS Name,
	[Application].CreatedOn AS ReferralDate,
	dbo.[Data.Application.GetBusinessDays]([Application].CreatedOn, GETDATE()) AS TimeInQueue,
	Job.JobTitle,
	BusinessUnit.Name AS EmployerName,
	Job.EmployerId,
	Job.PostingHtml AS Posting,
	[Resume].PersonId AS CandidateId,
	Job.Id AS JobId, 
	[Application].ApprovalRequiredReason,
	CASE WHEN [Resume].IsVeteran = 1 THEN 'Yes' WHEN [Resume].IsVeteran = 0 THEN 'No' ELSE '' END AS Veteran,
	BusinessUnitAddress.TownCity AS Town, 
  '' AS [State], 
	BusinessUnitAddress.StateId, 
	[Resume].PersonId, 
	[Application].ApplicationScore, 
	[Person].AssignedToId,
	[Application].LockVersion, 
	Job.ForeignLabourCertificationH2A, 
  Job.ForeignLabourCertificationH2B
FROM
	dbo.[Data.Application.Application] AS [Application] WITH (NOLOCK)
	INNER JOIN dbo.[Data.Application.Posting] AS Posting WITH (NOLOCK) ON [Application].PostingId = Posting.Id
	INNER JOIN dbo.[Data.Application.Job] AS Job WITH (NOLOCK) ON Posting.JobId = Job.Id
	INNER JOIN dbo.[Data.Application.BusinessUnit] AS BusinessUnit WITH (NOLOCK) ON Job.BusinessUnitId = BusinessUnit.Id
	INNER JOIN dbo.[Data.Application.BusinessUnitAddress] AS BusinessUnitAddress ON BusinessUnit.Id = BusinessUnitAddress.BusinessUnitId AND BusinessUnitAddress.IsPrimary = 1
	INNER JOIN dbo.[Data.Application.Resume] AS [Resume] WITH (NOLOCK) ON [Application].ResumeId = [Resume].Id
	INNER JOIN dbo.[Data.Application.Person] AS [Person] WITH (NOLOCK) ON [Resume].PersonId = [Person].Id
WHERE
	[Application].ApplicationStatus = 1
AND
	[Application].ApprovalStatus = 1

GO




