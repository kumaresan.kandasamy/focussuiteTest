UPDATE
	E
SET
	EntityIdAdditional01 = U.PersonId
FROM
	[Data.Core.ActionType] T
INNER JOIN [Data.Core.ActionEvent] E
	ON E.ActionTypeId = T.Id
INNER JOIN [Data.Application.User] U
	ON U.Id = E.EntityId
WHERE
	T.[Name] = 'ActivateAccount'