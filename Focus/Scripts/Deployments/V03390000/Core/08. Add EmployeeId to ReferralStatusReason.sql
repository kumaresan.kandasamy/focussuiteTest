﻿IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.ReferralStatusReason' AND COLUMN_NAME = 'EmployeeId')
BEGIN
    ALTER TABLE 
		[dbo].[Data.Application.ReferralStatusReason]
	ADD 
		EmployeeId BIGINT NULL

  ALTER TABLE [dbo].[Data.Application.ReferralStatusReason]
    ADD CONSTRAINT [FK_Data.Application.ReferralStatusReason_EmployeeId] FOREIGN KEY ([EmployeeId])	REFERENCES [dbo].[Data.Application.Employee] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;


END
GO