IF NOT EXISTS(SELECT * FROM Sys.indexes WHERE Object_Id = OBJECT_ID('[Data.Application.Person]') AND [name] = 'IX_Data.Application.Person_SocialSecurityNumber')
BEGIN
	CREATE INDEX [IX_Data.Application.Person_SocialSecurityNumber] ON [Data.Application.Person] (SocialSecurityNumber)
END
