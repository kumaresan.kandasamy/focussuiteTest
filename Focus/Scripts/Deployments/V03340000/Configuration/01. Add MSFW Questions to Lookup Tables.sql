DECLARE @LookupType NVARCHAR(20)
DECLARE @CodeGroupId BIGINT
DECLARE @LocalisationId BIGINT

SET @LookupType = 'MSFWQuestions'

DECLARE @Lookups TABLE
(
	[Key] NVARCHAR(50),
	DisplayOrder INT,
	Value NVARCHAR(255),
	ExternalId NVARCHAR(10)
)

INSERT INTO @Lookups ([Key], DisplayOrder, Value, ExternalId)
VALUES 
(@LookupType + '.HalfFromFarmWork', 1, 'At least half of my income or my family�s income was from farm work', '1'),
(@LookupType + '.HalfFromFoodProcessing', 2, 'At least half of my income was from food processing work', '2'),
(@LookupType + '.25DaysFarmWork', 3, 'At least 25 days were spent doing farm work during the past year', '3'),
(@LookupType + '.25DaysFoodProcessing', 4, 'At least 25 days were spent doing food processing work during the past year', '4'),
(@LookupType + '.DifferentFarms', 5, 'I worked on different farms or for different food processors during the past year', '5')

IF NOT EXISTS(SELECT 1 FROM [Config.CodeGroup] WHERE [Key] = @LookupType)
BEGIN
	INSERT INTO [Config.CodeGroup] ([Key])
	VALUES (@LookupType)
END

INSERT INTO [Config.CodeItem] 
(
	[Key], 
	IsSystem,
	ExternalId
)
SELECT
	S.[Key],
	0,
	S.ExternalId
FROM
	@Lookups S
WHERE
	NOT EXISTS
	(
		SELECT
			1
		FROM
			[Config.CodeItem] CI
		WHERE
			CI.[Key] = S.[Key]
	)

UPDATE
	CI
SET
	ExternalId = S.ExternalId
FROM
	@Lookups S
INNER JOIN [Config.CodeItem] CI
	ON CI.[Key] = S.[Key]
WHERE
	ISNULL(CI.ExternalId, '') <> S.ExternalId	

SELECT
	@CodeGroupId = Id
FROM 
	[Config.CodeGroup]
WHERE
	[Key] = @LookupType

INSERT INTO [Config.CodeGroupItem]
(
	DisplayOrder,
	CodeGroupId,
	CodeItemId
)
SELECT
	S.DisplayOrder,
	@CodeGroupId,
	CI.Id
FROM
	@Lookups S
INNER JOIN [Config.CodeItem] CI
	ON CI.[Key] = S.[Key]
WHERE
	NOT EXISTS
	(
		SELECT
			1
		FROM
			[Config.CodeGroupItem] CGI
		WHERE
			CGI.CodeGroupId = @CodeGroupId
			AND CGI.CodeItemId = CI.Id
	)
	
UPDATE
	CGI
SET
	DisplayOrder = S.DisplayOrder
FROM
	@Lookups S
INNER JOIN [Config.CodeItem] CI
	ON CI.[Key] = S.[Key]
INNER JOIN [Config.CodeGroupItem] CGI
	ON CGI.CodeGroupId = @CodeGroupId
	AND CGI.CodeItemId = CI.Id
WHERE
	CGI.DisplayOrder <> S.DisplayOrder
	
SELECT 
	@LocalisationId = L.Id
FROM 
	[Config.Localisation] L
WHERE 
	L.Culture = '**-**'

INSERT INTO [Config.LocalisationItem]
(
	[Key],
	Value,
	LocalisationId
)
SELECT
	S.[Key],
	S.Value,
	@LocalisationId
FROM 
	@Lookups S
WHERE
	NOT EXISTS
	(
		SELECT 
			1
		FROM
			[Config.LocalisationItem] LI
		WHERE
			LI.[Key] = S.[Key]
	)

	
UPDATE
	LI
SET
	Value = S.Value
FROM
	@Lookups S
INNER JOIN [Config.LocalisationItem] LI
	ON LI.[Key] = S.[Key]
WHERE
	LI.Value <> S.Value