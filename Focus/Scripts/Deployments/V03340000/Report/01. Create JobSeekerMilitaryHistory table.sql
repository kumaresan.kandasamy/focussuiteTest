IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Report.JobSeekerMilitaryHistory')
BEGIN
	CREATE TABLE [Report.JobSeekerMilitaryHistory]
	(
		Id BIGINT NOT NULL PRIMARY KEY,
		JobSeekerId BIGINT NOT NULL,
		VeteranType INT NULL,
		VeteranDisability INT NULL,
		VeteranTransitionType INT NULL,
		VeteranMilitaryDischarge INT NULL,
		VeteranBranchOfService INT NULL,
		CampaignVeteran BIT NULL,
		MilitaryStartDate DATETIME NULL,
		MilitaryEndDate DATETIME NULL,
		CONSTRAINT [FK_Report.JobSeekerMilitaryHistory_JobSeekerId] FOREIGN KEY (JobSeekerId) REFERENCES [Report.JobSeeker] (Id)
	)
END
GO

IF NOT EXISTS(SELECT 1 FROM [Report.JobSeekerMilitaryHistory])
BEGIN
	DECLARE @NextID BIGINT
	
	SELECT @NextID = NextId FROM KeyTable
	
	INSERT INTO [Report.JobSeekerMilitaryHistory]
	(
		Id,
		JobSeekerId,
		VeteranType,
		VeteranMilitaryDischarge,
		VeteranBranchOfService,
		VeteranDisability,
		CampaignVeteran,
		VeteranTransitionType,
		MilitaryStartDate,
		MilitaryEndDate
	)
	SELECT 
		@NextId + ROW_NUMBER() OVER (ORDER BY Id ASC) - 1,
		Id,
		VeteranType,
		VeteranMilitaryDischarge,
		VeteranBranchOfService,
		VeteranDisability,
		CampaignVeteran,
		VeteranTransitionType,
		MilitaryStartDate,
		MilitaryEndDate
	FROM 
		[Report.JobSeeker] 
	WHERE 
		VeteranType IS NOT NULL
		
	UPDATE
		KeyTable
	SET
		NextId = NextId + @@ROWCOUNT
END

