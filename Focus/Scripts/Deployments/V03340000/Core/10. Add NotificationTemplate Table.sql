﻿
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Config.NotificationTemplate')
BEGIN
CREATE TABLE [Config.NotificationTemplate] (Id BIGINT NOT NULL PRIMARY KEY ,
[NotificationType] INT NOT NULL DEFAULT 0,
[Title] NVARCHAR(MAX) NOT NULL DEFAULT '',
[Text] NVARCHAR(MAX) NOT NULL DEFAULT '')
END
GO

DECLARE @NextId BIGINT



IF NOT EXISTS(SELECT TOP 1 1 FROM [Config.NotificationTemplate] WHERE NotificationType = 0)
begin
      SELECT @NextId = NextId FROM KeyTable
      INSERT INTO [Config.NotificationTemplate]
      (
            Id,
            NotificationType, 
            Title, 
            [Text]
      )
      VALUES 
      (
            @NextId,
            0, 
            'Invitation Notification',
            'You have received an invitation to apply for the position of #JOBTITLE#. Click on the job link to apply - #JOBLINK#'     
      )
end
      

IF NOT EXISTS(SELECT TOP 1 1 FROM [Config.NotificationTemplate] WHERE NotificationType = 1)
begin
      INSERT INTO [Config.NotificationTemplate]
      (
            Id,
            NotificationType, 
            Title, 
            [Text]
      )
      VALUES 
      (
            @NextId + 1,
            1, 
            'Referral Notification',
            'You have received a referral for the position of #JOBTITLE#.'
      )

      UPDATE KeyTable SET NextId = NextId + 2
end   
