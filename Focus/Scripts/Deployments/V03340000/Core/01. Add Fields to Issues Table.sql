IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME LIKE 'Data.Application.Issues' AND COLUMN_NAME = 'MigrantSeasonalFarmWorkerTriggered')
BEGIN
	ALTER TABLE
		[Data.Application.Issues]
	ADD
		MigrantSeasonalFarmWorkerTriggered BIT NOT NULL DEFAULT(0)
END
GO
