IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Data.Application.ResumeSelectedData')
BEGIN
	CREATE TABLE [Data.Application.ResumeSelectedData]
	(
		Id BIGINT NOT NULL PRIMARY KEY,
		ResumeId BIGINT NOT NULL,
		LookupType NVARCHAR(200) NOT NULL,
		LookupId BIGINT NOT NULL,
		CONSTRAINT [FK_Data.Application.ResumeSelectedData_ResumeId] FOREIGN KEY (ResumeId) REFERENCES [Data.Application.Resume] (Id)
	)
END
GO
