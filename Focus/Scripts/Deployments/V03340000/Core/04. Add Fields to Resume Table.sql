IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Resume' AND COLUMN_NAME = 'MigrantSeasonalFarmWorker')
BEGIN
	ALTER TABLE
		[Data.Application.Resume]
	ADD
		MigrantSeasonalFarmWorker BIT NULL		
END
GO