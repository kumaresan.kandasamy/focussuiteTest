IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Person' AND COLUMN_NAME = 'MigrantSeasonalFarmWorkerVerified')
BEGIN
	ALTER TABLE
		[Data.Application.Person]
	ADD
		MigrantSeasonalFarmWorkerVerified BIT NULL		
END
GO