﻿DECLARE @NextId BIGINT
SELECT @NextId = NextId FROM KeyTable

INSERT INTO [Data.Application.UserRole] (Id, UserId, RoleId)
SELECT
	@NextId,
	U.Id,
	R.Id
FROM
	[Data.Application.User] U
CROSS JOIN [Data.Application.Role] R
WHERE
	U.UserName = 'dev@client.com'
	AND R.[Key] = 'AssistJobSeekersAccountInactivator'
	AND NOT EXISTS
	(
		SELECT
			1
		FROM
			[Data.Application.UserRole] UR
		WHERE
			UR.UserId = U.Id
			AND UR.RoleId = R.Id
	)
	
UPDATE [KeyTable] SET NextId = NextId + 1