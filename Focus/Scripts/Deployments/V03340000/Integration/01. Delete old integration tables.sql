-- Delete old integration tables
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Data.Integration.ActionEvent]') AND type in (N'U'))
DROP TABLE [dbo].[Data.Integration.ActionEvent]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Data.Integration.ActionType]') AND type in (N'U'))
DROP TABLE [dbo].[Data.Integration.ActionType]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Data.Integration.EntityType]') AND type in (N'U'))
DROP TABLE [dbo].[Data.Integration.EntityType]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Data.Integration.ProviderMapping]') AND type in (N'U'))
DROP TABLE [dbo].[Data.Integration.ProviderMapping]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Data.Integration.ProviderType]') AND type in (N'U'))
DROP TABLE [dbo].[Data.Integration.ProviderType]
GO