SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF EXISTS (SELECT * FROM sys.all_views v where v.name = 'Library.MilitaryOccupationJobTitleView' )
BEGIN
	DROP VIEW [dbo].[Library.MilitaryOccupationJobTitleView]
END

GO

-- I have altered the below view to use the id from the military occupation rather than the id from the localisation item.
-- This means that the ids are unique for each of the records in the view.

CREATE VIEW [dbo].[Library.MilitaryOccupationJobTitleView]
AS

SELECT 
	MilitaryOccupation.Id AS Id,
	MilitaryOccupation.Id AS MilitaryOccupationId,
	MilitaryOccupation.Code + ' - ' + LocalisationItem.Value AS JobTitle,
	MilitaryOccupation.BranchOfServiceId AS BranchOfServiceId
FROM 
	[Library.MilitaryOccupation] AS MilitaryOccupation WITH (NOLOCK)
	INNER JOIN [Library.LocalisationItem] AS LocalisationItem WITH (NOLOCK) ON MilitaryOccupation.[Key] = LocalisationItem.[Key]
	
GO