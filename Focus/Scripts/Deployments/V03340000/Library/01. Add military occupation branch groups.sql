-- N.B.
--
-- The script below contains references to both config and library tables and so breaks the separation that there should be
-- between the 2 areas of the database.  However it is neccessary to do this as the config localisation items are already
-- referenced by the library tables and need to be inplace before the library data is created so have to be in the same file
-- as there is no guaruntee that config scripts will be run before library ones.
--
-- N.B.

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT *
    FROM [dbo].[Config.CodeItem]
    WHERE [Key] = 'MilitaryBranchOfService.AirForceReserves')
BEGIN

	DECLARE @MOCodeGroupId as bigint
	DECLARE @MRCodeGroupId as bigint
	declare @CultureId as bigint

	SELECT TOP 1 @MOCodeGroupId = CodeGroupId
		FROM [dbo].[Config.CodeGroupItem] cgi
		INNER JOIN [dbo].[Config.CodeItem] ci ON cgi.CodeItemId = ci.Id
		WHERE [Key] like 'MilitaryBranchOfService.%'

	SELECT TOP 1 @MRCodeGroupId = CodeGroupId
		FROM [dbo].[Config.CodeGroupItem] cgi
		INNER JOIN [dbo].[Config.CodeItem] ci ON cgi.CodeItemId = ci.Id
		WHERE [Key] like 'MilitaryRank.%'

	SELECT @CultureId = Id
		FROM [dbo].[Config.Localisation]
		WHERE Culture = '**-**'

	INSERT INTO [dbo].[Config.CodeItem]
			   ([Key]
			   ,[IsSystem]
			   ,[ParentKey]
			   ,[ExternalId]
			   ,[CustomFilterId])
		 VALUES
			   ('MilitaryBranchOfService.AirForceReserves',1,NULL,'USAFR',NULL),
			   ('MilitaryBranchOfService.AirNationalGuard',1,NULL,'USANG',NULL),
			   ('MilitaryBranchOfService.ArmyReserves',1,NULL,'USARMYR',NULL),
			   ('MilitaryBranchOfService.ArmyNationalGuard',1,NULL,'USARMYNG',NULL),
			   ('MilitaryBranchOfService.CoastGuardReserves',1,NULL,'USCGR',NULL),
			   ('MilitaryBranchOfService.MarineCorpsReserves',1,NULL,'USMCR',NULL),
			   ('MilitaryBranchOfService.NavyReserves',1,NULL,'USNAVYR',NULL)

	INSERT INTO [dbo].[Config.CodeGroupItem]
			   ([DisplayOrder]
			   ,[CodeGroupId]
			   ,[CodeItemId])
		 SELECT 
			0, @MOCodeGroupId, Id
		 FROM [dbo].[Config.CodeItem]
		 WHERE [Key] in ('MilitaryBranchOfService.AirForceReserves',
			   'MilitaryBranchOfService.AirNationalGuard',
			   'MilitaryBranchOfService.ArmyReserves',
			   'MilitaryBranchOfService.ArmyNationalGuard',
			   'MilitaryBranchOfService.CoastGuardReserves',
			   'MilitaryBranchOfService.MarineCorpsReserves',
			   'MilitaryBranchOfService.NavyReserves')

	INSERT INTO [dbo].[Config.LocalisationItem]
			   ([ContextKey]
			   ,[Key]
			   ,[Value]
			   ,[Localised]
			   ,[LocalisationId])
		 VALUES
			   ('','MilitaryBranchOfService.AirForceReserves','United States Air Force Reserves',0,@CultureId),
			   ('','MilitaryBranchOfService.AirNationalGuard','United States Air National Guard',0,@CultureId),
			   ('','MilitaryBranchOfService.ArmyReserves','United States Army Reserves',0,@CultureId),
			   ('','MilitaryBranchOfService.ArmyNationalGuard','United States Army National Guard',0,@CultureId),
			   ('','MilitaryBranchOfService.CoastGuardReserves','United States Coast Guards Reserves',0,@CultureId),
			   ('','MilitaryBranchOfService.MarineCorpsReserves','United States Marine Corps Reserves',0,@CultureId),
			   ('','MilitaryBranchOfService.NavyReserves','United States Navy Reserves',0,@CultureId)
	
		INSERT INTO [dbo].[Library.MilitaryOccupation]
           ([Code]
           ,[Key]
           ,[BranchOfServiceId]
           ,[IsCommissioned]
           ,[MilitaryOccupationGroupId])
		SELECT
			[Code]
           ,mo.[Key]
           ,nbos.Id as BranchOfServiceId
           ,[IsCommissioned]
           ,[MilitaryOccupationGroupId]
		FROM [dbo].[Library.MilitaryOccupation] mo
		INNER JOIN [Config.LookupItemsView] bos on mo.BranchOfServiceId = bos.Id
		CROSS JOIN [Config.LookupItemsView] nbos
		where (nbos.[Key] like (bos.[Key] + '%')
		or (nbos.[Key] = 'MilitaryBranchOfService.AirNationalGuard' and bos.[Key] = 'MilitaryBranchOfService.AirForce'))
		and nbos.[Key] <> bos.[Key]

		INSERT INTO [dbo].[Config.CodeItem]
			   ([Key]
			   ,[IsSystem]
			   ,[ParentKey]
			   ,[ExternalId]
			   ,[CustomFilterId])
			SELECT
				CASE
					WHEN liv.[Key] = 'MilitaryBranchOfService.AirForceReserves' THEN REPLACE(ci.[Key],'.AirForce','.AirForceReserves')
					WHEN liv.[Key] = 'MilitaryBranchOfService.AirNationalGuard' THEN REPLACE(ci.[Key],'.AirForce','.AirNationalGuard')
					WHEN liv.[Key] = 'MilitaryBranchOfService.ArmyReserves' THEN REPLACE(ci.[Key],'.Army','.ArmyReserves')
					WHEN liv.[Key] = 'MilitaryBranchOfService.ArmyNationalGuard' THEN REPLACE(ci.[Key],'.Army','.ArmyNationalGuard')
					WHEN liv.[Key] = 'MilitaryBranchOfService.CoastGuardReserves' THEN REPLACE(ci.[Key],'.CoastGuard','.CoastGuardReserves')
					WHEN liv.[Key] = 'MilitaryBranchOfService.MarineCorpsReserves' THEN REPLACE(ci.[Key],'.MarineCorps','.MarineCorpsReserves')
					WHEN liv.[Key] = 'MilitaryBranchOfService.NavyReserves' THEN REPLACE(ci.[Key],'.Navy','.NavyReserves')
				END
			   ,[IsSystem]
			   ,liv.[Key] as ParentKey
			   ,ci.[ExternalId]
			   ,ci.[CustomFilterId]
			FROM [dbo].[Config.CodeItem] ci
			CROSS JOIN [dbo].[Config.LookupItemsView] liv
			WHERE ci.[Key] LIKE 'MilitaryRank.%'
			and (liv.[Key] like (ci.[ParentKey] + '%')
			or (liv.[Key] = 'MilitaryBranchOfService.AirNationalGuard' and ci.[ParentKey] = 'MilitaryBranchOfService.AirForce'))
			and liv.[Key] <> ci.[ParentKey]

		INSERT INTO [dbo].[Config.CodeGroupItem]
			   ([DisplayOrder]
			   ,[CodeGroupId]
			   ,[CodeItemId])
		 SELECT 
			0, @MRCodeGroupId, ci.Id
		 FROM [dbo].[Config.CodeItem] ci
			WHERE ci.[Key] LIKE 'MilitaryRank.%'
			AND NOT EXISTS
			(
				SELECT * FROM [dbo].[Config.CodeGroupItem]
				WHERE CodeItemId = ci.Id
			)

		INSERT INTO [dbo].[Config.LocalisationItem]
			   ([ContextKey]
			   ,[Key]
			   ,[Value]
			   ,[Localised]
			   ,[LocalisationId])
		SELECT
			li.ContextKey,
			ci.[Key],
			li.[Value],
			li.[Localised],
			li.[LocalisationId]
		FROM [dbo].[Config.CodeItem] ci
		CROSS JOIN [dbo].[Config.LocalisationItem] li
		WHERE REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(ci.[Key]
			, '.AirForceReserves', '.AirForce')
			, '.AirNationalGuard', '.AirForce')
			, '.ArmyReserves', '.Army')
			, '.ArmyNationalGuard', '.Army')
			, '.CoastGuardReserves', '.CoastGuard')
			, '.MarineCorpsReserves', '.MarineCorps')
			, '.NavyReserves', '.Navy')
			= li.[Key]
		AND ci.[Key] LIKE 'MilitaryRank.%'
		AND NOT EXISTS
			(
				SELECT * FROM [dbo].[Config.LocalisationItem]
				WHERE [Key] = ci.[Key]
			)

END