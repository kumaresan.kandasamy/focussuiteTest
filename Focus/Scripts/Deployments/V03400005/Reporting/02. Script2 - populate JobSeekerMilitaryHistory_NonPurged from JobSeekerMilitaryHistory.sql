DELETE FROM [dbo].[Report.JobSeekerMilitaryHistory_NonPurged]

DECLARE @NextId BIGINT
SELECT @NextId = NextId FROM KeyTable

INSERT INTO [dbo].[Report.JobSeekerMilitaryHistory_NonPurged]
	([Id]
	  ,[VeteranDisability]
	  ,[VeteranTransitionType]
	  ,[VeteranMilitaryDischarge]
	  ,[VeteranBranchOfService]
	  ,[CampaignVeteran]
	  ,[MilitaryStartDate]
	  ,[MilitaryEndDate]
	  ,[JobSeekerId]
	  ,[CreatedOn]
	  ,[UpdatedOn]
	  ,[DeletedOn]
	  ,[VeteranType])
SELECT @NextId + ROW_NUMBER() OVER (ORDER BY JSMH.Id ASC)
	  ,JSMH.[VeteranDisability]
	  ,JSMH.[VeteranTransitionType]
	  ,JSMH.[VeteranMilitaryDischarge]
	  ,JSMH.[VeteranBranchOfService]
	  ,JSMH.[CampaignVeteran]
	  ,JSMH.[MilitaryStartDate]
	  ,JSMH.[MilitaryEndDate]
	  ,JSMH.[JobSeekerId]
	  ,JS.[CreatedOn]
	  ,JS.[UpdatedOn]
	  ,NULL
	  ,JSMH.[VeteranType]
FROM [dbo].[Report.JobSeekerMilitaryHistory] JSMH
	INNER JOIN [dbo].[Report.JobSeeker] JS ON JSMH.JobSeekerId = JS.Id

UPDATE [KeyTable] SET NextId = NextId + @@ROWCOUNT