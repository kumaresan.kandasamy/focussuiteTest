UPDATE [dbo].[Config.EmailTemplate]
SET Body = 
'The referral you have requested for the #JOBTITLE# position at #EMPLOYERNAME# has been approved.
  
Please log into your #FOCUSCAREER# account and view the details of your approved referral request together with the posting''s interview contact information. You will find this on your dashboard in the section titled "Jobs to which you have referral activity".

Click here to sign in;
#FOCUSCAREERURL#

Regards,
#SENDERNAME#'
WHERE EmailTemplateType = 9