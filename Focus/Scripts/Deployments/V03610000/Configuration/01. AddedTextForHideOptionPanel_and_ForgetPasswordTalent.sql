﻿-- =============================================
-- Script Template
-- =============================================
IF NOT EXISTS(SELECT 1 FROM [dbo].[Config.LocalisationItem] WHERE [KEY]='HideJobsSelectMultiple.Label')
BEGIN
INSERT INTO  [dbo].[Config.LocalisationItem]
VALUES('','HideJobsSelectMultiple.Label','<b>If you would like to place your hidden job(s) back in resume, press and hold CTRL key to remove highlight and select “Go”.</b>',0,12090)
END

IF NOT EXISTS(SELECT 1 FROM [dbo].[Config.LocalisationItem] WHERE [KEY]='HideJobs.Label')
BEGIN
INSERT INTO  [dbo].[Config.LocalisationItem]
VALUES('','HideJobs.Label','<b>To hide a job, select the job listed below ,then select Go. To hide multiple jobs, press and hold CTRL while highlighting the jobs you would not like to display in resume, then select Go.</b>',0,12090)
END


IF NOT EXISTS(SELECT 1 FROM [dbo].[Config.LocalisationItem] WHERE [KEY]='SocialSecurityNumberValidator.AlreadyInUse')
BEGIN
INSERT INTO  [dbo].[Config.LocalisationItem]
VALUES('','SocialSecurityNumberValidator.AlreadyInUse','SSN is already in use. This may indicate you already have an Employ Georgia account. If you need assistance accessing this account, please contact Employ Georgia Support at 1-844-283-0997. Please do not attempt to create a second account.',0,12090)
END

IF NOT EXISTS(SELECT 1 FROM [dbo].[Config.LocalisationItem] WHERE [KEY]='UsernameErrorMessage.Text')
BEGIN
INSERT INTO  [dbo].[Config.LocalisationItem]
VALUES('','UsernameErrorMessage.Text','Email address is already in use. This may indicate you already have an Employ Georgia account. If you need assistance accessing this account, please contact Employ Georgia Support at 1-844-283-0997. Please do not attempt to create a second account.',0,12090)
END

IF NOT EXISTS(SELECT 1 FROM [dbo].[Config.LocalisationItem] WHERE [KEY]='InvalidEmail.Text')BEGININSERT INTO  [dbo].[Config.LocalisationItem]VALUES('','InvalidEmail.Text','Email address not recognized. Please check the details and try again.',0,12090)ENDIF NOT EXISTS(SELECT 1 FROM [dbo].[Config.LocalisationItem] WHERE [KEY]='InvalidUser.Text')BEGININSERT INTO  [dbo].[Config.LocalisationItem]VALUES('','InvalidUser.Text','Username not recognized. Please check the details and try again.',0,12090)END