﻿-- =============================================
-- Script Template
-- =============================================
IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='311224'AND [NAME]='Soybean and Other Oilseed Processing')
 BEGIN 
  INSERT INTO [dbo].[Library.NAICS] VALUES('311224','Soybean and Other Oilseed Processing',274)
  END 
 
IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='311314'AND [NAME]='Cane Sugar Manufacturing')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('311314','Cane Sugar Manufacturing',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='31135'AND [NAME]='Chocolate and Confectionery Manufacturing')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('31135','Chocolate and Confectionery Manufacturing',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='311351'AND [NAME]='Chocolate and Confectionery Manufacturing from Cacao Beans')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('311351','Chocolate and Confectionery Manufacturing from Cacao Beans',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='311352'AND [NAME]='Confectionery Manufacturing from Purchased Chocolate')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('311352','Confectionery Manufacturing from Purchased Chocolate',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='311710'AND [NAME]='Seafood Product Preparation and Packaging')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('311710','Seafood Product Preparation and Packaging',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='311824'AND [NAME]='Dry Pasta, Dough, and Flour Mixes Manufacturing from Purchased Flour')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('311824','Dry Pasta, Dough, and Flour Mixes Manufacturing from Purchased Flour',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='31223'AND [NAME]='Tobacco Manufacturing')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('31223','Tobacco Manufacturing',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='312230'AND [NAME]='Tobacco Manufacturing')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('312230','Tobacco Manufacturing',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='313110'AND [NAME]='Fiber, Yarn, and Thread Mills')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('313110','Fiber, Yarn, and Thread Mills',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='313220'AND [NAME]='Narrow Fabric Mills and Schiffli Machine Embroidery')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('313220','Narrow Fabric Mills and Schiffli Machine Embroidery',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='313240'AND [NAME]='Knit Fabric Mills')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('313240','Knit Fabric Mills',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='313310'AND [NAME]='Textile and Fabric Finishing Mills')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('313310','Textile and Fabric Finishing Mills',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='314120'AND [NAME]='Curtain and Linen Mills')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('314120','Curtain and Linen Mills',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='314910'AND [NAME]='Textile Bag and Canvas Mills')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('314910','Textile Bag and Canvas Mills',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='314994'AND [NAME]='Rope, Cordage, Twine, Tire Cord, and Tire Fabric Mills')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('314994','Rope, Cordage, Twine, Tire Cord, and Tire Fabric Mills',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='315110'AND [NAME]='Hosiery and Sock Mills')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('315110','Hosiery and Sock Mills',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='315190'AND [NAME]='Other Apparel Knitting Mills')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('315190','Other Apparel Knitting Mills',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='315210'AND [NAME]='Cut and Sew Apparel Contractors')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('315210','Cut and Sew Apparel Contractors',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='315220'AND [NAME]='Men''s and Boy''s Cut and Sew Apparel Manufacturing')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('315220','Men''s and Boy''s Cut and Sew Apparel Manufacturing',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='31524'AND [NAME]='Women''s, Girl''s, and Infant''s Cut and Sew Apparel Manufacturing')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('31524','Women''s, Girl''s, and Infant''s Cut and Sew Apparel Manufacturing',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='315240'AND [NAME]='Women''s, Girl''s, and Infant''s Cut and Sew Apparel Manufacturing')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('315240','Women''s, Girl''s, and Infant''s Cut and Sew Apparel Manufacturing',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='31528'AND [NAME]='Other Cut and Sew Apparel Manufacturing')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('31528','Other Cut and Sew Apparel Manufacturing',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='315280'AND [NAME]='Other Cut and Sew Apparel Manufacturing')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('315280','Other Cut and Sew Apparel Manufacturing',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='315990'AND [NAME]='Apparel Accessories and Other Apparel Manufacturing')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('315990','Apparel Accessories and Other Apparel Manufacturing',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='316210'AND [NAME]='Footwear Manufacturing')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('316210','Footwear Manufacturing',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='316998'AND [NAME]='All Other Leather Good and Allied Product Manufacturing')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('316998','All Other Leather Good and Allied Product Manufacturing',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='322219'AND [NAME]='Other Paperboard Container Manufacturing')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('322219','Other Paperboard Container Manufacturing',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='322220'AND [NAME]='Paper Bag and Coated and Treated Paper Manufacturing')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('322220','Paper Bag and Coated and Treated Paper Manufacturing',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='322230'AND [NAME]='Stationery Product Manufacturing')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('322230','Stationery Product Manufacturing',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='323120'AND [NAME]='Support Activities for Printing')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('323120','Support Activities for Printing',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='325130'AND [NAME]='Synthetic Dye and Pigment Manufacturing')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('325130','Synthetic Dye and Pigment Manufacturing',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='325180'AND [NAME]='Other Basic Inorganic Chemical Manufacturing')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('325180','Other Basic Inorganic Chemical Manufacturing',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='325194'AND [NAME]='Cyclic Crude, Intermediate, and Gum and Wood Chemical Manufacturing')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('325194','Cyclic Crude, Intermediate, and Gum and Wood Chemical Manufacturing',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='325220'AND [NAME]='Artificial and Synthetic Fibers and Filaments Manufacturing')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('325220','Artificial and Synthetic Fibers and Filaments Manufacturing',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='327110'AND [NAME]='Pottery, Ceramics, and Plumbing Fixture Manufacturing')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('327110','Pottery, Ceramics, and Plumbing Fixture Manufacturing',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='327120'AND [NAME]='Clay Building Material and Refractories Manufacturing')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('327120','Clay Building Material and Refractories Manufacturing',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='331110'AND [NAME]='Iron and Steel Mills and Ferroalloy Manufacturing')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('331110','Iron and Steel Mills and Ferroalloy Manufacturing',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='331313'AND [NAME]='Alumina Refining and Primary Aluminum Production')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('331313','Alumina Refining and Primary Aluminum Production',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='331318'AND [NAME]='Other Aluminum Rolling, Drawing, and Extruding')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('331318','Other Aluminum Rolling, Drawing, and Extruding',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='331410'AND [NAME]='Nonferrous Metal (except Aluminum) Smelting and Refining')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('331410','Nonferrous Metal (except Aluminum) Smelting and Refining',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='331420'AND [NAME]='Copper Rolling, Drawing, Extruding, and Alloying')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('331420','Copper Rolling, Drawing, Extruding, and Alloying',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='331523'AND [NAME]='Nonferrous Metal Die-Casting Foundries ')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('331523','Nonferrous Metal Die-Casting Foundries',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='331529'AND [NAME]='Other Nonferrous Metal Foundries (except Die-Casting)')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('331529','Other Nonferrous Metal Foundries (except Die-Casting)',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='332119'AND [NAME]='Metal Crown, Closure, and Other Metal Stamping (except Automotive)')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('332119','Metal Crown, Closure, and Other Metal Stamping (except Automotive)',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='332215'AND [NAME]='Metal Kitchen Cookware, Utensil, Cutlery, and Flatware (except Precious) Manufacturing')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('332215','Metal Kitchen Cookware, Utensil, Cutlery, and Flatware (except Precious) Manufacturing',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='332216'AND [NAME]='Saw Blade and Handtool Manufacturing')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('332216','Saw Blade and Handtool Manufacturing',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='332613'AND [NAME]='Spring Manufacturing')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('332613','Spring Manufacturing',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='33324'AND [NAME]='Industrial Machinery Manufacturing')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('33324','Industrial Machinery Manufacturing',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='333241'AND [NAME]='Food Product Machinery Manufacturing')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('333241','Food Product Machinery Manufacturing',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='333242'AND [NAME]='Semiconductor Machinery Manufacturing')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('333242','Semiconductor Machinery Manufacturing',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='333243'AND [NAME]='Sawmill, Woodworking, and Paper Machinery Manufacturing')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('333243','Sawmill, Woodworking, and Paper Machinery Manufacturing',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='333244'AND [NAME]='Printing Machinery and Equipment Manufacturing')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('333244','Printing Machinery and Equipment Manufacturing',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='333249'AND [NAME]='Other Industrial Machinery Manufacturing')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('333249','Other Industrial Machinery Manufacturing',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='333316'AND [NAME]='Photographic and Photocopying Equipment Manufacturing')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('333316','Photographic and Photocopying Equipment Manufacturing',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='333318'AND [NAME]='Other Commercial and Service Industry Machinery Manufacturing')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('333318','Other Commercial and Service Industry Machinery Manufacturing',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='333413'AND [NAME]='Industrial and Commercial Fan and Blower and Air Purification Equipment Manufacturing')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('333413','Industrial and Commercial Fan and Blower and Air Purification Equipment Manufacturing',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='333517'AND [NAME]='Machine Tool Manufacturing')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('333517','Machine Tool Manufacturing',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='333519'AND [NAME]='Rolling Mill and Other Metalworking Machinery Manufacturing')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('333519','Rolling Mill and Other Metalworking Machinery Manufacturing',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='334118'AND [NAME]='Computer Terminal and Other Computer Peripheral Equipment Manufacturing')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('334118','Computer Terminal and Other Computer Peripheral Equipment Manufacturing',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='334614'AND [NAME]='Software and Other Prerecorded Compact Disc, Tape, and Record Reproducing')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('334614','Software and Other Prerecorded Compact Disc, Tape, and Record Reproducing',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='335210'AND [NAME]='Small Electrical Appliance Manufacturing')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('335210','Small Electrical Appliance Manufacturing',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='336310'AND [NAME]='Motor Vehicle Gasoline Engine and Engine Parts Manufacturing')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('336310','Motor Vehicle Gasoline Engine and Engine Parts Manufacturing',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='336320'AND [NAME]='Motor Vehicle Electrical and Electronic Equipment Manufacturing')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('336320','Motor Vehicle Electrical and Electronic Equipment Manufacturing',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='336390'AND [NAME]='Other Motor Vehicle Parts Manufacturing')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('336390','Other Motor Vehicle Parts Manufacturing',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='339910'AND [NAME]='Jewelry and Silverware Manufacturing')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('339910','Jewelry and Silverware Manufacturing',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='339930'AND [NAME]='Doll, Toy, and Game Manufacturing')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('339930','Doll, Toy, and Game Manufacturing',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='339940'AND [NAME]='Office Supplies (except Paper) Manufacturing')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('339940','Office Supplies (except Paper) Manufacturing',274)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='441228'AND [NAME]='Motorcycle, ATV, and All Other Motor Vehicle Dealers')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('441228','Motorcycle, ATV, and All Other Motor Vehicle Dealers',1203)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='44314'AND [NAME]='Electronics and Appliance Stores')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('44314','Electronics and Appliance Stores',1203)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='443141'AND [NAME]='Household Appliance Stores')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('443141','Household Appliance Stores',1203)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='443142'AND [NAME]='Electronics Stores')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('443142','Electronics Stores',1203)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='454310'AND [NAME]='Fuel Dealers')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('454310','Fuel Dealers',1203)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='7225'AND [NAME]='Restaurants and Other Eating Places')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('7225','Restaurants and Other Eating Places',2120)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='72251'AND [NAME]='Restaurants and Other Eating Places')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('72251','Restaurants and Other Eating Places',2120)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='722513'AND [NAME]='Limited-Service Restaurants')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('722513','Limited-Service Restaurants',2120)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='722514'AND [NAME]='Cafeterias, Grill Buffets, and Buffets')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('722514','Cafeterias, Grill Buffets, and Buffets',2120)
 END 

IF NOT EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='722515'AND [NAME]='Snack and Nonalcoholic Beverage Bars')
 BEGIN 
 INSERT INTO [dbo].[Library.NAICS] VALUES('722515','Snack and Nonalcoholic Beverage Bars',2120)
 END 

 
 IF EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='221119'AND [NAME]='Other Electric Power Generation')
 BEGIN 
 UPDATE [dbo].[Library.NAICS] SET [CODE]='221118',[ParentId]='180' WHERE [code]='221119'AND [NAME]='Other Electric Power Generation'
 END
 
 IF EXISTS(SELECT 1 FROM[dbo].[Library.NAICS] WHERE [code]='722110'AND [NAME]='Full-Service Restaurants')
 BEGIN 
 UPDATE [dbo].[Library.NAICS] SET [CODE]='722511',[ParentId]='2120' WHERE [code]='722110'AND [NAME]='Full-Service Restaurants'
 END

 IF EXISTS(SELECT 1 FROM [DBO].[Data.Application.Employer] where [IndustrialClassification]='221119 - Other Electric Power Generation')
 BEGIN
 UPDATE [DBO].[Data.Application.Employer] SET [IndustrialClassification]='221118 - Other Electric Power Generation'
 WHERE [IndustrialClassification]='221119 - Other Electric Power Generation'
 END
 
  IF EXISTS(SELECT 1 FROM [DBO].[Data.Application.BusinessUnit] where [IndustrialClassification]='221119 - Other Electric Power Generation')
 BEGIN
 UPDATE [DBO].[Data.Application.BusinessUnit] SET [IndustrialClassification]='221118 - Other Electric Power Generation'
 WHERE [IndustrialClassification]='221119 - Other Electric Power Generation'
 END
 
 
 IF EXISTS(SELECT 1 FROM [DBO].[Data.Application.Employer] where [IndustrialClassification]='722110 - Full-Service Restaurants')
 BEGIN
 UPDATE [DBO].[Data.Application.Employer] SET [IndustrialClassification]='722511 - Full-Service Restaurants'
 WHERE [IndustrialClassification]='722110 - Full-Service Restaurants'
 END
 
  IF EXISTS(SELECT 1 FROM [DBO].[Data.Application.BusinessUnit] where [IndustrialClassification]='722110 - Full-Service Restaurants')
 BEGIN
 UPDATE [DBO].[Data.Application.BusinessUnit] SET [IndustrialClassification]='722511 - Full-Service Restaurants'
 WHERE [IndustrialClassification]='722110 - Full-Service Restaurants'
 END
