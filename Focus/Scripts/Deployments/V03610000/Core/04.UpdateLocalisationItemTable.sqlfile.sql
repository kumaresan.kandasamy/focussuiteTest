﻿IF EXISTS( select 1 from [Config.LocalisationItem] where [key]='Focus.Web.WebAssist.Controls.AssistDefaults.LaborInsightsDefaults.Label')
BEGIN
	Delete [Config.LocalisationItem] where [key]='Focus.Web.WebAssist.Controls.AssistDefaults.LaborInsightsDefaults.Label'
END


IF EXISTS( select 1 from [Config.LocalisationItem] where [key]='ActivityType.Employer.NoEdit')
BEGIN
	Delete [Config.LocalisationItem] where [key]='ActivityType.Employer.NoEdit'
END

IF EXISTS (select 1 from [Config.LocalisationItem] where [key]='Focus.Web.WebAssist.Controls.TalentDefaults.NewEmployerApproval.Label')
BEGIN
	Delete [Config.LocalisationItem] where [key]='Focus.Web.WebAssist.Controls.TalentDefaults.NewEmployerApproval.Label'
END