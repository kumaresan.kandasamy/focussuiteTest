﻿declare  @tempLocation table (jobid bigint, Location nvarchar(max), City nvarchar(max), County nvarchar(max), Code nvarchar(max), Zip nvarchar(max),
[State] nvarchar(max))

Insert into @tempLocation 
select Jl.JobId,JL.Location,
LEFT(Location,CHARINDEX(',',Location,1)-1) as City,
LEFT(RIGHT(Location,len(Location)-CHARINDEX(',',Location,1)),CHARINDEX(',',RIGHT(Location,len(Location)-CHARINDEX(',',Location,1)),1)-1) as County,
LEFT(RIGHT(Location,len(Location)-CHARINDEX('(',Location,1)+4),CHARINDEX('(',RIGHT(Location,len(Location)-CHARINDEX('(',Location,1)+4),1)-1) as Code,
LEFT(RIGHT(Location,len(Location)-CHARINDEX('(',Location,1)),CHARINDEX(')',RIGHT(Location,len(Location)-CHARINDEX('(',Location,1)),1)-1) as ZIP,
s.Name as [State]
from dbo.[Library.State] s, dbo.[Data.Application.JobLocation] jl
where s.code = LEFT(RIGHT(Location,len(Location)-CHARINDEX('(',Location,1)+4),CHARINDEX('(',RIGHT(Location,len(Location)-CHARINDEX('(',Location,1)+4),1)-2)
and len(JL.Location) - len(replace(JL.Location, ',', '')) = 2 and jl.Zip Is NULL

update JL
set jl.City = substring(t.City,1,127),jl.county=substring(t.county,1,127),jl.zip=substring(t.zip,1,10),jl.State=substring(t.State,1,127)
from  [Data.Application.JobLocation]as JL inner join @tempLocation t on t.jobid=jl.JobId


declare  @tempLocation1 table (jobid bigint, Location nvarchar(max), City nvarchar(max), Code nvarchar(max), Zip nvarchar(max),
[State] nvarchar(max))

Insert into @tempLocation1 
select Jl.JobId,JL.Location,
LEFT(Location,CHARINDEX(',',Location,1)-1) as City,
LEFT(RIGHT(Location,LEN(Location)-CHARINDEX(',',Location,1)-1),3) as Code,
LEFT(RIGHT(Location,len(Location)-CHARINDEX('(',Location,1)),CHARINDEX(')',RIGHT(Location,len(Location)-CHARINDEX('(',Location,1)),1)-1) as ZIP,
s.Name as [State]
from dbo.[Library.State] s, dbo.[Data.Application.JobLocation] jl
where s.code = LEFT(RIGHT(Location,LEN(Location)-CHARINDEX(',',Location,1)-1),3)
and len(JL.Location) - len(replace(JL.Location, ',', '')) = 1 and jl.Zip Is NULL

update JL
set jl.City = substring(t.City,1,127),jl.zip=substring(t.zip,1,10),jl.State=substring(t.State,1,127)
from  [Data.Application.JobLocation]as JL inner join @tempLocation1 t on t.jobid=jl.JobId

declare  @tempLocation2 table (jobid bigint, Location nvarchar(max), Code nvarchar(max), Zip nvarchar(max),
[State] nvarchar(max))

Insert into @tempLocation2 
select Jl.JobId,JL.Location,
LEFT(Location,CHARINDEX('(',Location,1)-1) as Code,
LEFT(RIGHT(Location,len(Location)-CHARINDEX('(',Location,1)),CHARINDEX(')',RIGHT(Location,len(Location)-CHARINDEX('(',Location,1)),1)-1) as ZIP,
s.Name as [State]
from dbo.[Library.State] s, dbo.[Data.Application.JobLocation] jl
where (s.code = LEFT(Location,CHARINDEX('(',Location,1)-1)
and len(JL.Location) - len(replace(JL.Location, ',', '')) = 0 and jl.Zip Is NULL) and location like '%(%'

update JL
set jl.zip=substring(t.zip,1,10),jl.State=substring(t.State,1,127)
from  [Data.Application.JobLocation]as JL inner join @tempLocation2 t on t.jobid=jl.JobId 



