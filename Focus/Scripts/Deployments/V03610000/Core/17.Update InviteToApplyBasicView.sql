/****** Object:  View [dbo].[Data.Application.InviteToApplyBasicView]    Script Date: 03/16/2018 03:31:45 ******/
IF EXISTS (SELECT * FROM sys.VIEWS WHERE object_id = OBJECT_ID(N'[dbo].[Data.Application.InviteToApplyBasicView]'))
	DROP VIEW [dbo].[Data.Application.InviteToApplyBasicView]
GO

/****** Object:  View [dbo].[Data.Application.InviteToApplyBasicView]    Script Date: 03/16/2018 03:31:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[Data.Application.InviteToApplyBasicView]
AS
SELECT ITA.Id AS Id
	,ITA.JobId AS JobId
	,ITA.PersonId AS PersonId
	,PE.FirstName AS FirstName
	,PE.LastName AS LastName
	,R.TownCity AS Town
	,R.StateId AS StateId
	,ITA.CreatedOn AS CreatedOn
	,ISNULL([AddIn].[Description], '') AS Branding
	,R.IsContactInfoVisible AS IsContactInfoVisible
	,R.YearsExperience AS YearsExperience
	,R.NcrcLevelId AS NcrcLevelId
	,ISNULL(R.IsVeteran, 0) AS CandidateIsVeteran
	,ITA.QueueInvitation AS InvitationQueued
FROM [Data.Application.InviteToApply] ITA
	INNER JOIN [Data.Application.Person] AS PE ON ITA.PersonId = PE.Id
	INNER JOIN [Data.Application.Resume] AS R ON ITA.PersonId = R.PersonId
	LEFT OUTER JOIN [Data.Application.ResumeAddIn] AS [AddIn] WITH (NOLOCK) ON [AddIn].ResumeId = R.Id
	AND [AddIn].ResumeSectionType = 8192 -- Branding Section
WHERE NOT EXISTS (
		SELECT 1
		FROM [Data.Application.Application] AS A
		INNER JOIN [Data.Application.Resume] AS R ON A.ResumeID = R.Id
		INNER JOIN [Data.Application.Posting] AS P ON A.PostingId = P.Id
		WHERE P.JobId IS NOT NULL
			AND P.JobId = ITA.JobId
			AND R.PersonId = ITA.PersonId
		)
	AND R.IsDefault = 1

UNION

SELECT jps.Id AS Id
	,jps.JobId AS JobId
	,jps.PersonId AS PersonId
	,PE.FirstName AS FirstName
	,PE.LastName AS LastName
	,R.TownCity AS Town
	,R.StateId AS StateId
	,jps.CreatedOn AS CreatedOn
	,ISNULL([AddIn].[Description], '') AS Branding
	,R.IsContactInfoVisible AS IsContactInfoVisible
	,R.YearsExperience AS YearsExperience
	,R.NcrcLevelId AS NcrcLevelId
	,ISNULL(R.IsVeteran, 0) AS CandidateIsVeteran
	,jps.QueueRecommendation AS InvitationQueued
FROM dbo.[Data.Application.JobPostingOfInterestSent] jps
	INNER JOIN dbo.[Data.Application.Person] pe ON jps.PersonId = pe.Id
	INNER JOIN dbo.[Data.Application.Resume] r ON r.PersonId = jps.PersonId
	LEFT OUTER JOIN [Data.Application.ResumeAddIn] AS [AddIn] WITH (NOLOCK) ON [AddIn].ResumeId = R.Id
	AND [AddIn].ResumeSectionType = 8192 -- Branding Section
WHERE NOT EXISTS (
		SELECT 1
		FROM [Data.Application.Application] AS A
		INNER JOIN [Data.Application.Resume] AS R ON A.ResumeID = R.Id
		INNER JOIN [Data.Application.Posting] AS P ON A.PostingId = P.Id
		WHERE P.JobId IS NOT NULL
			AND P.JobId = jps.JobId
			AND R.PersonId = jps.PersonId
		)
	AND NOT EXISTS (
		SELECT 1
		FROM [Data.Application.InviteToApply] i
		WHERE i.PersonId = jps.PersonId
			AND i.JobId = jps.JobId
		)
	AND R.IsDefault = 1
GO