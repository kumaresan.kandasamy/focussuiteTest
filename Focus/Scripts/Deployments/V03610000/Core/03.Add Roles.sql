﻿IF NOT EXISTS (SELECT 1 FROM [dbo].[Data.Application.Role] WHERE [Key] = 'ReassignJobPostingToNewOffice')
BEGIN
	INSERT INTO [dbo].[Data.Application.Role](Id, [Key], Value)
	VALUES
	((SELECT MAX(Id) + 1 FROM [dbo].[Data.Application.Role]),'ReassignJobPostingToNewOffice','Reassign Job Posting To New Office')
END

IF NOT EXISTS (SELECT 1 FROM [dbo].[Data.Application.Role] WHERE [Key] = 'ReassignBusinessToNewOffice')
BEGIN
	INSERT INTO [dbo].[Data.Application.Role](Id, [Key], Value)
	VALUES
	((SELECT MAX(Id) + 1 FROM [dbo].[Data.Application.Role]),'ReassignBusinessToNewOffice','Reassign Business To New Office')
END