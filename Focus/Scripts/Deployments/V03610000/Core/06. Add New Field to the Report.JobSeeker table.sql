﻿IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.JobSeeker' AND COLUMN_NAME = 'Enabled')
BEGIN
	ALTER TABLE [Report.JobSeeker] ADD [Enabled] bit NOT NULL DEFAULT(0)
	ALTER TABLE [Report.JobSeeker] ADD [Blocked] bit NOT NULL DEFAULT(0) 
	Exec('update j
	set j.Enabled = u.enabled, j.Blocked = u.Blocked
	from dbo.[Report.JobSeeker] AS j
	inner join [dbo].[Data.Application.User] u on u.PersonId=j.FocusPersonId')
END
