﻿/****** Object:  View [dbo].[Data.Application.JobActionEventView]    Script Date: 10/11/2017 06:39:31 ******/
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Data.Application.JobActionEventView]'))
DROP VIEW [dbo].[Data.Application.JobActionEventView]
GO

/****** Object:  View [dbo].[Data.Application.JobActionEventView]    Script Date: 10/11/2017 06:39:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[Data.Application.JobActionEventView]
AS
SELECT	ae.Id, 
		j.Id AS JobId, 
		ae.UserId, 
		p.FirstName, 
		p.LastName,		
		ae.ActionedOn, 
		at.Name AS ActionName, 
		CASE at.Name
			WHEN 'InviteJobSeekerToApply' THEN CONVERT(varchar,js.Id)
			WHEN 'InviteJobSeekerToApplyThroughTalent' THEN CONVERT(varchar,js.Id)
			ELSE ae.AdditionalDetails
		END AS AdditionalDetails,
		ISNULL(js.FirstName + ' ' + js.LastName, '') AS AdditionalName,
		ae.EntityIdAdditional01,
		ae.EntityIdAdditional02
FROM  dbo.[Data.Core.ActionType] AS at WITH (NOLOCK) 
	INNER JOIN dbo.[Data.Core.ActionEvent] AS ae WITH (NOLOCK) ON at.Id = ae.ActionTypeId
	INNER JOIN dbo.[Data.Application.Job] AS j WITH (NOLOCK) ON ae.EntityId = j.Id 
	INNER JOIN dbo.[Data.Application.User] AS u WITH (NOLOCK) ON ae.UserId = u.Id 
	INNER JOIN dbo.[Data.Application.Person] AS p WITH (NOLOCK) ON u.PersonId = p.Id
	LEFT OUTER JOIN dbo.[Data.Application.Person] js WITH (NOLOCK) ON js.Id = ae.EntityIdAdditional01 --AND at.Name IN ('InviteJobSeekerToApply', 'InviteJobSeekerToApplyThroughTalent')
WHERE at.Name IN (
	'PostJob',
	'SaveJob',
	'HoldJob',
	'CloseJob',
	'RefreshJob',
	'ReactivateJob',
	'CreateJob',
	'ApprovePostingReferral',
	'SelfReferral',
	'ReapplyReferralRequest',
	'AutoApprovedReferralBypass',
	'AutoResolvedIssue',
	'InviteJobSeekerToApply',
	'InviteJobSeekerToApplyThroughTalent')
	
UNION ALL

SELECT 
	ae.Id, 
	po.JobId, 
	ae.UserId, 
	p.FirstName, 
	p.LastName, 
	ae.ActionedOn, 
	at.Name AS ActionName, 
	ae.AdditionalDetails,
	CASE at.Name
		WHEN 'WaivedRequirements' THEN ISNULL(js.FirstName + ' ' + js.LastName, '') 
		ELSE ''
	END AS AdditionalName,
	ae.EntityIdAdditional01,
	ae.EntityIdAdditional02
FROM  dbo.[Data.Core.ActionType] AS at WITH (NOLOCK) 
	INNER JOIN dbo.[Data.Core.ActionEvent] AS ae WITH (NOLOCK) ON at.Id = ae.ActionTypeId 
	INNER JOIN dbo.[Data.Application.Application] AS ca WITH (NOLOCK) ON ae.EntityId = ca.Id 
	INNER JOIN dbo.[Data.Application.User] AS u WITH (NOLOCK) ON ae.UserId = u.Id 
	INNER JOIN dbo.[Data.Application.Person] AS p WITH (NOLOCK) ON u.PersonId = p.Id 
	INNER JOIN dbo.[Data.Application.Posting] AS po WITH (NOLOCK) ON ca.PostingId = po.Id
	LEFT OUTER JOIN dbo.[Data.Application.Person] js WITH (NOLOCK) ON js.Id = ae.EntityIdAdditional02 --AND at.Name = 'WaivedRequirements'
WHERE at.Name IN (
	'ApproveCandidateReferral', 
	'DenyCandidateReferral', 
	'CreateCandidateApplication',
	'WaivedRequirements',
	'SelfReferral',
	'ReferralRequest',
	'ReapplyReferralRequest',
	'AutoApprovedReferralBypass',
	'UpdateReferralStatusToAutoDenied',
	'HoldCandidateReferral')

GO


