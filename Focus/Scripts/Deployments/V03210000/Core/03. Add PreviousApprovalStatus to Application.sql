﻿   IF NOT EXISTS(	SELECT * FROM sys.columns	INNER JOIN sys.tables on sys.columns.object_id = sys.tables.object_id  
	 WHERE sys.columns.[name] = 'PreviousApprovalStatus'	AND sys.tables.[name] = 'Data.Application.Application')
	ALTER TABLE [Data.Application.Application]
  ADD PreviousApprovalStatus int null
  GO