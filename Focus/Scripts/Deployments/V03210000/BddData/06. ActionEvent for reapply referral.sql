DECLARE @ResumeId BIGINT
DECLARE @ApplicationId BIGINT
DECLARE @ActionTypeId BIGINT
DECLARE @NextId BIGINT
DECLARE @EntityTypeId BIGINT
DECLARE @TalentUserId BIGINT

SELECT @ResumeId = Id FROM [Data.Application.Resume] WHERE ResumeName = 'FVN638 Jobseeker 1 Resume'

SELECT @ApplicationId = Id FROM [Data.Application.Application] WHERE ResumeId = @ResumeId

IF NOT EXISTS(SELECT 1 FROM [Data.Core.ActionType] WHERE Name = 'AutoApprovedReferralBypass')
BEGIN
	SELECT @NextId = NextId FROM dbo.KeyTable

	UPDATE dbo.KeyTable
	SET  NextId = NextId + 1;

	INSERT INTO [FocusSuite].[dbo].[Data.Core.ActionType]
           ([Id]
           ,[Name]
           ,[ReportOn]
           ,[AssistAction])
     VALUES
           (@NextId
           ,'AutoApprovedReferralBypass'
           ,0
           ,0)
END

SELECT @ActionTypeId = Id FROM [Data.Core.ActionType] WHERE Name = 'AutoApprovedReferralBypass'

IF NOT EXISTS(SELECT 1 FROM [Data.Core.EntityType] WHERE Name = 'Application')
BEGIN
	SELECT @NextId = NextId FROM dbo.KeyTable

	UPDATE dbo.KeyTable
	SET  NextId = NextId + 1;

	INSERT INTO [FocusSuite].[dbo].[Data.Core.EntityType]
           ([Id]
           ,[Name])
     VALUES
           (@NextId
           ,'Application')
END

SELECT @EntityTypeId = Id FROM [Data.Core.EntityType] WHERE Name = 'Application'

SELECT @TalentUserId = Id FROM [Data.Application.User] WHERE USerName = 'dev@employer.com'


BEGIN TRANSACTION

	SELECT @NextId = NextId FROM KeyTable

	UPDATE KeyTable SET NextId = NextId + 1

	IF @@ERROR <> 0
	BEGIN
		ROLLBACK TRANSACTION
		RETURN
	END

	INSERT INTO [Data.Core.ActionEvent]
	(		[Id]
			,[SessionId]
		  ,[RequestId]
		  ,[UserId]
		  ,[ActionedOn]
		  ,[EntityId]
		  ,[EntityIdAdditional01]
		  ,[EntityIdAdditional02]
		  ,[AdditionalDetails]
		  ,[ActionTypeId]
		  ,[EntityTypeId])
	VALUES
	( @NextId, '3AC0CB7F-D414-450E-B37D-18DB75C52F9B','A7CBB29B-3B0E-4BB3-BF75-264144CF2FE0', @TalentUserId, '4 November 2014', @ApplicationId, NULL, NULL, 'FVN638 Explorer1', @ActionTypeId, @EntityTypeId)

	IF @@ERROR <> 0
	BEGIN
		ROLLBACK TRANSACTION
		RETURN
	END
	
	COMMIT TRANSACTION