﻿DECLARE @Apps TABLE
(
	Id BIGINT IDENTITY(1, 1),
	ResumeName NVARCHAR(255),
	EmailAddress NVARCHAR(255),
	JobTitle NVARCHAR(255),
	BusinessUnitName NVARCHAR(255),
	ResumeId BIGINT,
	PostingId BIGINT,
	ApprovalStatus INT
)

INSERT INTO @Apps (ResumeName, EmailAddress, JobTitle, BusinessUnitName, ApprovalStatus)
VALUES
		('FVN-547 Applicant 1 Resume', 'fvn547@appce1.com', 'Job FVN-973 1', 'BU FVN973C 1', 2),
		('FVN-547 Applicant 1 Resume', 'fvn547@appce1.com', 'Job FVN-973 2', 'BU FVN973C 1', 2),
		('FVN-547 Applicant 2 Resume', 'fvn547@appce2.com', 'Job FVN-973 3', 'BU FVN973C 2', 2)

UPDATE 
	A
SET
	ResumeId = R.Id,
	PostingId = PO.Id
FROM
	@Apps A
INNER JOIN [Data.Application.Person] P
	ON P.EmailAddress = A.EmailAddress
INNER JOIN [Data.Application.Resume] R
	ON R.PersonId = P.Id
	AND R.ResumeName = A.ResumeName
	AND R.StatusId = 1
INNER JOIN [Data.Application.BusinessUnit] BU
	ON BU.Name = A.BusinessUnitName
INNER JOIN [Data.Application.Job] J
	ON J.JobTitle = A.JobTitle
	AND J.BusinessUnitId = BU.Id
INNER JOIN [Data.Application.Posting] PO
	ON PO.JobId = J.Id

DECLARE @AppIndex INT
DECLARE @AppCount INT
DECLARE @ResumeId BIGINT
DECLARE @PostingId BIGINT
DECLARE @AppId BIGINT
DECLARE @StatusLogId BIGINT
DECLARE @ApprovalStatus INT
DECLARE @NextId BIGINT
DECLARE @EntityTypeId BIGINT

SET @AppIndex = 1
SELECT @AppCount = COUNT(1) FROM @Apps

SELECT 
	@EntityTypeId = Id
FROM
	[Data.Core.EntityType]
WHERE
	Name = 'Application'

IF @EntityTypeId IS NULL
BEGIN
		SELECT @EntityTypeId = NextId FROM KeyTable
		UPDATE KeyTable SET NextId = NextId + 1
		INSERT INTO [Data.Core.EntityType](Id,Name) VALUES(@EntityTypeId, 'Application')
END

WHILE @AppIndex <= @AppCount
BEGIN
	SELECT 
		@ResumeId = ResumeId,
		@PostingId = PostingId,
		@ApprovalStatus = ApprovalStatus
	FROM
		@Apps
	WHERE
		Id = @AppIndex
	
	IF @ResumeId IS NOT NULL AND @PostingId IS NOT NULL 
	AND NOT EXISTS(SELECT 1 FROM [Data.Application.Application] WHERE ResumeId = @ResumeId AND PostingId = @PostingId)
	BEGIN
		BEGIN TRANSACTION
		
		SELECT @NextId = NextId FROM KeyTable
		
		UPDATE KeyTable SET NextId = NextId + 2
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		SET @AppId = @NextId
		SET @StatusLogId = @NextId + 2
		
		INSERT INTO [Data.Application.Application]
		(
			Id,
			ApprovalStatus,
			ApprovalRequiredReason,
			ApplicationStatus,
			ApplicationScore,
			StatusLastChangedOn,
			CreatedOn,
			UpdatedOn,
			ResumeId,
			PostingId,
			Viewed,
			PostHireFollowUpStatus,
			AutomaticallyApproved
		)
		VALUES
		(
			@AppId,
			@ApprovalStatus,
			'All referrals are approved automatically',
			1,
			25,
			GETDATE(),
			GETDATE(),
			GETDATE(),
			@ResumeId,
			@PostingId,
			1,
			NULL,
			CASE @ApprovalStatus WHEN 2 THEN 1 ELSE 0 END
		)
				
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		INSERT INTO [Data.Core.StatusLog]
		(
			Id,
			EntityId,
			OriginalStatus,
			NewStatus,
			UserId,
			ActionedOn,
			EntityTypeId
		)
		VALUES
		(
			@StatusLogId,
			@AppId,
			0,
			1,
			0,
			GETDATE(),
			@EntityTypeId
		)

		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		COMMIT TRANSACTION
	END
	
	SET @AppIndex = @AppIndex + 1
END
