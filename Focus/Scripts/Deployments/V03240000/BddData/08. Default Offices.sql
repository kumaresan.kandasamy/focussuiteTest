DECLARE @NextId BIGINT

DECLARE @OfficeIndex INT
DECLARE @OfficeCount INT

DECLARE @OfficeName NVARCHAR(20)
DECLARE @Line1 NVARCHAR(200)
DECLARE @TownCity NVARCHAR(100)
DECLARE @CountyId BIGINT
DECLARE @StateId BIGINT
DECLARE @CountryId BIGINT
DECLARE @PostcodeZip NVARCHAR(20)
DECLARE @UnassignedDefault BIT
DECLARE @InActive BIT
DECLARE @BusinessOutreachMailbox NVARCHAR(200)
DECLARE @DefaultType INT

SET @Line1 = 'Address Line 1'
SET @TownCity = 'Bluegrove'
SET @CountyId = 1586411
SET @StateId = 12896
SET @CountryId = 13370
SET @PostcodeZip = 76352
SET @UnassignedDefault = 0
SET @InActive = 0

DECLARE @Offices TABLE
(
	Id BIGINT IDENTITY(1, 1),
	OfficeName NVARCHAR(20),
	BusinessOutreachMailbox NVARCHAR(200),
	DefaultType INT
)

INSERT INTO @Offices 
(
	OfficeName,
	BusinessOutreachMailbox,
	DefaultType
)
VALUES
('JobSeeker Office', 'businessoutreach@jobseekeroffice.com', 0),
('JobOrder Office', 'businessoutreach@joborderoffice.com', 0),
('Employer Office', 'businessoutreach@employeroffice.com', 0)

SET @OfficeIndex = 1
SELECT @OfficeCount = COUNT(1) FROM @Offices

WHILE @OfficeIndex <= @OfficeCount
BEGIN
	SELECT 
		@OfficeName = OfficeName,
		@BusinessOutreachMailbox = BusinessOutreachMailbox,
		@DefaultType = DefaultType
	FROM
		@Offices
	WHERE
		Id = @OfficeIndex

	BEGIN TRANSACTION
			
		SELECT @NextId = NextId FROM KeyTable
			
		UPDATE KeyTable SET NextId = NextId + 1
			
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		INSERT INTO [Data.Application.Office] 
		(
			Id,
			OfficeName,
			Line1,
			TownCity,
			CountyId,
			StateId,
			CountryId,
			PostcodeZip,
			UnassignedDefault,
			InActive,
			BusinessOutreachMailbox,
			DefaultType
		)
		VALUES
		(
			@NextId,
			@OfficeName,
			@Line1,
			@TownCity,
			@CountyId,
			@StateId,
			@CountryId,
			@PostcodeZip,
			@UnassignedDefault,
			@InActive,
			@BusinessOutreachMailbox,
			@DefaultType
		)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
			
	COMMIT TRANSACTION
	
	SET @OfficeIndex = @OfficeIndex + 1
END