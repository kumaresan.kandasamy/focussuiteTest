-- Job Seeker --

IF EXISTS
(
	SELECT 
		1
	FROM 
		sys.tables T
	INNER JOIN sys.indexes I
		ON I.object_id = T.object_id
	INNER JOIN sys.index_columns IC
		ON IC.index_id = I.index_id
		AND IC.object_id = T.object_id
	INNER JOIN sys.columns C
		ON C.column_id = IC.column_id
		AND C.object_id = T.object_id
	WHERE T.name = 'Data.Migration.JobSeekerRequest'
		AND I.name = 'IX_Data.Migration.JobSeekerRequest_RecordType3'
		AND C.name = 'ExternalId'
		AND key_ordinal = 2
)
BEGIN
	DROP INDEX [Data.Migration.JobSeekerRequest].[IX_Data.Migration.JobSeekerRequest_RecordType3]
	
	CREATE INDEX 
		[IX_Data.Migration.JobSeekerRequest_RecordType3] 
	ON 
		[Data.Migration.JobSeekerRequest] (RecordType, [Status], BatchNumber)
END
GO

-- Job Order --

IF EXISTS
(
	SELECT 
		1
	FROM 
		sys.tables T
	INNER JOIN sys.indexes I
		ON I.object_id = T.object_id
	INNER JOIN sys.index_columns IC
		ON IC.index_id = I.index_id
		AND IC.object_id = T.object_id
	INNER JOIN sys.columns C
		ON C.column_id = IC.column_id
		AND C.object_id = T.object_id
	WHERE T.name = 'Data.Migration.JobOrderRequest'
		AND I.name = 'IX_Data.Migration.JobOrderRequest_RecordType3'
		AND C.name = 'ExternalId'
		AND key_ordinal = 2
)
BEGIN
	DROP INDEX [Data.Migration.JobOrderRequest].[IX_Data.Migration.JobOrderRequest_RecordType3]
	
	CREATE INDEX 
		[IX_Data.Migration.JobOrderRequest_RecordType3] 
	ON 
		[Data.Migration.JobOrderRequest] (RecordType, [Status], BatchNumber)
END
GO

-- Employer --

IF EXISTS
(
	SELECT 
		1
	FROM 
		sys.tables T
	INNER JOIN sys.indexes I
		ON I.object_id = T.object_id
	INNER JOIN sys.index_columns IC
		ON IC.index_id = I.index_id
		AND IC.object_id = T.object_id
	INNER JOIN sys.columns C
		ON C.column_id = IC.column_id
		AND C.object_id = T.object_id
	WHERE T.name = 'Data.Migration.EmployerRequest'
		AND I.name = 'IX_Data.Migration.EmployerRequest_RecordType3'
		AND C.name = 'ExternalId'
		AND key_ordinal = 2
)
BEGIN
	DROP INDEX [Data.Migration.EmployerRequest].[IX_Data.Migration.EmployerRequest_RecordType3]
	
	CREATE INDEX 
		[IX_Data.Migration.EmployerRequest_RecordType3] 
	ON 
		[Data.Migration.EmployerRequest] (RecordType, [Status], BatchNumber)
END
GO

-- User --

IF EXISTS
(
	SELECT 
		1
	FROM 
		sys.tables T
	INNER JOIN sys.indexes I
		ON I.object_id = T.object_id
	INNER JOIN sys.index_columns IC
		ON IC.index_id = I.index_id
		AND IC.object_id = T.object_id
	INNER JOIN sys.columns C
		ON C.column_id = IC.column_id
		AND C.object_id = T.object_id
	WHERE T.name = 'Data.Migration.UserRequest'
		AND I.name = 'IX_Data.Migration.UserRequest_RecordType3'
		AND C.name = 'ExternalId'
		AND key_ordinal = 2
)
BEGIN
	DROP INDEX [Data.Migration.UserRequest].[IX_Data.Migration.UserRequest_RecordType3]
	
	CREATE INDEX 
		[IX_Data.Migration.UserRequest_RecordType3] 
	ON 
		[Data.Migration.UserRequest] (RecordType, [Status], BatchNumber)
END
GO
