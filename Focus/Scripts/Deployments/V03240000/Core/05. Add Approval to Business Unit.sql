﻿

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.BusinessUnit' AND COLUMN_NAME = 'ApprovalStatus')
BEGIN
  ALTER TABLE [dbo].[Data.Application.BusinessUnit] ADD [ApprovalStatus] [int] NOT NULL DEFAULT (0)
END
GO


UPDATE [dbo].[Data.Application.BusinessUnit]
SET    [dbo].[Data.Application.BusinessUnit].ApprovalStatus = [dbo].[Data.Application.Employer].ApprovalStatus
FROM   [dbo].[Data.Application.BusinessUnit]
INNER JOIN   [dbo].[Data.Application.Employer] ON [dbo].[Data.Application.BusinessUnit].EmployerId = [dbo].[Data.Application.Employer].Id
