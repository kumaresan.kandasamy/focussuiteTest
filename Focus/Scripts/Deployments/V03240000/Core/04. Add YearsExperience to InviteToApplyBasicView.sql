﻿IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Data.Application.InviteToApplyBasicView]'))
DROP VIEW [dbo].[Data.Application.InviteToApplyBasicView]
GO

CREATE VIEW [dbo].[Data.Application.InviteToApplyBasicView] 
AS
SELECT 
	ITA.Id AS Id,
	ITA.JobId AS JobId,		
	ITA.PersonId AS PersonId,
	PE.FirstName AS FirstName,
	PE.LastName AS LastName,
	R.TownCity AS Town,
	R.StateId AS StateId,
	ITA.CreatedOn AS CreatedOn,
	CAST(CAST(R.ResumeXml AS XML).query('(//branding/node())[1]') AS NVARCHAR(max)) AS Branding,
	R.IsContactInfoVisible AS IsContactInfoVisible,
	R.YearsExperience AS YearsExperience
FROM [Data.Application.InviteToApply] ITA
	INNER JOIN [Data.Application.Person] AS PE ON ITA.PersonId = PE.Id
	INNER JOIN [Data.Application.Resume] AS R ON ITA.PersonId = R.PersonId
WHERE CAST(ITA.PersonId AS NVARCHAR(10)) + '|' + CAST(JobId AS NVARCHAR(10)) NOT IN
	(SELECT CAST(R.PersonId AS NVARCHAR(10)) + '|' + CAST(p.JobId AS NVARCHAR(10)) FROM [Data.Application.Application] AS A
		INNER JOIN [Data.Application.Resume] AS R ON A.ResumeID = R.Id
		INNER JOIN [Data.Application.Posting] AS P ON A.PostingId = P.Id
	WHERE JobId IS NOT NULL)
AND R.IsDefault = 1
GO