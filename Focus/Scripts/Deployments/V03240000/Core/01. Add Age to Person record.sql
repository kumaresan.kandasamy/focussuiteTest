IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Person' AND COLUMN_NAME = 'Age')
BEGIN
	ALTER TABLE
		[Data.Application.Person]
	ADD
		Age SMALLINT NULL
END
GO

DECLARE @Comparison INT
DECLARE @CurrentDate DATETIME

SET @CurrentDate = CAST(CONVERT(VARCHAR, GETDATE(), 106) AS DATETIME)
SET @Comparison = MONTH(@CurrentDate) * 100 + DAY(@CurrentDate)

UPDATE
	P
SET
	DateOfBirth = ISNULL(R.DateOfBirth, P.DateOfBirth),
	Age = DATEDIFF(YY, CAST(ISNULL(R.DateOfBirth, P.DateOfBirth) AS DATETIME), @CurrentDate) 
		    - CASE 
				WHEN (MONTH(CAST(ISNULL(R.DateOfBirth, P.DateOfBirth) AS DATETIME)) * 100 + DAY(CAST(ISNULL(R.DateOfBirth, P.DateOfBirth) AS DATETIME))) > @Comparison THEN 1
				ELSE 0 
			END
FROM
	[Data.Application.Person] P
LEFT OUTER JOIN [Data.Application.Resume] R
	ON R.PersonId = P.Id
	AND R.IsDefault = 1
	AND R.StatusId = 1
WHERE	
	P.Age IS NULL
	AND ISNULL(R.DateOfBirth, P.DateOfBirth) IS NOT NULL
