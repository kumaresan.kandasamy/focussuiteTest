IF NOT EXISTS(SELECT * FROM Sys.indexes WHERE Object_Id = OBJECT_ID('[Data.Application.Person]') AND [name] = 'IX_Data.Application.Person_EmailAddress')
BEGIN
	CREATE INDEX [IX_Data.Application.Person_EmailAddress] ON [Data.Application.Person] (EmailAddress)
END

