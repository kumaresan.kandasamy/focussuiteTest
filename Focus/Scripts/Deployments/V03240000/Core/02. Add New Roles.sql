DECLARE @Role TABLE
(
	[Key] NVARCHAR(200),
	Value NVARCHAR(200)
)

DECLARE @NextID BIGINT
DECLARE @RowCount INT

SELECT @NextID = NextId FROM KeyTable

INSERT INTO @Role ([Key], Value)
VALUES ('AHomeBasedApprover', 'Assist Home-Based Approver')

INSERT INTO [Data.Application.Role] (Id, [Key], Value)
SELECT
	@NextId - 1 + ROW_NUMBER() OVER(ORDER BY TR.[Key] ASC),
	[Key],
	Value
FROM
	@Role TR
WHERE
	NOT EXISTS
	(
		SELECT
			1
		FROM 
			[Data.Application.Role] R
		WHERE
			R.[Key] = TR.[Key]
	)

GO