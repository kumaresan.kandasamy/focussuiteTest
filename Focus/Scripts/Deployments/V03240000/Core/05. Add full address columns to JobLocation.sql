﻿IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.JobLocation' AND COLUMN_NAME = 'AddressLine1')
BEGIN
alter table	dbo.[Data.Application.JobLocation]
add			[AddressLine1]	nvarchar(127)
END

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.JobLocation' AND COLUMN_NAME = 'City')
BEGIN
alter table	dbo.[Data.Application.JobLocation]
add			[City]			nvarchar(127)
END

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.JobLocation' AND COLUMN_NAME = 'State')
BEGIN
alter table	dbo.[Data.Application.JobLocation]
add			[State]			nvarchar(127)
END

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.JobLocation' AND COLUMN_NAME = 'Zip')
BEGIN			
alter table	dbo.[Data.Application.JobLocation]
add			[Zip]			nvarchar(10)
END