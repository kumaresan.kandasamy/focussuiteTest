﻿IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.application.job' AND COLUMN_NAME = 'ExtendVeteranPriority')
BEGIN
	ALTER TABLE [Data.application.job] ADD [ExtendVeteranPriority] INT NULL;
END
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.application.job' AND COLUMN_NAME = 'ClosingOnUpdated')
BEGIN
	ALTER TABLE [Data.application.job] ADD [ClosingOnUpdated] BIT NULL;
END
GO