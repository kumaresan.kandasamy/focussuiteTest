DECLARE @PersonEntityId INT
SELECT @PersonEntityId = Id FROM [Data.Core.EntityType] ET WHERE ET.Name = 'Person'
	
UPDATE
	AE
SET
	EntityTypeId = @PersonEntityId,
	EntityId = AE.EntityIdAdditional01,
	EntityIdAdditional01 = AE.EntityId
FROM
	[Data.Core.ActionEvent] AE
INNER JOIN [Data.Core.ActionType] AT
	ON AT.Id = AE.ActionTypeId
INNER JOIN [Data.Core.EntityType] ET
	ON ET.Id = AE.EntityTypeId
WHERE 
	AT.Name IN ('ActivateAccount', 'InactivateAccount', 'SendContactRequest',	'BlockUser', 'UnblockUser')
	AND ET.Name = 'User'
