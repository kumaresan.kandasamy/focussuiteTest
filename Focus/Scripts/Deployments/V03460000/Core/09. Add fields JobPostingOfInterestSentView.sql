IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Data.Application.JobPostingOfInterestSentView]'))
	DROP VIEW [dbo].[Data.Application.JobPostingOfInterestSentView]
GO

CREATE VIEW [Data.Application.JobPostingOfInterestSentView]
AS
SELECT
	jp.Id,
	jp.CreatedOn AS SentOn,
	jp.PersonId,
	p.JobTitle,
	p.EmployerName,
	p.LensPostingId,
	jp.Applied,
	COALESCE(P.JobLocation, J.Location, '') AS JobLocation,
	jp.Score,
	j.JobStatus,
	jp.IsRecommendation
FROM
	[Data.Application.JobPostingOfInterestSent] jp
INNER JOIN [Data.Application.Posting] p 
	ON jp.PostingId = p.Id
LEFT JOIN [Data.Application.Job] j 
	ON jp.JobId = j.Id
GO

