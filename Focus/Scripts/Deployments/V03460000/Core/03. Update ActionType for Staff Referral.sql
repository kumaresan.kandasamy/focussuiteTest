UPDATE
	AT
SET 
	Name = 'StaffReferralDeprecated'
FROM
	[Data.Core.ActionType] AT
WHERE
	AT.Name = 'StaffReferral'
	AND NOT EXISTS
	(
		SELECT
			1
		FROM
			[Data.Core.ActionType] AT2
		WHERE
			AT2.Name = 'StaffReferralDeprecated'
	)
	
SELECT
	*
FROM
	[Data.Core.ActionType]
WHERE
	Name IN ('StaffReferralDeprecated', 'StaffReferral', 'CreateCandidateApplication')

