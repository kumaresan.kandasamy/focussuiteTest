IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Posting' AND COLUMN_NAME = 'JobLocation')
BEGIN
	ALTER TABLE
		[Data.Application.Posting]
	ADD
		JobLocation NVARCHAR(200)
END
GO

IF OBJECT_ID('tempdb..#Posting') IS NOT NULL
    DROP TABLE #Posting

CREATE TABLE #Posting
(
	PostingId INT PRIMARY KEY,
	[Posting] XML
)
INSERT INTO #Posting 
(
	PostingId,
	[Posting]
)
SELECT 
	P.Id,
	CAST(P.PostingXml AS XML)
FROM 
	dbo.[Data.Application.Posting] P
WHERE
	P.PostingXml IS NOT NULL
	AND P.PostingXml NOT LIKE '<?xml%'
	
INSERT INTO #Posting 
(
	PostingId,
	[Posting]
)
SELECT 
	P.Id,
	CAST(SUBSTRING(P.PostingXml, CHARINDEX('?>', P.PostingXml) + 2, LEN(P.PostingXml) - CHARINDEX('?>', P.PostingXml) - 1) AS XML)
FROM 
	dbo.[Data.Application.Posting] P
WHERE
	P.PostingXml IS NOT NULL
	AND P.PostingXml LIKE '<?xml%'

;WITH PostingsView (PostingId, City, [State]) AS
(
	SELECT
		P.PostingId,
		c.value('city[1]', 'NVARCHAR(100)') AS City,
		c.value('state[1]', 'NVARCHAR(100)') AS [State]
	FROM
		#Posting P
	CROSS APPLY P.[Posting].nodes('//jobposting/JobDoc/posting/contact/address') as t(c)
)
UPDATE
	P
SET
	JobLocation = ISNULL(PV.[City] + ', ', '') + PV.[State]
FROM
	[Data.Application.Posting] P
INNER JOIN PostingsView PV
	ON PV.PostingId = P.Id
WHERE
	P.JobLocation IS NULL
