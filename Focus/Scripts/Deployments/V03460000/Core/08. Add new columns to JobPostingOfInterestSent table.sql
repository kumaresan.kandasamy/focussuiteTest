IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.JobPostingOfInterestSent' AND COLUMN_NAME = 'Score')
BEGIN
	ALTER TABLE
		[Data.Application.JobPostingOfInterestSent]
	ADD
		Score INT NULL
END
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.JobPostingOfInterestSent' AND COLUMN_NAME = 'Applied')
BEGIN
	ALTER TABLE
		[Data.Application.JobPostingOfInterestSent]
	ADD
		Applied BIT NOT NULL CONSTRAINT [DF_Data.Application.JobPostingOfInterestSent_Applied] DEFAULT(0)
END
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.JobPostingOfInterestSent' AND COLUMN_NAME = 'IsRecommendation')
BEGIN
	ALTER TABLE
		[Data.Application.JobPostingOfInterestSent]
	ADD
		IsRecommendation BIT NULL
END
GO