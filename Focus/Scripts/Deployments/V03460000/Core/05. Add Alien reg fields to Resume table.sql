﻿IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Resume' AND COLUMN_NAME = 'IsUSCitizen')
BEGIN
	ALTER TABLE dbo.[Data.Application.Resume] ADD [IsUSCitizen] BIT NULL;
END
GO
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Resume' AND COLUMN_NAME = 'IsPermanentResident')
BEGIN
	ALTER TABLE dbo.[Data.Application.Resume] ADD [IsPermanentResident] BIT NULL;
END
GO
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Resume' AND COLUMN_NAME = 'AlienRegistrationNumber')
BEGIN
	ALTER TABLE dbo.[Data.Application.Resume] ADD [AlienRegistrationNumber] NVARCHAR(10) NULL;
END
GO
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Resume' AND COLUMN_NAME = 'AlienRegExpiryDate')
BEGIN
	ALTER TABLE dbo.[Data.Application.Resume] ADD [AlienRegExpiryDate] DATETIME NULL;
END
GO

SET DATEFORMAT MDY

-- <citizen_flag>-1</citizen_flag>
UPDATE 
	r
SET 
	IsUSCitizen = 1,
	IsPermanentResident = NULL,
	AlienRegistrationNumber = NULL,
	AlienRegExpiryDate = NULL
FROM
	dbo.[Data.Application.Resume] r 
WHERE 
	CHARINDEX(N'<citizen_flag>-1</citizen_flag>', R.ResumeXml) > 0

-- <citizen_flag>0</citizen_flag>
;WITH ResumeDataSet AS
(
	SELECT	
		Id, 
		CHARINDEX(N'<alien_perm_flag>',ResumeXml) AS AlienPermFlagDataSetStartIndex, 
		CHARINDEX(N'</alien_perm_flag>',ResumeXml) AS AlienPermFlagDataSetEndIndex,
		CHARINDEX(N'<alien_id>',ResumeXml) AS AlienIdStartIndex, 
		CHARINDEX(N'</alien_id>',ResumeXml) AS AlienIdEndIndex,
		CHARINDEX(N'<alien_expires>',ResumeXml) AS AlienExpiresStartIndex, 
		CHARINDEX(N'</alien_expires>',ResumeXml) AS AlienExpiresEndIndex
	FROM
		dbo.[Data.Application.Resume]
	WHERE 
		CHARINDEX(N'<citizen_flag>0</citizen_flag>',ResumeXml) > 0
)
UPDATE
	R
SET
	IsUSCitizen = 0,
	IsPermanentResident = CASE RDS.AlienPermFlagDataSetStartIndex
		WHEN 0 THEN 0
		ELSE ABS(CONVERT(INT, SUBSTRING(R.ResumeXml, RDS.AlienPermFlagDataSetStartIndex + 17, RDS.AlienPermFlagDataSetEndIndex - RDS.AlienPermFlagDataSetStartIndex - 17)))
	END,
	AlienRegistrationNumber = CASE RDS.AlienIdStartIndex
		WHEN 0 THEN ''
		ELSE SUBSTRING(R.ResumeXml, RDS.AlienIdStartIndex + 10, RDS.AlienIdEndIndex - RDS.AlienIdStartIndex - 10)
	END,
	AlienRegExpiryDate = CASE RDS.AlienExpiresStartIndex
		WHEN 0 THEN NULL
		ELSE CONVERT(DATE, SUBSTRING(R.ResumeXml, RDS.AlienExpiresStartIndex + 15, RDS.AlienExpiresEndIndex - RDS.AlienExpiresStartIndex - 15))
	END
FROM
	ResumeDataSet RDS
INNER JOIN [Data.Application.Resume] R
	ON R.Id = RDS.Id
	