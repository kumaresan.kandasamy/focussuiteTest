﻿DELETE FROM 
	[Config.EmailTemplate] 
WHERE 
	EmailTemplateType = 48
	AND Body LIKE '%We regret to inform you that the alien certification registration date you entered on your resume has expired%'
	
IF NOT EXISTS(SELECT TOP 1 1 FROM [Config.EmailTemplate] WHERE EmailTemplateType = 48)
BEGIN
INSERT INTO [dbo].[Config.EmailTemplate]
           ([EmailTemplateType],
           [Subject],
           [Body],
           [Salutation],
           [Recipient],
           [SenderEmailType],
           [ClientSpecificEmailAddress])
     VALUES
           (48,
           'Expired Alien Registration Notification',
'We regret to inform you that the alien certification registration date you entered on your resume has expired. 
As a result you now may be unauthorized to work in the United States. 
If you have updated your certification status, it is imperative that you contact us with the current information so you will not miss potential job opportunities or important services that are available to you.
Until then, we are required by law to deny access to your #FOCUSCAREER# account. 
Our apologies for this inconvenience, but you will not be able to sign in until this matter is resolved.
Please contact your local Career Center for further assistance.

Regards, 
#FOCUSASSIST#',
           'Dear #RECIPIENTNAME#',
           null,
           1,
           null)
END