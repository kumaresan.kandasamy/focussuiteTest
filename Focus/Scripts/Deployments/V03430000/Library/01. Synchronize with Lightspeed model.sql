﻿IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS v WHERE v.TABLE_NAME = 'Library.OnetView')
	DROP VIEW [dbo].[Library.OnetView];
GO

CREATE VIEW [dbo].[Library.OnetView]
AS
SELECT
			o.Id,
			o.[Key], 
			oli.PrimaryValue AS Occupation,
			oli.SecondaryValue AS [Description], 
			o.JobFamilyId, 
			o.OnetCode,
			l.Culture,
			CONVERT(bit,CASE
				WHEN EXISTS (SELECT 1 FROM  [Library.JobTask] jt WITH (NOLOCK) WHERE jt.OnetId = o.Id)
					THEN 1
					ELSE 0
				END) AS JobTasksAvailable
FROM
			[Library.Onet] o WITH (NOLOCK)
			INNER JOIN [Library.OnetLocalisationItem] oli WITH (NOLOCK) ON o.[Key] = oli.[Key]
			INNER JOIN [Library.Localisation] l WITH (NOLOCK) ON oli.LocalisationId = l.Id
