﻿-- NOTE: If the following statement returns rows, then the index changes below will fail.

SELECT [FocusBusinessUnitId],COUNT(*)
								FROM [dbo].[Report.Employer]
								GROUP BY [FocusBusinessUnitId]
								HAVING COUNT(*) > 1;

-- The commented out script below to clean up the Report.Employer table before applying the index changes
-- If you are on a development or testing database and you want to keep the Report.Employer data, run this script to clean up the table so that you can then apply the index changes.
/*
-- This script will remove duplicate rows from the Report.Employer whilst keeping the latest row for each FocusBusinessUnitId
DECLARE @FocusBusinessUnitId int, @ReportEmployerId int;

WHILE EXISTS (	SELECT [FocusBusinessUnitId],COUNT(*)
								FROM [dbo].[Report.Employer]
								GROUP BY [FocusBusinessUnitId]
								HAVING COUNT(*) > 1
							)
BEGIN

	SELECT [FocusBusinessUnitId],COUNT(*)
	FROM [dbo].[Report.Employer]
	GROUP BY [FocusBusinessUnitId]
	HAVING COUNT(*) > 1;

	SELECT TOP 1 @FocusBusinessUnitId = [FocusBusinessUnitId]
	FROM [dbo].[Report.Employer]
	GROUP BY [FocusBusinessUnitId]
	HAVING COUNT(*) > 1;

	SELECT TOP 1 @ReportEmployerId = Id
	FROM [dbo].[Report.Employer]
	WHERE FocusBusinessUnitId = @FocusBusinessUnitId
	ORDER BY Id;
	
	DELETE FROM dbo.[Report.EmployerOffice]
	WHERE EmployerId = @ReportEmployerId;
	
	DELETE FROM dbo.[Report.EmployerAction]
	WHERE EmployerId = @ReportEmployerId;
	
	DELETE FROM [dbo].[Report.Employer]
	WHERE Id = @ReportEmployerId;
	
	PRINT 'Deleted row (Id = ' + CONVERT(varchar,@ReportEmployerId) + ')';

END
*/

IF EXISTS (SELECT * FROM sys.indexes WHERE name = 'IX_Messaging.Message_EnqueuedOn')
	DROP INDEX [IX_Messaging.Message_EnqueuedOn] ON [dbo].[Messaging.Message];
GO

CREATE NONCLUSTERED INDEX [IX_Messaging.Message_EnqueuedOn] ON [dbo].[Messaging.Message]
(
       [EnqueuedOn] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];
GO


--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Jim's comment: There is already an index on EnqueuedOn in the following but I added the above which reduced a query using the field from 9 minutes to 11 seconds so it’s worth doing.

IF EXISTS (SELECT * FROM sys.indexes WHERE name = 'IX_Report.JobSeeker_FocusPersonId')
	DROP INDEX [IX_Report.JobSeeker_FocusPersonId] ON [dbo].[Report.JobSeeker];
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_Report.JobSeeker_FocusPersonId] ON [dbo].[Report.JobSeeker]
(
       [FocusPersonId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];
GO


--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

IF EXISTS (SELECT * FROM sys.indexes WHERE name = 'IX_Report.Employer_FocusBusinessUnitId')
	DROP INDEX [IX_Report.Employer_FocusBusinessUnitId] ON [dbo].[Report.Employer];
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_Report.Employer_FocusBusinessUnitId] ON [dbo].[Report.Employer]
(
       [FocusBusinessUnitId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY];
GO
