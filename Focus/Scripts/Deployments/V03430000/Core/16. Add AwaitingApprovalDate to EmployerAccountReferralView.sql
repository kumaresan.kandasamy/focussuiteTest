IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'Data.Application.EmployerAccountReferralView')
BEGIN
	DROP VIEW [dbo].[Data.Application.EmployerAccountReferralView]
END
GO

CREATE VIEW [dbo].[Data.Application.EmployerAccountReferralView] AS
WITH EmployersAndEmployees AS
(
      SELECT
            Employer.Id AS EmployerId, 
            Employer.Name AS EmployerName, 
            ROW_NUMBER() OVER (PARTITION BY Employer.Id ORDER BY Employee.Id) AS EmployerEmployeeNumber,
            Employee.Id AS EmployeeId, 
            Person.FirstName AS EmployeeFirstName, 
            Person.LastName AS EmployeeLastName, 
			[User].CreatedOn AS AccountCreationDate,
			BusinessUnitAddress.Line1 AS EmployerAddressLine1, 
            BusinessUnitAddress.Line2 AS EmployerAddressLine2, 
			BusinessUnitAddress.TownCity AS EmployerAddressTownCity,
			BusinessUnitAddress.StateId AS EmployerAddressStateId, 
            BusinessUnitAddress.PostcodeZip AS EmployerAddressPostcodeZip, 
			BusinessUnitAddress.PublicTransitAccessible AS EmployerPublicTransitAccessible, 
			BusinessUnit.PrimaryPhone, 
            BusinessUnit.PrimaryPhoneType, 
			BusinessUnit.AlternatePhone1, 
			BusinessUnit.AlternatePhone1Type, 
			BusinessUnit.AlternatePhone2, 
			BusinessUnit.AlternatePhone2Type, 
            CASE WHEN BusinessUnit.PrimaryPhoneType = 'Phone' THEN BusinessUnit.PrimaryPhone WHEN BusinessUnit.PrimaryPhoneType = 'Mobile' THEN BusinessUnit.PrimaryPhone WHEN BusinessUnit.AlternatePhone1Type = 'Phone' THEN BusinessUnit.AlternatePhone1 WHEN BusinessUnit.AlternatePhone1Type = 'Mobile' THEN BusinessUnit.AlternatePhone1 WHEN BusinessUnit.AlternatePhone2Type = 'Phone' THEN BusinessUnit.AlternatePhone2 WHEN BusinessUnit.AlternatePhone2Type = 'Mobile' THEN BusinessUnit.AlternatePhone2 END AS EmployerPhoneNumber, 
            CASE WHEN BusinessUnit.PrimaryPhoneType = 'Fax' THEN BusinessUnit.PrimaryPhone WHEN BusinessUnit.AlternatePhone1Type = 'Fax' THEN BusinessUnit.AlternatePhone1 WHEN BusinessUnit.AlternatePhone2Type = 'Fax' THEN BusinessUnit.AlternatePhone2 END AS EmployerFaxNumber, 
			PhoneNumber.Number AS EmployeePhoneNumber, 
			FaxNumber.Number AS EmployeeFaxNumber, 
            BusinessUnit.Url AS EmployerUrl, 
            ISNULL(Employee.AwaitingApprovalDate, [User].CreatedOn) AS AwaitingApprovalDate,
            BusinessUnit.AwaitingApprovalDate AS BusinessUnitAwaitingApprovalDate,
			Employer.FederalEmployerIdentificationNumber AS EmployerFederalEmployerIdentificationNumber, 
            Employer.StateEmployerIdentificationNumber AS EmployerStateEmployerIdentificationNumber, 
			BusinessUnit.IndustrialClassification AS EmployerIndustrialClassification, 
            PersonAddress.Line1 AS EmployeeAddressLine1, 
			PersonAddress.Line2 AS EmployeeAddressLine2, 
			PersonAddress.TownCity AS EmployeeAddressTownCity, 
            PersonAddress.StateId AS EmployeeAddressStateId, 
			PersonAddress.PostcodeZip AS EmployeeAddressPostcodeZip, 
			Person.EmailAddress AS EmployeeEmailAddress, 
            Employee.ApprovalStatus AS EmployeeApprovalStatus,
			Employer.ApprovalStatus AS EmployerApprovalStatus, 
			BusinessUnit.ApprovalStatus As BusinessUnitApprovalStatus,
			BusinessUnit.OwnershipTypeId AS EmployerOwnershipTypeId, 
            Employer.AssignedToId AS EmployerAssignedToId, 
			Employee.RedProfanityWords, 
			Employee.YellowProfanityWords, 
			ISNULL(BusinessUnitDescription.Description, '') AS BusinessUnitDescription, 
            BusinessUnit.Name AS BusinessUnitName, 
			BusinessUnit.Id AS BusinessUnitId,
			 ROW_NUMBER() OVER (PARTITION BY BusinessUnit.Id ORDER BY Employee.Id) AS BusinessUnitEmployeeNumber,
			Employee.ApprovalStatusChangedBy, 
			Employee.ApprovalStatusChangedOn, 
            BusinessUnit.AccountTypeId AS EmployerAccountTypeId,
			BusinessUnit.IsPrimary AS EmployerIsPrimary,
			BusinessUnit.LegalName AS BusinessUnitLegalName,
			BusinessUnit.RedProfanityWords AS BusinessUnitRedProfanityWords,
			BusinessUnit.YellowProfanityWords AS BusinessUnitYellowProfanityWords,
			EmployeeBusinessUnit.[Default] AS BusinessUnitIsDefault,
			Person.SuffixId,
			CAST(Employer.LockVersion as nvarchar(10)) + '.' + CAST(BusinessUnit.LockVersion as nvarchar(10)) + '.' + CAST(Employee.LockVersion as nvarchar(10)) as AccountLockVersion
      FROM
      dbo.[Data.Application.Employer] AS Employer WITH (NOLOCK) 
      INNER JOIN dbo.[Data.Application.Employee] AS Employee WITH (NOLOCK) ON Employer.Id = Employee.EmployerId 
      INNER JOIN dbo.[Data.Application.Person] AS Person WITH (NOLOCK) ON Employee.PersonId = Person.Id    
	  INNER JOIN dbo.[Data.Application.User] AS [User] WITH (NOLOCK) ON Person.Id = [User].PersonId 
	  INNER JOIN dbo.[Data.Application.EmployeeBusinessUnit] AS EmployeeBusinessUnit WITH (NOLOCK) ON Employee.Id = EmployeeBusinessUnit.EmployeeId 
	  INNER JOIN dbo.[Data.Application.BusinessUnit] AS BusinessUnit WITH (NOLOCK) ON EmployeeBusinessUnit.BusinessUnitId = BusinessUnit.Id 
	  INNER JOIN dbo.[Data.Application.BusinessUnitAddress] AS BusinessUnitAddress WITH (NOLOCK) ON BusinessUnit.Id = BusinessUnitAddress.BusinessUnitId AND BusinessUnitAddress.IsPrimary = 1 
	  LEFT OUTER JOIN dbo.[Data.Application.BusinessUnitDescription] AS BusinessUnitDescription WITH (NOLOCK) ON BusinessUnit.Id = BusinessUnitDescription.BusinessUnitId AND BusinessUnitDescription.IsPrimary = 1 
	  LEFT OUTER JOIN dbo.[Data.Application.PersonAddress] AS PersonAddress WITH (NOLOCK) ON Person.Id = PersonAddress.PersonId 
	  LEFT OUTER JOIN dbo.[Data.Application.PhoneNumber] AS PhoneNumber WITH (NOLOCK) ON Person.Id = PhoneNumber.PersonId AND (PhoneNumber.PhoneType = 0 OR PhoneNumber.PhoneType = 1) AND PhoneNumber.IsPrimary = 1 
	  LEFT OUTER JOIN dbo.[Data.Application.PhoneNumber] AS FaxNumber WITH (NOLOCK) ON Person.Id = FaxNumber.PersonId AND FaxNumber.PhoneType = 4
)
SELECT 
	  Employer.EmployerId AS Id,
      Employer.EmployerId, 
      Employer.EmployerName, 
      Employer.EmployerApprovalStatus,
      1 AS TypeOfApproval,
      Employer.EmployeeId, 
      Employer.EmployeeFirstName, 
      Employer.EmployeeLastName, 
      Employer.EmployeeApprovalStatus,
	  Employer.BusinessUnitApprovalStatus,
	  Employer.AwaitingApprovalDate,
	  Employer.AccountCreationDate,
	  Employer.EmployerAddressLine1,
	  Employer.EmployerAddressLine2,
	  Employer.EmployerAddressTownCity,
	  Employer.EmployerAddressStateId,
	  Employer.EmployerAddressPostcodeZip,
	  Employer.EmployerPublicTransitAccessible,
	  Employer.PrimaryPhone,
	  Employer.PrimaryPhoneType,
	  Employer.AlternatePhone1,
	  Employer.AlternatePhone1Type,
	  Employer.AlternatePhone2,
	  Employer.AlternatePhone2Type,
	  Employer.EmployerPhoneNumber,
	  Employer.EmployerFaxNumber,
	  Employer.EmployeePhoneNumber,
	  Employer.EmployeeFaxNumber,
	  Employer.EmployerUrl,
	  Employer.EmployerFederalEmployerIdentificationNumber,
	  Employer.EmployerStateEmployerIdentificationNumber,
	  Employer.EmployerIndustrialClassification,
	  Employer.EmployeeAddressLine1,
	  Employer.EmployeeAddressLine2,
	  Employer.EmployeeAddressTownCity,
	  Employer.EmployeeAddressStateId,
	  Employer.EmployeeAddressPostcodeZip,
	  Employer.EmployeeEmailAddress,
	  Employer.EmployerOwnershipTypeId,
	  Employer.EmployerAssignedToId,
	  Employer.RedProfanityWords,
	  Employer.YellowProfanityWords,
	  Employer.BusinessUnitDescription,
	  Employer.BusinessUnitName,
	  Employer.BusinessUnitId,
	  Employer.ApprovalStatusChangedBy,
	  Employer.ApprovalStatusChangedOn,
	  Employer.EmployerAccountTypeId,
	  Employer.EmployerIsPrimary,
	  Employer.BusinessUnitLegalName,
	  Employer.BusinessUnitRedProfanityWords,
	  Employer.BusinessUnitYellowProfanityWords,
	  Employer.SuffixId,
	  Employer.AccountLockVersion
FROM
      EmployersAndEmployees AS Employer
WHERE 
      Employer.EmployerApprovalStatus IN (1, 4, 3)
      AND Employer.EmployerEmployeeNumber = 1
UNION 
SELECT
	  Employee.EmployeeId AS Id,
      Employee.EmployerId, 
      Employee.BusinessUnitName, 
      Employee.EmployerApprovalStatus,
      3 AS TypeOfApproval,
      Employee.EmployeeId, 
      Employee.EmployeeFirstName, 
      Employee.EmployeeLastName, 
      Employee.EmployeeApprovalStatus,
	  Employee.BusinessUnitApprovalStatus,
	  Employee.AwaitingApprovalDate,
	  Employee.AccountCreationDate,
	  Employee.EmployerAddressLine1,
	  Employee.EmployerAddressLine2,
	  Employee.EmployerAddressTownCity,
	  Employee.EmployerAddressStateId,
	  Employee.EmployerAddressPostcodeZip,
	  Employee.EmployerPublicTransitAccessible,
	  Employee.PrimaryPhone,
	  Employee.PrimaryPhoneType,
	  Employee.AlternatePhone1,
	  Employee.AlternatePhone1Type,
	  Employee.AlternatePhone2,
	  Employee.AlternatePhone2Type,
	  Employee.EmployerPhoneNumber,
	  Employee.EmployerFaxNumber,
	  Employee.EmployeePhoneNumber,
	  Employee.EmployeeFaxNumber,
	  Employee.EmployerUrl,
	  Employee.EmployerFederalEmployerIdentificationNumber,
	  Employee.EmployerStateEmployerIdentificationNumber,
	  Employee.EmployerIndustrialClassification,
	  Employee.EmployeeAddressLine1,
	  Employee.EmployeeAddressLine2,
	  Employee.EmployeeAddressTownCity,
	  Employee.EmployeeAddressStateId,
	  Employee.EmployeeAddressPostcodeZip,
	  Employee.EmployeeEmailAddress,
	  Employee.EmployerOwnershipTypeId,
	  Employee.EmployerAssignedToId,
	  Employee.RedProfanityWords,
	  Employee.YellowProfanityWords,
	  Employee.BusinessUnitDescription,
	  Employee.BusinessUnitName,
	  Employee.BusinessUnitId,
	  Employee.ApprovalStatusChangedBy,
	  Employee.ApprovalStatusChangedOn,
	  Employee.EmployerAccountTypeId,
	  Employee.EmployerIsPrimary,
	  Employee.BusinessUnitLegalName,
	  Employee.BusinessUnitRedProfanityWords,
	  Employee.BusinessUnitYellowProfanityWords,
	  Employee.SuffixId,
	  Employee.AccountLockVersion
FROM
      EmployersAndEmployees AS Employee
WHERE 
      Employee.EmployeeApprovalStatus IN (1, 4, 3)
      AND 
      (
		(Employee.BusinessUnitEmployeeNumber > 1 AND Employee.EmployerEmployeeNumber > 1)
		OR (Employee.EmployerApprovalStatus IN (0, 2) AND Employee.BusinessUnitApprovalStatus  IN (0, 2) AND Employee.BusinessUnitIsDefault = 1)
	  )
	  AND NOT EXISTS
	  (
		SELECT
			1
		FROM
			[Data.Application.EmployeeBusinessUnit] EBU
		INNER JOIN [Data.Application.BusinessUnit] BU
			ON BU.Id = EBU.BusinessUnitId
		WHERE
			EBU.EmployeeId = Employee.EmployeeId
			AND BU.Id <> Employee.BusinessUnitId
			AND BU.ApprovalStatus IN (1, 3, 4)
	  )
UNION
SELECT 
      BusinessUnit.BusinessUnitId AS Id,
	  BusinessUnit.EmployerId, 
      BusinessUnit.BusinessUnitName, 
      BusinessUnit.EmployerApprovalStatus,
      2 AS TypeOfApproval,
      BusinessUnit.EmployeeId, 
      BusinessUnit.EmployeeFirstName, 
      BusinessUnit.EmployeeLastName, 
      BusinessUnit.EmployeeApprovalStatus,
	  BusinessUnit.BusinessUnitApprovalStatus,
	  ISNULL(BusinessUnit.BusinessUnitAwaitingApprovalDate, BusinessUnit.AwaitingApprovalDate) AS AwaitingApprovalDate,
	  BusinessUnit.AccountCreationDate,
	  BusinessUnit.EmployerAddressLine1,
	  BusinessUnit.EmployerAddressLine2,
	  BusinessUnit.EmployerAddressTownCity,
	  BusinessUnit.EmployerAddressStateId,
	  BusinessUnit.EmployerAddressPostcodeZip,
	  BusinessUnit.EmployerPublicTransitAccessible,
	  BusinessUnit.PrimaryPhone,
	  BusinessUnit.PrimaryPhoneType,
	  BusinessUnit.AlternatePhone1,
	  BusinessUnit.AlternatePhone1Type,
	  BusinessUnit.AlternatePhone2,
	  BusinessUnit.AlternatePhone2Type,
	  BusinessUnit.EmployerPhoneNumber,
	  BusinessUnit.EmployerFaxNumber,
	  BusinessUnit.EmployeePhoneNumber,
	  BusinessUnit.EmployeeFaxNumber,
	  BusinessUnit.EmployerUrl,
	  BusinessUnit.EmployerFederalEmployerIdentificationNumber,
	  BusinessUnit.EmployerStateEmployerIdentificationNumber,
	  BusinessUnit.EmployerIndustrialClassification,
	  BusinessUnit.EmployeeAddressLine1,
	  BusinessUnit.EmployeeAddressLine2,
	  BusinessUnit.EmployeeAddressTownCity,
	  BusinessUnit.EmployeeAddressStateId,
	  BusinessUnit.EmployeeAddressPostcodeZip,
	  BusinessUnit.EmployeeEmailAddress,
	  BusinessUnit.EmployerOwnershipTypeId,
	  BusinessUnit.EmployerAssignedToId,
	  BusinessUnit.RedProfanityWords,
	  BusinessUnit.YellowProfanityWords,
	  BusinessUnit.BusinessUnitDescription,
	  BusinessUnit.BusinessUnitName,
	  BusinessUnit.BusinessUnitId,
	  BusinessUnit.ApprovalStatusChangedBy,
	  BusinessUnit.ApprovalStatusChangedOn,
	  BusinessUnit.EmployerAccountTypeId,
	  BusinessUnit.EmployerIsPrimary,
	  BusinessUnit.BusinessUnitLegalName,
	  BusinessUnit.BusinessUnitRedProfanityWords,
	  BusinessUnit.BusinessUnitYellowProfanityWords,
	  BusinessUnit.SuffixId,
	  BusinessUnit.AccountLockVersion
FROM
      EmployersAndEmployees AS BusinessUnit
WHERE 
      BusinessUnit.BusinessUnitApprovalStatus IN (1, 4, 3) 
      AND (BusinessUnit.EmployerIsPrimary = 0 OR BusinessUnit.EmployerApprovalStatus IN (0, 2))
	  AND BusinessUnit.BusinessUnitEmployeeNumber = 1


GO


