﻿----------------------------------------------------------------------------------------------------------------------------------------------------------------
-- Set Job Title back to NOT NULL
DECLARE @IsNullable varchar(3) = NULL
SELECT @IsNullable = IS_NULLABLE FROM INFORMATION_SCHEMA.COLUMNS c WHERE c.TABLE_NAME = 'Data.Application.Job' AND c.COLUMN_NAME = 'JobTitle';
IF @IsNullable <> 'NO'
BEGIN
	ALTER TABLE [dbo].[Data.Application.Job] ALTER COLUMN JobTitle nvarchar(200) NOT NULL;
END
GO
------------------------------------------------------------------------------------------------------------------------------------------------------------------ Add missing key
DECLARE @ConstraintName sysname;

SELECT @ConstraintName = CONSTRAINT_NAME FROM INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE WHERE TABLE_NAME = 'Data.Application.ReferralEmail' AND COLUMN_NAME = 'JobId'
IF @ConstraintName IS NOT NULL
BEGIN
	EXEC ('ALTER TABLE dbo.[Data.Application.ReferralEmail] DROP CONSTRAINT [' + @ConstraintName + ']');
END
ALTER TABLE [dbo].[Data.Application.ReferralEmail]
    ADD CONSTRAINT [FK_Data.Application.ReferralEmail_JobId] FOREIGN KEY ([JobId]) REFERENCES [dbo].[Data.Application.Job] ([Id]);
GO
------------------------------------------------------------------------------------------------------------------------------------------------------------------ Add mising keys for new table

DECLARE @ConstraintName sysname;

SELECT @ConstraintName = CONSTRAINT_NAME FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE TABLE_NAME = 'Data.Application.JobPrevData' AND CONSTRAINT_TYPE = 'PRIMARY KEY'
IF @ConstraintName IS NOT NULL
BEGIN
	EXEC ('ALTER TABLE dbo.[Data.Application.JobPrevData] DROP CONSTRAINT [' + @ConstraintName + ']');
END
ALTER TABLE [dbo].[Data.Application.JobPrevData]
    ADD CONSTRAINT [PK_Data.Application.JobPrevData] PRIMARY KEY ([Id]);
GO

--------------------------------

DECLARE @ConstraintName sysname;

SELECT @ConstraintName = CONSTRAINT_NAME FROM INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE WHERE TABLE_NAME = 'Data.Application.JobPrevData' AND COLUMN_NAME = 'JobId'
IF @ConstraintName IS NOT NULL
BEGIN
	EXEC ('ALTER TABLE dbo.[Data.Application.JobPrevData] DROP CONSTRAINT [' + @ConstraintName + ']');
END
ALTER TABLE [dbo].[Data.Application.JobPrevData]
    ADD CONSTRAINT [FK_Data.Application.JobPrevData_JobId] FOREIGN KEY ([JobId]) REFERENCES [dbo].[Data.Application.Job] ([Id]);
GO
