IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Data.Application.Document')
BEGIN

	CREATE TABLE [dbo].[Data.Application.Document](
		[Id] [bigint] NOT NULL,
		[File] [varbinary](max) NOT NULL,
		[Title] [nvarchar](200) NOT NULL,
		[Description] [nvarchar](2000) NULL,
		[Category] [bigint] NOT NULL,
		[Group] [bigint] NOT NULL,
		[Order] [int] NOT NULL,
		[Module] [int] NOT NULL,
		[FileName] [nvarchar](200) NOT NULL
	)

END
GO

IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_NAME = 'PK_Data.Application.Document')
BEGIN
	ALTER TABLE [dbo].[Data.Application.Document] 
    ADD CONSTRAINT [PK_Data.Application.Document] PRIMARY KEY CLUSTERED ([Id] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF);
END

IF NOT EXISTS (SELECT * FROM sys.default_constraints WHERE name = 'DF_Data.Application.Document_Title')
BEGIN
	ALTER TABLE [dbo].[Data.Application.Document]
	    ADD CONSTRAINT [DF_Data.Application.Document_Title] DEFAULT ('') FOR [Title];
END

IF NOT EXISTS (SELECT * FROM sys.default_constraints WHERE name = 'DF_Data.Application.Document_Category')
BEGIN
	ALTER TABLE [dbo].[Data.Application.Document]
	    ADD CONSTRAINT [DF_Data.Application.Document_Category] DEFAULT ((0)) FOR [Category];
END

IF NOT EXISTS (SELECT * FROM sys.default_constraints WHERE name = 'DF_Data.Application.Document_Group')
BEGIN
	ALTER TABLE [dbo].[Data.Application.Document]
	    ADD CONSTRAINT [DF_Data.Application.Document_Group] DEFAULT ((0)) FOR [Group];
END

IF NOT EXISTS (SELECT * FROM sys.default_constraints WHERE name = 'DF_Data.Application.Document_Module')
BEGIN
	ALTER TABLE [dbo].[Data.Application.Document]
	    ADD CONSTRAINT [DF_Data.Application.Document_Module] DEFAULT ((0)) FOR [Module];
END

IF NOT EXISTS (SELECT * FROM sys.default_constraints WHERE name = 'DF_Data.Application.Document_FileName')
BEGIN
	ALTER TABLE [dbo].[Data.Application.Document]
	    ADD CONSTRAINT [DF_Data.Application.Document_FileName] DEFAULT ('') FOR [FileName];
END



