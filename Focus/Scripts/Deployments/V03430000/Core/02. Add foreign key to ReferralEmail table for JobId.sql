IF NOT EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_Data.Application.ReferralEmail_JobId]') AND parent_object_id = OBJECT_ID(N'[dbo].[Data.Application.ReferralEmail]'))
BEGIN
	ALTER TABLE [dbo].[Data.Application.ReferralEmail]
		ADD CONSTRAINT [FK_Data.Application.ReferralEmail_JobId] FOREIGN KEY ([JobId]) REFERENCES [dbo].[Data.Application.Job] ([Id]) ON DELETE NO ACTION ON UPDATE NO ACTION;
END