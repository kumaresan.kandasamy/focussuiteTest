DECLARE @Configs TABLE
(
	OldCode NVARCHAR(100),
	NewCode NVARCHAR(100),
	Name NVARCHAR(100)
)
INSERT INTO @Configs (OldCode, NewCode, Name)
VALUES 
	('Activity.300', 'Activity.728', 'Info: Bonding'),
	('Activity.659', 'Activity.729', 'Orientation: Ex-Offender')
	
UPDATE
	CA
	
SET
	LocalisationKey = T.NewCode
FROM
	[Config.Activity] CA
INNER JOIN @Configs T
	ON T.OldCode = CA.LocalisationKey
	AND T.Name = CA.Name
WHERE
	EXISTS
	(
		SELECT
			1
		FROM
			[Config.Activity] CA2
		WHERE
			CA2.LocalisationKey = T.OldCode
			AND CA2.Name <> T.Name
	)
	AND NOT EXISTS
	(
		SELECT
			1
		FROM
			[Config.Activity] CA3
		WHERE
			CA3.LocalisationKey = T.NewCode
	)
