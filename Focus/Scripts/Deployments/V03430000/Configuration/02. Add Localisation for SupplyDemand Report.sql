DECLARE @LocalisationId BIGINT
DECLARE @Localisations TABLE
(
	[Key] NVARCHAR(100),
	Value NVARCHAR(250)
)
INSERT INTO @Localisations VALUES
	('Global.SupplyDemandReport.Report.SupplyCount', 'Number of resumes'),
	('Global.SupplyDemandReport.Report.DemandCount', 'Number of postings')

SELECT
	@LocalisationId = Localisation.Id
FROM
	[Config.Localisation] Localisation
WHERE
	Localisation.Culture = '**-**';

INSERT INTO [Config.LocalisationItem]
(
	[Key],
	Value,
	LocalisationId
)
SELECT
	TL.[Key],
	TL.Value,
	@LocalisationId
FROM
	@Localisations TL
WHERE 
	NOT EXISTS
	(
		SELECT 
			1 
		FROM 
			[Config.LocalisationItem] LI
		WHERE 
			LI.[Key] = TL.[Key]
	)
		