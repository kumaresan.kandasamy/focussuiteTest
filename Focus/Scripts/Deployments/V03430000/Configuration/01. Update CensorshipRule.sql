DECLARE @Type INT
DECLARE @Level INT

Set @Type = 1
Set @Level = 2

--INSERTS
IF NOT EXISTS(SELECT 1 FROM [dbo].[Config.CensorshipRule] WHERE [Phrase] = 'bad credit')
BEGIN
	INSERT INTO [dbo].[Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList)
	VALUES (@Type, @Level,'bad credit','')
END

IF NOT EXISTS(SELECT 1 FROM [dbo].[Config.CensorshipRule] WHERE [Phrase] = 'background')
BEGIN
	INSERT INTO [dbo].[Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList)
	VALUES (@Type, @Level,'background','')
END

IF NOT EXISTS(SELECT 1 FROM [dbo].[Config.CensorshipRule] WHERE [Phrase] = 'clean credit')
BEGIN
	INSERT INTO [dbo].[Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList)
	VALUES (@Type, @Level,'clean credit','')
END

IF NOT EXISTS(SELECT 1 FROM [dbo].[Config.CensorshipRule] WHERE [Phrase] = 'credit')
BEGIN
	INSERT INTO [dbo].[Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList)
	VALUES (@Type, @Level,'credit','')
END

IF NOT EXISTS(SELECT 1 FROM [dbo].[Config.CensorshipRule] WHERE [Phrase] = 'credit approval*')
BEGIN
	INSERT INTO [dbo].[Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList)
	VALUES (@Type, @Level,'credit approval*','')
END

IF NOT EXISTS(SELECT 1 FROM [dbo].[Config.CensorshipRule] WHERE [Phrase] = 'credit background')
BEGIN
	INSERT INTO [dbo].[Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList)
	VALUES (@Type, @Level,'credit background','')
END

IF NOT EXISTS(SELECT 1 FROM [dbo].[Config.CensorshipRule] WHERE [Phrase] = 'credit card')
BEGIN
	INSERT INTO [dbo].[Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList)
	VALUES (@Type, @Level,'credit card','')
END

IF NOT EXISTS(SELECT 1 FROM [dbo].[Config.CensorshipRule] WHERE [Phrase] = 'credit card required')
BEGIN
	INSERT INTO [dbo].[Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList)
	VALUES (@Type, @Level,'credit card required','')
END

IF NOT EXISTS(SELECT 1 FROM [dbo].[Config.CensorshipRule] WHERE [Phrase] = 'credit counsel*')
BEGIN
	INSERT INTO [dbo].[Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList)
	VALUES (@Type, @Level,'credit counsel*','')
END

IF NOT EXISTS(SELECT 1 FROM [dbo].[Config.CensorshipRule] WHERE [Phrase] = 'credit check')
BEGIN
	INSERT INTO [dbo].[Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList)
	VALUES (@Type, @Level,'credit check','')
END

IF NOT EXISTS(SELECT 1 FROM [dbo].[Config.CensorshipRule] WHERE [Phrase] = 'credit review')
BEGIN
	INSERT INTO [dbo].[Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList)
	VALUES (@Type, @Level,'credit review','')
END

IF NOT EXISTS(SELECT 1 FROM [dbo].[Config.CensorshipRule] WHERE [Phrase] = 'exoffender')
BEGIN
	INSERT INTO [dbo].[Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList)
	VALUES (@Type, @Level,'exoffender','')
END

IF NOT EXISTS(SELECT 1 FROM [dbo].[Config.CensorshipRule] WHERE [Phrase] = 'exoffender background check')
BEGIN
	INSERT INTO [dbo].[Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList)
	VALUES (@Type, @Level,'exoffender background check','')
END

IF NOT EXISTS(SELECT 1 FROM [dbo].[Config.CensorshipRule] WHERE [Phrase] = 'felony-free workplace')
BEGIN
	INSERT INTO [dbo].[Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList)
	VALUES (@Type, @Level,'felony-free workplace','')
END

IF NOT EXISTS(SELECT 1 FROM [dbo].[Config.CensorshipRule] WHERE [Phrase] = 'good credit')
BEGIN
	INSERT INTO [dbo].[Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList)
	VALUES (@Type, @Level,'good credit','')
END

IF NOT EXISTS(SELECT 1 FROM [dbo].[Config.CensorshipRule] WHERE [Phrase] = 'no credit')
BEGIN
	INSERT INTO [dbo].[Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList)
	VALUES (@Type, @Level,'no credit','')
END

IF NOT EXISTS(SELECT 1 FROM [dbo].[Config.CensorshipRule] WHERE [Phrase] = 'poor credit')
BEGIN
	INSERT INTO [dbo].[Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList)
	VALUES (@Type, @Level,'poor credit','')
END

--Update Match type to Yellow words 
 Update [dbo].[Config.CensorshipRule] 
 Set [Level] = 2
 Where [Level] = 1

 Update [dbo].[Config.CensorshipRule] 
 Set [Type] = 1
 Where [Type] = 2

