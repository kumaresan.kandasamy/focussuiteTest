DECLARE @NextID BIGINT
DECLARE @RowsAdded INT

SELECT @NextID = NextId FROM KeyTable

INSERT INTO [Report.Employer]
(
	Id,
	Name,
	FederalEmployerIdentificationNumber,
	StateId,
	PostalCode,
	Office,
	CountyId,
	FocusBusinessUnitId,
	[State],
	County,
	LatitudeRadians,
	LongitudeRadians,
	CreatedOn,
	UpdatedOn,
	FocusEmployerId,
	AccountTypeId,
	NoOfEmployees
)
SELECT
	@NextID + (ROW_NUMBER() OVER (ORDER BY BU.Id ASC)) - 1,
	BU.Name,
	E.FederalEmployerIdentificationNumber,
	BUA.StateId,
	BUA.PostcodeZip,
	'',
	BUA.CountyId,
	BU.Id,
	LI_S.Value,
	LI_C.Value,
	0,
	0,
	E.CreatedOn,
	E.UpdatedOn,
	E.Id,
	NULL,
	NULL
FROM
	[Data.Application.BusinessUnit] BU
INNER JOIN [Data.Application.Employer] E
	ON E.Id = BU.EmployerId
INNER JOIN [Data.Application.BusinessUnitAddress] BUA
	ON BUA.BusinessUnitId = BU.Id
	AND BUA.IsPrimary = 1
INNER JOIN [Config.LookupItemsView] LI_S
	ON LI_S.Id = BUA.StateId
INNER JOIN [Config.LookupItemsView] LI_C
	ON LI_C.Id = BUA.CountyId
WHERE
	NOT EXISTS
	(
		SELECT 
			1
		FROM
			[Report.Employer] RE
		WHERE 
			RE.FocusBusinessUnitId = BU.Id
	)

SET @RowsAdded = @@ROWCOUNT

IF @RowsAdded > 0 
BEGIN
	UPDATE [KeyTable] SET NextId = NextId + @RowsAdded
END
