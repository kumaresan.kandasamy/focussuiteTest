DECLARE @MessageTypeName NVARCHAR(450)
DECLARE @Version NVARCHAR(10)

SET @MessageTypeName = 'Focus.Services.Messages.ProcessSessionMaintenanceMessage, Focus.Services, Version=2.0.6.0, Culture=neutral, PublicKeyToken=null'
SET @Version = '3.29.0000'

IF NOT EXISTS(SELECT 1 FROM [Messaging.MessageType] WHERE TypeName = @MessageTypeName)
BEGIN
	DECLARE @MessageTypeId UNIQUEIDENTIFIER
	DECLARE @MessageId UNIQUEIDENTIFIER
	DECLARE @MessagePayloadId UNIQUEIDENTIFIER
	
	SET @MessageTypeId = NEWID()
	SET @MessageId = NEWID()
	SET @MessagePayloadId = NEWID()
	
	BEGIN TRANSACTION
	
	INSERT INTO [Messaging.MessageType]
	(
		Id,
		TypeName,
		SchedulePlan
	)
	VALUES
	(
		@MessageTypeId,
		@MessageTypeName,
		N'{"ScheduleType":1,"Interval":1,"StartMinutes":0,"StartHour":null,"StartDay":null}'
	)
	
	IF @@ERROR <> 0
	BEGIN
		ROLLBACK TRANSACTION
		RETURN
	END
	
	INSERT INTO [Messaging.MessagePayload]
	(
		Id,
		Data
	)
	VALUES
	(
		@MessagePayloadId,
		N'{"Id":"' + LOWER(@MessageId) + N'","Version":"' + @Version + N'"}'
	)
	
	IF @@ERROR <> 0
	BEGIN
		ROLLBACK TRANSACTION
		RETURN
	END

	INSERT INTO [Messaging.Message]
	(
		Id,
		[Status],
		Priority,
		EnqueuedOn,
		MessageTypeId,
		MessagePayloadId,
		[Version],
		ProcessOn
	)
	VALUES
	(
		@MessageId,
		1,
		2,
		GETUTCDATE(),
		@MessageTypeId,
		@MessagePayloadId,
		@Version,
		DATEADD(HOUR, DATEPART(HOUR, GETDATE()) + 1, CAST(CONVERT(DATE, GETDATE()) AS DATETIME))
	)
	
	IF @@ERROR <> 0
	BEGIN
		ROLLBACK TRANSACTION
		RETURN
	END
	
	INSERT INTO [Messaging.MessageLog]
	(
		Id,
		[Timestamp],
		Details,
		MessageId
	)
	VALUES
	(
		NEWID(),
		GETUTCDATE(),
		N'Enqueued @ ' + CONVERT(NVARCHAR(30), GETUTCDATE(), 120),
		@MessageId
	)	
	
	IF @@ERROR <> 0
	BEGIN
		ROLLBACK TRANSACTION
		RETURN
	END
	
	COMMIT TRANSACTION
END