﻿DECLARE @LocalisationId BIGINT

SELECT @LocalisationId = Id FROM [Config.Localisation] WHERE Culture = '**-**'
DELETE FROM [Config.LocalisationItem] WHERE [Key] = 'PostingLowQualityResume.Text'
IF NOT EXISTS(SELECT 1 FROM [Config.LocalisationItem] WHERE [Key] = 'PostingLowQualityResume.Text')
	BEGIN
		RAISERROR ('Start - Localisation Item - PostingLowQualityResume.Text', 0, 1) WITH NOWAIT

		INSERT INTO [Config.LocalisationItem] ([ContextKey], [Key], [Value], [Localised], [LocalisationId])
		VALUES ('', 'PostingLowQualityResume.Text', 'Your default resume is defined as low quality because it doesn''t contain many words or skills. Please review your resume to see if you have additional skills that should be added', 0, @LocalisationId)
		
		RAISERROR ('End - Localisation Item - PostingLowQualityResume.Text', 0, 1) WITH NOWAIT
END




