﻿DECLARE @NextId BIGINT

DECLARE @CountyId BIGINT
DECLARE @StateId BIGINT
DECLARE @CountryId BIGINT

SELECT @CountyId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Counties' AND [Key] = 'County.ClayTX'
SELECT @StateId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'States' AND [Key] = 'State.TX'
SELECT @CountryId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Countries' AND [Key] = 'Country.US'


DECLARE @OfficeName NVARCHAR(20)
DECLARE @DefaultAdminId BIGINT

DECLARE @DevClientPersonId BIGINT

SELECT @DevClientPersonId = Id FROM [Data.Application.Person] WHERE EmailAddress = 'Default-Administrator@assist.com'

DECLARE @Offices TABLE
(
	Id BIGINT IDENTITY(1, 1),
	OfficeName NVARCHAR(20),
	DefaultAdminId BIGINT
)

INSERT INTO @Offices 
	(OfficeName, DefaultAdminId)
VALUES 
	('Office with staff', NULL),
	('Default admin office', @DevClientPersonId)

DECLARE @OfficeCount INT
DECLARE @TotalOffices INT

SET @OfficeCount = 1
SELECT @TotalOffices = COUNT(1) FROM @Offices

WHILE @OfficeCount <= @TotalOffices
BEGIN
	SELECT 
		@OfficeName = OfficeName,
		@DefaultAdminId = DefaultAdminId
	FROM
		@Offices
	WHERE
		Id = @OfficeCount

	IF NOT EXISTS(SELECT 1 FROM [Data.Application.Office] WHERE OfficeName = @OfficeName)
	BEGIN

		BEGIN TRANSACTION 
			SELECT @NextId = NextId FROM KeyTable
					
			UPDATE KeyTable SET NextId = NextId + 1
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRANSACTION
				RETURN
			END
					
			INSERT INTO [dbo].[Data.Application.Office]
			  (	Id,
				OfficeName,
				Line1,
				Line2,
				TownCity,
				CountyId,
				StateId,
				CountryId,
				PostcodeZip,
				AssignedPostcodeZip,
				UnassignedDefault,
				OfficeManagerMailbox,
				ExternalId,
				InActive,
				DefaultAdministratorId,
				BusinessOutreachMailbox
				)
			VALUES
			(@NextId,
			@OfficeName,
			'Address Line 1',
			'',
			'Bluegrove',
			@CountyId,
			@StateId,
			@CountryId,
			'76352',
			NULL,
			NULL,
			'manager@office.com',
			'987654',
			0,
			@DefaultAdminId,
			'business@outreach.com'
			)
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRANSACTION
				RETURN
			END

			DECLARE @Staff TABLE
			(
				Id BIGINT IDENTITY(1, 1),
				PersonId BIGINT
			)

			INSERT INTO @Staff
				(PersonId)
			SELECT TOP 3 PersonId 
			FROM [dbo].[Data.Application.User] 
			WHERE UserType = 1

			DECLARE @StaffCount INT
			DECLARE @TotalStaff INT

			SET @StaffCount = 1
			SELECT @TotalStaff = COUNT(1) FROM @Staff

			DECLARE @POMNextId BIGINT
			DECLARE @StaffId BIGINT

			WHILE @StaffCount <= @TotalStaff
			BEGIN
				SELECT 
					@StaffId = PersonId
				FROM
					@Staff
				WHERE
					Id = @StaffCount

				SELECT @POMNextId	 = NextId FROM KeyTable
					
				UPDATE KeyTable SET NextId = NextId + 1
			
				IF @@ERROR <> 0
				BEGIN
					ROLLBACK TRANSACTION
					RETURN
				END

				INSERT INTO [dbo].[Data.Application.PersonOfficeMapper]
					(Id,
					StateId,
					PersonId,
					OfficeId,
					OfficeUnassigned,
					CreatedOn)
				VALUES
				(
					@POMNextId,
					null,
					@StaffId,
					@NextId,
					null,
					GETDATE()
				)
				
				IF @@ERROR <> 0
				BEGIN
					ROLLBACK TRANSACTION
					RETURN
				END
				
				SET @StaffCount = @StaffCount + 1
			END

		COMMIT TRANSACTION
	END
	SET @OfficeCount = @OfficeCount + 1
END