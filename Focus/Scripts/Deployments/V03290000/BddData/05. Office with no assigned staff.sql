﻿DECLARE @NextId BIGINT

DECLARE @CountyId BIGINT
DECLARE @StateId BIGINT
DECLARE @CountryId BIGINT

SELECT @CountyId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Counties' AND [Key] = 'County.ClayTX'
SELECT @StateId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'States' AND [Key] = 'State.TX'
SELECT @CountryId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Countries' AND [Key] = 'Country.US'


DECLARE @OfficeName NVARCHAR(20)

DECLARE @Offices TABLE
(
	Id BIGINT IDENTITY(1, 1),
	OfficeName NVARCHAR(20)
)

INSERT INTO @Offices 
	(OfficeName)
VALUES 
	('Office with no staff')

DECLARE @OfficeCount INT
DECLARE @TotalOffices INT

SET @OfficeCount = 1
SELECT @TotalOffices = COUNT(1) FROM @Offices

WHILE @OfficeCount <= @TotalOffices
BEGIN
	SELECT 
		@OfficeName = OfficeName
	FROM
		@Offices
	WHERE
		Id = @OfficeCount

	IF NOT EXISTS(SELECT 1 FROM [Data.Application.Office] WHERE OfficeName = @OfficeName)
	BEGIN

		BEGIN TRANSACTION 
			SELECT @NextId = NextId FROM KeyTable
					
			UPDATE KeyTable SET NextId = NextId + 1
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRANSACTION
				RETURN
			END
					
			INSERT INTO [dbo].[Data.Application.Office]
			  (	Id,
				OfficeName,
				Line1,
				Line2,
				TownCity,
				CountyId,
				StateId,
				CountryId,
				PostcodeZip,
				AssignedPostcodeZip,
				UnassignedDefault,
				OfficeManagerMailbox,
				ExternalId,
				InActive,
				DefaultAdministratorId,
				BusinessOutreachMailbox
				)
			VALUES
			(@NextId,
			@OfficeName,
			'Address Line 1',
			'',
			'Bluegrove',
			@CountyId,
			@StateId,
			@CountryId,
			'76352',
			NULL,
			NULL,
			'manager@office.com',
			'872431', --this needs to be unique on the db
			0,
			NULL,
			'business@outreach.com'
			)
			
			IF @@ERROR <> 0
			BEGIN
				ROLLBACK TRANSACTION
				RETURN
			END

		COMMIT TRANSACTION
	END
	SET @OfficeCount = @OfficeCount + 1
END