IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'Data.Application.StaffSearchView')
	DROP VIEW [dbo].[Data.Application.StaffSearchView]
GO

CREATE VIEW [dbo].[Data.Application.StaffSearchView]
AS

SELECT 
	[User].Id, 
	Person.FirstName, 
	Person.LastName,
	Person.EmailAddress,
	Person.ExternalOffice,
	[User].[Enabled],
	Person.Id AS PersonId,
	Person.Manager,
	[User].Blocked
FROM  
	[Data.Application.Person] AS Person WITH (NOLOCK)
	INNER JOIN dbo.[Data.Application.User] AS [User] WITH (NOLOCK) ON Person.Id = [User].PersonId
WHERE
	[User].UserType = 1
GO



