﻿SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Config.DefaultLocalisationItem]') AND type in (N'U'))
BEGIN

	CREATE TABLE [dbo].[Config.DefaultLocalisationItem](
		[Id] [bigint] IDENTITY(1,1) NOT NULL,
		[Key] [nvarchar](200) NOT NULL DEFAULT (''),
		[DefaultValue] [nvarchar](max) NOT NULL DEFAULT (''),
		[CreatedOn] [datetime] NOT NULL,
	PRIMARY KEY CLUSTERED 
	(
		[Id] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
	) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

	CREATE NONCLUSTERED INDEX [IX_Config.DefaultLocalisationItem_Key] ON [dbo].[Config.DefaultLocalisationItem]
	(
		[Key] ASC
	)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

END
GO

