﻿IF NOT EXISTS (	SELECT	* FROM INFORMATION_SCHEMA.Columns
			WHERE	[Table_Name] = N'Data.Core.SingleSignOn'
			AND		[Column_Name] = N'ExternalAdminId'	)
BEGIN
	ALTER TABLE	[Data.Core.SingleSignOn]
	ADD			ExternalAdminId varchar(20) null
END

IF NOT EXISTS (	SELECT	* FROM INFORMATION_SCHEMA.Columns
			WHERE	[Table_Name] = N'Data.Core.SingleSignOn'
			AND		[Column_Name] = N'ExternalOfficeId'	)
BEGIN
	ALTER TABLE	[Data.Core.SingleSignOn]
	ADD			ExternalOfficeId varchar(20) null
END

IF NOT EXISTS (	SELECT	* FROM INFORMATION_SCHEMA.Columns
			WHERE	[Table_Name] = N'Data.Core.SingleSignOn'
			AND		[Column_Name] = N'ExternalPassword'	)
BEGIN
	ALTER TABLE	[Data.Core.SingleSignOn]
	ADD			ExternalPassword varchar(20) null
END

IF NOT EXISTS (	SELECT	* FROM INFORMATION_SCHEMA.Columns
			WHERE	[Table_Name] = N'Data.Core.SingleSignOn'
			AND		[Column_Name] = N'ExternalUsername'	)
BEGIN
	ALTER TABLE	[Data.Core.SingleSignOn]
	ADD			ExternalUsername varchar(20) null
END