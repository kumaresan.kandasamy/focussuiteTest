﻿IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Office' AND COLUMN_NAME = 'DefaultAdministratorId')
BEGIN
	ALTER TABLE [Data.Application.Office] ADD [DefaultAdministratorId] BIGINT NULL REFERENCES [Data.Application.Person](Id);
END