﻿SET IDENTITY_INSERT [Library.MilitaryOccupationGroupROnet] ON

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 329)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (329,84,24)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 330)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (330,30,24)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 331)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (331,219,24)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 332)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (332,29,24)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 333)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (333,77,24)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 334)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (334,64,24)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 335)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (335,72,24)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 336)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (336,101,24)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 337)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (337,25,25)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 338)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (338,23,25)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 339)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (339,718,25)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 340)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (340,22,25)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 341)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (341,69,25)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 342)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (342,13,25)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 343)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (343,253,26)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 344)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (344,496,26)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 345)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (345,544,26)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 346)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (346,501,26)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 347)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (347,527,26)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 348)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (348,541,26)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 349)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (349,81,26)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 350)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (350,500,26)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 351)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (351,13,26)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 352)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (352,45,27)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 353)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (353,257,27)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 354)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (354,44,27)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 355)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (355,243,27)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 356)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (356,46,27)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 357)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (357,176,28)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 358)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (358,489,28)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 359)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (359,280,28)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 360)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (360,179,28)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 361)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (361,172,28)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 362)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (362,178,28)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 363)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (363,168,28)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 364)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (364,41,28)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 365)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (365,156,28)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 366)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (366,113,79)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 367)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (367,63,79)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 368)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (368,61,79)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 369)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (369,2,79)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 370)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (370,3,79)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 371)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (371,117,29)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 372)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (372,79,29)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 373)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (373,113,29)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 374)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (374,112,29)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 375)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (375,148,29)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 376)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (376,78,29)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 377)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (377,140,29)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 378)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (378,141,29)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 379)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (379,80,29)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 380)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (380,288,30)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 381)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (381,314,30)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 382)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (382,279,30)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 383)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (383,285,30)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 384)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (384,284,30)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 385)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (385,25,31)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 386)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (386,707,31)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 387)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (387,156,31)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 388)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (388,243,32)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 389)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (389,12,32)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 390)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (390,586,32)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 391)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (391,247,32)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 392)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (392,418,33)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 393)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (393,414,33)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 394)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (394,59,33)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 395)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (395,408,33)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 396)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (396,411,33)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 397)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (397,399,33)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 398)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (398,407,33)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 399)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (399,115,33)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 400)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (400,60,33)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 401)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (401,231,33)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 402)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (402,406,33)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 403)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (403,401,33)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 404)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (404,30,34)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 405)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (405,28,34)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 406)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (406,63,34)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 407)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (407,61,34)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 408)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (408,29,34)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 409)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (409,57,35)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 410)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (410,632,35)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 411)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (411,20,35)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 412)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (412,597,35)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 413)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (413,12,35)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 414)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (414,75,35)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 415)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (415,35,35)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 416)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (416,562,35)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 417)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (417,586,35)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 418)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (418,176,36)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 419)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (419,489,36)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 420)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (420,21,36)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 421)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (421,63,36)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 422)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (422,20,36)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 423)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (423,280,36)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 424)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (424,35,36)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 425)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (425,176,37)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 426)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (426,489,37)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 427)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (427,61,37)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 428)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (428,6,37)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 429)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (429,280,37)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 430)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (430,41,37)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 431)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (431,49,38)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 432)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (432,44,38)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 433)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (433,47,38)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 434)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (434,274,39)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 435)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (435,39,39)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 436)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (436,38,39)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 437)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (437,265,39)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 438)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (438,176,40)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 439)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (439,489,40)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 440)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (440,63,40)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 441)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (441,632,40)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 442)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (442,20,40)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 443)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (443,280,40)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 444)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (444,75,40)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 445)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (445,41,40)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 446)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (446,18,41)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 447)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (447,110,41)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 448)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (448,97,41)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 449)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (449,96,41)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 450)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (450,104,41)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 451)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (451,510,41)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 452)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (452,499,41)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 453)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (453,17,41)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 454)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (454,16,41)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 455)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (455,107,41)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 456)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (456,103,41)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 457)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (457,64,41)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 458)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (458,72,41)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 459)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (459,101,41)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 460)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (460,509,41)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 461)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (461,99,41)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 462)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (462,503,41)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 463)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (463,502,41)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 464)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (464,98,41)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 465)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (465,377,42)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 466)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (466,378,42)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 467)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (467,421,42)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 468)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (468,42,42)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 469)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (469,21,42)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 470)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (470,3,42)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 471)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (471,497,42)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 472)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (472,44,43)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 473)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (473,243,43)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 474)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (474,40,43)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 475)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (475,247,43)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 476)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (476,59,44)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 477)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (477,1088,44)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 478)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (478,49,44)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 479)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (479,501,44)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 480)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (480,44,44)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 481)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (481,47,44)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 482)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (482,500,44)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 483)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (483,13,44)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 484)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (484,84,45)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 485)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (485,30,45)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 486)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (486,28,45)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 487)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (487,76,45)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 488)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (488,510,45)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 489)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (489,499,45)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 490)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (490,219,45)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 491)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (491,29,45)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 492)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (492,527,45)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 493)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (493,77,45)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 494)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (494,64,45)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 495)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (495,72,45)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 496)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (496,27,45)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 497)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (497,83,45)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 498)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (498,236,45)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 499)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (499,13,45)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 500)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (500,240,46)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 501)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (501,59,46)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 502)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (502,246,46)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 503)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (503,463,46)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 504)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (504,247,46)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 505)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (505,84,47)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 506)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (506,30,47)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 507)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (507,219,47)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 508)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (508,2,47)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 509)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (509,29,47)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 510)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (510,77,47)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 511)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (511,3,47)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 512)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (512,500,47)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 513)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (513,13,47)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 514)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (514,258,48)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 515)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (515,84,48)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 516)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (516,30,48)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 517)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (517,219,48)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 518)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (518,29,48)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 519)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (519,77,48)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 520)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (520,274,48)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 521)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (521,39,48)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 522)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (522,273,48)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 523)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (523,418,49)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 524)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (524,414,49)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 525)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (525,408,49)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 526)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (526,222,49)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 527)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (527,411,49)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 528)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (528,399,49)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 529)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (529,406,49)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 530)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (530,401,49)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 531)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (531,136,50)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 532)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (532,124,50)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 533)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (533,129,50)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 534)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (534,132,50)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 535)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (535,117,50)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 536)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (536,134,50)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 537)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (537,120,50)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 538)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (538,143,50)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 539)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (539,128,50)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 540)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (540,131,50)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 541)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (541,127,50)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 542)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (542,1089,50)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 543)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (543,142,50)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 544)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (544,161,50)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 545)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (545,126,50)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 546)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (546,139,50)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 547)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (547,138,50)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 548)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (548,140,50)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 549)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (549,118,50)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 550)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (550,135,50)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 551)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (551,130,50)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 552)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (552,116,50)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 553)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (553,119,50)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 554)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (554,122,50)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 555)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (555,15,50)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 556)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (556,121,50)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 557)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (557,149,51)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 558)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (558,721,51)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 559)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (559,207,51)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 560)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (560,51,51)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 561)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (561,208,51)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 562)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (562,206,51)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 563)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (563,342,51)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 564)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (564,147,51)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 565)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (565,344,51)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 566)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (566,343,51)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 567)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (567,53,51)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 568)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (568,210,51)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 569)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (569,150,51)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 570)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (570,197,51)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 571)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (571,226,51)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 572)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (572,198,51)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 573)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (573,222,52)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 574)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (574,251,52)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 575)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (575,252,52)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 576)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (576,250,52)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 577)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (577,253,53)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 578)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (578,496,53)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 579)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (579,544,53)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 580)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (580,514,53)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 581)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (581,256,53)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 582)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (582,542,53)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 583)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (583,541,53)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 584)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (584,500,53)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 585)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (585,13,53)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 586)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (586,200,80)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 587)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (587,233,80)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 588)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (588,215,80)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 589)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (589,199,80)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 590)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (590,342,80)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 591)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (591,74,80)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 592)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (592,53,80)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 593)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (593,227,80)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 594)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (594,198,80)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 595)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (595,308,54)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 596)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (596,304,54)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 597)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (597,11,54)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 598)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (598,303,54)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 599)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (599,266,54)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 600)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (600,686,55)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 601)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (601,25,55)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 602)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (602,66,55)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 603)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (603,79,55)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 604)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (604,1081,55)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 605)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (605,21,55)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 606)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (606,511,55)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 607)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (607,20,55)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 608)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (608,2,55)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 609)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (609,465,55)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 610)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (610,716,55)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 611)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (611,597,55)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 612)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (612,26,55)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 613)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (613,78,55)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 614)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (614,540,55)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 615)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (615,12,55)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 616)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (616,689,55)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 617)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (617,69,55)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 618)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (618,530,55)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 619)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (619,68,55)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 620)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (620,13,55)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 621)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (621,176,57)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 622)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (622,113,57)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 623)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (623,112,57)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 624)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (624,179,57)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 625)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (625,229,57)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 626)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (626,177,57)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 627)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (627,90,57)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 628)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (628,1088,58)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 629)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (629,45,58)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 630)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (630,333,58)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 631)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (631,332,58)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 632)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (632,44,58)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 633)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (633,243,58)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 634)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (634,46,58)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 635)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (635,152,58)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 636)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (636,25,59)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 637)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (637,1081,59)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 638)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (638,2,59)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 639)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (639,148,59)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 640)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (640,26,59)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 641)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (641,3,59)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 642)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (642,689,59)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 643)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (643,69,59)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 644)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (644,500,59)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 645)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (645,13,59)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 646)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (646,210,60)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 647)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (647,308,61)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 648)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (648,87,61)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 649)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (649,143,61)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 650)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (650,300,61)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 651)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (651,298,61)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 652)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (652,302,61)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 653)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (653,304,61)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 654)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (654,301,61)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 655)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (655,85,61)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 656)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (656,477,61)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 657)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (657,5,61)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 658)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (658,88,61)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 659)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (659,86,61)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 660)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (660,284,61)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 661)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (661,10,61)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 662)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (662,82,61)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 663)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (663,305,61)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 664)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (664,309,61)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 665)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (665,9,61)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 666)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (666,11,61)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 667)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (667,303,61)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 668)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (668,277,61)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 669)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (669,472,61)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 670)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (670,4,61)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 671)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (671,511,62)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 672)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (672,63,62)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 673)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (673,61,62)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 674)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (674,6,62)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 675)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (675,23,62)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 676)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (676,69,62)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 677)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (677,68,62)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 678)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (678,80,62)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 679)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (679,101,62)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 680)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (680,237,63)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 681)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (681,246,63)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 682)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (682,239,63)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 683)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (683,242,63)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 684)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (684,236,63)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 685)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (685,522,63)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 686)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (686,235,63)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 687)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (687,222,64)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 688)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (688,1088,64)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 689)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (689,44,64)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 690)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (690,40,64)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 691)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (691,240,65)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 692)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (692,234,65)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 693)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (693,239,65)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 694)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (694,238,65)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 695)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (695,249,65)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 696)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (696,248,65)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 697)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (697,721,66)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 698)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (698,51,66)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 699)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (699,63,66)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 700)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (700,61,66)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 701)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (701,6,66)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 702)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (702,718,66)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 703)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (703,69,66)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 704)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (704,53,66)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 705)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (705,80,66)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 706)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (706,101,66)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 707)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (707,8,67)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 708)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (708,467,67)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 709)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (709,485,67)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 710)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (710,7,67)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 711)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (711,492,67)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 712)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (712,466,67)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 713)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (713,465,67)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 714)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (714,3,67)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 715)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (715,497,67)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 716)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (716,21,68)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 717)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (717,675,68)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 718)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (718,233,68)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 719)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (719,403,68)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 720)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (720,12,68)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 721)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (721,74,68)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 722)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (722,586,68)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 723)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (723,706,68)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 724)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (724,215,69)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 725)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (725,229,69)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 726)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (726,213,69)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 727)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (727,137,69)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 728)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (728,211,69)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 729)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (729,8,71)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 730)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (730,466,71)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 731)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (731,42,71)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 732)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (732,2,71)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 733)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (733,43,71)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 734)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (734,3,71)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 735)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (735,40,71)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 736)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (736,81,71)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 737)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (737,497,71)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 738)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (738,308,72)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 739)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (739,307,72)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 740)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (740,721,72)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 741)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (741,51,72)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 742)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (742,63,72)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 743)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (743,61,72)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 744)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (744,718,72)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 745)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (745,305,72)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 746)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (746,66,73)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 747)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (747,79,73)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 748)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (748,511,73)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 749)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (749,63,73)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 750)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (750,61,73)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 751)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (751,6,73)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 752)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (752,23,73)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 753)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (753,2,73)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 754)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (754,148,73)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 755)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (755,26,73)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 756)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (756,78,73)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 757)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (757,3,73)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 758)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (758,68,73)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 759)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (759,80,73)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 760)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (760,500,73)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 761)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (761,13,73)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 762)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (762,84,74)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 763)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (763,30,74)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 764)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (764,28,74)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 765)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (765,76,74)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 766)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (766,219,74)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 767)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (767,29,74)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 768)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (768,77,74)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 769)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (769,93,74)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 770)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (770,80,74)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 771)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (771,129,75)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 772)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (772,117,75)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 773)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (773,128,75)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 774)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (774,131,75)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 775)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (775,127,75)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 776)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (776,142,75)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 777)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (777,118,75)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 778)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (778,44,76)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 779)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (779,47,76)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 780)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (780,686,77)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 781)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (781,25,77)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 782)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (782,66,77)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 783)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (783,79,77)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 784)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (784,1081,77)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 785)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (785,716,77)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 786)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (786,78,77)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 787)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (787,540,77)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 788)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (788,689,77)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 789)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (789,530,77)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 790)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (790,360,78)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 791)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (791,330,78)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 792)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (792,441,78)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroupROnet] WHERE [ID] = 793)
BEGIN
	INSERT INTO [Library.MilitaryOccupationGroupROnet](Id,RonetId,MilitaryOccupationGroupId)
	VALUES (793,32,78)
END

SET IDENTITY_INSERT [Library.MilitaryOccupationGroupROnet] OFF