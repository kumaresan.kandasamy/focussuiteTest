﻿IF NOT EXISTS
(
	SELECT 
		1
	FROM 
		sys.tables T
	INNER JOIN sys.indexes I
		ON I.object_id = T.object_id
	INNER JOIN sys.index_columns IC
		ON IC.index_id = I.index_id
		AND IC.object_id = T.object_id
	INNER JOIN sys.columns C
		ON C.column_id = IC.column_id
		AND C.object_id = T.object_id
	WHERE T.name = 'Library.MilitaryOccupationGroupROnet'
		AND C.name = 'ROnetId'
		AND key_ordinal = 1
)
BEGIN
	CREATE INDEX 
		[IX_Library.MilitaryOccupationGroupROnet_ROnetId] 
	ON 
		[Library.MilitaryOccupationGroupROnet] ([ROnetId])
END


IF NOT EXISTS
(
	SELECT 
		1
	FROM 
		sys.tables T
	INNER JOIN sys.indexes I
		ON I.object_id = T.object_id
	INNER JOIN sys.index_columns IC
		ON IC.index_id = I.index_id
		AND IC.object_id = T.object_id
	INNER JOIN sys.columns C
		ON C.column_id = IC.column_id
		AND C.object_id = T.object_id
	WHERE T.name = 'Library.MilitaryOccupationGroupROnet'
		AND C.name = 'MilitaryOccupationGroupId'
		AND key_ordinal = 1
)
BEGIN
	CREATE INDEX 
		[IX_Library.MilitaryOccupationGroupROnet_MilitaryOccupationGroupId] 
	ON 
		[Library.MilitaryOccupationGroupROnet] ([MilitaryOccupationGroupId])
END

IF NOT EXISTS
(
	SELECT 
		1
	FROM 
		sys.tables T
	INNER JOIN sys.indexes I
		ON I.object_id = T.object_id
	INNER JOIN sys.index_columns IC
		ON IC.index_id = I.index_id
		AND IC.object_id = T.object_id
	INNER JOIN sys.columns C
		ON C.column_id = IC.column_id
		AND C.object_id = T.object_id
	WHERE T.name = 'Library.MilitaryOccupationROnet'
		AND C.name = 'ROnetId'
		AND key_ordinal = 1
)
BEGIN
	CREATE INDEX 
		[IX_Library.MilitaryOccupationROnet_ROnetId] 
	ON 
		[Library.MilitaryOccupationROnet] ([ROnetId])
END

IF NOT EXISTS
(
	SELECT 
		1
	FROM 
		sys.tables T
	INNER JOIN sys.indexes I
		ON I.object_id = T.object_id
	INNER JOIN sys.index_columns IC
		ON IC.index_id = I.index_id
		AND IC.object_id = T.object_id
	INNER JOIN sys.columns C
		ON C.column_id = IC.column_id
		AND C.object_id = T.object_id
	WHERE T.name = 'Library.MilitaryOccupationROnet'
		AND C.name = 'MilitaryOccupationId'
		AND key_ordinal = 1
)
BEGIN
	CREATE INDEX 
		[IX_Library.MilitaryOccupationROnet_MilitaryOccupationId] 
	ON 
		[Library.MilitaryOccupationROnet] ([MilitaryOccupationId])
END

