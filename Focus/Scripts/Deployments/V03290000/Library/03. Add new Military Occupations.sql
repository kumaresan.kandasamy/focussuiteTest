﻿SET IDENTITY_INSERT [Library.MilitaryOccupation] ON

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupation] WHERE [Id] = 9828)
BEGIN
	INSERT INTO [Library.MilitaryOccupation](Id, Code, [Key], BranchOfServiceId, IsCommissioned, MilitaryOccupationGroupId)
	VALUES (9828, '9730', 'MilitaryOccupation.US.NAVY.9730.0.MedicalandDentalSpecialists', 1000527, 0, 13)
	
	INSERT INTO [Library.LocalisationItem]([Key],Value, LocalisationId)
	VALUES ('MilitaryOccupation.US.NAVY.9730.0.MedicalandDentalSpecialists', 'Medical and Dental Specialists', 12090)
END

SET IDENTITY_INSERT [Library.MilitaryOccupation] OFF