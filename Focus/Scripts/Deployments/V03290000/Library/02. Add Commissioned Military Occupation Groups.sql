﻿--Update the two commissioned groups already present
UPDATE [Library.MilitaryOccupationGroup] SET [IsCommissioned] = 1 WHERE Id = 18
UPDATE [Library.MilitaryOccupationGroup] SET [IsCommissioned] = 1 WHERE Id = 21

SET IDENTITY_INSERT [Library.MilitaryOccupationGroup] ON

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroup] WHERE [Id] = 24)
	BEGIN
		INSERT INTO [Library.MilitaryOccupationGroup] ([Id], [Name], [IsCommissioned])
		VALUES (24, 'ABM', 1)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroup] WHERE [Id] = 25)
	BEGIN
		INSERT INTO [Library.MilitaryOccupationGroup] ([Id], [Name], [IsCommissioned])
		VALUES (25, 'Acquisition', 1)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroup] WHERE [Id] = 26)
	BEGIN
		INSERT INTO [Library.MilitaryOccupationGroup] ([Id], [Name], [IsCommissioned])
		VALUES (26, 'Admin', 1)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroup] WHERE [Id] = 27)
	BEGIN
		INSERT INTO [Library.MilitaryOccupationGroup] ([Id], [Name], [IsCommissioned])
		VALUES (27, 'AdvNursing', 1)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroup] WHERE [Id] = 28)
	BEGIN
		INSERT INTO [Library.MilitaryOccupationGroup] ([Id], [Name], [IsCommissioned])
		VALUES (28, 'AeroEng', 1)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroup] WHERE [Id] = 29)
	BEGIN
		INSERT INTO [Library.MilitaryOccupationGroup] ([Id], [Name], [IsCommissioned])
		VALUES (29, 'Analyst', 1)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroup] WHERE [Id] = 30)
	BEGIN
		INSERT INTO [Library.MilitaryOccupationGroup] ([Id], [Name], [IsCommissioned])
		VALUES (30, 'Artist', 1)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroup] WHERE [Id] = 31)
	BEGIN
		INSERT INTO [Library.MilitaryOccupationGroup] ([Id], [Name], [IsCommissioned])
		VALUES (31, 'Astronaut', 1)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroup] WHERE [Id] = 32)
	BEGIN
		INSERT INTO [Library.MilitaryOccupationGroup] ([Id], [Name], [IsCommissioned])
		VALUES (32, 'Bioenvironmental', 1)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroup] WHERE [Id] = 33)
	BEGIN
		INSERT INTO [Library.MilitaryOccupationGroup] ([Id], [Name], [IsCommissioned])
		VALUES (33, 'CombatArms', 1)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroup] WHERE [Id] = 34)
	BEGIN
		INSERT INTO [Library.MilitaryOccupationGroup] ([Id], [Name], [IsCommissioned])
		VALUES (34, 'Commander', 1)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroup] WHERE [Id] = 35)
	BEGIN
		INSERT INTO [Library.MilitaryOccupationGroup] ([Id], [Name], [IsCommissioned])
		VALUES (35, 'Construction', 1)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroup] WHERE [Id] = 36)
	BEGIN
		INSERT INTO [Library.MilitaryOccupationGroup] ([Id], [Name], [IsCommissioned])
		VALUES (36, 'ConstructionEngineering', 1)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroup] WHERE [Id] = 37)
	BEGIN
		INSERT INTO [Library.MilitaryOccupationGroup] ([Id], [Name], [IsCommissioned])
		VALUES (37, 'DevENg', 1)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroup] WHERE [Id] = 38)
	BEGIN
		INSERT INTO [Library.MilitaryOccupationGroup] ([Id], [Name], [IsCommissioned])
		VALUES (38, 'Doctor', 1)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroup] WHERE [Id] = 39)
	BEGIN
		INSERT INTO [Library.MilitaryOccupationGroup] ([Id], [Name], [IsCommissioned])
		VALUES (39, 'EducationAdmin', 1)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroup] WHERE [Id] = 40)
	BEGIN
		INSERT INTO [Library.MilitaryOccupationGroup] ([Id], [Name], [IsCommissioned])
		VALUES (40, 'Engineers', 1)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroup] WHERE [Id] = 41)
	BEGIN
		INSERT INTO [Library.MilitaryOccupationGroup] ([Id], [Name], [IsCommissioned])
		VALUES (41, 'Finance', 1)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroup] WHERE [Id] = 42)
	BEGIN
		INSERT INTO [Library.MilitaryOccupationGroup] ([Id], [Name], [IsCommissioned])
		VALUES (42, 'FoodService', 1)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroup] WHERE [Id] = 43)
	BEGIN
		INSERT INTO [Library.MilitaryOccupationGroup] ([Id], [Name], [IsCommissioned])
		VALUES (43, 'Health', 1)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroup] WHERE [Id] = 44)
	BEGIN
		INSERT INTO [Library.MilitaryOccupationGroup] ([Id], [Name], [IsCommissioned])
		VALUES (44, 'HealthAdmin', 1)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroup] WHERE [Id] = 45)
	BEGIN
		INSERT INTO [Library.MilitaryOccupationGroup] ([Id], [Name], [IsCommissioned])
		VALUES (45, 'HR', 1)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroup] WHERE [Id] = 46)
	BEGIN
		INSERT INTO [Library.MilitaryOccupationGroup] ([Id], [Name], [IsCommissioned])
		VALUES (46, 'Human Services', 1)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroup] WHERE [Id] = 47)
	BEGIN
		INSERT INTO [Library.MilitaryOccupationGroup] ([Id], [Name], [IsCommissioned])
		VALUES (47, 'IG', 1)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroup] WHERE [Id] = 48)
	BEGIN
		INSERT INTO [Library.MilitaryOccupationGroup] ([Id], [Name], [IsCommissioned])
		VALUES (48, 'Instructors', 1)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroup] WHERE [Id] = 49)
	BEGIN
		INSERT INTO [Library.MilitaryOccupationGroup] ([Id], [Name], [IsCommissioned])
		VALUES (49, 'Intel', 1)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroup] WHERE [Id] = 50)
	BEGIN
		INSERT INTO [Library.MilitaryOccupationGroup] ([Id], [Name], [IsCommissioned])
		VALUES (50, 'IT', 1)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroup] WHERE [Id] = 51)
	BEGIN
		INSERT INTO [Library.MilitaryOccupationGroup] ([Id], [Name], [IsCommissioned])
		VALUES (51, 'Lab', 1)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroup] WHERE [Id] = 52)
	BEGIN
		INSERT INTO [Library.MilitaryOccupationGroup] ([Id], [Name], [IsCommissioned])
		VALUES (52, 'Law', 1)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroup] WHERE [Id] = 53)
	BEGIN
		INSERT INTO [Library.MilitaryOccupationGroup] ([Id], [Name], [IsCommissioned])
		VALUES (53, 'Legal Admin', 1)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroup] WHERE [Id] = 54)
	BEGIN
		INSERT INTO [Library.MilitaryOccupationGroup] ([Id], [Name], [IsCommissioned])
		VALUES (54, 'Linguist', 1)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroup] WHERE [Id] = 55)
	BEGIN
		INSERT INTO [Library.MilitaryOccupationGroup] ([Id], [Name], [IsCommissioned])
		VALUES (55, 'Logistics', 1)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroup] WHERE [Id] = 56)
	BEGIN
		INSERT INTO [Library.MilitaryOccupationGroup] ([Id], [Name], [IsCommissioned])
		VALUES (56, 'Misc', 1)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroup] WHERE [Id] = 57)
	BEGIN
		INSERT INTO [Library.MilitaryOccupationGroup] ([Id], [Name], [IsCommissioned])
		VALUES (57, 'NuclearPhys', 1)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroup] WHERE [Id] = 58)
	BEGIN
		INSERT INTO [Library.MilitaryOccupationGroup] ([Id], [Name], [IsCommissioned])
		VALUES (58, 'Nursing', 1)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroup] WHERE [Id] = 59)
	BEGIN
		INSERT INTO [Library.MilitaryOccupationGroup] ([Id], [Name], [IsCommissioned])
		VALUES (59, 'Operator', 1)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroup] WHERE [Id] = 60)
	BEGIN
		INSERT INTO [Library.MilitaryOccupationGroup] ([Id], [Name], [IsCommissioned])
		VALUES (60, 'Pharm', 1)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroup] WHERE [Id] = 61)
	BEGIN
		INSERT INTO [Library.MilitaryOccupationGroup] ([Id], [Name], [IsCommissioned])
		VALUES (61, 'PR', 1)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroup] WHERE [Id] = 62)
	BEGIN
		INSERT INTO [Library.MilitaryOccupationGroup] ([Id], [Name], [IsCommissioned])
		VALUES (62, 'Project', 1)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroup] WHERE [Id] = 63)
	BEGIN
		INSERT INTO [Library.MilitaryOccupationGroup] ([Id], [Name], [IsCommissioned])
		VALUES (63, 'Psych', 1)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroup] WHERE [Id] = 64)
	BEGIN
		INSERT INTO [Library.MilitaryOccupationGroup] ([Id], [Name], [IsCommissioned])
		VALUES (64, 'Public Health', 1)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroup] WHERE [Id] = 65)
	BEGIN
		INSERT INTO [Library.MilitaryOccupationGroup] ([Id], [Name], [IsCommissioned])
		VALUES (65, 'Religion', 1)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroup] WHERE [Id] = 66)
	BEGIN
		INSERT INTO [Library.MilitaryOccupationGroup] ([Id], [Name], [IsCommissioned])
		VALUES (66, 'Research', 1)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroup] WHERE [Id] = 67)
	BEGIN
		INSERT INTO [Library.MilitaryOccupationGroup] ([Id], [Name], [IsCommissioned])
		VALUES (67, 'Retail', 1)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroup] WHERE [Id] = 68)
	BEGIN
		INSERT INTO [Library.MilitaryOccupationGroup] ([Id], [Name], [IsCommissioned])
		VALUES (68, 'Safety', 1)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroup] WHERE [Id] = 69)
	BEGIN
		INSERT INTO [Library.MilitaryOccupationGroup] ([Id], [Name], [IsCommissioned])
		VALUES (69, 'Science', 1)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroup] WHERE [Id] = 70)
	BEGIN
		INSERT INTO [Library.MilitaryOccupationGroup] ([Id], [Name], [IsCommissioned])
		VALUES (70, 'Sciences', 1)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroup] WHERE [Id] = 71)
	BEGIN
		INSERT INTO [Library.MilitaryOccupationGroup] ([Id], [Name], [IsCommissioned])
		VALUES (71, 'Services', 1)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroup] WHERE [Id] = 72)
	BEGIN
		INSERT INTO [Library.MilitaryOccupationGroup] ([Id], [Name], [IsCommissioned])
		VALUES (72, 'SocialScience', 1)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroup] WHERE [Id] = 73)
	BEGIN
		INSERT INTO [Library.MilitaryOccupationGroup] ([Id], [Name], [IsCommissioned])
		VALUES (73, 'SpaceOps', 1)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroup] WHERE [Id] = 74)
	BEGIN
		INSERT INTO [Library.MilitaryOccupationGroup] ([Id], [Name], [IsCommissioned])
		VALUES (74, 'Staff', 1)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroup] WHERE [Id] = 75)
	BEGIN
		INSERT INTO [Library.MilitaryOccupationGroup] ([Id], [Name], [IsCommissioned])
		VALUES (75, 'Telecomm', 1)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroup] WHERE [Id] = 76)
	BEGIN
		INSERT INTO [Library.MilitaryOccupationGroup] ([Id], [Name], [IsCommissioned])
		VALUES (76, 'Therapy', 1)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroup] WHERE [Id] = 77)
	BEGIN
		INSERT INTO [Library.MilitaryOccupationGroup] ([Id], [Name], [IsCommissioned])
		VALUES (77, 'TransportMgmt', 1)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroup] WHERE [Id] = 78)
	BEGIN
		INSERT INTO [Library.MilitaryOccupationGroup] ([Id], [Name], [IsCommissioned])
		VALUES (78, 'Vets', 1)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroup] WHERE [Id] = 79)
	BEGIN
		INSERT INTO [Library.MilitaryOccupationGroup] ([Id], [Name], [IsCommissioned])
		VALUES (79, 'AirWarfare', 1)
END

IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupationGroup] WHERE [Id] = 80)
	BEGIN
		INSERT INTO [Library.MilitaryOccupationGroup] ([Id], [Name], [IsCommissioned])
		VALUES (80, 'LifeSci', 1)
END

SET IDENTITY_INSERT [Library.MilitaryOccupationGroup] OFF