﻿--Script 3 did not seem to add the localisation item so doing it again here


IF NOT EXISTS(SELECT 1 FROM [Library.LocalisationItem] WHERE [Key] = 'MilitaryOccupation.US.NAVY.9730.0.MedicalandDentalSpecialists')
BEGIN
	INSERT INTO [Library.LocalisationItem]([Key],Value, LocalisationId)
	VALUES ('MilitaryOccupation.US.NAVY.9730.0.MedicalandDentalSpecialists', 'Medical and Dental Specialists', 12090)
END