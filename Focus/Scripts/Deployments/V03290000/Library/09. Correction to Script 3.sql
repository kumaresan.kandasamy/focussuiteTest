﻿
IF NOT EXISTS(SELECT 1 FROM [Library.MilitaryOccupation] WHERE [Id] = 9828)
BEGIN
	SET IDENTITY_INSERT [Library.MilitaryOccupation] ON

	INSERT INTO [Library.MilitaryOccupation](Id, Code, [Key], BranchOfServiceId, IsCommissioned, MilitaryOccupationGroupId)
	VALUES (9828, '9730', 'MilitaryOccupation.US.NAVY.9730.0.MedicalandDentalSpecialists', 1000527, 0, 13)

	SET IDENTITY_INSERT [Library.MilitaryOccupation] OFF
END


