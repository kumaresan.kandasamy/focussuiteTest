-- Change AppFabric configuration item to the new style using the old ones, then delete the old ones

DECLARE @AppFabricServer NVARCHAR(100)
DECLARE @AppFabricPort NVARCHAR(100)

if (exists (select 1 from [Config.ConfigurationItem] where [key] = 'AppFabricServer')) and (exists (select 1 from [Config.ConfigurationItem] where [key] = 'AppFabricPort'))
BEGIN
	select @AppFabricServer = [Value] from [Config.ConfigurationItem] where [key] = 'AppFabricServer'
	select @AppFabricPort = [Value] from [Config.ConfigurationItem] where [key] = 'AppFabricPort'
	INSERT INTO [Config.ConfigurationItem] ([Key], [Value], [InternalOnly]) VALUES  ('AppFabricLeadHosts','{"Item1":"' + @AppFabricServer + '","Item2":' + @AppFabricPort + '}',1)
END

DELETE FROM [Config.ConfigurationItem] WHERE [Key] IN ('AppFabricServer', 'AppFabricPort')