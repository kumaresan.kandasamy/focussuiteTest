﻿UPDATE [dbo].[Config.EmailTemplate]
SET [Body] = 'We have reviewed your request to register with #FOCUSTALENT# but must place your request on temporary hold for the following reasons:'+CHAR(13)+CHAR(10)+CHAR(13)+CHAR(10)+'#HOLDREASONS#'+CHAR(13)+CHAR(10)+CHAR(13)+CHAR(10)+'If you have any questions, please feel free to contact me at #SENDERPHONENUMBER# or #SENDEREMAILADDRESS#.'+CHAR(13)+CHAR(10)+''+CHAR(13)+CHAR(10)+'Regards,  #SENDERNAME#'
WHERE [EmailTemplateType] = 19;
