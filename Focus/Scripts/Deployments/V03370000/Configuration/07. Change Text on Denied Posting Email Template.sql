﻿
UPDATE [dbo].[Config.EmailTemplate]
SET Body = 'The #JOBTITLE# posting you recently created has been denied due to the following reasons:    #FREETEXT#    

Your job posting is in the #JOBSTATUS# Postings tab on your home page.

If you would like to repost this job, please click the Duplicate Icon and make the appropriate changes to address the issues with your previous posting.    

If you have any questions, please feel free to contact me at #SENDERPHONENUMBER# or #SENDEREMAILADDRESS#.   

 Regards,  #SENDERNAME#'
 WHERE EmailTemplateType = 7