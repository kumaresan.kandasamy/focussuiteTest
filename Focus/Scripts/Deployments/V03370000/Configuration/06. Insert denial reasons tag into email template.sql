﻿UPDATE [dbo].[Config.EmailTemplate]
SET [Body] = 'We have reviewed your request for a referral for the #JOBTITLE# position at #EMPLOYERNAME#. Unfortunately we cannot approve your request for the following reasons:'+CHAR(13)+CHAR(10)+CHAR(13)+CHAR(10)+'#DENIALREASONS#'+CHAR(13)+CHAR(10)+CHAR(13)+CHAR(10)+'For more information, you may log in to Focus/Career to view feedback on your request, or contact me at #SENDERPHONENUMBER# or #SENDEREMAILADDRESS#.'+CHAR(13)+CHAR(10)+CHAR(13)+CHAR(10)+'Regards,'+CHAR(13)+CHAR(10)+'#SENDERNAME#'
WHERE [EmailTemplateType] = 5;
