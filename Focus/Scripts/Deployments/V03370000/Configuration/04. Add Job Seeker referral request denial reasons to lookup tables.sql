﻿DECLARE @LookupType nvarchar(200) = 'JobSeekerDenialReasons';
DECLARE @CodeGroupId bigint;
DECLARE @LocalisationId bigint;


DECLARE @Lookups TABLE
(
	[Key] nvarchar(200),
	DisplayOrder int,
	Value nvarchar(255),
	ExternalId nvarchar(10)
);


INSERT INTO @Lookups ([Key], DisplayOrder, Value, ExternalId)
VALUES 
(@LookupType + '.DidNotMeetEducationRequirements', 1, 'Did not meet minimum education requirements', '1'),
(@LookupType + '.DidNotMeetExperienceRequirements', 2, 'Did not meet minimum work experience requirements', '2'),
(@LookupType + '.DidNotMeetWorkOrEducationRequirements', 3, 'Did not meet minimum requirements for combination of work and education', '3'),
(@LookupType + '.DidNotMeetLicensingRequirements', 4, 'Did not meet licensing/certification requirements', '4'),
(@LookupType + '.DidNotMeetAgeRequirements', 5, 'Did not meet minimum age requirements', '5'),
(@LookupType + '.AlienRegistrationInfoNeedsUpdating', 6, 'Alien registration information needs to be updated', '6'),
(@LookupType + '.DidNotMeetSkillRequirements', 7, 'Did not possess specific work skills required', '7'),
(@LookupType + '.Other', 8, 'Other', '8');


IF NOT EXISTS (SELECT 1 FROM [Config.CodeGroup] WHERE [Key] = @LookupType)
BEGIN
	INSERT INTO [Config.CodeGroup] ([Key])
	VALUES (@LookupType);
END


INSERT INTO [Config.CodeItem] 
(
	[Key], 
	IsSystem,
	ExternalId
)
SELECT	S.[Key],
				0,
				S.ExternalId
FROM		@Lookups S
WHERE		NOT EXISTS (SELECT * FROM [Config.CodeItem] CI WHERE CI.[Key] = S.[Key]);


UPDATE			CodeItem
SET					ExternalId = LookupItems.ExternalId
FROM				@Lookups LookupItems
INNER JOIN	[Config.CodeItem] CodeItem ON CodeItem.[Key] = LookupItems.[Key]
WHERE				ISNULL(CodeItem.ExternalId, '') <> LookupItems.ExternalId;


SELECT	@CodeGroupId = Id
FROM		[Config.CodeGroup]
WHERE		[Key] = @LookupType;


INSERT INTO [Config.CodeGroupItem]
(
	DisplayOrder,
	CodeGroupId,
	CodeItemId
)
SELECT			LookupItems.DisplayOrder,
						@CodeGroupId,
						CodeItem.Id
FROM				@Lookups LookupItems
INNER JOIN	[Config.CodeItem] CodeItem ON CodeItem.[Key] = LookupItems.[Key]
WHERE				NOT EXISTS
						(
							SELECT	*
							FROM		[Config.CodeGroupItem] CodeGroupItem
							WHERE		CodeGroupItem.CodeGroupId = @CodeGroupId
							AND			CodeGroupItem.CodeItemId = CodeItem.Id
						);

	
UPDATE			CodeGroupItem
SET					DisplayOrder = LookupItems.DisplayOrder
FROM				@Lookups LookupItems
INNER JOIN	[Config.CodeItem] CodeItem ON CodeItem.[Key] = LookupItems.[Key]
INNER JOIN	[Config.CodeGroupItem] CodeGroupItem
						ON CodeGroupItem.CodeGroupId = @CodeGroupId
						AND CodeGroupItem.CodeItemId = CodeItem.Id
WHERE				CodeGroupItem.DisplayOrder <> LookupItems.DisplayOrder;


SELECT	@LocalisationId = Localisation.Id
FROM		[Config.Localisation] Localisation
WHERE		Localisation.Culture = '**-**';


INSERT INTO [Config.LocalisationItem]
(
	[Key],
	Value,
	LocalisationId
)
SELECT	LookupItems.[Key],
				LookupItems.Value,
				@LocalisationId
FROM		@Lookups LookupItems
WHERE		NOT EXISTS
				(
					SELECT	*
					FROM		[Config.LocalisationItem] LocalisationItem
					WHERE		LocalisationItem.[Key] = LookupItems.[Key]
				);

	
UPDATE			LocalisationItem
SET					Value = LookupItems.Value
FROM				@Lookups LookupItems
INNER JOIN	[Config.LocalisationItem] LocalisationItem ON LocalisationItem.[Key] = LookupItems.[Key]
WHERE				LocalisationItem.Value <> LookupItems.Value;
