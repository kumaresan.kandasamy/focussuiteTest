﻿IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.staffMember' AND COLUMN_NAME = 'LocalVeteranEmploymentRepresentative')
BEGIN
  ALTER TABLE 
    [Report.staffMember] 
  ADD [LocalVeteranEmploymentRepresentative] BIT NULL;
END

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.staffMember' AND COLUMN_NAME = 'DisabledVeteransOutreachProgramSpecialist')
BEGIN
  ALTER TABLE 
    [Report.staffMember] 
  ADD [DisabledVeteransOutreachProgramSpecialist] BIT NULL;
END