IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.User' AND COLUMN_NAME = 'SecurityQuestion2')
BEGIN
	ALTER TABLE [dbo].[Data.Application.User]
	ADD [SecurityQuestion2] [nvarchar](100) NULL
END

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.User' AND COLUMN_NAME = 'SecurityAnswer2Hash')
BEGIN
	ALTER TABLE [dbo].[Data.Application.User]
	ADD [SecurityAnswer2Hash] [nvarchar](86) NULL
END

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.User' AND COLUMN_NAME = 'SecurityAnswer2Encrypted')
BEGIN
	ALTER TABLE [dbo].[Data.Application.User]
	ADD [SecurityAnswer2Encrypted] [nvarchar](250) NULL
END

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.User' AND COLUMN_NAME = 'SecurityQuestion3')
BEGIN
	ALTER TABLE [dbo].[Data.Application.User]
	ADD [SecurityQuestion3] [nvarchar](100) NULL
END

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.User' AND COLUMN_NAME = 'SecurityAnswer3Hash')
BEGIN
	ALTER TABLE [dbo].[Data.Application.User]
	ADD [SecurityAnswer3Hash] [nvarchar](86) NULL
END

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.User' AND COLUMN_NAME = 'SecurityAnswer3Encrypted')
BEGIN
	ALTER TABLE [dbo].[Data.Application.User]
	ADD [SecurityAnswer3Encrypted] [nvarchar](250) NULL
END