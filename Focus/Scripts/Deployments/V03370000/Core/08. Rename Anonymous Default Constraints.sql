﻿DECLARE @ConstraintName varchar(200);
DECLARE @NewConstraintName varchar(200);
DECLARE @SQL varchar(2000);
DECLARE @DefaultConstraints TABLE (ConstraintName varchar(200), NewConstraintName varchar(200));

INSERT INTO @DefaultConstraints
(ConstraintName, NewConstraintName)
SELECT dc.name AS ConstraintName, 'DF_' + t.name + '_' + c.name AS NewConstraintName
FROM sys.default_constraints dc
INNER JOIN sys.tables t ON dc.parent_object_id = t.object_id
INNER JOIN sys.columns c ON t.object_id = c.object_id AND dc.parent_column_id = c.column_id

WHILE EXISTS (SELECT * FROM @DefaultConstraints)
BEGIN
	SELECT TOP 1 @ConstraintName = ConstraintName, @NewConstraintName = NewConstraintName
	FROM @DefaultConstraints;

	IF @ConstraintName <> @NewConstraintName
	BEGIN
		SELECT @SQL = 'exec sp_rename @objname = ''dbo.'+Quotename(@ConstraintName)+''', @newname = '''+@NewConstraintName + ''', @objtype = ''OBJECT''';
		PRINT @SQL;
		EXEC (@SQL);
	END

	DELETE FROM @DefaultConstraints
	WHERE @ConstraintName = ConstraintName;
END
