DECLARE @NextID INT
DECLARE @ViewRoleId BIGINT

DECLARE @Roles TABLE
(
	RoleKey NVARCHAR(255),
	RoleDesc NVARCHAR(255)
)
INSERT INTO @Roles (RoleKey, RoleDesc)
VALUES 
	('AssistStaffDetails', 'Assist Staff Details'),
	('AssistUpdatePermissions', 'Assist Update Permissions')

SELECT @NextID = NextId FROM [dbo].[KeyTable]

INSERT INTO [dbo].[Data.Application.Role] (Id, [Key], Value)
SELECT 
	@NextID + (ROW_NUMBER() OVER (ORDER BY TR.RoleKey ASC)) - 1, 
	RoleKey, 
	RoleDesc
FROM
	@Roles TR
WHERE
	NOT EXISTS
	(
		SELECT 
			1 
		FROM 
			[Data.Application.Role] R
		WHERE
			R.[Key] = TR.RoleKey
	)
	
UPDATE [KeyTable] SET NextId = NextId + @@ROWCOUNT
SELECT @NextID = NextId FROM [dbo].[KeyTable]

-- AccountAdmin users, or those with View/Edit, get the new permissions
INSERT INTO [Data.Application.UserRole]
(
	Id,
	UserId,
	RoleId
)
SELECT
	@NextID + (ROW_NUMBER() OVER (ORDER BY U.Id ASC, R.Id ASC)) - 1, 
	U.Id,
	R.Id
FROM
	[Data.Application.User] U
CROSS JOIN @Roles TR
INNER JOIN [Data.Application.Role] R
	ON R.[Key] = TR.RoleKey
WHERE
	U.UserType = 1
	AND NOT EXISTS
	(
		SELECT
			1
		FROM
			[Data.Application.UserRole] UR2
		INNER JOIN [Data.Application.Role] R2
			ON R2.Id = UR2.RoleId
		WHERE
			UR2.UserId = U.Id
			AND R2.[Key] = TR.RoleKey
	)
	AND EXISTS
	(
		SELECT
			1
		FROM
			[Data.Application.UserRole] UR3
		INNER JOIN [Data.Application.Role] R3
			ON R3.Id = UR3.RoleId
		WHERE
			UR3.UserId = U.Id
			AND R3.[Key] IN ('AssistAccountAdministrator', 'AssistStaffViewEdit')
	)
	
UPDATE [KeyTable] SET NextId = NextId + @@ROWCOUNT
SELECT @NextID = NextId FROM [dbo].[KeyTable]

-- Staff with just View/Edit need to be assigned just "View" if they don't have it already
SELECT @ViewRoleId = Id FROM [Data.Application.Role] WHERE [Key] = 'AssistStaffView'

INSERT INTO [Data.Application.UserRole]
(
	Id,
	UserId,
	RoleId
)
SELECT
	@NextID + (ROW_NUMBER() OVER (ORDER BY U.Id ASC, R.Id ASC)) - 1, 
	U.Id,
	@ViewRoleId
FROM
	[Data.Application.User] U
INNER JOIN [Data.Application.UserRole] UR
	ON UR.UserId = U.Id
INNER JOIN [Data.Application.Role] R
	ON R.Id = UR.RoleId
WHERE
	U.UserType = 1
	AND R.[Key] = 'AssistStaffViewEdit'
	AND NOT EXISTS
	(
		SELECT
			1
		FROM
			[Data.Application.UserRole] UR2
		WHERE
			UR2.UserId = U.Id
			AND UR2.RoleId = @ViewRoleId
	)

UPDATE [KeyTable] SET NextId = NextId + @@ROWCOUNT
