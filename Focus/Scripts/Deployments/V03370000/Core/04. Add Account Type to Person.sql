﻿  IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Person' AND COLUMN_NAME = 'AccountType')
BEGIN
  ALTER TABLE 
    [Data.application.Person] 
  ADD 
    AccountType INT NULL 
END
GO