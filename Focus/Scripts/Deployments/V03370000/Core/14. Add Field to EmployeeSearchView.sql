﻿
IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Data.Application.EmployeeSearchView]'))
DROP VIEW [dbo].[Data.Application.EmployeeSearchView]
GO


CREATE VIEW [dbo].[Data.Application.EmployeeSearchView] AS
SELECT     
	EmployeeBusinessUnit.Id AS Id,     
	Employee.Id AS EmployeeId,    
	Person.FirstName,     
	Person.LastName,    
	Person.EmailAddress,    
	PhoneNumber.Number AS PhoneNumber, 
	PhoneNumber.Extension AS Extension,   
	Employer.Id AS EmployerId,     
	Employer.Name AS EmployerName,     
	BusinessUnit.IndustrialClassification,     
	Employer.FederalEmployerIdentificationNumber,     
	NULL AS WorkOpportunitiesTaxCreditHires,     
	Person.JobTitle,     
	NULL AS PostingFlags,     
	NULL AS ScreeningPreferences,    
	Employer.CreatedOn AS EmployerCreatedOn,    
	[User].[Enabled] AS AccountEnabled,    
	Employer.ApprovalStatus AS EmployerApprovalStatus,    
	Employee.ApprovalStatus AS EmployeeApprovalStatus,  
	BusinessUnitAddress.TownCity,  
	BusinessUnitAddress.StateId,
	'' AS [State],
	BusinessUnit.Name AS BusinessUnitName,
	BusinessUnit.Id AS BusinessUnitId,
	[User].LoggedInOn AS LastLogin,
	EmployeeBusinessUnit.Id AS EmployeeBusinessUnitId,
	[User].Blocked AS AccountBlocked,
	BusinessUnit.IsPreferred AS EmployerIsPreferred,
	BusinessUnit.IsPrimary As BusinessUnitIsPrimary,
	CASE WHEN Person.Unsubscribed IS NULL THEN CAST(0 AS BIT) ELSE Person.Unsubscribed END AS Unsubscribed,
	Employee.ApprovalStatusReason AS EmployeeApprovalStatusReason,
	BusinessUnit.ApprovalStatus AS BusinessUnitApprovalStatus,
	BusinessUnit.ApprovalStatusReason AS BusinessUnitApprovalStatusReason,
	Person.SuffixId as SuffixId,
	[User].BlockedReason AS AccountBlockedReason
FROM      
	dbo.[Data.Application.Person] AS Person WITH (NOLOCK)    
	INNER JOIN dbo.[Data.Application.Employee] AS Employee WITH (NOLOCK) ON Person.Id = Employee.PersonId    
	INNER JOIN dbo.[Data.Application.Employer] AS Employer WITH (NOLOCK) ON Employee.EmployerId = Employer.Id  
	LEFT OUTER JOIN dbo.[Data.Application.PhoneNumber] AS PhoneNumber WITH (NOLOCK) ON Person.Id = PhoneNumber.PersonId AND PhoneNumber.IsPrimary = 1    
	INNER JOIN dbo.[Data.Application.User] AS [User] WITH (NOLOCK) ON Person.Id = [User].PersonId     
	INNER JOIN dbo.[Data.Application.EmployeeBusinessUnit] AS EmployeeBusinessUnit WITH (NOLOCK) ON Employee.Id = EmployeeBusinessUnit.EmployeeId
	INNER JOIN dbo.[Data.Application.BusinessUnit] AS BusinessUnit WITH (NOLOCK) ON EmployeeBusinessUnit.BusinessUnitId = BusinessUnit.Id
	INNER JOIN dbo.[Data.Application.BusinessUnitAddress] AS BusinessUnitAddress WITH (NOLOCK) ON BusinessUnit.Id = BusinessUnitAddress.BusinessUnitId










GO


