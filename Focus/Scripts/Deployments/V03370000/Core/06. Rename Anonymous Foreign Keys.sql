﻿DECLARE @ConstraintName varchar(200);
DECLARE @NewConstraintName varchar(200);
DECLARE @TableName varchar(200);
DECLARE @SQL varchar(2000);
DECLARE @ForeignKeys TABLE (ConstraintName varchar(200), NewConstraintName varchar(200), TableName varchar(200));

INSERT INTO @ForeignKeys
(ConstraintName, NewConstraintName, TableName)
SELECT fk.name AS ConstraintName, 'FK_' + t.name + '_' + c.name AS NewConstraintName, t.name AS TableName
FROM sys.foreign_keys fk
INNER JOIN sys.tables t ON fk.parent_object_id = t.object_id
INNER JOIN sys.foreign_key_columns fkc ON fkc.constraint_object_id = fk.object_id
INNER JOIN sys.columns c ON fkc.parent_object_id = c.object_id AND fkc.parent_column_id = c.column_id

WHILE EXISTS (SELECT * FROM @ForeignKeys)
BEGIN
	SELECT TOP 1 @ConstraintName = ConstraintName, @NewConstraintName = NewConstraintName, @TableName = TableName
	FROM @ForeignKeys;
	
	IF @ConstraintName <> @NewConstraintName
	BEGIN
		IF EXISTS (SELECT * FROM sys.foreign_keys WHERE name = @NewConstraintName)
		BEGIN
			-- This will delete a duplicate foreign key
			SELECT @SQL = 'ALTER TABLE dbo.' + Quotename(@TableName) + ' DROP CONSTRAINT '+Quotename(@ConstraintName)+';'
			PRINT @SQL;
			EXEC (@SQL);
		END
		ELSE
		BEGIN
			SELECT @SQL = 'exec sp_rename @objname = ''dbo.'+Quotename(@ConstraintName)+''', @newname = '''+@NewConstraintName + ''', @objtype = ''OBJECT''';
			PRINT @SQL;
			EXEC (@SQL);
		END
	END

	DELETE FROM @ForeignKeys
	WHERE @ConstraintName = ConstraintName;
END
