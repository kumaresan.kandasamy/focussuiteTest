﻿DECLARE	@TableId int,
		@IndexName varchar(200),
		@IndexId int,
		@IsUnique bit,
		@TableName varchar(200),
		@NewIndexName varchar(200),
		@IndexColId int,
		@ColumnId int;
DECLARE @SQL varchar(2000);
DECLARE @Indexes TABLE
	(
		TableId int,
		IndexName varchar(200),
		IndexId int,
		IsUnique bit,
		TableName varchar(200),
		NewIndexName varchar(200)
	);

INSERT INTO @Indexes
(TableId, IndexName, IndexId, IsUnique, TableName, NewIndexName)
SELECT		i.object_id AS TableId,
			i.name AS IndexName,
			i.index_id AS IndexId,
			i.is_unique AS IsUnique,
			t.name AS TableName,
			NULL AS NewIndexName
FROM		sys.indexes i
INNER JOIN	sys.tables t ON i.object_id = t.object_id
WHERE		is_primary_key = 0
AND			index_id > 0
ORDER BY	i.object_id, i.index_id

WHILE EXISTS (SELECT * FROM @Indexes)
BEGIN
	SELECT	TOP 1 @TableId = TableId,
			@IndexName = IndexName,
			@IndexId = IndexId,
			@IsUnique = IsUnique,
			@TableName = TableName
	FROM	@Indexes
	
	IF @IsUnique = 1
		SELECT @NewIndexName = 'UQ_' + @TableName;
	ELSE
		SELECT @NewIndexName = 'IX_' + @TableName;
	
	SELECT @IndexColId = 1;
	
	WHILE EXISTS (SELECT * FROM sys.index_columns WHERE object_id = @TableId AND index_id = @IndexId AND index_column_id = @IndexColId)
	BEGIN
		SELECT	@ColumnId = column_id
		FROM	sys.index_columns
		WHERE	object_id = @TableId
		AND		index_id = @IndexId
		AND		index_column_id = @IndexColId;
		
		SELECT @NewIndexName = @NewIndexName + '_' + c.name
		FROM sys.columns c
		WHERE c.column_id = @ColumnId
		AND	c.object_id = @TableId;
		
		SELECT @IndexColId = @IndexColId + 1;
	END
	
	IF @IndexName <> @NewIndexName
	BEGIN
		SELECT @SQL = 'exec sp_rename @objname = '''+QUOTENAME(@TableName)+'.'+Quotename(@IndexName)+''', @newname = '''+@NewIndexName + ''', @objtype = ''INDEX''';
		PRINT @SQL;
		EXEC (@SQL);
	END

	DELETE FROM @Indexes
	WHERE	TableId = @TableId
	AND		IndexId = @IndexId
END

