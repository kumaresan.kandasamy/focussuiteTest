﻿IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.FailedIntegrationMessage' AND COLUMN_NAME = 'IntegrationPoint')
BEGIN
	ALTER TABLE
		[Data.Application.FailedIntegrationMessage]
	ADD
		IntegrationPoint INT NULL
END
GO
