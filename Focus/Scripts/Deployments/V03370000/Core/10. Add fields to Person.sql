﻿IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Person' AND COLUMN_NAME = 'LocalVeteranEmploymentRepresentative')
BEGIN
  ALTER TABLE 
    [Data.application.person] 
  ADD [LocalVeteranEmploymentRepresentative] BIT NULL;
END

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Person' AND COLUMN_NAME = 'DisabledVeteransOutreachProgramSpecialist')
BEGIN
  ALTER TABLE 
    [Data.application.person] 
  ADD [DisabledVeteransOutreachProgramSpecialist] BIT NULL;
END