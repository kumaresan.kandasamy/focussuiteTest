-- YELLOW Word Censorship Rules SQL

DECLARE @Type INT
DECLARE @Level INT

Set @Type = 1
Set @Level = 2

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'accent')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'accent'
Print 'Deleted ''accent'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'aboriginal*')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'aboriginal*','')
Print 'Added ''aboriginal*'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'african*')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'african*','')
Print 'Added ''african*'''
END

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'african-american, african art, african artist, african awareness, african background, african bookstore, african broadcasting, african business, african businesses, african celebration, african cemetary, african center, african community, african cooking, african cuisine, african cultural, african culture, african custom, african discipline, african exercise, african expression, african food, african geography, african heritage, african history, african holiday, african identity, african market, african media, african music, african musician, african nation, african national, african neighborhood, african network, african newspaper, african origin, african outreach, african-owned, african politics, african population, african radio, african restaurant, african senior, african support, african symbol, african technologies, african technology, african TV, african television, african tradition, african youth' where [Phrase] = 'african*'
Print 'Added White List Items for ''african*'''
ENd

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'ADD')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'ADD'
Print 'Deleted ''ADD'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'AADD')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'AADD'
Print 'Deleted ''AADD'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'adult')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'adult'
Print 'Deleted ''adult'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'adult movie')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'adult movie','')
Print 'Added ''adult movie'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'adult programming')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'adult programming','')
Print 'Added ''adult programming'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'adult television')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'adult television','')
Print 'Added ''adult television'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'age*')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'age*','')
Print 'Added ''age*'''
END

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'agee, ageless, agelessness, agelessly, agenes, agenesias, agenize, agenized, agenizing, agenizes, agent, agents, agenting, agentings, agented, agential, agentive, agentives, agentival, agentry, agentries, ageratum, ageratums, agency, agencies, agenda, agendas, agendaless, agelong, agemate, agemates, agendum, agendums, ageagenes, dark age, dark ages, new age, new ages' where [Phrase] = 'age*'
Print 'Added White List Items for ''age*'''
END

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'aids assistance, aids coordination, aids coordinator, aids counseling, aids counselor, aids help, aids intervention, aids preventive, aids prevention, aids outreach, aids project, aids program, aids support' where [Phrase] = 'aids'
Print 'Added White List Items for ''aids'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'all others may apply')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'all others may apply','')
Print 'Added ''all others may apply'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti immigra*')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'anti immigra*','')
Print 'Added ''anti immigra*'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti-immigra*')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'anti-immigra*','')
Print 'Added ''anti-immigra*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'asian')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'asian*' WHERE [Phrase] = 'asian'
END
Print 'Updated ''asian'' to ''asian*'''

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'asian-american, asian art, asian artist, asian awareness, asian background, asian bookstore, asian broadcasting, asian business, asian businesses, asian celebration, asian cemetary, asian center, asian community, asian cooking, asian cuisine, asian cultural, asian culture, asian custom, asian discipline, asian exercise, asian expression, asian food, asian geography, asian heritage, asian history, asian holiday, asian identity, asian market, asian media, asian music, asian musician, asian nation, asian national, asian neighborhood, asian network, asian newspaper, asian origin, asian outreach, asian-owned, asian politics, asian population, asian radio, asian restaurant, asian senior, asian support, asian symbol, asian technologies, asian technology, asian TV, asian television, asian tradition, asian youth' where [Phrase] = 'asian*'
Print 'Added White List Items for ''asian*'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'attractive')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'attractive','')
Print 'Added ''attractive'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'attention deficit disorder*')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'attention deficit disorder*'
Print 'Deleted ''attention deficit disorder*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'augment')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'augment'
Print 'Deleted ''augment'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'background')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'background'
Print 'Deleted ''background'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'bad c3it*')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'bad c3it*'
Print 'Deleted ''bad c3it*'''
END

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'accounted, accounting' where [Phrase] = 'bank account*'
Print 'Added White List Items for ''bank account*'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'beautiful*')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'beautiful*','')
Print 'Added ''beautiful*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'billionaire')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'billionaire*' WHERE [Phrase] = 'billionaire'
END
Print 'Updated ''billionaire'' to ''billionaire*'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'biracial')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'biracial*' WHERE [Phrase] = 'biracial'
END
Print 'Updated ''biracial'' to ''biracial*'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'bisexual')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'bisexual*' WHERE [Phrase] = 'bisexual'
END
Print 'Updated ''bisexual'' to ''bisexual*'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'bi-sexual')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'bi-sexual*' WHERE [Phrase] = 'bi-sexual'
END
Print 'Updated ''bi-sexual'' to ''bi-sexual*'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'black')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'black*' WHERE [Phrase] = 'black'
END
Print 'Updated ''black'' to ''black*'''

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'black angus, black-american, black-and-blue, black-and-tan, black art, black artist, black awareness, black background, black barn, blackbelt, black belt, black-belt, blackboard, black board, black-board, blackberry, blackberries, black bookstore, blackbox, black box, black-box, black broadcasting, black business, black businesses, black celebration, black cemetery, black center, black community, black crappie,  black cooking, black crude, black cuisine, black cultural, black culture, black custom, blackdamp, black death, black diamond, black discipline, blackened, black exercise, black eye, blackeye, black-eyed, black expression, black fin, blackfin, black fish, blackfish, black food, black forest, black friar, black frost, black grouse, black haw, blackhead, blackheart, black heritage, black hills, black history, black hole, black holiday, black identity, blacking, blackjack, black jack, black knot, black lead, blackleg, black letter, black light, black lung, black measles, black media, black medic, black mountains, blackmun, black music, black musician, black neighborhood, black network, black newspaper, black nightshade, black oak, black origin, black oil, black out, black outreach, black-owned, black pepper, black politics, blackpoll, blackpool, black population, black practice, black prince, black radio, black raspberry, black rod, blackrot, blackrust, black sea, black senior, blacksmith, blacksmiths, black snake, black spruce, blackstone, black strap, black-strap,  black support, black symbol, blacktail, blacktails, blacktailed, back tea, blackthorn, black tie, blacktop, blacktops, blacktopped, blacktopper, blacktoppers, blacktopping, black TV, black television, black tradition, black walnut, blackwater, blackwell, black widow, black widows, black youth' where [Phrase] = 'black*'
Print 'Added White List Items for ''black*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'black jack')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'black jack'
Print 'Deleted ''black jack'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'blonde*')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'blonde*','')
Print 'Added ''blonde*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'boy')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'boy*' WHERE [Phrase] = 'boy'
END
Print 'Updated ''boy'' to ''boy*'''

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'boyle, boysenberry, boysenberries, boys rock, boy''s casual, boy''s class, boy''s classroom, boy''s clothing, boy''s department, boy''s sports, boy''s sportsgear, boy''s sportswear, boy''s wear,  boys'' casual, boys'' class, boys'' classroom, boys'' clothing, boys'' department, boys'' sports, boys'' sportsgear, boys'' sportswear, boys'' wear, Boy''s Clubs of America, Boy Scouts of America' where [Phrase] = 'boy*'
Print 'Added White List Items for ''boy*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'boy friend')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'boy friend'
Print 'Deleted ''boy friend'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'brave')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'brave*' WHERE [Phrase] = 'brave'
END
Print 'Updated ''brave'' to ''brave*'''

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'braved, bravely, braveness, bravery, brave american, brave americans, brave soldier, brave soldiers, brave veteran, brave veterans' where [Phrase] = 'brave*'
Print 'Added White List Items for ''brave*'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'breast*')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'breast*','')
Print 'Added ''breast*'''
END

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'breastfeeding, breast feeding, breast-feeding, breastfed, breast fed, breast-fed, breast exam, breast exams, breast examination, breast examinations, doublebreasted, double breasted, double-breasted' where [Phrase] = 'breast*'
Print 'Added White List Items for ''breast*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'breast augmentation')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'breast augmentation'
Print 'Deleted ''breast augmentation'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'breast enhancement')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'breast enhancement'
Print 'Deleted ''breast enhancement'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'breast enlargement')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'breast enlargement'
Print 'Deleted ''breast enlargement'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'brunette*')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'brunette*','')
Print 'Added ''brunette*'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'buddhis*')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'buddhis*','')
Print 'Added ''buddhis*'''
END

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'buddhism, buddhist-american, buddhist art, buddhist artist, buddhist awareness, buddhist background, buddhist belief, buddhist believer, buddhist bookstore, buddhist business, buddhist businesses, buddhist celebration, buddhist center, buddhist chanting, buddhist community, buddhist cooking, buddhist cuisine, buddhist cultural, buddhist culture,buddhist dalia,  buddhist deities,  buddhist deity, buddhist discipline, buddhist exercise, buddhist expression, buddhist faith, buddhist follower, buddhist food, buddhist holiday, buddhist identity, buddhist market, buddhist media, buddhist monk, buddhist music, buddhist magician, buddhist nation, buddhist national, buddhist neighborhood, buddhist network, buddhist newspaper, buddhist origin, buddhist outreach, buddhist-owned, buddhist politics, buddhist population, buddhist practice, buddhist radio, buddhist religion, buddhist sangha, buddhist sect, buddhist senior, buddhist support, buddhist symbol, buddhist TV, buddhist television, buddhist tradition, buddhist youth' where [Phrase] = 'buddhis*'
Print 'Added White List Items for ''buddhis*'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'bum*')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'bum*','')
Print 'Added ''bum*'''
END

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'bumbailiff, bumbailiffs, bumbershoot, bumbershoots, bumblebee, bumblebees, bumble, bumbles, bumbled, bumbling, bumboat, bumboats, bumboats, bummer, bummers, bumfbumph, bump, bumps, bumped, bumper, bumpers, bumping, bumpy, bumpier, bumpiest, bumpily, bumper sticker, bumper stickers, bumptious, bumptiously, bumptiousness,' where [Phrase] = 'bum*'
Print 'Added White List Items for ''bum*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'butt')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'butt*' WHERE [Phrase] = 'butt'
END
Print 'Updated ''butt'' to ''butt*'''

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'butte, butted, buttes, butter, buttered, butteres, buttering, butterbean, butterbeans, butter bean, butter beans, butterbur, butterburs, butter bur, butter burs, butterclam, butterclams, butter clam, butter clams, buttercup, buttercups, butter cup, butter cups, butterfat, butterfats, butter fat, butter fats, butterfinger, butterfingers, butter finger, butter fingers, butterfly, butterflies, butting, buttings, butterknife, butterknives, butter knife, butter knives, buttermilk, butter milk, butternut, butternuts, butter nut, butter nuts, butterscotch, butter scotch, buttertree, buttertrees, butter tree, butter trees, butterweed, butterweeds, butter weed, butter weeds, butterwort, butterworts, butter wort, butter worts, buttery, butteries, button, buttonball, button balls, button ball, button balls, buttonbush, buttonbushes, button bush, button bushes, buttonhole, buttonholed, button-holed, buttonholes, button hole, button holed,  buttonholes, buttonhook, buttonhooked, button-hooked, buttonhooks, button hooks, button hooked, button hooks, buttonmold, buttonmolded, buttonmolds, button mold, button molded, button-molded, button molds, buttshaft, buttonshafts, button shaft, button shafts, buttonwood, buttonwoods, button wood, button woods, buttress, buttressed, buttresses, buttressing, butthinge, butthinged, butthinges, butthinging, butt hinge, butt hinged, butt hinges, butt hinging, butt-hinge, butt-hinged, butt-hinges, butt-hinging, buttjoint, buttjoints, buttjointed, buttjointing, butt joint, butt joints, butt jointed, butt jointing, butt-joint, butt-joints, butt-jointed, butt-jointing, butt shaft, butt shafts, buttweld, buttwelds, buttwelded, buttwelders, buttweldings, buttwelderings, butt weld, butt welds, butt welded, butt welders, butt weldings, butt welderings,  butt-welds, butt-welded, butt-welders, butt-weldings, butt-welderings, buttondown, button down, button-down, buttondowned, button downed, button-downed, buttondowns, button downs, button-downs, buttondowning, button downing, button-downing, buttontree, button tree, buttontrees, button trees' where [Phrase] = 'butt*'
Print 'Added White List Items for ''butt*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'cajun')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'cajun*' WHERE [Phrase] = 'cajun'
END
Print 'Updated ''cajun'' to ''cajun*'''

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'cajun accent, cajun-american, cajun art, cajun artist, cajun awareness, cajun background, cajun bookstore, cajun business, cajun businesses, cajun celebration, cajun cookin'', cajun cooking, cajun cuisine, cajun cultural, cajun culture, cajun custom, cajun dialect, cajun expression, cajun food, cajun heritage, cajun history, cajun identity, cajun market, cajun media, cajun music, cajun musician, cajun neighborshood, cajun network, cajun newspaper, cajun origin, cajun outreach, cajun politics, cajun population, cajun practice, cajun radio, cajun restaurant, cajun senior, cajun spice, cajun spicy, cajun support, cajun symbol, cajun tradition, cajun youth' where [Phrase] = 'cajun*'
Print 'Added White List Items for ''cajun*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'cash back')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'cash*' WHERE [Phrase] = 'cash back'
END
Print 'Updated ''cash back'' to ''cash*'''

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'cashaw, cashaws, cashed, cashing, cashes, cashless, cashable, cashbook, cashbooks, cashbox, cashboxes, cashboxed, cash box, cash boxes, cashew, cashews, cashier, cashiers, cashiered, cashiering, cashflow, cashflows, cash flow, cash flows, cashflowed, cashpoint, cashpoints, cashflowing, cashmere, cashmeres, cashoo, cashoos' where [Phrase] = 'cash*'
Print 'Added White List Items for ''cash*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'cash prize*')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'cash prize*'
Print 'Deleted ''cash prize*'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'catholic*')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'catholic*','')
Print 'Added ''catholic*'''
END


IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'children')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'child*' WHERE [Phrase] = 'children'
END
Print 'Updated ''children'' to ''child*'''

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'children''s behavior, children''s books, children''s casual, children''s classroom, children''s clothes, children''s clothing, children''s department, children''s education, children''s furnishings, children''s furniture, children''s games, children''s health, children''s healthcare, children''s nutrition, children''s toys, children''s wear' where [Phrase] = 'child*'
Print 'Added White List Items for ''child*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'chinese')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'chinese*' WHERE [Phrase] = 'chinese'
END
Print 'Updated ''chinese'' to ''chinese*'''

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'chinese-american, chinese art, chinese artist, chinese awareness, chinese background, chinese bookstore, chinese broadcasting, chinese business, chinese businesses, chinese celebration, chinese center, chinese community, chinese cooking, chinese cuisine, chinese cultural, chinese culture, chinese culture, chinese faith, chinese food, chinese holiday, chinese interpreter, chinese language, chinese market, chinese media, chinese music, chinese musicians, chinese nation, chinese national, chinese neigborhood, chinese network, chinese new year, chinese outreach, chinese-owned, chinese politics, chinese radio, chinese restaurant, chinese senior, chinese support, chinese technologies, chinese technologies, chinese temple, chinese translator, chinese youth' where [Phrase] = 'chinese*'
Print 'Added White List Items for ''chinese*'''
END

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'christian-american, christian art, christian artist, christian awareness, christian background, christian belief, christian believers, christian bookstore, christian broadcasting, christian business, christian businesses, christian celebration, christian cemetery, christian center, christian canon, christian cathedral, christian chapel, christian charismatic, christian church, christian churchman, christian clergy, christian clergyman, christian clergymen, christian clergywoman, christian clergywomen, christian commandments, christian communion,  christian community, christian congregation, christian conservative, christian counselor, christian counseling, christian cultural, christian culture, christian custom, christian deacon, christian deity, christian discipline,christian dogma, christian evangelical, christian expression, christian faith, christian fundamentalism, christian fundamentalist, christian heritage, christian history, christian holiday, christian holiness, christian identity, christian media, christian minister, christian ministry, christian music, christian musicians, christian nation, christian neighborhood, christian network, christian newspaper, christian ordination, christian origin, christian outreach, christian-owned, christian pastor, christian politics, christian preacher, christian prophecies, christian prophecy, christian prophet, christian protestant, christian radio, christian reform, christian reformation, christian religion, christian restuarant, christian revelation, christian sabbath, christian sacraments, christian seminary, christian senior, christian support, christian tv, christian television, christian tenets, christian theology, christian tradition, christian trinity, christian worshiper, christian youth, christmas, christmas help, christmas hiring, christmas staff, christmas worker' where [Phrase] = 'christ*'
Print 'Added White List Items for ''christ*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'christian')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'christian'
Print 'Deleted ''christian'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'cracker')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'cracker*' WHERE [Phrase] = 'cracker'
END
Print 'Updated ''cracker'' to ''cracker*'''

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'cracker barrel, cracker barrels, crackerjack, crackerjacks, cracker jack, cracker jacks, cheese and cracker, cheese and crackers' where [Phrase] = 'cracker*'
Print 'Added White List Items for ''cracker*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'crackers')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'crackers'
Print 'Deleted ''crackers'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'credit')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'credit'
Print 'Deleted ''credit'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'credit background')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'credit background*' WHERE [Phrase] = 'credit background'
END
Print 'Updated ''credit background'' to ''credit background*'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'c3it approval')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'c3it approval'
Print 'Deleted ''c3it approval'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'credit card')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'credit card'
Print 'Deleted ''credit card'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'c3it card* requi3')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'c3it card* requi3'
Print 'Deleted ''c3it card* requi3'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'credit check')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'credit check*' WHERE [Phrase] = 'credit check'
END
Print 'Updated ''credit check'' to ''credit check*'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'c3it counsel')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'c3it counsel'
Print 'Deleted ''c3it counsel'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'credit history*')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'credit history*','')
Print 'Added ''credit history*'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'credit past')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'credit past','')
Print 'Added ''credit past'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'credit record*')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'credit record*','')
Print 'Added ''credit record*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'credit review')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'credit review*' WHERE [Phrase] = 'credit review'
END
Print 'Updated ''credit review'' to ''credit review*'''

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'credit search*')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'credit search*','')
Print 'Added ''credit search*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'creole')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'creole*' WHERE [Phrase] = 'creole'
END
Print 'Updated ''creole'' to ''creole*'''

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'creole-american, creole art, creole artist, creole awareness, creole background, creole bookstore, creole business, creole businesses, creole celebration, creole cookin'', creole cooking, creole cuisine, creole cultural, creole culture, creole custom, creole expression, creole food, creole heritage, creole history, creole identity, creole market, creole media, creole music, creole musician, creole neighborshood, creole network, creole newspaper, creole origin, creole outreach, creole-owned, creole politics, creole population, creole practice, creole radio, creole restaurant, creole senior, creole spice, creole spicy, creole support, creole symbol, creole tradition, creole youth' where [Phrase] = 'creole*'
Print 'Added White List Items for ''creole*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'criminal')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'criminal'
Print 'Deleted ''criminal'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'criminal background check')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'criminal background*' WHERE [Phrase] = 'criminal background check'
END
Print 'Updated ''criminal background check'' to ''criminal background*'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'criminal history check')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'criminal history*' WHERE [Phrase] = 'criminal history check'
END
Print 'Updated ''criminal history check'' to ''criminal history*'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'criminal history search')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'criminal history search'
Print 'Deleted ''criminal history search'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'criminal record check')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'criminal record*' WHERE [Phrase] = 'criminal record check'
END
Print 'Updated ''criminal record check'' to ''criminal record*'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'criminal record search')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'criminal record search'
Print 'Deleted ''criminal record search'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'criminal review*')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'criminal review*','')
Print 'Added ''criminal review*'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'criminal search*')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'criminal search*','')
Print 'Added ''criminal search*'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'communis*')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'communis*','')
Print 'Added ''communis*'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'coptic')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'coptic','')
Print 'Added ''coptic'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'cute')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'cute','')
Print 'Added ''cute'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'dance')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'danc*' WHERE [Phrase] = 'dance'
END
Print 'Updated ''dance'' to ''danc*'''

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'dance celebration, dance instruction, dance instructor, dance literacy, dance literature, dance performance, dance recital, dance rehearsal, dance school, dance show, dance studio, dance teacher' where [Phrase] = 'danc*'
Print 'Added White List Items for ''danc*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'debt*')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'debt*'
Print 'Deleted ''debt*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'debt consol*')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'debt consolidation*' WHERE [Phrase] = 'debt consol*'
END
Print 'Updated ''debt consol*'' to ''debt consolidation*'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'diet')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'diet*' WHERE [Phrase] = 'diet'
END
Print 'Updated ''diet'' to ''diet*'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'diet*')
BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'diet center, dietician, dieticians' where [Phrase] = 'diet*'
Print 'Added White List Items for ''diet*'''
END

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'australian dingo, dingo breed, dingo dog, dingo fence, dingo conservation, dingo hunting, dingo preservation, dingo poaching, dingo species, dingo trapping, dingo trapper, domesticated dingo, endangered dingo, wild dingo' where [Phrase] = 'dingo'
Print 'Added White List Items for ''dingo'''
END

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'dvd authoring, dvd burn, dvd burner, dvd decrypted, dvd disc, dvd disk, dvd reorder, dvd region code, dvd release, dvd ripper, dvd sales, dvd shrink, dvd single, dvd support, readable dvd, read-write dvd, rewritable dvd, writable dvd, dvd video disc, dvd video disk' where [Phrase] = 'dvd'
Print 'Added White List Items for ''dvd'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'dwarves')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'dwarves','')
Print 'Added ''dwarves'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'ebony')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'ebony','')
Print 'Added ''ebony'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'enhance')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'enhance'
Print 'Deleted ''enhance'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'employee drug testing')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'employee drug testing','')
Print 'Added ''employee drug testing'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'enlarge')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'enlarge'
Print 'Deleted ''enlarge'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'escort*')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'escort*'
Print 'Deleted ''escort*'''
END

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'ethnic cooking, ethnic cuisine, ethnic food, ethnic foods, ethnic food preparation' where [Phrase] = 'ethnic*'
Print 'Added White List Items for ''ethnic*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'exoffeder')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'exoffeder'
Print 'Deleted ''exoffeder'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'exoffeder background')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'exoffeder background'
Print 'Deleted ''exoffeder background'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'ex-offender background check')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'ex-offender background check','')
Print 'Added ''ex-offender background check'''
END

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'exotic animal, exotic animals, exotic pet, exotic pets, exotic pet shop, exotic pet store' where [Phrase] = 'exotic*'
Print 'Added White List Items for ''exotic*'''
END

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = '' where [Phrase] = 'fairy'
Print 'Added White List Items for ''fairy'''
END

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'butter fat, butter fats, butterfat, butterfats, fat Tuesday, fatty acid, fatty acids, fatback, fat back, fatal, fatal flaw, fatal flaws, fatalities, fatalistic, fatalism, fatally, faltally flawed, fatality, fate, fated, fateful, fatefully, fatefulness, father, fathers, fatherhood, father-in-law, fatherland, fatherless, fatherly, fatherly, father''s day, father time, fathom, fathoms, fathomable, fathometer, fathomless, fathomlessly, fatidic, fatidical, fatigue, fatigable, fatigability, fatima, fatimid, fatimid, fatling, fatsoluble, fatuity, fatuitous, fatuous, fatuously, fatuousness, infatiguable, unfathomable' where [Phrase] = 'fat*'
Print 'Added White List Items for ''fat*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'fee')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'fee*' WHERE [Phrase] = 'fee'
END
Print 'Updated ''fee'' to ''fee*'''

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'fee*')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'fee*','')
Print 'Added ''fee*'''
END

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'feed, feedable, feedback, feedbacks, feedbacking, feedbag, feedbags, feedbox, feedboxes, feedboxed, feeder, feeders, feedhole, feedholes, feeding, feedings, feedling, feedlings, feedlot, feedlots, feedlotted, feedlotting, feeds, feedstock, feedstocks, feedstuff, feedstuffs, feed trough, feed troughs, feed troughing, feedthrough, feedthroughs, feedthroughput, feedthroughputs, feedyard, feedyards, feeing, feel, feeler, feelers, feeless, feelgood, feelgoods, feel good, feel goods, feeling, feelings, feelingless, feelingly, feelingness, feelingnesses, feelings, feels, feer, feered, feeries, feerin, feering, feerings, feers, feese, feesed, feeses, feesing, feet, feetfirst, feet first, feetless, feeze, feezed, feezes, feezing' where [Phrase] = 'fee*'
Print 'Added White List Items for ''fee*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'fee for service')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'fee for service'
Print 'Deleted ''fee for service'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'fee-for-service')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'fee-for-service'
Print 'Deleted ''fee-for-service'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'felony')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'felon*' WHERE [Phrase] = 'felony'
END
Print 'Updated ''felony'' to ''felon*'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'felony background')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'felony background'
Print 'Deleted ''felony background'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'felony offender')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'felony offender'
Print 'Deleted ''felony offender'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'felony-free workplace')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'felony-free workplace'
Print 'Deleted ''felony-free workplace'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'flat fee*')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'flat fee*'
Print 'Deleted ''flat fee*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'focus group*')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'focus group*'
Print 'Deleted ''focus group*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'foreign')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'foreign accent*' WHERE [Phrase] = 'foreign'
END
Print 'Updated ''foreign'' to ''foreign accent*'''

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'accented, accenting, accentless, accentor, accentors, acentualities, accentuality, accentually, accentuate, accentuated, accentuating, accentuation, accentuations' where [Phrase] = 'foreign accent*'
Print 'Added White List Items for ''foreign accent*'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'foreigner*')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'foreigner*','')
Print 'Added ''foreigner*'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'gal*')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'gal*','')
Print 'Added ''gal*'''
END

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'gala, galas, gala event, gala events, galabia, galabiya, galactagogue, galacctic, galacto, galactometer, galactorrha, galactose, galactosemia, galactoside, galah, galahad, galangal, galantine, galantines, galanty, galapagos, galata, galatea, galati, galatia, galatians, galavant, galax, galaxies, galaxy, galba, galbanum, gale, gales, gale-force, galea, galeae, galeate, galeated, galen, galena, galenical, galenism, galenite, galibi, galicia, galician, galilean, galilee, galileo, galimatias, galingale, galiot, galipot, gall, galls, galled, galling, galla, gallant, gallantly, gallantry, gallatin, gallbladder, gallbladders, gall bladder,gall bladders, galle, galleass, gallein, galleon, galleried, galleries, galles, gallery, gallet, gallets, galleta, galleym gallies, gallyproof, gally-proof, galleywest, gallfly, gallflies, galia, galliard, galliass, gallic, gallican, gallicanism, gallicize, gallicizes gallicized, gallicizing, gallicurci, galligaskins, gallimaufry, gallinacean, gallinaceous, gallinipper, gallinippers, gallinule, galliot, gallipoli, gallipot, gallium, gallivant, galliwasp, galliwasps, gallmidge, gallmidges, gall midge, gall midges, gallmite, gallmites, gall mite, gall mites, gallnut, gallnuts, galloglass, gallon, gallons, gallonage, galloon, gallop, gallops, galloped, galloping, galloper, gallopers, gallopade, gallous, galloway, gallowglass, gallow, gallows, gallstone, gallstones, gall stone, gall stones, galluses, galios, gallot, galop, galore, galosh, galoshes, galsworth, galton, galumph, galcani, galvanic, galvanical galvanically, galvanism, galvinize, galvanization, galvano, galvanometer, galvanometers, galvomagnetic, galvaston, galway, galwegian, galyac, galyak' where [Phrase] = 'gal*'
Print 'Added White List Items for ''gal*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'gay')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'gay*' WHERE [Phrase] = 'gay'
END
Print 'Updated ''gay'' to ''gay*'''

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'gayal, gayals, gayer, gayeties, gayety, gaylord' where [Phrase] = 'gay*'
Print 'Added White List Items for ''gay*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'gentleman''s club')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'gentleman''s club'
Print 'Deleted ''gentleman''s club'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'gentlemen''s club')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'gentlemen''s club'
Print 'Deleted ''gentlemen''s club'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'girl')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'girl*' WHERE [Phrase] = 'girl'
END
Print 'Updated ''girl'' to ''girl*'''

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'girls rock, girl''s casual, girl''s class, girl''s classroom, girl''s clothing, girl''s department, girl''s school, girl''s schools, girl''s sports, girl''s sportsgear, girl''s sportswear, girl''s wear, girls'' casual, girls'' class, girls'' classroom, girls'' clothing, girls'' department, girls'' school, girls'' schools, girls'' sports, girls'' sportsgear, girls'' sportswear, girls'' wear, Girl''s Clubs of America, Girl Scouts of America' where [Phrase] = 'girl*'
Print 'Added White List Items for ''girl*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'girl friend*')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'girl friend*'
Print 'Deleted ''girl friend*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'girlfriend*')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'girlfriend*'
Print 'Deleted ''girlfriend*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'giva-away*')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'give-away*' WHERE [Phrase] = 'giva-away*'
END
Print 'Updated ''giva-away*'' to ''give-away*'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'glbt')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'glbt*' WHERE [Phrase] = 'glbt'
END
Print 'Updated ''glbt'' to ''glbt*'''

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'glbt alliance, glbt activism, glbt activist, glbt coalition, glbt community, glbt counseling, glbt counselor, glbt hotline, glbt lobbyist, glbt movement, glbt outreach, glbt-owned, glbt rights, glbt support, glbt volunteer' where [Phrase] = 'glbt*'
Print 'Added White List Items for ''glbt*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'good c3it*')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'good c3it*'
Print 'Deleted ''good c3it*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'guaranteed')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'guaranteed'
Print 'Deleted ''guaranteed'''
END

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'jobation, jobations, jobbed, jobber, jobberies, jobbers, jobbery, jobbie, jobbies, jobbing, jobbings, job center, job centers, job centre, job centres, jobe, jobernowl, jobernowls, jobes, job holder, job holders, jobholder, jobholders, jobing, jobless, joblessness, joblessnesses, job ready, job-ready, job seeker, job seekers, jobseeker, jobseekers, job share, jobshare, jobsworth, jobsworths, job worthy, job-worthy' where [Phrase] = 'guaranteed job*'
Print 'Added White List Items for ''guaranteed job*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'guaranteed job placement*')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'guaranteed job placement*'
Print 'Deleted ''guaranteed job placement*'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'guy*')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'guy*','')
Print 'Added ''guy*'''
END

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'guyana, guyanas, guyanese, guyenne, guyot' where [Phrase] = 'guy*'
Print 'Added White List Items for ''guy*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'height')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'height'
Print 'Deleted ''height'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'herbal*')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'herbal*'
Print 'Deleted ''herbal*'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'hijab')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'hijab','')
Print 'Added ''hijab'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'hispanic')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'hispanic*' WHERE [Phrase] = 'hispanic'
END
Print 'Updated ''hispanic'' to ''hispanic*'''

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'hispanic-american, hispanic art, hispanic artist, hispanic awareness, hispanic background, hispanic bookstore, hispanic broadcasting, hispanic business, hispanic businesses, hispanic celebration, hispanic cemetary, hispanic center, hispanic cooking, hispanic cuisine, hispanic cultural, hispanic culture, hispanic custom, hispanic dialect, hispanic expression, hispanic faith, hispanic food, hispanic heritage, hispanic history, hispanic holiday, hispanic identity, hispanic interpreter, hispanic language, hispanic market, hispanic media, hispanic music, hispanic musician, hispanic nation, hispanic national, hispanic neighborhood, hispanic network, hispanic newspaper, hispanic origin, hispanic outreach, hispanic-owned, hispanic politics, hispanic population, hispanic practice, hispanic radio, hispanic restaurant, hispanic seniors, hispanic support, hispanic symbol, hispanic TV, hispanic television, hispanic tradition, hispanic translator, hispanic youth' where [Phrase] = 'hispanic*'
Print 'Added White List Items for ''hispanic*'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'hooker*')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'hooker*','')
Print 'Added ''hooker*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'holy spirit')
BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'dispirit, dispirits, dispirited, dispiriting, inspirit, inspirits, inspirited, inspiriting, spirits, spirited, spiritedness, spiritedly, spiritless, spiritlessness, spiriting, spirituous' where [Phrase] = 'holy spirit'
Print 'Added White List Items for ''holy spirit'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'home based position*')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'home based position*','')
Print 'Added ''home based position*'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'home-based position*')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'home-based position*','')
Print 'Added ''home-based position*'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'home based work')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'home based work','')
Print 'Added ''home based work'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'home-based work')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'home-based work','')
Print 'Added ''home-based work'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'homo')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'homo*' WHERE [Phrase] = 'homo'
END
Print 'Updated ''homo'' to ''homo*'''

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'homocercal, homochromatic, homochromous, homoecious, homgamy, homogenate, homogeneous, homogeneously, homogeneity, homogeneousness, homogenize, homogenizes, homogenized, homogenizing, homogenization, homogenous, homograph, homographs, homographic, homoio, homoiothermal, homoiousian, homolecithal, homologate, homologates, homologated, homologating, homologation, homological, homologically, homologize, homologizes, homologized, homologizing, homologous, homolographic, homologue, homologues, homolog, homologs, homology, homolosine, homomorphism, homomophy, homomophic, homomophous, homonym, homonyms, homonymic, homoymous, homoousian, homophone, homophonic, homophony, homoplastic, homoplastically, homoplasty, homopteran, homopterous, homosapien, homosapiens, homo sapien, homo sapiens, homo-sapien, homo-sapiens, homosphere, homospheres, homospheric, homoporous, homostyly, homostylous, homotaxis, homothalic, homothallism, homotransplant, homotransplantation, homozygote, homozygotic, homozygosis, homozygous' where [Phrase] = 'homo*'
Print 'Added White List Items for ''homo*'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'hottie')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'hottie','')
Print 'Added ''hottie'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'implant')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'implant'
Print 'Deleted ''implant'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'indian')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'indian*' WHERE [Phrase] = 'indian'
END
Print 'Updated ''indian'' to ''indian*'''

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'bureau of indian affairs, indiana, indianapolis, indian-american, indian art, indian awareness, indian background, indian bookstore, indian broadcasting, indian burial, indian business, indian businesses, indian celebration, indian casino, indian center, indian chief, indian community, indian cooking, indian cuisine, indian cultural, indian culture, indian custom, indian discipline, indian drug, indian faith, indian food, indian health, indian government, indian heritage, indian history, indian holiday, indian identity, indian interpretor, indian language, indian law, indian market, indian media, indian medicine, indian music, indian musician, indian nation, indian national, indian network, indian newspaper, indian origin, indian outreach, indian-owned, ndian philosophy, indian politics, indian population, indian practice, indian prophecies, prophecy, prophet, indian radio, indian reservation, indian restaurant, indian sect, indian seniors, indian substance, indian support, indian symbol, indian TV, indian television, indian tradition, indian translator, indian tribal, indian tribe, indian youth' where [Phrase] = 'indian*'
Print 'Added White List Items for ''indian*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'islamic')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'islamic'
END
Print 'Deleted ''islamic'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'islam')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'islam*' WHERE [Phrase] = 'islam'
END
Print 'Updated ''islam'' to ''islam*'''

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'islamic-american, islamic ahmadiyya, islamic ayatollah, islamic art, islamic artist, islamic awareness, islamic background, islamic belief, islamic believer, islamic bookstore, islamic broadcasting, islamic burial, islamic business, islamic businesses, islamic celebration, islamic center, islamic cleric, islamic community, islamic cooking, islamic counseling, islamic cuisine, islamic cultural, islamic culture, islamic custom, islamic diet, islamic discipline, islamic expression, islamic faith, islamic faith-based, islamic food, islamic flag, islamic government, islamic heritage, islamic history, islamic holiday, islamic hospital, islamic identity, islam iman, islamic interpreter, islamic khatib, islamic khutba, islamic language, islamic law, islamic market, islamic media, islamic mosque, islamic mufti, islamic mullah, islamic music, islamic musician, islamic nation, islamic national, islamic neighborhood, islamic network, islamic newspaper, islamic origin, islamic outreach, islamic-owned, islamic philosophy, islamic politics, islamic population, islamic practice, islamic prophecies, islamic prophecy, islamic prophet, islamic radio, islamic restaurant, islamic religion, islamic sect, islamic senior, islamic seniors, slamic support, islamic symbol, islamic TV, islamic television, islamic theology, islamic tradition, islamic translator, islamic tribe, islamic tribal, islamic university, islamic worship, islamic youth' where [Phrase] = 'islam*'
Print 'Added White List Items for ''islam*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'japanese')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'japanese*' WHERE [Phrase] = 'japanese'
END
Print 'Updated ''japanese'' to ''japanese*'''

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'japanese-american, japanese art, japanese artist, japanese awareness, japanese background, japanese broadcasting, japanese business, japanese businesses, japanese car, japanese celebration, japanese center, japanese community, japanese cooking, japanese cuisine, japanese cultural, japanese culture, japanese custom, japanese discipline, japanese food, japanese faith, japanese holiday, japanese language, japanese market, japanese media, japanese nation, japanese national, japanese network, japanese newspaper, japanese origin, japanese outreach, japanese-owned, japanese politics, japanese population, japanese practice, japanese radio, japanese religion, japanese restaurant, japanese senior, japanese support, japanese symbol, japanese sushi, japanese TV, japanese technologies, japanese technology, japanese tradition, japanese translator, japanese youth' where [Phrase] = 'japanese*'
Print 'Added White List Items for ''japanese*'''
END

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'jehovah witness, jehovah witnesses' where [Phrase] = 'jehovah'
Print 'Added White List Items for ''jehovah'''
END

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = null where [Phrase] = 'jesus*'
Print 'Deleted White List Items for ''jesus*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'jew')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'jew*' WHERE [Phrase] = 'jew'
END
Print 'Updated ''jew'' to ''jew*'''

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'bejewel, bejewels, bejeweled, bejeweling, jewel, jewels, jewelry, jewish-american, jewish art, jewish artist, jewish awareness, jewish background, jewish bakery, jewish belief, jewish believer, jewish business, jewish businesses, jewish canon, jewish cantor, jewish celebration, jewish cemetery, jewish center, jewish community, jewish cooking, jewish cuisine, jewish cultural, jewish culture, jewish deli, jewish discipline, jewish dogma, jewish expression, jewish faith, jewish flag, jewish follower, jewish fundamentalism, jewish fundamentalist, jewish food, jewish government, jewish hazzan, jewish heritage, jewish history, jewish holiday, jewish hospital, jewish kosher, jewish language, jewish law, jewish market, jewish media, jewish midrash, jewish music, jewish musician, jewish nation, jewish national, jewish neighborhood, jewish network, jewish newspaper, jewish orthodox, jewish origin, jewish outreach, jewish-owned, jewish philosophy, jewish politics, jewish population, jewish practice, jewish prophecies, jewish prophecy, jewish prophet, jewish rabbi, jewish restaurant, jewish revelation, jewish sabbath, jewish sect, jewish seniors, jewish shochet, jewish support, jewish symbol, jewish synagogue, jewish tabernacle, jewish TV, jewish television, jewish temple, jewish tenets, jewish theology, jewish tradition, jewish translator, jewish tribe, jewish university, jewish youth, orthodox jew, orthodox jewish' where [Phrase] = 'jew*'
Print 'Added White List Items for ''jew*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'jig')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'jig*' WHERE [Phrase] = 'jig'
END
Print 'Updated ''jig'' to ''jig*'''

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'jiggle, jiggles, jiggled, jiggling' where [Phrase] = 'jig*'
Print 'Added White List Items for ''jig*'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'judaism')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'judaism','')
Print 'Added ''judaism'''
END

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'grown kiwi, fresh kiwi, frozen kiwi, gourmet kiwi, kiwi bird, kiwi cultivation, kiwi exports, kiwi exporter, kiwi farming, kiwi fruit, kiwi growing, kiwi harvest, kiwi harvester, kiwi harvesting, kiwi health benefits, kiwi imports, kiwi importer, kiwi magazine, kiwi packaging, kiwi salad, kiwi seeds, kiwi shipment, kiwi shipper, kiwi shipping, kiwi shoe, kiwi picker, kiwi vine' where [Phrase] = 'kiwi'
Print 'Added White List Items for ''kiwi'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'koran')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'koran','')
Print 'Added ''koran'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'korean')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'korean*' WHERE [Phrase] = 'korean'
END
Print 'Updated ''korean'' to ''korean*'''

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'korean-american, korean art, korean artist, korean awareness, korean background, korean bookstore, korean broadcasting, korean business, korean businesses, korean celebration, korean center, korean community, korean cooking, korean cuisine, korean cultural, korean culture, korean custom, korean discipline, korean expression, korean flag, korean food, korean government, korean heritage, korean history, korean holiday, korean identity, korean interpreter, korean language, korean law, korean market, korean media, korean music, korean musician, korean nation, korean national, korean neighborhood, korean network, korean origin, korean outreach, korean-owned, korean politics, korean population, korean practice, korean radio, korean restaurant, korean senior, korean support, korean symbol, korean TV, korean television, korean tradition, korean translator, korean youth' where [Phrase] = 'korean*'
Print 'Added White List Items for ''korean*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'ladies')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'ladies'
Print 'Deleted ''ladies'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'lady*')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'lad*' WHERE [Phrase] = 'lady*'
END
Print 'Updated ''lady*'' to ''lad*'''

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'ladies accessories, ladies casual, ladies clothing, ladies department, ladies formal, ladies hats, ladies jewelry, ladies slacks, ladies shoes, ladies sports, ladies sportsgear, ladies sportswear, ladies wear, ladieswear, ladies day, ladies night, ladiestress, ladiestresses, ladin, lading, ladino, lade, laded, laden, lading, ladle, lades, ladled, ladling, ladling, ladleful, ladlefuls, ladoga, ladrone, ladybird, ladybirds, ladybug, ladybugs, lady chapel, lady day, lady days, ladyfinger, ladyfingers, ladyfish, lady fish, lady in waiting, lady of the lake, ladyship, ladyslipper, ladyslippers, lady slipper, lady slippers, lady''s smock, lady''s-smock, lady''s thumb, lady''s-thumb, ladder, ladders, ladderback, ladderbacks, ladder back, ladder backs, ladder stitch, ladderstitch' where [Phrase] = 'lad*'
Print 'Added White List Items for ''lad*'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'latter day saints')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'latter day saints','')
Print 'Added ''latter day saints'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'lgbt')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'lgbt*' WHERE [Phrase] = 'lgbt'
END
Print 'Updated ''lgbt'' to ''lgbt*'''

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'lgbt alliance, lgbt activism, lgbt activist, lgbt coalition, Lgbt community, lgbt counseling, lgbt counselor, lgbt hotline, lgbt lobbyist, lgbt movement, lgbt outreach, lgbt-owned, lgbt support, lgbt volunteer' where [Phrase] = 'lgbt*'
Print 'Added White List Items for ''lgbt*'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'lingerie')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'lingerie','')
Print 'Added ''lingerie'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'lord')
BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'lorded, lording, lordings, lordkin, lordkins, lords, lordship, lordships, lordy' where [Phrase] = 'lord'
Print 'Added White List Items for ''lord'''
END

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'maledict, malediction,maledictions, maledictory, malefaction, malefactions, malefactor, malefactors, malefactress, male fern, malefic, maleficient, maleficience, maleic, malemute, malemutes, malentendu, malevolence, malevolent, malevolency, male-owned' where [Phrase] = 'male*'
Print 'Added White List Items for ''male*'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'man*')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'man*','')
Print 'Added ''man*'''
END

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'mana, manacle, manacles, manacled, manacling, manado, manage, manages, managed, manager, managers, mangeress, managership, managerships, managerial, managing, managable, managability, managableness, managua, manakin, manakins, manama, manassas, manasseh, manatarms, man-at-arms, manatee, manatees, manaus, manchester, manchet, machette, machettes, manchineel, manchu, manchukuo, manchuria, manchurian, manciple, manciples, mancunian, mandaean, mandala, mandalas, mandalay, mandamus, mandan, mandans, mandarin, mandate, mandates, mandated, mandating, mandator, mandators, mandatory, mandatorily, mandean, mandeville, mandible, mandibles, mandibular, mandibulate, mandibulates, mandolin, mandolins, mandrake, mandrel, mandril, mandrill, mandrills, manducate, manduates, manducated, manducating, manducation, manducatory, mane, manes, maned, maneless, maneges, maneuver, maneuvers, maneuved, maneuvering, maneuverable, maneuverability, mangabey, mangabeys, manganate, manganese, manganic, manganite, manganous, mange, mangelwurzel, manger, mangers, mangle, mangles, mangled, mangling, mangler, manglers, mango, mangoes, mangos, mangonel, mangosteen, mangrove, mangroves, mangy, mangier, mangiest, manhattan, manhattanize, manhattanized, manhattinizing, mani, manical, maniacally, manicotti, manicure, manicures, manicured, manicuring, manicurist, manicurists, manifest, manifestable, manifestly, manifestation, manifestations, manifesto, manifestoes, manifestos, manifold, manifolds, manila, man-in-the-street, manioc, maniple, manipular, manipulate, manipulates, manipulated, manipulating, manipulator, manipulators, manipulation, manipulations, manipulatable, manipulability, manipulative, manipulatory, manipur, manisa, manitoba, manitoulin, manizales, manjack, manmade, manna, mannequin, mannequins, manner, manners, mannered, mannering, mannerism, mannerisms, mannerist, manneristic, mannerless, mannerly, unmannerly, unmanned, mannheim, mannikin, mannite, mannitol, mannose, manoeuvre, manoeuvred, manoeuvres, manoeuvring, manoeuvrer, manoeuvrers, man-of-war, manometer, manometers, manor, manors, manorial, manpower, manque, manrope, mansard, manse, mansfield, manship, mansion, mansions, mansuetude, mansur, manta, manteau, mantegna, mantel, mantels, mantelelet, mantelletta, mantelpiece, mantelpieces, manteltree, mantic, mantic, mantilla, mantis, mantises, mantes, mantissa, mantle, mantles, mantled, mantling, mantlerock, mantlet, mantous, mantra, mantras, mantua, manual, manuals, manualized, manually, manubrium, manubrium, manuel, manfactory, manufacture, manufactures, manufactured, manufacturing, manfacturer, manfacturers, manumission, manumit, manure, manures, manured, manuring, manus, manuscript, manuscripts, manuscripted, manuscripting, manutius, manx, many, manypiles, manysided, many-sided, mayzanita, manzoni, tally man' where [Phrase] = 'man*'
Print 'Added White List Items for ''man*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'man''s job')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'man''s job'
Print 'Deleted ''man''s job'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'man''s work')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'man''s work'
Print 'Deleted ''man''s work'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'men')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'men*' WHERE [Phrase] = 'men'
END
Print 'Updated ''men'' to ''men*'''

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'men''s accessories, men''s casual, men''s clothing, men''s department, men''s formal, men''s hats, men''s jewelry, men''s shoes, men''s slacks, men''s sports, men''s sportsgear, men''s sportswear, menswear, menswears, men''s wear, men''s rights, men''s colleges, men''s schools, men''s universities, menagerie, menageries, mend, mendable, mended, mendelevioum, mendeleviums, mender, menders, mending, mendings, mends, mendes, mendiz, mendez, mene, menes, meng, menge, menged, menges, menging, mengs, menhir, menhirs, memiere''s, menthol, menolated, menthols, mention, mentionable, mentioned, mentioner, mentioners, mentioning, mentions, mento, mentonniere, mentonnieres, mentor, mentored, mentorial, mentoring, mentorings, mentors, mentorship, mentorships, mentos, mentum, menu, menuisier, menuisiers, menus, meningioma, meningiomas, meningiomata, meningitic, meningitides, meningitis, meningitises, meningocele, meningoceles, meningococci, meninggoccic, meningococcus, meningoencephalitic, meningorncephalitides, meningoencephatitis, meninx. meniscal, meniscectomies, meniscectomy, menisci, meniscoid, meniscoidal, meniscus, menispermaceous, menispermum, menispermums, meno, menologies, memology, menominee, menominees, memomini, menominis, menopausal, menopause, menopauses, menopome, menopomes, memorah, menorahs, menorrhagias, menorrhagic, menorrhea, menorrheas, menorrhoea, memorrhoeas, mensa, mensae, mensal, mensas, mense, mensed, menseful, menseless, menses, mentalism, mentalisms, mentalist, mentalistic, mentalistically, mentalists, mentalities, mentality, mentally, mentation, mentations, mentee, mentees, menthene, menthenes' where [Phrase] = 'men*'
Print 'Added White List Items for ''men*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'men''s job')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'men''s job'
Print 'Deleted ''men''s job'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'men''s work')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'men''s work'
Print 'Deleted ''men''s work'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'mentally challenged')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'mentally challenged','')
Print 'Added ''mentally challenged'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'mentally-challenged')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'mentally-challenged','')
Print 'Added ''mentally-challenged'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'mentally impaired')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'mentally impaired','')
Print 'Added ''mentally impaired'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'mentally-impaired')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'mentally-impaired','')
Print 'Added ''mentally-impaired'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'mexican')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'mexican*' WHERE [Phrase] = 'mexican'
END
Print 'Updated ''mexican'' to ''mexican*'''

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'mexican-american, mexican art, mexican artists, mexican awareness, mexican background, mexican band, mexican broadcasting, mexican business, mexican businesses, mexican celebration, mexican center, mexican community, mexican cooking, mexican cuisine, mexican cultural, mexican culture, mexican custom, mexican expression, mexican food, mexican flag, mexican government, mexican heritage, mexican history, mexican holiday, mexican identity, mexican interpreter, mexican language, mexican law, mexican market, mexican media, mexican music, mexican musician, mexican nation, mexican national, mexican neighborhood, mexican network, mexican newspaper, mexican origin, mexican outreach, mexican-owned, mexican politics, mexican population, mexican practice,  mexican radio, mexican religion, mexican restaurant, mexican senior, mexican support, mexican symbol, mexican TV, mexican television, mexican tradition, mexican translator, mexican youth' where [Phrase] = 'mexican*'
Print 'Added White List Items for ''mexican*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'middle eastern*')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'middle eastern' WHERE [Phrase] = 'middle eastern*'
END
Print 'Updated ''middle eastern*'' to ''middle eastern*'''

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = '' where [Phrase] = 'middle eastern'
Print 'Removed White List Items for ''middle eastern'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'minimum age')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'minimum age'
Print 'Deleted ''minimum age'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'mixed')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'mixed rac*', WhiteList = '' WHERE [Phrase] = 'mixed'
END
Print 'Updated ''mixed'' to ''mixed rac*'''

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'mixed-rac*')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'mixed-rac*','')
Print 'Added ''mixed-rac*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'model')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'model*' WHERE [Phrase] = 'model'
END
Print 'Updated ''model'' to ''model*'''

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'experienced model, professional model, model clay, model data, model dental, model impressions, studio model, runway model ' where [Phrase] = 'model*'
Print 'Added White List Items for ''model*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'model* needed')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'model* needed'
Print 'Deleted ''model* needed'''
END

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'transactional, transactionally' where [Phrase] = 'monetary transaction*'
Print 'Added White List Items for ''monetary transaction*'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'money-back guarantee*')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'money-back guarantee*','')
Print 'Added ''money-back guarantee*'''
END

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'ordered, ordering' where [Phrase] = 'money order*'
Print 'Added White List Items for ''money order*'''
END

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'problematic, problematical, problematically' where [Phrase] = 'money problem*'
Print 'Added White List Items for ''money problem*'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'mormon')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'mormon','')
Print 'Added ''mormon'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'native')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'native*' WHERE [Phrase] = 'native'
END
Print 'Updated ''native'' to ''native*'''


BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'native american, native art, native artist, native awareness, native background, native bookstore, native broadcasting, native business, native businesses, native casino, native celebration, native cemetery, native center, native community, native cooking, native cuisine, native cultural, native culture, native drug, native expression, native faith, native food, native government, native health, native heritage, native history, native holiday, native identity, native interpreter, native language, native law, native market, native media, native music, native musician, native nation, native national, native neighborhood, native network, native newspaper, native origin, native outreach, native-owned, native politics, native population, native practice, native radio, native restaurant, native rehabilitation, native senior, native support, native substance, native translator, native tongue, native youth' where [Phrase] = 'native*'
Print 'Added White List Items for ''native*'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'negra*')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'negra*','acquanegra, bocanegra, canegrate, cnegra, hanegraaff, negrar, negramaro, montenegran, openscengraph, quebradanegra, rhinegraves, stonegrave, valnegra')
Print 'Added ''negra*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'negro*')
BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'negroni, winegrower, winegrowers, winegrowing' where [Phrase] = 'negro*'
Print 'Added White List Items for ''negro*'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'no bankruptc*')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'no bankruptc*','')
Print 'Added ''no bankruptc*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'no arrest warrants')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'no arrest warrants'
Print 'Deleted ''no arrest warrants'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'no arrest record')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'no arrest*' WHERE [Phrase] = 'no arrest record'
END
Print 'Updated ''no arrest record'' to ''no arrest*'''

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'no debt')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'no debt','')
Print 'Added ''no debt'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'no felon')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'no felon*' WHERE [Phrase] = 'no felon'
END
Print 'Updated ''no felon'' to ''no felon*'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'no felony offenses')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'no felony offenses'
Print 'Deleted ''no felony offenses'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'nonfelon*')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'nonfelon*','')
Print 'Added ''nonfelon*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'non-felon')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'non-felon*' WHERE [Phrase] = 'non-felon'
END
Print 'Updated ''non-felon'' to ''non-felon*'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'non-felony record')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'non-felony record'
Print 'Deleted ''non-felony record'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'obese*')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'obes*' WHERE [Phrase] = 'obese*'
END
Print 'Updated ''obese*'' to ''obes*'''


IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'old*')
BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'anticold, behold, beholds, beholden, beholder, beholders, beholding, billfold, billfolds, blindfold, blindfolds, blindfolded, blindfolder, blindfolding, blindfoldings, bold, bolds, bolder, boldest, bolden, boldness, boldface, bold face, bold-face, boldfaced, bold faced, bold-faced, bondholder, bondholders, bondholding, bondholdings, candleholder, candleholders, cardholder, cardholders, cardholding, cardholdings, chokehold, chokeholds, chokeholding, cohold, coholds, coholder, coholders, coholding, coholdings, cold, colds, colder, coldest, coldcock, coldcocks, coldcocked, coldcocking, colddown, coldhearted, coldheartedly, coldheartedness, copyhold, copyholder, copyholders, copyholding, copyholdings, eightfold, embolden, emboldens, emboldened, extrabold, extrabolds, eyefold, eyefolds, fanfold, fanfolds, fanfolded, fanfolding, fingerhold, fingerholds, fivefold, fold, foldaway, foldboat, foldboats, folds, folded, folder, folders, folding, folderol, folderols, foldout, foldouts, foldup, fold-up, foothold, footholds, fortold, fourfold, freehold, freeholds, freeholder, freeholders, freeholding, freeholdings, gasholder, gasholders, gatefold, gatefolds, gold, goldbrick, goldbricks, goldbricking, goldbug, goldbugs, golden, goldeness, goldenseal, goldenseals, goldensealed, goldeneye, goldeneyed, golden-eyed, goldeye, goldeyed, gold-eyed, goldfield, goldfields, goldfinch, goldfinches, goldfish, goldfishes, goldfinger, goldfingers, goldmine, goldmines, goldmined, goldmining,  goldminer, goldminers, goldsmith, goldsmiths, goldsmithed, goldsmithing, goldstone, goldstones, goldurn, goldurns, handhold, handholds, hold, holdable,holds, holder, holders, holding, holdings, holdback, holddown, holddowns, holdfast, holdover, holdovers, holdup, hold-up, holdups, hold-ups, hoodmold, hoodmolds, household, households, householding, householder, householders, hundredfold, imbold, imbolden, imboldens, imbolder, imbolders, imboldening, inhold, inholding, inholdings, interfold, interfolds, infolder, infolders, interfolding, interfoldings, jobholder, jobholders, jobholding,  jobholdings, landhold, landholds, landholder, landholders, landholding, landholdings, leaseholder, leaseholders, leaseholding, mangolds, manifold, manifolds, manifolding, manifoldness, manifoldnesses, manifoldly, manyfold, marigold, marigolds, mold, molds, molder, molders, molding, moldier, modiest, moldiness, moldy, moldwarp, moldwarps, ninefold, old gold, oldschool, old school, old-school, oldschooled, old schooled, old-schooled, olden, officeholder, officeholders, officeholding, oldfangled, oldsmobile, oldsmobiles, onefold, outscold, outscolds, outscolded, outscolding, outtold, overhold, overholds, overholding, overwithhold, overwithholds, overwithholding, overwithholdings, pencil holder, pencil holders, penholder, penholders, pewholder, pewholders, pinfold, pinfolds, pinfolded, pinfolder, pinfolders, pinfolding, pinfoldings, placeholder, placeholders, placeholding, polder, polders, policyholder, policyholders, potholder, potholders, premold, premoldable, premolds, premolded, premolding, premoldings, premolder, premolders, presold, pretold, refold, refolds, refolded, refolding, refoldings, resold, resolder, resolders, resoldered, resoldering, retold, roadholding, roadholdings, rold, roothold, rootholds, rootholder, rootholders, rootholding, rootholdings, scaffold, scaffolds, scaffolded, scaffolding, scaffoldings, scold, scolds, scolded, scolder, scolders, scolding, scoldings, sevenfold, shareholder, shareholders, shareholding, shareholdings, sheephold, sheepholds, sixfold, smallholder, smallholders, smallholding, smallholdings, smolder, smolders, smoldered, smoldering, smolderings, snowmold, snow mold, snowmolds, snow molds, snowmolder, snowmolders, snowmoldering, sold,  soldan, soldans, solder, solderability, solders, solderer, solderers, soldering, solderings, soldier, soldiers, soldiered, soldiering, soldieries, soldiership, soldierships, stadtholder, stadtholders, stadtholded, stadtholding, stadtholderates, stadtholdership, stakeholder, stakeholders, stakeholding, stakeholdings, stallholder, stallholders, stallholding, stockholder, stockholders, stockholding, stockholdings, stokehold, stokeholds, stranglehold, strangleholds, stronghold, strongholds, strongholding, strongholdings, subthreshold, subthresholds, supersold, tenfold, titleholder, titleholders, titleholding, told, threefold, throttlehold, throttleholds, thousandfold, threshold, thresholds, thesholding, thresholdings, toehold, toeholds, toolholder, toolholders, toolholding, twofold, ultracold, unbeholding, undersold, unfold, unfolds, unfolded, unfolding, unfoldment, unfoldments, unmold, unmolds, unmolded, unmoldering, unsolder, unsolders, unsoldered, unsoldering, unsoldierly, uphold, upholds, upholder, upholders, upholding, wold, wolds, withhold, withholder, withholders, withholding, withholdings' where [Phrase] = 'old*'
Print 'Added White List Items for ''old*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'one day model*')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'one day model*'
Print 'Deleted ''one day model*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'one-day model*')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'one-day model*'
Print 'Deleted ''one-day model*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'one time fee*')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'one time fee*'
Print 'Deleted ''one time fee*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'one-time fee*')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'one-time fee*'
Print 'Deleted ''one-time fee*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'overweight')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'overweight*' WHERE [Phrase] = 'overweight'
END
Print 'Updated ''overweight'' to ''overweight*'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'ovum donor')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'ovum don*' WHERE [Phrase] = 'ovum donor'
END
Print 'Updated ''ovum donor'' to ''ovum don*'''

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'pagan*')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'pagan*','')
Print 'Added ''pagan*'''
END

UPDATE [Config.CensorshipRule] set WhiteList = 'paganini' where [Phrase] = 'pagan*'
Print 'Added White List Items for ''pagan*'''

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'paid survey')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'paid survey','')
Print 'Added ''paid survey'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'participate in a research study*')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'participate in a research study*'
Print 'Deleted ''participate in a research study*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'participate in a study')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'participate in a study'
Print 'Deleted ''participate in a study'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'participate in a survey')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'participate in a survey'
Print 'Deleted ''participate in a survey'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'pansexual*')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'pansexual*','')
Print 'Added ''pansexual*'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'pan-sexual*')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'pan-sexual*','')
Print 'Added ''pan-sexual*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'pay off*')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'pay off*'
Print 'Deleted ''pay off*'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'physical condition*')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'physical condition*','')
Print 'Added ''physical condition*'''
END

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'conditional, conditionally, conditionality' where [Phrase] = 'physical condition*'
Print 'Added White List Items for ''physical condition*'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'physically challenged')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'physically challenged','')
Print 'Added ''physically challenged'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'physically-challenged')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'physically-challenged','')
Print 'Added ''physically-challenged'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'physically impaired')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'physically impaired','')
Print 'Added ''physically impaired'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'physically-impaired')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'physically-impaired','')
Print 'Added ''physically-impaired'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'pixie sized')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'pixie sized'
Print 'Deleted ''pixie sized'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'pixie-sized')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'pixie-sized'
Print 'Deleted ''pixie-sized'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'pixy')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'pixy','')
Print 'Added ''pixy'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'poker')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'poker'
Print 'Deleted ''poker'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'poor c3it*')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'poor c3it*'
Print 'Deleted ''poor c3it*'''
END

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'at-risk pregnancy, planned pregnancy, pregnancy center, pregnancy coach, pregnancy counselor, pregnancy counselors, pregnancy crisis, pregnancy outreach, pregnancy planning, pregnancy prevention, pregnancy support, pregnancy testing, unplanned pregnancy' where [Phrase] = 'pregnancy'
Print 'Added White List Items for ''pregnancy'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'pregnant applicant')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'pregnant applicant*' WHERE [Phrase] = 'pregnant applicant'
END
Print 'Updated ''pregnant applicant'' to ''pregnant applicant*'''

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'prize*')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'prize*','')
Print 'Added ''prize*'''
END

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'prized' where [Phrase] = 'prize*'
Print 'Added White List Items for ''prize*'''
END

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'promontory, promontories, promote, promotes, promoted, promoting, promotive' where [Phrase] = 'promo*'
Print 'Added White List Items for ''promo*'''
END


BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'rican american, rican art, rican artist, rican awareness, rican background, rican bookstore, rican broadcasting, rican  business, rican businesses, rican celebration, rican cemetery, rican center, rican community, rican cooking, rican cuisine, rican cultural, rican culture, rican drug, rican  expression, rican faith, rican food, rican government, rican health, rican heritage, rican  history, rican holiday, rican identity, rican interpreter,rican language, rican law, rican market, rican media, rican music, rican musician, rican nation, rican national, rican neighborhood, rican network, rican newspaper, rican origin, rican outreach, rican-owned, rican politics, rican population, rican practice, rican radio, rican restaurant, rican rehabilitation, rican senior, rican support, rican substance, rican translator, rican tongue, rican youth' where [Phrase] = 'puerto rican'
Print 'Added White List Items for ''puerto rican'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'psycho')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'psycho*' WHERE [Phrase] = 'psycho'
END
Print 'Updated ''psycho'' to ''psycho*'''

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'psychology, psychologist, psychologists, psychological, psychologically, psychoanalysis, psychoanalytic, psychoanalytical' where [Phrase] = 'psycho*'
Print 'Added White List Items for ''psycho*'''
END

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'queen anne, queen charlotte, queen consort, queen dowager, queen elizabeth, queenly, queen mab, queen maud lan, queen maud range, queen mary, queen mother, queen mum, queen olive, queen post, queen regent, queen regnnant, queens NY, queens new york, queens bench, queens queensberry, queensland, queens metal, queens truss' where [Phrase] = 'queen*'
Print 'Added White List Items for ''queen*'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'quoran')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'quoran','')
Print 'Added ''quoran'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'queer*')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'queer*','')
Print 'Added ''queer*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'race')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'race'
Print 'Deleted ''race'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'rape*')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'rape*','')
Print 'Added ''rape*'''
END

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'rape crisis, rape center, rape counselor, rape counselors, rape prevention, rape support, rape testing, rapeseed, rapeseeds' where [Phrase] = 'rape*'
Print 'Added White List Items for ''rape*'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'redhead*')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'redhead*','')
Print 'Added ''redhead*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'religion')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'religion'
Print 'Deleted ''religion'''
END

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'religious education, religious educator, religious educators, religious counseling, religious counselor, religious counselors, religious faith, religious instruction, religious instructor, religious instructors, religious pastor, religious pastors, religious priest, religious priests, religious professor, religious professors, religious seminary, religious teacher, religious teachers, religious teaching, religious training' where [Phrase] = 'religious'
Print 'Added White List Items for ''religious'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'remote work')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'remote work','')
Print 'Added ''remote work'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'require* fee')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'require* fee'
Print 'Deleted ''require* fee'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'requi3 fee*')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'requi3 fee*'
Print 'Deleted ''requi3 fee*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'requires fee*')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'requires fee*'
Print 'Deleted ''requires fee*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'research stud*')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'research stud*'
Print 'Deleted ''research stud*'''
END

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'scumble, scumbled, scumbling' where [Phrase] = 'scum*'
Print 'Added White List Items for ''scum*'''
END

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'sex education, sex orientation, sexual education, sexual orientation, sexivalent, sextain, sextan, sextans, sextanses, sextant, sextantal, sextants, sextarii, sextarius, sextet, sextets, sextett, sextette, sextettes, sextetts, sextile, sextiles, sextillion, sextillions, sextillionth, sextillionths, sexto, sextodecimo, sextodecimos, sextolet, sextolets, sexton, sextoness, sextonesses, sextons, sextonship, sextonships, sextos, sextuple, sextupled, sextuples, sextuplet, sextuplets, sextupicate, sextuplicated, sextuplicates, sextuplicating, sextupling, sextuply' where [Phrase] = 'sex*'
Print 'Added White List Items for ''sex*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'sexual')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'sexual'
Print 'Deleted ''sexual'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'skin')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'skin*' WHERE [Phrase] = 'skin'
Print 'Updated ''skin'' to ''skin*'''
END

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'coonskin, coonskins, coon skin, coon skins, skinner, skinners, skinning, skinner box' where [Phrase] = 'skin*'
Print 'Added White List Items for ''skin*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'skinny*')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'skinny*'
END
Print 'Deleted ''skinny*'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'small fee*')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'small fee*'
Print 'Deleted ''small fee*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'snag')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'snag*' WHERE [Phrase] = 'snag'
END
Print 'Updated ''snag'' to ''snag*'''

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'snaggle, snaggled, snaggly, snaggle tooth, snaggle-toothed, snaggled tooth, snaggled-toothed, snaggy' where [Phrase] = 'snag*'
Print 'Added White List Items for ''snag*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'snatch')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'snatch*' WHERE [Phrase] = 'snatch'
END
Print 'Updated ''snatch'' to ''snatch*'''

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'snatch block, snatches, snatchier, snatchiest, snatching, snatchy' where [Phrase] = 'snatch*'
Print 'Added White List Items for ''snatch*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'sperm donat*')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'sperm don*' WHERE [Phrase] = 'sperm donat*'
END
Print 'Updated ''sperm donat*'' to ''sperm don*'''

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'donatello, donatist, donatism, donativee, donau' where [Phrase] = 'sperm don*'
Print 'Added White List Items for ''sperm don*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'sperm donor*')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'sperm donor*'
Print 'Deleted ''sperm donor*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'steward*')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'stewardess' WHERE [Phrase] = 'steward*'
END
Print 'Updated ''steward*'' to ''stewardess'''

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'strip crop, strip cropped, strip cropping, stripe, striped, striping, striper, stripers, stripling, stripy, strip mine, strip miner, strip miners, strip mined, strip mining' where [Phrase] = 'strip*'
Print 'Added White List Items for ''strip*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'stripper*')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'stripper*'
Print 'Deleted ''stripper*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'study')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'study'
Print 'Deleted ''study'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'surrogate')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'surrogate*' WHERE [Phrase] = 'surrogate'
END
Print 'Updated ''surrogate'' to ''surrogate*'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'surrogate mom')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'surrogate mom'
Print 'Deleted ''surrogate mom'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'surrogate mother*')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'surrogate mother*'
Print 'Deleted ''surrogate mother*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'survey*')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'survey*'
Print 'Deleted ''survey*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'swim suit model')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'swim suit model'
Print 'Deleted ''swim suit model'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'swimsuit model')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'swimsuit model'
Print 'Deleted ''swimsuit model'''
END

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'tallagm, tallaged, tallaging, tallahassee, tallboy, talleyrand, tallin, tallis, tallit, tallith, talloil, tallow, tally, tallies, tallyho, tally ho, tallyman, tally man' where [Phrase] = 'tall*'
Print 'Added White List Items for ''tall*'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'talmud')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'talmud','')
Print 'Added ''talmud'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'tankh')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'tankh','')
Print 'Added ''tankh'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'teen*')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'teen*','')
Print 'Added ''teen*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'ten commandments')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'ten commandments'
Print 'Deleted ''ten commandments'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'thai')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'thai*' WHERE [Phrase] = 'thai'
END
Print 'Updated ''thai'' to ''thai*'''

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'thai-american, thai art, thai artist, thai awareness, thai background, thai bookstore, thai broadcasting, thai business, thai businesses, thai celebration, thai center, thai cooking, thai cuisine, thai cultural, thai culture, thai custom, thai discipline, thai ethncity, thai exercise, thai expression, thai food, thai flag, thai government, thai heritage, thai history, thai holiday, thai identity, thai interpreter, thai language, thai law, thai market, thai media, thai music, thai musician, thai nation, thai national, thai neighborhood, thai network, thai newspaper, thai-owned, thai origin, thai outreach, thai politics, thai population, thai practice, thai radio, thai restaurant, thai religion, thai senior, thai support, thai symbol, thai TV, thai television, thai translator, thai youth, thailand' where [Phrase] = 'thai*'
Print 'Added White List Items for ''thai*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'thin')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'thin*' WHERE [Phrase] = 'thin'
END
Print 'Updated ''thin'' to ''thin*'''

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'acanthine, airworthiness, amaranthine, anything, anythings, bathing, beclothing, berthing, berthings, bethink, bethinks, bethinking, breathiness, birthing, birthings, breathing, breathings, clothing, clothings, earthiness, earthing, earthlings, enswathing, ethinyl, ethinyls, faithing, farthing, farthings, filthiness, freethinking, freethinkings, frothing, frothings, frothiness, girthing, groupthink, gunsmithing, healthiness, inearthing, kything, lathing, lathings, lecithin, lecithins, loathing, loathings, locksmithing, methinks, misthink, misthinks, misthinking, mouthing, naething,  naethings, nonbreathing, nothing, nothings, northing, northings, noteworthiness, ornithine, ornithines, outthink, overbreathing, overthink, overthinks, overthinking, pithing, pithiness, plaything, playthings, reclothing, rethink, rethinkable, rethinker, rethinkers, rethinks, rethinking, roadworthiness, scathing, scything, seaworthiness, seething, sheathing, sheathings, silversmithing, sleuthing, smoothing, smoothings, soothing, soothingly, something, somethings, southing, stealthiness, sunbathing, sunbathings, swathing, sympathin, sympathins, teething, teethings, thinclad,  thinclads, thine, thingummy, thinned, thinning, thing, things, thingness, think, thinkable, thinkably, thinker, thinkers, thinks, thinking, thinkings, tinsmithing, tithing, tithings, toothing, trithing, trithings, trothing, trustiworthiness, unclothing, underclothing, unswathing, unthink, unthinkable, unthinkably, unthinks, unthinking, unthinkingly, unworthiness, within, withing, withins, wrathing, wreathing, writhing, wordsmithing, worthiness, worthing, xanthin, xanthine, xanthines, xanthins' where [Phrase] = 'thin*'
Print 'Added White List Items for ''thin*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'traditional*')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'traditional*'
Print 'Deleted ''traditional*'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'trans gender*')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'trans gender*','')
Print 'Added ''trans gender*'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'trans-gender*')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'trans-gender*','')
Print 'Added ''trans-gender*'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'transexual*')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'transexual*','')
Print 'Added ''transexual*'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'trans sexual*')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'trans sexual*','')
Print 'Added ''trans sexual*'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'trans-sexual*  ')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'trans-sexual*  ','')
Print 'Added ''trans-sexual*  '''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'torah')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'torah','')
Print 'Added ''torah'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'tween*')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'tween*','atween, between, betweenness')
Print 'Added ''tween*'''
END


IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'under age*')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'under age*','')
Print 'Added ''under age*'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'under-age*')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'under-age*','')
Print 'Added ''under-age*'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'underweight')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'underweight','')
Print 'Added ''underweight'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'webcam')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'webcam','')
Print 'Added ''webcam'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'webscam')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'webscam','')
Print 'Added ''webscam'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'weight*')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'weight*'
Print 'Deleted ''weight*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'white')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'white*' WHERE [Phrase] = 'white'
END
Print 'Updated ''white'' to ''white*'''

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'white admiral, white alkali, white ant, white-american, white art, white artist, white awareness, white background, whitebait, white bass, whitebeard, white beard, white birch, white blood, whitebook, white book, white bread, white broadcasting, white bush, white business, white businesses, whitecap, white castle, white cedar, white celebration, white cemetery, white center, whitechapel, white clover, white coal, white collar, whitecoller, white-coller, white community, white crappie, white cultural, white culture, white custom, white dove, white elephant, white expression, white-eye, whited, whiting, white-faced, white faith, whitefield, whitefish, white food, white fly, white fox, white friar, whitefriars, white gold, whitehall, whitehead, white heat, white heritage, white history, whitehorse, white horse,  white holiday, white house, white identity, white knight, whitelead, white lie, white leather, white lupine, white matter, white meat, white metal, white mountains, white mustard, whitener, white network, white newspaper, white nile, whitening, white noise, white oak, white origin, white out, white outreach, white-owned, white paper, whitepaper, white pepper, white perch, white pine, white plague, white plains, white politics, white poplar, white population, white potato, white potatoes, white practice, white rat, white river, white room, whiteroom, white sale, white sapphire, white sauce, white sea, white senior, white shark, whitesmith, whitesmiths, white support, white symbol, whitetail, whitetailed, whitethroat, whitethroated, white tie,  white tradition, whitewall, white wall, white walnut, white walnuts, whitewater, white whale, whitewing, whitewings, whitewinged, whitewood, whitewoods, white wood, white woods, white youth.' where [Phrase] = 'white*'
Print 'Added White List Items for ''white*'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'without pay')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'without pay','')
Print 'Added ''without pay'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'woman')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'woman*' WHERE [Phrase] = 'woman'
END
Print 'Updated ''woman'' to ''woman*'''

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'woman''s accessories, woman''s casual, woman''s clothing, woman''s department, woman''s formal, woman''s hats, woman''s jewelry, woman-owned, woman''s shoes, woman''s slacks, woman''s sports, woman''s sportsgear, woman''s sportswear, woman''s wear, womanswear, woman''s rights, woman''s colleges, woman''s schools, woman''s universities' where [Phrase] = 'woman*'
Print 'Added White List Items for ''woman*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'women')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'women*' WHERE [Phrase] = 'women'
END
Print 'Updated ''women'' to ''women*'''

BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'women''s accessories, women''s casual, women''s clothing, women''s department, women''s formal, women''s hats, women''s jewelry, women-owned, women''s shoes, women''s slacks, women''s sports, women''s sportsgear, women''s sportswear, women''s wear, womenswear, women''s rights, women''s colleges, women''s schools, women''s universities' where [Phrase] = 'women*'
Print 'Added White List Items for ''women*'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'work remote')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'work remote','')
Print 'Added ''work remote'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'worship')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'worship*' WHERE [Phrase] = 'worship'
END
Print 'Updated ''worship'' to ''worship*'''

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'youthful')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'youthful','')
Print 'Added ''youthful'''
END
