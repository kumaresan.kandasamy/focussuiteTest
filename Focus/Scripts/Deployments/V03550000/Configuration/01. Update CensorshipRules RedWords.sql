-- RED Word Censorship Rules SQL

DECLARE @Type INT
DECLARE @Level INT

Set @Type = 1
Set @Level = 3

UPDATE [Config.CensorshipRule] set WhiteList = 'aboard, abode, abolish, abolition, abolitionist, abolitionists, abolitionism, abomasum, abomb, abominable, abominate, abomination, abominations, aborning, abort, aborted, aborting, aborts, abortion, abortions, aborticide, abortifacient, abortifacients, abortionist, abortionists, abortive, ABO system, abound, about, about face, aboutface, about-face, about ship, aboutship, about-ship, above, above board, aboveboard, above-board, abovo, taboo, taboos, tabor, taboret, taborin, taborine' where [Phrase] = 'abo'
Print 'Added White List Items for ''abo'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'adult agency')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'adult agency'
Print 'Deleted ''adult agency'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'adult entertainer')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'adult entertainer'
Print 'Moved ''adult entertainer'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'adult entertainment')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'adult entertainment'
Print 'Moved ''adult entertainment'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'adult female actress')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'adult female actress'
Print 'Moved ''adult female actress'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'adult female actress')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'adult actress' WHERE [Phrase] = 'adult female actress'
END
Print 'Updated ''adult female actress'' to ''adult actress'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'adult female dancer')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'adult female dancer'
Print 'Moved ''adult female dancer'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'adult female dancer')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'adult dancer' WHERE [Phrase] = 'adult female dancer'
END
Print 'Updated ''adult female dancer'' to ''adult dancer'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'adult female dancers')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'adult female dancers'
Print 'Deleted ''adult female dancers'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'adult flic')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'adult flic'
Print 'Moved ''adult flic'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'adult flick')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'adult flick'
Print 'Moved ''adult flick'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'adult flix')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'adult flix'
Print 'Moved ''adult flix'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'adult film')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'adult film'
Print 'Moved ''adult film'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'adult male actor')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'adult male actor'
Print 'Moved ''adult male actor'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'adult male actor')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'adult actor' WHERE [Phrase] = 'adult male actor'
END
Print 'Updated ''adult male actor'' to ''adult actor'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'adult model')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'adult model'
Print 'Moved ''adult model'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'adult models')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'adult models'
Print 'Deleted ''adult models'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'adult service')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'adult service'
Print 'Deleted ''adult service'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'adult video')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'adult video'
Print 'Moved ''adult video'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'all american only')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'all american only'
Print 'Moved ''all american only'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'all-american only')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'all-american only'
Print 'Moved ''all-american only'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'amateur female')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'amateur female'
Print 'Deleted ''amateur female'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'amateur film')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'amateur film'
Print 'Deleted ''amateur film'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'amateur model')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'amateur model'
Print 'Deleted ''amateur model'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'amateur modeling')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'amateur modeling'
Print 'Deleted ''amateur modeling'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'amateur models')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'amateur models'
Print 'Deleted ''amateur models'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'amateur teen')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'amateur teen'
Print 'Deleted ''amateur teen'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'amateur video')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'amateur video'
Print 'Deleted ''amateur video'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'amateur model')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'amateur model'
Print 'Deleted ''amateur model'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'amature female')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'amature female'
Print 'Deleted ''amature female'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'amature model')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'amature model'
Print 'Deleted ''amature model'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'amature models')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'amature models'
Print 'Deleted ''amature models'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'amature teen')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'amature teen'
Print 'Deleted ''amature teen'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'american only')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'american only'
Print 'Moved ''american only'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti african')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti african'
Print 'Deleted ''anti african'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti arab')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti arab'
Print 'Deleted ''anti arab'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti black')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti black'
Print 'Deleted ''anti black'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti christian')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti christian'
Print 'Deleted ''anti christian'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti conservative')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti conservative'
Print 'Deleted ''anti conservative'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti democrat')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti democrat'
Print 'Deleted ''anti democrat'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti female')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti female'
Print 'Deleted ''anti female'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti foreign')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti foreign'
Print 'Deleted ''anti foreign'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti gay')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti gay'
Print 'Deleted ''anti gay'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti glbt')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti glbt'
Print 'Deleted ''anti glbt'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti jewish')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti jewish'
Print 'Deleted ''anti jewish'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti lgbt')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti lgbt'
Print 'Deleted ''anti lgbt'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti liberal')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti liberal'
Print 'Deleted ''anti liberal'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti male')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti male'
Print 'Deleted ''anti male'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti muslim')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti muslim'
Print 'Deleted ''anti muslim'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti indian')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti indian'
Print 'Deleted ''anti indian'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti injun')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti injun'
Print 'Deleted ''anti injun'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti republican')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti republican'
Print 'Deleted ''anti republican'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti religion')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti religion'
Print 'Deleted ''anti religion'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti religious')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti religious'
Print 'Deleted ''anti religious'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti semitic')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'anti semitic'
Print 'Moved ''anti semitic'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti tea party')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti tea party'
Print 'Deleted ''anti tea party'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti union')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti union'
Print 'Deleted ''anti union'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti veteran')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti veteran'
Print 'Deleted ''anti veteran'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti white')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti white'
Print 'Deleted ''anti white'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti wom')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti wom'
Print 'Deleted ''anti wom'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti-african')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti-african'
Print 'Deleted ''anti-african'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti-african american')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti-african american'
Print 'Deleted ''anti-african american'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti-arab')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti-arab'
Print 'Deleted ''anti-arab'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti-black')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti-black'
Print 'Deleted ''anti-black'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti-christian')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti-christian'
Print 'Deleted ''anti-christian'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti-conservative')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti-conservative'
Print 'Deleted ''anti-conservative'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti-democrat')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti-democrat'
Print 'Deleted ''anti-democrat'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti-female')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti-female'
Print 'Deleted ''anti-female'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti-foreign')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti-foreign'
Print 'Deleted ''anti-foreign'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti-gay')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti-gay'
Print 'Deleted ''anti-gay'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti-glbt')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti-glbt'
Print 'Deleted ''anti-glbt'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti-indian')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti-indian'
Print 'Deleted ''anti-indian'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti-injun')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti-injun'
Print 'Deleted ''anti-injun'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti-jewish')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti-jewish'
Print 'Deleted ''anti-jewish'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti-lgbt')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti-lgbt'
Print 'Deleted ''anti-lgbt'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti-liberal')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti-liberal'
Print 'Deleted ''anti-liberal'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti-male')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti-male'
Print 'Deleted ''anti-male'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti-muslim')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti-muslim'
Print 'Deleted ''anti-muslim'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti-native')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti-native'
Print 'Deleted ''anti-native'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti-republican')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti-republican'
Print 'Deleted ''anti-republican'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti-religion')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti-religion'
Print 'Deleted ''anti-religion'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti-religious')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti-religious'
Print 'Deleted ''anti-religious'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti-semitic')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'anti-semitic'
Print 'Moved ''anti-semitic'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti-tea party')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti-tea party'
Print 'Deleted ''anti-tea party'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti-union')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti-union'
Print 'Deleted ''anti-union'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti-veteran')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti-veteran'
Print 'Deleted ''anti-veteran'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti-white')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti-white'
Print 'Deleted ''anti-white'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti-wom')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'anti-wom'
Print 'Deleted ''anti-wom'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'arapahoe')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'arapaho' WHERE [Phrase] = 'arapahoe'
END
Print 'Updated ''arapaho'' to ''arapahoe'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'arm candy')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'arm candy'
Print 'Moved ''arm candy'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'aspiring model')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'aspiring model'
Print 'Deleted ''aspiring model'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'aspiring models')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'aspiring models'
Print 'Deleted ''aspiring models'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'attractive female')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'attractive female'
Print 'Deleted ''attractive female'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'attractive girl')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'attractive girl'
Print 'Deleted ''attractive girl'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'attractive guy')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'attractive guy'
Print 'Deleted ''attractive guy'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'attractive ladies')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'attractive ladies'
Print 'Deleted ''attractive ladies'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'attractive male')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'attractive male'
Print 'Deleted ''attractive male'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'attractive man')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'attractive man'
Print 'Deleted ''attractive man'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'attractive men')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'attractive men'
Print 'Deleted ''attractive men'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'attractive sexy')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'attractive sexy'
Print 'Deleted ''attractive sexy'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'attractive wom')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'attractive wom'
Print 'Deleted ''attractive wom'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'autoerotic*')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'autoerotic*','')
Print 'Added ''autoerotic*'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'auto erotic*')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'auto erotic*','')
Print 'Added ''auto erotic*'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'auto-erotic*')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'auto-erotic*','')
Print 'Added ''auto-erotic*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'bastard')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'bastard*' WHERE [Phrase] = 'bastard'
END
Print 'Updated ''bastard'' to ''bastard*'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'basterd')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'basterd*' WHERE [Phrase] = 'basterd'
END
Print 'Updated ''basterd'' to ''basterd*'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'beaver')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'beaver'
Print 'Moved ''beaver'' from RED to YELLOW List'
END

UPDATE [Config.CensorshipRule] set WhiteList = 'beaver board, beaver board, beaver-board, beaver-boards' where [Phrase] = 'beaver'
Print 'Added White List Items for ''beaver'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'bible thump')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'bible thumper' WHERE [Phrase] = 'bible thump'
END
Print 'Updated ''bible thump'' to ''bible thumper'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'bikini body')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'bikini body'
Print 'Moved ''bikini body'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'bikini body')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'bikini' WHERE [Phrase] = 'bikini body'
END
Print 'Updated ''bikini body'' to ''bikini'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'bikini dancer')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'bikini dancer'
Print 'Deleted ''bikini dancer'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'bikini model')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'bikini model'
Print 'Deleted ''bikini model'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'bisexual' and [Level] = @Level)
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'bisexual' and [Level] = @Level
Print 'Deleted ''bisexual'' from red-word list'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'bi-sexual' and [Level] = @Level)
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'bi-sexual' and [Level] = @Level
Print 'Deleted ''bi-sexual'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'bitch')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'bitch*' WHERE [Phrase] = 'bitch'
END
Print 'Updated ''bitch'' to ''bitch*'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'bitchslap')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'bitchslap'
Print 'Deleted ''bitchslap'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'bitch slap')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'bitch slap'
Print 'Deleted ''bitch slap'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'black* only')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'black* only'
Print 'Deleted ''black* only'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'black supremac*')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'black supremac*'
Print 'Deleted ''black supremac*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'body rub*')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'body rub*'
Print 'Moved ''body rub*'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'bodyrub*')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'bodyrub*'
Print 'Moved ''bodyrub*'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'boner')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'boner'
Print 'Moved ''boner'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'brick shit house')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'brick shit house'
Print 'Deleted ''brick shit house'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'brown skin')
BEGIN
DELETE FROM [Config.CensorshipRule] where [Phrase] = 'brown skin'
Print 'Deleted ''brown skin'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'brown-skinned')
BEGIN
DELETE FROM [Config.CensorshipRule] where [Phrase] = 'brown-skinned'
Print 'Deleted ''brown-skinned'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'brown shirt')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'brown shirt'
Print 'Moved ''brown shirt'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'brownshirt')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'brownshirt'
Print 'Moved ''brownshirt'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'bugger')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'bugger*' WHERE [Phrase] = 'bugger'
END
Print 'Updated ''bugger'' to ''bugger*'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'bung')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'bung*' WHERE [Phrase] = 'bung'
END
Print 'Updated ''bung'' to ''bung*'''

UPDATE [Config.CensorshipRule] set WhiteList = 'bungalow, bungalows, bungle, bungled, bungling, bunglingly, bungles' where [Phrase] = 'bung*'
Print 'Added White List Items for ''bung*'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'bunghole')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'bunghole'
Print 'Deleted ''bunghole'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'bung hole')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'bung hole'
Print 'Deleted ''bung hole'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'bush nigger')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'bush nigger'
Print 'Deleted ''bush nigger'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'butts')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'butts'
Print 'Deleted ''butts'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'camel jock')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'camel jock*' WHERE [Phrase] = 'camel jock'
END
Print 'Updated ''camel jock'' to ''camel jock*'''

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'camel toe')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'camel toe','')
Print 'Added ''camel toe'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'cameltoe')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'cameltoe','')
Print 'Added ''cameltoe'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'chink')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'chink'
Print 'Moved ''chink'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'chink')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'chink*' WHERE [Phrase] = 'chink'
END
Print 'Updated ''chink'' to ''chink*'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'chink*')
BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'chinkapin, chinqupin, chinkiang' where [Phrase] = 'chink*'
END
Print 'Added White List Items for ''chink*'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'Christian extremist')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'Christian extremist'
Print 'Deleted ''Christian extremist'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'Christian terrorist')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'Christian terrorist'
Print 'Deleted ''Christian terrorist'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'cialis')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'cialis'
Print 'Moved ''cialis'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'cock')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'cock*' WHERE [Phrase] = 'cock'
END
Print 'Updated ''cock'' to ''cock*'''

UPDATE [Config.CensorshipRule] set WhiteList = 'cockade, cockades, cock-a-dooddle-do, cock-a-hoop, cockaigne, cock-a-leekie, cockamamie, cockalorum, cock and bull, cock-and-bull, cockateel, cockateels, cockatiel, cockatiels, cockatoo, cockatoos, cockatrice, cockboat, cockchafer, cockcroft, cock crow, cock crowed, cock crows, cock crowing, cockcrow, cockcrowed, cockcrows, cockcrowing, cocked, cocked hat, cocker, cockers, cocker spaniel, cockeye, cockeyed, cock horse, cockhorse, cockily, cockiness, cockle, cockles, cocklebur, cockleburs, cockleburr, cockleburrs, cockleshell, cockleshells, cockle shell, cockle shells, cockloft, cock of the walk, cock-of-the-walk, cockpit, cockpits, cockroach, cockroaches, cockscomb, cockshut, cockshy, cockspur, cockspurs, cock sure, cocksure, cockswain, cocktail, cocktails, cocky, glascock, glasscock, hancock, handcock, hitchcock, whitcock' where [Phrase] = 'cock*'
Print 'Added White List Items for ''cock*'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'cocks')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'cocks'
Print 'Deleted ''cocks'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'comi')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'comi'
Print 'Moved ''comi'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'commie')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'commie'
Print 'Moved ''commie'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'coon')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'coon*' WHERE [Phrase] = 'coon'
END
Print 'Updated ''coon'' to ''coon*'''

UPDATE [Config.CensorshipRule] set WhiteList = 'cooncan, coon dog, coon dogs, coondog, coondogs, coon hound, coon hounds, coonhound, coonhounds, coonskin, coonskins, coon skin, coon skins, contie, raccoon, raccoons' where [Phrase] = 'coon*'
Print 'Added White List Items for ''coon*'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'coons')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'coons'
Print 'Deleted ''coons'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'crack head')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'crack head*' WHERE [Phrase] = 'crack head'
END
Print 'Updated ''crack head'' to ''crack head*'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'crackhead')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'crackhead*' WHERE [Phrase] = 'crackhead'
END
Print 'Updated ''crackhead'' to ''crackhead*'''

UPDATE [Config.CensorshipRule] set WhiteList = 'crap shoot, crap shooting, crap shooter' where [Phrase] = 'crap'
Print 'Added White List Items for ''crap'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'cum')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'cum*' WHERE [Phrase] = 'cum'
END
Print 'Updated ''cum'' to ''cum*'''

UPDATE [Config.CensorshipRule] set WhiteList = 'accumulate, accumulated, accumulates, accumulative, accumulatively, encumber, encumbered, encumbers, cumber, cumberland, cumbersome, cumbrance, cumbria, cumbrous, cumgranosalis, cumin, cum laude, cumlaude, cummerbund, cummings, cumquat, cumshaw, cumulate, cumulated, cumulating, cumulation, cumulative, cumulatively, cumliform, cumulonimbus, cumulous, cumuli, cumulus' where [Phrase] = 'cum*'
Print 'Added White List Items for ''cum*'''

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'cunnilingus')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'cunnilingus','')
Print 'Added ''cunnilingus'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'cunt')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'cunt*' WHERE [Phrase] = 'cunt'
END
Print 'Updated ''cunt'' to ''cunt*'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'cunts')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'cunts'
Print 'Deleted ''cunts'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'cute boy')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'cute boy'
Print 'Deleted ''cute boy'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'cute female')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'cute female'
Print 'Deleted ''cute female'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'cute gal')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'cute gal'
Print 'Deleted ''cute gal'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'cute girl')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'cute girl'
Print 'Deleted ''cute girl'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'cute guy')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'cute guy'
Print 'Deleted ''cute guy'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'cute ladies')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'cute ladies'
Print 'Deleted ''cute ladies'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'cute lady')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'cute lady'
Print 'Deleted ''cute lady'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'cute man')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'cute man'
Print 'Deleted ''cute man'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'cute men')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'cute men'
Print 'Deleted ''cute men'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'cute woman')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'cute woman'
Print 'Deleted ''cute woman'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'cute women')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'cute women'
Print 'Deleted ''cute women'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'dago')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'dago*' WHERE [Phrase] = 'dago'
END
Print 'Updated ''dago'' to ''dago*'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'damn')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'damn*' WHERE [Phrase] = 'damn'
END
Print 'Updated ''damn'' to ''damn*'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'dancer bikini')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'dancer bikini'
Print 'Deleted ''dancer bikini'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'dancerbikini')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'dancerbikini'
Print 'Deleted ''dancerbikini'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'dancers bikini')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'dancers bikini'
Print 'Deleted ''dancers bikini'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'dancersbikini')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'dancersbikini'
Print 'Deleted ''dancersbikini'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'dark skin*')
BEGIN
DELETE FROM [Config.CensorshipRule] where [Phrase] = 'dark skin*'
Print 'Deleted ''dark skin*'''
END


IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'dark-skinned')
BEGIN
DELETE FROM [Config.CensorshipRule] where [Phrase] = 'dark-skinned'
Print 'Deleted ''dark-skinned'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'dildo')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'dildo*' WHERE [Phrase] = 'dildo'
END
Print 'Updated ''dildo'' to ''dildo*'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'dildos')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'dildos'
Print 'Deleted ''dildos'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'dingo' and level = @Level)
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'dingo' and level = @Level
Print 'Deleted ''dingo'' from red list'
END

UPDATE [Config.CensorshipRule] set WhiteList = 'australian dingo, dingo breed, dingo dog, dingo fence, dingo conservation, dingo hunting, dingo preservation, dingo poaching, dingo species, dingo trapping, dingo trapper' where [Phrase] = 'dingo'
Print 'Added White List Items for ''dingo'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'dirty brown animal')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'dirty brown animal'
Print 'Deleted ''dirty brown animal'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'dirty injun')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'dirty injun'
Print 'Deleted ''dirty injun'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'dirty nigger')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'dirty nigger'
Print 'Deleted ''dirty nigger'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'dirty wetback')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'dirty wetback'
Print 'Deleted ''dirty wetback'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'domestic terrorist')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'domestic terrorist'
Print 'Deleted ''domestic terrorist'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'dyke')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'dyke'
Print 'Moved ''dyke'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'ebony female')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'ebony female'
Print 'Deleted ''ebony female'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'ebony girl')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'ebony girl'
Print 'Deleted ''ebony girl'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'ebony ladies')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'ebony ladies'
Print 'Deleted ''ebony ladies'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'ebony lady')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'ebony lady'
Print 'Deleted ''ebony lady'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'ebony woman')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'ebony woman'
Print 'Deleted ''ebony woman'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'ebony women')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'ebony women'
Print 'Deleted ''ebony women'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'english only')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'english only'
Print 'Moved ''english only'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'erotic')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'erotic'
Print 'Moved ''erotic'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'escort agency')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'escort agency'
Print 'Moved ''escort agency'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'escort service')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'escort service'
Print 'Moved ''escort service'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'exotic amateur')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'exotic amateur'
Print 'Deleted ''exotic amateur'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'exotic amature')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'exotic amature'
Print 'Deleted ''exotic amature'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'exotic boy')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'exotic boy'
Print 'Deleted ''exotic boy'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'exotic dance')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'exotic dance'
Print 'Deleted ''exotic dance'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'exotic female')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'exotic female'
Print 'Deleted ''exotic female'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'exotic girl')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'exotic girl'
Print 'Deleted ''exotic girl'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'exotic guy')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'exotic guy'
Print 'Deleted ''exotic guy'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'exotic ladies')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'exotic ladies'
Print 'Deleted ''exotic ladies'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'exotic lady')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'exotic lady'
Print 'Deleted ''exotic lady'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'exotic look')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'exotic look'
Print 'Deleted ''exotic look'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'exotic male')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'exotic male'
Print 'Deleted ''exotic male'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'exotic man')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'exotic man'
Print 'Deleted ''exotic man'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'exotic men')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'exotic men'
Print 'Deleted ''exotic men'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'exotic model')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'exotic model'
Print 'Deleted ''exotic model'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'exotic naked')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'exotic naked'
Print 'Deleted ''exotic naked'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'exotic nude')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'exotic nude'
Print 'Deleted ''exotic nude'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'exotic stripper')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'exotic stripper'
Print 'Deleted ''exotic stripper'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'exotic web cam')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'exotic web cam'
Print 'Deleted ''exotic web cam'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'exotic webcam')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'exotic webcam'
Print 'Deleted ''exotic webcam'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'exotic woman')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'exotic woman'
Print 'Deleted ''exotic woman'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'eye candy')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'eye candy'
Print 'Moved ''eye candy'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'fag')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'fag*' WHERE [Phrase] = 'fag'
END
Print 'Updated ''fag'' to ''fag*'''

UPDATE [Config.CensorshipRule] set WhiteList = 'fagend, fagot' where [Phrase] = 'fag*'
Print 'Added White List Items for ''fag*'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'faggot')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'faggot'
Print 'Deleted ''faggot'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'faggots')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'faggots'
Print 'Deleted ''faggots'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'fairies')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'fairies'
Print 'Moved ''fairies'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'fair skin')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'fair skin'
END
Print 'Deleted ''fair skin'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'fair-skinned')
BEGIN
DELETE FROM [Config.CensorshipRule] where [Phrase] = 'fair-skinned'
Print 'Deleted ''fair-skinned'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'fascist')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'fascist'
Print 'Moved ''fascist'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'fascist')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'fascis*' WHERE [Phrase] = 'fascist'
END
Print 'Updated ''fascist'' to ''fascis*'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'fatass')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'fatass'
Print 'Deleted ''fatass'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'fat ass')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'fat ass'
Print 'Deleted ''fat ass'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'female adult act')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'female adult act'
Print 'Deleted ''female adult act'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'female dancer')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'female dancer'
Print 'Deleted ''female dancer'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'female escort')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'female escort'
Print 'Deleted ''female escort'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'female model')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'female model'
Print 'Deleted ''female model'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'female* on webcam')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'female* on webcam'
Print 'Deleted ''female* on webcam'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'female* only')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'female* only'
Print 'Deleted ''female* only'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'female stripper')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'female stripper'
Print 'Deleted ''female stripper'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'female* to model')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'female* to model'
Print 'Deleted ''female* to model'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'female* to escort')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'female* to escort'
Print 'Deleted ''female* to escort'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'fellatio')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'fellatio','')
Print 'Added ''fellatio'''
END

IF EXISTS (
	SELECT Phrase, count(1) as 'count' FROM [Config.CensorshipRule]
	WHERE [Phrase] = 'g spot'
	GROUP BY Phrase
	HAVING count(1) > 1)
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'g spot' and [ID] <> (SELECT TOP 1 [ID] FROM [Config.CensorshipRule] WHERE [Phrase] = 'g spot' order by id asc)
Print 'Deleted duplicate ''g spot'' records'
END

IF EXISTS (
	SELECT Phrase, count(1) as 'count' FROM [Config.CensorshipRule]
	WHERE [Phrase] = 'g-spot'
	GROUP BY Phrase
	HAVING count(1) > 1)
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'g-spot' and [ID] <> (SELECT TOP 1 [ID] FROM [Config.CensorshipRule] WHERE [Phrase] = 'g-spot' order by id asc)
Print 'Deleted duplicate ''g-spot'' records'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'georgia cracker')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'georgia cracker'
Print 'Deleted ''georgia cracker'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'girl model')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'girl model'
Print 'Deleted ''girl model'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'girl* on webcam')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'girl* on webcam'
Print 'Deleted ''girl* on webcam'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'god')
BEGIN
UPDATE [Config.CensorshipRule] set WhiteList = 'god''s creatures, god''s grace, god''s graces, god''s kitchen, god''s pantry, godspeed' where [Phrase] = 'god'
Print 'Added White List Items for ''god'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'godammit')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'godammit'
Print 'Deleted ''godammit'''
END


IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'goddamn')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'goddam*' WHERE [Phrase] = 'goddamn'
END
Print 'Updated ''goddamn'' to ''goddam*'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'god damn')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'god damn'
Print 'Deleted ''god damn'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'god damn it')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'god damn it'
Print 'Deleted ''god damn it'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'gold digger')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'gold digger'
Print 'Moved ''gold digger'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'gold-digger')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'gold-digger'
Print 'Moved ''gold-digger'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'go go dancer')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'go go dancer'
Print 'Deleted ''go go dancer'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'go-go girl')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'go-go girl'
Print 'Deleted ''go-go girl'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'guy model')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'guy model'
Print 'Deleted ''guy model'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'guy* on webcam')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'guy* on webcam'
Print 'Deleted ''guy* on webcam'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'hard body')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'hard body'
Print 'Moved ''hard body'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'haulis')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'haulis'
Print 'Deleted ''haulis'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'homo' and level = @Level)
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'homo' and level = @Level
Print 'Deleted ''homo'' from red list'
END

UPDATE [Config.CensorshipRule] set WhiteList = 'homo sapien' where [Phrase] = 'homo'
Print 'Added White List Items for ''homo'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'honest injun')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'honest injun'
Print 'Deleted ''honest injun'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'hot and nasty')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'hot and nasty'
Print 'Deleted ''hot and nasty'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'hot and sexy')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'hot and sexy'
Print 'Deleted ''hot and sexy'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'hotbod')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'hotbod'
Print 'Deleted ''hotbod'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'hot bod')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'hot bod'
Print 'Moved ''hot bod'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'hot bod')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'hot body' WHERE [Phrase] = 'hot bod'
END
Print 'Updated ''hot bod'' to ''hot body'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'hot boy')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'hot boy'
Print 'Deleted ''hot boy'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'hot fag')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'hot fag'
Print 'Deleted ''hot fag'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'hot female')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'hot female'
Print 'Deleted ''hot female'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'hot gal')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'hot gal'
Print 'Deleted ''hot gal'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'hot gay')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'hot gay'
Print 'Deleted ''hot gay'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'hot glbt')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'hot glbt'
Print 'Deleted ''hot glbt'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'hot girl')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'hot girl'
Print 'Deleted ''hot girl'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'hot guy')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'hot guy'
Print 'Deleted ''hot guy'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'hot lady')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'hot lady'
Print 'Deleted ''hot lady'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'hot ladies')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'hot ladies'
Print 'Deleted ''hot ladies'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'hot lgbt')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'hot lgbt'
Print 'Deleted ''hot lgbt'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'hot male')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'hot male'
Print 'Deleted ''hot male'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'hot man')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'hot man'
Print 'Deleted ''hot man'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'hot men')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'hot men'
Print 'Deleted ''hot men'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'hot model')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'hot model'
Print 'Deleted ''hot model'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'hot sex')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'hot sex'
Print 'Deleted ''hot sex'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'hot teen')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'hot teen'
Print 'Deleted ''hot teen'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'hot woman')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'hot woman'
Print 'Deleted ''hot woman'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'hot women')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'hot women'
Print 'Deleted ''hot women'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'islamic extremist')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'islamic extremist'
Print 'Deleted ''islamic extremist'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'islamic terrorist')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'islamic terrorist'
Print 'Deleted ''islamic terrorist'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'jap')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'jap'
Print 'Deleted ''jap'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'jewish extremist')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'jewish extremist'
Print 'Deleted ''jewish extremist'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'jewish terrorist')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'jewish terrorist'
Print 'Deleted ''jewish terrorist'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'jigg')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'jigg'
Print 'Deleted ''jigg'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'jungle bunny')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'jungle bunn*' WHERE [Phrase] = 'jungle bunny'
END
Print 'Updated ''jungle bunny'' to ''jungle bunn*'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'jungle bunnies')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'jungle bunnies'
Print 'Deleted ''jungle bunnies'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'lad* on webcam')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'lad* on webcam'
Print 'Deleted ''lad* on webcam'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'left wing crackpot')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'left wing crackpot'
Print 'Moved ''left wing crackpot'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'left wing crackpot')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'left wing' WHERE [Phrase] = 'left wing crackpot'
END
Print 'Updated ''left wing crackpot'' to ''left wing'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'left wing craz')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'left wing craz'
Print 'Deleted ''left wing craz'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'left wing nut')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'left wing nut'
Print 'Deleted ''left wing nut'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'left wing wack')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'left wing wack'
Print 'Deleted ''left wing wack'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'left-wing crackpot')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'left-wing crackpot'
Print 'Moved ''left-wing crackpot'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'left-wing crackpot')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'left-wing' WHERE [Phrase] = 'left-wing crackpot'
END
Print 'Updated ''left-wing crackpot'' to ''left-wing'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'left-wing crazy')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'left-wing crazy'
Print 'Deleted ''left-wing crazy'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'left-wing nut')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'left-wing nut'
Print 'Deleted ''left-wing nut'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'left-wing wack')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'left-wing wack'
Print 'Deleted ''left-wing wack'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'levitra')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'levitra'
Print 'Moved ''levitra'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'light skin')
BEGIN
DELETE FROM [Config.CensorshipRule] where [Phrase] = 'light skin'
Print 'Deleted ''light skin'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'light-skin')
BEGIN
DELETE FROM [Config.CensorshipRule] where [Phrase] = 'light-skin'
Print 'Deleted ''light-skin'''
END


IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'lingerie model*')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'lingerie model*'
Print 'Deleted ''lingerie model*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'lolita')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'lolita'
Print 'Moved ''lolita'' from RED to YELLOW List'
END

UPDATE [Config.CensorshipRule] set WhiteList = 'Lolita TX, Lolita Texas' where [Phrase] = 'lolita'
Print 'Added White List Items for ''lolita'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'look* exotic')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'look* exotic'
Print 'Deleted ''look* exotic'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'male adult act')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'male adult act'
Print 'Deleted ''male adult act'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'male dancer')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'male dancer'
Print 'Deleted ''male dancer'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'male enhancement')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'male enhancement'
Print 'Deleted ''male enhancement'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'male enlargement')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'male enlargement'
Print 'Deleted ''male enlargement'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'male escort')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'male escort'
Print 'Deleted ''male escort'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'male model')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'male model'
Print 'Deleted ''male model'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'male* on webcam')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'male* on webcam'
Print 'Deleted ''male* on webcam'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'male* only')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'male* only'
Print 'Deleted ''male* only'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'male photoshoot model')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'male photoshoot model'
Print 'Deleted ''male photoshoot model'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'male stripper')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'male stripper'
Print 'Deleted ''male stripper'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'male* to model')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'male* to model'
Print 'Deleted ''male* to model'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'male* for strip')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'male* for strip'
Print 'Deleted ''male* for strip'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'male* modeling')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'male* modeling'
Print 'Deleted ''male* modeling'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'male* to escort')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'male* to escort'
Print 'Deleted ''male* to escort'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'male* to model')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'male* to model'
Print 'Deleted ''male* to model'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'male* to strip')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'male* to strip'
Print 'Deleted ''male* to strip'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'man on webcam')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'man on webcam'
Print 'Deleted ''man on webcam'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'man* job')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'man* job'
Print 'Deleted ''man* job'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'man* work')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'man* work'
Print 'Deleted ''man* work'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'men are encouraged to apply')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'men are encouraged to apply'
Print 'Moved ''men are encouraged to apply'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'men on webcam')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'men on webcam'
Print 'Deleted ''men on webcam'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'men only')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'men only'
Print 'Moved ''men only'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'men should not apply')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'men should not apply'
Print 'Moved ''men should not apply'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'men''s job')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'men''s job'
Print 'Moved ''men''s job'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'men''s work')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'men''s work'
Print 'Moved ''men''s work'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'mental case')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'mental case'
Print 'Moved ''mental case'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'mental midget')
BEGIN
DELETE FROM [Config.CensorshipRule] where [Phrase] = 'mental midget'
Print 'Deleted ''mental midget'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'mental retard')
BEGIN
DELETE FROM [Config.CensorshipRule] where [Phrase] = 'mental retard'
Print 'Deleted ''mental retard'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'mexican pretender')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'mexican pretender'
Print 'Deleted ''mexican pretender'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'mistress')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'mistress'
Print 'Moved ''mistress'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'model your own bikini')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'model your own bikini'
Print 'Deleted ''model your own bikini'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'model amateur')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'model amateur'
Print 'Deleted ''model amateur'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'model bikini')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'model bikini'
Print 'Deleted ''model bikini'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'model dancer')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'model dancer'
Print 'Deleted ''model dancer'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'model* lingerie')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'model* lingerie'
Print 'Deleted ''model* lingerie'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'model* swimsuit')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'model* swimsuit'
Print 'Deleted ''model* swimsuit'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'model your bikini')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'model your bikini'
Print 'Deleted ''model your bikini'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'model your lingerie')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'model your lingerie'
Print 'Deleted ''model your lingerie'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'model your own lingerie')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'model your own lingerie'
Print 'Deleted ''model your own lingerie'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'models amateur')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'models amateur'
Print 'Deleted ''models amateur'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'models bikini')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'models bikini'
Print 'Deleted ''models bikini'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'models lingerie')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'models lingerie'
Print 'Deleted ''models lingerie'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'mulatto')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'mulatto'
Print 'Moved ''mulatto'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'must speak english')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'must speak english'
Print 'Moved ''must speak english'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'mut')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'mut*' WHERE [Phrase] = 'mut'
END
Print 'Updated ''mut'' to ''mut*'''

UPDATE [Config.CensorshipRule] set WhiteList = 'mutable, mutability, mutableness, mutably, mutagen, mutagenic, mutagenically, mutagenesis, mutant, mutants, mutate, mutated, mutates, mutating, mutation, mutational, mutationally, mutatismutandis, mutative, mutchkin, mute, muted, mutes, muting, mutely, muteness, muticous, mutilate, mutilates, mutilated, mutilating, mutilator, mutilators, mutilative, mutilation, mutilization, mutineer, mutineers, mutinous, mutinously, mutinousness, mutiny, mutinies, mutinied, mutinying, mutisum, mutt, mutts, mutter, mutters, muttered, muttering, mutterer, mutton, mutton chop, mutton chops, muttonhead, muttonheads, muttonheaded, muttoneheadedness, mutual, mutally, mutuality, mutualities, mutualism, mutualize, mutualizes, mutualized, mutualizing, mutualization, mutualizations, mutual fund, mutual funds, mutual savings, mutuel, paramutuel' where [Phrase] = 'mut*'
Print 'Added White List Items for ''mut*'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'naked')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'naked'
Print 'Moved ''naked'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'new model')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'new model'
Print 'Deleted ''new model'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'nigger')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'nigger*' WHERE [Phrase] = 'nigger'
END
Print 'Updated ''nigger'' to ''nigger*'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'no americans')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'no americans'
Print 'Deleted ''no americans'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'no arabs')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'no arabs'
Print 'Deleted ''no arabs'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'no asians')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'no asians'
Print 'Deleted ''no asians'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'no blacks')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'no blacks'
Print 'Deleted ''no blacks'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'no boys')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'no boys'
Print 'Deleted ''no boys'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'no christians')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'no christians'
Print 'Deleted ''no christians'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'no conservatives')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'no conservatives'
Print 'Deleted ''no conservatives'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'no females')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'no females'
Print 'Deleted ''no females'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'no foreigners')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'no foreigners'
Print 'Deleted ''no foreigners'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'no gays')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'no gays'
Print 'Deleted ''no gays'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'no glbts')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'no glbts'
Print 'Deleted ''no glbts'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'no green card')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'no green card'
Print 'Deleted ''no green card'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'no indians')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'no indians'
Print 'Deleted ''no indians'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'no injuns')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'no injuns'
Print 'Deleted ''no injuns'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'no irish')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'no irish'
Print 'Deleted ''no irish'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'no jews')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'no jews'
Print 'Deleted ''no jews'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'no lgbt')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'no lgbt'
Print 'Deleted ''no lgbt'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'no liberals')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'no liberals'
Print 'Deleted ''no liberals'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'no males')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'no males'
Print 'Deleted ''no males'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'no men')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'no men'
Print 'Deleted ''no men'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'no mexicans')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'no mexicans'
Print 'Deleted ''no mexicans'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'no muslims')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'no muslims'
Print 'Deleted ''no muslims'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'no niggers')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'no niggers'
Print 'Deleted ''no niggers'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'no women')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'no women'
Print 'Deleted ''no women'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'no veterans')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'no veterans'
Print 'Deleted ''no veterans'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'nude')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'nude'
Print 'Moved ''nude'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'nude boy')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'nude boy'
Print 'Deleted ''nude boy'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'nude dancer')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'nude dancer'
Print 'Deleted ''nude dancer'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'nude female')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'nude female'
Print 'Deleted ''nude female'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'nude girl')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'nude girl'
Print 'Deleted ''nude girl'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'nude guy')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'nude guy'
Print 'Deleted ''nude guy'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'nude ladies')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'nude ladies'
Print 'Deleted ''nude ladies'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'nude lady')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'nude lady'
Print 'Deleted ''nude lady'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'nude male')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'nude male'
Print 'Deleted ''nude male'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'nude man')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'nude man'
Print 'Deleted ''nude man'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'nude men')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'nude men'
Print 'Deleted ''nude men'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'nude photo')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'nude photo'
Print 'Deleted ''nude photo'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'nude pic')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'nude pic'
Print 'Deleted ''nude pic'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'nude pix')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'nude pix'
Print 'Deleted ''nude pix'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'nude woman')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'nude woman'
Print 'Deleted ''nude woman'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'old queen')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'old queen'
Print 'Deleted ''old queen'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'olive skin')
BEGIN
DELETE FROM [Config.CensorshipRule] where [Phrase] = 'olive skin'
Print 'Deleted ''olive skin'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'olive-skinned')
BEGIN
DELETE FROM [Config.CensorshipRule] where [Phrase] = 'olive-skinned'
Print 'Deleted ''olive-skinned'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'only female applicants')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'only female applicants'
Print 'Moved ''only female applicants'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'only male applicants')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'only male applicants'
Print 'Moved ''only male applicants'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'only men need apply')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'only men need apply'
Print 'Moved ''only men need apply'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'only women need apply')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'only women need apply'
Print 'Moved ''only women need apply'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'only men should apply')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'only men should apply'
Print 'Moved ''only men should apply'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'only women should apply')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'only women should apply'
Print 'Moved ''only women should apply'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'pecker')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'pecker'
Print 'Moved ''pecker'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'penis')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'penis'
Print 'Moved ''penis'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'penis')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'penis*' WHERE [Phrase] = 'penis'
END
Print 'Updated ''penis'' to ''penis*'''


IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'penis enhancement')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'penis enhancement'
Print 'Deleted ''penis enhancement'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'penis enlargement')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'penis enlargement'
Print 'Deleted ''penis enlargement'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'phone girl')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'phone girl'
Print 'Deleted ''phone girl'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'phone sex')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'phone sex'
Print 'Deleted ''phone sex'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'photo naked')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'photo naked'
Print 'Deleted ''photo naked'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'photo nude')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'photo nude'
Print 'Deleted ''photo nude'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'picture naked')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'picture naked'
Print 'Deleted ''picture naked'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'picture nude')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'picture nude'
Print 'Deleted ''picture nude'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'pis')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'pis*' WHERE [Phrase] = 'pis'
END
Print 'Updated ''pis'' to ''pis*'''

UPDATE [Config.CensorshipRule] set WhiteList = 'pisa, pisaller, pisano, piscary, piscatology, piscator, piscatorial, piscatory, piscatorially, pisces, pisci, pisciculture, piscina, piscine, piscis austrinus, piscivoroua, pisco, pisgah, pish, pisidia, pisiform, pisistratus, pismire, pismoclam, pisolite, pissarro, pissoir, pistachio, pistachios, pistrareen, piste, pistil, pistillate, pistoia, pistol, pistols, pistoled, pistolled, pistolling, pistole,pistoleer  pistol whip, piston, pistons, piston ring, piston rings, piston rod, piston rods' where [Phrase] = 'pis*'
Print 'Added White List Items for ''pis*'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'piss')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'piss'
Print 'Deleted ''piss'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'plantation nigger')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'plantation nigger'
Print 'Deleted ''plantation nigger'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'play boy')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'play boy'
Print 'Deleted ''play boy'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'play girl')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'play girl'
Print 'Deleted ''play girl'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'pole dancer')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'pole dancer'
Print 'Deleted ''pole dancer'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'poon')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'poon*' WHERE [Phrase] = 'poon'
END
Print 'Updated ''poon'' to ''poon*'''

UPDATE [Config.CensorshipRule] set WhiteList = 'poona' where [Phrase] = 'poon*'
Print 'Added White List Items for ''poon*'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'poontang')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'poontang'
Print 'Deleted ''poontang'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'poon tang')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'poon tang'
Print 'Deleted ''poon tang'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'poonk')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'poonk'
Print 'Deleted ''poonk'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'porch monkey')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'porch monk*' WHERE [Phrase] = 'porch monkey'
END
Print 'Updated ''porch monkey'' to ''porch monk*'''

UPDATE [Config.CensorshipRule] set WhiteList = 'monkey bars, monkey bread, monkey flower, monkey jacket, monkey pot, monkey puzzle, monkey shine, monkey suit, monkey wrench, monkey wrenches' where [Phrase] = 'porch monk*'
Print 'Added White List Items for ''porch monk*'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'porn')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'porno*' WHERE [Phrase] = 'porn'
END
Print 'Updated ''porn'' to ''porno*'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'prairie nigger')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'prairie nigger'
Print 'Deleted ''prairie nigger'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'prefer females')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'prefer females'
Print 'Moved ''prefer females'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'prefer males')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'prefer males'
Print 'Moved ''prefer males'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'prefer men')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'prefer men'
Print 'Moved ''prefer men'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'prefer women')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'prefer women'
Print 'Moved ''prefer women'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'pretty girls')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'pretty girls'
Print 'Deleted ''pretty girls'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'promo boy')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'promo boy'
Print 'Deleted ''promo boy'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'promo girl')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'promo girl'
Print 'Deleted ''promo girl'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'promo model')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'promo model'
Print 'Deleted ''promo model'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'promotion boy')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'promotion boy'
Print 'Deleted ''promotion boy'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'promotion girl')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'promotion girl'
Print 'Deleted ''promotion girl'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'promotion model')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'promotion model'
Print 'Deleted ''promotion model'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'promotional boy')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'promotional boy'
Print 'Deleted ''promotional boy'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'promotional girl')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'promotional girl'
Print 'Deleted ''promotional girl'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'promotional model')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'promotional model'
Print 'Deleted ''promotional model'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'prostitute')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'prostitut*' WHERE [Phrase] = 'prostitute'
END
Print 'Updated ''prostitute'' to ''prostitut*'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'prostitution')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'prostitution'
Print 'Deleted ''prostitution'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'psycho' and level = @Level)
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'psycho' and level = @Level
Print 'Deleted ''psycho'' from red list'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'pussy')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'puss*' WHERE [Phrase] = 'pussy'
END
Print 'Updated ''pussy'' to ''puss*'''

UPDATE [Config.CensorshipRule] set WhiteList = 'pussley, pussly, pussycat, pussycats, pussy cat, pussy cats, pussyfoot, pussy foot, pussyfooter, pussy footers, pussywillow, pusswillows, pussy willow, pussy willows' where [Phrase] = 'puss*'
Print 'Added White List Items for ''puss*'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'pussies')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'pussies'
Print 'Deleted ''pussies'''
END

IF EXISTS (
	SELECT Phrase, count(1) as 'count' FROM [Config.CensorshipRule]
	WHERE [Phrase] = 'putch'
	GROUP BY Phrase
	HAVING count(1) > 1)
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'putch' and [ID] <> (SELECT TOP 1 [ID] FROM [Config.CensorshipRule] WHERE [Phrase] = 'putch' order by id asc)
Print 'Deleted duplicate ''putch'' records'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'radical christian')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'radical christian'
Print 'Deleted ''radical christian'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'radical christian extremist')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'radical christian extremist'
Print 'Deleted ''radical christian extremist'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'radical christian terroristt')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'radical christian terroristt'
Print 'Deleted ''radical christian terroristt'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'radical extremist')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'radical extremist'
Print 'Deleted ''radical extremist'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'radical islamic extremist')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'radical islamic extremist'
Print 'Deleted ''radical islamic extremist'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'radical islamic terrorist')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'radical islamic terrorist'
Print 'Deleted ''radical islamic terrorist'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'radical left')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'radical left'
Print 'Deleted ''radical left'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'radical left wing')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'radical left wing'
Print 'Deleted ''radical left wing'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'radical left-wing extremist')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'radical left-wing extremist'
Print 'Deleted ''radical left-wing extremist'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'radical right')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'radical right'
Print 'Deleted ''radical right'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'radical right wing')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'radical right wing'
Print 'Deleted ''radical right wing'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'radical right-wing extremist')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'radical right-wing extremist'
Print 'Deleted ''radical right-wing extremist'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'radical terrorist')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'radical terrorist'
Print 'Deleted ''radical terrorist'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'red blooded american')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'red blooded american'
Print 'Deleted ''red blooded american'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'red-blooded american')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'red-blooded american'
Print 'Deleted ''red-blooded american'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'red dawg democrat')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'red dawg democrat'
Print 'Deleted ''red dawg democrat'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'red dog democrat')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'red dog democrat'
Print 'Deleted ''red dog democrat'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'redskin')
BEGIN
DELETE FROM [Config.CensorshipRule] where [Phrase] = 'redskin'
Print 'Deleted ''redskin'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'red skin')
BEGIN
DELETE FROM [Config.CensorshipRule] where [Phrase] = 'red skin'
Print 'Deleted ''red skin'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'red-skin')
BEGIN
DELETE FROM [Config.CensorshipRule] where [Phrase] = 'red-skin'
Print 'Deleted ''red-skin'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'retard')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'retard'
Print 'Moved ''retard'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'retard')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'retard*' WHERE [Phrase] = 'retard'
END
Print 'Updated ''retard'' to ''retard*'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'rez nigger')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'rez nigger'
Print 'Deleted ''rez nigger'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'right wing crackpot')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'right wing crackpot'
Print 'Moved ''right wing crackpot'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'right wing crackpot')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'right wing' WHERE [Phrase] = 'right wing crackpot'
END
Print 'Updated ''right wing crackpot'' to ''right wing'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'right wing crazy')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'right wing crazy'
Print 'Deleted ''right wing crazy'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'right wing nut')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'right wing nut'
Print 'Deleted ''right wing nut'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'right wing wack')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'right wing wack'
Print 'Deleted ''right wing wack'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'right-wing crackpot')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'right-wing crackpot'
Print 'Moved ''right-wing crackpot'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'right-wing crackpot')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'right-wing' WHERE [Phrase] = 'right-wing crackpot'
END
Print 'Updated ''right-wing crackpot'' to ''right-wing'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'right-wing crazy')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'right-wing crazy'
Print 'Deleted ''right-wing crazy'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'right-wing nut')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'right-wing nut'
Print 'Deleted ''right-wing nut'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'right-wing wack')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'right-wing wack'
Print 'Deleted ''right-wing wack'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'rim job')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'rim job','')
Print 'Added ''rim job'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'river nigger')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'river nigger'
Print 'Deleted ''river nigger'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'scum')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'scum'
Print 'Moved ''scum'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'scum')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'scum*' WHERE [Phrase] = 'scum'
END
Print 'Updated ''scum'' to ''scum*'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'sex change')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'sex change'
Print 'Deleted ''sex change'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'sex dvd')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'sex dvd'
Print 'Deleted ''sex dvd'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'sex film')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'sex film'
Print 'Deleted ''sex film'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'sex slave')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'sex slave'
Print 'Deleted ''sex slave'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'sex toy')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'sex toy'
Print 'Deleted ''sex toy'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'sex video')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'sex video'
Print 'Deleted ''sex video'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'sexxy')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'sexxy'
Print 'Deleted ''sexxy'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'sexy')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'sexy'
Print 'Deleted ''sexy'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'sexy and hot')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'sexy and hot'
Print 'Deleted ''sexy and hot'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'sexy attractive')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'sexy attractive'
Print 'Deleted ''sexy attractive'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'sexy dancer')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'sexy dancer'
Print 'Deleted ''sexy dancer'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'sexy female')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'sexy female'
Print 'Deleted ''sexy female'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'sexy female dancer')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'sexy female dancer'
Print 'Deleted ''sexy female dancer'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'sexy female model')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'sexy female model'
Print 'Deleted ''sexy female model'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'sexy girl')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'sexy girl'
Print 'Deleted ''sexy girl'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'sexy guy')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'sexy guy'
Print 'Deleted ''sexy guy'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'sexy hot')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'sexy hot'
Print 'Deleted ''sexy hot'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'sexy ladies')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'sexy ladies'
Print 'Deleted ''sexy ladies'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'sexy lady')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'sexy lady'
Print 'Deleted ''sexy lady'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'sexy male')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'sexy male'
Print 'Deleted ''sexy male'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'sexy male dancer')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'sexy male dancer'
Print 'Deleted ''sexy male dancer'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'sexy male model')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'sexy male model'
Print 'Deleted ''sexy male model'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'sexy man')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'sexy man'
Print 'Deleted ''sexy man'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'sexy men')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'sexy men'
Print 'Deleted ''sexy men'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'sexy model')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'sexy model'
Print 'Deleted ''sexy model'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'sexy woman')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'sexy woman'
Print 'Deleted ''sexy woman'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'sexy women')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'sexy women'
Print 'Deleted ''sexy women'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'shadow dancer')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'shadow dancer'
Print 'Deleted ''shadow dancer'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'shit')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'shit*' WHERE [Phrase] = 'shit'
END
Print 'Updated ''shit'' to ''shit*'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'shitface')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'shitface'
Print 'Deleted ''shitface'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'shit face')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'shit face'
Print 'Deleted ''shit face'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'shithead')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'shithead'
Print 'Deleted ''shithead'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'shit head')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'shit head'
Print 'Deleted ''shit head'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'shot boy')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'shot boy'
Print 'Deleted ''shot boy'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'shot girl')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'shot girl'
Print 'Deleted ''shot girl'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'shotboy')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'shotboy'
Print 'Deleted ''shotboy'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'shotgirl')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'shotgirl'
Print 'Deleted ''shotgirl'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'spice nigger')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'spice nigger'
Print 'Deleted ''spice nigger'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'steel monk*')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'steel monk*','')
Print 'Added ''steel monk*'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'strip* on web cam')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'strip* on web cam'
Print 'Deleted ''strip* on web cam'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'strip* on webcam')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'strip* on webcam'
Print 'Deleted ''strip* on webcam'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'sugar daddies')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'sugar daddies'
Print 'Moved ''sugar daddies'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'sugar daddies')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'sugar dadd*' WHERE [Phrase] = 'sugar daddies'
END
Print 'Updated ''sugar daddies'' to ''sugar dadd*'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'swinger')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'swinger'
Print 'Moved ''swinger'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'swisher')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'swisher'
Print 'Moved ''swisher'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'tar baby')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'tar bab*' WHERE [Phrase] = 'tar baby'
END
Print 'Updated ''tar baby'' to ''tar bab*'''

UPDATE [Config.CensorshipRule] set WhiteList = 'tars, tarred, tarring, tar and feather, tar with the same brush, tarred with the same brush, taradiddle, tarantella, tarantism, taranto, tarantula, tarantulas, tarawa, tarbell, tarboosh, tardenoisian, tardigrade, tardive, tardo, tardy, tardiness, tardiest, tardier, tare, tarentum, target, targets, targeted, targum, tarheel, tariff, tariffs, tarim, tarkindton, tarlatan, tarletan, tarmac, tarn, tarnal, tarnation, tarnish, tarnishes, tarnished, tarnishing, taro, tarot, tarp, tarpaper, tarpaulin, tarpeia, tarpeian, tarpon, tarpons, tarquin, tarradiddle, tarragon, tarriance, tarry, tarsal tarsands, tar sands, tarshish, tarsi, tarsier, tarso, tarsometatarsus, tarsus, tart, tarts, tartiness, tarty, tartly, tartan, tartar, tartarean, tartarian, tartaric, tartaric, tartarous, tartar, tartarus, tartary, tartini, tartlet, tartrate, tartrated, tartu, tartuffe, tarzan' where [Phrase] = 'tar bab*'
Print 'Added White List Items for ''tar bab*'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'tar babies')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'tar babies'
Print 'Deleted ''tar babies'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'teen amateur')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'teen amateur'
Print 'Deleted ''teen amateur'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'teen amature')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'teen amature'
Print 'Deleted ''teen amature'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'teen* on web cam')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'teen* on web cam'
Print 'Deleted ''teen* on web cam'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'teen* webcam')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'teen* webcam'
Print 'Deleted ''teen* webcam'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'terrorist')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'terrorist'
Print 'Moved ''terrorist'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'terrorist')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'terroris*' WHERE [Phrase] = 'terrorist'
END
Print 'Updated ''terrorist'' to ''terroris*'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'terrorists')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'terrorists'
Print 'Deleted ''terrorists'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'tit')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'tit*' WHERE [Phrase] = 'tit'
END
Print 'Updated ''tit'' to ''tit*'''

UPDATE [Config.CensorshipRule] set WhiteList = 'institute, instituted, instituting, institutes, institutional, institutionally, institutionalize, institutionalized, institutionalizing, institutionalizes, institutionalization, institutionary, stitch, stitched, stitching, stitches, stitcher, stitchers, stitchery, stitcheries, stitchwort, stitchy, stitchless, stitchled, stitchling, titan, titans, titanate, titania, titanic, titaniferous, titanism, titanite, titanium, titanomachy, titsnosaur, titanous, titbit, titer, tit for tat, tithe, tithed, tithing, tither, tithes, tithable, tithonus, titian, titiacaca, titivate, titlark, title, titles, titled, titling, title deed, titleholder, titleholders, titleholding, titalist, titalists, titmouse, titmice, tito, titograd, titrate, titrated, titrating, titrates, titration, titre, tit-tac-toe, titter, titivate, titivated, titivating, titivates, tittle, tittle tattle, tittletattle, tittup, tittuped, tittuping, tittupping, tittupes, titubation titular, titus' where [Phrase] = 'tit*'
Print 'Added White List Items for ''tit*'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'tits')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'tits'
Print 'Deleted ''tits'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'tittie')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'tittie'
Print 'Deleted ''tittie'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'titties')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'titties'
Print 'Deleted ''titties'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'titty')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'titty'
Print 'Deleted ''titty'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'topless dancer')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'topless dancer'
Print 'Deleted ''topless dancer'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'trouser snake')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'trouser snake','')
Print 'Added ''trouser snake'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'ugly')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'ugly'
Print 'Moved ''ugly'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'uncle tom')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'uncle tom'
Print 'Moved ''uncle tom'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'under age boy')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'under age boy'
Print 'Deleted ''under age boy'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'under-age boy')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'under-age boy'
Print 'Deleted ''under-age boy'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'under age girl')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'under age girl'
Print 'Deleted ''under age girl'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'under-age girl')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'under-age girl'
Print 'Deleted ''under-age girl'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'uppity nigger')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'uppity nigger'
Print 'Deleted ''uppity nigger'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'video model')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'video model'
Print 'Deleted ''video model'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'wank')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'wank*' WHERE [Phrase] = 'wank'
END
Print 'Updated ''wank'' to ''wank*'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'wanker')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'wanker'
Print 'Deleted ''wanker'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'web cam female')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'web cam female'
Print 'Deleted ''web cam female'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'web cam male')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'web cam male'
Print 'Deleted ''web cam male'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'web cam model')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'web cam model'
Print 'Deleted ''web cam model'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'web cam stripp')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'web cam stripp'
Print 'Deleted ''web cam stripp'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'web cam teen')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'web cam teen'
Print 'Deleted ''web cam teen'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'webcam female')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'webcam female'
Print 'Deleted ''webcam female'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'webcam male')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'webcam male'
Print 'Deleted ''webcam male'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'webcam model')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'webcam model'
Print 'Deleted ''webcam model'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'webcam stripp')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'webcam stripp'
Print 'Deleted ''webcam stripp'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'webcam teen')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'webcam teen'
Print 'Deleted ''webcam teen'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'well endowed')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'well endowed'
Print 'Moved ''well endowed'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'well-endowed')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'well-endowed'
Print 'Moved ''well-endowed'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'white* only')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'white* only'
Print 'Deleted ''white* only'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'white trash')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'white trash'
Print 'Deleted ''white trash'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'white supremacy')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'white supremacy'
Print 'Deleted ''white supremacy'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'woman model')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'woman model'
Print 'Deleted ''woman model'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'wom* on webcam')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'wom* on webcam'
Print 'Deleted ''wom* on webcam'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'wom* only')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'wom* only'
Print 'Moved ''wom* only'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'wom* only')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'women only' WHERE [Phrase] = 'wom* only'
END
Print 'Updated ''wom* only'' to ''women only'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'wom* job')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'wom* job'
Print 'Moved ''wom* job'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'wom* job')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'woman''s job' WHERE [Phrase] = 'wom* job'
END
Print 'Updated ''wom* job'' to ''women''s job'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'wom* work')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'wom* work'
Print 'Moved ''wom* work'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'wom* work')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'woman''s work' WHERE [Phrase] = 'wom* work'
END
Print 'Updated ''wom* work'' to ''women''s work'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'wom* are encouraged to apply')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'wom* are encouraged to apply'
Print 'Moved ''wom* are encouraged to apply'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'wom* are encouraged to apply')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'women are encouraged to apply' WHERE [Phrase] = 'wom* are encouraged to apply'
END
Print 'Updated ''wom* are encouraged to apply'' to ''women are encouraged to apply'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'wom* on webcam')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'wom* on webcam'
Print 'Deleted ''wom* on webcam'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'wom* only')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'wom* only'
Print 'Deleted ''wom* only'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'wom* should not apply')
BEGIN
UPDATE [Config.CensorshipRule] set [Level] = 2 where [Phrase] = 'wom* should not apply'
Print 'Moved ''wom* should not apply'' from RED to YELLOW List'
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'wom* should not apply')
BEGIN
UPDATE [Config.CensorshipRule] set [Phrase] = 'women should not apply' WHERE [Phrase] = 'wom* should not apply'
END
Print 'Updated ''wom* should not apply'' to ''women should not apply'''

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'x rated dancer')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'x rated dancer'
Print 'Deleted ''x rated dancer'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'x rated film')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'x rated film'
Print 'Deleted ''x rated film'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'x rated girl')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'x rated girl'
Print 'Deleted ''x rated girl'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'x rated model')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'x rated model'
Print 'Deleted ''x rated model'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'x rated photo')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'x rated photo'
Print 'Deleted ''x rated photo'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'x rated pic')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'x rated pic'
Print 'Deleted ''x rated pic'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'x rated pix')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'x rated pix'
Print 'Deleted ''x rated pix'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'x rated video')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'x rated video'
Print 'Deleted ''x rated video'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'x rated web cam')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'x rated web cam'
Print 'Deleted ''x rated web cam'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'x rated webcam')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'x rated webcam'
Print 'Deleted ''x rated webcam'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'x rated women')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'x rated women'
Print 'Deleted ''x rated women'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'x-rated dancer')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'x-rated dancer'
Print 'Deleted ''x-rated dancer'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'x-rated film')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'x-rated film'
Print 'Deleted ''x-rated film'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'x-rated girl')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'x-rated girl'
Print 'Deleted ''x-rated girl'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'x-rated model')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'x-rated model'
Print 'Deleted ''x-rated model'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'x-rated photo')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'x-rated photo'
Print 'Deleted ''x-rated photo'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'x-rated pic')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'x-rated pic'
Print 'Deleted ''x-rated pic'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'x-rated pix')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'x-rated pix'
Print 'Deleted ''x-rated pix'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'x-rated video')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'x-rated video'
Print 'Deleted ''x-rated video'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'x-rated web cam')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'x-rated web cam'
Print 'Deleted ''x-rated web cam'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'x-rated webcam')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'x-rated webcam'
Print 'Deleted ''x-rated webcam'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'x-rated women')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'x-rated women'
Print 'Deleted ''x-rated women'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'xrated dancer')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'xrated dancer'
Print 'Deleted ''xrated dancer'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'xrated film')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'xrated film'
Print 'Deleted ''xrated film'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'xrated girl')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'xrated girl'
Print 'Deleted ''xrated girl'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'xrated model')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'xrated model'
Print 'Deleted ''xrated model'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'xrated photo')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'xrated photo'
Print 'Deleted ''xrated photo'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'xrated pic')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'xrated pic'
Print 'Deleted ''xrated pic'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'xrated pix')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'xrated pix'
Print 'Deleted ''xrated pix'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'xrated video')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'xrated video'
Print 'Deleted ''xrated video'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'xrated web cam')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'xrated web cam'
Print 'Deleted ''xrated web cam'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'xrated webcam')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'xrated webcam'
Print 'Deleted ''xrated webcam'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'xrated women')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'xrated women'
Print 'Deleted ''xrated women'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'xx ')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'xx ','')
Print 'Added ''xx '''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'xx rated')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'xx rated','')
Print 'Added ''xx rated'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'xx-rated')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'xx-rated','')
Print 'Added ''xx-rated'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'xxrated')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'xxrated','')
Print 'Added ''xxrated'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'xxx dancer')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'xxx dancer'
Print 'Deleted ''xxx dancer'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'xxx female')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'xxx female'
Print 'Deleted ''xxx female'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'xxx film')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'xxx film'
Print 'Deleted ''xxx film'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'xxx gay')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'xxx gay'
Print 'Deleted ''xxx gay'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'xxx girl')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'xxx girl'
Print 'Deleted ''xxx girl'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'xxx guy')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'xxx guy'
Print 'Deleted ''xxx guy'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'xxx male')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'xxx male'
Print 'Deleted ''xxx male'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'xxx man')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'xxx man'
Print 'Deleted ''xxx man'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'xxx men')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'xxx men'
Print 'Deleted ''xxx men'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'xxx model')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'xxx model'
Print 'Deleted ''xxx model'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'xxx photo')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'xxx photo'
Print 'Deleted ''xxx photo'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'xxx pic')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'xxx pic'
Print 'Deleted ''xxx pic'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'xxx pix')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'xxx pix'
Print 'Deleted ''xxx pix'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'xxx rated')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'xxx rated'
Print 'Deleted ''xxx rated'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'xxx rated model')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'xxx rated model'
Print 'Deleted ''xxx rated model'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'xxx video')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'xxx video'
Print 'Deleted ''xxx video'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'xxx web cam')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'xxx web cam'
Print 'Deleted ''xxx web cam'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'xxx webcam')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'xxx webcam'
Print 'Deleted ''xxx webcam'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'xxx women')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'xxx women'
Print 'Deleted ''xxx women'''
END

IF NOT EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'xxxrated')
BEGIN
INSERT INTO [Config.CensorshipRule] ([Type], [Level], Phrase, WhiteList) VALUES (@Type, @Level, 'xxxrated','')
Print 'Added ''xxxrated'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'xxx-rated dancer')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'xxx-rated dancer'
Print 'Deleted ''xxx-rated dancer'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'xxx-rated model')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'xxx-rated model'
Print 'Deleted ''xxx-rated model'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'xxx-rated web cam')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'xxx-rated web cam'
Print 'Deleted ''xxx-rated web cam'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'xxx-rated webcam')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'xxx-rated webcam'
Print 'Deleted ''xxx-rated webcam'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'xxx-rated women')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'xxx-rated women'
Print 'Deleted ''xxx-rated women'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'yellow dawg democrat')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'yellow dawg democrat'
Print 'Deleted ''yellow dawg democrat'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'yellow dog democrat')
BEGIN
DELETE FROM [Config.CensorshipRule] WHERE [Phrase] = 'yellow dog democrat'
Print 'Deleted ''yellow dog democrat'''
END

IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'yellow skin')
BEGIN
DELETE FROM [Config.CensorshipRule] where [Phrase] = 'yellow skin'
Print 'Deleted ''yellow skin'''
END


IF EXISTS (SELECT 1 FROM [Config.CensorshipRule] WHERE [Phrase] = 'yellow-skinned')
BEGIN
DELETE FROM [Config.CensorshipRule] where [Phrase] = 'yellow-skinned'
Print 'Deleted ''yellow-skinned'''
END

-- CHECK THIS ALL WORKS _ It should be complete