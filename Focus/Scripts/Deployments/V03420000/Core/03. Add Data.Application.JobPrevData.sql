﻿ALTER TABLE [dbo].[Data.Application.Job] ALTER COLUMN JobTitle nvarchar(200) NULL;
ALTER TABLE [dbo].[Data.Application.Job] ALTER COLUMN OtherBenefitsDetails nvarchar(3000) NULL;
ALTER TABLE [dbo].[Data.Application.Job] ALTER COLUMN InterviewEmailAddress nvarchar(200) NULL;
ALTER TABLE [dbo].[Data.Application.Job] ALTER COLUMN InterviewApplicationUrl nvarchar(4000) NULL;
ALTER TABLE [dbo].[Data.Application.Job] ALTER COLUMN InterviewMailAddress nvarchar(500) NULL;
ALTER TABLE [dbo].[Data.Application.Job] ALTER COLUMN InterviewFaxNumber nvarchar(20) NULL;
ALTER TABLE [dbo].[Data.Application.Job] ALTER COLUMN InterviewPhoneNumber nvarchar(20) NULL;
ALTER TABLE [dbo].[Data.Application.Job] ALTER COLUMN InterviewDirectApplicationDetails nvarchar(2000) NULL;
--ALTER TABLE [dbo].[Data.Application.Job] ALTER COLUMN InterviewOtherInstructions nvarchar(2000) NULL;
ALTER TABLE [dbo].[Data.Application.Job] ALTER COLUMN MinimumAgeReason nvarchar(500) NULL;
ALTER TABLE [dbo].[Data.Application.Job] ALTER COLUMN RedProfanityWords nvarchar(1000) NULL;
ALTER TABLE [dbo].[Data.Application.Job] ALTER COLUMN YellowProfanityWords nvarchar(1000) NULL;
ALTER TABLE [dbo].[Data.Application.Job] ALTER COLUMN ExternalId nvarchar(36) NULL;
ALTER TABLE [dbo].[Data.Application.Job] ALTER COLUMN OtherSalary nvarchar(4000) NULL;
ALTER TABLE [dbo].[Data.Application.Job] ALTER COLUMN Location nvarchar(255) NULL;
GO

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Data.Application.JobPrevData')
	DROP TABLE [dbo].[Data.Application.JobPrevData];
CREATE TABLE [dbo].[Data.Application.JobPrevData]
(
    Id                                     bigint          NOT NULL,
    JobId                                  bigint          NOT NULL,
    ClosingDate                            date            NULL,
    NumOpenings                            int             NULL,
    Locations                              nvarchar (200)  NULL,
    [Description]                          nvarchar (MAX)  NULL,
    JobRequirements                        nvarchar (1000) NULL,
    JobDetails                             nvarchar (1000) NULL,
    JobSalaryBenefits                      nvarchar (1000) NULL,
    RecruitmentInformation                 nvarchar (MAX) NULL,
    ForeignLabourCertificationDetails      nvarchar (200)  NULL,
    CourtOrderedAffirmativeActionDetails   nvarchar (200)  NULL,
    FederalContractorDetails               nvarchar (200)  NULL
) ON [PRIMARY];

CREATE UNIQUE CLUSTERED INDEX [IX_Data.Application.JobPrevData_JobId] ON [dbo].[Data.Application.JobPrevData] (JobId);
