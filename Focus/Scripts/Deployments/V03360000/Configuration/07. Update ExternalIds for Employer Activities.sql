IF (SELECT Value FROM [Config.ConfigurationItem] WHERE [Key] = 'IntegrationClient') = '3'
BEGIN
	DECLARE @Activites TABLE
	(
		ActivityType INT,
		[Name] NVARCHAR(50),
		ExternalId INT
	)
	INSERT INTO @Activites (ActivityType, [Name], ExternalId)
	VALUES
		(1, 'Service: Job Fair Assistance/Preparation', 14),
		(1, 'On-Site Review/Customized LMI', 23)

	UPDATE
		A
	SET
		ExternalId = TA.ExternalId
	FROM
		[Config.ActivityCategory] AC
	INNER JOIN [Config.Activity] A
		ON A.ActivityCategoryId = AC.Id
	INNER JOIN @Activites TA
		ON TA.ActivityType = AC.ActivityType
		AND TA.Name = A.Name
END