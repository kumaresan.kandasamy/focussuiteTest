IF (SELECT Value FROM [Config.ConfigurationItem] WHERE [Key] = 'IntegrationClient') = '3'
BEGIN
	DELETE FROM [Config.ConfigurationItem] WHERE [Key] = 'MaximumEmailLength'

	INSERT INTO [Config.ConfigurationItem] ([Key], Value, InternalOnly)
	VALUES ('MaximumEmailLength',80,1)
END
GO

