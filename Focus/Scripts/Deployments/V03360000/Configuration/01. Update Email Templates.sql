UPDATE
	[Config.EmailTemplate]
SET
	Body = REPLACE(Body, 'Draft Postings', '#JOBSTATUS# Postings')
WHERE
	EmailTemplateType = 7