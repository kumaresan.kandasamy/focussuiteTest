﻿IF EXISTS (SELECT * FROM sys.indexes i WHERE i.name = 'IX_Data.Application.BusinessUnit_Inc')
	DROP INDEX [IX_Data.Application.BusinessUnit_Inc] ON [dbo].[Data.Application.BusinessUnit];
CREATE NONCLUSTERED INDEX [IX_Data.Application.BusinessUnit_Inc]
		ON [dbo].[Data.Application.BusinessUnit]([Id] ASC)
		INCLUDE
		(
			[AccountTypeId], [AlternatePhone1], [AlternatePhone1Type], [AlternatePhone2], [AlternatePhone2Type], [AwaitingApprovalDate],
			[IndustrialClassification], [LegalName], [Name], [OwnershipTypeId], [PrimaryPhone], [PrimaryPhoneType],
			[RedProfanityWords], [YellowProfanityWords], [Url]
		)
		ON [PRIMARY];

	
IF EXISTS (SELECT * FROM sys.indexes i WHERE i.name = 'IX_Data.Application.BusinessUnit_Id')
	DROP INDEX [IX_Data.Application.BusinessUnit_Id] ON [dbo].[Data.Application.BusinessUnit];
CREATE NONCLUSTERED INDEX [IX_Data.Application.BusinessUnit_Id]
		ON [dbo].[Data.Application.BusinessUnit]([Id] ASC)
		INCLUDE
		(
			[ApprovalStatus], [IsPrimary]
		)
		ON [PRIMARY];
			

IF EXISTS (SELECT * FROM sys.indexes i WHERE i.name = 'IX_Data.Application.User_PersonId')
	DROP INDEX [IX_Data.Application.User_PersonId] ON [dbo].[Data.Application.User];
CREATE NONCLUSTERED INDEX [IX_Data.Application.User_PersonId]
	ON [dbo].[Data.Application.User]([PersonId] ASC)
	INCLUDE([CreatedOn])
	ON [PRIMARY]


IF EXISTS (SELECT * FROM sys.indexes i WHERE i.name = 'IX_Data.Application.BusinessUnitAddress_BusinessUnitId')
	DROP INDEX [IX_Data.Application.BusinessUnitAddress_BusinessUnitId] ON [dbo].[Data.Application.BusinessUnitAddress];
CREATE NONCLUSTERED INDEX [IX_Data.Application.BusinessUnitAddress_BusinessUnitId]
		ON [dbo].[Data.Application.BusinessUnitAddress]([BusinessUnitId] ASC)
		INCLUDE([Line1], [Line2], [TownCity], [PostcodeZip], [StateId], [PublicTransitAccessible], [IsPrimary])
		ON [PRIMARY];


IF EXISTS (SELECT * FROM sys.indexes i WHERE i.name = 'IX_Data.Application.Employee_ApprovalStatus')
	DROP INDEX [IX_Data.Application.Employee_ApprovalStatus] ON [dbo].[Data.Application.Employee];
CREATE NONCLUSTERED INDEX [IX_Data.Application.Employee_ApprovalStatus]
		ON [dbo].[Data.Application.Employee]([ApprovalStatus] ASC)
		INCLUDE
		(
			[Id], [RedProfanityWords], [YellowProfanityWords], [ApprovalStatusChangedBy],
			[ApprovalStatusChangedOn], [AwaitingApprovalDate], [EmployerId], [PersonId]
		)
		ON [PRIMARY];


IF EXISTS (SELECT * FROM sys.indexes i WHERE i.name = 'IX_Data.Application.Employee_Id')
	DROP INDEX [IX_Data.Application.Employee_Id] ON [dbo].[Data.Application.Employee];
CREATE NONCLUSTERED INDEX [IX_Data.Application.Employee_Id]
		ON [dbo].[Data.Application.Employee]([Id] ASC)
		INCLUDE
		(
			[ApprovalStatusChangedBy], [ApprovalStatusChangedOn], [AwaitingApprovalDate], 
			[PersonId], [RedProfanityWords], [YellowProfanityWords]
		)
		ON [PRIMARY];
	

IF EXISTS (SELECT * FROM sys.indexes i WHERE i.name = 'IX_Data.Application.EmployeeBusinessUnit_EmployeeId')
	DROP INDEX [IX_Data.Application.EmployeeBusinessUnit_EmployeeId] ON [dbo].[Data.Application.EmployeeBusinessUnit];
CREATE NONCLUSTERED INDEX [IX_Data.Application.EmployeeBusinessUnit_EmployeeId]
		ON [dbo].[Data.Application.EmployeeBusinessUnit]([EmployeeId] ASC)
		INCLUDE([BusinessUnitId],[Default])
		ON [PRIMARY];


IF EXISTS (SELECT * FROM sys.indexes i WHERE i.name = 'IX_Data.Application.Employer_ReferralView')
	DROP INDEX [IX_Data.Application.Employer_ReferralView] ON [dbo].[Data.Application.Employer];
CREATE NONCLUSTERED INDEX [IX_Data.Application.Employer_ReferralView]
		ON [dbo].[Data.Application.Employer]([Id] ASC)
		INCLUDE([Name], [FederalEmployerIdentificationNumber], [StateEmployerIdentificationNumber], [ApprovalStatus], [AssignedToId])
		ON [PRIMARY];


IF EXISTS (SELECT * FROM sys.indexes i WHERE i.name = 'IX_Data.Application.Person_Id')
	DROP INDEX [IX_Data.Application.Person_Id] ON [dbo].[Data.Application.Person];
CREATE NONCLUSTERED INDEX [IX_Data.Application.Person_Id]
		ON [dbo].[Data.Application.Person]([Id] ASC)
		INCLUDE([FirstName], [LastName], [EmailAddress], [SuffixId])
		ON [PRIMARY];


IF EXISTS (SELECT * FROM sys.indexes i WHERE i.name = 'IX_Data.Application.PersonAddress_PersonId')
	DROP INDEX [IX_Data.Application.PersonAddress_PersonId] ON [dbo].[Data.Application.PersonAddress];
CREATE NONCLUSTERED INDEX [IX_Data.Application.PersonAddress_PersonId]
		ON [dbo].[Data.Application.PersonAddress]([PersonId] ASC)
		INCLUDE([Line1], [Line2], [TownCity], [PostcodeZip], [StateId])
		ON [PRIMARY];


IF EXISTS (SELECT * FROM sys.indexes i WHERE i.name = 'IX_Data.Application.PhoneNumber_PersonId_IsPrimary')
	DROP INDEX [IX_Data.Application.PhoneNumber_PersonId_IsPrimary] ON [dbo].[Data.Application.PhoneNumber];
CREATE NONCLUSTERED INDEX [IX_Data.Application.PhoneNumber_PersonId_IsPrimary]
		ON [dbo].[Data.Application.PhoneNumber]([PersonId] ASC, [IsPrimary] ASC)
		INCLUDE([Number], [PhoneType])
		ON [PRIMARY];
