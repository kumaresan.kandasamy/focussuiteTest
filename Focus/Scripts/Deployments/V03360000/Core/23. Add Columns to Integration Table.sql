IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Core.IntegrationRequestResponse' AND COLUMN_NAME = 'StatusCode')
BEGIN
	ALTER TABLE
		[Data.Core.IntegrationRequestResponse]
	ADD
		StatusCode INT NULL
END
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Core.IntegrationRequestResponse' AND COLUMN_NAME = 'StatusDescription')
BEGIN
	ALTER TABLE
		[Data.Core.IntegrationRequestResponse]
	ADD
		StatusDescription NVARCHAR(250)
END
GO
