﻿IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Data_Core_UpdateSession]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Data_Core_UpdateSession]
GO
/****** Object:  StoredProcedure [dbo].[Data_Core_UpdateSession]    Script Date: 26/06/2015 10:25:22 ******/


/****** Object:  StoredProcedure [dbo].[Data_Core_UpdateSession]    Script Date: 26/06/2015 10:25:22 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[Data_Core_UpdateSession] 
	@SessionId	uniqueidentifier,
	@UserId		bigint,
	@LastRequestOn datetime,
	@CheckUser bit
AS
IF(@CheckUser = 0)
BEGIN
	UPDATE [Data.Core.Session]
	SET UserId = @UserId, LastRequestOn = @LastRequestOn
	WHERE Id = @SessionId 
END ELSE BEGIN
	UPDATE [Data.Core.Session]
	SET LastRequestOn = @LastRequestOn
	WHERE Id = @SessionId AND UserId = @UserId
END




GO

GRANT EXECUTE ON [dbo].[Data_Core_UpdateSession] TO FocusAgent
GO

