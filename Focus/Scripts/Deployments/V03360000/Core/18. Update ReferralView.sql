IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'Data.Application.ReferralView')
BEGIN
	DROP VIEW [dbo].[Data.Application.ReferralView]
END
GO

CREATE VIEW [dbo].[Data.Application.ReferralView]
AS

SELECT
	Posting.Id AS Id,
	[Posting].LensPostingId,
	[Application].CreatedOn AS ApplicationDate,
	Posting.JobTitle,
	[BusinessUnit].Name AS EmployerName,
	Posting.EmployerName AS PostingEmployerName,
	[Application].AutomaticallyApproved AS CandidateApplicationAutomaticallyApproved,
	[Application].PreviousApprovalStatus,
	[Person].FirstName + ' ' + [Person].LastName AS EmployeeName,
	[Resume].PersonId AS CandidateId,
	[Application].ApplicationScore AS CandidateApplicationScore,
	[Application].ApprovalStatus AS ApplicationApprovalStatus,
	[PhoneNumber].Number AS EmployeePhoneNumber,
	Person.EmailAddress AS EmployeeEmail,
	Job.Location,
	Job.ClosingOn AS JobClosingOn,
	Posting.CreatedOn AS PostingCreatedOn,
	Job.Id AS JobId,
	Job.VeteranPriorityEndDate,
	[Resume].IsVeteran,
	Job.IsConfidential,
	Job.InterviewContactPreferences, 
	Job.InterviewMailAddress, 
	Job.InterviewEmailAddress,
	Job.InterviewFaxNumber,
	Job.InterviewDirectApplicationDetails,
	Job.InterviewApplicationUrl,
	Job.InterviewPhoneNumber,
	Job.InterviewOtherInstructions
FROM [Data.Application.Application] AS [Application] WITH (NOLOCK)
	INNER JOIN [Data.Application.Posting] AS Posting WITH (NOLOCK) ON [Application].PostingId = Posting.Id
	INNER JOIN [Data.Application.Job] AS Job WITH (NOLOCK) ON Posting.JobId = Job.Id
	INNER JOIN [Data.Application.Resume] AS [Resume] WITH (NOLOCK) ON [Application].ResumeId = [Resume].Id
	INNER JOIN [Data.Application.BusinessUnit] AS [BusinessUnit] WITH (NOLOCK) ON Job.BusinessUnitId = [BusinessUnit].Id
	INNER JOIN [Data.Application.Employee] AS [Employee] WITH (NOLOCK) ON Job.EmployeeId = [Employee].Id
	INNER JOIN [Data.Application.Person] AS [Person] WITH (NOLOCK) ON Employee.PersonId = Person.Id
	LEFT OUTER JOIN [Data.Application.PhoneNumber] AS [PhoneNumber] WITH (NOLOCK) ON [Person].Id = [PhoneNumber].PersonId AND [PhoneNumber].IsPrimary = 1

UNION 

SELECT	Posting.Id AS Id, 
		Posting.LensPostingId AS LensPostingId,
		referral.ReferralDate AS ApplicationDate, 
		Posting.JobTitle AS JobTitle,
		bu.Name AS EmployerName,
		Posting.EmployerName AS PostingEmployerName,
		CAST(1 AS bit) AS CandidateApplicationAutomaticallyApproved,
		CAST(1 AS bit) As PreviousApprovalStatus,
		employee.FirstName + ' ' + employee.LastName AS EmployeeName,
		candidate.Id AS CandidateId,
		ISNULL(referral.ReferralScore, 0) AS CandidateApplicationScore,
		'' AS ApplicationApprovalStatus,
		[PhoneNumber].Number AS EmployeePhoneNumber,
		employee.EmailAddress AS EmployeeEmail,
		J.Location,
		J.ClosingOn AS JobClosingOn,
		Posting.CreatedOn AS PostingCreatedOn,
		J.Id AS JobId,
		J.VeteranPriorityEndDate,
		candidate.IsVeteran,
		j.IsConfidential,
		j.InterviewContactPreferences, 
		j.InterviewMailAddress, 
		j.InterviewEmailAddress,
		j.InterviewFaxNumber,
		j.InterviewDirectApplicationDetails,
		j.InterviewApplicationUrl,
		j.InterviewPhoneNumber,
		j.InterviewOtherInstructions
FROM  dbo.[Data.Application.Referral] AS referral WITH (NOLOCK)
	INNER JOIN dbo.[Data.Application.Posting] AS Posting WITH (NOLOCK) ON referral.PostingId = Posting.Id
	LEFT JOIN 
	(
		dbo.[Data.Application.Job] AS j WITH (NOLOCK)  
		INNER JOIN dbo.[Data.Application.BusinessUnit] AS bu WITH (NOLOCK) ON j.BusinessUnitId = bu.Id
		INNER JOIN dbo.[Data.Application.Employee] AS e WITH (NOLOCK) ON j.EmployeeId = e.Id
		INNER JOIN dbo.[Data.Application.Person] AS employee WITH (NOLOCK) ON e.PersonId = employee.Id
	) ON Posting.JobId = j.Id 
	INNER JOIN dbo.[Data.Application.Person] AS candidate WITH (NOLOCK) ON referral.PersonId = candidate.Id
	LEFT OUTER JOIN [Data.Application.PhoneNumber] AS [PhoneNumber] WITH (NOLOCK) ON employee.Id = [PhoneNumber].PersonId AND [PhoneNumber].IsPrimary = 1
WHERE referral.UserId = referral.ActionerId
GO

