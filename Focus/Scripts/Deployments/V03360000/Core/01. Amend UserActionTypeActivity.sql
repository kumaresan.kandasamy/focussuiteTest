IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.UserActionTypeActivity' AND COLUMN_NAME = 'ExternalActivityId')
BEGIN
	ALTER TABLE
		[Data.Application.UserActionTypeActivity]
	ADD
		ExternalActivityId NVARCHAR(200) NULL
END
GO

IF NOT EXISTS(SELECT 1 FROM [Data.Application.UserActionTypeActivity] WHERE ExternalActivityId IS NOT NULL)
BEGIN
	UPDATE
		UA
	SET
		ExternalActivityId = EL.ExternalId
	FROM
		[Data.Application.UserActionTypeActivity] UA
	INNER JOIN [Config.ExternalLookUpItem] EL
		ON EL.InternalId = UA.ActionType
		AND EL.ExternalLookUpType = 23
END
GO