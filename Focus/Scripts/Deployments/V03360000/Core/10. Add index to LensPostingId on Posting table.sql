IF NOT EXISTS(SELECT 1 FROM Sys.indexes WHERE name = 'IX_Data.Application.Posting_LensPostingId_StatusId')
BEGIN
	CREATE NONCLUSTERED INDEX [IX_Data.Application.Posting_LensPostingId_StatusId]
	ON [dbo].[Data.Application.Posting] ([LensPostingId] , [StatusId])
END
GO
