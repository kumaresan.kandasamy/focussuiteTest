
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Config.StarRatingMinimumScore')
BEGIN
	CREATE TABLE [Config.StarRatingMinimumScore]
	(
		[Id] BIGINT NOT NULL IDENTITY(1, 1) CONSTRAINT [PK_Config.StarRatingMinimumScore] PRIMARY KEY CLUSTERED,
		MinimumScore INT NOT NULL,
		MaximumScore INT NOT NULL,
		StarRating INT NOT NULL
	)	
END
GO

IF NOT EXISTS(SELECT 1 FROM [Config.StarRatingMinimumScore])
BEGIN
	DECLARE @Scores NVARCHAR(255)
	DECLARE @ScoreTable TABLE
	(
		Score INT
	)
	
	SELECT @Scores = Value FROM [Config.ConfigurationItem] WHERE [Key] = 'StarRatingMinimumScores'
	IF @Scores IS NULL
	BEGIN
		SET @Scores = '0,200,400,550,700,850'
	END
	
	INSERT INTO @ScoreTable
	(
		Score
	)
	SELECT 
		 Split.a.value('.', 'VARCHAR(100)') AS CVS  
	FROM  
	(
		SELECT CAST ('<M>' + REPLACE(@Scores, ',', '</M><M>') + '</M>' AS XML) AS CVS 
	) AS A 
	CROSS APPLY CVS.nodes ('/M') AS Split(a)	
	
	INSERT INTO [Config.StarRatingMinimumScore]
	(
		MinimumScore,
		MaximumScore,
		StarRating
	)
	SELECT
		ST.Score,
		ISNULL(ST2.Score - 1, 10000),
		ROW_NUMBER() OVER (ORDER BY ST.Score ASC) - 1
	FROM 
		@ScoreTable ST
	OUTER APPLY
	(
		SELECT TOP 1
			ST2.Score
		FROM
			@ScoreTable ST2
		WHERE
			ST2.Score > ST.Score
		ORDER BY
			ST2.Score ASC
	) ST2
END
GO


SELECT * FROM [Config.StarRatingMinimumScore]