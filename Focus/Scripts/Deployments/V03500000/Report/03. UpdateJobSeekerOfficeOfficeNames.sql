IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Report.JobSeekerOfficeNameUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Report.JobSeekerOfficeNameUpdate]
GO

CREATE PROCEDURE [dbo].[Report.JobSeekerOfficeNameUpdate] 
	@OfficeId bigint
AS
BEGIN
	SET NOCOUNT ON;
	
	UPDATE
	 RJS
	SET 
	 Office = STUFF(
	  (SELECT 
		'||' + JSO.OfficeName
	   FROM 
		[Report.JobSeeker] RJS2
	   INNER JOIN [Report.JobSeekerOffice] JSO
		ON JSO.JobSeekerId = RJS2.Id
	   WHERE
		RJS2.Id = RJS.Id
	   ORDER BY
		JSO.OfficeName
	   FOR XML PATH('')
	  ), 1, 2, '')
	FROM
	 [Report.JobSeeker] RJS
	WHERE
	 EXISTS (SELECT 1 FROM [Report.JobSeekerOffice] JSO2 WHERE JSO2.JobSeekerId = RJS.Id AND JSO2.OfficeId = @OfficeId)
	 
END
GO


GRANT EXEC ON [dbo].[Report.JobSeekerOfficeNameUpdate] TO [FocusAgent]