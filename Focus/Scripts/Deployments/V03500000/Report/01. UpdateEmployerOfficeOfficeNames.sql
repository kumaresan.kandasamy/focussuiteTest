IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Report.EmployerOfficeNameUpdate]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[Report.EmployerOfficeNameUpdate]
GO

CREATE PROCEDURE [dbo].[Report.EmployerOfficeNameUpdate] 
	@OfficeId bigint
AS
BEGIN
	SET NOCOUNT ON;
	
	UPDATE
		RE
	SET 
		Office = STUFF(
		(SELECT 
		'||' + EO.OfficeName
			FROM 
		[Report.Employer] RJS2
			INNER JOIN [Report.EmployerOffice] EO
		ON EO.EmployerId = RJS2.Id
			WHERE
		RJS2.Id = RE.Id
			ORDER BY
		EO.OfficeName
			FOR XML PATH('')
		), 1, 2, '')
	FROM
		[Report.Employer] RE
	WHERE
		EXISTS (SELECT 1 FROM [Report.EmployerOffice] EO2 WHERE EO2.EmployerId = RE.Id AND EO2.OfficeId = @OfficeId)
END
GO


GRANT EXEC ON [dbo].[Report.EmployerOfficeNameUpdate] TO [FocusAgent]