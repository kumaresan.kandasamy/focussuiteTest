IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'Data.Application.ApplicationStatusLogView')
BEGIN
	DROP VIEW [dbo].[Data.Application.ApplicationStatusLogView]
END
GO

CREATE VIEW [dbo].[Data.Application.ApplicationStatusLogView]
AS
WITH logs AS
(
	SELECT	StatusLog.Id AS StatusLogId, 
					StatusLog.ActionedOn AS ActionedOn, 
					StatusLog.NewStatus AS CandidateApplicationStatus, 
					StatusLog.OriginalStatus AS CandidateOriginalApplicationStatus, 
					StatusLog.EntityId AS ApplicationId,
					[Application].ApplicationScore AS CandidateApplicationScore,
					[Application].Id AS CandidateApplicationId,
					[Application].ApprovalStatus AS CandidateApprovalStatus,
					[Application].ResumeId,
					[Application].PostingId,
					(SELECT TOP 1 Id FROM dbo.[Data.Application.ResumeJob] WHERE ResumeId = [Application].ResumeId ORDER BY EndYear DESC, StartYear DESC, Id) AS ResumeJobId
	FROM  	dbo.[Data.Core.StatusLog] AS StatusLog WITH (NOLOCK)
	INNER JOIN dbo.[Data.Application.Application] AS [Application] WITH (NOLOCK) ON StatusLog.EntityId = [Application].Id 
)
SELECT	logs.StatusLogId AS Id,
				logs.ActionedOn AS ActionedOn,
				logs.CandidateApplicationScore,
				[Resume].FirstName AS CandidateFirstName,
				[Resume].LastName AS CandidateLastName,
				[Resume].YearsExperience AS CandidateYearsExperience,
				[Resume].IsVeteran AS CandidateIsVeteran,
				logs.CandidateApplicationStatus,
				logs.CandidateOriginalApplicationStatus,
				Job.Id AS JobId,
				LatestJob.JobTitle AS LatestJobTitle,
				LatestJob.Employer AS LatestJobEmployer,
				LatestJob.StartYear AS LatestJobStartYear,
				LatestJob.EndYear AS LatestJobEndYear,
				logs.CandidateApplicationId,
				Job.BusinessUnitId AS BusinessUnitId,
				Job.JobTitle AS JobTitle,
				[Resume].PersonId,
				logs.CandidateApprovalStatus,
				Job.VeteranPriorityEndDate,
				[Person].ProgramAreaId,
				[Person].DegreeId,
				[Resume].EducationCompletionDate,
				Job.JobStatus
FROM  	logs
INNER JOIN dbo.[Data.Application.Resume] AS [Resume] WITH (NOLOCK) ON logs.ResumeId = [Resume].Id 
INNER JOIN dbo.[Data.Application.Person] AS [Person] WITH (NOLOCK) ON [Resume].PersonId = [Person].Id 
INNER JOIN dbo.[Data.Application.Posting] AS Posting WITH (NOLOCK) ON logs.PostingId = Posting.Id
INNER JOIN dbo.[Data.Application.Job] AS Job WITH (NOLOCK) ON Job.Id = [Posting].JobId 
LEFT OUTER JOIN dbo.[Data.Application.ResumeJob] LatestJob WITH (NOLOCK) ON logs.ResumeJobId = LatestJob.Id