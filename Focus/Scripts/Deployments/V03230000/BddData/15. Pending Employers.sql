DECLARE @NextId BIGINT

DECLARE @PersonId BIGINT
DECLARE @PersonAddressId BIGINT
DECLARE @UserId BIGINT
DECLARE @UserRoleId BIGINT
DECLARE @EmployerId BIGINT
DECLARE @EmployerAddressId BIGINT
DECLARE @BusinessUnitId BIGINT
DECLARE @BusinessUnitAddressId BIGINT
DECLARE @BusinessUnitDescriptionId BIGINT
DECLARE @EmployeeId BIGINT
DECLARE @EmployeeBusinessUnitId BIGINT
DECLARE @PhoneNumberId BIGINT

DECLARE @EmployerName NVARCHAR(255)
DECLARE @FirstName NVARCHAR(255)
DECLARE @LastName NVARCHAR(255)
DECLARE @EmailAddress NVARCHAR(255)
DECLARE @ScreenName NVARCHAR(255)
DECLARE @CompanyDescription NVARCHAR(255)
DECLARE @EmployeeApprovalStatus INT
DECLARE @RedWords NVARCHAR(100)
DECLARE @YellowWords NVARCHAR(100)
DECLARE @ApprovalStatusChangedOn DATETIME
DECLARE @ApprovalStatusChangedBy BIGINT
	
DECLARE @TitleId BIGINT
DECLARE @RoleId BIGINT
DECLARE @CountyId BIGINT
DECLARE @StateId BIGINT
DECLARE @CountryId BIGINT
DECLARE @OwnershipTypeId BIGINT
DECLARE @AssistUserId BIGINT

SELECT @TitleId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Titles' AND [Key] = 'Title.Mr'
SELECT @RoleId = Id FROM [Data.Application.Role] WHERE [Key] = 'TalentUser'
SELECT @CountyId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Counties' AND [Key] = 'County.ClayTX'
SELECT @StateId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'States' AND [Key] = 'State.TX'
SELECT @CountryId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Countries' AND [Key] = 'Country.US'
SELECT @OwnershipTypeId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'OwnershipTypes' AND [Key] = 'OwnershipTypes.PrivateCorporation'
SELECT @AssistUserId = Id FROM [Data.Application.User] WHERE UserName = 'dev@client.com'

DECLARE @Employers TABLE
(
	Id BIGINT IDENTITY(1, 1),
	EmployerName NVARCHAR(255),
	FirstName NVARCHAR(255),
	LastName NVARCHAR(255),
	EmailAddress NVARCHAR(255),
	ScreenName NVARCHAR(255),
	CompanyDescription  NVARCHAR(255),
	EmployeeApprovalStatus INT,
	RedWords NVARCHAR(100),
	YellowWords NVARCHAR(100),
	ApprovalStatusChangedOn DATETIME,
	ApprovalStatusChangedBy BIGINT
)

INSERT INTO @Employers 
	(EmployerName, FirstName, LastName, EmailAddress, ScreenName, CompanyDescription, EmployeeApprovalStatus, RedWords, YellowWords, ApprovalStatusChangedOn, ApprovalStatusChangedBy)
VALUES 
	('A Pending Employer', 'Pending', 'Employer', 'pending@employer.com', 'Pending Employer', 'A Pending Employer', 1, NULL, NULL, NULL, NULL)
	
DECLARE @EmployerCount INT
DECLARE @TotalEmployers INT

SET @EmployerCount = 1
SELECT @TotalEmployers = COUNT(1) FROM @Employers

WHILE @EmployerCount <= @TotalEmployers
BEGIN
	
	SELECT 
		@EmployerName = EmployerName,
		@FirstName = FirstName,
		@LastName = LastName,
		@EmailAddress = EmailAddress,
		@ScreenName = ScreenName, 
		@CompanyDescription = CompanyDescription,
		@EmployeeApprovalStatus = EmployeeApprovalStatus,
		@RedWords = RedWords,
		@YellowWords = YellowWords,
		@ApprovalStatusChangedOn = ApprovalStatusChangedOn,
		@ApprovalStatusChangedBy = ApprovalStatusChangedBy
	FROM
		@Employers
	WHERE
		Id = @EmployerCount
	
	IF NOT EXISTS(SELECT 1 FROM [Data.Application.Employer] WHERE Name = @EmployerName) AND
		NOT EXISTS(SELECT 1 FROM [Data.Application.Person] WHERE EmailAddress = @EmailAddress)
	BEGIN
		BEGIN TRANSACTION
		
		SELECT @NextId = NextId FROM KeyTable
		
		UPDATE KeyTable SET NextId = NextId + 12
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		SET @PersonId = @NextId
		SET @PersonAddressId = @NextId + 1
		SET @PhoneNumberId = @NextId + 2
		SET @UserId = @NextId + 3
		SET @UserRoleId = @NextId + 4
		SET @EmployerId = @NextId + 5
		SET @EmployerAddressId = @NextId + 6
		SET @BusinessUnitId = @NextId + 7
		SET @BusinessUnitAddressId = @NextId + 8
		SET @BusinessUnitDescriptionId = @NextId + 9
		SET @EmployeeId = @NextId + 10
		SET @EmployeeBusinessUnitId = @NextId + 11
		
		INSERT INTO [Data.Application.Person]
		(
			Id,
			TitleId,
			FirstName,
			MiddleInitial,
			LastName,
			DateOfBirth,
			SocialSecurityNumber,
			EmailAddress,
			EnrollmentStatus,
			ProgramAreaId,
			CampusId,
			DegreeId
		)
		VALUES
		(
			@PersonId,
			@TitleId,
			@FirstName,
			'',
			@LastName,
			NULL,
			NULL,
			@EmailAddress,
			NULL,
			NULL,
			NULL,
			NULL
		)

		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		INSERT INTO [Data.Application.PersonAddress]
		(
			Id,
			PersonId,
			Line1,
			Line2,
			TownCity,
			StateId ,
			CountyId,
			CountryId,
			PostcodeZip,
			IsPrimary
		)
		VALUES
		(
			@PersonAddressId,
			@PersonId,
			'Address Line 1',
			'',
			'Bluegrove',
			@StateId,
			@CountyId,
			@CountryId,
			'76352',
			1
		)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		INSERT INTO [Data.Application.PhoneNumber]
		(
			Id,
			PersonId,
			Number,
			PhoneType,
			IsPrimary,
			Extension,
			ProviderId
		)
		VALUES
		(
			@PhoneNumberId,
			@PersonId,
			'4234234234',
			0,
			1,
			NULL,
			NULL
		)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
			
		INSERT INTO [Data.Application.User]
		(
			Id,
			PersonId,
			UserName,
			PasswordHash,
			PasswordSalt,
			UserType,
			[Enabled],
			ScreenName,
			IsMigrated,
			RegulationsConsent,
			CreatedOn,
			UpdatedOn
		)
		VALUES
		(
			@UserId,
			@PersonId,
			@EmailAddress,
			'I/mugkpu/aiJINDf7aBlKfYCSI3PjN/UViGvmkNS9RIM1BmUdDEPZFKimSQjNMnqKKzjKvyRK507usXbikQVmA',
			'cded5112',
			2,
			1,
			@ScreenName,
			1,
			1,
			GETDATE(),
			GETDATE()
		)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		INSERT INTO [Data.Application.UserRole]
		(
			Id,
			RoleId,
			UserId
		)
		VALUES
		(
			@UserRoleId,
			@RoleId,
			@UserId
		)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		INSERT INTO [Data.Application.Employer]
		(
			Id,
			Name,
			CommencedOn,
			FederalEmployerIdentificationNumber,
			IsValidFederalEmployerIdentificationNumber,
			StateEmployerIdentificationNumber,
			ApprovalStatus,
			Url,
			ExpiredOn,
			OwnershipTypeId,
			IndustrialClassification,
			TermsAccepted,
			PrimaryPhone,
			PrimaryPhoneExtension,
			PrimaryPhoneType,
			AlternatePhone1,
			AlternatePhone1Type,
			AlternatePhone2,
			AlternatePhone2Type,
			IsRegistrationComplete,
			CreatedOn,
			UpdatedOn
		)
		VALUES
		(
			@EmployerId,
			@EmployerName,
			'01 January 2014',
			'11-7777777',
			1,
			'',
			2,
			'',
			NULL,
			@OwnershipTypeId,
			'11194 - Hay Farming',
			1,
			'1111111111',
			'',
			'Phone',
			'',
			'Phone',
			'',
			'Phone',
			1,
			GETDATE(),
			GETDATE()
		)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		INSERT INTO [Data.Application.EmployerAddress]
		(
			Id,
			EmployerId,
			Line1,
			Line2,
			Line3,
			TownCity,
			CountyId,
			PostcodeZip,
			StateId,
			CountryId,
			IsPrimary,
			PublicTransitAccessible
		)
		VALUES
		(
			@EmployerAddressId,
			@EmployerId,
			'Address Line 1',
			'',
			'',
			'Bluegrove',
			@CountyId,
			'76352',
			@StateId,
			@CountryId,
			1,
			1
		)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		INSERT INTO [Data.Application.BusinessUnit]
		(
			Id,
			EmployerId,
			Name,
			IsPrimary,
			Url,
			OwnershipTypeId,
			IndustrialClassification,
			PrimaryPhone,
			PrimaryPhoneExtension,
			PrimaryPhoneType,
			AlternatePhone1,
			AlternatePhone1Type,
			AlternatePhone2,
			AlternatePhone2Type,
			IsPreferred
		)
		VALUES
		(
			@BusinessUnitId,
			@EmployerId,
			@EmployerName,
			1,
			'',
			@OwnershipTypeId,
			'11194 - Hay Farming',
			'1111111111',
			'',
			'Phone',
			'',
			'Phone',
			'',
			'Phone',
			1
		)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		INSERT INTO [Data.Application.BusinessUnitAddress]
		(
			Id,
			BusinessUnitId,
			Line1,
			Line2,
			Line3,
			TownCity,
			CountyId,
			PostcodeZip,
			StateId,
			CountryId,
			IsPrimary,
			PublicTransitAccessible
		)
		VALUES
		(
			@BusinessUnitAddressId,
			@BusinessUnitId,
			'Address Line 1',
			'',
			'',
			'Bluegrove',
			@CountyId,
			'76352',
			@StateId,
			@CountryId,
			1,
			1
		)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		INSERT INTO [Data.Application.BusinessUnitDescription]
		(
			[Id],
			[BusinessUnitId],
			[Title],
			[Description],
			[IsPrimary]
		)
		VALUES
		(
			@BusinessUnitDescriptionId,
			@BusinessUnitId,
			@CompanyDescription,
			@CompanyDescription,
			1
		)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		INSERT INTO [Data.Application.Employee]
		(
			Id,
			EmployerId,
			ApprovalStatus,
			StaffUserId,
			PersonId,
			RedProfanityWords,
			YellowProfanityWords,
			ApprovalStatusChangedBy,
			ApprovalStatusChangedOn
		)
		VALUES
		(
			@EmployeeId,
			@EmployerId,
			@EmployeeApprovalStatus,
			NULL,
			@PersonId,
			@RedWords,
			@YellowWords,
			CASE 
				WHEN @ApprovalStatusChangedBy IS NOT NULL THEN @ApprovalStatusChangedBy 
				WHEN @EmployeeApprovalStatus IN (3, 4) THEN @AssistUserId 
				ELSE NULL 
			END,
			CASE 
				WHEN @ApprovalStatusChangedOn IS NOT NULL THEN @ApprovalStatusChangedOn 
				WHEN @EmployeeApprovalStatus IN (3, 4) THEN GETDATE() 
				ELSE NULL 
			END
		)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		INSERT INTO [Data.Application.EmployeeBusinessUnit]
		(
			Id,
			[Default],
			[BusinessUnitId],
			[EmployeeId]
		)
		VALUES
		(
			@EmployeeBusinessUnitId,
			1,
			@BusinessUnitId,
			@EmployeeId
		)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		COMMIT TRANSACTION
	END
	SET @EmployerCount = @EmployerCount + 1
END

