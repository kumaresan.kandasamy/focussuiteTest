﻿-- Assist Users

DECLARE @AssistUsers TABLE
(
	Id BIGINT IDENTITY(1, 1),
	FirstName NVARCHAR(40),
	LastName NVARCHAR(40),
	EmailAddress NVARCHAR(255),
	PhoneNumber NVARCHAR(20),
	RoleKeyToExclude NVARCHAR(100)
)

INSERT INTO @AssistUsers (FirstName, LastName, EmailAddress, PhoneNumber, RoleKeyToExclude)
VALUES 
	('FVN935', 'NoJobSeekerSendMessagePermissions', 'FVN935.NoJobSeekerSendMessagePermissions@bgt.com', '87894424245', 'AssistJobSeekersSendMessages'),
	('FVN935', 'JobSeekerSendMessagePermissions', 'FVN935.JobSeekerSendMessagePermissions@bgt.com', '87896544245', ''),
	('FVN934', 'NoEmployerSendMessagePermissions', 'FVN934.NoEmployerSendMessagePermissions@bgt.com', '87893214245', 'AssistEmployersSendMessages'),
	('FVN934', 'EmployerSendMessagePermissions', 'FVN934.EmployerSendMessagePermissions@bgt.com', '87894357245', '')
	
DECLARE @AssistUserIndex INT
DECLARE @AssistUserCount INT

DECLARE @NextId BIGINT

DECLARE @PersonId BIGINT
DECLARE @PersonAddressId BIGINT
DECLARE @PhoneNumberId BIGINT
DECLARE @UserId BIGINT
DECLARE @UserRoleId BIGINT
DECLARE @RoleId BIGINT
DECLARE @PersonOfficeMapperId BIGINT

DECLARE @TitleId BIGINT
DECLARE @CountyId BIGINT
DECLARE @StateId BIGINT
DECLARE @CountryId BIGINT

DECLARE @FirstName NVARCHAR(40)
DECLARE @LastName NVARCHAR(40)
DECLARE @EmailAddress NVARCHAR(255)
DECLARE @PhoneNumber NVARCHAR(20)
DECLARE @RoleKeyToExclude NVARCHAR(100)
	
SELECT @TitleId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Titles' AND [Key] = 'Title.Mr'
SELECT @RoleId = Id FROM [Data.Application.Role] WHERE [Key] = 'AssistUser'
SELECT @CountyId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Counties' AND [Key] = 'County.ClayTX'
SELECT @StateId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'States' AND [Key] = 'State.TX'
SELECT @CountryId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Countries' AND [Key] = 'Country.US'

SET @AssistUserIndex = 1
SELECT @AssistUserCount = COUNT(1) FROM @AssistUsers

WHILE @AssistUserIndex <= @AssistUserCount
BEGIN
	SELECT 
		@FirstName = FirstName,
		@LastName = LastName,
		@EmailAddress = EmailAddress,
		@PhoneNumber = PhoneNumber,
		@RoleKeyToExclude = RoleKeyToExclude
	FROM
		@AssistUsers
	WHERE
		Id = @AssistUserIndex
		
	IF NOT EXISTS(SELECT 1 FROM [Data.Application.Person] WHERE EmailAddress = @EmailAddress)
	BEGIN
		BEGIN TRANSACTION
		
		SELECT @NextId = NextId FROM KeyTable
		
		UPDATE KeyTable SET NextId = NextId + 5 + (SELECT COUNT(1) FROM [Data.Application.Role] R WHERE	R.[Key] NOT IN (@RoleKeyToExclude, 'SystemAdmin'))
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		SET @PersonId = @NextId
		SET @PersonAddressId = @NextId + 1
		SET @PhoneNumberId = @NextId + 2
		SET @UserId = @NextId + 3
		SET @PersonOfficeMapperId = @NextId + 4
		SET @UserRoleId = @NextId + 5
		
		INSERT INTO [Data.Application.Person]
		(
			Id,
			TitleId,
			FirstName,
			MiddleInitial,
			LastName,
			DateOfBirth,
			SocialSecurityNumber,
			EmailAddress,
			EnrollmentStatus,
			CampusId
		)
		VALUES
		(
			@PersonId,
			@TitleId,
			@FirstName,
			'',
			@LastName,
			NULL,
			NULL,
			@EmailAddress,
			NULL,
			NULL
		)

		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		INSERT INTO [Data.Application.PersonAddress]
		(
			Id,
			PersonId,
			Line1,
			Line2,
			TownCity,
			StateId ,
			CountyId,
			CountryId,
			PostcodeZip,
			IsPrimary
		)
		VALUES
		(
			@PersonAddressId,
			@PersonId,
			'Address Line 1',
			'',
			'Bluegrove',
			@StateId,
			@CountyId,
			@CountryId,
			'76352',
			1
		)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
			
		INSERT INTO [Data.Application.PhoneNumber]
		(
			Id,
			PersonId,
			Number,
			PhoneType,
			IsPrimary,
			Extension,
			ProviderId
		)
		VALUES
		(
			@PhoneNumberId,
			@PersonId,
			@PhoneNumber,
			0,
			1,
			NULL,
			NULL
		)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
			
		INSERT INTO [Data.Application.User]
		(
			Id,
			PersonId,
			UserName,
			PasswordHash,
			PasswordSalt,
			UserType,
			[Enabled],
			ScreenName,
			IsMigrated,
			RegulationsConsent,
			CreatedOn,
			UpdatedOn,
			ValidationKey,
			ValidationKeyExpiry
		)
		VALUES
		(
			@UserId,
			@PersonId,
			@EmailAddress,
			'I/mugkpu/aiJINDf7aBlKfYCSI3PjN/UViGvmkNS9RIM1BmUdDEPZFKimSQjNMnqKKzjKvyRK507usXbikQVmA',
			'cded5112',
			1,
			1,
			@FirstName + ' ' + @LastName,
			1,
			1,
			GETDATE(),
			GETDATE(),
			NULL,
			NULL
		)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		INSERT INTO [Data.Application.PersonOfficeMapper]
		(
			Id,
			PersonId,
			StateId,
			CreatedOn
		)
		VALUES
		(
			@PersonOfficeMapperId,
			@PersonId,
			@StateId,
			GETDATE()
		)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		INSERT INTO [Data.Application.UserRole]
		(
			Id,
			RoleId,
			UserId
		)
		SELECT
			@UserRoleId + ROW_NUMBER() OVER (ORDER BY R.Id ASC),
			R.Id,
			@UserId
		FROM
			[Data.Application.Role] R
		WHERE
			R.[Key] NOT IN (@RoleKeyToExclude, 'SystemAdmin')
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
			
		COMMIT TRANSACTION
	END
	
	SET @AssistUserIndex = @AssistUserIndex + 1
END
