﻿DECLARE @JobTitle NVARCHAR(200)

DECLARE @EmailIndex INT
DECLARE @EmailCount INT

DECLARE @AIIndex INT
DECLARE @AICount INT

DECLARE @AIJobId BIGINT
DECLARE @AILensId NVARCHAR(MAX)
DECLARE @AIPersonId BIGINT

DECLARE @NextId BIGINT

DECLARE @JobId BIGINT
DECLARE @LensId NVARCHAR(MAX)
DECLARE @PersonId BIGINT
DECLARE @Email NVARCHAR(200)

DECLARE @EmailAddresses TABLE
(
	Id BIGINT IDENTITY(1, 1),
	Email NVARCHAR(200)
)

INSERT INTO @EmailAddresses 
VALUES
('fvn547@invce1.com'), 
('fvn547@invce2.com'), 
('fvn547@appce1.com'),
('fvn547@appce2.com')	

DECLARE @ApplicationInvites TABLE
(
	Id BIGINT IDENTITY(1, 1),
	JobId BIGINT,
	LensID NVARCHAR(MAX),
	PersonId BIGINT
)

SET @JobTitle = 'FVN-547 Job invitees and applicants'
SELECT @JobId = [Id] FROM [Data.Application.Job] WHERE [JobTitle] = @JobTitle
SELECT @LensId = [LensPostingId] FROM [Data.Application.Posting] WHERE [JobId] = @JobId

SET @EmailIndex = 1
SELECT @EmailCount = COUNT(1) FROM @EmailAddresses

WHILE @EmailIndex <= @EmailCount
BEGIN
	SELECT @Email = Email
	FROM 
		@EmailAddresses
	WHERE
		Id = @EmailIndex

	SELECT @PersonId = [Id] FROM [Data.Application.Person] WHERE [EmailAddress] = @Email
	
	INSERT INTO 
		@ApplicationInvites 
	(
		JobId,
		LensId,
		PersonId
	)
	VALUES
	(
		@JobId,
		@LensId,
		@PersonId
	)
		
	SET @EmailIndex = @EmailIndex + 1
END

SET @AIIndex = 1
SELECT @AICount = COUNT(1) FROM @ApplicationInvites

SET @AIJobId = NULL
SET @AILensId = NULL
SET @AIPersonId = NULL

WHILE @AIIndex <= @AICount
BEGIN
	SELECT 
		@AIJobId = JobId,
		@AILensId = LensId,
		@AIPersonId = PersonId
	FROM
		@ApplicationInvites
	WHERE
		[Id] = @AIIndex
	
	BEGIN TRANSACTION
		
	SELECT @NextId = NextId FROM KeyTable
	
	UPDATE KeyTable SET NextId = NextId + 1
	
	IF @@ERROR <> 0
	BEGIN
		ROLLBACK TRANSACTION
		RETURN
	END
	
	INSERT INTO [Data.Application.InviteToApply]
	(
		Id,
		LensPostingId,
		Viewed,
		CreatedBy,
		CreatedOn,
		UpdatedOn,
		JobId,
		PersonId
	)
	VALUES
	(
		@NextId,
		@AILensId,
		0,
		4760058,
		GETDATE(),
		GETDATE(),
		@AIJobId,
		@AIPersonId
	)
	
	IF @@ERROR <> 0
	BEGIN
		ROLLBACK TRANSACTION
		RETURN
	END
	
	COMMIT TRANSACTION
	
	SET @AIIndex = @AIIndex + 1
END
