IF NOT EXISTS(SELECT 1 FROM [Config.ConfigurationItem] WHERE [Key] = 'NearbyStateKeys')
BEGIN
	INSERT INTO [Config.ConfigurationItem] ([Key], Value)
	VALUES ('NearbyStateKeys', 'State.IL,State.IN,State.KY,State.MO,State.OH,State.TN,State.TX,State.VA,State.WV')
END