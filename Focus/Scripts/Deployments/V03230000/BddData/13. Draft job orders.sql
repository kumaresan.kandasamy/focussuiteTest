DECLARE @Jobs TABLE
(
	Id BIGINT IDENTITY(1, 1),
	JobTitle NVARCHAR(40),
	EmployeeEmailAddress NVARCHAR(255),
	BusinessUnitId BIGINT,
	EmployeeId BIGINT,
	EmployeeUserId BIGINT,
	EmployerId BIGINT,
	BusinessUnitDescriptionId BIGINT,
	EmployerName NVARCHAR(255),
	OnetCode NVARCHAR(30),
	ROnetCode NVARCHAR(30)
)

INSERT INTO @Jobs (JobTitle, EmployeeEmailAddress, OnetCode, ROnetCode)
VALUES 
	('A draft workforce job', 'dev@employer.com', '11-3042.00', NULL),
	('A draft education job', 'dev@employer.com', '21-2021.00', '21-2021.00')
	
DECLARE @NextId BIGINT

DECLARE @JobTitle NVARCHAR(50)

DECLARE @JobId BIGINT
DECLARE @JobAddressId BIGINT
DECLARE @JobLocationId BIGINT
DECLARE @BusinessUnitId BIGINT
DECLARE @EmployeeId BIGINT
DECLARE @EmployeeUserId BIGINT
DECLARE @EmployerId BIGINT
DECLARE @BusinessUnitDescriptionId BIGINT
DECLARE @EmployerName NVARCHAR(255)

DECLARE @CountyId BIGINT
DECLARE @StateId BIGINT
DECLARE @CountryId BIGINT

DECLARE @OnetCode NVARCHAR(30)
DECLARE @ROnetCode NVARCHAR(30)
DECLARE @OnetId BIGINT
DECLARE @ROnetId BIGINT

SELECT @CountyId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Counties' AND [Key] = 'County.ClayTX'
SELECT @StateId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'States' AND [Key] = 'State.TX'
SELECT @CountryId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Countries' AND [Key] = 'Country.US'

UPDATE
	TJ
SET
	BusinessUnitId = EBU.BusinessUnitId,
	EmployeeId = E.Id,
	EmployeeUserId = U.Id,
	EmployerId = E.EmployerId,
	BusinessUnitDescriptionId = BUD.Id,
	EmployerName = BU.Name
FROM
	@Jobs TJ
INNER JOIN [Data.Application.Person] P
	ON P.EmailAddress = TJ.EmployeeEmailAddress
INNER JOIN [Data.Application.User] U
	ON U.PersonId = P.Id
INNER JOIN [Data.Application.Employee] E
	ON E.PersonId = P.Id
INNER JOIN [Data.Application.EmployeeBusinessUnit] EBU
	ON EBU.EmployeeId = E.Id
	AND EBU.[Default] = 1
INNER JOIN [Data.Application.BusinessUnit] BU
	ON BU.Id = EBU.BusinessUnitId
INNER JOIN [Data.Application.BusinessUnitDescription] BUD
	ON BUD.BusinessUnitId = EBU.BusinessUnitId
	AND BUD.IsPrimary = 1

DECLARE @JobCount INT
DECLARE @TotalJobs INT

SET @JobCount = 1
SELECT @TotalJobs = COUNT(1) FROM @Jobs

WHILE @JobCount <= @TotalJobs
BEGIN
	SELECT 
		@JobTitle = JobTitle,
		@BusinessUnitId = BusinessUnitId,
		@EmployeeId = EmployeeId,
		@EmployeeUserId = EmployeeUserId,
		@EmployerId = EmployerId,
		@BusinessUnitDescriptionId = BusinessUnitDescriptionId,
		@EmployerName = EmployerName,
		@OnetCode = OnetCode,
		@ROnetCode = ROnetCode
	FROM
		@Jobs
	WHERE
		Id = @JobCount
	
	IF @BusinessUnitId IS NOT NULL
	AND NOT EXISTS(SELECT 1 FROM [Data.Application.Job] WHERE JobTitle = @JobTitle AND BusinessUnitId = @BusinessUnitId)
	BEGIN
		SET @OnetId = NULL
		SET @ROnetId = NULL
		
		SELECT @OnetId = Id FROM [Library.Onet] WHERE OnetCode = @OnetCode
		SELECT @ROnetId = Id FROM [Library.ROnet] WHERE Code = @ROnetCode
		
		BEGIN TRANSACTION
		
		SELECT @NextId = NextId FROM KeyTable
		
		UPDATE KeyTable SET NextId = NextId + 3
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		SET @JobId = @NextId
		SET @JobAddressId = @NextId + 1
		SET @JobLocationId = @NextId + 2
		
		INSERT INTO [Data.Application.Job]
		(
			Id,
			JobTitle,
			CreatedBy,
			UpdatedBy,
			ApprovalStatus,
			JobStatus,
			MinSalary,
			MaxSalary,
			SalaryFrequencyId,
			HoursPerWeek,
			OverTimeRequired,
			ClosingOn,
			NumberOfOpenings,
			PostedOn,
			PostedBy,
			[Description],
			PostingHtml,
			EmployerDescriptionPostingPosition,
			WizardStep,
			WizardPath,
			HeldOn,
			HeldBy,
			ClosedOn,
			ClosedBy,
			JobLocationType,
			FederalContractor,
			ForeignLabourCertification,
			CourtOrderedAffirmativeAction,
			FederalContractorExpiresOn,
			WorkOpportunitiesTaxCreditHires,
			PostingFlags,
			IsCommissionBased,
			NormalWorkDays,
			WorkDaysVary,
			NormalWorkShiftsId,
			LeaveBenefits,
			RetirementBenefits,
			InsuranceBenefits,
			MiscellaneousBenefits,
			OtherBenefitsDetails,
			InterviewContactPreferences,
			InterviewEmailAddress,
			InterviewApplicationUrl,
			InterviewMailAddress,
			InterviewFaxNumber,
			InterviewPhoneNumber,
			InterviewDirectApplicationDetails,
			InterviewOtherInstructions,
			ScreeningPreferences,
			MinimumEducationLevel,
			MinimumEducationLevelRequired,
			MinimumAge,
			MinimumAgeReason,
			MinimumAgeRequired,
			LicencesRequired,
			CertificationRequired,
			LanguagesRequired,
			Tasks,
			RedProfanityWords,
			YellowProfanityWords,
			EmploymentStatusId,
			JobTypeId,
			JobStatusId,
			ApprovedOn,
			ApprovedBy,
			ExternalId,
			AwaitingApprovalOn,
			MinimumExperience,
			MinimumExperienceMonths,
			MinimumExperienceRequired,
			DrivingLicenceClassId,
			DrivingLicenceRequired,
			HideSalaryOnPosting,
			ForeignLabourCertificationH2A,
			ForeignLabourCertificationH2B,
			ForeignLabourCertificationOther,
			HiringFromTaxCreditProgramNotificationSent,
			IsConfidential,
			HasCheckedCriminalRecordExclusion,
			LastPostedOn,
			LastRefreshedOn,
			MinimumAgeReasonValue,
			OtherSalary,
			HideOpeningsOnPosting,
			StudentEnrolled,
			MinimumCollegeYears,
			CreatedOn,
			UpdatedOn,
			LockVersion,
			BusinessUnitId,
			EmployeeId,
			EmployerId,
			BusinessUnitDescriptionId,
			BusinessUnitLogoId,
			ProgramsOfStudyRequired,
			StartDate,
			EndDate,
			JobType,
			OnetId,
			HideEducationOnPosting,
			HideExperienceOnPosting,
			HideMinimumAgeOnPosting,
			HideProgramOfStudyOnPosting,
			HideDriversLicenceOnPosting,
			HideLicencesOnPosting,
			HideCertificationsOnPosting,
			HideLanguagesOnPosting,
			HideSpecialRequirementsOnPosting,
			HideWorkWeekOnPosting,
			DescriptionPath,
			WorkWeekId,
			AssignedToId,
			CriminalBackgroundExclusionRequired,
			ROnetId,
			IsSalaryAndCommissionBased,
			VeteranPriorityEndDate,
			MeetsMinimumWageRequirement
		)
		VALUES
		(
			@JobId,
			@JobTitle,
			@EmployeeUserId,
			@EmployeeUserId,
			0,
			0,
			NULL,
			NULL,
			0,
			NULL,
			0,
			'28 February 2015',
			2,
			GETDATE(),
			@EmployeeUserId,
			'Test Job Description',
			'<html><body><p><b>' + @JobTitle + '</b><br>' + @EmployerName + '<br>Bluegrove, TX (76352)(public transit accessible)<br>Number of openings: 2<br>Application closing date: 22/07/2015<br></p><p>Test Job Description</p><p><b>Benefits</b><br>No benefits are offered with this job</p><p><b>About ' + @EmployerName + '</b><br>Company Description</p><p><b>How to apply</b><br><a href="http://localhost:57990" target="_blank">Log in to Focus/Career and submit your resume</a></p><p><small>REF/4760944/4761031</small></p></body>#POSTINGFOOTER#</html>',
			0,
			7,
			22,
			NULL,
			NULL,
			NULL,
			NULL,
			0,
			0,
			NULL,
			0,
			NULL,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			'',
			64,
			'',
			'',
			'',
			'',
			'',
			'',
			'',
			0,
			0,
			0,
			NULL,
			'',
			0,
			0,
			0,
			0,
			NULL,
			NULL,
			NULL,
			0,
			0,
			0,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			NULL,
			0,
			NULL,
			0,
			0,
			0,
			0,
			0,
			NULL,
			0,
			0,
			GETDATE(),
			NULL,
			0,
			NULL,
			NULL,
			0,
			NULL,
			GETDATE(),
			GETDATE(),
			10,
			@BusinessUnitId,
			@EmployeeId,
			@EmployerId,
			@BusinessUnitDescriptionId,
			NULL,
			NULL,
			NULL,
			NULL,
			1,
			@OnetId,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			0,
			1,
			NULL,
			NULL,
			0,
			@ROnetId,
			0,
			NULL,
			1
		)

		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		INSERT INTO [Data.Application.JobAddress]
		(
			Id,
			JobId,
			Line1,
			Line2,
			Line3,
			TownCity,
			CountyId,
			PostcodeZip,
			StateId,
			CountryId,
			IsPrimary
		)
		VALUES
		(
			@JobAddressId,
			@JobId,
			'Address Line 1',
			'',
			'',
			'Bluegrove',
			@CountyId,
			'76352',
			@StateId,
			@CountryId,
			1
		)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		INSERT INTO [Data.Application.JobLocation]
		(
			Id,
			JobId,
			Location,
			IsPublicTransitAccessible
		)
		VALUES
		(
			@JobLocationId,
			@JobId,
			'Bluegrove, TX (76352)',
			1
		)
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		COMMIT TRANSACTION
	END
	
	SET @JobCount = @JobCount + 1
END