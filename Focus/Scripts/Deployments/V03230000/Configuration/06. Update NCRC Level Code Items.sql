DECLARE @Level TABLE
(
	LevelKey NVARCHAR(20),
	ExternalId NVARCHAR(36)
)
INSERT INTO @Level (LevelKey, ExternalId)
VALUES 
	('NCRCLevel.Bronze', '1'),
	('NCRCLevel.Silver', '2'),
	('NCRCLevel.Gold', '3'),
	('NCRCLevel.Platinum', '4')
	
UPDATE
	CI
SET
	ExternalId = L.ExternalId
FROM
	[Config.CodeItem] CI
INNER JOIN @Level L
	ON L.LevelKey = CI.[Key]
WHERE
	CI.ExternalId IS NULL

