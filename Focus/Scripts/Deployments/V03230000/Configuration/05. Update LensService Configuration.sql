UPDATE
	[Config.ConfigurationItem] 
SET
	Value = REPLACE(Value, '{"key":"HomeBased","tag":"cf027"}', '{"key":"HomeBased","tag":"cf027"},{"key":"NCRCLevel","tag":"cf028"}')
WHERE [Key] = 'LensService'
	AND Value LIKE '%customFilters%'
	AND Value LIKE '%"serviceType":"Resume"%'
	AND Value NOT LIKE '%NCRCLevel%'

