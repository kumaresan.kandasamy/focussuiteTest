DECLARE @Key NVARCHAR(10)

SET @Key = 'NCRCLevel'

IF NOT EXISTS(SELECT 1 FROM [Config.CodeGroup] WHERE [Key] = @Key)
BEGIN
	INSERT INTO [Config.CodeGroup] ([Key])
	VALUES (@Key)
END

DECLARE @Level TABLE
(
	[Key] NVARCHAR(200),
	Value NVARCHAR(1000),
	DisplayOrder INT
)
INSERT INTO @Level ([Key], Value, DisplayOrder)
VALUES 
	(@Key + '.Bronze', 'Bronze', 1),
	(@Key + '.Silver', 'Silver', 2),
	(@Key + '.Gold', 'Gold', 3),
	(@Key + '.Platinum', 'Platinum', 4)	
		
INSERT INTO [Config.CodeItem] 
(
	[Key]
)
SELECT
	L.[Key]
FROM
	@Level L
WHERE
	NOT EXISTS
	(
		SELECT
			1
		FROM
			[Config.CodeItem] CI
		WHERE
			CI.[Key] = L.[Key]
	)
	
INSERT INTO [Config.CodeGroupItem] (CodeGroupId, CodeItemId, DisplayOrder)
SELECT
	CG.Id,
	CI.Id,
	L.DisplayOrder
FROM
	[Config.CodeGroup] CG
CROSS JOIN @Level L
INNER JOIN [Config.CodeItem] CI
	ON CI.[Key] = L.[Key]
WHERE
	CG.[Key] = @Key
	AND NOT EXISTS
	(
		SELECT
			1
		FROM
			[Config.CodeGroupItem] CGI
		WHERE 
			CGI.CodeGroupId = CG.Id
			AND CGI.CodeItemId = CI.Id
	)

DECLARE @LocalisationId BIGINT

SELECT 
	@LocalisationId = L.Id 
FROM 
	[Config.Localisation] L
WHERE
	L.Culture = '**-**'

INSERT INTO [Config.LocalisationItem] 
(
	[Key],
	Value,
	LocalisationId
)
SELECT
	L.[Key],
	L.Value,
	@LocalisationId
FROM
	@Level L
WHERE
	NOT EXISTS
	(
		SELECT
			1
		FROM
			[Config.LocalisationItem] LI
		WHERE
			LI.[Key] = L.[Key]
	)
