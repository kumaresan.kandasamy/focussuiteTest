﻿
UPDATE [Config.LocalisationItem]
SET [Value] ='Hourly "Non-exempt"'
WHERE [KEY] = 'JobStatuses.Hourly' 

UPDATE [Config.LocalisationItem]
SET [Value] ='Contractor'
WHERE [KEY] = 'JobStatuses.Contractor' 