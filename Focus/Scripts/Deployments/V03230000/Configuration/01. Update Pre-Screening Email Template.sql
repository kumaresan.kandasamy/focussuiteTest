UPDATE
	[Config.EmailTemplate]
SET
	Salutation = 'Dear #OFFICENAME#'
WHERE
	EmailTemplateType = 39
	AND Salutation = 'Dear #RECIPIENTNAME#'