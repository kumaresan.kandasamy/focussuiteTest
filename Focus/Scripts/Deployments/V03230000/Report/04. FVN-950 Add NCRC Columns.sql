﻿SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS(select * from sys.columns where Name = N'NCRCLevel' and Object_ID = Object_ID(N'dbo.[Report.JobSeeker]'))
BEGIN
    alter table dbo.[Report.JobSeeker]
    add NCRCLevel nvarchar(50) null
END
GO