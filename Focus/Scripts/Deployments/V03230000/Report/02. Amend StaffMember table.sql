IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.StaffMember' AND COLUMN_NAME = 'ExternalStaffID')
BEGIN
	ALTER TABLE
		[Report.StaffMember]
	ADD 
		ExternalStaffID NVARCHAR(255)
END
GO

UPDATE
	RSM
SET
	ExternalStaffID = U.ExternalId
FROM
	[Report.StaffMember] RSM
INNER JOIN [Data.Application.User] U
	ON U.PersonId = RSM.FocusPersonId
