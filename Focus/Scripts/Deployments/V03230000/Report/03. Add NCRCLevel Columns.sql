﻿SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF NOT EXISTS(select * from sys.columns where Name = N'NCRCLevel' and Object_ID = Object_ID(N'dbo.[Report.JobOrder]'))
BEGIN
    ALTER TABLE dbo.[Report.JobOrder]
    ADD NCRCLevel nvarchar(50) null
END
GO

IF NOT EXISTS(select * from sys.columns where Name = N'NCRCLevelRequired' and Object_ID = Object_ID(N'dbo.[Report.JobOrder]'))
BEGIN
    ALTER TABLE dbo.[Report.JobOrder]
    ADD NCRCLevelRequired bit null
END
GO

IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'Report.JobOrderView')
BEGIN
	DROP VIEW [dbo].[Report.JobOrderView]
END
GO

CREATE VIEW [dbo].[Report.JobOrderView]
AS
	SELECT
		J.Id,
		J.FocusJobId,
		J.EmployerId,
		J.JobStatus,
		J.CountyId,
		J.County,
		J.StateId,
		J.[State],
		J.PostalCode,
		J.Office,
		J.MinimumEducationLevel,
		J.PostingFlags,
		J.WorkOpportunitiesTaxCreditHires,
		J.ForeignLabourCertification,
		J.ScreeningPreferences,
		J.MinSalary,
		J.MaxSalary,
		J.SalaryFrequency,
		J.OnetCode,
		J.PostedOn,
		J.ClosingOn,
		J.LatitudeRadians,
		J.LongitudeRadians,
		J.CreatedOn,
		J.UpdatedOn,
		J.JobTitle,
		J.[Description],
		J.ForeignLabourType,
		J.FederalContractor,
		J.CourtOrderedAffirmativeAction,
		J.NCRCLevel,
		J.NCRCLevelRequired,
		E.[Name] AS EmployerName,
		E.FederalEmployerIdentificationNumber,
		E.AccountTypeId		
	FROM 
		[Report.JobOrder] J
	INNER JOIN [Report.Employer] E
		ON E.Id = J.EmployerId
GO

IF  EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[Data.Application.JobOrderReportView]'))
	DROP VIEW [dbo].[Data.Application.JobOrderReportView]
GO

CREATE VIEW [dbo].[Data.Application.JobOrderReportView]
AS
	SELECT 
		J.Id,
		J.BusinessUnitId,
		J.OnetId,
		J.JobStatus,
		JA.CountyId,
		JA.StateId,
		JA.PostcodeZip,
		J.JobTitle,
		J.MinimumEducationLevel,
		J.PostingFlags,
		J.WorkOpportunitiesTaxCreditHires,
		J.ForeignLabourCertification,
		J.ForeignLabourCertificationH2A,
		J.ForeignLabourCertificationH2B,
		J.ForeignLabourCertificationOther,
		J.FederalContractor,
		J.CourtOrderedAffirmativeAction,
		J.ScreeningPreferences,
		J.SalaryFrequencyId,
		J.MinSalary,
		J.MaxSalary,
		J.PostedOn,
		J.ClosingOn,
		J.[Description],
		J.CareerReadinessLevel,
		J.CareerReadinessLevelRequired
	FROM
		[Data.Application.Job] J
	LEFT OUTER JOIN [Data.Application.JobAddress] JA
		ON JA.JobId = J.Id
		AND JA.IsPrimary = 1
GO