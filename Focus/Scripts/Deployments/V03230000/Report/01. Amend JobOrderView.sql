IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'Report.JobOrderView')
BEGIN
	DROP VIEW [dbo].[Report.JobOrderView]
END
GO

CREATE VIEW [dbo].[Report.JobOrderView]

AS
	SELECT
		J.Id,
		J.FocusJobId,
		J.EmployerId,
		J.JobStatus,
		J.CountyId,
		J.County,
		J.StateId,
		J.[State],
		J.PostalCode,
		J.Office,
		J.MinimumEducationLevel,
		J.PostingFlags,
		J.WorkOpportunitiesTaxCreditHires,
		J.ForeignLabourCertification,
		J.ScreeningPreferences,
		J.MinSalary,
		J.MaxSalary,
		J.SalaryFrequency,
		J.OnetCode,
		J.PostedOn,
		J.ClosingOn,
		J.LatitudeRadians,
		J.LongitudeRadians,
		J.CreatedOn,
		J.UpdatedOn,
		J.JobTitle,
		J.[Description],
		J.ForeignLabourType,
		J.FederalContractor,
		J.CourtOrderedAffirmativeAction,
		E.[Name] AS EmployerName,
		E.FederalEmployerIdentificationNumber,
		E.AccountTypeId		
	FROM 
		[Report.JobOrder] J
	INNER JOIN [Report.Employer] E
		ON E.Id = J.EmployerId
GO
