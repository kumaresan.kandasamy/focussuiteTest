IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Office' AND COLUMN_NAME = 'DefaultType')
BEGIN
	ALTER TABLE
		[Data.Application.Office]
	ADD
		DefaultType INT NOT NULL DEFAULT(0)
END
GO
