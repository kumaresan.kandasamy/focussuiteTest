IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'Data.Application.StaffMemberReportView')
BEGIN
	DROP VIEW [dbo].[Data.Application.StaffMemberReportView]
END 
GO


CREATE VIEW [dbo].[Data.Application.StaffMemberReportView]

AS
	SELECT 
		P.Id,
		P.FirstName,
		P.LastName,
		P.EmailAddress,
		PA.Line1,
		PA.CountyId,
		PA.StateId,
		PA.PostcodeZip,
		U.CreatedOn,
		U.Blocked,
		U.ExternalId
	FROM
		[Data.Application.Person] P
	INNER JOIN [Data.Application.User] U
		ON U.PersonId = P.Id
	LEFT OUTER JOIN [Data.Application.PersonAddress] PA
		ON PA.PersonId = P.Id
		AND PA.IsPrimary = 1
	WHERE
		U.UserType = 1
GO

