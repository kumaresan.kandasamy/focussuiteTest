﻿/****** Add send messages roles to role table ******/
DECLARE @NextID INT

IF NOT EXISTS(SELECT 1 FROM [Data.Application.Role] WHERE [Key] = 'AssistJobSeekersSendMessages')
BEGIN
	SELECT @NextID = NextId FROM [dbo].[KeyTable]
	
	INSERT INTO [dbo].[Data.Application.Role] (Id, [Key], Value)
  VALUES
  ((@NextID + 1), 'AssistJobSeekersSendMessages', 'Assist Job Seekers Send Messages')
	
	UPDATE [KeyTable] SET NextId = NextId + 2
END

IF NOT EXISTS(SELECT 1 FROM [Data.Application.Role] WHERE [Key] = 'AssistEmployersSendMessages')
BEGIN
	SELECT @NextID = NextId FROM [dbo].[KeyTable]
	
	INSERT INTO [dbo].[Data.Application.Role] (Id, [Key], Value)
  VALUES
  ((@NextID + 1), 'AssistEmployersSendMessages', 'Assist Employers Send Messages')
	
	
	UPDATE [KeyTable] SET NextId = NextId + 2
END