IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Job' AND COLUMN_NAME = 'HideCareerReadinessOnPosting')
BEGIN
	ALTER TABLE
		[Data.Application.Job]
	ADD 
		HideCareerReadinessOnPosting BIT DEFAULT(0) NOT NULL
END

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Job' AND COLUMN_NAME = 'CareerReadinessLevelRequired')
BEGIN
	ALTER TABLE
		[Data.Application.Job]
	ADD 
		CareerReadinessLevelRequired BIT NULL
END

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.Job' AND COLUMN_NAME = 'CareerReadinessLevel')
BEGIN
	ALTER TABLE
		[Data.Application.Job]
	ADD 
		CareerReadinessLevel BIGINT NULL
END
