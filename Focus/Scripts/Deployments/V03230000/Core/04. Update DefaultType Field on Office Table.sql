IF NOT EXISTS(SELECT 1 FROM [Data.Application.Office] WHERE DefaultType <> 0)
BEGIN
	UPDATE
		[Data.Application.Office]
	SET
		DefaultType = CASE ISNULL(UnassignedDefault, 0) WHEN 1 THEN 7 ELSE 0 END
END