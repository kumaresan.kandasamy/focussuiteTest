DECLARE @Role TABLE
(
	[Key] NVARCHAR(200),
	Value NVARCHAR(200),
	AssignKey NVARCHAR(200)
)

DECLARE @NextID BIGINT
DECLARE @RowCount INT

SELECT @NextID = NextId FROM KeyTable

INSERT INTO @Role ([Key], Value, AssignKey)
VALUES 
	('AssistJobSeekerReportsViewOnly', 'Assist Job Seeker Reports View Only', 'AssistJobSeekerReports'),
	('AssistJobOrderReportsViewOnly', 'Assist Job Order Reports View Only', 'AssistJobOrderReports'),
	('AssistEmployerReportsViewOnly', 'Assist Employer/Hiring Manager Reports View Only', 'AssistEmployerReports')

INSERT INTO [Data.Application.Role] (Id, [Key], Value)
SELECT
	@NextId - 1 + ROW_NUMBER() OVER(ORDER BY TR.[Key] ASC),
	[Key],
	Value
FROM
	@Role TR
WHERE
	NOT EXISTS
	(
		SELECT
			1
		FROM 
			[Data.Application.Role] R
		WHERE
			R.[Key] = TR.[Key]
	)

SET @RowCount = @@ROWCOUNT

IF @RowCount > 0
BEGIN
	SET @NextID = @NextID + @@ROWCOUNT

	INSERT INTO [Data.Application.UserRole] (Id, UserId, RoleId)
	SELECT
		@NextId - 1 + ROW_NUMBER() OVER(ORDER BY UR.Id ASC),
		UR.UserId, 
		R1.Id
	FROM
		@Role TR
	INNER JOIN [Data.Application.Role] R1
		ON R1.[Key] = TR.[Key]
	INNER JOIN [Data.Application.Role] R2
		ON R2.[Key] = TR.AssignKey
	INNER JOIN [Data.Application.UserRole] UR
		ON UR.RoleId = R2.Id
	WHERE
		NOT EXISTS
		(
			SELECT
				1
			FROM
				[Data.Application.UserRole] UR1
			WHERE
				UR1.UserId = UR.UserId
				AND UR1.RoleId = R1.Id
		)

	UPDATE KeyTable SET NextId = @NextID + @@ROWCOUNT
END
