INSERT INTO [Config.LocalisationItem]
(
	ContextKey,
	[Key],
	Value,
	Localised,
	LocalisationId
)
SELECT
	LI.ContextKey,
	LI.[Key] + '.NoEdit',
	LI.Value,
	LI.Localised,
	LI.LocalisationId
FROM
	[Config.LocalisationItem] LI
WHERE 
	LI.[Key] LIKE 'DistanceUnits.%'
	AND LI.[Key] NOT LIKE '%.NoEdit'
	AND NOT EXISTS
	(
		SELECT 
			1
		FROM
			[Config.LocalisationItem] LI2
		WHERE
			LI2.[Key] = LI.[Key] + '.NoEdit'
	)