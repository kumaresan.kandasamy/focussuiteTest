UPDATE [dbo].[Config.LocalisationItem]
   SET [Value] = 'Hazardous materials'
 where [Key] = 'DrivingLicenceEndorsements.HazerdousMaterials'
GO

UPDATE [dbo].[Config.LocalisationItem]
   SET [Value] = 'Farsi'
 where [Key] = 'Languages.Farsit'
GO