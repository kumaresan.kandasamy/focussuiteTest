-- Case sensitive replace for capitalised words
UPDATE
	[Library.JobTaskLocalisationItem]
SET 
	PrimaryValue = REPLACE(PrimaryValue COLLATE Latin1_General_CS_AS, 'Organisation', 'Organization')
WHERE 
	PrimaryValue COLLATE Latin1_General_CS_AS LIKE '%Organisation%'

UPDATE
	[Library.JobTaskLocalisationItem]
SET 
	SecondaryValue = REPLACE(SecondaryValue COLLATE Latin1_General_CS_AS, 'Organisation', 'Organization')
WHERE 
	SecondaryValue COLLATE Latin1_General_CS_AS LIKE '%Organisation%'

-- Replace all other instances (assume lower case)
UPDATE
	[Library.JobTaskLocalisationItem]
SET 
	PrimaryValue = REPLACE(PrimaryValue, 'organisation', 'organization')
WHERE 
	PrimaryValue LIKE '%organisation%'

UPDATE
	[Library.JobTaskLocalisationItem]
SET 
	SecondaryValue = REPLACE(SecondaryValue, 'organisation', 'organization')
WHERE 
	SecondaryValue LIKE '%organisation%'
	
-- Correct grammar
UPDATE
	[Library.JobTaskLocalisationItem]
SET
	PrimaryValue = REPLACE(PrimaryValue, 'kinds of organization ', 'kinds of organizations ')
WHERE 
	PrimaryValue LIKE '%kinds of organization %'
	
UPDATE
	[Library.JobTaskLocalisationItem]
SET
	SecondaryValue = REPLACE(SecondaryValue, 'kinds of organization ', 'kinds of organizations ')
WHERE 
	SecondaryValue LIKE '%kinds of organization %'

