IF OBJECT_ID('tempdb..#Resume') IS NOT NULL
	DROP TABLE #Resume

CREATE TABLE #Resume
(
	Id BIGINT,
	ResumeId NVARCHAR(10),
	ResumeXml XML,
	Updated BIT DEFAULT(0)
)

-- Get the resumes
INSERT INTO #Resume 
(
	Id,
	ResumeId,
	ResumeXml
)
SELECT
	R.Id,
	CAST(R.Id AS NVARCHAR(10)),
	CAST(R.ResumeXml AS XML)
FROM 
	[Data.Application.Resume] R
	
RAISERROR ('Got resumes', 0, 1) WITH NOWAIT

UPDATE 
	R
SET 
	ResumeXml.modify('replace value of (/ResDoc/resume/header/resumeid/text())[1] with sql:column("ResumeId")'),
	Updated = 1
FROM 
	#Resume R
WHERE 
	ResumeXml.exist('(/ResDoc/resume/header/resumeid/text())[1][. != ""]') = 1
	
RAISERROR ('Updated resumes in temp table', 0, 1) WITH NOWAIT

UPDATE
	R
SET
	ResumeXml = CAST(TR.ResumeXml AS VARCHAR(MAX))
FROM
	[Data.Application.Resume] R
INNER JOIN #Resume TR
	ON TR.Id = R.Id
WHERE
	TR.Updated = 1
	
RAISERROR ('Updated resumes in main table', 0, 1) WITH NOWAIT