DECLARE @NextId BIGINT

DECLARE @PersonId BIGINT
DECLARE @PersonAddressId BIGINT
DECLARE @UserId BIGINT
DECLARE @UserRoleId1 BIGINT
DECLARE @UserRoleId2 BIGINT
DECLARE @PhoneNumberId BIGINT

DECLARE @TitleId BIGINT
DECLARE @CountyId BIGINT
DECLARE @StateId BIGINT
DECLARE @CountryId BIGINT
DECLARE @AssistUserId BIGINT

SELECT @TitleId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Titles' AND [Key] = 'Title.Mr'
SELECT @CountyId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Counties' AND [Key] = 'County.ClayTX'
SELECT @StateId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'States' AND [Key] = 'State.TX'
SELECT @CountryId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Countries' AND [Key] = 'Country.US'
SELECT @AssistUserId = Id FROM [Data.Application.User] WHERE UserName = 'dev@client.com'

IF NOT EXISTS(SELECT 1 FROM [Data.Application.Person] WHERE EmailAddress = 'viewrequest@assist.com')
BEGIN
	BEGIN TRANSACTION
		
	SELECT @NextId = NextId FROM KeyTable
		
	UPDATE KeyTable SET NextId = NextId + 6
		
	IF @@ERROR <> 0
	BEGIN
		ROLLBACK TRANSACTION
		RETURN
	END
		
	SET @PersonId = @NextId
	SET @PersonAddressId = @NextId + 1
	SET @PhoneNumberId = @NextId + 2
	SET @UserId = @NextId + 3
	SET @UserRoleId1 = @NextId + 4
	SET @UserRoleId2 = @NextId + 5
		
	INSERT INTO [Data.Application.Person]
	(
		Id,
		TitleId,
		FirstName,
		MiddleInitial,
		LastName,
		DateOfBirth,
		SocialSecurityNumber,
		EmailAddress,
		EnrollmentStatus,
		ProgramAreaId,
		CampusId,
		DegreeId
	)
	VALUES
	(
		@PersonId,
		@TitleId,
		'View request',
		'',
		'Assist',
		NULL,
		NULL,
		'viewrequest@assist.com',
		NULL,
		NULL,
		NULL,
		NULL
	)

	IF @@ERROR <> 0
	BEGIN
		ROLLBACK TRANSACTION
		RETURN
	END
		
	INSERT INTO [Data.Application.PersonAddress]
	(
		Id,
		PersonId,
		Line1,
		Line2,
		TownCity,
		StateId ,
		CountyId,
		CountryId,
		PostcodeZip,
		IsPrimary
	)
	VALUES
	(
		@PersonAddressId,
		@PersonId,
		'Address Line 1',
		'',
		'Bluegrove',
		@StateId,
		@CountyId,
		@CountryId,
		'76352',
		1
	)
		
	IF @@ERROR <> 0
	BEGIN
		ROLLBACK TRANSACTION
		RETURN
	END
		
	INSERT INTO [Data.Application.PhoneNumber]
	(
		Id,
		PersonId,
		Number,
		PhoneType,
		IsPrimary,
		Extension,
		ProviderId
	)
	VALUES
	(
		@PhoneNumberId,
		@PersonId,
		'4234234234',
		0,
		1,
		NULL,
		NULL
	)
		
	IF @@ERROR <> 0
	BEGIN
		ROLLBACK TRANSACTION
		RETURN
	END
			
	INSERT INTO [Data.Application.User]
	(
		Id,
		PersonId,
		UserName,
		PasswordHash,
		PasswordSalt,
		UserType,
		[Enabled],
		ScreenName,
		IsMigrated,
		RegulationsConsent,
		CreatedOn,
		UpdatedOn
	)
	VALUES
	(
		@UserId,
		@PersonId,
		'viewrequest@assist.com',
		'I/mugkpu/aiJINDf7aBlKfYCSI3PjN/UViGvmkNS9RIM1BmUdDEPZFKimSQjNMnqKKzjKvyRK507usXbikQVmA',
		'cded5112',
		1,
		1,
		'View request Assist',
		1,
		1,
		GETDATE(),
		GETDATE()
	)
		
	IF @@ERROR <> 0
	BEGIN
		ROLLBACK TRANSACTION
		RETURN
	END
		
	INSERT INTO [Data.Application.UserRole]
	(
		Id,
		RoleId,
		UserId
	)
	SELECT
	@UserRoleId1,
	Id,
	@UserId
	FROM [Data.Application.Role] WHERE [Key] = 'AssistUser'

	INSERT INTO [Data.Application.UserRole]
	(
		Id,
		RoleId,
		UserId
	)
	SELECT
	@UserRoleId2,
	Id,
	@UserId
	FROM [Data.Application.Role] WHERE [Key] = 'AssistEmployerAccountApprovalViewer'
		
	IF @@ERROR <> 0
	BEGIN
		ROLLBACK TRANSACTION
		RETURN
	END
		
	COMMIT TRANSACTION
END