IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Application.User' AND COLUMN_NAME = 'AccountDisabledReason')
BEGIN
	ALTER TABLE
		[Data.Application.User]
	ADD
		AccountDisabledReason INT NULL
END
GO

IF 'True' = (SELECT Value FROM [Config.ConfigurationItem] WHERE [Key] = 'JobSeekerSendInactivityEmail')
BEGIN
	DECLARE @Limit INT
	DECLARE @Grace INT

	SELECT 
		@Limit = Value 
	FROM 
		[Config.ConfigurationItem] 
	WHERE [Key] = 'JobSeekerInactivityEmailLimit'
	
	IF @Limit IS NULL
		SET @Limit = 60

	SELECT 
		@Grace = Value 
	FROM 
		[Config.ConfigurationItem] 
	WHERE [Key] = 'JobSeekerInactivityEmailGrace'
	
	IF @Grace IS NULL
		SET @Grace = 60
	
	UPDATE
		U
	SET
		AccountDisabledReason = 1
	FROM
		[Data.Application.User] U
	INNER JOIN [Data.Application.Person] P
		ON P.Id = U.PersonId
	INNER JOIN [Data.Application.Issues] I
		ON I.PersonId = P.Id
	WHERE
		U.UserType IN (4, 8, 12)
		AND U.[Enabled] = 0
		AND DATEDIFF(DAY, ISNULL(U.LoggedInOn, U.CreatedOn), GETDATE()) >= @Limit + @Grace
END
	
