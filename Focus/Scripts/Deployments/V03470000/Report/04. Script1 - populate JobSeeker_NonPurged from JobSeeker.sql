IF NOT EXISTS(SELECT 1 FROM [Report.JobSeeker_NonPurged])
BEGIN
	DECLARE @NextId BIGINT
	SELECT @NextId = NextId FROM KeyTable

	INSERT INTO [dbo].[Report.JobSeeker_NonPurged]
		([Id]
		  ,[JobSeekerReportId]
		  ,[DateOfBirth]
		  ,[Gender]
		  ,[DisabilityStatus]
		  ,[DisabilityType]
		  ,[EthnicHeritage]
		  ,[Race]
		  ,[MinimumEducationLevel]
		  ,[EnrollmentStatus]
		  ,[EmploymentStatus]
		  ,[Age]
		  ,[CreatedOn]
		  ,[UpdatedOn]
		  ,[IsCitizen]
		  ,[AlienCardExpires]
		  ,[AlienCardId]
		  ,[CountyId])
	SELECT @NextId + ROW_NUMBER() OVER (ORDER BY Id ASC)
		  ,[Id]
		  ,[DateOfBirth]
		  ,[Gender]
		  ,[DisabilityStatus]
		  ,[DisabilityType]
		  ,[EthnicHeritage]
		  ,[Race]
		  ,[MinimumEducationLevel]
		  ,[EnrollmentStatus]
		  ,[EmploymentStatus]
		  ,[Age]
		  ,[CreatedOn]
		  ,[UpdatedOn]
		  ,[IsCitizen]
		  ,[AlienCardExpires]
		  ,[AlienCardId]
		  ,[CountyId]
	FROM [dbo].[Report.JobSeeker]

	UPDATE [KeyTable] SET NextId = NextId + @@ROWCOUNT
END