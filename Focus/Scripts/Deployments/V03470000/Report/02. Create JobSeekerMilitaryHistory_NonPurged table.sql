﻿IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Report.JobSeekerMilitaryHistory_NonPurged')
BEGIN
CREATE TABLE [dbo].[Report.JobSeekerMilitaryHistory_NonPurged] 
(
	[Id]												BIGINT NOT NULL,
	[VeteranDisability]					INT NULL,
	[VeteranTransitionType]			INT NULL,
	[VeteranMilitaryDischarge]	INT NULL,
	[VeteranBranchOfService]		INT NULL,
	[CampaignVeteran]						BIT NULL,
	[MilitaryStartDate]					DATETIME NULL,
	[MilitaryEndDate]						DATETIME NULL,
	[JobSeekerId]								BIGINT NULL,
	[CreatedOn]									DATETIME NOT NULL,
	[UpdatedOn]									DATETIME NOT NULL,
	[DeletedOn]									DATETIME NULL,
	[VeteranType]								INT NULL
)
END
GO

DECLARE @Sql varchar(1000);
SELECT @Sql = 'ALTER TABLE dbo.[Report.JobSeekerMilitaryHistory_NonPurged] DROP CONSTRAINT [' + CONSTRAINT_NAME + ']' FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE TABLE_NAME = 'Report.JobSeekerMilitaryHistory_NonPurged' AND CONSTRAINT_TYPE = 'PRIMARY KEY';
--SELECT @Sql;
EXEC (@Sql);

ALTER TABLE dbo.[Report.JobSeekerMilitaryHistory_NonPurged] ADD CONSTRAINT [PK_Report.JobSeekerMilitaryHistory_NonPurged] PRIMARY KEY CLUSTERED ([Id] ASC);


