﻿IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Report.JobSeeker_NonPurged')
BEGIN
CREATE TABLE [dbo].[Report.JobSeeker_NonPurged] 
(
	[Id]								BIGINT NOT NULL,
	[JobSeekerReportId] BIGINT NOT NULL DEFAULT 0,
	[DateOfBirth]				DATETIME NULL,
	[Gender]						INT NULL,
	[DisabilityStatus]	INT NULL,
	[DisabilityType]		INT NULL,
	[EthnicHeritage]		INT NULL,
	[Race]							INT NULL,
	[MinimumEducationLevel] INT NULL,
	[EnrollmentStatus]	INT NULL,
	[EmploymentStatus]	INT NULL,
	[Age]								SMALLINT NULL,
	[CreatedOn]					DATETIME NOT NULL,
	[UpdatedOn]					DATETIME NOT NULL,
	[IsCitizen]					BIT NULL,
	[AlienCardExpires]	DATETIME NULL,
	[AlienCardId]				NVARCHAR(MAX) NULL,
	[CountyId]					BIGINT NULL
)
END
GO

DECLARE @Sql varchar(1000);
SELECT @Sql = 'ALTER TABLE dbo.[Report.JobSeeker_NonPurged] DROP CONSTRAINT [' + CONSTRAINT_NAME + ']' FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE TABLE_NAME = 'Report.JobSeeker_NonPurged' AND CONSTRAINT_TYPE = 'PRIMARY KEY';
SELECT @Sql;
EXEC (@Sql);

ALTER TABLE dbo.[Report.JobSeeker_NonPurged] ADD CONSTRAINT [PK_Report.JobSeeker_NonPurged] PRIMARY KEY CLUSTERED ([Id] ASC);

