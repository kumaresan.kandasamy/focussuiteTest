IF (SELECT DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.JobSeeker' AND COLUMN_NAME = 'DateOfBirth') IN ('nvarchar', 'varchar')
BEGIN
	IF EXISTS(SELECT 1 FROM Sys.indexes WHERE [name] = 'IX_Report.JobSeeker_DateOfBirth' AND object_id = object_id('[Report.JobSeeker]'))
	BEGIN
		DROP INDEX [dbo].[Report.JobSeeker].[IX_Report.JobSeeker_DateOfBirth]
	END
	
	ALTER TABLE 
		[dbo].[Report.JobSeeker] 
	ALTER COLUMN 
		DateOfBirth DATETIME
		
	CREATE NONCLUSTERED INDEX [IX_Report.JobSeeker_DateOfBirth]
		ON [dbo].[Report.JobSeeker]([DateOfBirth] ASC)
END
GO

IF (SELECT DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Report.JobSeeker_NonPurged' AND COLUMN_NAME = 'DateOfBirth') IN ('nvarchar', 'varchar')
BEGIN
	ALTER TABLE 
		[dbo].[Report.JobSeeker_NonPurged] 
	ALTER COLUMN 
		DateOfBirth DATETIME
END
GO
