IF TYPE_ID(N'GuidTableType') IS NULL
BEGIN
	CREATE TYPE [dbo].[GuidTableType] AS TABLE
	(
		[Guid] UNIQUEIDENTIFIER,
		PRIMARY KEY CLUSTERED 
		( [Guid] ASC ) WITH (IGNORE_DUP_KEY = OFF)
	)
END
GO
