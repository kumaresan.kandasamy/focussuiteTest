DECLARE @Date DATETIME
DECLARE @RowCount INT
SET @Date = '01 January 1900'

SET @RowCount = -1

WHILE @RowCount <> 0
BEGIN
	UPDATE TOP (1000)
		[Messaging.Message]
	SET
		ProcessOn = @Date
	WHERE
		[Status] = 1
		AND ProcessOn IS NULL
		
	SET @RowCount = @@ROWCOUNT
END