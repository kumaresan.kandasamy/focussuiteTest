---- Message

---- add the sequential primary key to message table
--ALTER TABLE [messaging.message] 
--ADD SequentialId BIGINT IDENTITY NOT NULL
--GO

---- drop the constraint referencing the messageid primary key
ALTER TABLE [messaging.messagelog]
DROP CONSTRAINT [FK_Messaging.MessageLog_MessageId]
GO

-- drop the primary key constraint on the guid
ALTER TABLE [messaging.message]
DROP CONSTRAINT [PK_Messaging.Message]
GO

--ALTER TABLE [messaging.message]
--DROP CONSTRAINT [PK_Messaging.Message] 
--GO

-- make the sequential id the primary key
ALTER TABLE [messaging.message]
ADD CONSTRAINT [PK_Messaging.Message] PRIMARY KEY NONCLUSTERED (Id)
GO

---- create a non clustered index on the guid
--CREATE NONCLUSTERED INDEX [IX_Messaging.Message_Id] ON [Messaging.Message](Id)
--GO

---- add a column to the messagelog table to reference the message primary key
--ALTER TABLE [messaging.messagelog]
--ADD MessageSequentialId bigint
--GO

---- map the new messagesequentialid to the new primary key in the message table
--UPDATE [Messaging.MessageLog]
--SET MessageSequentialId = m.SequentialId
--FROM [Messaging.Message] m
--WHERE MessageId = m.Id
--GO

ALTER TABLE [messaging.messagelog]
ADD CONSTRAINT [FK_Messaging.MessageLog_MessageId] FOREIGN KEY (MessageId) REFERENCES [messaging.message] (Id)

---- MessageLog

--alter table [messaging.messagelog] add SequentialId bigint identity not null
--GO

--ALTER TABLE [messaging.messagelog]
--DROP CONSTRAINT [PK_Messaging.MessageLog]
--GO

--ALTER TABLE [messaging.messagelog]
--ADD CONSTRAINT [PK_Messaging.MessageLog] PRIMARY KEY (SequentialId)
--GO

---- MessagePayload

--alter table [messaging.messagepayload] add SequentialId bigint identity not null
--GO

--ALTER TABLE [Messaging.Message]
--ADD MessagePayloadSequentialId bigint
--GO

--UPDATE [Messaging.Message]
--SET MessagePayloadSequentialId = mp.SequentialId
--FROM [Messaging.MessagePayload] mp
--WHERE MessagePayloadId = mp.Id
--GO

---- MessageType

--alter table [messaging.messagetype] add SequentialId bigint identity not null

--ALTER TABLE [Messaging.Message]
--ADD MessageTypeSequentialId bigint
--GO

--UPDATE [Messaging.Message]
--SET MessageTypeSequentialId = mt.SequentialId
--FROM [Messaging.MessageType] mt
--WHERE MessageTypeId = mt.Id
--GO