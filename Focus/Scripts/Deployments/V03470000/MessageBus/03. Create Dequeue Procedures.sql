IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'Messaging.DequeueMessage')
BEGIN
	DROP PROCEDURE [Messaging.DequeueMessage]
END
GO

CREATE PROCEDURE [Messaging.DequeueMessage]
	@Version NVARCHAR(10),
	@DequeuedOn DATETIME,
	@MessageId UNIQUEIDENTIFIER OUT
AS
DECLARE @DequeueAttempts INT

SET @MessageId = NULL
SET @DequeueAttempts = 10

WHILE (@dequeueAttempts > 0)
BEGIN         
	SELECT TOP 1
		@MessageId = [Messaging.Message].[Id]
	FROM
		[Messaging.Message] WITH (NOLOCK)
	WHERE
		[Messaging.Message].[Status] = 1 
		AND [Messaging.Message].[Version] = @Version
		AND [Messaging.Message].[ProcessOn] <= GETDATE()
	ORDER BY
		[Messaging.Message].Priority DESC,
		[Messaging.Message].EnqueuedOn ASC
		
	IF @MessageId IS NULL
	BEGIN
		SET @dequeueAttempts = 0
	END
	ELSE
	BEGIN
		UPDATE
			[Messaging.Message]
		SET
			[Status] = 2,
			DequeuedOn = @DequeuedOn
		WHERE
			Id = @MessageId
			AND [Status] = 1
			
		IF @@ROWCOUNT = 0
		BEGIN
			SET @DequeueAttempts = @DequeueAttempts - 1
			SET @MessageId = NULL
		END
		ELSE
		BEGIN
			SET @dequeueAttempts = 0
		END
	END
END
GO

GRANT EXECUTE ON [dbo].[Messaging.DequeueMessage] TO FocusAgent
GO

IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'Messaging.DequeueMessageIncludeTypes')
BEGIN
	DROP PROCEDURE [Messaging.DequeueMessageIncludeTypes]
END
GO

CREATE PROCEDURE [Messaging.DequeueMessageIncludeTypes]
	@Version NVARCHAR(10),
	@MessageTypeIds GUIDTableType READONLY,
	@DequeuedOn DATETIME,
	@MessageId UNIQUEIDENTIFIER OUT
AS
DECLARE @DequeueAttempts INT

SET @MessageId = NULL
SET @DequeueAttempts = 10

WHILE (@dequeueAttempts > 0)
BEGIN         
	SELECT TOP 1
		@MessageId = [Messaging.Message].[Id]
	FROM
		[Messaging.Message] WITH (NOLOCK)
	WHERE
		[Messaging.Message].[Status] = 1 
		AND [Messaging.Message].[Version] = @Version
		AND [Messaging.Message].[MessageTypeId] IN (SELECT [Guid] FROM @MessageTypeIds)
		AND [Messaging.Message].[ProcessOn] <= GETDATE()
	ORDER BY
		[Messaging.Message].Priority DESC,
		[Messaging.Message].EnqueuedOn ASC
		
	IF @MessageId IS NULL
	BEGIN
		SET @dequeueAttempts = 0
	END
	ELSE
	BEGIN
		UPDATE
			[Messaging.Message]
		SET
			[Status] = 2,
			DequeuedOn = @DequeuedOn
		WHERE
			Id = @MessageId
			AND [Status] = 1
			
		IF @@ROWCOUNT = 0
		BEGIN
			SET @DequeueAttempts = @DequeueAttempts - 1
			SET @MessageId = NULL
		END
		ELSE
		BEGIN
			SET @dequeueAttempts = 0
		END
	END
END
GO

GRANT EXECUTE ON [dbo].[Messaging.DequeueMessageIncludeTypes] TO FocusAgent
GO

IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'Messaging.DequeueMessageExcludeTypes')
BEGIN
	DROP PROCEDURE [Messaging.DequeueMessageExcludeTypes]
END
GO

CREATE PROCEDURE [Messaging.DequeueMessageExcludeTypes]
	@Version NVARCHAR(10),
	@MessageTypeIds GUIDTableType READONLY,
	@DequeuedOn DATETIME,
	@MessageId UNIQUEIDENTIFIER OUT
AS
DECLARE @DequeueAttempts INT

SET @MessageId = NULL
SET @DequeueAttempts = 10

WHILE (@dequeueAttempts > 0)
BEGIN         
	SELECT TOP 1
		@MessageId = [Messaging.Message].[Id]
	FROM
		[Messaging.Message] WITH (NOLOCK)
	WHERE
		[Messaging.Message].[Status] = 1 
		AND [Messaging.Message].[Version] = @Version
		AND [Messaging.Message].[MessageTypeId] NOT IN (SELECT [Guid] FROM @MessageTypeIds)
		AND [Messaging.Message].[ProcessOn] <= GETDATE()
	ORDER BY
		[Messaging.Message].Priority DESC,
		[Messaging.Message].EnqueuedOn ASC
		
	IF @MessageId IS NULL
	BEGIN
		SET @dequeueAttempts = 0
	END
	ELSE
	BEGIN
		UPDATE
			[Messaging.Message]
		SET
			[Status] = 2,
			DequeuedOn = @DequeuedOn
		WHERE
			Id = @MessageId
			AND [Status] = 1
			
		IF @@ROWCOUNT = 0
		BEGIN
			SET @DequeueAttempts = @DequeueAttempts - 1
			SET @MessageId = NULL
		END
		ELSE
		BEGIN
			SET @dequeueAttempts = 0
		END
	END
END
GO

GRANT EXECUTE ON [dbo].[Messaging.DequeueMessageExcludeTypes] TO FocusAgent
GO

