﻿DECLARE @SavedSearches TABLE
(
	Id BIGINT IDENTITY(1, 1),
	Name NVARCHAR(200),
	SearchCriteria NVARCHAR(MAX),
	AlertEmailAddress NVARCHAR(255)	
)

INSERT INTO @SavedSearches (Name, SearchCriteria, AlertEmailAddress)
VALUES
	(
	'UIClaimant Only SavedSearch',
	'<?xml version="1.0" encoding="utf-16"?>  <SearchCriteria xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">    <PageIndex>0</PageIndex>    <PageSize>10</PageSize>    <ListSize>250</ListSize>    <FetchOption>Single</FetchOption>    <SearchType>Postings</SearchType>    <RequiredResultCriteria>      <DocumentsToSearch>Posting</DocumentsToSearch>      <MaximumDocumentCount>200</MaximumDocumentCount>      <MinimumStarMatch>None</MinimumStarMatch>      <IncludeDuties>false</IncludeDuties>    </RequiredResultCriteria>    <KeywordCriteria>      <KeywordText>manager</KeywordText>      <SearchLocation>Anywhere</SearchLocation>      <Operator>And</Operator>    </KeywordCriteria>    <JobLocationCriteria>      <OnlyShowJobInMyState>false</OnlyShowJobInMyState>      <Area>        <StateId>0</StateId>        <MsaId>0</MsaId>      </Area>    </JobLocationCriteria>    <PostingAgeCriteria>      <PostingAgeInDays>7</PostingAgeInDays>      <MinimumPostingAgeInDays xsi:nil="true" />    </PostingAgeCriteria>    <InternshipCriteria>      <Internship>NoFilter</Internship>    </InternshipCriteria>  </SearchCriteria>',
	'UIClaimantOSS@explorer.com'	 
	),
	(
	'UIClaimant Multiple SavedSearches 1',
	'<?xml version="1.0" encoding="utf-16"?>  <SearchCriteria xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">    <PageIndex>0</PageIndex>    <PageSize>10</PageSize>    <ListSize>250</ListSize>    <FetchOption>Single</FetchOption>    <SearchType>Postings</SearchType>    <RequiredResultCriteria>      <DocumentsToSearch>Posting</DocumentsToSearch>      <MaximumDocumentCount>200</MaximumDocumentCount>      <MinimumStarMatch>None</MinimumStarMatch>      <IncludeDuties>false</IncludeDuties>    </RequiredResultCriteria>    <KeywordCriteria>      <KeywordText>manager</KeywordText>      <SearchLocation>Anywhere</SearchLocation>      <Operator>And</Operator>    </KeywordCriteria>    <JobLocationCriteria>      <OnlyShowJobInMyState>false</OnlyShowJobInMyState>      <Area>        <StateId>0</StateId>        <MsaId>0</MsaId>      </Area>    </JobLocationCriteria>    <PostingAgeCriteria>      <PostingAgeInDays>7</PostingAgeInDays>      <MinimumPostingAgeInDays xsi:nil="true" />    </PostingAgeCriteria>    <InternshipCriteria>      <Internship>NoFilter</Internship>    </InternshipCriteria>  </SearchCriteria>',
	'UIClaimantMSS@explorer.com'
	),
	(
	'UIClaimant Multiple SavedSearches 2',
	'<?xml version="1.0" encoding="utf-16"?>  <SearchCriteria xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">    <PageIndex>0</PageIndex>    <PageSize>10</PageSize>    <ListSize>250</ListSize>    <FetchOption>Single</FetchOption>    <SearchType>Postings</SearchType>    <RequiredResultCriteria>      <DocumentsToSearch>Posting</DocumentsToSearch>      <MaximumDocumentCount>200</MaximumDocumentCount>      <MinimumStarMatch>None</MinimumStarMatch>      <IncludeDuties>false</IncludeDuties>    </RequiredResultCriteria>    <KeywordCriteria>      <KeywordText>manager</KeywordText>      <SearchLocation>Anywhere</SearchLocation>      <Operator>And</Operator>    </KeywordCriteria>    <JobLocationCriteria>      <OnlyShowJobInMyState>false</OnlyShowJobInMyState>      <Area>        <StateId>0</StateId>        <MsaId>0</MsaId>      </Area>    </JobLocationCriteria>    <PostingAgeCriteria>      <PostingAgeInDays>7</PostingAgeInDays>      <MinimumPostingAgeInDays xsi:nil="true" />    </PostingAgeCriteria>    <InternshipCriteria>      <Internship>NoFilter</Internship>    </InternshipCriteria>  </SearchCriteria>',
	'UIClaimantMSS@explorer.com'
	),
	(
	'None UIClaimant Only SavedSearch',
	'<?xml version="1.0" encoding="utf-16"?>  <SearchCriteria xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">    <PageIndex>0</PageIndex>    <PageSize>10</PageSize>    <ListSize>250</ListSize>    <FetchOption>Single</FetchOption>    <SearchType>Postings</SearchType>    <RequiredResultCriteria>      <DocumentsToSearch>Posting</DocumentsToSearch>      <MaximumDocumentCount>200</MaximumDocumentCount>      <MinimumStarMatch>None</MinimumStarMatch>      <IncludeDuties>false</IncludeDuties>    </RequiredResultCriteria>    <KeywordCriteria>      <KeywordText>manager</KeywordText>      <SearchLocation>Anywhere</SearchLocation>      <Operator>And</Operator>    </KeywordCriteria>    <JobLocationCriteria>      <OnlyShowJobInMyState>false</OnlyShowJobInMyState>      <Area>        <StateId>0</StateId>        <MsaId>0</MsaId>      </Area>    </JobLocationCriteria>    <PostingAgeCriteria>      <PostingAgeInDays>7</PostingAgeInDays>      <MinimumPostingAgeInDays xsi:nil="true" />    </PostingAgeCriteria>    <InternshipCriteria>      <Internship>NoFilter</Internship>    </InternshipCriteria>  </SearchCriteria>',
	'NonClaimantOSS@explorer.com'
	)
	
DECLARE @Name NVARCHAR(200)
DECLARE @SearchCriteria NVARCHAR(MAX)	
DECLARE @AlertEmailAddress NVARCHAR(200)	

DECLARE @SavedSearchId BIGINT
DECLARE @SavedSearchUserId BIGINT	
DECLARE @UserId BIGINT
	
DECLARE @SavedSearchIndex INT
DECLARE @SavedSearchCount INT

DECLARE @NextId BIGINT

SET @SavedSearchIndex = 1
SELECT @SavedSearchCount = COUNT(1) FROM @SavedSearches

WHILE @SavedSearchIndex <= @SavedSearchCount
BEGIN
	SELECT 
		@Name = Name,
		@SearchCriteria = SearchCriteria,
		@AlertEmailAddress = AlertEmailAddress
	FROM
		@SavedSearches
	WHERE
		Id = @SavedSearchIndex
	
	BEGIN TRANSACTION
	
	SELECT @NextId = NextId FROM KeyTable
	
	UPDATE KeyTable SET NextId = NextId + 2
	
	IF @@ERROR <> 0
	BEGIN
		ROLLBACK TRANSACTION
		RETURN
	END
	
	SET @SavedSearchId = @NextId
	SET @SavedSearchUserId = @NextId + 1
	
	INSERT INTO [Data.Application.SavedSearch]
	(
		Id,
		Name,
		SearchCriteria,
		AlertEmailRequired,
		AlertEmailFrequency,
		AlertEmailFormat,
		AlertEmailAddress,
		AlertEmailScheduledOn,
		[Type],
		CreatedOn,
		UpdatedOn
	)
	VALUES
	(
		@SavedSearchId,
		@Name,
		@SearchCriteria,
		1,
		1,
		0,
		@AlertEmailAddress,
		NULL,
		2,
		GETDATE(),
		GETDATE()
	)
	
	IF @@ERROR <> 0
	BEGIN
		ROLLBACK TRANSACTION
		RETURN
	END
	
	SELECT @UserId = Id FROM [Data.Application.User] WHERE [UserName] = @AlertEmailAddress
	
	INSERT INTO [Data.Application.SavedSearchUser]
	(
		Id,
		UserId,
		SavedSearchId
	)
	VALUES
	(
		@SavedSearchUserId,
		@UserId,
		@SavedSearchId
	)
	
	IF @@ERROR <> 0
	BEGIN
		ROLLBACK TRANSACTION
		RETURN
	END
	
	COMMIT TRANSACTION
	
	SET @SavedSearchIndex = @SavedSearchIndex +1
END

