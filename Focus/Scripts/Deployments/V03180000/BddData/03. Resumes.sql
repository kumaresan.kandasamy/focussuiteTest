DECLARE @Resumes TABLE
(
	Id BIGINT IDENTITY(1, 1),
	PersonEmailAddress NVARCHAR(255),
	ResumeName NVARCHAR(255),
	IsDefault BIT,
	PersonId BIGINT,
	FirstName NVARCHAR(40),
	LastName NVARCHAR(40),
	OverrideResumeXml NVARCHAR(Max),
	CompletionDate DATETIME,
	IsVeteran BIT
)

INSERT INTO @Resumes (PersonEmailAddress, ResumeName, IsDefault, CompletionDate, OverrideResumeXml, IsVeteran)
VALUES 
	('fvn459@explorer.com', 'FVN459 Resume with Branding', 1, NULL, '<ResDoc>    <special>      <technicalskills include="1">technical skills</technicalskills>      <branding include="1">FVN-459 test</branding>      <preserved_format>Standard</preserved_format>      <preserved_order>Emphasize my experience</preserved_order>    </special>    <resume>      <contact>        <name>          <givenname>Dev</givenname>          <surname>Explorer</surname>        </name> Home:<phone type="home">1111111111</phone><email>fvn459@explorer.com</email><website></website><address><street>Address Line 1</street><street></street><city>Bluegrove</city><state_county>48077</state_county><county_fullname>Clay</county_fullname><state>TX</state><state_fullname>Texas</state_fullname><postalcode>76352</postalcode><country>US</country><country_fullname>US</country_fullname></address></contact>      <experience>        <employment_status_cd>1</employment_status_cd>        <job>          <jobid>325e1895-009f-4536-b6d5-647d066e71b0</jobid>          <employer>BGT</employer>          <title>Senior Programmer</title>          <description>* Developed computer application programs.  * Created applications.  * Modified existing applications for specific purposes.  * Corrected problems in old versions of applications.  * Analyzed user needs.</description>          <address>            <city>Boston</city>            <state>MA</state>            <state_fullname>Massachusetts</state_fullname>            <country>US</country>            <country_fullname>United States</country_fullname>          </address>          <daterange>            <start>01-2010</start>            <end currentjob="1">09-2014</end>          </daterange>        </job>        <job>          <jobid>967ad7db-53dd-44ec-98b8-3abc7bb6c647</jobid>          <employer>Another Software House</employer>          <title>Analyst Programmer</title>          <description>* Created programs to handle specific jobs such as storing data, retrieving data.  * Revised and repaired of existing programs.  * Expanded existing programs.  * Adapted programs to new requirements.</description>          <address>            <city>Boston</city>            <state>MA</state>            <state_fullname>Massachusetts</state_fullname>            <country>US</country>            <country_fullname>United States</country_fullname>          </address>          <daterange>            <start>01-2005</start>            <end>01-2010</end>          </daterange>        </job>        <job>          <jobid>f9f7352a-dd7e-472d-83f1-bead3c6c93f5</jobid>          <employer>A Software House</employer>          <title>Software Developer</title>          <description>* Wrote computer programs to meet project goals and specifications.  * Conducted trial runs of programs.  * Minimized run time for program efficiency.  * Updated and maintained computer programs.  * Updated and maintained software packages.</description>          <address>            <city>Boston</city>            <state>MA</state>            <state_fullname>Massachusetts</state_fullname>            <country>US</country>            <country_fullname>United States</country_fullname>          </address>          <daterange>            <start>01-2000</start>            <end>01-2005</end>          </daterange>        </job>        <months_experience>176</months_experience>      </experience>      <education>        <school_status_cd>5</school_status_cd>        <norm_edu_level_cd>20</norm_edu_level_cd>      </education>      <header>        <default>1</default>        <buildmethod>1</buildmethod>        <resumeid>4840876</resumeid>        <resumename>FVN459 Resume with Branding</resumename>        <onet>15-1131.00</onet>        <ronet>15-1131.00</ronet>        <status>1</status>        <completionstatus>127</completionstatus>        <updatedon>22/09/2014 14:44:45</updatedon>      </header>      <statements>        <personal>          <sex>2</sex>          <ethnic_heritages>            <ethnic_heritage>              <ethnic_id>3</ethnic_id>              <selection_flag>-1</selection_flag>            </ethnic_heritage>            <ethnic_heritage>              <ethnic_id>4</ethnic_id>              <selection_flag>0</selection_flag>            </ethnic_heritage>            <ethnic_heritage>              <ethnic_id>5</ethnic_id>              <selection_flag>0</selection_flag>            </ethnic_heritage>            <ethnic_heritage>              <ethnic_id>2</ethnic_id>              <selection_flag>0</selection_flag>            </ethnic_heritage>            <ethnic_heritage>              <ethnic_id>6</ethnic_id>              <selection_flag>0</selection_flag>            </ethnic_heritage>            <ethnic_heritage>              <ethnic_id>1</ethnic_id>              <selection_flag>0</selection_flag>            </ethnic_heritage>            <ethnic_heritage>              <ethnic_id>-1</ethnic_id>              <selection_flag>-1</selection_flag>            </ethnic_heritage>          </ethnic_heritages>          <citizen_flag>-1</citizen_flag>          <disability_status>3</disability_status>          <license>            <driver_flag>0</driver_flag>          </license>          <veteran>            <vet_flag>0</vet_flag>            <subscribe_vps>0</subscribe_vps>          </veteran>          <preferences>            <sal>10</sal>            <salary_unit_cd>1</salary_unit_cd>            <resume_searchable>1</resume_searchable>            <contact_info_visible>0</contact_info_visible>            <postings_interested_in>0</postings_interested_in>            <relocate>-1</relocate>            <shift>              <shift_first_flag>-1</shift_first_flag>              <shift_second_flag>-1</shift_second_flag>              <shift_third_flag>-1</shift_third_flag>              <shift_rotating_flag>-1</shift_rotating_flag>              <shift_split_flag>-1</shift_split_flag>              <shift_varies_flag>-1</shift_varies_flag>              <work_week>3</work_week>              <work_type>3</work_type>              <work_over_time>-1</work_over_time>            </shift>          </preferences>        </personal>      </statements>      <summary>        <summary include="1">Summary</summary>      </summary>      <skills>        <internship_skills />      </skills>      <skillrollup />      <professional>        <affiliations include="1">test</affiliations>      </professional>      <hide_options>        <hide_daterange>0</hide_daterange>        <hide_eduDates>0</hide_eduDates>        <hide_militaryserviceDates>0</hide_militaryserviceDates>        <hide_workDates>0</hide_workDates>        <hide_contact>0</hide_contact>        <hide_name>0</hide_name>        <hide_email>0</hide_email>        <hide_homePhoneNumber>0</hide_homePhoneNumber>        <hide_workPhoneNumber>0</hide_workPhoneNumber>        <hide_cellPhoneNumber>0</hide_cellPhoneNumber>        <hide_faxPhoneNumber>0</hide_faxPhoneNumber>        <hide_nonUSPhoneNumber>0</hide_nonUSPhoneNumber>        <hide_affiliations>0</hide_affiliations>        <hide_certifications_professionallicenses>0</hide_certifications_professionallicenses>        <hide_employer>0</hide_employer>        <hide_driver_license>0</hide_driver_license>        <unhide_driver_license>0</unhide_driver_license>        <hide_honors>0</hide_honors>        <hide_interests>0</hide_interests>        <hide_internships>0</hide_internships>        <hide_languages>0</hide_languages>        <hide_veteran>0</hide_veteran>        <hide_objective>0</hide_objective>        <hide_personalinformation>0</hide_personalinformation>        <hide_professionaldevelopment>0</hide_professionaldevelopment>        <hide_publications>0</hide_publications>        <hide_references>0</hide_references>        <hide_skills>0</hide_skills>        <hide_technicalskills>0</hide_technicalskills>        <hide_volunteeractivities>0</hide_volunteeractivities>        <hide_thesismajorprojects>0</hide_thesismajorprojects>        <hidden_skills></hidden_skills>      </hide_options>    </resume>  </ResDoc>', 0),
	('fvn460@explorer.com', 'FVN460 Resume without Branding', 1, NULL, '<ResDoc>    <special>      <technicalskills include="1">technical skills</technicalskills>      <preserved_format>Standard</preserved_format>      <preserved_order>Emphasize my experience</preserved_order>    </special>    <resume>      <contact>        <name>          <givenname>Dev</givenname>          <surname>Explorer</surname>        </name> Home:<phone type="home">1111111111</phone><email>fvn460@explorer.com</email><website></website><address><street>Address Line 1</street><street></street><city>Bluegrove</city><state_county>48077</state_county><county_fullname>Clay</county_fullname><state>TX</state><state_fullname>Texas</state_fullname><postalcode>76352</postalcode><country>US</country><country_fullname>US</country_fullname></address></contact>      <experience>        <employment_status_cd>1</employment_status_cd>        <job>          <jobid>325e1895-009f-4536-b6d5-647d066e71b0</jobid>          <employer>BGT</employer>          <title>Senior Programmer</title>          <description>* Developed computer application programs.  * Created applications.  * Modified existing applications for specific purposes.  * Corrected problems in old versions of applications.  * Analyzed user needs.</description>          <address>            <city>Boston</city>            <state>MA</state>            <state_fullname>Massachusetts</state_fullname>            <country>US</country>            <country_fullname>United States</country_fullname>          </address>          <daterange>            <start>01-2010</start>            <end currentjob="1">09-2014</end>          </daterange>        </job>        <job>          <jobid>967ad7db-53dd-44ec-98b8-3abc7bb6c647</jobid>          <employer>Another Software House</employer>          <title>Analyst Programmer</title>          <description>* Created programs to handle specific jobs such as storing data, retrieving data.  * Revised and repaired of existing programs.  * Expanded existing programs.  * Adapted programs to new requirements.</description>          <address>            <city>Boston</city>            <state>MA</state>            <state_fullname>Massachusetts</state_fullname>            <country>US</country>            <country_fullname>United States</country_fullname>          </address>          <daterange>            <start>01-2005</start>            <end>01-2010</end>          </daterange>        </job>        <job>          <jobid>f9f7352a-dd7e-472d-83f1-bead3c6c93f5</jobid>          <employer>A Software House</employer>          <title>Software Developer</title>          <description>* Wrote computer programs to meet project goals and specifications.  * Conducted trial runs of programs.  * Minimized run time for program efficiency.  * Updated and maintained computer programs.  * Updated and maintained software packages.</description>          <address>            <city>Boston</city>            <state>MA</state>            <state_fullname>Massachusetts</state_fullname>            <country>US</country>            <country_fullname>United States</country_fullname>          </address>          <daterange>            <start>01-2000</start>            <end>01-2005</end>          </daterange>        </job>        <months_experience>176</months_experience>      </experience>      <education>        <school_status_cd>5</school_status_cd>        <norm_edu_level_cd>20</norm_edu_level_cd>      </education>      <header>        <default>1</default>        <buildmethod>1</buildmethod>        <resumeid>4840876</resumeid>        <resumename>FVN460 Resume without Branding</resumename>        <onet>15-1131.00</onet>        <ronet>15-1131.00</ronet>        <status>1</status>        <completionstatus>127</completionstatus>        <updatedon>18/09/2014 15:55:46</updatedon>      </header>      <statements>        <personal>          <sex>2</sex>          <ethnic_heritages>            <ethnic_heritage>              <ethnic_id>3</ethnic_id>              <selection_flag>-1</selection_flag>            </ethnic_heritage>            <ethnic_heritage>              <ethnic_id>4</ethnic_id>              <selection_flag>0</selection_flag>            </ethnic_heritage>            <ethnic_heritage>              <ethnic_id>5</ethnic_id>              <selection_flag>0</selection_flag>            </ethnic_heritage>            <ethnic_heritage>              <ethnic_id>2</ethnic_id>              <selection_flag>0</selection_flag>            </ethnic_heritage>            <ethnic_heritage>              <ethnic_id>6</ethnic_id>              <selection_flag>0</selection_flag>            </ethnic_heritage>            <ethnic_heritage>              <ethnic_id>1</ethnic_id>              <selection_flag>0</selection_flag>            </ethnic_heritage>            <ethnic_heritage>              <ethnic_id>-1</ethnic_id>              <selection_flag>-1</selection_flag>            </ethnic_heritage>          </ethnic_heritages>          <citizen_flag>-1</citizen_flag>          <disability_status>3</disability_status>          <license>            <driver_flag>0</driver_flag>          </license>          <veteran>            <vet_flag>0</vet_flag>            <subscribe_vps>0</subscribe_vps>          </veteran>          <preferences>            <sal>10</sal>            <salary_unit_cd>1</salary_unit_cd>            <resume_searchable>0</resume_searchable>            <contact_info_visible>0</contact_info_visible>            <postings_interested_in>0</postings_interested_in>            <relocate>-1</relocate>            <shift>              <shift_first_flag>-1</shift_first_flag>              <shift_second_flag>-1</shift_second_flag>              <shift_third_flag>-1</shift_third_flag>              <shift_rotating_flag>-1</shift_rotating_flag>              <shift_split_flag>-1</shift_split_flag>              <shift_varies_flag>-1</shift_varies_flag>              <work_type>3</work_type>              <work_over_time>-1</work_over_time>            </shift>          </preferences>        </personal>      </statements>      <summary>        <summary include="1">Summary</summary>      </summary>      <skills>        <internship_skills />      </skills>      <skillrollup />      <professional>        <affiliations include="1">test</affiliations>      </professional>      <hide_options>        <hide_daterange>0</hide_daterange>        <hide_eduDates>0</hide_eduDates>        <hide_militaryserviceDates>0</hide_militaryserviceDates>        <hide_workDates>0</hide_workDates>        <hide_contact>0</hide_contact>        <hide_name>0</hide_name>        <hide_email>0</hide_email>        <hide_homePhoneNumber>0</hide_homePhoneNumber>        <hide_workPhoneNumber>0</hide_workPhoneNumber>        <hide_cellPhoneNumber>0</hide_cellPhoneNumber>        <hide_faxPhoneNumber>0</hide_faxPhoneNumber>        <hide_nonUSPhoneNumber>0</hide_nonUSPhoneNumber>        <hide_affiliations>0</hide_affiliations>        <hide_certifications_professionallicenses>0</hide_certifications_professionallicenses>        <hide_employer>0</hide_employer>        <hide_driver_license>0</hide_driver_license>        <unhide_driver_license>0</unhide_driver_license>        <hide_honors>0</hide_honors>        <hide_interests>0</hide_interests>        <hide_internships>0</hide_internships>        <hide_languages>0</hide_languages>        <hide_veteran>0</hide_veteran>        <hide_objective>0</hide_objective>        <hide_personalinformation>0</hide_personalinformation>        <hide_professionaldevelopment>0</hide_professionaldevelopment>        <hide_publications>0</hide_publications>        <hide_references>0</hide_references>        <hide_skills>0</hide_skills>        <hide_technicalskills>0</hide_technicalskills>        <hide_volunteeractivities>0</hide_volunteeractivities>        <hide_thesismajorprojects>0</hide_thesismajorprojects>        <hidden_skills></hidden_skills>      </hide_options>    </resume>  </ResDoc>', 0),
	('fvn531@explorer.com', 'Incomplete resume to preferences', 1, NULL, '<ResDoc>    <special />    <resume>      <contact>        <name>          <givenname>Partial</givenname>          <surname>Resume</surname>        </name> Home:<phone type="home">1111111111</phone><email>partial@resume.com</email><website></website><address><street>2 Street Address</street><street></street><city>Arlington</city><state_county>51101</state_county><county_fullname>King William</county_fullname><state>VA</state><state_fullname>Virginia</state_fullname><postalcode>22222-2222</postalcode><country>US</country><country_fullname>US</country_fullname></address></contact>      <experience>        <employment_status_cd>1</employment_status_cd>        <job>          <jobid>b7ab28d1-e317-4934-9571-ababdb216be0</jobid>          <employer>BGT</employer>          <title>Applications Programmer</title>          <description>* Wrote computer programs to meet project goals and specifications.  * Conducted trial runs of programs.  * Ensured that programs retrieved information accurately.  * Ensured that programs produced desired information accurately.  * Debugged programs to ensure they run smoothly.</description>          <address>            <city>Boston</city>            <state>MA</state>            <state_fullname>Massachusetts</state_fullname>            <country>US</country>            <country_fullname>United States</country_fullname>          </address>          <daterange>            <start>01-2000</start>            <end currentjob="1">09-2014</end>          </daterange>        </job>        <months_experience>176</months_experience>      </experience>      <education>        <school_status_cd>5</school_status_cd>        <norm_edu_level_cd>26</norm_edu_level_cd>      </education>      <header>        <default>1</default>        <buildmethod>1</buildmethod>        <resumeid>4843362</resumeid>        <resumename>Partial Resume Preferences</resumename>        <onet>15-1131.00</onet>        <ronet>15-1131.S1</ronet>        <status>0</status>        <completionstatus>63</completionstatus>        <updatedon>24/09/2014 17:11:15</updatedon>      </header>      <statements>        <personal>          <sex>1</sex>          <ethnic_heritages>            <ethnic_heritage>              <ethnic_id>3</ethnic_id>              <selection_flag>-1</selection_flag>            </ethnic_heritage>            <ethnic_heritage>              <ethnic_id>4</ethnic_id>              <selection_flag>0</selection_flag>            </ethnic_heritage>            <ethnic_heritage>              <ethnic_id>5</ethnic_id>              <selection_flag>0</selection_flag>            </ethnic_heritage>            <ethnic_heritage>              <ethnic_id>2</ethnic_id>              <selection_flag>0</selection_flag>            </ethnic_heritage>            <ethnic_heritage>              <ethnic_id>6</ethnic_id>              <selection_flag>0</selection_flag>            </ethnic_heritage>            <ethnic_heritage>              <ethnic_id>1</ethnic_id>              <selection_flag>0</selection_flag>            </ethnic_heritage>            <ethnic_heritage>              <ethnic_id>-1</ethnic_id>              <selection_flag>-1</selection_flag>            </ethnic_heritage>          </ethnic_heritages>          <citizen_flag>-1</citizen_flag>          <disability_status>1</disability_status>          <license>            <driver_flag>0</driver_flag>          </license>          <veteran>            <vet_flag>0</vet_flag>            <subscribe_vps>0</subscribe_vps>          </veteran>          <preferences>            <postings_interested_in>0</postings_interested_in>            <shift />          </preferences>        </personal>      </statements>      <summary>        <summary include="1">I have 15 years of experience, including as an Applications Programmer in industries including Building Materials, Hardware, Garden Supply, and Mobile Home Dealers.    Most recently, I have been working as an Applications Programmer at BGT from January 2000 to September 2014.</summary>      </summary>      <skills>        <internship_skills />      </skills>      <skillrollup />      <hide_options>        <hide_daterange>0</hide_daterange>        <hide_eduDates>0</hide_eduDates>        <hide_militaryserviceDates>0</hide_militaryserviceDates>        <hide_workDates>0</hide_workDates>        <hide_contact>0</hide_contact>        <hide_name>0</hide_name>        <hide_email>0</hide_email>        <hide_homePhoneNumber>0</hide_homePhoneNumber>        <hide_workPhoneNumber>0</hide_workPhoneNumber>        <hide_cellPhoneNumber>0</hide_cellPhoneNumber>        <hide_faxPhoneNumber>0</hide_faxPhoneNumber>        <hide_nonUSPhoneNumber>0</hide_nonUSPhoneNumber>        <hide_affiliations>0</hide_affiliations>        <hide_certifications_professionallicenses>0</hide_certifications_professionallicenses>        <hide_employer>0</hide_employer>        <hide_driver_license>0</hide_driver_license>        <unhide_driver_license>0</unhide_driver_license>        <hide_honors>0</hide_honors>        <hide_interests>0</hide_interests>        <hide_internships>0</hide_internships>        <hide_languages>0</hide_languages>        <hide_veteran>0</hide_veteran>        <hide_objective>0</hide_objective>        <hide_personalinformation>0</hide_personalinformation>        <hide_professionaldevelopment>0</hide_professionaldevelopment>        <hide_publications>0</hide_publications>        <hide_references>0</hide_references>        <hide_skills>0</hide_skills>        <hide_technicalskills>0</hide_technicalskills>        <hide_volunteeractivities>0</hide_volunteeractivities>        <hide_thesismajorprojects>0</hide_thesismajorprojects>        <hidden_skills></hidden_skills>      </hide_options>    </resume>  </ResDoc>', 0)
	
UPDATE
	TR
SET
	PersonId = P.Id,
	FirstName = P.FirstName,
	LastName = P.LastName
FROM
	@Resumes TR
INNER JOIN [Data.Application.Person] P
	ON P.EmailAddress = TR.PersonEmailAddress

DECLARE @PersonId INT
DECLARE @PersonEmailAddress NVARCHAR(255)
DECLARE @ResumeName NVARCHAR(255)
DECLARE @IsDefault BIT
DECLARE @FirstName NVARCHAR(40)
DECLARE @LastName NVARCHAR(40)
DECLARE @ResumeId BIGINT
DECLARE @OverrideResumeXml NVARCHAR(MAX)
DECLARE @CompletionDate DATETIME
DECLARE @IsVeteran BIT
DECLARE @DefaultResumeXml NVARCHAR(MAX)

DECLARE @NextId BIGINT

DECLARE @StateId BIGINT
DECLARE @CountryId BIGINT
DECLARE @CountyId BIGINT

SELECT @StateId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'States' AND [Key] = 'State.TX'
SELECT @CountryId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Countries' AND [Key] = 'Country.US'
SELECT @CountyId = Id FROM [Config.LookupItemsView] WHERE LookupType = 'Counties' AND [Key] = 'County.ClayTX'

DECLARE @ResumeCount INT
DECLARE @TotalResumes INT

SET @ResumeCount = 1
SELECT @TotalResumes = COUNT(1) FROM @Resumes

WHILE @ResumeCount <= @TotalResumes
BEGIN
	SELECT 
		@PersonId = PersonId,
		@ResumeName = ResumeName,
		@IsDefault = IsDefault,
		@PersonEmailAddress = PersonEmailAddress,
		@FirstName = FirstName,
		@LastName = LastName,
		@OverrideResumeXml = OverrideResumeXml,
		@CompletionDate = CompletionDate,
		@IsVeteran = IsVeteran
	FROM
		@Resumes
	WHERE
		Id = @ResumeCount
		
	SET @DefaultResumeXml = '<ResDoc><special><preserved_format>Standard</preserved_format><preserved_order>Emphasize my experience</preserved_order></special><resume><contact><name><givenname>' + @FirstName + '</givenname><surname>' + @LastName + '</surname></name> Home:<phone type="home">1111111111</phone><email>' + @PersonEmailAddress + '</email><website></website><address><street>Address Line 1</street><street></street><city>Bluegrove</city><state_county>48077</state_county><county_fullname>Clay</county_fullname><state>TX</state><state_fullname>Texas</state_fullname><postalcode>76352</postalcode><country>US</country><country_fullname>US</country_fullname></address></contact><experience><employment_status_cd>1</employment_status_cd></experience><education><school_status_cd>5</school_status_cd><norm_edu_level_cd>20</norm_edu_level_cd></education><header><default>1</default><buildmethod>1</buildmethod><resumeid>4760449</resumeid><status>1</status><completionstatus>127</completionstatus><updatedon>23/07/2014 08:44:57</updatedon></header><statements><personal><sex>2</sex><ethnic_heritages><ethnic_heritage><ethnic_id>3</ethnic_id><selection_flag>-1</selection_flag></ethnic_heritage><ethnic_heritage><ethnic_id>4</ethnic_id><selection_flag>0</selection_flag></ethnic_heritage><ethnic_heritage><ethnic_id>5</ethnic_id><selection_flag>0</selection_flag></ethnic_heritage><ethnic_heritage><ethnic_id>2</ethnic_id><selection_flag>0</selection_flag></ethnic_heritage><ethnic_heritage><ethnic_id>6</ethnic_id><selection_flag>0</selection_flag></ethnic_heritage><ethnic_heritage><ethnic_id>1</ethnic_id><selection_flag>0</selection_flag></ethnic_heritage><ethnic_heritage><ethnic_id>-1</ethnic_id><selection_flag>-1</selection_flag></ethnic_heritage></ethnic_heritages><citizen_flag>-1</citizen_flag><disability_status>3</disability_status><license><driver_flag>0</driver_flag></license><veteran><vet_flag>0</vet_flag><subscribe_vps>0</subscribe_vps></veteran><preferences><sal>10</sal><salary_unit_cd>1</salary_unit_cd><resume_searchable>0</resume_searchable><postings_interested_in>0</postings_interested_in><relocate>-1</relocate><shift><shift_first_flag>-1</shift_first_flag><shift_second_flag>-1</shift_second_flag><shift_third_flag>-1</shift_third_flag><shift_rotating_flag>-1</shift_rotating_flag><shift_split_flag>-1</shift_split_flag><shift_varies_flag>-1</shift_varies_flag><work_type>1</work_type><work_over_time>-1</work_over_time></shift></preferences></personal></statements><summary><summary include="1">Summary</summary></summary><skills><internship_skills /></skills><skillrollup /><hide_options><hide_daterange>0</hide_daterange><hide_eduDates>0</hide_eduDates><hide_militaryserviceDates>0</hide_militaryserviceDates><hide_workDates>0</hide_workDates><hide_contact>0</hide_contact><hide_name>0</hide_name><hide_email>0</hide_email><hide_homePhoneNumber>0</hide_homePhoneNumber><hide_workPhoneNumber>0</hide_workPhoneNumber><hide_cellPhoneNumber>0</hide_cellPhoneNumber><hide_faxPhoneNumber>0</hide_faxPhoneNumber><hide_nonUSPhoneNumber>0</hide_nonUSPhoneNumber><hide_affiliations>0</hide_affiliations><hide_certifications_professionallicenses>0</hide_certifications_professionallicenses><hide_employer>0</hide_employer><hide_driver_license>0</hide_driver_license><unhide_driver_license>0</unhide_driver_license><hide_honors>0</hide_honors><hide_interests>0</hide_interests><hide_internships>0</hide_internships><hide_languages>0</hide_languages><hide_veteran>0</hide_veteran><hide_objective>0</hide_objective><hide_personalinformation>0</hide_personalinformation><hide_professionaldevelopment>0</hide_professionaldevelopment><hide_publications>0</hide_publications><hide_references>0</hide_references><hide_skills>0</hide_skills><hide_technicalskills>0</hide_technicalskills><hide_volunteeractivities>0</hide_volunteeractivities><hide_thesismajorprojects>0</hide_thesismajorprojects><hidden_skills></hidden_skills></hide_options></resume></ResDoc>'
	
	IF @PersonId IS NOT NULL 
	AND NOT EXISTS(SELECT 1 FROM [Data.Application.Resume] WHERE PersonId = @PersonId AND ResumeName = @ResumeName AND StatusId = 1)
	BEGIN
		BEGIN TRANSACTION
		
		SELECT @NextId = NextId FROM KeyTable
		
		UPDATE KeyTable SET NextId = NextId + 1
		
		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END
		
		SET @ResumeId = @NextId

		INSERT INTO [Data.Application.Resume]
		(
			Id,
			PrimaryOnet,
			PrimaryROnet,
			ResumeSkills,
			FirstName,
			LastName,
			MiddleName,
			DateOfBirth,
			Gender,
			AddressLine1,
			TownCity,
			StateId,
			CountryId,
			PostcodeZip,
			WorkPhone,
			HomePhone,
			MobilePhone,
			FaxNumber,
			EmailAddress,
			HighestEducationLevel,
			YearsExperience,
			IsVeteran,
			IsActive,
			StatusId,
			ResumeXml,
			ResumeCompletionStatusId,
			IsDefault,
			ResumeName,
			IsSearchable,
			CreatedOn,
			UpdatedOn,
			PersonId,
			SkillsAlreadyCaptured,
			CountyId,
			SkillCount,
			WordCount,
			VeteranPriorityServiceAlertsSubscription,
			EducationCompletionDate
		)
		VALUES
		(
			@ResumeId,
			'',
			'',
			'',
			@FirstName,
			@LastName,
			NULL,
			'01 Jan 1980 00:00:00.000',
			2,
			'Address Line 1',
			'Bluegrove',
			@StateId,
			@CountryId,
			'76352',
			NULL,
			'1111111111',
			NULL,
			NULL,
			@PersonEmailAddress,
			20,
			0,
			@IsVeteran,
			1,
			1,
			ISNULL(REPLACE(@OverrideResumeXml, '##RESUMEID##', @ResumeId), REPLACE(@DefaultResumeXml, '##RESUMEID##', @ResumeId)),
			127,
			1,
			@ResumeName,
			0,
			GETDATE(),
			GETDATE(),
			@PersonId,
			NULL,
			@CountyId,
			0,
			49,
			0,
			@CompletionDate
		)

		IF @@ERROR <> 0
		BEGIN
			ROLLBACK TRANSACTION
			RETURN
		END

		COMMIT TRANSACTION
	END
	
	SET @ResumeCount = @ResumeCount + 1
END