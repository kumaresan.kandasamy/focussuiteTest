DECLARE @SavedSearch TABLE
(
	Id BIGINT IDENTITY(1, 1),
	EmailAddress NVARCHAR(255),
	SearchName NVARCHAR(200),
	SearchCriteria NVARCHAR(MAX)
)

INSERT INTO @SavedSearch (EmailAddress, SearchName, SearchCriteria)
VALUES 
	('UIClaimantWith1SavedSearch@explorer.com', 'Search 1', '<?xml version="1.0" encoding="utf-16"?>  <SearchCriteria xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">    <PageIndex>0</PageIndex>    <PageSize>10</PageSize>    <ListSize>250</ListSize>    <FetchOption>Single</FetchOption>    <SearchType>Postings</SearchType>    <RequiredResultCriteria>      <DocumentsToSearch>Posting</DocumentsToSearch>      <MaximumDocumentCount>200</MaximumDocumentCount>      <MinimumStarMatch>None</MinimumStarMatch>      <IncludeDuties>false</IncludeDuties>    </RequiredResultCriteria>    <KeywordCriteria>      <KeywordText>Programmer</KeywordText>      <SearchLocation>Anywhere</SearchLocation>      <Operator>And</Operator>    </KeywordCriteria>    <JobLocationCriteria>      <OnlyShowJobInMyState>false</OnlyShowJobInMyState>      <Area>        <StateId>0</StateId>        <MsaId>0</MsaId>      </Area>    </JobLocationCriteria>    <PostingAgeCriteria>      <PostingAgeInDays>7</PostingAgeInDays>      <MinimumPostingAgeInDays xsi:nil="true" />    </PostingAgeCriteria>    <InternshipCriteria>      <Internship>NoFilter</Internship>    </InternshipCriteria>  </SearchCriteria>'),
	('UIClaimantWith3jobAlerts@explorer.com', 'Search 1', '<?xml version="1.0" encoding="utf-16"?>  <SearchCriteria xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">    <PageIndex>0</PageIndex>    <PageSize>10</PageSize>    <ListSize>250</ListSize>    <FetchOption>Single</FetchOption>    <SearchType>Postings</SearchType>    <RequiredResultCriteria>      <DocumentsToSearch>Posting</DocumentsToSearch>      <MaximumDocumentCount>200</MaximumDocumentCount>      <MinimumStarMatch>None</MinimumStarMatch>      <IncludeDuties>false</IncludeDuties>    </RequiredResultCriteria>    <KeywordCriteria>      <KeywordText>Programmer</KeywordText>      <SearchLocation>Anywhere</SearchLocation>      <Operator>And</Operator>    </KeywordCriteria>    <JobLocationCriteria>      <OnlyShowJobInMyState>false</OnlyShowJobInMyState>      <Area>        <StateId>0</StateId>        <MsaId>0</MsaId>      </Area>    </JobLocationCriteria>    <PostingAgeCriteria>      <PostingAgeInDays>7</PostingAgeInDays>      <MinimumPostingAgeInDays xsi:nil="true" />    </PostingAgeCriteria>    <InternshipCriteria>      <Internship>NoFilter</Internship>    </InternshipCriteria>  </SearchCriteria>'),
	('UIClaimantWith3jobAlerts@explorer.com', 'Search 2', '<?xml version="1.0" encoding="utf-16"?>  <SearchCriteria xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">    <PageIndex>0</PageIndex>    <PageSize>10</PageSize>    <ListSize>250</ListSize>    <FetchOption>Single</FetchOption>    <SearchType>Postings</SearchType>    <RequiredResultCriteria>      <DocumentsToSearch>Posting</DocumentsToSearch>      <MaximumDocumentCount>200</MaximumDocumentCount>      <MinimumStarMatch>None</MinimumStarMatch>      <IncludeDuties>false</IncludeDuties>    </RequiredResultCriteria>    <KeywordCriteria>      <KeywordText>Programmer</KeywordText>      <SearchLocation>Anywhere</SearchLocation>      <Operator>And</Operator>    </KeywordCriteria>    <JobLocationCriteria>      <OnlyShowJobInMyState>false</OnlyShowJobInMyState>      <Area>        <StateId>0</StateId>        <MsaId>0</MsaId>      </Area>    </JobLocationCriteria>    <PostingAgeCriteria>      <PostingAgeInDays>7</PostingAgeInDays>      <MinimumPostingAgeInDays xsi:nil="true" />    </PostingAgeCriteria>    <InternshipCriteria>      <Internship>NoFilter</Internship>    </InternshipCriteria>  </SearchCriteria>'),
	('UIClaimantWith3jobAlerts@explorer.com', 'Search 3', '<?xml version="1.0" encoding="utf-16"?>  <SearchCriteria xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">    <PageIndex>0</PageIndex>    <PageSize>10</PageSize>    <ListSize>250</ListSize>    <FetchOption>Single</FetchOption>    <SearchType>Postings</SearchType>    <RequiredResultCriteria>      <DocumentsToSearch>Posting</DocumentsToSearch>      <MaximumDocumentCount>200</MaximumDocumentCount>      <MinimumStarMatch>None</MinimumStarMatch>      <IncludeDuties>false</IncludeDuties>    </RequiredResultCriteria>    <KeywordCriteria>      <KeywordText>Programmer</KeywordText>      <SearchLocation>Anywhere</SearchLocation>      <Operator>And</Operator>    </KeywordCriteria>    <JobLocationCriteria>      <OnlyShowJobInMyState>false</OnlyShowJobInMyState>      <Area>        <StateId>0</StateId>        <MsaId>0</MsaId>      </Area>    </JobLocationCriteria>    <PostingAgeCriteria>      <PostingAgeInDays>7</PostingAgeInDays>      <MinimumPostingAgeInDays xsi:nil="true" />    </PostingAgeCriteria>    <InternshipCriteria>      <Internship>NoFilter</Internship>    </InternshipCriteria>  </SearchCriteria>'),
	('NonUIClaimant1JobAlert@explorer.com', 'Search 1', '<?xml version="1.0" encoding="utf-16"?>  <SearchCriteria xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">    <PageIndex>0</PageIndex>    <PageSize>10</PageSize>    <ListSize>250</ListSize>    <FetchOption>Single</FetchOption>    <SearchType>Postings</SearchType>    <RequiredResultCriteria>      <DocumentsToSearch>Posting</DocumentsToSearch>      <MaximumDocumentCount>200</MaximumDocumentCount>      <MinimumStarMatch>None</MinimumStarMatch>      <IncludeDuties>false</IncludeDuties>    </RequiredResultCriteria>    <KeywordCriteria>      <KeywordText>Programmer</KeywordText>      <SearchLocation>Anywhere</SearchLocation>      <Operator>And</Operator>    </KeywordCriteria>    <JobLocationCriteria>      <OnlyShowJobInMyState>false</OnlyShowJobInMyState>      <Area>        <StateId>0</StateId>        <MsaId>0</MsaId>      </Area>    </JobLocationCriteria>    <PostingAgeCriteria>      <PostingAgeInDays>7</PostingAgeInDays>      <MinimumPostingAgeInDays xsi:nil="true" />    </PostingAgeCriteria>    <InternshipCriteria>      <Internship>NoFilter</Internship>    </InternshipCriteria>  </SearchCriteria>'),
	('NonUIClaimant2JobAlerts@explorer.com', 'Search 1', '<?xml version="1.0" encoding="utf-16"?>  <SearchCriteria xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">    <PageIndex>0</PageIndex>    <PageSize>10</PageSize>    <ListSize>250</ListSize>    <FetchOption>Single</FetchOption>    <SearchType>Postings</SearchType>    <RequiredResultCriteria>      <DocumentsToSearch>Posting</DocumentsToSearch>      <MaximumDocumentCount>200</MaximumDocumentCount>      <MinimumStarMatch>None</MinimumStarMatch>      <IncludeDuties>false</IncludeDuties>    </RequiredResultCriteria>    <KeywordCriteria>      <KeywordText>Programmer</KeywordText>      <SearchLocation>Anywhere</SearchLocation>      <Operator>And</Operator>    </KeywordCriteria>    <JobLocationCriteria>      <OnlyShowJobInMyState>false</OnlyShowJobInMyState>      <Area>        <StateId>0</StateId>        <MsaId>0</MsaId>      </Area>    </JobLocationCriteria>    <PostingAgeCriteria>      <PostingAgeInDays>7</PostingAgeInDays>      <MinimumPostingAgeInDays xsi:nil="true" />    </PostingAgeCriteria>    <InternshipCriteria>      <Internship>NoFilter</Internship>    </InternshipCriteria>  </SearchCriteria>'),
	('NonUIClaimant2JobAlerts@explorer.com', 'Search 2', '<?xml version="1.0" encoding="utf-16"?>  <SearchCriteria xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">    <PageIndex>0</PageIndex>    <PageSize>10</PageSize>    <ListSize>250</ListSize>    <FetchOption>Single</FetchOption>    <SearchType>Postings</SearchType>    <RequiredResultCriteria>      <DocumentsToSearch>Posting</DocumentsToSearch>      <MaximumDocumentCount>200</MaximumDocumentCount>      <MinimumStarMatch>None</MinimumStarMatch>      <IncludeDuties>false</IncludeDuties>    </RequiredResultCriteria>    <KeywordCriteria>      <KeywordText>Programmer</KeywordText>      <SearchLocation>Anywhere</SearchLocation>      <Operator>And</Operator>    </KeywordCriteria>    <JobLocationCriteria>      <OnlyShowJobInMyState>false</OnlyShowJobInMyState>      <Area>        <StateId>0</StateId>        <MsaId>0</MsaId>      </Area>    </JobLocationCriteria>    <PostingAgeCriteria>      <PostingAgeInDays>7</PostingAgeInDays>      <MinimumPostingAgeInDays xsi:nil="true" />    </PostingAgeCriteria>    <InternshipCriteria>      <Internship>NoFilter</Internship>    </InternshipCriteria>  </SearchCriteria>')

DECLARE @SearchIndex INT
DECLARE @SearchCount INT

DECLARE @NextId BIGINT

DECLARE @SavedSearchId BIGINT
DECLARE @SavedSearchUserId BIGINT
DECLARE @UserId BIGINT

DECLARE @EmailAddress NVARCHAR(255)
DECLARE @SearchName NVARCHAR(200)
DECLARE @SearchCriteria NVARCHAR(MAX)
	
SET @SearchIndex = 1
SELECT @SearchCount = COUNT(1) FROM @SavedSearch

WHILE @SearchIndex <= @SearchCount
BEGIN
	SELECT 
		@EmailAddress = EmailAddress,
		@SearchName = SearchName,
		@SearchCriteria = SearchCriteria
	FROM
		@SavedSearch
	WHERE
		Id = @SearchIndex
		
	BEGIN TRANSACTION
	
	SELECT @NextId = NextId FROM KeyTable
	
	UPDATE KeyTable SET NextId = NextId + 2
	
	IF @@ERROR <> 0
	BEGIN
		ROLLBACK TRANSACTION
		RETURN
	END
	
	SET @SavedSearchId = @NextId
	SET @SavedSearchUserId = @NextId + 1
	
	INSERT INTO [Data.Application.SavedSearch]
	(
		Id,
		Name
		  ,SearchCriteria
		  ,AlertEmailRequired
		  ,AlertEmailFrequency
		  ,AlertEmailFormat
		  ,AlertEmailAddress
		  ,AlertEmailScheduledOn
		  ,[Type]
		  ,CreatedOn
		  ,UpdatedOn
	)
	VALUES
	(
		@SavedSearchId,
		@SearchName,
		@SearchCriteria,
		0,
		1,
		0,
		'',
		NULL,
		2,
		GETDATE(),
		GETDATE()
	)

	IF @@ERROR <> 0
	BEGIN
		ROLLBACK TRANSACTION
		RETURN
	END
	
	SELECT @UserId = Id FROM [Data.Application.User] WHERE UserName = @EmailAddress
	
	INSERT INTO [Data.Application.SavedSearchUser]
	(
		Id,
		UserId,
		SavedSearchId
	)
	VALUES
	(
		@SavedSearchUserId,
		@UserId,
		@SavedSearchId
	)
	
	IF @@ERROR <> 0
	BEGIN
		ROLLBACK TRANSACTION
		RETURN
	END
	
	COMMIT TRANSACTION
	
	SET @SearchIndex = @SearchIndex + 1
END