IF EXISTS (
  SELECT * FROM sys.objects WHERE NAME = N'DF__Config.Co__Value__1273C1CD'
)
ALTER TABLE [Config.ConfigurationItem] DROP CONSTRAINT [DF__Config.Co__Value__1273C1CD]
GO
ALTER TABLE [Config.ConfigurationItem] ALTER COLUMN [Value] NVARCHAR(MAX) 
GO
ALTER TABLE [Config.ConfigurationItem]
  ADD CONSTRAINT [DF__Config.Co__Value__1273C1CD]
  DEFAULT '' FOR [Value]