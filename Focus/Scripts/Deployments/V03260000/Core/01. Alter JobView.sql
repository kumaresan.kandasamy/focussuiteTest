IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'Data.Application.JobView') 
BEGIN
	DROP VIEW [dbo].[Data.Application.JobView]
END
GO

CREATE VIEW [dbo].[Data.Application.JobView] 

AS
WITH 
	JobLocation AS
	(
		SELECT 
			jl.JobId,
			ROW_NUMBER() OVER (PARTITION BY jl.JobId ORDER BY jl.id) AS LocationNumber,
			jl.Location
		FROM
			dbo.[Data.Application.JobLocation] jl WITH (NOLOCK) 
	), 
	ApplicationCount AS
	(
		SELECT
			p.JobId,
			COUNT(CASE WHEN ca.ApprovalStatus = 2 THEN 1 ELSE NULL END) AS ApplicationCount,
			COUNT(CASE WHEN ca.ApprovalStatus = 2 AND R.IsVeteran = 1 THEN 1 ELSE NULL END) AS VeteranApplicationCount,
			COUNT(CASE WHEN ca.ApprovalStatus IN (1, 5) THEN 1 ELSE NULL END) AS ReferralCount
		FROM 
			dbo.[Data.Application.Application] AS ca WITH (NOLOCK) 
		INNER JOIN dbo.[Data.Application.Resume] AS r WITH (NOLOCK)
			ON r.Id = ca.ResumeId
		INNER JOIN dbo.[Data.Application.Posting] AS p WITH (NOLOCK)
			ON p.Id = ca.PostingId
		WHERE 
			ca.ApprovalStatus IN (1, 2, 5)  
		GROUP BY 
			p.JobId
	)
SELECT 
	j.Id, 
	j.EmployerId, 
	e.Name AS EmployerName, 
	j.JobTitle, 
	bu.Name AS BusinessUnitName, 
	j.BusinessUnitId, 
	j.JobStatus, 
	j.PostedOn, 
	j.ClosingOn, 
	j.HeldOn,   
    j.ClosedOn, 
    j.UpdatedOn, 
	ISNULL(cag.ApplicationCount, 0) AS ApplicationCount, 
	ISNULL(cag.VeteranApplicationCount, 0) AS VeteranApplicationCount,
    j.EmployeeId, j.ApprovalStatus, 
    ISNULL(cag.ReferralCount, 0) AS ReferralCount,   
    j.FederalContractor, 
    j.ForeignLabourCertificationH2A, 
    j.ForeignLabourCertificationH2B, 
    j.ForeignLabourCertificationOther, 
    j.CourtOrderedAffirmativeAction,   
    u.UserName, 
    p.FirstName AS HiringManagerFirstName, 
    p.LastName AS HiringManagerLastName, 
    j.CreatedOn, 
    j.YellowProfanityWords, 
    j.IsConfidential, 
    j.CreatedBy,
    j.UpdatedBy, 
    j.JobType,
    j.AssignedToId,
    j.NumberOfOpenings,
    j.VeteranPriorityEndDate,
    jl.Location
FROM  
	dbo.[Data.Application.Job] AS j WITH (NOLOCK) 
INNER JOIN dbo.[Data.Application.Employee] AS employee WITH (NOLOCK) 
	ON j.EmployeeId = employee.Id 
INNER JOIN dbo.[Data.Application.Person] AS p WITH (NOLOCK) 
	ON employee.PersonId = p.Id 
INNER JOIN dbo.[Data.Application.User] AS u WITH (NOLOCK) 
	ON employee.PersonId = u.PersonId 
INNER JOIN dbo.[Data.Application.Employer] AS e WITH (NOLOCK) 
	ON j.EmployerId = e.Id 
INNER JOIN dbo.[Data.Application.BusinessUnit] AS bu WITH (NOLOCK) 
	ON bu.Id = j.BusinessUnitId
LEFT OUTER JOIN ApplicationCount cag
	ON j.id = cag.JobId
LEFT JOIN JobLocation jl WITH (NOLOCK) 
	ON jl.JobId = j.Id
WHERE
	ISNULL(jl.LocationNumber, 1) = 1