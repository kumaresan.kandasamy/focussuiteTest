IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Data.Core.IntegrationImportRecord')
BEGIN
	CREATE TABLE [Data.Core.IntegrationImportRecord]
	(
		Id BIGINT IDENTITY(1, 1) PRIMARY KEY,
		RecordType INT NOT NULL,
		ExternalId NVARCHAR(255) NOT NULL,
		[Status] INT NOT NULL DEFAULT(0),
		ImportStartTime DATETIME NULL,
		ImportEndTime DATETIME NULL,
		Details NVARCHAR(2000) NULL,
		CreatedOn DATETIME NOT NULL DEFAULT(GETDATE())
	)
	
	CREATE NONCLUSTERED INDEX [IX_Data.Core.IntegrationImportRecord_RecordType]  ON [Data.Core.IntegrationImportRecord] (RecordType) 
END

