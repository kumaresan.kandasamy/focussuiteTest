﻿IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Report.JobSeekerMilitaryHistory_NonPurged')
BEGIN
CREATE TABLE [dbo].[Report.JobSeekerMilitaryHistory_NonPurged] 
(
	[Id]												BIGINT NOT NULL,
	[VeteranDisability]					INT NULL,
	[VeteranTransitionType]			INT NULL,
	[VeteranMilitaryDischarge]	INT NULL,
	[VeteranBranchOfService]		INT NULL,
	[CampaignVeteran]						BIT NULL,
	[MilitaryStartDate]					DATETIME NULL,
	[MilitaryEndDate]						DATETIME NULL,
	[JobSeekerId]								BIGINT NULL,
	[CreatedOn]									DATETIME NOT NULL,
	[UpdatedOn]									DATETIME NOT NULL,
	[DeletedOn]									DATETIME NULL,
	[VeteranType]								INT NULL,
	PRIMARY KEY CLUSTERED ([Id] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF)
)
END
GO




