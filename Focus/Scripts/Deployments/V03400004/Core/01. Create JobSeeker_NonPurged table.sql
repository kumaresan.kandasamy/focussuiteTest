﻿IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'Report.JobSeeker_NonPurged')
BEGIN
CREATE TABLE [dbo].[Report.JobSeeker_NonPurged] 
(
	[Id]								BIGINT NOT NULL,
	[JobSeekerReportId] BIGINT NOT NULL DEFAULT 0,
	[DateOfBirth]				NVARCHAR(100) NULL,
	[Gender]						INT NULL,
	[DisabilityStatus]	INT NULL,
	[DisabilityType]		INT NULL,
	[EthnicHeritage]		INT NULL,
	[Race]							INT NULL,
	[MinimumEducationLevel] INT NULL,
	[EnrollmentStatus]	INT NULL,
	[EmploymentStatus]	INT NULL,
	[Age]								SMALLINT NULL,
	[CreatedOn]					DATETIME NOT NULL,
	[UpdatedOn]					DATETIME NOT NULL,
	[IsCitizen]					BIT NULL,
	[AlienCardExpires]	DATETIME NULL,
	[AlienCardId]				NVARCHAR(MAX) NULL,
	[CountyId]					BIGINT NULL,
	PRIMARY KEY CLUSTERED ([Id] ASC) WITH (ALLOW_PAGE_LOCKS = ON, ALLOW_ROW_LOCKS = ON, PAD_INDEX = OFF, IGNORE_DUP_KEY = OFF, STATISTICS_NORECOMPUTE = OFF)
)
END
GO

