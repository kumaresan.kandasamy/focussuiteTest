--SELECT * FROM [Config.ConfigurationItem] WHERE [Key] = 'BatchProcessSettings'

DECLARE @BatchSettings NVARCHAR(4000)

SELECT @BatchSettings = Value FROM [Config.ConfigurationItem] WHERE [Key] = 'BatchProcessSettings'

IF NOT (@BatchSettings LIKE '%{"Process":12,%')
BEGIN
	UPDATE 
		[Config.ConfigurationItem] 
	SET
		Value = LEFT(@BatchSettings, LEN(@BatchSettings) - 1) + ',{"Process":12,"Identifier":"8510e112659c4e5699595e5c7be12f8c"}]'
	WHERE 
		[Key] = 'BatchProcessSettings'
		AND Value = @BatchSettings
END
