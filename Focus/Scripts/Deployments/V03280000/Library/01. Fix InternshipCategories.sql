DECLARE @InternshipCategories TABLE
(
	Id BIGINT,
	LensFilterId INT
)
INSERT INTO @InternshipCategories (Id, LensFilterId) VALUES (1,1)
INSERT INTO @InternshipCategories (Id, LensFilterId) VALUES (2,2)
INSERT INTO @InternshipCategories (Id, LensFilterId) VALUES (3,4)
INSERT INTO @InternshipCategories (Id, LensFilterId) VALUES (4,5)
INSERT INTO @InternshipCategories (Id, LensFilterId) VALUES (5,6)
INSERT INTO @InternshipCategories (Id, LensFilterId) VALUES (6,7)
INSERT INTO @InternshipCategories (Id, LensFilterId) VALUES (7,9)
INSERT INTO @InternshipCategories (Id, LensFilterId) VALUES (8,10)
INSERT INTO @InternshipCategories (Id, LensFilterId) VALUES (9,11)
INSERT INTO @InternshipCategories (Id, LensFilterId) VALUES (10,12)
INSERT INTO @InternshipCategories (Id, LensFilterId) VALUES (11,14)
INSERT INTO @InternshipCategories (Id, LensFilterId) VALUES (12,15)
INSERT INTO @InternshipCategories (Id, LensFilterId) VALUES (13,16)
INSERT INTO @InternshipCategories (Id, LensFilterId) VALUES (14,17)
INSERT INTO @InternshipCategories (Id, LensFilterId) VALUES (15,18)
INSERT INTO @InternshipCategories (Id, LensFilterId) VALUES (16,19)
INSERT INTO @InternshipCategories (Id, LensFilterId) VALUES (17,20)
INSERT INTO @InternshipCategories (Id, LensFilterId) VALUES (18,21)
INSERT INTO @InternshipCategories (Id, LensFilterId) VALUES (19,22)
INSERT INTO @InternshipCategories (Id, LensFilterId) VALUES (20,23)
INSERT INTO @InternshipCategories (Id, LensFilterId) VALUES (21,24)
INSERT INTO @InternshipCategories (Id, LensFilterId) VALUES (22,25)

UPDATE
	IC
SET
	LensFilterId = T.LensFilterId
FROM
	[Library.InternshipCategory] IC
INNER JOIN @InternshipCategories T
	ON T.Id = IC.Id
WHERE
	IC.LensFilterId = 0
GO