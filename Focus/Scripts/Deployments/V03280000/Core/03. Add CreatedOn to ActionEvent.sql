﻿IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'Data.Core.ActionEvent' AND COLUMN_NAME = 'CreatedOn')
BEGIN
  ALTER TABLE [dbo].[Data.Core.ActionEvent] ADD [CreatedOn] [DateTime] NOT NULL DEFAULT (GETDATE())
END
GO

UPDATE [dbo].[Data.Core.ActionEvent]
SET    [dbo].[Data.Core.ActionEvent].CreatedOn = [dbo].[Data.Core.ActionEvent].ActionedOn
FROM   [dbo].[Data.Core.ActionEvent]