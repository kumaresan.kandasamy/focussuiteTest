﻿IF NOT EXISTS
(
	SELECT 
		1
	FROM 
		sys.tables T
	INNER JOIN sys.indexes I
		ON I.object_id = T.object_id
	INNER JOIN sys.index_columns IC
		ON IC.index_id = I.index_id
		AND IC.object_id = T.object_id
	INNER JOIN sys.columns C
		ON C.column_id = IC.column_id
		AND C.object_id = T.object_id
	WHERE T.name = 'Data.Core.Session'
		AND C.name = 'LastRequestOn'
		AND key_ordinal = 1
)
BEGIN
	CREATE INDEX 
		[IX.Data.Core.Session.LastRequestedOn] 
	ON 
		[Data.Core.Session] ([LastRequestOn])
END
GO
