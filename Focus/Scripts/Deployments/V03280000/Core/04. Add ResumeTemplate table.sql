-- Create [Data.Application.ResumeTemplate] table for Focus
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[Data.Application.ResumeTemplate]') AND type in (N'U'))
BEGIN
CREATE TABLE [dbo].[Data.Application.ResumeTemplate](
	[Id] [bigint] NOT NULL,
	[ResumeXml] [nvarchar](max) NOT NULL,
	[CreatedOn] [datetime] NOT NULL,
	[UpdatedOn] [datetime] NOT NULL,
	[PersonId] [bigint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

ALTER TABLE [dbo].[Data.Application.ResumeTemplate] ADD  DEFAULT ('') FOR [ResumeXml]
ALTER TABLE [dbo].[Data.Application.ResumeTemplate] ADD  DEFAULT ((0)) FOR [PersonId]
ALTER TABLE [dbo].[Data.Application.ResumeTemplate]  WITH CHECK ADD FOREIGN KEY([PersonId]) REFERENCES [dbo].[Data.Application.Person] ([Id])
END
GO