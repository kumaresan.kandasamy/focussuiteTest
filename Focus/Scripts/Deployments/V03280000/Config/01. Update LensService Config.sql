UPDATE 
	[Config.ConfigurationItem] 
SET
	[Value] = REPLACE(Value, '"readonly":false', '"readonly":false,"anonymousaccess":true')
WHERE 
	[Key] = 'LensService' 
	AND Value LIKE '%"serviceType":"posting"%' 
	AND Value LIKE '%"readonly":false%'
	AND Value NOT LIKE '%"vendor":"spideredjobs"%' 
	AND Value NOT LIKE '%"description":"Spidered%'
	AND Value NOT LIKE '%"anonymousaccess"%'


