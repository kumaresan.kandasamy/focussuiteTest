UPDATE FocusDevLibraryImport.dbo.StudyPlace
SET StateAreaId = NULL, StudyPlaceId = NULL
GO

-- set StateAreaId
UPDATE
	spdc
SET
	spdc.StateAreaId = g.StateAreaId
FROM
	FocusDevLibraryImport.dbo.StudyPlace spdc
	INNER JOIN FocusDevLibraryImport.dbo.[Geography] g ON spdc.[State] = g.PrimaryState AND CONVERT(nvarchar(255),spdc.Area) = g.Area
WHERE
	g.AreaType = 'MSA' OR g.AreaType ='State'

-- StudyPlace
INSERT INTO FocusDevLibrary.dbo.[Library.StudyPlace]
(Name,StateAreaId)
SELECT DISTINCT
	Institution,
	StateAreaId
FROM
	FocusDevLibraryImport.dbo.StudyPlace
WHERE
	StateAreaId IS NOT NULL


UPDATE
	spdc
SET
	spdc.StudyPlaceId = sp.Id
FROM
	FocusDevLibraryImport.dbo.StudyPlace spdc
	INNER JOIN FocusDevLibrary.dbo.[Library.StudyPlace] sp ON spdc.Institution = sp.Name AND spdc.StateAreaId = sp.StateAreaId
WHERE
	spdc.StateAreaId IS NOT NULL

-- StudyPlaceDegreeEducationLevel
INSERT INTO FocusDevLibrary.dbo.[Library.StudyPlaceDegreeEducationLevel]
(StudyPlaceId,DegreeEducationLevelId)
SELECT DISTINCT
	spdc.StudyPlaceId,
	del.Id
FROM
	FocusDevLibraryImport.dbo.StudyPlace spdc
	INNER JOIN FocusDevLibrary.dbo.[Library.Degree] d ON spdc.Rcip = d.RCIPCode
	INNER JOIN FocusDevLibrary.dbo.[Library.DegreeEducationLevel] del ON d.Id = del.DegreeId AND spdc.DegreeLevel = del.EducationLevel
WHERE
	spdc.StudyPlaceId IS NOT NULL
