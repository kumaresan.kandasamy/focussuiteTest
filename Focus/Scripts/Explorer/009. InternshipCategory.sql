SET IDENTITY_INSERT FocusDevLibrary.dbo.[Library.InternshipCategory] ON
INSERT INTO FocusDevLibrary.dbo.[Library.InternshipCategory]
(Id,Name,LocalisationId)
SELECT DISTINCT
	Id,
	Name,
	12090
FROM
	FocusDevLibraryImport.dbo.Bookmarks_InternshipCategory


SET IDENTITY_INSERT FocusDevLibrary.dbo.[Library.InternshipCategory] OFF
