DELETE FROM FocusDevLibrary.dbo.[Library.JobRelatedJob]
INSERT INTO FocusDevLibrary.dbo.[Library.JobRelatedJob]
(JobId,RelatedJobId,Priority)
SELECT
	r.JobId,
	r2.JobId,
	rr.Priority
FROM
	FocusDevLibraryImport.dbo.JobRelatedJob rr
	INNER JOIN FocusDevLibraryImport.dbo.Job r ON rr.A_onetplus = r.R_Onet
	INNER JOIN FocusDevLibraryImport.dbo.Job r2 ON rr.B_onetplus = r2.R_Onet
WHERE
	r.JobId <> r2.JobId
