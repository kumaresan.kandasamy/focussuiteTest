﻿DELETE FROM FocusDevLibraryImport.dbo.CareerArea WHERE Name IS NULL
SET IDENTITY_INSERT FocusDevLibrary.dbo.[Library.CareerArea] ON
	DECLARE @LocalisationId bigint
	SET @LocalisationId = 12090

	INSERT INTO FocusDevLibrary.dbo.[Library.CareerArea]
	(ID,Name,LocalisationId)
	SELECT
		ca.Id,
		ca.Name,
		@LocalisationId
	FROM
		FocusDevLibraryImport.dbo.CareerArea ca
SET IDENTITY_INSERT FocusDevLibrary.dbo.[Library.CareerArea] OFF
