DECLARE @LocalisationId bigint
SET @LocalisationId = 12090

-- Before we start ensure all RONet records have a corresponding RONet Title Record

INSERT INTO FocusDevLibraryImport.dbo.JobTitle
	(r_onet, r_onettitle, CleanJobTitle, Frequency, [Rank])
select
	j.R_Onet,
	j.R_OnetTitle,
	j.R_OnetTitle,
	0,
	0
from Job j 
where j.R_Onet not in
	(select R_Onet from JobTitle)
AND j.R_Onet <> 'NA'

UPDATE
	j
SET
	j.JobId = bj.Id
FROM
	FocusDevLibraryImport.dbo.Job j
INNER JOIN FocusDevLibraryImport.dbo.bookmarks_job bj
	ON j.R_Onet = bj.R_Onet


SET IDENTITY_INSERT FocusDevLibrary.dbo.[Library.Job] ON
-- Job
INSERT INTO FocusDevLibrary.dbo.[Library.Job]
(Name,ROnet,LocalisationId, Id)
SELECT DISTINCT
	j.R_OnetTitle,
	j.R_Onet,
	@LocalisationId,
	j.JobId
FROM
	FocusDevLibraryImport.dbo.Job j
WHERE j.JobId IS NOT NULL
AND j.R_Onet <> 'NA'
SET IDENTITY_INSERT FocusDevLibrary.dbo.[Library.Job] OFF

INSERT INTO FocusDevLibrary.dbo.[Library.Job]
(Name,ROnet,LocalisationId)
SELECT DISTINCT
	j.R_OnetTitle,
	j.R_Onet,
	@LocalisationId
FROM
	FocusDevLibraryImport.dbo.Job j
WHERE j.JobId IS NULL
AND j.R_Onet <> 'NA'


UPDATE
	r
SET
	r.JobId = j.Id
FROM
	FocusDevLibraryImport.dbo.Job r
	INNER JOIN FocusDevLibrary.dbo.[Library.Job] j ON r.R_Onet = j.ROnet
WHERE r.JobId IS NULL
