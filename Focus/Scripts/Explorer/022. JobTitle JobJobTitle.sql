INSERT INTO FocusDevLibrary.dbo.[Library.JobTitle]
(Name)
SELECT DISTINCT 
	CleanJobTitle
FROM
	JobTitle

UPDATE
	rt
SET
	rt.JobTitleId = jt.Id
FROM
	FocusDevLibraryImport.dbo.JobTitle rt
	INNER JOIN FocusDevLibrary.dbo.[Library.JobTitle] jt ON rt.CleanJobTitle = jt.Name

INSERT INTO FocusDevLibrary.dbo.[Library.JobJobTitle]
(JobId,JobTitleId,DemandPercentile)
SELECT
	r.JobId,
	rt.JobTitleId,
	[Rank]
FROM
	FocusDevLibraryImport.dbo.JobTitle rt
	INNER JOIN FocusDevLibraryImport.dbo.Job r ON rt.R_onet = r.R_Onet

DECLARE @MissingTitles TABLE
(
	JobId bigint NOT NULL PRIMARY KEY,
	Name nvarchar(200) NOT NULL
)

INSERT INTO @MissingTitles
(JobId,Name)
SELECT
	j.Id,
	j.Name
FROM
	FocusDevLibrary.dbo.[Library.Job] j
WHERE
	j.Name NOT IN
	(
		SELECT
			jt.Name
		FROM
			FocusDevLibrary.dbo.[Library.JobTitle] jt
	)

INSERT INTO FocusDevLibrary.dbo.[Library.JobTitle]
(Name)
SELECT DISTINCT 
	Name
FROM
	@MissingTitles

INSERT INTO FocusDevLibrary.dbo.[Library.JobJobTitle]
(JobId,JobTitleId,DemandPercentile)
SELECT
	m.JobId,
	jt.Id,
	0
FROM
	@MissingTitles m
	INNER JOIN FocusDevLibrary.dbo.[Library.JobTitle] jt ON m.Name = jt.Name
