DELETE FROM FocusDevLibrary.dbo.[Library.EmployerJob]

INSERT INTO FocusDevLibrary.dbo.[Library.EmployerJob]
(EmployerId,JobId,Positions,StateAreaId)
SELECT DISTINCT
	e.Id,
	r.JobId,
	ej.Demand,
	g.StateAreaId
FROM
	FocusDevLibraryImport.dbo.EmployerJob ej
	INNER JOIN FocusDevLibraryImport.dbo.Employer e ON ej.CanonEmployer = e.CanonEmployer
	INNER JOIN FocusDevLibraryImport.dbo.Job r ON ej.R_Onet = r.R_Onet
	INNER JOIN FocusDevLibraryImport.dbo.[Geography] g ON ej.Area = g.Area
WHERE
	ej.AreaType = 'National'
	--AND ej.[Action] <> 'D'
	AND g.AreaType = 'National'
	
INSERT INTO FocusDevLibrary.dbo.[Library.EmployerJob]
(EmployerId,JobId,Positions,StateAreaId)
SELECT DISTINCT
	e.Id,
	r.JobId,
	ej.Demand,
	g.StateAreaId
FROM
	FocusDevLibraryImport.dbo.EmployerJob ej
	INNER JOIN FocusDevLibraryImport.dbo.Employer e ON ej.CanonEmployer = e.CanonEmployer
	INNER JOIN FocusDevLibraryImport.dbo.Job r ON ej.R_Onet = r.R_Onet
	INNER JOIN FocusDevLibraryImport.dbo.[Geography] g ON ej.Area = g.Area
WHERE
	ej.AreaType = 'State'
	--AND ej.[Action] <> 'D'
	AND g.AreaType = 'State'

INSERT INTO FocusDevLibrary.dbo.[Library.EmployerJob]
(EmployerId,JobId,Positions,StateAreaId)
SELECT DISTINCT
	e.Id,
	r.JobId,
	ej.Demand,
	g.StateAreaId
FROM
	FocusDevLibraryImport.dbo.EmployerJob ej
	INNER JOIN FocusDevLibraryImport.dbo.Employer e ON ej.CanonEmployer = e.CanonEmployer
	INNER JOIN FocusDevLibraryImport.dbo.Job r ON ej.R_Onet = r.R_Onet
	INNER JOIN FocusDevLibraryImport.dbo.[Geography] g ON ej.Area = g.Area
WHERE
	ej.AreaType = 'MSA'
	--AND ej.[Action] <> 'D'
	AND g.AreaType = 'MSA'
