DECLARE @LocalisationId bigint
DECLARE @MaxClusterId	BIGINT
DECLARE @MaxSubClusterId	BIGINT
DECLARE @NewSeedId BIGINT
SET @LocalisationId = 12090

SELECT @MaxClusterId = MAX(ISNULL(Id, 0)) FROM FocusDevLibraryImport.dbo.SkillCluster
SELECT @MaxSubClusterId = MAX(ISNULL(SubClusterId,0)) FROM FocusDevLibraryImport.dbo.SkillSubCluster

IF @MaxClusterId > @MaxSubClusterId
	SET @NewSeedId = @MaxClusterId + 1
ELSE
	SET @NewSeedId = @MaxSubClusterId + 1


SET IDENTITY_INSERT FocusDevLibrary.dbo.[Library.SkillCategory] ON

INSERT INTO FocusDevLibrary.dbo.[Library.SkillCategory]
(Id, Name, LocalisationId)
SELECT
	Id,
	Name,
	@LocalisationId
FROM
	FocusDevLibraryImport.dbo.SkillCluster
WHERE
	Id IS NOT NULL

SET IDENTITY_INSERT FocusDevLibrary.dbo.[Library.SkillCategory] OFF

DBCC CHECKIDENT ("FocusDevLibrary.dbo.[Library.SkillCategory]", RESEED, @NewSeedId);

INSERT INTO FocusDevLibrary.dbo.[Library.SkillCategory]
(Name, LocalisationId)
SELECT
	Name,
	@LocalisationId
FROM
	FocusDevLibraryImport.dbo.SkillCluster
WHERE
	Id IS NULL
	
UPDATE
	sc
SET
	sc.Id = lsc.Id
FROM
	FocusDevLibraryImport.dbo.SkillCluster sc
INNER JOIN FocusDevLibrary.dbo.[Library.SkillCategory] lsc
	on sc.Name = lsc.Name
WHERE
	sc.Id IS NULL
	
UPDATE
	sc
SET
	sc.ClusterId = lsc.Id
FROM
	FocusDevLibraryImport.dbo.SkillSubCluster sc
INNER JOIN FocusDevLibrary.dbo.[Library.SkillCategory] lsc
	on sc.Cluster = lsc.Name

SET IDENTITY_INSERT FocusDevLibrary.dbo.[Library.SkillCategory] ON

INSERT INTO FocusDevLibrary.dbo.[Library.SkillCategory]
(Id, Name, LocalisationId, ParentId)
SELECT
	SubClusterId,
	SubCluster,
	@LocalisationId,
	ClusterId
FROM
	FocusDevLibraryImport.dbo.SkillSubCluster
WHERE
	SubClusterId IS NOT NULL

SET IDENTITY_INSERT FocusDevLibrary.dbo.[Library.SkillCategory] OFF

INSERT INTO FocusDevLibrary.dbo.[Library.SkillCategory]
(Name, LocalisationId, ParentId)
SELECT
	SubCluster,
	@LocalisationId,
	ClusterId
FROM
	FocusDevLibraryImport.dbo.SkillSubCluster
WHERE
	SubClusterId IS NULL
AND SubCluster IS NOT NULL
	
UPDATE
	sc
SET
	sc.SubClusterId = lsc.Id
FROM
	FocusDevLibraryImport.dbo.SkillSubCluster sc
INNER JOIN FocusDevLibrary.dbo.[Library.SkillCategory] lsc
	on sc.SubCluster = lsc.Name
WHERE
	sc.SubClusterId IS NULL
AND lsc.ParentId IS NOT NULL

SET IDENTITY_INSERT FocusDevLibrary.dbo.[Library.Skill] ON

INSERT INTO FocusDevLibrary.dbo.[Library.Skill]
(Id, SkillType, Name, LocalisationId)
SELECT DISTINCT
	s.Id,
	CASE
			WHEN sc.SkillType = 'Specialized' THEN 1
			WHEN sc.SkillType = 'Foundation' THEN 3
			WHEN sc.SkillType = 'Software' THEN 2
		END,
	s.Name,
	12090
FROM
	FocusDevLibraryImport.dbo.Skill s
INNER JOIN FocusDevLibraryImport.dbo.SkilClusterSubClusterSkill sc
	ON s.Name = sc.SkillName
WHERE
	s.Id IS NOT NULL


SET IDENTITY_INSERT FocusDevLibrary.dbo.[Library.Skill] OFF

INSERT INTO FocusDevLibrary.dbo.[Library.Skill]
(SkillType, Name, LocalisationId)
SELECT DISTINCT
	CASE
			WHEN sc.SkillType = 'Specialized' THEN 1
			WHEN sc.SkillType = 'Foundation' THEN 3
			WHEN sc.SkillType = 'Software' THEN 2
		END,
	s.Name,
	12090
FROM
	FocusDevLibraryImport.dbo.Skill s
INNER JOIN FocusDevLibraryImport.dbo.SkilClusterSubClusterSkill sc
	ON s.Name = sc.SkillName
WHERE
	s.Id IS NULL

UPDATE
	s
SET
	s.Id = sk.Id
FROM
	FocusDevLibraryImport.dbo.Skill s
INNER JOIN FocusDevLibrary.dbo.[Library.Skill] sk
	on s.Name = sk.Name
WHERE s.Id IS NULL



UPDATE FocusDevLibraryImport.dbo.SkilClusterSubClusterSkill SET SubCluster = NULL WHERE SubCluster = ''

INSERT INTO FocusDevLibrary.dbo.[Library.SkillCategorySkill]
(SkillCategoryId,SkillId)
SELECT DISTINCT
	scc.SubClusterId,
	s.Id
FROM
	FocusDevLibraryImport.dbo.Skill s
INNER JOIN FocusDevLibraryImport.dbo.SkilClusterSubClusterSkill sc
	ON s.Name = sc.SkillName
INNER JOIN FocusDevLibraryImport.dbo.SkillSubCluster scc
	ON scc.SubCluster = sc.SubCluster
WHERE
	sc.SubCluster IS NOT NULL
	
INSERT INTO FocusDevLibrary.dbo.[Library.SkillCategorySkill]
(SkillCategoryId,SkillId)
SELECT DISTINCT
	scc.Id,
	s.Id
FROM
	FocusDevLibraryImport.dbo.Skill s
INNER JOIN FocusDevLibraryImport.dbo.SkilClusterSubClusterSkill sc
	ON s.Name = sc.SkillName
INNER JOIN FocusDevLibrary.dbo.[Library.SkillCategory] scc
	ON scc.Name = sc.Cluster
WHERE
	sc.SubCluster IS NULL
AND sc.Cluster IS NOT NULL
AND scc.ParentId IS NULL	

