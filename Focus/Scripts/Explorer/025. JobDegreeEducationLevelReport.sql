UPDATE FocusDevLibraryImport.dbo.JobDegreeReport
SET JobDegreeEducationLevelId = NULL
GO

-- set JobDegreeEducationLevelId
UPDATE
	rdr
SET
	rdr.JobDegreeEducationLevelId = jdel.Id
FROM 
	FocusDevLibraryImport.dbo.JobDegreeReport rdr
	INNER JOIN FocusDevLibraryImport.dbo.Job r ON rdr.R_Onet = r.R_Onet
	INNER JOIN 
	(
		SELECT DISTINCT
			Rcip,
			DegreeLevel,
			DegreeEducationLevelId
		FROM
			FocusDevLibraryImport.dbo.JobDegree
	) AS rd ON rdr.Rcip = rd.Rcip AND rdr.DegreeLevel = rd.DegreeLevel
	INNER JOIN FocusDevLibrary.dbo.[Library.JobDegreeEducationLevel] jdel ON r.JobId = jdel.JobId AND rd.DegreeEducationLevelId = jdel.DegreeEducationLevelId
WHERE rdr.Client != 'Wisconsin'

	-- set JobDegreeEducationLevelId for wisconsin
UPDATE
	rdr
SET
	rdr.JobDegreeEducationLevelId = jdel.Id
FROM 
	FocusDevLibraryImport.dbo.JobDegreeReport rdr
	INNER JOIN FocusDevLibraryImport.dbo.Job r ON rdr.R_Onet = r.R_Onet
	INNER JOIN 
	(
		SELECT DISTINCT
			Rcip,
			DegreeLevel,
			DegreeEducationLevelId,
			Client
		FROM
			FocusDevLibraryImport.dbo.JobDegree
	) AS rd ON rdr.Rcip = rd.Rcip AND rdr.DegreeLevel = rd.DegreeLevel AND rd.Client = 'Wisconsin'
	INNER JOIN FocusDevLibrary.dbo.[Library.JobDegreeEducationLevel] jdel ON r.JobId = jdel.JobId AND rd.DegreeEducationLevelId = jdel.DegreeEducationLevelId 
	WHERE rdr.Client = 'Wisconsin'
	GO
-- National
INSERT INTO FocusDevLibrary.dbo.[Library.JobDegreeEducationLevelReport]
(StateAreaId,JobDegreeEducationLevelId,HiringDemand,DegreesAwarded)
SELECT DISTINCT
	g.StateAreaId,
	rdr.JobDegreeEducationLevelId,
	ISNULL(rdr.Demand,0),
	0
FROM
	FocusDevLibraryImport.dbo.JobDegreeReport rdr
	INNER JOIN FocusDevLibraryImport.dbo.[Geography] g ON rdr.Area = g.Area
WHERE
	rdr.Area_Type = 'National'
	AND g.AreaType = 'National'
	AND rdr.JobDegreeEducationLevelId IS NOT NULL

-- State
INSERT INTO FocusDevLibrary.dbo.[Library.JobDegreeEducationLevelReport]
(StateAreaId,JobDegreeEducationLevelId,HiringDemand,DegreesAwarded)
SELECT DISTINCT
	g.StateAreaId,
	rdr.JobDegreeEducationLevelId,
	ISNULL(rdr.Demand,0),
	0
FROM
	FocusDevLibraryImport.dbo.JobDegreeReport rdr
	INNER JOIN FocusDevLibraryImport.dbo.[Geography] g ON rdr.Area = g.Area
WHERE
	rdr.Area_Type = 'State'
	AND g.AreaType = 'State'
	AND rdr.JobDegreeEducationLevelId IS NOT NULL

-- MSA
INSERT INTO FocusDevLibrary.dbo.[Library.JobDegreeEducationLevelReport]
(StateAreaId,JobDegreeEducationLevelId,HiringDemand,DegreesAwarded)
SELECT DISTINCT
	g.StateAreaId,
	rdr.JobDegreeEducationLevelId,
	ISNULL(rdr.Demand,0),
	0
FROM
	FocusDevLibraryImport.dbo.JobDegreeReport rdr
	INNER JOIN FocusDevLibraryImport.dbo.[Geography] g ON rdr.Area = g.Area
WHERE
	rdr.Area_Type = 'MSA'
	AND g.AreaType = 'MSA'
	AND rdr.JobDegreeEducationLevelId IS NOT NULL
