UPDATE
	rd
SET
	rd.DegreeEducationLevelId = d.DegreeEducationLevelId
FROM
	FocusDevLibraryImport.dbo.DegreeExtended rd
	INNER JOIN FocusDevLibraryImport.dbo.JobDegree d ON rd.Rcip = d.RCIP and rd.DegreeLevel = d.DegreeLevel
	
INSERT INTO FocusDevLibrary.dbo.[Library.DegreeEducationLevelExt]
(Name, Url, [Description], DegreeEducationLevelId)
SELECT RCIP_Title, Url, ISNULL([Description], ''), DegreeEducationLevelId
FROM FocusDevLibraryImport.dbo.DegreeExtended