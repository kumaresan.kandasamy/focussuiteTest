DELETE FROM FocusDevLibraryImport.dbo.JobCertification
WHERE R_Onet IS NULL
GO

DELETE FROM FocusDevLibrary.dbo.[Library.JobCertification]
DELETE FROM FocusDevLibrary.dbo.[Library.Certification]
GO
-- Certification
INSERT INTO FocusDevLibrary.dbo.[Library.Certification]
(Name)
SELECT DISTINCT
	CanonCertification
FROM
	FocusDevLibraryImport.dbo.JobCertification

UPDATE
	rc
SET
	rc.CertificationId = c.Id
FROM
	FocusDevLibraryImport.dbo.JobCertification rc
	INNER JOIN FocusDevLibrary.dbo.[Library.Certification] c ON rc.CanonCertification = c.Name

-- JobCertification
INSERT INTO FocusDevLibrary.dbo.[Library.JobCertification]
(JobId,CertificationId,DemandPercentile)
SELECT
	r.JobId,
	rc.CertificationId,
	ROUND((ISNULL(rc.ValidCertRatio,0) * 100),4)
FROM
	FocusDevLibraryImport.dbo.JobCertification rc
	INNER JOIN FocusDevLibraryImport.dbo.Job r ON rc.R_onet = r.R_Onet