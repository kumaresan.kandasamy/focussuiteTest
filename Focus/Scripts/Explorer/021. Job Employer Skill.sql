TRUNCATE TABLE FocusDevLibrary.dbo.[Library.JobEmployerSkill]
INSERT INTO FocusDevLibrary.dbo.[Library.JobEmployerSkill]
(
	JobId,
	EmployerId,
	SkillId,
	SkillCount,
	[Rank]
)
	
select DISTINCT
j.Id,
e.Id,
s.Id,
jess.SkillCount_Employer,
jess.[Rank]
from
	FocusDevLibraryImport.dbo.JobEmployerSkills jess
inner join FocusDevLibrary.dbo.[Library.Job] j
	on jess.R_Onet = j.ROnet
inner join FocusDevLibrary.dbo.[Library.Employer] e
	on jess.CanonEmployer = e.Name
inner join FocusDevLibrary.dbo.[Library.skill] s
	on jess.Skillname = s.Name
