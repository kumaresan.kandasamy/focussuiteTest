 DELETE FROM	FocusDevLibraryImport.dbo.JobDegreeLevel  WHERE Diploma_CertificateRatio = 0 AND BachelorRatio = 0 AND GraduateRatio = 0 AND AssociateRatio = 0

DELETE FROM FocusDevLibrary.dbo.[Library.JobEducationRequirement]
INSERT INTO FocusDevLibrary.dbo.[Library.JobEducationRequirement]
(JobId,EducationRequirementType,RequirementPercentile)
SELECT
	r.JobId,
	1,
	ROUND((rdl.Diploma_CertificateRatio * 100),0)
FROM
	FocusDevLibraryImport.dbo.JobDegreeLevel rdl
	INNER JOIN FocusDevLibraryImport.dbo.Job r ON rdl.r_onet = r.R_Onet
WHERE
	rdl.Diploma_CertificateRatio IS NOT NULL

INSERT INTO FocusDevLibrary.dbo.[Library.JobEducationRequirement]
(JobId,EducationRequirementType,RequirementPercentile)
SELECT
	r.JobId,
	2,
	ROUND((rdl.AssociateRatio * 100),0)
FROM
	FocusDevLibraryImport.dbo.JobDegreeLevel rdl
	INNER JOIN FocusDevLibraryImport.dbo.Job r ON rdl.r_onet = r.R_Onet
WHERE
	rdl.AssociateRatio IS NOT NULL

INSERT INTO FocusDevLibrary.dbo.[Library.JobEducationRequirement]
(JobId,EducationRequirementType,RequirementPercentile)
SELECT
	r.JobId,
	3,
	ROUND((rdl.BachelorRatio * 100),0)
FROM
	FocusDevLibraryImport.dbo.JobDegreeLevel rdl
	INNER JOIN FocusDevLibraryImport.dbo.Job r ON rdl.r_onet = r.R_Onet
WHERE
	rdl.BachelorRatio IS NOT NULL

INSERT INTO FocusDevLibrary.dbo.[Library.JobEducationRequirement]
(JobId,EducationRequirementType,RequirementPercentile)
SELECT
	r.JobId,
	4,
	ROUND((rdl.GraduateRatio * 100),0)
FROM
	FocusDevLibraryImport.dbo.JobDegreeLevel rdl
	INNER JOIN FocusDevLibraryImport.dbo.Job r ON rdl.r_onet = r.R_Onet
WHERE
	rdl.GraduateRatio IS NOT NULL
