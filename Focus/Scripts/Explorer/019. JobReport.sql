DECLARE @LocalisationId bigint
SET @LocalisationId = 12090	

INSERT INTO FocusDevLibrary.dbo.[Library.JobReport]
(
	JobId,StateAreaId,HiringTrend,HiringTrendPercentile,HiringDemand,GrowthTrend,GrowthPercentile,
	SalaryTrend,SalaryTrendPercentile,SalaryTrendAverage,SalaryTrendMin,SalaryTrendMax,
	SalaryTrendRealtime,SalaryTrendRealtimeAverage,SalaryTrendRealtimeMin,SalaryTrendRealtimeMax,
	LocalisationId
)
SELECT
	r.JobId,
	g.StateAreaId,
	jr.HiringTrend,
	ISNULL(jr.HiringTrendPercentile,0),
	ISNULL(jr.Hiring_Demand,0),
	'',
	0,
	ISNULL(jr.SalaryTrend_Employed, 'NA'), 
	0,
	ISNULL(jr.AverageSalary_Employed,0),
	ISNULL(jr.LowerSalaryRange_Employed,0),
	ISNULL(jr.UpperSalaryRange_Employed,0),
	jr.SalaryTrend_RealTime,
	jr.AverageSalary_RealTime,
	jr.LowerSalaryRange_RealTime,
	jr.UpperSalaryRange_RealTime,
	@LocalisationId
FROM
	FocusDevLibraryImport.dbo.JobReport jr
	INNER JOIN FocusDevLibraryImport.dbo.Job r ON jr.R_Onet = r.R_Onet
	INNER JOIN FocusDevLibraryImport.dbo.[Geography] g ON jr.Area = g.Area
WHERE
	jr.Area_Type = 'National'
	AND g.AreaType = 'National'

-- State
INSERT INTO FocusDevLibrary.dbo.[Library.JobReport]
(
	JobId,StateAreaId,HiringTrend,HiringTrendPercentile,HiringDemand,GrowthTrend,GrowthPercentile,
	SalaryTrend,SalaryTrendPercentile,SalaryTrendAverage,SalaryTrendMin,SalaryTrendMax,
	SalaryTrendRealtime,SalaryTrendRealtimeAverage,SalaryTrendRealtimeMin,SalaryTrendRealtimeMax,
	LocalisationId
)
SELECT
	r.JobId,
	g.StateAreaId,
	jr.HiringTrend,
	ISNULL(jr.HiringTrendPercentile,0),
	ISNULL(jr.Hiring_Demand,0),
	'',
	0,
	ISNULL(jr.SalaryTrend_Employed, 'NA'),
	0,
	ISNULL(jr.AverageSalary_Employed,0),
	ISNULL(jr.LowerSalaryRange_Employed,0),
	ISNULL(jr.UpperSalaryRange_Employed,0),
	jr.SalaryTrend_RealTime,
	jr.AverageSalary_RealTime,
	jr.LowerSalaryRange_RealTime,
	jr.UpperSalaryRange_RealTime,
	@LocalisationId
FROM
	FocusDevLibraryImport.dbo.JobReport jr
	INNER JOIN FocusDevLibraryImport.dbo.Job r ON jr.R_Onet = r.R_Onet
	INNER JOIN FocusDevLibraryImport.dbo.[Geography] g ON jr.Area = g.Area
WHERE
	jr.Area_Type = 'State'
	AND g.AreaType = 'State'

-- MSA
INSERT INTO FocusDevLibrary.dbo.[Library.JobReport]
(
	JobId,StateAreaId,HiringTrend,HiringTrendPercentile,HiringDemand,GrowthTrend,GrowthPercentile,
	SalaryTrend,SalaryTrendPercentile,SalaryTrendAverage,SalaryTrendMin,SalaryTrendMax,
	SalaryTrendRealtime,SalaryTrendRealtimeAverage,SalaryTrendRealtimeMin,SalaryTrendRealtimeMax,
	LocalisationId
)
SELECT
	r.JobId,
	g.StateAreaId,
	jr.HiringTrend,
	ISNULL(jr.HiringTrendPercentile,0),
	ISNULL(jr.Hiring_Demand,0),
	'',
	0,
	ISNULL(jr.SalaryTrend_Employed, 'NA'),
	0,
	ISNULL(jr.AverageSalary_Employed,0),
	ISNULL(jr.LowerSalaryRange_Employed,0),
	ISNULL(jr.UpperSalaryRange_Employed,0),
	jr.SalaryTrend_RealTime,
	jr.AverageSalary_RealTime,
	jr.LowerSalaryRange_RealTime,
	jr.UpperSalaryRange_RealTime,
	@LocalisationId
FROM
	FocusDevLibraryImport.dbo.JobReport jr
	INNER JOIN FocusDevLibraryImport.dbo.Job r ON jr.R_Onet = r.R_Onet
	INNER JOIN FocusDevLibraryImport.dbo.[Geography] g ON jr.Area = g.Area
WHERE
	jr.Area_Type = 'MSA'
	AND g.AreaType = 'MSA'
