DELETE FROM FocusDevLibraryImport.dbo.MajorCodes
WHERE RCIP IS NULL
GO

UPDATE FocusDevLibraryImport.dbo.JobDegree
SET DegreeId = NULL, DegreeEducationLevelId = NULL
GO
DELETE FROM FocusDevLibrary.dbo.[Library.JobDegreeEducationLevelReport]
DELETE FROM FocusDevLibrary.dbo.[Library.StudyPlaceDegreeEducationLevel]
DELETE FROM FocusDevLibrary.dbo.[Library.JobDegreeEducationLevel]
DELETE FROM FocusDevLibrary.dbo.[Library.DegreeEducationLevelExt]
DELETE FROM FocusDevLibrary.dbo.[Library.DegreeEducationLevel]

DELETE FROM FocusDevLibrary.dbo.[Library.DegreeAlias]

DELETE FROM FocusDevLibrary.dbo.[Library.ProgramAreaDegree]
DELETE FROM FocusDevLibrary.dbo.[Library.Degree]

-- Degree
INSERT INTO FocusDevLibrary.dbo.[Library.Degree]
(Name,RCIPCode,IsClientData,ClientDataTag)
SELECT DISTINCT
	RcipTitle,
	Rcip,
	ClientData,
	Client
FROM
	FocusDevLibraryImport.dbo.MajorCodes

UPDATE
	rd
SET
	rd.DegreeId = d.Id
FROM
	FocusDevLibraryImport.dbo.JobDegree rd
	INNER JOIN FocusDevLibrary.dbo.[Library.Degree] d ON rd.Rcip = d.RCIPCode


		UPDATE
	rd
SET
	rd.DegreeId = d.Id
FROM
	FocusDevLibraryImport.dbo.JobDegree rd
	INNER JOIN FocusDevLibrary.dbo.[Library.Degree] d ON rd.Rcip = d.RCIPCode AND d.ClientDataTag = 'Wisconsin' AND rd.Client = 'Wisconsin'
	WHERE rd.Client = 'Wisconsin'

UPDATE
	FocusDevLibrary.dbo.[Library.Degree]
SET
	IsDegreeArea = 1
WHERE
	RCIPCode IN (SELECT DISTINCT Rcip FROM FocusDevLibraryImport.dbo.JobDegree WHERE DegreeArea = '1')
	
SET IDENTITY_INSERT FocusDevLibrary.dbo.[Library.DegreeEducationLevel] ON
INSERT INTO FocusDevLibrary.dbo.[Library.DegreeEducationLevel]
(Id,DegreeId,EducationLevel,DegreesAwarded,Name)
SELECT DISTINCT	
	jdi.Id,
	rd.DegreeId,
	rd.DegreeLevel,
	0,
	CASE
		WHEN rd.OverrideDegreeEducationLabel IS NOT NULL THEN rd.OverrideDegreeEducationLabel		
		WHEN rd.DegreeLevel = 13 THEN 'Certificate'+ ' - ' + d.Name
		WHEN rd.DegreeLevel = 14 THEN 'Associate''s Degree' + ' - ' + d.Name
		WHEN rd.DegreeLevel = 16 THEN 'Bachelor''s Degree' + ' - ' + d.Name
		WHEN rd.DegreeLevel = 17 THEN 'Post-Baccalaureate Certificate' + ' - ' + d.Name
		WHEN rd.DegreeLevel = 18 THEN 'Graduate/Professional Degree' + ' - ' + d.Name
	END
FROM
	FocusDevLibraryImport.dbo.JobDegree rd
	INNER JOIN FocusDevLibrary.dbo.[Library.Degree] d ON rd.Rcip = d.RCIPCode
	INNER JOIN FocusDevLibraryImport.dbo.Bookmarks_DegreeEducationLevel jdi on rd.Rcip = jdi.Rcip and rd.DegreeLevel = jdi.Education_Level
	where jdi.Id is not null AND (rd.Client !='Wisconsin' OR rd.Client IS NULL)

SET IDENTITY_INSERT FocusDevLibrary.dbo.[Library.DegreeEducationLevel] OFF

INSERT INTO FocusDevLibrary.dbo.[Library.DegreeEducationLevel]
(DegreeId,EducationLevel,DegreesAwarded,Name)
SELECT DISTINCT	
	rd.DegreeId,
	rd.DegreeLevel,
	0,
	CASE
		WHEN rd.OverrideDegreeEducationLabel IS NOT NULL THEN rd.OverrideDegreeEducationLabel
		WHEN rd.DegreeLevel = 13 AND d.ClientDataTag = 'Wisconsin' THEN 'Diploma'+ ' - ' + d.Name
				WHEN rd.DegreeLevel = 13 THEN 'Certificate'+ ' - ' + d.Name
		WHEN rd.DegreeLevel = 14 THEN 'Associate''s Degree' + ' - ' + d.Name
		WHEN rd.DegreeLevel = 16 THEN 'Bachelor''s Degree' + ' - ' + d.Name
		WHEN rd.DegreeLevel = 17 THEN 'Post-Baccalaureate Certificate' + ' - ' + d.Name
		WHEN rd.DegreeLevel = 18 THEN 'Graduate/Professional Degree' + ' - ' + d.Name
	END
FROM
	FocusDevLibraryImport.dbo.JobDegree rd
	INNER JOIN FocusDevLibrary.dbo.[Library.Degree] d ON rd.Rcip = d.RCIPCode
	left JOIN FocusDevLibraryImport.dbo.Bookmarks_DegreeEducationLevel jdi on rd.Rcip = jdi.Rcip and rd.DegreeLevel = jdi.Education_Level
	where jdi.Id is null
	

	INSERT INTO FocusDevLibrary.dbo.[Library.DegreeEducationLevel]
(DegreeId,EducationLevel,DegreesAwarded,Name)
SELECT DISTINCT	
	rd.DegreeId,
	rd.DegreeLevel,
	0,
	CASE
		WHEN rd.OverrideDegreeEducationLabel IS NOT NULL THEN rd.OverrideDegreeEducationLabel		
		WHEN rd.DegreeLevel = 13 AND d.ClientDataTag = 'Wisconsin' THEN 'Diploma'+ ' - ' + d.Name
		WHEN rd.DegreeLevel = 14 THEN 'Associate''s Degree' + ' - ' + d.Name
		WHEN rd.DegreeLevel = 16 THEN 'Bachelor''s Degree' + ' - ' + d.Name
		WHEN rd.DegreeLevel = 17 THEN 'Post-Baccalaureate Certificate' + ' - ' + d.Name
		WHEN rd.DegreeLevel = 18 THEN 'Graduate/Professional Degree' + ' - ' + d.Name
	END
FROM
	FocusDevLibraryImport.dbo.JobDegree rd
	INNER JOIN FocusDevLibrary.dbo.[Library.Degree] d ON rd.Rcip = d.RCIPCode
	INNER JOIN FocusDevLibraryImport.dbo.Bookmarks_DegreeEducationLevel jdi on rd.Rcip = jdi.Rcip and rd.DegreeLevel = jdi.Education_Level
	where jdi.Id is not null AND d.ClientDataTag ='Wisconsin' AND rd.Client ='Wisconsin'


UPDATE
	rd
SET
	rd.DegreeEducationLevelId = del.Id
FROM
	FocusDevLibraryImport.dbo.JobDegree rd
	INNER JOIN FocusDevLibrary.dbo.[Library.DegreeEducationLevel] del ON rd.DegreeId = del.DegreeId AND rd.DegreeLevel = del.EducationLevel

UPDATE
	del
SET
	del.ExcludeFromReport = 1
FROM
	FocusDevLibrary.dbo.[Library.Degree] d
	INNER JOIN FocusDevLibrary.dbo.[Library.DegreeEducationLevel] del ON d.Id = del.DegreeId
WHERE
	d.RCIPCode IN ('23.0100','54.0100','45.1000','42.0100')
	AND del.EducationLevel = 16


update d
set ExcludeFromReport = 1
FROM FocusDevLibrary.dbo.[Library.DegreeEducationLevel] d
INNER JOIN FocusDevLibraryImport.dbo.JobDegree r on r.DegreeEducationLevelId = d.Id where r.BlockfromDegreeReport = 1


-- JobDegreeEducationLevel
INSERT INTO FocusDevLibrary.dbo.[Library.JobDegreeEducationLevel]
(JobId,DegreeEducationLevelId,ExcludeFromJob)
SELECT DISTINCT
	r.JobId,
	rd.DegreeEducationLevelId,
	CASE WHEN ISNULL(rd.BlockDegreeforJobLightBox,'0') = '1' THEN 1 ELSE 0 END
FROM
	FocusDevLibraryImport.dbo.JobDegree rd
	INNER JOIN FocusDevLibraryImport.dbo.Job r ON rd.R_Onet = r.R_Onet
WHERE
	rd.DegreeEducationLevelId IS NOT NULL
	
-- Update tiers
update jdel
set jdel.Tier = rd.Tier
from 
FocusDevLibrary.dbo.[Library.JobDegreeEducationLevel] jdel
INNER JOIN FocusDevLibrary.dbo.[Library.Job] j on j.Id = jdel.JobId
INNER JOIN FocusDevLibraryImport.dbo.JobDegree rd ON rd.R_Onet = j.ROnet and jdel.DegreeEducationLevelID = rd.DegreeEducationLevelID
