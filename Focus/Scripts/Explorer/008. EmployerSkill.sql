TRUNCATE TABLE FocusDevLibrary.dbo.[Library.EmployerSkill]

UPDATE FocusDevLibraryImport.dbo.EmployerSkill
SET EmployerId = NULL, SkillId = NULL
GO

UPDATE
	es
SET
	es.EmployerId = e.Id
FROM
	FocusDevLibraryImport.dbo.EmployerSkill es
	INNER JOIN FocusDevLibraryImport.dbo.Employer e ON es.CanonEmployer = e.CanonEmployer

UPDATE
	es
SET
	es.SkillId = s.Id
FROM
	FocusDevLibraryImport.dbo.EmployerSkill es
	INNER JOIN FocusDevLibraryImport.dbo.Skill s ON es.SkillName = s.Name

INSERT INTO FocusDevLibrary.dbo.[Library.EmployerSkill]
(EmployerId,SkillId,Postings)
SELECT DISTINCT
	EmployerId,
	SkillId,
	SkillCount
FROM
	FocusDevLibraryImport.dbo.EmployerSkill
WHERE
	EmployerId IS NOT NULL
	AND SkillId IS NOT NULL
