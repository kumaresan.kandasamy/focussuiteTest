UPDATE FocusDevLibrary.dbo.[Library.Job]
SET DegreeIntroStatement = NULL
GO

-- set Job Degree Intro Statement column
UPDATE
	j
SET
	j.DegreeIntroStatement = rdi.[IntroStatement]
FROM
	FocusDevLibrary.dbo.[Library.Job] j
	INNER JOIN FocusDevLibraryImport.dbo.Job r ON j.Id = r.JobId
	INNER JOIN FocusDevLibraryImport.dbo.JobDegreeIntroStatement rdi ON r.R_Onet = rdi.R_Onet
