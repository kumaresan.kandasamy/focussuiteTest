DECLARE @LocalisationID BIGINT
DECLARE @BatchSize INT
DECLARE @TotalRows INT
DECLARE @FirstRow INT, @LastRow INT

SET @BatchSize = 100000

SELECT @LocalisationID = Id FROM FocusStage03XXWF.dbo.[Config.Localisation] WHERE Culture = '**-**'

-- Add primary key to JobDegreeEducationLevelReport table to improve performance of query
IF NOT EXISTS (SELECT 1 FROM FocusExplorer.INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_TYPE = 'PRIMARY KEY' AND TABLE_SCHEMA = 'dev' AND TABLE_NAME  = 'JobDegreeEducationLevelReport')
BEGIN
    ALTER TABLE FocusExplorer.dev.JobDegreeEducationLevelReport ADD CONSTRAINT PK_JobDegreeEducationLevelReport PRIMARY KEY CLUSTERED (Id ASC)
END

--BEGIN TRANSACTION
RAISERROR('Deleting Tables', 0, 1) WITH NOWAIT

DELETE FROM FocusStage03XXWF.dbo.[Library.JobCertification]
DELETE FROM FocusStage03XXWF.dbo.[Library.Certification]
DELETE FROM FocusStage03XXWF.dbo.[Library.DegreeAlias]
DELETE FROM FocusStage03XXWF.dbo.[Library.DegreeEducationLevelExt]
DELETE FROM FocusStage03XXWF.dbo.[Library.ProgramAreaDegree]
DELETE FROM FocusStage03XXWF.dbo.[Library.ProgramArea]
DELETE FROM FocusStage03XXWF.dbo.[Library.StudyPlaceDegreeEducationLevel]
DELETE FROM FocusStage03XXWF.dbo.[Library.StudyPlace]

-- Employer
RAISERROR('Deleting Employer Tables', 0, 1) WITH NOWAIT

DELETE FROM FocusStage03XXWF.dbo.[Library.EmployerJob]
DELETE FROM FocusStage03XXWF.dbo.[Library.EmployerSkill]

-- Skills
RAISERROR('Deleting Skill Tables', 0, 1) WITH NOWAIT

DELETE FROM FocusStage03XXWF.dbo.[Library.SkillCategorySkill]
DELETE FROM FocusStage03XXWF.dbo.[Library.SkillReport]
DELETE FROM FocusStage03XXWF.dbo.[Library.SkillCategory]

-- Internships
RAISERROR('Deleting Internship Tables', 0, 1) WITH NOWAIT

DELETE FROM FocusStage03XXWF.dbo.[Library.InternshipEmployer]
DELETE FROM FocusStage03XXWF.dbo.[Library.InternshipJobTitle]
DELETE FROM FocusStage03XXWF.dbo.[Library.InternshipSkill]
DELETE FROM FocusStage03XXWF.dbo.[Library.InternshipReport]
DELETE FROM FocusStage03XXWF.dbo.[Library.InternshipCategory]

-- Job
RAISERROR('Deleting Job Tables', 0, 1) WITH NOWAIT

DELETE FROM FocusStage03XXWF.dbo.[Library.JobReport]
DELETE FROM FocusStage03XXWF.dbo.[Library.JobJobTitle]
DELETE FROM FocusStage03XXWF.dbo.[Library.JobTitle]
DELETE FROM FocusStage03XXWF.dbo.[Library.JobRelatedJob]
DELETE FROM FocusStage03XXWF.dbo.[Library.JobCareerArea]
DELETE FROM FocusStage03XXWF.dbo.[Library.JobCareerPathFrom]
DELETE FROM FocusStage03XXWF.dbo.[Library.JobCareerPathTo]
DELETE FROM FocusStage03XXWF.dbo.[Library.JobDegreeEducationLevelReport]
DELETE FROM FocusStage03XXWF.dbo.[Library.JobDegreeEducationLevel]
DELETE FROM FocusStage03XXWF.dbo.[Library.JobDegreeLevel]
DELETE FROM FocusStage03XXWF.dbo.[Library.JobEducationRequirement]
DELETE FROM FocusStage03XXWF.dbo.[Library.JobEmployerSkill]
DELETE FROM FocusStage03XXWF.dbo.[Library.JobExperienceLevel]
DELETE FROM FocusStage03XXWF.dbo.[Library.JobSkill]
DELETE FROM FocusStage03XXWF.dbo.[Library.CareerArea]

DELETE FROM FocusStage03XXWF.dbo.[Library.Skill]
DELETE FROM FocusStage03XXWF.dbo.[Library.Employer]
DELETE FROM FocusStage03XXWF.dbo.[Library.DegreeEducationLevel]
DELETE FROM FocusStage03XXWF.dbo.[Library.Degree]
DELETE FROM FocusStage03XXWF.dbo.[Library.Job]

-- Geography
RAISERROR('Deleting Geography Tables', 0, 1) WITH NOWAIT

DELETE FROM FocusStage03XXWF.dbo.[Library.StateArea]
DELETE FROM FocusStage03XXWF.dbo.[Library.State]
DELETE FROM FocusStage03XXWF.dbo.[Library.Area]

RAISERROR('Populating [Library.Area]', 0, 1) WITH NOWAIT

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.Area] ON

INSERT INTO [FocusStage03XXWF].[dbo].[Library.Area]
(
	[ID]
    ,[Name]
    ,[SortOrder]
)
SELECT [Id]
      ,[Name]
      ,[SortOrder]
FROM [FocusExplorer].[dev].[Area]

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.Area] OFF

RAISERROR('Populating [Library.State]', 0, 1) WITH NOWAIT

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.State] ON

INSERT INTO [FocusStage03XXWF].[dbo].[Library.State]
           ([Id]
           ,[CountryId]
           ,[Name]
           ,[Code]
           ,[SortOrder])
SELECT [Id]
      ,[CountryId]
      ,[Name]
      ,[Code]
      ,[SortOrder]
  FROM [FocusExplorer].[dev].[State]

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.State] OFF

RAISERROR('Populating [Library.StateArea]', 0, 1) WITH NOWAIT

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.StateArea] ON

INSERT INTO [FocusStage03XXWF].[dbo].[Library.StateArea]
           ([Id]
           ,[AreaCode]
           ,[Display]
           ,[IsDefault]
           ,[StateId]
           ,[AreaId])
SELECT [Id]
      ,[AreaCode]
      ,[Display]
      ,[IsDefault]
      ,[StateId]
      ,[AreaId]
  FROM [FocusExplorer].[dev].[StateArea]
  
SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.StateArea] OFF

RAISERROR('Populating [Library.Job]', 0, 1) WITH NOWAIT

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.Job] ON

INSERT INTO [FocusStage03XXWF].[dbo].[Library.Job]
           ([Id]
           ,[Name]
           ,[ROnet]
           ,[LocalisationId]
           ,[DegreeIntroStatement]
           ,[Description]
           ,[StarterJob])
 SELECT [Id]
      ,[Name]
      ,[ROnet]
      ,[LocalisationId]
      ,[DegreeIntroStatement]
      ,[Description]
      ,CASE ISNULL(LEFT(StarterJob, 1), '0') WHEN '0' THEN NULL ELSE 1 END AS [StarterJob]
  FROM [FocusExplorer].[dev].[Job]

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.Job] OFF

RAISERROR('Populating [Library.Degree]', 0, 1) WITH NOWAIT

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.Degree] ON

INSERT INTO [FocusStage03XXWF].[dbo].[Library.Degree]
           ([Id]
           ,[Name]
           ,[RcipCode]
           ,[IsDegreeArea]
           ,[IsClientData]
           ,[ClientDataTag]
           ,[Tier])
SELECT [Id]
      ,[Name]
      ,[RcipCode]
      ,[IsDegreeArea]
      ,[IsClientData]
      ,[ClientDataTag]
      ,[Tier]
  FROM [FocusExplorer].[dev].[Degree]

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.Degree] OFF

RAISERROR('Populating [Library.DegreeEducationLevel]', 0, 1) WITH NOWAIT

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.DegreeEducationLevel] ON

INSERT INTO [FocusStage03XXWF].[dbo].[Library.DegreeEducationLevel]
           ([Id]
           ,[EducationLevel]
           ,[DegreesAwarded]
           ,[Name]
           ,[ExcludeFromReport]
           ,[DegreeId])
SELECT [Id]
      ,[EducationLevel]
      ,[DegreesAwarded]
      ,[Name]
      ,[ExcludeFromReport]
      ,[DegreeId]
  FROM [FocusExplorer].[dev].[DegreeEducationLevel]

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.DegreeEducationLevel] OFF

RAISERROR('Populating [Library.Employer]', 0, 1) WITH NOWAIT

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.Employer] ON

INSERT INTO [FocusStage03XXWF].[dbo].[Library.Employer]
           ([Id]
           ,[Name])
SELECT [Id]
      ,[Name]
  FROM [FocusExplorer].[dev].[Employer]

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.Employer] OFF

RAISERROR('Populating [Library.Skill]', 0, 1) WITH NOWAIT

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.Skill] ON

INSERT INTO [FocusStage03XXWF].[dbo].[Library.Skill]
           ([Id]
           ,[SkillType]
           ,[Name]
           ,[LocalisationId])
SELECT [Id]
      ,[SkillType]
      ,[Name]
      ,[LocalisationId]
  FROM [FocusExplorer].[dev].[Skill]

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.Skill] OFF

RAISERROR('Populating [Library.CareerArea]', 0, 1) WITH NOWAIT

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.CareerArea] ON

INSERT INTO [FocusStage03XXWF].[dbo].[Library.CareerArea]
           ([Id]
           ,[Name]
           ,[LocalisationId])
SELECT [Id]
      ,[Name]
      ,@LocalisationId
  FROM [FocusExplorer].[dev].[CareerArea]

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.CareerArea] OFF

RAISERROR('Populating [Library.JobSkill]', 0, 1) WITH NOWAIT

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.JobSkill] ON

INSERT INTO [FocusStage03XXWF].[dbo].[Library.JobSkill]
           ([Id]
           ,[JobSkillType]
           ,[DemandPercentile]
           ,[Rank]
           ,[JobId]
           ,[SkillId])
SELECT ROW_NUMBER() OVER(ORDER BY JobID, SkillID)
      ,[JobSkillType]
      ,[DemandPercentile]
      ,[Rank]
      ,[JobId]
      ,[SkillId]
  FROM [FocusExplorer].[dev].[JobSkill]
  
SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.JobSkill] OFF

RAISERROR('Populating [Library.JobExperienceLevel]', 0, 1) WITH NOWAIT

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.JobExperienceLevel] ON

INSERT INTO [FocusStage03XXWF].[dbo].[Library.JobExperienceLevel]
           ([Id]
           ,[ExperienceLevel]
           ,[ExperiencePercentile]
           ,[JobId])
SELECT ROW_NUMBER() OVER(ORDER BY JobID, ExperienceLevel)
      ,[ExperienceLevel]
      ,[ExperiencePercentile]
      ,[JobId]
  FROM [FocusExplorer].[dev].[JobExperienceLevel]

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.JobExperienceLevel] OFF

RAISERROR('Populating [Library.JobEmployerSkill]', 0, 1) WITH NOWAIT

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.JobEmployerSkill] ON

SELECT @TotalRows = COUNT(1) FROM [FocusExplorer].[dev].[JobEmployerSkill]
SET @FirstRow = 1

WHILE @FirstRow < @TotalRows
BEGIN
	SET @LastRow = @FirstRow + @BatchSize - 1

	RAISERROR('Row %d', 0, 1, @FirstRow) WITH NOWAIT
	
	;WITH JobEmployerSkills AS
	(
		SELECT ROW_NUMBER() OVER(ORDER BY JobId, EmployerId, SkillId) AS Id
		  ,[SkillCount]
		  ,[Rank]
		  ,[JobId]
		  ,[EmployerId]
		  ,[SkillId]
		FROM [FocusExplorer].[dev].[JobEmployerSkill]
	)
	INSERT INTO [FocusStage03XXWF].[dbo].[Library.JobEmployerSkill]
           ([Id]
           ,[SkillCount]
           ,[Rank]
           ,[JobId]
           ,[EmployerId]
           ,[SkillId])
	SELECT Id
      ,[SkillCount]
      ,[Rank]
      ,[JobId]
      ,[EmployerId]
      ,[SkillId]
	FROM JobEmployerSkills
	WHERE Id BETWEEN @FirstRow AND @LastRow

	SET @FirstRow = @FirstRow + @BatchSize
END
  
SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.JobEmployerSkill] OFF

RAISERROR('Populating [Library.JobEducationRequirement]', 0, 1) WITH NOWAIT

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.JobEducationRequirement] ON

INSERT INTO [FocusStage03XXWF].[dbo].[Library.JobEducationRequirement]
           ([Id]
           ,[EducationRequirementType]
           ,[RequirementPercentile]
           ,[JobId])
SELECT ROW_NUMBER() OVER(ORDER BY JobId, EducationRequirementType)
      ,[EducationRequirementType]
      ,[RequirementPercentile]
      ,[JobId]
  FROM [FocusExplorer].[dev].[JobEducationRequirement]

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.JobEducationRequirement] OFF

RAISERROR('Populating [Library.JobDegreeLevel]', 0, 1) WITH NOWAIT

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.JobDegreeLevel] ON

INSERT INTO [FocusStage03XXWF].[dbo].[Library.JobDegreeLevel]
           ([Id]
           ,[DegreeLevel]
           ,[JobId])
SELECT ROW_NUMBER() OVER(ORDER BY JobId, DegreeLevel)
      ,[DegreeLevel]
      ,[JobId]
  FROM [FocusExplorer].[dev].[JobDegreeLevel]

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.JobDegreeLevel] OFF

RAISERROR('Populating [Library.JobDegreeEducationLevel]', 0, 1) WITH NOWAIT

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.JobDegreeEducationLevel] ON

INSERT INTO [FocusStage03XXWF].[dbo].[Library.JobDegreeEducationLevel]
           ([Id]
           ,[ExcludeFromJob]
           ,[Tier]
           ,[JobId]
           ,[DegreeEducationLevelId])
 SELECT [Id]
      ,[ExcludeFromJob]
      ,[Tier]
      ,[JobId]
      ,[DegreeEducationLevelId]
  FROM [FocusExplorer].[dev].[JobDegreeEducationLevel]

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.JobDegreeEducationLevel] OFF

RAISERROR('Populating [Library.JobDegreeEducationLevelReport]', 0, 1) WITH NOWAIT

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.JobDegreeEducationLevelReport] ON

SET @FirstRow = 1
SELECT @LastRow = MAX(Id) FROM [FocusExplorer].[dev].[JobDegreeEducationLevelReport]

WHILE @FirstRow <= @LastRow
BEGIN
	RAISERROR('From Id %d', 0, 1, @FirstRow) WITH NOWAIT

	INSERT INTO [FocusStage03XXWF].[dbo].[Library.JobDegreeEducationLevelReport]
           ([Id]
           ,[HiringDemand]
           ,[DegreesAwarded]
           ,[StateAreaId]
           ,[JobDegreeEducationLevelId])
		SELECT TOP (@BatchSize) 
		  [Id]
          ,[HiringDemand]
          ,[DegreesAwarded]
          ,[StateAreaId]
          ,[JobDegreeEducationLevelId]
		FROM [FocusExplorer].[dev].[JobDegreeEducationLevelReport]
	WHERE Id >= @FirstRow
	ORDER BY Id ASC

	SELECT @FirstRow = MAX(Id) + 1 FROM [FocusStage03XXWF].[dbo].[Library.JobDegreeEducationLevelReport]
END

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.JobDegreeEducationLevelReport] OFF

RAISERROR('Populating [Library.JobCareerPathTo]', 0, 1) WITH NOWAIT

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.JobCareerPathTo] ON

INSERT INTO [FocusStage03XXWF].[dbo].[Library.JobCareerPathTo]
           ([Id]
           ,[Rank]
           ,[CurrentJobId]
           ,[Next1JobId]
           ,[Next2JobId])
SELECT [Id]
      ,[Rank]
      ,[CurrentJobId]
      ,[Next1JobId]
      ,[Next2JobId]
  FROM [FocusExplorer].[dev].[JobCareerPathTo]

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.JobCareerPathTo] OFF

RAISERROR('Populating [Library.JobCareerPathFrom]', 0, 1) WITH NOWAIT

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.JobCareerPathFrom] ON

INSERT INTO [FocusStage03XXWF].[dbo].[Library.JobCareerPathFrom]
           ([Id]
           ,[Rank]
           ,[FromJobId]
           ,[ToJobId])
SELECT [Id]
      ,[Rank]
      ,[FromJobId]
      ,[ToJobId]
  FROM [FocusExplorer].[dev].[JobCareerPathFrom]

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.JobCareerPathFrom] OFF

RAISERROR('Populating [Library.JobCareerArea]', 0, 1) WITH NOWAIT

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.JobCareerArea] ON

INSERT INTO [FocusStage03XXWF].[dbo].[Library.JobCareerArea]
           ([Id]
           ,[JobId]
           ,[CareerAreaId])
SELECT ROW_NUMBER() OVER(ORDER BY JobId, CareerAreaId)
      ,[JobId]
      ,[CareerAreaId]
  FROM [FocusExplorer].[dev].[JobCareerArea]

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.JobCareerArea] OFF

RAISERROR('Populating [Library.JobRelatedJob]', 0, 1) WITH NOWAIT

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.JobRelatedJob] ON

INSERT INTO [FocusStage03XXWF].[dbo].[Library.JobRelatedJob]
           ([Id]
           ,[RelatedJobId]
           ,[Priority]
           ,[JobId])
SELECT ROW_NUMBER() OVER(ORDER BY JobID, SimJobId)
      ,SimJobId AS [RelatedJobId]
      ,1 AS [Priority]
      ,[JobId]
  FROM [FocusExplorer].[dev].[JobSimJob]

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.JobRelatedJob] OFF

RAISERROR('Populating [Library.JobTitle]', 0, 1) WITH NOWAIT

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.JobTitle] ON

INSERT INTO [FocusStage03XXWF].[dbo].[Library.JobTitle]
           ([Id]
           ,[Name])
SELECT [Id]
      ,[Name]
  FROM [FocusExplorer].[dev].[JobTitle]

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.JobTitle] OFF

RAISERROR('Populating [Library.JobJobTitle]', 0, 1) WITH NOWAIT

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.JobJobTitle] ON

INSERT INTO [FocusStage03XXWF].[dbo].[Library.JobJobTitle]
           ([Id]
           ,[DemandPercentile]
           ,[JobId]
           ,[JobTitleId])
SELECT ROW_NUMBER() OVER(ORDER BY JobID, JobTitleId)
      ,[DemandPercentile]
      ,[JobId]
      ,[JobTitleId]
  FROM [FocusExplorer].[dev].[JobJobTitle]

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.JobJobTitle] OFF

RAISERROR('Populating [Library.JobReport]', 0, 1) WITH NOWAIT

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.JobReport] ON

INSERT INTO [FocusStage03XXWF].[dbo].[Library.JobReport]
           ([Id]
           ,[HiringDemand]
           ,[SalaryTrend]
           ,[SalaryTrendPercentile]
           ,[SalaryTrendAverage]
           ,[LocalisationId]
           ,[GrowthTrend]
           ,[GrowthPercentile]
           ,[HiringTrend]
           ,[HiringTrendPercentile]
           ,[SalaryTrendMin]
           ,[SalaryTrendMax]
           ,[SalaryTrendRealtime]
           ,[SalaryTrendRealtimeAverage]
           ,[SalaryTrendRealtimeMin]
           ,[SalaryTrendRealtimeMax]
           ,[JobId]
           ,[StateAreaId]
		   ,[HiringTrendSubLevel])
SELECT ROW_NUMBER() OVER(ORDER BY JRD.JobId)
      ,ISNULL(JRD.[HiringDemand], 0)
      ,ISNULL(JRS.[SalaryTrend], 0)
      ,ISNULL(JRS.[SalaryTrendPercentile], 0)
      ,ISNULL(JRS.[SalaryTrendAverage], 0)
      ,JRD.[LocalisationId]
      ,'' AS [GrowthTrend]
      ,0 AS [GrowthPercentile]
      ,ISNULL(JRD.[HiringTrend], 0)
      ,ISNULL(JRD.[HiringTrendPercentile], 0)
      ,ISNULL(JRS.[SalaryTrendMin], 0)
      ,ISNULL(JRS.[SalaryTrendMax], 0)
      ,ISNULL(JRS.[SalaryTrendRealtime], 0)
      ,ISNULL(JRS.[SalaryTrendRealtimeAverage], 0)
      ,ISNULL(JRS.[SalaryTrendRealtimeMin], 0)
      ,ISNULL(JRS.[SalaryTrendRealtimeMax], 0)
      ,JRD.[JobId]
      ,JRD.[StateAreaId]
	  ,ISNULL(JRD.[HiringTrendSubLevel], 0)
  FROM [FocusExplorer].[dev].[JobReport_Demand] JRD
  LEFT JOIN [FocusExplorer].[dev].[JobReport_Salary] JRS
	ON JRS.JobId = JRD.JobId
	AND JRS.StateAreaId = JRD.StateAreaId

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.JobReport] OFF

RAISERROR('Populating [Library.InternshipCategory]', 0, 1) WITH NOWAIT

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.InternshipCategory] ON

INSERT INTO [FocusStage03XXWF].[dbo].[Library.InternshipCategory]
           ([Id]
           ,[Name]
           ,[LocalisationId]
           ,[LensFilterId])
SELECT [Id]
      ,[Name]
      ,[LocalisationId]
      ,[LensFilterId]
  FROM [FocusExplorer].[dev].[InternshipCategory]

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.InternshipCategory] OFF

RAISERROR('Populating [Library.InternshipReport]', 0, 1) WITH NOWAIT

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.InternshipReport] ON

INSERT INTO [FocusStage03XXWF].[dbo].[Library.InternshipReport]
           ([Id]
           ,[NumberAvailable]
           ,[StateAreaId]
           ,[InternshipCategoryId])
SELECT ROW_NUMBER() OVER(ORDER BY InternshipCategoryId, StateAreaId)
      ,[NumberAvailable]
      ,[StateAreaId]
      ,[InternshipCategoryId]
  FROM [FocusExplorer].[dev].[InternshipReport]

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.InternshipReport] OFF

RAISERROR('Populating [Library.InternshipSkill]', 0, 1) WITH NOWAIT

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.InternshipSkill] ON

INSERT INTO [FocusStage03XXWF].[dbo].[Library.InternshipSkill]
           ([Id]
           ,[Rank]
           ,[SkillId]
           ,[InternshipCategoryId])
SELECT ROW_NUMBER() OVER(ORDER BY InternshipCaregoryId, SkillId)
      ,[Rank]
      ,[SkillId]
      ,[InternshipCaregoryId] AS [InternshipCategoryId]
  FROM [FocusExplorer].[dev].[InternshipSkill]

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.InternshipSkill] OFF

RAISERROR('Populating [Library.InternshipJobTitle]', 0, 1) WITH NOWAIT

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.InternshipJobTitle] ON

INSERT INTO [FocusStage03XXWF].[dbo].[Library.InternshipJobTitle]
           ([Id]
           ,[JobTitle]
           ,[Frequency]
           ,[InternshipCategoryId])
SELECT ROW_NUMBER() OVER(ORDER BY InternshipCategoryId, JobTitle)
      ,[JobTitle]
      ,[Frequency]
      ,[InternshipCategoryId]
  FROM [FocusExplorer].[dev].[InternshipJobTitle]

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.InternshipJobTitle] OFF

RAISERROR('Populating [Library.InternshipEmployer]', 0, 1) WITH NOWAIT

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.InternshipEmployer] ON

INSERT INTO [FocusStage03XXWF].[dbo].[Library.InternshipEmployer]
           ([Id]
           ,[Frequency]
           ,[EmployerId]
           ,[StateAreaId]
           ,[InternshipCategoryId])
SELECT ROW_NUMBER() OVER(ORDER BY InternshipCategoryId, EmployerId, StateAreaId)
      ,[Frequency]
      ,[EmployerId]
      ,[StateAreaId]
      ,[InternshipCategoryId]
  FROM [FocusExplorer].[dev].[InternshipEmployer]

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.InternshipEmployer] OFF

RAISERROR('Populating [Library.SkillCategory]', 0, 1) WITH NOWAIT

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.SkillCategory] ON

INSERT INTO [FocusStage03XXWF].[dbo].[Library.SkillCategory]
           ([Id]
           ,[Name]
           ,[LocalisationId]
           ,[ParentId])
SELECT [Id]
      ,[Name]
      ,[LocalisationId]
      ,[ParentId]
  FROM [FocusExplorer].[dev].[SkillCategory]

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.SkillCategory] OFF

RAISERROR('Populating [Library.SkillReport]', 0, 1) WITH NOWAIT

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.SkillReport] ON

INSERT INTO [FocusStage03XXWF].[dbo].[Library.SkillReport]
           ([Id]
           ,[Frequency]
           ,[SkillId]
           ,[StateAreaId])
SELECT [Id]
      ,[Frequency]
      ,[SkillId]
      ,[StateAreaId]
  FROM [FocusExplorer].[dev].[SkillReport]

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.SkillReport] OFF

RAISERROR('Populating [Library.SkillCategorySkill]', 0, 1) WITH NOWAIT

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.SkillCategorySkill] ON

INSERT INTO [FocusStage03XXWF].[dbo].[Library.SkillCategorySkill]
           ([Id]
           ,[SkillCategoryId]
           ,[SkillId])
SELECT ROW_NUMBER() OVER(ORDER BY SkillCategoryId, SkillId)
      ,[SkillCategoryId]
      ,[SkillId]
  FROM [FocusExplorer].[dev].[SkillCategorySkill]
  
SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.SkillCategorySkill] OFF

RAISERROR('Populating [Library.EmployerSkill', 0, 1) WITH NOWAIT

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.EmployerSkill] ON

INSERT INTO [FocusStage03XXWF].[dbo].[Library.EmployerSkill]
           ([Id]
           ,[Postings]
           ,[EmployerId]
           ,[SkillId])
SELECT ROW_NUMBER() OVER(ORDER BY EmployerId, SkillId)
      ,[Postings]
      ,[EmployerId]
      ,[SkillId]
  FROM [FocusExplorer].[dev].[EmployerSkill]

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.EmployerSkill] OFF

RAISERROR('Populating [Library.EmployerJob', 0, 1) WITH NOWAIT

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.EmployerJob] ON

INSERT INTO [FocusStage03XXWF].[dbo].[Library.EmployerJob]
           ([Id]
           ,[Positions]
           ,[JobId]
           ,[EmployerId]
           ,[StateAreaId])
SELECT ROW_NUMBER() OVER(ORDER BY EmployerId, JobId, StateAreaId)
      ,[Positions]
      ,[JobId]
      ,[EmployerId]
      ,[StateAreaId]
  FROM [FocusExplorer].[dev].[EmployerJob]

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.EmployerJob] OFF

RAISERROR('Populating [Library.StudyPlace', 0, 1) WITH NOWAIT

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.StudyPlace] ON

INSERT INTO [FocusStage03XXWF].[dbo].[Library.StudyPlace]
           ([Id]
           ,[Name]
           ,[StateAreaId])
SELECT [Id]
      ,[Name]
      ,[StateAreaId]
  FROM [FocusExplorer].[dev].[StudyPlace]

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.StudyPlace] OFF

RAISERROR('Populating [Library.StudyPlaceDegreeEducationLevel', 0, 1) WITH NOWAIT

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.StudyPlaceDegreeEducationLevel] ON

INSERT INTO [FocusStage03XXWF].[dbo].[Library.StudyPlaceDegreeEducationLevel]
           ([Id]
           ,[StudyPlaceId]
           ,[DegreeEducationLevelId])
SELECT [Id]
      ,[StudyPlaceId]
      ,[DegreeEducationLevelId]
  FROM [FocusExplorer].[dev].[StudyPlaceDegreeEducationLevel]

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.StudyPlaceDegreeEducationLevel] OFF

RAISERROR('Populating [Library.ProgramArea', 0, 1) WITH NOWAIT

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.ProgramArea] ON

INSERT INTO [FocusStage03XXWF].[dbo].[Library.ProgramArea]
           ([Id]
           ,[Name]
           ,[Description]
           ,[IsClientData]
           ,[ClientDataTag])
SELECT [Id]
      ,[Name]
      ,[Description]
      ,[IsClientData]
      ,[ClientDataTag]
  FROM [FocusExplorer].[dev].[ProgramArea]

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.ProgramArea] OFF

/*
RAISERROR('Populating [Library.ProgramAreaDegree', 0, 1) WITH NOWAIT

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.ProgramAreaDegree] ON

INSERT INTO [FocusStage03XXWF].[dbo].[Library.ProgramAreaDegree]
           ([Id]
           ,[DegreeId]
           ,[ProgramAreaId])
SELECT [Id]
      ,[DegreeId]
      ,[ProgramAreaId]
  FROM [FocusExplorer].[dev].[ProgramAreaDegree]


SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.ProgramAreaDegree] OFF
*/

RAISERROR('Populating [Library.DegreeEducationLevelExt', 0, 1) WITH NOWAIT

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.DegreeEducationLevelExt] ON

INSERT INTO [FocusStage03XXWF].[dbo].[Library.DegreeEducationLevelExt]
           ([Id]
           ,[Name]
           ,[Url]
           ,[Description]
           ,[DegreeEducationLevelId])
SELECT [Id]
      ,[Name]
      ,[Url]
      ,[Description]
      ,[DegreeEducationLevelId]
  FROM [FocusExplorer].[dev].[DegreeEducationLevelExt]

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.DegreeEducationLevelExt] OFF

RAISERROR('Populating [Library.DegreeAlias', 0, 1) WITH NOWAIT

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.DegreeAlias] ON

INSERT INTO [FocusStage03XXWF].[dbo].[Library.DegreeAlias]
           ([Id]
           ,[AliasExtended]
           ,[EducationLevel]
           ,[Alias]
           ,[DegreeId]
           ,[DegreeEducationLevelId])
SELECT [Id]
      ,[AliasExtended]
      ,[EducationLevel]
      ,[Alias]
      ,[DegreeId]
      ,[DegreeEducationLevelId]
  FROM [FocusExplorer].[dev].[DegreeAlias]

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.DegreeAlias] OFF

RAISERROR('Populating [Library.Certification', 0, 1) WITH NOWAIT

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.Certification] ON

INSERT INTO [FocusStage03XXWF].[dbo].[Library.Certification]
           ([Id]
           ,[Name])
SELECT [Id]
      ,[Name]
  FROM [FocusExplorer].[dev].[Certification]
  
SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.Certification] OFF

RAISERROR('Populating [Library.JobCertification', 0, 1) WITH NOWAIT

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.JobCertification] ON

INSERT INTO [FocusStage03XXWF].[dbo].[Library.JobCertification]
           ([Id]
           ,[DemandPercentile]
           ,[JobId]
           ,[CertificationId])
SELECT ROW_NUMBER() OVER(ORDER BY JobId, CertificationId)
      ,[DemandPercentile]
      ,[JobId]
      ,[CertificationId]
  FROM [FocusExplorer].[dev].[JobCertification]

SET IDENTITY_INSERT [FocusStage03XXWF].[dbo].[Library.JobCertification] OFF

--COMMIT TRANSACTION

--ROLLBACK TRANSACTION
