﻿
namespace Focus.Core
{	
	public class Constants
	{
		public const string DefaultCulture = "**-**";
		public const string StateKeyPrefix = "Focus";
		public const string DataContractNamespace = "http://www.burning-glass.com/at/types/";
		
		// 00.00.0000 = Version . Release . Patch
		internal const string SystemVersion = "2.01.0000"; 

		public class AlertMessageKeys
		{
			public const string JobPosted = "AlertMessage.JobPosted";
      public const string JobExpiring = "AlertMessage.JobExpiring";
      public const string JobExpired = "AlertMessage.JobExpired";
		}

		public class WebConfigKeys
		{
			public const string Config = "Focus-Config";
			public const string Module = "Focus-Module";
			public const string Application = "Focus-Application";
			public const string ClientTag = "Focus-ClientTag";
			public const string LogSeverity = "Focus-LogSeverity";
			public const string ProfilingEnabled = "Focus-ProfilingEnabled";
			public const string CachePrefix = "Focus-CachePrefix";
			public const string OnErrorEmail = "Focus-OnErrorEmail";
			public const string EmailsEnabled = "Focus-EmailsEnabled";

			public const string LensServerHost = "LensServerHost";
			public const string LensServerPort = "LensServerPort";
			public const string LensServerTimeOut = "LensServerTimeOut";
			public const string LensServerVendorKey = "LensServerVendorKey";
			public const string LensServerCharacterSet = "LensServerCharacterSet";

			public const string FocusCareerServiceUrl = "Focus-FocusCareer-ServiceUrl";
			public const string FocusCareerAccessKey = "Focus-FocusCareer-AccessKey";
			public const string FocusCareerUrl = "Focus-FocusCareer-Url";
		}

		public class ConfigurationItemKeys
		{
			public const string AppVersion = "AppVersion";
			public const string FocusTalentApplicationName = "FocusTalentApplicationName";
			public const string FocusAssistApplicationName = "FocusAssistApplicationName";
			public const string FocusCareerApplicationName = "FocusCareerApplicationName";
			public const string FocusReportingApplicationName = "FocusReportingApplicationName";
			public const string FocusExplorerApplicationName = "FocusExplorerApplicationName";

			public const string SupportEmail = "SupportEmail";
			public const string SupportPhone = "SupportPhone";
			public const string SupportHoursOfBusiness = "SupportHoursOfBusiness";

			public const string LicenseExpiration = "LE"; // LE is this way so that it doesn't make sense in the config table			
			public const string ValidClientTags = "ValidClientTags";			
			public const string SmtpServer = "SmtpServer";
			public const string SystemEmailFrom = "SystemEmailFrom";
			public const string UserNameRegExPattern = "UserNameRegExPattern";
			public const string EmailAddressRegExPattern = "EmailAddressRegExPattern";
			public const string PasswordRegExPattern = "PasswordRegExPattern";
			public const string UrlRegExPattern = "UrlRegExPattern";
			public const string AllowTalentCompanyInformationUpdate = "AllowTalentCompanyInformationUpdate";
			public const string ResumeUploadAllowedFileRegExPattern = "ResumeUploadAllowedFileRegExPattern";
			public const string ResumeUploadMaximumAllowedFileSize = "ResumeUploadMaximumAllowedFileSize";
			public const string TalentPoolSearchMaximumRecordsToReturn = "TalentPoolSearchMaximumRecordsToReturn";
			public const string StarRatingMinimumScores = "StarRatingMinimumScores";
			public const string PageStatePersister = "Focus-PageStatePersister";
			public const string PageStateQueueMaxLength = "Focus-PageStateQueueMaxLength";
			public const string PageStateTimeOut = "Focus-PageStateTimeOut";
			public const string PageStateConnectionString = "Focus-PageStateConnectionString";			
			public const string DistanceUnits = "DistanceUnits";
			public const string DefaultJobExpiry = "DefaultJobExpiry";
			public const string SimilarJobsCount = "SimilarJobsCount";
      public const string SessionTimeout = "UserSessionTimeout";
      public const string UserSessionTimeout = "SessionTimeout";
			public const string EmployeeSearchMaximumRecordsToReturn = "EmployeeSearchMaximumRecordsToReturn";
			public const string TalentApplicationPath = "TalentApplicationPath";
			public const string AssistApplicationPath = "AssistApplicationPath";
			public const string ShowBurningGlassLogoInTalent = "ShowBurningGlassLogoInTalent";
			public const string ShowBurningGlassLogoInAssist = "ShowBurningGlassLogoInAssist";
			public const string ShowBurningGlassLogoInExplorer = "ShowBurningGlassLogoInExplorer";
			public const string AllJobPostingsRequireApproval = "AllJobPostingsRequireApproval";
			public const string EmployerPreferencesOverideSystemDefaults = "EmployerPreferencesOverideSystemDefaults";
			public const string FeedbackEmailAddress = "FeedbackEmailAddress";
			public const string JobPostingStylesheet = "JobPostingStylesheet";
			public const string FullResumeStylesheet = "FullResumeStylesheet";
			public const string PartialResumeStylesheet = "PartialResumeStylesheet";
      public const string RegistrationCompleteAction = "RegistrationCompleteAction";
      public const string FEINRegExPattern = "FEINRegExPattern";
		  public const string RegistrationRoute = "RegistrationRoute";
			public const string DummyUsernameFormat = "DummyUsernameFormat";
			public const string AuthenticateAssistUserViaClient = "AuthenticateAssistUserViaClient";
			public const string ClientAllowsPasswordReset = "ClientAllowsPasswordReset";
			public const string TalentLightColour = "TalentLightColour";
			public const string TalentDarkColour = "TalentDarkColour";
			public const string AssistLightColour = "AssistLightColour";
			public const string AssistDarkColour = "AssistDarkColour";
		  public const string JobExpiryCountdownDays = "JobExpiryCountdownDays";
		  public const string ProcessExpiredJobsRunning = "ProcessExpiredJobsRunning";
		  public const string ProcessExpiredJobsLastRun = "ProcessExpiredJobsLastRun";
		  public const string ProcessExpiringJobsRunning = "ProcessExpiringJobsRunning";
		  public const string ProcessExpiringJobsLastRun = "ProcessExpiringJobsLastRun";
		  public const string DefaultStateKey = "DefaultStateKey";
			public const string JobSeekerReferralsEnabled = "JobSeekerReferralsEnabled";
			public const string MaximumNumberOfDaysCanExtendJobBy = "MaximumNumberOfDaysCanExtendJobBy";
			public const string MaximumSalary = "MaximumSalary";
			public const string PostalCodeRegExPattern = "PostalCodeRegExPattern";
			public const string PostalCodeMaskPattern = "PostalCodeMaskPattern";

			// Lens Config
			public const string LensVeteranStatusCustomFilterTag = "LensVeteranStatusCustomFilterTag";
			public const string LensDrivingLicenceClassCustomFilterTag = "LensDrivingLicenceClassCustomFilterTag";
			public const string LensDrivingLicenceEndorsementCustomFilterTag = "LensDrivingLicenceEndorsementCustomFilterTag";
			public const string LensWorkOvertimeCustomFilterTag = "LensWorkOvertimeCustomFilterTag";
			public const string LensShiftTypeCustomFilterTag = "LensShiftTypeCustomFilterTag";
			public const string LensWorkTypeCustomFilterTag = "LensWorkTypeCustomFilterTag";
			public const string LensWorkWeekCustomFilterTag = "LensWorkWeekCustomFilterTag";
			public const string LensJobseekerIdCustomFilterTag = "LensJobseekerIdCustomFilterTag";
		}

		public class CacheKeys
		{
			public const string IsLicensedKey = "IsLicensedKey";
			public const string Application = "Application-{0}";
			public const string Configuration = "Configuration";
			public const string LocalisationDictionary = "Localisation-{0}";
			public const string LookupKey = "Lookup-{0}";
			public const string JobTitlesKey = "JobTitlesKey";
			public const string OnetKey = "Onet-{0}:{1}";
			public const string OnetTaskKey = "OnetTask-{0}:{1}";
			public const string CertificateLicenseKey = "CertLic-{0}";
			public const string ProfanityKey = "Profanities";
			public const string NaicsNamesKey = "NaicsNamesKey";
			public const string ExplorerLocationsKey = "ExplorerLocations-{0}";
			public const string ExplorerCareerAreasKey = "ExplorerCareerAreas-{0}";
			public const string ExplorerDegreeCertificationsKey = "ExplorerDegreeCertifications-{0}";
			public const string ExplorerSkillCategoriesKey = "ExplorerSkillCategories-{0}";
			public const string ExplorerSkillsKey = "ExplorerSkills-{0}";
			public const string ApplicationImage = "ApplicationImage-{0}";
		}

		public class RoleKeys
		{
			public const string SystemAdmin = "SystemAdmin";
			public const string Admin = "Admin";

			public const string TalentUser = "TalentUser";
			public const string AssistUser = "AssistUser";
			public const string AssistAccountAdministrator = "AssistAccountAdministrator";
			public const string AssistSystemAdministrator = "AssistSystemAdministrator";
			public const string AssistBroadcastsAdministrator = "AssistBroadcastsAdministrator";
			public const string AssistPostingApprover = "AssistPostingApprover";
			public const string AssistEmployerAccountApprover = "AssistEmployerAccountApprover";
			public const string AssistJobSeekerReferralApprover = "AssistJobSeekerReferralApprover";
			public const string AssistEmployersReadOnly = "AssistEmployersReadOnly";
			public const string AssistEmployersAdministrator = "AssistEmployersAdministrator";
			public const string AssistEmployersAccountBlocker = "AssistEmployersAccountBlocker";
			public const string AssistJobSeekersReadOnly = "AssistJobSeekersReadOnly";
			public const string AssistJobSeekersAdministrator = "AssistJobSeekersAdministrator";
		}

		public class StateKeys
		{
			public const string UploadedJobDescription = "UploadedJobDescription";
			public const string UploadedLogo = "UploadedLogo";
			public const string UploadedLogoIsValid = "UploadedLogoIsValid";
			public const string ManageCompanyModel = "ManageCompanyModel";
			public const string CandidateSearhCriteria = "CandidateSearhCriteria";
			public const string CandidateSearhTitle = "CandidateSearhTitle";
			public const string UploadedImage = "UploadedImage";
			public const string UploadedImageIsValid = "UploadedImageIsValid";
			public const string PageError = "PageError";
		}

		public class PlaceHolders
		{
			public const string FocusTalent = "#FOCUSTALENT#";
			public const string FocusAssist = "#FOCUSASSIST#";
			public const string FocusCareer = "#FOCUSCAREER#";
			public const string FocusReporting = "#FOCUSREPORTING#";
			public const string FocusExplorer = "#FOCUSEXPLORER#";

			public const string SupportEmail = "#SUPPORTEMAIL#";
			public const string SupportPhone = "#SUPPORTPHONE#";
			public const string SupportHoursOfBusiness = "#SUPPORTHOURSOFBUSINESS#";

			public const string RecipientName = "#RECIPIENTNAME#";
			public const string JobTitle = "#JOBTITLE#";
			public const string JobPosting = "#JOBPOSTING#";
			public const string EmployerName = "#EMPLOYERNAME#";
			public const string SenderName = "#SENDERNAME#";
			public const string SenderPhoneNumber = "#SENDERPHONENUMBER#";
			public const string SenderEmailAddress = "#SENDEREMAILADDRESS#";
			public const string FreeText = "#FREETEXT#";
			public const string JobLinkUrl = "#JOBLINKURL#";
			public const string ApplicationInstructions = "#APPLICATIONINSTRUCTIONS#";
			public const string SavedSearchName = "#SAVEDSEARCHNAME#";
			public const string MatchedCandidates = "#MATCHEDCANDIDATES#";
		}
	}
}
