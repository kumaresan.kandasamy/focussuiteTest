﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core;

#endregion

namespace Focus.Services.Messages
{
	public class GenerateReportMessage : EventMessage
	{
		[DataMember]
		public DateTime ReportFromDate { get; set; }

		[DataMember]
		public DateTime ReportToDate { get; set; }

		[DataMember]
		public AsyncReportType ReportType { get; set; }

		[DataMember]
		public List<string> Emails { get; set; }
	}
}
