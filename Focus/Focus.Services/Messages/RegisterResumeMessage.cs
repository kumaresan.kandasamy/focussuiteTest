﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

using Focus.Core;

#endregion

namespace Focus.Services.Messages
{
	[Obsolete]
	public class RegisterResumeMessage : EventMessage
	{
		[DataMember(Name = "ResumeId", IsRequired = true, Order = 10)]
		public long ResumeId { get; set; }

		[DataMember(Name = "PersonId", IsRequired = true, Order = 11)]
		public long PersonId { get; set; }

    [DataMember(Name = "IgnoreLens", IsRequired = false, Order = 12)]
    public bool IgnoreLens { get; set; }

    [DataMember(Name = "IgnoreClient", IsRequired = false, Order = 13)]
    public bool IgnoreClient { get; set; }

		[DataMember(Name = "ActionTypesToPublish", IsRequired = false, Order = 14)]
		public List<ActionTypes> ActionTypesToPublish { get; set; }
	}
}
