﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core;

#endregion

namespace Focus.Services.Messages
{
	public class ProcessRegistrationPinEmailMessage : EventMessage
	{
		[DataMember(Name = "Emails", IsRequired = true)]
		public List<string> Emails { get; set; }

		[DataMember(Name = "RegistrationUrl", IsRequired = true)]
		public string RegistrationUrl { get; set; }

    [DataMember(Name = "ResetPasswordUrl", IsRequired = true)]
    public string ResetPasswordUrl { get; set; }
    
    [DataMember(Name = "TargetModule", IsRequired = true)]
		public FocusModules TargetModule { get; set; }
	}
}
