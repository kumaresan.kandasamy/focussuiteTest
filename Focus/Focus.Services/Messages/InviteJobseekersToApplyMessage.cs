﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;

#endregion

namespace Focus.Services.Messages
{
	public class InviteJobseekersToApplyMessage : EventMessage
	{
		/// <summary>
		/// Gets or sets the person ids of the jobseekers to invite. (No longer used, but left to handle existing messages created prior to it being deprecated)
		/// </summary>
		[DataMember(Name = "PersonIds", IsRequired = false, Order = 10)]
		public List<long> PersonIds { get; set; }

		/// <summary>
		/// Gets or sets the job id.
		/// </summary>
		[DataMember(Name = "JobId", IsRequired = true, Order = 11)]
		public long JobId { get; set; }

		/// <summary>
		/// Gets or sets the job link.
		/// </summary>
		[DataMember(Name = "JobLink", IsRequired = true, Order = 12)]
		public string JobLink { get; set; }

		/// <summary>
		/// Gets or sets the lens posting id.
		/// </summary>
		[DataMember(Name = "LensPostingId", IsRequired = true, Order = 13)]
		public string LensPostingId { get; set; }

		/// <summary>
		/// Gets or sets the name of the senders.
		/// </summary>
		[DataMember(Name = "SendersName", IsRequired = true, Order = 14)]
		public string SendersName { get; set; }

		/// <summary>
		/// Gets or sets the sender email.
		/// </summary>
		[DataMember(Name = "SenderEmail", IsRequired = true, Order = 15)]
		public string SenderEmail { get; set; }

		/// <summary>
		/// Gets or sets the person ids of the jobseekers to invite, along with their matching scores
		/// </summary>
		[DataMember(Name = "PersonIds", IsRequired = false, Order = 16)]
		public Dictionary<long, int> PersonIdsWithScores { get; set; }
	}
}
