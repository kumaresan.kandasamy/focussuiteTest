﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using Focus.Core.Messages.CandidateService;
using System.Globalization;

namespace Focus.Services.Messages
{
    public class SendBlastEmailMessages : ProcessMessage
    {
        [DataMember(Name = "EmailsToSend")]
        public List<Focus.Core.Views.CandidateEmailView> Emails { get; set; }

        [DataMember(Name = "EmployeeEmailsToSend")]
        public Focus.Core.Views.EmployeeEmailView EmployeesEmails { get; set; }

        [DataMember]
        public bool DetectUrl { get; set; }

        [DataMember]
        public bool SendCopy { get; set; }

        [DataMember]
        public Focus.Core.EmailFooterTypes FooterType { get; set; }

        [DataMember]
        public string FooterUrl { get; set; }

        [DataMember]
        public bool CheckSubscribed { get; set; }

        [DataMember]
        public bool IsEmployeeEmail { get; set; }

       
    }
}
