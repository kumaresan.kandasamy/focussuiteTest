﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

using Focus.Core;

#endregion

namespace Focus.Services.Messages
{
  public class UpdateReportingMessage : EventMessage
  {
    /// <summary>
    /// Multiple entities to update
    /// </summary>
    [DataMember]
    public List<Tuple<long, long?>> EntityIds { get; set; }

    /// <summary>
    /// The type of entity to update
    /// </summary>
    [DataMember]
    public ReportEntityType EntityType { get; set; }

    /// <summary>
    /// Whether the selected entities need to be deleted
    /// </summary>
    [DataMember]
    public bool EntityDeletion { get; set; }

    /// <summary>
    /// When updating all entities (EntityIds is empty), whether to delete all entities first
    /// </summary>
    [DataMember]
    public bool ResetAll { get; set; }
  }
}
