﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;
using Focus.Core;

#endregion

namespace Focus.Services.Messages
{
	public class HoldReferralMessage : EventMessage
	{
		[DataMember(Name = "ApplicationId", IsRequired = true, Order = 1)]
		public long ApplicationId { get; set; }

		[DataMember(Name = "Reason", IsRequired = true, Order = 2)]
		public ApplicationOnHoldReasons Reason { get; set; }
	}
}
