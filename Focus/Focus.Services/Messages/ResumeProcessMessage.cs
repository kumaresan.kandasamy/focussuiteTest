﻿#region Copyright © 2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core;

#endregion

namespace Focus.Services.Messages
{
	public class ResumeProcessMessage : EventMessage
	{
		[DataMember(Name = "RegisterResume", IsRequired = true, Order = 10)]
		public bool RegisterResume { get; set; }

		[DataMember(Name = "ResumeId", IsRequired = true, Order = 11)]
		public long ResumeId { get; set; }

		[DataMember(Name = "PersonId", IsRequired = true, Order = 12)]
		public long PersonId { get; set; }

		[DataMember(Name = "IgnoreLens", IsRequired = false, Order = 13)]
		public bool IgnoreLens { get; set; }

		[DataMember(Name = "IgnoreClient", IsRequired = false, Order = 14)]
		public bool IgnoreClient { get; set; }

		[DataMember(Name = "PersonIds", IsRequired = false, Order = 15)]
		public List<long> PersonIds { get; set; }

		[DataMember(Name = "PersonIds", IsRequired = false, Order = 16)]
		public List<ActionTypes> ActionTypesToPublish { get; set; }
	}
}
