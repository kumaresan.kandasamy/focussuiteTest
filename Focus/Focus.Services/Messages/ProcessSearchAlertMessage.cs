﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

#endregion

namespace Focus.Services.Messages
{
	public class ProcessSearchAlertMessage : ProcessMessage
	{
		[DataMember(Name = "SearchAlertId", IsRequired = true, Order = 10)]
		public long SearchAlertId { get; set; }
	}
}
