﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Services.Messages
{
	[Obsolete]
	public class RegisterPostingMessage : EventMessage
	{
		[DataMember(Name = "PostingId", IsRequired = true, Order = 10)]
		public long PostingId { get; set; }

    [DataMember(Name = "IsVeteranJob", IsRequired = false, Order = 11)]
    public bool IsVeteranJob { get; set; }

    [DataMember(Name = "IgnoreLens", IsRequired = false, Order = 12)]
    public bool IgnoreLens { get; set; }

    [DataMember(Name = "JobId", IsRequired = true, Order = 13)]
    public long JobId { get; set; }

    [DataMember(Name = "IgnoreClient", IsRequired = false, Order = 14)]
    public bool IgnoreClient { get; set; }
	}
}
