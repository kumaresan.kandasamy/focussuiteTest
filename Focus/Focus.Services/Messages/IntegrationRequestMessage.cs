﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

using Focus.Core;
using Focus.Core.IntegrationMessages;

#endregion

namespace Focus.Services.Messages
{
  public class IntegrationRequestMessage : EventMessage
  {
    [DataMember(Name = "IntegrationPoint", IsRequired = true, Order = 10)]
    public IntegrationPoint IntegrationPoint { get; set; }

    [DataMember(Name = "SaveJobRequest", IsRequired = false, Order = 11)]
    public AuthenticateJobSeekerRequest AuthenticateJobSeekerRequest { get; set; }

    [DataMember(Name = "SaveJobSeekerRequest", IsRequired = false, Order = 12)]
    public SaveJobSeekerRequest SaveJobSeekerRequest { get; set; }

    [DataMember(Name = "SaveJobMatchesRequest", IsRequired = false, Order = 13)]
    public SaveJobMatchesRequest SaveJobMatchesRequest { get; set; }

    [DataMember(Name = "AssignJobSeekerActivityRequest", IsRequired = false, Order = 14)]
    public AssignJobSeekerActivityRequest AssignJobSeekerActivityRequest { get; set; }

    [DataMember(Name = "ChangeUserPasswordRequest", IsRequired = false, Order = 15)]
    public ChangeUserPasswordRequest ChangeUserPasswordRequest { get; set; }

    [DataMember(Name = "SaveJobRequest", IsRequired = false, Order = 16)]
    public SaveJobRequest SaveJobRequest { get; set; }

    [DataMember(Name = "GetJobRequest", IsRequired = false, Order = 17)]
    public GetJobRequest GetJobRequest { get; set; }

    [DataMember(Name = "UpdateJobStatusRequest", IsRequired = false, Order = 18)]
    public UpdateJobStatusRequest UpdateJobStatusRequest { get; set; }

    [DataMember(Name = "ReferJobSeekerRequest", IsRequired = false, Order = 18)]
    public ReferJobSeekerRequest ReferJobSeekerRequest { get; set; }

    [DataMember(Name = "GetEmployerRequest", IsRequired = false, Order = 19)]
    public GetJobSeekerRequest GetEmployerRequest { get; set; }

    [DataMember(Name = "SaveEmployerRequest", IsRequired = false, Order = 20)]
    public SaveEmployerRequest SaveEmployerRequest { get; set; }

    [DataMember(Name = "AssignAdminToEmployerRequest", IsRequired = false, Order = 21)]
    public AssignAdminToEmployerRequest AssignAdminToEmployerRequest { get; set; }

    [DataMember(Name = "AssignEmployerActivityRequest", IsRequired = false, Order = 22)]
    public AssignEmployerActivityRequest AssignEmployerActivityRequest { get; set; }

    [DataMember(Name = "AssignEmployerActivityRequest", IsRequired = false, Order = 23)]
    public AssignAdminToJobSeekerRequest AssignAdminToJobSeekerRequest { get; set; }

    [DataMember(Name = "SaveEmployeeRequest", IsRequired = false, Order = 24)]
    public SaveEmployeeRequest SaveEmployeeRequest { get; set; }

    [DataMember(Name = "SetApplicationStatusRequest", IsRequired = false, Order = 25)]
    public SetApplicationStatusRequest SetApplicationStatusRequest { get; set; }

    [DataMember(Name = "LogActionRequest", IsRequired = false, Order = 26)]
    public LogActionRequest LogActionRequest { get; set; }

    [DataMember(Name = "JobSeekerLoginRequest", IsRequired = false, Order = 27)]
    public JobSeekerLoginRequest JobSeekerLoginRequest { get; set; }

    [DataMember(Name = "UpdateEmployeeStatusRequest", IsRequired = false, Order = 28)]
    public UpdateEmployeeStatusRequest UpdateEmployeeStatusRequest { get; set; }

		[DataMember(Name = "AssignAdminToJobOrderRequest", IsRequired = false, Order = 29)]
		public AssignAdminToJobOrderRequest AssignAdminToJobOrderRequest { get; set; }

    [DataMember(Name = "DisableJobSeekersReportRequest", IsRequired = false, Order = 30)]
    public DisableJobSeekersReportRequest DisableJobSeekersReportRequest { get; set; }
  }
}
