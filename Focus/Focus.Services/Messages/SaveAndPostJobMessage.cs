﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Services.Messages
{
  public class SaveAndPostJobMessage : EventMessage
  {
    [DataMember(Name = "JobCreatedBy", IsRequired = true)]
    public long JobCreatedBy { get; set; }

    [DataMember(Name = "EmployeeId", IsRequired = true)]
    public long EmployeeId { get; set; }

    [DataMember(Name = "BusinessUnitId", IsRequired = true)]
    public long BusinessUnitId { get; set; }

    [DataMember(Name = "JobTitle", IsRequired = true)]
    public string JobTitle { get; set; }

    [DataMember(Name = "JobDescription", IsRequired = true)]
    public string JobDescription { get; set; }

    [DataMember(Name = "NumberOfOpenings", IsRequired = true)]
    public int? NumberOfOpenings { get; set; }

    [DataMember(Name = "ClosingOn", IsRequired = true)]
    public DateTime? ClosingOn { get; set; }

    [DataMember(Name = "PostalCode", IsRequired = true)]
    public string PostalCode { get; set; }

    [DataMember(Name = "UploadIndicator", IsRequired = true)]
    public bool UploadIndicator { get; set; }

    [DataMember(Name = "OnetId", IsRequired = true)]
    public long? OnetId { get; set; }

		[DataMember(Name = "ApplicantCriminalCheck", IsRequired = false)]
		public bool? ApplicantCriminalCheck { get; set; }

		[DataMember(Name = "ApplicantCreditCheck", IsRequired = false)]
		public bool? ApplicantCreditCheck { get; set; }

		[DataMember( Name = "EmploymentStatusId", IsRequired = false )]
		public long? EmploymentStatusId { get; set; }

		[DataMember( Name = "JobTypeId", IsRequired = false )]
		public long? JobTypeId { get; set; }
	}
}
