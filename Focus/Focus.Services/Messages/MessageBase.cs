﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using Focus.Core;
using Framework.Core;
using Framework.Messaging;

#endregion

namespace Focus.Services.Messages
{
	public class MessageBase : Message
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="MessageBase"/> class.
		/// </summary>
		public MessageBase()
		{
			Version = Constants.SystemVersion;
		}

		/// <summary>
		/// Gets the message bus user id.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <returns></returns>
		public long GetMessageBusUserId(IRuntimeContext runtimeContext)
		{
			var result = runtimeContext.Repositories.Core.Users.Where(x => x.UserName == runtimeContext.AppSettings.MessageBusUsername).Select(x => x.Id).FirstOrDefault();

			if(result.IsNull() || result == 0)
				throw new Exception("Message Bus user could not be found.");

			return result;
		}
	}
}
