﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Linq;
using Focus.Data.Core.Entities;
using Framework.Messaging;

#endregion

namespace Focus.Services.Messages.Handlers
{
	public class ApplicantHiredMessageHandler : MessageHandlerBase, IMessageHandler<ApplicantHiredMessage>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="ApplicantHiredMessageHandler"/> class.
		/// </summary>
		public ApplicantHiredMessageHandler() : base(null) {}

		/// <summary>
		/// Initializes a new instance of the <see cref="ApplicantHiredMessageHandler"/> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		public ApplicantHiredMessageHandler(IRuntimeContext runtimeContext) : base(runtimeContext) { }

		/// <summary>
		/// Handles the specified message.
		/// </summary>
		/// <param name="message">The message.</param>
		public void Handle(ApplicantHiredMessage message)
		{
			message.InitialiseRuntimeContext(RuntimeContext);
			Helpers.JobDevelopment.CleanPersonPostingMatches(0, message.PersonId);

			var application = Repositories.Core.FindById<Application>(message.ApplicationId);
			var resume = application.Resume;
			var posting = application.Posting;

			var recentlyPlaced = Repositories.Core.RecentlyPlaced.SingleOrDefault(x => x.JobId == posting.JobId && x.PersonId == message.PersonId);

			if (recentlyPlaced == null)
			{
				recentlyPlaced = new RecentlyPlaced
				{
					PlacementDate = application.UpdatedOn,
					Job = posting.Job,
					Person = resume.Person
				};

				Repositories.Core.Add(recentlyPlaced);
			}
			else
			{
				recentlyPlaced.PlacementDate = application.UpdatedOn;
			}
			Repositories.Core.SaveChanges();

			Helpers.JobDevelopment.RefreshMatchesToRecentlyPlacedJobSeeker(message.PersonId);

		}
	}
}
