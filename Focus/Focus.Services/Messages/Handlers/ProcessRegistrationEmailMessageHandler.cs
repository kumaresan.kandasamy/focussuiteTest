﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

using Focus.Core;
using Focus.Core.EmailTemplate;
using Focus.Data.Core.Entities;

using Framework.Core;
using Framework.Messaging;

#endregion

namespace Focus.Services.Messages.Handlers
{
	public class ProcessRegistrationPinEmailMessageHandler : MessageHandlerBase, IMessageHandler<ProcessRegistrationPinEmailMessage>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="ProcessRegistrationPinEmailMessageHandler"/> class.
		/// </summary>
		public ProcessRegistrationPinEmailMessageHandler() : base(null) { }

		/// <summary>
		/// Initializes a new instance of the <see cref="ProcessRegistrationPinEmailMessageHandler"/> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		public ProcessRegistrationPinEmailMessageHandler(IRuntimeContext runtimeContext) : base(runtimeContext) { }

		/// <summary>
		/// Handles the specified message.
		/// </summary>
		/// <param name="message">The message.</param>
		public void Handle(ProcessRegistrationPinEmailMessage message)
		{
			message.InitialiseRuntimeContext(RuntimeContext);

      // Convert all emails to lower case to ensure comparisons work
      message.Emails.ForEach(email =>
      {
        var childMessage = new ProcessRegistrationPinEmailChildMessage
        {
          ActionerId = message.ActionerId,
          Culture = message.Culture,
          Email = email,
          RegistrationUrl = message.RegistrationUrl,
          ResetPasswordUrl = message.ResetPasswordUrl,
          TargetModule = message.TargetModule
        };

        Helpers.Messaging.Publish(childMessage, MessagePriority.Low);
      });
		}
	}
}
