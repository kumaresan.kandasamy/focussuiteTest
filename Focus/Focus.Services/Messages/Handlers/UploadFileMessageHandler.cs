﻿#region Copyright © 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;

using Focus.Core;
using Focus.Core.Models.Upload;
using Focus.Data.Core.Entities;
using Focus.Services.Core.Censorship;
using Focus.Services.Mappers;
using CensorshipRule = Focus.Data.Configuration.Entities.CensorshipRule;

using Framework.Core;
using Framework.Messaging;

#endregion

namespace Focus.Services.Messages.Handlers
{
  public class UploadFileMessageHandler : MessageHandlerBase, IMessageHandler<UploadFileMessage>
  {
		private long? _defaultEmploymentStatusId;
		private long? _defaultJobTypeId;

		private long? DefaultEmploymentStatusId
		{
			get
			{
				if( !AppSettings.MandatoryHours || !AppSettings.MandatoryJobType )
				{
					return null;
				}
				return _defaultEmploymentStatusId ?? (_defaultEmploymentStatusId = Helpers.Lookup.GetLookup( LookupTypes.EmploymentStatuses ).First( li => li.Key == "EmploymentStatuses.Fulltime" ).Id);
			}
		}

		private long? DefaultJobTypeId
		{
			get
			{
				if( !AppSettings.MandatoryHours || !AppSettings.MandatoryJobType )
				{
					return null;
				}
				return _defaultJobTypeId ?? (_defaultJobTypeId = Helpers.Lookup.GetLookup( LookupTypes.JobTypes ).First( li => li.Key == "JobTypes.Permanent" ).Id);
			}
		}
		
		private CensorshipRuleList _censorshipRules;
    /// <summary>
    /// Initializes a new instance of the <see cref="UploadFileMessageHandler"/> class.
		/// </summary>
		public UploadFileMessageHandler() : base(null) {}

		/// <summary>
    /// Initializes a new instance of the <see cref="UploadFileMessageHandler"/> class.
		/// </summary>
    public UploadFileMessageHandler(IRuntimeContext runtimeContext) : base(runtimeContext) { }

    /// <summary>
    /// Handles the specified message.
    /// </summary>
    /// <param name="message">The message.</param>
    public void Handle(UploadFileMessage message)
    {
      message.InitialiseRuntimeContext(RuntimeContext);

      var uploadFile = Repositories.Core.FindById<UploadFile>(message.UploadFileId);

      switch (message.ProcessingState)
      {
        case UploadFileProcessingState.RequiresValidation:
          ValidateUploadFile(uploadFile, message);
          break;

        case UploadFileProcessingState.RequiresCommitting:
          CommitUploadFile(uploadFile);
          break;
      }
    }

    /// <summary>
    /// Validates the upload file
    /// </summary>
    /// <param name="uploadFile">The file to validate</param>
    /// <param name="message">The message requesting the file be validated.</param>
    private void ValidateUploadFile(UploadFile uploadFile, UploadFileMessage message)
    {
      if (uploadFile.ProcessingState != UploadFileProcessingState.RequiresValidation)
        return;

      var profanityRules = GetCensorshipRules(CensorshipType.Profanity);
			var criminalRules = new CensorshipRuleList();

	    var result = Helpers.Validation.ValidateFile(uploadFile.SchemaType, uploadFile.FileData, uploadFile.FileType, profanityRules, criminalRules, message.IgnoreHeader);

      uploadFile.Status = result.Status;
      uploadFile.ProcessingState = UploadFileProcessingState.Validated;

      result.RecordValidationResults.ForEach(recordResult =>
      {
        var customInformation = uploadFile.CustomInformation.DeserializeJson<Dictionary<string, string>>();
				((JobUploadRecord)recordResult.UploadRecord).EmploymentStatusId = DefaultEmploymentStatusId;
	      ((JobUploadRecord)recordResult.UploadRecord).JobTypeId = DefaultJobTypeId;
        UploadMapper.PopulateFields(recordResult.UploadRecord, uploadFile.UserId, recordResult.Status, customInformation, RuntimeContext);

        var uploadRecord = new UploadRecord
        {
          RecordNumber = recordResult.LineNumber,
          Status = recordResult.Status,
          ValidationMessage = recordResult.Message ?? string.Empty,
          FieldValidation = recordResult.FieldValidationResults.Serialize(),
          FieldData = recordResult.UploadRecord.Serialize()
        };
        uploadFile.UploadRecords.Add(uploadRecord);
      });

      Repositories.Core.SaveChanges(true);
    }

    /// <summary>
    /// Commits the upload file
    /// </summary>
    /// <param name="uploadFile">The file to commit</param>
    private void CommitUploadFile(UploadFile uploadFile)
    {
      if (uploadFile.ProcessingState != UploadFileProcessingState.RequiresCommitting)
        return;

      var recordsToPublish = new List<IUploadedRecord>();

      var uploadRecords = uploadFile.UploadRecords.Where(record => (record.Status & ValidationRecordStatus.SchemaFailure) != ValidationRecordStatus.SchemaFailure).ToList();
      uploadRecords.ForEach(record =>
      {
        var type = Type.GetType(uploadFile.RecordTypeName);
        var recordData = (IUploadedRecord)record.FieldData.Deserialize(type);

        recordsToPublish.Add(recordData);
      });

      uploadFile.ProcessingState = UploadFileProcessingState.Committed;
      Repositories.Core.SaveChanges(true);

      recordsToPublish.ForEach(recordData => UploadMapper.PublishMessage(recordData, RuntimeContext));
    }

    /// <summary>
    /// Gets the censorship rules.
    /// </summary>
    /// <param name="type">The type.</param>
    /// <returns>The rules</returns>
    private CensorshipRuleList GetCensorshipRules(CensorshipType type)
    {
      if (_censorshipRules.IsNull())
      {
        _censorshipRules = new CensorshipRuleList();
        _censorshipRules.AddRange(Repositories.Core.Query<CensorshipRule>().Select(censorshipRule => new Core.Censorship.CensorshipRule(censorshipRule)).ToList());
      }

      return _censorshipRules.Filter(type);
    }
  }
}
