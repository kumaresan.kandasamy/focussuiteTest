﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Focus.Core;
using Focus.Core.Criteria.CandidateSearch;
using Focus.Core.EmailTemplate;
using Focus.Core.IntegrationMessages;
using Focus.Core.Models.Career;
using Focus.Data.Core.Entities;
using Focus.Services.Core.Extensions;
using Framework.Core;
using Framework.Messaging;
using System.Globalization;

namespace Focus.Services.Messages.Handlers
{
    public class SendBlastEmailMessagesHandler : MessageHandlerBase, IMessageHandler<SendBlastEmailMessages>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ProcessSearchAlertMessageHandler"/> class.
        /// </summary>
        public SendBlastEmailMessagesHandler() : base(null) { }


        /// <summary>
        /// Initializes a new instance of the <see cref="ProcessSearchAlertMessageHandler"/> class.
        /// </summary>
        /// <param name="runtimeContext">The runtime context.</param>
        public SendBlastEmailMessagesHandler(IRuntimeContext runtimeContext) : base(runtimeContext) { }



        public void Handle(SendBlastEmailMessages message)
        {
            //Send Blast email to list of employers 
            if (message.IsEmployeeEmail)
            {
                foreach (var employeeId in message.EmployeesEmails.employeeIds)
                {
                    // Find the employee so we can get their email address
                    var employee = Repositories.Core.Employees.First(x => x.Id == employeeId);
                    var emailAddress = employee.Person.EmailAddress;

                    var bcc = message.EmployeesEmails.Bcc;

                    var footerUrl = string.Empty;

                    if (message.FooterType == EmailFooterTypes.HiringManagerUnsubscribe)
                    {
                        var encryptionModel = Helpers.Encryption.GenerateVector(TargetTypes.HiringManagerUnsubscribe, EntityTypes.Employee, employee.PersonId);
                        var encryptedPersonId = Helpers.Encryption.Encrypt(employee.PersonId.ToString(CultureInfo.InvariantCulture), encryptionModel.Vector, true);
                        footerUrl = string.Join("", message.FooterUrl, "?", "value=", encryptedPersonId, "&key=", encryptionModel.EncryptionId);
                    }

                    Helpers.Email.SendEmail(emailAddress, "", bcc, message.EmployeesEmails.Subject, message.EmployeesEmails.Body, senderAddress: message.EmployeesEmails.SenderAddress, footerType: message.FooterType, footerUrl: footerUrl.IsNotNullOrEmpty() ? footerUrl : null);
                    // Send message as push notification if person has mobile device
                    var devices = Repositories.Core.PersonMobileDevices.Where(x => x.PersonId == employee.PersonId).ToList();
                    if (devices.Any())
                    {
                        foreach (var device in devices.Where(x => x.Token != null))
                        {
                            Helpers.PushNotification.SendNotification(device.DeviceType, device.Token, device.Id, message.EmployeesEmails.Subject, message.EmployeesEmails.Body);
                        }
                    }

                    // Log the action
                    Helpers.Logging.LogAction(ActionTypes.EmailEmployee, typeof(Person).Name, employee.PersonId, true);
                }


            }
            else //Send Blast email to list of jobseekers 
            {
                message.InitialiseRuntimeContext(RuntimeContext);
                var bcc = "";
                var emailsSent = 0;
                foreach (var candidateEmail in message.Emails)
                {
                    var person = Repositories.Core.Persons.Where(x => x.Id == candidateEmail.PersonId)
                        .Select(x => new { x.EmailAddress, x.Unsubscribed })
                        .FirstOrDefault();

                    var devices = Repositories.Core.PersonMobileDevices.Where(x => x.PersonId == candidateEmail.PersonId).ToList();
                    var emailAddress = person.IsNotNull() ? person.EmailAddress : null;
                    var isUnsubcribed = person.IsNotNull() && person.Unsubscribed.GetValueOrDefault(false);

                    if (emailAddress.IsNotNullOrEmpty() && !(message.CheckSubscribed && isUnsubcribed))
                    {
                        emailsSent++;

                        var footerUrl = string.Empty;
                        candidateEmail.To = emailAddress;

                        if (message.FooterType == EmailFooterTypes.JobSeekerUnsubscribe)
                        {
                            var encryptionModel = Helpers.Encryption.GenerateVector(TargetTypes.JobSeekerUnsubscribe, EntityTypes.JobSeeker, candidateEmail.PersonId);
                            var encryptedPersonId = Helpers.Encryption.Encrypt(candidateEmail.PersonId.ToString(CultureInfo.InvariantCulture), encryptionModel.Vector, true);
                            footerUrl = string.Join("", message.FooterUrl, "?", "value=", encryptedPersonId, "&key=", encryptionModel.EncryptionId);
                        }

                        Helpers.Email.SendEmail(emailAddress, "", candidateEmail.BccRequestor ? bcc : "", candidateEmail.Subject, candidateEmail.Body, true, detectUrl: message.DetectUrl, senderAddress: candidateEmail.SenderAddress, footerType: message.FooterType, footerUrl: footerUrl.IsNotNullOrEmpty() ? footerUrl : null);

                        // Send message as push notification if person has mobile device
                        if (devices.Any())
                        {
                            foreach (var device in devices.Where(x => x.Token != null))
                            {
                                Helpers.PushNotification.SendNotification(device.DeviceType, device.Token, device.Id, candidateEmail.Subject, candidateEmail.Body);
                            }
                        }

                        if (!message.Emails[0].IsInviteToApply)
                        {
                            //Helpers.Logging.LogAction(message, ActionTypes.JobSeekerEmailed, typeof(Person).Name, candidateEmail.PersonId, message.UserContext.UserId);
                            // Helpers.Logging.LogAction(message, ActionTypes.JobSeekerEmailed, typeof(Person).Name, candidateEmail.PersonId, null);
                            Helpers.Logging.LogAction(ActionTypes.JobSeekerEmailed, typeof(Person).Name, candidateEmail.PersonId, true);
                        }
                    }
                }
            }
        }



    }
}
