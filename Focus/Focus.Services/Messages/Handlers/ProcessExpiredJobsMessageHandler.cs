﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

using Focus.Core;
using Focus.Core.EmailTemplate;
using Focus.Core.Views;
using Focus.Data.Configuration.Entities;
using Focus.Data.Core.Entities;
using Focus.Services.Core;
using Focus.Services.Mappers;
using Focus.Services.Views;

using Framework.Core;
using Framework.Logging;
using Framework.Messaging;

#endregion

namespace Focus.Services.Messages.Handlers
{
	public class ProcessExpiredJobsMessageHandler : MessageHandlerBase, IMessageHandler<ProcessExpiredJobsMessage>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="ProcessExpiredJobsMessageHandler"/> class.
		/// </summary>
		public ProcessExpiredJobsMessageHandler() : base(null) {}

		/// <summary>
		/// Initializes a new instance of the <see cref="ProcessExpiredJobsMessageHandler"/> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		public ProcessExpiredJobsMessageHandler(IRuntimeContext runtimeContext) : base(runtimeContext) {}

		private ProcessExpiredJobsMessage _message;

		/// <summary>
		/// Handles the specified message.
		/// </summary>
		/// <param name="message">The message.</param>
		public void Handle(ProcessExpiredJobsMessage message)
		{
			_message = message;

			_message.InitialiseRuntimeContext(RuntimeContext);

			var runTime = DateTime.Now;

      var processFailed = false;

			#region Check if job is running

			var runningSetting = Helpers.Configuration.GetConfigurationItem(Constants.ConfigurationItemKeys.ProcessExpiredJobsRunning, false.ToString());

			var processIsRunning = false;

			if (runningSetting.Value.IsNotNullOrEmpty())
			{
				try
				{
					processIsRunning = Convert.ToBoolean(runningSetting.Value);
				}
				catch { }
			}

			if (processIsRunning)
				throw new Exception("Process Expired Jobs is already running");

			// Set running flag to true
			runningSetting.Value = true.ToString();
			Repositories.Configuration.SaveChanges(true);

			#endregion

			#region Check if job has run in last 24 hours

			var lastRunSetting = Helpers.Configuration.GetConfigurationItem(Constants.ConfigurationItemKeys.ProcessExpiredJobsLastRun, DateTime.MinValue.ToShortDateString());

			var lastRunDate = DateTime.MinValue;

			if (lastRunSetting.Value.IsNotNullOrEmpty())
			{
				try
				{
					lastRunDate = Convert.ToDateTime(lastRunSetting.Value);
				}
				catch { }
			}

			if (lastRunDate > DateTime.Now.AddDays(0))
				throw new Exception("Process Expired Jobs has already run in the last 24 hours");

			#endregion

			try
			{
				Helpers.Logging.LogAction(ActionTypes.ProcessExpiredJobsStarted, String.Empty, null, true);

			  var expiryDate = DateTime.Now.Date.AddDays(0 - _message.ExpiryDays ?? -1);

				// get all expired jobs
				var expiredJobIds = Repositories.Core.Jobs.Where(x => x.ClosingOn != null && x.ClosingOn <= expiryDate).Where(y => y.JobStatus == JobStatuses.Active).Select(x => x.Id).ToList();

				//Loop through jobs
				foreach (var jobId in expiredJobIds)
				{
					CloseExpiredJob(jobId);
				}
			}
			catch (Exception ex)
			{
				Logger.Error(RuntimeContext.CurrentRequest.LogData(), "Unhandled Exception", ex);
				processFailed = true;
			}

			// Reset config settings (For some reason we need to reset the configuration setting)
			runningSetting = Repositories.Configuration.ConfigurationItems.FirstOrDefault(x => x.Key == Constants.ConfigurationItemKeys.ProcessExpiredJobsRunning);

			if (runningSetting.IsNull())
			{
				runningSetting = new ConfigurationItem { Key = Constants.ConfigurationItemKeys.ProcessExpiredJobsRunning };
				Repositories.Configuration.Add(runningSetting);
			}

			runningSetting.Value = false.ToString();

			if (!processFailed)
			{
				lastRunSetting = Repositories.Configuration.ConfigurationItems.FirstOrDefault(x => x.Key == Constants.ConfigurationItemKeys.ProcessExpiredJobsLastRun);

				if (lastRunSetting.IsNull())
				{
					lastRunSetting = new ConfigurationItem { Key = Constants.ConfigurationItemKeys.ProcessExpiredJobsLastRun };
					Repositories.Configuration.Add(lastRunSetting);
				}

				lastRunSetting.Value = runTime.AddMinutes(-5).ToString();
			}

			Repositories.Configuration.SaveChanges(true);

			Helpers.Logging.LogAction(ActionTypes.ProcessExpiredJobsCompleted, String.Empty, null, true);
		}

		/// <summary>
		/// Closes the expired job.
		/// </summary>
		/// <param name="jobId">The job identifier.</param>
		private void CloseExpiredJob(long jobId)
		{
			var processExpiredJobMessage = new CloseExpiredJobMessage
			{
				ActionerId = (RuntimeContext.CurrentRequest.IsNotNull() && RuntimeContext.CurrentRequest.UserContext.IsNotIn()) ? RuntimeContext.CurrentRequest.UserContext.ActionerId : 0,
				Culture = Constants.DefaultCulture,
				JobId = jobId
			};

			Helpers.Messaging.Publish(processExpiredJobMessage);
		}
	}
}
