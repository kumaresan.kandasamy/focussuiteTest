﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using Focus.Core;
using Framework.Messaging;

#endregion

namespace Focus.Services.Messages.Handlers
{
	public class ProcessRemindersMessageHandler : MessageHandlerBase, IMessageHandler<ProcessRemindersMessage>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="ProcessRemindersMessageHandler"/> class.
		/// </summary>
		public ProcessRemindersMessageHandler() : base(null) {}

		/// <summary>
		/// Initializes a new instance of the <see cref="ProcessRemindersMessageHandler"/> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		public ProcessRemindersMessageHandler(IRuntimeContext runtimeContext) : base(runtimeContext) {}

		/// <summary>
		/// Handles the specified message.
		/// </summary>
		/// <param name="message">The message.</param>
		public void Handle(ProcessRemindersMessage message)
		{
			message.InitialiseRuntimeContext(RuntimeContext);

			Helpers.Logging.LogAction(ActionTypes.SendRemindersProcessStarted, String.Empty, null, true);

			var remindersToSend = Repositories.Core.NoteReminders.Where(x => x.NoteReminderType == NoteReminderTypes.Reminder && x.ReminderDue <= DateTime.Today && x.ReminderSentOn == null).ToList();

			foreach (var reminderToSend in remindersToSend)
			{
				Helpers.Alerts.SendReminder(reminderToSend.Id);
			}

			Helpers.Logging.LogAction(ActionTypes.SendReminderProcessCompleted, String.Empty, null, true);
		}
	}
}
