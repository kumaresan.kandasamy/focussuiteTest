﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Focus.Data.Core.Entities;
using Framework.Core;
using Framework.Messaging;

using Aspose.Cells;

using Focus.Core;
using Focus.Core.Criteria.JobDistribution;
using Focus.Core.EmailTemplate;
using Focus.Core.Models.Report;

#endregion

namespace Focus.Services.Messages.Handlers
{
	public class GenerateReportHandler : MessageHandlerBase, IMessageHandler<GenerateReportMessage>
	{
    private class StyleInfo
    {
      public Style MainHeaderStyle { get; set; }
      public Style TableHeaderStyle { get; set; }
      public Style BodyStyle { get; set; }
      public StyleFlag StyleFlag { get; set; }
    }

    private class DisplayText
    {
      public string DetailedBreakdownHeaderText { get; set; }
      public string CompanyNameHeaderText { get; set; }
      public string RecruiterNameHeaderText { get; set; }
    }

		/// <summary>
		/// Initializes a new instance of the <see cref="GenerateReportHandler"/> class.
		/// </summary>
		public GenerateReportHandler() : base(null) { }

		/// <summary>
		/// Initializes a new instance of the <see cref="GenerateReportHandler"/> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		public GenerateReportHandler(IRuntimeContext runtimeContext) : base(runtimeContext) { }

		/// <summary>
		/// Handles the specified message.
		/// </summary>
		/// <param name="message">The message.</param>
		public void Handle(GenerateReportMessage message)
		{
			message.InitialiseRuntimeContext(RuntimeContext);

			switch (message.ReportType)
			{
			  case AsyncReportType.EmployerActivity:
          ProcessEmployerActivityReport(message);
			    break;
        case AsyncReportType.StudentActivity:
          ProcessStudentActivityReport(message);
			    break;
			}
		}

    #region Employer Activity

    /// <summary>
		/// Processes the employer activity report.
		/// </summary>
		/// <param name="message">The message.</param>
		public void ProcessEmployerActivityReport(GenerateReportMessage message)
		{
			// Manipluate the Report To Date to have the time at the end of the day so the range is inclusive
			var endTs = new TimeSpan(23, 59, 59);
			message.ReportToDate = message.ReportToDate.Date + endTs;

			// Get the employer's (hiring manager's account activity)
			var employerActivityReportModel = EmployerAccountActivity(message);

			if (employerActivityReportModel.IsNull())
				employerActivityReportModel = new EmployerActivityReportModel();

			employerActivityReportModel.ReportFromDate = message.ReportFromDate;
			employerActivityReportModel.ReportToDate = message.ReportToDate;

			// Get List of Talent jobs posted
			employerActivityReportModel.TalentJobsPosted = GetEmployerJobActivity(message);

			// Get count of spidered jobs from Live Jobs Feed
			employerActivityReportModel.SpideredJobCount = GetTotalActiveSpideredJobs();

			// Create Excel spreadsheet
			var workbook = CreateEmployerAccountActivityExcelReport(employerActivityReportModel);

			#region Create emails - create email messages including the Excel attachment

			// Create a new memory stream.
			var outStream = new MemoryStream();

			// Save the document to stream.
			workbook.Save(outStream, SaveFormat.Xlsx);

			// Convert the workbook to byte form.
			var reportBytes = outStream.ToArray();

			// Send the emails to any inactive job seekers that have been identified
			foreach (var email in message.Emails)
			{
				var template = new EmailTemplateData { ReportFromDate = message.ReportFromDate.ToShortDateString(), ReportToDate = message.ReportToDate.ToShortDateString() };
				var preview = Helpers.Email.GetEmailTemplatePreview(EmailTemplateTypes.TalentEmployerActivityReport, template);

				Helpers.Email.SendEmail(email, "", "", preview.Subject, preview.Body, sendAsync: true, attachment: reportBytes, attachmentName: "Employer activity report.xlsx");
			}

			#endregion
		}

    /// <summary>
		/// Employers the account activity.
		/// </summary>
		/// <param name="message">The message.</param>
		/// <returns></returns>
		private EmployerActivityReportModel EmployerAccountActivity(GenerateReportMessage message)
		{
      var employerActivityReportModel = new EmployerActivityReportModel();

      var employerAccountActivity = Repositories.Core.EmployerAccountActivityViews.Where(x => x.ActivityCreatedOn >= message.ReportFromDate && x.ActivityCreatedOn <= message.ReportToDate).OrderByDescending(x => x.ActivityType).ToList();

			employerActivityReportModel.EmployeeLogIns = employerAccountActivity.Where(x => x.ActivityType.Equals("LogIn")).Select(x => new EmployerLogInsModel
				                                                                                                                            {
					                                                                                                                            CompanyName = x.BusinessUnitName,
					                                                                                                                            EmployeeName = x.EmployeeName,
					                                                                                                                            EmployeeEmailAddress = x.EmployeeEmail
				                                                                                                                            }).ToList();


			employerActivityReportModel.TalentRegistrations = employerAccountActivity.Where(x => x.ActivityType.Equals("RegisterAccount")).Select(x => new TalentRegistrationsModel
				                                                                                                                                           {
					                                                                                                                                           CompanyName = x.BusinessUnitName,
					                                                                                                                                           EmployeeName = x.EmployeeName,
																																																																										 AccountStatus = Helpers.Localisation.GetEnumLocalisedText(x.EmployeeApprovalStatus, true)
				                                                                                                                                           }).ToList();

			employerActivityReportModel.CandidateSearch = employerAccountActivity.Where(x => x.ActivityType.Equals("SearchCandidates")).Select(x => new CandidateSearchModel
				                                                                                                                             {
					                                                                                                                             CompanyName = x.BusinessUnitName,
					                                                                                                                             EmployeeName = x.EmployeeName
				                                                                                                                             }).ToList();

			return employerActivityReportModel;
		}

		/// <summary>
		/// Gets the employer job activity.
		/// </summary>
		/// <param name="message">The message.</param>
		/// <returns></returns>
		private List<TalentJobsPostedModel> GetEmployerJobActivity(GenerateReportMessage message)
		{
			// The employer activity report is only to be available for EDU therefore the following linq joins via rOnet, this would need to be altered if we needed this for WF too
			var startTs = new TimeSpan(00, 00, 00);
			var fromDateTime = message.ReportFromDate.Date + startTs;

			var endTs = new TimeSpan(23, 59, 59);
			var toDateTime = message.ReportToDate.Date + endTs;

			var talentJobsPosted = (from j in Repositories.Core.Jobs
															join u in Repositories.Core.Users on j.PostedBy equals u.Id
															where j.PostedOn >= fromDateTime
																&& j.PostedOn <= toDateTime
																&& j.JobStatus.Equals(JobStatuses.Active)
															select new TalentJobsPostedModel
															{
																JobTitle = j.JobTitle,
																CompanyName = j.BusinessUnit.Name,
																PostedBy = string.Concat(u.Person.FirstName, " ", u.Person.LastName),
																ROnetId = j.ROnetId
															}).ToList();

				talentJobsPosted.ForEach(job => job.ROnetJobTitle = job.ROnetId != null && job.ROnetId > 0 ? Helpers.Occupation.GetROnetById(job.ROnetId.Value, message.Culture).Occupation : string.Empty);

			return talentJobsPosted;
		}

		/// <summary>
		/// Gets the total active spidered jobs.
		/// </summary>
		/// <returns></returns>
		private int GetTotalActiveSpideredJobs()
		{
			var criteria = new JobDistributionCriteria
			{
				Category = "Active"
			};

			var counts = Repositories.LiveJobs.GetJobDistribution(criteria);

			var feeds = AppSettings.JobDistributionFeeds.Split(',');

			return counts.Where(count => feeds.Contains(count.FeedName, StringComparer.OrdinalIgnoreCase))
									 .Sum(count => count.Count);
		}

		/// <summary>
		/// Creates the employer account activity excel report.
		/// </summary>
		/// <param name="model">The model.</param>
		private Workbook CreateEmployerAccountActivityExcelReport(EmployerActivityReportModel model)
		{
			var license = new License();
			license.SetLicense("Aspose.Cells.lic");

      var styleInfo = new StyleInfo();
      
      //Instantiating a Workbook object
			var workbook = new Workbook();

			// Define the styling for the detailed table headers
			styleInfo.TableHeaderStyle = new Style();
      styleInfo.TableHeaderStyle.Font.Size = 12;

			// Define the styling for the detailed table headers
      styleInfo.MainHeaderStyle = new Style();
      styleInfo.MainHeaderStyle.Font.Size = 14;

      styleInfo.StyleFlag = new StyleFlag { Font = true };
      styleInfo.StyleFlag.Font = true;

			// Headers common to more than one worksheet
		  var displayText = new DisplayText
      {
	  		DetailedBreakdownHeaderText = Helpers.Localisation.Localise("DetailedBreakdownHeader.Text", "Detailed breakdown"),
		  	CompanyNameHeaderText = Helpers.Localisation.Localise("CompanyNameHeader.Text", "Company name"),
			  RecruiterNameHeaderText = Helpers.Localisation.Localise("RecruiterNameHeader.Text", "Recruiter name")
		  };

			#region Visits worksheet

      CreateVisitsReport(workbook, styleInfo, displayText, model);

			#endregion

			#region Jobs Posted worksheet

      CreateJobsPostedReport(workbook, styleInfo, displayText, model);

			#endregion

			#region Recruiter registrations worksheet

      CreateRecruiterRegistrationsReport(workbook, styleInfo, displayText, model);

			#endregion

			#region resume searches worksheet

      CreateResumeSearchesReport(workbook, styleInfo, displayText, model);

			#endregion

			//Saving the Excel file -  LEAVING HERE FOR FURTURE DEBUGGING PURPOSES
			//_workbook.Save("C:\\temp\\output.xls");

		  return workbook;
		}

    /// <summary>
    /// Creates the employer account activity report worksheet.
    /// </summary>
    /// <param name="workbook">The workbook.</param>
    /// <param name="styleInfo">The style information.</param>
    /// <param name="worksheetName">Name of the worksheet.</param>
    /// <param name="model">The model.</param>
    /// <returns></returns>
		private Worksheet CreateEmployerAccountActivityReportWorksheet(Workbook workbook, StyleInfo styleInfo, string worksheetName, EmployerActivityReportModel model)
		{
			var sheetIndex = 0;

			// Add a new worksheet to the Workbook object - the workbook is created with a worksheet therefore don't need to create one for the 1st report
			if (workbook.Worksheets.Count >= 1 && workbook.Worksheets[0].Name == "Visits")
				sheetIndex = workbook.Worksheets.Add();

			// Get reference to the new worksheet
			var worksheet = workbook.Worksheets[sheetIndex];

			// Set the name of the worksheet
			worksheet.Name = worksheetName;

			// Reporting period text to be used on all worksheets
			var reportingPeriod = Helpers.Localisation.Localise("ReportingPeriod.Text", string.Concat("Reporting Period; ", model.ReportFromDate.ToShortDateString(), " to ", model.ReportToDate.ToShortDateString()));

			// Add reporting period header
			worksheet.Cells["A1"].PutValue(reportingPeriod);

			// Apply styling
			var range = worksheet.Cells.CreateRange("A1");
      range.ApplyStyle(styleInfo.MainHeaderStyle, styleInfo.StyleFlag);

			return worksheet;
		}

		#region Report worksheets

    /// <summary>
    /// Creates the visits report.
    /// </summary>
    /// <param name="workbook">The workbook.</param>
    /// <param name="styleInfo">The style information.</param>
    /// <param name="displayText">The display text.</param>
    /// <param name="model">The model.</param>
    private void CreateVisitsReport(Workbook workbook, StyleInfo styleInfo, DisplayText displayText, EmployerActivityReportModel model)
		{
			var visitsWorksheet = CreateEmployerAccountActivityReportWorksheet(workbook, styleInfo, "Visits", model);

			#region Total number of recruiter logins

			var totalLoginsText = Helpers.Localisation.Localise("TotalLogins.Text", "Total number of recruiter logins");
			visitsWorksheet.Cells["A3"].PutValue(totalLoginsText);
			visitsWorksheet.Cells["B3"].PutValue(model.EmployeeLogIns.Count);

			#endregion

			#region Detailed breakdown

			visitsWorksheet.Cells["A5"].PutValue(displayText.DetailedBreakdownHeaderText);

			// Apply styling
			var range = visitsWorksheet.Cells.CreateRange("A5");
			range.ApplyStyle(styleInfo.MainHeaderStyle, styleInfo.StyleFlag);

			var emailAddressHeaderText = Helpers.Localisation.Localise("EmailAddressHeader.Text", "Email address");

			visitsWorksheet.Cells["A6"].PutValue(displayText.CompanyNameHeaderText);
			visitsWorksheet.Cells["B6"].PutValue(displayText.RecruiterNameHeaderText);
			visitsWorksheet.Cells["C6"].PutValue(emailAddressHeaderText);

			// Apply styling
			range = visitsWorksheet.Cells.CreateRange("A6", "C6");
			range.ApplyStyle(styleInfo.TableHeaderStyle, styleInfo.StyleFlag);

			// The date format is hardcoded here because it's not needed, as soon as it is then it needs to be localised
			visitsWorksheet.Cells.ImportCustomObjects(model.EmployeeLogIns,
																								new[] { "CompanyName", "EmployeeName", "EmployeeEmailAddress" },
																								false,
																								6,
																								0,
																								model.EmployeeLogIns.Count,
																								true,
																								"mm/dd/yyyy",
																								false);

			#endregion
		}

    /// <summary>
    /// Creates the jobs posted report.
    /// </summary>
    /// <param name="workbook">The workbook.</param>
    /// <param name="styleInfo">The style information.</param>
    /// <param name="displayText">The display text.</param>
    /// <param name="model">The model.</param>
    private void CreateJobsPostedReport(Workbook workbook, StyleInfo styleInfo, DisplayText displayText, EmployerActivityReportModel model)
		{
			var jobsWorksheet = CreateEmployerAccountActivityReportWorksheet(workbook, styleInfo, "Jobs Posted", model);

			#region Total numbers of jobs posted

			var totalTalentJobsText = Helpers.Localisation.Localise("TotalTalentJobs.Text", "Total number of Talent jobs posted");
			jobsWorksheet.Cells["A3"].PutValue(totalTalentJobsText);
			jobsWorksheet.Cells["B3"].PutValue(model.TalentJobsPosted.Count);

			var totalSpideredJobsText = Helpers.Localisation.Localise("TotalSpideredJobs.Text", "Total number of spidered jobs as at reporting date");
			jobsWorksheet.Cells["A4"].PutValue(totalSpideredJobsText);
			jobsWorksheet.Cells["B4"].PutValue(model.SpideredJobCount);

			#endregion

			#region Detailed breakdown

			jobsWorksheet.Cells["A6"].PutValue(Helpers.Localisation.Localise("JobsPostedDetailedBreakdownHeader.Text", string.Concat(displayText.DetailedBreakdownHeaderText, " ", "(Talent jobs only)")));

			// Apply styling
			var range = jobsWorksheet.Cells.CreateRange("A6");
			range.ApplyStyle(styleInfo.MainHeaderStyle, styleInfo.StyleFlag);

			var jobTitleHeaderText = Helpers.Localisation.Localise("JobTitleHeader.Text", "Job title");
			var rOnetJobTitleHeaderText = Helpers.Localisation.Localise("ROnetJobTitleHeader.Text", "rOnet job title");
			var postedByHeaderText = Helpers.Localisation.Localise("PostedByHeader.Text", "Posted by");

			jobsWorksheet.Cells["A7"].PutValue(jobTitleHeaderText);
			jobsWorksheet.Cells["B7"].PutValue(rOnetJobTitleHeaderText);
			jobsWorksheet.Cells["C7"].PutValue(displayText.CompanyNameHeaderText);
			jobsWorksheet.Cells["D7"].PutValue(postedByHeaderText);

			// Apply styling
			range = jobsWorksheet.Cells.CreateRange("A7", "D7");
			range.ApplyStyle(styleInfo.TableHeaderStyle, styleInfo.StyleFlag);

			jobsWorksheet.Cells.ImportCustomObjects(model.TalentJobsPosted,
																								new[] { "JobTitle", "ROnetJobTitle", "CompanyName", "PostedBy" },
																								false,
																								7,
																								0,
																								model.TalentJobsPosted.Count,
																								true,
																								"mm/dd/yyyy",
																								false);

			#endregion
		}

    /// <summary>
    /// Creates the recruiter registrations report.
    /// </summary>
    /// <param name="workbook">The workbook.</param>
    /// <param name="styleInfo">The style information.</param>
    /// <param name="displayText">The display text.</param>
    /// <param name="model">The model.</param>
    private void CreateRecruiterRegistrationsReport(Workbook workbook, StyleInfo styleInfo, DisplayText displayText, EmployerActivityReportModel model)
		{
			var registrationsWorksheet = CreateEmployerAccountActivityReportWorksheet(workbook, styleInfo, "Recruiter Registrations", model);

			#region Total number of recruiter registrations

			var totalRegistrationsText = Helpers.Localisation.Localise("TotalRegistrations.Text", "Total number of recruiter registrations");
			registrationsWorksheet.Cells["A3"].PutValue(totalRegistrationsText);
			registrationsWorksheet.Cells["B3"].PutValue(model.TalentRegistrations.Count);

			#endregion

			#region Detailed breakdown

			registrationsWorksheet.Cells["A5"].PutValue(displayText.DetailedBreakdownHeaderText);

			// Apply styling
			var range = registrationsWorksheet.Cells.CreateRange("A5");
			range.ApplyStyle(styleInfo.MainHeaderStyle, styleInfo.StyleFlag);

			var accountStatusHeaderText = Helpers.Localisation.Localise("AccountStatusHeader.Text", "Account status");

			registrationsWorksheet.Cells["A6"].PutValue(displayText.CompanyNameHeaderText);
			registrationsWorksheet.Cells["B6"].PutValue(displayText.RecruiterNameHeaderText);
			registrationsWorksheet.Cells["C6"].PutValue(accountStatusHeaderText);

			// Apply styling
			range = registrationsWorksheet.Cells.CreateRange("A6", "C6");
			range.ApplyStyle(styleInfo.TableHeaderStyle, styleInfo.StyleFlag);

			registrationsWorksheet.Cells.ImportCustomObjects(model.TalentRegistrations,
																								new[] { "CompanyName", "EmployeeName", "AccountStatus" },
																								false,
																								6,
																								0,
																								model.TalentRegistrations.Count,
																								true,
																								"mm/dd/yyyy",
																								false);

			#endregion
		}

    /// <summary>
    /// Creates the resume searches report.
    /// </summary>
    /// <param name="workbook">The workbook.</param>
    /// <param name="styleInfo">The style information.</param>
    /// <param name="displayText">The display text.</param>
    /// <param name="model">The model.</param>
    private void CreateResumeSearchesReport(Workbook workbook, StyleInfo styleInfo, DisplayText displayText, EmployerActivityReportModel model)
		{
			var resumeSearchesWorksheet = CreateEmployerAccountActivityReportWorksheet(workbook, styleInfo, "Resume Searches", model);

			#region Total number of Resume Searches

			var totalResumeSearchesText = Helpers.Localisation.Localise("TotalResumeSearches.Text", "Total number of resume searches run");
			resumeSearchesWorksheet.Cells["A3"].PutValue(totalResumeSearchesText);
			resumeSearchesWorksheet.Cells["B3"].PutValue(model.CandidateSearch.Count);

			#endregion

			#region Detailed breakdown

			resumeSearchesWorksheet.Cells["A5"].PutValue(displayText.DetailedBreakdownHeaderText);

			// Apply styling
			var range = resumeSearchesWorksheet.Cells.CreateRange("A5");
			range.ApplyStyle(styleInfo.MainHeaderStyle, styleInfo.StyleFlag);

			resumeSearchesWorksheet.Cells["A6"].PutValue(displayText.CompanyNameHeaderText);
			resumeSearchesWorksheet.Cells["B6"].PutValue(displayText.RecruiterNameHeaderText);

			// Apply styling
			range = resumeSearchesWorksheet.Cells.CreateRange("A6", "B6");
			range.ApplyStyle(styleInfo.TableHeaderStyle, styleInfo.StyleFlag);

			resumeSearchesWorksheet.Cells.ImportCustomObjects(model.CandidateSearch,
																								new[] { "CompanyName", "EmployeeName" },
																								false,
																								6,
																								0,
																								model.CandidateSearch.Count,
																								true,
																								"mm/dd/yyyy",
																								false);

			#endregion
		}

		#endregion

    #endregion

    #region Student Activity Report

    /// <summary>
    /// Processes the student activity report.
    /// </summary>
    /// <param name="message">The message.</param>
    public void ProcessStudentActivityReport(GenerateReportMessage message)
    {
      // Manipluate the Report To Date to have the time at the end of the day so the range is inclusive
      var endTs = new TimeSpan(23, 59, 59);
      message.ReportToDate = message.ReportToDate.Date + endTs;

      // Get the report details
      var totals = Repositories.Core.StudentActivityReport(message.ReportFromDate, message.ReportToDate).ToList();
      var emails = Repositories.Core.RegistrationPins.Select(p => p.EmailAddress).ToList();

      // Create an excel work book and convert it to a byte array
      var workbook = CreateStudentAccountActivityExcelReport(message.ReportFromDate, message.ReportToDate, totals, emails);
      var outStream = new MemoryStream();
      workbook.Save(outStream, SaveFormat.Xlsx);
      var reportBytes = outStream.ToArray();

      // Send the report to the emails specified
      foreach (var email in message.Emails)
      {
        var template = new EmailTemplateData { ReportFromDate = message.ReportFromDate.ToShortDateString(), ReportToDate = message.ReportToDate.ToShortDateString() };
        var preview = Helpers.Email.GetEmailTemplatePreview(EmailTemplateTypes.StudentActivityReport, template);

        Helpers.Email.SendEmail(email, "", "", preview.Subject, preview.Body, sendAsync: true, attachment: reportBytes, attachmentName: "StudentActivityReport.xlsx");
      }
    }

    /// <summary>
    /// Creates the student account activity excel report.
    /// </summary>
    /// <param name="fromDate">From date.</param>
    /// <param name="toDate">To date.</param>
    /// <param name="totals">The activity totals.</param>
    /// <param name="emails">Unregistered emails.</param>
    /// <returns></returns>
    private Workbook CreateStudentAccountActivityExcelReport(DateTime fromDate, DateTime toDate, IEnumerable<StudentActivityReportView> totals, List<string> emails)
	  {
	    var license = new License();
	    license.SetLicense("Aspose.Cells.lic");

      var styleInfo = new StyleInfo();

      //Instantiating a Workbook object
      var workbook = new Workbook();

      // Define the styling for the sheets
      styleInfo.TableHeaderStyle = new Style();
      styleInfo.TableHeaderStyle.Font.Size = 11;
      styleInfo.TableHeaderStyle.Font.Name = "Calibri";
      styleInfo.TableHeaderStyle.Font.IsBold = true;

      styleInfo.MainHeaderStyle = new Style();
      styleInfo.TableHeaderStyle.Font.Size = 10;
      styleInfo.TableHeaderStyle.Font.Name = "Calibri";
      styleInfo.MainHeaderStyle.Font.IsBold = true;

      styleInfo.BodyStyle = new Style();
      styleInfo.TableHeaderStyle.Font.Size = 10;
      styleInfo.TableHeaderStyle.Font.Name = "Calibri";

      styleInfo.StyleFlag = new StyleFlag { Font = true };
      styleInfo.StyleFlag.Font = true;

      CreateStudentActivityTotalsWorksheet(workbook, styleInfo, fromDate, toDate, totals);
      CreateStudentActivityEmailsWorksheet(workbook, styleInfo, emails);

	    return workbook;
	  }

    private void CreateStudentActivityTotalsWorksheet(Workbook workbook, StyleInfo styleInfo, DateTime fromDate, DateTime toDate, IEnumerable<StudentActivityReportView> totals)
    {
      var sheetName = Helpers.Localisation.Localise("StudentActivity.Activity.Text", "Activity");

      var worksheet = workbook.Worksheets[0];
      worksheet.Name = sheetName;

      // Add reporting period header
      worksheet.Cells["A1"].PutValue(sheetName);
      worksheet.Cells["B1"].PutValue(Helpers.Localisation.Localise("StudentActivity.SinceInception.Text", "Since Inception"));
      worksheet.Cells["C1"].PutValue(Helpers.Localisation.Localise("StudentActivity.ReportingPeriod.Text", "Reporting Period"));
      worksheet.Cells["C2"].PutValue(Helpers.Localisation.Localise("StudentActivity.ReportingPeriodDates.Text", "{0} to {1}", fromDate.ToShortDateString(), toDate.ToShortDateString()));

      // Apply styling
      var range = worksheet.Cells.CreateRange("A1", "C2");
      range.ApplyStyle(styleInfo.MainHeaderStyle, styleInfo.StyleFlag);

      var rowCount = 0;
      foreach (var total in totals)
      {
        rowCount++;

        worksheet.Cells[rowCount + 1, 0].PutValue(total.Activity);
        worksheet.Cells[rowCount + 1, 1].PutValue(total.SinceInception);
        worksheet.Cells[rowCount + 1, 2].PutValue(total.ThisPeriod);
      }

      range = worksheet.Cells.CreateRange(2, 0, rowCount, 1);
      range.ApplyStyle(styleInfo.TableHeaderStyle, styleInfo.StyleFlag);

      range = worksheet.Cells.CreateRange(2, 1, rowCount, 2);
      range.ApplyStyle(styleInfo.BodyStyle, styleInfo.StyleFlag);

      worksheet.AutoFitColumns();
    }

    private void CreateStudentActivityEmailsWorksheet(Workbook workbook, StyleInfo styleInfo, IEnumerable<string> emails)
    {
      var sheetName = Helpers.Localisation.Localise("StudentActivity.Invitees.Text", "Invitees");

      var sheetIndex = workbook.Worksheets.Add();
      var worksheet = workbook.Worksheets[sheetIndex];
      worksheet.Name = sheetName;

      // Add reporting header
      worksheet.Cells["A1"].PutValue(Helpers.Localisation.Localise("StudentActivity.EmailAddress.Text", "Email addresses of invitees who haven't yet registered"));

      // Apply styling
      var range = worksheet.Cells.CreateRange("A1");
      range.ApplyStyle(styleInfo.MainHeaderStyle, styleInfo.StyleFlag);

      var rowCount = 0;
      foreach (var email in emails)
      {
        rowCount++;
        worksheet.Cells[rowCount + 1, 0].PutValue(email);
      }

      range = worksheet.Cells.CreateRange(2, 0, rowCount, 1);
      range.ApplyStyle(styleInfo.BodyStyle, styleInfo.StyleFlag);

      worksheet.AutoFitColumns();
    }

	  #endregion
  }
}
