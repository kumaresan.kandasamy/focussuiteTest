﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;

using Focus.Core;

using Framework.Messaging;

#endregion

namespace Focus.Services.Messages.Handlers
{
  public class ProcessSearchAlertsMessageHandler : MessageHandlerBase, IMessageHandler<ProcessSearchAlertsMessage>
  {
    /// <summary>
    /// Initializes a new instance of the <see cref="ProcessSearchAlertsMessageHandler"/> class.
    /// </summary>
    public ProcessSearchAlertsMessageHandler() : base(null) { }

    /// <summary>
    /// Initializes a new instance of the <see cref="ProcessSearchAlertsMessageHandler"/> class.
    /// </summary>
    /// <param name="runtimeContext">The runtime context.</param>
    public ProcessSearchAlertsMessageHandler(IRuntimeContext runtimeContext) : base(runtimeContext) { }

		/// <summary>
    /// Handles the specified message.
    /// </summary>
    /// <param name="message">The message.</param>
    public void Handle(ProcessSearchAlertsMessage message)
    {
	    message.InitialiseRuntimeContext(RuntimeContext);

      Helpers.Logging.LogAction(ActionTypes.ProcessSearchAlertsStarted, String.Empty, null, true);

      var alertsToSend = Repositories.Core.ActiveSearchAlertViews.Where(x => (x.Type == SavedSearchTypes.CareerPostingSearch || x.Type == SavedSearchTypes.TalentCandidateSearch || x.Type == SavedSearchTypes.AssistCandidateSearch)
                                                                          && (x.AlertEmailScheduledOn == null || x.AlertEmailScheduledOn < DateTime.Now)).Select(x => x.Id).ToList();


			foreach (var alertId in alertsToSend)
			{
				ProcessSearchAlert(alertId);
			}

	    Helpers.Logging.LogAction(ActionTypes.ProcessSearchAlertsCompleted, String.Empty, null, true);
    }

		/// <summary>
		/// Processes the search alert.
		/// </summary>
		/// <param name="searchAlertId">The search alert identifier.</param>
	  private void ProcessSearchAlert(long searchAlertId)
	  {
		  var message = new ProcessSearchAlertMessage
		  {
			  SearchAlertId = searchAlertId,
		  };

			Helpers.Messaging.Publish(message, MessagePriority.Low);
	  }
  }
}
