﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using Focus.Core;
using Focus.Core.EmailTemplate;
using Focus.Data.Core.Entities;
using Framework.Core;
using Framework.Messaging;

#endregion

namespace Focus.Services.Messages.Handlers
{
	public class ProcessQueuedInvitesAndRecommendations : MessageHandlerBase, IMessageHandler<ProcessQueuedInvitesAndRecommendationsMessage>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="ProcessQueuedInvitesAndRecommendations"/> class.
		/// </summary>
		public ProcessQueuedInvitesAndRecommendations() : base(null) {}

		/// <summary>
		/// Initializes a new instance of the <see cref="ProcessQueuedInvitesAndRecommendations"/> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		public ProcessQueuedInvitesAndRecommendations(IRuntimeContext runtimeContext) : base(runtimeContext) { }

		/// <summary>
		/// Handles the specified message.
		/// </summary>
		/// <param name="message">The message.</param>
		public void Handle(ProcessQueuedInvitesAndRecommendationsMessage message)
		{
		  message.InitialiseRuntimeContext(RuntimeContext);

      // Mark the job as running and update the logs
      MarkJobAsRunning(Constants.ConfigurationItemKeys.ProcessQueuedInvitesAndRecommendationsRunning);
      Helpers.Logging.LogAction(ActionTypes.ProcessQueuedInvitesToApplyStarted, String.Empty, null, true);

      // Run the Queued Invitates to Apply for Jobs
		  ProcessQueuedInvitations();

      // Run the Queued Recommendations to Apply for Jobs
		  ProcessQueuedRecommendations();

      // Mark the job as complete and update the logs
      ResetConfigSettings(Constants.ConfigurationItemKeys.ProcessQueuedInvitesAndRecommendationsRunning, Constants.ConfigurationItemKeys.ProcessQueuedInvitesAndRecommendationsLastRun);
      Helpers.Logging.LogAction(ActionTypes.ProcessQueuedInvitesToApplyCompleted, String.Empty, null, true);
		}

    /// <summary>
    /// Marks the job as running in the database.
    /// </summary>
    /// <param name="runningKey">The job key to look up in the ConfigurationItem table to see if the job is running.</param>
    /// <returns>True to indicate job has been marked as running, false if it was already running</returns>
    private bool MarkJobAsRunning(string runningKey)
    {
      var processIsRunning = false;

      var runningSetting = Helpers.Configuration.GetConfigurationItem(runningKey, "false");

      if (runningSetting.Value.IsNotNullOrEmpty())
        Boolean.TryParse(runningSetting.Value, out processIsRunning);

      // Set running flag to true
      runningSetting.Value = true.ToString();
      Repositories.Configuration.SaveChanges(true);

      return processIsRunning;
    }

    /// <summary>
    /// Resets the configuration item settings to say a job has completed.
    /// </summary>
    /// <param name="runningKey">The key used to indicate the job is running.</param>
    /// <param name="lastRunKey">The key used to indicate when the job last run.</param>
    private void ResetConfigSettings(string runningKey, string lastRunKey)
    {
      var runningSetting = Helpers.Configuration.GetConfigurationItem(runningKey, "false");
      var lastRunSetting = Helpers.Configuration.GetConfigurationItem(lastRunKey, DateTime.MinValue.ToLongDateString());
      runningSetting.Value = "false";
      lastRunSetting.Value = DateTime.Now.ToString();
      Repositories.Configuration.SaveChanges(true);
    }

    /// <summary>
    /// Generates the Career job link.
    /// </summary>
    /// <returns></returns>
    public string CareerInviteToJobPosting(string lensPostingId)
    {
      string path;

      if (!string.IsNullOrEmpty(AppSettings.ClientApplyToPostingUrl))
      {
        path = AppSettings.ClientApplyToPostingUrl.Replace(Constants.PlaceHolders.LensJobId,
          lensPostingId ?? string.Empty);
      }
      else
      {
        var careerPath = AppSettings.CareerApplicationPath.EndsWith("/")
          ? AppSettings.CareerApplicationPath
          : string.Concat(AppSettings.CareerApplicationPath, "/");

        path = string.IsNullOrWhiteSpace(lensPostingId)
          ? ""
          : string.Format("{0}jobposting/{1}/invite/email", careerPath, lensPostingId);
      }

      return path;
    }

    /// <summary>
    /// Process queued Job Invitations.
    /// </summary>
    /// <returns></returns>
	  public void ProcessQueuedInvitations()
	  {
      var recordsProcessed = false;

	    // Get all queued Invitations
			var queuedInvites = (from invite in Repositories.Core.Query<InviteToApply>()
													join job in Repositories.Core.Jobs
														on invite.JobId equals job.Id
													where invite.QueueInvitation == true
														&& (job.VeteranPriorityEndDate == null || DateTime.Now > job.VeteranPriorityEndDate)
													select invite).ToList();

      // Loop through the queued invitations 
	    foreach (var queuedInvite in queuedInvites)
	    {
	      var jobId = queuedInvite.JobId;
	      var jobView = Repositories.Core.FindById<JobView>(jobId);
	      var jobLinkUrl = CareerInviteToJobPosting(queuedInvite.LensPostingId);
	      var companyName = "[Confidential]";
	      var recipient = Repositories.Core.FindById<Person>(queuedInvite.PersonId);

	      var sender = (from u in Repositories.Core.Users
	        join p in Repositories.Core.Persons
	          on u.PersonId equals p.Id
	        where
	          u.Id == queuedInvite.CreatedBy
	        select p).FirstOrDefault();

	      // Send the email if the Veteran Priority End Date has expired
	      if (jobView.VeteranPriorityEndDate != null && (jobView.Id != 0 && DateTime.Now > jobView.VeteranPriorityEndDate))
	      {
	        // Set the company name if possible
	        if (jobView.IsNotNull() && jobView.IsConfidential.IsNotNull() && !jobView.IsConfidential.Value)
	        {
	          companyName = jobView.BusinessUnitName;
	        }

	        // Create the email template
	        var templateValues = new EmailTemplateData
	        {
						RecipientName = recipient.FirstName,
	          JobTitle = jobView.JobTitle,
	          EmployerName = companyName,
						SenderName = sender.IsNotNull() ? String.Format("{0} {1}", sender.FirstName, sender.LastName) : "",
	          ShowSalutation = false,
	          JobLinkUrls = new List<string> {jobLinkUrl}
	        };

	        // Send the email
	        var emailTemplate = Helpers.Email.GetEmailTemplatePreview(EmailTemplateTypes.InvitationToApplyForJobThroughFocusCareer,
	            templateValues);
	        Helpers.Email.SendEmail(recipient.EmailAddress, "", "", emailTemplate.Subject, emailTemplate.Body,
	          senderAddress: sender.EmailAddress);

            //Log action to indicate QueuedInvitations are sent to the jobseekers

            var action = sender.User.UserType==UserTypes.Talent? ActionTypes.InviteJobSeekerToApplyThroughTalent : ActionTypes.InviteJobSeekerToApply;
            Helpers.Logging.LogAction(action, typeof(Job).Name, jobId, recipient.Id, additionalDetails: string.Concat(recipient.FirstName, "", recipient.LastName));

	        // Update the Queued status
	        queuedInvite.QueueInvitation = false;

	        recordsProcessed = true;
	      }

				// Update the InviteToApplyTable with the processed queued messages
				if (recordsProcessed)
				{
					Repositories.Core.SaveChanges();
				}
	    }

			Repositories.Core.SaveChanges(true);
	  }

    /// <summary>
    /// Process queued Job Recommendations.
    /// </summary>
    /// <returns></returns>
    public void ProcessQueuedRecommendations()
    {
      var recordsProcessed = false;

      // Get all queued Recommendations
	    var queuedRecommendations = (from recommendation in Repositories.Core.Query<JobPostingOfInterestSent>()
																	 join job in Repositories.Core.Jobs
																	   on recommendation.JobId equals job.Id
	                                 where recommendation.QueueRecommendation == true
																		 && (job.VeteranPriorityEndDate == null || DateTime.Now > job.VeteranPriorityEndDate)
	                                 select recommendation).ToList();

      // Loop through the queued invitations 
      foreach (var queuedRecommendation in queuedRecommendations)
      {
        var jobId = queuedRecommendation.JobId;
        var jobView = Repositories.Core.FindById<JobView>(jobId);
        // var jobLinkUrl = CareerInviteToJobPosting(queuedRecommendation.LensPostingId);
        var jobLinkUrl = CareerInviteToJobPosting(Repositories.Core.FindById<Posting>(queuedRecommendation.PostingId).LensPostingId);
        var companyName = "[Confidential]";
        var recipient = Repositories.Core.FindById<Person>(queuedRecommendation.PersonId);

        var sender = (from u in Repositories.Core.Users
            join p in Repositories.Core.Persons
              on u.PersonId equals p.Id
            where
              u.Id == queuedRecommendation.CreatedBy
            select p).FirstOrDefault();

        // Send the email if the Veteran Priority End Date has expired
        if (jobView.VeteranPriorityEndDate != null && (jobView.Id != 0 && DateTime.Now > jobView.VeteranPriorityEndDate))
        {
          // Set the company name if possible
          if (jobView.IsNotNull() && jobView.IsConfidential.IsNotNull() && !jobView.IsConfidential.Value)
          {
            companyName = jobView.BusinessUnitName;
          }

          // Create the email template
          var templateValues = new EmailTemplateData
          {
            RecipientName = recipient.FirstName,
            JobTitle = jobView.JobTitle,
            EmployerName = companyName,
						SenderName = sender.IsNotNull() ? sender.FirstName : "",
            ShowSalutation = true,
            JobLinkUrls = new List<string> { jobLinkUrl }
          };

          // Send the email
          var emailTemplate = Helpers.Email.GetEmailTemplatePreview(EmailTemplateTypes.RecommendJobSeeker, templateValues);
          Helpers.Email.SendEmail(recipient.EmailAddress, "", "", emailTemplate.Subject, emailTemplate.Body, senderAddress: sender.EmailAddress);

          //FVN - 6011 - To log action when the queued Job recommendation is sent
          Helpers.Logging.LogAction(ActionTypes.InviteJobSeekerToApply, typeof(Job).Name, jobId, recipient.Id, additionalDetails: string.Concat(recipient.FirstName, "", recipient.LastName));

          // Update the Queued status
          queuedRecommendation.QueueRecommendation = false;

          recordsProcessed = true;
        }

        // Update the InviteToApplyTable with the processed queued messages
        if (recordsProcessed)
        {
          Repositories.Core.SaveChanges();
        }
      }

			Repositories.Core.SaveChanges(true);
    }
	}
}
