﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;

using Focus.Core;
using Focus.Core.Models.Integration;
using Focus.Data.Core.Entities;
using Focus.Services.Repositories.Integration;
using Job = Focus.Data.Core.Entities.Job;

using Framework.Core;
using Framework.Messaging;
using Focus.Core.IntegrationMessages;
using Focus.Services.DtoMappers;

#endregion

namespace Focus.Services.Messages.Handlers
{
  public class IntegrationRequestMessageHandler : MessageHandlerBase, IMessageHandler<IntegrationRequestMessage>
  {
    public IntegrationRequestMessageHandler()
      : base(null)
    { }

    public IntegrationRequestMessageHandler(IRuntimeContext runtimeContext)
      : base(runtimeContext)
    { }

    #region Implementation of IMessageHandler<in IntegrationRequestMessage>

    /// <summary>
    /// Calls supporting functions depending on the integration point
    /// </summary>
    /// <param name="requestMessage">The message.</param>
    public void Handle(IntegrationRequestMessage requestMessage)
    {
      requestMessage.InitialiseRuntimeContext(RuntimeContext);

      switch (requestMessage.IntegrationPoint)
      {
        case IntegrationPoint.SaveEmployer:
          SaveEmployer(requestMessage);
          break;
        case IntegrationPoint.AssignAdminToEmployer:
          AssignAdminToEmployer(requestMessage);
          break;
				case IntegrationPoint.AssignAdminToJobOrder:
					AssignAdminToJobOrder(requestMessage);
					break;
        case IntegrationPoint.AssignEmployerActivity:
          AssignEmployerActivity(requestMessage);
          break;
        case IntegrationPoint.SaveEmployee:
          SaveEmployee(requestMessage);
          break;
        case IntegrationPoint.SaveJob:
          SaveJob(requestMessage);
          break;
        case IntegrationPoint.ReferJobSeeker:
          ReferJobSeeker(requestMessage);
          break;
        case IntegrationPoint.SaveJobMatches:
          SaveJobMatches(requestMessage);
          break;
        case IntegrationPoint.SaveJobSeeker:
          SaveJobSeeker(requestMessage);
          break;
        case IntegrationPoint.JobSeekerLogin:
          JobSeekerLogin(requestMessage);
          break;
        case IntegrationPoint.AssignJobSeekerActivity:
          AssignJobSeekerActivity(requestMessage);
          break;
        case IntegrationPoint.AssignAdminToJobSeeker:
          AssignAdminToJobSeeker(requestMessage);
          break;
        case IntegrationPoint.SetApplicationStatus:
          SetApplicationStatus(requestMessage);
          break;
        case IntegrationPoint.LogAction:
          LogAction(requestMessage);
          break;
        case IntegrationPoint.UpdateEmployeeStatus:
          UpdateEmployeeStatus(requestMessage);
          break;
        case IntegrationPoint.DisableJobSeekersReport:
          DisableJobSeekersReport(requestMessage);
          break;
      }
    }

    #endregion

    #region Save Job

	  /// <summary>
	  /// Saves a job in any integrated client system
	  /// </summary>
	  /// <param name="requestMessage">The message.</param>
	  private void SaveJob(IntegrationRequestMessage requestMessage)
    {
      var request = requestMessage.SaveJobRequest;
			if (request.ExternalAdminId.IsNullOrEmpty()) request.ExternalAdminId = requestMessage.ExternalAdminId;
      if (request.ExternalOfficeId.IsNullOrEmpty()) request.ExternalOfficeId = requestMessage.ExternalOfficeId;
      if (request.ExternalPassword.IsNullOrEmpty()) request.ExternalPassword = requestMessage.ExternalPassword;
      if (request.ExternalUsername.IsNullOrEmpty()) request.ExternalUsername = requestMessage.ExternalUsername;

			UpdateOfficeToCurrent(request);

      var response = Repositories.Integration.SaveJob(request);
      if (response.Outcome == IntegrationOutcome.Success)
      {
        // Update the job and posting with the external user Id
        var job = Repositories.Core.FindById<Job>(request.JobId);

        job.ExternalId = response.ExternalJobId;
        job.Posting.ExternalId = response.ExternalJobId;
        Repositories.Core.SaveChanges(true);

				if (response.IsNewJob && response.ExternalJobId.IsNotNullOrEmpty())
				{
					var postingId = Repositories.Core.Postings.Where(p => p.JobId == job.Id).Select(p => p.Id).FirstOrDefault();
					var approvalStatuses = new List<ApprovalStatuses> {ApprovalStatuses.None, ApprovalStatuses.Approved};
					var applicationIds = Repositories.Core.Applications.Where(a => a.PostingId == postingId && approvalStatuses.Contains(a.ApprovalStatus))
																                             .Select(a => a.Id)
																	  												 .ToList();
					applicationIds.ForEach(id =>
					{
						requestMessage = new IntegrationRequestMessage
						{
							IntegrationPoint = IntegrationPoint.ReferJobSeeker,
							ReferJobSeekerRequest = new ReferJobSeekerRequest
							{
								ApplicationId = id
							}
						};

						ReferJobSeeker(requestMessage);
					});
				}
      }
      else
      {
        SaveFailedMessage(requestMessage, request.GetType().Name, response);
      }
    }

	  /// <summary>
	  /// Saves a job seeker referral in the client system
	  /// </summary>
	  /// <param name="requestMessage"></param>
	  private void ReferJobSeeker(IntegrationRequestMessage requestMessage)
    {
			var request = requestMessage.ReferJobSeekerRequest;
			UpdateOfficeToCurrent(request);
			
			if (request.ApplicationId.IsNotNull())
      {
        var details = GetApplicationDetails(request.ApplicationId.Value);

        if (details.IsNotNull())
        {
          request.PersonId = details.PersonId;
          request.JobSeekerId = details.JobSeekerId;
          request.JobId = details.JobId;
          request.ExternalJobId = details.ExternalJobId;
        }
      }

			var response = Repositories.Integration.ReferJobSeeker(request);
			if (response.Outcome != IntegrationOutcome.Success)
			{
				SaveFailedMessage(requestMessage, request.GetType().Name, response);
			}
			else
			{
				if (request.ExternalJobId.IsNullOrEmpty() && response.ExternalJobId.IsNotNullOrEmpty() && Repositories.Core.Jobs.Any(job => job.Id == request.JobId))
				{
					var coreJob = Repositories.Core.Jobs.First(job => job.Id == request.JobId);
					coreJob.ExternalId = response.ExternalJobId;
					Repositories.Core.SaveChanges(true);
				}
			}
    }

    /// <summary>
    /// Updates the applications status
    /// </summary>
    /// <param name="requestMessage"></param>
    private void SetApplicationStatus(IntegrationRequestMessage requestMessage)
    {
      var request = requestMessage.SetApplicationStatusRequest;
      if (request.ApplicationId.IsNotNull())
      {
        var details = GetApplicationDetails(request.ApplicationId.Value);

        if (details.IsNotNull())
        {
          request.JobSeekerId = details.JobSeekerId;
          request.JobId = details.ExternalJobId;
          request.ApplicationStatus = details.Status;
        }
      }

      if (request.JobId.IsNotNullOrEmpty() && request.JobSeekerId.IsNotNullOrEmpty())
      {
        var response = Repositories.Integration.SetApplicationStatus(request);
        if (response.Outcome != IntegrationOutcome.Success)
        {
          SaveFailedMessage(requestMessage, request.GetType().Name, response);
        }
      }
    }

    /// <summary>
    /// Get details of the application from the Focus DB
    /// </summary>
    /// <param name="applicationId">The ID of the application in focus</param>
    /// <returns>Details of the application</returns>
    private ApplicationDetails GetApplicationDetails(long applicationId)
    {
      return (from application in Repositories.Core.Applications
              join resume in Repositories.Core.Resumes
                on application.ResumeId equals resume.Id
              join user in Repositories.Core.Users
                on resume.PersonId equals user.PersonId
              join posting in Repositories.Core.Postings
                on application.PostingId equals posting.Id
              where application.Id == applicationId
              select new ApplicationDetails
              {
                PersonId = user.PersonId,
                JobSeekerId = user.ExternalId,
                JobId = posting.JobId.GetValueOrDefault(),
                ExternalJobId = posting.ExternalId,
                Status = application.ApplicationStatus
              }).FirstOrDefault();
    }

    #endregion

    #region Employer

    /// <summary>
    /// Saves an employer in any integrated client system
    /// </summary>
    /// <param name="requestMessage">The request.</param>
    private void SaveEmployer(IntegrationRequestMessage requestMessage)
    {
      var request = requestMessage.SaveEmployerRequest;
      if (request.Employer.IsNull())
        request.Employer = GetEmployerModel(request.EmployerId);

			UpdateOfficeToCurrent(request);

      var response = Repositories.Integration.SaveEmployer(request);
      if (response.Outcome == IntegrationOutcome.Success)
      {
        var employer = Repositories.Core.Employers.Single(x => x.Id == request.EmployerId);
        employer.ExternalId = response.ExternalEmployerId;
        Repositories.Core.SaveChanges(true);

				if (requestMessage.SaveEmployeeRequest.IsNotNull() && requestMessage.SaveEmployeeRequest.PersonId > 0)
				{
					SaveEmployee(requestMessage);
				}
      }
      else
      {
        SaveFailedMessage(requestMessage, request.GetType().Name, response);
      }
    }

    private void SaveFailedMessage(IntegrationRequestMessage requestMessage, string requestType, IntegrationResponse response)
		{
			if (response.Outcome.IsIn(IntegrationOutcome.NotImplemented, IntegrationOutcome.Ignored)) 
				return;

			// Add the request to the failed messages for admin
			var failedIntegrationMessage = new FailedIntegrationMessage
			{
        IntegrationPoint = requestMessage.IntegrationPoint.IsNotNull() ? requestMessage.IntegrationPoint : (IntegrationPoint?) null,
				DateSubmitted = DateTime.Now,
				MessageType = requestType,
        ErrorDescription = response.Exception.IsNotNull() ? response.Exception.Message : response.Outcome.ToString(),
				Request = requestMessage.SerializeJson(),
				IsIgnored = false
			};

			Repositories.Core.Add(failedIntegrationMessage);
			Repositories.Core.SaveChanges();
    }

    /// <summary>
    /// Assigns an actitivity to an employer
    /// </summary>
    /// <param name="requestMessage">The message</param>
    private void AssignAdminToEmployer(IntegrationRequestMessage requestMessage)
    {
      var request = requestMessage.AssignAdminToEmployerRequest;
      // Get Assist User External Id if not specified in request
      if (request.AdminId.IsNullOrEmpty() && request.UserId != 0)
      {
        var user = Repositories.Core.Users.FirstOrDefault(u => u.Id == request.UserId);
        if (user != null)
          request.AdminId = user.ExternalId;
      }

      if (request.EmployerId.IsNull() || request.AdminId.IsNull())
        return;

      var response = Repositories.Integration.AssignAdminToEmployer(request);
      if (response.Outcome != IntegrationOutcome.Success)
      {
        SaveFailedMessage(requestMessage, request.GetType().Name, response);
      }
    }

		/// <summary>
		/// Assigns an actitivity to a job order
		/// </summary>
		/// <param name="requestMessage">The message</param>
		private void AssignAdminToJobOrder(IntegrationRequestMessage requestMessage)
		{
			var request = requestMessage.AssignAdminToJobOrderRequest;
			// Get Assist User External Id if not specified in request
			if (request.AdminId.IsNullOrEmpty() && request.UserId != 0)
			{
				var user = Repositories.Core.Users.FirstOrDefault(u => u.Id == request.UserId);
				if (user != null)
					request.AdminId = user.ExternalId;
			}

			if (request.JobExternalId.IsNull() || request.AdminId.IsNull())
				return;

			var response = Repositories.Integration.AssignAdminToJobOrder(request);
			if (response.Outcome != IntegrationOutcome.Success)
			{
				SaveFailedMessage(requestMessage, request.GetType().Name, response);
			}
		}

    private void AssignEmployerActivity(IntegrationRequestMessage requestMessage)
    {
      var request = requestMessage.AssignEmployerActivityRequest;
      // Get Assist User External Id if not specified in request
      if (request.AdminId.IsNullOrEmpty() && request.UserId != 0)
      {
        var user = Repositories.Core.Users.FirstOrDefault(u => u.Id == request.UserId);
        if (user != null)
          request.AdminId = user.ExternalId;
      }

      if (request.EmployeeId.IsNull() || request.AdminId.IsNull())
        return;

      // Get the current office of the Assist user
      if (request.OfficeId.IsNullOrEmpty())
      {
        var personId = request.UserId != 0
          ? Repositories.Core.Users.Where(u => u.Id == request.UserId).Select(u => u.PersonId).FirstOrDefault()
          : Repositories.Core.Users.Where(u => u.ExternalId == request.AdminId).Select(u => u.PersonId).FirstOrDefault();
        var currentOfficeId = Repositories.Core.PersonsCurrentOffices
                                             .Where(office => office.PersonId == personId)
                                             .OrderByDescending(office => office.StartTime)
                                             .Select(office => office.OfficeId)
                                             .Take(1)
                                             .FirstOrDefault();
        if (currentOfficeId.IsNotNull() && currentOfficeId > 0)
        {
          var office = Repositories.Core.FindById<Office>(currentOfficeId);
          request.OfficeId = office.ExternalId;
        }
      }

      var response = Repositories.Integration.AssignEmployerActivity(request);
      if (response.Outcome != IntegrationOutcome.Success)
      {
        SaveFailedMessage(requestMessage, request.GetType().Name, response);
      }
    }

    /// <summary>
    /// Gets the EmployerModel for an employer id
    /// </summary>
    /// <param name="employerId">The id of the employer</param>
    /// <returns>The employer model</returns>
    private EmployerModel GetEmployerModel(long employerId)
    {
      var employer = Repositories.Core.Employers.Single(x => x.Id == employerId);
      var businessUnit = employer.BusinessUnits.FirstOrDefault(bu => bu.IsPreferred);
      if (businessUnit.IsNull())
        businessUnit = employer.BusinessUnits.FirstOrDefault();

      if (businessUnit.IsNull())
        throw new Exception(string.Format("Business Unit not found for employer id {0}", employer.Id));

      // Get employer address
      var businessUnitAddress = businessUnit.BusinessUnitAddresses.FirstOrDefault(add => add.IsPrimary);

      var businessUnitAddressModel = businessUnitAddress.IsNull() ? null : new AddressModel
      {
        AddressLine1 = businessUnitAddress.Line1,
        AddressLine2 = businessUnitAddress.Line2,
        AddressLine3 = businessUnitAddress.Line3,
        City = businessUnitAddress.TownCity,
        CountyId = businessUnitAddress.CountyId.GetValueOrDefault(0),
        StateId = businessUnitAddress.StateId,
        CountryId = businessUnitAddress.CountryId,
        PostCode = businessUnitAddress.PostcodeZip
      };

      // Get employees
      var employeeIds = Repositories.Core.Employees.Where(e => e.EmployerId == employerId).Select(e => e.Id).ToList();
      var employees = GetEmployeeModels(employeeIds);

      // Build model
      return new EmployerModel
      {
        Id = employerId,
        Name = businessUnit.Name,
        Naics = businessUnit.IndustrialClassification,
        LegalName = businessUnit.LegalName.IsNullOrEmpty() ? employer.LegalName : businessUnit.LegalName,
        Owner = businessUnit.OwnershipTypeId,
        AccountType = businessUnit.AccountTypeId,
        Phone = businessUnit.PrimaryPhone,
        PhoneExt = businessUnit.PrimaryPhoneExtension,
        AltPhone = businessUnit.AlternatePhone1,
        AltPhoneExt = string.Empty,
        Fax = string.Empty,
        Email = string.Empty,
        Url = businessUnit.Url,
        PublicTransportAccessible = businessUnitAddress.IsNotNull() && businessUnitAddress.PublicTransitAccessible,
        FederalEmployerIdentificationNumber = employer.FederalEmployerIdentificationNumber,
        StateEmployerIdentificationNumber = employer.StateEmployerIdentificationNumber,
        Employees = employees,
        Address = businessUnitAddressModel,
        ExternalId = employer.ExternalId
      };
    }

    #endregion

    #region Employee

    /// <summary>
    /// Saves an employee in any integrated client system
    /// </summary>
    /// <param name="requestMessage">The request.</param>
    private void SaveEmployee(IntegrationRequestMessage requestMessage)
    {
      var request = requestMessage.SaveEmployeeRequest;
      if (request.Employee.IsNull())
        request.Employee = GetEmployeeModel(request.PersonId);

	    var response = Repositories.Integration.SaveEmployee(request);
      if (response.Outcome == IntegrationOutcome.Success)
      {
        if (request.Employee.ExternalId.IsNullOrEmpty() && !response.ExternalEmployeeId.IsNotNullOrEmpty())
        {
          // Update the user with the external user Id
          var user = Repositories.Core.Employees.Single(x => x.Id == request.Employee.Id).Person.User;
          user.ExternalId = response.ExternalEmployeeId;
          Repositories.Core.SaveChanges(true);
        }
      }
      else
      {
        SaveFailedMessage(requestMessage, request.GetType().Name, response);
      }
    }

    /// <summary>
    /// Gets the EmployeeModel for a person
    /// </summary>
    /// <param name="personId">The person id</param>
    /// <returns>The EmployeeModel</returns>
    private EmployeeModel GetEmployeeModel(long personId)
    {
      var employeeId = Repositories.Core.Employees.Where(e => e.PersonId == personId).Select(e => e.Id).FirstOrDefault();
      return employeeId > 0
        ? GetEmployeeModels(new List<long> { employeeId }).FirstOrDefault()
        : null;
    }

    /// <summary>
    /// Gets the EmployeeModels for a list of employee ids
    /// </summary>
    /// <param name="employeeIds">The list of employee ids</param>
    /// <returns>A list of EmployeeModels</returns>
    private List<EmployeeModel> GetEmployeeModels(List<long> employeeIds)
    {
      // Get employees
      var employees = (from employee in Repositories.Core.Employees
                       join person in Repositories.Core.Persons on employee.PersonId equals person.Id
                       join user in Repositories.Core.Users on person.Id equals user.PersonId
                       where employeeIds.Contains(employee.Id)
											 orderby user.CreatedOn ascending 
                       select new EmployeeModel
                       {
                         Id = employee.Id,
                         SalutationId = person.TitleId,
                         FirstName = person.FirstName,
                         LastName = person.LastName,
                         Email = person.EmailAddress,
                         ExternalId = user.ExternalId,
                         ExternalEmployerId = employee.Employer.ExternalId,
                         JobTitle = person.JobTitle
                       }).ToList();


      // Get employee addresses
      var employeeAddresses = (from employee in Repositories.Core.Employees
                               join address in Repositories.Core.PersonAddresses
                                 on employee.PersonId equals address.PersonId
                               where employeeIds.Contains(employee.Id)
                                 && address.IsPrimary
                               select new
                               {
                                 employee.Id,
                                 Address = address
                               }).ToLookup(x => x.Id, x => x.Address);

      // Get employee phone numbers
      var employeePhoneNumbers = (from employee in Repositories.Core.Employees
                                  join phone in Repositories.Core.PhoneNumbers
                                    on employee.PersonId equals phone.PersonId
                                  where employeeIds.Contains(employee.Id)
                                  select new
                                  {
                                    employee.Id,
                                    Phone = phone
                                  }).ToLookup(x => x.Id, x => x.Phone);

      // Update each employee record with any address or phone number
      employees.ForEach(employee =>
      {
        if (employeeAddresses.Contains(employee.Id))
        {
          var address = employeeAddresses[employee.Id].FirstOrDefault();
          if (address.IsNotNull())
          {
            employee.Address = new AddressModel
            {
              AddressLine1 = address.Line1,
              AddressLine2 = address.Line2,
              AddressLine3 = address.Line3,
              City = address.TownCity,
              CountyId = address.CountyId.GetValueOrDefault(0),
              StateId = address.StateId,
              CountryId = address.CountryId,
              PostCode = address.PostcodeZip
            };
          }
        }

        if (employeePhoneNumbers.Contains(employee.Id))
        {
          var primaryPhone = employeePhoneNumbers[employee.Id].FirstOrDefault(phone => phone.IsPrimary);
          if (primaryPhone.IsNotNull())
          {
            employee.Phone = primaryPhone.Number;
            employee.PhoneExt = primaryPhone.Extension;
          }

          var altPhone = employeePhoneNumbers[employee.Id].FirstOrDefault(phone => !phone.IsPrimary && phone.PhoneType == PhoneTypes.Phone);
          if (altPhone.IsNotNull())
          {
            employee.AltPhone = altPhone.Number;
            employee.AltPhoneExt = altPhone.Extension;
          }

          var fax = employeePhoneNumbers[employee.Id].FirstOrDefault(phone => !phone.IsPrimary && phone.PhoneType == PhoneTypes.Fax);
	        if (fax.IsNotNull())
	        {
		        employee.Fax = fax.Number;
	        }
        }
      });

      return employees;
    }

    private void UpdateEmployeeStatus(IntegrationRequestMessage requestMessage)
    {
      var request = requestMessage.UpdateEmployeeStatusRequest;
      var response = Repositories.Integration.UpdateEmployeeStatus(request);
      if (response.Outcome == IntegrationOutcome.Failure)
      {
        SaveFailedMessage(requestMessage, request.GetType().Name, response);
      }
    }

    #endregion

    #region Job seeker

    /// <summary>
    /// Saves the job seeker in the client system
    /// </summary>
    /// <param name="requestMessage">The request.</param>
    private void SaveJobSeeker(IntegrationRequestMessage requestMessage)
    {
      var request = requestMessage.SaveJobSeekerRequest;

			UpdateOfficeToCurrent(request);

      var response = Repositories.Integration.SaveJobSeeker(request);
      if (response.Outcome == IntegrationOutcome.Success)
      {
        var user = Repositories.Core.Users.Single(x => x.PersonId == request.PersonId);
        user.ExternalId = response.ExternalJobSeekerId;
        Repositories.Core.SaveChanges(true);

				if (response.IsNewJobSeeker && response.ExternalJobSeekerId.IsNotNullOrEmpty())
				{
					var approvalStatuses = new List<ApprovalStatuses> { ApprovalStatuses.None, ApprovalStatuses.Approved };
					var applicationIds = (from a in Repositories.Core.Applications
					                      join r in Repositories.Core.Resumes
						                      on a.ResumeId equals r.Id
					                      where r.PersonId == user.PersonId
																  && approvalStatuses.Contains(a.ApprovalStatus)
					                      select a.Id).ToList();

					applicationIds.ForEach(id =>
					{
						requestMessage = new IntegrationRequestMessage
						{
							IntegrationPoint = IntegrationPoint.ReferJobSeeker,
							ReferJobSeekerRequest = new ReferJobSeekerRequest
							{
								ApplicationId = id
							}
						};

						ReferJobSeeker(requestMessage);
					});
				}
      }
      else
      {
        SaveFailedMessage(requestMessage, request.GetType().Name, response);
      }

			if (response.ExternalJobSeekerId.IsNotNullOrEmpty() && request.Activities.IsNotNullOrEmpty())
			{
				request.Activities.ForEach(activity =>
				{
					var message = new IntegrationRequestMessage
					{
						IntegrationPoint = IntegrationPoint.AssignJobSeekerActivity,
						ActionerId = requestMessage.ActionerId,
						AssignJobSeekerActivityRequest = activity
					};
					AssignJobSeekerActivity(message);
				});
			}
    }

    /// <summary>
    /// Sets the job seeker as active in the client system
    /// </summary>
    /// <param name="requestMessage">The request.</param>
    private void JobSeekerLogin(IntegrationRequestMessage requestMessage)
    {
      var request = requestMessage.JobSeekerLoginRequest;
      var response = Repositories.Integration.JobSeekerLogin(request);
      if (response.Outcome != IntegrationOutcome.Success)
      {
        SaveFailedMessage(requestMessage, request.GetType().Name, response);
      }

      if (request.Activities.IsNotNullOrEmpty())
      {
          request.Activities.ForEach(activity =>
          {
              var message = new IntegrationRequestMessage
              {
                  IntegrationPoint = IntegrationPoint.AssignJobSeekerActivity,
                  ActionerId = requestMessage.ActionerId,
                  AssignJobSeekerActivityRequest = activity
              };
              AssignJobSeekerActivity(message);
          });
      }
    }

    private void SaveJobMatches(IntegrationRequestMessage requestMessage)
    {
      var request = requestMessage.SaveJobMatchesRequest;

			UpdateOfficeToCurrent(request);

      var response = Repositories.Integration.SaveJobMatches(request);
      if (response.Outcome != IntegrationOutcome.Success)
      {
        SaveFailedMessage(requestMessage, request.GetType().Name, response);
      }
    }

    /// <summary>
    /// Assigns an actitivity to a job seeker
    /// </summary>
    /// <param name="requestMessage"></param>
    private void AssignJobSeekerActivity(IntegrationRequestMessage requestMessage)
    {
      var request = requestMessage.AssignJobSeekerActivityRequest;

			UpdateOfficeToCurrent(request);

      var response = Repositories.Integration.AssignJobSeekerActivity(request);
      if (response.Outcome != IntegrationOutcome.Success)
      {
        SaveFailedMessage(requestMessage, request.GetType().Name, response);
      }
    }

    /// <summary>
    /// Assigns an actitivity to a job seeker
    /// </summary>
    /// <param name="requestMessage"></param>
    private void AssignAdminToJobSeeker(IntegrationRequestMessage requestMessage)
    {
      var request = requestMessage.AssignAdminToJobSeekerRequest;
      // Get Assist User External Id if not specified in request
      if (request.AdminId.IsNullOrEmpty() && request.UserId != 0)
      {
        var user = Repositories.Core.Users.FirstOrDefault(u => u.Id == request.UserId);
        if (user != null)
          request.AdminId = user.ExternalId;
      }

      if (request.JobSeekerId.IsNull() || request.AdminId.IsNull())
        return;

      var response = Repositories.Integration.AssignAdminToJobSeeker(request);
      if (response.Outcome != IntegrationOutcome.Success)
      {
        SaveFailedMessage(requestMessage, request.GetType().Name, response);
      }
    }

		/// <summary>
		/// Updates the ExternalOfficeId to the Assist user's current office (if it is the Assist user performing the action)
		/// Also updates the ExternalUserName to avoid case sensitivity issues in EKOS
		/// </summary>
		/// <param name="request">The request to update</param>
		private void UpdateOfficeToCurrent(IIntegrationRequest request)
		{
			if (request.ExternalAdminId.IsNotNullOrEmpty() && Repositories.Integration.OfficeRequiresUpdate(request))
			{
				var userDetails = Repositories.Core.Users
																					 .Where(u => u.ExternalId == request.ExternalAdminId && u.UserType == UserTypes.Assist)
																					 .Select(u => new
																					 {
																						 u.PersonId,
																						 u.UserName
																					 }).FirstOrDefault();
				if (userDetails.IsNotNull())
				{
					var currentOffice = (from pco in Repositories.Core.PersonsCurrentOffices
					                     join o in Repositories.Core.Offices
						                     on pco.OfficeId equals o.Id
															 where pco.PersonId == userDetails.PersonId
					                     orderby pco.StartTime descending
					                     select o.ExternalId).FirstOrDefault();

					request.ExternalOfficeId = currentOffice.IsNotNullOrEmpty() ? currentOffice : RuntimeContext.CurrentRequest.UserContext.ExternalOfficeId;

					// Also update the user name, as it may have been entered in the incorrect case when logging in. User names are not case-sensitive in FOCUS but they are in EKOS
					request.ExternalUsername = userDetails.UserName;
				}
			}
		}

    #endregion

    #region System

    private void LogAction(IntegrationRequestMessage requestMessage)
    {
      var request = requestMessage.LogActionRequest;
      var response = Repositories.Integration.LogAction(request);
      if (response.Outcome != IntegrationOutcome.Success)
      {
        SaveFailedMessage(requestMessage, request.GetType().Name, response);
      }
    }

    #endregion

    #region Reporting

    private void DisableJobSeekersReport(IntegrationRequestMessage requestMessage)
    {      
      var jobSeekerIdList = requestMessage.DisableJobSeekersReportRequest.JobSeekerIdList.Select(js => js.JobSeekerId).ToList();
      var startingPoint = 0;
      var jobSeekerIdListResult = new List<DisableJobSeekersReportModel>();

      // Get the Social Security Numbers from the Persons table (in chunks of 500)
      while (startingPoint < jobSeekerIdList.Count)
      {
        var jobSeekerIdListSection = jobSeekerIdList.Skip(startingPoint).Take(500);
        var query = (from item in Repositories.Core.Persons
                     where jobSeekerIdListSection.Contains(item.Id)
                     select new DisableJobSeekersReportModel
                     {
                       JobSeekerId = item.Id,
                       SSN = item.SocialSecurityNumber ?? ""
                     });
        jobSeekerIdListResult.AddRange(query);
        startingPoint += 500;
      }

      var request = new DisableJobSeekersReportRequest { JobSeekerIdList = jobSeekerIdListResult };

      var response = Repositories.Integration.DisableJobSeekersReport(request);
      if (response.Outcome != IntegrationOutcome.Success)
      {
        SaveFailedMessage(requestMessage, request.GetType().Name, response);
      }
    }

    #endregion

    private class ApplicationDetails
    {
      public long PersonId { get; set; }
      public string JobSeekerId { get; set; }
      public long JobId { get; set; }
      public string ExternalJobId { get; set; }
      public ApplicationStatusTypes Status { get; set; }
    }
  }
}
