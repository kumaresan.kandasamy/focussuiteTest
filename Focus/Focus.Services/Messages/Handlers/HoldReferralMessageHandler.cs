﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using Framework.Messaging;

#endregion

namespace Focus.Services.Messages.Handlers
{
	public class HoldReferralMessageHandler : MessageHandlerBase, IMessageHandler<HoldReferralMessage>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="HoldReferralMessageHandler"/> class.
		/// </summary>
		public HoldReferralMessageHandler() : base(null) {}

		/// <summary>
		/// Initializes a new instance of the <see cref="HoldReferralMessageHandler"/> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		public HoldReferralMessageHandler(IRuntimeContext runtimeContext) : base(runtimeContext) {}

		/// <summary>
		/// Handles the specified message.
		/// </summary>
		/// <param name="message">The message.</param>
		public void Handle(HoldReferralMessage message)
		{
			message.InitialiseRuntimeContext(RuntimeContext);

			RuntimeContext.Helpers.Candidate.HoldReferral(message.ApplicationId, message.Reason, true, message.ActionerId);
		}
	}
}
