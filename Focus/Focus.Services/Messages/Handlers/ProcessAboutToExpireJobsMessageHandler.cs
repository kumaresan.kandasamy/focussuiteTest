﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

using Focus.Core;
using Focus.Core.EmailTemplate;
using Focus.Core.Views;
using Focus.Data.Configuration.Entities;
using Focus.Data.Core.Entities;
using Focus.Services.Core;
using Focus.Services.Views;

using Framework.Core;
using Framework.Logging;
using Framework.Messaging;

#endregion

namespace Focus.Services.Messages.Handlers
{
	public class ProcessAboutToExpireJobsMessageHandler : MessageHandlerBase, IMessageHandler<ProcessAboutToExpireJobsMessage>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="ProcessAboutToExpireJobsMessageHandler"/> class.
		/// </summary>
		public ProcessAboutToExpireJobsMessageHandler() : base(null) {}

		/// <summary>
		/// Initializes a new instance of the <see cref="ProcessAboutToExpireJobsMessageHandler"/> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		public ProcessAboutToExpireJobsMessageHandler(IRuntimeContext runtimeContext) : base(runtimeContext) { }

		/// <summary>
		/// Handles the specified message.
		/// </summary>
		/// <param name="message">The message.</param>
		public void Handle(ProcessAboutToExpireJobsMessage message)
		{
			message.InitialiseRuntimeContext(RuntimeContext);

			var runTime = DateTime.Now;
			//var emailItemsList = new List<EmailItem>();
			var emailItemsList = new List<Tuple<string, EmailTemplateView>>();
			var processFailed = false;

			#region Check if job is running

			var runningSetting = Helpers.Configuration.GetConfigurationItem(Constants.ConfigurationItemKeys.ProcessExpiringJobsRunning, false.ToString());
			var processIsRunning = false;

			if (runningSetting.Value.IsNotNullOrEmpty())
			{
				try
				{
					processIsRunning = Convert.ToBoolean(runningSetting.Value);
				}
				catch { }
			}

			if (processIsRunning)
				throw new Exception("Process Expiring Jobs is already running");

			// Set running flag to true
			runningSetting.Value = true.ToString();
			Repositories.Configuration.SaveChanges(true);

			#endregion

			#region Check if job has run in last 24 hours

			var lastRunSetting = Helpers.Configuration.GetConfigurationItem(Constants.ConfigurationItemKeys.ProcessExpiringJobsLastRun, DateTime.MinValue.ToShortDateString());

			var lastRunDate = DateTime.MinValue;

			if (lastRunSetting.Value.IsNotNullOrEmpty())
			{
			  try
			  {
			    lastRunDate = Convert.ToDateTime(lastRunSetting.Value);
			  }
			  catch { }
			}

			if (lastRunDate > DateTime.Now.AddDays(0))
			  throw new Exception("Process About to Expire Jobs has already run in the last 24 hours");

			#endregion

			try
			{
				Helpers.Logging.LogAction(ActionTypes.ProcessExpiringJobsStarted, String.Empty, null, true);

				var jobExpiryCountdownDays = AppSettings.JobExpiryCountdownDays.OrderBy(x => x);

				foreach (var jobExpiryCountdownDay in jobExpiryCountdownDays)
				{
					// Get all jobs whose closing on date is x days away
					var expiringJobViews = Repositories.Core.ExpiringJobViews.Where(x => x.DaysToClosing == jobExpiryCountdownDay).ToList();

					// Loop through jobs
					foreach (var expiringJobView in expiringJobViews)
					{
						// First remove all expiry message pertaining to this jobs
						var messages = Repositories.Core.Messages.Where(x => x.EntityId == expiringJobView.Id && x.MessageType == MessageTypes.ImpendingJobExpiryWarning).ToList();

						foreach (var exisyingMessage in messages)
							Repositories.Core.Remove(exisyingMessage);

						var user = Repositories.Core.FindById<User>(expiringJobView.UserId);

						var employee = Repositories.Core.FindById<Employee>(expiringJobView.EmployeeId.GetValueOrDefault());
						var emailTemplateData = new EmailTemplateData
            {
							RecipientName = employee.Person.FirstName + " " + employee.Person.LastName,
							JobTitle = expiringJobView.JobTitle,
							JobId = expiringJobView.Id.ToString(CultureInfo.InvariantCulture),
							DaysLeftToJobExpiry = jobExpiryCountdownDay.ToString(CultureInfo.InvariantCulture)
            };

						var emailTemplate = Helpers.Email.GetEmailTemplatePreview(EmailTemplateTypes.EmployerNotificationPendingJobExpiration, emailTemplateData);

						// Add email item and alert for each hiring manager that posted job
						emailItemsList.Add(Tuple.Create(employee.Person.EmailAddress, emailTemplate));
							//(new EmailItem
						  //               	{
						    //               		EmailAddress = user.Person.EmailAddress,
									//								emailTemplate
						                   		//Subject = Helpers.Localisation.Localise("System.Email.JobExpiring.Title", string.Empty, expiringJobView.JobTitle, expiringJobView.Id, expiringJobView.DaysToClosing),
																	//Body = Helpers.Localisation.Localise("System.Email.JobExpiring.Body", string.Empty, expiringJobView.JobTitle, expiringJobView.Id, expiringJobView.ClosingOn, expiringJobView.BusinessUnitName, expiringJobView.DaysToClosing)
						    //               	});

						Helpers.Alerts.CreateAlertMessage(Constants.AlertMessageKeys.JobExpiring, expiringJobView.UserId, DateTime.Now.Date.AddDays(1), MessageAudiences.User, MessageTypes.ImpendingJobExpiryWarning,
																							expiringJobView.Id, expiringJobView.JobTitle, expiringJobView.PostedOn.ToString(), expiringJobView.ClosingOn);
					}
				}
			}
			catch (Exception ex)
			{
				Logger.Error(RuntimeContext.CurrentRequest.LogData(), "Unhandled Exception", ex);
				processFailed = true;
			}

			// Reset config settings (For some reason we need to reset the configuration setting)
			runningSetting = Repositories.Configuration.ConfigurationItems.FirstOrDefault(x => x.Key == Constants.ConfigurationItemKeys.ProcessExpiringJobsRunning);

			if (runningSetting.IsNull())
			{
				runningSetting = new ConfigurationItem { Key = Constants.ConfigurationItemKeys.ProcessExpiringJobsRunning };
				Repositories.Configuration.Add(runningSetting);
			}

			runningSetting.Value = false.ToString();

			if (!processFailed)
			{
			  lastRunSetting = Repositories.Configuration.ConfigurationItems.FirstOrDefault(x => x.Key == Constants.ConfigurationItemKeys.ProcessExpiringJobsLastRun);

			  if (lastRunSetting.IsNull())
			  {
			    lastRunSetting = new ConfigurationItem {Key = Constants.ConfigurationItemKeys.ProcessExpiringJobsLastRun};
			    Repositories.Configuration.Add(lastRunSetting);
			  }

			  lastRunSetting.Value = runTime.AddMinutes(-5).ToString();
			}

			Repositories.Configuration.SaveChanges(true);

			if (!processFailed)
				Helpers.Logging.LogAction(ActionTypes.ProcessExpiringJobsCompleted, String.Empty, null, true);

			foreach (var emailItem in emailItemsList)
				//Helpers.Email.SendEmail(emailItem.EmailAddress, string.Empty, string.Empty, emailItem.Subject, emailItem.Body, true);
				Helpers.Email.SendEmail(emailItem.Item1, string.Empty, string.Empty, emailItem.Item2.Subject, emailItem.Item2.Body, true);
		}
	}
}
