﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using Focus.Core;
using Focus.Services.ServiceImplementations;
using Framework.Core;
using Focus.Core.IntegrationMessages;
using Framework.Messaging;

#endregion

namespace Focus.Services.Messages.Handlers
{
	[Obsolete]
	public class UnregisterResumeMessageHandler : MessageHandlerBase, IMessageHandler<UnregisterResumeMessage>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="UnregisterResumeMessageHandler"/> class.
		/// </summary>
		public UnregisterResumeMessageHandler() : base(null) {}
		
		/// <summary>
		/// Initializes a new instance of the <see cref="UnregisterResumeMessageHandler"/> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		public UnregisterResumeMessageHandler(IRuntimeContext runtimeContext) : base(runtimeContext) { }

		/// <summary>
		/// Handles the specified message.
		/// </summary>
		/// <param name="message">The message.</param>
		public void Handle(UnregisterResumeMessage message)
		{
			// Convert to ResumeProcessMessage to avoid upgrade issues
			var resumeMessage = new ResumeProcessMessage
			{
				ActionerId = message.ActionerId,
				Culture = message.Culture,
				ExternalAdminId = message.ExternalAdminId,
				ExternalOfficeId = message.ExternalOfficeId,
				ExternalUsername = message.ExternalUsername,
				ExternalPassword = message.ExternalPassword,
				ResumeId = message.ResumeId,
				PersonId = message.PersonId,
				PersonIds = message.PersonIds,
				IgnoreLens = message.IgnoreLens,
				IgnoreClient = message.IgnoreClient,
				RegisterResume = false,
				ActionTypesToPublish = message.ActionTypesToPublish
			};

			// Set priority as direct to ensure it is picked up straightaway
			Helpers.Messaging.Publish(resumeMessage, MessagePriority.Direct);


			//message.InitialiseRuntimeContext(RuntimeContext);
			//
			//if (!message.IgnoreLens)
			//{
			//  if (message.PersonIds.IsNullOrEmpty())
			//    Helpers.Resume.UnregisterResume(message.ResumeId, message.PersonId);
			//  else
			//    Helpers.Resume.UnregisterResume(message.PersonIds);
			//}

			//if (!message.IgnoreClient && Helpers.Configuration.AppSettings.IntegrationClient != IntegrationClient.Standalone)
			//{
			//  var user = Repositories.Core.Users.First(u => u.PersonId == message.PersonId);
			//  user.PublishUserUpdateIntegrationRequest(message.ActionerId, RuntimeContext);
			//}

			//if (!message.IgnoreLens)
			//{
			//  Helpers.JobDevelopment.RefreshRecentlyPlacedMatches(message.Culture);

			//  if (message.PersonIds.IsNullOrEmpty())
			//    Helpers.JobDevelopment.GeneratePostingMatchesForPerson(message.PersonId, message.Culture);
			//  else
			//    message.PersonIds.ForEach(personId => Helpers.JobDevelopment.GeneratePostingMatchesForPerson(personId, message.Culture));
			//}
		}
	}
}
