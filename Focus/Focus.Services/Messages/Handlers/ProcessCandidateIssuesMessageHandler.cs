﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.EmailTemplate;
using Focus.Core.IntegrationMessages;
using Focus.Core.Models.Integration;
using Focus.Data.Core.Entities;
using Focus.Services.Helpers;
using Framework.Core;
using Framework.Messaging;
using Mindscape.LightSpeed;
using Mindscape.LightSpeed.Querying;

#endregion

namespace Focus.Services.Messages.Handlers
{
	public class ProcessCandidateIssuesMessageHandler : MessageHandlerBase, IMessageHandler<ProcessCandidateIssuesMessage>
	{
		public struct JobSeekerIds
		{
			public long Id;
			public long UserId;
		}

		private const int BatchSizeForRead = 1000;
		private const int BatchSizeForUpdate = 500; // Max is 2100 when using WHERE Id In (1, 2, 3, ...

		/// <summary>
		/// Initializes a new instance of the <see cref="ProcessCandidateIssuesMessageHandler"/> class.
		/// </summary>
		public ProcessCandidateIssuesMessageHandler() : base(null) {}

		/// <summary>
		/// Initializes a new instance of the <see cref="ProcessCandidateIssuesMessageHandler"/> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		public ProcessCandidateIssuesMessageHandler(IRuntimeContext runtimeContext) : base(runtimeContext) {}

		public IQueryable<ProcessCandidateIssuesRecord> GetJobSeekerQuery()
		{
			var minAgeForProcessing = AppSettings.UnderAgeJobSeekerRestrictionThreshold;
			return (from persons in RuntimeContext.Repositories.Core.Query<Person>()
				join users in RuntimeContext.Repositories.Core.Users on persons.Id equals users.PersonId
				join issues in RuntimeContext.Repositories.Core.Issues on persons.Id equals issues.PersonId
				join resume in RuntimeContext.Repositories.Core.Resumes on persons.Id equals resume.PersonId
				where (persons.Age == null || persons.Age >= minAgeForProcessing)
				      && resume.IsDefault && resume.StatusId == ResumeStatuses.Active
				orderby persons.Id
				select new ProcessCandidateIssuesRecord
				{
					Candidate = new CandidateBasicViewDto
					{
						Id = persons.Id,
						ClientId = users.ExternalId,
						EmailAddress = persons.EmailAddress,
						Enabled = users.Enabled,
						FirstName = persons.FirstName,
						LastName = persons.LastName,
						LastLoggedInOn = users.LoggedInOn,
						CreatedOn = users.CreatedOn,
						UserId = users.Id
					},
					Issues = issues,
					Resume = new ResumeShortView { IsUSCitizen = resume.IsUSCitizen, AlienRegExpiryDate = resume.AlienRegExpiryDate }
				});
		}

		/// <summary>
		/// Handles the specified message.
		/// </summary>
		/// <param name="message">The message.</param>
		public void Handle(ProcessCandidateIssuesMessage message)
		{
			message.InitialiseRuntimeContext(RuntimeContext);
			var jobSeekerQuery = GetJobSeekerQuery();

			var processIssues = true;
			var seekersRead = 0;

			var jobOfferRejectionIssues = new List<ProcessCandidateIssuesUpdate>();
			var notReportingIssues = new List<ProcessCandidateIssuesUpdate>();
			var notRespondingToInvitesIssues = new List<ProcessCandidateIssuesUpdate>();
			var notLoggingIssues = new List<ProcessCandidateIssuesUpdate>();
			var postHireIssues = new List<ProcessCandidateIssuesUpdate>();
			var lowQualityMatchesIssues = new List<ProcessCandidateIssuesUpdate>();
			var notSearchingJobsIssues = new List<ProcessCandidateIssuesUpdate>();
			var notClickingJobsIssues = new List<ProcessCandidateIssuesUpdate>();
			var expiredAlienRegistrationIssues = new List<ProcessCandidateIssuesUpdate>();

			var jobSeekerEmailsToSend = new List<CandidateBasicViewDto>();
			var jobSeekerDetailsToDisable = new List<JobSeekerIds>();
			var jobSeekersToBlock = new List<ProcessCandidateIssuesRecord>();

			while (processIssues)
			{
				var jobSeekersList = jobSeekerQuery.Skip(seekersRead).Take(BatchSizeForRead).ToList();

				if (jobSeekersList.Count > 0)
				{
					var issuesList = jobSeekersList.Select(seeker => seeker.Issues).ToList();

					RecalculateRejectionAndNotReportingIssues(issuesList, jobOfferRejectionIssues, notReportingIssues);
					RecalculateNotRespondingToEmployerInvitesIssue(issuesList, notRespondingToInvitesIssues);
					RecalculateNotLoggingInIssue(jobSeekersList, notLoggingIssues);
					RecalculatePostHireFollowUpIssue(issuesList, postHireIssues);
					RecalculateShowingLowQualityMatchesIssue(jobSeekersList, lowQualityMatchesIssues);
					RecalculateNotSearchingAndNotClickingIssue(jobSeekersList, notSearchingJobsIssues, notClickingJobsIssues);
					RecalculateAlienRegistrationIssue(jobSeekersList, expiredAlienRegistrationIssues, jobSeekersToBlock);

					BuildJobSeekerInactivityEmails(jobSeekersList, jobSeekerEmailsToSend, jobSeekerDetailsToDisable);

					// This should not do any db updates, but is done to reset the repository (to free up memory)
					Repositories.Core.SaveChanges(true);

					seekersRead += jobSeekersList.Count;
				}
				else
				{
					processIssues = false;
				}
			}

			// Update issues in batches
			BatchUpdateIssues(jobOfferRejectionIssues, "JobOfferRejectionTriggered", "JobOfferRejectionCount");
			BatchUpdateIssues(notReportingIssues, "NotReportingToInterviewTriggered", "NotReportingToInterviewCount");
			BatchUpdateIssues(notRespondingToInvitesIssues, "NotRespondingToEmployerInvitesTriggered", "NotRespondingToEmployerInvitesCount");
			BatchUpdateIssues(notLoggingIssues, "NoLoginTriggered");
			BatchUpdateIssues(postHireIssues, "PostHireFollowUpTriggered");
			BatchUpdateIssues(lowQualityMatchesIssues, "ShowingLowQualityMatchesCount", "ShowingLowQualityMatchesTriggered");
			BatchUpdateIssues(notSearchingJobsIssues, "NotSearchingJobsTriggered");
			BatchUpdateIssues(notClickingJobsIssues, "NotClickingOnLeadsTriggered", "NotClickingOnLeadsCount");
			BatchUpdateAlienRegistrationIssues(expiredAlienRegistrationIssues);
			BatchBlockJobSeekers(jobSeekersToBlock);

			UnregisterResumes(jobSeekersToBlock.Select(js => js.Candidate.UserId).ToList());

			// Disable job seekers, if any
			DisableJobSeekers(jobSeekerDetailsToDisable);

			// Send the emails to any inactive job seekers that have been identified
			EmailInactiveJobSeekers(jobSeekerEmailsToSend);

			// Email Job Seekers with expired alien registration
			EmailJobSeekersWithExpiredAlienRegistration(jobSeekersToBlock);

		}

		public void DisableJobSeekers(List<JobSeekerIds> jobSeekerDetailsToDisable)
		{
			if (jobSeekerDetailsToDisable.Any())
			{
				var jobSeekerIdsToDisable = jobSeekerDetailsToDisable.Select(t => t.UserId).ToList();

				BatchUpdateByIdList(typeof(User), jobSeekerIdsToDisable, new Dictionary<string, object> { { "Enabled", 0 }, { "AccountDisabledReason", AccountDisabledReason.Inactivity } });
				UnregisterResumes(jobSeekerIdsToDisable);

				jobSeekerDetailsToDisable.ForEach(candidate => Helpers.Logging.LogAction(ActionTypes.InactivateAccount, typeof(Person).Name, candidate.Id, candidate.UserId));
			}

		  if (AppSettings.IntegrationClient == IntegrationClient.Georgia)
		  {
        var integrationRequest = new IntegrationRequestMessage
        {
          ActionerId = RuntimeContext.CurrentRequest.UserContext.UserId,
          IntegrationPoint = IntegrationPoint.DisableJobSeekersReport,
          DisableJobSeekersReportRequest = new DisableJobSeekersReportRequest()
          {
            JobSeekerIdList = jobSeekerDetailsToDisable.Select(t => new DisableJobSeekersReportModel{JobSeekerId = t.Id}).ToList()
          }
        };

        Helpers.Messaging.Publish(integrationRequest);
		  }

		}

		public void EmailInactiveJobSeekers( List<CandidateBasicViewDto> jobSeekerEmailsToSend )
		{
			foreach (var candidate in jobSeekerEmailsToSend)
			{
				var template = new EmailTemplateData { RecipientName = candidate.FirstName };
				var preview = Helpers.Email.GetEmailTemplatePreview(EmailTemplateTypes.CareerJobSeekerInactivity, template);

				Helpers.Email.SendEmail(candidate.EmailAddress, "", "", preview.Subject, preview.Body, true);
			}
		}

		public void EmailJobSeekersWithExpiredAlienRegistration(List<ProcessCandidateIssuesRecord> jobSeekersToBlock)
		{
			var helper = new CandidateHelper(RuntimeContext);
			foreach (var seeker in jobSeekersToBlock)
			{
				helper.SendExpiredAlienRegistrationEmail(seeker.Candidate.FirstName, seeker.Candidate.EmailAddress);
			}
		}



		#region Batch Updating

		public void BatchUpdateAlienRegistrationIssues(List<ProcessCandidateIssuesUpdate> issuesToUpdate)
		{
			BatchUpdateIssues(issuesToUpdate, "HasExpiredAlienCertificationRegistration");
		}

		/// <summary>
		/// Updates the "triggered" and "count" field on issues in batches
		/// </summary>
		/// <param name="issuesToUpdate">The issues to update.</param>
		/// <param name="triggerFieldName">Name of the trigger field.</param>
		/// <param name="countFieldName">Name of the count field.</param>
		private void BatchUpdateIssues(List<ProcessCandidateIssuesUpdate> issuesToUpdate, string triggerFieldName, string countFieldName = "")
		{
			if (!issuesToUpdate.Any())
				return;

			var triggersToEnable = issuesToUpdate.Where(issues => issues.IssuesTriggered.GetValueOrDefault()).Select(issues => issues.PersonId).ToList();
			if (triggersToEnable.Any())
				BatchUpdateByIdList(typeof(Issues), triggersToEnable, new Dictionary<string, object> { { triggerFieldName, true } }, "PersonId");

			if (countFieldName.IsNotNullOrEmpty())
			{
				var countsToUpdate = issuesToUpdate.Where(issues => issues.IssuesCount.HasValue).ToLookup(issues => issues.IssuesCount.GetValueOrDefault(), issues => issues.PersonId);
				if (countsToUpdate.Any())
					BatchUpdateByGroupedIds(typeof(Issues), countsToUpdate, countFieldName, "PersonId");
			}
		}

		public void BatchBlockJobSeekers(List<ProcessCandidateIssuesRecord> jobSeekersToBlock)
		{
			if (jobSeekersToBlock.Any())
			{
				var jobSeekerIdsToDisable = jobSeekersToBlock.Select(js => js.Candidate.UserId).ToList();

				BatchUpdateByIdList(typeof(User), jobSeekerIdsToDisable, new Dictionary<string, object> { { "Blocked", 1 }, { "BlockedReason", BlockedReason.AlienRegistrationDateExpired } });

				jobSeekersToBlock.ForEach(js => Helpers.Logging.LogAction(ActionTypes.BlockUser, typeof(Person).Name, js.Candidate.Id, js.Candidate.UserId, (long)BlockedReason.AlienRegistrationDateExpired));
				Repositories.Core.SaveChanges(true);
			}
		}

		/// <summary>
		/// Performs a batch update on a list of ids
		/// </summary>
		/// <param name="entityType">Type of the entity.</param>
		/// <param name="idsToUpdate">The ids to update.</param>
		/// <param name="changes">The change dictionary.</param>
		/// <param name="idFieldName">Name of the id field.</param>
		/// <remarks>See http://www.mindscapehq.com/documentation/lightspeed/Performance-and-Tuning/Bulk-Updates-and-Deletes </remarks>
		private void BatchUpdateByIdList<T>(Type entityType, List<T> idsToUpdate, Dictionary<string, object> changes, string idFieldName = "Id")
		{
			var index = 0;
			while (index < idsToUpdate.Count)
			{
				// Perform bulk update in batches
				var batchIds = idsToUpdate.Skip(index).Take(BatchSizeForUpdate).Cast<object>().ToArray();

				// Note: If batchSeekers were set to be an array of longs, instead an array of objects, Lightspeed would translates it to the following:
				//       WHERE [Id] IN ('System.Int64[]')

				var batchQuery = new Query(entityType, Entity.Attribute(idFieldName).In(batchIds));

				Repositories.Core.Update(batchQuery, changes);
				Repositories.Core.SaveChanges(true);

				index += batchIds.Length;
			}
		}

		/// <summary>
		/// Performs a batch update on a lists of ids grouped by the value that needs to be updated
		/// </summary>
		/// <param name="entityType">Type of the entity.</param>
		/// <param name="groupedIds">The ids to update.</param>
		/// <param name="fieldToUpdate">The name of the field to updat.</param>
		/// <param name="idFieldName">Name of the id field.</param>
		private void BatchUpdateByGroupedIds<T1, T2>(Type entityType, ILookup<T1, T2> groupedIds, string fieldToUpdate, string idFieldName = "Id")
		{
			// Dictionary is keyed by word counts (although unlikely too may resumes have the same number of words)
			var allFieldValues = groupedIds.Select(g => g.Key).ToList();
			allFieldValues.ForEach(fieldValue =>
			{
				var updateIds = groupedIds[fieldValue].ToList();
				BatchUpdateByIdList(entityType, updateIds, new Dictionary<string, object> { { fieldToUpdate, fieldValue } }, idFieldName);
			});
		}

		/// <summary>
		/// Unregisters resumes for disabled job seekers
		/// </summary>
		/// <param name="jobSeekerIds">The job seeker ids.</param>
		private void UnregisterResumes(List<long> jobSeekerIds)
		{
			const string updateSql = @"
UPDATE
	[Data.Application.Resume]
SET
  ResumeXml = REPLACE(ResumeXml, '<resume_searchable>1</resume_searchable>', '<resume_searchable>0</resume_searchable>'),
  IsSearchable = 0,
  ResetSearchable = 1
WHERE
  PersonId IN ({0})
  AND IsDefault = 1
  AND StatusId = 1
  AND IsSearchable = 1
  AND ResumeCompletionStatusId = 127";

			var index = 0;
			while (index < jobSeekerIds.Count)
			{
				// Perform bulk update in batches
				var batchIds = jobSeekerIds.Skip(index).Take(BatchSizeForUpdate).ToArray();

				var query = from resume in Repositories.Core.Resumes
					join person in Repositories.Core.Persons
						on resume.PersonId equals person.Id
					join user in Repositories.Core.Users
						on person.Id equals user.PersonId
					where batchIds.Contains(user.Id)
					      && resume.IsDefault
					      && resume.StatusId == ResumeStatuses.Active
					      && resume.IsSearchable
					      && resume.ResumeCompletionStatusId == ResumeCompletionStatuses.Completed
					select person.Id;

				var allPersonIds = query.Distinct().ToList();

				if (allPersonIds.Any())
				{
					var sql = string.Format(updateSql, allPersonIds.AsCommaListForSql());
					Repositories.Core.ExecuteNonQuery(sql);
					Repositories.Core.SaveChanges(true);

					// Unregister resumes in batches of 10
					var subIndex = 0;
					while (subIndex < allPersonIds.Count)
					{
						var personIds = allPersonIds.Skip(subIndex).Take(10).ToList();

						Helpers.Messaging.Publish(new ResumeProcessMessage
						{
							ActionerId = RuntimeContext.CurrentRequest.UserContext.ActionerId,
							Culture = RuntimeContext.CurrentRequest.UserContext.Culture,
							PersonIds = personIds,
							IgnoreLens = false,
							RegisterResume = false
						});

						subIndex += personIds.Count;
					}
				}

				index += batchIds.Length;
			}
		}

		#endregion

		#region Recalculate Issue Counts

		/// <summary>
		/// Recalculates the job offer rejection issues.
		/// </summary>
		/// <param name="issuesList">A list of issues.</param>
		/// <param name="jobOfferRejectionIssues">A list to populate with job offer rejection issues.</param>
		/// <param name="notReportingIssues">A list to populate with "Not reporting to interview" issues.</param>
		private void RecalculateRejectionAndNotReportingIssues(List<Issues> issuesList, List<ProcessCandidateIssuesUpdate> jobOfferRejectionIssues, List<ProcessCandidateIssuesUpdate> notReportingIssues)
		{
			// Issue not enabled so don't recalculate
			if (!AppSettings.RejectingJobOffersEnabled && !AppSettings.NotReportingForInterviewEnabled)
				return;

			issuesList = issuesList.Where(issues => !issues.JobOfferRejectionTriggered || !issues.NotReportingToInterviewTriggered).ToList();

			// All issues already triggered so don't recalculate
			if (!issuesList.Any())
				return;

			var calcStartDateJobOffer = DateTime.Now.Date.AddDays(AppSettings.RejectingJobOffersDaysThreshold * -1);
			var calcStartDateNotReporting = DateTime.Now.Date.AddDays(AppSettings.NotReportingForInterviewDaysThreshold * -1);

			var calcStartDate = calcStartDateJobOffer < calcStartDateNotReporting ? calcStartDateJobOffer : calcStartDateNotReporting;

			var minPersonId = issuesList.Min(issues => issues.PersonId);
			var maxPersonId = issuesList.Max(issues => issues.PersonId);

			const long statusJobOffer = (long)ApplicationStatusTypes.RefusedOffer;
			const long statusNotReporting = (long)ApplicationStatusTypes.FailedToShow;

			var statuses = new List<long> { statusJobOffer, statusNotReporting };

			var allLogs = Repositories.Core.Query<ApplicationStatusLogBasicView>()
				.Where(x => x.ActionedOn >= calcStartDate && statuses.Contains(x.CandidateApplicationStatus) && x.PersonId >= minPersonId && x.PersonId <= maxPersonId)
				.GroupBy(x => new
				{
					x.PersonId,
					x.CandidateApplicationStatus,
					x.ActionedOn
				})
				.Select(x => new
				{
					x.Key.PersonId,
					x.Key.CandidateApplicationStatus,
					x.Key.ActionedOn,
					Count = x.Count()
				})
				.ToList();

			issuesList.ForEach(issues =>
			{
				if (AppSettings.RejectingJobOffersEnabled && !issues.JobOfferRejectionTriggered)
				{
					var seekerCheckDate = (issues.JobOfferRejectionResolvedDate.IsNotNull() && calcStartDateJobOffer < issues.JobOfferRejectionResolvedDate)
						? issues.JobOfferRejectionResolvedDate.Value
						: calcStartDateJobOffer;

					var update = new ProcessCandidateIssuesUpdate { PersonId = issues.PersonId };

					var newCount = allLogs.Where(log => log.PersonId == issues.PersonId && log.CandidateApplicationStatus == statusJobOffer && log.ActionedOn >= seekerCheckDate)
						.Sum(log => log.Count);

					if (newCount != issues.JobOfferRejectionCount)
						update.IssuesCount = newCount;

					if (newCount >= AppSettings.RejectingJobOffersCountThreshold)
						update.IssuesTriggered = true;

					if (update.IssuesCount.HasValue || update.IssuesTriggered.HasValue)
						jobOfferRejectionIssues.Add(update);
				}

				if (AppSettings.NotReportingForInterviewEnabled && !issues.NotReportingToInterviewTriggered)
				{
					var seekerCheckDate = (issues.NotReportingToInterviewResolvedDate.IsNotNull() && calcStartDateNotReporting < issues.NotReportingToInterviewResolvedDate)
						? issues.NotReportingToInterviewResolvedDate.Value
						: calcStartDateNotReporting;

					var update = new ProcessCandidateIssuesUpdate { PersonId = issues.PersonId };

					var newCount = allLogs.Where(log => log.PersonId == issues.PersonId && log.CandidateApplicationStatus == statusNotReporting && log.ActionedOn >= seekerCheckDate)
						.Sum(log => log.Count);

					if (issues.NotReportingToInterviewCount != newCount)
						update.IssuesCount = newCount;

					if (newCount >= AppSettings.NotReportingForInterviewCountThreshold)
						update.IssuesTriggered = true;

					if (update.IssuesCount.HasValue || update.IssuesTriggered.HasValue)
						notReportingIssues.Add(update);
				}
			});
		}

		public void RecalculateAlienRegistrationIssue(List<ProcessCandidateIssuesRecord> jobSeekersList, List<ProcessCandidateIssuesUpdate> alienRegistrationIssues, List<ProcessCandidateIssuesRecord> jobSeekersToBlock)
		{
			if (!AppSettings.ExpiredAlienCertificationEnabled)
				return;

			jobSeekersList.ForEach(seeker =>
			{
				var resume = seeker.Resume;
				var issues = seeker.Issues;

				if (resume.IsUSCitizen.HasValue && resume.AlienRegExpiryDate.HasValue && resume.IsUSCitizen.Value == false && resume.AlienRegExpiryDate.Value.Date < DateTime.Today && issues.HasExpiredAlienCertificationRegistration != true)
				{
					var update = new ProcessCandidateIssuesUpdate { PersonId = issues.PersonId, IssuesTriggered = true };
					alienRegistrationIssues.Add(update);
					jobSeekersToBlock.Add(seeker);
				}
			});
		}

		/// <summary>
		/// Recalculates the showing low quality matches issues.
		/// </summary>
		/// <param name="jobSeekersList">The list of job seekers.</param>
		/// <param name="lowQualityMatchesIssues">A list to populate with "Not logging in" issues.</param>
		private void RecalculateShowingLowQualityMatchesIssue(List<ProcessCandidateIssuesRecord> jobSeekersList, List<ProcessCandidateIssuesUpdate> lowQualityMatchesIssues)
		{
			// Issue not enabled so don't recalculate
			if (!AppSettings.ShowingLowQualityMatchesEnabled)
				return;

			jobSeekersList = jobSeekersList.Where(seeker => !seeker.Issues.ShowingLowQualityMatchesTriggered).ToList();

			// All issues already triggered so don't recalculate
			if (!jobSeekersList.Any())
				return;

			var calcStartDate = DateTime.Now.Date.AddDays(AppSettings.ShowingLowQualityMatchesDaysThreshold * -1);

			var minPersonId = jobSeekersList.Min(seekers => seekers.Issues.PersonId);
			var maxPersonId = jobSeekersList.Max(seekers => seekers.Issues.PersonId);

			var score = AppSettings.StarRatingMinimumScores[AppSettings.ShowingLowQualityMatchesMinStarThreshold];

			var allPostingMatches = Repositories.Core.Query<PersonPostingMatch>()
				.Where(x => (x.PersonId >= minPersonId && x.PersonId <= maxPersonId) && x.Score <= score && x.CreatedOn > calcStartDate)
				.GroupBy(x => new
				{
					x.PersonId,
					x.CreatedOn
				})
				.Select(x => new
				{
					x.Key.PersonId,
					x.Key.CreatedOn,
					Count = x.Count()
				})
				.ToList();

			jobSeekersList.ForEach(seeker =>
			{
				// Candidate hasn't been a user long enough for this to be an issue
				if (seeker.Candidate.CreatedOn <= calcStartDate)
				{
					var issues = seeker.Issues;

					var seekerCheckDate = (issues.ShowingLowQualityMatchesResolvedDate.IsNotNull() && calcStartDate < issues.ShowingLowQualityMatchesResolvedDate)
						? issues.ShowingLowQualityMatchesResolvedDate.Value
						: calcStartDate;

					var update = new ProcessCandidateIssuesUpdate { PersonId = issues.PersonId };

					var newCount = allPostingMatches.Where(match => match.PersonId == issues.PersonId && match.CreatedOn >= seekerCheckDate)
						.Sum(match => match.Count);

					if (issues.ShowingLowQualityMatchesCount != newCount)
						update.IssuesCount = newCount;

					if (issues.ShowingLowQualityMatchesCount >= AppSettings.ShowingLowQualityMatchesCountThreshold)
						update.IssuesTriggered = true;

					if (update.IssuesCount.HasValue || update.IssuesTriggered.HasValue)
						lowQualityMatchesIssues.Add(update);
				}
			});
		}

		/// <summary>
		/// Recalculates the post hire follow up issues.
		/// </summary>
		/// <param name="issuesList">The list of issues.</param>
		/// <param name="postHireIssues">A list to populate with "Not logging in" issues.</param>
		private void RecalculatePostHireFollowUpIssue(List<Issues> issuesList, List<ProcessCandidateIssuesUpdate> postHireIssues)
		{
			// Issue not enabled so don't recalculate
			if (!AppSettings.PostHireFollowUpEnabled)
				return;

			issuesList = issuesList.Where(issue => !issue.PostHireFollowUpTriggered).ToList();

			// All issues already triggered so don't recalculate
			if (!issuesList.Any())
				return;

			var calcStartDate = DateTime.Now.Date.AddDays(-14);

			var minPersonId = issuesList.Min(issues => issues.PersonId);
			var maxPersonId = issuesList.Max(issues => issues.PersonId);

			// Get max before calcStartDate and all dates after (necause date to check depends on date on issue record which could be later)
			var allLogs = Repositories.Core.Query<ApplicationBasicView>()
				.Where(x => (x.CandidateId >= minPersonId && x.CandidateId <= maxPersonId) && x.ApplicationStatusLastChangedOn <= calcStartDate && x.CandidateApplicationStatus == ApplicationStatusTypes.Hired)
				.GroupBy(x => new
				{
					x.CandidateId
				})
				.Select(x => new
				{
					x.Key.CandidateId,
					ApplicationStatusLastChangedOn = x.Max(y => y.ApplicationStatusLastChangedOn)
				})
				.ToList();

			var otherLogs = Repositories.Core.Query<ApplicationBasicView>()
				.Where(x => (x.CandidateId >= minPersonId && x.CandidateId <= maxPersonId) && x.ApplicationStatusLastChangedOn > calcStartDate && x.CandidateApplicationStatus == ApplicationStatusTypes.Hired)
				.Select(x => new
				{
					x.CandidateId,
					x.ApplicationStatusLastChangedOn
				})
				.ToList();

			allLogs.AddRange(otherLogs);

			issuesList.ForEach(issues =>
			{
				var seekerCheckDate = (issues.PostHireFollowUpResolvedDate.IsNotNull() && calcStartDate < issues.PostHireFollowUpResolvedDate)
					? issues.PostHireFollowUpResolvedDate.Value
					: calcStartDate;

				var lastLogDate = allLogs.Where(x => x.CandidateId == issues.PersonId && x.ApplicationStatusLastChangedOn <= seekerCheckDate)
					.Max(x => x.ApplicationStatusLastChangedOn);

				if (lastLogDate.IsNotNull())
				{
					if (issues.PostHireFollowUpResolvedDate.IsNull() || issues.PostHireFollowUpResolvedDate.Value < lastLogDate.Value.AddDays(14))
					{
						var update = new ProcessCandidateIssuesUpdate { PersonId = issues.PersonId, IssuesTriggered = true };
						postHireIssues.Add(update);
					}
				}
			});
		}

		/// <summary>
		/// Recalculates the not responding to employer invites issues.
		/// </summary>
		/// <param name="issuesList">The list of issues.</param>
		/// <param name="notRespondingToInvitesIssues">A list to populate with "Not responnding to invite" issues.</param>
		private void RecalculateNotRespondingToEmployerInvitesIssue(List<Issues> issuesList, List<ProcessCandidateIssuesUpdate> notRespondingToInvitesIssues)
		{
			// Issue not enabled so don't recalculate
			if (!AppSettings.NotRespondingToEmployerInvitesEnabled)
				return;

			issuesList = issuesList.Where(issue => !issue.NotRespondingToEmployerInvitesTriggered).ToList();

			// All issues already triggered so don't recalculate
			if (!issuesList.Any())
				return;

			var calcStartDate = DateTime.Now.Date.AddDays(AppSettings.NotRespondingToEmployerInvitesDaysThreshold * -1);
			var calcEndDate = DateTime.Now.AddDays(AppSettings.NotRespondingToEmployerInvitesGracePeriodDays * -1);

			var minPersonId = issuesList.Min(issues => issues.PersonId);
			var maxPersonId = issuesList.Max(issues => issues.PersonId);

			var allInvites = Repositories.Core.Query<InviteToApply>()
				.Where(x => x.Viewed == false && (x.PersonId >= minPersonId && x.PersonId <= maxPersonId) && x.CreatedOn > calcStartDate && x.CreatedOn < calcEndDate)
				.GroupBy(x => new
				{
					x.PersonId,
					x.CreatedOn
				})
				.Select(x => new
				{
					x.Key.PersonId,
					x.Key.CreatedOn,
					Count = x.Count()
				})
				.ToList();

			issuesList.ForEach(issues =>
			{
				var seekerCheckDate = (issues.NotRespondingToEmployerInvitesResolvedDate.IsNotNull() && calcStartDate < issues.NotRespondingToEmployerInvitesResolvedDate)
					? issues.NotRespondingToEmployerInvitesResolvedDate.Value
					: calcStartDate;

				var update = new ProcessCandidateIssuesUpdate { PersonId = issues.PersonId };

				var newCount = allInvites.Where(log => log.PersonId == issues.PersonId && log.CreatedOn >= seekerCheckDate)
					.Sum(log => log.Count);

				if (newCount != issues.NotReportingToInterviewCount)
					update.IssuesCount = newCount;

				if (newCount >= AppSettings.NotRespondingToEmployerInvitesCountThreshold)
					update.IssuesTriggered = true;

				if (update.IssuesCount.HasValue || update.IssuesTriggered.HasValue)
					notRespondingToInvitesIssues.Add(update);
			});
		}

		/// <summary>
		/// Recalculates the not logging in issue.
		/// </summary>
		/// <param name="jobSeekersList">The list of job seekers.</param>
		/// <param name="notLoggingIssues">A list to populate with "Not logging in" issues.</param>
		private void RecalculateNotLoggingInIssue(List<ProcessCandidateIssuesRecord> jobSeekersList, List<ProcessCandidateIssuesUpdate> notLoggingIssues)
		{
			// Issue not enabled so don't recalculate
			if (!AppSettings.NotLoggingInEnabled)
				return;

			jobSeekersList = jobSeekersList.Where(jobSeeker => !jobSeeker.Issues.NoLoginTriggered).ToList();

			// All issues already triggered so don't recalculate
			if (!jobSeekersList.Any())
				return;

			var calcStartDate = DateTime.Now.Date.AddDays(AppSettings.NotLoggingInDaysThreshold * -1);

			jobSeekersList.ForEach(jobSeeker =>
			{
				if (jobSeeker.Candidate.CreatedOn <= calcStartDate)
				{
					var issues = jobSeeker.Issues;

					if (issues.NoLoginResolvedDate.IsNotNull() && calcStartDate < issues.NoLoginResolvedDate)
						calcStartDate = issues.NoLoginResolvedDate.Value;

					if (jobSeeker.Candidate.LastLoggedInOn.IsNull() || jobSeeker.Candidate.LastLoggedInOn < calcStartDate)
					{
						var update = new ProcessCandidateIssuesUpdate { PersonId = issues.PersonId, IssuesTriggered = true };
						notLoggingIssues.Add(update);
					}
				}
			});
		}

		/// <summary>
		/// Recalculates the not searching jobs issue.
		/// </summary>
		/// <param name="jobSeekersList">The list of job seekers.</param>
		/// <param name="notSearchingJobsIssues">A list to populate with "Not searching jobs" issues.</param>
		/// <param name="notClickingJobsIssues">A list to populate with "Not clicking on job leads" issues.</param>
		private void RecalculateNotSearchingAndNotClickingIssue(List<ProcessCandidateIssuesRecord> jobSeekersList, List<ProcessCandidateIssuesUpdate> notSearchingJobsIssues, List<ProcessCandidateIssuesUpdate> notClickingJobsIssues)
		{
			// Issue not enabled so don't recalculate
			if (!AppSettings.NotSearchingJobsEnabled && !AppSettings.NotClickingLeadsEnabled)
				return;

			jobSeekersList = jobSeekersList.Where(seeker => !seeker.Issues.NotSearchingJobsTriggered || !seeker.Issues.NotClickingOnLeadsTriggered).ToList();

			// All issues already triggered so don't recalculate
			if (!jobSeekersList.Any())
				return;

			var calcStartDateNotSearching = DateTime.Now.Date.AddDays(AppSettings.NotSearchingJobsDaysThreshold * -1);
			var calcStartDateNotClicking = DateTime.Now.Date.AddDays(AppSettings.NotClickingLeadsDaysThreshold * -1);

			var calcStartDate = calcStartDateNotSearching < calcStartDateNotClicking ? calcStartDateNotSearching : calcStartDateNotClicking;

			var minPersonId = jobSeekersList.Min(seeker => seeker.Issues.PersonId);
			var maxPersonId = jobSeekersList.Max(seeker => seeker.Issues.PersonId);

			var actionTypeIdJobSearch = Repositories.Core.ActionTypes.Where(type => type.Name == ActionTypes.JobSearch.ToString()).Select(type => type.Id).FirstOrDefault();
			var actionTypeIdViewPosting = Repositories.Core.ActionTypes.Where(type => type.Name == ActionTypes.ViewedPostingFromSearch.ToString()).Select(type => type.Id).FirstOrDefault();

			if (actionTypeIdJobSearch == 0 && actionTypeIdViewPosting == 0)
				return;

			var actionTypeIds = new List<long> { actionTypeIdJobSearch, actionTypeIdViewPosting };

			var allEvents = Repositories.Core.ActionEvents
				.Where(x => (x.EntityId >= minPersonId && x.EntityId <= maxPersonId) && actionTypeIds.Contains(x.ActionTypeId) && x.ActionedOn > calcStartDate)
				.GroupBy(x => new
				{
					x.EntityId,
					x.ActionTypeId,
					x.ActionedOn
				})
				.Select(x => new
				{
					x.Key.EntityId,
					x.Key.ActionTypeId,
					x.Key.ActionedOn,
					Count = x.Count()
				})
				.ToList();

			jobSeekersList.ForEach(jobSeeker =>
			{
				var issues = jobSeeker.Issues;

				if (AppSettings.NotSearchingJobsEnabled && !jobSeeker.Issues.NotSearchingJobsTriggered && jobSeeker.Candidate.CreatedOn <= calcStartDateNotSearching)
				{
					var seekerCheckDate = (issues.NotSearchingJobsResolvedDate.IsNotNull() && calcStartDateNotSearching < issues.NotSearchingJobsResolvedDate)
						? issues.NotSearchingJobsResolvedDate.Value
						: calcStartDateNotSearching;

					var count = allEvents.Where(x => x.EntityId == issues.PersonId && x.ActionTypeId == actionTypeIdJobSearch && x.ActionedOn >= seekerCheckDate)
						.Sum(log => log.Count);

					if (count < AppSettings.NotSearchingJobsCountThreshold)
					{
						var update = new ProcessCandidateIssuesUpdate { PersonId = issues.PersonId, IssuesTriggered = true };
						notSearchingJobsIssues.Add(update);
					}
				}

				if (AppSettings.NotClickingLeadsEnabled && !jobSeeker.Issues.NotClickingOnLeadsTriggered && jobSeeker.Candidate.CreatedOn <= calcStartDateNotClicking)
				{
					var seekerCheckDate = (issues.NotClickingOnLeadsResolvedDate.IsNotNull() && calcStartDateNotClicking < issues.NotClickingOnLeadsResolvedDate)
						? issues.NotClickingOnLeadsResolvedDate.Value
						: calcStartDateNotClicking;

					var update = new ProcessCandidateIssuesUpdate { PersonId = issues.PersonId };

					var newCount = allEvents.Where(x => x.EntityId == issues.PersonId && x.ActionTypeId == actionTypeIdViewPosting && x.ActionedOn >= seekerCheckDate)
						.Sum(log => log.Count);

					if (newCount != issues.NotClickingOnLeadsCount)
						update.IssuesCount = newCount;

					if (newCount < AppSettings.NotClickingLeadsCountThreshold)
						update.IssuesTriggered = true;

					if (update.IssuesCount.HasValue || update.IssuesTriggered.HasValue)
						notClickingJobsIssues.Add(update);
				}
			});
		}

		#endregion

		/// <summary>
		/// Determines which job seekers need to be sent emails due to inactivity
		/// </summary>
		/// <param name="jobSeekersList">The list of job seekers.</param>
		/// <param name="jobSeekerEmailsToSend">The job seeker to which emails should be sent.</param>
		/// <param name="jobSeekerDetailsToDisable">The job seekers which need to be disabled.</param>
		public void BuildJobSeekerInactivityEmails(List<ProcessCandidateIssuesRecord> jobSeekersList, List<CandidateBasicViewDto> jobSeekerEmailsToSend, List<JobSeekerIds> jobSeekerDetailsToDisable)
		{
			if (!AppSettings.JobSeekerSendInactivityEmail)
				return;

			jobSeekersList.ForEach(seeker =>
			{
				var candidate = seeker.Candidate;
				var issues = seeker.Issues;

				var checkDate = candidate.LastLoggedInOn ?? candidate.CreatedOn;
				var daysSinceLastLogin = DateTime.Now.Date.Subtract(checkDate).TotalDays;

				if (daysSinceLastLogin >= AppSettings.JobSeekerInactivityEmailLimit)
				{
					if (issues.LastLoggedInDateForAlert != checkDate)
					{
						issues.LastLoggedInDateForAlert = checkDate;
						jobSeekerEmailsToSend.Add(candidate);
					}

					if (daysSinceLastLogin >= AppSettings.JobSeekerInactivityEmailLimit + AppSettings.JobSeekerInactivityEmailGrace)
					{
						if (candidate.Enabled)
						{
							jobSeekerDetailsToDisable.Add(new JobSeekerIds { Id = candidate.Id, UserId = candidate.UserId });
						}
					}
				}
			});
		}
	}

	public class ResumeShortView
	{
		public bool? IsUSCitizen { get; set; }
		public DateTime? AlienRegExpiryDate { get; set; }
	}

	public class ProcessCandidateIssuesRecord
	{
		public CandidateBasicViewDto Candidate { get; set; }
		public ResumeShortView Resume { get; set; }
		public Issues Issues { get; set; }
	}

	public struct ProcessCandidateIssuesUpdate
	{
		public long PersonId;
		public int? IssuesCount;
		public bool? IssuesTriggered;
	}
}
