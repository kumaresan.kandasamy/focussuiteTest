﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Linq;
using Framework.Core;
using Framework.Messaging;

#endregion

namespace Focus.Services.Messages.Handlers
{
	public class InviteJobseekersToApplyHandler : MessageHandlerBase, IMessageHandler<InviteJobseekersToApplyMessage>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="InviteJobseekersToApplyHandler"/> class.
		/// </summary>
		public InviteJobseekersToApplyHandler() : base(null) {}

		/// <summary>
		/// Initializes a new instance of the <see cref="InviteJobseekersToApplyHandler"/> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		public InviteJobseekersToApplyHandler(IRuntimeContext runtimeContext) : base(runtimeContext) {}

		private InviteJobseekersToApplyMessage _message;
		
		/// <summary>
		/// Handles the specified message.
		/// </summary>
		/// <param name="message">The message.</param>
		public void Handle(InviteJobseekersToApplyMessage message)
		{
			_message = message;
			
			_message.InitialiseRuntimeContext(RuntimeContext);

			if(_message.LensPostingId.IsNullOrEmpty())
				return;

			// Get all previous invites for this posting so we do not send invites to jobseekers who have aleady been invited
			var previousInvites = Repositories.Core.InviteToApply.Where(x => x.LensPostingId == _message.LensPostingId).Select(x => x.PersonId).ToList();

			if (message.PersonIdsWithScores.IsNotNullOrEmpty())
			{
				foreach (var personId in _message.PersonIdsWithScores.Keys.Where(personId => previousInvites.All(x => x != personId)))
				{
					Invite(personId, _message.PersonIdsWithScores[personId]);
				}
			}
			else
			{
				foreach (var personId in _message.PersonIds.Where(personId => previousInvites.All(x => x != personId)))
				{
					Invite(personId, 0);
				}
			}
		}

		/// <summary>
		/// Invites the specified person identifier.
		/// </summary>
		/// <param name="personId">The person identifier.</param>
		/// <param name="score">The score.</param>
		private void Invite(long personId, int score)
		{
			var inviteMessage = new InviteJobseekerToApplyMessage
			{
				ActionerId = _message.ActionerId,
				Culture = _message.Culture,
				PersonId = personId,
				Score = score,
				JobId = _message.JobId,
				JobLink = _message.JobLink,
				LensPostingId = _message.LensPostingId,
				SendersName = _message.SendersName,
				SenderEmail = _message.SenderEmail
			};

			Helpers.Messaging.Publish(inviteMessage);
		}
	}
}
