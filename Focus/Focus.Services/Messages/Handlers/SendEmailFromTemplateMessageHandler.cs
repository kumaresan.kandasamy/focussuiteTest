﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using System.Net.Mail;
using System.Text.RegularExpressions;
using Focus.Core;
using Focus.Services.Helpers;
using Framework.Core;
using Framework.Email;
using Framework.Messaging;

#endregion

namespace Focus.Services.Messages.Handlers
{
	public class SendEmailFromTemplateMessageHandler : MessageHandlerBase, IMessageHandler<SendEmailFromTemplateMessage>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="SendEmailMessageHandler"/> class.
		/// </summary>
		public SendEmailFromTemplateMessageHandler() : base(null) {}

		/// <summary>
		/// Initializes a new instance of the <see cref="SendEmailMessageHandler"/> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		public SendEmailFromTemplateMessageHandler(IRuntimeContext runtimeContext) : base(runtimeContext) { }

		/// <summary>
		/// Handles the specified message.
		/// </summary>
		/// <param name="message">The message.</param>
		public void Handle(SendEmailFromTemplateMessage message)
		{
			message.InitialiseRuntimeContext(RuntimeContext);

			var emailTemplate = RuntimeContext.Helpers.Email.GetEmailTemplatePreview(message.TemplateType, message.TemplateData);

			if (message.DetectUrl && message.IsBodyHtml)
			{
				var urlRegex = AppSettings.UrlRegExPattern;
				if (urlRegex.IsNotNullOrEmpty())
				{
					if (urlRegex.StartsWith("^"))
						urlRegex = urlRegex.Substring(1);
					if (urlRegex.EndsWith("$"))
						urlRegex = urlRegex.Substring(0, urlRegex.Length - 1);

					var regex = new Regex(string.Concat("(", urlRegex, ")"));
					emailTemplate.Body = regex.Replace(emailTemplate.Body, @"<a href='$1'>$1</a>");
				}
			}

			RuntimeContext.Helpers.Email.SendEmail(message.To, message.Cc, message.Bcc, emailTemplate.Subject, emailTemplate.Body, message.SendAsync, message.IsBodyHtml, message.Attachment, message.AttachmentName);
		}
	}
}
