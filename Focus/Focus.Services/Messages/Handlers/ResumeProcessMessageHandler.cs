﻿#region Copyright © 2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives


using System.Collections.Generic;
using System.Linq;

using Focus.Core;
using Focus.Core.IntegrationMessages;
using Focus.Services.ServiceImplementations;

using Framework.Core;
using Framework.Messaging;

#endregion

namespace Focus.Services.Messages.Handlers
{
	public class ResumeProcessMessageHandler : MessageHandlerBase, IMessageHandler<ResumeProcessMessage>
	{
		public ResumeProcessMessageHandler() : base(null) { }

		public ResumeProcessMessageHandler(IRuntimeContext runtimeContext) : base(runtimeContext){}

		public void Handle(ResumeProcessMessage message)
		{
			message.InitialiseRuntimeContext(RuntimeContext);

			if (!message.IgnoreLens)
			{
				// Register / Unregister the resume accordingly
				if (message.RegisterResume)
				 RegisterResumeInLens(message.ResumeId);
				else
					UnregisterResumeInLens(message.ResumeId, message.PersonId, message.PersonIds);
			}

			var personIds	= new List<long>();

			if (message.PersonId > 0)
				personIds.Add(message.PersonId);

			if (message.PersonIds.IsNotNullOrEmpty())
				personIds.AddRange(message.PersonIds);

			foreach (var personId in personIds)
			{
				if (!message.IgnoreClient && Helpers.Configuration.AppSettings.IntegrationClient != IntegrationClient.Standalone)
				{
					var actions = new List<AssignJobSeekerActivityRequest>();
					if (message.RegisterResume)
					{
						var actionerType = Repositories.Core.Users.Where(u => u.Id == message.ActionerId).Select(u => u.UserType).FirstOrDefault();
						if (actionerType == UserTypes.Assist)
						{
							var action = Helpers.SelfService.PublishSelfServiceActivity(ActionTypes.ResumePreparationAssistance, message.ActionerId, jobSeekerId: personId, publishIfNoExternalId: true, ignorePublish: true);
							if (action.IsNotNull())
								actions.Add(action);
						}
					}

					if (message.ActionTypesToPublish.IsNotNullOrEmpty())
					{
						message.ActionTypesToPublish.ForEach(actionType =>
						{
							var action = Helpers.SelfService.PublishSelfServiceActivity(actionType, message.ActionerId, jobSeekerId: personId, publishIfNoExternalId: true, ignorePublish: true);
							if (action.IsNotNull())
								actions.Add(action);
						});
					}

					var user = Repositories.Core.Users.First(u => u.PersonId == personId);
					user.PublishUserUpdateIntegrationRequest(message.ActionerId, RuntimeContext, actions);
				}			
			}

			// Generate matches for new resumes
			if (!message.IgnoreLens)
			{
				if (message.PersonIds.IsNullOrEmpty())
					Helpers.JobDevelopment.GeneratePostingMatchesForPerson(message.PersonId, message.Culture);
				else
					message.PersonIds.ForEach(personId => Helpers.JobDevelopment.GeneratePostingMatchesForPerson(personId, message.Culture));
			}	

			Helpers.Messaging.Publish(new ProcessMatchesToRecentPlacementsMessage
			{
				UpdatedJobSeekers = message.PersonIds.IsNotNullOrEmpty()? message.PersonIds : new List<long>{message.PersonId}
			});
		}

		private void RegisterResumeInLens(long resumeId)
		{
			Helpers.Resume.RegisterResume(resumeId);
		}

		private void UnregisterResumeInLens(long resumeId, long personId, List<long> personIds)
		{
			if (personIds.IsNullOrEmpty())
				Helpers.Resume.UnregisterResume(resumeId, personId);
			else
				Helpers.Resume.UnregisterResume(personIds);
		}
	}
}
