﻿#region Copyright © 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Focus.Core;
using Focus.Core.Messages;
using Focus.Core.Messages.AccountService;
using Focus.Data.Core.Entities;
using Focus.Services.ServiceClients;

using Framework.Core;
using Framework.Messaging;

#endregion

namespace Focus.Services.Messages.Handlers
{
  public class IntegrationImportMessageHandler : MessageHandlerBase, IMessageHandler<IntegrationImportMessage>
  {
    private delegate ServiceResponse ImportFunction(IntegrationImportRecord record, IServiceRequest currentRequest, List<string> exceptions, out long? focusId);

    private ImportFunction _importFunction;

    /// <summary>
    /// Initializes a new instance of the <see cref="IntegrationImportMessageHandler"/> class.
		/// </summary>
		public IntegrationImportMessageHandler() : base(null) {}

		/// <summary>
    /// Initializes a new instance of the <see cref="IntegrationImportMessageHandler"/> class.
		/// </summary>
    public IntegrationImportMessageHandler(IRuntimeContext runtimeContext) : base(runtimeContext) { }

    /// <summary>
    /// Handles the specified message.
    /// </summary>
    /// <param name="message">The message.</param>
    public void Handle(IntegrationImportMessage message)
    {
      message.InitialiseRuntimeContext(RuntimeContext);

      switch (message.RecordType)
      {
        case IntegrationImportRecordType.Employer:
          _importFunction = ImportEmployerRequest;
          break;

        case IntegrationImportRecordType.JobSeeker:
          _importFunction = ImportJobSeekerRequest;
          break;
      }
     
      var query = Repositories.Core.IntegrationImportRecords
                              .Where(record => record.RecordType == message.RecordType && record.Status == IntegrationImportRecordStatus.ToProcess)
                              .Take(AppSettings.IntegrationImportBatchSize);

      var currentRequest = RuntimeContext.CurrentRequest;

      var process = true;
      while (process)
      {
        var importRecords = query.ToList();

        importRecords.ForEach(record =>
        {
          record.Status = IntegrationImportRecordStatus.Processing;
        });

        Repositories.Core.SaveChanges();

        Parallel.ForEach(importRecords, record =>
        {
          record.ImportStartTime = DateTime.UtcNow;

          var exceptions = new List<string>();

          long? focusId;
          var response = _importFunction(record, currentRequest, exceptions, out focusId);

          record.FocusId = focusId;
          record.ImportEndTime = DateTime.UtcNow;
          record.RequestId = response.CorrelationId;

          if (response.Acknowledgement == AcknowledgementType.Success)
          {
            record.Status = IntegrationImportRecordStatus.Imported;
            record.Details = string.Empty;
          }
          else
          {
            record.Status = IntegrationImportRecordStatus.Failed;

            record.Details = exceptions.Any()
              ? string.Format("Message: {0}\nException: {1}", response.Message, string.Join("\nInner Exception: ", exceptions))
              : string.Format("Message: {0}", response.Message);
          }
        });

        Repositories.Core.SaveChanges(true);

        if (importRecords.Count() < AppSettings.IntegrationImportBatchSize)
          process = false;
      }
    }

    /// <summary>
    /// Imports a single employer record based on the External Id
    /// </summary>
    /// <param name="record">The record holding details of the Id to import</param>
    /// <param name="currentRequest">The current service request from which to copy the details</param>
    /// <param name="exceptions">Will be populated with any exceptions that occured.</param>
    /// <param name="focusId">The id of the Employer in Focus</param>
    /// <returns></returns>
    private ServiceResponse ImportEmployerRequest(IntegrationImportRecord record, IServiceRequest currentRequest, List<string> exceptions, out long? focusId)
    {
      var importRequest = new ImportEmployerAndArtifactsRequest
      {
        EmployerExternalId = record.ExternalId
      };
      PrepareRequest(importRequest, currentRequest);

      var accountClient = new AccountServiceClient();
      var importResponse = accountClient.ImportEmployerAndArtifacts(importRequest);

      focusId = importResponse.FocusId;

      if (importResponse.Exceptions.IsNotNullOrEmpty())
        exceptions.AddRange(importResponse.Exceptions);

      return importResponse;
    }

    /// <summary>
    /// Imports a single employer job seeker based on the External Id
    /// </summary>
    /// <param name="record">The record holding details of the Id to import</param>
    /// <param name="currentRequest">The current service request from which to copy the details</param>
    /// <param name="exceptions">Will be populated with any exceptions that occured.</param>
    /// <param name="focusId">The id of the JobSeeker (Person) in Focus</param>
    private ServiceResponse ImportJobSeekerRequest(IntegrationImportRecord record, IServiceRequest currentRequest, List<string> exceptions, out long? focusId)
    {
      var importRequest = new ImportJobSeekerAndResumesRequest
      {
        JobSeekerExternalId = record.ExternalId
      };
      PrepareRequest(importRequest, currentRequest);

      var accountClient = new AccountServiceClient();
      var importResponse = accountClient.ImportJobSeekerAndResumes(importRequest);

      focusId = importResponse.FocusId;

      if (importResponse.Exceptions.IsNotNullOrEmpty())
        exceptions.AddRange(importResponse.Exceptions);

      return importResponse;
    }

    /// <summary>
    /// Sets the standard fields in the request object
    /// </summary>
    /// <param name="request">The request object</param>
    /// <param name="currentRequest">The current service request from which to copy the details</param>
    private void PrepareRequest(ServiceRequest request, IServiceRequest currentRequest)
    {
      request.ClientTag = currentRequest.ClientTag;
      request.SessionId = currentRequest.SessionId;
      request.RequestId = Guid.NewGuid();
      request.UserContext = currentRequest.UserContext;
      request.VersionNumber = Constants.SystemVersion;
    }
  }
}
