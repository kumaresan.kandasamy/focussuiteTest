﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Linq;

using Focus.Core;
using Focus.Core.IntegrationMessages;

using Framework.Messaging;

#endregion

namespace Focus.Services.Messages.Handlers
{
	public class UnregisterPostingMessageHandler : MessageHandlerBase, IMessageHandler<UnregisterPostingMessage>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="UnregisterPostingMessageHandler"/> class.
		/// </summary>
		public UnregisterPostingMessageHandler() : base(null) {}

		/// <summary>
		/// Initializes a new instance of the <see cref="UnregisterPostingMessageHandler"/> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		public UnregisterPostingMessageHandler(IRuntimeContext runtimeContext) : base(runtimeContext) { }

		/// <summary>
		/// Handles the specified message.
		/// </summary>
		/// <param name="message">The message.</param>
		public void Handle(UnregisterPostingMessage message)
		{
			// Convert to process posting message to avoid upgrade issues
			var postingMessage = new ProcessPostingMessage
			{
				ActionerId = message.ActionerId,
				Culture = message.Culture,
				PostingId = message.PostingId,
				RegisterPosting = false,
				ExternalAdminId = message.ExternalAdminId,
				ExternalOfficeId = message.ExternalOfficeId,
				ExternalPassword = message.ExternalPassword,
				ExternalUsername = message.ExternalUsername
			};

			// Set priority as direct to ensure it is picked up straightaway
			Helpers.Messaging.Publish(postingMessage, MessagePriority.Direct);
		}
	}
}
