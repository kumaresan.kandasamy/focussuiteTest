﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Models.Career;
using Focus.Data.Configuration.Entities;
using Focus.Data.Core.Entities;
using Focus.Data.Library.Entities;
using Focus.Data.Report.Entities;
using Focus.Services.DtoMappers;
using Focus.Services.Mappers;
using Job = Focus.Core.Models.Career.Job;

using Framework.Core;
using Framework.Messaging;

using Mindscape.LightSpeed;
using Mindscape.LightSpeed.Querying;

#endregion

namespace Focus.Services.Messages.Handlers
{
  public class UpdateReportingMessageHandler : MessageHandlerBase, IMessageHandler<UpdateReportingMessage>
  {
    private int _actionHistoryDays = -120;

    private delegate void ReportEntityFunction(long entityId, long? additionalEntityId);
    private delegate void JobSeekerActionEventFunction(JobSeekerAction reportRecord, ActionEvent actionEvent, long userId, JobSeekerSurveyDto surveyRecord);
    private delegate void JobOrderActionEventFunction(JobOrderAction reportRecord, ActionEvent actionEvent, InviteToApplyDto inviteToApply);
    private delegate void BusinessUnitActionEventFunction(EmployerAction reportRecord);

    private static Dictionary<long, ActionEventDetailsForJobSeeker> _actionEventFunctionsForJobSeeker;
    private static Dictionary<long, ActionEventDetailsForJobOrder> _actionEventFunctionsForJobOrder;
    private static Dictionary<long, ActionEventDetailsForBusinessUnit> _actionEventFunctionsForBusinessUnit;
    private static List<UserTypes?> _validJobSeekerUserTypes;
    private static Dictionary<long, ActivityDetails> _activityDetailLookup;
    private static long? _assignActivityToCandidateTypeId;

    /// <summary>
    /// Gets a lookup for supported Action Events for Job Seekers
    /// </summary>
    private Dictionary<long, ActionEventDetailsForJobSeeker> ActionEventFunctionsForJobSeeker
    {
      get
      {
        if (_actionEventFunctionsForJobSeeker.IsNull())
        {
          var validActionTypes = new Dictionary<string, ActionEventDetailsForJobSeeker>
          {
            {
              ActionTypes.ShowNotWhatYoureLookingFor.ToString(), 
              new ActionEventDetailsForJobSeeker(ActionEventDetailsType.Person, (record, action, userId, survey) => { record.UsedOnlineResumeHelp++; }, true)
            },
            {
              ActionTypes.LogIn.ToString(), 
              new ActionEventDetailsForJobSeeker(ActionEventDetailsType.User, (record, action, userId, survey) => { record.Logins++; }, true)
            },
            {
              ActionTypes.ViewJobDetails.ToString(), 
              new ActionEventDetailsForJobSeeker(ActionEventDetailsType.Person, (record, action, userId, survey) => { record.PostingsViewed++; }, true)
            },
            {
              ActionTypes.ReferralRequest.ToString(), 
              new ActionEventDetailsForJobSeeker(ActionEventDetailsType.Application, (record, action, userId, survey) => { record.ReferralRequests++; }, true)
            },
            {
              ActionTypes.CreateCandidateApplication.ToString(), 
              new ActionEventDetailsForJobSeeker(ActionEventDetailsType.Application, (record, action, userId, survey) => { record.StaffReferrals++; record.NewApplicant++; })
            },
            {
              ActionTypes.SaveApplication.ToString(), 
              new ActionEventDetailsForJobSeeker(ActionEventDetailsType.Application, (record, action, userId, survey) => { if (action.AdditionalDetails == ApplicationStatusTypes.NewApplicant.ToString()) record.NewApplicant++; })
            },
            {
              ActionTypes.SelfReferral.ToString(), 
              new ActionEventDetailsForJobSeeker(ActionEventDetailsType.Application, (record, action, userId, survey) => { record.SelfReferrals++; }, true)
            },
            {
              ActionTypes.ExternalReferral.ToString(), 
              new ActionEventDetailsForJobSeeker(ActionEventDetailsType.EntityIdAdditional01Person, IncrementActionCountForExternalReferrals, true)
            },
						{
              ActionTypes.ExternalStaffReferral.ToString(), 
              new ActionEventDetailsForJobSeeker(ActionEventDetailsType.EntityIdAdditional01Person, IncrementActionCountForExternalReferrals, false)
            },
            {
              ActionTypes.SavePostingSavedSearch.ToString(), 
              new ActionEventDetailsForJobSeeker(ActionEventDetailsType.SavedSearch, (record, action, userId, survey) => { record.SavedJobAlerts++; }, true)
            },
            {
              ActionTypes.JobSearchHighGrowth.ToString(), 
              new ActionEventDetailsForJobSeeker(ActionEventDetailsType.Person, (record, action, userId, survey) => { record.TargetingHighGrowthSectors++; }, true)
            },
            {
              ActionTypes.SaveNote.ToString(), 
              new ActionEventDetailsForJobSeeker(ActionEventDetailsType.NoteReminder, (record, action, userId, survey) => { record.NotesAdded++; })
            },
            {
              ActionTypes.AddJobSeekersToList.ToString(), 
              new ActionEventDetailsForJobSeeker(ActionEventDetailsType.EntityIdAdditional01Person, (record, action, userId, survey) => { record.AddedToLists++; })
            },
            {
              ActionTypes.AssignActivityToCandidate.ToString(), 
              new ActionEventDetailsForJobSeeker(ActionEventDetailsType.Person, (record, action, userId, survey) => { record.ActivitiesAssigned++; } )
            },
            {
              ActionTypes.JobSeekerEmailed.ToString(),
              new ActionEventDetailsForJobSeeker(ActionEventDetailsType.Person, (record, action, userId, survey) => { record.EmailsSent++; } )
            },
            {
              ActionTypes.UpdateApplicationStatusToHired.ToString(), 
              new ActionEventDetailsForJobSeeker(ActionEventDetailsType.Application, (record, action, userId, survey) => { record.Hired++; })
            },
            {
              ActionTypes.UpdateApplicationStatusToNotHired.ToString(), 
              new ActionEventDetailsForJobSeeker(ActionEventDetailsType.Application, (record, action, userId, survey) => { record.NotHired++; })
            },
            {
              ActionTypes.UpdateApplicationStatusToDidNotApply.ToString(),
              new ActionEventDetailsForJobSeeker(ActionEventDetailsType.Application, (record, action, userId, survey) => { record.FailedToApply++; })
            },
            {
              ActionTypes.UpdateApplicationStatusToFailedToShow.ToString(), 
              new ActionEventDetailsForJobSeeker(ActionEventDetailsType.Application, (record, action, userId, survey) => { record.FailedToReportToInterview++; })
            },
            {
              ActionTypes.UpdateApplicationStatusToInterviewDenied.ToString(), 
              new ActionEventDetailsForJobSeeker(ActionEventDetailsType.Application, (record, action, userId, survey) => { record.InterviewDenied++; })
            },
            {
              ActionTypes.UpdateApplicationStatusToInterviewScheduled.ToString(), 
              new ActionEventDetailsForJobSeeker(ActionEventDetailsType.Application, (record, action, userId, survey) => { record.InterviewScheduled++; })
            },
            {
              ActionTypes.UpdateApplicationStatusToNewApplicant.ToString(), 
              new ActionEventDetailsForJobSeeker(ActionEventDetailsType.Application, (record, action, userId, survey) => { record.NewApplicant++; })
            },
            {
              ActionTypes.UpdateApplicationStatusToRecommended.ToString(),
              new ActionEventDetailsForJobSeeker(ActionEventDetailsType.Application, (record, action, userId, survey) => { record.Recommended++; })
            },
            {
              ActionTypes.UpdateApplicationStatusToRefusedOffer.ToString(), 
              new ActionEventDetailsForJobSeeker(ActionEventDetailsType.Application, (record, action, userId, survey) => { record.RefusedOffer++; })
            },
            { 
              ActionTypes.UpdateApplicationStatusToUnderConsideration.ToString(), 
              new ActionEventDetailsForJobSeeker(ActionEventDetailsType.Application, (record, action, userId, survey) => { record.UnderConsideration++; }) 
            },
            { 
              ActionTypes.UpdateApplicationStatusToFailedToReportToJob.ToString(), 
              new ActionEventDetailsForJobSeeker(ActionEventDetailsType.Application, (record, action, userId, survey) => { record.FailedToReportToJob++; }) 
            },
            { 
              ActionTypes.UpdateApplicationStatusToFailedToRespondToInvitation.ToString(), 
              new ActionEventDetailsForJobSeeker(ActionEventDetailsType.Application, (record, action, userId, survey) => { record.FailedToRespondToInvitation++; }) 
            },
            { 
              ActionTypes.UpdateApplicationStatusToFoundJobFromOtherSource.ToString(), 
              new ActionEventDetailsForJobSeeker(ActionEventDetailsType.Application, (record, action, userId, survey) => { record.FoundJobFromOtherSource++; }) 
            },
            { 
              ActionTypes.UpdateApplicationStatusToJobAlreadyFilled.ToString(), 
              new ActionEventDetailsForJobSeeker(ActionEventDetailsType.Application, (record, action, userId, survey) => { record.JobAlreadyFilled++; }) 
            },
            { 
              ActionTypes.UpdateApplicationStatusToNotQualified.ToString(), 
              new ActionEventDetailsForJobSeeker(ActionEventDetailsType.Application, (record, action, userId, survey) => { record.NotQualified++; }) 
            },
            { 
              ActionTypes.UpdateApplicationStatusToNotYetPlaced.ToString(), 
              new ActionEventDetailsForJobSeeker(ActionEventDetailsType.Application, (record, action, userId, survey) => { record.NotYetPlaced++; }) 
            },
            { 
              ActionTypes.UpdateApplicationStatusToRefusedReferral.ToString(), 
              new ActionEventDetailsForJobSeeker(ActionEventDetailsType.Application, (record, action, userId, survey) => { record.RefusedReferral++; }) 
            },
            {
              ActionTypes.ApproveCandidateReferral.ToString(),
              new ActionEventDetailsForJobSeeker(ActionEventDetailsType.Application, (record, action, userId, survey) => { record.ReferralsApproved++; })
            },
            {
              ActionTypes.FindJobsForSeeker.ToString(),
              new ActionEventDetailsForJobSeeker(ActionEventDetailsType.Person, (record, action, userId, survey) => { record.FindJobsForSeeker++; })
            },
            {
              ActionTypes.RequestFollowUp.ToString(),
              new ActionEventDetailsForJobSeeker(ActionEventDetailsType.Person, (record, action, userId, survey) => { record.FollowUpIssues++; })
            },
            {
              ActionTypes.MarkCandidateIssuesResolved.ToString(),
              new ActionEventDetailsForJobSeeker(ActionEventDetailsType.Person, (record, action, userId, survey) => { record.IssuesResolved++; })
            },
            {
              ActionTypes.SaveJobSeekerSurvey.ToString(), 
              new ActionEventDetailsForJobSeeker(ActionEventDetailsType.EntityIdAdditional01Person, IncrementActionCountForJobSeekerSurvey, true, true)
            }
          };

          var validActionTypeNames = validActionTypes.Keys.ToList();

          // Add action type name to each type
          validActionTypeNames.ForEach(typeName =>
          {
            var actionType = validActionTypes[typeName];
            actionType.ActionTypeName = typeName;
            validActionTypes[typeName] = actionType;
          });

          var actionTypes = (from actionType in Repositories.Core.ActionTypes
                             select actionType.AsDto()).ToList();

          if (actionTypes.IsNullOrEmpty())
            _actionEventFunctionsForJobSeeker = new Dictionary<long, ActionEventDetailsForJobSeeker>();
          else
            _actionEventFunctionsForJobSeeker = actionTypes.Where(a => validActionTypeNames.Contains(a.Name))
                                                           .ToDictionary(a => a.Id.GetValueOrDefault(), a => validActionTypes[a.Name]);

          // Some actions may not exist
          var actionTypesToAdd = validActionTypeNames.Where(name => actionTypes.All(type => name != type.Name)).ToList();
          foreach (var validActionTypeName in actionTypesToAdd)
          {
            var actionType = new ActionType { Name = validActionTypeName, AssistAction = false };
            Repositories.Core.Add(actionType);
            _actionEventFunctionsForJobSeeker.Add(actionType.Id, validActionTypes[validActionTypeName]);
          }

          Repositories.Core.SaveChanges(true);
        }

        return _actionEventFunctionsForJobSeeker;
      }
    }

    /// <summary>
    /// Gets a lookup for supported Action Events for Job Orders
    /// </summary>
    private Dictionary<long, ActionEventDetailsForJobOrder> ActionEventFunctionsForJobOrder
    {
      get
      {
        if (_actionEventFunctionsForJobOrder.IsNull())
        {
          var validActionTypes = new Dictionary<string, ActionEventDetailsForJobOrder>
          {
            {
              ActionTypes.CreateCandidateApplication.ToString(), 
              new ActionEventDetailsForJobOrder(ActionEventDetailsType.EntityIdAdditional01Job, (record, action, invite) => { record.StaffReferrals++; })
            },
            {
              ActionTypes.SelfReferral.ToString(), 
              new ActionEventDetailsForJobOrder(ActionEventDetailsType.Application, (record, action, invite) => { record.SelfReferrals++; })
            },
            {
              ActionTypes.ReferralRequest.ToString(), 
              new ActionEventDetailsForJobOrder(ActionEventDetailsType.Application, (record, action, invite) => { record.ReferralsRequested++; })
            },
            {
              ActionTypes.InviteJobSeekerToApply.ToString(),
              new ActionEventDetailsForJobOrder(ActionEventDetailsType.Job, (record, action, invite) => { record.EmployerInvitationsSent++; })
            },
            {
              ActionTypes.ViewJobDetails.ToString(),
              new ActionEventDetailsForJobOrder(ActionEventDetailsType.EntityIdAdditional01Job, IncrementActionCountForInvitedJobSeekersViewed, true)
            },
            {
              ActionTypes.RegisterHowToApply.ToString(),
              new ActionEventDetailsForJobOrder(ActionEventDetailsType.Job, IncrementActionCountForInvitedJobSeekersClicked, true)
            },
            {
              ActionTypes.UpdateApplicationStatusToHired.ToString(), 
              new ActionEventDetailsForJobOrder(ActionEventDetailsType.EntityIdAdditional01Job, (record, action, invite) => { record.ApplicantsHired++; })
            },
            {
              ActionTypes.UpdateApplicationStatusToNotHired.ToString(), 
              new ActionEventDetailsForJobOrder(ActionEventDetailsType.EntityIdAdditional01Job, (record, action, invite) => { record.ApplicantsNotHired++; })
            },
            {
              ActionTypes.UpdateApplicationStatusToDidNotApply.ToString(),
              new ActionEventDetailsForJobOrder(ActionEventDetailsType.EntityIdAdditional01Job, (record, action, invite) => { record.ApplicantsDidNotApply++; })
            },
            {
              ActionTypes.UpdateApplicationStatusToFailedToShow.ToString(), 
              new ActionEventDetailsForJobOrder(ActionEventDetailsType.EntityIdAdditional01Job, (record, action, invite) => { record.ApplicantsFailedToShow++; })
            },
            {
              ActionTypes.UpdateApplicationStatusToInterviewDenied.ToString(), 
              new ActionEventDetailsForJobOrder(ActionEventDetailsType.EntityIdAdditional01Job, (record, action, invite) => { record.ApplicantsDeniedInterviews++; })
            },
            {
              ActionTypes.UpdateApplicationStatusToInterviewScheduled.ToString(), 
              new ActionEventDetailsForJobOrder(ActionEventDetailsType.EntityIdAdditional01Job, (record, action, invite) => { record.ApplicantsInterviewed++; })
            },
            {
              ActionTypes.UpdateApplicationStatusToNewApplicant.ToString(), 
              new ActionEventDetailsForJobOrder(ActionEventDetailsType.EntityIdAdditional01Job, (record, action, invite) => { record.NewApplicant++; })
            },
            {
              ActionTypes.UpdateApplicationStatusToRecommended.ToString(),
              new ActionEventDetailsForJobOrder(ActionEventDetailsType.EntityIdAdditional01Job, (record, action, invite) => { record.ApplicantRecommended++; })
            },
            {
              ActionTypes.UpdateApplicationStatusToRefusedOffer.ToString(), 
              new ActionEventDetailsForJobOrder(ActionEventDetailsType.EntityIdAdditional01Job, (record, action, invite) => { record.JobOffersRefused++; })
            },
            { 
              ActionTypes.UpdateApplicationStatusToUnderConsideration.ToString(), 
              new ActionEventDetailsForJobOrder(ActionEventDetailsType.EntityIdAdditional01Job, (record, action, invite) => { record.UnderConsideration++; }) 
            },
            { 
              ActionTypes.UpdateApplicationStatusToFailedToReportToJob.ToString(), 
              new ActionEventDetailsForJobOrder(ActionEventDetailsType.EntityIdAdditional01Job, (record, action, invite) => { record.FailedToReportToJob++; }) 
            },
            { 
              ActionTypes.UpdateApplicationStatusToFailedToRespondToInvitation.ToString(), 
              new ActionEventDetailsForJobOrder(ActionEventDetailsType.EntityIdAdditional01Job, (record, action, invite) => { record.FailedToRespondToInvitation++; }) 
            },
            { 
              ActionTypes.UpdateApplicationStatusToFoundJobFromOtherSource.ToString(), 
              new ActionEventDetailsForJobOrder(ActionEventDetailsType.EntityIdAdditional01Job, (record, action, invite) => { record.FoundJobFromOtherSource++; }) 
            },
            { 
              ActionTypes.UpdateApplicationStatusToJobAlreadyFilled.ToString(), 
              new ActionEventDetailsForJobOrder(ActionEventDetailsType.EntityIdAdditional01Job, (record, action, invite) => { record.JobAlreadyFilled++; }) 
            },
            { 
              ActionTypes.UpdateApplicationStatusToNotQualified.ToString(), 
              new ActionEventDetailsForJobOrder(ActionEventDetailsType.EntityIdAdditional01Job, (record, action, invite) => { record.NotQualified++; }) 
            },
            { 
              ActionTypes.UpdateApplicationStatusToNotYetPlaced.ToString(), 
              new ActionEventDetailsForJobOrder(ActionEventDetailsType.EntityIdAdditional01Job, (record, action, invite) => { record.ApplicantsNotYetPlaced++; }) 
            },
            { 
              ActionTypes.UpdateApplicationStatusToRefusedReferral.ToString(), 
              new ActionEventDetailsForJobOrder(ActionEventDetailsType.EntityIdAdditional01Job, (record, action, invite) => { record.RefusedReferral++; }) 
            }
          };

          var validActionTypeNames = validActionTypes.Keys.ToList();

          var actionTypes = (from actionType in Repositories.Core.ActionTypes
                             select actionType.AsDto()).ToList();

          if (actionTypes.IsNullOrEmpty())
            _actionEventFunctionsForJobOrder = new Dictionary<long, ActionEventDetailsForJobOrder>();
          else
            _actionEventFunctionsForJobOrder = actionTypes.Where(a => validActionTypeNames.Contains(a.Name))
                                                           .ToDictionary(a => a.Id.GetValueOrDefault(), a => validActionTypes[a.Name]);

          // Some actions may not exist
          var actionTypesToAdd = validActionTypeNames.Where(name => actionTypes.All(type => name != type.Name)).ToList();
          foreach (var validActionTypeName in actionTypesToAdd)
          {
            var actionType = new ActionType { Name = validActionTypeName, AssistAction = false };
            Repositories.Core.Add(actionType);
            _actionEventFunctionsForJobOrder.Add(actionType.Id, validActionTypes[validActionTypeName]);
          }

          Repositories.Core.SaveChanges(true);
        }

        return _actionEventFunctionsForJobOrder;
      }
    }

    /// <summary>
    /// Gets a lookup for supported Action Events for Business Units
    /// </summary>
    private Dictionary<long, ActionEventDetailsForBusinessUnit> ActionEventFunctionsForBusinessUnit
    {
      get
      {
        if (_actionEventFunctionsForBusinessUnit.IsNull())
        {
          var validActionTypes = new Dictionary<string, ActionEventDetailsForBusinessUnit>
          {
            {
              ActionTypes.SaveJob.ToString(), 
              new ActionEventDetailsForBusinessUnit(ActionEventDetailsType.Job, record => { record.JobOrdersEdited++; })
            },
            {
              ActionTypes.UpdateApplicationStatusToInterviewScheduled.ToString(), 
              new ActionEventDetailsForBusinessUnit(ActionEventDetailsType.EntityIdAdditional01Job, record => { record.JobSeekersInterviewed++; })
            },
            {
              ActionTypes.UpdateApplicationStatusToHired.ToString(), 
              new ActionEventDetailsForBusinessUnit(ActionEventDetailsType.EntityIdAdditional01Job, record => { record.JobSeekersHired++; })
            },
            {
              ActionTypes.CreateJob.ToString(), 
              new ActionEventDetailsForBusinessUnit(ActionEventDetailsType.JobOrAdditionalBusinessUnit, record => { record.JobOrdersCreated++; })
            },
            {
              ActionTypes.DuplicateJob.ToString(), 
              new ActionEventDetailsForBusinessUnit(ActionEventDetailsType.JobOrAdditionalBusinessUnit, record => { record.JobOrdersCreated++; })
            },
            {
              ActionTypes.PostJob.ToString(), 
              new ActionEventDetailsForBusinessUnit(ActionEventDetailsType.Job, record => { record.JobOrdersPosted++; })
            },
            {
              ActionTypes.HoldJob.ToString(), 
              new ActionEventDetailsForBusinessUnit(ActionEventDetailsType.Job, record => { record.JobOrdersPutOnHold++; })
            },
            {
              ActionTypes.RefreshJob.ToString(), 
              new ActionEventDetailsForBusinessUnit(ActionEventDetailsType.Job, record => { record.JobOrdersRefreshed++; })
            },
            {
              ActionTypes.CloseJob.ToString(), 
              new ActionEventDetailsForBusinessUnit(ActionEventDetailsType.Job, record => { record.JobOrdersClosed++; })
            },
            {
              ActionTypes.UpdateApplicationStatusToNotHired.ToString(), 
              new ActionEventDetailsForBusinessUnit(ActionEventDetailsType.EntityIdAdditional01Job, record => { record.JobSeekersNotHired++; })
            },
            {
              ActionTypes.InviteJobSeekerToApply.ToString(),
              new ActionEventDetailsForBusinessUnit(ActionEventDetailsType.Job, record => { record.InvitationsSent++; })
            },
            {
              ActionTypes.SelfReferral.ToString(), 
              new ActionEventDetailsForBusinessUnit(ActionEventDetailsType.Application, record => { record.SelfReferrals++; })
            },
            {
              ActionTypes.CreateCandidateApplication.ToString(), 
              new ActionEventDetailsForBusinessUnit(ActionEventDetailsType.Application, record => { record.StaffReferrals++; })
            }
          };

          var validActionTypeNames = validActionTypes.Keys.ToList();

          var actionTypes = (from actionType in Repositories.Core.ActionTypes
                             select actionType.AsDto()).ToList();

          if (actionTypes.IsNullOrEmpty())
            _actionEventFunctionsForBusinessUnit = new Dictionary<long, ActionEventDetailsForBusinessUnit>();
          else
            _actionEventFunctionsForBusinessUnit = actionTypes.Where(a => validActionTypeNames.Contains(a.Name))
                                                           .ToDictionary(a => a.Id.GetValueOrDefault(), a => validActionTypes[a.Name]);

          // Some actions may not exist
          var actionTypesToAdd = validActionTypeNames.Where(name => actionTypes.All(type => name != type.Name)).ToList();
          foreach (var validActionTypeName in actionTypesToAdd)
          {
            var actionType = new ActionType { Name = validActionTypeName, AssistAction = false };
            Repositories.Core.Add(actionType);
            _actionEventFunctionsForBusinessUnit.Add(actionType.Id, validActionTypes[validActionTypeName]);
          }

          Repositories.Core.SaveChanges(true);
        
        }

        return _actionEventFunctionsForBusinessUnit;
      }
    }

    public long AssignActivityToCandidateTypeId
    {
      get
      {
        if (_assignActivityToCandidateTypeId.IsNull())
          _assignActivityToCandidateTypeId = Repositories.Core.ActionTypes.Where(a => a.Name == ActionTypes.AssignActivityToCandidate.ToString()).Select(a => a.Id).FirstOrDefault();

        return _assignActivityToCandidateTypeId.GetValueOrDefault(0);
      }
    }

    /// <summary>
    /// Valid User Types for job seekers
    /// </summary>
    private List<UserTypes?> ValidJobSeekerUserTypes
    {
      get
      {
        if (_validJobSeekerUserTypes.IsNull())
        {
          _validJobSeekerUserTypes = new List<UserTypes?>
          {
            UserTypes.Career,
            UserTypes.Explorer,
            UserTypes.Career | UserTypes.Explorer
          };
        }
        return _validJobSeekerUserTypes;
      }
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="UpdateReportingMessageHandler"/> class.
    /// </summary>
    public UpdateReportingMessageHandler() : base(null) { }

		/// <summary>
		/// Initializes a new instance of the <see cref="UpdateReportingMessageHandler"/> class.
		/// </summary>
    public UpdateReportingMessageHandler(IRuntimeContext runtimeContext) : base(runtimeContext) { }

    /// <summary>
    /// Handles the specified message.
    /// </summary>
    /// <param name="message">The message.</param>
    public void Handle(UpdateReportingMessage message)
    {
      message.InitialiseRuntimeContext(RuntimeContext);

      _actionHistoryDays = 0 - Math.Abs(AppSettings.ReportActionDaysLimit);

      ReportEntityFunction updateFunction, deleteFunction = null;

      var resetStaffMembers = false;
      var resetJobSeekers = false;
      var resetJobOrders = false;
      var resetBusinessUnits = false;

      switch (message.EntityType)
      {
        case ReportEntityType.Employer:
          updateFunction = UpdateBusinessUnit;
          deleteFunction = DeleteBusinessUnit;

          if (message.EntityIds.IsNullOrEmpty())
          {
            resetBusinessUnits = true;

            if (message.ResetAll)
            {
              var deleteEmployerQuery = new Query(typeof(EmployerAction));
              Repositories.Report.Remove(deleteEmployerQuery);

              deleteEmployerQuery = new Query(typeof(EmployerOffice));
              Repositories.Report.Remove(deleteEmployerQuery);

              deleteEmployerQuery = new Query(typeof (Data.Report.Entities.Employer));
              Repositories.Report.Remove(deleteEmployerQuery);

              Repositories.Report.SaveChanges(true);
            }

            message.EntityIds = GetAllBusinessUnits();
          }
          break;

        case ReportEntityType.JobOrder:
          updateFunction = UpdateJobOrder;
          deleteFunction = DeleteJobOrder;

          if (message.EntityIds.IsNullOrEmpty())
          {
            resetJobOrders = true;

            if (message.ResetAll)
            {
              var deleteJobQuery = new Query(typeof (JobOrderAction));
              Repositories.Report.Remove(deleteJobQuery);

              deleteJobQuery = new Query(typeof(JobOrderOffice));
              Repositories.Report.Remove(deleteJobQuery);

              deleteJobQuery = new Query(typeof(JobOrder));
              Repositories.Report.Remove(deleteJobQuery);

              Repositories.Report.SaveChanges(true);
            }

            message.EntityIds = GetAllJobOrders();
          }
          break;

        case ReportEntityType.Person:
          updateFunction = UpdateRecordForPerson;
          deleteFunction = DeleteRecordForPerson;
          break;

        case ReportEntityType.StaffMember:
          updateFunction = UpdateStaffMember;
          deleteFunction = DeleteStaffMember;

          if (message.EntityIds.IsNullOrEmpty())
          {
            resetStaffMembers = true;

            if (message.ResetAll)
            {
              var deleteStaffMemberQuery = new Query(typeof(StaffMemberOffice));
              Repositories.Report.Remove(deleteStaffMemberQuery);

              deleteStaffMemberQuery = new Query(typeof(StaffMemberCurrentOffice));
              Repositories.Report.Remove(deleteStaffMemberQuery);

              Repositories.Report.SaveChanges(true);
            }

            message.EntityIds = GetAllStaffMembers();
          }
          break;

        case ReportEntityType.JobSeeker:
          updateFunction = UpdateJobSeeker;
          deleteFunction = DeleteJobSeeker;

          if (message.EntityIds.IsNullOrEmpty())
          {
            resetJobSeekers = true;

            if (message.ResetAll)
            {
              var deleteJobSeekerQuery = new Query(typeof (JobSeekerData));
              Repositories.Report.Remove(deleteJobSeekerQuery);

              deleteJobSeekerQuery = new Query(typeof (JobSeekerAction));
              Repositories.Report.Remove(deleteJobSeekerQuery);

              deleteJobSeekerQuery = new Query(typeof (JobSeekerActivityAssignment));
              Repositories.Report.Remove(deleteJobSeekerQuery);

              deleteJobSeekerQuery = new Query(typeof(JobSeekerOffice));
              Repositories.Report.Remove(deleteJobSeekerQuery);

              deleteJobSeekerQuery = new Query(typeof(JobSeekerJobHistory));
              Repositories.Report.Remove(deleteJobSeekerQuery);

              deleteJobSeekerQuery = new Query(typeof(JobSeekerMilitaryHistory));
              Repositories.Report.Remove(deleteJobSeekerQuery);

              deleteJobSeekerQuery = new Query(typeof (JobSeeker));
              Repositories.Report.Remove(deleteJobSeekerQuery);

              Repositories.Report.SaveChanges(true);
            }

            message.EntityIds = GetAllJobSeekers();
          }
          break;

        case ReportEntityType.Action:
          updateFunction = UpdateActionEvent;
          break;

        case ReportEntityType.JobSeekerAction:
          updateFunction = UpdateAllJobSeekerActions;

          if (message.EntityIds.IsNullOrEmpty())
          {
            if (message.ResetAll)
            {
              var deleteJobSeekerActionQuery = new Query(typeof (JobSeekerAction));
              Repositories.Report.Remove(deleteJobSeekerActionQuery);
              Repositories.Report.SaveChanges(true);
            }

            message.EntityIds = GetAllJobSeekers();
          }
          break;

        case ReportEntityType.JobSeekerActivityAssignment:
          updateFunction = UpdateAllJobSeekerActivityAssignments;

          if (message.EntityIds.IsNullOrEmpty())
          {
            if (message.ResetAll)
            {
              var deleteJobSeekerActivityQuery = new Query(typeof (JobSeekerActivityAssignment));
              Repositories.Report.Remove(deleteJobSeekerActivityQuery);
              Repositories.Report.SaveChanges(true);
            }

            message.EntityIds = GetAllJobSeekers();
          }
          break;

        case ReportEntityType.StaffMemberOffice:
          updateFunction = UpdateOfficeForStaffMember;
          deleteFunction = UpdateOfficeForStaffMember;
          if (message.EntityIds.IsNullOrEmpty())
          {
            if (message.ResetAll)
            {
              var updateStaffMemberQuery = new Query(typeof(StaffMember));
              Repositories.Report.Update(updateStaffMemberQuery, new { Office = string.Empty });

              var deleteStaffMemberOfficeQuery = new Query(typeof(StaffMemberOffice));
              Repositories.Report.Remove(deleteStaffMemberOfficeQuery);
              Repositories.Report.SaveChanges(true);
            }

            message.EntityIds = GetAllStaffMembers();
          }
          break;

        case ReportEntityType.JobSeekerOffice:
          updateFunction = UpdateOfficeForJobSeeker;
          deleteFunction = UpdateOfficeForJobSeeker;
          if (message.EntityIds.IsNullOrEmpty())
          {
            if (message.ResetAll)
            {
              var updateJobSeekerQuery = new Query(typeof(JobSeeker));
              Repositories.Report.Update(updateJobSeekerQuery, new { Office = string.Empty });

              var deleteJobSeekerOfficeQuery = new Query(typeof(JobSeekerOffice));
              Repositories.Report.Remove(deleteJobSeekerOfficeQuery);
              Repositories.Report.SaveChanges(true);
            }

            message.EntityIds = GetAllJobSeekers();
          }
          break;

        case ReportEntityType.PersonOffice:
          updateFunction = UpdateRecordForPersonOffice;
          deleteFunction = UpdateRecordForPersonOffice;
          break;

        case ReportEntityType.PersonsCurrentOffice:
          updateFunction = AddRecordForPersonsCurrentOffice;

          if (message.EntityIds.IsNullOrEmpty())
          {
            if (message.ResetAll)
            {
              var deleteStaffMemberCurrentOfficeQuery = new Query(typeof(StaffMemberCurrentOffice));
              Repositories.Report.Remove(deleteStaffMemberCurrentOfficeQuery);
              Repositories.Report.SaveChanges(true);
            }

            message.EntityIds = GetAllStaffMembers(true);
          }
          break;

        case ReportEntityType.JobOrderAction:
          updateFunction = UpdateAllJobOrderActions;

          if (message.EntityIds.IsNullOrEmpty())
          {
            if (message.ResetAll)
            {
              var deleteJobOrderActionQuery = new Query(typeof (JobOrderAction));
              Repositories.Report.Remove(deleteJobOrderActionQuery);
              Repositories.Report.SaveChanges(true);
            }

            message.EntityIds = GetAllJobOrders();
          }
          break;

        case ReportEntityType.JobOffice:
          updateFunction = UpdateOfficeForJobOrder;
          deleteFunction = UpdateOfficeForJobOrder;

          if (message.EntityIds.IsNullOrEmpty())
          {
            if (message.ResetAll)
            {
              var updateJobOrderQuery = new Query(typeof(JobOrder));
              Repositories.Report.Update(updateJobOrderQuery, new { Office = string.Empty }); 

              var deleteJobOrderOfficeQuery = new Query(typeof(JobOrderOffice));
              Repositories.Report.Remove(deleteJobOrderOfficeQuery);
              Repositories.Report.SaveChanges(true);
            }

            message.EntityIds = GetAllJobOrders();
          }
          break;

        case ReportEntityType.EmployerAction:
          updateFunction = UpdateAllBusinessUnitActions;

          if (message.EntityIds.IsNullOrEmpty())
          {
            if (message.ResetAll)
            {
              var deleteEmployerActionQuery = new Query(typeof(EmployerAction));
              Repositories.Report.Remove(deleteEmployerActionQuery);
              Repositories.Report.SaveChanges(true);
            }

            message.EntityIds = GetAllBusinessUnits();
          }
          break;

        case ReportEntityType.EmployerOffice:
          updateFunction = UpdateOfficeForEmployer;
          deleteFunction = UpdateOfficeForEmployer;
          break;

        case ReportEntityType.BusinessUnitOffice:
          updateFunction = UpdateOfficeForBusinessUnit;
          deleteFunction = UpdateOfficeForBusinessUnit;

          if (message.EntityIds.IsNullOrEmpty())
          {
            if (message.ResetAll)
            {
              var updateBusinessUnitQuery = new Query(typeof(Data.Report.Entities.Employer));
              Repositories.Report.Update(updateBusinessUnitQuery, new { Office = string.Empty });

              var deleteBusinessUnitOfficeQuery = new Query(typeof(EmployerOffice));
              Repositories.Report.Remove(deleteBusinessUnitOfficeQuery);
              Repositories.Report.SaveChanges(true);
            }

            message.EntityIds = GetAllBusinessUnits();
          }
          break;

				case ReportEntityType.Office:
					updateFunction = UpdateOffice;

		      break;

        default:
          throw new Exception("Unhandled Type");
      }

      if (message.EntityIds.IsNullOrEmpty())
        return;

      if (message.EntityDeletion)
      {
        if (deleteFunction.IsNotNull())
          message.EntityIds.ForEach(id => deleteFunction(id.Item1, id.Item2));
      }
      else
      {
        if (updateFunction.IsNotNull())
          message.EntityIds.ForEach(id => updateFunction(id.Item1, id.Item2));
      }

      if (resetStaffMembers)
      {
        message.EntityIds.ForEach(id => UpdateOfficeForStaffMember(id.Item1, null));

        message.EntityIds.ForEach(id => UpdateCurrentOfficesForStaffMember(id.Item1));
      }

      if (resetJobSeekers)
      {
        message.EntityIds.ForEach(id => UpdateOfficeForJobSeeker(id.Item1, null));

        message.EntityType = ReportEntityType.JobSeekerAction;
        Handle(message);

        message.EntityType = ReportEntityType.JobSeekerActivityAssignment;
        Handle(message);
      }

      if (resetJobOrders)
      {
        message.EntityIds.ForEach(id => UpdateOfficeForJobOrder(id.Item1, null));

        message.EntityType = ReportEntityType.JobOrderAction;
        Handle(message);        
      }

      if (resetBusinessUnits)
      {
        message.EntityIds.ForEach(id => UpdateOfficeForBusinessUnit(id.Item1, null));

        message.EntityType = ReportEntityType.EmployerAction;
        Handle(message);
      }
    }

    #region Employer/Business Unit

    /// <summary>
    /// Get all employers
    /// </summary>
    /// <returns></returns>
    private List<Tuple<long, long?>> GetAllBusinessUnits()
    {
      return (from businessUnit in Repositories.Core.BusinessUnits
              select businessUnit.Id).ToList().Select(id => Tuple.Create(id, (long?)null)).ToList();
    }

    /// <summary>
    /// Updates details of the business unit in the report database
    /// </summary>
    /// <param name="businessUnitId">The Id of the business unit (from the focus db)</param>
    /// <param name="additionalEntityId">Any additional entity id</param>
    private void UpdateBusinessUnit(long businessUnitId, long? additionalEntityId)
    {
      var unitDetails = Repositories.Core.Query<EmployerReportView>().FirstOrDefault(unit => unit.Id == businessUnitId);

      if (unitDetails.IsNull())
      {
        DeleteBusinessUnit(businessUnitId, additionalEntityId);
        return;
      }

      var reportRecord = Repositories.Report.Employers.FirstOrDefault(js => js.FocusBusinessUnitId == businessUnitId);
      if (reportRecord.IsNull())
      {
        reportRecord = new Data.Report.Entities.Employer
        {
          FocusBusinessUnitId = businessUnitId,
          FocusEmployerId = unitDetails.EmployerId,
          Office = null,
          PostalCode = null,
          LatitudeRadians = 0,
          LongitudeRadians = 0,
          NoOfEmployees = unitDetails.NoOfEmployees
        };
        Repositories.Report.Add(reportRecord);
      }

      var originalPostcode = reportRecord.PostalCode;

      reportRecord.Name = unitDetails.Name;
      reportRecord.FederalEmployerIdentificationNumber = unitDetails.FederalEmployerIdentificationNumber;
      reportRecord.CountyId = unitDetails.CountyId == 0 ? null : unitDetails.CountyId;
      reportRecord.StateId = unitDetails.StateId == 0 ? null : unitDetails.StateId;
      reportRecord.PostalCode = unitDetails.PostcodeZip.IsNullOrEmpty() ? null : unitDetails.PostcodeZip;
      reportRecord.County = null;
      reportRecord.State = null;
      reportRecord.Office = "";
	    reportRecord.AccountTypeId = unitDetails.AccountTypeId;

      if (reportRecord.CountyId.HasValue && reportRecord.County.IsNullOrEmpty())
        reportRecord.County = Helpers.Lookup.GetLookupText(LookupTypes.Counties, reportRecord.CountyId.Value);

      if (reportRecord.StateId.HasValue && reportRecord.State.IsNullOrEmpty())
        reportRecord.State = Helpers.Lookup.GetLookupText(LookupTypes.States, reportRecord.StateId.Value);

      var zip = reportRecord.PostalCode;
      if (zip != originalPostcode)
      {
        reportRecord.LatitudeRadians = 0;
        reportRecord.LongitudeRadians = 0;

        if (zip.IsNotNullOrEmpty())
        {
          if (Regex.IsMatch(zip, AppSettings.ExtendedPostalCodeRegExPattern) && zip.Contains("-"))
            zip = zip.Substring(0, zip.IndexOf("-", StringComparison.Ordinal));

          var codeLookup = Repositories.Library.Query<PostalCode>().FirstOrDefault(postalCode => postalCode.Code == zip);
          if (codeLookup.IsNotNull())
          {
            reportRecord.LatitudeRadians = Math.PI * codeLookup.Latitude / 180.0;
            reportRecord.LongitudeRadians = Math.PI * codeLookup.Longitude / 180.0;
          }
        }
      }

      Repositories.Report.SaveChanges(true);
    }

    /// <summary>
    /// Deletes the report record for a business unit
    /// </summary>
    /// <param name="businessUnitId">The Id of the business unit record (in the main Focus db)</param>
    /// <param name="additionalEntityId">Any additional entity id</param>
    private void DeleteBusinessUnit(long businessUnitId, long? additionalEntityId)
    {
      var reportRecord = Repositories.Report.Employers.FirstOrDefault(j => j.FocusBusinessUnitId == businessUnitId);
      if (reportRecord.IsNotNull())
      {
        RemoveBusinessUnitActions(reportRecord);
        RemoveBusinessUnitOffices(reportRecord);

        Repositories.Report.Remove(reportRecord);
        Repositories.Report.SaveChanges(true);
      }
    }

    /// <summary>
    /// Removes the report action records for a business unit
    /// </summary>
    /// <param name="businessUnit">The business unit report record</param>
    private void RemoveBusinessUnitActions(Data.Report.Entities.Employer businessUnit)
    {
      var businessUnitActions = businessUnit.EmployerActions.ToList();
      foreach (var businessUnitAction in businessUnitActions)
      {
        Repositories.Report.Remove(businessUnitAction);
      }
    }

    /// <summary>
    /// Removes the report office records for a business unit
    /// </summary>
    /// <param name="businessUnit">The business unitreport record</param>
    private void RemoveBusinessUnitOffices(Data.Report.Entities.Employer businessUnit)
    {
      var businessUnitOffices = businessUnit.EmployerOffices.ToList();
      foreach (var businessUnitOffice in businessUnitOffices)
      {
        Repositories.Report.Remove(businessUnitOffice);
      }
    }

    #endregion

    #region Job Order

    /// <summary>
    /// Get all job orders in the system
    /// </summary>
    /// <returns></returns>
    private List<Tuple<long, long?>> GetAllJobOrders()
    {
      return Repositories.Core.Jobs.Select(job => job.Id).ToList().Select(id => Tuple.Create(id, (long?)null)).ToList();
    }

    /// <summary>
    /// Updates the job report record
    /// </summary>
    /// <param name="jobId">The job id (from the focus db)</param>
    /// <param name="additionalEntityId">Any additional entity id</param>
    private void UpdateJobOrder(long jobId, long? additionalEntityId)
    {
      var jobDetails = Repositories.Core.Query<JobOrderReportView>().FirstOrDefault(job => job.Id == jobId);

      if (jobDetails.IsNull())
      {
        DeleteJobOrder(jobId, additionalEntityId);
        return;
      }

      var reportRecord = Repositories.Report.JobOrders.FirstOrDefault(j => j.FocusJobId == jobId);
      if (reportRecord.IsNull())
      {
        var employerReport = Repositories.Report.Employers.FirstOrDefault(e => e.FocusBusinessUnitId == jobDetails.BusinessUnitId);
        reportRecord = new JobOrder
        {
          FocusJobId = jobId,
          EmployerId = employerReport.IsNull() ? 0 : employerReport.Id,
          FocusBusinessUnitId = employerReport.IsNull() ? (long?)null : employerReport.FocusBusinessUnitId,
          Office = null,
          PostalCode = null,
          LatitudeRadians = 0,
          LongitudeRadians = 0
        };
        Repositories.Report.Add(reportRecord);
      }
      else
      {
        if (reportRecord.FocusBusinessUnitId != jobDetails.BusinessUnitId)
        {
          var employerReport = Repositories.Report.Employers.FirstOrDefault(e => e.FocusBusinessUnitId == jobDetails.BusinessUnitId);
          reportRecord.EmployerId = employerReport.IsNull() ? 0 : employerReport.Id;
          reportRecord.FocusBusinessUnitId = employerReport.IsNull() ? (long?) null : employerReport.FocusBusinessUnitId;
        }
      }

      var originalPostcode = reportRecord.PostalCode;

      var onet = jobDetails.OnetId.IsNull()
        ? null
        : Repositories.Library.Onets.FirstOrDefault(o => o.Id == jobDetails.OnetId);

      reportRecord.OnetCode = onet.IsNull() ? null : onet.OnetCode;

      reportRecord.JobStatus = jobDetails.JobStatus;
			reportRecord.PostalCode = jobDetails.Zip.IsNullOrEmpty() ? null : jobDetails.Zip;
			reportRecord.County = jobDetails.County;
			reportRecord.State = jobDetails.State;
      reportRecord.JobTitle = jobDetails.JobTitle;
      reportRecord.Description = jobDetails.Description;
      reportRecord.MinimumEducationLevel = jobDetails.MinimumEducationLevel;
      reportRecord.PostingFlags = jobDetails.PostingFlags;
      reportRecord.WorkOpportunitiesTaxCreditHires = jobDetails.WorkOpportunitiesTaxCreditHires;

      if (jobDetails.ForeignLabourCertificationH2A.GetValueOrDefault(false))
        reportRecord.ForeignLabourType = ForeignLaborTypes.ForeignLabourCertificationH2A;
      else if (jobDetails.ForeignLabourCertificationH2B.GetValueOrDefault(false))
        reportRecord.ForeignLabourType = ForeignLaborTypes.ForeignLabourCertificationH2B;
      else if (jobDetails.ForeignLabourCertificationOther.GetValueOrDefault(false))
        reportRecord.ForeignLabourType = ForeignLaborTypes.ForeignLabourCertificationOther;
      else
        reportRecord.ForeignLabourType = null;

      reportRecord.ForeignLabourCertification = reportRecord.ForeignLabourType.IsNotNull();
      reportRecord.CourtOrderedAffirmativeAction = jobDetails.CourtOrderedAffirmativeAction;
      reportRecord.FederalContractor = jobDetails.FederalContractor;
      reportRecord.ScreeningPreferences = jobDetails.ScreeningPreferences;
      reportRecord.SalaryFrequency = MapLookupToEnum<ReportSalaryFrequency>(LookupTypes.Frequencies, jobDetails.SalaryFrequencyId);
      reportRecord.MinSalary = jobDetails.MinSalary;
      reportRecord.MaxSalary = jobDetails.MaxSalary;
      reportRecord.PostedOn = jobDetails.PostedOn;
      reportRecord.ClosingOn = jobDetails.ClosingOn;

      var zip = reportRecord.PostalCode;
      if (zip != originalPostcode)
      {
        reportRecord.LatitudeRadians = 0;
        reportRecord.LongitudeRadians = 0;

        if (zip.IsNotNullOrEmpty())
        {
          if (Regex.IsMatch(zip, AppSettings.ExtendedPostalCodeRegExPattern) && zip.Contains("-"))
            zip = zip.Substring(0, zip.IndexOf("-", StringComparison.Ordinal));

          var codeLookup = Repositories.Library.Query<PostalCode>().FirstOrDefault(postalCode => postalCode.Code == zip);
          if (codeLookup.IsNotNull())
          {
            reportRecord.LatitudeRadians = Math.PI * codeLookup.Latitude / 180.0;
            reportRecord.LongitudeRadians = Math.PI * codeLookup.Longitude / 180.0;
          }
        }
      }

	    var ncrcLevel = Helpers.Lookup.GetLookup(LookupTypes.NCRCLevel)
		    .FirstOrDefault(ncrclevel => ncrclevel.Id == jobDetails.CareerReadinessLevel);
	    if (ncrcLevel.IsNotNull())
	    {
		    reportRecord.NcrcLevel = ncrcLevel.Key;
		    reportRecord.NcrcLevelRequired = jobDetails.CareerReadinessLevelRequired;
	    }

      reportRecord.NumberOfOpenings = jobDetails.NumberOfOpenings;

	    Repositories.Report.SaveChanges(true);
    }

    /// <summary>
    /// Deletes the report record for a job 
    /// </summary>
    /// <param name="jobId">The Id of the job record (in the main Focus db)</param>
    /// <param name="additionalEntityId">Any additional entity id</param>
    private void DeleteJobOrder(long jobId, long? additionalEntityId)
    {
      var reportRecord = Repositories.Report.JobOrders.FirstOrDefault(j => j.FocusJobId == jobId);
      if (reportRecord.IsNotNull())
      {
        RemoveJobOrderActions(reportRecord);
        RemoveJobOrderOffices(reportRecord);

        Repositories.Report.Remove(reportRecord);
        Repositories.Report.SaveChanges(true);
      }
    }

    /// <summary>
    /// Removes the report action records for a job order
    /// </summary>
    /// <param name="jobOrder">The job order report record</param>
    private void RemoveJobOrderActions(JobOrder jobOrder)
    {
      var jobOrderActions = jobOrder.JobOrderActions.ToList();
      foreach (var jobOrderAction in jobOrderActions)
      {
        Repositories.Report.Remove(jobOrderAction);
      }
    }

    /// <summary>
    /// Removes the office records for a job order
    /// </summary>
    /// <param name="jobOrder">The job order report record</param>
    private void RemoveJobOrderOffices(JobOrder jobOrder)
    {
      var jobOrderOffices = jobOrder.JobOrderOffices.ToList();
      foreach (var jobOrderOffice in jobOrderOffices)
      {
        Repositories.Report.Remove(jobOrderOffice);
      }
    }

    #endregion

    #region Job Seekers and Staff Members

    /// <summary>
    /// Updates the relevant report record based on the person being changed
    /// </summary>
    /// <param name="personId">The id of the person</param>
    /// <param name="additionalEntityId">Any additional entity </param>
    private void UpdateRecordForPerson(long personId, long? additionalEntityId)
    {
      var userRecord = (from user in Repositories.Core.Users
                        where user.PersonId == personId
                        select user).FirstOrDefault();

      if (userRecord.IsNull())
        return;

      if (ValidJobSeekerUserTypes.Contains(userRecord.UserType))
        UpdateJobSeeker(personId, additionalEntityId);
      else if (userRecord.UserType == UserTypes.Assist)
        UpdateStaffMember(personId, additionalEntityId);
    }

    /// <summary>
    /// Updates the relevant report record based on the person being changed
    /// </summary>
    /// <param name="personId">The id of the person</param>
    /// <param name="additionalEntityId">Any additional entity </param>
    private void DeleteRecordForPerson(long personId, long? additionalEntityId)
    {
      var userRecord = (from user in Repositories.Core.Users
                        where user.PersonId == personId
                        select user).FirstOrDefault();

      if (userRecord.IsNull())
        return;

      if (ValidJobSeekerUserTypes.Contains(userRecord.UserType))
        DeleteJobSeeker(personId, additionalEntityId);
      else if (userRecord.UserType == UserTypes.Assist)
        DeleteStaffMember(personId, additionalEntityId);
    }

    #region Staff

    /// <summary>
    /// Get all staff members in the system
    /// </summary>
    /// <param name="forCurrentOffices">if set to true, reverse the tuple as normally current offices require current office id.</param>
    /// <returns></returns>
    private List<Tuple<long, long?>> GetAllStaffMembers(bool forCurrentOffices = false)
    {
      var ids = from person in Repositories.Core.Persons
                join user in Repositories.Core.Users
                  on person.Id equals user.PersonId
                where user.UserType == UserTypes.Assist
                select person.Id;

      return forCurrentOffices 
        ? ids.Select(id => Tuple.Create((long)0, (long?)id)).ToList()
        : ids.Select(id => Tuple.Create(id, (long?)null)).ToList();
    }

    /// <summary>
    /// Deletes the report record for an Assist user
    /// </summary>
    /// <param name="staffMemberId">The Id of the staff member record (in the main Focus db)</param>
    /// <param name="additionalEntityId">Any additional entity id</param>
    private void DeleteStaffMember(long staffMemberId, long? additionalEntityId)
    {
      var reportRecord = Repositories.Report.StaffMembers.FirstOrDefault(s => s.FocusPersonId == staffMemberId);
      if (reportRecord.IsNotNull())
      {
        RemoveStaffMemberOffices(reportRecord);
        RemoveStaffMemberCurrentOffices(reportRecord);

        Repositories.Report.Remove(reportRecord);
        Repositories.Report.SaveChanges(true);
      }
    }

    /// <summary>
    /// Updates the staff member report record
    /// </summary>
    /// <param name="staffMemberId">The staff member id (from the focus db).</param>
    /// <param name="additionalEntityId">Any additional entity id</param>
    private void UpdateStaffMember(long staffMemberId, long? additionalEntityId)
    {
      var staffMemberDetails = Repositories.Core.Query<StaffMemberReportView>().FirstOrDefault(seeker => seeker.Id == staffMemberId);

      if (staffMemberDetails.IsNull())
      {
        DeleteStaffMember(staffMemberId, additionalEntityId);
        return;
      }

      var reportRecord = Repositories.Report.StaffMembers.FirstOrDefault(js => js.FocusPersonId == staffMemberId);
      if (reportRecord.IsNull())
      {
        reportRecord = new StaffMember
        {
          FocusPersonId = staffMemberId,
          AccountCreationDate = staffMemberDetails.CreatedOn,
          Office = null,
          PostalCode = null,
          LatitudeRadians = 0,
          LongitudeRadians = 0
        };
        Repositories.Report.Add(reportRecord);
      }

      var originalPostcode = reportRecord.PostalCode;

      reportRecord.FirstName = staffMemberDetails.FirstName;
      reportRecord.LastName = staffMemberDetails.LastName;
      reportRecord.EmailAddress = staffMemberDetails.EmailAddress;
      reportRecord.AddressLine1 = staffMemberDetails.Line1;
      reportRecord.CountyId = staffMemberDetails.CountyId == 0 ? null : staffMemberDetails.CountyId;
      reportRecord.StateId = staffMemberDetails.StateId == 0 ? null : staffMemberDetails.StateId;
      reportRecord.PostalCode = staffMemberDetails.PostcodeZip.IsNullOrEmpty() ? null : staffMemberDetails.PostcodeZip;
      reportRecord.State = null;
      reportRecord.County = null;
      reportRecord.Blocked = staffMemberDetails.Blocked;
      reportRecord.ExternalStaffId = staffMemberDetails.ExternalId;
      reportRecord.LocalVeteranEmploymentRepresentative = staffMemberDetails.LocalVeteranEmploymentRepresentative;
      reportRecord.DisabledVeteransOutreachProgramSpecialist = staffMemberDetails.DisabledVeteransOutreachProgramSpecialist;

      if (reportRecord.CountyId.HasValue && reportRecord.County.IsNullOrEmpty())
        reportRecord.County = Helpers.Lookup.GetLookupText(LookupTypes.Counties, reportRecord.CountyId.Value);

      if (reportRecord.StateId.HasValue && reportRecord.State.IsNullOrEmpty())
        reportRecord.State = Helpers.Lookup.GetLookupText(LookupTypes.States, reportRecord.StateId.Value);

      var zip = reportRecord.PostalCode;
      if (zip != originalPostcode)
      {
        reportRecord.LatitudeRadians = 0;
        reportRecord.LongitudeRadians = 0;

        if (zip.IsNotNullOrEmpty())
        {
          if (Regex.IsMatch(zip, AppSettings.ExtendedPostalCodeRegExPattern) && zip.Contains("-"))
            zip = zip.Substring(0, zip.IndexOf("-", StringComparison.Ordinal));

          var codeLookup = Repositories.Library.Query<PostalCode>().FirstOrDefault(postalCode => postalCode.Code == zip);
          if (codeLookup.IsNotNull())
          {
            reportRecord.LatitudeRadians = Math.PI * codeLookup.Latitude / 180.0;
            reportRecord.LongitudeRadians = Math.PI * codeLookup.Longitude / 180.0;
          }
        }
      }

      Repositories.Report.SaveChanges(true);
    }

    /// <summary>
    /// Removes the office records for a staff member
    /// </summary>
    /// <param name="staffMember">The staff member report record</param>
    private void RemoveStaffMemberOffices(StaffMember staffMember)
    {
      var staffMemberOffices = staffMember.StaffMemberOffices.ToList();
      foreach (var staffMemberOffice in staffMemberOffices)
      {
        Repositories.Report.Remove(staffMemberOffice);
      }
    }

    /// <summary>
    /// Removes the current office records for a staff member
    /// </summary>
    /// <param name="staffMember">The staff member report record</param>
    private void RemoveStaffMemberCurrentOffices(StaffMember staffMember)
    {
      var currentOffices = staffMember.StaffMemberCurrentOffices.ToList();
      foreach (var currentOffice in currentOffices)
      {
        Repositories.Report.Remove(currentOffice);
      }
    }

    #endregion

    #region Job Seeker

    /// <summary>
    /// Get all job seekers in the system
    /// </summary>
    /// <returns></returns>
    private List<Tuple<long, long?>> GetAllJobSeekers()
    {
      return (from person in Repositories.Core.Persons
              join user in Repositories.Core.Users
                on person.Id equals user.PersonId
              where ValidJobSeekerUserTypes.Contains(user.UserType)
              select Tuple.Create(person.Id, (long?)null)).ToList();
    }

    /// <summary>
    /// Updates the job seeker report record
    /// </summary>
    /// <param name="jobSeekerId">The job seeker id (from the focus db).</param>
    /// <param name="additionalEntityId">Any additional entity id</param>
    private void UpdateJobSeeker(long jobSeekerId, long? additionalEntityId)
    {
      var jobSeekerDetails = Repositories.Core.Query<JobSeekerReportView>().FirstOrDefault(seeker => seeker.Id == jobSeekerId);

      if (jobSeekerDetails.IsNull())
      {
        DeleteJobSeeker(jobSeekerId, additionalEntityId);
        return;
      }

      if (jobSeekerDetails.UserType.IsNotIn(ValidJobSeekerUserTypes))
        return;

      var reportRecord = Repositories.Report.JobSeekers.FirstOrDefault(js => js.FocusPersonId == jobSeekerId);
      if (reportRecord.IsNull())
      {
        reportRecord = new JobSeeker
        {
          FocusPersonId = jobSeekerId,
          AccountCreationDate = DateTime.Now,
          Office = null,
          PostalCode = null,
          LatitudeRadians = 0,
          LongitudeRadians = 0
        };
        Repositories.Report.Add(reportRecord);
      }
      else
      {
        RemoveJobSeekerData(reportRecord);
				RemoveJobSeekerJobHistory(reportRecord);
        RemoveJobSeekerMilitaryHistory(reportRecord);
      }

      var originalPostcode = reportRecord.PostalCode;

      reportRecord.FirstName = jobSeekerDetails.FirstName;
      reportRecord.LastName = jobSeekerDetails.LastName;
      reportRecord.EmailAddress = jobSeekerDetails.EmailAddress;
      reportRecord.CountyId = jobSeekerDetails.CountyId == 0 ? null : jobSeekerDetails.CountyId;
      reportRecord.StateId = jobSeekerDetails.StateId == 0 ? null : jobSeekerDetails.StateId;
      reportRecord.PostalCode = jobSeekerDetails.PostcodeZip.IsNullOrEmpty() ? null : jobSeekerDetails.PostcodeZip;
      reportRecord.State = null;
      reportRecord.County = null;
      reportRecord.Gender = null;
      reportRecord.DateOfBirth = null;
      reportRecord.Age = null;
      reportRecord.DisabilityStatus = null;
      reportRecord.DisabilityType = null;
      reportRecord.EthnicHeritage = null;
      reportRecord.Race = null;
      reportRecord.EmploymentStatus = null;
      reportRecord.Salary = null;
      reportRecord.VeteranType = null;
      reportRecord.VeteranMilitaryDischarge = null;
      reportRecord.VeteranBranchOfService = null;
      reportRecord.VeteranRank = null;
      reportRecord.VeteranDisability = null;
      reportRecord.VeteranTransitionType = null;
      reportRecord.CampaignVeteran = null;
      reportRecord.ResumeCount = 0;
      reportRecord.HasPendingResume = false;
      reportRecord.ResumeSearchable = null;
      reportRecord.WillingToRelocate = null;
      reportRecord.WillingToWorkOvertime = null;
      reportRecord.MinimumEducationLevel = null;
	    reportRecord.EnrollmentStatus = null;
      reportRecord.MilitaryStartDate = null;
      reportRecord.MilitaryEndDate = null;
	    reportRecord.IsHomelessVeteran = null;
	    reportRecord.SocialSecurityNumber = jobSeekerDetails.SocialSecurityNumber;
	    reportRecord.MiddleInitial = jobSeekerDetails.MiddleInitial;
      reportRecord.Suffix = jobSeekerDetails.SuffixId.IsNotNullOrZero()
                              ? Helpers.Lookup.GetLookupText(LookupTypes.Suffixes, jobSeekerDetails.SuffixId.Value)
                              : null;
	    reportRecord.AddressLine1 = jobSeekerDetails.Line1;
	    reportRecord.IsCitizen = null;
	    reportRecord.AlienCardExpires = null;
	    reportRecord.AlienCardId = null;
	    reportRecord.Unsubscribed = jobSeekerDetails.Unsubscribed.IsNotNull() && (bool)jobSeekerDetails.Unsubscribed;
	    reportRecord.AccountType = jobSeekerDetails.AccountType;
        reportRecord.Enabled = Repositories.Core.Users.Where(r => r.PersonId == jobSeekerId).Select(r => r.Enabled).First();
        reportRecord.Blocked = Repositories.Core.Users.Where(r => r.PersonId == jobSeekerId).Select(r => r.Blocked).First();
			

      var resumeQuery = Repositories.Core.Resumes.Where(r => r.PersonId == jobSeekerDetails.Id && r.StatusId == ResumeStatuses.Active && !string.IsNullOrEmpty(r.ResumeXml) && r.ResumeXml != "INVALID");

      var jobSeekerResume = resumeQuery.Where(r => r.IsDefault).Select(r => new ResumeDetails { ResumeXml = r.ResumeXml, DateOfBirth = r.DateOfBirth, CompletionStatus = r.ResumeCompletionStatusId }).FirstOrDefault();

      if (jobSeekerResume.IsNotNull() && jobSeekerResume.ResumeXml.IsNotNullOrEmpty())
      {
        reportRecord.DateOfBirth = jobSeekerResume.DateOfBirth;
        if (jobSeekerResume.DateOfBirth.IsNotNull())
          reportRecord.Age = jobSeekerResume.DateOfBirth.Value.GetAge();

        var resumeModel = jobSeekerResume.ResumeXml.ToResumeModel(RuntimeContext, "**-**");

        var resumeStatuses = resumeQuery.Select(r => r.ResumeCompletionStatusId).ToList();

        var resumeCount = resumeStatuses.Count;
        reportRecord.ResumeCount = resumeCount;
        reportRecord.HasPendingResume = resumeCount > 0 && resumeStatuses.All(status => status != ResumeCompletionStatuses.Completed);
				
        var special = resumeModel.Special;
        if (special.IsNotNull())
        {
          var preferences = special.Preferences;
          if (preferences.IsNotNull())
          {
            reportRecord.SalaryFrequency = MapLookupToEnum<ReportSalaryFrequency>(LookupTypes.Frequencies, preferences.SalaryFrequencyId);
            reportRecord.Salary = (decimal?) preferences.Salary;
            reportRecord.ResumeSearchable = preferences.IsResumeSearchable;
            reportRecord.WillingToRelocate = preferences.WillingToRelocate;
						
            var shiftDetail = preferences.ShiftDetail;
            if (shiftDetail.IsNotNull())
              reportRecord.WillingToWorkOvertime = shiftDetail.WorkOverTime;
          }
        }

				
				reportRecord.HasAttendedTap = resumeModel.ResumeContent.Profile.Veteran.AttendedTapCourse ?? false;
	      reportRecord.AttendedTapDate = resumeModel.ResumeContent.Profile.Veteran.DateAttendedTapCourse;


	      var content = resumeModel.ResumeContent;
        if (content.IsNotNull())
				{
					#region Resume - contact

					var contact = content.SeekerContactDetails;
          if (contact.IsNotNull())
          {
            var address = contact.PostalAddress;
            if (address.IsNotNull())
            {
              reportRecord.CountyId = address.CountyId == 0 ? null : address.CountyId;
              reportRecord.StateId = address.StateId == 0 ? null : address.StateId;
              reportRecord.PostalCode = address.Zip.Length == 0 ? null : address.Zip;
            }

	          var phoneNumbers = contact.PhoneNumber;

						if (phoneNumbers.IsNotNullOrEmpty())
						{
							foreach (var phoneNumber in phoneNumbers)
							{
								PopulateJobSeekerDataRecord(reportRecord, phoneNumber.PhoneType.ToString()[0] + phoneNumber.PhoneNumber, ReportJobSeekerDataType.PhoneNumber);
							}
						}
          }

					#endregion

					#region Resume - experience

					var experience = content.ExperienceInfo;
          if (experience.IsNotNull())
          {
            reportRecord.EmploymentStatus = experience.EmploymentStatus;
            if (experience.Jobs.IsNotNullOrEmpty())
            {
              var jobTitles = new List<string>();
              var jobEmployers = new List<string>();
              var jobCount = 0;
              foreach (var job in experience.Jobs.OrderByDescending(j => j.JobEndDate ?? DateTime.MaxValue).ThenByDescending(j => j.JobStartDate ?? DateTime.MinValue))
              {
                jobCount += 1;

                PopulateJobSeekerDataRecord(reportRecord, job.Title, ReportJobSeekerDataType.JobTitle, jobTitles);
                PopulateJobSeekerDataRecord(reportRecord, job.Description, ReportJobSeekerDataType.JobDescription);
                PopulateJobSeekerDataRecord(reportRecord, job.Employer, ReportJobSeekerDataType.JobEmployer, jobEmployers);

                if (Regex.IsMatch(job.Title ?? "", @"(\bintern\b)|(\binterns\b)|(\binternship\b)|(\binternships\b)|(\bapprentice\b)", RegexOptions.IgnoreCase)
                || Regex.IsMatch(job.Description ?? "", @"(\bintern\b)|(\binterns\b)|(\binternship\b)|(\binternships\b)|(\bapprentice\b)", RegexOptions.IgnoreCase))
                  PopulateJobSeekerDataRecord(reportRecord, string.Concat(job.Title ?? "", " ", job.Description ?? ""), ReportJobSeekerDataType.Internship);

                if (jobCount <= 3)
                {
                  PopulateJobSeekerDataRecord(reportRecord, job.OnetCode, ReportJobSeekerDataType.JobOnet);
                  PopulateJobSeekerDataRecord(reportRecord, job.ROnetCode, ReportJobSeekerDataType.JobROnet);
                  PopulateJobSeekerDataRecord(reportRecord, job.MilitaryOccupationCode, ReportJobSeekerDataType.MilitaryOccupationCode);

                  if (job.MilitaryOccupationCode.IsNotNullOrEmpty())
                    PopulateJobSeekerDataRecord(reportRecord, LookupMilitaryOccupationTitle(job.MilitaryOccupationCode), ReportJobSeekerDataType.MilitaryOccupationTitle);

                }

								if (jobCount == 1)
									PopulateJobSeekerHistoryRecord(reportRecord, job);
              }
            }
					}

					#endregion

					#region Resume - profile

					var profile = content.Profile;
          if (profile.IsNotNull())
          {
            reportRecord.Gender = profile.Sex;
            reportRecord.DisabilityStatus = profile.DisabilityStatus;
            if(profile.DisabilityStatus == DisabilityStatus.Disabled)
                if (profile.DisabilityCategoryIds.IsNotNullOrEmpty())
                {
                    List<long?> disability = profile.DisabilityCategoryIds;
                    reportRecord.DisabilityType = MapReportDisabilitytypes(disability);
                }

            var ethnicity = profile.EthnicHeritage;
            if (ethnicity.IsNotNull())
            {
              reportRecord.EthnicHeritage = MapLookupToEnum<ReportEthnicHeritage>(LookupTypes.EthnicHeritages, ethnicity.EthnicHeritageId);
              if (ethnicity.RaceIds.IsNotNullOrEmpty())
                reportRecord.Race = MapReportRaces(ethnicity.RaceIds);
            }

	          reportRecord.IsCitizen = profile.IsUSCitizen;

						if (profile.IsUSCitizen.IsNotNull() && !(bool)profile.IsUSCitizen)
						{
							reportRecord.AlienCardExpires = profile.AlienExpires;
							reportRecord.AlienCardId = profile.AlienId;
						}

            var veteran = profile.Veteran;
            if ((jobSeekerResume.CompletionStatus & ResumeCompletionStatuses.Profile) == ResumeCompletionStatuses.Profile && veteran.IsNotNull() && veteran.IsVeteran.GetValueOrDefault(false))
            {
              reportRecord.VeteranType = veteran.History[0].VeteranEra;
              reportRecord.VeteranMilitaryDischarge = MapLookupToEnum<ReportMilitaryDischargeType>(LookupTypes.MilitaryDischargeTypes, veteran.History[0].MilitaryDischargeId);
              reportRecord.VeteranBranchOfService = MapLookupToEnum<ReportMilitaryBranchOfService>(LookupTypes.MilitaryBranchesOfService, veteran.History[0].MilitaryBranchOfServiceId);
              reportRecord.VeteranDisability = veteran.History[0].VeteranDisabilityStatus;
              reportRecord.VeteranRank = RuntimeContext.Helpers.Lookup.GetLookup(LookupTypes.MilitaryRanks).SingleOrDefault(x => x.Id == veteran.History[0].RankId).ExternalId;
              reportRecord.CampaignVeteran = veteran.History[0].IsCampaignVeteran;
              reportRecord.VeteranTransitionType = veteran.History[0].TransitionType;
              reportRecord.IsHomelessVeteran = veteran.IsHomeless;
              reportRecord.MilitaryStartDate = veteran.History[0].VeteranStartDate;
              reportRecord.MilitaryEndDate = veteran.History[0].VeteranEndDate;
              reportRecord.VeteranAttendedTapCourse = veteran.AttendedTapCourse;
              foreach (var history in veteran.History)
              {
                PopulateJobSeekerMilitaryRecord(reportRecord, history);
              }
            }
					}

					#endregion

					#region Resume - education

					var education = content.EducationInfo;
          if (education.IsNotNull())
          {
	          var ncrcLevel = Helpers.Lookup.GetLookup(LookupTypes.NCRCLevel)
		          .FirstOrDefault(ncrcLevelITem => ncrcLevelITem.Id == education.NCRCLevel);
	          if (ncrcLevel.IsNotNull())
		          reportRecord.NcrcLevel = ncrcLevel.Key;
            reportRecord.MinimumEducationLevel = education.EducationLevel;
            var schools = education.Schools;
            if (schools.IsNotNullOrEmpty())
              schools.ForEach(school => PopulateJobSeekerDataRecord(reportRecord, school.Degree, ReportJobSeekerDataType.EducationQualification));

	          reportRecord.EnrollmentStatus = education.SchoolStatus;

            if (education.TargetIndustries.IsNotNullOrEmpty())
              education.TargetIndustries.ForEach(industry => PopulateJobSeekerDataRecord(reportRecord, industry.ToString(CultureInfo.InvariantCulture), ReportJobSeekerDataType.TargetIndustry));

            if (education.MostExperienceIndustries.IsNotNullOrEmpty())
            education.MostExperienceIndustries.ForEach(industry => PopulateJobSeekerDataRecord(reportRecord,industry.ToString(CultureInfo.InvariantCulture),ReportJobSeekerDataType.MostExperienceIndustry));
          }

					#endregion

					#region Resume - skills

					var skills = content.Skills;
          if (skills.IsNotNull())
          {
            if (skills.Skills.IsNotNullOrEmpty())
              skills.Skills.ForEach(skill => PopulateJobSeekerDataRecord(reportRecord, skill.ToLower(), ReportJobSeekerDataType.Skill));

            if (skills.Certifications.IsNotNullOrEmpty())
              skills.Certifications.ForEach(certificate => PopulateJobSeekerDataRecord(reportRecord, certificate.Certificate, ReportJobSeekerDataType.CertificationLicence));
          }

					#endregion

          var additionalSections = content.AdditionalResumeSections;
          if (additionalSections.IsNotNull())
          {
            var internshipSection = additionalSections.FirstOrDefault(section => section.SectionType == ResumeSectionType.Internships);
            if (internshipSection.IsNotNull())
              PopulateJobSeekerDataRecord(reportRecord, internshipSection.SectionContent.ToString(), ReportJobSeekerDataType.Internship);
          }
        }
      }

      if (reportRecord.CountyId.HasValue && reportRecord.County.IsNullOrEmpty())
        reportRecord.County = Helpers.Lookup.GetLookupText(LookupTypes.Counties, reportRecord.CountyId.Value);

      if (reportRecord.StateId.HasValue && reportRecord.State.IsNullOrEmpty())
        reportRecord.State = Helpers.Lookup.GetLookupText(LookupTypes.States, reportRecord.StateId.Value);

      var zip = reportRecord.PostalCode;
      if (zip != originalPostcode)
      {
        reportRecord.LatitudeRadians = 0;
        reportRecord.LongitudeRadians = 0;

        if (zip.IsNotNullOrEmpty())
        {
          if (Regex.IsMatch(zip, AppSettings.ExtendedPostalCodeRegExPattern) && zip.Contains("-"))
            zip = zip.Substring(0, zip.IndexOf("-", StringComparison.Ordinal));

          var codeLookup = Repositories.Library.Query<PostalCode>().FirstOrDefault(postalCode => postalCode.Code == zip);
          if (codeLookup.IsNotNull())
          {
            reportRecord.LatitudeRadians = Math.PI*codeLookup.Latitude/180.0;
            reportRecord.LongitudeRadians = Math.PI*codeLookup.Longitude/180.0;
          }
        }
      }

			UpdateJobSeekerNonPurged(reportRecord);

			Repositories.Report.SaveChanges(true);
    }

    /// <summary>
    /// Adds a Job Seeker Data record for data of a given type
    /// </summary>
    /// <param name="jobSeekerReportRecord">The Job Seeker report record</param>
    /// <param name="dataDescription">The text of the data record</param>
    /// <param name="dataType">The type of the data record</param>
    /// <param name="existingData">A list of existing descriptions added for that data type</param>
    private void PopulateJobSeekerDataRecord(JobSeeker jobSeekerReportRecord, string dataDescription, ReportJobSeekerDataType dataType, List<string> existingData = null)
    {
      if (dataDescription.IsNotNullOrEmpty() && (existingData.IsNull() || !existingData.Contains(dataDescription.ToLower())))
      {
        var jobTitleDataRecord = new JobSeekerData
        {
          Description = dataDescription,
          DataType = dataType
        };
        jobSeekerReportRecord.JobSeekerData.Add(jobTitleDataRecord);

        if (existingData.IsNotNull())
          existingData.Add(dataDescription.ToLower());
      }
    }

		/// <summary>
		/// Populates the job seeker history record.
		/// </summary>
		/// <param name="jobSeekerReportRecord">The job seeker report record.</param>
		/// <param name="lastJob">The last job.</param>
		private void PopulateJobSeekerHistoryRecord(JobSeeker jobSeekerReportRecord, Job lastJob)
		{
			var job = new JobSeekerJobHistory
			{
				EmployerName = lastJob.Employer.IsNotNullOrEmpty() ? lastJob.Employer : "",
				EmploymentStartDate = lastJob.JobStartDate,
				EmploymentEndDate = lastJob.JobEndDate,
				JobTitle = lastJob.Title.IsNotNullOrEmpty() ? lastJob.Title : "",
				EmployerCity = lastJob.JobAddress.City,
				EmployerState = lastJob.JobAddress.StateId.IsNotNull() ? Helpers.Lookup.GetLookupText(LookupTypes.States, lastJob.JobAddress.StateId.Value) : null
			};

			jobSeekerReportRecord.JobSeekerJobHistories.Add(job);
		}

    /// <summary>
    /// Populates the job seeker military history record.
    /// </summary>
    /// <param name="jobSeekerReportRecord">The job seeker report record.</param>
    /// <param name="history">The history record.</param>
    private void PopulateJobSeekerMilitaryRecord(JobSeeker jobSeekerReportRecord, VeteranHistory history)
    {
      var historyRecord = new JobSeekerMilitaryHistory
      {
        VeteranType = history.VeteranEra,
        VeteranMilitaryDischarge = MapLookupToEnum<ReportMilitaryDischargeType>(LookupTypes.MilitaryDischargeTypes, history.MilitaryDischargeId),
        VeteranBranchOfService = MapLookupToEnum<ReportMilitaryBranchOfService>(LookupTypes.MilitaryBranchesOfService, history.MilitaryBranchOfServiceId),
        VeteranDisability = history.VeteranDisabilityStatus,
        CampaignVeteran = history.IsCampaignVeteran,
        VeteranTransitionType = history.TransitionType,
        MilitaryStartDate = history.VeteranStartDate,
        MilitaryEndDate = history.VeteranEndDate,
        VeteranRank = RuntimeContext.Helpers.Lookup.GetLookup(LookupTypes.MilitaryRanks).SingleOrDefault(x => x.Id == history.RankId).ExternalId
      };

      jobSeekerReportRecord.JobSeekerMilitaryHistories.Add(historyRecord);
    }

    /// <summary>
    /// Looks up the military occupation title for a code
    /// </summary>
    /// <param name="code">The military occupation title</param>
    /// <returns>The military occupation code</returns>
    private string LookupMilitaryOccupationTitle(string code)
    {
      if (code.IsNullOrEmpty())
        return string.Empty;

      var record = (from militaryOccupation in Repositories.Library.MilitaryOccupations
                    join militaryTitleView in Repositories.Library.MilitaryOccupationJobTitleViews
                      on militaryOccupation.Id equals militaryTitleView.MilitaryOccupationId
                    where militaryOccupation.Code == code
                    select militaryTitleView).FirstOrDefault();

      return record.IsNull() ? string.Empty : record.JobTitle;
    }

    /// <summary>
    /// Converts a list of races into a bitwise field
    /// </summary>
    /// <param name="raceIds">A list of races</param>
    /// <returns>The bitwise field of race enumerations</returns>
    private ReportRace? MapReportRaces(List<long?> raceIds)
    {
      var race = (ReportRace?)ReportRace.NA;

      raceIds.ForEach(raceId =>
      {
          var raceEnum = MapLookupToEnum<ReportRace>(LookupTypes.Races, raceId);
          if (raceEnum.HasValue)
            race |= raceEnum.Value;
      });

      return race == ReportRace.NA ? null : race;
    }

          /// <summary>
    /// Converts a list of races into a bitwise field
    /// </summary>
    /// <param name="raceIds">A list of races</param>
    /// <returns>The bitwise field of race enumerations</returns>
    private ReportDisabilityType? MapReportDisabilitytypes(List<long?> DisabilityCategoryIds)
    {
      var disabilitytype = (ReportDisabilityType?)ReportDisabilityType.NA;

      DisabilityCategoryIds.ForEach(DisabilityCategoryId =>
      {
          var disabilityEnum = MapLookupToEnum<ReportDisabilityType>(LookupTypes.DisabilityCategories, DisabilityCategoryId);
          if (disabilityEnum.HasValue)
            disabilitytype |= disabilityEnum.Value;
      });

      return disabilitytype == ReportDisabilityType.NA ? null : disabilitytype;
    }

    /// <summary>
    /// Deletes the report record for a job seeker
    /// </summary>
    /// <param name="jobSeekerId">The Id of the job seeker record (in the main Focus db)</param>
    /// <param name="additionalEntityId">Any additional entity id</param>
    private void DeleteJobSeeker(long jobSeekerId, long? additionalEntityId)
    {
      var reportRecord = Repositories.Report.JobSeekers.FirstOrDefault(js => js.FocusPersonId == jobSeekerId);
      if (reportRecord.IsNotNull())
      {
        RemoveJobSeekerData(reportRecord);
        RemoveJobSeekerActions(reportRecord);
        RemoveJobSeekerActivityAssignments(reportRecord);
        RemoveJobSeekerOffices(reportRecord);
				RemoveJobSeekerJobHistory(reportRecord);
        RemoveJobSeekerMilitaryHistory(reportRecord);

        Repositories.Report.Remove(reportRecord);
        Repositories.Report.SaveChanges(true);
      }
    }

		/// <summary>
		/// Removes the job seeker job history.
		/// </summary>
		/// <param name="jobSeeker">The job seeker.</param>
		private void RemoveJobSeekerJobHistory(JobSeeker jobSeeker)
		{
			var jobHistoryRecords = jobSeeker.JobSeekerJobHistories.ToList();

			foreach (var jobHistory in jobHistoryRecords)
			{
				Repositories.Report.Remove(jobHistory);
			}
		}

    /// <summary>
    /// Removes the job seeker job history.
    /// </summary>
    /// <param name="jobSeeker">The job seeker.</param>
    private void RemoveJobSeekerMilitaryHistory(JobSeeker jobSeeker)
    {
      var militaryHistoryRecords = jobSeeker.JobSeekerMilitaryHistories.ToList();

      foreach (var militaryHistory in militaryHistoryRecords)
      {
        Repositories.Report.Remove(militaryHistory);
      }
    }

    /// <summary>
    /// Removes the report job data records for a job seeker
    /// </summary>
    /// <param name="jobSeeker">The job seeker report record</param>
    private void RemoveJobSeekerData(JobSeeker jobSeeker)
    {
      var jobDataRecords = jobSeeker.JobSeekerData.ToList();
      foreach (var jobData in jobDataRecords)
      {
        Repositories.Report.Remove(jobData);
      }
    }

    /// <summary>
    /// Removes the report action records for a job seeker
    /// </summary>
    /// <param name="jobSeeker">The job seeker report record</param>
    private void RemoveJobSeekerActions(JobSeeker jobSeeker)
    {
      var jobSeekerActions = jobSeeker.JobSeekerActions.ToList();
      foreach (var jobSeekerAction in jobSeekerActions)
      {
        Repositories.Report.Remove(jobSeekerAction);
      }

      var jobSeekerActionDetails = jobSeeker.JobSeekerActivityLogs.ToList();
      foreach (var jobSeekerActionDetail in jobSeekerActionDetails)
      {
        Repositories.Report.Remove(jobSeekerActionDetail);
      }
    }

    /// <summary>
    /// Removes the office records for a job seeker
    /// </summary>
    /// <param name="jobSeeker">The job seeker report record</param>
    private void RemoveJobSeekerOffices(JobSeeker jobSeeker)
    {
      var jobSeekerOffices = jobSeeker.JobSeekerOffices.ToList();
      foreach (var jobSeekerOffice in jobSeekerOffices)
      {
        Repositories.Report.Remove(jobSeekerOffice);
      }
    }

    /// <summary>
    /// Removes the report activity assignments records for a job seeker
    /// </summary>
    /// <param name="jobSeeker">The job seeker report record</param>
    private void RemoveJobSeekerActivityAssignments(JobSeeker jobSeeker)
    {
      var jobSeekerActivityAssignments = jobSeeker.JobSeekerActivityAssignments.ToList();
      foreach (var jobSeekerActivityAssignment in jobSeekerActivityAssignments)
      {
        Repositories.Report.Remove(jobSeekerActivityAssignment);
      }
    }

    #endregion

    #endregion

    #region Action Events

    /// <summary>
    /// Updates a report based on an action event 
    /// </summary>
    /// <param name="actionEventId">The id of the action event</param>
    /// <param name="actionTypeId">Any id of the action type</param>
    private void UpdateActionEvent(long actionEventId, long? actionTypeId)
    {
      ProcessActionEventForJobSeeker(actionEventId, actionTypeId);
      ProcessActionEventForJobOrder(actionEventId, actionTypeId);
      ProcessActionEventForBusinessUnit(actionEventId, actionTypeId);
    }

    #region Job Seeker Actions

    /// <summary>
    /// Updates a report based on an action event 
    /// </summary>
    /// <param name="actionEventId">The id of the action event</param>
    /// <param name="actionTypeId">Any id of the action type</param>
    private void ProcessActionEventForJobSeeker(long actionEventId, long? actionTypeId)
    {
      if (actionTypeId.HasValue && !ActionEventFunctionsForJobSeeker.ContainsKey(actionTypeId.Value))
        return;

      var actionEvent = Repositories.Core.ActionEvents.FirstOrDefault(ae => ae.Id == actionEventId);
      if (actionEvent.IsNull() || (!actionTypeId.HasValue && !ActionEventFunctionsForJobSeeker.ContainsKey(actionEvent.ActionTypeId)))
        return;

      User userDetails = null;

      // Get the job seeker user record from the action event. This depends on the type of action event
      switch (ActionEventFunctionsForJobSeeker[actionEvent.ActionTypeId].EntityType)
      {
        case ActionEventDetailsType.User:
          userDetails = (from user in Repositories.Core.Users
                         where user.Id == actionEvent.EntityId
                         select user).FirstOrDefault();
          break;

        case ActionEventDetailsType.Resume:
          userDetails = (from resume in Repositories.Core.Resumes
                         join user in Repositories.Core.Users
                           on resume.PersonId equals user.PersonId
                         where resume.Id == actionEvent.EntityId
                         select user).FirstOrDefault();
          break;

        case ActionEventDetailsType.Person:
          userDetails = (from user in Repositories.Core.Users
                         where user.PersonId == actionEvent.EntityId
                         select user).FirstOrDefault();
          break;

        case ActionEventDetailsType.EntityIdAdditional01Person:
          userDetails = (from user in Repositories.Core.Users
                         where user.PersonId == actionEvent.EntityIdAdditional01
                         select user).FirstOrDefault();
          break;

        case ActionEventDetailsType.Application:
          userDetails = (from application in Repositories.Core.Applications
                         join resume in Repositories.Core.Resumes
                           on application.ResumeId equals resume.Id
                         join user in Repositories.Core.Users
                           on resume.PersonId equals user.PersonId
                         where application.Id == actionEvent.EntityId
                         select user).FirstOrDefault();
          break;

        case ActionEventDetailsType.NoteReminder:
          var noteEntityTypes = new List<EntityTypes> {EntityTypes.JobSeeker};
          userDetails = (from noteReminder in Repositories.Core.NoteReminders
                         join user in Repositories.Core.Users
                           on noteReminder.EntityId equals user.PersonId
                         where noteReminder.Id == actionEvent.EntityId
                           && noteEntityTypes.Contains(noteReminder.EntityType)
                         select user).FirstOrDefault();
          break;

        case ActionEventDetailsType.SavedSearch:
          userDetails = (from savedSearchUser in Repositories.Core.SavedSearchUsers
                         join user in Repositories.Core.Users
                           on savedSearchUser.UserId equals user.Id
                         where savedSearchUser.SavedSearchId == actionEvent.EntityId
                         select user).FirstOrDefault();
          break;
      }

      if (userDetails.IsNull())
        return;

      if (userDetails.UserType.IsIn(ValidJobSeekerUserTypes))
      {
        var actionEvents = new List<ActionEvent> {actionEvent};
        UpdateJobSeekerActionEvents(userDetails.PersonId, userDetails.Id, actionEvents, false);
        if (actionEvent.ActionTypeId == AssignActivityToCandidateTypeId)
          UpdateJobSeekerActivityAssignment(userDetails.PersonId, actionEvents, false);
      }
    }

    /// <summary>
    /// Updates the action report for a job seeker for a list of activities
    /// </summary>
    /// <param name="jobSeekerId">The id of the job seeker</param>
    /// <param name="userId">The id of the associated user id record</param>
    /// <param name="actionEvents">The activity being performed</param>
    /// <param name="deletePreviousActions">Whether to delete all previous activites</param>
    private void UpdateJobSeekerActionEvents(long jobSeekerId, long userId, List<ActionEvent> actionEvents, bool deletePreviousActions)
    {
      var jobSeekerReportRecord = Repositories.Report.JobSeekers.FirstOrDefault(js => js.FocusPersonId == jobSeekerId);
      if (jobSeekerReportRecord.IsNull())
        return;

      if (deletePreviousActions)
        RemoveJobSeekerActions(jobSeekerReportRecord);

      actionEvents.ForEach(actionEvent =>
      {
        // Check if the event is one that appears in the job seeker activity log
        var isLogEvent = Repositories.Core.JobSeekerActivityActionViews.Where(view => view.Id == actionEvent.Id).Select(view => view.Id).FirstOrDefault() != 0;
        if (isLogEvent)
        {
          // Get the event details needed for the report record
          var actionEventDetails = Repositories.Core.Query<ActionEventReportView>().FirstOrDefault(details => details.Id == actionEvent.Id);
          if (actionEventDetails.IsNotNull())
          {
            var reportDetailRecord = new JobSeekerActivityLog
            {
              ActionDate = actionEvent.ActionedOn,
              ActionTypeName = ActionEventFunctionsForJobSeeker[actionEvent.ActionTypeId].ActionTypeName,
              FocusUserId = actionEvent.UserId
            };

            // Actions performed by staff users have extra details
            if (actionEventDetails.UserType == UserTypes.Assist)
            {
              reportDetailRecord.OfficeId = actionEventDetails.OfficeId;
              reportDetailRecord.OfficeName = actionEventDetails.OfficeName;
              reportDetailRecord.StaffExternalId = actionEventDetails.ExternalId;

              var staffMemberReportId = Repositories.Report.StaffMembers
                                                    .Where(staff => staff.FocusPersonId == actionEventDetails.PersonId)
                                                    .Select(staff => staff.Id)
                                                    .FirstOrDefault();
              if (staffMemberReportId != 0)
                reportDetailRecord.StaffMemberId = staffMemberReportId;
            }

            jobSeekerReportRecord.JobSeekerActivityLogs.Add(reportDetailRecord);
          }
        }
      });

      // Remove staff-assisted activities
      actionEvents = actionEvents.Where(action => !ActionEventFunctionsForJobSeeker[action.ActionTypeId].SelfAssistOnly || action.UserId == 0 || action.UserId == userId).ToList();
      if (!actionEvents.Any())
        return;

      var actionEventDates = actionEvents.Select(a => a.ActionedOn.Date).Distinct();

      foreach (var actionEventDate in actionEventDates)
      {
        var reportRecord = jobSeekerReportRecord.JobSeekerActions.FirstOrDefault(a => a.ActionDate == actionEventDate);
        if (reportRecord.IsNull())
        {
          reportRecord = new JobSeekerAction
          {
            ActionDate = actionEventDate,
            Logins = 0,
            PostingsViewed = 0,
            SelfReferrals = 0,
            StaffReferrals = 0,
            ReferralRequests = 0,
            NotesAdded = 0,
            AddedToLists = 0,
            ActivitiesAssigned = 0,
            StaffAssignments = 0,
            EmailsSent = 0,
            FollowUpIssues = 0,
            IssuesResolved = 0,
            Hired = 0,
            NotHired = 0,
            FailedToApply = 0,
            FailedToReportToInterview = 0,
            InterviewDenied = 0,
            InterviewScheduled = 0,
            NewApplicant = 0,
            Recommended = 0,
            RefusedOffer = 0,
            UnderConsideration = 0,
            SelfReferralsExternal = 0,
            StaffReferralsExternal = 0,
            TargetingHighGrowthSectors = 0,
            ReferralsApproved = 0,
            FindJobsForSeeker = 0,
            FailedToReportToJob = 0,
            FailedToRespondToInvitation = 0,
            FoundJobFromOtherSource = 0,
            JobAlreadyFilled = 0,
            NotQualified = 0,
            NotYetPlaced = 0,
            RefusedReferral = 0,
            SavedJobAlerts = 0,
            ActivityServices = 0,
            UsedOnlineResumeHelp = 0,
            SurveyVerySatisfied = 0,
            SurveySatisfied = 0,
            SurveyDissatisfied = 0,
            SurveyVeryDissatisfied = 0,
            SurveyNoUnexpectedMatches = 0,
            SurveyUnexpectedMatches = 0,
            SurveyReceivedInvitations = 0,
            SurveyDidNotReceiveInvitations = 0
          };
          jobSeekerReportRecord.JobSeekerActions.Add(reportRecord);
        }

        var actionEventsForDate = actionEvents.Where(a => a.ActionedOn.Date == actionEventDate).ToList();
        actionEventsForDate.ForEach(actionEvent =>
        {
          if (ActionEventFunctionsForJobSeeker.ContainsKey(actionEvent.ActionTypeId))
          {
            JobSeekerSurveyDto surveyRecord = null;

            var details = ActionEventFunctionsForJobSeeker[actionEvent.ActionTypeId];
            if (details.RequiresSurvey)
            {
              var baseSurvey = Repositories.Core.JobSeekerSurveys.FirstOrDefault(survey => survey.Id == actionEvent.EntityId);
              if (baseSurvey.IsNotNull())
                surveyRecord = baseSurvey.AsDto();
            }

            details.ActivityFunction(reportRecord, actionEvent, userId, surveyRecord);
          }
        });
      }

      Repositories.Report.SaveChanges(true);
    }

    /// <summary>
    /// Updates the number of staff referrals
    /// </summary>
    /// <param name="reportRecord">The report record to update</param>
    /// <param name="actionEvent">The action event</param>
    /// <param name="userId">The user Id for the job seeker</param>
    /// <param name="surveyRecord">A job seeker survey (not used in this method, but needed due to delegate).</param>
    private void IncrementActionCountForExternalReferrals(JobSeekerAction reportRecord, ActionEvent actionEvent, long userId, JobSeekerSurveyDto surveyRecord)
    {
      if (actionEvent.ActionType.Name == ActionTypes.ExternalReferral.ToString())
        reportRecord.SelfReferralsExternal++;
      else
        reportRecord.StaffReferralsExternal++;
    }

    /// <summary>
    /// Updates the numbers for survey respones
    /// </summary>
    /// <param name="reportRecord">The report record to update</param>
    /// <param name="actionEvent">The action event</param>
    /// <param name="userId">The user Id for the job seeker</param>
    /// <param name="surveyRecord">A job seeker survey (not used in this method, but needed due to delegate).</param>
    private void IncrementActionCountForJobSeekerSurvey(JobSeekerAction reportRecord, ActionEvent actionEvent, long userId, JobSeekerSurveyDto surveyRecord)
    {
      //var surveyRecord = Repositories.Core.JobSeekerSurveys.FirstOrDefault(survey => survey.Id == actionEvent.EntityId);
      if (surveyRecord.IsNotNull())
      {
          if (surveyRecord.SatisfactionLevel.IsNotNull())
          {
              switch (surveyRecord.SatisfactionLevel)
              {
                  case SatisfactionLevels.VerySatisfied:
                      reportRecord.SurveyVerySatisfied++;
                      break;
                  case SatisfactionLevels.SomewhatSatisfied:
                      reportRecord.SurveySatisfied++;
                      break;
                  case SatisfactionLevels.SomewhatDissatisfied:
                      reportRecord.SurveyDissatisfied++;
                      break;
                  case SatisfactionLevels.VeryDissatisfied:
                      reportRecord.SurveyVeryDissatisfied++;
                      break;
              }
          }

          if (surveyRecord.UnanticipatedMatches.IsNotNull())
          {
              if (surveyRecord.UnanticipatedMatches.GetValueOrDefault())
                  reportRecord.SurveyUnexpectedMatches++;
              else
                  reportRecord.SurveyNoUnexpectedMatches++;
          }

          if (surveyRecord.WasInvitedDidApply.IsNotNull())
          {
              if (surveyRecord.WasInvitedDidApply.GetValueOrDefault())
                  reportRecord.SurveyReceivedInvitations++;
              else
                  reportRecord.SurveyDidNotReceiveInvitations++;
          }
      }
    }

    /// <summary>
    /// Updates all actions for a job seeker
    /// </summary>
    /// <param name="jobSeekerId">The id of the job seeker</param>
    /// <param name="additionalEntityId">Any additional entity id</param>
    private void UpdateAllJobSeekerActions(long jobSeekerId, long? additionalEntityId)
    {
      var actions = new List<ActionEvent>();

      var firstDateForActions = DateTime.Now.Date.AddDays(_actionHistoryDays);

      var userActionTypes = ActionEventFunctionsForJobSeeker.Keys.Where(a => ActionEventFunctionsForJobSeeker[a].EntityType == ActionEventDetailsType.User).ToList();
      var loginActions = (from user in Repositories.Core.Users
                          join actionEvent in Repositories.Core.ActionEvents
                            on user.Id equals actionEvent.EntityId
                          where user.PersonId == jobSeekerId
                            && userActionTypes.Contains(actionEvent.ActionTypeId)
                            && actionEvent.ActionedOn >= firstDateForActions
                          select actionEvent).ToList();
      actions.AddRange(loginActions);

      var personActionTypes = ActionEventFunctionsForJobSeeker.Keys.Where(a => ActionEventFunctionsForJobSeeker[a].EntityType == ActionEventDetailsType.Person).ToList();
      var personActions = (from actionEvent in Repositories.Core.ActionEvents
                           where actionEvent.EntityId == jobSeekerId
                             && personActionTypes.Contains(actionEvent.ActionTypeId)
                             && actionEvent.ActionedOn >= firstDateForActions
                           select actionEvent).ToList();
      actions.AddRange(personActions);

      var additionalEntityActionTypes = ActionEventFunctionsForJobSeeker.Keys.Where(a => ActionEventFunctionsForJobSeeker[a].EntityType == ActionEventDetailsType.EntityIdAdditional01Person).ToList();
      var additionalEntityActions = (from actionEvent in Repositories.Core.ActionEvents
                                     where actionEvent.EntityIdAdditional01 == jobSeekerId
                                       && additionalEntityActionTypes.Contains(actionEvent.ActionTypeId)
                                       && actionEvent.ActionedOn >= firstDateForActions
                                     select actionEvent).ToList();
      actions.AddRange(additionalEntityActions);

      var resumeActionTypes = ActionEventFunctionsForJobSeeker.Keys.Where(a => ActionEventFunctionsForJobSeeker[a].EntityType == ActionEventDetailsType.Resume).ToList();
      var resumeActions = (from resume in Repositories.Core.Resumes
                           join actionEvent in Repositories.Core.ActionEvents
                             on resume.Id equals actionEvent.EntityId
                           where resume.PersonId == jobSeekerId
                             && resumeActionTypes.Contains(actionEvent.ActionTypeId)
                             && actionEvent.ActionedOn >= firstDateForActions
                           select actionEvent).ToList();
      actions.AddRange(resumeActions);

      var applicationActionTypes = ActionEventFunctionsForJobSeeker.Keys.Where(a => ActionEventFunctionsForJobSeeker[a].EntityType == ActionEventDetailsType.Application).ToList();
      var applicationActions = (from resume in Repositories.Core.Resumes
                                join application in Repositories.Core.Applications
                                  on resume.Id equals application.ResumeId
                                join actionEvent in Repositories.Core.ActionEvents
                                  on application.Id equals actionEvent.EntityId
                                where resume.PersonId == jobSeekerId
                                  && applicationActionTypes.Contains(actionEvent.ActionTypeId)
                                  && actionEvent.ActionedOn >= firstDateForActions
                                select actionEvent).ToList();
      actions.AddRange(applicationActions);

      var noteReminderActionTypes = ActionEventFunctionsForJobSeeker.Keys.Where(a => ActionEventFunctionsForJobSeeker[a].EntityType == ActionEventDetailsType.NoteReminder).ToList();
      var noteReminderActions = (from noteReminder in Repositories.Core.NoteReminders
                                 join actionEvent in Repositories.Core.ActionEvents
                                   on noteReminder.Id equals actionEvent.EntityId
                                 where noteReminder.EntityId == jobSeekerId
                                   && noteReminder.EntityType == EntityTypes.JobSeeker
                                   && noteReminderActionTypes.Contains(actionEvent.ActionTypeId)
                                   && actionEvent.ActionedOn >= firstDateForActions
                                 select actionEvent).ToList();
      actions.AddRange(noteReminderActions);

      var savedSearchActionTypes = ActionEventFunctionsForJobSeeker.Keys.Where(a => ActionEventFunctionsForJobSeeker[a].EntityType == ActionEventDetailsType.SavedSearch).ToList();
      var savedSearchActions = (from savedSearchUser in Repositories.Core.SavedSearchUsers
                                join user in Repositories.Core.Users
                                  on savedSearchUser.UserId equals user.Id
                                join actionEvent in Repositories.Core.ActionEvents
                                  on savedSearchUser.SavedSearchId equals actionEvent.EntityId
                                where user.PersonId == jobSeekerId
                                  && savedSearchActionTypes.Contains(actionEvent.ActionTypeId)
                                  && actionEvent.ActionedOn >= firstDateForActions
                                select actionEvent).ToList();
      actions.AddRange(savedSearchActions);

      var userId = Repositories.Core.Users.Where(u => u.PersonId == jobSeekerId).Select(u => u.Id).FirstOrDefault();

      UpdateJobSeekerActionEvents(jobSeekerId, userId, actions, true);
    }

    #endregion

    #region Job Order Action Events

    /// <summary>
    /// Updates a report based on an action event 
    /// </summary>
    /// <param name="actionEventId">The id of the action event</param>
    /// <param name="actionTypeId">Any id of the action type</param>
    private void ProcessActionEventForJobOrder(long actionEventId, long? actionTypeId)
    {
      if (actionTypeId.HasValue && !ActionEventFunctionsForJobOrder.ContainsKey(actionTypeId.Value))
        return;

      var actionEvent = Repositories.Core.ActionEvents.FirstOrDefault(ae => ae.Id == actionEventId);
      if (actionEvent.IsNull() || (!actionTypeId.HasValue && !ActionEventFunctionsForJobOrder.ContainsKey(actionEvent.ActionTypeId)))
        return;

      long? jobId = null;

      // Get the job from the action event. This depends on the type of action event
      switch (ActionEventFunctionsForJobOrder[actionEvent.ActionTypeId].EntityType)
      {
        case ActionEventDetailsType.Application:
          jobId = (from application in Repositories.Core.Applications
                   join posting in Repositories.Core.Postings
                     on application.PostingId equals posting.Id
                   where application.Id == actionEvent.EntityId
                   select posting.JobId).FirstOrDefault();
          break;

        case ActionEventDetailsType.Job:
          jobId = actionEvent.EntityId;
          break;

        case ActionEventDetailsType.EntityIdAdditional01Job:
          jobId = actionEvent.EntityIdAdditional01;
          break;
      }

      if (jobId.IsNull())
        return;

      var actionEvents = new List<ActionEvent> { actionEvent };
      UpdateJobOrderActionEvents(jobId.Value, actionEvents, false);
    }

    /// <summary>
    /// Updates the action report for a job order for a list of actions
    /// </summary>
    /// <param name="jobOrderId">The id of the job order</param>
    /// <param name="actionEvents">The activity being performed</param>
    /// <param name="deletePreviousActions">Whether to delete all previous activites</param>
    private void UpdateJobOrderActionEvents(long jobOrderId, List<ActionEvent> actionEvents, bool deletePreviousActions)
    {
      var jobOrderReportRecord = Repositories.Report.JobOrders.FirstOrDefault(js => js.FocusJobId == jobOrderId);
      if (jobOrderReportRecord.IsNull())
        return;

      if (deletePreviousActions)
        RemoveJobOrderActions(jobOrderReportRecord);

      var actionEventDates = actionEvents.Select(a => a.ActionedOn.Date).Distinct();

      foreach (var actionEventDate in actionEventDates)
      {
        var reportRecord = jobOrderReportRecord.JobOrderActions.FirstOrDefault(a => a.ActionDate == actionEventDate);
        if (reportRecord.IsNull())
        {
          reportRecord = new JobOrderAction
          {
            ActionDate = actionEventDate,
            StaffReferrals = 0,
            SelfReferrals = 0,
            ReferralsRequested = 0,
            EmployerInvitationsSent = 0,
            ApplicantsInterviewed = 0,
            ApplicantsFailedToShow = 0,
            ApplicantsDeniedInterviews = 0,
            ApplicantsHired = 0,
            JobOffersRefused = 0,
            ApplicantsDidNotApply = 0,
            InvitedJobSeekerViewed = 0,
            InvitedJobSeekerClicked = 0,
            ApplicantsNotHired = 0,
            ApplicantsNotYetPlaced = 0,
            FailedToRespondToInvitation = 0,
            FailedToReportToJob = 0,
            FoundJobFromOtherSource = 0,
            JobAlreadyFilled = 0,
            NewApplicant = 0,
            NotQualified = 0,
            ApplicantRecommended = 0,
            RefusedReferral = 0,
            UnderConsideration = 0
          };
          jobOrderReportRecord.JobOrderActions.Add(reportRecord);
        }

        var actionEventsForDate = actionEvents.Where(a => a.ActionedOn.Date == actionEventDate).ToList();
        actionEventsForDate.ForEach(actionEvent =>
        {
          if (ActionEventFunctionsForJobOrder.ContainsKey(actionEvent.ActionTypeId))
          {
            InviteToApplyDto inviteRecord = null;

            var details = ActionEventFunctionsForJobOrder[actionEvent.ActionTypeId];
            if (details.RequiresInviteToApply)
            {
              var baseInvite = details.EntityType == ActionEventDetailsType.Job
                ? Repositories.Core.InviteToApply.FirstOrDefault(invite => invite.JobId == actionEvent.EntityId && invite.PersonId == actionEvent.EntityIdAdditional01)
                : Repositories.Core.InviteToApply.FirstOrDefault(invite => invite.JobId == actionEvent.EntityIdAdditional01 && invite.PersonId == actionEvent.EntityId);

              if (baseInvite.IsNotNull())
                inviteRecord = baseInvite.AsDto();
            }

            details.ActivityFunction(reportRecord, actionEvent, inviteRecord);
          }
        });
      }

      Repositories.Report.SaveChanges(true);
    }

    /// <summary>
    /// Updates the number of invited job seekers who viewed the job
    /// </summary>
    /// <param name="reportRecord">The report record to update</param>
    /// <param name="actionEvent">The action event</param>
    /// <param name="inviteToApply">The InviteToApply record</param>
    private void IncrementActionCountForInvitedJobSeekersViewed(JobOrderAction reportRecord, ActionEvent actionEvent, InviteToApplyDto inviteToApply)
    {
      //var inviteToApply = Repositories.Core.InviteToApply.FirstOrDefault(invite => invite.JobId == actionEvent.EntityIdAdditional01 && invite.PersonId == actionEvent.EntityId);
      if (inviteToApply.IsNotNull())
        reportRecord.InvitedJobSeekerViewed++;
    }

    /// <summary>
    /// Updates the number of invited job seekers who clicked the job
    /// </summary>
    /// <param name="reportRecord">The report record to update</param>
    /// <param name="actionEvent">The action event</param>
    /// <param name="inviteToApply">The InviteToApply record</param>
    private void IncrementActionCountForInvitedJobSeekersClicked(JobOrderAction reportRecord, ActionEvent actionEvent, InviteToApplyDto inviteToApply)
    {
      //var inviteToApply = Repositories.Core.InviteToApply.FirstOrDefault(invite => invite.JobId == actionEvent.EntityId && invite.PersonId == actionEvent.EntityIdAdditional01);
      if (inviteToApply.IsNotNull())
        reportRecord.InvitedJobSeekerClicked++;
    }

    /// <summary>
    /// Updates all actions for a job order
    /// </summary>
    /// <param name="jobOrderId">The id of the job order</param>
    /// <param name="additionalEntityId">Any additional entity id</param>
    private void UpdateAllJobOrderActions(long jobOrderId, long? additionalEntityId)
    {
      var actions = new List<ActionEvent>();

      var firstDateForActions = DateTime.Now.Date.AddDays(_actionHistoryDays);

      var additionalEntityActionTypes = ActionEventFunctionsForJobOrder.Keys.Where(a => ActionEventFunctionsForJobOrder[a].EntityType == ActionEventDetailsType.EntityIdAdditional01Job).ToList();
      var additionalEntityActions = (from actionEvent in Repositories.Core.ActionEvents
                                     where actionEvent.EntityIdAdditional01 == jobOrderId
                                       && additionalEntityActionTypes.Contains(actionEvent.ActionTypeId)
                                       && actionEvent.ActionedOn >= firstDateForActions
                                     select actionEvent).ToList();
      actions.AddRange(additionalEntityActions);

      var jobActionTypes = ActionEventFunctionsForJobOrder.Keys.Where(a => ActionEventFunctionsForJobOrder[a].EntityType == ActionEventDetailsType.Job).ToList();
      var jobActions = (from actionEvent in Repositories.Core.ActionEvents
                        where actionEvent.EntityId == jobOrderId
                          && jobActionTypes.Contains(actionEvent.ActionTypeId)
                          && actionEvent.ActionedOn >= firstDateForActions
                        select actionEvent).ToList();
      actions.AddRange(jobActions);

      var applicationActionTypes = ActionEventFunctionsForJobOrder.Keys.Where(a => ActionEventFunctionsForJobOrder[a].EntityType == ActionEventDetailsType.Application).ToList();
      var applicationActions = (from posting in Repositories.Core.Postings
                                join application in Repositories.Core.Applications
                                  on posting.Id equals application.PostingId
                                join actionEvent in Repositories.Core.ActionEvents
                                  on application.Id equals actionEvent.EntityId
                                where posting.JobId == jobOrderId
                                  && applicationActionTypes.Contains(actionEvent.ActionTypeId)
                                  && actionEvent.ActionedOn >= firstDateForActions
                                select actionEvent).ToList();
      actions.AddRange(applicationActions);

      UpdateJobOrderActionEvents(jobOrderId, actions, true);
    }

    #endregion

    #region Employer Action Events

    /// <summary>
    /// Updates a report based on an action event 
    /// </summary>
    /// <param name="actionEventId">The id of the action event</param>
    /// <param name="actionTypeId">Any id of the action type</param>
    private void ProcessActionEventForBusinessUnit(long actionEventId, long? actionTypeId)
    {
      if (actionTypeId.HasValue && !ActionEventFunctionsForBusinessUnit.ContainsKey(actionTypeId.Value))
        return;

      var actionEvent = Repositories.Core.ActionEvents.FirstOrDefault(ae => ae.Id == actionEventId);
      if (actionEvent.IsNull() || (!actionTypeId.HasValue && !ActionEventFunctionsForBusinessUnit.ContainsKey(actionEvent.ActionTypeId)))
        return;

      long? businessUnitId = null;

      // Get the job from the action event. This depends on the type of action event
      switch (ActionEventFunctionsForBusinessUnit[actionEvent.ActionTypeId].EntityType)
      {
        case ActionEventDetailsType.Application:
          businessUnitId = (from application in Repositories.Core.Applications
                            join posting in Repositories.Core.Postings
                              on application.PostingId equals posting.Id
                            join job in Repositories.Core.Jobs
                              on posting.JobId equals job.Id
                            where application.Id == actionEvent.EntityId
                            select job.BusinessUnitId).FirstOrDefault();
          break;

        case ActionEventDetailsType.Job:
          businessUnitId = (from job in Repositories.Core.Jobs
                            where job.Id == actionEvent.EntityId
                            select job.BusinessUnitId).FirstOrDefault();
          break;

        case ActionEventDetailsType.JobOrAdditionalBusinessUnit:
          businessUnitId = actionEvent.EntityIdAdditional01.IsNotNull()
                             ? actionEvent.EntityIdAdditional01
                             : (from job in Repositories.Core.Jobs
                                where job.Id == actionEvent.EntityId
                                select job.BusinessUnitId).FirstOrDefault();
          break;

        case ActionEventDetailsType.EntityIdAdditional01Job:
          businessUnitId = (from job in Repositories.Core.Jobs
                            where job.Id == actionEvent.EntityIdAdditional01
                            select job.BusinessUnitId).FirstOrDefault();
          break;
      }

      if (businessUnitId.IsNull())
        return;

      var actionEvents = new List<ActionEvent> { actionEvent };
      UpdateBusinessUnitActionEvents(businessUnitId.Value, actionEvents, false);
    }

    /// <summary>
    /// Updates the action report for a job order for a list of actions
    /// </summary>
    /// <param name="businessUnitId">The id of the job order</param>
    /// <param name="actionEvents">The activity being performed</param>
    /// <param name="deletePreviousActions">Whether to delete all previous activites</param>
    private void UpdateBusinessUnitActionEvents(long businessUnitId, List<ActionEvent> actionEvents, bool deletePreviousActions)
    {
      var businessUnitReportRecord = Repositories.Report.Employers.FirstOrDefault(js => js.FocusBusinessUnitId == businessUnitId);
      if (businessUnitReportRecord.IsNull())
        return;

      if (deletePreviousActions)
        RemoveBusinessUnitActions(businessUnitReportRecord);

      var actionEventDates = actionEvents.Select(a => a.ActionedOn.Date).Distinct();

      foreach (var actionEventDate in actionEventDates)
      {
        var reportRecord = businessUnitReportRecord.EmployerActions.FirstOrDefault(a => a.ActionDate == actionEventDate);
        if (reportRecord.IsNull())
        {
          reportRecord = new EmployerAction
          {
            ActionDate = actionEventDate,
            JobOrdersEdited = 0,
            JobSeekersInterviewed = 0,
            JobSeekersHired = 0,
            JobOrdersCreated = 0,
            JobOrdersPosted = 0,
            JobOrdersPutOnHold = 0,
            JobOrdersRefreshed = 0,
            JobOrdersClosed = 0,
            InvitationsSent = 0,
            JobSeekersNotHired = 0,
            SelfReferrals = 0,
            StaffReferrals = 0
          };
          businessUnitReportRecord.EmployerActions.Add(reportRecord);
        }

        var actionEventsForDate = actionEvents.Where(a => a.ActionedOn.Date == actionEventDate).ToList();
        actionEventsForDate.ForEach(actionEvent =>
        {
          if (ActionEventFunctionsForBusinessUnit.ContainsKey(actionEvent.ActionTypeId))
            ActionEventFunctionsForBusinessUnit[actionEvent.ActionTypeId].ActivityFunction(reportRecord);
        });
      }

      Repositories.Report.SaveChanges(true);
    }

    /// <summary>
    /// Updates all actions for a business unit
    /// </summary>
    /// <param name="businessUnitId">The id of the job order</param>
    /// <param name="additionalEntityId">Any additional entity id</param>
    private void UpdateAllBusinessUnitActions(long businessUnitId, long? additionalEntityId)
    {
      var actions = new List<ActionEvent>();

      var firstDateForActions = DateTime.Now.Date.AddDays(_actionHistoryDays);

      var additionalEntityActionTypes = ActionEventFunctionsForBusinessUnit.Keys.Where(a => ActionEventFunctionsForBusinessUnit[a].EntityType == ActionEventDetailsType.EntityIdAdditional01Job).ToList();
      var additionalEntityActions = (from job in Repositories.Core.Jobs
                                     join actionEvent in Repositories.Core.ActionEvents
                                       on job.Id equals actionEvent.EntityIdAdditional01
                                     where job.BusinessUnitId == businessUnitId
                                       && additionalEntityActionTypes.Contains(actionEvent.ActionTypeId)
                                       && actionEvent.ActionedOn >= firstDateForActions
                                     select actionEvent).ToList();
      actions.AddRange(additionalEntityActions);

      var jobActionTypes = ActionEventFunctionsForBusinessUnit.Keys.Where(a => ActionEventFunctionsForBusinessUnit[a].EntityType == ActionEventDetailsType.Job).ToList();
      var jobActions = (from job in Repositories.Core.Jobs
                        join actionEvent in Repositories.Core.ActionEvents
                          on job.Id equals actionEvent.EntityId
                        where job.BusinessUnitId == businessUnitId
                          && jobActionTypes.Contains(actionEvent.ActionTypeId)
                          && actionEvent.ActionedOn >= firstDateForActions
                        select actionEvent).ToList();
      actions.AddRange(jobActions);

      jobActionTypes = ActionEventFunctionsForBusinessUnit.Keys.Where(a => ActionEventFunctionsForBusinessUnit[a].EntityType == ActionEventDetailsType.JobOrAdditionalBusinessUnit).ToList();
      jobActions = (from actionEvent in Repositories.Core.ActionEvents
                    where actionEvent.EntityIdAdditional01 == businessUnitId
                      && jobActionTypes.Contains(actionEvent.ActionTypeId)
                      && actionEvent.ActionedOn >= firstDateForActions
                    select actionEvent).ToList();
      actions.AddRange(jobActions);
      
      jobActions = (from job in Repositories.Core.Jobs
                    join actionEvent in Repositories.Core.ActionEvents
                      on job.Id equals actionEvent.EntityId
                    where job.BusinessUnitId == businessUnitId
                      && jobActionTypes.Contains(actionEvent.ActionTypeId)
                      && actionEvent.EntityIdAdditional01 == null
                      && actionEvent.ActionedOn >= firstDateForActions
                    select actionEvent).ToList();
      actions.AddRange(jobActions);

      var applicationActionTypes = ActionEventFunctionsForBusinessUnit.Keys.Where(a => ActionEventFunctionsForBusinessUnit[a].EntityType == ActionEventDetailsType.Application).ToList();
      var applicationActions = (from job in Repositories.Core.Jobs
                                join posting in Repositories.Core.Postings
                                  on job.Id equals posting.JobId
                                join application in Repositories.Core.Applications
                                  on posting.Id equals application.PostingId
                                join actionEvent in Repositories.Core.ActionEvents
                                  on application.Id equals actionEvent.EntityId
                                where job.BusinessUnitId == businessUnitId
                                  && applicationActionTypes.Contains(actionEvent.ActionTypeId)
                                  && actionEvent.ActionedOn >= firstDateForActions
                                select actionEvent).ToList();
      actions.AddRange(applicationActions);

      UpdateBusinessUnitActionEvents(businessUnitId, actions, true);
    }

    #endregion

    #endregion

    #region Job Seeker Activity Assignment

    /// <summary>
    /// Updates the action report for a job seeker for a ac
    /// </summary>
    /// <param name="jobSeekerId">The id of the job seeker</param>
    /// <param name="actionEvents">The activity being performed</param>
    /// <param name="deletePreviousAssigments">Whether to delete all previous assignments</param>
    private void UpdateJobSeekerActivityAssignment(long jobSeekerId, IEnumerable<ActionEvent> actionEvents, bool deletePreviousAssigments)
    {
      var jobSeekerReportRecord = Repositories.Report.JobSeekers.FirstOrDefault(js => js.FocusPersonId == jobSeekerId);
      if (jobSeekerReportRecord.IsNull())
        return;

      if (deletePreviousAssigments)
        RemoveJobSeekerActivityAssignments(jobSeekerReportRecord);

      foreach (var actionEvent in actionEvents)
      {
        var actionEventDate = actionEvent.ActionedOn.Date;
        var activityDetails = GetActivityDetails(actionEvent.EntityIdAdditional01.GetValueOrDefault(0));
        var activityName = activityDetails.Name ?? string.Empty;

        var reportRecord = jobSeekerReportRecord.JobSeekerActivityAssignments.FirstOrDefault(a => a.AssignDate == actionEventDate && a.Activity == activityDetails.Name);
        if (reportRecord.IsNull())
        {
          reportRecord = new JobSeekerActivityAssignment
          {
            AssignDate = actionEventDate,
            Activity = activityName,
            ActivityCategory = activityDetails.CategoryName ?? string.Empty,
            AssignmentsMade = 0
          };
          jobSeekerReportRecord.JobSeekerActivityAssignments.Add(reportRecord);
        }

        reportRecord.AssignmentsMade++;
        Repositories.Report.SaveChanges(true);
      }
    }

    /// <summary>
    /// Gets the activity detail name
    /// </summary>
    /// <param name="activityId">The Id of the activity</param>
    /// <returns>The Activity Details</returns>
    private ActivityDetails GetActivityDetails(long activityId)
    {
      ActivityDetails activityDetails;

      if (_activityDetailLookup.IsNull())
        _activityDetailLookup = new Dictionary<long, ActivityDetails>();

      if (!_activityDetailLookup.ContainsKey(activityId))
      {
        activityDetails = (from activity in Repositories.Configuration.Query<Activity>()
                           join category in Repositories.Configuration.Query<ActivityCategory>()
                              on activity.ActivityCategoryId equals category.Id
                           where activity.Id == activityId
                           select new ActivityDetails
                           {
                             Name = activity.LocalisationKey, 
                             CategoryName = category == null ? string.Empty : category.LocalisationKey
                           }).FirstOrDefault();

        _activityDetailLookup.Add(activityId, activityDetails);
      }
      else
      {
        activityDetails = _activityDetailLookup[activityId];
      }

      return activityDetails;
    }

    /// <summary>
    /// Updates all activity assignments for a job seeker
    /// </summary>
    /// <param name="jobSeekerId">The id of the job seeker</param>
    /// <param name="additionalEntityId">Any additional entity id</param>
    private void UpdateAllJobSeekerActivityAssignments(long jobSeekerId, long? additionalEntityId)
    {
      var firstDateForActions = DateTime.Now.Date.AddDays(_actionHistoryDays);

      var activities = (from actionEvent in Repositories.Core.ActionEvents
                        where actionEvent.EntityId == jobSeekerId
                          && actionEvent.ActionTypeId == AssignActivityToCandidateTypeId
                          && actionEvent.ActionedOn >= firstDateForActions
                        select actionEvent).ToList();

      UpdateJobSeekerActivityAssignment(jobSeekerId, activities, true);
    }

    #endregion

    #region Offices

    /// <summary>
    /// Updates the relevant report record based on the office being changed
    /// </summary>
    /// <param name="personId">The id of the person</param>
    /// <param name="additionalEntityId">Any additional entity </param>
    private void UpdateRecordForPersonOffice(long personId, long? additionalEntityId)
    {
      var userRecord = (from user in Repositories.Core.Users
                        where user.PersonId == personId
                        select user).FirstOrDefault();

      if (userRecord.IsNull())
        return;

      if (ValidJobSeekerUserTypes.Contains(userRecord.UserType))
        UpdateOfficeForJobSeeker(personId, additionalEntityId);
      else if (userRecord.UserType == UserTypes.Assist)
        UpdateOfficeForStaffMember(personId, additionalEntityId);
    }

    /// <summary>
    /// Updates the relevant staff member record when the office changes
    /// </summary>
    /// <param name="staffMemberId">The id of the person</param>
    /// <param name="additionalEntityId">Any additional entity </param>
    private void UpdateOfficeForStaffMember(long staffMemberId, long? additionalEntityId)
    {
      var reportRecord = Repositories.Report.StaffMembers.FirstOrDefault(js => js.FocusPersonId == staffMemberId);

      if (reportRecord.IsNull())
        return;

      var offices = (from personOffice in Repositories.Core.PersonOfficeMappers
                     join office in Repositories.Core.Offices
                       on personOffice.OfficeId equals office.Id
                     where personOffice.PersonId == staffMemberId
                     select new
                     {
                       office.Id,
                       office.OfficeName,
                       AssignedDate = personOffice.CreatedOn
                     }).ToList();

      RemoveStaffMemberOffices(reportRecord);

      offices.ForEach(office => reportRecord.StaffMemberOffices.Add(new StaffMemberOffice
      {
        OfficeId = office.Id,
        OfficeName = office.OfficeName,
        AssignedDate = office.AssignedDate
      }));

      reportRecord.Office = offices.Count > 0 ? string.Join("||", offices.Select(office => office.OfficeName)) : string.Empty;

      Repositories.Report.SaveChanges(true);
    }

    /// <summary>
    /// Updates the relevant job seeker record when the office changes
    /// </summary>
    /// <param name="jobSeekerId">The id of the person</param>
    /// <param name="additionalEntityId">Any additional entity </param>
    private void UpdateOfficeForJobSeeker(long jobSeekerId, long? additionalEntityId)
    {
      var reportRecord = Repositories.Report.JobSeekers.FirstOrDefault(js => js.FocusPersonId == jobSeekerId);

      if (reportRecord.IsNull())
        
        return;

      var offices = (from personOffice in Repositories.Core.PersonOfficeMappers
                     join office in Repositories.Core.Offices
                       on personOffice.OfficeId equals office.Id
                     where personOffice.PersonId == jobSeekerId
                     select new 
                     {
                       office.Id,
                       office.OfficeName,
                       AssignedDate = personOffice.CreatedOn
                     }).ToList();

      RemoveJobSeekerOffices(reportRecord);

      offices.ForEach(office => reportRecord.JobSeekerOffices.Add(new JobSeekerOffice
      {
        OfficeId = office.Id,
        OfficeName = office.OfficeName,
        AssignedDate = office.AssignedDate
      }));

			reportRecord.Office = offices.Count > 0 ? string.Join("||", offices.Select(office => office.OfficeName)).TruncateString(400, true).Replace("||...", "|...").Replace("|...", " ...") : string.Empty;

      Repositories.Report.SaveChanges(true);
    }

    /// <summary>
    /// Updates the relevant report record based on the office being changed
    /// </summary>
    /// <param name="jobId">The id of the job</param>
    /// <param name="additionalEntityId">Any additional entity </param>
    private void UpdateOfficeForJobOrder(long jobId, long? additionalEntityId)
    {
      
      var reportRecord = Repositories.Report.JobOrders.FirstOrDefault(js => js.FocusJobId == jobId);

      if (reportRecord.IsNull())
        return;

      var offices = (from jobOffice in Repositories.Core.JobOfficeMappers
                     join office in Repositories.Core.Offices
                       on jobOffice.OfficeId equals office.Id
                     where jobOffice.JobId == jobId
                     select office).ToList();

      RemoveJobOrderOffices(reportRecord);

      offices.ForEach(office => reportRecord.JobOrderOffices.Add(new JobOrderOffice
      {
        OfficeId = office.Id,
        OfficeName = office.OfficeName
      }));

			reportRecord.Office = offices.Count > 0 ? string.Join("||", offices.Select(office => office.OfficeName)).TruncateString(400, true).Replace("||...", "|...").Replace("|...", " ...") : string.Empty;

      Repositories.Report.SaveChanges(true);
    }

    /// <summary>
    /// Updates the relevant report record based on the office being changed
    /// </summary>
    /// <param name="employerId">The id of the employer</param>
    /// <param name="additionalEntityId">Any additional entity </param>
    private void UpdateOfficeForEmployer(long employerId, long? additionalEntityId)
    {
      var reportRecords = Repositories.Report.Employers.Where(js => js.FocusEmployerId == employerId).ToList();

      if (reportRecords.IsNullOrEmpty())
        return;

      var offices = (from employerOffice in Repositories.Core.EmployerOfficeMappers
                     join office in Repositories.Core.Offices
                       on employerOffice.OfficeId equals office.Id
                     where employerOffice.EmployerId == employerId
                     select office).ToList();

			var officeName = offices.Count > 0 ? string.Join("||", offices.Select(office => office.OfficeName)).TruncateString(400, true).Replace("||...", "|...").Replace("|...", " ...") : string.Empty;
      reportRecords.ForEach(reportRecord =>
      {
        RemoveBusinessUnitOffices(reportRecord);

        offices.ForEach(office => reportRecord.EmployerOffices.Add(new EmployerOffice
        {
          OfficeId = office.Id,
          OfficeName = office.OfficeName
        }));

        reportRecord.Office = officeName;
      });

      Repositories.Report.SaveChanges(true);
    }

    /// <summary>
    /// Updates the relevant report record based on the office being changed
    /// </summary>
    /// <param name="unitId">The id of the business unit</param>
    /// <param name="additionalId">An additional Id (not used)</param>
    private void UpdateOfficeForBusinessUnit(long unitId, long? additionalId)
    {
      var reportRecord = Repositories.Report.Employers.FirstOrDefault(js => js.FocusBusinessUnitId == unitId);

      if (reportRecord.IsNull())
        return;

      var employerId = reportRecord.FocusEmployerId;

      var offices= (from employerOffice in Repositories.Core.EmployerOfficeMappers
                    join office in Repositories.Core.Offices
                      on employerOffice.OfficeId equals office.Id
                    where employerOffice.EmployerId == employerId
                    select office).ToList();

      RemoveBusinessUnitOffices(reportRecord);

      offices.ForEach(office => reportRecord.EmployerOffices.Add(new EmployerOffice
      {
        OfficeId = office.Id,
        OfficeName = office.OfficeName
      }));

			reportRecord.Office = offices.Count > 0 ? string.Join("||", offices.Select(office => office.OfficeName)).TruncateString(400, true).Replace("||...", "|...").Replace("|...", " ...") : string.Empty;

      Repositories.Report.SaveChanges(true);
    }

    /// <summary>
    /// Adds the relevant report record based on the current office being set
    /// </summary>
    /// <param name="personCurrentOfficeId">The id of the current person office record</param>
    /// <param name="additionalEntityId">Any additional entity (which will be the person id if personCurrentOfficeId = 0)</param>
    /// <remarks>Only inserts are supported</remarks>
    private void AddRecordForPersonsCurrentOffice(long personCurrentOfficeId, long? additionalEntityId)
    {
      if (personCurrentOfficeId == 0)
      {
        UpdateCurrentOfficesForStaffMember(additionalEntityId.GetValueOrDefault(0));
        return;
      }

      var currentOffice = (from current in Repositories.Core.PersonsCurrentOffices
                           join office in Repositories.Core.Offices
                             on current.OfficeId equals office.Id
                           where current.Id == personCurrentOfficeId
                           select new
                           {
                             office.Id,
                             office.OfficeName,
                             current.StartTime,
                             current.PersonId
                           }).FirstOrDefault();

      if (currentOffice.IsNull())
        return;

      var reportRecord = Repositories.Report.StaffMembers.FirstOrDefault(js => js.FocusPersonId == currentOffice.PersonId);

      if (reportRecord.IsNull())
        return;

      reportRecord.StaffMemberCurrentOffices.Add(new StaffMemberCurrentOffice
      {
        StaffMemberId = reportRecord.Id,
        OfficeId = currentOffice.Id,
        OfficeName = currentOffice.OfficeName,
        StartDate = currentOffice.StartTime
      });
      Repositories.Report.SaveChanges(true);
    }

    /// <summary>
    /// Updates all current offices for a staff member
    /// </summary>
    /// <param name="personId">The id of the person</param>
    private void UpdateCurrentOfficesForStaffMember(long personId)
    {
      var reportRecord = Repositories.Report.StaffMembers.FirstOrDefault(js => js.FocusPersonId == personId);

      if (reportRecord.IsNull())
        return;

      var currentOffices = (from current in Repositories.Core.PersonsCurrentOffices
                            join office in Repositories.Core.Offices
                              on current.OfficeId equals office.Id
                            where current.PersonId == personId
                            select new
                            {
                              office.Id,
                              office.OfficeName,
                              current.StartTime,
                              current.PersonId
                            }).ToList();

      RemoveStaffMemberCurrentOffices(reportRecord);

      currentOffices.ForEach(currentOffice => reportRecord.StaffMemberCurrentOffices.Add(new StaffMemberCurrentOffice
      {
        StaffMemberId = reportRecord.Id,
        OfficeId = currentOffice.Id,
        OfficeName = currentOffice.OfficeName,
        StartDate = currentOffice.StartTime
      }));

      Repositories.Report.SaveChanges(true);
    }

		/// <summary>
		/// Updates the office.
		/// </summary>
		/// <param name="officeId">The office id.</param>
		/// <param name="additionalEntityId">The additional entity id.</param>
		private void UpdateOffice(long officeId, long? additionalEntityId)
		{
			var officeName = Repositories.Core.FindById<Office>(officeId).OfficeName;

			var jobSeekerOffices = new Query(typeof(JobSeekerOffice), Entity.Attribute("OfficeId") == officeId);
			Repositories.Report.Update(jobSeekerOffices, new { OfficeName = officeName });

			var jobOrderOffices = new Query(typeof(JobOrderOffice), Entity.Attribute("OfficeId") == officeId);
			Repositories.Report.Update(jobOrderOffices, new { OfficeName = officeName });

			var employerOffices = new Query(typeof(EmployerOffice), Entity.Attribute("OfficeId") == officeId);
			Repositories.Report.Update(employerOffices, new { OfficeName = officeName });

			var staffMemberCurrentOffices = new Query(typeof(StaffMemberCurrentOffice), Entity.Attribute("OfficeId") == officeId);
			Repositories.Report.Update(staffMemberCurrentOffices, new { OfficeName = officeName });

			var staffMemberOffices = new Query(typeof(StaffMemberOffice), Entity.Attribute("OfficeId") == officeId);
			Repositories.Report.Update(staffMemberOffices, new { OfficeName = officeName });

			Repositories.Report.SaveChanges();

			Repositories.Report.JobSeekerOfficeNameUpdate(officeId);
			Repositories.Report.EmployerOfficeNameUpdate(officeId);
			Repositories.Report.StaffMemberOfficeNameUpdate(officeId);
			Repositories.Report.JobOrderOfficeNameUpdate(officeId);
		}

    #endregion

    #region Helper Methods

    /// <summary>
    /// Maps a look-up value to an enumeration
    /// </summary>
    /// <param name="lookupType">The type of the lookup</param>
    /// <param name="lookupId">The id of the lookup</param>
    /// <returns>The corresonding enumeration</returns>
    private T? MapLookupToEnum<T>(LookupTypes lookupType, long? lookupId) where T : struct
    {
      if (!lookupId.HasValue)
        return null;

      T? mappedEnum = null;

      var lookup = RuntimeContext.Helpers.Lookup.GetLookup(lookupType).FirstOrDefault(m => m.Id == lookupId);
      if (lookup.IsNotNull())
      {
        var lookupKeyName = lookup.Key.Substring(lookup.Key.IndexOf(".", StringComparison.InvariantCulture) + 1);
        if (Enum.IsDefined(typeof(T), lookupKeyName))
          mappedEnum = (T)Enum.Parse(typeof(T), lookupKeyName);
      }

      return mappedEnum;
    }

    #endregion

    #region Structs and Enums

    private class ResumeDetails
    {
      public string ResumeXml { get; set; }
      public DateTime? DateOfBirth { get; set; }
			public ResumeCompletionStatuses? CompletionStatus { get; set; }
    }

    private struct ActivityDetails
    {
      public string Name { get; set; }
      public string CategoryName { get; set; }
    }

    private enum ActionEventDetailsType
    {
      User,
      Person,
      Resume,
      Application,
      NoteReminder,
      SavedSearch,
      EntityIdAdditional01Person,
      EntityIdAdditional01Job,
      Job,
      JobOrAdditionalBusinessUnit
    }

    private struct ActionEventDetailsForJobSeeker
    {
      public readonly JobSeekerActionEventFunction ActivityFunction;
      public readonly ActionEventDetailsType EntityType;
      public readonly bool RequiresSurvey;
      public readonly bool SelfAssistOnly;

      public string ActionTypeName;

      public ActionEventDetailsForJobSeeker(ActionEventDetailsType entityType, JobSeekerActionEventFunction activityFunction)
      {
        EntityType = entityType;
        ActivityFunction = activityFunction;
        RequiresSurvey = false;
        SelfAssistOnly = false;
        ActionTypeName = string.Empty;
      }

      public ActionEventDetailsForJobSeeker(ActionEventDetailsType entityType, JobSeekerActionEventFunction activityFunction, bool selfAssistOnly)
      {
        EntityType = entityType;
        ActivityFunction = activityFunction;
        RequiresSurvey = false;
        SelfAssistOnly = false;
        SelfAssistOnly = selfAssistOnly;
        ActionTypeName = string.Empty;
      }

      public ActionEventDetailsForJobSeeker(ActionEventDetailsType entityType, JobSeekerActionEventFunction activityFunction, bool selfAssistOnly, bool requiresSurvey)
      {
        EntityType = entityType;
        ActivityFunction = activityFunction;
        RequiresSurvey = requiresSurvey;
        SelfAssistOnly = selfAssistOnly;
        ActionTypeName = string.Empty;
      }
    }

    private struct ActionEventDetailsForJobOrder
    {
      public readonly JobOrderActionEventFunction ActivityFunction;
      public readonly ActionEventDetailsType EntityType;
      public readonly bool RequiresInviteToApply;

      public ActionEventDetailsForJobOrder(ActionEventDetailsType entityType, JobOrderActionEventFunction activityFunction)
      {
        EntityType = entityType;
        ActivityFunction = activityFunction;
        RequiresInviteToApply = false;
      }

      public ActionEventDetailsForJobOrder(ActionEventDetailsType entityType, JobOrderActionEventFunction activityFunction, bool requiresInviteToApply)
      {
        EntityType = entityType;
        ActivityFunction = activityFunction;
        RequiresInviteToApply = requiresInviteToApply;
      }
    }

    private struct ActionEventDetailsForBusinessUnit
    {
      public readonly BusinessUnitActionEventFunction ActivityFunction;
      public readonly ActionEventDetailsType EntityType;

      public ActionEventDetailsForBusinessUnit(ActionEventDetailsType entityType, BusinessUnitActionEventFunction activityFunction)
      {
        EntityType = entityType;
        ActivityFunction = activityFunction;
      }
    }

    #endregion

		#region Write to Non Purged tables

		// Requirement from Georgia client not to overwrite certain job seeker fields with null
		// Created 'Non Purged' entities to keep hold of this data

		private void UpdateJobSeekerNonPurged(JobSeeker reportRecord)
	  {
			// Get JobSeekerNonPurged record
		  var jsNonPurged = Repositories.Report.JobSeekerNonPurged
			  .SingleOrDefault(js => js.JobSeekerReportId == reportRecord.Id);

		  if (jsNonPurged == null)
		  {
			  jsNonPurged = new JobSeeker_NonPurged
			  {
				  JobSeekerReportId = reportRecord.Id
			  };
				Repositories.Report.Add(jsNonPurged);
		  }
		  // Map reportRecord to JobSeekerNonPurged
		  jsNonPurged.DateOfBirth = reportRecord.DateOfBirth ?? jsNonPurged.DateOfBirth;
			jsNonPurged.Gender = reportRecord.Gender ?? jsNonPurged.Gender;
			jsNonPurged.DisabilityStatus = reportRecord.DisabilityStatus ?? jsNonPurged.DisabilityStatus;
			jsNonPurged.DisabilityType = reportRecord.DisabilityType ?? jsNonPurged.DisabilityType;
		  jsNonPurged.EthnicHeritage = reportRecord.EthnicHeritage ?? jsNonPurged.EthnicHeritage;
		  jsNonPurged.Race = reportRecord.Race ?? jsNonPurged.Race;
		  jsNonPurged.MinimumEducationLevel = reportRecord.MinimumEducationLevel ?? jsNonPurged.MinimumEducationLevel;
		  jsNonPurged.EnrollmentStatus = reportRecord.EnrollmentStatus ?? jsNonPurged.EnrollmentStatus;
		  jsNonPurged.EmploymentStatus = reportRecord.EmploymentStatus ?? jsNonPurged.EmploymentStatus;
		  jsNonPurged.Age = reportRecord.Age ?? jsNonPurged.Age;
			jsNonPurged.IsCitizen = reportRecord.IsCitizen ?? jsNonPurged.IsCitizen;
			jsNonPurged.AlienCardExpires = reportRecord.AlienCardExpires ?? jsNonPurged.AlienCardExpires;
			jsNonPurged.AlienCardId = reportRecord.AlienCardId ?? jsNonPurged.AlienCardId;
			jsNonPurged.CountyId = reportRecord.CountyId ?? jsNonPurged.CountyId;

			var militaryHistoryRecords = reportRecord.JobSeekerMilitaryHistories;

			var mhl = Repositories.Report.JobSeekerMilitaryHistoryNonPurged.Where(m => m.JobSeekerId == reportRecord.Id).ToList();
			foreach (var mh in mhl)
			{
				Repositories.Report.Remove(mh);
			}

			foreach (var militaryHistory in militaryHistoryRecords)
			{
				var mhNonPurged = new JobSeekerMilitaryHistory_NonPurged
				{
					VeteranDisability = militaryHistory.VeteranDisability,
					VeteranTransitionType = militaryHistory.VeteranTransitionType,
					VeteranMilitaryDischarge = militaryHistory.VeteranMilitaryDischarge,
					VeteranBranchOfService = militaryHistory.VeteranBranchOfService,
					CampaignVeteran = militaryHistory.CampaignVeteran,
					MilitaryStartDate = militaryHistory.MilitaryStartDate,
					MilitaryEndDate = militaryHistory.MilitaryEndDate,
					JobSeekerId = reportRecord.Id,
					VeteranType = militaryHistory.VeteranType,
                    VeteranRank = militaryHistory.VeteranRank
				};
				Repositories.Report.Add(mhNonPurged);
			}
	  }

	  #endregion
	}
}
