﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using Focus.Core.Settings.Interfaces;

#endregion

namespace Focus.Services.Messages.Handlers
{
	public class MessageHandlerBase
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="MessageHandlerBase"/> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		public MessageHandlerBase(IRuntimeContext runtimeContext)
		{
			RuntimeContext = runtimeContext ?? new RuntimeContext(ServiceRuntimeEnvironment.DllOrExe);
		}

		internal IRuntimeContext RuntimeContext { get; private set; }

		internal IAppSettings AppSettings { get { return RuntimeContext.AppSettings; } }
		internal IRepositories Repositories { get { return RuntimeContext.Repositories; } }
		internal IHelpers Helpers { get { return RuntimeContext.Helpers; } }
	}
}
