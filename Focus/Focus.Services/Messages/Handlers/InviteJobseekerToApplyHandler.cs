﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using Focus.Core;
using Focus.Core.EmailTemplate;
using Focus.Data.Core.Entities;
using Framework.Core;
using Framework.Messaging;

#endregion

namespace Focus.Services.Messages.Handlers
{
	public class InviteJobseekerToApplyHandler : MessageHandlerBase, IMessageHandler<InviteJobseekerToApplyMessage>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="InviteJobseekerToApplyHandler"/> class.
		/// </summary>
		public InviteJobseekerToApplyHandler() : base(null) {}

		/// <summary>
		/// Initializes a new instance of the <see cref="InviteJobseekerToApplyHandler"/> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		public InviteJobseekerToApplyHandler(IRuntimeContext runtimeContext) : base(runtimeContext) {}

		/// <summary>
		/// Handles the specified message.
		/// </summary>
		/// <param name="message">The message.</param>
		public void Handle(InviteJobseekerToApplyMessage message)
		{
			message.InitialiseRuntimeContext(RuntimeContext);

			if(message.PersonId.IsNull())
				throw new Exception("No Jobseeker Id.");

			if(message.JobId.IsNull())
				throw new Exception("No Job Id.");

			// Get Template Data

			// Get job seeker details
			var jobseeker = Repositories.Core.Persons.Where(x => x.Id == message.PersonId).Select(x => new{x.FirstName, x.LastName, x.EmailAddress}).FirstOrDefault();

			if(jobseeker.IsNull())
				throw new Exception(string.Format("Jobseeker not found with Id {0}.", message.PersonId));

			// Get job
			var job = Repositories.Core.Jobs.Where(x => x.Id == message.JobId).Select(x => new {x.JobTitle, x.BusinessUnit.Name}).FirstOrDefault();

			if(job.IsNull())
				throw new Exception(string.Format("Job not found with Id {0}.", message.JobId));
			
			var jobseekerName = string.Format("{0} {1}", jobseeker.FirstName, jobseeker.LastName);

			var templateData = new EmailTemplateData
			                     	{
															RecipientName = jobseekerName,
			                     		JobTitle = job.JobTitle,
			                     		EmployerName = job.Name,
			                     		SenderName = message.SendersName,
															ShowSalutation = true,
															JobLinkUrls = new List<string>{message.JobLink}
			                     	};

			// Get Template
			var email = Helpers.Email.GetEmailTemplatePreview(EmailTemplateTypes.InvitationToApplyForJobThroughFocusCareer, templateData);
			
			// Send Email
			Helpers.Email.SendEmail(jobseeker.EmailAddress, "", "", email.Subject, email.Body, detectUrl:true, senderAddress: message.SenderEmail);

      var devices = Repositories.Core.PersonMobileDevices.Where(x => x.PersonId == message.PersonId).ToList();
      // Send message as push notification if person has mobile device
		  if (devices.Any())
		  {
		      foreach (var device in devices.Where(x => x.Token != null))
		      {
                  Helpers.PushNotification.SendNotification(device.DeviceType, device.Token, device.Id, email.Subject, email.Body);            
		      }
		  }

		  // Save Invite to apply
			var invite = new InviteToApply
			{
				JobId = message.JobId,
				LensPostingId = message.LensPostingId,
				PersonId = message.PersonId,
				Viewed = false,
				CreatedBy = message.ActionerId
			};
			Repositories.Core.Add(invite);

			var postingId = Repositories.Core.Postings.Where(p => p.LensPostingId == message.LensPostingId).Select(p => p.Id).FirstOrDefault();
			if (postingId > 0)
			{
				if (Repositories.Core.Query<JobPostingOfInterestSent>().Where(j => j.PersonId == message.PersonId && j.PostingId == postingId).Select(j => j.Id).FirstOrDefault() == 0)
				{
					var postingOfInterest = new JobPostingOfInterestSent
					{
						JobId = message.JobId,
						PostingId = postingId,
						PersonId = message.PersonId,
						Score = message.Score
					};
					Repositories.Core.Add(postingOfInterest);
				}
			}

			Repositories.Core.SaveChanges();

			// Log Invite to apply
			Helpers.Logging.LogAction(ActionTypes.InviteJobSeekerToApplyThroughTalent, typeof(Job).Name, message.JobId, additionalDetails: jobseekerName, entityIdAdditional01: message.PersonId);
		}
	}
}
