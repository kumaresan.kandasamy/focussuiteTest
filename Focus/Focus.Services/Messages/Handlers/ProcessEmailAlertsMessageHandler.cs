﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;

using Focus.Core;
using Focus.Core.EmailTemplate;
using Focus.Data.Core.Entities;

using Framework.Core;
using Framework.Messaging;

#endregion

namespace Focus.Services.Messages.Handlers
{
	public class ProcessEmailAlertsMessageHandler : MessageHandlerBase, IMessageHandler<ProcessEmailAlertsMessage>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="ProcessEmailAlertsMessageHandler"/> class.
		/// </summary>
		public ProcessEmailAlertsMessageHandler() : base(null)
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ProcessEmailAlertsMessageHandler"/> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		public ProcessEmailAlertsMessageHandler(IRuntimeContext runtimeContext) : base(runtimeContext)
		{
		}

		/// <summary>
		/// Handles the specified message.
		/// </summary>
		/// <param name="message">The message.</param>
		public void Handle(ProcessEmailAlertsMessage message)
		{
			message.InitialiseRuntimeContext(RuntimeContext);

			// Get date when the job last ran
			var lastRunSetting = Helpers.Configuration.GetConfigurationItem(Constants.ConfigurationItemKeys.ProcessEmailAlertsLastRun, DateTime.MinValue.ToString());
			var lastRunDate = lastRunSetting.Value.IsNotNullOrEmpty() ? DateTime.Parse(lastRunSetting.Value) : DateTime.MinValue;

			// Only runs once a day
			if (lastRunDate.Date == DateTime.Now.Date)
				return;

			MarkJobAsRunning(Constants.ConfigurationItemKeys.ProcessEmailAlertsRunning);


			Helpers.Logging.LogAction(ActionTypes.ProcessEmailAlertsStarted, String.Empty, null, true);

			var validFrequencies = new List<string> {"D"};

			// For Monday, include users who have requested weekly alerts
			if (DateTime.Now.DayOfWeek == DayOfWeek.Monday)
				validFrequencies.Add("W");

			// Get all user alert records where at least one option has been set
			var userAlerts = Repositories.Core.ActiveUserAlertViews.Where(ua => validFrequencies.Contains(ua.Frequency));
			if (userAlerts.Any())
			{
				// Email for Approve referral requests queue
				var itemCount = Repositories.Core.JobSeekerReferralViews.Count();
				if (itemCount > 0)
				{
					SendEmailAlertsForQueue(userAlerts.Where(ua => ua.NewReferralRequests), itemCount, "Referral Requests");
				}

				// Email for Approve employer account requests queue
				itemCount = Repositories.Core.EmployerAccountReferralViews.Count();
				if (itemCount > 0)
				{
					SendEmailAlertsForQueue(userAlerts.Where(ua => ua.EmployerAccountRequests), itemCount, "Employer Account Requests");
				}

				// Email for Approve job postings queue
				itemCount = Repositories.Core.JobPostingReferralViews.Count();
				if (itemCount > 0)
				{
					SendEmailAlertsForQueue(userAlerts.Where(ua => ua.NewJobPostings), itemCount, "Job Postings");
				}
			}

			ResetConfigSettings(Constants.ConfigurationItemKeys.ProcessEmailAlertsRunning, Constants.ConfigurationItemKeys.ProcessEmailAlertsLastRun);

			Helpers.Logging.LogAction(ActionTypes.ProcessEmailAlertsCompleted, String.Empty, null, true);
		}

		/// <summary>
		/// Marks the job as running in the database.
		/// </summary>
		/// <param name="runningKey">The job key to look up in the ConfigurationItem table to see if the job is running.</param>
		/// <returns>True to indicate job has been marked as running, false if it was already running</returns>
		private bool MarkJobAsRunning(string runningKey)
		{
			var processIsRunning = false;

			var runningSetting = Helpers.Configuration.GetConfigurationItem(runningKey, "false");

			if (runningSetting.Value.IsNotNullOrEmpty())
				Boolean.TryParse(runningSetting.Value, out processIsRunning);

			// Set running flag to true
			runningSetting.Value = true.ToString();
			Repositories.Configuration.SaveChanges(true);

			return processIsRunning;
		}

		/// <summary>
		/// Resets the configuration item settings to say a job has completed.
		/// </summary>
		/// <param name="runningKey">The key used to indicate the job is running.</param>
		/// <param name="lastRunKey">The key used to indicate when the job last run.</param>
		private void ResetConfigSettings(string runningKey, string lastRunKey)
		{
			var runningSetting = Helpers.Configuration.GetConfigurationItem(runningKey, "false");
			var lastRunSetting = Helpers.Configuration.GetConfigurationItem(lastRunKey, DateTime.MinValue.ToLongDateString());
			runningSetting.Value = "false";
			lastRunSetting.Value = DateTime.Now.ToString();
			Repositories.Configuration.SaveChanges(true);
		}

		/// <summary>
		/// Sends the email alerts to a list of users relating to a specific approval queue
		/// </summary>
		/// <param name="userAlertList">The list of users who requested alerts for the queue.</param>
		/// <param name="itemCount">The number of items in the queue.</param>
		/// <param name="queueName">The name of the email queue for displaying in the email.</param>
		private void SendEmailAlertsForQueue(IEnumerable<ActiveUserAlertView> userAlertList, int itemCount, string queueName)
		{
			foreach (var userAlert in userAlertList)
			{
				var emailTemplateData = new EmailTemplateData
				                        	{
				                        		RecipientName = userAlert.FirstName,
				                        		ApprovalCount = itemCount.ToString(),
				                        		ApprovalQueueName = queueName
				                        	};

				var emailTemplate = Helpers.Email.GetEmailTemplatePreview(EmailTemplateTypes.ApprovalQueueAlert, emailTemplateData);

				Helpers.Email.SendEmail(userAlert.EmailAddress, "", "", emailTemplate.Subject, emailTemplate.Body, senderAddress: emailTemplateData.SenderEmailAddress);
			}
		}
	}
}
