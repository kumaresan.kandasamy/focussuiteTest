﻿#region Copyright © 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;

using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Messages.JobService;
using Focus.Data.Core.Entities;
using Focus.Data.Library.Entities;
using Focus.Services.ServiceImplementations;

using Framework.Core;
using Framework.Logging;
using Framework.Messaging;

#endregion

namespace Focus.Services.Messages.Handlers
{
  public class SaveAndPostJobHandler : MessageHandlerBase, IMessageHandler<SaveAndPostJobMessage>
  {
		/// <summary>
		/// Initializes a new instance of the <see cref="SaveAndPostJobHandler"/> class.
		/// </summary>
		public SaveAndPostJobHandler() : base(null) {}

		/// <summary>
		/// Initializes a new instance of the <see cref="SaveAndPostJobHandler"/> class.
		/// </summary>
		public SaveAndPostJobHandler(IRuntimeContext runtimeContext) : base(runtimeContext) { }

		/// <summary>
		/// Handles the specified message.
		/// </summary>
		/// <param name="message">The message.</param>
    public void Handle(SaveAndPostJobMessage message)
		{
      message.InitialiseRuntimeContext(RuntimeContext);

      var jobService = new JobService();	

		  var businessUnit = Repositories.Core.FindById<BusinessUnit>(message.BusinessUnitId);
		  var businessUnitAddress = Repositories.Core.BusinessUnitAddresses.First(unitAddress => unitAddress.BusinessUnitId == businessUnit.Id && unitAddress.IsPrimary);

      var job = new JobDto
      {
        JobTitle = message.JobTitle,
        Description = message.JobDescription,
        EmployeeId = message.EmployeeId,
        BusinessUnitId = businessUnit.Id,
				CreditCheckRequired = message.ApplicantCreditCheck,
        EmployerId = businessUnit.EmployerId,
        MeetsMinimumWageRequirement = true,
        EmployerDescriptionPostingPosition = EmployerDescriptionPostingPositions.BelowJobPosting,
        MinimumEducationLevelRequired = false,
        HideEducationOnPosting = true,
        HideExperienceOnPosting = true,
        HideMinimumAgeOnPosting = true,
        HideProgramOfStudyOnPosting = true,
        HideDriversLicenceOnPosting = true,
        HideLicencesOnPosting = true,
        HideCertificationsOnPosting = true,
        HideLanguagesOnPosting = true,
        HideSpecialRequirementsOnPosting = true,
        HideWorkWeekOnPosting = true,
				EmploymentStatusId = message.EmploymentStatusId,
				JobTypeId = message.JobTypeId,
        LockVersion = 0,
        JobType = JobTypes.Job,
        CreatedBy = message.JobCreatedBy,
        UpdatedBy = message.JobCreatedBy,
        ApprovalStatus = ApprovalStatuses.None,
        JobStatus = JobStatuses.Draft,
        NumberOfOpenings = message.NumberOfOpenings,
        MinimumEducationLevel = EducationLevels.None,
        WizardStep = 7,
        WizardPath = 1,
        HasCheckedCriminalRecordExclusion = message.ApplicantCriminalCheck.GetValueOrDefault(false),
				CriminalBackgroundExclusionRequired = message.ApplicantCriminalCheck,
        CreatedOn = DateTime.Now,
        UpdatedOn = DateTime.Now,
        OnetId = message.OnetId,
        RedProfanityWords = string.Empty,
        YellowProfanityWords = string.Empty,
        ClosingOn = message.ClosingOn,
        InterviewContactPreferences = ContactMethods.FocusTalent,
        UploadIndicator = message.UploadIndicator
      };

      switch (AppSettings.ResumeReferralApprovalsMinimumStars)
      {
        case 0:
          job.ScreeningPreferences = ScreeningPreferences.AllowUnqualifiedApplications;
          break;
        case 1:
          job.ScreeningPreferences = ScreeningPreferences.JobSeekersMustHave1StarMatch;
          break;
        case 2:
          job.ScreeningPreferences = ScreeningPreferences.JobSeekersMustHave2StarMatch;
          break;
        case 3:
          job.ScreeningPreferences = ScreeningPreferences.JobSeekersMustHave3StarMatch;
          break;
        case 4:
          job.ScreeningPreferences = ScreeningPreferences.JobSeekersMustHave4StarMatch;
          break;
        case 5:
          job.ScreeningPreferences = ScreeningPreferences.JobSeekersMustHave5StarMatch;
          break;
        default:
          job.ScreeningPreferences = ScreeningPreferences.JobSeekersMustBeScreened;
          break;
      }

		  var postalCode = message.PostalCode;
      if (postalCode.Contains("-"))
        postalCode = postalCode.SubstringBefore("-");

      var postcalCodeView = Repositories.Library.Query<PostalCodeView>().FirstOrDefault(postcalCode => postcalCode.Code == postalCode);
		  
		  if (postcalCodeView.IsNull())
		    throw new Exception("Unknown postal code");
      
      var locationToAdd = postcalCodeView.CityName.IsNotNullOrEmpty() 
        ? Helpers.Localisation.Localise("Global.Location.FormatFull", "{0}, {1} ({2})", postcalCodeView.CityName, postcalCodeView.StateKey.Replace("State.", ""), message.PostalCode) 
        : Helpers.Localisation.Localise("Global.Location.Format", "{0} ({1})", postcalCodeView.StateKey.Replace("State.", ""), message.PostalCode);

      var location = new JobLocationDto
      {
        JobId = 0,
        IsPublicTransitAccessible = true,
        Location = locationToAdd,
        City = postcalCodeView.CityName,
        State = postcalCodeView.StateName,
        Zip = message.PostalCode,
        County = postcalCodeView.CountyName

      };

      var address = new JobAddressDto
      {
					JobId = 0,
					Line1 = businessUnitAddress.Line1,
					Line2 = businessUnitAddress.Line2,
					Line3 = businessUnitAddress.Line3,
					TownCity = businessUnitAddress.TownCity,
					CountyId = businessUnitAddress.CountyId,
					PostcodeZip = businessUnitAddress.PostcodeZip,
					StateId = businessUnitAddress.StateId,
					CountryId = businessUnitAddress.CountryId,
					IsPrimary = true
      };

		  var currentRequest = RuntimeContext.CurrentRequest;

		  var saveJobRequest = new SaveJobRequest
		  {
        ClientTag = currentRequest.ClientTag,
			  SessionId = currentRequest.SessionId,
			  RequestId = currentRequest.RequestId,
        UserContext = currentRequest.UserContext,
	      VersionNumber = Constants.SystemVersion,
		    Job = job,
        JobCreatedBy = message.JobCreatedBy,
        JobLocations = new List<JobLocationDto> { location },
        JobAddress = address,
        GeneratePostingForNewJob = true,
				Module = FocusModules.General
		  };

      var saveJobResponse = jobService.SaveJob(saveJobRequest);

		  var userType = Repositories.Core.Users.Where(user => user.Id == message.JobCreatedBy).Select(user => user.UserType).FirstOrDefault();

		  var postJobRequest = new PostJobRequest
		  {
		    ClientTag = currentRequest.ClientTag,
		    SessionId = currentRequest.SessionId,
		    RequestId = currentRequest.RequestId,
		    UserContext = currentRequest.UserContext,
		    VersionNumber = Constants.SystemVersion,
        Job = saveJobResponse.Job,
        JobPostedBy = message.JobCreatedBy,
        Module = (userType == UserTypes.Assist) ? FocusModules.Assist : FocusModules.Talent
		  };

      jobService.PostJob(postJobRequest);

      Logger.Reset();
		}
  }
}
