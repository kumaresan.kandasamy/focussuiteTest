﻿#region Copyright © 2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Linq;

using Framework.Core;
using Framework.Messaging;

#endregion

namespace Focus.Services.Messages.Handlers
{
	public class ProcessMatchesToRecentPlacementsMessageHandler : MessageHandlerBase, IMessageHandler<ProcessMatchesToRecentPlacementsMessage>
	{

		public ProcessMatchesToRecentPlacementsMessageHandler() : base(null){}

		public ProcessMatchesToRecentPlacementsMessageHandler(IRuntimeContext runtimeContext) : base(runtimeContext){}

		public void Handle(ProcessMatchesToRecentPlacementsMessage message)
		{
			message.InitialiseRuntimeContext(RuntimeContext);
			// Either calculate those to refresh
			if (message.UpdatedJobSeekers.IsNotNullOrEmpty())
			{
				var recentlyPlacedToUpdate = new List<long>();

				// See if they are recently placed
				recentlyPlacedToUpdate.AddRange(
					RuntimeContext.Repositories.Core.RecentlyPlaced.Where(x => message.UpdatedJobSeekers.Contains(x.PersonId))
						.Select(x => x.PersonId)
						.ToList());

				// Or if they are a recent match (Get applicant they are matched to)
				recentlyPlacedToUpdate.AddRange(from matches in RuntimeContext.Repositories.Core.RecentlyPlacedMatches
																					where message.UpdatedJobSeekers.Contains(matches.PersonId)
																					join placed in RuntimeContext.Repositories.Core.RecentlyPlaced
																						on matches.RecentlyPlacedId equals placed.Id
																					select placed.PersonId);

				if (recentlyPlacedToUpdate.Any())
					Helpers.JobDevelopment.RefreshMatchesToRecentlyPlacedJobSeekers(recentlyPlacedToUpdate.Distinct().ToList());
			}
			// Or general processing
			else
				Helpers.JobDevelopment.RefreshMatchesToAllRecentlyPlacedJobSeekers();
		}
	}
}
