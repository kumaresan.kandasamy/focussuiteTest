﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives



#endregion

using System;
using System.Linq;
using Focus.Core;
using Focus.Core.IntegrationMessages;
using Focus.Services.ServiceImplementations;
using Framework.Core;
using Framework.Messaging;

namespace Focus.Services.Messages.Handlers
{
	[Obsolete]
	public class RegisterResumeMessageHandler : MessageHandlerBase, IMessageHandler<RegisterResumeMessage>
	{

		/// <summary>
		/// Initializes a new instance of the <see cref="RegisterResumeMessageHandler"/> class.
		/// </summary>
		public RegisterResumeMessageHandler() : base(null) {}

		/// <summary>
		/// Initializes a new instance of the <see cref="RegisterResumeMessageHandler"/> class.
		/// </summary>
		public RegisterResumeMessageHandler(IRuntimeContext runtimeContext) : base(runtimeContext) { }

		/// <summary>
		/// Handles the specified message.
		/// </summary>
		/// <param name="message">The message.</param>
		public void Handle(RegisterResumeMessage message)
		{
			// Convert to ResumeProcessMessage to avoid upgrade issues
			var resumeMessage = new ResumeProcessMessage
			{
				ActionerId = message.ActionerId,
				Culture = message.Culture,
				ExternalAdminId = message.ExternalAdminId,
				ExternalOfficeId = message.ExternalOfficeId,
				ExternalUsername = message.ExternalUsername,
				ExternalPassword = message.ExternalPassword,
				ResumeId = message.ResumeId,
				PersonId = message.PersonId,
				IgnoreLens = message.IgnoreLens,
				IgnoreClient = message.IgnoreClient,
				RegisterResume = true,
				ActionTypesToPublish =  message.ActionTypesToPublish
			};

			// Set priority as direct to ensure it is picked up straightaway
			Helpers.Messaging.Publish(resumeMessage, MessagePriority.Direct);

			//message.InitialiseRuntimeContext(RuntimeContext);

			//if (!message.IgnoreLens)
			//{
			//  Helpers.Resume.RegisterResume(message.ResumeId);
			//}

			//if (!message.IgnoreClient && Helpers.Configuration.AppSettings.IntegrationClient != IntegrationClient.Standalone)
			//{
			//  var user = Repositories.Core.Users.First(u => u.PersonId == message.PersonId);
			//  user.PublishUserUpdateIntegrationRequest(message.ActionerId, RuntimeContext);
			//}
      
			//if (!message.IgnoreLens)
			//{
			//  Helpers.JobDevelopment.RefreshRecentlyPlacedMatches(message.Culture);
			//  Helpers.JobDevelopment.GeneratePostingMatchesForPerson(message.PersonId, message.Culture, message.IgnoreClient);
			//}
		}
	}
}
