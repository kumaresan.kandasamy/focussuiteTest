﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Linq;
using System.Text;

using Framework.Core;
using Framework.Messaging;

#endregion

namespace Focus.Services.Messages.Handlers
{
  public class ProcessJobSeekerReportAgesMessageHandler : MessageHandlerBase, IMessageHandler<ProcessJobSeekerReportAgesMessage>
  {
    const int UpdateBatchSize = 500;

    public ProcessJobSeekerReportAgesMessageHandler() : base(null) {}

    public ProcessJobSeekerReportAgesMessageHandler(IRuntimeContext runtimeContext) : base(runtimeContext) {}

    /// <summary>
    /// Handles the specified message as called by the message bus
    /// </summary>
    /// <param name="message">The message.</param>
    public void Handle(ProcessJobSeekerReportAgesMessage message)
    {
      message.InitialiseRuntimeContext(RuntimeContext);

      UpdateAge("[Data.Application.Person]", "SELECT Id, DateOfBirth, Age FROM [Data.Application.Person] WHERE DateOfBirth IS NOT NULL");
			UpdateAge("[Report.JobSeeker]", "SELECT Id, DateOfBirth, Age FROM [Report.JobSeeker] WHERE DateOfBirth IS NOT NULL");
			UpdateAge("[Report.JobSeeker_NonPurged]", "SELECT Id, DateOfBirth, Age FROM [Report.JobSeeker_NonPurged] WHERE DateOfBirth IS NOT NULL");
		}

    /// <summary>
    /// Updates the Age field on a table
    /// </summary>
    /// <param name="tableToUpdate">The table to update</param>
    /// <param name="selectQuery">A query to get the id, date of birth and age of each record</param>
    private void UpdateAge(string tableToUpdate, string selectQuery)
    {
      var seekersToUpdate = new Dictionary<short, List<long>>();

      // Bypass Lightspeed to avoid having to read 1 million job seekers into memory
      var seekersQuery = new StringBuilder(selectQuery);

      using (var reader = Repositories.Report.ExecuteDataReader(seekersQuery.ToString()))
      {
        while (reader.Read())
        {
          var seekerId = reader.GetInt64(0);
          var currentAge = reader.IsDBNull(2) ? 0 : reader.GetInt16(2);
					var newAge = reader.GetDateTime(1).GetAge();

          // Group the job seekers by age
          if (currentAge != newAge)
          {
            if (!seekersToUpdate.ContainsKey(newAge))
              seekersToUpdate.Add(newAge, new List<long>());

            var ageList = seekersToUpdate[newAge];
            ageList.Add(seekerId);
          }
        }
        reader.Close();
      }

      // Update each recorded age separately
      foreach (var age in seekersToUpdate.Keys)
      {
        var seekersOfAge = seekersToUpdate[age];
        var startIndex = 0;

        do
        {
          // To avoid too large an "IN" clause, perform updates of the same age in batches of 500
          var batchSeekers = seekersOfAge.Skip(startIndex).Take(UpdateBatchSize).ToList();
          if (batchSeekers.Any())
          {
            var batchQuery = new StringBuilder();
            batchQuery.AppendFormat("UPDATE {0} SET Age = {1} WHERE Id IN ({2})", tableToUpdate, age, batchSeekers.AsCommaListForSql());

            Repositories.Report.ExecuteNonQuery(batchQuery.ToString());

            startIndex += UpdateBatchSize;
          }
        } while (startIndex < seekersOfAge.Count);
      }
    }
  }
}
