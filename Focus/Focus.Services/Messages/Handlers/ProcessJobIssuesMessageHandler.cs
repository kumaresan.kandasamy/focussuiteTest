﻿#region Copyright © 2000 - 2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;

using Framework.Core;
using Framework.Messaging;

using Focus.Services.DtoMappers;
using Focus.Core;
using Focus.Data.Core.Entities;

#endregion

namespace Focus.Services.Messages.Handlers
{
  public class ProcessJobIssuesMessageHandler : MessageHandlerBase, IMessageHandler<ProcessJobIssuesMessage>
  {
    #region Properties

    private DateTime CurrentDate { get; set; }
    private List<JobIssues> JobIssuesRecords { get; set; }
    
    #endregion

    #region constructors

    public ProcessJobIssuesMessageHandler() : base(null) {}

    public ProcessJobIssuesMessageHandler(IRuntimeContext runtimeContext) : base(runtimeContext) {}

    #endregion

    #region Implementation of IMessageHandler<in JobIssuesMessage>

    /// <summary>
    /// Handles the specified message as called by the message bus
    /// </summary>
    /// <param name="message">The message.</param>
    public void Handle(ProcessJobIssuesMessage message)
    {
      message.InitialiseRuntimeContext(RuntimeContext);
      CurrentDate = DateTime.Now.Date;

      // Clear down data so that we have as little data to interrogate as possible
      CleardownCandidateSearchHistory();

      // Get the existing job issue records for jobs with the right status
      JobIssuesRecords = Repositories.Core.JobIssues.Where(x => x.Job.JobStatus == JobStatuses.Active).ToList();

      // Process the necessary issues if config allows for it
      if (AppSettings.JobIssueFlagLowMinimumStarMatches) HandleLowQualityMatches();
      if (AppSettings.JobIssueFlagLowReferrals) HandleLowReferralActivity();
      if (AppSettings.JobIssueFlagNotClickingReferrals) HandleNotViewingReferrals();
      if (AppSettings.JobIssueFlagSearchingNotInviting) HandleSearchingButNotInviting();
      if (AppSettings.JobIssueFlagJobAfterHiring) HandlePostHireFollowUp();

      // Save the changes
      Repositories.Core.SaveChanges(true);
    }

    #endregion

    #region low quality matches issue

    /// <summary>
    /// Handles the low quality matches issue
    /// </summary>
    private void HandleLowQualityMatches()
    {
      var minStarThreshold = AppSettings.JobIssueMatchMinimumStarsThreshold;
      var minCandidateCount = AppSettings.JobIssueMinimumMatchCountThreshold;
      var starRatings = AppSettings.StarRatingMinimumScores;

      // Get projected search start date
      var dateRangeStart = CurrentDate.AddDays(AppSettings.JobIssueMatchDaysThreshold*-1).Date;

      // Get all active jobs and the searches carried out on them in the last x days (left join results as search may return no results)
      var searches = (from searchHistory in Repositories.Core.CandidateSearchHistories
                      where searchHistory.CreatedOn >= dateRangeStart && searchHistory.CreatedOn < CurrentDate
                      join jobs in Repositories.Core.Jobs
                        on searchHistory.JobId equals jobs.Id
                      where jobs.JobStatus == JobStatuses.Active && jobs.LastPostedOn <= dateRangeStart
                      join searchResults in Repositories.Core.CandidateSearchResults
                        on searchHistory.Id equals searchResults.CandidateSearchHistoryId into historyJoin
                      from optionalSearchResults in historyJoin.DefaultIfEmpty()
                      select new {History = searchHistory.AsDto(), Results = optionalSearchResults}).ToList();

      if (searches.IsNullOrEmpty()) return;

      // Loop through all distinct jobs
      foreach (var jobId in searches.Select(x => x.History).Select(x => x.JobId).Distinct())
      {
        // Get the job issues record 
        var jobIssues = JobIssuesRecords.SingleOrDefault(x => x.JobId == jobId);
        var queryDate = dateRangeStart;

        // Don't go any further if issue already triggered for this job
        if (jobIssues.IsNull() || !jobIssues.LowQualityMatches)
        {
          // If the issue has been resolved since the date set by the config then use the resolved date as the date limit
          if (jobIssues.IsNotNull() && jobIssues.LowQualityMatchesResolvedDate.GetValueOrDefault() > dateRangeStart)
            queryDate = jobIssues.LowQualityMatchesResolvedDate.Value;

          var count =
            searches.Where(
              x =>
              x.History.JobId == jobId && x.History.CreatedOn >= queryDate && x.Results.IsNotNull() &&
              x.Results.Score >= starRatings[minStarThreshold]).Select(x => x.Results.PersonId).Distinct().Count();

          if (count < minCandidateCount)
          {
            // Update issue record (or add issue and update)
            if (jobIssues.IsNull())
            {
              jobIssues = GetNewJobIssueRecord(jobId);
            }
            jobIssues.LowQualityMatches = true;
            jobIssues.LowQualityMatchesResolvedDate = null;
          }
        }
      }
    }

    #endregion

    #region low referral activity

    /// <summary>
    /// Handles the low referral activity issue
    /// </summary>
    private void HandleLowReferralActivity()
    {
      // Set up variables
      var queryStartDate = CurrentDate.AddDays(AppSettings.JobIssueReferralCountDaysThreshold*-1).Date;
      var minimumReferralCount = AppSettings.JobIssueMinimumReferralsCountThreshold;

      // Get all active jobs and their referrals (left join as we need to know if no referrals have been made)
      var referrals = (from jobs in Repositories.Core.Jobs
                       where jobs.LastPostedOn <= queryStartDate && jobs.JobStatus == JobStatuses.Active
                       join jobPosting in Repositories.Core.Postings
                         on jobs.Id equals jobPosting.JobId
                       join applications in Repositories.Core.Applications
                         on jobPosting.Id equals applications.PostingId into applicationJoin
                       from optionalApplications in applicationJoin.DefaultIfEmpty()
                       select new {JobId = jobs.Id, Application = applicationJoin.SingleOrDefault()}).ToList();

      // Loop through the jobs
      foreach (var jobId in referrals.Select(x => x.JobId).Distinct())
      {
        // Get the job issues record 
        var jobIssues = JobIssuesRecords.SingleOrDefault(x => x.JobId == jobId);
        var queryDate = queryStartDate;

        // Check to see if we need to calculate this issue at all
        if (jobIssues.IsNull() || !jobIssues.LowReferralActivity)
        {
          // If the issue has been resolved since the date set by the config then use the resolved date as the date limit
          if (jobIssues.IsNotNull() && jobIssues.LowReferralActivityResolvedDate.GetValueOrDefault() > queryDate)
          {
            queryDate = jobIssues.LowReferralActivityResolvedDate.Value;
            // If issue resolved within the day threshold then don't rerun issue check
            if (queryDate.Date.AddDays(AppSettings.JobIssueReferralCountDaysThreshold) > CurrentDate)
              continue;
          }

          // Get the count
          var referralCount =
            referrals.Count(x => x.JobId == jobId && x.Application.IsNotNull() && x.Application.CreatedOn >= queryDate);

          if (referralCount < minimumReferralCount)
          {
            // Update issue record (or add issue and update)
            if (jobIssues.IsNull())
            {
              jobIssues = GetNewJobIssueRecord(jobId);
            }
            jobIssues.LowReferralActivity = true;
            jobIssues.LowReferralActivityResolvedDate = null;
          }
        }
      }
    }

    #endregion

    #region referrals not being viewed

    /// <summary>
    /// Handles the not viewing referrals issue
    /// </summary>
    private void HandleNotViewingReferrals()
    {
      // Get start date
      var queryStartDate = CurrentDate.AddDays(AppSettings.JobIssueNotClickingReferralsDaysThreshold*-1);

      // Get all approved, unviewed (approved) referrals created before query start date (Active jobs only)
      var applicants = (from applications in Repositories.Core.ApplicationViews
                       where !applications.Viewed && applications.CandidateApplicationApprovalStatus == ApprovalStatuses.Approved && applications.ApplicationStatusLastChangedOn <= queryStartDate
                       join job in Repositories.Core.Jobs on applications.JobId equals job.Id
                       where job.JobStatus == JobStatuses.Active
                       select applications.AsDto()).ToList();

      foreach (var jobId in applicants.Select(x => x.JobId).Distinct().ToList())
      {
        // Get the job issues record 
        var jobIssues = JobIssuesRecords.SingleOrDefault(x => x.JobId == jobId);

        if (jobIssues.IsNull() || !jobIssues.NotViewingReferrals)
        {
          // Assume we have not viewed applications
          var notViewed = true;

          // Unless issue has been resolved previously in which case count if any haven't been viewed between issue resolution and query start date
          if (jobIssues.IsNotNull() && jobIssues.NotViewingReferralsResolvedDate.GetValueOrDefault() > queryStartDate)
            notViewed =
              applicants.Any(
                x =>
                x.JobId == jobId && x.ApplicationStatusLastChangedOn <= queryStartDate &&
                x.ApplicationStatusLastChangedOn >= jobIssues.NotViewingReferralsResolvedDate.GetValueOrDefault());
          
          if (notViewed)
          {
            // Update issue record (or add issue and update)
            if (jobIssues.IsNull())
            {
              jobIssues = GetNewJobIssueRecord(jobId);
            }
            jobIssues.NotViewingReferrals = true;
            jobIssues.NotViewingReferralsResolvedDate = null;
          }
        }
      }
    }
    
    #endregion

    #region searching not inviting

    /// <summary>
    /// Handles the searching but not inviting issue
    /// </summary>
    private void HandleSearchingButNotInviting()
    {
      var searchCountThreshold = AppSettings.JobIssueSearchesWithoutInviteThreshold;
      var queryStartDate = CurrentDate.AddDays(AppSettings.JobIssueSearchesWithoutInviteDaysThreshold*-1);

      // Get active jobs with search histories that have results which have been run by a talent user in the last x days
      var searches = (from history in Repositories.Core.CandidateSearchHistories
                      where history.CreatedOn >= queryStartDate && history.CreatedOn < CurrentDate && (from 
                       results in Repositories.Core.CandidateSearchResults 
                       select results.CandidateSearchHistoryId).Contains(history.Id)
                      join userRole in Repositories.Core.Users on history.CreatedBy equals userRole.Id
                      where userRole.UserType == UserTypes.Talent
                      join jobs in Repositories.Core.Jobs on history.JobId equals jobs.Id
                      where jobs.JobStatus == JobStatuses.Active
                      select history.AsDto()).ToList();

      // Get all invites in the last x days
      var invites =
        Repositories.Core.InviteToApply.Where(x => x.CreatedOn >= queryStartDate).Select(x => x.AsDto()).ToList();

      foreach (var jobId in searches.Select(x => x.JobId).Distinct())
      {
        // Get the job issues record 
        var jobIssues = JobIssuesRecords.SingleOrDefault(x => x.JobId == jobId);
        var queryDate = queryStartDate;

        if (jobIssues.IsNull() || !jobIssues.SearchingButNotInviting)
        {
          // If issue resolved then reset start date for query
          if (jobIssues.IsNotNull() && jobIssues.SearchingButNotInvitingResolvedDate.GetValueOrDefault() > queryDate)
            queryDate = jobIssues.SearchingButNotInvitingResolvedDate.GetValueOrDefault();

          // Ensure the serach count meets the criteria
          var searchcount = searches.Where(x => x.JobId == jobId && x.CreatedOn >= queryDate).Select(x => x.Id).Distinct().Count();

          if (searchcount >= searchCountThreshold)
          {
            // If so then see if there are any invites
            if (!invites.Any(x => x.JobId == jobId && x.CreatedOn >= queryDate))
            {
              // Update issue record (or add issue and update)
              if (jobIssues.IsNull())
              {
                jobIssues = GetNewJobIssueRecord(jobId);
              }
              jobIssues.SearchingButNotInviting = true;
              jobIssues.SearchingButNotInvitingResolvedDate = null;
            }
          }
        }
      }

    }


    #endregion

    #region post hire follow up

    /// <summary>
    /// Handles the post hire follow up.
    /// </summary>
    private void HandlePostHireFollowUp()
    {
      var queryStartDate = CurrentDate.AddDays(AppSettings.JobIssueHiredDaysElapsedThreshold*-1);

      Repositories.Core.Applications.Where(
        x =>
        x.ApplicationStatus == ApplicationStatusTypes.Hired && x.StatusLastChangedOn <= queryStartDate &&
        x.PostHireFollowUpStatus == null).ToList().ForEach(
          x => x.PostHireFollowUpStatus = PostHireFollowUpStatus.Pending);
    }

    #endregion

    #region house keeping

    /// <summary>
    /// Gets a new job issue record.
    /// </summary>
    /// <param name="jobId">The job unique identifier.</param>
    /// <returns></returns>
    private JobIssues GetNewJobIssueRecord(long jobId)
    {
      var jobIssues = new JobIssues {JobId = jobId};
      Repositories.Core.Add(jobIssues);
      JobIssuesRecords.Add(jobIssues);
      return jobIssues;
    }

    /// <summary>
    /// Clears down the candidate search history.
    /// </summary>
    private void CleardownCandidateSearchHistory()
    {
      Repositories.Core.Remove<CandidateSearchHistory>(x => x.CreatedOn < CurrentDate.AddDays(-20));
      Repositories.Core.SaveChanges(true);
    }
    
    #endregion
  }
}
