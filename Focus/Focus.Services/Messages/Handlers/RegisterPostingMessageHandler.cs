﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;

using Focus.Core;
using Focus.Core.IntegrationMessages;
using Focus.Services.Repositories;
using Framework.Core;
using Framework.Messaging;

#endregion

namespace Focus.Services.Messages.Handlers
{
	public class RegisterPostingMessageHandler : MessageHandlerBase, IMessageHandler<RegisterPostingMessage>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="RegisterPostingMessageHandler"/> class.
		/// </summary>
		public RegisterPostingMessageHandler() : base(null) {}

		/// <summary>
		/// Initializes a new instance of the <see cref="RegisterPostingMessageHandler"/> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		public RegisterPostingMessageHandler(IRuntimeContext runtimeContext) : base(runtimeContext) { }

		/// <summary>
		/// Handles the specified message.
		/// </summary>
		/// <param name="message">The message.</param>
		public void Handle(RegisterPostingMessage message)
		{
			// Convert to process posting message to avoid upgrade issues
			var postingMessage = new ProcessPostingMessage
			{
				ActionerId = message.ActionerId,
				Culture = message.Culture,
				PostingId = message.PostingId,
				JobId = message.JobId,
				IsVeteranJob = message.IsVeteranJob,
				IgnoreLens = message.IgnoreLens,
				IgnoreClient = message.IgnoreClient,
				RegisterPosting = true,
				ExternalAdminId = message.ExternalAdminId,
				ExternalOfficeId = message.ExternalOfficeId,
				ExternalPassword = message.ExternalPassword,
				ExternalUsername = message.ExternalUsername
			};

			// Set priority as direct to ensure it is picked up straightaway
			Helpers.Messaging.Publish(postingMessage, MessagePriority.Direct);
		}
	}
}
