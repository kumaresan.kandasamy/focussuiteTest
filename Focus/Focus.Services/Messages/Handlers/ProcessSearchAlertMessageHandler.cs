﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Focus.Core;
using Focus.Core.Criteria.CandidateSearch;
using Focus.Core.EmailTemplate;
using Focus.Core.IntegrationMessages;
using Focus.Core.Models.Career;
using Focus.Data.Core.Entities;
using Focus.Services.Core.Extensions;
using Framework.Core;
using Framework.Messaging;
using System.Collections.Generic;
using Focus.Core.Models;
using System.Web.Script.Serialization;

#endregion

namespace Focus.Services.Messages.Handlers
{
	public class ProcessSearchAlertMessageHandler : MessageHandlerBase, IMessageHandler<ProcessSearchAlertMessage>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="ProcessSearchAlertMessageHandler"/> class.
		/// </summary>
		public ProcessSearchAlertMessageHandler() : base(null) {}

		/// <summary>
		/// Initializes a new instance of the <see cref="ProcessSearchAlertMessageHandler"/> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		public ProcessSearchAlertMessageHandler(IRuntimeContext runtimeContext) : base(runtimeContext) { }

		/// <summary>
		/// Handles the specified message.
		/// </summary>
		/// <param name="message">The message.</param>
		public void Handle(ProcessSearchAlertMessage message)
		{
			message.InitialiseRuntimeContext(RuntimeContext);

			var alert = Repositories.Core.FindById<ActiveSearchAlertView>(message.SearchAlertId);

			if (alert.IsNull())
				throw new Exception(string.Format("Active search alert {0} was not found.", message.SearchAlertId));

			if (alert.SearchCriteria.IsNull()) return;

			DateTime lastAlertDate;
			DateTime nextScheduledAlert;
			EmailTemplateData emailTemplateData = null;
			var emailTemplateType = EmailTemplateTypes.TalentCandidateSearchAlert;

			switch (alert.AlertEmailFrequency)
			{
				case EmailAlertFrequencies.Weekly:
					lastAlertDate = (alert.AlertEmailScheduledOn.HasValue) ? alert.AlertEmailScheduledOn.Value.AddDays(-7) : DateTime.Now.Date.AddDays(-7);
					nextScheduledAlert = DateTime.Now.Date.AddDays(7);
					break;

				default:
					lastAlertDate = (alert.AlertEmailScheduledOn.HasValue) ? alert.AlertEmailScheduledOn.Value.AddDays(-1) : DateTime.Now.Date.AddDays(-1);
					nextScheduledAlert = DateTime.Now.Date.AddDays(1);
					break;
			}

			switch (alert.Type)
			{
				case SavedSearchTypes.AssistCandidateSearch:
				case SavedSearchTypes.TalentCandidateSearch:
					emailTemplateData = ProcessCandidateSearchAlert(alert, lastAlertDate);
					emailTemplateType = EmailTemplateTypes.TalentCandidateSearchAlert;
					break;

				case SavedSearchTypes.CareerPostingSearch:
					emailTemplateData = ProcessJobSearchAlert(alert, lastAlertDate);
					emailTemplateType = EmailTemplateTypes.CareerJobSearchAlert;
					break;
			}

			if (emailTemplateData.IsNotNull())
			{
				var emailTemplate = Helpers.Email.GetEmailTemplatePreview(emailTemplateType, emailTemplateData);
				Helpers.Email.SendEmail(alert.AlertEmailAddress, "", "", emailTemplate.Subject, emailTemplate.Body, isBodyHtml: (alert.AlertEmailFormat == EmailFormats.HTML));
			}

			var savedSearch = Repositories.Core.FindById<SavedSearch>(alert.Id);

			if (savedSearch.IsNotNull())
			{
				savedSearch.AlertEmailScheduledOn = nextScheduledAlert;
			}

			Repositories.Core.SaveChanges();
		}

		/// <summary>
		/// Processes an alert for candidate searches against jobs in Talent or Assist
		/// </summary>
		/// <param name="alert">The alert to process</param>
		/// <param name="lastAlertDate">The date/time the alert was last processed</param>
		/// <returns>
		/// The template data for an email
		/// </returns>
		private EmailTemplateData ProcessCandidateSearchAlert(ActiveSearchAlertView alert, DateTime lastAlertDate)
		{
			EmailTemplateData emailTemplateData = null;

			var searchCriteria = (CandidateSearchCriteria)alert.SearchCriteria.Deserialize(typeof(CandidateSearchCriteria));
			searchCriteria.CandidateUpdatedFrom = lastAlertDate;

			#region Search matching candidates

			// TODO: Don't think it needs all the functionality in the full search, may run quicker if we create a new method specific to alerts a bit like SearchPostings
			var searchResult = Helpers.Search.SearchResumes(searchCriteria, Guid.Empty);

			var matchingResumes = (searchResult.IsNotNull()) ? searchResult.Resumes : null;

			#endregion

			#region Construct email

			if (matchingResumes.IsNotNullOrEmpty())
			{
				var matchingResumeSummary = new StringBuilder("");

                foreach (var matchingResume in matchingResumes)
                {
                    matchingResumeSummary.AppendFormat("Candidate: {0}{1}{2}", matchingResume.Id, Environment.NewLine, Environment.NewLine);
					if (alert.AlertEmailFormat == EmailFormats.HTML)
					{
						if (matchingResume.StarRating == 5)
                            matchingResumeSummary.AppendFormat("<html><body>Match level: <img src='" + AppSettings.AssistApplicationPath + "/Branding/Default/Images/starsfive.png '/> </body></html>", Environment.NewLine);
						else if (matchingResume.StarRating == 4)
                            matchingResumeSummary.AppendFormat("<html><body>Match level: <img src='" + AppSettings.AssistApplicationPath + "/Branding/Default/Images/starsfour.png '/> </body></html>", Environment.NewLine);
						else if (matchingResume.StarRating == 3)
                            matchingResumeSummary.AppendFormat("<html><body>Match level: <img src='" + AppSettings.AssistApplicationPath + "/Branding/Default/Images/starsthree.png '/> </body></html>", Environment.NewLine);
						else if (matchingResume.StarRating == 2)
                            matchingResumeSummary.AppendFormat("<html><body>Match level: <img src='" + AppSettings.AssistApplicationPath + "/Branding/Default/Images/starstwo.png '/> </body></html>", Environment.NewLine);
						else if (matchingResume.StarRating == 1)
                            matchingResumeSummary.AppendFormat("<html><body>Match level: <img src='" + AppSettings.AssistApplicationPath + "/Branding/Default/Images/starsone.png '/> </body></html>", Environment.NewLine);
						else
							matchingResumeSummary.AppendFormat("<html><body>Match level:<img src='" + AppSettings.AssistApplicationPath +"/Branding/Default/Images/starszero.png '/> </body></html>", Environment.NewLine);
					}
					else
					{
						if (matchingResume.StarRating == 5)
							matchingResumeSummary.AppendFormat("Match level: {0}{1}", "***** (5 Stars)", Environment.NewLine);
						else if (matchingResume.StarRating == 4)
							matchingResumeSummary.AppendFormat("Match level: {0}{1}", "**** (4 Stars)", Environment.NewLine);
						else if (matchingResume.StarRating == 3)
							matchingResumeSummary.AppendFormat("Match level: {0}{1}", "*** (3 Stars)", Environment.NewLine);
						else if (matchingResume.StarRating == 2)
							matchingResumeSummary.AppendFormat("Match level: {0}{1}", "** (2 Stars)", Environment.NewLine);
						else if (matchingResume.StarRating == 1)
							matchingResumeSummary.AppendFormat("Match level: {0}{1}", "* (1 Star)", Environment.NewLine);
						else
							matchingResumeSummary.AppendFormat("Match level: {0}{1}", "No Stars", Environment.NewLine);
					}

					if (matchingResume.EmploymentHistories.IsNotNullOrEmpty())
						matchingResumeSummary.AppendFormat("Most recent position: {0} {1}-{2} ({3}){4}{4}", matchingResume.EmploymentHistories[0].JobTitle, matchingResume.EmploymentHistories[0].YearStart, matchingResume.EmploymentHistories[0].YearEnd, matchingResume.EmploymentHistories[0].Employer, Environment.NewLine);
				}
				emailTemplateData = new EmailTemplateData { MatchingCandidates = matchingResumeSummary.ToString(), SavedSearchName = alert.Name };
			}

			#endregion

			return emailTemplateData;
		}

		/// <summary>
		/// Processes an alert for candidate searches against jobs in Talent or Assist
		/// </summary>
		/// <param name="alert">The alert to process</param>
		/// <param name="lastAlertDate">The date/time the alert was last processed</param>
		/// <returns>
		/// The template data for an email
		/// </returns>
		private EmailTemplateData ProcessJobSearchAlert(ActiveSearchAlertView alert, DateTime lastAlertDate)
		{
			//bool? isVeteran = null;

			EmailTemplateData emailTemplateData = null;
			var searchCriteria = (SearchCriteria)alert.SearchCriteria.Deserialize(typeof(SearchCriteria));

			if (searchCriteria.PostingAgeCriteria.IsNull())
				searchCriteria.PostingAgeCriteria = new PostingAgeCriteria();

			searchCriteria.PostingAgeCriteria.PostingAgeInDays = Convert.ToInt32(DateTime.Now.Date.Subtract(lastAlertDate.Date).TotalDays);
			/*
			if (AppSettings.VeteranPriorityServiceEnabled)
			{
				isVeteran = Repositories.Core.Resumes
																.Where(resume => resume.PersonId == alert.PersonId && resume.IsDefault && resume.StatusId == ResumeStatuses.Active)
																.Select(resume => resume.IsVeteran)
																.FirstOrDefault();

				if (!isVeteran.GetValueOrDefault(false))
					searchCriteria.PostingAgeCriteria.PostingAgeInDays += 1;
			}
			*/

			if (searchCriteria.RequiredResultCriteria.IsNull())
				searchCriteria.RequiredResultCriteria = new RequiredResultCriteria();

			searchCriteria.RequiredResultCriteria.IncludeDuties = true;
			searchCriteria.RequiredResultCriteria.MaximumDocumentCount = AppSettings.MaximumNoDocumentToReturnInJobAlert;

			var matchingPostings = Helpers.Search.SearchPostings(searchCriteria, alert.UserId, alert.PersonId, true, false);

            List<string> pastPostingIds = new List<string>();
            //get past job sent in alerts as list. list contains posting information as json array string
            var pastJobsSentInAlerts = Repositories.Core.JobsSentInAlerts.Where(j => j.UserId == alert.UserId).Select(p => p.Postings).ToList();

            //extract all the posting id's to a list from the list of json array
            pastJobsSentInAlerts.ForEach(pastAlert =>
            {
                var pastJobAlertIds = pastAlert.DeserializeJson<JobsSentInAlertsModel[]>().ToList().Select(p => p.LensPostingId);

                if (pastJobAlertIds.IsNotNull())
                    pastPostingIds.AddRange(pastJobAlertIds);
            });

            //remove all the previosly sent job postings.
            matchingPostings.RemoveAll(mp => matchingPostings.Select(mps => mp.Id).ToList().Intersect(pastPostingIds).Contains(mp.Id));

			if (matchingPostings.IsNotNullOrEmpty())
			{
				var matchingPostingsSummary = new StringBuilder();

				// ReSharper disable PossibleNullReferenceException
				matchingPostings.ForEach(matchingPosting =>
				{
                    var starMatch = String.Empty;
                    if (alert.AlertEmailFormat == EmailFormats.HTML)
                    {
                        if (matchingPosting.StarRating == 5)
                            starMatch = "<html><body>Match level: <img src='" + AppSettings.AssistApplicationPath + "/Branding/Default/Images/starsfive.png '/> </body></html>";
                        else if (matchingPosting.StarRating == 4)
                            starMatch = "<html><body>Match level: <img src='" + AppSettings.AssistApplicationPath + "/Branding/Default/Images/starsfour.png '/> </body></html>";
                        else if (matchingPosting.StarRating == 3)
                            starMatch = "<html><body>Match level: <img src='" + AppSettings.AssistApplicationPath + "/Branding/Default/Images/starsthree.png '/> </body></html>";
                        else if (matchingPosting.StarRating == 2)
                            starMatch = "<html><body>Match level: <img src='" + AppSettings.AssistApplicationPath + "/Branding/Default/Images/starstwo.png '/> </body></html>";
                        else if (matchingPosting.StarRating == 1)
                            starMatch = "<html><body>Match level: <img src='" + AppSettings.AssistApplicationPath + "/Branding/Default/Images/starsone.png '/> </body></html>";
                        else
                            starMatch = "<html><body>Match level: <img src='" + AppSettings.AssistApplicationPath + "/Branding/Default/Images/starszero.png '/> </body></html>";
                    }
                    else
                    {
                        if (matchingPosting.StarRating == 5)
                            starMatch = "Match level: ***** (5 Stars)";
                        else if (matchingPosting.StarRating == 4)
                            starMatch = "Match level: **** (4 Stars)";
                        else if (matchingPosting.StarRating == 3)
                            starMatch = "Match level: *** (3 Stars)";
                        else if (matchingPosting.StarRating == 2)
                            starMatch = "Match level: ** (2 Stars)";
                        else if (matchingPosting.StarRating == 1)
                            starMatch = "Match level: * (1 Star)";
                        else
                            starMatch = "Match level: No Stars";
                    }
					var careerUrl = string.Concat(AppSettings.CareerApplicationPath, "/jobposting/", matchingPosting.Id, "/Home/Home");

					var duties = matchingPosting.Duties.ToConcatenatedString(Environment.NewLine);
					var truncatedDuties = GetTruncatedJobDescription(duties, AppSettings.JobAlertTruncateWords);

					if (alert.AlertEmailFormat == EmailFormats.HTML)
						matchingPostingsSummary.AppendFormat("{0} <a href='{1}'>{2}</a>: {3}\t{4}\t{5}{6}",
                                                                                                    starMatch,
																									careerUrl,
																									matchingPosting.JobTitle,
																									matchingPosting.Employer,
																									matchingPosting.Location,
																									matchingPosting.JobDate.ToString("dd MMM"),
																									Environment.NewLine);
					else
						matchingPostingsSummary.AppendFormat("{0} {1} ({2}): {3}\t{4}\t{5}{6}",
                                                                                                    starMatch,
																									matchingPosting.JobTitle,
																									careerUrl,
																									matchingPosting.Employer,
																									matchingPosting.Location,
																									matchingPosting.JobDate.ToString("dd MMM"),
																									Environment.NewLine);

					matchingPostingsSummary.AppendLine(truncatedDuties);
				});
				// ReSharper restore PossibleNullReferenceException

				if (matchingPostings.IsNotNullOrEmpty() && AppSettings.IntegrationClient != IntegrationClient.Standalone && alert.Name == "Default job alert")
				{
					var externalSeekerId = Repositories.Core.Users.Where(u => u.Id == alert.UserId).Select(u => u.ExternalId).FirstOrDefault();
					if (externalSeekerId.IsNotNullOrEmpty())
					{
						var jobMatchPostingIds = matchingPostings.Where(mp => mp.OriginId.IsTalentOrigin(AppSettings)).Select(mp => mp.Id).Take(10).ToList();
						var jobMatches = (from p in Repositories.Core.Postings
						                  join j in Repositories.Core.Jobs
							                  on p.JobId equals j.Id
						                  where jobMatchPostingIds.Contains(p.LensPostingId)
						                    & j.ExternalId != null
						                  select j.ExternalId).ToList();

						if (jobMatches.IsNotNullOrEmpty())
						{
							var saveJobMatchesRequest = new SaveJobMatchesRequest
							{
								ExternalAdminId = externalSeekerId,
								Jobs = jobMatches,
								ExternalJobSeekerId = externalSeekerId,
								UserId = alert.UserId
							};

							Helpers.Messaging.Publish(new IntegrationRequestMessage
							{
								ActionerId = alert.UserId,
								IntegrationPoint = IntegrationPoint.SaveJobMatches,
								SaveJobMatchesRequest = saveJobMatchesRequest
							});
						}
					}
				}
                #region Stores the Jobs sent to the JobSeeker via alert

                var MatchPostingIds = matchingPostings.Select(mp => mp.Id).ToList();
                var json = matchingPostings.Select(m => new Focus.Core.Models.JobsSentInAlertsModel { LensPostingId = m.Id, JobTitle = m.JobTitle, OpenDate = m.JobDate.ToString() }).SerializeJson();
                var jobsentinalerts = new JobsSentInAlerts();

                jobsentinalerts.AlertDate = DateTime.Now;
                jobsentinalerts.SavedSearchId = alert.Id;
                jobsentinalerts.UserId = alert.UserId;
                jobsentinalerts.Postings = json;
                Repositories.Core.Add(jobsentinalerts);
                Repositories.Core.SaveChanges();

                #endregion
				emailTemplateData = new EmailTemplateData
				{
					RecipientName = alert.Recipient,
					MatchingJobs = matchingPostingsSummary.ToString(),
					SavedSearchName = alert.Name
				};
			}

			return emailTemplateData;
		}

		/// <summary>
		/// Gets the first 'n' words of the job description.
		/// </summary>
		/// <param name="duties">The job description.</param>
		/// <param name="numberOfWords">The number of words required.</param>
		/// <returns>The description truncacted to 'n' words</returns>
		private static string GetTruncatedJobDescription(string duties, int numberOfWords)
		{
			if (duties.IsNullOrEmpty() || numberOfWords < 0)
				return duties;

			if (numberOfWords == 0)
				return string.Empty;

			var pattern = string.Format(@"(\w+\W+){{1,{0}}}", numberOfWords);
			var truncatedDuties = Regex.Match(duties, pattern).Value.Trim();
			return string.Concat(truncatedDuties, " ...", Environment.NewLine);
		}
	}
}
