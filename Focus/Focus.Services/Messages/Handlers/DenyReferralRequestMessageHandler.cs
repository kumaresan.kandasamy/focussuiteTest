﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Focus.Core;
using Focus.Core.EmailTemplate;
using Framework.Core;
using Framework.Lightspeed;
using Framework.Logging;
using Framework.Messaging;

#endregion

namespace Focus.Services.Messages.Handlers
{
	public class DenyReferralRequestMessageHandler : MessageHandlerBase, IMessageHandler<DenyReferralRequestMessage>
	{
		const int UpdateBatchSize = 500;

		public DenyReferralRequestMessageHandler() : base(null) { }

		public DenyReferralRequestMessageHandler(IRuntimeContext runtimeContext) : base(runtimeContext) { }

		/// <summary>
		/// Handles the specified message as called by the message bus
		/// </summary>
		/// <param name="message">The message.</param>
		public void Handle(DenyReferralRequestMessage message)
		{
			message.InitialiseRuntimeContext(RuntimeContext);

			RuntimeContext.Helpers.Candidate.AutoDenyReferrals();
		}
	}
}
