﻿#region Copyright © 2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;

using Focus.Core;
using Focus.Core.IntegrationMessages;
using Focus.Data.Core.Entities;
using Focus.Services.Repositories;
using Focus.Services.ServiceImplementations;
using Framework.Core;
using Framework.Messaging;

#endregion

namespace Focus.Services.Messages.Handlers
{
	public class ProcessPostingMessageHandler : MessageHandlerBase, IMessageHandler<ProcessPostingMessage>
	{

		public ProcessPostingMessageHandler() : base(null) { }
		public ProcessPostingMessageHandler(IRuntimeContext runtimeContext) : base(runtimeContext) {}

		public void Handle(ProcessPostingMessage message)
		{
			message.InitialiseRuntimeContext(RuntimeContext);

			#region Register/Unregister in Lens

			if (message.RegisterPosting)
			{
				if (!message.IgnoreLens)
					Helpers.Posting.RegisterPosting(message.PostingId);
			}
			else
			{
				Helpers.Posting.UnregisterPosting(message.PostingId);
				Helpers.JobDevelopment.CleanPersonPostingMatches(message.PostingId, 0);
			}

			#endregion

			#region Integration

			if (!message.IgnoreClient && AppSettings.IntegrationClient != IntegrationClient.Standalone)
			{
				var actionType = message.RegisterPosting
					                 ? ActionTypes.PostJobToLens
					                 : ActionTypes.UnregisterJobFromLens;

				var integrationRepository = new IntegrationRepository(RuntimeContext);
				var jobId = message.JobId;
				
				if (jobId.IsNull() || jobId == 0)
				{
					jobId = Repositories.Core.Postings.Where(p => p.Id == message.PostingId).Select(p => p.JobId).FirstOrDefault() ?? 0;
				}

				var actionData = integrationRepository.GetActionData(actionType, jobId, message.ActionerId);
				if (actionData.IsNotNullOrEmpty())
				{
					var activeJobsMessage = new IntegrationRequestMessage
					{
						ActionerId = message.ActionerId,
						IntegrationPoint = IntegrationPoint.LogAction,
						LogActionRequest = new LogActionRequest
						{
							ActionerExternalId = message.ExternalAdminId,
							ActionedOn = DateTime.Now,
							ActionType = actionType,
							ActionData = actionData
						}
					};
					Helpers.Messaging.Publish(activeJobsMessage);
				}
			}

			if (AppSettings.IntegrationClient != IntegrationClient.Standalone)
			{
				// This shouldn't ever happen as a Posting to get here should always have a job id
				// but in case it doesn't we are going to throw an exception
				var jobId = RuntimeContext.Repositories.Core.Postings.Single(x => x.Id == message.PostingId).JobId.GetValueOrDefault(-1);

				if (jobId == -1)
					throw new Exception("Posting #" + message.PostingId + " isn't associtated to a job and therefore can't be included in an integration request");

				var employerDetails = (from job in Repositories.Core.Jobs
															 join employee in Repositories.Core.Employees
																 on job.EmployeeId equals employee.Id
															 join user in Repositories.Core.Users
																 on employee.PersonId equals user.PersonId
															 join employer in Repositories.Core.Employers
																 on job.EmployerId equals employer.Id
															 where job.Id == message.JobId
															 select new
															 {
																 EmployeeId = user.ExternalId,
																 EmployerFocusId = employer.Id,
																 EmployerId = employer.ExternalId,
																 JobId = job.ExternalId
															 }).FirstOrDefault();

				// Attempt to re-send employer to integration if the employer has no external id
				if (employerDetails.IsNotNull() && employerDetails.EmployerId.IsNullOrEmpty() && employerDetails.JobId.IsNullOrEmpty())
				{
					var employer = Repositories.Core.FindById<Employer>(employerDetails.EmployerFocusId);
					employer.PublishEmployerUpdateIntegrationRequest(message.ActionerId, RuntimeContext);
				}

				var saveJobRequest = new SaveJobRequest
				{
					JobId = jobId,
					UserId = message.ActionerId,
					ExternalAdminId = message.ExternalAdminId,
					ExternalOfficeId = message.ExternalOfficeId,
					ExternalPassword = message.ExternalPassword,
					ExternalUsername = message.ExternalUsername
				};
				var integrationRequest = new IntegrationRequestMessage
				{
					IntegrationPoint = IntegrationPoint.SaveJob,
					ActionerId = message.ActionerId,
					SaveJobRequest = saveJobRequest,
					ExternalAdminId = message.ExternalAdminId,
					ExternalOfficeId = message.ExternalOfficeId,
					ExternalPassword = message.ExternalPassword,
					ExternalUsername = message.ExternalUsername
				};

				Helpers.Messaging.Publish(integrationRequest);

				if (message.RegisterPosting)
				{
					var userType = Repositories.Core.Users.Where(u => u.Id == message.ActionerId).Select(u => u.UserType).FirstOrDefault();
					if (userType == UserTypes.Assist)
					{
						if (employerDetails.IsNotNull() && employerDetails.JobId.IsNullOrEmpty())
						{
							var activityRequest = new AssignEmployerActivityRequest
							{
								EmployerId = employerDetails.EmployerId,
								EmployeeId = employerDetails.EmployeeId,
								ActivityId = "16",
								UserId = message.ActionerId,
							};
							integrationRequest = new IntegrationRequestMessage
							{
								IntegrationPoint = IntegrationPoint.AssignEmployerActivity,
								ActionerId = message.ActionerId,
								AssignEmployerActivityRequest = activityRequest,
								ExternalAdminId = message.ExternalAdminId,
								ExternalOfficeId = message.ExternalOfficeId,
								ExternalPassword = message.ExternalPassword,
								ExternalUsername = message.ExternalUsername
							};
							Helpers.Messaging.Publish(integrationRequest);
						}
					}
				}
			}
			#endregion

			#region Generate matches

			if (message.RegisterPosting && !message.IgnoreLens)
				Helpers.JobDevelopment.GeneratePersonMatchesForPosting(message.PostingId, message.IsVeteranJob, message.Culture, message.IgnoreClient);

			#endregion
		}
	}
}
