﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using Focus.Core;
using Focus.Core.Criteria.JobDistribution;
using Focus.Data.Report.Entities;
using Framework.Core;
using Framework.Messaging;

#endregion

namespace Focus.Services.Messages.Handlers
{
	public class ProcessStatisticsMessageHandler : MessageHandlerBase, IMessageHandler<ProcessStatisticsMessage>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="ProcessStatisticsMessageHandler"/> class.
		/// </summary>
		public ProcessStatisticsMessageHandler() : base(null) { }

		/// <summary>
		/// Initializes a new instance of the <see cref="ProcessStatisticsMessageHandler"/> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		public ProcessStatisticsMessageHandler(IRuntimeContext runtimeContext) : base(runtimeContext) { }

		/// <summary>
		/// Handles the specified message.
		/// </summary>
		/// <param name="message">The message.</param>
		public void Handle(ProcessStatisticsMessage message)
		{
			message.InitialiseRuntimeContext(RuntimeContext);

			var totalSearchableResumes = GetTotalSearchableResumes();
			AddStatistic(StatisticType.TotalSearchableResumes, totalSearchableResumes.ToString());

			var totalSearchableResumesAddedYesterday = GetTotalSearchableResumesAddedYesterday();
			AddStatistic(StatisticType.TotalSearchableResumesAddedYesterday, totalSearchableResumesAddedYesterday.ToString());

			var totalActiveTalentPostings = GetTotalActiveTalentPostings();
			AddStatistic(StatisticType.TotalActiveTalentPostings, totalActiveTalentPostings.ToString());

			var totalActiveTalentPostingsAddedYesterday = GetTotalActiveTalentPostingsAddedYesterday();
			AddStatistic(StatisticType.TotalActiveTalentPostingsAddedYesterday, totalActiveTalentPostingsAddedYesterday.ToString());

			if (AppSettings.JobDistributionFeeds.IsNotNullOrEmpty())
			{
				var totalActiveSpideredJobs = GetTotalActiveSpideredJobs();
				AddStatistic(StatisticType.TotalActiveSpideredJobs, totalActiveSpideredJobs.ToString());
			}

			Repositories.Report.SaveChanges(true);

			CleanUp();
		}

		/// <summary>
		/// Gets the total searchable resumes.
		/// </summary>
		/// <returns></returns>
		private int GetTotalSearchableResumes()
		{
			return Repositories.Core.Resumes.Count(r => r.IsSearchable && r.IsDefault && r.StatusId == ResumeStatuses.Active && r.ResumeCompletionStatusId.Equals(ResumeCompletionStatuses.Completed));
		}

		/// <summary>
		/// Gets the total searchable resumes added yesterday.
		/// </summary>
		/// <returns></returns>
		private int GetTotalSearchableResumesAddedYesterday()
		{
			var yesterday = DateTime.Now.AddDays(-1);
			var startTs = new TimeSpan(00, 00, 00);
			var startYesterday = yesterday.Date + startTs;

			var endTs = new TimeSpan(23, 59, 59);
			var endYesterday = yesterday.Date + endTs;

			return Repositories.Core.Resumes.Count(r => r.IsSearchable && r.IsDefault && r.StatusId == ResumeStatuses.Active && r.ResumeCompletionStatusId.Equals(ResumeCompletionStatuses.Completed) && r.CreatedOn >= startYesterday && r.CreatedOn <= endYesterday);
		}

		/// <summary>
		/// Gets the total active talent postings.
		/// </summary>
		/// <returns></returns>
		private int GetTotalActiveTalentPostings()
		{
			return Repositories.Core.Jobs.Count(j => j.JobStatus.Equals(JobStatuses.Active));
		}

		/// <summary>
		/// Gets the total active talent postings added yesterday.
		/// </summary>
		/// <returns></returns>
		private int GetTotalActiveTalentPostingsAddedYesterday()
		{
			var yesterday = DateTime.Now.AddDays(-1);
			var startTs = new TimeSpan(00, 00, 00);
			var startYesterday = yesterday.Date + startTs;

			var endTs = new TimeSpan(23, 59, 59);
			var endYesterday = yesterday.Date + endTs;

			return Repositories.Core.Jobs.Count(j => j.JobStatus.Equals(JobStatuses.Active) && j.PostedOn >= startYesterday && j.PostedOn <= endYesterday);
		}

		/// <summary>
		/// Gets the total active spidered jobs.
		/// </summary>
		/// <returns></returns>
		private int GetTotalActiveSpideredJobs()
		{
			var criteria = new JobDistributionCriteria
			{
				Category = "Active"
			};

			var counts = Repositories.LiveJobs.GetJobDistribution(criteria);

			var feeds = AppSettings.JobDistributionFeeds.Split(',');

			return counts.Where(count => feeds.Contains(count.FeedName, StringComparer.OrdinalIgnoreCase))
									 .Sum(count => count.Count);
		}

		/// <summary>
		/// Adds the statistic to the statistic entity
		/// </summary>
		/// <param name="statisticType">Type of the statistic.</param>
		/// <param name="value">The value.</param>
		private void AddStatistic(StatisticType statisticType, string value)
		{
			var statistic = new Statistic {StatisticType = statisticType, Value = value, CreatedOn = DateTime.Now};

			Repositories.Report.Add(statistic);
		}

		/// <summary>
		/// Cleans up the Statistic entity.
		/// Only data for the last x days should be kept, where x is defined in the StatisticsArchiveInDays config setting
		/// </summary>
		private void CleanUp()
		{
			var statistics = Repositories.Report.Statistics.Where(s => s.CreatedOn < DateTime.Now.AddDays(-(AppSettings.StatisticsArchiveInDays))).ToList();

			foreach (var statistic in statistics)
				Repositories.Report.Remove(statistic);

			Repositories.Report.SaveChanges(true);
		}
	}
}
