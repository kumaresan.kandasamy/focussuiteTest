﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Focus.Core;
using Focus.Core.EmailTemplate;
using Focus.Data.Core.Entities;
using Focus.Services.Core;
using Focus.Services.Mappers;
using Framework.Core;
using Framework.Logging;
using Framework.Messaging;

#endregion

namespace Focus.Services.Messages.Handlers
{
	/// <summary>
	/// 
	/// </summary>
	public class CloseExpiredJobMessageHandler : MessageHandlerBase, IMessageHandler<CloseExpiredJobMessage>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="CloseExpiredJobMessageHandler"/> class.
		/// </summary>
		public CloseExpiredJobMessageHandler() : base(null) {}

		/// <summary>
		/// Initializes a new instance of the <see cref="CloseExpiredJobMessageHandler"/> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		public CloseExpiredJobMessageHandler(IRuntimeContext runtimeContext) : base(runtimeContext) {}

		private CloseExpiredJobMessage _message;

		/// <summary>
		/// Handles the specified message.
		/// </summary>
		/// <param name="message">The message.</param>
		public void Handle(CloseExpiredJobMessage message)
		{
			_message = message;

			_message.InitialiseRuntimeContext(RuntimeContext);
			
			if(message.JobId.IsNull())
				throw new Exception("No Job Id.");

			var job = Repositories.Core.Jobs.FirstOrDefault(x => x.Id == message.JobId);

			if(job.IsNull())
				throw new Exception(string.Format("Job not found with Id {0}.", message.JobId));


			// First remove all expiry message pertaining to this jobs
			var messages = Repositories.Core.Messages.Where(x => x.EntityId == job.Id && x.MessageType == MessageTypes.ImpendingJobExpiryWarning).ToList();

			foreach (var existingMessage in messages)
				Repositories.Configuration.Remove(existingMessage);

			// Add email item for each hiring manager that posted job
			var employee = Repositories.Core.FindById<Employee>(job.EmployeeId.GetValueOrDefault());

			job.JobStatus = JobStatuses.Closed;
			job.ClosedBy = RuntimeContext.CurrentRequest.UserContext.UserId;
			job.ClosedOn = DateTime.Now;

      Repositories.Core.SaveChanges();

			try
			{
				if (job.Posting.IsNotNull())
					Helpers.Messaging.Publish(RuntimeContext.CurrentRequest.ToUnregisterPostingMessage(job.Posting.Id));
			}
			catch (Exception ex)
			{
				Logger.Error(RuntimeContext.CurrentRequest.LogData(), "Couldn't add message to ", ex);
			}

			if (employee != null)
			{
				var talentPath = AppSettings.TalentApplicationPath.EndsWith("/") ? AppSettings.TalentApplicationPath : string.Concat(AppSettings.TalentApplicationPath, "/");
				var jobLinkUrl = string.Format("{0}job/view/{1}", talentPath, job.Id);

				var emailTemplateData = new EmailTemplateData
				{
					JobTitle = job.JobTitle,
					JobId = job.Id.ToString(CultureInfo.InvariantCulture),
					JobLinkUrls = new List<string> { jobLinkUrl }
				};
				
				var emailTemplate = Helpers.Email.GetEmailTemplatePreview(EmailTemplateTypes.EmployerNotificationJobExpiration, emailTemplateData);
				Helpers.Email.SendEmail(employee.Person.EmailAddress, string.Empty, string.Empty, emailTemplate.Subject, emailTemplate.Body, true);

				Helpers.Alerts.CreateAlertMessage(Constants.AlertMessageKeys.JobExpired, employee.Person.User.Id, DateTime.Now.Date.AddDays(7), MessageAudiences.User, MessageTypes.General, job.Id, job.JobTitle);
			}

			// Check if there are any pending referrals and place these On-Hold
			if (job.Posting.IsNotNull() && job.Posting.Applications.IsNotNullOrEmpty())
			{
				var statuses = new List<ApprovalStatuses> { ApprovalStatuses.Reconsider, ApprovalStatuses.WaitingApproval };

				var pendingApplications = job.Posting.Applications.Where(x => statuses.Contains(x.ApprovalStatus)).Select(x => x.Id).ToList();

				foreach (var applicationId in pendingApplications)
				{
					HoldApplication(applicationId);
				}
			}

      Repositories.Core.SaveChanges();
			Helpers.Logging.LogAction(ActionTypes.CloseJob, job);
		}

		/// <summary>
		/// Holds the application.
		/// </summary>
		/// <param name="applicationId">The application identifier.</param>
		private void HoldApplication(long applicationId)
		{
			var holdMessage = new HoldReferralMessage()
			{
				ActionerId = _message.ActionerId,
				Culture = _message.Culture,
				ApplicationId = applicationId,
				Reason = ApplicationOnHoldReasons.JobClosed
			};

			Helpers.Messaging.Publish(holdMessage);
		}
	}
}
