﻿#region Copyright © 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives


using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

using Focus.Core;
using Focus.Core.EmailTemplate;
using Focus.Data.Core.Entities;

using Framework.Core;
using Framework.Messaging;

#endregion

namespace Focus.Services.Messages.Handlers
{
  public class ProcessRegistrationPinEmailChildMessageHandler : MessageHandlerBase, IMessageHandler<ProcessRegistrationPinEmailChildMessage>
  {
    /// <summary>
    /// Initializes a new instance of the <see cref="ProcessRegistrationPinEmailMessageHandler"/> class.
    /// </summary>
    public ProcessRegistrationPinEmailChildMessageHandler() : base(null) { }

    /// <summary>
    /// Initializes a new instance of the <see cref="ProcessRegistrationPinEmailMessageHandler"/> class.
    /// </summary>
    /// <param name="runtimeContext">The runtime context.</param>
    public ProcessRegistrationPinEmailChildMessageHandler(IRuntimeContext runtimeContext) : base(runtimeContext) { }

    /// <summary>
    /// Handles the specified message.
    /// </summary>
    /// <param name="message">The message.</param>
    public void Handle(ProcessRegistrationPinEmailChildMessage message)
    {
      message.InitialiseRuntimeContext(RuntimeContext);

      var email = message.Email;

      var existingUserDetails = (from u in Repositories.Core.Users
                                 join p in Repositories.Core.Persons
                                   on u.PersonId equals p.Id
                                 where p.EmailAddress == email || u.UserName == email
                                 select u).FirstOrDefault();

      var existingRegistrationPin = Repositories.Core.RegistrationPins.FirstOrDefault(x => x.EmailAddress == email);

      var emailsToSend = new List<object>();

      #region Work out the user type for the module

      var userType = UserTypes.Anonymous;

      switch (message.TargetModule)
      {
        case FocusModules.Assist:
          userType = UserTypes.Assist;
          break;
        case FocusModules.Talent:
          userType = UserTypes.Talent;
          break;
        case FocusModules.CareerExplorer:
          userType = UserTypes.Career | UserTypes.Explorer;
          break;
        case FocusModules.Career:
          userType = UserTypes.Career;
          break;
        case FocusModules.Explorer:
          userType = UserTypes.Explorer;
          break;
      }

      #endregion

	    RegistrationPin registrationPin = null;

      // If this email hasn't been used to register a user already
      if (existingUserDetails.IsNull())
      {
        string pin;

        if (existingRegistrationPin.IsNull())
        {
          #region If this email hasn't been used to generate a PIN already, generate a PIN and create a RegistrationPin record

          pin = GenerateRegistrionPin();

          registrationPin = new RegistrationPin
	        {
		        EmailAddress = email, 
						Pin = pin,
						CreatedBy = message.ActionerId
	        };

          Repositories.Core.Add(registrationPin);

          #endregion
        }
        else
        {
          #region If this email has been used to generate a PIN already, grab the existing PIN

          pin = existingRegistrationPin.Pin;

          #endregion
        }

        // Generate registration url with the PIN
        var pinRegistrationUrl = string.Format(message.RegistrationUrl, pin);

        // Send an email with the PIN and url
        emailsToSend.Add(new
        {
          EmailType = "AccountRegistration",
          Email = email,
          PinRegistrationUrl = pinRegistrationUrl,
          Pin = pin,
          message.TargetModule
        });
      }
      else
      {
        if (existingUserDetails.UserType == userType)
        {
          var user = existingUserDetails;
          var firstName = Repositories.Core.Persons.Where(p => p.Id == user.PersonId).Select(p => p.FirstName).FirstOrDefault();

          user.ValidationKey = GenerateRandomHash();

          emailsToSend.Add(new
          {
            EmailType = "ResetPassword",
            Email = email,
            ResetPasswordUrl = string.Concat(message.ResetPasswordUrl, "?", user.ValidationKey),
            FirstName = firstName,
            message.TargetModule
          });
        }
      }

      Repositories.Core.SaveChanges();

      // Send all the emails to the message bus for processing
      foreach (dynamic emailToSend in emailsToSend)
      {
        if (emailToSend.EmailType == "AccountRegistration")
          SendAccountRegistrationEmail(emailToSend.Email, emailToSend.PinRegistrationUrl, emailToSend.Pin, emailToSend.TargetModule);

        if (emailToSend.EmailType == "ResetPassword")
          SendResetPasswordEmail(emailToSend.Email, emailToSend.ResetPasswordUrl, emailToSend.FirstName, emailToSend.TargetModule);
      }

			if (registrationPin.IsNotNull())
			{
				Helpers.Logging.LogAction(ActionTypes.CreateRegistrationPin, typeof(RegistrationPin).Name, registrationPin.Id, additionalDetails: registrationPin.Pin);
			}
    }

    /// <summary>
    /// Sends the account registration email.
    /// </summary>
    /// <param name="email">The email.</param>
    /// <param name="registrationUrl">The registration URL.</param>
    /// <param name="pin">The pin.</param>
    /// <param name="targetModule">The target module.</param>
    private void SendAccountRegistrationEmail(string email, string registrationUrl, string pin, FocusModules targetModule)
    {
      switch (targetModule)
      {
        case FocusModules.Career:
        case FocusModules.CareerExplorer:
          var template = new EmailTemplateData
          {
            EmailAddress = email,
            PinNumber = pin,
            AuthenticateUrl = registrationUrl
          };
          var preview = Helpers.Email.GetEmailTemplatePreview(EmailTemplateTypes.CareerAccountRegistration, template);

          Helpers.Email.SendEmail(email, "", "", preview.Subject, preview.Body, true);
          break;
      }
    }

    /// <summary>
    /// Sends the reset password email.
    /// </summary>
    /// <param name="email">The email.</param>
    /// <param name="resetPasswordUrl">The url to reset the password.</param>
    /// <param name="recipientName">Name of the recipient.</param>
    /// <param name="targetModule">The target module.</param>
    private void SendResetPasswordEmail(string email, string resetPasswordUrl, string recipientName, FocusModules targetModule)
    {
      switch (targetModule)
      {
        case FocusModules.Career:
        case FocusModules.CareerExplorer:
          var template = new EmailTemplateData
          {
            EmailAddress = email,
            Url = resetPasswordUrl,
            RecipientName = recipientName
          };
          var preview = Helpers.Email.GetEmailTemplatePreview(EmailTemplateTypes.CareerPasswordReset, template);

          Helpers.Email.SendEmail(email, "", "", preview.Subject, preview.Body, true, detectUrl: true);
          break;
      }
    }

    /// <summary>
    /// Generates the registrion pin.
    /// </summary>
    /// <returns></returns>
    private string GenerateRegistrionPin()
    {
      long i = 1;

      foreach (var b in Guid.NewGuid().ToByteArray())
        i *= (b + 1);

      return string.Format("{0:x}", i - DateTime.Now.Ticks);
    }

    /// <summary>
    /// Gets the encrypted GUID.
    /// </summary>
    /// <returns></returns>
    private static string GenerateRandomHash()
    {
      var buffer = Encoding.UTF8.GetBytes(Guid.NewGuid().ToString().Replace("-", ""));
      buffer = SHA512.Create().ComputeHash(buffer);

      return Convert.ToBase64String(buffer).Substring(0, 86);
    }
  }
}
