﻿#region Copyright © 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;

using Focus.Data.Core.Entities;

using Framework.Messaging;

using Mindscape.LightSpeed;
using Mindscape.LightSpeed.Querying;

#endregion

namespace Focus.Services.Messages.Handlers
{
  public class ProcessSessionMaintenanceMessageHandler : MessageHandlerBase, IMessageHandler<ProcessSessionMaintenanceMessage>
  {
    /// <summary>
		/// Initializes a new instance of the <see cref="ProcessSessionMaintenanceMessageHandler"/> class.
		/// </summary>
		public ProcessSessionMaintenanceMessageHandler() : base(null) {}

		/// <summary>
		/// Initializes a new instance of the <see cref="ProcessSessionMaintenanceMessageHandler"/> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		public ProcessSessionMaintenanceMessageHandler(IRuntimeContext runtimeContext) : base(runtimeContext) { }

		/// <summary>
		/// Handles the specified message.
		/// </summary>
		/// <param name="message">The message.</param>
		public void Handle(ProcessSessionMaintenanceMessage message)
		{
      var mins = AppSettings.SessionTimeout + 5;
		  
      if (mins < 1440) // 24 * 60
        mins = 1440;

		  var expiryDate = DateTime.UtcNow.AddMinutes(-mins);

      var expiredSessionsSql = string.Format(@"
DELETE
  SS
FROM
	[Data.Core.Session] S
INNER JOIN [Data.Core.SessionState] SS
	ON SS.SessionId = S.Id
WHERE
	S.LastRequestOn < '{0}'
	
DELETE
	S
FROM
	[Data.Core.Session] S
WHERE
	S.LastRequestOn < '{0}'", expiryDate.ToString("dd MMM yyyy HH:mm:ss.fff"));

		  const string deletedSessionStatesSql = @"
DELETE FROM
  [Data.Core.SessionState]
WHERE
  DeletedOn IS NOT NULL
";

      Repositories.Core.ExecuteNonQuery(expiredSessionsSql);
      Repositories.Core.ExecuteNonQuery(deletedSessionStatesSql);

      // Note that because SessionState uses "Soft Delete" the following code did not work.
      // The "Remove" method actually does a SQL UPDATE on the DeletedOn field.
      // This actually causes errors when it subsequently tries to to properly delete the "Session" record (which does not use soft delete and so does actually get deleted by "remove").

      /*
      var expiredSessions = new Query(typeof(Session), Entity.Attribute("LastRequestOn") < DateTime.UtcNow.AddMinutes(-mins));
      Repositories.Core.Remove(expiredSessions);

      var deletedSessionStates = new Query(typeof(SessionState), Entity.Attribute("DeletedOn") != null);
      Repositories.Core.Remove(deletedSessionStates);

      Repositories.Core.SaveChanges(true);
      */
		}
  }
}
