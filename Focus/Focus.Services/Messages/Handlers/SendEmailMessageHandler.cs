﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using System.Net.Mail;
using Focus.Core;
using Framework.Core;
using Framework.Email;
using Framework.Messaging;

#endregion

namespace Focus.Services.Messages.Handlers
{
	public class SendEmailMessageHandler : MessageHandlerBase, IMessageHandler<SendEmailMessage>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="SendEmailMessageHandler"/> class.
		/// </summary>
		public SendEmailMessageHandler() : base(null) {}

		/// <summary>
		/// Initializes a new instance of the <see cref="SendEmailMessageHandler"/> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		public SendEmailMessageHandler(IRuntimeContext runtimeContext) : base(runtimeContext) {}

		/// <summary>
		/// Handles the specified message.
		/// </summary>
		/// <param name="message">The message.</param>
		public void Handle(SendEmailMessage message)
		{
			message.InitialiseRuntimeContext(RuntimeContext);

			if (message.MailMessage.IsNotNull())
			{
				if (!String.IsNullOrEmpty(message.From)) message.MailMessage.From = new MailAddress(message.From);
				Emailer.SendEmail(message.MailMessage, false, message.IsBodyHtml);
			}
			else
				Emailer.SendEmail(message.From, to: message.To, cc: message.Cc, bcc: message.Bcc, subject: message.Subject, body: message.Body, sendAsync: false, isBodyHtml: message.IsBodyHtml, attachment: message.Attachment, attachmentName: message.AttachmentName);
		}
	}
}
