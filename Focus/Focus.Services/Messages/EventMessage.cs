﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

using Framework.Core;

using Focus.Core;
using Focus.Core.Messages;
using Focus.Core.Models;

#endregion

namespace Focus.Services.Messages
{
  public class EventMessage : MessageBase
  {
    /// <summary>
    /// Gets or sets the actioner id.
    /// </summary>
    /// <value>The actioner id.</value>
    [DataMember(Name = "ActionerId", IsRequired = true, Order = 1)]
    public long ActionerId { get; set; }

    /// <summary>
    /// Gets or sets the culture.
    /// </summary>
    /// <value>The culture.</value>
    [DataMember(Name = "Culture", IsRequired = true, Order = 2)]
    public string Culture { get; set; }

    /// <summary>
    /// Gets or sets the admin identifier used when authenticating an AOSOS API call.
    /// </summary>
    /// <value>
    /// The admin identifier.
    /// </value>
    [DataMember]
    public string ExternalAdminId { get; set; }

    /// <summary>
    /// Gets or sets the office identifier used when authenticating an AOSOS API call.
    /// </summary>
    /// <value>
    /// The office identifier.
    /// </value>
    [DataMember]
    public string ExternalOfficeId { get; set; }

    /// <summary>
    /// Gets or sets the username used when authenticating an AOSOS API call.
    /// </summary>
    /// <value>
    /// The username.
    /// </value>
    [DataMember]
    public string ExternalUsername { get; set; }

    /// <summary>
    /// Gets or sets the password used when authenticating an AOSOS API call.
    /// </summary>
    /// <value>
    /// The password.
    /// </value>
    [DataMember]
    public string ExternalPassword { get; set; }

    /// <summary>
    /// Initialises the runtime context.
    /// </summary>
    /// <param name="runtimeContext">The runtime context.</param>
    public void InitialiseRuntimeContext(IRuntimeContext runtimeContext)
    {
      var userId = GetMessageBusUserId(runtimeContext);
      var actionerId = (ActionerId.IsNotNull() && ActionerId > 0) ? ActionerId : userId;
      var sessionId = runtimeContext.Helpers.Session.CreateSession();
      var userContext = new UserContext
      {
        UserId = userId,
        ActionerId = actionerId,
        Culture = (Culture.IsNotNullOrEmpty()) ? Culture : Constants.DefaultCulture,
        ExternalOfficeId = this.ExternalOfficeId,
        ExternalPassword = this.ExternalPassword,
        ExternalUserId = this.ExternalAdminId,
        ExternalUserName = this.ExternalUsername
      };
      var eventMessageRequest = new EventMessageRequest
      {
        UserContext = userContext,
        RequestId = Guid.NewGuid(),
        SessionId = sessionId,
        ClientTag = runtimeContext.AppSettings.ClientTag
      };
      runtimeContext.SetRequest(eventMessageRequest);
    }
  }

  public class EventMessageRequest : ServiceRequest
  {

  }
}
