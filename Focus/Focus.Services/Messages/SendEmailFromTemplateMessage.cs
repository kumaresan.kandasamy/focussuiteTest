﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Net.Mail;
using System.Runtime.Serialization;
using Focus.Core;
using Focus.Core.EmailTemplate;

#endregion

namespace Focus.Services.Messages
{
	public class SendEmailFromTemplateMessage : EventMessage
	{
		[DataMember(Name = "TemplateType", Order = 10)]
		public EmailTemplateTypes TemplateType { get; set; }

		[DataMember(Name = "TemplateData", Order = 11)]
		public EmailTemplateData TemplateData { get; set; }

		[DataMember(Name = "To", Order = 12)]
		public string To { get; set; }

		[DataMember(Name = "Cc", Order = 13)]
		public string Cc { get; set; }

		[DataMember(Name = "Bcc", Order = 14)]
		public string Bcc { get; set; }

		[DataMember(Name = "SendAsync", Order = 15)]
		public bool SendAsync { get; set; }

		[DataMember(Name = "IsBodyHtml", Order = 16)]
		public bool IsBodyHtml { get; set; }

		[DataMember(Name = "Attachment", Order = 17)]
		public byte[] Attachment { get; set; }

		[DataMember(Name = "AttachmentName", Order = 18)]
		public string AttachmentName { get; set; }

		[DataMember(Name = "DetectUrl", Order = 20)]
		public bool DetectUrl { get; set; }
	}
}
