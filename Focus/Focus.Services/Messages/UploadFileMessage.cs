﻿#region Copyright © 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Runtime.Serialization;
using Focus.Core;

#endregion

namespace Focus.Services.Messages
{
  public class UploadFileMessage : EventMessage
  {
    [DataMember(Name = "UploadFileId", IsRequired = true)]
    public long UploadFileId { get; set; }

    [DataMember(Name = "UploadFileProcessingState", IsRequired = true)]
    public UploadFileProcessingState ProcessingState { get; set; }

    [DataMember(Name = "IgnoreHeader", IsRequired = false)]
    public bool IgnoreHeader { get; set; }
  }
}
