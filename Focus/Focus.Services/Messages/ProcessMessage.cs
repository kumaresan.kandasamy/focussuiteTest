﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;

using Focus.Core;
using Focus.Core.Messages;
using Focus.Core.Models;

#endregion



namespace Focus.Services.Messages
{
	public class ProcessMessage : MessageBase
	{
		/// <summary>
		/// Initialises the runtime context.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		public void InitialiseRuntimeContext(IRuntimeContext runtimeContext)
		{
			var userId = GetMessageBusUserId(runtimeContext);

			var sessionId = runtimeContext.Helpers.Session.CreateSession();

			runtimeContext.SetRequest(new ProcessMessageRequest
			                          	{
			                          		UserContext = new UserContext {UserId = userId, ActionerId = userId, Culture = Constants.DefaultCulture}, 
																		RequestId = Guid.NewGuid(),
																		SessionId = sessionId,
																		ClientTag = runtimeContext.AppSettings.ClientTag
			                          	});
		}
	}

	public class ProcessMessageRequest : ServiceRequest
	{

	}
}
