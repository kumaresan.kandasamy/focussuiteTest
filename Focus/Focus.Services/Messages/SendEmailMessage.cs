﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Net.Mail;
using System.Runtime.Serialization;

#endregion

namespace Focus.Services.Messages
{
	public class SendEmailMessage : EventMessage
	{
		[DataMember(Name = "MailMessage", Order = 10)]
		public MailMessage MailMessage { get; set; }

		[DataMember(Name = "To", Order = 11)]
		public string To { get; set; }

		[DataMember(Name = "Cc", Order = 12)]
		public string Cc { get; set; }

		[DataMember(Name = "Bcc", Order = 13)]
		public string Bcc { get; set; }

		[DataMember(Name = "Subject", Order = 14)]
		public string Subject { get; set; }

		[DataMember(Name = "Body", Order = 15)]
		public string Body { get; set; }

		[DataMember(Name = "SendAsync", Order = 16)]
		public bool SendAsync { get; set; }

		[DataMember(Name = "IsBodyHtml", Order = 17)]
		public bool IsBodyHtml { get; set; }

    [DataMember(Name = "Attachment", Order = 18)]
    public byte[] Attachment { get; set; }

    [DataMember(Name = "AttachmentName", Order = 19)]
    public string AttachmentName { get; set; }

		[DataMember(Name = "From", Order = 20)]
		public string From { get; set; }
	}
}
