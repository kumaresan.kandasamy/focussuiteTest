﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using System.Transactions;
using Focus.Core;
using Focus.Core.Models.Upload;
using Focus.Core.Settings.Interfaces;
using Focus.Data.Report.Entities;
using Focus.Services.Helpers;
using Focus.Services.Mappers.Upload;
using Focus.Services.Messages;
using Focus.Services.Messages.Handlers;
using Focus.Services.Repositories.LiveJobs;
using Framework.Messaging;
using Mindscape.LightSpeed;

using Framework.Core;
using Framework.Email;
using Framework.Email.Providers;
using Framework.Lightspeed;
using Framework.Logging;
using Framework.Logging.Providers;
using Framework.Caching;
using Framework.Caching.Providers;
using Framework.Runtime;
using Framework.ServiceLocation;

using Focus.Data.Configuration.Entities;
using Focus.Data.Core.Entities;
using Focus.Data.Migration.Entities;
using Focus.Data.Library.Entities;
using Focus.Data.Repositories;
using Focus.Data.Repositories.Contracts;
using Focus.Services.Core;
using Focus.Services.Integration;
using Focus.Services.Repositories;
using Focus.Services.Repositories.Lens;
using Focus.Services.Repositories.Document;

#endregion

namespace Focus.Services
{
	/// <summary>
	/// Represents the runtime environment for a <b>Focus Suite</b> applications.
	/// </summary>
	public class ServicesRuntime<TServiceLocator> : IRuntime where TServiceLocator : class, IServiceLocator
	{
		private readonly ServiceRuntimeEnvironment _environment;

		internal const string ConfigurationLightspeedContextName = "Configuration";
		internal const string LibraryLightspeedContextName = "Library";
		internal const string DataCoreLightspeedContextName = "DataCore";
		internal const string ReportLightspeedContextName = "Report";
		internal const string DataMigrationLightspeedContextName = "DataMigration";

		/// <summary>
		/// Initializes a new instance of the <see cref="ServicesRuntime{TServiceLocator}" /> class.
		/// </summary>
		/// <param name="environment">The environment.</param>
		/// <param name="serviceLocator">The service locator.</param>
		public ServicesRuntime(ServiceRuntimeEnvironment environment, TServiceLocator serviceLocator)
		{
			_environment = environment;

			serviceLocator.Register(() => serviceLocator);
			Framework.ServiceLocation.ServiceLocator.SetServiceLocator(serviceLocator);

			RegisterServices(serviceLocator);
			ConfigureRuntime(serviceLocator);
			ConfigureMessaging(serviceLocator);
			ConfigureUploads(serviceLocator);
		}

		/// <summary>
		/// Gets the service locator associated with the runtime.
		/// </summary>
		public IServiceLocator ServiceLocator
		{
			get { return Framework.ServiceLocation.ServiceLocator.Current; }
		}

		/// <summary>
		/// Starts the runtime environment.
		/// </summary>
		/// <exception cref="System.NotImplementedException"></exception>
		public void Start()
		{
			OnStarted(ServiceLocator);
		}

		/// <summary>
		/// Shutdowns the runtime environment and release all the resouces held by the runtime environment.
		/// </summary>
		/// <exception cref="System.NotImplementedException"></exception>
		public void Shutdown()
		{
			// Finalise the Emailer, Profiler and Logger
			Emailer.Instance.Detach();
			Profiler.Instance.Detach();
			Logger.Instance.Detach();

			OnShutdown(ServiceLocator);
			ServiceLocator.Dispose();
		}

		/// <summary>
		/// Begins the request.
		/// </summary>
		/// <exception cref="System.NotImplementedException"></exception>
		public void BeginRequest()
		{ }

		/// <summary>
		/// Ends the request.
		/// </summary>
		public void EndRequest()
		{
			ServiceLocator.FreeLightspeedUnitOfWorkScope<ConfigurationUnitOfWork>();
			ServiceLocator.FreeLightspeedUnitOfWorkScope<LibraryUnitOfWork>();
			ServiceLocator.FreeLightspeedUnitOfWorkScope<DataCoreUnitOfWork>();
			ServiceLocator.FreeLightspeedUnitOfWorkScope<ReportUnitOfWork>();
			ServiceLocator.FreeLightspeedUnitOfWorkScope<DataMigrationUnitOfWork>();
		}

		/// <summary>
		/// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
		/// </summary>
		public void Dispose()
		{
			Shutdown();
		}

		/// <summary>
		/// Gets the app settings.
		/// </summary>
		public static IAppSettings AppSettings { get; private set; }

		/// <summary>
		/// Called after the runtime has been started.
		/// </summary>
		/// <param name="serviceLocator">The current <see cref="IServiceLocator"/>.</param>
		protected virtual void OnStarted(IServiceLocator serviceLocator)
		{ }

		/// <summary>
		/// Called when the runtime is to shutdown.
		/// </summary>
		/// <param name="serviceLocator">The current <see cref="IServiceLocator"/>.</param>
		protected virtual void OnShutdown(IServiceLocator serviceLocator)
		{ }

		/// <summary>
		/// Registers the services.
		/// </summary>
		/// <param name="serviceLocator">The service locator.</param>
		private void RegisterServices(IServiceLocator serviceLocator)
		{
			// Establish what Lightspeed UnitOfWork scope to use
			LightspeedUnitOfWorkScope lightspeedUnitOfWorkScope;

			switch (_environment)
			{
				case ServiceRuntimeEnvironment.Web:
					lightspeedUnitOfWorkScope = LightspeedUnitOfWorkScope.Web;
					break;

				case ServiceRuntimeEnvironment.Wcf:
					lightspeedUnitOfWorkScope = LightspeedUnitOfWorkScope.Wcf;
					break;

				default:
					lightspeedUnitOfWorkScope = LightspeedUnitOfWorkScope.Simple;
					break;
			}

			// Add the Lightspeed Contexts
			serviceLocator.AddLightspeedContextFor<ConfigurationUnitOfWork>(lightspeedUnitOfWorkScope, ConfigurationLightspeedContextName);
			serviceLocator.AddLightspeedContextFor<LibraryUnitOfWork>(lightspeedUnitOfWorkScope, LibraryLightspeedContextName);
			serviceLocator.AddLightspeedContextFor<DataCoreUnitOfWork>(lightspeedUnitOfWorkScope, DataCoreLightspeedContextName);
			serviceLocator.AddLightspeedContextFor<ReportUnitOfWork>(lightspeedUnitOfWorkScope, ReportLightspeedContextName);
			serviceLocator.AddLightspeedContextFor<DataMigrationUnitOfWork>(lightspeedUnitOfWorkScope, DataMigrationLightspeedContextName);

			// Add the Repositories
			serviceLocator.Register<IConfigurationRepository>(() => new ConfigurationRepository(serviceLocator.Resolve<UnitOfWorkScopeBase<ConfigurationUnitOfWork>>()));
			serviceLocator.Register<ILibraryRepository>(() => new LibraryRepository(serviceLocator.Resolve<UnitOfWorkScopeBase<LibraryUnitOfWork>>()));
			serviceLocator.Register<ICoreRepository>(() => new CoreRepository(serviceLocator.Resolve<UnitOfWorkScopeBase<DataCoreUnitOfWork>>()));
			serviceLocator.Register<ILensRepository>(() => new LensRepository(serviceLocator.Resolve<IRuntimeContext>()));
			serviceLocator.Register<IDocumentRepository>(() => new DocumentRepository((serviceLocator.Resolve<IConfigurationHelper>()).UncachedAppSettings.DocumentStoreServiceUrl));
			serviceLocator.Register<IReportRepository>(() => new ReportRepository(serviceLocator.Resolve<UnitOfWorkScopeBase<ReportUnitOfWork>>()));
			serviceLocator.Register<IMigrationRepository>(() => new MigrationRepository(serviceLocator.Resolve<UnitOfWorkScopeBase<DataMigrationUnitOfWork>>()));
			serviceLocator.Register<ILiveJobsRepository>(() => new LiveJobsRepository(serviceLocator.Resolve<IRuntimeContext>()));
			serviceLocator.Register<IIntegrationRepository>(() => new IntegrationRepository(serviceLocator.Resolve<IRuntimeContext>()));
			serviceLocator.Register<ILaborInsightRepository>(() => new LaborInsightRepository(serviceLocator.Resolve<IRuntimeContext>()));

			// Add the Helpers
			serviceLocator.Register<IConfigurationHelper>(() => new ConfigurationHelper(serviceLocator.Resolve<IRuntimeContext>()));
			serviceLocator.Register<ILookupHelper>(() => new LookupHelper(serviceLocator.Resolve<IRuntimeContext>()));
			serviceLocator.Register<IOccupationHelper>(() => new OccupationHelper(serviceLocator.Resolve<IRuntimeContext>()));
			serviceLocator.Register<IOrganizationHelper>(() => new OrganizationHelper(serviceLocator.Resolve<IRuntimeContext>()));
			serviceLocator.Register<IResumeHelper>(() => new ResumeHelper(serviceLocator.Resolve<IRuntimeContext>()));
			serviceLocator.Register<ISessionHelper>(() => new SessionHelper(serviceLocator.Resolve<IRuntimeContext>()));
			serviceLocator.Register<IMessagingHelper>(() => new MessagingHelper(serviceLocator.Resolve<IRuntimeContext>()));
			serviceLocator.Register<IPostingHelper>(() => new PostingHelper(serviceLocator.Resolve<IRuntimeContext>()));
			serviceLocator.Register<IJobDevelopmentHelper>(() => new JobDevelopmentHelper(serviceLocator.Resolve<IRuntimeContext>()));
			serviceLocator.Register<ILoggingHelper>(() => new LoggingHelper(serviceLocator.Resolve<IRuntimeContext>()));
			serviceLocator.Register<ILocalisationHelper>(() => new LocalisationHelper(serviceLocator.Resolve<IRuntimeContext>()));
			serviceLocator.Register<IAlertsHelper>(() => new AlertsHelper(serviceLocator.Resolve<IRuntimeContext>()));
			serviceLocator.Register<IEmailHelper>(() => new EmailHelper(serviceLocator.Resolve<IRuntimeContext>()));
			serviceLocator.Register<ISearchHelper>(() => new SearchHelper(serviceLocator.Resolve<IRuntimeContext>()));
			serviceLocator.Register<IOfficeHelper>(() => new OfficeHelper(serviceLocator.Resolve<IRuntimeContext>()));
			serviceLocator.Register<ICandidateHelper>(() => new CandidateHelper(serviceLocator.Resolve<IRuntimeContext>()));
			serviceLocator.Register<IEmployerHelper>(() => new EmployerHelper(serviceLocator.Resolve<IRuntimeContext>()));
			serviceLocator.Register<IEmployeeHelper>(() => new EmployeeHelper(serviceLocator.Resolve<IRuntimeContext>()));
			serviceLocator.Register<ISelfServiceHelper>(() => new SelfServiceHelper(serviceLocator.Resolve<IRuntimeContext>()));
			serviceLocator.Register<IValidationHelper>(() => new ValidationHelper(serviceLocator.Resolve<IRuntimeContext>()));
			serviceLocator.Register<IEncryptionHelper>(() => new EncryptionHelper(serviceLocator.Resolve<IRuntimeContext>()));
			serviceLocator.Register<IPushNotificationHelper>(() => new PushNotificationHelper(serviceLocator.Resolve<IRuntimeContext>()));
			serviceLocator.Register<IUserHelper>(() => new UserHelper(serviceLocator.Resolve<IRuntimeContext>()));

			// Add the IAppSettings, IRepositories and finally the IGlobals
			serviceLocator.Register(() => AppSettings);
			serviceLocator.Register<IRepositories>(() => new ServiceRepositories());
			serviceLocator.Register<IHelpers>(() => new ServiceHelpers());
			serviceLocator.Register<IRuntimeContext>(() => new RuntimeContext(_environment));
		}

		/// <summary>
		/// Configures the runtime.
		/// </summary>
		/// <param name="serviceLocator">The service locator.</param>
		private static void ConfigureRuntime(IServiceLocator serviceLocator)
		{
			var settings = (serviceLocator.Resolve<IConfigurationHelper>()).UncachedAppSettings;
			if (settings.CacheMethod == CachingMethod.AppFabric)
				Cacher.Initialize(new CacheToAppFabric(settings.AppFabricLeadHosts, settings.AppFabricCacheName));
			else
				Cacher.Initialize(new CacheToHttpRuntime(settings.CachePrefix));

			(serviceLocator.Resolve<IConfigurationHelper>()).RefreshAppSettings();
			AppSettings = (serviceLocator.Resolve<IConfigurationHelper>()).AppSettings;

			// Initialise the Logger
			// Read and assign application wide logging severity
			Logger.Severity = AppSettings.LogSeverity;

			// Send log / profile events to a database. 
			var application = AppSettings.Application;
			var errorEmailTo = AppSettings.OnErrorEmail;

			var logToDatabase = new LogToDatabase(application, "Log");

			// Attach LogToEmail first so we get an email if the Db is down!
			// If LogToDatabase is first the event handler will bomb out if the 
			// Logging / Profiling Db can't be found so LogToEmail is first
			if (errorEmailTo.IsNotNullOrEmpty()) Logger.Instance.Attach(new LogToEmail(application, errorEmailTo));
			Logger.Instance.Attach(logToDatabase);

			// Initialise the Profiler
			if (AppSettings.ProfilingEnabled)
				Profiler.Instance.Attach(logToDatabase);

			// Initialise the Emailer
			if (AppSettings.EmailsEnabled)
				Emailer.Instance.Attach(new EmailToSmtp(AppSettings.EmailRedirect));
			else
				Emailer.Instance.Attach(new EmailToFile(AppSettings.EmailLogFileLocation));
		}

		private static void ConfigureMessaging(IServiceLocator serviceLocator)
		{
			// Register the Message Store for the message bus
			serviceLocator.Register<IMessageStore>(() => new MessageStore());

			// Register Message Handlers
			serviceLocator.Register<IMessageHandler<RegisterPostingMessage>>(() => new RegisterPostingMessageHandler());
			serviceLocator.Register<IMessageHandler<UnregisterPostingMessage>>(() => new UnregisterPostingMessageHandler());
			serviceLocator.Register<IMessageHandler<RegisterResumeMessage>>(() => new RegisterResumeMessageHandler());
			serviceLocator.Register<IMessageHandler<UnregisterResumeMessage>>(() => new UnregisterResumeMessageHandler());
			serviceLocator.Register<IMessageHandler<ApplicantHiredMessage>>(() => new ApplicantHiredMessageHandler());
			serviceLocator.Register<IMessageHandler<SendEmailMessage>>(() => new SendEmailMessageHandler());
            serviceLocator.Register<IMessageHandler<SendBlastEmailMessages>>(() => new SendBlastEmailMessagesHandler());

			serviceLocator.Register<IMessageHandler<ProcessAboutToExpireJobsMessage>>(() => new ProcessAboutToExpireJobsMessageHandler());
			serviceLocator.Register<IMessageHandler<ProcessExpiredJobsMessage>>(() => new ProcessExpiredJobsMessageHandler());
			serviceLocator.Register<IMessageHandler<ProcessSearchAlertsMessage>>(() => new ProcessSearchAlertsMessageHandler());
			serviceLocator.Register<IMessageHandler<ProcessRemindersMessage>>(() => new ProcessRemindersMessageHandler());
			serviceLocator.Register<IMessageHandler<ProcessCandidateIssuesMessage>>(() => new ProcessCandidateIssuesMessageHandler());
			serviceLocator.Register<IMessageHandler<ProcessEmailAlertsMessage>>(() => new ProcessEmailAlertsMessageHandler());
			serviceLocator.Register<IMessageHandler<ProcessRegistrationPinEmailMessage>>(() => new ProcessRegistrationPinEmailMessageHandler());
			serviceLocator.Register<IMessageHandler<ProcessRegistrationPinEmailChildMessage>>(() => new ProcessRegistrationPinEmailChildMessageHandler());
			serviceLocator.Register<IMessageHandler<UpdateReportingMessage>>(() => new UpdateReportingMessageHandler());
			serviceLocator.Register<IMessageHandler<ProcessJobIssuesMessage>>(() => new ProcessJobIssuesMessageHandler());
			serviceLocator.Register<IMessageHandler<ProcessJobSeekerReportAgesMessage>>(() => new ProcessJobSeekerReportAgesMessageHandler());
			serviceLocator.Register<IMessageHandler<IntegrationRequestMessage>>(() => new IntegrationRequestMessageHandler());
			serviceLocator.Register<IMessageHandler<SendEmailFromTemplateMessage>>(() => new SendEmailFromTemplateMessageHandler());
			serviceLocator.Register<IMessageHandler<InviteJobseekersToApplyMessage>>(() => new InviteJobseekersToApplyHandler());
			serviceLocator.Register<IMessageHandler<InviteJobseekerToApplyMessage>>(() => new InviteJobseekerToApplyHandler());
			serviceLocator.Register<IMessageHandler<DenyReferralRequestMessage>>(() => new DenyReferralRequestMessageHandler());
			serviceLocator.Register<IMessageHandler<CloseExpiredJobMessage>>(() => new CloseExpiredJobMessageHandler());
			serviceLocator.Register<IMessageHandler<HoldReferralMessage>>(() => new HoldReferralMessageHandler());
			serviceLocator.Register<IMessageHandler<ProcessSearchAlertMessage>>(() => new ProcessSearchAlertMessageHandler());

			serviceLocator.Register<IMessageHandler<SaveAndPostJobMessage>>(() => new SaveAndPostJobHandler());
			serviceLocator.Register<IMessageHandler<UploadFileMessage>>(() => new UploadFileMessageHandler());
			serviceLocator.Register<IMessageHandler<IntegrationImportMessage>>(() => new IntegrationImportMessageHandler());
			serviceLocator.Register<IMessageHandler<ProcessStatisticsMessage>>(() => new ProcessStatisticsMessageHandler());
			serviceLocator.Register<IMessageHandler<ProcessSessionMaintenanceMessage>>(() => new ProcessSessionMaintenanceMessageHandler());
			serviceLocator.Register<IMessageHandler<GenerateReportMessage>>(() => new GenerateReportHandler());
			serviceLocator.Register<IMessageHandler<ResumeProcessMessage>>(() => new ResumeProcessMessageHandler());
			serviceLocator.Register<IMessageHandler<ProcessMatchesToRecentPlacementsMessage>>(() => new ProcessMatchesToRecentPlacementsMessageHandler());
			serviceLocator.Register<IMessageHandler<ProcessPostingMessage>>(() => new ProcessPostingMessageHandler());
      serviceLocator.Register<IMessageHandler<ProcessQueuedInvitesAndRecommendationsMessage>>(() => new ProcessQueuedInvitesAndRecommendations());
		}

		/// <summary>
		/// Configures mappers to work with upload records
		/// </summary>
		/// <param name="serviceLocator"></param>
		private static void ConfigureUploads(IServiceLocator serviceLocator)
		{
			serviceLocator.Register<IUploadMapper<JobUploadRecord>>(() => new JobUploadMapper());
		}
	}

	public enum ServiceRuntimeEnvironment
	{
		DllOrExe,
		Web,
		Wcf
	}
}
