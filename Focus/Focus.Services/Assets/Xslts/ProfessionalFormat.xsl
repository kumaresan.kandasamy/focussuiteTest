<!-- 
    ************************************************************
    Copyright(c) 2010, Burning Glass Technologies
    XSL for Professional format for Focus
    ************************************************************
    Version 1.2
    
    Change Log: 
	  03/SEP/2010 RIA Created the XSL
      07/SEP/2010 KNK Updated the XSL for the formatting as specified in the requirements
	  18/SEP/2010 RRC Included comma between employer and location.
	  13/Oct/2010 RRC Removed XXXX from date.
	  30/Oct/2010 RRC Right Align start date and end date with Job title in experience section
	  01/Nov/2010 AKV updated the xsl to format download PDF & RTF resume.
	  11/Nov/2010 RRC Sorted work experience in reverse chronological order
	  09/DEC/2010 AKV <NewLine>*<Space> is replaced with <Space> in Job Descriptions
	  21/DEC/2010 RRC Updated objective,skills & reference tag due to changes in xpath of new lens version.
	  27/DEC/2010 RRC Justifies bodies of the text. 
	  05/JAN/2011 AKV Job sorting by pos id has been removed, now we are getting sorted jobs from WS
    31/AUG/2011 RRC do not display employer, if the emploer is "Self" in experience.
    02/SEP/2011 RRC Added veteran discharge in military services & courses, honors, gpa, activities in education. 
	14/SEP/2011 AKV Dynamic font size change.
	16/SEP/2011 AKV Default font values are set for fontsize13, fontsize14 and fontsize16 parameters.
	16/SEP/2011 AKV Experience Date issue resolved if it is not in proper format 'to' or '-' appears. 
	20/SEP/2011 AKV Address2 is added in address section.
	28/SEP/2011 AKV Extra ',' issue in Experience Address issue is resolved.
	18/OCT/2012 SAIN Bug Fix: Configured state to county for UK build
	21/NOV/2012 RRC Added website in contact details
	21/NOV/2012 SAIN Bug Fix: Experience section display varies in RTF
	21/NOV/2012 SAIN Bug Fix: Experience section display varies in Career UI & RTF
	02/JAN/2013 SAV Rearranged the sections according to the new resume template.
	02/JAN/2013 SAV Removed Professional Summary and Apprenticeship section.
	02/JAN/2013 SAV Added emphasizeinfo parameter for dynamic section change.
-->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema">
	<xsl:output method="html" version="1.0" omit-xml-declaration="no" indent="yes" encoding="iso-8859-1"/>
	<xsl:variable name="lowercase" select="'abcdefghijklmnopqrstuvwxyz'" />
	<xsl:variable name="smallcase" select="'abcdefghijklmnopqrstuvwxyz'" />
	<xsl:variable name="uppercase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZ'" />
	<xsl:param name="fontsize13" select="'13px'"/>
	<xsl:param name="fontsize14" select="'14px'"/>
	<xsl:param name="fontsize16" select="'16px'"/>
	<xsl:param name="emphasizeinfo"/>
	<xsl:param name="layoutWidth" select="'100%'"/>
	<xsl:param name="crlf" select="'y'"/>
	<xsl:template match="/">
		<style type="text/css">
			.font13 { font-family:arial;font-size:<xsl:value-of select="$fontsize13"/>; }
			.font14 { font-family:arial;font-size:<xsl:value-of select="$fontsize14"/>; }
			.font16 { font-family:arial;font-size:<xsl:value-of select="$fontsize16"/>; font-weight:bold; }
			.alignCenter{text-align: center; }
		</style>
		<html>
			<body>
				<font class="font13">
					<!--CONTACT INFORMATION-->
					<!--<div class="container">-->
					<table border="0" width="{$layoutWidth}">
						<tr>
							<td>
								<xsl:if test="count(//contact)>0">
									<div class="alignCenter">
										<xsl:if test="count(//contact/name)>0">
											<font class="font16">
												<xsl:value-of select="(//givenname)[1]"/>
												<xsl:text>   </xsl:text>
												<xsl:value-of select="(//givenname)[2]"/>
												<xsl:text>   </xsl:text>
												<xsl:value-of select="(//surname)[1]"/>
											</font>
											<br/>
										</xsl:if>
										<font class="font13">
											<xsl:value-of select="(//resume/contact/address/street)[1]"/>
											<xsl:if test="string-length((//resume/contact/address)[1]/street[2])>0">
												<xsl:text>, </xsl:text>
												<xsl:value-of select="(//resume/contact/address)[1]/street[2]"/>
											</xsl:if>
											<xsl:if test="count((//resume/contact/address/city)[1]) > 0">
												<xsl:text>, </xsl:text>
												<xsl:value-of select="(//resume/contact/address/city)[1]"/>
											</xsl:if>
											<xsl:if test="count((//resume/contact/address/town)[1]) > 0">
												<xsl:text>, </xsl:text>
												<xsl:value-of select="(//resume/contact/address/town)[1]"/>
											</xsl:if>

											<xsl:if test="count((//resume/contact/address/state)[1]) > 0">
												<xsl:if test="count((//resume/contact/address)[1]/city[1])>0">
													<xsl:text>, </xsl:text>
												</xsl:if>
												<xsl:if test="(//resume/contact/address)[1]/state[1] != 'ZZ'">
													<xsl:value-of select="(//resume/contact/address)[1]/state[1]"/>
												</xsl:if>
											</xsl:if>
											<xsl:if test="count((//resume/contact/address)[1]/county[1]) > 0 and count((//resume/contact/address)[1]/state[1]) = 0">
												<xsl:if test="count((//resume/contact/address)[1]/town[1])>0">
													<xsl:text>, </xsl:text>
												</xsl:if>
												<xsl:if test="(//resume/contact/address)[1]/county[1] != 'ZZ'">
													<xsl:value-of select="(//resume/contact/address)[1]/county[1]"/>
												</xsl:if>
											</xsl:if>

											<xsl:if test="count((//resume/contact/address/postalcode)[1]) > 0">
												<xsl:text> </xsl:text>
												<xsl:value-of select="(//resume/contact/address/postalcode)[1]"/>
											</xsl:if>

											<xsl:if test="string-length((//phone))>0">
												<br/>
												<xsl:apply-templates select="(//resume/contact/phone)"/>
											</xsl:if>
											<xsl:if test="count((//email)[1])>0">
												<!--<br/>-->
												<xsl:apply-templates select="(//resume/contact/email)[1]"/>
											</xsl:if>
											<xsl:if test="count((//resume/contact/website)[1]) > 0">
												<br/>
												<xsl:value-of select="(//resume/contact/website)[1]" />
											</xsl:if>
											<br/>
										</font>
										<hr style="width:100%"/>
										<!--<table>
                      <tr>
                        <td></td>
                      </tr>
                    </table>-->
									</div>
								</xsl:if>
							</td>
						</tr>
						<tr>
							<td>
								<!-- BRANDING (top)-->
								<xsl:apply-templates select="//special/branding" mode="addins">
									<xsl:with-param name="summaryName" select="'Branding statement'"/>
								</xsl:apply-templates>

								<!-- OBJECTIVE (top)-->
								<xsl:apply-templates select="//summary/objective" mode="addins">
									<xsl:with-param name="summaryName" select="'Objective'"/>
								</xsl:apply-templates>

								<!-- SUMMARY (top of resume)-->
								<xsl:apply-templates select="//summary/summary" mode="addins">
									<xsl:with-param name="summaryName" select="'Summary'"/>
								</xsl:apply-templates>

								<!--Middle Sections-->
								<xsl:choose>
									<xsl:when test='$emphasizeinfo="skills"'>
										<xsl:call-template name='skillssection'/>
										<xsl:call-template name='educationsection'/>
										<xsl:call-template name='experiencesection'/>
									</xsl:when>

									<xsl:when test ='$emphasizeinfo="education"'>
										<xsl:call-template name='educationsection'/>
										<xsl:call-template name='skillssection'/>
										<xsl:call-template name='experiencesection'/>
									</xsl:when>

									<xsl:otherwise>
										<xsl:call-template name='experiencesection'/>
										<xsl:call-template name='skillssection'/>
										<xsl:call-template name='educationsection'/>
									</xsl:otherwise>
								</xsl:choose>

								<!--Bottom Sections-->
								<!-- Publications -->
								<xsl:if test="count(//professional/publications)>0">
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr>
											<td width="30%" valign="top">
												<font class="font13">
													<b>Publications</b>
												</font>
											</td>
											<td width="50%">
												<font class="font13">
													<div style="text-align: justify;">
														<xsl:value-of select="//professional/publications"/>
													</div>
												</font>
											</td>
											<td width="20%"></td>
										</tr>
										<tr>
											<td colspan="3" height="13pt">
											</td>
										</tr>
									</table>
								</xsl:if>

								<!-- Affiliations (bottom)-->
								<xsl:if test="count(//professional/affiliations)>0">
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr>
											<td width="30%" valign="top">
												<font class="font13">
													<b>Affiliations</b>
												</font>
											</td>
											<td width="50%">
												<font class="font13">
													<div style="text-align: justify;">
														<xsl:for-each select="//professional/affiliations">
															<xsl:value-of select="."/>
															<br/>
														</xsl:for-each>
													</div>
												</font>
											</td>
											<td width="20%"></td>
										</tr>
										<tr>
											<td colspan="3" height="13pt">
											</td>
										</tr>
									</table>
								</xsl:if>
								
								<!-- Honors -->
								<xsl:if test="count(//special/honors)>0">
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr>
											<td width="30%" valign="top">
												<font class="font13">
													<b>Honors</b>
												</font>
											</td>
											<td width="50%">
												<font class="font13">
													<div style="text-align: justify;">
														<xsl:value-of select="//special/honors"/>
													</div>
												</font>
											</td>
											<td width="20%"></td>
										</tr>
										<tr>
											<td colspan="3" height="13pt">
											</td>
										</tr>
									</table>
								</xsl:if>
								
								<!-- Volunteer Activities -->
								<xsl:if test="count(//special/volunteeractivities)>0">
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr>
											<td width="30%" valign="top">
												<font class="font13">
													<b>Volunteer Activities</b>
												</font>
											</td>
											<td width="50%">
												<font class="font13">
													<div style="text-align: justify;">
														<xsl:value-of select="//special/volunteeractivities"/>
													</div>
												</font>
											</td>
											<td width="20%"></td>
										</tr>
										<tr>
											<td colspan="3" height="13pt">
											</td>
										</tr>
									</table>
								</xsl:if>

								<!-- Interests -->
								<xsl:if test="count(//special/interests)>0">
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr>
											<td width="30%" valign="top">
												<font class="font13">
													<b>Interests</b>
												</font>
											</td>
											<td width="50%">
												<font class="font13">
													<div style="text-align: justify;">
														<xsl:value-of select="//special/interests"/>
													</div>
												</font>
											</td>
											<td width="20%"></td>
										</tr>
										<tr>
											<td colspan="3" height="13pt">
											</td>
										</tr>
									</table>

								</xsl:if>


								<!-- Personal (bottom)-->
								<xsl:if test="count(//special/personal)>0">
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr>
											<td width="30%" valign="top">
												<font class="font13">
													<b>Personal</b>
												</font>
											</td>
											<td width="50%">
												<font class="font13">
													<div style="text-align: justify;">
														<xsl:value-of select="//special/personal"/>
													</div>
												</font>
											</td>
											<td width="20%"></td>
										</tr>
										<tr>
											<td colspan="3" height="13pt">
											</td>
										</tr>
									</table>

								</xsl:if>

								<!-- Thesis/Major Projects -->
								<xsl:if test="count(//thesismajorprojects)>0">
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr>
											<td width="30%" valign="top">
												<font class="font13">
													<b>Thesis/Major Projects</b>
												</font>
											</td>
											<td width="50%">
												<font class="font13">
													<div style="text-align: justify;">
														<xsl:value-of select="//thesismajorprojects"/>
													</div>
												</font>
											</td>
											<td width="20%"></td>
										</tr>
										<tr>
											<td colspan="3" height="13pt">
											</td>
										</tr>
									</table>
								</xsl:if>

								<!-- References -->
								<xsl:if test="count(//references)>0">
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr>
											<td width="30%" valign="top">
												<font class="font13">
													<b>References</b>
												</font>
											</td>
											<td width="50%">
												<font class="font13">
													<div style="text-align: justify;">
														<xsl:value-of select="//references"/>
													</div>
												</font>
											</td>
											<td width="20%"></td>
										</tr>
										<tr>
											<td colspan="3" height="13pt">
											</td>
										</tr>
									</table>

								</xsl:if>

							</td>
						</tr>
					</table>
					<!--</div>-->
				</font>
				<br/>
			</body>
		</html>
	</xsl:template >
	<xsl:template name="skillssection">
		<!-- Skills -->
		<xsl:if test="string-length(//skills/skills)>0">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td width="30%" valign="top">
						<font class="font13">
							<b>Skills</b>
						</font>
					</td>
					<td width="50%">
						<font class="font13">
							<div style="text-align: justify;">
								<xsl:for-each select="//skills/skills">
									<xsl:value-of select="."/>
									<br/>
								</xsl:for-each>
							</div>
						</font>
					</td>
					<td width="20%"></td>
				</tr>
				<tr>
					<td style="height: 13px;">
					</td>
				</tr>
			</table>

		</xsl:if>

		<!-- Technical Skills (top)-->
		<xsl:if test="count(//special/technicalskillstop)>0">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td width="30%" valign="top">
						<font class="font13">
							<b>Technical Skills</b>
						</font>
					</td>
					<td width="50%">
						<font class="font13">
							<div style="text-align: justify;">
								<xsl:value-of select="//special/technicalskillstop"/>
							</div>
						</font>
					</td>
					<td width="20%"></td>
				</tr>
			</table>
		</xsl:if>

		<!-- Technical Skills -->
		<xsl:if test="count(//special/technicalskills)>0">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td width="30%" valign="top">
						<font class="font13">
							<b>Technical Skills</b>
						</font>
					</td>
					<td width="50%">
						<font class="font13">
							<div style="text-align: justify;">
								<xsl:value-of select="//special/technicalskills"/>
							</div>
						</font>
					</td>
					<td width="20%"></td>
				</tr>
				<tr>
					<td style="height: 13px;">
					</td>
				</tr>
			</table>
		</xsl:if>

		<!-- Technical Skills (bottom)-->
		<xsl:if test="count(//special/technicalskillsbottom)>0">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td width="30%" valign="top">
						<font class="font13">
							<b>Technical Skills</b>
						</font>
					</td>
					<td width="50%">
						<font class="font13">
							<div style="text-align: justify;">
								<xsl:value-of select="//special/technicalskillsbottom"/>
							</div>
						</font>
					</td>
					<td width="20%"></td>
				</tr>
				<tr>
					<td colspan="3">
						<br/>
					</td>
				</tr>
			</table>
		</xsl:if>

		<!-- Languages (bottom)-->
		<xsl:apply-templates select="//skills/languages[languages_profiency or language]" />

		<!-- Certifications (bottom)-->
		<!--<xsl:if test="count(//certification)>0"> -->
		<xsl:if test="string-length(//certification)>0">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td width="30%" valign="top">
						<font class="font13">
							<b>Certifications And Professional Licenses</b>
						</font>
					</td>
					<td width="50%" style="text-align: left; vertical-align: top;">
						<font class="font13">
								<xsl:for-each select="//certification">
									<xsl:sort select="@completion_juliandate"/>
									Certificate:
									<xsl:value-of select="certificate"/>,
									<xsl:value-of select="organization_name"/>,
									<xsl:if test="count(//certification/completion_date)>0">
										<xsl:apply-templates select="completion_date"/>;
									</xsl:if>
									<xsl:if test="count(//address/state_fullname)>0">
										<xsl:value-of select="address/state_fullname"/>
									</xsl:if>
									<xsl:if test="count(//address/state_fullname)=0">
										<xsl:call-template name="state">
											<xsl:with-param name="code" select="(address/state)"/>
										</xsl:call-template>
									</xsl:if>
									<!--<xsl:value-of select="address/state"/>-->
									<xsl:if test="address/country != 'US'">
										,  <xsl:value-of select="address/country_fullname"/>
									</xsl:if>
									<br/>
								</xsl:for-each>
						</font>
					</td>
					<td width="19%"></td>
				</tr>
				<tr>
					<td style="height: 13px;" colspan="3">
					</td>
				</tr>
			</table>
		</xsl:if>


		<!-- National Career Readiness Certificate Credentials (bottom)-->
		<xsl:if test="(//ncrc_confirmation) = 'true' and (//ncrc_displayed) = 'true'">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td width="30%" valign="top">
						<font class="font13">
							<b>National Career Readiness Certificate&#8482;</b>
						</font>
					</td>
					<td width="50%" style="text-align: left; vertical-align: top;">
						<font class="font13">
								<xsl:value-of select="//ncrc_level_name"/> level attained, ACT,
								<xsl:value-of select="//ncrc_issue_date"/>;
								<xsl:value-of select="//ncrc_state_name"/>
						</font>
					</td>
					<td width="19%"></td>
				</tr>
				<tr>
					<td style="height: 13px;">
					</td>
				</tr>
			</table>
		</xsl:if>

		<!-- Licenses (bottom of resume)-->
		<!-- <xsl:if test="count(//personal/license)>0"> -->
		<xsl:if test="string-length(//personal/license)>0">
			<xsl:if test="(//personal/license/driver_class) &gt; 0">

				<!--<font class="font13">
											<div style="text-align: justify;">-->
				<table border="0" cellpadding="0" cellspacing="0" width="100%">
					<xsl:if test="string-length(//personal/license/driver_class)>0 and string-length(//personal/license/driver_class_text)>0">
						<tr>
							<td width="30%" valign="top">
								<font class="font13">
									<b>Licenses</b>
								</font>
							</td>

							<!--<br/>-->

							<xsl:if test="string-length(//personal/license/driver_state) > 0">
								<td width="50%">
									<font class="font13">
										<div style="text-align: justify;">
											<font class="font13">
												<xsl:call-template name="state">
													<xsl:with-param name="code" select="(//personal/license/driver_state)"/>
												</xsl:call-template>
											</font>
											<font class="font13">
												<xsl:text>, </xsl:text>
												<xsl:value-of select="//personal/license/driver_class_text"/>
											</font>

										</div>
									</font>

								</td>
								<td width="20%"></td>
							</xsl:if>
						</tr>
					</xsl:if>

					<xsl:if test="string-length(//personal/license/driver_class_text)=0 and (//personal/license/driver_class) = 1">
						<tr>
							<td width="30%" valign="top">
								<font class="font13">
									<b>Licenses</b>
								</font>
							</td>

							<!--<br/>-->
							<xsl:if test="string-length(//personal/license/driver_state) > 0">
								<td width="50%">
									<font class="font13">
										<div style="text-align: justify;">
											<font class="font13">
												<xsl:call-template name="state">
													<xsl:with-param name="code" select="(//personal/license/driver_state)"/>
												</xsl:call-template>
											</font>
											<font class="font13">
												, Class A/CDL:
											</font>
										</div>
									</font>

								</td>
								<td width="20%"></td>
							</xsl:if>
						</tr>
					</xsl:if>

					<xsl:if test="string-length(//personal/license/driver_class_text)=0 and (//personal/license/driver_class) = 2">
						<tr>
							<td width="30%" valign="top">
								<font class="font13">
									<b>Licenses</b>
								</font>
							</td>
							<!--<br/>-->
							<xsl:if test="string-length(//personal/license/driver_state) > 0">
								<td width="50%">
									<font class="font13">
										<div style="text-align: justify;">
											<font class="font13">
												<xsl:call-template name="state">
													<xsl:with-param name="code" select="(//personal/license/driver_state)"/>
												</xsl:call-template>
											</font>
											<font class="font13">
												, Class B/CDL:
											</font>
										</div>
									</font>
								</td>
								<td width="20%"></td>
							</xsl:if>
						</tr>
					</xsl:if>

					<xsl:if test="string-length(//personal/license/driver_class_text)=0 and (//personal/license/driver_class) = 3">
						<tr>
							<td width="30%" valign="top">
								<font class="font13">
									<b>Licenses</b>
								</font>
							</td>
							<!--<br/>-->
							<xsl:if test="string-length(//personal/license/driver_state) > 0">
								<td width="50%">
									<font class="font13">
										<div style="text-align: justify;">
											<font class="font13">
												<xsl:call-template name="state">
													<xsl:with-param name="code" select="(//personal/license/driver_state)"/>
												</xsl:call-template>
											</font>
											<font class="font13">
												, Class C/CDL:
											</font>
										</div>
									</font>

								</td>
								<td width="20%"></td>
							</xsl:if>
						</tr>


					</xsl:if>

					<xsl:if test="string-length(//personal/license/driver_class_text)=0 and (//personal/license/driver_class) = 4">
						<xsl:if test="(//personal/license/drv_pass_flag) = -1 or (//personal/license/drv_hazard_flag) = -1 or (//personal/license/drv_tank_flag) = -1 or (//personal/license/drv_cycle_flag) = -1 or (//personal/license/drv_bus_flag) = -1 or (//personal/license/drv_double_flag) = -1 or (//personal/license/drv_tankhazard_flag) = -1 or (//personal/license/drv_airbrake_flag) = -1 ">
							<tr>
								<td width="30%" valign="top">
									<font class="font13">
										<b>Licenses</b>
									</font>
								</td>

								<!--<br/>-->
								<xsl:if test="string-length(//personal/license/driver_state) > 0">
									<td width="50%">
										<font class="font13">
											<div style="text-align: justify;">
												<font class="font13">
													<xsl:call-template name="state">
														<xsl:with-param name="code" select="(//personal/license/driver_state)"/>
													</xsl:call-template>
												</font>
												<font class="font13">
													, Class D/Regular:
												</font>
											</div>
										</font>

									</td>
									<td width="20%"></td>
								</xsl:if>
							</tr>

						</xsl:if>
					</xsl:if>

					<xsl:if test="string-length(//personal/license/driver_class_text)=0 and (//personal/license/driver_class) = 5">
						<xsl:if test="(//personal/license/drv_pass_flag) = -1 or (//personal/license/drv_hazard_flag) = -1 or (//personal/license/drv_tank_flag) = -1 or (//personal/license/drv_cycle_flag) = -1 or (//personal/license/drv_bus_flag) = -1 or (//personal/license/drv_double_flag) = -1 or (//personal/license/drv_tankhazard_flag) = -1 or (//personal/license/drv_airbrake_flag) = -1 ">
							<tr>
								<td width="30%" valign="top">
									<font class="font13">
										<b>Licenses</b>
									</font>
								</td>

								<!--<br/>-->
								<xsl:if test="string-length(//personal/license/driver_state) > 0">
									<td width="50%">
										<font class="font13">
											<div style="text-align: justify;">
												<font class="font13">
													<xsl:call-template name="state">
														<xsl:with-param name="code" select="(//personal/license/driver_state)"/>
													</xsl:call-template>
												</font>
												<font class="font13">
													, Motorcycle:
												</font>
											</div>
										</font>

									</td>
									<td width="20%"></td>
								</xsl:if>
							</tr>

						</xsl:if>
					</xsl:if>


					<tr>
						<td width="30%" valign="top">
						</td>
						<td width="50%">
							<font class="font13">
								<div style="text-align: justify;">
									<xsl:if test="(//personal/license/drv_pass_flag) = -1">
										<font class="font13">Pass Transport</font>
										<xsl:if test="(//personal/license/drv_hazard_flag) = -1 or (//personal/license/drv_tank_flag) = -1 or (//personal/license/drv_cycle_flag) = -1 or (//personal/license/drv_bus_flag) = -1 or (//personal/license/drv_double_flag) = -1 or (//personal/license/drv_tankhazard_flag) = -1 or (//personal/license/drv_airbrake_flag) = -1 or (//personal/license/drv_limo_flag) = -1 ">, </xsl:if>
									</xsl:if>
									<xsl:if test="(//personal/license/drv_hazard_flag) = -1">
										<font class="font13">Hazardous Materials</font>
										<xsl:if test="(//personal/license/drv_tank_flag) = -1 or (//personal/license/drv_cycle_flag) = -1 or (//personal/license/drv_bus_flag) = -1 or (//personal/license/drv_double_flag) = -1 or (//personal/license/drv_tankhazard_flag) = -1 or (//personal/license/drv_airbrake_flag) = -1 or (//personal/license/drv_limo_flag) = -1 ">, </xsl:if>
									</xsl:if>
									<xsl:if test="(//personal/license/drv_tank_flag) = -1">
										<font class="font13">Tank Vehicle</font>
										<xsl:if test="(//personal/license/drv_cycle_flag) = -1 or (//personal/license/drv_bus_flag) = -1 or (//personal/license/drv_double_flag) = -1 or (//personal/license/drv_tankhazard_flag) = -1 or (//personal/license/drv_airbrake_flag) = -1 or (//personal/license/drv_limo_flag) = -1 ">, </xsl:if>
									</xsl:if>
									<xsl:if test="(//personal/license/drv_cycle_flag) = -1">
										<font class="font13">Motorcycle</font>
										<xsl:if test="(//personal/license/drv_bus_flag) = -1 or (//personal/license/drv_double_flag) = -1 or (//personal/license/drv_tankhazard_flag) = -1 or (//personal/license/drv_airbrake_flag) = -1 or (//personal/license/drv_limo_flag) = -1 ">, </xsl:if>
									</xsl:if>
									<xsl:if test="(//personal/license/drv_bus_flag) = -1">
										<font class="font13">School Bus</font>
										<xsl:if test="(//personal/license/drv_double_flag) = -1 or (//personal/license/drv_tankhazard_flag) = -1 or (//personal/license/drv_airbrake_flag) = -1 or (//personal/license/drv_limo_flag) = -1 ">, </xsl:if>
									</xsl:if>
									<xsl:if test="(//personal/license/drv_double_flag) = -1">
										<font class="font13">Doubles/Triples</font>
										<xsl:if test="(//personal/license/drv_tankhazard_flag) = -1 or (//personal/license/drv_airbrake_flag) = -1 or (//personal/license/drv_limo_flag) = -1 ">, </xsl:if>
									</xsl:if>
									<xsl:if test="(//personal/license/drv_tankhazard_flag) = -1">
										<font class="font13">Tank Hazard</font>
										<xsl:if test="(//personal/license/drv_airbrake_flag) = -1 or (//personal/license/drv_limo_flag) = -1 ">, </xsl:if>
									</xsl:if>
									<xsl:if test="(//personal/license/drv_airbrake_flag) = -1">
										<font class="font13">Air Brakes</font>
										<xsl:if test="(//personal/license/drv_limo_flag) = -1 ">, </xsl:if>
									</xsl:if>
									<xsl:if test="(//personal/license/drv_limo_flag) = -1">
										<font class="font13">Limo/Chauffeur</font>
									</xsl:if>
								</div>
							</font>
						</td>
						<td width="20%"></td>
					</tr>
					<tr>
						<td height="13pt"></td>
					</tr>
				</table>
			</xsl:if>
		</xsl:if>

	</xsl:template>
	
	<xsl:template name="educationsection">
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td width="30%" valign="top">
					<!-- Education -->
					<xsl:if test="string-length(//school)>0">
						<font class="font13">
							<b>Education</b>
						</font>
					</xsl:if>
				</td>
				<td width="69%">
					<xsl:for-each select="//education">
						<table border="0" width="100%" cellpadding="0" cellspacing="0">
							<xsl:for-each select="school">
								<tr>
									<td width="71.5%" valign="top" cellpadding="0" cellspacing="0" align="left;">
										<font class="font13">
											<xsl:if test="string-length(institution[1]) > 0 ">
												<xsl:apply-templates select="institution[1]"/>
												<br/>
											</xsl:if>
											<xsl:if test="string-length(completiondate[1]) > 0 ">
												<b>
													<!--<xsl:value-of select="completiondate[1]"/>-->
													<xsl:apply-templates select="./completiondate[1]"/>
												</b>
												<br/>
											</xsl:if>
											<xsl:if test="count(degree)>0">
												<xsl:if test="translate(degree,$lowercase,$uppercase)!='GENERAL' and translate(degree,$lowercase,$uppercase)!='N/A' and translate(degree,$lowercase,$uppercase)!='NA'">
													<xsl:value-of select="degree"/>
													<br/>
												</xsl:if>
											</xsl:if>
											<xsl:if test="count(major)>0">
												<xsl:if test="translate(major,$lowercase,$uppercase)!='GENERAL' and translate(major,$lowercase,$uppercase)!='N/A' and translate(major,$lowercase,$uppercase)!='NA'">
													<xsl:value-of select="major"/>
												</xsl:if>
											</xsl:if>
										</font>
									</td>
									<td width="28.5%"></td>
								</tr>
								<xsl:variable name="schoolNodes" select="(courses|honors|gpa|activities)[string-length() > 0]" />
								<xsl:if test="$schoolNodes">
									<tr>
										<td valign="top" width="71.5%">
											<font class="font13">
												<xsl:for-each select="$schoolNodes">
													<xsl:sort select="string-length(substring-before('courses|honors|gpa|activities', local-name()))" data-type="number" />
													<xsl:if test="position() > 1">, </xsl:if>
													<b>
														<xsl:choose>
															<xsl:when test="self::courses">Course Work Completed : </xsl:when>
															<xsl:when test="self::honors">Honors : </xsl:when>
															<xsl:when test="self::gpa">GPA: </xsl:when>
															<xsl:otherwise>Activities: </xsl:otherwise>
														</xsl:choose>
													</b>
													<xsl:value-of select="." />
												</xsl:for-each>
											</font >
											<br/>
										</td>
										<td width="28.5%"></td>
									</tr>
								</xsl:if>
								<xsl:variable name="expectedcompletion" select="expectedcompletiondate[1]" />
								<xsl:if test="$expectedcompletion">
									<tr>
										<td valign="top" width="71.5%">
											<font class="font13">
												<xsl:apply-templates select="$expectedcompletion"/>
											</font>
											<br/>
										</td>
										<td width="28.5%"></td>
									</tr>
								</xsl:if>
                <xsl:if test="string-length(completiondate[1])=0">
                  <xsl:if test="string-length(expectedcompletiondate[1])=0" >
                    <xsl:text>Unfinished degree or diploma. Not currently enrolled</xsl:text>
                  </xsl:if>
                </xsl:if>
							</xsl:for-each>
						</table>
					</xsl:for-each>
				</td>
			</tr>
			<tr>
				<td style="height: 13px;" colspan="2"></td>
			</tr>
		</table>
		<!-- Professional Development (bottom)-->
		<xsl:if test="count(//professional/description)>0">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td width="30%" valign="top">
						<font class="font13">
							<b>Professional Development</b>
						</font>
					</td>
					<td width="50%">
						<font class="font13">
							<div style="text-align: justify;">
								<xsl:for-each select="//professional/description">
									<xsl:value-of select="."/>
									<br/>
								</xsl:for-each>
							</div>
						</font>
					</td>
					<td width="20%"></td>
				</tr>
				<tr>
					<td colspan="3" height="13pt">
					</td>
				</tr>
			</table>
		</xsl:if>


	</xsl:template>

	<xsl:template name="experiencesection">
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td width="30%" valign="top">
					<!-- Experience -->
					<xsl:if test="count(//job) > 0">
						<font class="font13">
							<b>Experience</b>
						</font>
					</xsl:if>
				</td>
				<td width="70%">
					<table border="0" width="100%" cellpadding="0" cellspacing="0">
						<xsl:for-each select="//experience">
							<xsl:for-each select="job">
								<tr>
									<td width="72.5%" align="justify">
										<font class="font13">
												<xsl:if test="string-length(@pos)=0">
													<xsl:if test ="string-length(title)>0">
														<xsl:value-of select="title"/>
														<!--<br/>-->
													</xsl:if>
													<xsl:if test="count(employer)>0 and string-length(employer) > 0">
														<xsl:text>, </xsl:text>
													</xsl:if>
													<b>
														<xsl:if test="employer != 'Self' and string-length(employer) > 0">
															<xsl:value-of select="employer"/>
															<xsl:if test="string-length(address) > 0 and (count(address/city[1]) > 0 or count((address)[1]/town[1]) > 0)">
																<xsl:if test="string-length(address/city[1]) > 0 or string-length(address[1]/town[1])>0">
																	<xsl:text>, </xsl:text>
																</xsl:if>
															</xsl:if>
														</xsl:if>


														<xsl:if test="count(address/city[1]) > 0">
															<xsl:value-of select="address/city[1]"/>
														</xsl:if>
														<xsl:if test="count((address)[1]/city[1]) = 0 and count((address)[1]/town[1]) > 0">
															<xsl:value-of select="(address)[1]/town[1]"/>
														</xsl:if>
														<xsl:if test="count(address/state[1]) > 0">
															<xsl:if test="string-length(address/state[1]) > 0">
																<xsl:text>, </xsl:text>
																<xsl:if test="address/state[1] != 'ZZ'">
																	<xsl:value-of select="address/state[1]"/>
																</xsl:if>
																<xsl:if test="address/state[1] = 'ZZ'">
																	<xsl:value-of select="address/state_fullname[1]"/>
																</xsl:if>

															</xsl:if>
														</xsl:if>
														<xsl:if test="count((address)[1]/state[1]) = 0 and count((address)[1]/county[1]) > 0">
															<xsl:text>, </xsl:text>
															<xsl:value-of select="(address)[1]/county[1]"/>
														</xsl:if>
														<xsl:if test="(count(./address) > 0 and count(./address/state[1]) = 0 and count(./address/county[1]) = 0  ) and ( count(./address/city[1]) = 0 and count(./address/town[1]) = 0 ) ">
															<xsl:if test="string-length(./address) > 0 and string-length(./address/state[1])>0 and string-length(./address/county[1])>0 and string-length(./address/city[1]) = 0 and string-length(./address/town[1])> 0 ">
																<xsl:text>, </xsl:text>
																<xsl:value-of select="./address[1]"/>
															</xsl:if>
														</xsl:if>

													</b>

													<xsl:if test="count(description)>0">
														<div style="text-align: justify; page-break-inside: avoid;" colspan="2">
															<!--<br/>-->
															<xsl:for-each select="description">
																<xsl:variable name="updatedDescription">
																	<xsl:call-template name="string-replace-FC_CRLF_BULLETS">
																		<xsl:with-param name="text" select="." />
																		<xsl:with-param name="replace" select="'FC_CRLF* '" />
																		<xsl:with-param name="by" select="' '" />
																	</xsl:call-template>
																</xsl:variable>
																<xsl:call-template name="string-replace-BULLETS">
																	<xsl:with-param name="text" select="$updatedDescription" />
																	<xsl:with-param name="replace" select="'* '" />
																	<xsl:with-param name="by" select="''" />
																</xsl:call-template>
															</xsl:for-each>
														</div>
													</xsl:if>
												</xsl:if>
										</font>
									</td>

									<td width="27.5%" align="right" valign="top">
										<font class="font13">
												<xsl:if test="string-length(@pos)=0">
													<xsl:if test="string-length(daterange/start) > 0">
														<xsl:if test="string-length(daterange/end) > 0 and (daterange/start = daterange/end)">
															<xsl:apply-templates select="daterange/start"/>
														</xsl:if>
														<xsl:if test="string-length(daterange/end) > 0 and (daterange/start != daterange/end)">
															<xsl:apply-templates select="daterange/start"/>
															<xsl:variable name="temp2End">
																<xsl:value-of select="substring-before(daterange/end ,'/')" />
															</xsl:variable>
															<xsl:if test="string-length(daterange/end) > 0 and string-length($temp2End) > 0">
																<xsl:text> - </xsl:text>
															</xsl:if>
															<xsl:choose>
																<xsl:when test="daterange/end/@currentjob = '1'">
																	<xsl:text>Present</xsl:text>
																</xsl:when>
																<xsl:otherwise>
																	<xsl:apply-templates select="daterange/end"/>
																</xsl:otherwise>
															</xsl:choose>
														</xsl:if>
														<xsl:if test="string-length(daterange/end) = '0'">
															<xsl:apply-templates select="daterange/start"/>
															<!--<xsl:text> - xxxx</xsl:text>-->
														</xsl:if>
													</xsl:if>
													<!--<br/>-->
												</xsl:if>
										</font >
										<!--<br/>-->
									</td>
								</tr>
									<tr>
										<td style="height: 13px;">
										</td>
									</tr>
							</xsl:for-each>
						</xsl:for-each>
					</table>
				</td>
			</tr>

		</table>
		<!-- Veteran experience -->
		<xsl:if test="((string-length(//veteran//rank) > 0 and ((//veteran//rank) != '- select rank -' and (//veteran//rank) != '0')) or (string-length(//veteran//moc) > 0) or (string-length(//veteran//vet_start_date) > 0) or (string-length(//veteran//branch_of_service) > 0 and (//veteran//branch_of_service) != 'Select Branch of Service') or (string-length(//veteran//unit_affiliation) > 0)) and (//veteran//vet_era) != 3">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td width="30%" valign="top">
						<font class="font13">
							<b>Military Service</b>
						</font>
					</td>

					<td width="70%">
						<div style="text-align: justify;">
							<xsl:apply-templates select="//veteran/history|//veteran[not(history)]" mode="veteran" />
						</div>
					</td>
				</tr>
			</table>
		</xsl:if>

		<!-- Internships (bottom of resume)-->
		<xsl:if test="count(//special/internships)>0">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td width="30%" valign="top">
						<font class="font13">
							<b>Internships</b>
						</font>
					</td>
					<td width="50%">
						<font class="font13">
							<div style="text-align: justify;">
								<xsl:for-each select="//special/internships">
									<xsl:value-of select="."/>
									<br/>
								</xsl:for-each>
							</div>
						</font>
					</td>
					<td width="20%"></td>
				</tr>
				<tr>
					<td style="height: 13px;"></td>
				</tr>
			</table>
		</xsl:if>
	</xsl:template>

	<xsl:template match="*" mode="veteran">
		<xsl:variable name="veteran" select="." />
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td  align="left" width="72.5%">
					<font class="font13">
						<xsl:if test="string-length($veteran/rank) > 0 and (($veteran/rank) != '- select rank -' and ($veteran/rank) != '0')">
							<xsl:value-of select="$veteran/rank"/>
						</xsl:if>
						<xsl:if test="string-length($veteran/moc) > 0">
							<xsl:if test="string-length($veteran/rank) > 0 and (($veteran/rank) != '- select rank -' and ($veteran/rank) != '0')">
								<xsl:text>, </xsl:text>
							</xsl:if>
							<xsl:if test="string-length($veteran/mostext) > 0">
								<xsl:value-of select="$veteran/mostext"/>
							</xsl:if>
							<xsl:if test="string-length($veteran/mostext) = 0">
								<xsl:value-of select="$veteran/moc"/>
							</xsl:if>
						</xsl:if>
					</font>
				</td>
				<td align="right" width="27.5%">
					<font class="font13">
						<div>
							<xsl:if test="string-length($veteran/vet_start_date) > 0">
								<xsl:if test="string-length($veteran/vet_end_date) > 0 and ($veteran/vet_start_date = $veteran/vet_end_date)">
									<xsl:apply-templates select="vet_start_date"/>
								</xsl:if>
								<xsl:if test="string-length($veteran/vet_end_date) > 0 and ($veteran/vet_start_date != $veteran/vet_end_date)">
									<xsl:apply-templates select="$veteran/vet_start_date"/>
									<xsl:text> - </xsl:text>
									<xsl:apply-templates select="$veteran/vet_end_date"/>

								</xsl:if>
								<xsl:if test="string-length($veteran/vet_end_date) = '0'">
									<xsl:apply-templates select="$veteran/vet_start_date"/>

								</xsl:if>
							</xsl:if>
						</div>
					</font>
				</td>
			</tr>
			<tr>
				<td valign="top" colspan="2">
					<b>
						<font class="font13">
							<xsl:value-of select="$veteran/branch_of_service"/>
						</font>

					</b>
				</td>
			</tr>
			<xsl:if test="string-length($veteran/unit_affiliation) > 0">
				<tr>
					<td valign="top" colspan="2">
						<font class="font13">
							<xsl:value-of select="($veteran/unit_affiliation)"/>
						</font>
					</td>
				</tr>
			</xsl:if>
			<tr>
				<td style="height: 13px;" colspan="2">
				</td>
			</tr>
		</table>
	</xsl:template>

	<xsl:template match="phone">

		<xsl:if test="(//resume/contact/address/country)[1] = 'US'">
			<xsl:choose>
				<xsl:when test="@type='work'">
					<xsl:value-of select="format-number(.,'(000) 000-0000')"/>
					<xsl:text> (Work)</xsl:text>
					<br/>
				</xsl:when>
				<xsl:when test="@type='cell'">
					<xsl:value-of select="format-number(.,'(000) 000-0000')"/>
					<xsl:text> (Cell)</xsl:text>
					<br/>
				</xsl:when>
				<xsl:when test="@type='fax'">
					<xsl:value-of select="format-number(.,'(000) 000-0000')"/>
					<xsl:text> (Fax)</xsl:text>
					<br/>
				</xsl:when>
				<xsl:when test="@type='pager'">
					<xsl:value-of select="format-number(.,'(000) 000-0000')"/>
					<xsl:text> (Pager)</xsl:text>
					<br/>
				</xsl:when>
				<xsl:when test="@type='home'">
					<xsl:value-of select="format-number(.,'(000) 000-0000')"/>
					<xsl:text> (Home)</xsl:text>
					<br/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="."/>
					<xsl:text> (Non US)</xsl:text>
					<br/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>

		<xsl:if test="(//resume/contact/address/country)[1] != 'US'">
			<xsl:choose>
				<xsl:when test="@type='work'">
					<xsl:value-of select="."/>
					<xsl:text> (Work)</xsl:text>
					<br/>
				</xsl:when>
				<xsl:when test="@type='cell'">
					<xsl:value-of select="."/>
					<xsl:text> (Cell)</xsl:text>
					<br/>
				</xsl:when>
				<xsl:when test="@type='fax'">
					<xsl:value-of select="."/>
					<xsl:text> (Fax)</xsl:text>
					<br/>
				</xsl:when>
				<xsl:when test="@type='pager'">
					<xsl:value-of select="."/>
					<xsl:text> (Pager)</xsl:text>
					<br/>
				</xsl:when>
				<xsl:when test="@type='home'">
					<xsl:value-of select="."/>
					<xsl:text> (Home)</xsl:text>
					<br/>
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="."/>
					<br/>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:if>
	</xsl:template>
	<xsl:template name="string-replace-FC_CRLF_BULLETS">
		<xsl:param name="text" />
		<xsl:param name="replace" />
		<xsl:param name="by" />
		<xsl:choose>
			<xsl:when test="contains($text, $replace)">
				<xsl:value-of select="substring-before($text,$replace)" />
				<xsl:value-of select="$by" />
				<xsl:call-template name="string-replace-FC_CRLF_BULLETS">
					<xsl:with-param name="text" select="substring-after($text,$replace)" />
					<xsl:with-param name="replace" select="$replace" />
					<xsl:with-param name="by" select="$by" />
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$text" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template name="string-replace-BULLETS">
		<xsl:param name="text" />
		<xsl:param name="replace" />
		<xsl:param name="by" />
		<xsl:choose>
			<xsl:when test="starts-with($text, $replace)">
				<xsl:value-of select="substring-before($text,$replace)" />
				<xsl:value-of select="$by" />
				<xsl:call-template name="string-replace-BULLETS">
					<xsl:with-param name="text" select="substring-after($text,$replace)" />
					<xsl:with-param name="replace" select="$replace" />
					<xsl:with-param name="by" select="$by" />
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$text" />
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>


	<xsl:template match="start">
		<!--
    <xsl:if test="count(@iso8601) > 0">
      <xsl:value-of select="."/>
    </xsl:if>
    <xsl:if test="count(@iso8601) = '0'">
      <xsl:value-of select="."/>
    </xsl:if>
    -->
		<xsl:variable name="mo">
			<xsl:value-of select="substring-before(. ,'/')" />
		</xsl:variable>
		<xsl:variable name="year">
			<xsl:value-of select="substring-after(.,'/')" />
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="$mo = '1' or $mo = '01'">Jan</xsl:when>
			<xsl:when test="$mo = '2' or $mo = '02'">Feb</xsl:when>
			<xsl:when test="$mo = '3' or $mo = '03'">Mar</xsl:when>
			<xsl:when test="$mo = '4' or $mo = '04'">Apr</xsl:when>
			<xsl:when test="$mo = '5' or $mo = '05'">May</xsl:when>
			<xsl:when test="$mo = '6' or $mo = '06'">Jun</xsl:when>
			<xsl:when test="$mo = '7' or $mo = '07'">Jul</xsl:when>
			<xsl:when test="$mo = '8' or $mo = '08'">Aug</xsl:when>
			<xsl:when test="$mo = '9' or $mo = '09'">Sep</xsl:when>
			<xsl:when test="$mo = '10'">Oct</xsl:when>
			<xsl:when test="$mo = '11'">Nov</xsl:when>
			<xsl:when test="$mo = '12'">Dec</xsl:when>
		</xsl:choose>
		<xsl:text> </xsl:text>
		<xsl:value-of select="$year"/>

	</xsl:template>

	<xsl:template match="end">
		<!--
    <xsl:if test="count(@iso8601) > 0">
      <xsl:value-of select="."/>
    </xsl:if>
    <xsl:if test="count(@iso8601) = '0'">
      <xsl:value-of select="."/>
    </xsl:if>
   -->
		<xsl:variable name="mo">
			<xsl:value-of select="substring-before(. ,'/')" />
		</xsl:variable>
		<xsl:variable name="year">
			<xsl:value-of select="substring-after(.,'/')" />
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="$mo = '1' or $mo = '01'">Jan</xsl:when>
			<xsl:when test="$mo = '2' or $mo = '02'">Feb</xsl:when>
			<xsl:when test="$mo = '3' or $mo = '03'">Mar</xsl:when>
			<xsl:when test="$mo = '4' or $mo = '04'">Apr</xsl:when>
			<xsl:when test="$mo = '5' or $mo = '05'">May</xsl:when>
			<xsl:when test="$mo = '6' or $mo = '06'">Jun</xsl:when>
			<xsl:when test="$mo = '7' or $mo = '07'">Jul</xsl:when>
			<xsl:when test="$mo = '8' or $mo = '08'">Aug</xsl:when>
			<xsl:when test="$mo = '9' or $mo = '09'">Sep</xsl:when>
			<xsl:when test="$mo = '10'">Oct</xsl:when>
			<xsl:when test="$mo = '11'">Nov</xsl:when>
			<xsl:when test="$mo = '12'">Dec</xsl:when>
		</xsl:choose>
		<xsl:text> </xsl:text>
		<xsl:value-of select="$year"/>

	</xsl:template>

	<xsl:template match="vet_start_date">

		<!--<xsl:if test="count(@iso8601) > 0">
      <xsl:value-of select="."/>
    </xsl:if>
    <xsl:if test="count(@iso8601) = '0'">
      <xsl:value-of select="."/>
    </xsl:if>
-->
		<xsl:variable name="mo">
			<xsl:value-of select="substring-before(. ,'/')" />
		</xsl:variable>
		<xsl:variable name="day-temp">
			<xsl:value-of select="substring-after(.,'/')" />
		</xsl:variable>
		<xsl:variable name="year">
			<xsl:value-of select="substring-after($day-temp,'/')" />
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="$mo = '1' or $mo = '01'">Jan.</xsl:when>
			<xsl:when test="$mo = '2' or $mo = '02'">Feb.</xsl:when>
			<xsl:when test="$mo = '3' or $mo = '03'">Mar.</xsl:when>
			<xsl:when test="$mo = '4' or $mo = '04'">Apr.</xsl:when>
			<xsl:when test="$mo = '5' or $mo = '05'">May</xsl:when>
			<xsl:when test="$mo = '6' or $mo = '06'">June</xsl:when>
			<xsl:when test="$mo = '7' or $mo = '07'">July</xsl:when>
			<xsl:when test="$mo = '8' or $mo = '08'">Aug.</xsl:when>
			<xsl:when test="$mo = '9' or $mo = '09'">Sept.</xsl:when>
			<xsl:when test="$mo = '10'">Oct.</xsl:when>
			<xsl:when test="$mo = '11'">Nov.</xsl:when>
			<xsl:when test="$mo = '12'">Dec.</xsl:when>
		</xsl:choose>
		<xsl:text> </xsl:text>
		<xsl:value-of select="$year"/>


	</xsl:template>

	<xsl:template match="vet_end_date">
		<!--<xsl:if test="count(@iso8601) > 0">
      <xsl:value-of select="."/>
    </xsl:if>
    <xsl:if test="count(@iso8601) = '0'">
      <xsl:value-of select="."/>
    </xsl:if>
    -->
		<xsl:variable name="mo">
			<xsl:value-of select="substring-before(. ,'/')" />
		</xsl:variable>
		<xsl:variable name="day-temp">
			<xsl:value-of select="substring-after(.,'/')" />
		</xsl:variable>
		<xsl:variable name="year">
			<xsl:value-of select="substring-after($day-temp,'/')" />
		</xsl:variable>
		<xsl:choose>
			<xsl:when test="$mo = '1' or $mo = '01'">Jan.</xsl:when>
			<xsl:when test="$mo = '2' or $mo = '02'">Feb.</xsl:when>
			<xsl:when test="$mo = '3' or $mo = '03'">Mar.</xsl:when>
			<xsl:when test="$mo = '4' or $mo = '04'">Apr.</xsl:when>
			<xsl:when test="$mo = '5' or $mo = '05'">May</xsl:when>
			<xsl:when test="$mo = '6' or $mo = '06'">June</xsl:when>
			<xsl:when test="$mo = '7' or $mo = '07'">July</xsl:when>
			<xsl:when test="$mo = '8' or $mo = '08'">Aug.</xsl:when>
			<xsl:when test="$mo = '9' or $mo = '09'">Sept.</xsl:when>
			<xsl:when test="$mo = '10'">Oct.</xsl:when>
			<xsl:when test="$mo = '11'">Nov.</xsl:when>
			<xsl:when test="$mo = '12'">Dec.</xsl:when>
		</xsl:choose>
		<xsl:text> </xsl:text>
		<xsl:value-of select="$year"/>

	</xsl:template>


	<xsl:template match="completion_date">

		<!--<xsl:if test="count(@iso8601) > 0">
      <xsl:value-of select="."/>
    </xsl:if>
    <xsl:if test="count(@iso8601) = '0'">
      <xsl:value-of select="."/>
    </xsl:if>
-->

		<xsl:variable name="year">
			<xsl:value-of select="substring-after(.,'/')" />
		</xsl:variable>

		<xsl:value-of select="$year"/>

	</xsl:template>

	<xsl:template match="completiondate">
		<xsl:value-of select="substring-after(.,'/')"/>
	</xsl:template>

	<xsl:template match="expectedcompletiondate">
		<xsl:text>Pending completion in </xsl:text>
		<xsl:value-of select="."/>
	</xsl:template>
	
	<xsl:template name="state">
		<xsl:param name="code" select="translate(.,$lowercase,$uppercase)" />
		<xsl:choose>
			<xsl:when test="$code='AE'">
				<xsl:value-of select="'A.F. Africa,CAN,EU,MidEast'"/>
			</xsl:when>
			<xsl:when test="$code='AA'">
				<xsl:value-of select="'A.F. Americas (Except CAN)'"/>
			</xsl:when>
			<xsl:when test="$code='AP'">
				<xsl:value-of select="'A.F. Pacific'"/>
			</xsl:when>
			<xsl:when test="$code='AL'">
				<xsl:value-of select="'Alabama'"/>
			</xsl:when>
			<xsl:when test="$code='AK'">
				<xsl:value-of select="'Alaska'"/>
			</xsl:when>
			<xsl:when test="$code='AS'">
				<xsl:value-of select="'American Samoa'"/>
			</xsl:when>
			<xsl:when test="$code='AZ'">
				<xsl:value-of select="'Arizona'"/>
			</xsl:when>
			<xsl:when test="$code='AR'">
				<xsl:value-of select="'Arkansas'"/>
			</xsl:when>
			<xsl:when test="$code='CA'">
				<xsl:value-of select="'California'"/>
			</xsl:when>
			<xsl:when test="$code='CO'">
				<xsl:value-of select="'Colorado'"/>
			</xsl:when>
			<xsl:when test="$code='CT'">
				<xsl:value-of select="'Connecticut'"/>
			</xsl:when>
			<xsl:when test="$code='DE'">
				<xsl:value-of select="'Delaware'"/>
			</xsl:when>
			<xsl:when test="$code='DC'">
				<xsl:value-of select="'District of Columbia'"/>
			</xsl:when>
			<xsl:when test="$code='FM'">
				<xsl:value-of select="'Fed. States of Micronesia'"/>
			</xsl:when>
			<xsl:when test="$code='FL'">
				<xsl:value-of select="'Florida'"/>
			</xsl:when>
			<xsl:when test="$code='GA'">
				<xsl:value-of select="'Georgia'"/>
			</xsl:when>
			<xsl:when test="$code='GU'">
				<xsl:value-of select="'Guam'"/>
			</xsl:when>
			<xsl:when test="$code='HI'">
				<xsl:value-of select="'Hawaii'"/>
			</xsl:when>
			<xsl:when test="$code='ID'">
				<xsl:value-of select="'Idaho'"/>
			</xsl:when>
			<xsl:when test="$code='IL'">
				<xsl:value-of select="'Illinois'"/>
			</xsl:when>
			<xsl:when test="$code='IN'">
				<xsl:value-of select="'Indiana'"/>
			</xsl:when>
			<xsl:when test="$code='IA'">
				<xsl:value-of select="'Iowa'"/>
			</xsl:when>
			<xsl:when test="$code='KS'">
				<xsl:value-of select="'Kansas'"/>
			</xsl:when>
			<xsl:when test="$code='KY'">
				<xsl:value-of select="'Kentucky'"/>
			</xsl:when>
			<xsl:when test="$code='LA'">
				<xsl:value-of select="'Louisiana'"/>
			</xsl:when>
			<xsl:when test="$code='ME'">
				<xsl:value-of select="'Maine'"/>
			</xsl:when>
			<xsl:when test="$code='MH'">
				<xsl:value-of select="'Marshall Islands'"/>
			</xsl:when>
			<xsl:when test="$code='MD'">
				<xsl:value-of select="'Maryland'"/>
			</xsl:when>
			<xsl:when test="$code='MA'">
				<xsl:value-of select="'Massachusetts'"/>
			</xsl:when>
			<xsl:when test="$code='MI'">
				<xsl:value-of select="'Michigan'"/>
			</xsl:when>
			<xsl:when test="$code='MN'">
				<xsl:value-of select="'Minnesota'"/>
			</xsl:when>
			<xsl:when test="$code='MS'">
				<xsl:value-of select="'Mississippi'"/>
			</xsl:when>
			<xsl:when test="$code='MO'">
				<xsl:value-of select="'Missouri'"/>
			</xsl:when>
			<xsl:when test="$code='MT'">
				<xsl:value-of select="'Montana'"/>
			</xsl:when>
			<xsl:when test="$code='NE'">
				<xsl:value-of select="'Nebraska'"/>
			</xsl:when>
			<xsl:when test="$code='NV'">
				<xsl:value-of select="'Nevada'"/>
			</xsl:when>
			<xsl:when test="$code='NH'">
				<xsl:value-of select="'New Hampshire'"/>
			</xsl:when>
			<xsl:when test="$code='NJ'">
				<xsl:value-of select="'New Jersey'"/>
			</xsl:when>
			<xsl:when test="$code='NM'">
				<xsl:value-of select="'New Mexico'"/>
			</xsl:when>
			<xsl:when test="$code='NY'">
				<xsl:value-of select="'New York'"/>
			</xsl:when>
			<xsl:when test="$code='NC'">
				<xsl:value-of select="'North Carolina'"/>
			</xsl:when>
			<xsl:when test="$code='ND'">
				<xsl:value-of select="'North Dakota'"/>
			</xsl:when>
			<xsl:when test="$code='MP'">
				<xsl:value-of select="'Northern Mariana Islands'"/>
			</xsl:when>
			<xsl:when test="$code='OH'">
				<xsl:value-of select="'Ohio'"/>
			</xsl:when>
			<xsl:when test="$code='OK'">
				<xsl:value-of select="'Oklahoma'"/>
			</xsl:when>
			<xsl:when test="$code='OR'">
				<xsl:value-of select="'Oregon'"/>
			</xsl:when>
			<xsl:when test="$code='ZZ'">
				<xsl:value-of select="'Outside U.S.'"/>
			</xsl:when>
			<xsl:when test="$code='PW'">
				<xsl:value-of select="'Palau'"/>
			</xsl:when>
			<xsl:when test="$code='PA'">
				<xsl:value-of select="'Pennsylvania'"/>
			</xsl:when>
			<xsl:when test="$code='PR'">
				<xsl:value-of select="'Puerto Rico'"/>
			</xsl:when>
			<xsl:when test="$code='RI'">
				<xsl:value-of select="'Rhode Island'"/>
			</xsl:when>
			<xsl:when test="$code='SC'">
				<xsl:value-of select="'South Carolina'"/>
			</xsl:when>
			<xsl:when test="$code='SD'">
				<xsl:value-of select="'South Dakota'"/>
			</xsl:when>
			<xsl:when test="$code='TN'">
				<xsl:value-of select="'Tennessee'"/>
			</xsl:when>
			<xsl:when test="$code='TX'">
				<xsl:value-of select="'Texas'"/>
			</xsl:when>
			<xsl:when test="$code='VI'">
				<xsl:value-of select="'U.S. Virgin Islands'"/>
			</xsl:when>
			<xsl:when test="$code='UT'">
				<xsl:value-of select="'Utah'"/>
			</xsl:when>
			<xsl:when test="$code='VT'">
				<xsl:value-of select="'Vermont'"/>
			</xsl:when>
			<xsl:when test="$code='VA'">
				<xsl:value-of select="'Virginia'"/>
			</xsl:when>
			<xsl:when test="$code='WA'">
				<xsl:value-of select="'Washington'"/>
			</xsl:when>
			<xsl:when test="$code='WV'">
				<xsl:value-of select="'West Virginia'"/>
			</xsl:when>
			<xsl:when test="$code='WI'">
				<xsl:value-of select="'Wisconsin'"/>
			</xsl:when>
			<xsl:when test="$code='WY'">
				<xsl:value-of select="'Wyoming'"/>
			</xsl:when>

			<xsl:otherwise>
				<xsl:value-of select="$code"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="*[@include='0' or normalize-space() = '']" priority="2" mode="addins" />

	<xsl:template match="*" mode="addins">
		<xsl:param name="summaryName" />
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td width="30%" valign="top">
					<font class="font13">
						<b>
							<xsl:value-of select="$summaryName"/>
						</b>
					</font>
				</td>
				<td width="50%">
					<font class="font13">
						<div style="text-align: justify;">
							<xsl:value-of select="."/>
						</div>
					</font>
				</td>
				<td width="20%"></td>
			</tr>
			<tr>
				<td style="height: 13px;">
				</td>
			</tr>
		</table>
	</xsl:template>

	<xsl:template match="languages">
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td width="30%" valign="top">
					<font class="font13">
						<b>Languages</b>
					</font>
				</td>
				<td width="50%">
					<div style="text-align: left;">
						<font class="font13">
							<xsl:choose>
								<xsl:when test="languages_profiency">
									<xsl:value-of select="languages_profiency"/>
									<br />
								</xsl:when>
								<xsl:otherwise>
									<xsl:for-each select="language">
										<xsl:value-of select="language"/>
										<xsl:value-of select="proficiency"/>
										<br/>
									</xsl:for-each>
								</xsl:otherwise>
							</xsl:choose>
						</font>
					</div>
				</td>
				<td width="20%"></td>
			</tr>
			<tr>
				<td style="height: 13px;">
				</td>
			</tr>
		</table>
	</xsl:template>
</xsl:stylesheet>
