﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
>
	<xsl:output method="html" indent="yes"/>

	<xsl:template match="/">
		<html>
			<body>
				<xsl:if test="(//EmployerLogoUrl) != ''">
					<p>
						<img src="{//EmployerLogoUrl}" />
					</p>
				</xsl:if>
				<p>
					<b><xsl:value-of select="(//JobTitle)"/></b>
					<xsl:if test="string-length(//CompanyName)>0">
						<br/>
						<xsl:value-of select="(//CompanyName)"/>
					</xsl:if>
					<xsl:if test="string-length(//Location)>0">
						<br/>
						<xsl:value-of select="(//Location)"/>
					</xsl:if>
          <xsl:apply-templates select="//HomeBasedWorker" mode="bypasscensorship" />
					<xsl:if test="string-length(//NumberOfOpenings)>0">
						<br/>
						<xsl:value-of select="(//NumberOfOpenings)"/>
					</xsl:if>
					<xsl:if test="string-length(//ClosingDate)>0">
						<br/>
						<xsl:value-of select="(//ClosingDate)"/>
						<br/>
					</xsl:if>
          <xsl:if test="string-length(//EducationWorkWeek)>0">
						<br/>
            <b>
              <xsl:value-of select="(//WorkWeekLabel)"/>
            </b>
            <br/>
						<xsl:value-of select="(//EducationWorkWeek)"/>
					</xsl:if>
					<xsl:if test="string-length(//NormalWorkingDays)>0">
						<br/>
						<xsl:value-of select="(//NormalWorkingDays)"/>
					</xsl:if>
					<xsl:if test="string-length(//NormalWorkShifts)>0">
						<br/>
						<xsl:value-of select="(//NormalWorkShifts)"/>
					</xsl:if>
					<xsl:if test="string-length(//OvertimeRequired)>0">
						<br/>
						<xsl:value-of select="(//OvertimeRequired)"/>
					</xsl:if>
					<xsl:if test="string-length(//JobStatus)>0">
						<br/>
						<xsl:value-of select="(//JobStatus)"/>
					</xsl:if>
					<xsl:if test="string-length(//FederalContractor)>0">
						<br/>
						<xsl:value-of select="(//FederalContractor)"/>
						<xsl:value-of select="(//FederalContractorExpiresOn)"/>
					</xsl:if>
          <!--
					<xsl:if test="string-length(//ForeignLabourCertification)>0">
						<br/>
						<xsl:value-of select="(//ForeignLabourCertification)"/>
					</xsl:if>
          -->
					<xsl:if test="string-length(//CourtOrderedAffirmativeAction)>0">
						<br/>
						<xsl:value-of select="(//CourtOrderedAffirmativeAction)"/>
					</xsl:if>
					<xsl:if test="string-length(//WorkOpportunitiesTaxCreditHires)>0">
						<br/>
						<xsl:value-of select="(//WorkOpportunitiesTaxCreditHires)"/>
					</xsl:if>
					<xsl:if test="string-length(//PostingFlags)>0">
						<br/>
						<xsl:value-of select="(//PostingFlags)"/>
					</xsl:if>
				</p>
				<xsl:if test="string-length(//CompanyName)>0 and string-length(//EmployerDescription)>0 and (//EmployerDescriptionPostingPosition) = 'AboveJobPosting'">
					<p>
						<b>
							<xsl:value-of select="(//EmployerDescriptionLabel)"/>
						</b>
						<br/>
						<xsl:value-of select="(//EmployerDescription)"/>
					</p>
				</xsl:if>
				<p>
					<xsl:call-template name="PreserveLineBreaks">
						<xsl:with-param name="text" select="(//JobDescription)"/>
					</xsl:call-template>
				</p>

        <xsl:variable name="HasNCRCDetails" select="//NCRCDetails[normalize-space() != '']" />
        
				<xsl:if test="string-length(//MinimumEducationLevelRequirement)>0 
                or string-length(//MinimumExperienceRequirement)>0 
                or string-length(//MinimumAgeRequirement)>0 
                or string-length(//StudentEnrolledRequirement)>0
                or string-length(//MinimumCollegeYearsRequirement)>0
                or string-length(//ProgramOfStudyRequirements)>0
                or string-length(//DrivingLicenceRequirement)>0 
                or string-length(//LicencesRequirements)>0 
                or string-length(//CertificationRequirements)>0 
                or string-length(//LanguagesRequirements)>0 
                or count(//SpecialRequirements/child::*)>0
                or $HasNCRCDetails
                or string-length(//HoursPerWeek)>0">
					<p>
						<b><xsl:value-of select="(//RequirementsLabel)"/></b>
            <xsl:if test="string-length(//HoursPerWeek)>0">
              <br/>
              <xsl:value-of select="//HoursPerWeek"/>
            </xsl:if>
						<xsl:if test="string-length(//MinimumEducationLevelRequirement)>0">
							<br/>
							<xsl:value-of select="(//MinimumEducationLevelRequirement)"/>
						</xsl:if>
						<xsl:if test="string-length(//MinimumExperienceRequirement)>0">
							<br/>
							<xsl:value-of select="(//MinimumExperienceRequirement)"/>
						</xsl:if>
						<xsl:if test="string-length(//MinimumAgeRequirement)>0">
							<br/>
							<xsl:value-of select="(//MinimumAgeRequirement)"/>
						</xsl:if>
            <xsl:apply-templates select="//StudentEnrolledRequirement" mode="text" />
            <xsl:apply-templates select="//MinimumCollegeYearsRequirement" mode="text" />
            <xsl:apply-templates select="//ProgramOfStudyRequirements" mode="text" />
            <xsl:if test="string-length(//DrivingLicenceRequirement)>0">
							<br/>
							<xsl:value-of select="(//DrivingLicenceRequirement)"/>
						</xsl:if>
						<xsl:if test="string-length(//LicencesRequirements)>0">
							<br/>
							<xsl:value-of select="(//LicencesRequirements)"/>
						</xsl:if>
						<xsl:if test="string-length(//CertificationRequirements)>0">
							<br/>
							<xsl:value-of select="(//CertificationRequirements)"/>
						</xsl:if>
						<xsl:if test="string-length(//LanguagesRequirements)>0">
							<br/>
							<xsl:value-of select="(//LanguagesRequirements)"/>
						</xsl:if>
						<xsl:for-each select="(//SpecialRequirements)">
							<xsl:for-each select="(string)">
								<br/>
								<xsl:value-of select="."/>
							</xsl:for-each>
						</xsl:for-each>
            <xsl:if test="$HasNCRCDetails">
              <br/>
              <xsl:value-of select="$HasNCRCDetails"/>
            </xsl:if>
					</p>
				</xsl:if>
				<xsl:if test="string-length(//LeaveBenefits)>0 or string-length(//RetirementBenefits)>0 or string-length(//InsuranceBenefits)>0 or string-length(//MiscellaneousBenefits)>0 or string-length(//SalaryDetails)>0 or string-length(//OtherSalary)>0">
					<p>
						<b><xsl:value-of select="(//BenefitsLabel)"/></b>
						<xsl:apply-templates select="//SalaryDetails" mode="text" />
						<xsl:apply-templates select="//OtherSalary" mode="text" />
						<xsl:variable name="HoursText" select="//HoursText[normalize-space()]" />
						<xsl:choose>
							<xsl:when test="$HoursText">
								<br/>
								<xsl:value-of select="$HoursText" />
							</xsl:when>
							<xsl:otherwise>
								<xsl:apply-templates select="//EmploymentStatus" mode="text" />
								<xsl:apply-templates select="//JobType" mode="text" />
							</xsl:otherwise>
						</xsl:choose>
						<xsl:if test="string-length(//CommissionLabel)>0">
							<br/>
							<xsl:value-of select="(//CommissionLabel)"/>
						</xsl:if>
						<xsl:if test="string-length(//LeaveBenefits)>0">
							<br/>
							<xsl:value-of select="(//LeaveBenefits)"/>
						</xsl:if>
						<xsl:if test="string-length(//RetirementBenefits)>0">
							<br/>
							<xsl:value-of select="(//RetirementBenefits)"/>
						</xsl:if>
						<xsl:if test="string-length(//InsuranceBenefits)>0">
							<br/>
							<xsl:value-of select="(//InsuranceBenefits)"/>
						</xsl:if>
						<xsl:if test="string-length(//MiscellaneousBenefits)>0">
							<br/>
							<xsl:value-of select="(//MiscellaneousBenefits)"/>
						</xsl:if>
					</p>					
				</xsl:if>
				<xsl:if test="string-length(//CompanyName)>0 and string-length(//EmployerDescription)>0 and (//EmployerDescriptionPostingPosition) = 'BelowJobPosting'">
					<p>
						<b><xsl:value-of select="(//EmployerDescriptionLabel)"/></b>
						<br/>
						<xsl:value-of select="(//EmployerDescription)"/>
					</p>
				</xsl:if>
				<p>
					<b><xsl:value-of select="(//HowToApplyLabel)"/></b>
					<xsl:if test="string-length(//ApplyThroughFocusCareerLabel)>0">
						<br/>
						<a href="{//FocusCareerUrl}" target="_blank"><xsl:value-of select="(//ApplyThroughFocusCareerLabel)"/></a>
					</xsl:if>
					<xsl:if test="string-length(//EmailApplicationTo)>0">
						<br/>
						<xsl:value-of select="(//EmailApplicationLabel)"/>
						<a href="mailto:{//EmailApplicationTo}"><xsl:value-of select="(//EmailApplicationTo)"/></a>
					</xsl:if>
					<xsl:if test="string-length(//ApplyOnlineUrl)>0">
						<br/>
						<a href="{//ApplyOnlineUrl}" target="_blank"><xsl:value-of select="(//ApplyOnlineLabel)"/></a>
					</xsl:if>
					<xsl:if test="string-length(//MailApplicationToAddress)>0">
						<br/>
						<xsl:value-of select="(//MailApplicationLabel)"/>
						<xsl:value-of select="(//MailApplicationToAddress)"/>
					</xsl:if>
					<xsl:if test="string-length(//FaxApplicationToNumber)>0">
						<br/>
						<xsl:value-of select="(//FaxApplicationLabel)"/>
						<xsl:value-of select="(//FaxApplicationToNumber)"/>
					</xsl:if>
					<xsl:if test="string-length(//TelephoneApplicationToNumber)>0">
						<br/>
						<xsl:value-of select="(//TelephoneApplicationLabel)"/>
						<xsl:value-of select="(//TelephoneApplicationToNumber)"/>
					</xsl:if>
					<xsl:if test="string-length(//InPersonApplicationAddress)>0">
						<br/>
						<xsl:value-of select="(//InPersonApplicationLabel)"/>
						<xsl:value-of select="(//InPersonApplicationAddress)"/>
					</xsl:if>
					<xsl:if test="string-length(//OtherApplicationInstructions)>0">
						<br/>
						<xsl:value-of select="(//OtherApplicationInstructions)"/>
					</xsl:if>
				</p>
				<p>
					<small><xsl:value-of select="(//PostingReference)"/></small>
				</p>
        #POSTINGFOOTERTITLE#
        #CREDFOOTER# <!-- This is for the credit check footer - called it CRED so that the word credit doesn't get flagged by the censorship check -->
        <xsl:if test="(//CreditCheckRequired) = 'true' or (//CreditCheckRequired) = '1'">
          <br/>
          <br/>
        </xsl:if>
        #CRIMFOOTER# <!-- This is for the criminal background check footer - called it CRIM so that the word criminal doesn't get flagged by the censorship check -->
			</body>
    </html>
	</xsl:template>

  <xsl:template match="*[string-length(.) = 0]" mode="text" />

  <xsl:template match="*" mode="text">
    <br/>
    <xsl:value-of select="."/>
  </xsl:template>

	<xsl:template match="*[string-length(.) = 0]" mode="bypasscensorship" />

	<xsl:template match="*" mode="bypasscensorship">
		<br/>
		<span data-nocensorship="true">
			<xsl:value-of select="."/>
		</span>
	</xsl:template>
	
	<xsl:template name="PreserveLineBreaks">
		<xsl:param name="text"/>
		<xsl:choose>
			<xsl:when test="contains($text,'&#xA;')">
				<xsl:value-of select="substring-before($text,'&#xA;')"/>
				<br/>
				<xsl:call-template name="PreserveLineBreaks">
					<xsl:with-param name="text">
						<xsl:value-of select="substring-after($text,'&#xA;')"/>
					</xsl:with-param>
				</xsl:call-template>
			</xsl:when>
			<xsl:otherwise>
				<xsl:value-of select="$text"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

</xsl:stylesheet>
