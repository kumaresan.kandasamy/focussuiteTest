﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

	<xsl:output method="text" encoding="UTF-8" />

	<xsl:template match="*">
		<!-- Simply recurse over the children. -->
		<xsl:apply-templates />
	</xsl:template>
  
  <!-- Don't count hidden options section or canon skill rollup-->
  <xsl:template match="hide_options" />
  <xsl:template match="skillrollup" />

</xsl:stylesheet>