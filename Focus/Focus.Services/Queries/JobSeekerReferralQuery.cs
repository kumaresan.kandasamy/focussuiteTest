﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Linq;
using Focus.Core;
using Focus.Core.Criteria.CandidateApplication;
using Focus.Core.Settings.Interfaces;
using Focus.Data.Core.Entities;
using Focus.Data.Repositories.Contracts;
using Framework.Core;

#endregion


namespace Focus.Services.Queries
{
	class JobSeekerReferralQuery : QueryBase<JobSeekerReferralAllStatusView, ICoreRepository, JobSeekerReferralCriteria>
	{
		private readonly IAppSettings _settings;

		/// <summary>
		/// Initializes a new instance of the <see cref="JobSeekerReferralQuery" /> class.
		/// </summary>
		/// <param name="repository">The repository.</param>
		/// <param name="criteria">The criteria.</param>
		/// <param name="settings">The settings.</param>
		public JobSeekerReferralQuery(ICoreRepository repository, JobSeekerReferralCriteria criteria, IAppSettings settings) : base(repository, criteria)
		{
			_settings = settings;
		}

		/// <summary>
		/// Produces a query for this type.
		/// </summary>
		/// <returns></returns>
		public override IQueryable<JobSeekerReferralAllStatusView> Query()
		{
			var query = Repository.JobSeekerReferralAllStatusViews;


            
          //  query = query.Join(Repository.Persons.Where(x => x.Age > _settings.UnderAgeJobSeekerRestrictionThreshold),q => q.PersonId,p => p.Id,(q, p) => q);
			if (Criteria.ApprovalStatus.IsNotNull())
			{
				query = query.Where(x => x.ApprovalStatus.Equals(Criteria.ApprovalStatus));
			}

			if (Criteria.JobSeekerType.IsNotNull() && Criteria.JobSeekerType != VeteranFilterTypes.None && Criteria.JobSeekerType.IsNotNull())
			{
				if (Criteria.JobSeekerType == VeteranFilterTypes.Veteran) query = query.Where(x => x.IsVeteran.Equals(true));
				if (Criteria.JobSeekerType == VeteranFilterTypes.NonVeteran) query = query.Where(x => x.IsVeteran.Equals(false) || x.IsVeteran.Equals(null));
			}

			if (_settings.OfficesEnabled && Criteria.OfficeIds.IsNotNullOrEmpty() && Criteria.OfficeIds.Count > 0)
			{
				var officeIds = Criteria.OfficeIds.ToArray();

				query = _settings.ReferralRequestApprovalQueueOfficeFilterType
					? query.Join(Repository.Jobs.Where(j => j.JobOfficeMappers.Any(jom => officeIds.Contains(jom.OfficeId))), q => q.JobId, j => j.Id, (q, j) => q)
                    : query.Join(Repository.Persons.Where(p => p.PersonOfficeMappers.Any(pom => officeIds.Contains(pom.OfficeId))), q => q.PersonId, p => p.Id, (q, p) => q);

				//query = query.Where(q => Repository.PersonOfficeMappers.Where(pom => Criteria.OfficeIds.Contains(pom.OfficeId)).Select(
				//	pom => pom.PersonId).Distinct().Contains(q.PersonId));
			}

			if (Criteria.StaffMemberId.IsNotNull() && Criteria.StaffMemberId > 0)
				query = query.Where(x => x.AssignedToId == Criteria.StaffMemberId);

			// Ordering by TimeInQueue and Location (town, state) has to be done after the query has been executed in the service method as they are populated after the query runs
			if (Criteria.OrderBy.IsNotNullOrEmpty() && Criteria.OrderBy.Trim().IsNotNullOrEmpty() && !Criteria.OrderBy.Contains("timeinqueue") && !Criteria.OrderBy.Contains("town"))
				query = query.OrderBy(Criteria.OrderBy);

			if (Criteria.OrderBy.IsNullOrEmpty())
			{
				query = query.OrderBy(x => x.ReferralDate).ThenBy(x => x.Name);
			}
			else if (Criteria.OrderBy.Contains("timeinqueue"))
			{
				if (Criteria.OrderBy.Contains("asc"))
				{
					query = query.OrderByDescending(x => x.ReferralDate);
				}
				else if (Criteria.OrderBy.Contains("desc"))
				{
					query = query.OrderBy(x => x.ReferralDate);
				}
			}
			else if (Criteria.OrderBy.Contains("jobseekertown"))
			{
				if (Criteria.OrderBy.Contains("asc"))
				{
					query = query.OrderBy(x => x.JobseekerTown).ThenBy(x => x.JobseekerStateId);
				}
				else if (Criteria.OrderBy.Contains("desc"))
				{
					query = query.OrderByDescending(x => x.JobseekerTown).ThenByDescending(x => x.JobseekerStateId);
				}
			}
			else if (Criteria.OrderBy.Contains("joblocation"))
			{
				if (Criteria.OrderBy.Contains("asc"))
				{
					query = query.OrderBy(x => x.JobLocation);
				}
				else if (Criteria.OrderBy.Contains("desc"))
				{
					query = query.OrderByDescending(x => x.JobLocation);
				}
			}
			else if (Criteria.OrderBy.Contains("town"))
			{
				if (Criteria.OrderBy.Contains("asc"))
				{
					query = query.OrderBy(x => x.Town).ThenBy(x => x.StateId);
				}
				else if (Criteria.OrderBy.Contains("desc"))
				{
					query = query.OrderByDescending(x => x.Town).ThenByDescending(x => x.StateId);
				}
			}
			return query;
		}

	}
}
