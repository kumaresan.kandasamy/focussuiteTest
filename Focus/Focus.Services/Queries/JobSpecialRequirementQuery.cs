﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Linq;
using Focus.Core.Criteria;
using Focus.Core.Criteria.JobSpecialRequirement;
using Focus.Data.Core.Entities;
using Focus.Data.Repositories.Contracts;
using Framework.Core;

#endregion



namespace Focus.Services.Queries
{
	public class JobSpecialRequirementQuery : QueryBase <JobSpecialRequirement, ICoreRepository, JobSpecialRequirementCriteria>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="JobQuery"/> class.
		/// </summary>
		/// <param name="repository">The repository.</param>
		/// <param name="criteria">The criteria.</param>
		public JobSpecialRequirementQuery(ICoreRepository repository, JobSpecialRequirementCriteria criteria) : base(repository, criteria) {}

		/// <summary>
		/// Produces a query for this type.
		/// </summary>
		/// <returns></returns>
		public override IQueryable<JobSpecialRequirement> Query()
		{
			var query = Repository.JobSpecialRequirements;

			if (Criteria.OrderBy.IsNotNullOrEmpty() && Criteria.OrderBy.Trim().IsNotNullOrEmpty())
				query = query.OrderBy(Criteria.OrderBy);

			if (Criteria.FetchOption != CriteriaBase.FetchOptions.Single)
			{
				if (Criteria.JobId.IsNotNull())
				{
					query = from q in query
					        where q.JobId == Criteria.JobId
					        select q;
				}
			}

			return query;
		}

		/// <summary>
		/// </summary>
		/// <returns></returns>
		public override JobSpecialRequirement QueryEntityId()
		{
			return Repository.FindById<JobSpecialRequirement>(Criteria.SpecialRequirementId);
		}
	}
}