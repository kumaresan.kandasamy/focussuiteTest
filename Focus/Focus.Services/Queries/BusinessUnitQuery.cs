﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Linq;
using Focus.Data.Repositories.Contracts;
using Framework.Core;

using Focus.Core.Criteria;
using Focus.Data.Core.Entities;
using Focus.Core.Criteria.BusinessUnit;

#endregion

namespace Focus.Services.Queries
{
	public class BusinessUnitQuery : QueryBase <BusinessUnit, ICoreRepository, BusinessUnitCriteria>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="BusinessUnitQuery"/> class.
		/// </summary>
		/// <param name="repository">The repository.</param>
		/// <param name="criteria">The criteria.</param>
		public BusinessUnitQuery(ICoreRepository repository, BusinessUnitCriteria criteria) : base(repository, criteria) {}

		/// <summary>
		/// Produces a query for this type.
		/// </summary>
		/// <returns></returns>
		public override IQueryable<BusinessUnit> Query()
		{
			var query = Repository.BusinessUnits;

			if (Criteria.EmployerId.IsNotNull())
				query = query.Where(x => x.EmployerId.Equals(Criteria.EmployerId));

			if (Criteria.OrderBy.IsNotNullOrEmpty() && Criteria.OrderBy.Trim().IsNotNullOrEmpty())
				query = query.OrderBy(Criteria.OrderBy);

			if (Criteria.FetchOption == CriteriaBase.FetchOptions.Single)
			{
				if (Criteria.BusinessUnitId.IsNotNull())
					query = query.Where(x => x.Id == Criteria.BusinessUnitId);

				if (Criteria.BusinessUnitName.IsNotNullOrEmpty())
					query = query.Where(x => x.Name == Criteria.BusinessUnitName);

				if (Criteria.IsPreferred.IsNotNull())
					query = query.Where(x => x.IsPreferred);

				return query;
			}

			if (Criteria.EmployerId.IsNotNull())
			{
				query = from q in query
						join e in Repository.Employers on q.EmployerId equals e.Id
						where e.Id == Criteria.EmployerId
						select q;
			}
			
			return query.OrderBy(x => x.Name);
		}

		/// <summary>
		/// Queries the entity id.
		/// </summary>
		/// <returns></returns>
		public override BusinessUnit QueryEntityId()
		{
			return Repository.FindById<BusinessUnit>(Criteria.BusinessUnitId);
		}
	}
}
