﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;

using Focus.Core.Criteria.CandidateApplication;
using Focus.Core.Views;
using Focus.Data.Core.Entities;
using Focus.Data.Repositories.Contracts;

using Framework.Core;

#endregion

namespace Focus.Services.Queries
{
	public class ResumeViewQuery : QueryBase <ResumeView, ICoreRepository, ResumeViewCriteria>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="ResumeViewQuery"/> class.
		/// </summary>
		/// <param name="repository">The repository.</param>
		/// <param name="criteria">The criteria.</param>
		public ResumeViewQuery(ICoreRepository repository, ResumeViewCriteria criteria) : base(repository, criteria) {}

		/// <summary>
		/// Produces a query for this type.
		/// </summary>
		/// <returns></returns>
		public override IQueryable<ResumeView> Query()
		{
			return Query(false, false);
		}

		/// <summary>
		/// Produces a query for this type.
		/// </summary>
		/// <returns></returns>
		public IQueryable<ResumeView> Query(bool checkVeteran, bool doSort)
		{
			var query = Repository.ApplicationViews;

			if (Criteria.ApplicationId.HasValue)
				query = query.Where(x => x.Id == Criteria.ApplicationId);

			if (Criteria.JobId.HasValue)
				query = query.Where(x => x.JobId == Criteria.JobId);

			// to support searching on a single approval status
			if (Criteria.ApprovalStatus.HasValue)
				query = query.Where(x => x.CandidateApplicationApprovalStatus == Criteria.ApprovalStatus);

			if (Criteria.ApprovalStatusList != null && Criteria.ApprovalStatusList.Count() != 0)
			{
				// filter on the approval statuses requested
				//query = Criteria.ApprovalStatusList.Aggregate(query, (current, item) => current.Where(x => x.CandidateApplicationApprovalStatus == item));
				//foreach (ApprovalStatuses item in Criteria.ApprovalStatusList)
				//{
				//  query = query.Where(x => x.CandidateApplicationApprovalStatus == item);
				//}

				query = query.Where(x => Criteria.ApprovalStatusList.Contains(x.CandidateApplicationApprovalStatus));
			}

			if (checkVeteran && Criteria.ExcludeNonVeterans)
			{
				query = query.Where(candidate => candidate.CandidateIsVeteran == true || candidate.VeteranPriorityEndDate == null || candidate.VeteranPriorityEndDate <= DateTime.Now);
			}

			#region Sort

			if (doSort)
			{
				var orderBy = (Criteria.OrderBy.IsNotNullOrEmpty()) ? Criteria.OrderBy : "PoolResultSortBys.Score";

				switch (orderBy.ToLower())
				{
					case "poolresultsortbys.status":
						query = query.OrderBy(x => x.CandidateApplicationStatus).ThenByDescending(x => x.CandidateIsVeteran).ThenByDescending(x => x.CandidateApplicationScore);
						break;
					case "poolresultsortbys.applicationreceivedon":
						query = query.OrderByDescending(x => x.CandidateApplicationReceivedOn).ThenByDescending(x => x.CandidateIsVeteran).ThenByDescending(x => x.CandidateApplicationScore);
						break;
					default:
						query = query.OrderByDescending(x => x.StarRating).ThenByDescending(x => x.CandidateIsVeteran).ThenByDescending(x => x.CandidateApplicationScore);
						break;
				}
			}

			#endregion

			return query.Select(x => new ResumeView
																							{
																								Id = x.CandidateId,
																								Score = x.CandidateApplicationScore,
																								Name = x.CandidateFirstName + " " + x.CandidateLastName,
																								IsVeteran = (x.CandidateIsVeteran.HasValue) && x.CandidateIsVeteran.Value,
																								YearsExperience = (x.CandidateYearsExperience.HasValue) ? x.CandidateYearsExperience.Value : 0,
																								Status = x.CandidateApplicationStatus,
																								ApplicationReceivedOn = x.CandidateApplicationReceivedOn,
																								ApplicationApprovalStatus = x.CandidateApplicationApprovalStatus,
                                                ApplicationVeteranPriorityEndDate = x.VeteranPriorityEndDate,
																								ContactInfoVisible = x.CandidateIsContactInfoVisible,
                                                ApplicationId = x.Id, 
																								Branding = x.Branding,
																								NcrcLevelId = x.CandidateNcrcLevelId,
																								ApplicationLockVersion = x.LockVersion,
																								StarRating = x.StarRating
																							});
		}

		/// <summary>
		/// Queries the entity id.
		/// </summary>
		/// <returns></returns>
		public override ResumeView QueryEntityId()
		{
			var applicationView = Repository.FindById<ApplicationView>(Criteria.ApplicationId);

			return new ResumeView
			       	{
			       		Id = applicationView.CandidateId,
								Score = applicationView.CandidateApplicationScore,
			       		Name = String.Format("{0} {1}", applicationView.CandidateFirstName, applicationView.CandidateLastName),
			       		IsVeteran = (applicationView.CandidateIsVeteran.HasValue) && applicationView.CandidateIsVeteran.Value,
			       		YearsExperience = (applicationView.CandidateYearsExperience.HasValue) ? applicationView.CandidateYearsExperience.Value : 0,
								Status = applicationView.CandidateApplicationStatus,
								ApplicationReceivedOn = applicationView.CandidateApplicationReceivedOn,
								ApplicationApprovalStatus = applicationView.CandidateApplicationApprovalStatus
			       	};
		}
	}
}
