﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Linq;
using Focus.Core;
using Focus.Core.Criteria.Job;
using Focus.Data.Core.Entities;
using Focus.Data.Repositories.Contracts;
using Framework.Core;

#endregion

namespace Focus.Services.Queries
{
	public class JobPostingReferralViewQuery : QueryBase <JobPostingReferralView, ICoreRepository, JobPostingReferralCriteria>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="JobPostingReferralViewQuery"/> class.
		/// </summary>
		/// <param name="repository">The repository.</param>
		/// <param name="criteria">The criteria.</param>
		public JobPostingReferralViewQuery(ICoreRepository repository, JobPostingReferralCriteria criteria) : base(repository, criteria) {}

		/// <summary>
		/// Queries this instance.
		/// </summary>
		/// <returns></returns>
		public override IQueryable<JobPostingReferralView> Query()
		{
			var query = (from r in Repository.JobPostingReferralViews select r);

			#region Job type

			if (Criteria.JobType.IsNotNull() && Criteria.JobType != JobListFilter.None)
			{
				switch (Criteria.JobType)
				{
					case JobListFilter.CourtOrderedAffirmativeActionJobs:
						query = query.Where(x => x.CourtOrderedAffirmativeAction == true);
						break;

					case JobListFilter.FederalContractorJobs:
						query = query.Where(x => x.FederalContractor == true);
						break;

					case JobListFilter.ForeignLaborJobsH2A:
						query = query.Where(x => x.ForeignLabourCertificationH2A == true);
						break;

					case JobListFilter.ForeignLaborJobsH2B:
						query = query.Where(x => x.ForeignLabourCertificationH2B == true);
						break;

					case JobListFilter.ForeignLaborJobsOther:
						query = query.Where(x => x.ForeignLabourCertificationOther == true);
						break;

					case JobListFilter.CommissionOnly:
						query = query.Where(x => x.IsCommissionBased == true);
						break;

					case JobListFilter.SalaryAndCommission:
						query = query.Where(x => x.IsSalaryAndCommissionBased == true);
						break;

					case JobListFilter.HomeBased:
						query = query.Where(x => x.SuitableForHomeWorker == true);
						break;

					case JobListFilter.NoFixedLocation:
						query = query.Where(x => x.JobLocationType.Equals(JobLocationTypes.NoFixedLocation));
						break;
				}
			}

			if (Criteria.ApprovalStatus.HasValue)
			{
				query = query.Where(x => x.ApprovalStatus.Equals(Criteria.ApprovalStatus));
			}

			#endregion

			#region Office

			if (Criteria.OfficeIds.IsNotNull() && Criteria.OfficeIds.Count > 0)
			{
				var officeIds = Criteria.OfficeIds.ToArray();
				query = query.Join(Repository.Jobs.Where(j => j.JobOfficeMappers.Any(jom => officeIds.Contains(jom.OfficeId))), r => r.Id, j => j.Id, (r, j) => r);

				//query = query.Where(x => Repository.JobOfficeMappers.Any(jom => jom.JobId == x.Id && offices.Contains(jom.OfficeId)));
			}

			#endregion

			#region Staff member

			if (Criteria.StaffMemberId.IsNotNull())
				query = query.Where(x => x.AssignedToId == Criteria.StaffMemberId);

			#endregion

			#region Ordering

			if (Criteria.OrderBy.IsNotNullOrEmpty() && Criteria.OrderBy.Trim().IsNotNullOrEmpty())
				query = query.OrderBy(Criteria.OrderBy);
			else
				query = query.OrderByDescending(x => x.TimeInQueue).ThenBy(x => x.JobTitle);

			#endregion

			return query;
		}
	}
}
