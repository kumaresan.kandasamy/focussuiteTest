﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using Focus.Core;
using Focus.Core.Criteria.Application;
using Focus.Core.Criteria.CandidateApplication;
using Focus.Data.Core.Entities;
using Focus.Data.Repositories.Contracts;
using Framework.Core;

#endregion

namespace Focus.Services.Queries
{
	public class ApplicationQuery : QueryBase<Application, ICoreRepository, ApplicationCriteria>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="ApplicationQuery"/> class.
		/// </summary>
		/// <param name="repository">The repository.</param>
		/// <param name="criteria">The criteria.</param>
		public ApplicationQuery(ICoreRepository repository, ApplicationCriteria criteria) : base(repository, criteria) {}

		/// <summary>
		/// Produces a query for this type.
		/// </summary>
		/// <returns></returns>
		public override IQueryable<Application> Query()
		{
			var query = Repository.Applications;

			if (!String.IsNullOrEmpty(Criteria.SeekerEmail)) query = query.Where(a => a.Resume.Person.EmailAddress == Criteria.SeekerEmail);

			if (!String.IsNullOrEmpty(Criteria.ResumeName)) query = query.Where(a => a.Resume.ResumeName == Criteria.ResumeName);

			if (!String.IsNullOrEmpty(Criteria.JobTitle)) query = query.Where(a => a.Posting.JobTitle == Criteria.JobTitle);

			if (!String.IsNullOrEmpty(Criteria.BusinessUnitName)) query = query.Where(a => a.Posting.EmployerName == Criteria.BusinessUnitName);

			return query;
		}
	}
}
