﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using Focus.Core;
using Focus.Core.Criteria.CandidateApplication;
using Focus.Data.Core.Entities;
using Focus.Data.Repositories.Contracts;
using Framework.Core;

#endregion

namespace Focus.Services.Queries
{
	public class ApplicationStatusLogViewQuery : QueryBase<ApplicationStatusLogView, ICoreRepository, ApplicationStatusLogViewCriteria>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="ApplicationStatusLogViewQuery"/> class.
		/// </summary>
		/// <param name="repository">The repository.</param>
		/// <param name="criteria">The criteria.</param>
		public ApplicationStatusLogViewQuery(ICoreRepository repository, ApplicationStatusLogViewCriteria criteria) : base(repository, criteria) {}

		/// <summary>
		/// Produces a query for this type.
		/// </summary>
		/// <returns></returns>
		public override IQueryable<ApplicationStatusLogView> Query()
		{
			var query = Repository.ApplicationStatusLogViews;

			if (Criteria.JobStatus.IsNotNull())
				query = query.Where(x => x.JobStatus == Criteria.JobStatus);

			/*
				query = from view in query
				        join j in Repository.Jobs on view.JobId equals j.Id
				        where j.JobStatus == Criteria.JobStatus
				        select view;
			*/

			if (Criteria.Id.HasValue)
				query = query.Where(x => x.Id == Criteria.Id);

			if (Criteria.BusinessUnitId.HasValue)
				query = query.Where(x => x.BusinessUnitId == Criteria.BusinessUnitId);
			
			if (Criteria.ApplicationId.HasValue)
				query = query.Where(x => x.CandidateApplicationId == Criteria.ApplicationId);

			if (Criteria.JobId.HasValue)
				query = query.Where(x => x.JobId == Criteria.JobId);

			if (Criteria.DaysSinceStatusChange.HasValue)
				query = query.Where(x => x.ActionedOn > DateTime.Now.AddDays(Criteria.DaysSinceStatusChange.Value > 0 ? (Criteria.DaysSinceStatusChange.Value*-1) : -30));

		  if (Criteria.ApplicationStatus.HasValue)
				query = query.Where(x => x.CandidateApplicationStatus == (long)Criteria.ApplicationStatus.Value);

      if (Criteria.IgnoreSameStatuses.GetValueOrDefault(false))
        query = query.Where(x => x.CandidateApplicationStatus != x.CandidateOriginalApplicationStatus);

			if (Criteria.ApprovalStatuses.IsNotNullOrEmpty())
				query = query.Where(x => Criteria.ApprovalStatuses.Contains(x.CandidateApprovalStatus));

            //Ignoring the Application Status Types SelfReferred(1), Interviewed(7), OfferMade(10), ReconsiderReferral(21) based on Gails's comments  "Display only the items from the drop-down when populating the Referral Outcome column and remove code that populates the Referral Outcome column with “Self-referred” or “Staff referred"
            query = query.Where(x => x.CandidateApplicationStatus != 1 && x.CandidateApplicationStatus != 7 && x.CandidateApplicationStatus != 21 && x.CandidateApplicationStatus != 10);

		  #region Sorting

			var orderBy = (Criteria.OrderBy.IsNotNullOrEmpty()) ? Criteria.OrderBy : Constants.SortOrders.DateReceivedDesc;

			switch (orderBy.ToLower())
			{
				case Constants.SortOrders.DateReceivedAsc:
					query = query.OrderBy(x => x.ActionedOn);
					break;
				case Constants.SortOrders.DateReceivedDesc:
					query = query.OrderByDescending(x => x.ActionedOn);
					break;
				case Constants.SortOrders.ScoreAsc:
					query = query.OrderBy(x => x.CandidateApplicationScore);
					break;
				case Constants.SortOrders.ScoreDesc:
					query = query.OrderByDescending(x => x.CandidateApplicationScore);
					break;
				case Constants.SortOrders.NameAsc:
					query = query.OrderBy(x => x.CandidateFirstName).ThenBy(x => x.CandidateLastName);
					break;
				case Constants.SortOrders.NameDesc:
					query = query.OrderByDescending(x => x.CandidateFirstName).ThenByDescending(x => x.CandidateLastName);
					break;
				case Constants.SortOrders.YearsExperienceAsc:
					query = query.OrderBy(x => x.CandidateYearsExperience);
					break;
				case Constants.SortOrders.YearsExperienceDesc:
					query = query.OrderByDescending(x => x.CandidateYearsExperience);
					break;
				case Constants.SortOrders.StatusAsc:
					query = query.OrderBy(x => x.CandidateApplicationStatus);
					break;
				case Constants.SortOrders.StatusDesc:
					query = query.OrderByDescending(x => x.CandidateApplicationStatus);
					break;
				case Constants.SortOrders.JobTitleAsc:
					query = query.OrderBy(x => x.JobTitle);
					break;
				case Constants.SortOrders.JobTitleDesc:
					query = query.OrderByDescending(x => x.JobTitle);
					break;
        case Constants.SortOrders.StudyProgramAsc:
          query = query.OrderBy(x => x.ProgramAreaId).ThenBy(x => x.DegreeId);
          break;
        case Constants.SortOrders.StudyProgramDesc:
          query = query.OrderByDescending(x => x.ProgramAreaId).ThenByDescending(x => x.DegreeId);
          break;
        case Constants.SortOrders.EducationCompletionDateAsc:
			    query = query.OrderBy(x => x.EducationCompletionDate);
          break;
        case Constants.SortOrders.EducationCompletionDateDesc:
          query = query.OrderByDescending(x => x.EducationCompletionDate);
          break;
			}

			#endregion

			return query;
		}

		/// <summary>
		/// Queries the entity id.
		/// </summary>
		/// <returns></returns>
		public override ApplicationStatusLogView QueryEntityId()
		{
			return Repository.FindById<ApplicationStatusLogView>(Criteria.Id);
		}
	}
}
