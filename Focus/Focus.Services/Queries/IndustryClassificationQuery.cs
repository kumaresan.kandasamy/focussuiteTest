﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Linq;
using Focus.Core.Criteria.IndustryClassification;
using Focus.Data.Library.Entities;
using Focus.Data.Repositories.Contracts;
using Framework.Core;

#endregion

namespace Focus.Services.Queries
{
	public class IndustryClassificationQuery : QueryBase <NAICS, ILibraryRepository, IndustryClassificationCriteria>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="IndustryClassificationQuery"/> class.
		/// </summary>
		/// <param name="repository">The repository.</param>
		/// <param name="criteria">The criteria.</param>
		public IndustryClassificationQuery(ILibraryRepository repository, IndustryClassificationCriteria criteria) : base(repository, criteria) {}

		/// <summary>
		/// Produces a query for this type.
		/// </summary>
		/// <returns></returns>
		public override IQueryable<NAICS> Query()
		{
			var query = Repository.NAICS;

			if (Criteria.Id.HasValue)
				query = query.Where(x => x.Id == Criteria.Id);

			if(Criteria.Code.IsNotNullOrEmpty())
				query = query.Where(x => x.Code == Criteria.Code);

			if (Criteria.ParentId.HasValue)
				query = query.Where(x => x.ParentId == Criteria.ParentId);

			if (Criteria.CodeLength.HasValue)
				query = query.Where(x => x.Code.Length == Criteria.CodeLength);

			query = query.OrderBy(x => x.Name);

			return query;
		}

		public override NAICS QueryEntityId()
		{
			return Repository.FindById<NAICS>(Criteria.Id);
		}
	}
}
