﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Linq;
using Focus.Core;
using Framework.Core;

using Focus.Core.Criteria.Employer;
using Focus.Data.Repositories.Contracts;
using Focus.Data.Core.Entities;

#endregion

namespace Focus.Services.Queries
{
	public class OfficeStaffMemberQuery : QueryBase <Person, ICoreRepository, StaffMemberCriteria>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="OfficeStaffMemberQuery"/> class.
		/// </summary>
		/// <param name="repository">The repository.</param>
		/// <param name="criteria">The criteria.</param>
		public OfficeStaffMemberQuery(ICoreRepository repository, StaffMemberCriteria criteria) : base(repository, criteria) {}

		/// <summary>
		/// Produces a query for this type.
		/// </summary>
		/// <returns></returns>
		public override IQueryable<Person> Query()
		{
			IQueryable<Person> query;

			// Select distinct here because a person can be assigned to more than one office

			// Get all managers for the specified office(s)
			if (Criteria.Manager.HasValue && Criteria.OfficeIds.IsNotNullOrEmpty() && Criteria.OfficeIds.Count > 0)
			{
				query = (from u in Repository.Users
								 join p in Repository.Persons on u.PersonId equals p.Id
								 where u.UserType == UserTypes.Assist
								 && p.Manager == Criteria.Manager
								 && (Criteria.Enabled == null || u.Enabled == Criteria.Enabled)
								 && (Criteria.Blocked == null || u.Blocked == Criteria.Blocked)
								 select p);

				query = query.Where(q => Repository.PersonOfficeMappers.Where(mapper => Criteria.OfficeIds.Contains(mapper.OfficeId)).Select(
									mapper => mapper.PersonId).Distinct().Contains(q.Id));
			}
			// Get staff members that aren't statewide
			else if (Criteria.Statewide.HasValue && !(bool)Criteria.Statewide)
			{
				query = (from p in Repository.Persons
								 join u in Repository.Users
									 on p.Id equals u.PersonId
								 where u.UserType == UserTypes.Assist
									 && !p.PersonOfficeMappers.Any(pom => pom.StateId != null)
									 && (Criteria.Enabled == null || u.Enabled == Criteria.Enabled)
									 && (Criteria.Blocked == null || u.Blocked == Criteria.Blocked)
								 select p);
			}
			// Get staff members that are statewide
			else if (Criteria.Statewide.HasValue && (bool)Criteria.Statewide)
			{
				query = (from u in Repository.Users
								 join p in Repository.Persons on u.PersonId equals p.Id
								 where u.UserType == UserTypes.Assist
									 && (Criteria.Enabled == null || u.Enabled == Criteria.Enabled)
									 && (Criteria.Blocked == null || u.Blocked == Criteria.Blocked)
								 select p);

				query = query.Where(q => Repository.PersonOfficeMappers.Where(mapper => !mapper.StateId.Equals(null)).Select(
									mapper => mapper.PersonId).Distinct().Contains(q.Id));
			}
			// Get all staff members that handle work for the office
			else if (Criteria.OfficeIds.IsNotNullOrEmpty() && Criteria.OfficeIds.Count > 0)
			{
				query = (from u in Repository.Users
								 join p in Repository.Persons on u.PersonId equals p.Id
								 where u.UserType == UserTypes.Assist
									 && (Criteria.Enabled == null || u.Enabled == Criteria.Enabled)
									 && (Criteria.Blocked == null || u.Blocked == Criteria.Blocked)
								 select p);

				query = query.Where(q => Repository.PersonOfficeMappers.Where(mapper => Criteria.OfficeIds.Contains(mapper.OfficeId)).Select(
									mapper => mapper.PersonId).Distinct().Contains(q.Id));
			}
			//Get all staff members
			else
				query = (from u in Repository.Users
								 join p in Repository.Persons on u.PersonId equals p.Id
								 where u.UserType == UserTypes.Assist
									 && (Criteria.Enabled == null || u.Enabled == Criteria.Enabled)
									 && (Criteria.Blocked == null || u.Blocked == Criteria.Blocked)
								 select p).Distinct();

			if (Criteria.OrderBy.IsNotNullOrEmpty() && Criteria.OrderBy.Trim().IsNotNullOrEmpty())
				query = query.OrderBy(Criteria.OrderBy);
			else
				query = query.OrderBy(x => x.LastName);

			return query;
		}

		/// <summary>
		/// Queries the entity id.
		/// </summary>
		/// <returns></returns>
		public override Person QueryEntityId()
		{
			return Repository.FindById<Person>(Criteria.PersonId);
		}
	}
}
