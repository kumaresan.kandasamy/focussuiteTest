﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Linq;
using Focus.Core;
using Focus.Core.Criteria.Resume;
using Focus.Data.Core.Entities;
using Focus.Data.Repositories.Contracts;
using Framework.Core;

#endregion

namespace Focus.Services.Queries
{
	public class ResumeQuery : QueryBase <Resume, ICoreRepository, ResumeCriteria>
	{
		public ResumeQuery(ICoreRepository repository, ResumeCriteria criteria) : base(repository, criteria) {}

		/// <summary>
		/// Produces a query for this type.
		/// </summary>
		/// <returns></returns>
		public override IQueryable<Resume> Query()
		{
			var query = Repository.Resumes;

			if (Criteria.Id.HasValue)
				query = query.Where(x => x.Id == Criteria.Id);

			// Only get the Active resumes when retrieving for a person
			if (Criteria.PersonId.HasValue)
				query = query.Where((x => x.PersonId == Criteria.PersonId && x.StatusId == ResumeStatuses.Active));

			if (Criteria.ResumeName.IsNotNullOrEmpty())
				query = query.Where(x => x.ResumeName == Criteria.ResumeName);

			return query;
		}

		/// <summary>
		/// Queries the entity id.
		/// </summary>
		/// <returns></returns>
		public override Resume QueryEntityId()
		{
			return Repository.FindById<Resume>(Criteria.Id);
		}
	}
}

