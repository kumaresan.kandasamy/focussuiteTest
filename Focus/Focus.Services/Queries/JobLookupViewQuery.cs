﻿using System.Linq;
using Focus.Core.Views;
using Focus.Data.Core.Entities;
using Focus.Core.Criteria.Job;
using Focus.Data.Repositories.Contracts;
using Framework.Core;


namespace Focus.Services.Queries
{
	public class JobLookupViewQuery : IEntityQuery<JobLookupView>
	{
		private readonly JobLookupCriteria _criteria;
		private readonly ICoreRepository _repository;

		/// <summary>
		/// Initializes a new instance of the <see cref="JobLookupViewQuery"/> class.
		/// </summary>
		/// <param name="repository">The repository.</param>
		/// <param name="criteria">The criteria.</param>
		public JobLookupViewQuery(ICoreRepository repository, JobLookupCriteria criteria)
		{
			_repository = repository;
			_criteria = criteria;
		}

		/// <summary>
		/// Queries this instance.
		/// </summary>
		/// <returns></returns>
		public IQueryable<JobLookupView> Query()
		{
			var query = _repository.Jobs;

			if (_criteria.JobId.HasValue)
				query = query.Where(x => x.Id == _criteria.JobId.Value);

			if(_criteria.JobTitle.IsNotNullOrEmpty())
				query = query.Where(x => x.JobTitle.Contains(_criteria.JobTitle));

			if (_criteria.EmployerName.IsNotNullOrEmpty())
				query = query.Where(x => x.Employer.Name.Contains(_criteria.EmployerName));

			if (_criteria.CreatedFrom.HasValue)
				query = query.Where(x => x.CreatedOn >= _criteria.CreatedFrom.Value);

			if (_criteria.CreatedTo.HasValue)
				query = query.Where(x => x.CreatedOn <= _criteria.CreatedTo.Value);

			if (_criteria.JobStatus.HasValue)
				query = query.Where(x => x.JobStatus == _criteria.JobStatus.Value);

			return query.Select(x => new JobLookupView
			                         	{
			                         		Employer = x.Employer.Name,
			                         		JobId = x.Id,
			                         		JobTitle = x.JobTitle
			                         	});
		}

		/// <summary>
		/// Queries the entity id.
		/// </summary>
		/// <returns></returns>
		public JobLookupView QueryEntityId()
		{
			var job = _repository.FindById<Job>(_criteria.JobId);
			return new JobLookupView
			       	{
			       		Employer = job.Employer.Name,
			       		JobId = job.Id,
			       		JobTitle = job.JobTitle
			       	};
		}
	}
}
