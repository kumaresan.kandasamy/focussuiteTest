﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Linq;
using Focus.Core.Criteria;
using Focus.Data.Repositories.Contracts;
using Mindscape.LightSpeed;

#endregion

namespace Focus.Services.Queries
{
	public abstract class QueryBase<T,R,C> : IEntityQuery<T> where T : class where R : Data.Repositories.Contracts.IRepository where C : CriteriaBase
	{
		protected R Repository { get; private set; }

		protected C Criteria { get; private set; }

		/// <summary>
		/// Gets or sets the index of the page.
		/// </summary>
		public int PageIndex { get; private set; }

		/// <summary>
		/// Gets or sets the size of the page.
		/// </summary>
		public int PageSize { get; private set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="QueryBase{T, R, C}"/> class.
		/// </summary>
		/// <param name="repository">The repository.</param>
		/// <param name="criteria">The criteria.</param>
		protected QueryBase(R repository, C criteria)
		{
			Repository = repository;
			Criteria = criteria;
			PageIndex = criteria.PageIndex;
			PageSize = criteria.PageSize;
		}

		public abstract IQueryable<T> Query();

		public virtual T QueryEntityId()
		{
			throw new System.NotImplementedException();
		}
	}
}
