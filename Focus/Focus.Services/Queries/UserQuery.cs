﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Linq;
using Focus.Data.Core.Entities;
using Focus.Core.Criteria.User;
using Focus.Data.Repositories.Contracts;
using Framework.Core;

#endregion

namespace Focus.Services.Queries
{

	public class UserQuery : QueryBase<User, ICoreRepository, UserCriteria>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="UserQuery"/> class.
		/// </summary>
		/// <param name="repository">The repository.</param>
		/// <param name="criteria">The criteria.</param>
		public UserQuery(ICoreRepository repository, UserCriteria criteria) : base(repository, criteria) {}

		/// <summary>
		/// Produces a query for this type.
		/// </summary>
		/// <returns></returns>
		public override IQueryable<User> Query()
		{
			var query = Repository.Users;

			if (Criteria.FirstName.IsNotNullOrEmpty())
				query = query.Where(x => x.Person.FirstName == Criteria.FirstName);

			if (Criteria.LastName.IsNotNullOrEmpty())
				query = query.Where(x => x.Person.LastName == Criteria.LastName);

			if (Criteria.ExternalOffice.IsNotNullOrEmpty())
				query = (from q in query
				         join p in Repository.Persons on q.PersonId equals p.Id
								 where p.ExternalOffice == Criteria.ExternalOffice
				         select q);

			if (Criteria.EmailAddress.IsNotNullOrEmpty())
				query = (from q in query
								 join p in Repository.Persons on q.PersonId equals p.Id
								 where p.EmailAddress == Criteria.EmailAddress
								 select q);

			if (Criteria.UserName.IsNotNullOrEmpty())
				query = query.Where(x => x.UserName == Criteria.UserName);

			if (Criteria.UserRole.IsNotNullOrEmpty())
				query = (from q in query
				         join ur in Repository.UserRoles on q.Id equals ur.UserId
				         join r in Repository.Roles on ur.RoleId equals r.Id
				         where r.Key == Criteria.UserRole
				         select q);

			if (Criteria.Enabled.IsNotNull())
				query = query.Where(x => x.Enabled == Criteria.Enabled);

			if (Criteria.OrderBy.IsNotNullOrEmpty() && Criteria.OrderBy.Trim().IsNotNullOrEmpty())
				query = query.OrderBy(Criteria.OrderBy);

			return query;
		}

		/// <summary>
		/// Queries the entity id.
		/// </summary>
		/// <returns></returns>
		public override User QueryEntityId()
		{
			return Repository.FindById<User>(Criteria.UserId);
		}
	}
}
