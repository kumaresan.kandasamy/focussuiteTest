﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using Focus.Core;
using Focus.Services.Core;
using Framework.Core;

using Focus.Core.Criteria;
using Focus.Data.Core.Entities;
using Focus.Core.Criteria.Employer;
using Focus.Data.Repositories.Contracts;

#endregion

namespace Focus.Services.Queries
{
	public class OfficeQuery : QueryBase <Office, ICoreRepository, OfficeCriteria>
	{

	  /// <summary>
	  /// Initializes a new instance of the <see cref="OfficeQuery"/> class.
	  /// </summary>
	  /// <param name="repository">The repository.</param>
	  /// <param name="criteria">The criteria.</param>
	  /// <param name="settings">The settings</param>
    public OfficeQuery(ICoreRepository repository, OfficeCriteria criteria) : base(repository, criteria)
    {

    }

		/// <summary>
		/// Produces a query for this type.
		/// </summary>
		/// <returns></returns>
		public override IQueryable<Office> Query()
		{
			var query = (from o in Repository.Offices
									 select o);

      if (Criteria.DefaultOffice.HasValue)
      {
        if (Criteria.DefaultType == OfficeDefaultType.None)
          throw new Exception("Invalid default office type");

        var defaultType = Criteria.DefaultType ?? OfficeDefaultType.All;

        query = Criteria.DefaultOffice.Value
                  ? query.Where(x => (x.DefaultType & defaultType) != 0)
                  : query.Where(x => (x.DefaultType & defaultType) == 0);
      }

		  if (!string.IsNullOrEmpty(Criteria.OfficeName)) query = query.Where(o => o.OfficeName == Criteria.OfficeName);

			if (Criteria.FetchOption == CriteriaBase.FetchOptions.Single)
			{
				if (Criteria.OfficeId.IsNotNull())
					query = query.Where(x => x.Id == Criteria.OfficeId);

				return query;
			}

			if (Criteria.FetchOption == CriteriaBase.FetchOptions.List || Criteria.FetchOption == CriteriaBase.FetchOptions.PagedList)
			{
				
				if(Criteria.InActive.IsNotNull())
				{
					query = from q in query where q.InActive == Criteria.InActive select q;
				}

				if (Criteria.PersonId.IsNotNull())
				{
					// For getting a person's offices where a person handles work statewide there won't be any office Ids assigned in POM, there will just be a StateId
					var personManagesState = (from pom in Repository.PersonOfficeMappers
																		where pom.PersonId == Criteria.PersonId &&
																					pom.StateId != null
																		select pom).Any();

          if (personManagesState)
					{
            if (Criteria.ReturnNoOfficesForStatewide.GetValueOrDefault(false))
            {
              query = new List<Office>().AsQueryable();
            }
					}
					else
					{
						query = from q in query
										join pom in Repository.PersonOfficeMappers on q.Id equals pom.OfficeId
										where pom.PersonId == Criteria.PersonId
										select q;
					}
				}
				else if (Criteria.StateId.IsNotNull() && Criteria.StateId > 0)
				{
					query = from q in query
									join pom in Repository.PersonOfficeMappers on q.Id equals pom.OfficeId
									where pom.StateId == Criteria.StateId
									select q;
				}
				else if (Criteria.JobId.IsNotNull())
				{
					query = from q in query
									join jom in Repository.JobOfficeMappers on q.Id equals jom.OfficeId
									where jom.JobId == Criteria.JobId
									select q;
				}
				else if (Criteria.EmployerId.IsNotNull())
				{
					query = from q in query
									join eom in Repository.EmployerOfficeMappers on q.Id equals eom.OfficeId
									where eom.EmployerId == Criteria.EmployerId
									select q;
				}
			}

			if (Criteria.OrderBy.IsNotNullOrEmpty() && Criteria.OrderBy.Trim().IsNotNullOrEmpty())
				query = query.OrderBy(Criteria.OrderBy);
			else
				query = query.OrderBy(x => x.OfficeName);

			return query;
		}
	}
}
