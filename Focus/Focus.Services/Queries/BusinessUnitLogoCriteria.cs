﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Linq;
using Focus.Core.Criteria.BusinessUnitLogo;
using Focus.Data.Core.Entities;
using Focus.Data.Repositories.Contracts;
using System.Collections.Generic;

#endregion


namespace Focus.Services.Queries
{
	public class BusinessUnitLogoQuery : QueryBase <BusinessUnitLogo, ICoreRepository, BusinessUnitLogoCriteria>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="BusinessUnitLogoQuery"/> class.
		/// </summary>
		/// <param name="repository">The repository.</param>
		/// <param name="criteria">The criteria.</param>
		public BusinessUnitLogoQuery(ICoreRepository repository, BusinessUnitLogoCriteria criteria) : base(repository, criteria) {}

		/// <summary>
		/// Produces a query for this type.
		/// </summary>
		/// <returns></returns>
		public override IQueryable<BusinessUnitLogo> Query()
		{
			var query = Repository.BusinessUnitLogos;

			if (Criteria.Id.HasValue)
				query = query.Where(x => x.Id == Criteria.Id);

			if (Criteria.BusinessUnitId.HasValue)
				query = query.Where(x => x.BusinessUnitId == Criteria.BusinessUnitId);

			if (Criteria.AssociatedWithJob.HasValue)
			{
				if (Criteria.Id.HasValue || Criteria.BusinessUnitId.HasValue)
				{
					List<long?> logosInJobs = new List<long?>();

					if (Criteria.Id.HasValue)
						logosInJobs = Repository.Jobs.Where(x => x.BusinessUnitLogoId == Criteria.Id).Select(x => x.BusinessUnitLogoId).Distinct().ToList();
					else
						logosInJobs = Repository.Jobs.Where(x => x.BusinessUnitId == Criteria.BusinessUnitId).Select(x => x.BusinessUnitLogoId).Distinct().ToList();

					if (logosInJobs.Count > 0)
						query = Criteria.AssociatedWithJob.Value ? query.Where(x => logosInJobs.Contains(x.Id)) : query.Where(x => !logosInJobs.Contains(x.Id));
				}
			}

			return query.OrderBy(x => x.Name);
		}

		/// <summary>
		/// Queries the entity id.
		/// </summary>
		/// <returns></returns>
		public override BusinessUnitLogo QueryEntityId()
		{
			return Repository.BusinessUnitLogos.SingleOrDefault(x => x.Id == Criteria.Id);
		}
	}
}

