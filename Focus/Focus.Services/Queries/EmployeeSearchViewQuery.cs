﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;

using Focus.Core;
using Focus.Data.Core.Entities;
using Focus.Core.Criteria.Employee;
using Focus.Data.Repositories.Contracts;
using Framework.Core;

#endregion

namespace Focus.Services.Queries
{
	public class EmployeeSearchViewQuery : QueryBase <EmployeeSearchView, ICoreRepository, EmployeeCriteria>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="EmployeeSearchViewQuery"/> class.
		/// </summary>
		/// <param name="repository">The repository.</param>
		/// <param name="criteria">The criteria.</param>
		public EmployeeSearchViewQuery(ICoreRepository repository, EmployeeCriteria criteria): base(repository, criteria) {}

		/// <summary>
		/// Produces a query for this type.
		/// </summary>
		/// <returns></returns>
		public override IQueryable<EmployeeSearchView> Query()
		{
			// For initial release we are only concerned with EmployerCharacteristicsCriteria
			var query = Repository.EmployeeSearchViews;

			#region Employee details

		  if (Criteria.Id.IsNotNull())
		  {
		    query = query.Where(x => x.EmployeeId == Criteria.Id);
		    return query;
		  }

		  if (Criteria.Firstname.IsNotNullOrEmpty())
				query = query.Where(x => x.FirstName.Contains(Criteria.Firstname));

			if (Criteria.Lastname.IsNotNullOrEmpty())
				query = query.Where(x => x.LastName.Contains(Criteria.Lastname));

			if (Criteria.EmailAddress.IsNotNullOrEmpty())
				query = query.Where(x => x.EmailAddress.Contains(Criteria.EmailAddress));

      if (Criteria.BusinessUnitId.IsNotNull())
        query = query.Where(x => x.BusinessUnitId == Criteria.BusinessUnitId);

			#endregion

			#region Employer Characteristics

			if (Criteria.EmployerCharacteristics.IsNotNull())
			{
				if(Criteria.EmployerCharacteristics.Sectors.IsNotNullOrEmpty())
				{
					query = query.Where(x => Criteria.EmployerCharacteristics.Sectors.Contains(x.IndustrialClassification));
				}

				if(Criteria.EmployerCharacteristics.FederalEmployerIdentificationNumber.IsNotNullOrEmpty())
				{
					query = query.Where(x => x.FederalEmployerIdentificationNumber.Contains(Criteria.EmployerCharacteristics.FederalEmployerIdentificationNumber));
				}

        if (Criteria.EmployerCharacteristics.FederalEmployerIdentificationNumbers.IsNotNullOrEmpty())
        {
          query = query.Where(x => Criteria.EmployerCharacteristics.FederalEmployerIdentificationNumbers.Contains(x.FederalEmployerIdentificationNumber));
        }

				if (Criteria.EmployerCharacteristics.EmployerName.IsNotNullOrEmpty())
				{
					query = query.Where(x => x.BusinessUnitName.Contains(Criteria.EmployerCharacteristics.EmployerName));
				}

				if(Criteria.EmployerCharacteristics.JobTitle.IsNotNullOrEmpty())
				{
					query = query.Where(x => x.JobTitle.Contains(Criteria.EmployerCharacteristics.JobTitle));
				}

				if(Criteria.EmployerCharacteristics.AccountCreationDate.IsNotNull())
				{
					if(Criteria.EmployerCharacteristics.AccountCreationDate.DateWithinRange.IsNotNull())
					{
						var days = 0;
						switch (Criteria.EmployerCharacteristics.AccountCreationDate.DateWithinRange)
						{
							case DateWithinRanges.LastThreeDays:
								days = -3;
								break;
							case DateWithinRanges.LastSevenDays:
								days = -7;
								break;
							case DateWithinRanges.LastThirtyDays:
								days = -30;
								break;
							case DateWithinRanges.LastSixtyDays:
								days = -60;
								break;
							case DateWithinRanges.LastNinetyDays:
								days = -90;
								break;
						}

						query = query.Where(x => x.EmployerCreatedOn >= DateTime.Today.AddDays(days));
					}

					if(Criteria.EmployerCharacteristics.AccountCreationDate.FromDate.IsNotNull())
					{
						query = query.Where(x => x.EmployerCreatedOn >= Criteria.EmployerCharacteristics.AccountCreationDate.FromDate);
					}

					if (Criteria.EmployerCharacteristics.AccountCreationDate.ToDate.IsNotNull())
					{
						query = query.Where(x => x.EmployerCreatedOn <= Criteria.EmployerCharacteristics.AccountCreationDate.ToDate);
					}
				}

				#region Job related queries (within employer characteristics)

				if (Criteria.EmployerCharacteristics.WorkOpportunitiesTaxCreditInterests.IsNotNull() && Criteria.EmployerCharacteristics.WorkOpportunitiesTaxCreditInterests != WorkOpportunitiesTaxCreditCategories.None)
				{
					query = query.Where(x => (x.WorkOpportunitiesTaxCreditHires & Criteria.EmployerCharacteristics.WorkOpportunitiesTaxCreditInterests) == Criteria.EmployerCharacteristics.WorkOpportunitiesTaxCreditInterests);
				}

				var postingFlags = JobPostingFlags.None;
				if (Criteria.EmployerCharacteristics.IsGreenJob.IsNotNull()) postingFlags = postingFlags | JobPostingFlags.GreenJob;
				if (Criteria.EmployerCharacteristics.IsITEmployer.IsNotNull()) postingFlags = postingFlags | JobPostingFlags.ITEmployer;
				if (Criteria.EmployerCharacteristics.IsHealthcareEmployer.IsNotNull()) postingFlags = postingFlags | JobPostingFlags.HealthcareEmployer;
				if (Criteria.EmployerCharacteristics.IsBiotechEmployer.IsNotNull()) postingFlags = postingFlags | JobPostingFlags.BiotechEmployer;

				if (postingFlags != JobPostingFlags.None)
				{
					query = query.Where(x => (x.PostingFlags & postingFlags) == postingFlags);
				}

				if(Criteria.EmployerCharacteristics.JobScreeningAssistanceRequested.IsNotNull())
				{
					query = query.Where(x => x.ScreeningPreferences == ScreeningPreferences.JobSeekersMustBeScreened);
				}

				#endregion
			}

			#endregion

			if (!Criteria.StatusTypes.Equals(FindEmployerStatusTypes.All))
			{
				if (Criteria.StatusTypes.Equals(FindEmployerStatusTypes.Unapproved))
					query = query.Where(x => !x.EmployeeApprovalStatus.Equals(ApprovalStatuses.Approved));

				if (Criteria.StatusTypes.Equals(FindEmployerStatusTypes.Active))
					query = query.Where(x => (x.EmployeeApprovalStatus.Equals(ApprovalStatuses.Approved) && !x.AccountBlocked.Equals(true)));

				if (Criteria.StatusTypes.Equals(FindEmployerStatusTypes.Blocked))
					query = query.Where(x => x.AccountBlocked.Equals(true));
			}

			#region Sorting

			// Apply default order by
			if (Criteria.OrderBy.IsNullOrEmpty()) Criteria.OrderBy = Constants.SortOrders.EmployerNameAsc;

			switch (Criteria.OrderBy.ToLower())
			{
        case Constants.SortOrders.BusinessUnitNameAsc:
          query = query.OrderBy(e => e.BusinessUnitName).ThenBy(l => l.LastName).ThenBy(f => f.FirstName);
          break;
        case Constants.SortOrders.BusinessUnitNameDesc:
          query = query.OrderByDescending(e => e.BusinessUnitName).ThenBy(l => l.LastName).ThenBy(f => f.FirstName);
          break;
        case Constants.SortOrders.EmployerNameAsc:
					query = query.OrderBy(e => e.EmployerName).ThenBy(l => l.LastName).ThenBy(f => f.FirstName);
					break;
				case Constants.SortOrders.EmployerNameDesc:
					query = query.OrderByDescending(e => e.EmployerName).ThenBy(l => l.LastName).ThenBy(f => f.FirstName);
					break;
				case Constants.SortOrders.ContactNameAsc:
					query = query.OrderBy(l => l.LastName).ThenBy(f => f.FirstName);
					break;
				case Constants.SortOrders.ContactNameDesc:
					query = query.OrderByDescending(l => l.LastName).ThenByDescending(f => f.FirstName);
					break;
				case Constants.SortOrders.PhoneNumberAsc:
					query = query.OrderBy(p => p.PhoneNumber);
					break;
				case Constants.SortOrders.PhoneNumberDesc:
					query = query.OrderByDescending(p => p.PhoneNumber);
					break;
				case Constants.SortOrders.LocationAsc:
					query = query.OrderBy(t => t.TownCity).ThenBy(s => s.State);
					break;
				case Constants.SortOrders.LocationDesc:
					query = query.OrderByDescending(t => t.TownCity).ThenByDescending(s => s.State);
					break;
        case Constants.SortOrders.TitleAsc:
          query = query.OrderBy(e => e.JobTitle).ThenBy(l => l.LastName).ThenBy(f => f.FirstName);
          break;
        case Constants.SortOrders.TitleDesc:
          query = query.OrderByDescending(e => e.JobTitle).ThenBy(l => l.LastName).ThenBy(f => f.FirstName);
          break;
        case Constants.SortOrders.EmailAddressAsc:
          query = query.OrderBy(e => e.EmailAddress);
          break;
        case Constants.SortOrders.EmailAddressDesc:
          query = query.OrderByDescending(e => e.EmailAddress);
          break;
        case Constants.SortOrders.LastLoginAsc:
          query = query.OrderBy(e => e.LastLogin);
          break;
        case Constants.SortOrders.LastLoginDesc:
          query = query.OrderByDescending(e => e.LastLogin);
          break;
				default:
					throw new Exception("Invalid sort order");
			}

			#endregion

			return query;
		}
	}
}
