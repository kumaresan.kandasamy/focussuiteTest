﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using Focus.Core.Views;
using Focus.Data.Library.Entities;
using Focus.Core.Criteria.Onet;
using Focus.Data.Repositories.Contracts;
using Framework.Core;

#endregion


namespace Focus.Services.Queries
{
	public class OnetDetailsViewQuery : QueryBase <OnetDetailsView, ILibraryRepository, OnetViewCriteria>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="OnetDetailsViewQuery"/> class.
		/// </summary>
		/// <param name="repository">The repository.</param>
		/// <param name="criteria">The criteria.</param>
		public OnetDetailsViewQuery(ILibraryRepository repository, OnetViewCriteria criteria) : base(repository, criteria) {}

		/// <summary>
		/// Produces a query for this type.
		/// </summary>
		/// <returns></returns>
		public override IQueryable<OnetDetailsView> Query()
		{
			var query = from o in Repository.Query<OnetView>()
			            where o.Culture == Criteria.Culture
			            select new OnetDetailsView
			                   	{
			                   		Id = o.Id,
														OnetCode = o.OnetCode,
														Key = o.Key,
			                   		Occupation = o.Occupation,
			                   		Description = o.Description,
                            JobFamilyId = o.JobFamilyId,
                            JobTasksAvailable = o.JobTasksAvailable
													};

			if (Criteria.OnetId.HasValue)
				query = query.Where(x => x.Id == Criteria.OnetId.Value);

			if (Criteria.JobFamilyId.HasValue)
				query = query.Where(x => x.JobFamilyId == Criteria.JobFamilyId);

			if (Criteria.OnetCode.IsNotNullOrEmpty())
				query = query.Where(x => x.OnetCode == Criteria.OnetCode);

			return query;
		}
	}
}
