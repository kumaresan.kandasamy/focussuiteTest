﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Linq;

using Focus.Core;
using Focus.Core.Criteria.JobSeeker;
using Focus.Core.Settings.Interfaces;
using Focus.Data.Core.Entities;
using Focus.Data.Repositories.Contracts;

using Framework.Core;

#endregion

namespace Focus.Services.Queries
{
	public class StudentAlumniIssuesQuery : QueryBase<StudentAlumniIssueView, ICoreRepository, JobSeekerCriteria>
	{
    private readonly IAppSettings _settings;
    private IQueryable<StudentAlumniIssueView> _query;

    public StudentAlumniIssuesQuery(ICoreRepository repository, JobSeekerCriteria criteria, IAppSettings settings): base(repository, criteria)
		{
      _settings = settings;
		}

		/// <summary>
		/// Produces a query for this type.
		/// </summary>
		/// <returns></returns>
		public override IQueryable<StudentAlumniIssueView> Query()
		{
			_query = Repository.Query<StudentAlumniIssueView>();
       
       
			if (Criteria.Firstname.IsNotNullOrEmpty())
				_query = _query.Where(x => x.FirstName.Contains(Criteria.Firstname));

			if (Criteria.Lastname.IsNotNullOrEmpty())
				_query = _query.Where(x => x.LastName.Contains(Criteria.Lastname));

			if (Criteria.EmailAddress.IsNotNullOrEmpty())
				_query = _query.Where(x => x.EmailAddress.Contains(Criteria.EmailAddress));

			if (Criteria.DateOfBirth.HasValue)
				_query = _query.Where(x => x.DateOfBirth == Criteria.DateOfBirth.Value);

			if (Criteria.Ssn.IsNotNullOrEmpty())
        _query = _query.Where(x => x.SocialSecurityNumber == Criteria.Ssn);

			if (Criteria.Username.IsNotNullOrEmpty())
				_query = _query.Where(x => x.UserName == Criteria.Username);

      #region Type Of Issues

      FilterTypeOfIssues();

      #endregion

      #region Enrollment Status

			#region Enrollment Status Year Two

			if (Criteria.JobSeekerSchoolStatus.Equals(0) && Criteria.JobSeekerSchoolType.Equals(SchoolTypes.TwoYear))
			{
				// All Student/Alumni
				_query = _query.Where(x => x.EnrollmentStatus.Equals(6)
					                        || x.EnrollmentStatus.Equals(7)
					                        || x.EnrollmentStatus.Equals(8)
					                        || x.EnrollmentStatus.Equals(9)
					                        || x.EnrollmentStatus.Equals(10));
			}
			if (Criteria.JobSeekerSchoolStatus > 0)
			{
				FilterEnrollmentStatus();
			}

			#endregion

			#region Enrollment Status Year Four

			if (Criteria.JobSeekerSchoolStatus.Equals(0) && Criteria.JobSeekerSchoolType.Equals(SchoolTypes.FourYear))
			{
				// All Student/Alumni
				_query = _query.Where(x => x.EnrollmentStatus.Equals(6)
					                        || x.EnrollmentStatus.Equals(7)
																	|| x.EnrollmentStatus.Equals(9)
																	|| x.EnrollmentStatus.Equals(10)
					                        || x.EnrollmentStatus.Equals(11)
					                        || x.EnrollmentStatus.Equals(12)
					                        || x.EnrollmentStatus.Equals(13)
					                        || x.EnrollmentStatus.Equals(14));
			}
			else if (Criteria.JobSeekerSchoolStatus > 0)
			{
				FilterEnrollmentStatus();
			}

			#endregion


			#endregion

			#region Program of study

		  FilterProgramOfStudy();

			#endregion

      #region Campus

		  if (Criteria.CampusId.HasValue)
		    _query = _query.Where(x => x.CampusId == Criteria.CampusId.Value);

      #endregion

			if (Criteria.UserType.IsNotNull())
			  _query = _query.Where(x => x.UserType == Criteria.UserType);

		  if (Criteria.ExternalId.IsNotNullOrEmpty())
		    _query = _query.Where(x => x.ExternalId.Contains(Criteria.ExternalId));

			if (Criteria.ExternalSeekerIdList.IsNotNullOrEmpty())
				_query = _query.Where(x => Criteria.ExternalSeekerIdList.Contains(x.ExternalId));

			#region Sorting

			switch (Criteria.OrderBy)
			{
				case Constants.SortOrders.JobSeekerNameAsc:
					_query = _query.OrderBy(x => x.FirstName);
					break;
				case Constants.SortOrders.JobSeekerNameDesc:
					_query = _query.OrderByDescending(x => x.FirstName);
					break;
				case Constants.SortOrders.StudyProgramAsc:
          _query = _query.OrderBy(x => x.ProgramAreaId).ThenBy(x => x.DegreeId);
					break;
				case Constants.SortOrders.StudyProgramDesc:
          _query = _query.OrderByDescending(x => x.ProgramAreaId).ThenByDescending(x => x.DegreeId);
					break;
				case Constants.SortOrders.LastActivityAsc:
					_query = _query.OrderBy(x => x.LastLoggedInOn);
					break;
				case Constants.SortOrders.LastActivityDesc:
					_query = _query.OrderByDescending(x => x.LastLoggedInOn);
					break;
				default:
					_query = _query.OrderBy(x => x.FirstName);
					break;
			}

      #endregion

      return _query;
	  }
		

		/// <summary>
		/// Filters the enrollment status.
		/// </summary>
		private void FilterEnrollmentStatus()
		{
			switch (Criteria.JobSeekerSchoolStatus)
		  {
				case SchoolStatus.Prospective:
					_query = _query.Where(x => x.EnrollmentStatus == 6);
				break;

				case SchoolStatus.FirstYear:
				_query = _query.Where(x => x.EnrollmentStatus == 7);
				break;

				case SchoolStatus.SophomoreOrAbove:
				_query = _query.Where(x => x.EnrollmentStatus == 8);
				break;

				case SchoolStatus.NonCreditOther:
				_query = _query.Where(x => x.EnrollmentStatus == 9);
				break;

				case SchoolStatus.Alumni:
				_query = _query.Where(x => x.EnrollmentStatus == 10);
				break;

				case SchoolStatus.Sophomore:
					_query = _query.Where(x => x.EnrollmentStatus == 11);
				break;

				case SchoolStatus.Junior:
				_query = _query.Where(x => x.EnrollmentStatus == 12);
				break;

				case SchoolStatus.Senior:
				_query = _query.Where(x => x.EnrollmentStatus == 13);
				break;

				case SchoolStatus.Graduate:
				_query = _query.Where(x => x.EnrollmentStatus == 14);
				break;
			}
		}

		/// <summary>
		/// Filters the type of issues.
		/// </summary>
    private void FilterTypeOfIssues()
    {
      #region Type of Issues

        if (Criteria.JobSeekerGroupedIssueSearch == true)
        {
          // Any issue
          _query =
            _query.Where(
              x =>
              x.NoLoginTriggered == true
              || x.JobOfferRejectionTriggered == true
              || x.NotReportingToInterviewTriggered == true
              || x.NotClickingOnLeadsTriggered == true
              || x.NotRespondingToEmployerInvitesTriggered == true
              || x.ShowingLowQualityMatchesTriggered == true
              || x.PostingLowQualityResumeTriggered == true
              || x.NotSearchingJobsTriggered == true
              || x.FollowUpRequested == true
              || x.PostHireFollowUpTriggered == true
              || x.InappropriateEmailAddress == true
							|| x.GivingPositiveFeedback == true
							|| x.GivingNegativeFeedback == true);
        }
        else if (Criteria.JobSeekerGroupedIssueSearch == false)
        {
          // No issues
          _query =
            _query.Where(
              x =>
              (x.NoLoginTriggered == false
               && x.JobOfferRejectionTriggered == false
               && x.NotReportingToInterviewTriggered == false
               && x.NotClickingOnLeadsTriggered == false
               && x.NotRespondingToEmployerInvitesTriggered == false
               && x.ShowingLowQualityMatchesTriggered == false
               && x.PostingLowQualityResumeTriggered == false
               && x.NotSearchingJobsTriggered == false
               && x.FollowUpRequested == false
               && x.PostHireFollowUpTriggered == false
               && x.InappropriateEmailAddress == false
               && (x.GivingNegativeFeedback == false || x.GivingNegativeFeedback == null)
               && (x.GivingPositiveFeedback == false || x.GivingPositiveFeedback == null)) || x.NoLoginTriggered == null); // Handle no issues record
        }
        else if (Criteria.JobSeekerIssue.IsNotNull())
        {
          // Specific issue
          if (Criteria.JobSeekerIssue.Value == CandidateIssueType.NotLoggingIn)
            _query = _query.Where(x => x.NoLoginTriggered == true);

          if (Criteria.JobSeekerIssue.Value == CandidateIssueType.NotClickingLeads)
            _query = _query.Where(x => x.NotClickingOnLeadsTriggered == true);

          if (Criteria.JobSeekerIssue.Value == CandidateIssueType.RejectingJobOffers)
            _query = _query.Where(x => x.JobOfferRejectionTriggered == true);

          if (Criteria.JobSeekerIssue.Value == CandidateIssueType.NotReportingForInterviews)
            _query = _query.Where(x => x.NotReportingToInterviewTriggered == true);

          if (Criteria.JobSeekerIssue.Value == CandidateIssueType.NotRespondingToEmployerInvites)
            _query = _query.Where(x => x.NotRespondingToEmployerInvitesTriggered == true);

          if (Criteria.JobSeekerIssue.Value == CandidateIssueType.ShowingLowQualityMatches)
            _query = _query.Where(x => x.ShowingLowQualityMatchesTriggered == true);

          if (Criteria.JobSeekerIssue.Value == CandidateIssueType.PoorQualityResume)
            _query = _query.Where(x => x.PostingLowQualityResumeTriggered == true);

          if (Criteria.JobSeekerIssue.Value == CandidateIssueType.SuggestingPostHireFollowUp)
            _query = _query.Where(x => x.PostHireFollowUpTriggered == true);

          if (Criteria.JobSeekerIssue.Value == CandidateIssueType.RequiringFollowUp)
            _query = _query.Where(x => x.FollowUpRequested == true);

          if (Criteria.JobSeekerIssue.Value == CandidateIssueType.NotSearchingJobs)
            _query = _query.Where(x => x.NotSearchingJobsTriggered == true);

          if (Criteria.JobSeekerIssue.Value == CandidateIssueType.InappropriateEmailAddress)
            _query = _query.Where(x => x.InappropriateEmailAddress == true);

					if (Criteria.JobSeekerIssue.Value == CandidateIssueType.GivingPositiveFeedback)
						_query = _query.Where(x => x.GivingPositiveFeedback == true);

					if (Criteria.JobSeekerIssue.Value == CandidateIssueType.GivingNegativeFeedback)
						_query = _query.Where(x => x.GivingNegativeFeedback == true);
        }
      
      #endregion
    }

		/// <summary>
		/// Filters the program of study.
		/// </summary>
    private void FilterProgramOfStudy()
    {
      if (Criteria.JobSeekerProgramAreaId > 0)
        _query = _query.Where(x => x.ProgramAreaId == Criteria.JobSeekerProgramAreaId);

      if (!_settings.ShowProgramArea || Criteria.JobSeekerProgramAreaId > 0)
      {
        if (Criteria.JobSeekerProgramDegreeId > 0)
          _query = _query.Where(x => x.DegreeId == Criteria.JobSeekerProgramDegreeId);
      }
    }

		/// <summary>
		/// Queries the entity id.
		/// </summary>
		/// <returns></returns>
		public override StudentAlumniIssueView QueryEntityId()
		{
			var candidate = Repository.FindById<StudentAlumniIssueView>(Criteria.PersonId);
			return candidate;
		}
	}
}
