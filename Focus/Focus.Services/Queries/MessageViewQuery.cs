﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using Focus.Core;
using Focus.Core.Views;
using Focus.Core.Criteria.Message;
using Focus.Data.Repositories.Contracts;

#endregion


namespace Focus.Services.Queries
{
	public class MessageViewQuery : QueryBase <MessageView, ICoreRepository, MessageViewCriteria>
	{
		private readonly IConfigurationRepository _configurationRepository;

		public MessageViewQuery(ICoreRepository repository, IConfigurationRepository configurationRepository, MessageViewCriteria criteria) : base(repository, criteria)
		{
			_configurationRepository = configurationRepository;
		}

		/// <summary>
		/// Produces a query for this type.
		/// </summary>
		/// <returns></returns>
		public override IQueryable<MessageView> Query()
		{
			//FVN-3164 constrain messages so that they only displayed on or after the users creation date
			var userCreatedOn = Repository.Users.Where(u => u.Id == Criteria.UserId).Select(u => u.CreatedOn).FirstOrDefault();

			var query = from m in Repository.Messages
			            join mt in Repository.MessageTexts on m.Id equals mt.MessageId
									join l in _configurationRepository.Localisations on mt.LocalisationId equals l.Id
			            where !(from dm in Repository.DismissedMessages where dm.UserId == Criteria.UserId select dm.MessageId).Contains(m.Id)
									&& l.Culture == Criteria.Culture
							    && m.ExpiresOn > DateTime.UtcNow
									&& m.CreatedOn.Date >= userCreatedOn.Date
									select new MessageView
			                   	{
			                   		Id = m.Id,
			                   		IsSystemAlert = m.IsSystemAlert,
			                   		Text = mt.Text,
			                   		CreatedOn = m.CreatedOn,
			                   		Audience = m.Audience,
			                   		EmployerId = m.EmployerId,
														UserId = m.UserId,
														BusinessUnitId = m.BusinessUnitId
			                   	};

			// Filter for application 
			switch(Criteria.Module)
			{
				case FocusModules.Talent:
					// Find BusinessUnitIDs available to employee
					var businessUnitList = (from bu in Repository.EmployeeBusinessUnits
																	where bu.EmployeeId == Criteria.EmployeeId
																	select (long?)bu.BusinessUnitId).ToList();
					
					query = query.Where(x => ((x.Audience == MessageAudiences.Employer && x.EmployerId == Criteria.EmployerId) || (x.Audience == MessageAudiences.BusinessUnit && businessUnitList.Contains(x.BusinessUnitId)) || (x.Audience == MessageAudiences.AllTalentUsers) ||
																		x.Audience == MessageAudiences.User && x.UserId == Criteria.UserId));
						
					break;

				case FocusModules.Assist:
					query = query.Where(x => x.Audience == MessageAudiences.AllAssistUsers || (x.Audience == MessageAudiences.User && x.UserId == Criteria.UserId));
					break;

				case FocusModules.CareerExplorer:
					query = query.Where(x => x.Audience == MessageAudiences.AllJobSeekers || (x.Audience == MessageAudiences.User && x.UserId == Criteria.UserId));
					break;
			}

			return query;
		}
	}
}
