﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Linq;
using Focus.Core.Criteria;
using Focus.Data.Library.Entities;
using Focus.Core.Criteria.Onet;
using Focus.Data.Repositories.Contracts;
using Framework.Core;

#endregion


namespace Focus.Services.Queries
{
	public class OnetQuery : QueryBase <Onet, ILibraryRepository, OnetCriteria>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="JobQuery"/> class.
		/// </summary>
		/// <param name="repository">The repository.</param>
		/// <param name="criteria">The criteria.</param>
		public OnetQuery(ILibraryRepository repository, OnetCriteria criteria): base(repository, criteria) {}

		/// <summary>
		/// Produces a query for this type.
		/// </summary>
		/// <returns></returns>
		public override IQueryable<Onet> Query()
		{
			var query = Repository.Onets;

			// If we dont have one of these values we dont data to come back
			if (Criteria.JobFamilyId.IsNotNull())
				query = query.Where(x => x.JobFamilyId == Criteria.JobFamilyId.Value);

			if (Criteria.OrderBy.IsNotNullOrEmpty() && Criteria.OrderBy.Trim().IsNotNullOrEmpty())
				query = query.OrderBy(Criteria.OrderBy);

			if (Criteria.FetchOption == CriteriaBase.FetchOptions.Single)
			{
				if (Criteria.OnetId.IsNotNull())
					query = query.Where(x => x.Id == Criteria.OnetId);

				return query;
			}

			return query;
		}

		/// <summary>
		/// Queries the entity id.
		/// </summary>
		/// <returns></returns>
		public override Onet QueryEntityId()
		{
			return Repository.FindById<Onet>(Criteria.OnetId);
		}
	}
}
