﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Linq;

using Focus.Core.Criteria;
using Focus.Core.Criteria.JobDrivingLicenceEndorsement;
using Focus.Data.Core.Entities;
using Focus.Data.Repositories.Contracts;

using Framework.Core;

#endregion

namespace Focus.Services.Queries
{
	public class JobDrivingLicenceEndorsementQuery : QueryBase <JobDrivingLicenceEndorsement, ICoreRepository, JobDrivingLicenceEndorsementCriteria>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="JobDrivingLicenceEndorsementQuery"/> class.
		/// </summary>
		/// <param name="repository">The repository.</param>
		/// <param name="criteria">The criteria.</param>
		public JobDrivingLicenceEndorsementQuery(ICoreRepository repository, JobDrivingLicenceEndorsementCriteria criteria) : base(repository, criteria) {}

		/// <summary>
		/// Produces a query for this type.
		/// </summary>
		/// <returns></returns>
		public override IQueryable<JobDrivingLicenceEndorsement> Query()
		{
			var query = Repository.JobDrivingLicenceEndorsements;

			if (Criteria.OrderBy.IsNotNullOrEmpty() && Criteria.OrderBy.Trim().IsNotNullOrEmpty())
				query = query.OrderBy(Criteria.OrderBy);

			if (Criteria.JobId.IsNotNull())
			{
				query = query.Where(x => x.JobId == Criteria.JobId);
			}
			
			return query;
		}

		/// <summary>
		/// </summary>
		/// <returns></returns>
		public override JobDrivingLicenceEndorsement QueryEntityId()
		{
			return Repository.FindById<JobDrivingLicenceEndorsement>(Criteria.Id);
		}
	}
}
