﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using Focus.Core;
using Focus.Core.Criteria.Employer;
using Focus.Data.Core.Entities;
using Focus.Data.Repositories.Contracts;
using Focus.Services.Core;
using Framework.Core;

#endregion

namespace Focus.Services.Queries
{
	public class EmployerAccountReferralQuery : QueryBase <EmployerAccountReferralView, ICoreRepository, EmployerAccountReferralCriteria>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="JobPostingReferralViewQuery"/> class.
		/// </summary>
		/// <param name="repository">The repository.</param>
		/// <param name="criteria">The criteria.</param>
		public EmployerAccountReferralQuery(ICoreRepository repository, EmployerAccountReferralCriteria criteria) : base(repository, criteria) {}

		/// <summary>
		/// Queries this instance.
		/// </summary>
		/// <returns></returns>
		public override IQueryable<EmployerAccountReferralView> Query()
		{
			var query = (from r in Repository.EmployerAccountReferralViews
									 select r);

			#region Employer type

			if (Criteria.EmployerType.IsNotNullOrEmpty())
			{
				switch (Criteria.EmployerType.ToLower())
				{
					case "instate":
						query = query.Where(x => x.EmployeeAddressStateId == Criteria.DefaultStateId);
						break;

					case "outofstate":
						query = query.Where(x => x.EmployeeAddressStateId != Criteria.DefaultStateId);
						break;

					default:
						query = query.Where(x => x.EmployerOwnershipTypeId == Convert.ToInt64(Criteria.EmployerType));
						break;
				}
			}

			#endregion

			#region Office

			if (Criteria.OfficeIds.IsNotNull() && Criteria.OfficeIds.Count > 0)
			{
				var officeIds = Criteria.OfficeIds.ToArray();
				query = query.Join(Repository.Employers.Where(e => e.EmployerOfficeMappers.Any(eom => officeIds.Contains(eom.OfficeId))), r => r.EmployerId, e => e.Id, (r, e) => r);
			}

			#endregion

			#region Staff member

			if (Criteria.StaffMemberId.IsNotNull())
				query = query.Where(x => x.EmployerAssignedToId == Criteria.StaffMemberId);

			#endregion

			#region Approval status

			if (Criteria.ApprovalStatus.IsNotNull() && Criteria.ApprovalStatus != ApprovalStatuses.None)
        query = query.Where(x => ((ApprovalStatuses)x.EmployeeApprovalStatus == Criteria.ApprovalStatus &&  x.TypeOfApproval == (int)ApprovalType.HiringManager) || ((ApprovalStatuses)x.EmployerApprovalStatus == Criteria.ApprovalStatus &&  x.TypeOfApproval == (int)ApprovalType.Employer)  || ((ApprovalStatuses)x.BusinessUnitApprovalStatus == Criteria.ApprovalStatus &&  x.TypeOfApproval == (int)ApprovalType.BusinessUnit));

		  if (Criteria.ApprovalType != null)
		  {
		    query = query.Where(x => x.TypeOfApproval == (int) Criteria.ApprovalType);
		  }
			#endregion

			#region Ordering

			if (Criteria.OrderBy.IsNotNullOrEmpty() && Criteria.OrderBy.Trim().IsNotNullOrEmpty())
				if (Criteria.OrderBy.StartsWith("EmployeeFullName asc"))
					query = query.OrderBy(x => x.EmployeeFirstName).ThenBy(x => x.EmployeeLastName);
				else if (Criteria.OrderBy.StartsWith("EmployeeFullName desc"))
					query = query.OrderByDescending(x => x.EmployeeFirstName).ThenByDescending(x => x.EmployeeLastName);
				else
					query = query.OrderBy(Criteria.OrderBy);
			else
				query = query.OrderBy(x => x.AwaitingApprovalDate).ThenBy(x => x.EmployerName);

			#endregion

			return query;
		}
	}
}
