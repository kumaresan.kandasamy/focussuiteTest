﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Linq;

using Framework.Core;
using Focus.Core.Criteria.Document;

using Focus.Data.Core.Entities;
using Focus.Data.Repositories.Contracts;

#endregion

namespace Focus.Services.Queries
{
	public class DocumentQuery : QueryBase<Document, ICoreRepository, DocumentCriteria>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="DocumentQuery" /> class.
		/// </summary>
		/// <param name="repository">The repository.</param>
		/// <param name="criteria">The criteria.</param>
		public DocumentQuery(ICoreRepository repository, DocumentCriteria criteria) : base(repository, criteria) { }

		/// <summary>
		/// Queries this instance.
		/// </summary>
		/// <returns></returns>
		public override IQueryable<Document> Query()
		{
			var query = Repository.Documents;

			if (Criteria.IsNotNull())
			{
				if (Criteria.Category.IsNotNull())
					query = query.Where(d => d.Category == Criteria.Category);

				// Documents can be relevant to more than one Focus Module therefore DocumentFocusModule is an enum flag
				// We want to show any documents that relvant to the specified module, even if they're relevant to other modules
				if (Criteria.Module.IsNotNull())
					query = query.Where(d => (d.Module & Criteria.Module) != 0);

				if (Criteria.Group.IsNotNull())
					query = query.Where(d => d.Group == Criteria.Group);

				if (Criteria.DocumentId.IsNotNull())
					query = query.Where(x => x.Id.Equals(Criteria.DocumentId));
			}
			
			// At the moment ordering is hardcoded until there's a specific requirement for something otherwise
			query = query.OrderBy(d => d.Group).ThenBy(d => d.Order);

			return query;
		}
	}
}
