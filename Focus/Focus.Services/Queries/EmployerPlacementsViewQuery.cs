﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using Focus.Core;
using Focus.Core.Criteria.Employer;
using Focus.Data.Core.Entities;
using Focus.Data.Repositories.Contracts;
using Framework.Core;

#endregion

namespace Focus.Services.Queries
{
	public class EmployerPlacementsViewQuery : QueryBase <EmployerPlacementsView, ICoreRepository, PlacementsCriteria>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="EmployerPlacementsViewQuery"/> class.
		/// </summary>
		/// <param name="repositiry">The repositiry.</param>
		/// <param name="criteria">The criteria.</param>
		public EmployerPlacementsViewQuery(ICoreRepository repositiry, PlacementsCriteria criteria) : base(repositiry, criteria) {}

		public override IQueryable<EmployerPlacementsView> Query()
		{
			var query = Repository.EmployerPlacementsViews;

			if (Criteria.NumberOfDays.HasValue)
				query = query.Where(x => x.StartDate.Value.Date >= DateTime.Today.AddDays(-Criteria.NumberOfDays.Value));

			#region Office filter

			if (Criteria.OfficeId.HasValue || Criteria.OfficeGroup == OfficeGroup.MyOffices)
			{
				if (Criteria.OfficeId.HasValue)
				{
					query = query.Join(Repository.Employers.Where(e => e.EmployerOfficeMappers.Any(eom => eom.OfficeId == Criteria.OfficeId)), r => r.EmployerId, e => e.Id, (r, e) => r);
				}
				else
				{
					var officeIds = Repository.PersonOfficeMappers.Where(x => x.PersonId == Criteria.PersonId).Select(x => x.OfficeId).Distinct().ToArray();
					if (officeIds.Length > 0 && officeIds[0].IsNotNullOrZero())
					{
						query = query.Join(Repository.Employers.Where(j => j.EmployerOfficeMappers.Any(eom => officeIds.Contains(eom.OfficeId))), r => r.EmployerId, e => e.Id, (r, e) => r);
					}
				}
			}

			#endregion

			// Sort results
			switch(Criteria.OrderBy)
			{
				case Constants.SortOrders.EmployerNameDesc:
					query = query.OrderByDescending(x => x.EmployerName);
				break;
				case Constants.SortOrders.EmployerNameAsc:
					query = query.OrderBy(x => x.EmployerName);
				break;
				case Constants.SortOrders.JobTitleDesc:
					query = query.OrderByDescending(x => x.JobTitle);
				break;
				case Constants.SortOrders.JobTitleAsc:
					query = query.OrderBy(x => x.JobTitle);
				break;
				case Constants.SortOrders.NameAsc:
					query = query.OrderBy(x => x.Name);
				break;
				case Constants.SortOrders.NameDesc:
					query = query.OrderByDescending(x => x.Name);
				break;
				default:
					query = query.OrderBy(x => x.EmployerName);
				break;
			}

			return query;
		}
	}
}
