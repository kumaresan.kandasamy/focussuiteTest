﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;

using Focus.Core;
using Focus.Data.Core.Entities;
using Focus.Core.Criteria.Employee;
using Focus.Data.Repositories.Contracts;
using Framework.Core;

#endregion

namespace Focus.Services.Queries
{
	public class EmployeeQuery : QueryBase <Employee, ICoreRepository, EmployeeCriteria>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="EmployeeSearchViewQuery"/> class.
		/// </summary>
		/// <param name="repository">The repository.</param>
		/// <param name="criteria">The criteria.</param>
		public EmployeeQuery(ICoreRepository repository, EmployeeCriteria criteria): base(repository, criteria) {}

		/// <summary>
		/// Produces a query for this type.
		/// </summary>
		/// <returns></returns>
		public override IQueryable<Employee> Query()
		{
			var query = Repository.Employees;

			if (!String.IsNullOrEmpty(Criteria.EmailAddress)) query = query.Where(e => e.Person.EmailAddress == Criteria.EmailAddress);

			return query;
		}
	}
}
