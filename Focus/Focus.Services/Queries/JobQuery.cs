﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Linq;
using Focus.Core.Criteria;
using Focus.Data.Core.Entities;
using Focus.Core.Criteria.Job;
using Focus.Data.Repositories.Contracts;
using Framework.Core;

#endregion

namespace Focus.Services.Queries
{
	public class JobQuery : QueryBase <Job, ICoreRepository, JobCriteria>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="JobQuery"/> class.
		/// </summary>
		/// <param name="repository">The repository.</param>
		/// <param name="criteria">The criteria.</param>
		public JobQuery(ICoreRepository repository, JobCriteria criteria): base(repository, criteria) {}

		/// <summary>
		/// Produces a query for this type.
		/// </summary>
		/// <returns></returns>
		public override IQueryable<Job> Query()
		{
			var query = Repository.Jobs;

			if (Criteria.EmployerId.IsNotNull())
  			query = query.Where(x => x.Employer.Id == Criteria.EmployerId);

      if (Criteria.JobId.HasValue)
        query = query.Where(x => x.Id == Criteria.JobId.Value);

      if (Criteria.JobTitle.IsNotNullOrEmpty())
        query = query.Where(x => x.JobTitle == Criteria.JobTitle);

			if( Criteria.ExternalId.IsNotNullOrEmpty() )
				query = query.Where( x => x.ExternalId == Criteria.ExternalId );

			if( Criteria.OrderBy.IsNotNullOrEmpty() && Criteria.OrderBy.Trim().IsNotNullOrEmpty() )
				query = query.OrderBy(Criteria.OrderBy);

			if (Criteria.FetchOption != CriteriaBase.FetchOptions.Single)
			{
				if (Criteria.JobStatus.IsNotNull())
					query = query.Where(x => x.JobStatus == Criteria.JobStatus.Value);

				if (Criteria.EmployerId.IsNotNull())
				{
					query = from q in query
									join e in Repository.Employers on q.EmployerId equals e.Id
									where e.Id == Criteria.EmployerId
									select q;
				}
			}
			
			return query;
		}

		/// <summary>
		/// Queries the entity id.
		/// </summary>
		/// <returns></returns>
		public override Job QueryEntityId()
		{
			return Repository.FindById<Job>(Criteria.JobId);
		}
	}
}
