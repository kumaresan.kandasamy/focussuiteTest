﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Linq;
using Focus.Core.Criteria;
using Focus.Data.Core.Entities;
using Focus.Core.Criteria.JobLocation;
using Focus.Data.Repositories.Contracts;
using Framework.Core;

#endregion

namespace Focus.Services.Queries
{
	public class JobLocationQuery: QueryBase <JobLocation, ICoreRepository, JobLocationCriteria>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="JobQuery"/> class.
		/// </summary>
		/// <param name="repository">The repository.</param>
		/// <param name="criteria">The criteria.</param>
		public JobLocationQuery(ICoreRepository repository, JobLocationCriteria criteria): base(repository, criteria) {}

		/// <summary>
		/// Produces a query for this type.
		/// </summary>
		/// <returns></returns>
		public override IQueryable<JobLocation> Query()
		{
			var query = Repository.JobLocations;

			if (Criteria.OrderBy.IsNotNullOrEmpty() && Criteria.OrderBy.Trim().IsNotNullOrEmpty())
				query = query.OrderBy(Criteria.OrderBy);

			if (Criteria.FetchOption != CriteriaBase.FetchOptions.Single)
			{
				if (Criteria.EmployerId.IsNotNull())
				{
					query = from q in query
									join j in Repository.Jobs on q.JobId equals  j.Id
									join e in Repository.Employers on j.EmployerId equals e.Id
									where e.Id == Criteria.EmployerId
									select q;
				}

				if (Criteria.JobId.IsNotNull())
				{
					query = from q in query
									join j in Repository.Jobs on q.JobId equals  j.Id
									where j.Id == Criteria.JobId
									select q;
				}

				if (Criteria.BusinessUnitId.IsNotNull())
				{
					query = from q in query
						join b in Repository.BusinessUnits on q.Job.BusinessUnitId equals b.Id
						where b.Id == Criteria.BusinessUnitId
						select q;
				}
			}
			
			return query;
		}

		/// <summary>
		/// </summary>
		/// <returns></returns>
		public override JobLocation QueryEntityId()
		{
			return Repository.FindById<JobLocation>(Criteria.JobLocationId);
		}
	}
}
