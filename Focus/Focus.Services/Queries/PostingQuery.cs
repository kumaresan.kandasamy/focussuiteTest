﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using Focus.Core;
using Focus.Core.Criteria.Application;
using Focus.Core.Criteria.CandidateApplication;
using Focus.Core.Criteria.Posting;
using Focus.Data.Core.Entities;
using Focus.Data.Repositories.Contracts;
using Framework.Core;

#endregion

namespace Focus.Services.Queries
{
	public class PostingQuery : QueryBase<Posting, ICoreRepository, PostingCriteria>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="PostingQuery"/> class.
		/// </summary>
		/// <param name="repository">The repository.</param>
		/// <param name="criteria">The criteria.</param>
		public PostingQuery(ICoreRepository repository, PostingCriteria criteria) : base(repository, criteria) { }

		/// <summary>
		/// Produces a query for this type.
		/// </summary>
		/// <returns></returns>
		public override IQueryable<Posting> Query()
		{
			var query = Repository.Postings;

			if (!String.IsNullOrEmpty(Criteria.JobTitle)) query = query.Where(p => p.JobTitle == Criteria.JobTitle);

			return query;
		}
	}
}
