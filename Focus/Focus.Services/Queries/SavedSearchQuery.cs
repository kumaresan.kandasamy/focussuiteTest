﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Linq;
using Focus.Data.Core.Entities;
using Focus.Core.Criteria.SavedSearch;
using Focus.Data.Repositories.Contracts;
using Framework.Core;

#endregion



namespace Focus.Services.Queries
{
	public class SavedSearchQuery : IEntityQuery <SavedSearch>
	{
		private readonly SavedSearchCriteria _criteria;
		private readonly ICoreRepository _repository;

		/// <summary>
		/// Initializes a new instance of the <see cref="SavedSearchQuery"/> class.
		/// </summary>
		/// <param name="repository">The repository.</param>
		/// <param name="criteria">The criteria.</param>
		public SavedSearchQuery(ICoreRepository repository, SavedSearchCriteria criteria)
		{
			_criteria = criteria;
			_repository = repository;
		}

		/// <summary>
		/// Produces a query for this type.
		/// </summary>
		/// <returns></returns>
		public IQueryable<SavedSearch> Query()
		{
			var query = _repository.SavedSearches;

			if (_criteria.OrderBy.IsNotNullOrEmpty() && _criteria.OrderBy.Trim().IsNotNullOrEmpty())
				query = query.OrderBy(_criteria.OrderBy);

			if (_criteria.UserId.IsNotNull())
				query = (
				        	from q in query
				        	join su in _repository.SavedSearchUsers on q.Id equals su.SavedSearchId
				        	join u in _repository.Users on su.UserId equals u.Id
				        	where u.Id == _criteria.UserId
				        	select q
				        );

			return query;
		}

		/// <summary>
		/// Queries the entity id.
		/// </summary>
		/// <returns></returns>
		public SavedSearch QueryEntityId()
		{
			return _repository.FindById<SavedSearch>(_criteria.SavedSearchId);
		}
	}
}
