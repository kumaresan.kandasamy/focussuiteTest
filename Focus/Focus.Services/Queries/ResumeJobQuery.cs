﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Linq;
using Focus.Core.Criteria.ResumeJob;
using Focus.Data.Core.Entities;
using Focus.Data.Repositories.Contracts;

#endregion

namespace Focus.Services.Queries
{
	public class ResumeJobQuery : QueryBase <ResumeJob, ICoreRepository, ResumeJobCriteria>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="ResumeJobQuery"/> class.
		/// </summary>
		/// <param name="repository">The repository.</param>
		/// <param name="criteria">The criteria.</param>
		public ResumeJobQuery(ICoreRepository repository, ResumeJobCriteria criteria) : base(repository, criteria) {}

		/// <summary>
		/// Produces a query for this type.
		/// </summary>
		/// <returns></returns>
		public override IQueryable<ResumeJob> Query()
		{
			var query = Repository.ResumeJobs;

			if (Criteria.ResumeId.HasValue)
				query = query.Where(x => x.ResumeId == Criteria.ResumeId);

			query = query.OrderByDescending(x => x.StartYear).ThenBy(x => x.EndYear);

			return query;
		}

		/// <summary>
		/// Queries the entity id.
		/// </summary>
		/// <returns></returns>
		public override ResumeJob QueryEntityId()
		{
			return Repository.FindById<ResumeJob>(Criteria.Id);
		}
	}
}
