﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Linq;

using Focus.Data.Core.Entities;
using Focus.Core.Criteria.User;
using Focus.Data.Repositories.Contracts;
using Framework.Core;

#endregion

namespace Focus.Services.Queries
{
	public class StaffSearchViewQuery : QueryBase<StaffSearchView, ICoreRepository, StaffCriteria>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="StaffSearchViewQuery"/> class.
		/// </summary>
		/// <param name="repository">The repository.</param>
		/// <param name="criteria">The criteria.</param>
		public StaffSearchViewQuery(ICoreRepository repository, StaffCriteria criteria) : base(repository, criteria) { }

		/// <summary>
		/// Produces a query for this type.
		/// </summary>
		/// <returns></returns>
		public override IQueryable<StaffSearchView> Query()
		{
			var query = Repository.StaffSearchViews;


			if (Criteria.OfficeId.HasValue)
			{
				query = from staff in query
								join pom in Repository.Query<PersonOfficeMapper>() on staff.PersonId equals pom.PersonId
								where pom.OfficeId == Criteria.OfficeId.Value
								select staff;
			}

			if (Criteria.Firstname.IsNotNullOrEmpty())
				query = query.Where(x => x.FirstName.Contains(Criteria.Firstname));

			if (Criteria.Lastname.IsNotNullOrEmpty())
				query = query.Where(x => x.LastName.Contains(Criteria.Lastname));

			if (Criteria.EmailAddress.IsNotNullOrEmpty())
				query = query.Where(x => x.EmailAddress.Contains(Criteria.EmailAddress));

			if (Criteria.IsEnabled.HasValue)
				query = query.Where(x => x.Enabled == Criteria.IsEnabled.Value);

			if (Criteria.UserId.HasValue)
				query = query.Where(x => x.Id == Criteria.UserId);

			if (Criteria.Blocked.HasValue)
				query = query.Where(x => x.Blocked == Criteria.Blocked.Value);

			return query;
		}
	}
}
