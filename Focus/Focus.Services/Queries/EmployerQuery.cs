﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Linq;

using Focus.Data.Core.Entities;
using Focus.Core.Criteria.Employer;
using Focus.Data.Repositories.Contracts;

using Framework.Core;

#endregion

namespace Focus.Services.Queries
{
  public class EmployerQuery : QueryBase<Employer, ICoreRepository, EmployerCriteria>
  {
    /// <summary>
    /// Initializes a new instance of the <see cref="EmployerQuery"/> class.
    /// </summary>
    /// <param name="repository">The repository.</param>
    /// <param name="criteria">The criteria.</param>
    public EmployerQuery(ICoreRepository repository, EmployerCriteria criteria) : base(repository, criteria) { }

    /// <summary>
    /// Produces a query for this type.
    /// </summary>
    /// <returns></returns>
    public override IQueryable<Employer> Query()
    {
      var query = Repository.Employers;

      if (Criteria.EmployerName.IsNotNullOrEmpty()) 
        query = query.Where(e => e.Name == Criteria.EmployerName);

      return query;
    }
  }
}
