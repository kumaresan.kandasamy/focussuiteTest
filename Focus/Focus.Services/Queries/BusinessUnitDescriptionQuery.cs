﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Linq;
using Focus.Core.Criteria.BusinessUnitDescription;
using Focus.Data.Core.Entities;
using Focus.Data.Repositories.Contracts;

#endregion


namespace Focus.Services.Queries
{
	public class BusinessUnitDescriptionQuery : QueryBase <BusinessUnitDescription, ICoreRepository, BusinessUnitDescriptionCriteria>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="BusinessUnitDescriptionQuery"/> class.
		/// </summary>
		/// <param name="repository">The repository.</param>
		/// <param name="criteria">The criteria.</param>
		public BusinessUnitDescriptionQuery(ICoreRepository repository, BusinessUnitDescriptionCriteria criteria) : base(repository, criteria) {}

		/// <summary>
		/// Produces a query for this type.
		/// </summary>
		/// <returns></returns>
		public override IQueryable<BusinessUnitDescription> Query()
		{
			var query = Repository.Query<BusinessUnitDescription>();

			if (Criteria.BusinessUnitDescriptionId.HasValue)
				query = query.Where(x => x.Id == Criteria.BusinessUnitDescriptionId);

			if (Criteria.BusinessUnitId.HasValue)
				query = query.Where(x => x.BusinessUnitId == Criteria.BusinessUnitId);

			if (Criteria.IsPrimary)
				query = query.Where(x => x.IsPrimary == Criteria.IsPrimary);

      if (Criteria.AssociatedWithJob.HasValue)
      {
        var descriptionsInJobs = Repository.Jobs.Where(x => x.BusinessUnitId == Criteria.BusinessUnitId)
                                                  .Select(x => x.BusinessUnitDescriptionId)
                                                  .Distinct()
                                                  .ToList();

        query = Criteria.AssociatedWithJob.Value
          ? query.Where(x => descriptionsInJobs.Contains(x.Id))
          : query.Where(x => !descriptionsInJobs.Contains(x.Id));
      }

			return query.OrderBy(x => x.Title);
		}

		/// <summary>
		/// Queries the entity id.
		/// </summary>
		/// <returns></returns>
		public override BusinessUnitDescription QueryEntityId()
		{
			return Repository.BusinessUnitDescriptions.SingleOrDefault(x => x.Id == Criteria.BusinessUnitDescriptionId);
		}
	}
}