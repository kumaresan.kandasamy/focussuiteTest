﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Linq;
using Focus.Core.Criteria.NoteReminder;
using Focus.Data.Core.Entities;
using Focus.Data.Repositories.Contracts;
using Framework.Core;

#endregion

namespace Focus.Services.Queries
{
	public class NoteReminderViewQuery : QueryBase <NoteReminderView, ICoreRepository, NoteReminderCriteria>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="NoteReminderViewQuery"/> class.
		/// </summary>
		/// <param name="repository">The repository.</param>
		/// <param name="criteria">The criteria.</param>
		public NoteReminderViewQuery(ICoreRepository repository, NoteReminderCriteria criteria) : base(repository, criteria) {}

		/// <summary>
		/// Queries this instance.
		/// </summary>
		/// <returns></returns>
		public override IQueryable<NoteReminderView> Query()
		{
			var query = Repository.NoteReminderViews;

			if (Criteria.Id.HasValue)
				query = query.Where(x => x.Id == Criteria.Id);

			if (Criteria.EntityId.HasValue)
				query = query.Where(x => x.EntityId == Criteria.EntityId);

			if (Criteria.NoteReminderType.IsNotNull())
				query = query.Where(x => x.NoteReminderType == Criteria.NoteReminderType);

			if (Criteria.EntityType.IsNotNull())
				query = query.Where(x => x.EntityType == Criteria.EntityType);

      if (Criteria.NoteType.IsNotNull())
        query = query.Where(x => x.NoteType == Criteria.NoteType);

			query = query.OrderByDescending(x => x.CreatedOn);

			return query;
		}

		/// <summary>
		/// Queries the entity id.
		/// </summary>
		/// <returns></returns>
		public override NoteReminderView QueryEntityId()
		{
			return Repository.FindById<NoteReminderView>(Criteria.Id);
		}
	}
}
