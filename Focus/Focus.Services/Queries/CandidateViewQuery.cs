﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;

using Focus.Core;
using Focus.Core.Criteria.JobSeeker;
using Focus.Data.Core.Entities;
using Focus.Data.Repositories.Contracts;
using Focus.Services.Core;

using Framework.Core;

#endregion

namespace Focus.Services.Queries
{
  internal class CandidateViewQuery : QueryBase <CandidateView, ICoreRepository, JobSeekerCriteria>
  {
    /// <summary>
    /// Initializes a new instance of the <see cref="CandidateIssueQuery" /> class.
    /// </summary>
    /// <param name="repository">The repository.</param>
    /// <param name="criteria">The criteria.</param>
    public CandidateViewQuery(ICoreRepository repository, JobSeekerCriteria criteria) : base(repository, criteria)
    {
    }

    /// <summary>
    /// Queries this instance.
    /// </summary>
    /// <returns></returns>
    public override IQueryable<CandidateView> Query()
    {
	    var query = Repository.CandidateViews;

      if (Criteria.EmailAddress.IsNotNullOrEmpty())
        query = query.Where(x => x.EmailAddress.Contains(Criteria.EmailAddress));

      return query;
    }

    /// <summary>
    /// Queries the entity id.
    /// </summary>
    /// <returns></returns>
    public override CandidateView QueryEntityId()
    {
      throw new NotImplementedException();
    }
  }
}
