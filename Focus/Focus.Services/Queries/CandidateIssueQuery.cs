﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Linq;
using System;

using Focus.Core;
using Focus.Core.Criteria.JobSeeker;
using Focus.Core.Settings.Interfaces;
using Focus.Data.Core.Entities;
using Focus.Data.Repositories.Contracts;
using Focus.Services.Core;

using Framework.Core;

#endregion

namespace Focus.Services.Queries
{
	internal class CandidateIssueQuery : QueryBase<CandidateIssueView, ICoreRepository, JobSeekerCriteria>
	{
		private readonly IAppSettings _settings;

		/// <summary>
		/// Initializes a new instance of the <see cref="CandidateIssueQuery" /> class.
		/// </summary>
		/// <param name="repository">The repository.</param>
		/// <param name="criteria">The criteria.</param>
		/// <param name="settings">The app settings.</param>
		public CandidateIssueQuery(ICoreRepository repository, JobSeekerCriteria criteria, IAppSettings settings)
			: base(repository, criteria)
		{
			_settings = settings;
		}

		/// <summary>
		/// Queries this instance.
		/// </summary>
		/// <returns></returns>
		public override IQueryable<CandidateIssueView> Query()
		{
			var query = Repository.Query<CandidateIssueView>();

			if (Criteria.Firstname.IsNotNullOrEmpty())
				query = query.Where(x => x.FirstName.Contains(Criteria.Firstname));

			if (Criteria.Lastname.IsNotNullOrEmpty())
				query = query.Where(x => x.LastName.Contains(Criteria.Lastname));

			if (Criteria.Username.IsNotNullOrEmpty())
				query = query.Where(x => x.UserName.Contains(Criteria.Username));

			if (Criteria.EmailAddress.IsNotNullOrEmpty())
				query = query.Where(x => x.EmailAddress.Contains(Criteria.EmailAddress));

			if (Criteria.DateOfBirth.HasValue)
				query = query.Where(x => x.DateOfBirth == Criteria.DateOfBirth.Value);

			if (Criteria.Ssn.IsNotNullOrEmpty())
				query = query.Where(x => x.SocialSecurityNumber == Criteria.Ssn);

			if (Criteria.ExternalId.IsNotNullOrEmpty())
				query = query.Where(x => x.ExternalId == Criteria.ExternalId);

			if (Criteria.OfficeIds.IsNotNullOrEmpty() && Criteria.OfficeIds.Count > 0)
			{
				query = query.Where(
					q => Repository.PersonOfficeMappers.Where(mapper => Criteria.OfficeIds.Contains(mapper.OfficeId)).Select(
						mapper => mapper.PersonId).Distinct().Contains(q.Id));
			}

			if (Criteria.StaffMemberId.IsNotNull() && Criteria.StaffMemberId > 0)
				query = query.Where(x => x.AssignedToId == Criteria.StaffMemberId);

			if (Criteria.VeteranType.IsNotNull() && Criteria.VeteranType != VeteranFilterTypes.None)
			{
				if (Criteria.VeteranType == VeteranFilterTypes.Veteran) query = query.Where(x => x.IsVeteran == true);
				if (Criteria.VeteranType == VeteranFilterTypes.NonVeteran)
					query = query.Where(x => x.IsVeteran == false || x.IsVeteran == null);
			}

			if (Criteria.JobSeekerFilterType.IsNotNull() && Criteria.JobSeekerFilterType != JobSeekerFilterType.None)
			{
				if (Criteria.JobSeekerFilterType == JobSeekerFilterType.Veteran) query = query.Where(x => x.IsVeteran == true);
				if (Criteria.JobSeekerFilterType == JobSeekerFilterType.NonVeteran)
					query = query.Where(x => x.IsVeteran == false || x.IsVeteran == null);
				if (Criteria.JobSeekerFilterType == JobSeekerFilterType.MSFWVerified)
					query = query.Where(x => x.MigrantSeasonalFarmWorkerVerified == true);
                if (Criteria.JobSeekerFilterType == JobSeekerFilterType.UnderAge)
                {
                    var year = DateTime.Now.Year - _settings.UnderAgeJobSeekerRestrictionThreshold;
                    query = query.Where(x => x.DateOfBirth.Value.Year > year);
                }
			}

			if (Criteria.JobSeekerUserStatus.IsNotNull())
				query = query.Where(x => x.Enabled == Criteria.JobSeekerUserStatus);

			if (Criteria.JobSeekerBlockedStatus.IsNotNull())
				query = query.Where(x => x.Blocked == Criteria.JobSeekerBlockedStatus);

			if (!(Criteria.SsnList.IsNullOrEmpty() && Criteria.ExternalSeekerIdList.IsNullOrEmpty() &&
				  Criteria.PersonIds.IsNullOrEmpty()))
			{
				var searchOnSsn = true;
				if (Criteria.SsnList.IsNullOrEmpty())
				{
					searchOnSsn = false;
					Criteria.SsnList = new List<string> {""};
				}

				var searchOnExternal = true;
				if (Criteria.ExternalSeekerIdList.IsNullOrEmpty())
				{
					searchOnExternal = false;
					Criteria.ExternalSeekerIdList = new List<string> {""};
				}

				var searchOnPerson = true;
				if (Criteria.PersonIds.IsNullOrEmpty())
				{
					searchOnPerson = false;
					Criteria.PersonIds = new List<long> {0};
				}

				query = query.Where(x => (searchOnSsn && Criteria.SsnList.Contains(x.SocialSecurityNumber))
				                         || (searchOnExternal && Criteria.ExternalSeekerIdList.Contains(x.ExternalId))
				                         || (searchOnPerson && Criteria.PersonIds.Contains(x.Id)
					                         )
					);
			}

			if (Criteria.JobSeekerGroupedIssueSearch == true)
			{
				// Any issue
				query =
					query.Where(
						x =>
							(x.NoLoginTriggered && _settings.NotLoggingInEnabled)
							|| (x.JobOfferRejectionTriggered && _settings.RejectingJobOffersEnabled)
							|| (x.NotReportingToInterviewTriggered && _settings.NotReportingForInterviewEnabled)
							|| (x.NotClickingOnLeadsTriggered && _settings.NotClickingLeadsEnabled)
							|| (x.NotRespondingToEmployerInvitesTriggered && _settings.NotRespondingToEmployerInvitesEnabled)
							|| (x.ShowingLowQualityMatchesTriggered && _settings.ShowingLowQualityMatchesEnabled)
							|| (x.PostingLowQualityResumeTriggered && _settings.PostingLowQualityResumeEnabled)
							|| (x.NotSearchingJobsTriggered && _settings.NotSearchingJobsEnabled)
							|| (x.FollowUpRequested && _settings.FollowUpEnabled)
							|| (x.PostHireFollowUpTriggered && _settings.PostHireFollowUpEnabled)
							|| (x.InappropriateEmailAddress == true && _settings.InappropriateEmailAddressCheckEnabled)
							|| (x.GivingNegativeFeedback == true && _settings.NegativeFeedbackCheckEnabled)
							|| (x.GivingPositiveFeedback == true && _settings.PositiveFeedbackCheckEnabled)
							|| (x.MigrantSeasonalFarmWorkerTriggered && _settings.JobIssueFlagPotentialMSFW)
							|| (x.HasExpiredAlienCertificationRegistration == true && _settings.ExpiredAlienCertificationEnabled));
			}
			else if (Criteria.JobSeekerGroupedIssueSearch == false)
			{
				// No issues
				query =
					query.Where(
						x =>
							 (!x.NoLoginTriggered || !_settings.NotLoggingInEnabled)
							 && (!x.JobOfferRejectionTriggered || !_settings.RejectingJobOffersEnabled)
							 && (!x.NotReportingToInterviewTriggered || !_settings.NotReportingForInterviewEnabled)
							 && (!x.NotClickingOnLeadsTriggered || !_settings.NotClickingLeadsEnabled)
							 && (!x.NotRespondingToEmployerInvitesTriggered || !_settings.NotRespondingToEmployerInvitesEnabled)
							 && (!x.ShowingLowQualityMatchesTriggered || !_settings.ShowingLowQualityMatchesEnabled)
							 && (!x.PostingLowQualityResumeTriggered || !_settings.PostingLowQualityResumeEnabled)
							 && (!x.NotSearchingJobsTriggered || !_settings.NotSearchingJobsEnabled)
							 && (!x.FollowUpRequested || !_settings.FollowUpEnabled)
							 && (!x.PostHireFollowUpTriggered || !_settings.PostHireFollowUpEnabled)
							 && (x.MigrantSeasonalFarmWorkerTriggered == false || !_settings.JobIssueFlagPotentialMSFW)
							 && (!(x.GivingNegativeFeedback ?? false) || !_settings.NegativeFeedbackCheckEnabled)
							 && (!(x.GivingPositiveFeedback ?? false) || !_settings.PositiveFeedbackCheckEnabled)
							 && (!(x.InappropriateEmailAddress ?? false) || !_settings.InappropriateEmailAddressCheckEnabled)
							 && (!(x.HasExpiredAlienCertificationRegistration ?? false) || !_settings.ExpiredAlienCertificationEnabled) );
					// Handle no issues record
			}
			else if (Criteria.JobSeekerIssue.IsNotNull())
			{
				// Specific issue
				if (Criteria.JobSeekerIssue.Value == CandidateIssueType.NotLoggingIn)
				{
					query = query.Where(
						x => x.NoLoginTriggered);
				}

				if (Criteria.JobSeekerIssue.Value == CandidateIssueType.NotClickingLeads)
					query = query.Where(x => x.NotClickingOnLeadsTriggered);

				if (Criteria.JobSeekerIssue.Value == CandidateIssueType.RejectingJobOffers)
					query = query.Where(x => x.JobOfferRejectionTriggered);

				if (Criteria.JobSeekerIssue.Value == CandidateIssueType.NotReportingForInterviews)
					query = query.Where(x => x.NotReportingToInterviewTriggered);

				if (Criteria.JobSeekerIssue.Value == CandidateIssueType.NotRespondingToEmployerInvites)
					query = query.Where(x => x.NotRespondingToEmployerInvitesTriggered);

				if (Criteria.JobSeekerIssue.Value == CandidateIssueType.ShowingLowQualityMatches)
					query = query.Where(x => x.ShowingLowQualityMatchesTriggered);

				if (Criteria.JobSeekerIssue.Value == CandidateIssueType.PoorQualityResume)
					query = query.Where(x => x.PostingLowQualityResumeTriggered);

				if (Criteria.JobSeekerIssue.Value == CandidateIssueType.SuggestingPostHireFollowUp)
					query = query.Where(x => x.PostHireFollowUpTriggered);

				if (Criteria.JobSeekerIssue.Value == CandidateIssueType.RequiringFollowUp)
					query = query.Where(x => x.FollowUpRequested);

				if (Criteria.JobSeekerIssue.Value == CandidateIssueType.NotSearchingJobs)
					query = query.Where(x => x.NotSearchingJobsTriggered);

				if (Criteria.JobSeekerIssue.Value == CandidateIssueType.InappropriateEmailAddress)
					query = query.Where(x => x.InappropriateEmailAddress == true);

				if (Criteria.JobSeekerIssue.Value == CandidateIssueType.GivingNegativeFeedback)
					query = query.Where(x => x.GivingNegativeFeedback == true);

				if (Criteria.JobSeekerIssue.Value == CandidateIssueType.GivingPositiveFeedback)
					query = query.Where(x => x.GivingPositiveFeedback == true);

				if (Criteria.JobSeekerIssue.Value == CandidateIssueType.PotentialMSFW)
					query = query.Where(x => x.MigrantSeasonalFarmWorkerTriggered);

				if( Criteria.JobSeekerIssue.Value == CandidateIssueType.ExpiredAlienCertification )
					query = query.Where( x => x.HasExpiredAlienCertificationRegistration == true );
			}

			#region Sorting

			switch (Criteria.OrderBy)
			{
				case Constants.SortOrders.JobSeekerNameAsc:
					query = query.OrderBy(x => x.LastName).ThenBy(x => x.FirstName);
					break;
				case Constants.SortOrders.JobSeekerNameDesc:
					query = query.OrderByDescending(x => x.LastName).ThenByDescending(x => x.FirstName);
					break;
				case Constants.SortOrders.LocationAsc:
					query = query.OrderBy(x => x.TownCity);
					break;
				case Constants.SortOrders.LocationDesc:
					query = query.OrderByDescending(x => x.TownCity);
					break;
				case Constants.SortOrders.LastActivityAsc:
					query = query.OrderBy(x => x.LastLoggedInOn);
					break;
				case Constants.SortOrders.LastActivityDesc:
					query = query.OrderByDescending(x => x.LastLoggedInOn);
					break;
				default:
					query = query.OrderBy(x => x.FirstName);
					break;
			}

			#endregion

			return query;
		}

		/// <summary>
		/// Queries the entity id.
		/// </summary>
		/// <returns></returns>
		public override CandidateIssueView QueryEntityId()
		{
			var candidate = Repository.FindById<CandidateIssueView>(Criteria.PersonId);
			return candidate;
		}
	}
}
