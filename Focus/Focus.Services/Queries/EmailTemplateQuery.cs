﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Linq;
using Focus.Core.Criteria.EmailTemplate;
using Focus.Data.Configuration.Entities;
using Focus.Data.Repositories.Contracts;

#endregion



namespace Focus.Services.Queries
{
	public class EmailTemplateQuery : QueryBase <EmailTemplate, IConfigurationRepository, EmailTemplateCriteria>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="EmailTemplateQuery"/> class.
		/// </summary>
		/// <param name="repository">The repository.</param>
		/// <param name="criteria">The criteria.</param>
		public EmailTemplateQuery(IConfigurationRepository repository, EmailTemplateCriteria criteria) : base(repository, criteria) { }

		/// <summary>
		/// Queries this instance.
		/// </summary>
		/// <returns></returns>
		public override IQueryable<EmailTemplate> Query()
		{
			var query = Repository.EmailTemplates;

			if (Criteria.Id.HasValue)
				query = query.Where(x => x.Id == Criteria.Id);

			if (Criteria.EmailTemplateType.HasValue)
				query = query.Where(x => x.EmailTemplateType == Criteria.EmailTemplateType);

			return query;
		}

		/// <summary>
		/// Queries the entity id.
		/// </summary>
		/// <returns></returns>
		public override EmailTemplate QueryEntityId()
		{
			return Repository.FindById<EmailTemplate>(Criteria.Id);
		}
	}
}
