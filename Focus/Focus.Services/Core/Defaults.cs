﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using Focus.Core;
using Focus.Core.Settings;

#endregion



namespace Focus.Services.Core
{
	public class Defaults
	{
		public static string CountryCode = "US";
		public static string Customer = "BGT";
		public static string CustomerCenterCode = "BGT001";
		public static string CustomerRepresentativeId = "1";

    // TODO: change back
    public static string Culture = "EN-US";

		public class ConfigurationItemDefaults
		{
			public static string WebServiceCommandVersion = "2";
			public static int MaximumAllowedResumeCount = 5;
			public static int SOCCount = 5;
		  public static int MinimumScoreForClarify = 500;
			public static int MaximumSearchCountForClarify = 20;
			public static int MinimumSearchScore;
			public static int MaxiumSearchDocumentCount = 200;
		  public static int MaximumNoDocumentToReturnInJobAlert = 200;
      public static int PostingAgeInDays = 7;
      public static int PostingAgeInDaysForAssist = 30;
      public static int CacheSizeForPrefixDomain = 100;
			public static string DefaultCountryKey = "Country.US";
			public static string DefaultStateKey = "State.MA";
			public static string DefaultSearchRadiusKey = "Radius.TwentyFiveMiles";
		  public static int MaximumFailedPasswordAttempts = 3;

			public static DrivingLicenceEndorsementRuleList DefaultDrivingLicenceEndorsementRules = new DrivingLicenceEndorsementRuleList
			{
				new DrivingLicenceEndorsementRule
				{
					LicenceKey = "ClassD",
					EndorsementKeys = new EndorsementKeyList {"Motorcycle", "SchoolBus"}
				},
				new DrivingLicenceEndorsementRule
				{
					LicenceKey = "Motorcycle",
					EndorsementKeys = new EndorsementKeyList {"Motorcycle"}
				},
				new DrivingLicenceEndorsementRule
				{
					LicenceKey = "ClassA",
					EndorsementKeys = new EndorsementKeyList {"PassTransport", "DoublesTriples", "HazerdousMaterials", "Airbrakes", "TankVehicle", "Motorcycle", "SchoolBus", "TankHazard"}
				},
				new DrivingLicenceEndorsementRule
				{
					LicenceKey = "ClassB",
					EndorsementKeys = new EndorsementKeyList {"PassTransport", "DoublesTriples", "HazerdousMaterials", "Airbrakes", "TankVehicle", "Motorcycle", "SchoolBus", "TankHazard"}
				},
				new DrivingLicenceEndorsementRule
				{
					LicenceKey = "ClassC",
					EndorsementKeys = new EndorsementKeyList {"PassTransport", "DoublesTriples", "HazerdousMaterials", "Airbrakes", "TankVehicle", "Motorcycle", "SchoolBus", "TankHazard"}
				}
			};
		}

		public class ResumeDefaults
		{
			public static string ResumeDisplayStylePath = @"~\Assets\Xslts\FocusCareerFormat.xsl";
		}

		public class JobPostingDefaults
		{
			public static string JobPostingStylePath = @"~\Assets\Xslts\JobPosting.xsl";
		}

		public class DomainValueDefaults
		{
			public static int SalaryUnitCode = 6;
		}
	}
}
