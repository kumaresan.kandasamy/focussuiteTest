﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;

using Focus.Core;
using Focus.Core.Models.Validation;

#endregion

namespace Focus.Services.Core.Validation
{
  [Serializable]
  public class BooleanFieldValidator : FieldValidator
  {
    /// <summary>
    /// Default Constructor
    /// </summary>
		public BooleanFieldValidator()
    {
      Validators.AddRange(new List<ValidationMethod>
      {
        ValidateMaxValue,
        ValidateMinValue
      });
    }

	  public override FieldValidationResult ValidateUniqueness(int fieldNumber, string value, HashSet<object> fieldValues)
	  {
      var validationResult = new FieldValidationResult
      {
        FieldNumber = fieldNumber,
        Status = ValidationRecordStatus.Success
      };

			// uniqueness no supported
      return validationResult;
	  }

		public override string GetSerializedValue( string value )
		{
			var serializedValue = ParseValue(value);
			return serializedValue == null ? "" : serializedValue.ToString().ToLower();
		}

	  /// <summary>
    /// Validates a value for the field
    /// </summary>
    /// <param name="value">The value of the field</param>
    /// <returns>Validation results</returns>
    public override string ValidateFormat(string value)
    {
			if( value.Length == 0 && !Mandatory )
				return null;

			return ParseValue( value ) == null ? Localiser.Localise( "Global.ValidationResources.IncorrectFormat", "Invalid format" ) : null;
    }

	  private static bool? ParseValue(string value)
	  {
			switch( value.ToLower() )
			{
				case "true":
				case "yes":
				case "y":
					return true;
				case "false":
				case "no":
				case "n":
					return false;
			}
		  return null;
	  }
  }
}
