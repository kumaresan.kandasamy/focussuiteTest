﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Xml.Serialization;

using Focus.Core;
using Focus.Core.Models.Validation;
using Focus.Core.Settings.Interfaces;
using Focus.Data.Library.Entities;
using Focus.Data.Repositories.Contracts;
using Focus.Services.Helpers;

using Framework.Core;

#endregion

namespace Focus.Services.Core.Validation
{
  public abstract class FieldValidator
  {
    private Regex _format;
    private decimal? _maxDecimalValue;
    private decimal? _minDecimalValue;

    [XmlAttribute("displayname")]
    public string DisplayName { get; set; }

    [XmlAttribute("fieldname")]
    public string FieldName { get; set; }

    [XmlAttribute("mandatory")]
    public bool Mandatory { get; set; }

    [XmlAttribute("maxlength")]
    public int MaxLength { get; set; }

    [XmlAttribute("minlength")]
    public int MinLength { get; set; }

    [XmlAttribute("maxvalue")]
    public string MaxValueSetting { get; set; }

    [XmlAttribute("minvalue")]
    public string MinValueSetting { get; set; }

    [XmlAttribute("unique")]
    public bool Unique { get; set; }

    [XmlAttribute("custom")]
    public string CustomValidatorScript { get; set; }

    [XmlAttribute("format")]
    public string FormatPattern { get; set; }

    [XmlAttribute("lookuptable")]
    public UploadLookupTable LookupTable { get; set; }

    [XmlIgnore]
    public MethodInfo CustomMethod { get; set; }

    [XmlIgnore]
    protected decimal MaxValue
    {
      get
      {
        if (_maxDecimalValue.IsNull())
          _maxDecimalValue = decimal.Parse(ParseValue(MaxValueSetting));

        return _maxDecimalValue.Value;
      }
    }

    [XmlIgnore]
    protected decimal MinValue
    {
      get
      {
        if (_minDecimalValue.IsNull())
          _minDecimalValue = decimal.Parse(ParseValue(MinValueSetting));

        return _minDecimalValue.Value;
      }
    }

    [XmlIgnore]
    protected Regex Format
    {
      get
      {
        if (_format.IsNull())
        {
          if (FormatPattern.IsNotNullOrEmpty())
          {
            var pattern = FormatPattern;

            if (!pattern.StartsWith("^"))
              pattern = string.Concat("^", pattern);

            if (!pattern.EndsWith("$"))
              pattern = string.Concat(pattern, "$");

            _format = new Regex(pattern);
          }
          else
          {
            _format =  new Regex("^.*$");
          }
        }

        return _format;
      }
    }

    [XmlIgnore]
    protected HashSet<string> Lookup;
    
    [XmlIgnore]
    protected List<ValidationMethod> Validators { get; set; }

    [XmlIgnore]
    public ILocalisationHelper Localiser { get; set; }

    [XmlIgnore]
    public ILibraryRepository LibraryRepository { get; set; }

    [XmlIgnore]
    public IAppSettings Settings { get; set; }

    protected delegate string ValidationMethod(string value);

    /// <summary>
    /// Default constructor
    /// </summary>
    protected FieldValidator()
    {
      Mandatory = false;
      Unique = false;
      MaxLength = int.MaxValue;
      MinLength = 0;

      Validators = new List<ValidationMethod>
      {
        ValidateMandatory,
        ValidateFormat
      };
    }

    /// <summary>
    /// Validates a value for the field
    /// </summary>
    /// <param name="fieldNumber">The field number in the CSV file</param>
    /// <param name="value">The value of the field</param>
    /// <returns>Validation results</returns>
    public FieldValidationResult Validate(int fieldNumber, string value)
    {
      var validationResult = new FieldValidationResult
      {
        FieldNumber = fieldNumber,
        Status = ValidationRecordStatus.Success,
        OriginalValue = value
      };

      if (Validators.IsNotNull())
      {
        var validatorCount = Validators.Count;
        var validatorIndex = 0;
        while (validationResult.Status == ValidationRecordStatus.Success && validatorIndex < validatorCount)
        {
          var validator = Validators[validatorIndex];
          var message = validator(value);
          if (message.IsNotNull())
          {
            validationResult.Status = ValidationRecordStatus.SchemaFailure;
            validationResult.Message = message;
          }

          validatorIndex++;
        }
      }

      return validationResult;
    }

    /// <summary>
    /// Gets the serialized value
    /// </summary>
    /// <param name="value">The value to serialize</param>
    /// <returns></returns>
    public virtual string GetSerializedValue(string value)
    {
      return value;
    }

    /// <summary>
    /// Performs any custom validation for the field
    /// </summary>
    /// <param name="fieldNumber">The field number in the CSV file</param>
    /// <param name="values">A list of all field values in the row</param>
    /// <param name="customValidatorInstance">The instance of the compiled custom validator object</param>
    /// <returns>Validation results</returns>
    public FieldValidationResult CustomValidate(int fieldNumber, List<string> values, object customValidatorInstance)
    {
      var validationResult = new FieldValidationResult
      {
        FieldNumber = fieldNumber,
        Status = ValidationRecordStatus.Success
      };

      if (CustomMethod.IsNotNull())
      {
        var message = (string) CustomMethod.Invoke(customValidatorInstance, new object[] { fieldNumber - 1, values });
        if (message.IsNotNull())
        {
          validationResult.Status = ValidationRecordStatus.SchemaFailure;
          validationResult.Message = message;
        }
      }

      return validationResult;
    }

    /// <summary>
    /// Validates the value in the field is mandatory
    /// </summary>
    /// <param name="value">The value to validate</param>
    /// <returns>A string with any validation message (null if valid)</returns>
    public string ValidateMandatory(string value)
    {
      return (value.Length > 0 || !Mandatory)
        ? null
        : Localiser.Localise("Global.ValidationResources.Mandatory", "Field is mandatory");
    }

    /// <summary>
    /// Validates the value in the field is mandatory
    /// </summary>
    /// <param name="value">The value to validate</param>
    /// <returns>A string with any validation message (null if valid)</returns>
    public virtual string ValidateFormat(string value)
    {
      if (value.Length == 0 && !Mandatory)
        return null;

      return (Format.IsNull() || Format.IsMatch(value))
        ? null
        : Localiser.Localise("Global.ValidationResources.IncorrectFormat", "Invalid format");
    }

    /// <summary>
    /// Validates the maximum length of the value in the field
    /// </summary>
    /// <param name="value">The value to validate</param>
    /// <returns>A string with any validation message (null if valid)</returns>
    public virtual string ValidateMaxLength(string value)
    {
      return (MaxLength.IsNull() || value.Length <= MaxLength)
        ? null
        : Localiser.Localise("Global.ValidationResources.MaxLengthExceeded", "Field exceeds the maximum length of {0}", MaxLength);
    }

    /// <summary>
    /// Validates the minimum length of the value in the field
    /// </summary>
    /// <param name="value">The value to validate</param>
    /// <returns>A string with any validation message (null if valid)</returns>
    public virtual string ValidateMinLength(string value)
    {
      return (MinLength.IsNull() || value.Length >= MinLength)
        ? null
        : Localiser.Localise("Global.ValidationResources.MinLengthNotReached", "Field is shorter than the minimum length of {0}", MinLength);
    }

    /// <summary>
    /// Validates the maximum value of the value in the field
    /// </summary>
    /// <param name="value">The value to validate</param>
    /// <returns>A string with any validation message (null if valid)</returns>
    public virtual string ValidateMaxValue(string value)
    {
      if (MaxValueSetting.IsNullOrEmpty())
        return null;

      if (value.Length == 0 && !Mandatory)
        return null;

      var decimalValue = decimal.Parse(value);
      return (decimalValue <= MaxValue)
        ? null
        : Localiser.Localise("Global.ValidationResources.MaxValueExceeded", "Field exceeds the maximum value of {0}", MaxValue);
    }

    /// <summary>
    /// Validates the minimum value of the value in the field
    /// </summary>
    /// <param name="value">The value to validate</param>
    /// <returns>A string with any validation message (null if valid)</returns>
    public virtual string ValidateMinValue(string value)
    {
      if (MinValueSetting.IsNullOrEmpty())
        return null;

      if (value.Length == 0 && !Mandatory)
        return null;

      var decimalValue = decimal.Parse(value);
      return (decimalValue >= MinValue)
        ? null
        : Localiser.Localise("Global.ValidationResources.MinValueNotReached", "Field is less than the minimum value of {0}", MinValue);
    }

    /// <summary>
    /// Validates the value in a lookup table in the database
    /// </summary>
    /// <param name="value">The value to validate</param>
    /// <returns>A string with any validation message (null if valid)</returns>
    public virtual string ValidateLookupTable(string value)
    {
      switch (LookupTable)
      {
        case UploadLookupTable.PostalCode:
          if (value.Contains("-"))
            value = value.SubstringBefore("-");

          var checkId = LibraryRepository.Query<PostalCode>()
                                         .Where(postalCode => postalCode.Code == value)
                                         .Select(postalCode => postalCode.Id)
                                         .FirstOrDefault();

          return (checkId != 0)
            ? null
            : Localiser.Localise("Global.ValidationResources.LookupTableNotExist", "Value is not a valid zip");
      }

      return null;
    }

    /// <summary>
    /// Validates the value in the field exists in a list of lookup values
    /// </summary>
    /// <param name="value">The value to validate</param>
    /// <returns>A string with any validation message (null if valid)</returns>
    public virtual string ValidateLookup(string value)
    {
      return (Lookup.IsNull() || Lookup.Contains(value.ToLowerInvariant()))
        ? null
        : Localiser.Localise("Global.ValidationResources.InvalidLookup", "Field does not contain expected value");
    }

    public abstract FieldValidationResult ValidateUniqueness(int fieldNumber, string value, HashSet<object> fieldValues);

    /// <summary>
    /// Parses the value in the validation schema as it might be a configurable value
    /// </summary>
    /// <param name="value">The value in the validation schema, which may begin with @config:</param>
    /// <returns>The parsed value</returns>
    private string ParseValue(string value)
    {
      if (value.StartsWith("@config:"))
      {
        var propertyName = value.SubstringAfter(":");
        var propertyValue = Settings.GetType().GetProperty(propertyName).GetValue(Settings, null);

        return propertyValue.ToString();
      }

      return value;
    }
  }
}
