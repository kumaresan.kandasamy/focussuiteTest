﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Xml.Serialization;

using Focus.Core;
using Focus.Core.Models.Validation;

using Framework.Core;

#endregion

namespace Focus.Services.Core.Validation
{
  [Serializable]
  public class DecimalFieldValidator : FieldValidator
  {
    [XmlAttribute("decimalplaces")]
    public short DecimalPlaces { get; set; }

    /// <summary>
    /// Default Constructor
    /// </summary>
    public DecimalFieldValidator()
    {
      FormatPattern = @"^\d*(.\d+)?$";
      DecimalPlaces = 10;

      Validators.AddRange(new List<ValidationMethod>
      {
        ValidateMaxValue,
        ValidateMinValue,
        ValidateScale
      });
    }

    /// <summary>
    /// Validates the number of decimal places
    /// </summary>
    /// <param name="value">The value to validate</param>
    /// <returns>The number of decimal places</returns>
    public string ValidateScale(string value)
    {
      if (DecimalPlaces.IsNull())
        return null;

      var decimalValue = decimal.Parse(value);

      var places = DecimalPlaces;
      while (places > 0)
      {
        decimalValue = decimalValue * 10;
        places--;
      }

      return (decimalValue == Math.Floor(decimalValue))
               ? null
               : Localiser.Localise("Global.ValidationResources.DecimalScaleExceeded", "Field must not have more the {0} decimal places", DecimalPlaces);
    }

    /// <summary>
    /// Validates a value for the field
    /// </summary>
    /// <param name="fieldNumber">The field number in the CSV file</param>
    /// <param name="value">The value of the field</param>
    /// <param name="fieldValues">Previous values for the field (used in uniqueness check)</param>
    /// <returns>Validation results</returns>
    public override FieldValidationResult ValidateUniqueness(int fieldNumber, string value, HashSet<object> fieldValues)
    {
      var validationResult = new FieldValidationResult
      {
        FieldNumber = fieldNumber,
        Status = ValidationRecordStatus.Success
      };

      if (!Unique)
        return validationResult;

      if (fieldValues.Contains(value))
      {
        validationResult.Status = ValidationRecordStatus.SchemaFailure;
        validationResult.Message = Localiser.Localise("Global.ValidationResources.FieldNotUnique", "Field is not unique");
      }

      fieldValues.Add(decimal.Parse(value));

      return validationResult;
    }
  }
}
