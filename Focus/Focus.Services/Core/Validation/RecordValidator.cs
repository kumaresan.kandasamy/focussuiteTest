﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using System.Xml.Serialization;

using Focus.Core;
using Focus.Core.Models.Upload;
using Focus.Core.Models.Validation;
using Focus.Core.Settings.Interfaces;
using Focus.Data.Repositories.Contracts;
using Focus.Services.Core.Censorship;
using Focus.Services.Helpers;

using Framework.Core;

using Microsoft.CSharp;

#endregion

namespace Focus.Services.Core.Validation
{
  [Serializable]
  public class RecordValidator
  {
    [XmlAttribute("profanitycheck")]
    public bool ProfanityCheck { get; set; }

    [XmlAttribute("criminalcheck")]
    public bool CriminalCheck { get; set; }

    [XmlAttribute("typename")]
    public string TypeName { get; set; }

    [XmlArray("fields"), XmlArrayItem("field", IsNullable = false)]
    public FieldValidator[] FieldValidators { get; set; }

    [XmlIgnore]
    public ILocalisationHelper Localiser { get; set; }

    [XmlIgnore]
    public ILibraryRepository LibraryRepository { get; set; }

    [XmlIgnore]
    public IAppSettings Settings { get; set; }

    [XmlIgnore]
    public Type RecordType
    {
      get { return _recordType ?? (_recordType = Type.GetType(TypeName)); }
      set { _recordType = value; }
    }

    private bool _customValidatorsBuilt;
    private object _customValidatorInstance;
    private Type _recordType;

    /// <summary>
    /// Validates a record in the CSV file
    /// </summary>
    /// <param name="lineNumber">The number of the line being validated</param>
    /// <param name="fields">A list of the fields in the record</param>
    /// <param name="uniqueFields">A record of fields for checking uniqueness</param>
    /// <param name="profanity">A list of censorship rules for profanities</param>
    /// <param name="criminal">A list of censorship rules for criminal background exclusion</param>
    /// <returns>Validation results</returns>
    public RecordValidationResult ValidateRecord(int lineNumber, List<string> fields, Dictionary<int, HashSet<object>> uniqueFields, CensorshipRuleList profanity, CensorshipRuleList criminal)
    {
      if (_customValidatorsBuilt)
      {
        var customFieldValidators = FieldValidators.Where(validator => !String.IsNullOrEmpty(validator.CustomValidatorScript)).ToList();
        if (customFieldValidators.Any())
          BuildCustomValidator(customFieldValidators);

        _customValidatorsBuilt = true;
      }

      var result = new RecordValidationResult
      {
        LineNumber = lineNumber,
        Status = ValidationRecordStatus.Success,
        FieldValidationResults = new List<FieldValidationResult>()
      };

      if (RecordType.IsNull())
        throw new Exception("Unknown type");

      var rootElement = new XElement(RecordType.Name);
      var uploadRecordXml = new XDocument(rootElement);

      var expectedFields = FieldValidators.Length;
      while (fields.Count < expectedFields)
      {
        fields.Add(string.Empty);
      }

      var index = 0;
      foreach (var field in fields.Take(expectedFields))
      {
        // Standard validation
        var validator = FieldValidators[index];

        if (validator.Localiser.IsNull())
          validator.Localiser = Localiser;

        if (validator.LibraryRepository.IsNull())
          validator.LibraryRepository = LibraryRepository;

        if (validator.Settings.IsNull())
          validator.Settings = Settings;

        var fieldResult = validator.Validate(index + 1, field);
        result.FieldValidationResults.Add(fieldResult);

        // Only add valid fields to record data
        if (fieldResult.Status == ValidationRecordStatus.Success && field.IsNotNullOrEmpty())
          rootElement.Add(new XElement(validator.FieldName, validator.GetSerializedValue(field)));

        // Custom validation
        if (fieldResult.Status == ValidationRecordStatus.Success && validator.CustomValidatorScript.IsNullOrEmpty())
        {
          var customResult = validator.CustomValidate(index + 1, fields, _customValidatorInstance);
          fieldResult.Message = customResult.Message;
          fieldResult.Status = customResult.Status;
        }

        // Uniqueness validation
        if (fieldResult.Status == ValidationRecordStatus.Success && validator.Unique)
        {
          var uniquenessResult = validator.ValidateUniqueness(index + 1, field, uniqueFields[index]);
          fieldResult.Message = uniquenessResult.Message;
          fieldResult.Status = uniquenessResult.Status;
        }

        // Censorship check
        if (fieldResult.Status == ValidationRecordStatus.Success)
        {
          if (ProfanityCheck)
          {
            var profanityResult = CheckProfanity(index + 1, field, profanity);
            fieldResult.Message = profanityResult.Message;
            fieldResult.Status = profanityResult.Status;
          }

          if (CriminalCheck)
          {
            var criminalResult = CheckCriminal(index + 1, field, criminal);
            fieldResult.Status |= criminalResult.Status;
            if (criminalResult.Message.IsNotNullOrEmpty())
              fieldResult.Message = fieldResult.Message.IsNotNullOrEmpty()
                                      ? string.Concat(fieldResult.Message, ", ", criminalResult.Message)
                                      : criminalResult.Message;
          }
        }

        index++;
      }

      using (var stream = new StringReader(uploadRecordXml.ToString()))
      {
        var serializer = new XmlSerializer(RecordType);
        result.UploadRecord = (IUploadedRecord)serializer.Deserialize(stream);

        stream.Close();
      }

      if (fields.Count > expectedFields)
      {
        result.Status = ValidationRecordStatus.SchemaFailure;
        result.Message = string.Format(Localiser.Localise("Global.ValidationResources.NumberOfFieldsExceeded", "Number of fields exceeds the expected number of {0}"), expectedFields);
      }

      result.Status |= result.FieldValidationResults.Select(field => field.Status).Aggregate((aggregated, current) => aggregated | current);

      return result;
    }

    /// <summary>
    /// Checks a field for red or yellow words
    /// </summary>
    /// <param name="fieldNumber">The number of the field to check</param>
    /// <param name="field">The field to check</param>
    /// <param name="censorshipRules">A list of censorship rules</param>
    /// <returns>Validation results</returns>
    public FieldValidationResult CheckProfanity(int fieldNumber, string field, CensorshipRuleList censorshipRules)
    {
      var result = new FieldValidationResult
      {
        FieldNumber = fieldNumber,
        Status = ValidationRecordStatus.Success
      };

      var censorshipCheck = new CensorshipCheck(censorshipRules, field);
      if (censorshipCheck.HasCensorships)
      {
        // Check for red words
        var redWords = censorshipCheck.FoundCensorshipRules.Where(x => x.Level == CensorshipLevel.Red).ToList();
        if (redWords.Count > 0)
        {
          var wordList = string.Join(",", redWords.Select(word => word.Phrase));

          result.Status = ValidationRecordStatus.RedWords;
          result.Message = Localiser.Localise("Global.ValidationResources.RedWords", "Field contains red words; {0}", wordList);
        }
        else
        {
          var yellowWords = censorshipCheck.FoundCensorshipRules.Where(x => x.Level == CensorshipLevel.Yellow).ToList();
          if (yellowWords.Count > 0)
          {
            var wordList = string.Join(",", yellowWords.Select(word => word.Phrase));

            result.Status = ValidationRecordStatus.YellowWords;
            result.Message = Localiser.Localise("Global.ValidationResources.YellowWords", "Field contains yellow words; {0}", wordList);
          }
        }
      }

      return result;
    }

    /// <summary>
    /// Checks a record for criminal background exclusion words
    /// </summary>
    /// <param name="fieldNumber">The number of the field to check</param>
    /// <param name="field">The field to check</param>
    /// <param name="censorshipRules">A list of censorship rules</param>
    /// <returns>Validation results</returns>
    public FieldValidationResult CheckCriminal(int fieldNumber, string field, CensorshipRuleList censorshipRules)
    {
      var result = new FieldValidationResult
      {
        FieldNumber = fieldNumber,
        Status = ValidationRecordStatus.Success
      };

      var censorshipCheck = new CensorshipCheck(censorshipRules, field);
      if (censorshipCheck.HasCensorships)
      {
        var wordList = string.Join(",", censorshipCheck.FoundCensorshipRules.Where(x => x.Level == CensorshipLevel.Match).Select(word => word.Phrase));

        result.Status = ValidationRecordStatus.CriminalBackgroundExclusion;
        result.Message = Localiser.Localise("Global.ValidationResources.CriminalBackgroundExclusion", "Field contains criminal background exclusion words; {0}", wordList);
      }

      return result;
    }

    /// <summary>
    /// Dynamically nuilds the custom validator using any custom scripts in the schema
    /// </summary>
    /// <param name="customFieldValidators"></param>
    private void BuildCustomValidator(List<FieldValidator> customFieldValidators)
    {
      var cp = new CompilerParameters();
      cp.ReferencedAssemblies.Add("system.dll");
      //cp.ReferencedAssemblies.Add(Assembly.GetExecutingAssembly().ManifestModule.FullyQualifiedName);

      cp.CompilerOptions = "/target:library /optimize";
      cp.GenerateExecutable = false;
      cp.GenerateInMemory = true;

      var sb = new StringBuilder();
      sb.AppendLine("using System;");
      sb.AppendLine("using System.Collections.Generic;");

      sb.AppendLine("namespace CSVValidator.Validators {");
      sb.AppendLine("public class CustomValidators {");

      foreach (var customField in customFieldValidators)
      {
        sb.AppendFormat("    public string {0}_CustomValidate(int fieldIndex, List<string> fields) {{\n", customField.FieldName.Replace(" ", ""));
        sb.AppendLine(customField.CustomValidatorScript);
        sb.AppendLine("}");
      }

      sb.AppendLine("}");
      sb.AppendLine("}");

      var compilerResult = (new CSharpCodeProvider()).CompileAssemblyFromSource(cp, sb.ToString());
      var errors = compilerResult.Errors;
      if (errors.Count > 0)
      {
        var error = new StringBuilder();
        for (var index = 0; index < errors.Count; index++)
          error.Append(index.ToString(CultureInfo.InvariantCulture)).Append(": ").AppendLine(errors[index].ToString());

        throw new Exception(Localiser.Localise("Global.ValidationResources.CustomValidatorCompilerError", "Error compiling custom validator"), new Exception(error.ToString()));
      }

      _customValidatorInstance = compilerResult.CompiledAssembly.CreateInstance("CSVValidator.Validators.CustomValidators");
      if (_customValidatorInstance.IsNull())
        throw new Exception(Localiser.Localise("Global.ValidationResources.CustomValidatorInstanceError", "Error creating custom validator instance"));

      var customValidatorType = _customValidatorInstance.GetType();
      foreach (var customField in customFieldValidators)
      {
        var methodName = string.Format("{0}_CustomValidate", customField.FieldName.Replace(" ", ""));
        customField.CustomMethod = customValidatorType.GetMethod(methodName);
      }
    }
  }
}
