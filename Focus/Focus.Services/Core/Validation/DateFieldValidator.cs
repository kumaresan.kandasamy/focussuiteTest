﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using System.Xml.Serialization;

using Focus.Core;
using Focus.Core.Models.Validation;

using Framework.Core;

#endregion

namespace Focus.Services.Core.Validation
{
  [Serializable]
  public class DateFieldValidator : FieldValidator
  {
    private CultureInfo _dateCulture;

    [XmlAttribute("culture")]
    public string Culture { get; set; }

    [XmlIgnore]
    public CultureInfo DateCulture
    {
      get
      {
        if (_dateCulture.IsNull())
          _dateCulture = Culture.IsNotNullOrEmpty()
                        ? CultureInfo.CreateSpecificCulture(Culture)
                        : Thread.CurrentThread.CurrentCulture;

        return _dateCulture;
      }
    }

    /// <summary>
    /// Default Constructor
    /// </summary>
    public DateFieldValidator()
    {
      Validators.AddRange(new List<ValidationMethod>
      {
        ValidateDateFormat,
        ValidateMaxValue,
        ValidateMinValue
      });
    }

    /// <summary>
    /// Gets the serialized value
    /// </summary>
    /// <param name="value">The value to serialize</param>
    /// <returns></returns>
    public override string GetSerializedValue(string value)
    {
      var dateTimeValue = ParseDate(value);

      return dateTimeValue.IsNotNull() ? dateTimeValue.Value.ToString("yyyy-MM-ddTHH:mm:ss.fffZ") : null;
    }

    /// <summary>
    /// Validates the date format
    /// </summary>
    /// <param name="value"></param>
    /// <returns></returns>
    public string ValidateDateFormat(string value)
    {
      if (value.Length == 0 && !Mandatory)
        return null;

      return ParseDate(value).IsNotNull()
        ? null
        : Localiser.Localise("Global.ValidationResources.IncorrectFormat", "Invalid format");
    }

    /// <summary>
    /// Validates a value for the field
    /// </summary>
    /// <param name="fieldNumber">The field number in the CSV file</param>
    /// <param name="value">The value of the field</param>
    /// <param name="fieldValues">Previous values for the field (used in uniqueness check)</param>
    /// <returns>Validation results</returns>
    public override FieldValidationResult ValidateUniqueness(int fieldNumber, string value, HashSet<object> fieldValues)
    {
      var validationResult = new FieldValidationResult
      {
        FieldNumber = fieldNumber,
        Status = ValidationRecordStatus.Success
      };

      if (!Unique)
        return validationResult;

      if (fieldValues.Contains(value))
      {
        validationResult.Status = ValidationRecordStatus.SchemaFailure;
        validationResult.Message = Localiser.Localise("Global.ValidationResources.FieldNotUnique", "Field is not unique");
      }

      fieldValues.Add(DateTime.Parse(value, DateCulture));

      return validationResult;
    }

    /// <summary>
    /// Validates the maximum value of the date field. 
    /// This will be based on a number to indicate number of days from today
    /// </summary>
    /// <param name="value">The value to validate</param>
    /// <returns>A string with any validation message (null if valid)</returns>
    public override string ValidateMaxValue(string value)
    {
      if (MaxValueSetting.IsNullOrEmpty())
        return null;

      if (value.Length == 0 && !Mandatory)
        return null;

      var maxDate = DateTime.Now.AddDays(Convert.ToInt32(MaxValue));

      var dateValue = ParseDate(value);
      return (dateValue.IsNotNull() && dateValue.Value <= maxDate)
        ? null
        : Localiser.Localise("Global.ValidationResources.MaxValueExceeded", "Field exceeds the maximum value of {0}", maxDate.ToString(DateCulture.DateTimeFormat.ShortDatePattern));
    }

    /// <summary>
    /// Validates the maximum value of the date field. 
    /// This will be based on a number to indicate number of days from today
    /// </summary>
    /// <param name="value">The value to validate</param>
    /// <returns>A string with any validation message (null if valid)</returns>
    public override string ValidateMinValue(string value)
    {
      if (MinValueSetting.IsNullOrEmpty())
        return null;

      if (value.Length == 0 && !Mandatory)
        return null;

      var minDate = DateTime.Now.AddDays(Convert.ToInt32(MinValue));

      var dateValue = ParseDate(value);
      return (dateValue.IsNotNull() && dateValue.Value >= minDate)
        ? null
        : Localiser.Localise("Global.ValidationResources.MinValueNotReached", "Field is less than the minimum value of {0}", minDate.ToString(DateCulture.DateTimeFormat.ShortDatePattern));
    }

    /// <summary>
    /// Parses a string into a date
    /// </summary>
    /// <param name="value">The string value of the date</param>
    /// <returns>The actual date value</returns>
    private DateTime? ParseDate(string value)
    {
      DateTime dateTimeValue;
      var valid = DateTime.TryParse(value, DateCulture, DateTimeStyles.None, out dateTimeValue);
      if (valid)
        return dateTimeValue;

      return null;
    }
  }
}
