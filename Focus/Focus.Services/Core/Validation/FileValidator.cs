﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Xml.Serialization;

using Focus.Core;
using Focus.Core.Models.Validation;
using Focus.Core.Settings.Interfaces;
using Focus.Data.Repositories.Contracts;
using Focus.Services.Core.Censorship;
using Focus.Services.Helpers;

using Framework.Core;

using Aspose.Cells;
using LoadOptions = Aspose.Cells.LoadOptions;

#endregion

namespace Focus.Services.Core.Validation
{
  [XmlRoot(Namespace = "", IsNullable = false, ElementName = "file")]
  public class FileValidator
  {
    [XmlIgnore]
    public ILocalisationHelper Localiser { get; set; }

    [XmlIgnore]
    public ILibraryRepository LibraryRepository { get; set; }

    [XmlIgnore]
    public IAppSettings Settings { get; set; }

    [XmlElement("record")]
    public RecordValidator RecordValidator { get; set; }

    /// <summary>
    /// Validates the file
    /// </summary>
    /// <param name="fileBytes">The bytes of the file to validate</param>
    /// <param name="fileType">The type of the file.</param>
    /// <param name="profanity">A list of profanity rules</param>
    /// <param name="criminal">A list of criminal background exclusion rules</param>
    /// <param name="ignoreHeader">Whether to ignore the first record</param>
    /// <returns>
    /// A list of validation messages for records in error
    /// </returns>
    public FileValidationResult Validate(byte[] fileBytes, UploadFileType fileType, CensorshipRuleList profanity, CensorshipRuleList criminal, bool ignoreHeader = true)
    {
      var fileData = new List<List<string>>();

      Workbook workbook;
      var stream = new MemoryStream(fileBytes);

      switch (fileType)
      {
        case UploadFileType.Excel:
          workbook = new Workbook(stream);
          break;

        case UploadFileType.CSV:
          var loadOptions = new LoadOptions(LoadFormat.CSV);
          workbook = new Workbook(stream, loadOptions);
          break;

        default:
          throw new Exception("Unhandled file type");
      }

     
      var worksheet = workbook.Worksheets[0];

      foreach (Row row in worksheet.Cells.Rows)
      {
        var fileRow = new List<String>();
        if (!row.IsBlank)
        {
          var cellCount = row.LastDataCell.Column;
          for (var cellIndex = 0; cellIndex <= cellCount; cellIndex++)
          {
            var cell = row.GetCellOrNull(cellIndex);
            if (fileType == UploadFileType.Excel && cell.Type == CellValueType.IsDateTime)
            {
              var dateValidator = RecordValidator.FieldValidators[cellIndex] as DateFieldValidator;
              var culture = dateValidator.IsNotNull() ? dateValidator.DateCulture : Thread.CurrentThread.CurrentCulture;

              fileRow.Add(cell.IsNull() ? string.Empty : cell.DateTimeValue.ToString(culture.DateTimeFormat.ShortDatePattern));
            }
            else
            {
              fileRow.Add(cell.IsNull() ? string.Empty : cell.DisplayStringValue);
            }
          }
        }
        else
        {
          fileRow.Add(null);
        }
        fileData.Add(fileRow);
      }

      return Validate(fileData, profanity, criminal, ignoreHeader);
    }

    /// <summary>
    /// Validates the file
    /// </summary>
    /// <param name="fileData">The text of the file to validate</param>
    /// <param name="profanity">A list of profanity rules</param>
    /// <param name="criminal">A list of criminal background exclusion rules</param>
    /// <param name="ignoreHeader">Whether to ignore the first record</param>
    /// <returns>
    /// A list of validation messages for records in error
    /// </returns>
    public FileValidationResult Validate(List<List<string>> fileData, CensorshipRuleList profanity, CensorshipRuleList criminal, bool ignoreHeader = true)
    {
      if (RecordValidator.Localiser.IsNull())
        RecordValidator.Localiser = Localiser;

      if (RecordValidator.LibraryRepository.IsNull())
        RecordValidator.LibraryRepository = LibraryRepository;

      if (RecordValidator.Settings.IsNull())
        RecordValidator.Settings = Settings;

      var results = new List<RecordValidationResult>();

      var fieldIndex = 0;
      var uniqueFields = new Dictionary<int, HashSet<object>>();
      foreach(var validator in RecordValidator.FieldValidators)
      {
        if (validator.Unique)
          uniqueFields.Add(fieldIndex, new HashSet<object>());

        fieldIndex++;
      }

      var fileQuery = ignoreHeader ? fileData.Skip(1) : fileData;
      var lineNumber = ignoreHeader ? 2 : 1;

      foreach(var fields in fileQuery)
      {
        if (fields.Count > 1 || fields[0].IsNotNull())
        {
          var result = RecordValidator.ValidateRecord(lineNumber, fields, uniqueFields, profanity, criminal);
          results.Add(result);
        }

        lineNumber++;
      }

      return new FileValidationResult
      {
        RecordType = RecordValidator.RecordType,
        Status = results.Any() 
          ? results.Select(result => result.Status).Aggregate((aggregated, current) => aggregated | current)
          : ValidationRecordStatus.Success,
        RecordValidationResults = results
      };
    }
  }
}
