﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;

#endregion

namespace Focus.Services.Core.ServiceHost
{
  [AttributeUsage(AttributeTargets.Method)]
  public class OperationRolesAttribute : Attribute
  {
    public OperationRolesAttribute(params string[] roles)
    {
      Roles = roles.ToList();
    }

    public List<string> Roles { get; private set; }
  }
}
