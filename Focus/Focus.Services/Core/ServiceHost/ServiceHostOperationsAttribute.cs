﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.ObjectModel;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;

#endregion

namespace Focus.Services.Core.ServiceHost
{
  internal class ServiceHostOperationsAttribute : Attribute, IServiceBehavior
  {
    public void Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
    {
      // Do Nothing
    }

    public void AddBindingParameters(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase, Collection<ServiceEndpoint> endpoints, BindingParameterCollection bindingParameters)
    {
      // Do Nothing
    }

    public void ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
    {
      foreach (var endpoint in serviceDescription.Endpoints)
      {
        foreach (var operation in endpoint.Contract.Operations)
        {
          IOperationBehavior behavior = new ServiceHostOperationAttribute();
					if (!operation.Behaviors.Contains(behavior))
						operation.Behaviors.Add(behavior);
        }
      }
    }
  }
}
