﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.ServiceModel.Dispatcher;

using Focus.Core;
using Focus.Core.Messages;
using Focus.Services.ServiceImplementations;
using Framework.Core;
using Framework.Logging;

#endregion

namespace Focus.Services.Core.ServiceHost
{
  internal class ServiceHostOperationInvoker : IOperationInvoker
  {
    private readonly IOperationInvoker _invoker;
    private readonly string _operationName;
    private readonly MethodInfo _methodInfo;
    //private readonly ILogger _logger;

    /// <summary>
    /// Initializes a new instance of the <see cref="ServiceHostOperationInvoker"/> class.
    /// </summary>
    /// <param name="invoker">The invoker.</param>
    /// <param name="operation">The operation.</param>
    /// <param name="methodInfo">Information of the method being called</param>
    public ServiceHostOperationInvoker(IOperationInvoker invoker, DispatchOperation operation, MethodInfo methodInfo)
    {
      _invoker = invoker;
      _operationName = operation.Name;
      _methodInfo = methodInfo;
      //_logger = ServiceLocator.Current.Resolve<ILogger>();
    }

    #region IOperationInvoker Members

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    public object[] AllocateInputs()
    {
      return _invoker.AllocateInputs();
    }

    /// <summary>
    /// Invokes the service method with input parameters.
    /// </summary>
    /// <param name="instance">object instance whose method to be invoked</param>
    /// <param name="inputs">input parameters</param>
    /// <param name="outputs">output parameters</param>
    /// <returns>return value</returns>
    public object Invoke(object instance, object[] inputs, out object[] outputs)
    {
      var input = (inputs.Length == 1 ? inputs[0] : null);
      var serviceRequest = (input as ServiceRequest);

      var startTime = DateTime.Now;
      var stopwatch = Stopwatch.StartNew();

      var serviceBase = (instance as ServiceBase);
      if (serviceBase.IsNotNull() && serviceRequest.IsNotNull())
      {
        var attributes = _methodInfo.GetCustomAttributes(typeof(OperationRolesAttribute), false).Cast<OperationRolesAttribute>().ToList();
        if (attributes.Count > 0)
        {
          var requiredKeys = attributes[0].Roles;
          var roles = serviceBase.Repositories.Core.UserRoles
                                 .Where(ur => ur.UserId == serviceRequest.UserContext.UserId)
                                 .Join(serviceBase.Repositories.Core.Roles, ur => ur.RoleId, r => r.Id, (ur, r) => r.Key)
                                 .ToList();

          if (!requiredKeys.Intersect(roles).Any())
            throw new Exception("Invalid Access");
        }
      }

      var returnValue = _invoker.Invoke(instance, inputs, out outputs);

      stopwatch.Stop();
      var endTime = DateTime.Now;

      if (serviceBase.IsNull() || !serviceBase.AppSettings.ProfilingEnabled)
        return returnValue;

      try
      {
        var serviceResponse = (returnValue as ServiceResponse);

        if (serviceRequest.IsNotNull() && serviceResponse.IsNotNull())
        {
          Profiler.Instance.OnProfile(new ProfileEventArgs(serviceRequest.SessionId, serviceRequest.RequestId,
                                                 serviceRequest.UserContext.UserId,
                                                 Environment.MachineName,
                                                 startTime, endTime, stopwatch.ElapsedMilliseconds,
                                                 instance.ToString(), _operationName, serviceRequest.DataContractSerializeRunTime()));
        }
      }
      catch (Exception ex)
      {
        Logger.Error("Error performing profiling in Invoke", ex);
      }

      return returnValue;
    }

    /// <summary>
    /// Async version of invoke.Initiates the call.
    /// </summary>
    /// <param name="instance">instance of object whose method is to be invoked</param>
    /// <param name="inputs">input parameters</param>
    /// <param name="callback">Async callback object</param>
    /// <param name="state">Associated state info</param>
    /// <returns>async result object</returns>
    public IAsyncResult InvokeBegin(object instance, object[] inputs, AsyncCallback callback, object state)
    {
      return _invoker.InvokeBegin(instance, inputs, callback, state);
    }

    /// <summary>
    /// Async version of invoke.Completes the call.
    /// </summary>
    /// <param name="instance">instance of object whose method is to be invoked</param>
    /// <param name="outputs">Output parameters</param>
    /// <param name="result">async result object</param>
    /// <returns>return value</returns>
    public object InvokeEnd(object instance, out object[] outputs, IAsyncResult result)
    {
      return _invoker.InvokeEnd(instance, out outputs, result);
    }

    /// <summary>
    /// Indicates whether call is sync or async.
    /// </summary>
    public bool IsSynchronous
    {
      get { return _invoker.IsSynchronous; }
    }

    #endregion
  }
}
