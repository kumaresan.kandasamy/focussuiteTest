﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.ServiceModel.Channels;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;

#endregion

namespace Focus.Services.Core.ServiceHost
{
  internal class ServiceHostOperationAttribute : Attribute, IOperationBehavior
  {
    public void Validate(OperationDescription operationDescription)
    {
      // Do Nothing
    }

    public void ApplyDispatchBehavior(OperationDescription operationDescription, DispatchOperation dispatchOperation)
    {
      var methodInfo = operationDescription.SyncMethod;

      dispatchOperation.Invoker = new ServiceHostOperationInvoker(dispatchOperation.Invoker, dispatchOperation, methodInfo);
    }

    public void ApplyClientBehavior(OperationDescription operationDescription, ClientOperation clientOperation)
    {
      throw new NotImplementedException();
    }

    public void AddBindingParameters(OperationDescription operationDescription, BindingParameterCollection bindingParameters)
    {
      // Do Nothing
    }
  }
}
