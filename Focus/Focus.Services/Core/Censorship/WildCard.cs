﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Text.RegularExpressions;

#endregion

namespace Focus.Services.Core.Censorship
{
  public class Wildcard : Regex
  {
    /// <summary>
    /// Initializes a wildcard with the given search pattern.
    /// </summary>
    /// <param name="pattern">The wildcard pattern to match.</param>
    public Wildcard(string pattern) : base(WildcardToRegex(pattern))
    { }

    /// <summary>
    /// Initializes a wildcard with the given search pattern and options.
    /// </summary>
    /// <param name="pattern">The wildcard pattern to match.</param>
    /// <param name="options">A combination of one or more
    /// <see cref="RegexOptions"/>.</param>
    public Wildcard(string pattern, RegexOptions options) : base(WildcardToRegex(pattern), options)
    { }

    /// <summary>
    /// Converts a wildcard to a regex.
    /// </summary>
    /// <param name="pattern">The wildcard pattern to convert.</param>
    /// <returns>A regex equivalent of the given wildcard.</returns>
    public static string WildcardToRegex(string pattern)
    {
      return string.Concat(Escape(pattern).Replace("\\*", "\\S*").Replace("\\?", "."), "\\b");
    }
  }
}
