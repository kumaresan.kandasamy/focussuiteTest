﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

using Framework.Core;

#endregion

namespace Focus.Services.Core.Censorship
{
	public class CensorshipCheck
	{
		private string _text;
		private bool _censorText;
    private IEnumerable<CensorshipRule> _censorshipRules;
		private string _censorshipPattern;
    private const RegexOptions Options = RegexOptions.IgnoreCase | RegexOptions.Multiline | RegexOptions.CultureInvariant;

		/// <summary>
		/// Initializes a new instance of the <see cref="CensorshipCheck" /> class.
		/// </summary>
		/// <param name="censorshipRules">The censor rules.</param>
		/// <param name="text">The text.</param>
		/// <param name="censorText">if set to <c>true</c> [censor text].</param>
		/// <param name="removeDuplicateSubsets">if set to <c>true</c> [remove duplicate subsets].</param>
		public CensorshipCheck(IEnumerable<CensorshipRule> censorshipRules, string text, bool censorText = false, bool removeDuplicateSubsets = false)
    {
			CheckText(censorshipRules, text, censorText, removeDuplicateSubsets);
    }

		public List<CensorshipRule> FoundCensorshipRules { get; set; }
		public bool HasCensorships { get; private set; }
		public string CensoredText { get; private set; }

		/// <summary>
		/// Checks the text.
		/// </summary>
		/// <param name="censorRules">The censor rules.</param>
		/// <param name="text">The text.</param>
		/// <param name="censorText">if set to <c>true</c> [censor text].</param>
		/// <param name="removeDuplicateSubsets">if set to <c>true</c> [remove duplicate subsets].</param>
    private void CheckText(IEnumerable<CensorshipRule> censorRules, string text, bool censorText, bool removeDuplicateSubsets)
    {
      _censorText = censorText;
      _text = text;
      _censorshipRules = censorRules;

      if (text.IsNullOrEmpty())
        FoundCensorshipRules = new List<CensorshipRule>();

      if (_censorText)
        _censorshipPattern = GetCensoringPattern();

		  string whiteListPhraseText;

			foreach (var censorshipRule in _censorshipRules)
			{
				censorshipRule.Frequency = 0;

				if (censorshipRule.Phrase.Contains("*"))
				{
					#region wild card match

					var wildCard = new Wildcard(censorshipRule.Phrase, Options);
					var contains = wildCard.IsMatch(_text);

					if (contains)
					{
            var pattern = ToRegexPattern(censorshipRule.Phrase);

					  if (censorshipRule.WhiteList != "")
					  {
              whiteListPhraseText = AdjustTextWhiteList(censorshipRule.WhiteList);
              //contains = wildCard.IsMatch(whiteListPhraseText);
					    var matches = Regex.Matches(whiteListPhraseText, pattern, Options);
					    contains = matches.Count > 0;
					  }

            if (contains)
						{
							var matches = Regex.Matches(_text, pattern, Options);
							censorshipRule.Frequency = matches.Count;

							if (_censorText) CensorText(matches);
						}
					}

					#endregion
				}
				else
				{
					#region exact match

					var pattern = ToRegexPattern(censorshipRule.Phrase);
					var matches = Regex.Matches(_text, pattern, Options);

					if (_censorText) CensorText(matches);

					if (matches.Count > 0)
					{
						if (censorshipRule.WhiteList != "")
            {
              whiteListPhraseText = AdjustTextWhiteList(censorshipRule.WhiteList);
              matches = Regex.Matches(whiteListPhraseText, pattern, Options);
            }

					  if (matches.Count > 0)
					  {
					    censorshipRule.Frequency = matches.Count;
              if (_censorText) CensorText(matches);
					  }
					}

					#endregion
				}
			}

			var foundCensorshipRules = _censorshipRules.Where(pf => pf.Frequency > 0).ToList();

			if (removeDuplicateSubsets && foundCensorshipRules.Count > 0)
			{
				var foundCensorshipRulesSizeDescending = foundCensorshipRules.OrderByDescending(x => x.Phrase.Length).Select(x => x.Phrase.ToLower()).ToList();
				var sourceText = text.ToLower();
				var hashOutRegEx = new Regex(@".");

				foreach (var foundCensorshipRuleSizeDescending in foundCensorshipRulesSizeDescending)
				{
					var currentText = sourceText;
					sourceText = sourceText.Replace(foundCensorshipRuleSizeDescending, hashOutRegEx.Replace(foundCensorshipRuleSizeDescending, "#"));

					if (currentText == sourceText)
						for (var i = foundCensorshipRules.Count - 1; i >= 0; i--)
							if (foundCensorshipRules[i].Phrase.ToLower() == foundCensorshipRuleSizeDescending)
							{
								foundCensorshipRules.RemoveAt(i);
								break;
							}							
				}
			}

			FoundCensorshipRules = foundCensorshipRules;

			if (FoundCensorshipRules.Count > 0)
				HasCensorships = true;          
		}

		/// <summary>
		/// Removes all white list words from the text so it can be re-checked for the censored word
		/// </summary>
		/// <param name="whiteList">The white list.</param>
		/// <returns>The text with no white list words in</returns>
    private string AdjustTextWhiteList(string whiteList)
    {
			if (whiteList.IsNullOrEmpty())
				return _text;

      var arrWhiteListPhrases = string.Join("|", whiteList.Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries).Select(x => x.Trim()).OrderByDescending(x => x.Length).ToArray());
      return Regex.Replace(_text, string.Concat("(", arrWhiteListPhrases, ")"), "", Options);
    }

		/// <summary>
		/// Censors the text found in the matches.
		/// </summary>
		/// <param name="matches">The matches.</param>
		private void CensorText(MatchCollection matches)
		{
			// Currently preserves the first and last letter in the string, may want to change this for hyphonate phrases
			// Could also have a the CensorPhrase dictate the need for censoring
			// Could also look at having a regex pattern to do this on the whoe string in one
			foreach (var match in matches)
			{
				var phrase = match.ToString();
				var censoredPhrase = CensorPhrase(phrase);

				if (CensoredText.IsNullOrEmpty())
					CensoredText = _text;

				CensoredText = CensoredText.Replace(phrase, censoredPhrase);
			}
		}

		/// <summary>
		/// Censors the phrase.
		/// </summary>
		/// <param name="phrase">The phrase.</param>
		/// <returns></returns>
		private string CensorPhrase(string phrase)
		{
			var censoredPhrase = Regex.Replace(phrase, _censorshipPattern, StarCensoredMatch, RegexOptions.IgnoreCase | RegexOptions.CultureInvariant);
			return censoredPhrase;
		}

		/// <summary>
		/// Gets the censoring pattern.
		/// </summary>
		/// <returns></returns>
		private static string GetCensoringPattern()
		{
			const string aplhaGapPattern = "[- a-zA-Z]";

			// aaa-aaa -> a******a middlePhrasePattern
			var censorshipPattern = string.Format("(?<={0}){0}(?={0})", aplhaGapPattern);

			return censorshipPattern;
		}

		/// <summary>
		/// Performs a Star Censor Match.
		/// </summary>
		/// <param name="m">The m.</param>
		/// <returns></returns>
		private static string StarCensoredMatch(Match m)
		{
			var word = m.Captures[0].Value;
			return new string('*', word.Length);
		}

		/// <summary>
		/// Converts the search text to a regex pattern.
		/// </summary>
		/// <param name="searchText">The search text.</param>
		/// <returns></returns>
		private static string ToRegexPattern(string searchText)
		{
			var regexPattern = Regex.Escape(searchText);

			regexPattern = regexPattern.Replace(@"\*", ".*?");
			regexPattern = regexPattern.Replace(@"\?", ".");

			if (regexPattern.StartsWith(".*?"))
			{
				regexPattern = regexPattern.Substring(3);
				regexPattern = @"(^\b)*?" + regexPattern;
			}

		  regexPattern = regexPattern.StartsWith(@"\$") 
        ? regexPattern + @"\b" 
        : @"\b" + regexPattern + @"\b";

		  return regexPattern;
		}
	}
}
