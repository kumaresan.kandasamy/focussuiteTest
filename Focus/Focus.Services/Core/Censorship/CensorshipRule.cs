﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

using Focus.Core;

#endregion

namespace Focus.Services.Core.Censorship
{
  [Serializable]
	public class CensorshipRule
  {
		/// <summary>
		/// Initializes a new instance of the <see cref="CensorshipRule" /> class.
		/// </summary>
		public CensorshipRule()
		{
			Phrase = WhiteList = String.Empty;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="CensorshipRule" /> class.
		/// </summary>
		/// <param name="source">The source.</param>
		public CensorshipRule(Data.Configuration.Entities.CensorshipRule source)
		{
			Type = source.Type;
			Level = source.Level;
			Phrase = source.Phrase;
			WhiteList = source.WhiteList;
		}

		public CensorshipType Type { get; set; }
		public CensorshipLevel Level { get; set; }
    public string Phrase { get; set; }
    public string WhiteList { get; set; }
    public int Frequency { get; set; }
  }

  [Serializable]
	public class CensorshipRuleList : List<CensorshipRule>
	{
		/// <summary>
		/// Filters the censorship rules by type.
		/// </summary>
		/// <param name="type">The type.</param>
		/// <returns></returns>
		public CensorshipRuleList Filter(CensorshipType type)
		{
			var filteredList = new CensorshipRuleList();
			filteredList.AddRange(this.Where(x => x.Type == type).Select(censorshipRule => new CensorshipRule
			                                                                               	{
			                                                                               		Type = censorshipRule.Type,
			                                                                               		Level = censorshipRule.Level,
			                                                                               		Phrase = censorshipRule.Phrase,
			                                                                               		WhiteList = censorshipRule.WhiteList,
			                                                                               		Frequency = censorshipRule.Frequency
			                                                                               	}));

			return filteredList;
		}
	}
}
