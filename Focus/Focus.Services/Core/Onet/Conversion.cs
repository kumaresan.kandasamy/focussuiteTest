﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Xml.Serialization;

#endregion

namespace Focus.Services.Core.Onet
{
	[Serializable]
	public class Conversion
	{
		[XmlElement("SqlOnetPlus")]
		public string SqlOnetPlus { get; set; }

		[XmlElement("SqlStep")]
		public string SqlStep { get; set; }

		[XmlElement("Rule1")]
		public string Rule1 { get; set; }

		[XmlElement("Type1")]
		public string Type1 { get; set; }

		[XmlElement("Rule2")]
		public string Rule2 { get; set; }

		[XmlElement("Type2")]
		public string Type2 { get; set; }

		[XmlElement("Rule3")]
		public string Rule3 { get; set; }

		[XmlElement("Type3")]
		public string Type3 { get; set; }

		[XmlElement("Onet")]
		public string Onet { get; set; }
	}

	public class ConversionList : List<Conversion> { }
}
