﻿namespace Focus.Services.Core.Onet
{
	internal class Weighting
	{
		public string Name { get; set; }
		public int Weight { get; set; }
	}
}