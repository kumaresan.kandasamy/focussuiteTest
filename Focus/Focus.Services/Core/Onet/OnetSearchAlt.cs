#region Copyright � 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Focus.Data.Repositories.Contracts;
using Framework.Core;

#endregion

namespace Focus.Services.Core.Onet
{
	public class OnetSearchAlt
	{
		private readonly ILibraryRepository _repository;

		private readonly List<RingWeighting> _thematicRings = new List<RingWeighting>
		{
			new RingWeighting{Name = "title", Weight = 16, RingMax = 1}, 
			new RingWeighting{Name = "laytitle", Weight = 16, RingMax = 1},
			new RingWeighting{Name = "description", Weight = 8, RingMax = 1}, 
			new RingWeighting{Name = "tasks", Weight = 2, RingMax = 5}, 
			new RingWeighting{Name = "dwa", Weight = 1, RingMax = 5}
		};

		private readonly List<Weighting> _searchTiers = new List<Weighting>
		{
			new Weighting{Name = "exact_stem", Weight = 4}
		};

		private readonly List<string> _stopWords = new List<string> { "a", "about", "after", "all", "also", "an", "and", "any", "are", "as", "at", "be", "because", "been", "but", "by", "can", "co", "corp", "could", "for", "from", "had", "has", "have", "he", "her", "his", "if", "in", "inc", "into", "is", "it", "its", "last", "more", "most", "mr", "mrs", "ms", "mz", "no", "not", "of", "on", "one", "only", "or", "other", "out", "over", "s", "says", "she", "so", "some", "such", "than", "that", "the", "their", "there", "they", "this", "to", "up", "was", "we", "were", "when", "which", "who", "will", "with", "would" };

		public OnetSearchAlt(ILibraryRepository repository)
		{
			_repository = repository;
		}

		////public ErrorIndex.eStatusCodes GetOnetCodes(String title, Int32 count, ref NameValueCollection outOnetCodes, ref Exception outException)
		/// <summary>
		/// Gets the onet codes.
		/// </summary>
		/// <param name="title">The title.</param>
		/// <param name="count">The count.</param>
		/// <returns></returns>
		public List<string> GetOnetCodes(string title, int count)
		{
			//var outOnetCodes = new NameValueCollection();
			var outOnetCodes = new List<string>();

			try
			{
					var words = CleanUpWords(title);
					var socScores = ExtractInputWords(words);
					var distinctSocs = GetDistinctOnetSocs(words);
					var totalSocScores = CalculateFrequency(words, distinctSocs, socScores);
					var maxScore = totalSocScores.Select(x => x.Score).Max();
					var finalScores = GetExactSearchPhrases(title, maxScore, totalSocScores);
					outOnetCodes = FinaliseOnetCodes(finalScores, maxScore, count);
			}
			catch (Exception ex)
			{
				if (ex.Message.Equals("Unable to connect with data server.") || ex.Message.Equals("Invalid data server access details."))
					outOnetCodes.Add("0");//, "0");
				////outException = ex;
				////statusCode = ErrorIndex.eStatusCodes.ERR_ONET_CODE_NORMALIZATION;
			}
			////return statusCode;
			return outOnetCodes;
		}

		/// <summary>
		/// Cleans up words.
		/// </summary>
		/// <param name="text">The text.</param>
		/// <returns></returns>
		public List<WordFrequency> CleanUpWords(string text)
		{
			var Words = GetNormalizedText(text.ToLower().Replace("&amp;", "&")).Split(' ').ToList();
			Words = Words.Where(x => x.IsNotNullOrEmpty()).Distinct().ToList();
			return (from word in Words select new WordFrequency { IsStopWord = (_stopWords.Contains(word)), Word = new Word(word) }).ToList();
		}

		/// <summary>
		/// Extracts the input words.
		/// </summary>
		/// <param name="words">The words.</param>
		/// <returns></returns>
		public List<SocScore> ExtractInputWords(List<WordFrequency> words)
		{
			// before there was a filter on stop words based on ring, these rings do not contain the stop words
			var nonStopWords = words.Where(x => !x.IsStopWord).ToList();

			return (from word in nonStopWords
					from tier in _searchTiers
					////from socScore in GetMatchingCount(db, word, tier).ToList()
					from socScore in GetMatchingCount(word, tier).ToList()
					select socScore).ToList();
		}

		/// <summary>
		/// Gets the matching count.
		/// </summary>
		/// <param name="word">The word.</param>
		/// <param name="tier">The tier.</param>
		/// <returns></returns>
		private IEnumerable<SocScore> GetMatchingCount(WordFrequency word, Weighting tier)
		{
			var tierWeighting = _searchTiers.SingleOrDefault(x => x.Name == tier.Name);

			var sb = new StringBuilder("SELECT onetsoc, ring, count(item_id) FROM srch_words WHERE ");

			switch (tier.Name)
			{
				case "exact_stem":
					sb.AppendFormat("(word='{0}' OR stem='{1}') ", word.Original, word.Stem);
					break;

				case "word_like":
					sb.AppendFormat("word LIKE '{0}%' ", word.Original);
					break;
			}

			sb.Append(" GROUP BY onetsoc, ring");
			var sql = sb.ToString();

			var socScores = new List<SocScore>();

			var reader = _repository.ExecuteSql(sql);
			RingWeighting ringWeighting = null;
			using (reader)
			{
				while (reader.Read())
				{
					string onetSoc = null;
					var count = 0;
					if (!reader.IsDBNull(0)) // ONetSoc
						onetSoc = reader.GetValue(0).AsString();

					if (!reader.IsDBNull(1)) // Ring
						ringWeighting = _thematicRings.SingleOrDefault(x => x.Name == reader.GetValue(1).AsString());

					if (!reader.IsDBNull(2)) // Count
						count = (Convert.ToInt32(reader.GetValue(2)));

					if (ringWeighting.IsNull())
						continue;

					count = count > ringWeighting.RingMax ? ringWeighting.RingMax : count;

					socScores.Add(new SocScore { OnetCode = onetSoc, Score = (count * ringWeighting.Weight * tierWeighting.Weight), Word = word });
				}
			}
			
			return socScores;
		}

		/// <summary>
		/// Gets the distinct onet socs.
		/// </summary>
		/// <param name="words">The words.</param>
		/// <returns></returns>
		public List<string> GetDistinctOnetSocs(List<WordFrequency> words)
		{
			var distinctSocs = new List<string>();
			var sb = new StringBuilder("SELECT DISTINCT(onetsoc) FROM srch_words WHERE ");

			foreach (var word in words)
			{
				sb.AppendFormat(
					!word.IsStopWord
						? "(word LIKE '{0}%' OR stem='{1}' ) OR "
						: "((word LIKE '{0}%' OR stem='{1}') AND (ring='title' OR ring='laytitle')) OR ", word.Original, word.Stem);
			}

			var sql = sb.ToString();
			sql = sql.Substring(0, sql.LastIndexOf(" OR ")) + " ORDER BY onetsoc";

			var reader = _repository.ExecuteSql(sql);
			
			while (reader.Read())
			{
				string soc = reader.GetValue(0).AsString();
				if (soc.IsNotNullOrEmpty())
					distinctSocs.Add(soc);
			}
			reader.Close();

			return distinctSocs;
		}

		/// <summary>
		/// Calculates the frequency.
		/// </summary>
		/// <param name="words">The words.</param>
		/// <param name="distinctSocs">The distinct socs.</param>
		/// <param name="socScores">The soc scores.</param>
		/// <returns></returns>
		public List<SocScore> CalculateFrequency(IEnumerable<WordFrequency> words, IEnumerable<string> distinctSocs, IEnumerable<SocScore> socScores)
		{
			// we dont need the distinct list of OnetCodes - because we are counting 
			// the data in the socScores - the query for distinct may return different data			
			foreach (var word in words)
			{
				var sql = !word.IsStopWord
					? string.Format("SELECT COUNT(item_id) FROM srch_words WHERE word LIKE '{0}%' OR stem='{1}' GROUP BY onetsoc", word.Original, word.Stem	)
					: string.Format("SELECT COUNT(item_id) FROM srch_words WHERE (word LIKE '{0}%' OR stem='{1}') AND (ring='title' OR ring='laytitle') GROUP BY onetsoc", word.Original, word.Stem);

				var reader = _repository.ExecuteSql(sql);
				
				while (reader.Read())
					word.Frequency += Convert.ToInt32(reader.GetValue(0));
				
				reader.Close();
			}

			return (from soc in distinctSocs
					let socLocal = soc
					let score = socScores.Where(x => x.OnetCode == socLocal).ToList().Sum(subSocScore => subSocScore.FrequencyScore)
							select new SocScore { OnetCode = soc, Score = score, Word = null }).ToList();
		}

		/// <summary>
		/// Gets the exact search phrases.
		/// </summary>
		/// <param name="title">The title.</param>
		/// <param name="maxScore">The max score.</param>
		/// <param name="totalSocScores">The total soc scores.</param>
		/// <returns></returns>
		public List<SocScore> GetExactSearchPhrases(string title, double maxScore, IEnumerable<SocScore> totalSocScores)
		{
			var matchingcodes = new List<string>();
			var sql = string.Format("SELECT onetsoc from srch_exact WHERE phrase='{0}'",GetNormalizedText(title).Trim()) ;

			var reader = _repository.ExecuteSql(sql);
			while (reader.Read())
			{
				var code = reader.GetValue(0).AsString();
				if (code.IsNotNullOrEmpty())
					matchingcodes.Add(code);
			}
			reader.Close();			

			var tmpMaxScore = maxScore;

			foreach (var matchingSoc in matchingcodes)
			{
				var soc = matchingSoc;
				var matchingScores = totalSocScores.Where(x => x.OnetCode == soc).ToList();

				foreach (var matchingScore in matchingScores)
				{
					matchingScore.Score = matchingScore.Score * 0.1 + tmpMaxScore;

					if (maxScore < matchingScore.Score)
						maxScore = matchingScore.Score;
				}
			}

			return totalSocScores.ToList();
		}

		/// <summary>
		/// Finalises the onet codes.
		/// </summary>
		/// <param name="totalSocScores">The total soc scores.</param>
		/// <param name="maxScore">The max score.</param>
		/// <param name="count">The count.</param>
		/// <returns></returns>
		public List<string> FinaliseOnetCodes(List<SocScore> totalSocScores, double maxScore, int count)
		{
			//var outOnetCodes = new NameValueCollection();

			// Normalize the scores to 100
			foreach (var score in totalSocScores)
			{
				score.Score = (score.Score / maxScore) * 100;
				if (score.Score.ToString().EndsWith(".99"))
					score.Score = score.Score - 1;
			}

			//order scores, take top count
			if (count == 0) count++;

			var orderedParallelQuery = totalSocScores.OrderByDescending(x => x.Score).Take(count).ToList();

			return totalSocScores.OrderByDescending(x => x.Score).Take(count).Select(score => score.OnetCode).ToList();
		}

		public string GetNormalizedText(string text)
		{
			return text.ToLower().Trim().RegexReplace("[^a-z0-9]", " ", true);
		}

		/////public ErrorIndex.eStatusCodes GetOccupation(String inOnetCode, ref String outOccupation, ref Exception outException)
		/// <summary>
		/// Gets the occupation.
		/// </summary>
		/// <param name="inOnetCode">The in onet code.</param>
		/// <param name="outOccupation">The out occupation.</param>
		/// <param name="outException">The out exception.</param>
		public void GetOccupation(String inOnetCode, ref String outOccupation, ref Exception outException)
		{
			outOccupation = "";
			////ErrorIndex.eStatusCodes statusCode = ErrorIndex.eStatusCodes.NO_ERROR;

			var sql = string.Format("SELECT occupation FROM onetinfo WHERE onetcode LIKE '{0}%'", inOnetCode);
			try
			{
					outOccupation = _repository.ExecuteSql(sql).AsString();
			}
			catch (Exception ex)
			{
				outException = ex;
				////statusCode = ErrorIndex.eStatusCodes.ERR_FETCHING_ONET_OCCUPATION;
			}
			////return statusCode;
		}
	}

	internal static class OnetSearchTempExtensions
	{
		/// <summary>
		/// Ases the string.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		public static string AsString(this object item)
		{
			return AsString(item, default(string));
		}

		/// <summary>
		/// Ases the string.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="defaultString">The default string.</param>
		/// <returns></returns>
		public static string AsString(this object item, string defaultString)
		{
			if (item == null || item.Equals(DBNull.Value))
				return defaultString;

			return item.ToString().Trim();
		}

		/// <summary>
		/// Regexes the replace.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <param name="matchPattern">The match pattern.</param>
		/// <param name="replacementValue">The replacement value.</param>
		/// <param name="ignoreCase">if set to <c>true</c> [ignore case].</param>
		/// <returns></returns>
		public static string RegexReplace(this string value, string matchPattern, string replacementValue, bool ignoreCase)
		{
			return ignoreCase ? Regex.Replace(value, matchPattern, replacementValue, RegexOptions.IgnoreCase) : Regex.Replace(value, matchPattern, replacementValue);
		}
	}
}
