﻿namespace Focus.Services.Core.Onet
{
	public interface IWord
	{
		string Original { get; }
		int Length { get; }
		string Stem { get; set; }
	}
}