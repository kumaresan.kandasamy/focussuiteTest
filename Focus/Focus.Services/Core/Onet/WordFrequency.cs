﻿using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.DataTransferObjects.FocusExplorer;
using Framework.Core;

namespace Focus.Services.Core.Onet
{
	public class WordFrequency
	{
		
		public string Original { get { return Word.IsNotNull() ? Word.Original : ""; } }
		public string Stem { get { return Word.IsNotNull() ? Word.Stem : ""; } }
		public Word Word { get; set; }
		public bool IsStopWord { get; set; }

		public string OnetWord { get; set; }
		public string OnetStem { get; set; }

		// Frequency in the DB
		public int Frequency { get ; set; }
		public int OnetFrequency { get; set; }

		public long OnetId { get; set; }
		public long OnetRingId { get; set; }
		public string OnetCode { get; set; }

		public OnetRingDto OnetRing { get; set; }
		public int StemWeight { get; set; }

		public double DistinctScore { get; set; }

		public int Score
		{
			get
			{
				var value = OnetFrequency > OnetRing.RingMax ? OnetRing.RingMax : OnetFrequency;

				return (value * OnetRing.Weighting * StemWeight);
			}
		}

		public double PhraseScore { get; private set; }
		public double FinalScore { get; set; }

		public double FrequencyScore
		{
			get { return Score * FrequencyFactor; }
		}

		public int FrequencyFactor
		{
			get
			{
				if (Frequency >= 1 && Frequency <= 4)
					return 64;

				if (Frequency >= 5 && Frequency <= 9)
					return 32;

				if (Frequency >= 10 && Frequency <= 24)
					return 16;

				if (Frequency >= 25 && Frequency <= 49)
					return 8;

				if (Frequency >= 50 && Frequency <= 99)
					return 4;

				if (Frequency >= 100 && Frequency <= 399)
					return 2;

				return 1;
			}
		}

		/// <summary>
		/// Sets the phrase score.
		/// </summary>
		/// <param name="maxScore">The max score.</param>
		public void SetPhraseScore(double maxScore)
		{								// Which score is this????
			PhraseScore = Score * 0.1 + maxScore;
		}

		/// <summary>
		/// Sets the final score.
		/// </summary>
		/// <param name="maxScore">The max score.</param>
		public void SetFinalScore(double maxScore)
		{								// Which score is this????
			FinalScore = (Score / maxScore) * 100;
			if (FinalScore.ToString().EndsWith(".99"))
				FinalScore = FinalScore - 1;
		}
	}
}