﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Text.RegularExpressions;
using System.Text;

using Focus.Core.Messages;

using Framework.Core;
using Framework.Logging;
using Framework.Caching;

using Focus.Core;

#endregion

namespace Focus.Services.Core.Onet
{
	public class OnetToROnet
	{
		private IRuntimeContext _runtimeContext;

		/// <summary>
		/// Initializes a new instance of the <see cref="OnetToROnet"/> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <exception cref="System.Exception">
		/// No configured Job Mine/DEXray service.
		/// or
		/// No configured Job Mine/DEXray service.
		/// </exception>
		public OnetToROnet(IRuntimeContext runtimeContext)
		{
			_runtimeContext = runtimeContext;

			if (_runtimeContext.AppSettings.LensServices.IsNullOrEmpty())
				throw new Exception("No configured Job Mine/DEXray service.");

			var jobMineServiceConfigured = _runtimeContext.AppSettings.LensServices.Any(x => x.ServiceType == LensServiceTypes.JobMine);

			if (!jobMineServiceConfigured)
				throw new Exception("No configured Job Mine/DEXray service.");
		}

		/// <summary>
		/// Converts the resume XML to ROnet.
		/// </summary>
		/// <param name="lensResumeXml">The lens resume XML.</param>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public string ConvertResumeXmlToROnet(string lensResumeXml, IServiceRequest request)
		{
			#region vars

			var onet = "";
			var jobTitle = "";
			var meta_position_type = "";
			var employer = "";
			var dexJobXml = new StringBuilder();
			var canonJobTitle = "";
			var cleanJobTitle = "";
			var strResult = "";
			var rOnet = "";
			var xmlDoc = new XmlDocument { PreserveWhitespace = true };

			#endregion

			//test
			//lensResumeXml =
			//  File.ReadAllText(lensResumeXml);

			//php string as provided by Indian team:
			//$jobAsCmd = "<?xml version='1.0' encoding='iso-8859-1'?><bgtcmd><canon>\n<JobDoc><posting>$jobXML\n</posting>\n<special>\n<CityName>$cityname</CityName>\n<StateCode>$statecode</StateCode>\n<ZipCode>$zipcode</ZipCode>\n<CountryName>$countryname</CountryName>\n<META_LOCATION>$meta_location</META_LOCATION>\n<META_Position_Type>$meta_position_type</META_Position_Type>\n<JobTitle>$jobtitle</JobTitle>\n<companyname>$companyname</companyname>\n</special>\n</JobDoc></canon></bgtcmd>"

			var jobAsCmd =
			 @"<?xml version='1.0' encoding='iso-8859-1'?><bgtcmd><canon><JobDoc><posting>$jobXML</posting><special><CityName></CityName><StateCode></StateCode><ZipCode>$zipcode</ZipCode><CountryName>$countryname</CountryName><META_LOCATION>$meta_location</META_LOCATION><META_Position_Type>$meta_position_type</META_Position_Type><JobTitle>$jobtitle</JobTitle><companyname>$companyname</companyname></special></JobDoc></canon></bgtcmd>";

			try
			{
				xmlDoc.LoadXml(lensResumeXml);
			}
			catch (Exception ex)
			{
				Logger.Info(request.LogData(), string.Format("Exception loading Lens resume xml {0}", ex.Message));
				throw new Exception("Exception loading Lens resume xml", ex);
			}
			var resumeNode = xmlDoc.SelectSingleNode("//resume");
			if (resumeNode.IsNotNull())
			{
				#region extract resume data
				var primaryJobNode = resumeNode.SelectSingleNode("//job[@pos=1]");

				if (primaryJobNode.IsNotNull())
				{
					var employerNode = primaryJobNode.SelectSingleNode("//employer");
					if (employerNode.IsNotNull())
					{
						dexJobXml.AppendFormat("{0}", employerNode.OuterXml);
						employer = employerNode.OuterXml;
					}

					var jobTitleNode = primaryJobNode.SelectSingleNode("//title");
					if (jobTitleNode.IsNotNull())
					{
						dexJobXml.AppendFormat("{0}", jobTitleNode.OuterXml);
						jobTitle = jobTitleNode.OuterXml;
					}

					var jobTypeNode = primaryJobNode.SelectSingleNode("//jobtype");
					if (jobTypeNode.IsNotNull())
					{
						dexJobXml.AppendFormat("{0}", jobTypeNode.OuterXml);
						meta_position_type = jobTypeNode.OuterXml;
					}

					var descriptionNode = primaryJobNode.SelectSingleNode("//description");
					if (descriptionNode.IsNotNull())
					{
						dexJobXml.AppendFormat("<duties>{0}</duties>", descriptionNode.OuterXml);
					}
				}
				#endregion
			}

			#region replace values

			jobAsCmd = jobAsCmd.Replace("$jobXML", dexJobXml.ToString());
			jobAsCmd = jobAsCmd.Replace("$jobtitle", jobTitle);
			jobAsCmd = jobAsCmd.Replace("$zipcode", "");
			jobAsCmd = jobAsCmd.Replace("$companyname", employer);
			jobAsCmd = jobAsCmd.Replace("$countryname", "");
			jobAsCmd = jobAsCmd.Replace("$meta_location", "");
			jobAsCmd = jobAsCmd.Replace("$meta_position_type", meta_position_type);

			#endregion

			try
			{
				#region send Lens message

				var command = StripIllegalXmlChars(jobAsCmd, "1.1");
				strResult = _runtimeContext.Repositories.Lens.ExecuteJobMineCommand(command);
				
				#endregion
			}
			catch (Exception lex)
			{
				Logger.Info(request.LogData(), string.Format("Exception sending/receiving DEX message {0}", lex.Message));
				throw new Exception("Exception sending/receiving DEX message", lex);
			}
			
			var xmlJobDoc = new XmlDocument { PreserveWhitespace = true };
			xmlJobDoc.LoadXml(strResult);

			var jobDocNode = xmlJobDoc.SelectSingleNode("//JobDoc");
			if (jobDocNode.IsNotNull())
			{
				#region extract DEX output data
				var dataElementsRollupNode = jobDocNode.SelectSingleNode("//DataElementsRollup");

				if (dataElementsRollupNode.IsNotNull())
				{
					var canonJobTitleNode = dataElementsRollupNode.SelectSingleNode("//CanonJobTitle");
					if (canonJobTitleNode.IsNotNull())
					{
						canonJobTitle = canonJobTitleNode.InnerText;
					}

					var cleanJobTitleNode = dataElementsRollupNode.SelectSingleNode("//CleanJobTitle");
					if (cleanJobTitleNode.IsNotNull())
					{
						cleanJobTitle = cleanJobTitleNode.InnerText;
					}

					var consolidatedOnetNode = dataElementsRollupNode.SelectSingleNode("//ConsolidatedONET");
					if (consolidatedOnetNode.IsNotNull())
					{
						onet = consolidatedOnetNode.InnerText;
					}
				}
				#endregion

				canonJobTitle = canonJobTitle.ToLower();
				cleanJobTitle = cleanJobTitle.ToLower();

				Logger.Debug(string.Format("{0} , {1}", canonJobTitle, cleanJobTitle));

				#region Onet to ROnet conversion
				if (!(Regex.IsMatch(cleanJobTitle, @"(^|\s)intern(\s|$)") || Regex.IsMatch(cleanJobTitle, @"(^|\s)interns(\s|$)")
							|| cleanJobTitle.Contains("internship") || cleanJobTitle.Contains("apprentice")
					&& (Regex.IsMatch(canonJobTitle, @"(^|\s)intern(\s|$)") || Regex.IsMatch(canonJobTitle, @"(^|\s)interns(\s|$)")
							|| canonJobTitle.Contains("internship") || canonJobTitle.Contains("apprentice"))))
				{
					rOnet = ConvertOnetJobTitlesToROnet(onet, canonJobTitle, cleanJobTitle, request);
				}
				else
				{
					rOnet = "NA";

				}
				#endregion
			}

			return rOnet;
		}

		/// <summary>
		/// Converts the onet job titles to ROnet.
		/// </summary>
		/// <param name="onet">The onet.</param>
		/// <param name="canonTitle">The canon title.</param>
		/// <param name="cleanTitle">The clean title.</param>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public string ConvertOnetJobTitlesToROnet(string onet, string canonTitle, string cleanTitle, IServiceRequest request)
		{
			var rOnet = "";

			try
			{
				#region clean title pattern matching

				var appPath = (string)AppDomain.CurrentDomain.GetData("DataDirectory") ??
											AppDomain.CurrentDomain.SetupInformation.ApplicationBase + @"\App_Data";

				var xmlPath = appPath + @"\CleanTitlePatternMatch.xml";

				if (!File.Exists(xmlPath))
				{
					throw new InvalidDataException("Xml file: " + xmlPath + " doesn't exist.");
				}

				//var xmlPath = @"C:\Burning Glass\Documentation\Work details\OnetToROnet\Conversions_Generated_1.xml";

				if (!File.Exists(xmlPath))
				{
					Logger.Info(request.LogData(), string.Format("Error loading file {0}", xmlPath));
					throw new InvalidDataException("Xml file: " + xmlPath + " doesn't exist.");
				}

				using (var reader = new StreamReader(xmlPath))
				{
					var onetToROnetConversions =
						new XmlSerializer(typeof(ConversionList), new XmlRootAttribute("OnetToROnetConversions")).Deserialize
							(reader) as
						ConversionList;

					if (onetToROnetConversions != null)
					{
						foreach (var onetToROnetConversion in onetToROnetConversions)
						{
							#region pattern matching

							bool contains = false;

							if (!string.IsNullOrEmpty(onetToROnetConversion.Rule1))
							{

								contains = cleanTitle.Contains(onetToROnetConversion.Rule1);

							}

							if (!string.IsNullOrEmpty(onetToROnetConversion.Rule2) && contains)
							{
								contains = cleanTitle.Contains(onetToROnetConversion.Rule2);
							}

							if (!string.IsNullOrEmpty(onetToROnetConversion.Rule3) && contains)
							{
								contains = cleanTitle.Contains(onetToROnetConversion.Rule3);
							}

							if (contains)
							{
								rOnet = onetToROnetConversion.SqlOnetPlus;
								break;
							}
						}

							#endregion
					}
				}

				#endregion

				if (canonTitle != "NA" && rOnet == "")
				{
					#region canon title pattern matching

					xmlPath = appPath + @"\CanonTitlePatternMatch.xml";
					
					if (!File.Exists(xmlPath))
					{
						Logger.Info(request.LogData(), string.Format("Error loading file {0}", xmlPath));
						throw new InvalidDataException("Xml file: " + xmlPath + " doesn't exist.");
					}

					using (var reader = new StreamReader(xmlPath))
					{
						var onetToROnetConversions =
							new XmlSerializer(typeof(ConversionList), new XmlRootAttribute("OnetToROnetConversions")).
								Deserialize
								(reader) as
							ConversionList;

						if (onetToROnetConversions != null)
						{
							foreach (var onetToROnetConversion in onetToROnetConversions)
							{
								#region pattern matching

								var contains = false;

								if (!string.IsNullOrEmpty(onetToROnetConversion.Rule1))
								{
									contains = onetToROnetConversion.Type1 == "literal"
                    ? Regex.IsMatch(canonTitle, @"(^|\s)" + onetToROnetConversion.Rule1 + @"(\s|$)")
                    : canonTitle.Contains(onetToROnetConversion.Rule1);
								}

								if (!string.IsNullOrEmpty(onetToROnetConversion.Rule2) && contains)
								{
									if (onetToROnetConversion.Type2 == "literal")
									{
										contains = Regex.IsMatch(canonTitle, @"(^|\s)" + onetToROnetConversion.Rule2 + @"(\s|$)");
									}
									else
									{
										if (onetToROnetConversion.Type2 != "NOT")
										{
											contains = canonTitle.Contains(onetToROnetConversion.Rule2);
										}
										else
										{
											contains = !canonTitle.Contains(onetToROnetConversion.Rule2);
										}
									}
								}

								if (!string.IsNullOrEmpty(onetToROnetConversion.Rule3) && contains)
								{
									contains = onetToROnetConversion.Type3 == "literal"
																? Regex.IsMatch(canonTitle, @"(^|\s)" + onetToROnetConversion.Rule3 + @"(\s|$)")
																: canonTitle.Contains(onetToROnetConversion.Rule3);
								}

								if (contains)
								{
									if (!string.IsNullOrEmpty(onetToROnetConversion.Onet))
									{
										if (onet == onetToROnetConversion.Onet)
										{
											rOnet = onetToROnetConversion.SqlOnetPlus;
											break;
										}

									}
									else
									{
										rOnet = onetToROnetConversion.SqlOnetPlus;
										break;
									}
								}

								#endregion
							}
						}
					}
					#endregion
				}
				
				if (string.IsNullOrEmpty(rOnet))
				{
					rOnet = (from o in _runtimeContext.Repositories.Library.Onets
						join or in _runtimeContext.Repositories.Library.OnetROnet on o.Id equals or.OnetId
						join r in _runtimeContext.Repositories.Library.ROnets on or.ROnetId equals r.Id
						where o.OnetCode == onet && or.BestFit
						select r.Code).FirstOrDefault();
				}
			}
			catch (Exception ex)
			{
				Logger.Info(request.LogData(), string.Format("Exception converting Onet to ROnet {0}", ex.Message));
				throw new Exception("Exception converting Onet to ROnet", ex);
			}

			if (rOnet == "")
				rOnet = "NA";

			return rOnet;
		}

		/// <summary>
		/// Strips non-printable ascii characters 
		/// Refer to http://www.w3.org/TR/xml11/#charsets for XML 1.1
		/// Refer to http://www.w3.org/TR/2006/REC-xml-20060816/#charsets for XML 1.0
		/// </summary>
		/// <param name="xml">xml string to be formatted</param>
		/// <param name="xmlVersion">XML Specification to use. Can be 1.0 or 1.1</param>
		private string StripIllegalXmlChars(string xml, string xmlVersion)
		{
			//Remove illegal character sequences
			var pattern = String.Empty;

			switch (xmlVersion)
			{
				case "1.0":
					pattern = @"#x((10?|[2-F])FFF[EF]|FDD[0-9A-F]|7F|8[0-46-9A-F]9[0-9A-F])";
					break;

				case "1.1":
					pattern = @"#x((10?|[2-F])FFF[EF]|FDD[0-9A-F]|[19][0-9A-F]|7F|8[0-46-9A-F]|0?[1-8BCEF])";
					break;

				default:
					throw new Exception("Error: Invalid XML Version!");
			}

			var regex = new Regex(pattern, RegexOptions.IgnoreCase);

			if (regex.IsMatch(xml))
				xml = regex.Replace(xml, String.Empty);

			return xml;
		}


    #region Mappings Class

    [Serializable]
    public class Mapping
    {
      [XmlElement("AlphaNumeric", IsNullable = true)]
      public string AlphaNumeric { get; set; }
      public bool ShouldSerializeAlphaNumeric() { return AlphaNumeric.IsNotNullOrEmpty(); }

      [XmlElement("Numeric", IsNullable = true)]
      public string Numeric { get; set; }
      public bool ShouldSerializeNumeric() { return Numeric.IsNotNullOrEmpty(); }
    }

    [Serializable]
    public class MappingList : List<Mapping> { }

    #endregion
	}
}
