﻿using System;

namespace Focus.Services.Core.Onet
{
	internal class WordRegion
	{
		public WordRegion(){ }

		public WordRegion(int start, int end)
		{
			Start = start;
			End = end;
		}

		public int Start { get; set; }
		public int End { get; set; }
		public string Text { get; private set; }
		
		internal bool InRange(int index)
		{
			return (index >= Start && index <= End);
		}

		/// <summary>
		/// Generates the region.
		/// </summary>
		/// <param name="text">The text.</param>
		internal void GenerateRegion(string text)
		{
			Text = text.Length > Start ? text.Substring(Start, Math.Min(End, text.Length) - Start) : String.Empty;
		}
	}
}