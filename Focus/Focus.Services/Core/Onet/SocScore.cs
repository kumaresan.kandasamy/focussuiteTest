﻿namespace Focus.Services.Core.Onet
{
	public class SocScore
	{
		public string OnetCode { get; set; }
		public long OnetCodeId { get; set; }
		public long RingId { get; set; }
		// returns the OnetCode, this is what is stored in the OnetSoc column
		public string OnetSoc { get { return OnetCode; } }
		public double Score { get; set; }

		public WordFrequency Word { get; set; }
		public double FrequencyScore
		{
			get { return Score * Word.FrequencyFactor; }
		}
	}
}