﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;

using Focus.Core.DataTransferObjects.FocusExplorer;
using Focus.Data.Library.Entities;
using Focus.Data.Repositories.Contracts;
using Focus.Services.DtoMappers;

using Framework.Core;

#endregion

namespace Focus.Services.Core.Onet
{
	public class OnetSearch
	{
		private readonly ILibraryRepository _repository;
		private readonly List<string> _stopWords = new List<string>();
		private readonly List<OnetRingDto> _onetRings = new List<OnetRingDto>();
	
		private long _titleRingId;
		private long _layTitleRingId;
		private string _phrase;

		private const int ExactStemWeight = 4;

		public List<WordFrequency> WordList { get; private set; }// SearchWordList
		public List<WordFrequency> OnetWordList { get; private set; }

		public OnetSearch(ILibraryRepository repository)
		{
			_repository = repository;
			GetReferenceData();
			OnetWordList = new List<WordFrequency>();
		}

		/// <summary>
		/// Gets the reference data used such and the OnetRings and Stop words.
		/// </summary>
		private void GetReferenceData()// could pass this in List<string> stopWords, list<ringweighting> rings
		{
			_onetRings.AddRange(_repository.OnetRings.Select(x => x.AsDto()).ToList());

			var titleRing = _onetRings.Where(x => x.Name == "Title").Select(x => x.Id).FirstOrDefault();
			_titleRingId = titleRing.HasValue ? titleRing.Value : 0;

			var latTitleRing = _onetRings.Where(x => x.Name == "LayTitle").Select(x => x.Id).FirstOrDefault();
			_layTitleRingId = latTitleRing.HasValue ? latTitleRing.Value : 0;

			_stopWords.AddRange(_repository.OnetLocalisationItems.SingleOrDefault(x => x.Key == "Onet.StopWords").PrimaryValue.Split(',').ToList());
		}
		
		/// <summary>
		/// Gets the onet codes.
		/// </summary>
		/// <param name="phrase">The phrase.</param>
		/// <param name="count">The count.</param>
		/// <param name="mustHaveJobTasks">if set to <c>true</c> [must have job tasks].</param>
		/// <returns></returns>
		public List<string> GetOnetCodes(string phrase, int count, bool mustHaveJobTasks = false)
		{
			var outOnetCodes = new List<string>();

			try
			{
				// Clean up the phrase passed in
				var words = ExtractWordsFromPhrase(phrase);

				// Get all the Onet words we will use - this is essentiall Like Word Or Exact Stem
				FindMatchingOnetWords(WordList, mustHaveJobTasks);

				// Match with Exact word OR exact stem
				var socScores = ProcessExactWordMatch(words);

				// Match with Like Word OR Exact Stem
				ProcessLikeWordMatches(words);

				// Sum the scores so far by the onet codes
				var totalSocScores = SumScoreByOnet(words, socScores);

				// Find the Highest score so far
				var maxScore = 0.0;
				if (totalSocScores.Any())
					maxScore = totalSocScores.Select(x => x.Score).Max();

				// Increases the weighting of the OnetWords found which 
				var finalScores = ProcessExactPhrase(maxScore, totalSocScores);

				// Select the range of codes we want
				outOnetCodes = SelectRequiredData(finalScores, maxScore, count);
			}
			catch (Exception ex)
			{
				if (ex.Message.Equals("Unable to connect with data server.") || ex.Message.Equals("Invalid data server access details."))
					outOnetCodes.Add("0");//, "0");
			}

			return outOnetCodes;
		}

		/// <summary>
		/// Cleans the phrase and extracts the words from phrase.
		/// </summary>
		/// <param name="phrase">The phrase.</param>
		/// <returns></returns>
		public List<WordFrequency> ExtractWordsFromPhrase(string phrase)
		{
			_phrase = GetNormalizedText(phrase.Replace("&amp;", "&"));

			var words = _phrase.ToLower().Split(' ').ToList();

			WordList = words.Where(x => x.IsNotNullOrEmpty()).Distinct()
							.Select(word => new WordFrequency { IsStopWord = (_stopWords.Contains(word)), Word = new Word(word) }).ToList();

			return WordList;
		}

		/// <summary>
		/// Finds the matching onet words we need to process.
		/// </summary>
		/// <param name="words">The words.</param>
		/// <param name="mustHaveJobTasks">if set to <c>true</c> [must have job tasks].</param>
		/// <returns></returns>
		public List<WordFrequency> FindMatchingOnetWords(IEnumerable<WordFrequency> words, bool mustHaveJobTasks)// produces the list of words we are working with
		{
			foreach (var word in words)
			{
				var query = from ow in _repository.OnetWords select ow;

				List<WordFrequency> results ;
				var wd = word;
				if (word.IsStopWord)
				{
					if (mustHaveJobTasks)
					{ // forces us to pull back onets with JobTasks
						results = (from owv in _repository.Query<OnetWordJobTaskView>()
						           where (owv.Word.StartsWith(wd.Original) || owv.Stem == wd.Stem && (owv.OnetRingId == _titleRingId || owv.OnetRingId == _layTitleRingId))
						           select new WordFrequency
						           		{
						           			OnetId = owv.OnetId,
						           			OnetRingId = owv.OnetRingId,
						           			OnetFrequency = owv.Frequency.HasValue ? owv.Frequency.Value : 0,
						           			OnetWord = owv.Word,
						           			OnetStem = owv.Stem,
						           			Word = wd.Word,
						           			OnetCode = owv.OnetCode
						           		}).Distinct().ToList();
					}
					else
					{
						results = (from owv in _repository.Query<OnetWordView>()
						           where
						           	(owv.Word.StartsWith(wd.Original) ||
						           	 owv.Stem == wd.Stem && (owv.OnetRingId == _titleRingId || owv.OnetRingId == _layTitleRingId))
						           //(from q in query
						           // join o in _repository.Onets on q.OnetId equals o.Id
						           //   where (q.Word.StartsWith(wd.Original) || q.Stem == wd.Stem && (o.Id == _titleRingId || o.Id == _layTitleRingId))
						           select
						           	new WordFrequency
						           		{
						           			OnetId = owv.OnetId,
						           			OnetRingId = owv.OnetRingId,
						           			OnetFrequency = owv.Frequency.HasValue ? owv.Frequency.Value : 0,
						           			OnetWord = owv.Word,
						           			OnetStem = owv.Stem,
						           			Word = wd.Word,
						           			OnetCode = owv.OnetCode
						           		}).Distinct().ToList();
					}
				}
				else
				{
					if (mustHaveJobTasks)
					{ // forces us to pull back onets with JobTasks
						results = (from owv in _repository.Query<OnetWordJobTaskView>()
											 where (owv.Word.StartsWith(wd.Original) || owv.Stem == wd.Stem)
											 select new WordFrequency
											 {
												 OnetId = owv.OnetId,
												 OnetRingId = owv.OnetRingId,
												 OnetFrequency = owv.Frequency.HasValue ? owv.Frequency.Value : 0,
												 OnetWord = owv.Word,
												 OnetStem = owv.Stem,
												 Word = wd.Word,
												 OnetCode = owv.OnetCode
											 }).Distinct().ToList();
					}
					else
					{
						results = (from owv in _repository.Query<OnetWordView>()
						           //from q in query
						           // join o in _repository.Onets on q.OnetId equals o.Id
						           where (owv.Word.StartsWith(wd.Original) || owv.Stem == wd.Stem)
						           select new WordFrequency
						                  	{
						                  		OnetId = owv.OnetId,
						                  		OnetRingId = owv.OnetRingId,
						                  		OnetFrequency = owv.Frequency.HasValue ? owv.Frequency.Value : 0,
						                  		OnetWord = owv.Word,
						                  		OnetStem = owv.Stem,
						                  		Word = wd.Word,
						                  		OnetCode = owv.OnetCode
						                  	}).Distinct().ToList();
					}
				}

				OnetWordList.AddRange(results);
			}

			return OnetWordList;
		}

		/// <summary>
		/// Processes the exact word matches in the data we have.
		/// </summary>
		/// <param name="words">The words.</param>
		/// <returns></returns>
		private List<SocScore> ProcessExactWordMatch(List<WordFrequency> words)
		{
			var socScores = new List<SocScore>();
			foreach (var word in words.Where(x => !x.IsStopWord))
			{
				var wd = word;
				var wordFrequencyMatches = (
				                           	from owl in OnetWordList
																		where owl.OnetWord == wd.Original || owl.OnetStem == wd.Stem
				                           	select
				                           		new
				                           			{
				                           				owl.OnetId,
																					owl.OnetCode,
				                           				owl.OnetRingId,
				                           				owl.OnetFrequency
				                           			}).ToList();

				var onetRingScores = wordFrequencyMatches.AsQueryable().GroupBy(x =>
				                                                                //new { x.OnetId, x.OnetRingId }
																																				new { x.OnetCode, x.OnetRingId }
					).Select(x => new { OnetSoc = x.Key.OnetCode, RingId = x.Key.OnetRingId, Frequency = x.Sum(of => of.OnetFrequency) }).
					ToList();


				foreach (var onetRingScore in onetRingScores)
				{
					var ringScore = onetRingScore;
					var ring = _onetRings.FirstOrDefault(x => x.Id == ringScore.RingId);
					var count = onetRingScore.Frequency;
					count = count > ring.RingMax ? ring.RingMax : count;
					socScores.Add(new SocScore
					              	{OnetCode = onetRingScore.OnetSoc, Score = (count*ring.Weighting*ExactStemWeight), Word = word});
				}
			}
			return socScores;
		}

		/// <summary>
		/// Sums the score by onet.
		/// </summary>
		/// <param name="words">The words.</param>
		/// <param name="socScores">The soc scores.</param>
		/// <returns></returns>
		public List<SocScore> SumScoreByOnet(List<WordFrequency> words, IEnumerable<SocScore> socScores)
		{
			var distinctSocs = new List<string>();
			var distinctOnets = (from owl in OnetWordList
			                     select new {owl.OnetCode, owl.OnetId}).ToList().Distinct().OrderBy(x => x.OnetCode);
			
			distinctSocs.AddRange(distinctOnets.Select(x => x.OnetCode));
			
			return (from soc in distinctSocs
							let score = socScores.Where(x => x.OnetCode == soc).ToList().Sum(subSocScore => subSocScore.FrequencyScore)
							select new SocScore { OnetCode = soc, Score = score, Word = null }).ToList();
		}

		/// <summary>
		/// Processes the like word matches in the data we have.
		/// </summary>
		/// <param name="words">The words.</param>
		public void ProcessLikeWordMatches(IEnumerable<WordFrequency> words)
		{
			// we dont need the distinct list of OnetCodes - because we are counting 
			// the data in the socScores - the query for distinct may return different data
			foreach (var word in words)
			{
				var wd = word;
				List<int> found;
				if (word.IsStopWord)
				{
					found = OnetWordList.Where(x => x.OnetWord.StartsWith(wd.Original) || x.OnetStem == wd.Stem
																					&& (x.OnetRingId == _titleRingId || x.OnetRingId == _layTitleRingId)
						).GroupBy(x => new { x.OnetId }).Select(x => x.Sum(f => f.Frequency)).ToList();
				}
				else
					found =
						OnetWordList.Where(x => x.OnetWord.StartsWith(wd.Original) || x.OnetStem == wd.Stem).GroupBy(
							x => new { x.OnetId }).Select(x => x.Sum(f => f.Frequency)).ToList();

				foreach (var value in found)
					word.Frequency += value; // totals the values into the search word
			}
		}

		/// <summary>
		/// Processes the exact phrase.
		/// </summary>
		/// <param name="maxScore">The max score.</param>
		/// <param name="totalSocScores">The total soc scores.</param>
		/// <returns></returns>
		public List<SocScore> ProcessExactPhrase(double maxScore, IEnumerable<SocScore> totalSocScores)
		{
			var matchingcodes = (from op in _repository.OnetPhrases
			                     join o in _repository.Onets on op.OnetId equals o.Id
													 where op.Phrase == _phrase.Trim()
													 select new { OnetId = o.Id, o.OnetCode }).ToList();

			var tmpMaxScore = maxScore;

			foreach (var matchingSoc in matchingcodes)
			{
				var score = matchingSoc;
				var matchingScores = totalSocScores.Where(x => x.OnetCode == score.OnetCode).ToList();

				foreach (var matchingScore in matchingScores)
				{
					matchingScore.Score = matchingScore.Score * 0.1 + tmpMaxScore;

					if (maxScore < matchingScore.Score)
						maxScore = matchingScore.Score;
				}
			}

			return totalSocScores.ToList();
		}

		/// <summary>
		/// Selects the required data.
		/// </summary>
		/// <param name="totalSocScores">The total soc scores.</param>
		/// <param name="maxScore">The max score.</param>
		/// <param name="count">The count.</param>
		/// <returns></returns>
		public List<string> SelectRequiredData(List<SocScore> totalSocScores, double maxScore, int count)
		{
			//var outOnetCodes = new NameValueCollection();

			// Normalize the scores to 100
			foreach (var score in totalSocScores)
			{
				score.Score = (score.Score / maxScore) * 100;
				if (score.Score.ToString().EndsWith(".99"))
					score.Score = score.Score - 1;
			}

			//order scores, take top count
			if (count == 0) count++;

			return totalSocScores.OrderByDescending(x => x.Score).Take(count).Select(score => score.OnetCode).ToList();
		}

		public string GetNormalizedText(string text)
		{
			return text.ToLower().Trim().RegexReplace("[^a-z0-9]", " ", true);
		}
	}
}
