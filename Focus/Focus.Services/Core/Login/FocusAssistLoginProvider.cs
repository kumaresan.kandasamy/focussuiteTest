﻿#region Copyright © 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Linq;

using Focus.Core;
using Focus.Core.Views;

using Framework.Core;

#endregion

namespace Focus.Services.Core.Login
{
	public class FocusAssistLoginProvider : FocusLoginProvider
	{
		public FocusAssistLoginProvider(IRuntimeContext runtimeContext, FocusModules module) : base(runtimeContext, module)
		{

		}

		/// <summary>
		/// Sets the external fields
		/// </summary>
		/// <param name="userView">The current user model</param>
		/// <param name="personId">The id of the person in Focus</param>
		/// <param name="userName">Name of the user.</param>
		/// <param name="password">The password.</param>
		/// <param name="externalId">The external identifier.</param>
		protected override void SetExternalDetails(ValidatedUserView userView, long personId, string userName, string password, string externalId)
		{
			if (RuntimeContext.AppSettings.AuthenticateAssistUserViaClient)
			{
				if (externalId.IsNotNullOrEmpty())
				{
					userView.ExternalUserId = externalId;
					userView.ExternalUserName = userName;
					userView.ExternalPassword = password;
					userView.ExternalOfficeId = (from pco in RuntimeContext.Repositories.Core.PersonsCurrentOffices
																				join o in RuntimeContext.Repositories.Core.Offices
																					on pco.OfficeId equals o.Id
																				where pco.PersonId == personId
																				orderby pco.StartTime descending
																				select o.ExternalId).FirstOrDefault();

					if (userView.ExternalOfficeId.IsNullOrEmpty())
					{
						userView.ExternalOfficeId = RuntimeContext.Repositories.Core.Offices.Where(o => (o.DefaultType & OfficeDefaultType.JobSeeker) == OfficeDefaultType.JobSeeker)
																																		.Select(o => o.ExternalId)
																																		.FirstOrDefault();
					}
				}
			}
		}
	}
}
