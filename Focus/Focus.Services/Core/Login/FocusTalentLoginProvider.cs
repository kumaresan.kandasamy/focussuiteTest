﻿#region Copyright © 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Linq;

using Focus.Core;
using Focus.Core.Models;

using Framework.Core;

#endregion

namespace Focus.Services.Core.Login
{
	public class FocusTalentLoginProvider : FocusLoginProvider
	{
		public FocusTalentLoginProvider(IRuntimeContext runtimeContext, FocusModules module): base(runtimeContext, module)
		{
			
		}

		/// <summary>
		/// Performs some extra validation for Talent
		/// </summary>
		/// <param name="userId"></param>
		/// <param name="userName"></param>
		/// <returns></returns>
		protected override LoginModel UserValidation(long userId, string userName)
		{
			// For Talent users, approval statuses need to be checked
			var employeeStatuses = (from user in RuntimeContext.Repositories.Core.Users
															join employee in RuntimeContext.Repositories.Core.Employees
																on user.PersonId equals employee.PersonId
															join employer in RuntimeContext.Repositories.Core.Employers
																on employee.EmployerId equals employer.Id
															where user.Id == userId
															select new
															{
																EmployeeId = employee.Id,
																EmployeeApprovalStatus = employee.ApprovalStatus,
																EmployerApprovalStatus = employer.ApprovalStatus,
															}).FirstOrDefault();

			if (employeeStatuses.IsNotNull())
			{
				// Check employer is not denied
				if (employeeStatuses.EmployeeApprovalStatus == ApprovalStatuses.Rejected)
				{
					return new LoginModel
					{
						FailureType = ErrorTypes.EmployeeIsDenied,
						FailureMessage = string.Format("Talent user '{0}' has been denied", userName)
					};
				}

				// Also check if they are awaiting approval
				if (employeeStatuses.EmployeeApprovalStatus == ApprovalStatuses.WaitingApproval && RuntimeContext.AppSettings.NewHiringManagerApproval == TalentApprovalOptions.ApprovalSystemNotAvailable)
				{
					return new LoginModel
					{
						FailureType = ErrorTypes.EmployerNotApproved,
						FailureMessage = string.Format("Hiring Manager '{0}' is not approved", userName)
					};
				}

				if (employeeStatuses.EmployerApprovalStatus == ApprovalStatuses.WaitingApproval && RuntimeContext.AppSettings.NewEmployerApproval == TalentApprovalOptions.ApprovalSystemNotAvailable)
				{
					return new LoginModel
					{
						FailureType = ErrorTypes.EmployerNotApproved,
						FailureMessage = string.Format("Employer '{0}' is not approved", userName)
					};
				}
				/* Commented out - See FVN-3690
				if (employeeStatuses.EmployerApprovalStatus == ApprovalStatuses.OnHold)
				{
					return new LoginModel
					{
						FailureType = ErrorTypes.EmployerOnHold,
						FailureMessage = string.Format("Employer '{0}' is on hold", userName)
					};
				}
				*/
				var businessUnitStatus = (from employeeBusinessUnit in RuntimeContext.Repositories.Core.EmployeeBusinessUnits
																	join businessUnit in RuntimeContext.Repositories.Core.BusinessUnits
																		on employeeBusinessUnit.BusinessUnitId equals businessUnit.Id
																	where employeeBusinessUnit.EmployeeId == employeeStatuses.EmployeeId
																	select businessUnit.ApprovalStatus).FirstOrDefault();

				if (businessUnitStatus == ApprovalStatuses.WaitingApproval && RuntimeContext.AppSettings.NewBusinessUnitApproval == TalentApprovalOptions.ApprovalSystemNotAvailable)
				{
					return new LoginModel
					{
						FailureType = ErrorTypes.EmployerNotApproved,
						FailureMessage = string.Format("Business unit '{0}' is not approved", userName)
					};
				}
			}

			return null;
		}

		/// <summary>
		/// Allow extra processing when a user does not exist 
		/// </summary>
		/// <param name="userName">The name of the user</param>
		/// <returns></returns>
		protected override LoginModel NonUserProcessing(string userName)
		{
			// Raise a specific message for Talent users if they have been denied (as their user name may have been changed in these circumstances)
			if (RuntimeContext.Repositories.Core.Users.Any(u => u.UserType == UserTypes.Talent
																					 && u.UserName.StartsWith("DENIED_")
																					 && u.UserName.EndsWith("_" + userName)
																					 && u.Person.Employee != null
																					 && u.Person.Employee.ApprovalStatus == ApprovalStatuses.Rejected))
			{
				return new LoginModel
				{
					FailureType = ErrorTypes.EmployeeIsDenied,
					FailureMessage = string.Format("Talent user '{0}' has been denied", userName)
				};
			}

			return null;
		}
	}
}
