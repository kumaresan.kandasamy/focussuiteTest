﻿#region Copyright © 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Linq;

using Focus.Core;
using Focus.Core.Models;
using Focus.Core.Views;

using Framework.Authentication;
using Framework.Core;

#endregion

namespace Focus.Services.Core.Login
{
	public class FocusLoginProvider : LoginProvider, ILoginProvider
	{
		public FocusLoginProvider(IRuntimeContext runtimeContext, FocusModules module) : base(runtimeContext, module)
		{
			
		}

		public LoginModel Authenticate(string userName, string password, string externalId, string firstName, string lastName, string screenName, string emailAddress)
		{
			ValidatedUserView userView = null;

			// Check if user name exists in the database
			var userDetails = RuntimeContext.Repositories.Core.Users.Where(x => x.UserName == userName).Select(u => new
			{
				u.Id,
				u.PersonId,
				u.PasswordHash,
				u.PasswordSalt,
				u.MigratedPasswordHash,
				u.ExternalId
			}).FirstOrDefault();

			if (userDetails.IsNotNull())
			{
				var loginModel = UserValidation(userDetails.Id, userName);
				if (loginModel.IsNotNull())
				{
					return loginModel;
				}

				// Is the password valid
				if (userDetails.MigratedPasswordHash.IsNullOrEmpty())
				{
					var checkPassword = new Password(RuntimeContext.AppSettings, password, userDetails.PasswordSalt, false);
					if (userDetails.PasswordHash != checkPassword.Hash)
					{
						return new LoginModel
						{
							FailureType = ErrorTypes.InvalidUserNameOrPassword,
							FailureMessage = string.Format("Invalid password for user '{0}'", userName)
						};
					}

					userView = new ValidatedUserView();
				}
				else
				{
					// A migrated password from V1 uses MD5 encryption
					if (userDetails.MigratedPasswordHash != Password.GetMD5Hash(password))
					{
						return new LoginModel
						{
							FailureType = ErrorTypes.InvalidUserNameOrPassword,
							FailureMessage = string.Format("Invalid password for user '{0}'", userName)
						};
					}

					// Password needs to be updated to a V3 password (using SHA-2)
					userView = new ValidatedUserView
					{
						Password = password
					};
				}

				userView.FocusUserId = userDetails.Id;

				// If provider has been called after previously checking integration layer, external details can be obtained from the user table
				SetExternalDetails(userView, userDetails.PersonId, userDetails.ExternalId, userName, password);
			}
			else
			{
				var loginModel = NonUserProcessing(userName);
				if (loginModel.IsNotNull())
				{
					return loginModel;
				}
			}

			return new LoginModel
			{
				UserView = userView,
				UpdateFocus = userView.IsNotNull() && userView.Password.IsNotNullOrEmpty()
			};
		}

		/// <summary>
		/// Allow extra validation of the user for modules
		/// </summary>
		/// <param name="userId">The id of the user</param>
		/// <param name="userName">The name of the user</param>
		/// <returns>A user model if validation errors occur. null if not</returns>
		protected virtual LoginModel UserValidation(long userId, string userName)
		{
			return null;
		}

		/// <summary>
		/// Sets the external fields
		/// </summary>
		/// <param name="userView">The current user model</param>
		/// <param name="personId">The id of the person in Focus</param>
		/// <param name="userName">Name of the user.</param>
		/// <param name="password">The password.</param>
		/// <param name="externalId">The external identifier.</param>
		protected virtual void SetExternalDetails(ValidatedUserView userView, long personId, string userName, string password, string externalId)
		{
			var postCodeZip = RuntimeContext.Repositories.Core.PersonAddresses
																												.Where(pa => pa.PersonId == personId)
																												.OrderByDescending(pa => pa.IsPrimary)
																												.ThenByDescending(pa => pa.Id)
																												.Select(pa => pa.PostcodeZip)
																												.FirstOrDefault();
			if (postCodeZip.IsNotNullOrEmpty())
			{
				var lookup = RuntimeContext.Repositories.Configuration.ExternalLookUpItems
																															.FirstOrDefault(e => e.ExternalLookUpType == ExternalLookUpType.IntegrationOfficeIdPerZip && e.InternalId == postCodeZip);
				if (lookup.IsNotNull())
				{
					userView.ExternalOfficeId = lookup.ExternalId;
					userView.ExternalUserName = string.Format("selfreg{0}", userView.ExternalOfficeId.ToLower());
				}
			}

			if (userView.ExternalOfficeId.IsNullOrEmpty())
			{
				if (Module == FocusModules.Talent)
				{
					var employerOfficeExternalId = (from employee in RuntimeContext.Repositories.Core.Employees
																					join employer in RuntimeContext.Repositories.Core.Employers
																						on employee.EmployerId equals employer.Id
																					join mapper in RuntimeContext.Repositories.Core.EmployerOfficeMappers
																						on employer.Id equals mapper.EmployerId
																					join office in RuntimeContext.Repositories.Core.Offices
																						on mapper.OfficeId equals office.Id
																					where employee.PersonId == personId
																						&& office.ExternalId != null && office.ExternalId != ""
																					select office.ExternalId).FirstOrDefault();

					if (employerOfficeExternalId.IsNotNullOrEmpty())
					{
						userView.ExternalOfficeId = employerOfficeExternalId;
						userView.ExternalUserName = string.Format("selfreg{0}", userView.ExternalOfficeId.ToLower());
					}
				}

				var officeExternalId = (from mapper in RuntimeContext.Repositories.Core.PersonOfficeMappers
																join office in RuntimeContext.Repositories.Core.Offices
																	on mapper.OfficeId equals office.Id
																where mapper.PersonId == personId
																			&& office.ExternalId != null && office.ExternalId != ""
																select office.ExternalId).FirstOrDefault();

				if (officeExternalId.IsNotNullOrEmpty())
				{
					userView.ExternalOfficeId = officeExternalId;
					userView.ExternalUserName = string.Format("selfreg{0}", userView.ExternalOfficeId.ToLower());
				}
			}
		}

		/// <summary>
		/// Allow extra processing when a user does not exist 
		/// </summary>
		/// <param name="userName">The name of the user</param>
		/// <returns>A user model if validation errors occur. null if not</returns>
		protected virtual LoginModel NonUserProcessing(string userName)
		{
			return null;
		}
	}
}
