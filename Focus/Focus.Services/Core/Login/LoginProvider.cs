﻿#region Copyright © 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using Focus.Core;

#endregion

namespace Focus.Services.Core.Login
{
	public abstract class LoginProvider
	{
		protected readonly IRuntimeContext RuntimeContext;

		protected readonly FocusModules Module;

		protected LoginProvider(IRuntimeContext runtimeContext, FocusModules module)
		{
			RuntimeContext = runtimeContext;
			Module = module;
		}
	}
}
