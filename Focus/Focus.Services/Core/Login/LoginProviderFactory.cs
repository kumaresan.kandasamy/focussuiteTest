﻿#region Copyright © 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;

using Focus.Core;

#endregion

namespace Focus.Services.Core.Login
{
	public class LoginProviderFactory
	{
		public readonly ILoginProvider Provider;

		public LoginProviderFactory(IRuntimeContext runtimeContext, FocusModules module, LoginType loginType)
		{
      switch (loginType)
      {
        case LoginType.Standard:
		      if (module == FocusModules.Talent)
		      {
			      Provider = new FocusTalentLoginProvider(runtimeContext, module);
		      }
		      else if (module == FocusModules.Assist)
		      {
			      Provider = new FocusAssistLoginProvider(runtimeContext, module);
		      }
		      else
		      {
						Provider = new FocusLoginProvider(runtimeContext, module);
		      }
		      break;

				case LoginType.SSO:
					Provider = new SSOLoginProvider(runtimeContext, module);
					break;

				case LoginType.IntegrationClient:
					Provider = new IntegrationClientLoginProvider(runtimeContext, module);
					break;

				default:
					throw new ArgumentException(string.Format("Login provider for type '{0}' is unknown", loginType), "loginType");
      }
		}
	}
}
