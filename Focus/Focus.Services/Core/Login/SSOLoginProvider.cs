﻿#region Copyright © 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;

using Focus.Core;
using Focus.Core.Models;
using Focus.Core.Views;
using Framework.Core;

#endregion

namespace Focus.Services.Core.Login
{
	public class SSOLoginProvider : LoginProvider, ILoginProvider
	{
		public SSOLoginProvider(IRuntimeContext runtimeContext, FocusModules module) : base(runtimeContext, module)
		{
			
		}

		public LoginModel Authenticate(string userName, string password, string externalId, string firstName, string lastName, string screenName, string emailAddress)
		{
			ValidatedUserView userView;

			if (externalId.IsNullOrEmpty())
				throw new Exception("External Id not provided");

			// Check if user exists based on external id
			var userId = RuntimeContext.Repositories.Core.Users.Where(x => x.ExternalId == externalId).Select(u => u.Id).FirstOrDefault();
			if (userId == 0)
			{
				ErrorTypes? errorType = null;
				string errorField = null;

				// User does not exist, so validate the fields to ensure a new user can be added to the database
				if (externalId.IsNullOrEmpty())
				{
					errorType = ErrorTypes.RegistrationFieldRequired;
					errorField = "ExternalId";
				}
				else if (screenName.IsNullOrEmpty())
				{
					errorType = ErrorTypes.RegistrationFieldRequired;
					errorField = "ScreenName";
				}
				else if (emailAddress.IsNullOrEmpty())
				{
					errorType = ErrorTypes.RegistrationFieldRequired;
					errorField = "EmailAddress";
				}
				else if (firstName.IsNullOrEmpty())
				{
					errorType = ErrorTypes.RegistrationFieldRequired;
					errorField = "FirstName";
				}
				else if (lastName.IsNullOrEmpty())
				{
					errorType = ErrorTypes.RegistrationFieldRequired;
					errorField = "LastName";
				}

				if (errorType.IsNotNull())
				{
					return new LoginModel
					{
						FailureType = errorType.Value,
						FailureMessage = errorField
					};
				}

				// Populate the user model with details so that it can be added to the Focus database
				firstName = firstName.IsNotNullOrEmpty() ? firstName : "Unknown";
				lastName = lastName.IsNotNullOrEmpty() ? lastName : "Unknown";

				userView = new ValidatedUserView
				{
					UserName = String.Format("sso-{0}@ci.bgt.com", externalId),
					Password = String.Format("Pasty99{0}", externalId),
					ExternalId = externalId,
					FirstName = firstName,
					LastName = lastName,
					ScreenName = screenName.IsNotNullOrEmpty() ? screenName : string.Concat(firstName," ", lastName),
					EmailAddress = emailAddress,
					IsMigrated = (Module == FocusModules.Explorer),
					Roles = (Module == FocusModules.Assist)
										? RuntimeContext.AppSettings.SamlDefaultAssistRoles.Split(';').Distinct().ToList()
										: null
				};
			}
			else
			{
				// User exists, so populate details that can be updated
				userView = new ValidatedUserView
				{
					FocusUserId = userId,
					EmailAddress = emailAddress,
					FirstName = firstName,
					LastName = lastName
				};
			}

			// Return details of the user model, with a flag to say it can either be updated or added to the Focus database
			return new LoginModel
			{
				UserView = userView,
				UpdateFocus = true,
				UpdateFromIntegration = true,
				BypassEnabledCheck = true,
				UserLogAction = (userView.FocusUserId == 0 ) ? ActionTypes.RegisterOAuthAccount : ActionTypes.NoAction
			};
		}
	}
}
