﻿#region Copyright © 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;

using Focus.Core;
using Focus.Core.IntegrationMessages;
using Focus.Core.Models;
using Focus.Core.Views;
using Framework.Core;

#endregion

namespace Focus.Services.Core.Login
{
	public class IntegrationClientLoginProvider : LoginProvider, ILoginProvider
	{
		public IntegrationClientLoginProvider(IRuntimeContext runtimeContext, FocusModules module) : base(runtimeContext, module)
		{
			
		}

		public LoginModel Authenticate(string userName, string password, string externalId, string firstName, string lastName, string screenName, string emailAddress)
		{
			ValidatedUserView userView = null;
			var loginModel = new LoginModel();

			// Check if Assist needs to be authenticated via the client system
			if (Module == FocusModules.Assist && RuntimeContext.AppSettings.AuthenticateAssistUserViaClient && RuntimeContext.Repositories.Integration.AuthenticationSupported(Module) == IntegrationAuthentication.Full)
			{
				// Call the client system to authenticate the Assist user
				var authenticationResponse = RuntimeContext.Repositories.Integration.AuthenticateStaffUser(new AuthenticateStaffRequest { Username = userName, Password = password });
				var staffUser = authenticationResponse.StaffUserInfo;
				if (staffUser.IsNotNull() && staffUser.ExternalId.IsNotNull())
				{
					// Check if user exists in Focus. If not, they will need to be added
					var userId = RuntimeContext.Repositories.Core.Users.Where(x => x.ExternalId == staffUser.ExternalId).Select(u => u.Id).FirstOrDefault();
					if (userId == 0)
					{
						// Create dummy username so user cannot access system directly
						var dummyUserName = string.Format(RuntimeContext.AppSettings.DummyUsernameFormat, userName);
						var dummyEmailName = dummyUserName.Contains("@") ? dummyUserName : string.Format(RuntimeContext.AppSettings.DummyEmailFormat, dummyUserName);

						userView = new ValidatedUserView
						{
							FirstName = (staffUser.FirstName.IsNotNullOrEmpty()) ? staffUser.FirstName : "Undefined",
							LastName = (staffUser.Surname.IsNotNullOrEmpty()) ? staffUser.Surname : "Undefined",
							UserName = dummyUserName,
							Password = password,
							ExternalId = staffUser.ExternalId,
							EmailAddress = (staffUser.Email.IsNotNullOrEmpty()) ? staffUser.Email : dummyEmailName,
							PhoneNumber = (staffUser.PhoneNumber.IsNotNullOrEmpty()) ? staffUser.PhoneNumber : "Undefined",
							IsClientAuthenticated = true,
							Roles = new List<string>
							{
								Constants.RoleKeys.AssistEmployersReadOnly,
								Constants.RoleKeys.AssistJobSeekersReadOnly,
								Constants.RoleKeys.AssistJobSeekersAdministrator
							}
						};

						loginModel.UpdateFocus = true;
					}
					else
					{
						userView = new ValidatedUserView
						{
							FocusUserId = userId
						};
					}

					userView.ExternalUserId = staffUser.ExternalId;
					userView.ExternalUserName = userName;
					userView.ExternalPassword = password;
					if (staffUser.AuthModel.IsNotNull())
					{
						userView.ExternalOfficeId = (staffUser.AuthModel.ExternalOfficeId.IsNotNullOrEmpty())
																					 ? staffUser.AuthModel.ExternalOfficeId
																					 : "Undefined";
					}
				}
				else
				{
					// If client authentication fails (which may be due to connection issues), fall back to Focus authentication
					loginModel.FallbackToFocus = true;
				}
			}
			else if (Module.IsIn(FocusModules.Explorer, FocusModules.CareerExplorer, FocusModules.Career) && RuntimeContext.Repositories.Integration.AuthenticationSupported(Module) == IntegrationAuthentication.ReturnDetails)
			{
				// Currently, Career/Explorer users are not actually authenticated. However, if they don't exist in Focus, the registration page can be pre-populated with external details
				var userId = RuntimeContext.Repositories.Core.Users.Where(x => x.UserName == userName).Select(u => u.Id).FirstOrDefault();
				if (userId == 0)
				{
					// Get job seeker details from client system so they can be passed back to registration page if required
					var authResponse = RuntimeContext.Repositories.Integration.AuthenticateJobSeeker(new AuthenticateJobSeekerRequest { UserName = userName, Password = password });
					if (authResponse.Outcome == IntegrationOutcome.Success && authResponse.JobSeeker.IsNotNull())
					{
						var jobSeeker = authResponse.JobSeeker;
						userView = new ValidatedUserView
						{
							ExternalId = jobSeeker.ExternalId,
							EmailAddress = jobSeeker.EmailAddress,
							FirstName = jobSeeker.FirstName,
							MiddleInitial = jobSeeker.MiddleInitial,
							LastName = jobSeeker.LastName,
							DateOfBirth = jobSeeker.DateOfBirth.GetValueOrDefault(DateTime.MinValue) != DateTime.MinValue ? jobSeeker.DateOfBirth : null,
							AddressLine1 = jobSeeker.AddressLine1,
							AddressTownCity = jobSeeker.AddressTownCity,
							AddressStateId = jobSeeker.AddressStateId,
							AddressCountyId = jobSeeker.AddressCountyId,
							AddressCountryId = jobSeeker.AddressCountryId,
							AddressPostcodeZip = jobSeeker.AddressPostcodeZip,
							PhoneNumber = jobSeeker.PrimaryPhone,
							SocialSecurityNumber = jobSeeker.SocialSecurityNumber
						};
					}
				}
				else
				{
					// Fall back to focus, as currently authentication of job seekers is not supported by the integration layer
					loginModel.FallbackToFocus = true;
				}
			}
			else
			{
				// Fall back to Focus in all other cases
				loginModel.FallbackToFocus = true;
			}

			loginModel.UserView = userView;
			return loginModel;
		}
	}
}
