﻿#region Copyright © 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using Focus.Core.Models;

#endregion

namespace Focus.Services.Core.Login
{
	public interface ILoginProvider
	{
		LoginModel Authenticate(string userName, string password, string externalId, string firstName, string lastName, string screenName, string emailAddress);
	}
}
