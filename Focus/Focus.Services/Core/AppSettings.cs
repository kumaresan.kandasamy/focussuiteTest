﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Web;

using ComponentPro.Saml2;

using Framework.Core;
using Framework.Logging;
using Framework.PageState;
using Framework.ServiceLocation;

using Focus.Core;
using Focus.Core.Settings;
using Focus.Core.Settings.Interfaces;
using Focus.Core.Settings.Lens;
using Focus.Core.Settings.OAuth;
using Focus.Core.Views;

#endregion

namespace Focus.Services.Core
{
    public class AppSettings : IAppSettings
    {
        private IEnumerable<ConfigurationItemView> _configurationItems;
        private readonly FocusModules _module;
        private readonly FocusThemes _theme;
        private readonly string _configKeyWithThemeAndModule;
        private readonly string _configKeyWithTheme;
        private DateTime _lastLoadFromCache = DateTime.MinValue;
        private readonly int _cacheTimeoutInMinutes;
        private readonly object _syncLock = new object();
        private readonly IConfigurationHelper _configurationHelper;

        /// <summary>
        /// Initializes a new instance of the <see cref="AppSettings" /> class.
        /// </summary>
        public AppSettings()
            : this(null)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="AppSettings" /> class.
        /// </summary>
        /// <param name="configurationItems">The configuration items.</param>
        /// <param name="cacheTimeoutInMinutes">The cache timeout in minutes. Default value is 5 (minutes).</param>
        public AppSettings(IEnumerable<ConfigurationItemView> configurationItems, int cacheTimeoutInMinutes = 5)
        {
            lock (_syncLock)
            {
                _cacheTimeoutInMinutes = cacheTimeoutInMinutes;
                _configurationHelper = ServiceLocator.Current.Resolve<IConfigurationHelper>();

                if (configurationItems.IsNull())
                {
                    if (_configurationHelper.IsNotNull())
                        _configurationItems = _configurationHelper.ConfigurationItems;
                }
                else
                    _configurationItems = configurationItems;

                _lastLoadFromCache = DateTime.Now;
            }

            _module = (FocusModules)Enum.Parse(typeof(FocusModules), GetStringWebConfigValue(Constants.WebConfigKeys.Module, FocusModules.Talent.ToString()), true);
            _theme = (FocusThemes)Enum.Parse(typeof(FocusThemes), GetStringConfigItemValue(Constants.ConfigurationItemKeys.Theme, FocusThemes.Workforce.ToString()), true);

            //_theme = FocusThemes.Education;
            _configKeyWithThemeAndModule = "{0}-" + _theme + "-" + _module;
            _configKeyWithTheme = "{0}-" + _theme;
        }

        /// <summary>
        /// Gets the configuration settings.
        /// </summary>
        /// <returns></returns>
        public ConfigSettingInfo[] GetConfigSettings()
        {
            var type = typeof(IAppSettings);

            var memberInfos = type.GetMembers().Where(x => x.GetCustomAttributes(typeof(ConfigSettingAttribute), true).Length > 0).ToList();
            // ReSharper disable PossibleNullReferenceException
            var configSettings = (from memberInfo in memberInfos
                                  let configSetting = (ConfigSettingAttribute)memberInfo.GetCustomAttributes(typeof(ConfigSettingAttribute), true).First()
                                  let configSettingType = configSetting is DbConfigSettingAttribute ? ConfigSettingType.Db : ConfigSettingType.Web
                                  let defaultValue = configSetting is DbConfigSettingAttribute ? (configSetting as DbConfigSettingAttribute).Default : null
                                  let currentValue = configSetting is DbConfigSettingAttribute ? (configSetting as DbConfigSettingAttribute).Default : null
                                  select new ConfigSettingInfo(memberInfo.Name, configSettingType, configSetting.Category, configSetting.SubCategory, configSetting.Key, configSetting.DisplayName, configSetting.Description, configSetting.DisplayOrder, defaultValue, currentValue)).ToArray();
            // ReSharper restore PossibleNullReferenceException

            foreach (var cfgSetting in configSettings)
                if (cfgSetting.CurrentValue is bool)
                    cfgSetting.CurrentValue = GetBoolConfigItemValue(cfgSetting.Key, (bool)cfgSetting.DefaultValue);
                else if (cfgSetting.CurrentValue is string)
                    cfgSetting.CurrentValue = GetStringConfigItemValue(cfgSetting.Key, (string)cfgSetting.DefaultValue);
                else if (cfgSetting.CurrentValue is int)
                    cfgSetting.CurrentValue = GetIntConfigItemValue(cfgSetting.Key, (int)cfgSetting.DefaultValue);
                else if (cfgSetting.CurrentValue is long)
                    cfgSetting.CurrentValue = GetLongConfigItemValue(cfgSetting.Key, (long)cfgSetting.DefaultValue);
                else if (cfgSetting.CurrentValue is int[])
                    cfgSetting.CurrentValue = GetIntArrayConfigItemValue(cfgSetting.Key, (int[])cfgSetting.DefaultValue);
                else if (cfgSetting.CurrentValue is string[])
                    cfgSetting.CurrentValue = GetStringArrayConfigValue(cfgSetting.Key, (string[])cfgSetting.DefaultValue);
                else if (cfgSetting.CurrentValue is Enum)
                {
                    if (cfgSetting.CurrentValue is FocusModules)
                    {
                        var temp = GetEnumConfigItemValue(cfgSetting.Key, (FocusModules)cfgSetting.DefaultValue);
                        cfgSetting.CurrentValue = Enum.Parse(typeof(FocusModules), temp.ToString(), true);
                    }
                    else if (cfgSetting.CurrentValue is JobAlertsOptions)
                    {
                        var temp = GetEnumConfigItemValue(cfgSetting.Key, (JobAlertsOptions)cfgSetting.DefaultValue);
                        cfgSetting.CurrentValue = Enum.Parse(typeof(JobAlertsOptions), temp.ToString(), true);
                    }
                    else if (cfgSetting.CurrentValue is AlertFrequencies)
                    {
                        var temp = GetEnumConfigItemValue(cfgSetting.Key, (AlertFrequencies)cfgSetting.DefaultValue);
                        cfgSetting.CurrentValue = Enum.Parse(typeof(AlertFrequencies), temp.ToString(), true);
                    }
                    else if (cfgSetting.CurrentValue is DistanceUnits)
                    {
                        var temp = GetEnumConfigItemValue(cfgSetting.Key, (DistanceUnits)cfgSetting.DefaultValue);
                        cfgSetting.CurrentValue = Enum.Parse(typeof(DistanceUnits), temp.ToString(), true);
                    }
                    else if (cfgSetting.CurrentValue is SearchCentrePointOptions)
                    {
                        var temp = GetEnumConfigItemValue(cfgSetting.Key, (SearchCentrePointOptions)cfgSetting.DefaultValue);
                        cfgSetting.CurrentValue = Enum.Parse(typeof(SearchCentrePointOptions), temp.ToString(), true);
                    }
                    else if (cfgSetting.CurrentValue is ResumeSearchableOptions)
                    {
                        var temp = GetEnumConfigItemValue(cfgSetting.Key, (ResumeSearchableOptions)cfgSetting.DefaultValue);
                        cfgSetting.CurrentValue = Enum.Parse(typeof(ResumeSearchableOptions), temp.ToString(), true);
                    }
                    else if (cfgSetting.CurrentValue is DegreeFilteringType)
                    {
                        var temp = GetEnumConfigItemValue(cfgSetting.Key, (DegreeFilteringType)cfgSetting.DefaultValue);
                        cfgSetting.CurrentValue = Enum.Parse(typeof(DegreeFilteringType), temp.ToString(), true);
                    }
                    else if (cfgSetting.CurrentValue is CareerExplorerFeatureEmphasis)
                    {
                        var temp = GetEnumConfigItemValue(cfgSetting.Key, (CareerExplorerFeatureEmphasis)cfgSetting.DefaultValue);
                        cfgSetting.CurrentValue = Enum.Parse(typeof(CareerExplorerFeatureEmphasis), temp.ToString(), true);
                    }
                    else if (cfgSetting.CurrentValue is SchoolTypes)
                    {
                        var temp = GetEnumConfigItemValue(cfgSetting.Key, (SchoolTypes)cfgSetting.DefaultValue);
                        cfgSetting.CurrentValue = Enum.Parse(typeof(SchoolTypes), temp.ToString(), true);
                    }
                    else if (cfgSetting.CurrentValue is TalentApprovalOptions)
                    {
                        var temp = GetEnumConfigItemValue(cfgSetting.Key, (TalentApprovalOptions)cfgSetting.DefaultValue);
                        cfgSetting.CurrentValue = Enum.Parse(typeof(TalentApprovalOptions), temp.ToString(), true);
                    }
                    else if (cfgSetting.CurrentValue is RegistrationRoute)
                    {
                        var temp = GetEnumConfigItemValue(cfgSetting.Key, (RegistrationRoute)cfgSetting.DefaultValue);
                        cfgSetting.CurrentValue = Enum.Parse(typeof(RegistrationRoute), temp.ToString(), true);
                    }
                    else if (cfgSetting.CurrentValue is ControlDisplayType)
                    {
                        var temp = GetEnumConfigItemValue(cfgSetting.Key, (ControlDisplayType)cfgSetting.DefaultValue);
                        cfgSetting.CurrentValue = Enum.Parse(typeof(ControlDisplayType), temp.ToString(), true);
                    }
                }

            return configSettings;
        }

        #region Web Config

        /// <summary>
        /// Gets the module.
        /// </summary>
        public FocusModules Module
        {
            get { return _module; }
        }

        /// <summary>
        /// Gets the application.
        /// </summary>
        public string Application
        {
            get { return GetStringWebConfigValue(Constants.WebConfigKeys.Application, "Focus Talent"); }
        }

        /// <summary>
        /// Gets the client tag.
        /// </summary>
        public string ClientTag
        {
            get { return GetStringWebConfigValue(Constants.WebConfigKeys.ClientTag, "ABC123"); }
        }

        /// <summary>
        /// Gets the Migration Provider.
        /// </summary>
        public string MigrationProvider
        {
            get { return GetStringWebConfigValue(Constants.WebConfigKeys.MigrationProvider, "Fake"); }
        }

        /// <summary>
        /// Gets the Migration Batch Size (the number of records to process to the Migration databas).
        /// </summary>
        public int MigrationBatchSize
        {
            get { return GetIntWebConfigValue(Constants.WebConfigKeys.MigrationBatchSize, 10); }
        }

        /// <summary>
        /// Gets the Import Batch Size (the number of records to process to the Migration databas).
        /// </summary>
        public int ImportBatchSize
        {
            get { return GetIntWebConfigValue(Constants.WebConfigKeys.ImportBatchSize, MigrationBatchSize); }
        }

        public bool EnableFrontEndMigration
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.EnableFrontEndMigration, false); }
        }

        public int IntegrationImportBatchSize
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.IntegrationImportBatchSize, 1); }
        }

        #region Service Method Validation

        /// <summary>
        /// Gets whether to check the version number in service method calls
        /// </summary>
        public bool ValidateServiceVersion
        {
            get { return GetBoolWebConfigValue(Constants.WebConfigKeys.ValidateServiceVersion, false); }
        }

        /// <summary>
        /// Gets whether AccessToken validation of service methods is enabled
        /// </summary>
        public bool UsesAccessToken
        {
            get { return GetBoolWebConfigValue(Constants.WebConfigKeys.UsesAccessToken, false); }
        }

        #endregion

        #region Database encryption

        /// <summary>
        /// Gets whether encryption of database fields is enabled
        /// </summary>
        public bool EncryptionEnabled
        {
            get { return GetBoolWebConfigValue(Constants.WebConfigKeys.EncryptionEnabled, false); }
        }

        /// <summary>
        /// Gets the key for encrypting database fields
        /// </summary>
        public string EncryptionKey
        {
            get { return GetStringWebConfigValue(Constants.WebConfigKeys.EncryptionKey, string.Empty); }
        }

        /// <summary>
        /// Gets the initialisaton vector for encryption
        /// </summary>
        public string EncryptionVector
        {
            get { return GetStringWebConfigValue(Constants.WebConfigKeys.EncryptionVector, string.Empty); }
        }

        #endregion

        #region Shared encryption

        public string SharedEncryptionKey
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.SharedEncryptionKey, string.Empty); }
            set { }
        }

        /// <summary>
        /// Gets the initialisaton vector for encryption
        /// </summary>
        public string SharedEncryptionVector
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.SharedEncryptionVector, string.Empty); }
            set { }
        }

        #endregion

        #endregion

        #region Configuration Items

        /// <summary>
        /// Gets the theme.
        /// </summary>
        /// <value>The theme.</value>
        public FocusThemes Theme
        {
            get { return _theme; }
        }

        /// <summary>
        /// Gets the log severity.
        /// </summary>
        public LogSeverity LogSeverity
        {
            get { return GetEnumConfigItemValue(Constants.ConfigurationItemKeys.LogSeverity, LogSeverity.Error); }
            // (LogSeverity)Enum.Parse(typeof(LogSeverity), GetStringConfigItemValue(Constants.ConfigurationItemKeys.LogSeverity, LogSeverity.Error.ToString()), true); }
        }

        /// <summary>
        /// Gets a value indicating whether profiling is enabled.
        /// </summary>
        public bool ProfilingEnabled
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ProfilingEnabled, false); }
        }

        /// <summary>
        /// Show proofing markup when viewing a job in Talent
        /// </summary>
        public bool JobProofingEnabled
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.JobProofingEnabled, false); }
        }

        /// <summary>
        /// Gets the cache prefix.
        /// </summary>
        public string CachePrefix
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.CachePrefix, "Focus"); }
        }

        /// <summary>
        /// Gets the on error email.
        /// </summary>
        public string OnErrorEmail
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.OnErrorEmail, "ukdevelopers@burning-glass.com", true); }
        }

        /// <summary>
        /// Gets a value indicating whether [emails enabled].
        /// </summary>
        public bool EmailsEnabled
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.EmailsEnabled, true); }
        }

        /// <summary>
        /// Direct emails, rather than by message bus
        /// </summary>
        public bool DirectEmails
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.DirectEmails, false); }
        }

        /// <summary>
        /// Send emails asynchronously
        /// </summary>
        public bool ASyncEmails
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ASyncEmails, false); }
        }

        /// <summary>
        /// Gets the email redirect. Leave empty for no redirect.
        /// </summary>
        /// <value>
        /// The email redirect.
        /// </value>
        public string EmailRedirect
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.EmailRedirect, string.Empty, true); }
        }

        /// <summary>
        /// Gets the email log file location. Leave empty for no logging.
        /// </summary>
        /// <value>
        /// The email log file location.
        /// </value>
        public string EmailLogFileLocation
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.EmailLogFileLocation, string.Empty, true); }
        }

        /// <summary>
        /// The maximum length allowed for an email address.
        /// </summary>
        /// <value>
        /// The maximum email length.
        /// </value>
        public int MaximumEmailLength
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.MaximumEmailLength, 100); }
        }

        /// <summary>
        /// The maximum length allowed for a username
        /// </summary>
        /// <value>
        /// The maximum username length.
        /// </value>
        public int MaximumUserNameLength
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.MaximumUserNameLength, 100); }
        }

        /// <summary>
        /// Gets whether to show hide the FEIN during Talent registration.
        /// </summary>
        /// <value>
        /// Either Mandatory, Hi
        /// </value>
        public ControlDisplayType DisplayFEIN
        {
            get { return GetEnumConfigItemValue(Constants.ConfigurationItemKeys.DisplayFEIN, (Theme == FocusThemes.Workforce ? ControlDisplayType.Mandatory : ControlDisplayType.Hidden)); }
        }

        /// <summary>
        /// Gets the show supply and demand report setting.
        /// </summary>
        /// <value></value>
        public bool ShowSupplyDemandReport
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ShowSupplyDemandReport, false); }
        }

        #region Demo Shield Settings

        /// <summary>
        /// Gets the demo shield customer code.
        /// </summary>
        public string DemoShieldCustomerCode
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.DemoShieldCustomerCode, string.Empty, true); }
        }

        /// <summary>
        /// Gets the demo shield redirect url.
        /// </summary>
        public string DemoShieldRedirectUrl
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.DemoShieldRedirectUrl, string.Empty, true); }
        }

        #endregion

        #region Page State Configuration

        /// <summary>
        /// Gets the page state persistor.
        /// </summary>
        public PageStatePersisters PageStatePersister
        {
            get
            {
                var value = GetStringConfigItemValue(Constants.ConfigurationItemKeys.PageStatePersister, PageStatePersisters.MsSql.ToString());
                PageStatePersisters pageStatePersister;

                Enum.TryParse(value, true, out pageStatePersister);
                return pageStatePersister;
            }
        }

        /// <summary>
        /// Gets the length of the page state queue max.
        /// </summary>
        public int PageStateQueueMaxLength
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.PageStateQueueMaxLength, 10); }
        }

        /// <summary>
        /// Gets the page state time out.
        /// </summary>
        public int PageStateTimeOut
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.PageStateTimeOut, 2880); }
        }

        /// <summary>
        /// Gets the page state connection string.
        /// </summary>
        public string PageStateConnectionString
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.PageStateConnectionString, "SQLServerDataCore", true); }
        }

        #endregion

        /// <summary>
        /// Gets the career explorer module.
        /// </summary>
        /// <value>The career explorer module.</value>
        public FocusModules CareerExplorerModule
        {
            get { return GetEnumConfigItemValue(Constants.ConfigurationItemKeys.CareerExplorerModule, FocusModules.CareerExplorer); }
        }

        /// <summary>
        /// Gets the license expiration. Maximum is 1440 minutes or 24 hours
        /// </summary>
        public int LicenseExpiration
        {
            get
            {
                var licenseExpiration = GetIntConfigItemValue(Constants.ConfigurationItemKeys.LicenseExpiration, 120);
                return (licenseExpiration > 1440 ? 1440 : licenseExpiration);
            }
        }

        /// <summary>
        /// Gets the app version.
        /// </summary>
        /// 
        public string AppVersion
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.AppVersion, @"<span style=""color:red;font-weight:bold;"">DEV-BDD</span>"); }
        }

        /// <summary>
        /// Gets whether to show Change SSN activity in dropdowns.
        /// </summary>
        public bool AssistShowChangeSSNActivityInWF
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.AssistShowChangeSSNActivityInWF, false); }
        }

        /// <summary>
        /// Gets a value indicating whether to save default localisation keys.
        /// </summary>
        public bool SaveDefaultLocalisationItems
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.SaveDefaultLocalisationItems, false); }
        }

        #region Settings that nobody has categorised as yet

        /// <summary>
        /// Gets the nation wide instance.
        /// True by default for education, false for workforce
        /// </summary>
        /// 
        public bool NationWideInstance
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.NationWideInstance, Theme == FocusThemes.Education); }
        }

        /// <summary>
        /// Gets the my account enabled flag.
        /// </summary>
        public bool MyAccountEnabled
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.MyAccountEnabled, true); }
        }

        /// <summary>
        /// Gets the valid client tags.
        /// </summary>
        public string[] ValidClientTags
        {
            get { return GetStringArrayConfigValue(Constants.ConfigurationItemKeys.ValidClientTags, new[] { "ABC123" }); }
        }

        /// <summary>
        /// Gets the SMTP server.
        /// </summary>
        public string SmtpServer
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.SmtpServer, "NOTSET"); }
        }

        /// <summary>
        /// Gets the user name reg ex pattern.
        /// </summary>
        public string UserNameRegExPattern
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.UserNameRegExPattern, @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"); }
        }

        /// <summary>
        /// Gets the email address reg ex pattern.
        /// </summary>
        public string EmailAddressRegExPattern
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.EmailAddressRegExPattern, @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"); }
        }

        /// <summary>
        /// Gets the strong password reg ex.
        /// </summary>
        public string PasswordRegExPattern
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.PasswordRegExPattern, @"^(?=.*\d)(?=.*[a-zA-Z])\S{6,20}$"); }
        }

        /// <summary>
        /// Gets the URL reg ex pattern.
        /// </summary>
        public string UrlRegExPattern
        {
            get
            {
                return GetStringConfigItemValue(Constants.ConfigurationItemKeys.UrlRegExPattern,
                                IntegrationClient == IntegrationClient.EKOS
                                    ? @"^(http|https)\://[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(:[a-zA-Z0-9]*)?(/[a-zA-Z0-9\-\._\?\,\'/\+&amp;%\$#\=~]*)?$"
                                    : @"^(http|https)\://[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(:[a-zA-Z0-9]*)?/?([a-zA-Z0-9\-\._\?\,\'/\\\+&amp;%\$#\=~])*[^\.\,\)\(\s]$");
            }
        }

        /// <summary>
        /// Gets the phone number reg ex pattern.
        /// </summary>
        public string PhoneNumberRegExPattern
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.PhoneNumberRegExPattern, @"^(([0-9]+|(\([0-9]+\)))((-| )(?!$))?)*$"); }
        }

        /// <summary>
        /// Gets the phone number reg ex pattern.
        /// </summary>
        public string PhoneNumberStrictRegExPattern
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.PhoneNumberStrictRegExPattern, @"(\d{3})(\d{3})(\d{4})"); }
        }

        public string PhoneNumberFormat
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.PhoneNumberFormat, "($1) $2-$3"); }
        }

        /// <summary>
        /// Gets the FEIN reg ex pattern.
        /// </summary>
        public string FEINRegExPattern
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.FEINRegExPattern, @"^[0-9]{2}-[0-9]{7}$"); }
        }

        /// <summary>
        /// Gets the SEIN reg ex pattern.
        /// </summary>
        public string SEINRegExPattern
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.SEINRegExPattern, @"^\d{8}-\d{2}$"); }
        }

        /// <summary>
        /// Gets the SEIN mask pattern.
        /// </summary>
        public string SEINMaskPattern
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.SEINMaskPattern, "99999999-99"); }
        }

        /// <summary>
        /// Gets the phone number mask.
        /// </summary>
        public string PhoneNumberMaskPattern
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.PhoneNumberMaskPattern, "(999) 999-9999"); }
        }

        /// <summary>
        /// Gets the non us phone number mask pattern.
        /// </summary>
        public string NonUsPhoneNumberMaskPattern
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.NonUsPhoneNumberMaskPattern, "99999999999999999999"); }
        }

        /// <summary>
        /// Regular expression for checking combining diacritical characters
        /// </summary>
        public string DiacriticalMarkCheck
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.DiacriticalMarkCheck, @"[\u0300-\u036F]{3,}"); }
        }

        /// <summary>
        /// Gets a value indicating whether to allow a talent registration to update company information.
        /// </summary>
        public bool AllowTalentCompanyInformationUpdate
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.AllowTalentCompanyInformationUpdate, false); }
        }

        public bool TalentModulePresent
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.TalentModulePresent, true); }
        }

        /// <summary>
        /// This determines the maximum number of pages supported by the page number drop-down on pager controls before switching to a text box
        /// </summary>
        public int PagerDropdownLimit
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.PagerDropdownLimit, 9999); }
        }

        /// <summary>
        /// Gets the resume upload allowed file reg ex pattern.
        /// </summary>
        public string ResumeUploadAllowedFileRegExPattern
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.ResumeUploadAllowedFileRegExPattern, @"(.*)(\.)(([d|D][o|O][c|C])|([p|P][d|D][f|F])|([t|T][x|X][t|T])|([o|O][d|D][f|F])|([r|R][t|T][f|F])|([o|O][d|D][t|T])|([d|D][o|O][c|C][x|X])|([c|C][l|L][n|N])|([h|H][t|T][m|M])|([h|H][t|T][m|M][l|L]))$"); }
        }

        /// <summary>
        /// Gets the size of the resume upload maximum allowed file in MB.
        /// </summary>
        public int ResumeUploadMaximumAllowedFileSize
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.ResumeUploadMaximumAllowedFileSize, 5); }
        }

        /// <summary>
        /// Gets the talent pool search maximum records to return.
        /// </summary>
        public int TalentPoolSearchMaximumRecordsToReturn
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.TalentPoolSearchMaximumRecordsToReturn, 100); }
        }

        /// <summary>
        /// Gets the star rating minimum scores.
        /// Array item 0 equals 0 star mimimum score
        /// Array item 1 equals 1 star mimimum score
        /// Array item 2 equals 2 star mimimum score
        /// Array item 3 equals 3 star mimimum score
        /// Array item 4 equals 4 star mimimum score
        /// Array item 5 equals 5 star mimimum score
        /// </summary>
        public int[] StarRatingMinimumScores
        {
            get { return GetIntArrayConfigItemValue(Constants.ConfigurationItemKeys.StarRatingMinimumScores, new[] { 0, 200, 400, 550, 700, 850 }); }
        }

        /// <summary>
        /// Gets the distance units to use.
        /// </summary>
        public DistanceUnits DistanceUnits
        {
            get
            {
                var value = GetStringConfigItemValue(Constants.ConfigurationItemKeys.DistanceUnits, DistanceUnits.Miles.ToString());
                DistanceUnits distanceUnits;

                Enum.TryParse(value, true, out distanceUnits);
                return distanceUnits;
            }
        }

        /// <summary>
        /// Gets the registration route.
        /// </summary>
        public RegistrationRoute RegistrationRoute
        {
            get
            {
                var value = GetStringConfigItemValue(Constants.ConfigurationItemKeys.RegistrationRoute, RegistrationRoute.Default.ToString());
                RegistrationRoute registrationRoute;
                Enum.TryParse(value, true, out registrationRoute);
                return registrationRoute;
            }
        }

        /// <summary>
        /// Gets the registration complete action.
        /// </summary>
        public RegistrationCompleteAction RegistrationCompleteAction
        {
            get
            {
                var value = GetStringConfigItemValue(Constants.ConfigurationItemKeys.RegistrationCompleteAction, RegistrationCompleteAction.EmailLink.ToString());
                RegistrationCompleteAction registrationCompleteAction;
                Enum.TryParse(value, true, out registrationCompleteAction);
                return registrationCompleteAction;
            }
        }

        /// <summary>
        /// Gets the default job expiry.
        /// </summary>
        public int DefaultJobExpiry
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.DefaultJobExpiry, 14); }
        }

        /// <summary>
        /// Gets the similar jobs count.
        /// </summary>
        public int SimilarJobsCount
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.SimilarJobsCount, 20); }
        }

        /// <summary>
        /// Gets the session timeout in minutes.
        /// </summary>
        public int SessionTimeout
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.SessionTimeout, (24 * 60)); }
        }

        /// <summary>
        /// Gets the session timeout in minutes.
        /// </summary>
        public int UserSessionTimeout
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.UserSessionTimeout, 60); }
        }

        /// <summary>
        /// Whether to display the server name on each page
        /// </summary>
        public bool DisplayServerName
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.DisplayServerName, false); }
        }

        /// <summary>
        /// Whether to use the SQL session state handler
        /// </summary>
        public bool UseSqlSessionState
        {
            get { return GetBoolConfigItemValue("UseSqlSessionState", false); }
        }

        /// <summary>
        /// Gets the employee search maximum records to return.
        /// </summary>
        public int EmployeeSearchMaximumRecordsToReturn
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.EmployeeSearchMaximumRecordsToReturn, 100); }
        }

        /// <summary>
        /// Gets the job expiry countdown days.
        /// </summary>
        public int[] JobExpiryCountdownDays
        {
            get { return GetIntArrayConfigItemValue(Constants.ConfigurationItemKeys.JobExpiryCountdownDays, new[] { 1, 8, 15 }); }
        }

        /// <summary>
        /// Gets the talent application path.
        /// </summary>
        public string TalentApplicationPath
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.TalentApplicationPath, @"http://localhost:11927"); }
        }

        /// <summary>
        /// Gets the assist application path.
        /// </summary>
        public string AssistApplicationPath
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.AssistApplicationPath, @"http://localhost:11927"); }
        }

        /// <summary>
        /// Gets the career application path.
        /// </summary>
        public string CareerApplicationPath
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.CareerApplicationPath, @"http://localhost:57990"); }
        }

        /// <summary>
        /// Gets a value indicating whether [show burning glass logo in talent].
        /// </summary>
        public bool ShowBurningGlassLogoInTalent
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ShowBurningGlassLogoInTalent, true); }
        }

        /// <summary>
        /// Gets a value indicating whether [search by industries].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [search by industries]; otherwise, <c>false</c>.
        /// </value>
        public bool SearchByIndustries
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.SearchByIndustries, false); }
        }

        /// <summary>
        /// Gets a value indicating whether [show burning glass logo in assist].
        /// </summary>
        public bool ShowBurningGlassLogoInAssist
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ShowBurningGlassLogoInAssist, true); }
        }

        /// <summary>
        /// Gets a value indicating whether [show burning glass logo in career/explorer].
        /// </summary>
        public bool ShowBurningGlassLogoInCareerExplorer
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ShowBurningGlassLogoInCareerExplorer, true); }
        }

        /// <summary>
        /// Gets a value indicating whether [display survey frequency].
        /// </summary>
        public bool DisplaySurveyFrequency
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.DisplaySurveyFrequency, false); }
        }

        /// <summary>
        /// Gets the survey frequency.
        /// </summary>
        public int SurveyFrequency
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.SurveyFrequency, 30); }
        }

        /// <summary>
        /// How many hours password validation keys are valid for
        /// </summary>
        public int PasswordValidationKeyExpiryHours
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.PasswordValidationKeyExpiryHours, 24); }
        }

        /// <summary>
        /// Gets a value indicating whether new employer requests require approval
        /// </summary>
        public TalentApprovalOptions NewEmployerApproval
        {
            get { return GetEnumConfigItemValue(Constants.ConfigurationItemKeys.NewEmployerApproval, TalentApprovalOptions.ApprovalSystemAvailable); }
        }

        /// <summary>
        /// Gets a value indicating whether new business unit requests require approval
        /// </summary>
        public TalentApprovalOptions NewBusinessUnitApproval
        {
            get { return GetEnumConfigItemValue(Constants.ConfigurationItemKeys.NewBusinessUnitApproval, TalentApprovalOptions.NoApproval); }
        }

        /// <summary>
        /// Gets a value indicating whether new hiring manager requests require approval
        /// </summary>
        public TalentApprovalOptions NewHiringManagerApproval
        {
            get { return GetEnumConfigItemValue(Constants.ConfigurationItemKeys.NewHiringManagerApproval, TalentApprovalOptions.NoApproval); }
        }

        /// <summary>
        /// Gets a value indicating whether [new employer censorship filtering].
        /// </summary>
        public bool NewEmployerCensorshipFiltering
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.NewEmployerCensorshipFiltering, true); }
        }

        /// <summary>
        /// Gets a value indicating whether [all job postings require approval].
        /// </summary>
        public bool AllJobPostingsRequireApproval
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.AllJobPostingsRequireApproval, false); }
        }

        /// <summary>
        /// Gets a value indicating whether [activation yellow word filtering].
        /// </summary>
        public bool ActivationYellowWordFiltering
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ActivationYellowWordFiltering, true); }
        }

        /// <summary>
        /// Gets a value indicating whether [employer preferences overide system defaults].
        /// </summary>
        public bool EmployerPreferencesOverideSystemDefaults
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.EmployerPreferencesOverideSystemDefaults, true); }
        }

        /// <summary>
        /// Gets the feedback email address.
        /// </summary>
        public bool SendHiringFromTaxCreditProgramNotification
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.SendHiringFromTaxCreditProgramNotification, true); }
        }

        /// <summary>
        /// Gets the feedback email address.
        /// </summary>
        public string FeedbackEmailAddress
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.FeedbackEmailAddress, "focus-dev@burning-glass.com"); }
        }

        /// <summary>
        /// Gets the job posting stylesheet.
        /// </summary>
        public string JobPostingStylesheet
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.JobPostingStylesheet, "DefaultPosting.xslt"); }
        }

        /// <summary>
        /// Gets the full resume stylesheet.
        /// </summary>
        public string FullResumeStylesheet
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.FullResumeStylesheet, "DefaultFullResume.xslt"); }
        }

        /// <summary>
        /// Gets the partial resume stylesheet.
        /// </summary>
        public string PartialResumeStylesheet
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.PartialResumeStylesheet, "DefaultPartialResume.xslt"); }
        }

        /// <summary>
        /// Gets the dummy username format.
        /// </summary>
        public string DummyUsernameFormat
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.DummyUsernameFormat, "{0}"); }
        }

        /// <summary>
        /// Gets the dummy email format.
        /// </summary>
        public string DummyEmailFormat
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.DummyEmailFormat, "{0}@ci.bgt.com"); }
        }

        /// <summary>
        /// Gets a value indicating whether [authenticate assist user via client].
        /// </summary>
        public bool AuthenticateAssistUserViaClient
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.AuthenticateAssistUserViaClient, false); }
        }

        /// <summary>
        /// Gets a value indicating whether [client allows password reset].
        /// </summary>
        public bool ClientAllowsPasswordReset
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ClientAllowsPasswordReset, false); }
        }

        /// <summary>
        /// Gets the talent light colour.
        /// </summary>
        public string TalentLightColour
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.TalentLightColour, "cccccc"); }
        }

        /// <summary>
        /// Gets the talent dark colour.
        /// </summary>
        public string TalentDarkColour
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.TalentDarkColour, "ececec"); }
        }

        /// <summary>
        /// Gets the assist light colour.
        /// </summary>
        public string AssistLightColour
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.AssistLightColour, "cccccc"); }
        }

        /// <summary>
        /// Gets the assist dark colour.
        /// </summary>
        public string AssistDarkColour
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.AssistDarkColour, "ececec"); }
        }

        /// <summary>
        /// Gets the career explorer light colour.
        /// </summary>
        /// <value>
        /// The career explorer light colour.
        /// </value>
        public string CareerExplorerLightColour
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.CareerExplorerLightColour, "cbd4db"); }
        }

        /// <summary>
        /// Gets the career explorer dark colour.
        /// </summary>
        /// <value>
        /// The career explorer dark colour.
        /// </value>
        public string CareerExplorerDarkColour
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.CareerExplorerDarkColour, "ececec"); }
        }

        /// <summary>
        /// Gets a value indicating whether [show experience industries].
        /// </summary>
        /// <value>
        /// <c>true</c> if [show experience industries]; otherwise, <c>false</c>.
        /// </value>
        public bool ShowExpIndustries
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ShowExpIndustries, false); }
        }

        /// <summary>
        /// Gets a value indicating whether [show target industries].
        /// </summary>
        /// <value>
        /// <c>true</c> if [show target industries]; otherwise, <c>false</c>.
        /// </value>
        public bool ShowTargetIndustries
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ShowTargetIndustries, false); }
        }

        /// <summary>
        /// Gets the default state key.
        /// </summary>
        public string DefaultStateKey
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.DefaultStateKey, Defaults.ConfigurationItemDefaults.DefaultStateKey); }
        }

        /// <summary>
        /// Gets a value indicating whether [job seeker referrals enabled].
        /// </summary>
        public bool JobSeekerReferralsEnabled
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.JobSeekerReferralsEnabled, true); }
        }

        /// <summary>
        /// Gets the name of the focus talent application.
        /// </summary>
        public string FocusTalentApplicationName
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.FocusTalentApplicationName, "Focus/Talent"); }
        }

        /// <summary>
        /// Gets the name of the focus assist application.
        /// </summary>
        public string FocusAssistApplicationName
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.FocusAssistApplicationName, "Focus/Assist"); }
        }

        /// <summary>
        /// Gets the name of the focus career application.
        /// </summary>
        public string FocusCareerApplicationName
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.FocusCareerApplicationName, "Focus/Career"); }
        }

        /// <summary>
        /// Gets the name of the focus reporting application.
        /// </summary>
        public string FocusReportingApplicationName
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.FocusReportingApplicationName, "Focus/Reporting"); }
        }

        /// <summary>
        /// Gets the name of the focus explorer application.
        /// </summary>
        public string FocusExplorerApplicationName
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.FocusExplorerApplicationName, "Focus/Explorer"); }
        }

        /// <summary>
        /// Gets the maximum salary.
        /// </summary>
        public int MaximumSalary
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.MaximumSalary, 350000); }
        }

        /// <summary>
        /// Gets the minimum salary.
        /// </summary>
        public int MinimumSalary
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.MinimumSalary, 0); }
        }

        /// <summary>
        /// Whether to adjust the minimum/maximum salary range depending on their type
        /// </summary>
        public bool AdjustSalaryRangeByType
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.AdjustSalaryRangeByType, false); }
        }

        /// <summary>
        /// Gets the maximum saved searches.
        /// </summary>
        public int MaximumSavedSearches
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.MaximumSavedSearches, 20); }
        }

        /// <summary>
        /// Gets the maximum salary.
        /// </summary>
        public bool HideSalary
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.HideSalary, false); }
        }

        /// <summary>
        /// Sets whether all UI actions that affect logging in, changing passwords, managing the users account should be hidden in Assist
        /// </summary>
        public bool DisableAssistAuthentication
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.DisableAssistAuthentication, false); }
        }

        /// <summary>
        /// Sets whether all UI actions that affect logging in, changing passwords, managing the users account should be hidden in Talent
        /// </summary>
        public bool DisableTalentAuthentication
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.DisableTalentAuthentication, false); }
        }

        /// <summary>
        /// Sets whether the talent pool search is displayed
        /// </summary>
        public bool TalentPoolSearch
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.TalentPoolSearch, false); }
        }

        /// <summary>
        /// Sets whether the talent pool search is displayed for hiring managers
        /// </summary>
        public bool HiringManagerTalentPoolSearch
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.HiringManagerTalentPoolSearch, false); }
        }

        /// <summary>
        /// Sets whether the talent pool search is displayed for business units
        /// </summary>
        public bool BusinessUnitTalentPoolSearch
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.BusinessUnitTalentPoolSearch, false); }
        }

        /// <summary>
        /// Sets whether the screening preferences are shown in the job wizard
        /// </summary>
        public bool HideScreeningPreferences
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.HideScreeningPreferences, false); }
        }

        /// <summary>
        /// Sets whether the Interview Contact preferences are shown in the job wizard
        /// </summary>
        public bool HideInterviewContactPreferences
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.HideInterviewContactPreferences, false); }
        }

        /// <summary>
        /// Show How To Apply secion on postings
        /// </summary>
        public bool ShowPostingHowToApply
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ShowPostingHowToApply, false); }
        }

        /// <summary>
        /// Show the 'Meets minumum wage requirement' checkbox
        /// </summary>
        public bool MeetsMinimumWageRequirementCheckBox
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.MeetsMinimumWageRequirementCheckBox, (Theme == FocusThemes.Workforce)); }
        }

        /// <summary>
        /// Number of days in future to default the job closing date
        /// </summary>
        public int DaysForDefaultJobClosingDate
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.DaysForDefaultJobClosingDate, 30); }
        }

        /// <summary>
        /// Minumum number of days in future to which the job closing date can be set
        /// </summary>
        public int DaysForMinimumJobClosingDate
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.DaysForMinimumJobClosingDate, 0); }
        }

        /// <summary>
        /// Number of days in a job can be active in the system (based on original job date)
        /// </summary>
        public int DaysForJobPostingLifetime
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.DaysForJobPostingLifetime, (Theme == FocusThemes.Workforce) ? 90 : 365); }
        }

        /// <summary>
        /// Number of days a federal contractor job can be active in the system (based on original job date)
        /// </summary>
        public int DaysForFederalContractorLifetime
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.DaysForFederalContractorLifetime, DaysForJobPostingLifetime); }
        }

        /// <summary>
        /// Whether a federal contractor job should have a mandatory date
        /// </summary>
        public bool FederalContratorExpiryDateMandatory
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.FederalContratorExpiryDateMandatory, true); }
        }

        /// <summary>
        /// Requirements preference default
        /// </summary>
        public RequirementsPreference RequirementsPreferenceDefault
        {
            get { return GetEnumConfigItemValue(Constants.ConfigurationItemKeys.RequirementsPreferenceDefault, RequirementsPreference.AllowAll); }
        }

        /// <summary>
        /// Number of days a foreign labor H2A job can be active in the system (based on original job date)
        /// </summary>
        public int DaysForForeignLaborH2ALifetime
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.DaysForForeignLaborH2ALifetime, DaysForJobPostingLifetime); }
        }

        /// <summary>
        /// Number of days a foreign labor H2B job can be active in the system (based on original job date)
        /// </summary>
        public int DaysForForeignLaborH2BLifetime
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.DaysForForeignLaborH2BLifetime, DaysForJobPostingLifetime); }
        }

        /// <summary>
        /// Number of days a foreign labor H2B job can be active in the system (based on original job date)
        /// </summary>
        public int DaysForForeignLaborOtherLifetime
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.DaysForForeignLaborOtherLifetime, DaysForJobPostingLifetime); }
        }

        /// <summary>
        /// Hide the WOTC panel in the job wizard
        /// </summary>
        public bool HideWOTC
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.HideWOTC, false); }
        }

        /// <summary>
        /// Hide the minimum age in the job wizard
        /// </summary>
        public bool HideMinimumAge
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.HideMinimumAge, (Theme == FocusThemes.Education)); }
        }

        /// <summary>
        /// Gets a value indicating whether to hide the upload multiple jobs option.
        /// </summary>
        /// <value>
        /// <c>true</c> if hide upload multiple jobs option; otherwise, <c>false</c>.
        /// </value>
        public bool HideUploadMultipleJobs
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.HideUploadMultipleJobs, false); }
        }

        /// <summary>
        /// Hide job conditions panel in job wizard
        /// </summary>
        /// <value>
        /// Default is true for Education and false for Workforce theme
        /// </value>
        public bool HideJobConditions
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.HideJobConditions, (Theme == FocusThemes.Education)); }
        }

        /// <summary>
        /// Job wizard has minimum hours per week field
        /// </summary>
        public bool HasMinimumHoursPerWeek
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.HasMinimumHoursPerWeek, (Theme == FocusThemes.Workforce)); }
        }

        /// <summary>
        /// Determines whether to show the "Only job seekers with a match of x stars or above can apply" option
        /// </summary>
        public bool ScreeningPrefExclusiveToMinStarMatch
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ScreeningPrefExclusiveToMinStarMatch, false); }
        }

        public bool ScreeningPrefBelowStarMatchApproval
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ScreeningPrefBelowStarMatchApproval, true); }
        }

        /// <summary>
        /// Gets the application fabric lead hosts.
        /// </summary>
        /// <value>
        /// The application fabric lead hosts.(String is server name, int is port number
        /// </value>
        /// <remarks>Has to be a tuple rather than a class as used in framework which is an independent class</remarks>
        public List<Tuple<string, int>> AppFabricLeadHosts
        {
            get
            {
                // Each value should in the format: {"Item1":"Servername","Item2":23564}
                var appFabricLeadHosts = _configurationItems.Where(ci => ci.Key == Constants.ConfigurationItemKeys.AppFabricLeadHosts).Select(ci => ci.Value).ToList();
                var hosts = appFabricLeadHosts.Select(configurationItem => configurationItem.DeserializeJson<Tuple<string, int>>()).ToList();
                return hosts;
            }
        }

        /// <summary>
        /// Gets a value indicating whether [reporting enabled].
        /// </summary>
        /// <value><c>true</c> if [reporting enabled]; otherwise, <c>false</c>.</value>
        public bool ReportingEnabled
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ReportingEnabled, (Theme == FocusThemes.Workforce)); }
        }

        /// <summary>
        /// Gets the number of days any 'actions' (used in total fields) should be kept
        /// </summary>
        public int ReportActionDaysLimit
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.ReportActionDaysLimit, 120); }
        }

        /// <summary>
        /// Gets a value indicating whether [career enabled].
        /// </summary>
        public bool CareerEnabled
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.CareerEnabled, false); }
        }

        /// <summary>
        /// Gets whether there should be a link to Labor/Insight
        /// </summary>
        public bool LaborInsightAccess
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.LaborInsightAccess, true); }
        }

        /// <summary>
        /// Gets the labor insight link URL.
        /// </summary>
        public string LaborInsightLinkUrl
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.LaborInsightLinkUrl, "http://www.burning-glass.com/products/labor.html"); }
        }

        /// <summary>
        /// Gets the lens services.
        /// </summary>
        /// <value>The lens services.</value>
        public List<LensService> LensServices
        {
            get
            {
                var lensServicesConfigurationItems = _configurationItems.Where(ci => ci.Key == Constants.ConfigurationItemKeys.LensService).Select(ci => ci.Value).ToList();
                var services = lensServicesConfigurationItems.Select(configurationItem => (LensService)configurationItem.DeserializeJson(typeof(LensService))).ToList();

                // To cut down the size of the Json saved in the database and as the custom filters have never changed between instances of Focus suite, 
                // we are going to default the values in code if none are set in the database.

                foreach (var service in services.Where(service => ((service.ServiceType == LensServiceTypes.Posting) || (service.ServiceType == LensServiceTypes.JobAlert) || (service.ServiceType == LensServiceTypes.Resume)) && service.CustomFilters.IsNull()))
                {
                    switch (service.ServiceType)
                    {
                        case LensServiceTypes.Posting:
                        case LensServiceTypes.JobAlert:

                            #region Set posting default custom filters

                            service.CustomFilters = new CustomFilterList
                            {
                                new CustomFilter {Key = "Status", Tag = "cf001"},
                                new CustomFilter {Key = "OpenDate", Tag = "cf002"},
                                new CustomFilter {Key = "ClosingOn", Tag = "cf003"},
                                new CustomFilter {Key = "MinimumSalary", Tag = "cf004"},
                                new CustomFilter {Key = "MaximumSalary", Tag = "cf005"},
                                new CustomFilter {Key = "ExternalPostingId", Tag = "cf007"},
                                new CustomFilter {Key = "Occupation", Tag = "cf008"},
                                new CustomFilter {Key = "StateCode", Tag = "cf009"},
                                new CustomFilter {Key = "PostingId", Tag = "cf010"},
                                new CustomFilter {Key = "Origin", Tag = "cf012"},
                                new CustomFilter {Key = "Sic2", Tag = "cf013"},
                                new CustomFilter {Key = "Industry", Tag = "cf014"},
                                new CustomFilter {Key = "EducationLevel", Tag = "cf016"},
                                new CustomFilter {Key = "GreenJob", Tag = "cf022"},
                                new CustomFilter {Key = "CountyCode", Tag = "cf025"},
                                new CustomFilter {Key = "Internship", Tag = "cf026"},
                                new CustomFilter {Key = "ProgramsOfStudy", Tag = "cf027"},
                                new CustomFilter {Key = "AnnualPay", Tag = "cf028"},
                                new CustomFilter {Key = "Salary", Tag = "cf029"},
                                new CustomFilter {Key = "Msa", Tag = "cf030"},
                                new CustomFilter {Key = "ROnet", Tag = "cf031"},
                                new CustomFilter {Key = "UnpaidPosition", Tag = "cf033"},
                                new CustomFilter {Key = "InternshipCategories", Tag = "cf034"},
                                new CustomFilter {Key = "RequiredProgramOfStudyId", Tag = "cf036"},
                                new CustomFilter {Key = "HomeBased", Tag = "cf037"},
                                new CustomFilter {Key = "CommissionBased", Tag = "cf038"},
                                new CustomFilter {Key = "SalaryAndCommissionBased", Tag = "cf039"}
                            };

                            #endregion

                            break;

                        case LensServiceTypes.Resume:

                            #region Set resume default custom filters

                            service.CustomFilters = new CustomFilterList
                            {
                                new CustomFilter {Key = "Status", Tag = "cf001"},
                                new CustomFilter {Key = "RegisteredOn", Tag = "cf002"},
                                new CustomFilter {Key = "ModifiedOn", Tag = "cf003"},
                                new CustomFilter {Key = "DateOfBirth", Tag = "cf004"},
                                new CustomFilter {Key = "CitizenFlag", Tag = "cf005"},
                                new CustomFilter {Key = "VeteranStatus", Tag = "cf007"},
                                new CustomFilter {Key = "Languages", Tag = "cf008"},
                                new CustomFilter {Key = "DrivingLicenceClass", Tag = "cf009"},
                                new CustomFilter {Key = "DrivingLicenceEndorsement", Tag = "cf010"},
                                new CustomFilter {Key = "Certificates", Tag = "cf011"},
                                new CustomFilter {Key = "WorkOvertime", Tag = "cf012"},
                                new CustomFilter {Key = "ShiftType", Tag = "cf013"},
                                new CustomFilter {Key = "WorkType", Tag = "cf014"},
                                new CustomFilter {Key = "WorkWeek", Tag = "cf015"},
                                new CustomFilter {Key = "EducationLevel", Tag = "cf016"},
                                new CustomFilter {Key = "JobseekerId", Tag = "cf017"},
                                new CustomFilter {Key = "Relocation", Tag = "cf019"},
                                new CustomFilter {Key = "JobTypes", Tag = "cf020"},
                                new CustomFilter {Key = "GPA", Tag = "cf021"},
                                new CustomFilter {Key = "MonthsExperience", Tag = "cf022"},
                                new CustomFilter {Key = "EnrollmentStatus", Tag = "cf023"},
                                new CustomFilter {Key = "ProgramsOfStudy", Tag = "cf024"},
                                new CustomFilter {Key = "DegreeCompletionDate", Tag = "cf025"},
                                new CustomFilter {Key = "ResumeIndustries", Tag = "cf026"},
                                new CustomFilter {Key = "HomeBased", Tag = "cf027"},
                                new CustomFilter {Key = "NCRCLevel", Tag = "cf028"}
                            };

                            #endregion

                            break;
                    }
                }

                return services;
            }
        }

        /// <summary>
        /// Gets the support email.
        /// </summary>
        public string SupportEmail
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.SupportEmail, "xxx@clientname.com"); }
        }

        /// <summary>
        /// Gets the support phone.
        /// </summary>
        public string SupportPhone
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.SupportPhone, "+1 (999) 999 9999"); }
        }

        /// <summary>
        /// Gets the support hours of business.
        /// </summary>
        public string SupportHoursOfBusiness
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.SupportHoursOfBusiness, "8:00-4:30 EST."); }
        }

        /// <summary>
        /// Gets the support hours of business.
        /// </summary>
        public string SupportHelpTeam
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.SupportHelpTeam, "Support Help Team"); }
        }

        /// <summary>
        /// Number of days support can take to respond
        /// </summary>
        public int SupportDaysResponse
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.SupportDaysResponse, 2); }
        }

        /// <summary>
        /// Gets the irs online application hours.
        /// </summary>
        public string IrsOnlineApplicationHours
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.IrsOnlineApplicationHours, "7:00 to 10:00 am (EST), Monday-Friday"); }
        }

        /// <summary>
        /// Gets the irs online application URL.
        /// </summary>
        public string IrsOnlineApplicationUrl
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.IrsOnlineApplicationUrl, @"https://sa1.www4.irs.gov/modiein/individual/index.jsp"); }
        }

        /// <summary>
        /// Gets the postal code reg ex pattern.
        /// </summary>
        public string PostalCodeRegExPattern
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.PostalCodeRegExPattern, @"(\d{5})"); }
        }

        /// <summary>
        /// Gets the postal code reg ex pattern.
        /// </summary>
        public string ExtendedPostalCodeRegExPattern
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.PostalCodeRegExPattern, @"(\d{5}(\-\d{4})?)"); }
        }

        /// <summary>
        /// Gets the postal code mask pattern.
        /// </summary>
        public string PostalCodeMaskPattern
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.PostalCodeMaskPattern, "99999"); }
        }

        /// <summary>
        /// Gets the extended postal code mask pattern.
        /// </summary>
        public string ExtendedPostalCodeMaskPattern
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.PostalCodeMaskPattern, "99999?-9999"); }
        }

        /// <summary>
        /// Gets the explorer resume upload allowed file reg ex pattern.
        /// </summary>
        public string ExplorerResumeUploadAllowedFileRegExPattern
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.ExplorerResumeUploadAllowedFileRegExPattern, @"(.*)(\.)(([d|D][o|O][c|C])|([p|P][d|D][f|F])|([r|R][t|T][f|F])|([d|D][o|O][c|C][x|X]))$"); }
        }

        /// <summary>
        /// Gets the maximum size of the explorer resume upload allowed file in mega bytes.
        /// </summary>
        /// <value>The maximum size of the explorer resume upload allowed file in mega bytes.</value>
        public int ExplorerResumeUploadMaximumAllowedFileSize
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.ExplorerResumeUploadMaximumAllowedFileSize, 5); }
        }

        /// <summary>
        /// Gets the explorer application path.
        /// </summary>
        public string ExplorerApplicationPath
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.ExplorerApplicationPath, @"http://explorerdev.burning-glass.com"); }
        }

        /// <summary>
        /// Gets a value indicating whether [bookmark shown for not logged in user].
        /// </summary>
        public bool ExplorerShowBookmarkForNonLoggedInUser
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ExplorerShowBookmarkForNonLoggedInUser, true); }
        }

        /// <summary>
        /// Gets a value indicating whether [send email shown for not logged in user].
        /// </summary>
        /// <value><c>true</c> if [send email shown for not logged in user]; otherwise, <c>false</c>.</value>
        public bool ExplorerShowEmailForNonLoggedInUser
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ExplorerShowEmailForNonLoggedInUser, true); }
        }

        /// <summary>
        /// Gets a value indicating whether [resume upload shown for not logged in user].
        /// </summary>
        public bool ExplorerShowResumeUploadForNonLoggedInUser
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ExplorerShowResumeUploadForNonLoggedInUser, true); }
        }

        /// <summary>
        /// Gets the explorer section order.
        /// </summary>
        public string ExplorerSectionOrder
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.ExplorerSectionOrder, (Theme == FocusThemes.Workforce) ? "Explore|Research|Study|Experience" : "School|Research|Explore|Experience"); }
        }

        /// <summary>
        /// Gets the order of the In-Demand dropdown
        /// </summary>
        public string ExplorerInDemandDropdownOrder
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.ExplorerInDemandDropdownOrder, "Job|CareerArea|Employer|DegreeCertification|Skill|Internship|CareerMoves"); }
        }

        /// <summary>
        /// Gets the explorer send email maximum message length.
        /// </summary>
        public int ExplorerSendEmailMaximumMessageLength
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.ExplorerSendEmailMaximumMessageLength, 1000); }
        }

        /// <summary>
        /// Gets a value indicating whether [email from shown in send email].
        /// </summary>
        public bool ExplorerShowEmailFromInSendEmail
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ExplorerSendEmailShowEmailFrom, true); }
        }

        /// <summary>
        /// Gets the explorer send email from email.
        /// </summary>
        /// <value>The explorer send email from email.</value>
        public string ExplorerSendEmailFromEmail
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.ExplorerSendEmailFromEmail, String.Empty); }
        }

        /// <summary>
        /// Gets a value indicating whether [personalization by resume is allowed].
        /// </summary>
        public bool ExplorerAllowPersonalizationByResume
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ExplorerAllowPersonalizationByResume, true); }
        }

        /// <summary>
        /// Gets the percentage to compare salaries.
        /// </summary>
        /// <value>The explorer job salary nationwide display type.</value>
        public int ExplorerJobSalaryComparisonPercent
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.ExplorerJobSalaryComparisonPercent, 2); }
        }

        /// <summary>
        /// Gets the explorer job salary nationwide display type.
        /// </summary>
        public string ExplorerJobSalaryNationwideDisplayType
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.ExplorerJobSalaryNationwideDisplayType, "Average"); }
        }

        /// <summary>
        /// Gets the explorer registration notification emails.
        /// </summary>
        public string ExplorerRegistrationNotificationEmails
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.ExplorerRegistrationNotificationEmails, null); }
        }

        /// <summary>
        /// Gets the explorer degree client data tag.
        /// </summary>
        /// <value>The explorer degree client data tag.</value>
        public string ExplorerDegreeClientDataTag
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.ExplorerDegreeClientDataTag, String.Empty); }
        }

        /// <summary>
        /// Gets a value indicating whether [degree study places are shown].
        /// </summary>
        public bool ExplorerShowDegreeStudyPlaces
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ExplorerShowDegreeStudyPlaces, false); }
        }

        /// <summary>
        /// Gets a value indicating whether to show the email icon in Explorer modal dialogs
        /// </summary>
        public bool ExplorerModalEmailIcon
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ExplorerModalEmailIcon, Theme == FocusThemes.Workforce); }
        }

        /// <summary>
        /// Gets a value indicating whether to show the download icon in Explorer modal dialogs
        /// </summary>
        public bool ExplorerModalDownloadIcon
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ExplorerModalDownloadIcon, Theme == FocusThemes.Education); }
        }

        /// <summary>
        /// Explorer lightbox - Show 'Ready to make a move' in separate bubble
        /// </summary>
        public bool ExplorerReadyToMakeAMoveBubble
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ExplorerReadyToMakeAMoveBubble, (BrandIdentifier != "RightManagement")); }
        }

        /// <summary>
        /// Gets a value indicating whether [confidential employers enabled].
        /// </summary>
        public bool ConfidentialEmployersEnabled
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ConfidentialEmployersEnabled, true); }
        }

        /// <summary>
        /// Gets a value indicating jobs are confidential by default or not
        /// </summary>
        public bool ConfidentialEmployersDefault
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ConfidentialEmployersDefault, false); }
        }

        /// <summary>
        /// Gets the explorer google tracking id.
        /// </summary>
        public string ExplorerGoogleTrackingId
        {
            get
            {
                //return GetStringConfigItemValue(Constants.ConfigurationItemKeys.ExplorerGoogleTrackingId, "UA-12345678-1");
                return GetStringConfigItemValue(Constants.ConfigurationItemKeys.ExplorerGoogleTrackingId, String.Empty);
            }
        }

        /// <summary>
        /// Gets a value indicating whether [google event tracking is enabled].
        /// </summary>
        public bool ExplorerGoogleEventTrackingEnabled
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ExplorerGoogleEventTrackingEnabled, false); }
        }

        /// <summary>
        /// Gets the explorer help email
        /// </summary>
        public string ExplorerHelpEmail
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.ExplorerHelpEmail, String.Empty); }
        }

        /// <summary>
        /// Gets the explorer location search radius.
        /// </summary>
        public int ExplorerLocationSearchRadius
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.ExplorerLocationSearchRadius, 50); }
        }

        /// <summary>
        /// Gets the explorer location search unit.
        /// </summary>
        public int ExplorerLocationSearchUnit
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.ExplorerLocationSearchUnit, 0); }
        }

        // Career

        public bool DisplayCareerAccountType
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.DisplayCareerAccountType, false); }
        }

        /// <summary>
        /// Gets the maximum allowed resume count.
        /// </summary>
        public int MaximumAllowedResumeCount
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.MaximumAllowedResumeCount, Defaults.ConfigurationItemDefaults.MaximumAllowedResumeCount); }
        }

        /// <summary>
        /// Gets the default country key.
        /// </summary>
        public string DefaultCountryKey
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.DefaultCountryKey, Defaults.ConfigurationItemDefaults.DefaultCountryKey); }
        }

        /// <summary>
        /// Gets the passive authentication code.
        /// </summary>
        public string PassiveAuthenticationCode
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.PassiveAuthenticationCode, string.Empty); }
        }

        /// <summary>
        /// Gets the masking address.
        /// </summary>
        public string MaskingAddress
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.MaskingAddress, "focuscareer@aperture-control.com"); }
        }

        /// <summary>
        /// Gets the minimum score for clarify.
        /// </summary>
        public int MinimumScoreForClarify
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.MinimumScoreForClarify, Defaults.ConfigurationItemDefaults.MinimumScoreForClarify); }
        }

        /// <summary>
        /// Gets the maximum search count for clarify.
        /// </summary>
        public int MaximumSearchCountForClarify
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.MaximumSearchCountForClarify, Defaults.ConfigurationItemDefaults.MaximumSearchCountForClarify); }
        }

        /// <summary>
        /// Gets the minimum score for search.
        /// </summary>
        public int MinimumScoreForSearch
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.MinimumScoreForSearch, Defaults.ConfigurationItemDefaults.MinimumSearchScore); }
        }

        /// <summary>
        /// Gets the maximum no document to return in search.
        /// </summary>
        public int MaximumNoDocumentToReturnInSearch
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.MaximumNoDocumentToReturnInSearch, Defaults.ConfigurationItemDefaults.MaxiumSearchDocumentCount); }
        }

        /// <summary>
        /// Gets the maximum no document to return in job alert.
        /// </summary>
        public int MaximumNoDocumentToReturnInJobAlert
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.MaximumNoDocumentToReturnInJobAlert, Defaults.ConfigurationItemDefaults.MaximumNoDocumentToReturnInJobAlert); }
        }

        /// <summary>
        /// Gets the spam job origin id.
        /// </summary>
        public string SpamJobOriginId
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.SpamJobOriginId, null); }
        }

        /// <summary>
        /// Gets the spam mail address.
        /// </summary>
        public string SpamMailAddress
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.SpamMailAddress, null); }
        }

        /// <summary>
        /// Gets the spam BCC address.
        /// </summary>
        public string SpamBccAddress
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.SpamBccAddress, null); }
        }

        /// <summary>
        /// Gets the closed job origin id.
        /// </summary>
        public string ClosedJobOriginId
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.ClosedJobOriginId, null); }
        }

        /// <summary>
        /// Gets the closed mail address.
        /// </summary>
        public string ClosedMailAddress
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.ClosedMailAddress, null); }
        }

        /// <summary>
        /// Gets the closed BCC address.
        /// </summary>
        public string ClosedBccAddress
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.ClosedBccAddress, null); }
        }

        /// <summary>
        /// Gets the cache size for prefix domain.
        /// </summary>
        public int CacheSizeForPrefixDomain
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.CacheSizeForPrefixDomain, Defaults.ConfigurationItemDefaults.CacheSizeForPrefixDomain); }
        }

        /// <summary>
        /// Gets the name of the brand.
        /// </summary>
        public string BrandName
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.BrandName, null); }
        }

        /// <summary>
        /// Gets the unique client branding identifier.
        /// </summary>
        public string BrandIdentifier
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.BrandIdentifier, "Default"); }
        }

        /// <summary>
        /// Gets the reset password format.
        /// </summary>
        public string ResetPasswordFormat
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.ResetPasswordFormat, "BGT_HtmlForgotPassword.xsl"); }
        }

        /// <summary>
        /// Gets the customer.
        /// </summary>
        public string Customer
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.Customer, Defaults.Customer); }
        }

        /// <summary>
        /// Gets the customer center code.
        /// </summary>
        public string CustomerCenterCode
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.CustomerCenterCode, Defaults.CustomerCenterCode); }
        }

        /// <summary>
        /// Gets the customer representative id.
        /// </summary>
        public string CustomerRepresentativeId
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.CustomerRepresentativeId, Defaults.CustomerRepresentativeId); }
        }

        /// <summary>
        /// Gets the nearby state keys.
        /// </summary>
        public string[] NearbyStateKeys
        {
            get { return GetStringArrayConfigValue(Constants.ConfigurationItemKeys.NearbyStateKeys, new[] { "State.IL", "State.IN", "State.KY", "State.MO", "State.OH", "State.TN", "State.VA", "State.WV" }); }
        }

        /// <summary>
        /// Gets the searchable state keys.
        /// </summary>
        /// <value>The searchable state keys.</value>
        public string[] SearchableStateKeys
        {
            get { return GetStringArrayConfigValue(Constants.ConfigurationItemKeys.SearchableStateKeys, GetDefaultSearchableStates()); }
        }

        /// <summary>
        /// Gets the special state keys (States that may be oversees territories)
        /// </summary>
        public string[] SpecialStateKeys
        {
            get { return GetStringArrayConfigValue(Constants.ConfigurationItemKeys.SpecialStateKeys, new[] { "State.AE", "State.AA", "State.AP", "State.AS", "State.FM", "State.MH", "State.MP", "State.ZZ", "State.PW", "State.GU", "State.PR", "State.VI" }); }
        }

        /// <summary>
        /// Gets a value indicating whether [job requirements].
        /// </summary>
        public bool JobRequirements
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.JobRequirements, true); }
        }

        /// <summary>
        /// Gets the referral min stars.
        /// </summary>
        public int ReferralMinStars
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.ReferralMinStars, 0); }
        }

        /// <summary>
        /// Gets the career explorer feature emphasis.
        /// </summary>
        public CareerExplorerFeatureEmphasis CareerExplorerFeatureEmphasis
        {
            get { return (CareerExplorerFeatureEmphasis)GetIntConfigItemValue(Constants.ConfigurationItemKeys.CareerExplorerFeatureEmphasis, (Theme == FocusThemes.Workforce) ? (int)CareerExplorerFeatureEmphasis.Career : (int)CareerExplorerFeatureEmphasis.Explorer); }
        }

        /// <summary>
        /// Gets the career registration notification emails.
        /// </summary>
        public string CareerRegistrationNotificationEmails
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.CareerRegistrationNotificationEmails, null); }
        }

        public bool ShowProgramArea
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ShowProgramArea, true); }
        }

        /// <summary>
        /// Whether to show the campus location selector to students (EDU only)
        /// </summary>
        public bool ShowCampus
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ShowCampus, false); }
        }

        /// <summary>
        /// Whether the client's Edu Focus instance supports prospective students
        /// </summary>
        public bool SupportsProspectiveStudents
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.SupportsProspectiveStudents, true); }
        }

        /// <summary>
        /// Allow job seeker contact details to be shown/hidden
        /// </summary>
        public ShowContactDetails ShowContactDetails
        {
            get { return GetEnumConfigItemValue(Constants.ConfigurationItemKeys.ShowContactDetails, ShowContactDetails.None); }
        }

        /// <summary>
        /// Hide the profile tab in the resume builder
        /// </summary>
        public bool HideResumeProfile
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.HideResumeProfile, false); }
        }

        /// <summary>
        /// Hide the objectives add-in in the resume builder
        /// </summary>
        public bool HideObjectives
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.HideObjectives, false); }
        }

        /// <summary>
        /// Minimum age for military service
        /// </summary>
        public int MilitaryServiceMinimumAge
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.MilitaryServiceMinimumAge, 17); }
        }

        /// <summary>
        /// Vietnam war start date
        /// </summary>
        public string VietmanWarStartDate
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.VietmanWarStartDate, "05 August 1964"); }
        }

        /// <summary>
        /// Vietnam war end date
        /// </summary>
        public string VietmanWarEndDate
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.VietmanWarEndDate, "07 May 1975"); }
        }

        public bool UseResumeTemplate
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.UseResumeTemplate, false); }
        }

        /// <summary>
        /// Resume Guided Path (forces job seekers to enter a resume before being enable to do anything else)
        /// </summary>
        public bool ResumeGuidedPath
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ResumeGuidedPath, false); }
        }

        /// <summary>
        /// Gets the country enabled flag.
        /// </summary>
        public bool CountyEnabled
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.CountyEnabled, Theme == FocusThemes.Workforce); }
        }

        /// <summary>
        /// Gets the logout redirect Url.
        /// </summary>
        public string LogoutRedirectUrl
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.LogoutRedirectUrl, null); }
        }

        /// <summary>
        /// Gets a value indicating whether [filter skills in automated summary].
        /// </summary>
        public bool FilterSkillsInAutomatedSummary
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.FilterSkillsInAutomatedSummary, true); }
        }

        /// <summary>
        /// Gets a value indicating whether [remove spaces from automated summary].
        /// </summary>
        public bool RemoveSpacesFromAutomatedSummary
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.RemoveSpacesFromAutomatedSummary, false); }
        }

        /// <summary>
        /// Gets the non customer system origin ids.
        /// </summary>
        public int[] NonCustomerSystemOriginIds
        {
            get { return GetIntArrayConfigItemValue(Constants.ConfigurationItemKeys.NonCustomerSystemOriginIds, new[] { 999 }); }
        }

        /// <summary>
        /// Gets the review application eligibility origin ids.
        /// </summary>
        public long[] ReviewApplicationEligibilityOriginIds
        {
            get { return GetNumericArrayConfigItemValue(Constants.ConfigurationItemKeys.ReviewApplicationEligibilityOriginIds, new long[] { 7, 8, 9 }); }
        }

        /// <summary>
        /// Gets the default type of the driving license.
        /// </summary>
        public string DefaultDrivingLicenseType
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.DefaultDrivingLicenseType, "DrivingLicenceClasses.ClassD"); }
        }

        /// <summary>
        /// Gets the labor regulations URL.
        /// </summary>
        public string LaborRegulationsUrl
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.LaborRegulationsUrl, null); }
        }

        /// <summary>
        /// Gets the name of the school.
        /// </summary>
        public string SchoolName
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.SchoolName, "Demo School"); }
        }

        /// <summary>
        /// Gets the type of the school.
        /// </summary>
        public SchoolTypes SchoolType
        {
            get { return GetEnumConfigItemValue(Constants.ConfigurationItemKeys.SchoolType, (Theme == FocusThemes.Workforce) ? SchoolTypes.None : SchoolTypes.TwoYear); }
        }

        /// <summary>
        /// Gets the document store service URL.
        /// </summary>
        public string DocumentStoreServiceUrl
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.DocumentStoreServiceUrl, "http://careerdocstoresvcstage.usdev.burninglass.com/DocStoreSvc.asmx"); }
        }

        /// <summary>
        /// Gets a value indicating whether [display resume builder duration].
        /// </summary>
        public bool DisplayResumeBuilderDuration
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.DisplayResumeBuilderDuration, false); }
        }

        /// <summary>
        /// Gets the message bus default user's username.
        /// </summary>
        public string MessageBusUsername
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.MessageBusUsername, "IntegrationUser"); }
        }

        /// <summary>
        /// Gets the batch process settings.
        /// </summary>
        public List<BatchProcessSetting> BatchProcessSettings
        {
            get
            {
                var json = GetStringConfigItemValue(Constants.ConfigurationItemKeys.BatchProcessSettings, string.Empty);
                return (json.IsNotNullOrEmpty()) ? (List<BatchProcessSetting>)json.DeserializeJson(typeof(List<BatchProcessSetting>)) : new List<BatchProcessSetting>();
            }
        }

        public bool EnableTalentPasswordSecurity
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.EnableTalentPasswordSecurity, false); }
        }

        /// <summary>
        /// Whether to split jobs that are set up with multiple locations
        /// </summary>
        public bool SplitJobsByLocation
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.SplitJobsByLocation, false); }
        }

        /// <summary>
        /// Gets a value indicating whether [office external id mandatory].
        /// </summary>
        public bool OfficeExternalIdMandatory
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.OfficeExternalIdMandatory, true); }
        }

        /// <summary>
        /// Gets a value indicating whether [job seeker giving positive feedback].
        /// </summary>
        /// <value>
        /// <c>true</c> if [job seeker giving positive feedback]; otherwise, <c>false</c>.
        /// </value>
        public bool PositiveFeedbackCheckEnabled
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.PositiveFeedbackCheckEnabled, true); }
        }

        /// <summary>
        /// Gets a value indicating whether [career hide search jobs by resume].
        /// </summary>
        public bool CareerHideSearchJobsByResume
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.CareerHideSearchJobsByResume, false); }
        }

        /// <summary>
        /// Allow anonymous access to certain functions in Career/Explorer for recognised domains
        /// </summary>
        public AnonymousAccess IsCareerExplorerAnonymousOnly
        {
            get { return GetEnumConfigItemValue(Constants.ConfigurationItemKeys.IsCareerExplorerAnonymousOnly, AnonymousAccess.None); }
        }

        /// <summary>
        /// Recognised domains that allow anoymous access to Career/Explorer
        /// </summary>
        public string[] IsCareerExplorerAnonymousOnlyDomainNames
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.IsCareerExplorerAnonymousOnlyDomainNames, "").Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries); }
        }

        /// <summary>
        /// Adds extra questions to the Career reset password function
        /// </summary>
        public bool EnableCareerPasswordSecurity
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.EnableCareerPasswordSecurity, true); }
        }

        ///<summary>
        ///Displays the seeker resume matched Talent jobs first 
        /// </summary>
        public bool ShowTalentJobsAsHighPriority
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ShowTalentJobsAsHighPriority, false); }
        }

        /// <summary>
        /// Whether to set the external id when creating a spidered posting from the document store
        /// </summary>
        public bool SetExternalIdOnSpideredPostings
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.SetExternalIdOnSpideredPostings, IntegrationClient != IntegrationClient.EKOS); }
        }

        /// <summary>
        /// Allow jobseekers without a resume to self refer for Spidered Postings
        /// </summary>
        public bool AllowSelfReferralOnSpideredPostingsWithNoResume
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.AllowSelfReferralOnSpideredPostingsWithNoResume, false); }
        }

        /// <summary>
        /// Gets the driving licence endorsement rules.
        /// </summary>
        /// <value>The driving licence endorsement rules.</value>
        public DrivingLicenceEndorsementRuleList DrivingLicenceEndorsementRules
        {
            get
            {
                var json = GetStringConfigItemValue(Constants.ConfigurationItemKeys.DrivingLicenceEndorsementRules, string.Empty);
                return (json.IsNotNullOrEmpty()) ? json.DeserializeJson<DrivingLicenceEndorsementRuleList>() : Defaults.ConfigurationItemDefaults.DefaultDrivingLicenceEndorsementRules;
            }
        }

        /// <summary>
        /// Gets the focus career URL.
        /// </summary>
        public string ClientApplyToPostingUrl
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.ClientApplyToPostingUrl, string.Empty); }
        }

        /// <summary>
        /// Gets a value indicating whether 'Add another' button is displayed for searching on multiple SSN's
        /// </summary>
        public bool ShowAssistMultipleJSSearchButtons
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ShowAssistMultipleJSSearchButtons, false); }
        }

        /// <summary>
        /// Gets the client specific external jobseeker identifier regex.
        /// </summary>
        /// <value>
        /// The client specific external jobseeker identifier regex.
        /// </value>
        public string ClientSpecificExternalJobseekerIDRegex
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.ClientSpecificExternalJobseekerIDRegex, string.Empty); }
        }

        /// <summary>
        /// Gets the client specific external jobseeker identifier regex.
        /// </summary>
        /// <value>
        /// The client specific external jobseeker identifier regex.
        /// </value>
        public int MaxSearchableIDs
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.MaxSearchableIDs, 20); }
        }

        /// <summary>
        /// Maximum number of staff that can be assigned to an office
        /// </summary>
        public int MaxStaffForOffices
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.MaxStaffForOffices, 50); }
        }

        /// <summary>
        /// Whether to enabled the preferred employer functionality
        /// </summary>
        public bool EnablePreferredEmployers
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.EnablePreferredEmployers, false); }
        }

        /// <summary>
        /// Gets the number of hiring managers to display for each linked company on an employer profile.
        /// </summary>
        /// <value>
        /// The number of hiring managers to display for each linked company on an employer profile.
        /// </value>
        public int NumberOfHiringManagersOnEmployerProfile
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.NumberOfHiringManagersOnEmployerProfile, 3); }
        }

        /// <summary>
        /// Check external system when searching for job seekers
        /// </summary>
        public bool CheckExternalSystemOnSeekerSearch
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.CheckExternalSystemOnSeekerSearch, IntegrationClient.IsIn(IntegrationClient.EKOS, IntegrationClient.Mock)); }
        }

        /// <summary>
        /// Show StaffAssistedLMI action
        /// </summary>
        public bool ShowStaffAssistedLMI
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ShowStaffAssistedLMI, IntegrationClient == IntegrationClient.EKOS); }
        }

        /// <summary>
        /// Gets a value indicating whether [multiple jobseeker invites enabled].
        /// </summary>
        /// <value>
        /// <c>true</c> if [multiple jobseeker invites enabled]; otherwise, <c>false</c>.
        /// </value>
        public bool MultipleJobseekerInvitesEnabled
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.MultipleJobseekerInvitesEnabled, false); }
        }

        /// <summary>
        /// Gets a value indicating whether driving licence functionality is displayed
        /// </summary>
        public bool HideDrivingLicence
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.HideDrivingLicence, Theme == FocusThemes.Education); }
        }

        /// <summary>
        /// Gets a value indicating whether the custom message template option is displayed on the Talent Applicants tab
        /// </summary>
        public bool ShowCustomMessageTemplate
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ShowCustomMessageTemplate, Theme == FocusThemes.Education); }
        }

        /// <summary>
        /// Gets a value indicating whether [hide personal information].
        /// </summary>
        public bool HidePersonalInformation
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.HidePersonalInformation, false); }
        }

        /// <summary>
        /// Gets a value indicating whether [hide references].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [hide references]; otherwise, <c>false</c>.
        /// </value>
        public bool HideReferences
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.HideReferences, false); }
        }

        /// <summary>
        /// Gets a value indicating whether hide the enrollments status.
        /// </summary>
        public bool HideEnrollmentStatus
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.HideEnrollmentStatus, false); }
        }

        /// <summary>
        /// Gets a value indicating whether [show branding statement].
        /// </summary>
        public bool ShowBrandingStatement
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ShowBrandingStatement, false); }
        }

        /// <summary>
        /// Gets a value indicating whether [job title required].
        /// </summary>
        public bool JobTitleRequired
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.JobTitleRequired, false); }
        }

        /// <summary>
        /// Gets a value indicating whether [hide seasonal duration].
        /// </summary>
        public bool HideSeasonalDuration
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.HideSeasonalDuration, false); }
        }

        /// <summary>
        /// Gets a value indicating whether [show localised edu levels].
        /// </summary>
        public bool ShowLocalisedEduLevels
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ShowLocalisedEduLevels, false); }
        }

        /// <summary>
        /// </summary>
        public int AutoDenyJobseekerTimePeriod
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.AutoDenyJobseekerTimePeriod, 30); }
        }

        /// <summary>
        /// Gets a value indicating whether [show work net link].
        /// </summary>
        public bool ShowWorkNetLink
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ShowWorkNetLink, false); }
        }

        /// <summary>
        /// Gets a value the work net link.
        /// </summary>
        public string WorkNetLink
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.WorkNetLink, string.Empty); }
        }

        /// <summary>
        /// System Default for sender email address in email templates
        /// </summary>
        public string SystemDefaultSenderEmailAddress
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.SystemDefaultSenderEmailAddress, null); }
        }

        /// <summary>
        /// Gets a value indicating whether [selective service registration].
        /// </summary>
        public bool SelectiveServiceRegistration
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.SelectiveServiceRegistration, false); }
        }

        /// <summary>
        /// Gets a value indicating how the AccountType control will be displayed
        /// </summary>
        public ControlDisplayType AccountTypeDisplayType
        {
            get { return (ControlDisplayType)Enum.Parse(typeof(ControlDisplayType), GetStringConfigItemValue(Constants.ConfigurationItemKeys.AccountTypeDisplayType, ControlDisplayType.Optional.ToString()), true); }
        }

        /// <summary>
        /// Gets the job description chars shown.
        /// </summary>
        public int JobDescriptionCharsShown
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.JobDescriptionCharsShown, 0); }
        }

        /// <summary>
        /// Gets a value indicating whether [selective service registration].
        /// </summary>
        public bool ShowEmployeeNumbers
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ShowEmployeeNumbers, false); }
        }

        /// <summary>
        /// Gets a value indicating whether [pre screening service request].
        /// </summary>
        public bool PreScreeningServiceRequest
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.PreScreeningServiceRequest, false); }
        }

        /// <summary>
        /// Whether the job upload functionality
        /// </summary>
        public bool HasJobUpload
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.HasJobUpload, Theme == FocusThemes.Workforce); }
        }

        /// <summary>
        /// Whether to display the job activity log in Talent Job View
        /// </summary>
        public bool DisplayJobActivityLogInTalent
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.DisplayJobActivityLogInTalent, false); }
        }

        /// <summary>
        /// Gets a value indicating whether [hide years of exp in search results].
        /// </summary>
        public bool HideYearsOfExpInSearchResults
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.HideYearsOfExpInSearchResults, false); }
        }

        /// <summary>
        /// Gets a value indicating whether [show years of experience in posting results].
        /// </summary>
        public bool ShowYearsOfExperienceInPostingResults
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ShowYearsOfExperienceInPostingResults, false); }
        }

        /// <summary>
        /// Gets a value indicating whether [show home location in search results].
        /// </summary>
        public bool ShowHomeLocationInSearchResults
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ShowHomeLocationInSearchResults, false); }
        }

        /// <summary>
        /// Gets a value indicating whether [show personal email].
        /// </summary>
        public bool ShowPersonalEmail
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ShowPersonalEmail, false); }
        }

        /// <summary>
        /// Gets a value indicating whether [enable default office per record type].
        /// </summary>
        public bool EnableDefaultOfficePerRecordType
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.EnableDefaultOfficePerRecordType, false); }
        }

        /// <summary>
        /// Gets a value indicating whether [extended veteran priorityof service].
        /// </summary>
        public bool ExtendedVeteranPriorityofService
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ExtendedVeteranPriorityofService, false); }
            set { }
        }

        /// <summary>
        /// Gets a value indicating whether to show work location full address.
        /// </summary>
        /// <value>
        /// <c>true</c> if show work location full address; otherwise, <c>false</c>.
        /// </value>
        public bool ShowWorkLocationFullAddress
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ShowWorkLocationFullAddress, false); }
        }

        /// <summary>
        /// Gets the under age job seeker restriction threshold. A value of zero indicates there are no restrictions otherwise restrictions should apply to job seekers under the specified value.
        /// </summary>
        public int UnderAgeJobSeekerRestrictionThreshold
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.UnderAgeJobSeekerRestrictionThreshold, 0); }
        }

        /// <summary>
        /// Gets the minimum career age threshold.
        /// </summary>
        /// <value>
        /// The minimum career age threshold.
        /// </value>
        public int MinimumCareerAgeThreshold
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.MinimumCareerAgeThreshold, 14); }
        }

        /// <summary>
        /// Gets a value indicating whether [use office address to send messages].
        /// </summary>
        /// <value>
        /// <c>true</c> if [use office address to send messages]; otherwise, <c>false</c>.
        /// </value>
        public bool UseOfficeAddressToSendMessages
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.UseOfficeAddressToSendMessages, false); }
            set { }
        }

        /// <summary>
        /// Gets or sets a value indicating whether [show asset access].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [show asset access]; otherwise, <c>false</c>.
        /// </value>
        public bool ShowAssetAccess
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ShowAssetAccess, false); }
        }

        /// <summary>
        /// Gets a value indicating whether the site is currently under maintenance.
        /// </summary>
        /// <value>
        /// <c>true</c> if the site is currently under maintenance, else <c>false</c>.
        /// </value>
        public bool SiteUnderMaintenance
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.SiteUnderMaintenance, false); }
        }

        /// <summary>
        /// Gets the statistics archive in days.
        /// </summary>
        /// <value>
        /// The statistics archive in days.
        /// </value>
        public int StatisticsArchiveInDays
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.StatisticsArchiveInDays, 1); }
        }

        /// <summary>
        /// Gets the screening pref exclusive to min star match default.
        /// </summary>
        /// <value>
        /// The screening pref exclusive to min star match default.
        /// </value>
        public ScreeningPreferences ScreeningPrefExclusiveToMinStarMatchDefault
        {
            get { return GetEnumConfigItemValue(Constants.ConfigurationItemKeys.ScreeningPrefExclusiveToMinStarMatchDefault, ScreeningPreferences.JobSeekersMustHave3StarMatchToApply); }
        }

        /// <summary>
        /// Maximum number of hours for part-time jobs
        /// </summary>
        public int PartTimeEmploymentLimit
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.PartTimeEmploymentLimit, 0); }
        }

        /// <summary>
        /// Hours (Full-time/Part-Time) mandatory for jobs
        /// </summary>
        public bool MandatoryHours
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.MandatoryHours, false); }
        }

        /// <summary>
        /// Hours per week mandatory for jobs
        /// </summary>
        public bool MandatoryHoursPerWeek
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.MandatoryHoursPerWeek, false); }
        }

        /// <summary>
        /// Job Type mandatory for jobs
        /// </summary>
        public bool MandatoryJobType
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.MandatoryJobType, false); }
        }

        /// <summary>
        /// Gets a value indicating whether [display poor resume announcement].
        /// </summary>
        /// <value>
        /// <c>true</c> if [display poor resume announcement]; otherwise, <c>false</c>.
        /// </value>
        public bool DisplayPoorResumeAnnouncement
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.DisplayPoorResumeAnnouncement, false); }
        }

        /// <summary>
        /// Gets a value indicating whether [read only resume DOB].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [read only resume DOB]; otherwise, <c>false</c>.
        /// </value>
        public bool ReadOnlyResumeDOB
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ReadOnlyResumeDOB, false); }
        }

        /// <summary>
        /// Gets a value indicating whether [enable office default admin].
        /// </summary>
        /// <value>
        /// <c>true</c> if [enable office default admin]; otherwise, <c>false</c>.
        /// </value>
        public bool EnableOfficeDefaultAdmin
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.EnableOfficeDefaultAdmin, false); }
        }

        /// <summary>
        /// Show all offices on referral queue
        /// </summary>
        public bool ShowAllOfficesReferralRequestQueue
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ShowAllOfficesReferralRequestQueue, false); }
        }

        /// <summary>
        /// Gets a value indicating whether [hide moc codes].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [hide moc codes]; otherwise, <c>false</c>.
        /// </value>
        public bool HideMocCodes
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.HideMocCodes, false); }
        }

        /// <summary>
        /// Mandatory Job Seeker job alerts
        /// </summary>
        public bool MandatoryJSJobAlerts
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.MandatoryJSJobAlerts, false); }
        }

        /// <summary>
        /// Show military status panel in Talent Pool search
        /// </summary>
        public bool ShowMilitaryStatus
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ShowMilitaryStatus, (Theme == FocusThemes.Workforce)); }
        }

        /// <summary>
        /// Maximum preferred salary threshold
        /// </summary>
        public int MaxPreferredSalaryThreshold
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.MaxPreferredSalaryThreshold, 350000); }
        }

        /// <summary>
        /// Allow multiple military histories
        /// </summary>
        public bool AllowDateBoundMilitaryService
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.AllowDateBoundMilitaryService, false); }
        }

        /// <summary>
        /// Enable job search by Job Id
        /// </summary>
        public bool EnableJobSearchByJobId
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.EnableJobSearchByJobId, false); }
        }

        /// <summary>
        /// Enable optional alien registration expiry date
        /// </summary>
        public bool EnableOptionalAlienRegExpiryDate
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.EnableOptionalAlienRegExpiryDate, false); }
        }

        /// <summary>
        /// Show link for Bridges to Opportunities in Career Explorer
        /// </summary>
        public bool ShowBridgestoOppsInterest
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ShowBridgestoOppsInterest, false); }
            set { }
        }

        /// <summary>
        /// Get the activity id to log if Bridges to Opportunities is selected
        /// </summary>
        public long BridgestoOppsInterestActivityExternalId
        {
            get { return GetLongConfigItemValue(Constants.ConfigurationItemKeys.BridgestoOppsInterestActivityExternalId, 0); }
        }

        /// <summary>
        /// Display a permanent message on the CE dashboard about the SSN
        /// </summary>
        public bool ShowSSNStatusMessage
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ShowSSNStatusMessage, false); }
        }

        /// <summary>
        /// Maximum Grade Point Average allowed
        /// </summary>
        public int MaximumGradePointAverage
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.MaximumGradePointAverage, 4); }
        }

        /// <summary>
        /// Maximum number of military service periods in resume
        /// </summary>
        public int MaxDateBoundMilitaryServicePeriods
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.MaxDateBoundMilitaryServicePeriods, 0); }
        }

        /// <summary>
        /// Gets a value indicating whether [unsubscribe from JS messages].
        /// </summary>
        /// <value>
        /// <c>true</c> if [unsubscribe from JS messages]; otherwise, <c>false</c>.
        /// </value>
        public bool UnsubscribeFromJSMessages
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.UnsubscribeFromJSMessages, false); }
        }

        /// <summary>
        /// Gets the unsubscribe email footer.
        /// </summary>
        /// <value>
        /// The unsubscribe email footer.
        /// </value>
        public string UnsubscribeEmailFooter
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.UnsubscribeEmailFooter, "<br /><br />This email has been sent by #FOCUSASSIST#. To unsubscribe from future messages of this nature, please <a href=\"{0}\" target=\"_blank\">click here</a><br /><br />"); }
        }

        /// <summary>
        /// Gets a value indicating whether [unsubscribe from HM messages].
        /// </summary>
        /// <value>
        /// <c>true</c> if [unsubscribe from HM messages]; otherwise, <c>false</c>.
        /// </value>
        public bool UnsubscribeFromHMMessages
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.UnsubscribeFromHMMessages, false); }
        }

        /// <summary>
        /// Gets a value indicating whether [no fixed location].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [no fixed location]; otherwise, <c>false</c>.
        /// </value>
        public bool NoFixedLocation
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.NoFixedLocation, false); }
        }

        /// <summary>
        /// Gets the job distribution feeds.
        /// </summary>
        public string JobDistributionFeeds
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.JobDistributionFeeds, string.Empty); }
        }

        /// <summary>
        /// Gets the business terminology.
        /// </summary>
        public string BusinessTerminology
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.BusinessTerminology, "Business"); }
        }

        /// <summary>
        /// Gets the businesses terminology.
        /// </summary>
        public string BusinessesTerminology
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.BusinessesTerminology, "Businesses"); }
        }

        /// <summary>
        /// Whether to enable CDN in the Ajax Control Toolkit", Default = false, DisplayOrder = 73)]
        /// </summary>
        public bool EnableCDN
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.EnableCDN, true); }
        }

        /// <summary>
        /// Gets a value indicating whether [show talent activity report].
        /// </summary>
        /// <value>
        /// <c>true</c> if [show talent activity report]; otherwise, <c>false</c>.
        /// </value>
        public bool ShowTalentActivityReport
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ShowTalentActivityReport, false); }
        }

        /// <summary>
        /// Gets a value indicating whether Assist user staff contact information is read only
        /// </summary>
        /// <value>
        /// <c>true</c> if Assist user staff contact information is read only; otherwise, <c>false</c>.
        /// </value>
        public bool ReadOnlyContactInfo
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ReadOnlyContactInfo, false); }
        }

        /// <summary>
        /// Enable Case Management Error Handling
        /// </summary>
        public bool EnableAssistErrorHandling
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.EnableAssistErrorHandling, false); }
        }

        public bool HideJSCharacteristicsFromReporting
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.HideJSCharacteristicsFromReporting, false); }
        }

        public bool HideJSCharacteristicsFromMessages
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.HideJSCharacteristicsFromMessages, false); }
        }

        /// <summary>
        /// Gets the maximum number of job openings.
        /// </summary>
        /// <value>The maximum number of job openings.</value>
        public int MaximumNumberJobOpenings
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.MaximumNumberJobOpenings, 200); }
        }

        #endregion

        #region Issues

        #region Issues Enabled config

        /// <summary>
        /// Gets a value indicating whether user [not logging in enabled].
        /// </summary>
        /// <value>
        /// <c>true</c> if [not logging in enabled]; otherwise, <c>false</c>.
        /// </value>
        public bool NotLoggingInEnabled
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.NotLoggingInEnabled, true); }
        }

        /// <summary>
        /// Gets a value indicating whether user [not clicking leads enabled].
        /// </summary>
        /// <value>
        /// <c>true</c> if [not clicking leads enabled]; otherwise, <c>false</c>.
        /// </value>
        public bool NotClickingLeadsEnabled
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.NotClickingLeadsEnabled, true); }
        }

        /// <summary>
        /// Gets a value indicating whether user [rejecting job offers enabled].
        /// </summary>
        /// <value>
        /// <c>true</c> if [rejecting job offers enabled]; otherwise, <c>false</c>.
        /// </value>
        public bool RejectingJobOffersEnabled
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.RejectingJobOffersEnabled, true); }
        }

        /// <summary>
        /// Gets a value indicating whether user [not reporting for interview enabled].
        /// </summary>
        /// <value>
        /// <c>true</c> if [not reporting for interview enabled]; otherwise, <c>false</c>.
        /// </value>
        public bool NotReportingForInterviewEnabled
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.NotReportingForInterviewEnabled, true); }
        }

        /// <summary>
        /// Gets a value indicating whether user [not responding to employer invites enabled].
        /// </summary>
        /// <value>
        /// <c>true</c> if [not responding to employer invites enabled]; otherwise, <c>false</c>.
        /// </value>
        public bool NotRespondingToEmployerInvitesEnabled
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.NotRespondingToEmployerInvitesEnabled, true); }
        }

        /// <summary>
        /// Gets a value indicating whether user [not responding to referral suggestions enabled].
        /// </summary>
        /// <value>
        /// <c>true</c> if [not responding to referral suggestions enabled]; otherwise, <c>false</c>.
        /// </value>
        public bool NotRespondingToReferralSuggestionsEnabled
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.NotRespondingToReferralSuggestionsEnabled, true); }
        }

        /// <summary>
        /// Gets a value indicating whether user [self referring to unqualified jobs enabled].
        /// </summary>
        /// <value>
        /// <c>true</c> if [self referring to unqualified jobs enabled]; otherwise, <c>false</c>.
        /// </value>
        public bool SelfReferringToUnqualifiedJobsEnabled
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.SelfReferringToUnqualifiedJobsEnabled, true); }
        }

        /// <summary>
        /// Gets a value indicating whether user [showing low quality matches enabled].
        /// </summary>
        /// <value>
        /// <c>true</c> if [showing low quality matches enabled]; otherwise, <c>false</c>.
        /// </value>
        public bool ShowingLowQualityMatchesEnabled
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ShowingLowQualityMatchesEnabled, true); }
        }

        /// <summary>
        /// Gets a value indicating whether user [posting low quality resume enabled].
        /// </summary>
        /// <value>
        /// <c>true</c> if [posting low quality resume enabled]; otherwise, <c>false</c>.
        /// </value>
        public bool PostingLowQualityResumeEnabled
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.PostingLowQualityResumeEnabled, true); }
        }

        /// <summary>
        /// Gets a value indicating whether user [post hire follow up enabled].
        /// </summary>
        /// <value>
        /// <c>true</c> if [post hire follow up enabled]; otherwise, <c>false</c>.
        /// </value>
        public bool PostHireFollowUpEnabled
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.PostHireFollowUpEnabled, true); }
        }

        /// <summary>
        /// Gets a value indicating whether user [follow up enabled].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [follow up enabled]; otherwise, <c>false</c>.
        /// </value>
        public bool FollowUpEnabled
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.FollowUpEnabled, true); }
        }

        /// <summary>
        /// Gets a value indicating whether [not searching jobs enabled].
        /// </summary>
        /// <value>
        /// <c>true</c> if [not searching jobs enabled]; otherwise, <c>false</c>.
        /// </value>
        public bool NotSearchingJobsEnabled
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.NotSearchingJobsEnabled, true); }
        }

        /// <summary>
        /// Whether to check for inappropriate email addresses
        /// </summary>
        public bool InappropriateEmailAddressCheckEnabled
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.InappropriateEmailAddressCheckEnabled, false); }
        }

        /// <summary>
        /// Gets a value indicating whether [negative survey check enabled].
        /// </summary>
        public bool NegativeFeedbackCheckEnabled
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.NegativeFeedbackCheckEnabled, true); }
        }

        /// <summary>
        /// Flag potential MSFW
        /// </summary>
        public bool JobIssueFlagPotentialMSFW
        {
            get { return Theme == FocusThemes.Workforce && GetBoolConfigItemValue(Constants.ConfigurationItemKeys.JobIssueFlagPotentialMSFW, false); }
        }

        public bool ExpiredAlienCertificationEnabled
        {
            get { return Theme == FocusThemes.Workforce && GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ExpiredAlienCertificationEnabled, true); }
        }

        /// <summary>
        /// Enable potential MSFW questions
        /// </summary>
        public bool PotentialMSFWQuestionsEnabled
        {
            get { return Theme == FocusThemes.Workforce && GetBoolConfigItemValue(Constants.ConfigurationItemKeys.PotentialMSFWQuestionsEnabled, false); }
        }

        #endregion

        #region Issue thresholds

        /// <summary>
        /// Gets the not logging in days threshold.
        /// </summary>
        /// <value>
        /// The not logging in days threshold.
        /// </value>
        public int NotLoggingInDaysThreshold
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.NotLoggingInDaysThreshold, 2); }
        }

        /// <summary>
        /// Gets the not clicking leads count threshold.
        /// </summary>
        /// <value>
        /// The not clicking leads count threshold.
        /// </value>
        public int NotClickingLeadsCountThreshold
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.NotClickingLeadsCountThreshold, 5); }
        }

        /// <summary>
        /// Gets the not clicking leads days threshold.
        /// </summary>
        /// <value>
        /// The not clicking leads days threshold.
        /// </value>
        public int NotClickingLeadsDaysThreshold
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.NotClickingLeadsDaysThreshold, 14); }
        }

        /// <summary>
        /// Gets the rejecting job offers count threshold.
        /// </summary>
        /// <value>
        /// The rejecting job offers count threshold.
        /// </value>
        public int RejectingJobOffersCountThreshold
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.RejectingJobOffersCountThreshold, 2); }
        }

        /// <summary>
        /// Gets the rejecting job offers days threshold.
        /// </summary>
        /// <value>
        /// The rejecting job offers days threshold.
        /// </value>
        public int RejectingJobOffersDaysThreshold
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.RejectingJobOffersDaysThreshold, 14); }
        }

        /// <summary>
        /// Gets the not reporting for interview count threshold.
        /// </summary>
        /// <value>
        /// The not reporting for interview count threshold.
        /// </value>
        public int NotReportingForInterviewCountThreshold
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.NotReportingForInterviewCountThreshold, 2); }
        }

        /// <summary>
        /// Gets the not reporting for interview days threshold.
        /// </summary>
        /// <value>
        /// The not reporting for interview days threshold.
        /// </value>
        public int NotReportingForInterviewDaysThreshold
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.NotReportingForInterviewDaysThreshold, 14); }
        }

        /// <summary>
        /// Gets the not responding to employer invites count threshold.
        /// </summary>
        /// <value>
        /// The not responding to employer invites count threshold.
        /// </value>
        public int NotRespondingToEmployerInvitesCountThreshold
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.NotRespondingToEmployerInvitesCountThreshold, 2); }
        }

        /// <summary>
        /// Gets the not responding to employer invites days threshold.
        /// </summary>
        /// <value>
        /// The not responding to employer invites days threshold.
        /// </value>
        public int NotRespondingToEmployerInvitesDaysThreshold
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.NotRespondingToEmployerInvitesDaysThreshold, 14); }
        }

        /// <summary>
        /// Gets the not viewing invite grace period no of days.
        /// </summary>
        /// <value>The grace period in days.</value>
        public int NotRespondingToEmployerInvitesGracePeriodDays
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.NotRespondingToEmployerInvitesGracePeriodDays, 2); }
        }

        /// <summary>
        /// Gets the not responding to referral suggestions count threshold.
        /// </summary>
        /// <value>
        /// The not responding to referral suggestions count threshold.
        /// </value>
        public int NotRespondingToReferralSuggestionsCountThreshold
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.NotRespondingToReferralSuggestionsCountThreshold, 2); }
        }

        /// <summary>
        /// Gets the not responding to referral suggestions days threshold.
        /// </summary>
        /// <value>
        /// The not responding to referral suggestions days threshold.
        /// </value>
        public int NotRespondingToReferralSuggestionsDaysThreshold
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.NotRespondingToReferralSuggestionsDaysThreshold, 14); }
        }

        /// <summary>
        /// Gets the self referring to unqualified jobs count threshold.
        /// </summary>
        /// <value>
        /// The self referring to unqualified jobs count threshold.
        /// </value>
        public int SelfReferringToUnqualifiedJobsCountThreshold
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.SelfReferringToUnqualifiedJobsCountThreshold, 2); }
        }

        /// <summary>
        /// Gets the self referring to unqualified jobs days threshold.
        /// </summary>
        /// <value>
        /// The self referring to unqualified jobs days threshold.
        /// </value>
        public int SelfReferringToUnqualifiedJobsDaysThreshold
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.SelfReferringToUnqualifiedJobsDaysThreshold, 14); }
        }

        /// <summary>
        /// Gets the self referring to unqualified min star threshold.
        /// </summary>
        /// <value>
        /// The self referring to unqualified min star threshold.
        /// </value>
        public int SelfReferringToUnqualifiedMinStarThreshold
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.SelfReferringToUnqualifiedMinStarThreshold, 3); }
        }

        /// <summary>
        /// Gets the self referring to unqualified max star threshold.
        /// </summary>
        /// <value>
        /// The self referring to unqualified max star threshold.
        /// </value>
        public int SelfReferringToUnqualifiedMaxStarThreshold
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.SelfReferringToUnqualifiedMaxStarThreshold, 2); }
        }

        /// <summary>
        /// Gets the showing low quality matches count threshold.
        /// </summary>
        /// <value>
        /// The showing low quality matches count threshold.
        /// </value>
        public int ShowingLowQualityMatchesCountThreshold
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.ShowingLowQualityMatchesCountThreshold, 2); }
        }

        /// <summary>
        /// Gets the showing low quality matches days threshold.
        /// </summary>
        /// <value>
        /// The showing low quality matches days threshold.
        /// </value>
        public int ShowingLowQualityMatchesDaysThreshold
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.ShowingLowQualityMatchesDaysThreshold, 14); }
        }

        /// <summary>
        /// Gets the showing low quality matches min star threshold.
        /// </summary>
        /// <value>
        /// The showing low quality matches min star threshold.
        /// </value>
        public int ShowingLowQualityMatchesMinStarThreshold
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.ShowingLowQualityMatchesMinStarThreshold, 2); }
        }

        /// <summary>
        /// Gets the posting low quality resume word count threshold.
        /// </summary>
        /// <value>
        /// The posting low quality resume word count threshold.
        /// </value>
        public int PostingLowQualityResumeWordCountThreshold
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.PostingLowQualityResumeWordCountThreshold, 100); }
        }

        /// <summary>
        /// Gets the posting low quality resume skill count threshold.
        /// </summary>
        /// <value>
        /// The posting low quality resume skill count threshold.
        /// </value>
        public int PostingLowQualityResumeSkillCountThreshold
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.PostingLowQualityResumeSkillCountThreshold, 5); }
        }

        /// <summary>
        /// Gets the not searching jobs days threshold.
        /// </summary>
        /// <value>
        /// The not searching jobs days threshold.
        /// </value>
        public int NotSearchingJobsDaysThreshold
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.NotSearchingJobsDaysThreshold, 7); }
        }

        /// <summary>
        /// Gets the not searching jobs count threshold.
        /// </summary>
        /// <value>
        /// The not searching jobs count threshold.
        /// </value>
        public int NotSearchingJobsCountThreshold
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.NotSearchingJobsCountThreshold, 2); }
        }

        #endregion

        #endregion

        #region Special Configuration Items

        /// <summary>
        /// Gets the custom XSL for resume.
        /// </summary>
        /// <param name="resumeFormat">The resume format.</param>
        /// <returns></returns>
        public string GetCustomXslForResume(string resumeFormat)
        {
            return GetStringConfigItemValue(Constants.ConfigurationItemKeys.ResumeFormat + "_" + resumeFormat, Defaults.ResumeDefaults.ResumeDisplayStylePath);
        }

        /// <summary>
        /// Gets the job posting style path.
        /// </summary>
        /// <value>
        /// The job posting style path.
        /// </value>
        public string JobPostingStylePath
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.JobPostingFormat, Defaults.JobPostingDefaults.JobPostingStylePath); }
        }

        /// <summary>
        /// Gets the recent placement timeframe.
        /// </summary>
        /// <value>The recent placement timeframe.</value>
        public int RecentPlacementTimeframe
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.RecentPlacementTimeframe, 30); }
        }

        /// <summary>
        /// Gets the default search radius key.
        /// </summary>
        /// <value>The default search radius key.</value>
        public string DefaultSearchRadiusKey
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.DefaultSearchRadiusKey, Defaults.ConfigurationItemDefaults.DefaultSearchRadiusKey); }
        }

        /// <summary>
        /// Gets the maximum recently viewed postings.
        /// </summary>
        /// <value>The maximum recently viewed postings.</value>
        public int MaximumRecentlyViewedPostings
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.MaximumRecentlyViewedPostings, 5); }
        }

        #region Job Development settings

        /// <summary>
        /// Gets the job development score threshold.
        /// </summary>
        /// <value>The job development score threshold.</value>
        public int JobDevelopmentScoreThreshold
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.JobDevelopmentScoreThreshold, 850); }
        }

        /// <summary>
        /// Gets the minimum stars for the spidered jobs search on the Assist job development pages.
        /// </summary>
        /// <value>The minimum stars.</value>
        public int JobDevelopmentSpideredJobSearchMatchesMinimumStars
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.JobDevelopmentSpideredJobSearchMatchesMinimumStars, 1); }
        }

        /// <summary>
        /// Gets the job development employer placements no of days.
        /// </summary>
        /// <value>The job development employer placements no of days.</value>
        public int JobDevelopmentEmployerPlacementsNoOfDays
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.JobDevelopmentEmployerPlacementsNoOfDays, 7); }
        }

        /// <summary>
        /// Gets the number of postings required for an employer to appear on the job development spidered job list.
        /// </summary>
        /// <value>
        /// The number of postings required.
        /// </value>
        public int JobDevelopmentSpideredJobListNumberOfPostingsRequired
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.JobDevelopmentSpideredJobListNumberOfPostingsRequired, 2); }
        }

        /// <summary>
        /// Gets the number of postings to display on the job development spidered job list.
        /// </summary>
        /// <value>The number of postings to display.</value>
        public int JobDevelopmentSpideredJobListPostingsToDisplay
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.JobDevelopmentSpideredJobListPostingsToDisplay, 25); }
        }

        /// <summary>
        /// Gets the maximum posting age to display on the job development spidered job list.
        /// </summary>
        /// <value>The posting age.</value>
        public int? JobDevelopmentSpideredJobListPostingAge
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.JobDevelopmentSpideredJobListPostingAge, 30); }
        }

        /// <summary>
        /// Gets the maximum number of employers to diaplay on the job development spidered job list.
        /// </summary>
        /// <value>
        /// The  maximum number of employers.
        /// </value>
        public int JobDevelopmentSpideredJobListMaximumNumberOfEmployers
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.JobDevelopmentSpideredJobListMaximumNumberOfEmployers, 30); }
        }

        /// <summary>
        /// Gets the job development page's minimum recent matches count.
        /// </summary>
        /// <value>
        /// The job development minimum recent matches count.
        /// </value>
        public int JobDevelopmentMinimumRecentMatchesCount
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.JobDevelopmentMinimumRecentMatchesCount, 25); }
        }

        /// <summary>
        /// Gets the job development maximum posting matches count.
        /// </summary>
        /// <value>
        /// The job development maximum posting matches count.
        /// </value>
        public int JobDevelopmentMaximumPostingMatchesCount
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.JobDevelopmentMaximumPostingMatchesCount, 25); }
        }

        #endregion

        #region Find Employer settings

        /// <summary>
        /// Determines the maximum number of FEINs that can be entered in the Find Employer control
        /// </summary>
        public int FindEmployerFEINsAllowed
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.FindEmployerFEINsAllowed, Theme == FocusThemes.Education ? 0 : 20); }
        }

        /// <summary>
        /// Determines the maximum number of FEINs that can be shown before scrolling occurs in the Find Employer control
        /// </summary>
        public int FindEmployerFEINsScrolling
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.FindEmployerFEINsScrolling, 3); }
        }

        #endregion

        #region Live Jobs Repository Settings

        /// <summary>
        /// Gets the live jobs API root URL.
        /// </summary>
        /// <value>The live jobs API root URL.</value>
        public string LiveJobsApiRootUrl
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.LiveJobsApiRootUrl, "http://10.0.1.223/LiveJobsApi_v1127"); }
        }

        /// <summary>
        /// Gets the live jobs API key.
        /// </summary>
        /// <value>The live jobs API key.</value>
        public string LiveJobsApiKey
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.LiveJobsApiKey, "BGT"); }
        }

        /// <summary>
        /// Gets the live jobs API secret.
        /// </summary>
        /// <value>The live jobs API secret.</value>
        public string LiveJobsApiSecret
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.LiveJobsApiSecret, "086718ED10AC48B685F93535208AE903"); }
        }

        /// <summary>
        /// Gets the live jobs API token.
        /// </summary>
        /// <value>The live jobs API token.</value>
        public string LiveJobsApiToken
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.LiveJobsApiToken, "Core"); }
        }

        /// <summary>
        /// Gets the live jobs API token secret.
        /// </summary>
        /// <value>The live jobs API token secret.</value>
        public string LiveJobsApiTokenSecret
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.LiveJobsApiTokenSecret, "3250A588F0FD11E1A0EA0CF26088709B"); }
        }

        #endregion

        /// <summary>
        /// Gets the recommended matches minimum stars.
        /// </summary>
        public int RecommendedMatchesMinimumStars
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.RecommendedMatchesMinimumStars, 2); }
        }

        /// <summary>
        /// Gets the resume referral approvals minimums stars.
        /// </summary>
        /// <value>The resume referral approvals minimums stars.</value>
        public int ResumeReferralApprovalsMinimumStars
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.ResumeReferralApprovalsMinimumStars, 0); }
        }

        /// <summary>
        /// Gets a value indicating whether [show license requirement].
        /// </summary>
        /// <value>
        /// 	<c>true</c> if [show license requirement]; otherwise, <c>false</c>.
        /// </value>
        public bool ShowLicenseRequirement
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ShowLicenseRequirement, true); }
        }

        /// <summary>
        /// Whether to show the Student's External Id in Assist
        /// </summary>
        public bool ShowStudentExternalId
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ShowStudentExternalId, true); }
        }

        /// <summary>
        /// Gets the type of the explorer degree filtering.
        /// </summary>
        public DegreeFilteringType ExplorerDegreeFilteringType
        {
            get { return GetEnumConfigItemValue(Constants.ConfigurationItemKeys.ExplorerDegreeFilteringType, DegreeFilteringType.Ours); }
        }

        /// <summary>
        /// Gets a value indicating whether [explorer uses degree tiering].
        /// </summary>
        public bool ExplorerUseDegreeTiering
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ExplorerUseDegreeTiering, true); }
        }

        /// <summary>
        /// Gets a value indicating whether [explorer only show jobs based configuration client degrees].
        /// </summary>
        /// <value>
        /// <c>true</c> if [explorer only show jobs based configuration client degrees]; otherwise, <c>false</c>.
        /// </value>
        public bool ExplorerOnlyShowJobsBasedOnClientDegrees
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ExplorerOnlyShowJobsBasedOnClientDegrees, false); }
        }

        /// <summary>
        /// Gets a value indicating whether to check for criminal record exclusion.
        /// </summary>
        public bool CheckCriminalRecordExclusion
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.CheckCriminalRecordExclusion, Theme == FocusThemes.Workforce); }
        }

        /// <summary>
        /// Gets a value indicating whether [offices enabled].
        /// </summary>
        /// <value><c>true</c> if [offices enabled]; otherwise, <c>false</c>.</value>
        public bool OfficesEnabled
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.OfficesEnabled, Theme == FocusThemes.Workforce); }
        }

        /// <summary>
        /// Gets the Asset URL.
        /// </summary>
        /// <value>
        /// The Asset URL.
        /// </value>
        public string AssetUrl
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.AssetUrl, string.Empty); }
        }

        #endregion

        #region Assist Job Issues

        // Assist job order issue settings

        /// <summary>
        /// Gets the job minimum match count threshold.
        /// </summary>
        /// 
        public int JobIssueMinimumMatchCountThreshold
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.JobIssueHiredDaysElapsedThreshold, 5); }
        }

        /// <summary>
        /// Gets the job minimum stars threshold (used for minimum match count).
        /// </summary>
        /// 
        public int JobIssueMatchMinimumStarsThreshold
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.JobIssueMatchMinimumStarsThreshold, 4); }
        }

        /// <summary>
        /// Gets the job match days threshold (used for minimum match count).
        /// </summary>
        /// 
        public int JobIssueMatchDaysThreshold
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.JobIssueMatchDaysThreshold, 5); }
        }

        /// <summary>
        /// Gets the job minimum referrals count.
        /// </summary>
        /// 
        public int JobIssueMinimumReferralsCountThreshold
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.JobIssueMinimumReferralsCountThreshold, 5); }
        }

        /// <summary>
        /// Gets the days threshold for the job referral count.
        /// </summary>
        /// 
        public int JobIssueReferralCountDaysThreshold
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.JobIssueReferralCountDaysThreshold, 10); }
        }

        /// <summary>
        /// Gets the job days threshold for not clicking referrals.
        /// </summary>
        /// 
        public int JobIssueNotClickingReferralsDaysThreshold
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.JobIssueNotClickingReferralsDaysThreshold, 3); }
        }

        /// <summary>
        /// Gets the job days threshold for not clicking referrals.
        /// </summary>
        /// 
        public int JobIssueSearchesWithoutInviteThreshold
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.JobIssueSearchesWithoutInviteThreshold, 3); }
        }

        /// <summary>
        /// Gets the days threshold for searching but not inviting.
        /// </summary>
        /// 
        public int JobIssueSearchesWithoutInviteDaysThreshold
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.JobIssueSearchesWithoutInviteDaysThreshold, 5); }
        }

        /// <summary>
        /// Gets the job days threshold for not clicking referrals.
        /// </summary>
        /// 
        public int JobIssuePostClosedEarlyDaysThreshold
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.JobIssuePostClosedEarlyDaysThreshold, 3); }
        }

        /// <summary>
        /// Gets the job days threshold for not clicking referrals.
        /// </summary>
        /// 
        public int JobIssueHiredDaysElapsedThreshold
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.JobIssueHiredDaysElapsedThreshold, 14); }
        }

        // Assist job order issue switches

        /// <summary>
        /// Gets whether a low number of minimum star matches is flagged.
        /// </summary>
        /// 
        public bool JobIssueFlagLowMinimumStarMatches
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.JobIssueFlagLowMinimumStarMatches, true); }
        }

        /// <summary>
        /// Gets whether low number of referrals is flagged.
        /// </summary>
        /// 
        public bool JobIssueFlagLowReferrals
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.JobIssueFlagLowReferrals, true); }
        }

        /// <summary>
        /// Gets whether a contact not clicking referrals is flagged.
        /// </summary>
        /// 
        public bool JobIssueFlagNotClickingReferrals
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.JobIssueFlagNotClickingReferrals, true); }
        }

        /// <summary>
        /// Gets whether a contact searching but not inviting candidates is flagged.
        /// </summary>
        /// 
        public bool JobIssueFlagSearchingNotInviting
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.JobIssueFlagSearchingNotInviting, true); }
        }

        /// <summary>
        /// Gets whether refreshing job closing date is flagged.
        /// </summary>
        ///
        public bool JobIssueFlagClosingDateRefreshed
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.JobIssueFlagClosingDateRefreshed, true); }
        }

        /// <summary>
        /// Gets whether closing a job before it's closing date is flagged.
        /// </summary>
        ///
        public bool JobIssueFlagJobClosedEarly
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.JobIssueFlagJobClosedEarly, true); }
        }

        /// <summary>
        /// Gets whether a job is flagged a certain period of time after hiring.
        /// </summary>
        ///
        public bool JobIssueFlagJobAfterHiring
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.JobIssueFlagJobAfterHiring, true); }
        }

        /// <summary>
        /// Gets whether a contact providing negative feedback to customer service questions is flagged.
        /// </summary>
        ///
        public bool JobIssueFlagNegativeFeedback
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.JobIssueFlagNegativeFeedback, true); }
        }

        /// <summary>
        /// Gets whether a contact providing positive feedback to customer service questions is flagged.
        /// </summary>
        /// 
        public bool JobIssueFlagPositiveFeedback
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.JobIssueFlagPositiveFeedback, true); }
        }

        #endregion

        #region Assist Activities Backdate Feature Configurations - DEPRECATED
        /// <summary>
        /// Enables Activity Date Selection
        /// </summary>
        //public bool EnableEmployerActivityDateSelection
        //{
        //    get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.EnableEmployerActivityDateSelection, false); }
        //}

        /// <summary>
        /// Gets the backdate employer activity maximum days.
        /// </summary>
        //public int BackdateEmployerActivityMaxDays
        //{
        //    get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.BackdateEmployerActivityMaxDays, 30); }
        //} 
        #endregion

        #region Assist Activities/Services Feature Configurations
        public bool EnableJSActivityBackdatingDateSelection
        { get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.EnableJSActivityBackdatingDateSelection, true); } }

        public bool EnableHiringManagerActivityBackdatingDateSelection
        { get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.EnableHiringManagerActivityBackdatingDateSelection, true); } }
        #endregion

        #region Assist Activities and Services settings
        public bool EnableJobSeekerActivitiesServices
        {
            get { return Theme == FocusThemes.Workforce && GetBoolConfigItemValue(Constants.ConfigurationItemKeys.EnableJobSeekerActivitiesServices, true); }
        }
        public bool EnableHiringManagerActivitiesServices
        {
            get { return Theme == FocusThemes.Workforce && GetBoolConfigItemValue(Constants.ConfigurationItemKeys.EnableHiringManagerActivitiesServices, true); }
        }
        #endregion

        #region Career Search Defaults

        /// <summary>
        /// Default Career Search - Show all jobs regardless of score
        /// </summary>
        public bool CareerSearchShowAllJobs
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.CareerSearchShowAllJobs, true); }
        }

        /// <summary>
        /// Default Career Search - Minimum stars when searching on resumes
        /// </summary>
        public int CareerSearchMinimumStarMatchScore
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.CareerSearchMinimumStarMatchScore, 3); }
        }

        /// <summary>
        /// Default Career Search - Search radius for seeker's postcode
        /// </summary>
        public int CareerSearchDefaultRadiusId
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.CareerSearchDefaultRadiusId, 0); }
        }

        /// <summary>
        /// Default Career Search - Which search to perform when no location specified
        /// </summary>
        public SearchCentrePointOptions CareerSearchCentrePointType
        {
            get { return GetEnumConfigItemValue(Constants.ConfigurationItemKeys.CareerSearchCentrePointType, SearchCentrePointOptions.Zipcode); }
        }

        /// <summary>
        /// Default Career Search - Radius search when no location specified
        /// Only applicable when CareerSearchCentrePointType is Zipcode
        /// </summary>
        public long CareerSearchRadius
        {
            get { return GetLongConfigItemValue(Constants.ConfigurationItemKeys.CareerSearchRadius, -1); }
        }

        /// <summary>
        /// Default Career Search - Zipcode to search when no location specified
        /// Only applicable when CareerSearchCentrePointType is Zipcode
        /// </summary>
        public string CareerSearchZipcode
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.CareerSearchZipcode, string.Empty); }
        }

        /// <summary>
        /// Default Career Search - Searchable resume options
        /// </summary>
        public ResumeSearchableOptions CareerSearchResumesSearchable
        {
            get { return GetEnumConfigItemValue(Constants.ConfigurationItemKeys.CareerSearchResumesSearchable, ResumeSearchableOptions.NotApplicable); }
        }

        /// <summary>
        /// Whether veteran priority search service is enabled
        /// </summary>
        public bool VeteranPriorityServiceEnabled
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.VeteranPriorityServiceEnabled, Theme == FocusThemes.Workforce); }
            set { }
        }

        /// <summary>
        /// Minimum star match required for veteran priority search service
        /// </summary>
        public int VeteranPriorityStarMatching
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.VeteranPriorityStarMatching, 3); }
        }

        /// <summary>
        /// The number of hours veterans gain exclusivtity to new jobs.
        /// </summary>
        public int VeteranPriorityExclusiveHours
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.VeteranPriorityExclusiveHours, 24); }
        }

        /// <summary>
        /// Whether it is possible to search on work availability
        /// </summary>
        public bool WorkAvailabilitySearch
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.WorkAvailabilitySearch, Theme == FocusThemes.Workforce); }
        }

        /// <summary>
        /// Default posting days on search
        /// </summary>
        public int JobSearchPostingDate
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.JobSearchPostingDate, Theme == FocusThemes.Workforce ? 7 : 14); }
        }

        #endregion

        #region Custom Footer/Header

        public bool UseCustomHeaderHtml
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.UseCustomHeaderHtml, false); }
        }

        public bool UseCustomFooterHtml
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.UseCustomFooterHtml, false); }
        }

        public bool HideTalentAssistAccountSettings
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.HideTalentAssistAccountSettings, false); }
        }

        public bool HideTalentAssistSignOut
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.HideTalentAssistSignOut, false); }
        }

        public bool HideCareerSignIn
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.HideCareerSignIn, false); }
        }

        public bool HideCareerRegister
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.HideCareerRegister, false); }
        }

        public bool HideCareerMyAccount
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.HideCareerMyAccount, false); }
        }

        public bool HideCareerSignOut
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.HideCareerSignOut, false); }
        }

        public string CustomHeaderHtml
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.CustomHeaderHtml, ""); }
        }

        public string CustomFooterHtml
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.CustomFooterHtml, ""); }
        }

        public bool HideLegalNoticeLink
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.HideLegalNoticeLink, false); }
        }

        public bool ShowCareerTutorialLink
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ShowCareerHelpLink, false); }
        }

        public bool UseUsernameAsAccountLink
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.UseUsernameAsAccountLink, false); }
        }

        #endregion

        #region Alert Defaults

        public JobAlertsOptions JobAlertsOption
        {
            get { return GetEnumConfigItemValue(Constants.ConfigurationItemKeys.JobAlertsOption, JobAlertsOptions.UserConfiguration); }
        }

        public AlertFrequencies JobAlertFrequency
        {
            get { return GetEnumConfigItemValue(Constants.ConfigurationItemKeys.JobAlertFrequency, AlertFrequencies.Weekly); }
        }

        public bool SendOtherJobsEmail
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.SendOtherJobsEmail, false); }
        }

        public AlertFrequencies StaffEmailAlerts
        {
            get { return GetEnumConfigItemValue(Constants.ConfigurationItemKeys.StaffEmailAlerts, AlertFrequencies.Never); }
        }

        public int JobAlertTruncateWords
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.JobAlertTruncateWords, 50); }
        }

        #endregion

        #region Job Disclaimer Defaults

        public bool SpideredJobDisclaimerEnabled
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.SpideredJobDisclaimerEnabled, Theme == FocusThemes.Workforce); }
        }

        public string SpideredJobDisclaimerText
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.SpideredJobDisclaimerText, "When you click the link below you will leave this website. Our organization does not endorse, take responsibility for, and exercise any control over the linked organization or its contents. We cannot verify the destination site for accuracy, accessibility, security, or legal compliance."); }
        }

        public bool SearchResultsDisclaimerEnabled
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.SearchResultsDisclaimerEnabled, Theme == FocusThemes.Workforce); }
        }

        public string SearchResultsDisclaimerText
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.SearchResultsDisclaimerText, "Job search results include two types of jobs. Jobs posted directly by #BUSINESSES#:LOWER registered with this website, are designated by an icon. Spidered jobs from other #BUSINESSES#:LOWER and job sources, which are not registered or verified by this provider, also are provided for your area. Job seekers should use caution in applying for these jobs, as we cannot guarantee the wages, accuracy of the information, or the validity of the #BUSINESS#:LOWER or job opening."); }
        }

        #endregion

        #region Sharing Defaults

        /// <summary>
        /// Whether employer users at the same FEIN may share job postings
        /// </summary>
        public bool SharingJobsForSameFEIN
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.SharingJobsForSameFEIN, true); }
        }

        #endregion

        #region Job Seeker Inactivity Email Defaults

        /// <summary>
        /// Whether to send an email if a job seeker has been inactive for a given number of days
        /// </summary>
        public bool JobSeekerSendInactivityEmail
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.JobSeekerSendInactivityEmail, false); }
        }

        /// <summary>
        /// The number of days a job seeker has been inactive before an email is sent
        /// </summary>
        public int JobSeekerInactivityEmailLimit
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.JobSeekerInactivityEmailLimit, 60); }
        }

        /// <summary>
        /// The number of days an inactive job seeker must log in after an email is sent
        /// </summary>
        public int JobSeekerInactivityEmailGrace
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.JobSeekerInactivityEmailGrace, 14); }
        }

        #endregion

        #region SSO

        /// <summary>
        /// Gets the SSO return URL.
        /// </summary>
        public string SSOReturnUrl
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.SSOReturnUrl, "~/ssoclose"); }
        }

        /// <summary>
        /// Gets the SSO manage account URL.
        /// </summary>
        public string SSOErrorRedirectUrl
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.SSOErrorRedirectUrl, "~/ssoerror?message=#ERROR#"); }
        }

        public bool SSOEnableDeactivateJobSeeker
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.SSOEnableDeactivateJobSeeker, false); }
        }

        public bool SSOForceNewUserToRegistration
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.SSOForceNewUserToRegistration, false); }
        }

        public bool SSOEnabledForCareer
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.SSOEnabledForCareer, false); }
        }

        public bool SSOEnabled
        {
            get { return SamlEnabled || OAuthEnabled; }
        }

        /// <summary>
        /// Gets the SSO manage account URL.
        /// </summary>
        public string SSOManageAccountUrl
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.SSOManageAccountUrl, string.Empty); }
        }

        /// <summary>
        /// Gets the sso change username and password URL.
        /// </summary>
        public string SSOChangeUsernamePasswordUrl
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.SSOChangeUsernamePasswordUrl, string.Empty); }
        }

        #region oAuth

        /// <summary>
        /// Gets the oAuth consumer key.
        /// </summary>
        public string OAuthConsumerKey
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.OAuthConsumerKey, String.Empty); }
        }

        /// <summary>
        /// Gets the oAuth consumer secret.
        /// </summary>
        public string OAuthConsumerSecret
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.OAuthConsumerSecret, String.Empty); }
        }

        /// <summary>
        /// Gets the oAuth request token URL.
        /// </summary>
        public string OAuthRequestTokenUrl
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.OAuthRequestTokenUrl, String.Empty); }
        }

        /// <summary>
        /// Gets the oAuth verifier URL.
        /// </summary>
        public string OAuthVerifierUrl
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.OAuthVerifierUrl, String.Empty); }
        }

        /// <summary>
        /// Gets the oAuth request access token URL.
        /// </summary>
        public string OAuthRequestAccessTokenUrl
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.OAuthRequestAccessTokenUrl, String.Empty); }
        }

        /// <summary>
        /// Gets the oAuth request profile URL.
        /// </summary>
        public string OAuthRequestProfileUrl
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.OAuthRequestProfileUrl, String.Empty); }
        }

        /// <summary>
        /// Gets the oAuth scope.
        /// </summary>
        public string OAuthScope
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.OAuthScope, String.Empty); }
        }

        /// <summary>
        /// Gets the oAuth realm.
        /// </summary>
        public string OAuthRealm
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.OAuthRealm, String.Empty); }
        }

        /// <summary>
        /// Gets the oAuth profile mapper.
        /// </summary>
        public string OAuthProfileMapperCode
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.OAuthProfileMapper, String.Empty); }
        }

        /// <summary>
        /// Gets the oAuth version.
        /// </summary>
        public string OAuthVersion
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.OAuthVersion, "1.0"); }
        }

        /// <summary>
        /// Gets a value indicating whether oAuth is enabled.
        /// 
        /// This will default to what could be set int he web / app config file to allow for easier development & testing
        /// 
        /// </summary>
        public bool OAuthEnabled
        {
            get
            {
                var oauthEnabledInWebConfig = GetBoolWebConfigValue(Constants.ConfigurationItemKeys.OAuthEnabled, false);
                return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.OAuthEnabled, oauthEnabledInWebConfig);
            }
        }

        public List<OAuthSettings> OutgoingOAuthSettings
        {
            get
            {
                //TODO swap over when finalised
                //var outgoingOAuthSettings = _configurationItems.Where(ci => ci.Key == Constants.ConfigurationItemKeys.OutgoingOAuthSettings).Select(ci => ci.Value).ToList();
                //var settings = outgoingOAuthSettings.Select(configurationItem => configurationItem.DeserializeJson<OAuthSettings>()).ToList();
                //return settings;

                var outgoingOAuthSettings = new List<OAuthSettings>
                {
                    new OAuthSettings
                    {
                        Client = OAuthClient.LaborInsight,
                        ConsumerKey = "BGT",
                        ConsumerSecret = "086718ED10AC48B685F93535208AE903",
                        Token = "Insight",
                        TokenSecret = "32611633362D4A76A3C6038E71BFE90C",
                        EndPoint = "http://sandbox.api.burning-glass.com/v202/"
                    }
                };

                return outgoingOAuthSettings;
            }
        }

        #endregion

        #region Saml

        /// <summary>
        /// Gets a value indicating whether Saml is enabled.
        /// 
        /// This will default to what could be set int the web / app config file to allow for easier development & testing
        /// 
        /// </summary>
        public bool SamlEnabled
        {
            get
            {
                return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.SamlEnabled, false);
            }
        }

        /// <summary>
        /// Gets a value indicating whether Saml is enabled only for career.
        /// </summary>
        public bool SamlEnabledForCareer { get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.SamlEnabledForCareer, false); } }

        /// <summary>
        /// Gets a value indicating whether Saml is enabled only for Talent.
        /// </summary>
        public bool SamlEnabledForTalent { get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.SamlEnabledForTalent, false); } }

        /// <summary>
        /// Gets a value indicating whether Saml is enabled only for Assist.
        /// </summary>
        public bool SamlEnabledForAssist { get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.SamlEnabledForAssist, false); } }

        /// <summary>
        /// Gets the saml artifact resolution service URL.
        /// </summary>
        /// <value>
        /// The saml artifact resolution service URL.
        /// </value>
        public string SamlIdPArtifactIdProviderUrl
        {
            get
            {
                return GetStringConfigItemValue(Constants.ConfigurationItemKeys.SamlIdPArtifactIdProviderUrl, String.Empty);
            }
        }

        /// <summary>
        /// Gets the URL of the SAML SSO service.
        /// </summary>
        /// <value>
        /// The URL of the SAML SSO service.
        /// </value>
        public string SamlIdPSingleSignonIdProviderUrl
        {
            get
            {
                return GetStringConfigItemValue(Constants.ConfigurationItemKeys.SamlIdPSingleSignonIdProviderUrl, String.Empty);
            }
        }

        /// <summary>
        /// Gets the URL of the secondary SAML SSO service.
        /// </summary>
        /// <value>
        /// The URL of the secondary SAML SSO service.
        /// </value>
        public string SamlSecIdPSingleSignonIdProviderUrl { get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.SamlSecIdPSingleSignonIdProviderUrl, string.Empty); } }

        /// <summary>
        /// Gets the saml metadata URL (this functionality is untested)
        /// </summary>
        /// <value>
        /// The saml metadata URL.
        /// </value>
        public string SamlMetadataUrl
        {
            get
            {
                return GetStringConfigItemValue(Constants.ConfigurationItemKeys.SamlMetadataUrl, string.Empty);
            }
        }

        /// <summary>
        /// Gets the saml service provider to identity provider binding URL.
        /// </summary>
        /// <value>
        /// The saml service provider to identity provider binding URL.
        /// </value>
        public string SamlSpToIdPBindingUrl
        {
            get
            {
                return GetStringConfigItemValue(Constants.ConfigurationItemKeys.SamlSpToIdPBindingUrl, SamlBindingUri.HttpPost);
            }
        }

        /// <summary>
        /// Gets the saml identity provider to service provider binding URL.
        /// </summary>
        /// <value>
        /// The saml identity provider to service provider binding URL.
        /// </value>
        public string SamlIdPToSpBindingUrl
        {
            get
            {
                return GetStringConfigItemValue(Constants.ConfigurationItemKeys.SamlIdPToSpBindingUrl, SamlBindingUri.HttpPost);
            }
        }

        /// <summary>
        /// Gets the saml query string binding variable (For test use - allows us to tell the test identity provider what binding to use)
        /// </summary>
        /// <value>
        /// The saml query string binding variable.
        /// </value>
        public string SamlQueryStringBindingVar
        {
            get
            {
                return GetStringConfigItemValue(Constants.ConfigurationItemKeys.SamlQueryStringBindingVar, String.Empty);
            }
        }

        /// <summary>
        /// Gets the saml artifact identity URL.
        /// </summary>
        /// <value>
        /// The saml artifact identity URL.
        /// </value>
        public string SamlArtifactIdentityUrl
        {
            get
            {
                return GetStringConfigItemValue(Constants.ConfigurationItemKeys.SamlArtifactIdentityUrl
                    , (new Uri(HttpContext.Current.Request.Url, HttpContext.Current.Request.ApplicationPath ?? "/")).AbsoluteUri);
            }
        }

        /// <summary>
        /// Gets the saml sp key location.
        /// </summary>
        /// <value>
        /// The saml sp key location.
        /// </value>
        public string SamlSpKeyLocation
        {
            get
            {
                return GetStringConfigItemValue(Constants.ConfigurationItemKeys.SamlSpKeyLocation, HttpContext.Current.Server.MapPath("~/WebAuth/Keys/"));
            }
        }

        /// <summary>
        /// Gets the name of the saml sp key.
        /// </summary>
        /// <value>
        /// The name of the saml sp key.
        /// </value>
        public string SamlSpKeyName
        {
            get
            {
                return GetStringConfigItemValue(Constants.ConfigurationItemKeys.SamlSpKeyName, String.Empty);
            }
        }

        /// <summary>
        /// Gets the saml sp key password.
        /// </summary>
        /// <value>
        /// The saml sp key password.
        /// </value>
        public string SamlSpKeyPassword
        {
            get
            {
                return GetStringConfigItemValue(Constants.ConfigurationItemKeys.SamlSpKeyPassword, String.Empty);
            }
        }

        /// <summary>
        /// Gets the saml unique identifier application cert key location.
        /// </summary>
        /// <value>
        /// The saml unique identifier application cert key location.
        /// </value>
        public string SamlIdPCertKeyLocation
        {
            get
            {
                return GetStringConfigItemValue(Constants.ConfigurationItemKeys.SamlIdPCertKeyLocation, HttpContext.Current.Server.MapPath("~/WebAuth/Keys/"));
            }
        }

        /// <summary>
        /// Gets the name of the saml unique identifier application cert key.
        /// </summary>
        /// <value>
        /// The name of the saml unique identifier application cert key.
        /// </value>
        public string SamlIdPCertKeyName
        {
            get
            {
                return GetStringConfigItemValue(Constants.ConfigurationItemKeys.SamlIdPCertKeyName, string.Empty);
            }
        }

        /// <summary>
        /// Gets the secondary saml unique identifier application cert key location.
        /// </summary>
        /// <value>
        /// The secondary saml unique identifier application cert key location.
        /// </value>
        public string SamlSecIdPCertKeyName { get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.SamlSecIdPCertKeyName, string.Empty); } }

        /// <summary>
        /// Gets the Saml SSO logout return URL. - used only for secondary saml settings. for default use the "SSOReturnUrl" config 
        /// </summary>
        public string SamlLogoutRedirect
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.SamlLogoutRedirect, "~/ssoclose"); }
        }
        public string SamlSecExternalClientTag { get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.SamlSecExternalClientTag, string.Empty); } }

        /// <summary>
        /// Gets the name of the saml first name attribute.
        /// </summary>
        /// <value>
        /// The name of the saml first name attribute.
        /// </value>
        public string SamlFirstNameAttributeName
        {
            get
            {
                return GetStringConfigItemValue(Constants.ConfigurationItemKeys.SamlFirstNameAttributeName, "FirstName");
            }
        }

        /// <summary>
        /// Gets the name of the saml last name attribute.
        /// </summary>
        /// <value>
        /// The name of the saml last name attribute.
        /// </value>
        public string SamlLastNameAttributeName
        {
            get
            {
                return GetStringConfigItemValue(Constants.ConfigurationItemKeys.SamlLastNameAttributeName, "LastName");
            }
        }

        /// <summary>
        /// Gets the name of the saml screen name attribute.
        /// </summary>
        /// <value>
        /// The name of the saml screen name attribute.
        /// </value>
        public string SamlScreenNameAttributeName
        {
            get
            {
                return GetStringConfigItemValue(Constants.ConfigurationItemKeys.SamlScreenNameAttributeName, "ScreenName");
            }
        }

        /// <summary>
        /// Gets the name of the saml email address attribute.
        /// </summary>
        /// <value>
        /// The name of the saml email address attribute.
        /// </value>
        public string SamlEmailAddressAttributeName
        {
            get
            {
                return GetStringConfigItemValue(Constants.ConfigurationItemKeys.SamlEmailAddressAttributeName, "EmailAddress");
            }
        }

        public string SamlCreateRolesAttributeName
        {
            get
            {
                return GetStringConfigItemValue(Constants.ConfigurationItemKeys.SamlCreateRolesAttributeName, "CreateRoles");
            }
        }

        public string SamlUpdateRolesAttributeName
        {
            get
            {
                return GetStringConfigItemValue(Constants.ConfigurationItemKeys.SamlUpdateRolesAttributeName, "UpdateRoles");
            }
        }

        public bool SamlPrintXMLtoDebug
        {
            get
            {
                return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.SamlPrintXMLtoDebug, false);
            }
        }

        public string SamlFederatedLogoutReturnUrl
        {
            get
            {
                return GetStringConfigItemValue(Constants.ConfigurationItemKeys.SamlFederatedLogoutReturnUrl, String.Empty);
            }
        }

        public string SamlDefaultAssistRoles
        {
            get
            {
                return GetStringConfigItemValue(Constants.ConfigurationItemKeys.SamlDefaultAssistRoles, "AssistEmployersReadOnly;AssistJobSeekersReadOnly;AssistJobSeekersAdministrator");
            }
        }

        #endregion

        #endregion

        #region Recaptcha

        /// <summary>
        /// Gets a value indicating whether [recaptcha enabled].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [recaptcha enabled]; otherwise, <c>false</c>.
        /// </value>
        public bool RecaptchaEnabled
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.RecaptchaEnabled, false); }
        }

        /// <summary>
        /// Gets the recaptcha public key.
        /// </summary>
        /// <value>
        /// The recaptcha public key.
        /// </value>
        public string RecaptchaPublicKey
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.RecaptchaPublicKey, ""); }
        }

        /// <summary>
        /// Gets the recaptcha private key.
        /// </summary>
        /// <value>
        /// The recaptcha private key.
        /// </value>
        public string RecaptchaPrivateKey
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.RecaptchaPrivateKey, ""); }
        }

        /// <summary>
        /// Gets a value indicating RecaptchaAuthorizeAfterFirstCheck enabled.
        /// </summary>
        /// <value>
        /// The recaptcha authorize if previoulsy checked.
        /// </summary>
        public bool RecaptchaAuthorizeAfterFirstCheck
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.RecaptchaAuthorizeAfterFirstCheck, false); }
        }

        #endregion

        #region Access Token

        /// <summary>
        /// Gets the key for AccessToken validation
        /// </summary>
        public string AccessTokenKey
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.AccessTokenKey, string.Empty); }
        }

        /// <summary>
        /// Gets the secret code for AccessToken validation
        /// </summary>
        public string AccessTokenSecret
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.AccessTokenSecret, string.Empty); }
        }

        #endregion

        #region Caching

        public CachingMethod CacheMethod
        {
            get
            {
                var value = GetStringConfigItemValue(Constants.ConfigurationItemKeys.CacheMethod, CachingMethod.HTTP.ToString());
                CachingMethod cacheMethod;
                Enum.TryParse(value, true, out cacheMethod);
                return cacheMethod;
            }
            //get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.CacheMethod, CachingMethod.HTTP.to); }
        }

        public string AppFabricCacheName
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.AppFabricCacheName, string.Empty); }
        }

        #endregion

        #region Integration

        public IntegrationClient IntegrationClient
        {
            get { return GetEnumConfigItemValue(Constants.ConfigurationItemKeys.IntegrationClient, IntegrationClient.Standalone); }
            set { }
        }

        public string IntegrationSettings
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.IntegrationSettings, string.Empty); }
        }

        public string IntegrationNodesAndAttributesToObfuscate
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.IntegrationNodesAndAttributesToObfuscate, string.Empty); }
        }

        public ActionTypes[] IntegrationLoggableActions
        {
            get { return GetEnumArrayConfigValue<ActionTypes>(Constants.ConfigurationItemKeys.IntegrationLoggableActions, new[] { ActionTypes.CreateNewResume, ActionTypes.RegisterHowToApply, ActionTypes.SaveJobAlert }); }
        }

        public ActionTypes[] IntegrationOneADayActions
        {
            get { return GetEnumArrayConfigValue<ActionTypes>(Constants.ConfigurationItemKeys.IntegrationOneADayActions, new[] { ActionTypes.CreateNewResume }); }
        }

        #endregion

        #region Posting footer title

        public string PostingFooterTitle
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.PostingFooterTitle, "<strong>Required notice about this job</strong><br/>"); }
        }

        public IntegrationLoggingLevel IntegrationLoggingLevel
        {
            get { return GetEnumConfigItemValue(Constants.ConfigurationItemKeys.IntegrationLoggingLevel, IntegrationLoggingLevel.All); }
        }

        #endregion

        #region Credit check

        /// <summary>
        /// Gets a value indicating whether [show credit check].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [show credit check]; otherwise, <c>false</c>.
        /// </value>
        public bool ShowCreditCheck
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ShowCreditCheck, false); }
            set { }
        }

        /// <summary>
        /// Gets the credit check job posting warning displayed to the job seeker.
        /// </summary>
        /// <value>
        /// The job seeker credit check job posting warning.
        /// </value>
        public string JobSeekerCreditCheckJobPostingWarning
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.JobSeekerCreditCheckJobPostingWarning, "<i><strong>Credit check notice</strong></i></br>The attached job announcement contains a hiring restriction based on credit information. You are not prohibited from applying for this job, even though you may have “bad” credit or a poor credit history. Further, rejecting you for reasons related to credit may violate federal civil rights laws, depending on the circumstances.<br/><br/><a href= '{0}' target='_blank'>Please click for more information</a>"); }
        }

        /// <summary>
        /// Gets the TEGL notice to employers URL.
        /// </summary>
        /// <value>
        /// The TEGL notice to employers URL.
        /// </value>
        public string TEGLNoticeToEmployersURL
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.TEGLNoticeToEmployersURL, "http://wdr.doleta.gov/directives/attach/TEGL/TEGL-11-14_Attachment2_Acc.pdf"); }
        }

        /// <summary>
        /// Gets the TEGL notice to job seekers URL.
        /// </summary>
        /// <value>
        /// The TEGL notice to job seekers URL.
        /// </value>
        public string TEGLNoticeToJobSeekersURL
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.TEGLNoticeToJobSeekersURL, "http://wdr.doleta.gov/directives/attach/TEGL/TEGL-11-14_Attachment3_Acc.pdf"); }
        }

        /// <summary>
        /// Gets the credit check job posting warning.
        /// </summary>
        /// <value>
        /// The credit check job posting warning.
        /// </value>
        public string TalentCreditCheckJobPostingWarning
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.TalentCreditCheckJobPostingWarning, "The attached job announcement contains a hiring restriction based on credit information. You are not prohibited from applying for this job, even though you may have “bad” credit or a poor credit history. Further, rejecting you for reasons related to credit may violate federal civil rights laws, depending on the circumstances.<br/><br/><a href= '{0}' target='_blank'>Please click for more information</a>"); }
        }

        /// <summary>
        /// Gets the assist credit check job referral warning.
        /// </summary>
        /// <value>
        /// The assist credit check job referral warning.
        /// </value>
        public string AssistCreditCheckJobReferralWarning
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.AssistCreditCheckJobPostingWarning, @"<br/><br/><font size=""4""><b>Required notice about this job</b></font><br/><br/>The public workforce system has identified hiring restrictions based on credit history in the job announcement submitted for posting by this employer or in a job announcement referenced in a job bank.<br/><br/>We advise employers not to automatically exclude job seekers based on their credit history unless the employer can show that a credit history restriction is related to the job posted and consistent with the employer’s business needs. While employers are permitted to use credit reports in hiring and other decisions, this type of screening requirement may unjustifiably limit the employment opportunities of applicants in protected groups and may therefore violate federal civil rights laws. Any employer that submits a job announcement containing restrictions or exclusions based on an applicant’s credit information has an opportunity to edit or remove the announcement.<br/><br/>Please take this opportunity to remove or edit the announcement as needed. If you continue to post the announcement as is, the announcement will be posted along with information for job seekers about the civil rights laws that may apply to restrictions based on credit history.<br/><br/><a href= '{0}' target='_blank'>Please click for more information</a>"); }
        }

        /// <summary>
        /// Gets the TEGL notice to staff members URL.
        /// </summary>
        /// <value>
        /// The TEGL notice to staff members URL.
        /// </value>
        public string TEGLNoticeToStaffMembersURL
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.TEGLNoticeToJobSeekersURL, "http://wdr.doleta.gov/directives/attach/TEGL/TEGL-11-14_Attachment2_Acc.pdf"); }
        }

        #endregion

        #region Criminal background check

        /// <summary>
        /// Gets a value indicating whether [show criminal background check].
        /// </summary>
        /// <value>
        /// <c>true</c> if [show criminal background check]; otherwise, <c>false</c>.
        /// </value>
        public bool ShowCriminalBackgroundCheck
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ShowCriminalBackgroundCheck, false); }
        }

        /// <summary>
        /// Gets the talent criminal background job posting warning.
        /// </summary>
        /// <value>
        /// The talent criminal background job posting warning.
        /// </value>
        public string JobSeekerCriminalBackgroundJobPostingWarning
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.JobSeekerCriminalBackgroundJobPostingWarning, "<strong><i>Criminal background check notice</i></strong><br/>Individuals with conviction or arrest histories are not prohibited from applying for this job. The public workforce system has identified criminal record exclusions or restrictions in the attached job announcement. These exclusions or restrictions may be unlawful under certain circumstances. Therefore, the system is providing this notice to job seekers.<br/><br/><a href= '#CRIMINALBACKGROUNDURL#' target='_blank'>Please click for more information</a><br/><br/>"); }
        }

        #endregion

        #region Approval Settings

        /// <summary>
        /// Approval default for Customer-created job posting
        /// </summary>
        public JobApprovalOptions ApprovalDefaultForCustomerCreatedJobs
        {
            get { return GetEnumConfigItemValue(Constants.ConfigurationItemKeys.ApprovalDefaultForCustomerCreatedJobs, JobApprovalOptions.SpecifiedApproval); }
        }

        /// <summary>
        /// Approval check for Customer-created job posting
        /// </summary>
        public JobApprovalCheck[] ApprovalChecksForCustomerCreatedJobs
        {
            get { return GetEnumArrayConfigValue(Constants.ConfigurationItemKeys.ApprovalChecksForCustomerCreatedJobs, new[] { JobApprovalCheck.SpecialRequirements }); }
        }

        /// <summary>
        /// Approval default for Staff-created job posting
        /// </summary>
        public JobApprovalOptions ApprovalDefaultForStaffCreatedJobs
        {
            get { return GetEnumConfigItemValue(Constants.ConfigurationItemKeys.ApprovalDefaultForStaffCreatedJobs, JobApprovalOptions.NoApproval); }
        }

        /// <summary>
        /// Approval defaults for Customer-created job posting
        /// </summary>
        public JobApprovalCheck[] ApprovalChecksForStaffCreatedJobs
        {
            get { return GetEnumArrayConfigValue(Constants.ConfigurationItemKeys.ApprovalChecksForStaffCreatedJobs, new[] { JobApprovalCheck.SpecialRequirements }); }
        }

        ///<summary>
        /// Check whether yellow word filtering should be enabled for staff-created job
        ///</summary>
        public bool ActivateYellowWordFilteringForStaffCreatedJobs
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ActivateYellowWordFilteringForStaffCreatedJobs, false); }
        }

        #endregion

        #endregion

        #region WebConfig Helpers Methods

        /// <summary>
        /// Gets the string web config value.
        /// </summary>
        /// <param name="key">A key.</param>
        /// <returns></returns>
        private string GetStringWebConfigValue(string key)
        {
            var value = (ConfigurationManager.AppSettings[ResolveConfigKeyWithThemeAndModule(key)]
                                     ??
                                     ConfigurationManager.AppSettings[ResolveConfigKeyWithTheme(key)])
                                    ??
                                    ConfigurationManager.AppSettings[key];

            return value;
        }

        /// <summary>
        /// Gets the int web config value.
        /// </summary>
        /// <param name="key">A key.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns></returns>
        private int GetIntWebConfigValue(string key, int defaultValue)
        {
            var stringValue = GetStringWebConfigValue(key);
            return string.IsNullOrEmpty(stringValue) ? defaultValue : Convert.ToInt32(stringValue);
        }

        /// <summary>
        /// Gets the bool web config value.
        /// </summary>
        /// <param name="key">A key.</param>
        /// <param name="defaultValue">if set to <c>true</c> [default value].</param>
        /// <returns></returns>
        private bool GetBoolWebConfigValue(string key, bool defaultValue)
        {
            var stringValue = GetStringWebConfigValue(key);
            return string.IsNullOrEmpty(stringValue) ? defaultValue : Convert.ToBoolean(stringValue);
        }

        /// <summary>
        /// Gets the string web config value.
        /// </summary>
        /// <param name="key">A key.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns></returns>
        private string GetStringWebConfigValue(string key, string defaultValue)
        {
            var stringValue = GetStringWebConfigValue(key);
            return string.IsNullOrEmpty(stringValue) ? defaultValue : stringValue;
        }

        #endregion

        #region ConfigurationItem Helpers

        /// <summary>
        /// Gets the string config item value.
        /// </summary>
        /// <param name="key">A key.</param>
        /// <returns></returns>
        private string GetStringConfigItemValue(string key)
        {
            if (_lastLoadFromCache.AddMinutes((_cacheTimeoutInMinutes)) < DateTime.Now) /* && _configurationHelper.IsNotNull() && _configurationHelper.ConfigurationItems.IsNotNull())*/
            {
                lock (_syncLock)
                {
                    if (_lastLoadFromCache.AddMinutes((_cacheTimeoutInMinutes)) < DateTime.Now)
                    {
                        _configurationHelper.RefreshAppSettings();
                        _configurationItems = _configurationHelper.ConfigurationItems.ToList();
                        _lastLoadFromCache = DateTime.Now;
                    }
                }
            }

            var item = (_configurationItems.FirstOrDefault(ci => ci.Key == ResolveConfigKeyWithThemeAndModule(key))
                                    ??
                                    _configurationItems.FirstOrDefault(ci => ci.Key == ResolveConfigKeyWithTheme(key))
                                    ??
                                    _configurationItems.FirstOrDefault(ci => ci.Key == key));
            return item.IsNotNull() ? item.Value : null;
        }

        /// <summary>
        /// Gets the int config item value.
        /// </summary>C:\Data\BGT\Focus\trunk\Focus.Web\WebTalent\Controls\JobWizardStep6.ascx
        /// <param name="key">A key.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns></returns>
        private int GetIntConfigItemValue(string key, int defaultValue)
        {
            var stringValue = GetStringConfigItemValue(key);
            return string.IsNullOrEmpty(stringValue) ? defaultValue : Convert.ToInt32(stringValue);
        }

        /// <summary>
        /// Gets the long config item value.
        /// </summary>
        /// <param name="key">A key.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns></returns>
        private long GetLongConfigItemValue(string key, long defaultValue)
        {
            var stringValue = GetStringConfigItemValue(key);
            return string.IsNullOrEmpty(stringValue) ? defaultValue : Convert.ToInt64(stringValue);
        }

        /// <summary>
        /// Gets the bool config item  value.
        /// </summary>
        /// <param name="key">A key.</param>
        /// <param name="defaultValue">if set to <c>true</c> [default value].</param>
        /// <returns></returns>
        private bool GetBoolConfigItemValue(string key, bool defaultValue)
        {
            var stringValue = GetStringConfigItemValue(key);
            return string.IsNullOrEmpty(stringValue) ? defaultValue : Convert.ToBoolean(stringValue);
        }

        /// <summary>
        /// Gets the string config item  value.
        /// </summary>
        /// <param name="key">A key.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <param name="emptyStringValid">if set to <c>true</c> [empty string valid].</param>
        /// <returns></returns>
        private string GetStringConfigItemValue(string key, string defaultValue, bool emptyStringValid = false)
        {
            var stringValue = GetStringConfigItemValue(key);

            if (emptyStringValid)
                return stringValue ?? defaultValue;

            return string.IsNullOrEmpty(stringValue) ? defaultValue : stringValue;
        }

        /// <summary>
        /// Gets the int array config item value.
        /// </summary>
        /// <param name="key">A key.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns></returns>
        private int[] GetIntArrayConfigItemValue(string key, int[] defaultValue)
        {
            var stringValue = GetStringConfigItemValue(key);

            if (string.IsNullOrEmpty(stringValue)) return defaultValue;

            var stringArray = stringValue.Split(',');
            var intArray = new int[stringArray.Length];

            for (var i = 0; i < stringArray.Length; i++)
            {
                intArray[i] = int.Parse(stringArray[i]);
            }

            return intArray.Length == 0 ? defaultValue : intArray;
        }

        /// <summary>
        /// Gets a numeric array config item value.
        /// </summary>
        /// <param name="key">A key.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns></returns>
        private T[] GetNumericArrayConfigItemValue<T>(string key, T[] defaultValue)
        {
            var stringValue = GetStringConfigItemValue(key);

            if (string.IsNullOrEmpty(stringValue))
                return defaultValue;

            var stringArray = stringValue.Split(',');
            var array = new T[stringArray.Length];

            for (var i = 0; i < stringArray.Length; i++)
            {
                array[i] = (T)Convert.ChangeType(stringArray[i], typeof(T));
            }

            return array.Length == 0 ? defaultValue : array;
        }

        /// <summary>
        /// Gets the string array config value.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns></returns>
        private string[] GetStringArrayConfigValue(string key, string[] defaultValue)
        {
            var stringValue = GetStringConfigItemValue(key);
            return string.IsNullOrEmpty(stringValue) ? defaultValue : stringValue.Split(',');
        }

        /// <summary>
        /// Gets the enum config value.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key">The key.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns></returns>
        private T GetEnumConfigItemValue<T>(string key, T defaultValue) where T : struct
        {
            var stringValue = GetStringConfigItemValue(key);
            int i;

            if (int.TryParse(stringValue, out i))
                return (T)(object)i;

            T enumValue;

            if (Enum.TryParse(stringValue, true, out enumValue))
                return enumValue;

            return defaultValue;
        }

        /// <summary>
        /// Parses a string value of an Enum
        /// </summary>
        /// <typeparam name="T">The enum type</typeparam>
        /// <param name="stringValue">The string value</param>
        /// <returns>The enum value</returns>
        private T? ParseEnumConfigItemValue<T>(string stringValue) where T : struct
        {
            int i;

            if (int.TryParse(stringValue, out i))
                return (T)(object)i;

            T enumValue;

            if (Enum.TryParse(stringValue, true, out enumValue))
                return enumValue;

            return null;
        }

        /// <summary>
        /// Gets the enum array config value.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="defaultValue">The default value.</param>
        /// <returns></returns>
        private T[] GetEnumArrayConfigValue<T>(string key, T[] defaultValue) where T : struct
        {
            var stringValue = GetStringConfigItemValue(key);

            if (string.IsNullOrEmpty(stringValue))
            {
                return defaultValue;
            }

            return (from s in stringValue.Split(',')
                    let e = ParseEnumConfigItemValue<T>(s)
                    where e != null
                    select e.Value).ToArray();
        }

        #endregion

        #region Helper classes

        /// <summary>
        /// Resolves the config key with theme and module.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        private string ResolveConfigKeyWithThemeAndModule(string key)
        {
            return _configKeyWithThemeAndModule.IsNull() ? null : String.Format(_configKeyWithThemeAndModule, key);
        }

        /// <summary>
        /// Resolves the config key with theme.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns></returns>
        private string ResolveConfigKeyWithTheme(string key)
        {
            return _configKeyWithTheme.IsNull() ? null : String.Format(_configKeyWithTheme, key);
        }

        /// <summary>
        /// Gets the default searchable states based on the nearby states and the default state.
        /// </summary>
        /// <returns></returns>
        private string[] GetDefaultSearchableStates()
        {
            var stateKeys = NearbyStateKeys.ToList();

            if (!stateKeys.Contains(DefaultStateKey))
                stateKeys.Add(DefaultStateKey);

            return stateKeys.ToArray();
        }

        #endregion

        /// <summary>
        /// Get username minimum length from the config settings 
        /// </summary>
        public string UserNameMinLengthPattern
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.UserNameMinLengthRegExPattern, @"(\s*(\S)\s*){6,}"); }
        }

        /// <summary>
        /// Get settings for Edit FEIN visibility
        /// </summary>
        public bool CanEditFEIN
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.EditFEIN, (Theme == FocusThemes.Workforce)); }
        }

        /// <summary>
        /// Supported document file types
        /// </summary>
        public string DocumentFileTypes
        {
            get { return GetStringConfigItemValue(Constants.ConfigurationItemKeys.DocumentFileTypes, "pdf,xls,xlsx"); }
        }

        /// <summary>
        /// Gets a value indicating whether [referral request approval queue office filter type].
        /// </summary>
        /// <value>
        /// <c>true</c> if [referral request approval queue office filter type]; otherwise, <c>false</c>.
        /// </value>
        public bool ReferralRequestApprovalQueueOfficeFilterType
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ReferralRequestApprovalQueueOfficeFilterType, false); }
        }

        #region SQL Jobs

        /// <summary>
        /// Number of days to retain Data.Core.PageState records
        /// </summary>
        public int SessionRetentionDays
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.SessionRetentionDays, 1); }
        }

        /// <summary>
        /// Number of days to retain Data.Core.PageState records
        /// </summary>
        public int PageStateRentionDays
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.PageStateRentionDays, 1); }
        }

        /// <summary>
        /// Number of days to retain Log.LogEvent records
        /// </summary>
        public int LogEventRetentionDays
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.LogEventRetentionDays, 30); }
        }

        /// <summary>
        /// Number of days to retain Log.ProfileEvent records
        /// </summary>
        public int ProfileEventRetentionDays
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.ProfileEventRetentionDays, 30); }
        }

        /// <summary>
        /// Number of days to retain Messaging.Message, and associated, records
        /// </summary>
        public int MessageRetentionDays
        {
            get { return GetIntConfigItemValue(Constants.ConfigurationItemKeys.MessageRetentionDays, 30); }
        }

        #endregion

        #region CompTIA specific

        /// <summary>
        /// Comptia related configuration enabled or not
        /// </summary>
        public bool IsComptiaSpecific
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.IsComptiaSpecific, false); }
        }

        public bool EnableSsnFeatureGroup
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.EnableSsnFeatureGroup, true); }
        }

        public bool EnableDobFeatureGroup
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.EnableDobFeatureGroup, true); }
        }
        public bool EnableNCRCFeatureGroup
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.EnableNCRCFeatureGroup, true); }
        }
        public bool ShowReasonForLeavingFeature
        {
            get { return GetBoolConfigItemValue(Constants.ConfigurationItemKeys.ShowReasonForLeavingFeature, true); }
        }
        #endregion
    }
}