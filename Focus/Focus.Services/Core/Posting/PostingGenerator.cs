﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Xsl;

using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Settings.Interfaces;
using Focus.Services.Helpers;

using Framework.Core;

#endregion

namespace Focus.Services.Core.Posting
{
	public class PostingGenerator
	{
		private readonly long? _jobId;
		private readonly JobDto _job;
		private readonly BusinessUnitDto _businessUnit;
		private readonly BusinessUnitDescriptionDto _businessUnitDescription;
		private readonly List<JobLicenceDto> _jobLicences;
		private readonly List<JobCertificateDto> _jobCertificates;
		private readonly List<JobLanguageDto> _jobLanguages;
		private readonly List<JobProgramOfStudyDto> _jobProgramsOfStudy;
		private readonly List<JobSpecialRequirementDto> _jobSpecialRequirements;
		private readonly List<JobLocationDto> _jobLocations;
		private readonly List<JobDrivingLicenceEndorsementDto> _jobDrivingLicenceEndorsements;
		private readonly ILocalisationHelper _localisationHelper;
		private readonly ILookupHelper _lookupHelper;
		private readonly IAppSettings _appSettings;
		private readonly FocusModules _module;

		/// <summary>
		/// Initializes a new instance of the <see cref="PostingGenerator"/> class.
		/// </summary>
		/// <param name="model">The model.</param>
		/// <param name="culture">The culture.</param>
		/// <param name="appSettings">The app settings.</param>
		/// <param name="localisationHelper">The localisation helper.</param>
		public PostingGenerator(PostingGeneratorModel model, string culture, IAppSettings appSettings, ILocalisationHelper localisationHelper, ILookupHelper lookupHelper)
		{
			_jobId = model.JobId ?? model.Job.Id;
			_job = model.Job;
			_businessUnit = model.BusinessUnit;
			_businessUnitDescription = model.BusinessUnitDescription;
			_jobLicences = model.JobLicences;
			_jobCertificates = model.JobCertificates;
			_jobLanguages = model.JobLanguages;
			_jobProgramsOfStudy = model.JobProgramsOfStudy;
			_jobSpecialRequirements = model.JobSpecialRequirements;
			_jobLocations = model.JobLocations;
			_jobDrivingLicenceEndorsements = model.JobDrivingLicenceEndorsements;
			_localisationHelper = localisationHelper;
			_lookupHelper = lookupHelper;
			_appSettings = appSettings;
			_module = model.Module;
		}

		/// <summary>
		/// Generates the posting html.
		/// </summary>
		public string Generate()
		{
			var postingView = ConvertJobToPostingView();

			//set any properties not required by the posting to null
			postingView.WorkOpportunitiesTaxCreditHires = null;
			//postingView.MinimumAgeRequirement = null;

			var postingXml = postingView.Serialize();

			var xmlDocument = new XmlDocument { PreserveWhitespace = true };
			xmlDocument.LoadXml(postingXml);

			var stringWriter = new StringWriter();
			var xslTransform = new XslCompiledTransform();

			try
			{
				var assembly = Assembly.GetExecutingAssembly();
				var stream = assembly.GetManifestResourceStream(string.Concat("Focus.Services.Assets.Xslts.", _appSettings.JobPostingStylesheet));
				if (stream == null)
					throw new Exception("Unable to get JobPostingStylesheet xslt");

				var reader = new StreamReader(stream);
				var xmlReader = XmlReader.Create(reader);

				xslTransform.Load(xmlReader);

				xmlReader.Close();
				reader.Close();
				stream.Close();

				xslTransform.Transform(xmlDocument.CreateNavigator(), null, stringWriter);
			}
			catch { }

			return stringWriter.ToString();
		}

		/// <summary>
		/// Generates the posting view.
		/// </summary>
		public PostingView GenerateView()
		{
			var postingView = ConvertJobToPostingView(true, true);

			return postingView;
		}

		/// <summary>
		/// Converts the job to posting view.
		/// </summary>
		/// <returns></returns>
		private PostingView ConvertJobToPostingView(bool overrideHideSettings = false, bool overrideHowToApplySetting = false)
		{
			var postingView = new PostingView { JobTitle = (_job.JobTitle.IsNotNullOrEmpty() ? _job.JobTitle : "") };

			#region Title & Company

			if (_job.IsConfidential != null && !_job.IsConfidential.Value)
			{
				postingView.CompanyName = (_businessUnit.IsNotNull() ? _businessUnit.Name : "");

				if (_job.BusinessUnitLogoId.IsNotNull() && _job.BusinessUnitLogoId > 0)
				{
					postingView.EmployerLogoUrl = _appSettings.TalentApplicationPath + "/logo/" + _job.BusinessUnitLogoId;
				}

				if (_businessUnitDescription.IsNotNull() && _businessUnit.IsNotNull())
				{
					postingView.EmployerDescription = _businessUnitDescription.Description;
					//postingView.BusinessUnitDescription = _businessUnitDescription.Description;
					postingView.EmployerDescriptionPostingPosition = _job.EmployerDescriptionPostingPosition;
					postingView.EmployerDescriptionLabel = _localisationHelper.Localise("Posting.EmployerDescription.Label", "About {0}", _businessUnit.Name);
				}
			}

			#endregion

			#region Description

			postingView.JobDescription = (_job.Description.IsNotNullOrEmpty() ? _job.Description : "");

			#endregion

			#region Requirements

			postingView.RequirementsLabel = _localisationHelper.Localise("Posting.Requirements.Label", "Requirements");

			if (!_job.HideEducationOnPosting || overrideHideSettings)
			{
				if (_job.MinimumEducationLevel.IsNotNull() && _job.MinimumEducationLevel.Value != EducationLevels.None)
				{
					var educationLevelLocalised = "";

					// Doing this as a switch so we don't have to ensure the localisation item exists in every database
					switch (_job.MinimumEducationLevel)
					{
						case EducationLevels.NoDiploma:
							educationLevelLocalised = _localisationHelper.GetEnumLocalisedText(_job.MinimumEducationLevel,
																																								"No Diploma");
							break;
						case EducationLevels.HighSchoolDiplomaOrEquivalent:
							educationLevelLocalised = _localisationHelper.GetEnumLocalisedText(_job.MinimumEducationLevel,
																																				"High school diploma or equivalent");
							break;
						case EducationLevels.HighSchoolDiploma:
							educationLevelLocalised = _localisationHelper.GetEnumLocalisedText(_job.MinimumEducationLevel,
																																				"High school diploma/GED or equivalent");
							break;
						case EducationLevels.SomeCollegeNoDegree:
							educationLevelLocalised = _localisationHelper.GetEnumLocalisedText(_job.MinimumEducationLevel,
																																						"Some college, no degree");
							break;
						case EducationLevels.AssociatesDegree:
							educationLevelLocalised = _localisationHelper.GetEnumLocalisedText(_job.MinimumEducationLevel,
																																			"Associate’s or vocational degree");
							break;
						case EducationLevels.GraduateDegree:
							educationLevelLocalised = _localisationHelper.GetEnumLocalisedText(_job.MinimumEducationLevel,
																																				"Masters degree or equivalent");
							break;
						case EducationLevels.BachelorsDegree:
							educationLevelLocalised = _localisationHelper.GetEnumLocalisedText(_job.MinimumEducationLevel,
																																				"Bachelor's degree");
							break;
						case EducationLevels.MastersDegree:
							educationLevelLocalised = _localisationHelper.GetEnumLocalisedText(_job.MinimumEducationLevel,
																																				"Master's degree");
							break;
						case EducationLevels.DoctorateDegree:
							educationLevelLocalised = _localisationHelper.GetEnumLocalisedText(_job.MinimumEducationLevel,
																																					"Doctorate degree");
							break;
					}

					if (_job.MinimumEducationLevelRequired.IsNull() || _job.MinimumEducationLevelRequired.GetValueOrDefault())
						postingView.MinimumEducationLevelRequirement = _localisationHelper.Localise("Posting.EducationRequired.Text", "Applicants must have at least a {0}", educationLevelLocalised);
					else
						postingView.MinimumEducationLevelRequirement = _localisationHelper.Localise("Posting.EducationPreferred.Text", "Applicants preferred to have at least a {0}", educationLevelLocalised);
				}

				if (_job.JobType != JobTypes.Job)
				{
					if (_job.StudentEnrolled.HasValue && _job.StudentEnrolled.Value)
					{
						if (_job.MinimumEducationLevelRequired.IsNull() || _job.MinimumEducationLevelRequired.GetValueOrDefault())
							postingView.StudentEnrolledRequirement =
								_localisationHelper.Localise("Posting.StudentEnrolledRequired.Text",
																						 "Applicants must be currently enrolled in college");
						else
							postingView.StudentEnrolledRequirement =
								_localisationHelper.Localise("Posting.StudentEnrolledPreferred.Text",
																						 "Applicants preferred to be currently enrolled in college");
					}

					if (_job.MinimumCollegeYears.IsNotNull() && _job.MinimumCollegeYears > 0)
					{
						if (_job.MinimumEducationLevelRequired.IsNull() || _job.MinimumEducationLevelRequired.GetValueOrDefault())
							postingView.MinimumCollegeYearsRequirement =
								_localisationHelper.Localise("Posting.CollegeYearsRequired.Text",
																						 "Applicants must have completed at least {0} year(s) of college",
																						 _job.MinimumCollegeYears);
						else
							postingView.MinimumCollegeYearsRequirement =
								_localisationHelper.Localise("Posting.CollegeYearsPreferred.Text",
																						 "Applicants preferred to have completed at least {0} year(s) of college",
																						 _job.MinimumCollegeYears);
					}
				}
			}


			if (!_job.HideExperienceOnPosting || overrideHideSettings)
			{
				#region Experience
				if (_job.MinimumExperience.IsNotNull() || _job.MinimumExperienceMonths.IsNotNull())
				{
					if (_job.MinimumExperienceRequired.IsNull() || _job.MinimumExperienceRequired.GetValueOrDefault())
					{
						// Text differs depending on experience required params
						if (_job.MinimumExperienceMonths.IsNull())
							postingView.MinimumExperienceRequirement = _localisationHelper.Localise("Posting.ExperienceRequiredYears.Text", "Applicants must have at least {0} years experience", _job.MinimumExperience);
						else if (_job.MinimumExperience.IsNull())
							postingView.MinimumExperienceRequirement = _localisationHelper.Localise("Posting.ExperienceRequiredMonths.Text", "Applicants must have at least {0} months experience", _job.MinimumExperienceMonths);
						else
							postingView.MinimumExperienceRequirement = _localisationHelper.Localise("Posting.ExperienceRequired.Text", "Applicants must have at least {0} years {1} months experience", _job.MinimumExperience, _job.MinimumExperienceMonths);
					}
					else
					{
						// Text differs depending on experience required params
						if (_job.MinimumExperienceMonths.IsNull())
							postingView.MinimumExperienceRequirement = _localisationHelper.Localise("Posting.ExperienceRequiredYears.Preferred", "Applicants are preferred to have had at least {0} years experience", _job.MinimumExperience);
						else if (_job.MinimumExperience.IsNull())
							postingView.MinimumExperienceRequirement = _localisationHelper.Localise("Posting.ExperienceRequiredMonths.Preferred", "Applicants are preferred to have had at least {0} months experience", _job.MinimumExperienceMonths);
						else
							postingView.MinimumExperienceRequirement = _localisationHelper.Localise("Posting.ExperienceRequired.Preferred", "Applicants are preferred to have had at least {0} years {1} months experience",
																																						 _job.MinimumExperience, _job.MinimumExperienceMonths);
					}
				}
				#endregion
			}


			if (!_job.HideMinimumAgeOnPosting || overrideHideSettings)
			{
				#region Age

				if (_job.MinimumAge.IsNotNull())
				{
					if (_job.MinimumAgeRequired.IsNull() || _job.MinimumAgeRequired.GetValueOrDefault())
						postingView.MinimumAgeRequirement = _localisationHelper.Localise("Posting.AgeRequired.Text", "Applicants must be at least {0}", _job.MinimumAge);
					else
						postingView.MinimumAgeRequirement = _localisationHelper.Localise("Posting.AgeRequired.Preferred", "Applicants preferred to be at least {0}", _job.MinimumAge);
				}

				#endregion
			}

			if (!_job.HideDriversLicenceOnPosting || overrideHideSettings)
			{
				#region Driving licence

				if (_job.DrivingLicenceClassId.IsNotNull() && _job.DrivingLicenceClassId > 0)
				{
					var drivingLicenceClass = _localisationHelper.GetCodeItemLocalisedValue(_job.DrivingLicenceClassId.GetValueOrDefault());
					var drivingLicenceEndorsements = (_jobDrivingLicenceEndorsements.IsNotNullOrEmpty()) ? DrivingLicenceEndorsementsToString() : "";

					if (_job.DrivingLicenceRequired.IsNull() || _job.DrivingLicenceRequired.GetValueOrDefault())
						postingView.DrivingLicenceRequirement = _localisationHelper.Localise("Posting.DrivingLicence.Text", "Applicants must hold a {0} driving license {1}", drivingLicenceClass, drivingLicenceEndorsements);
					else
						postingView.DrivingLicenceRequirement = _localisationHelper.Localise("Posting.DrivingLicence.Text", "Applicants preferred to hold a {0} driving license {1}", drivingLicenceClass, drivingLicenceEndorsements);
				}

				#endregion
			}

			if (!_job.HideLicencesOnPosting || overrideHideSettings)
			{
				#region Licences
				if (_jobLicences.IsNotNullOrEmpty())
				{
					var licences = _jobLicences.Select(entity => entity.Licence).ToList();

					if (_job.LicencesRequired.IsNull() || _job.LicencesRequired.GetValueOrDefault())
						postingView.LicencesRequirements = _localisationHelper.Localise("Posting.LicencesRequired.Text", "Applicants must hold the occupational license of {0}",
																																	 licences.ToSentence(_localisationHelper.Localise("Global.And.Text", "and")));
					else
						postingView.LicencesRequirements = _localisationHelper.Localise("Posting.LicencesPreferred.Text", "Applicants preferred to hold the occupational license of {0}",
																																	 licences.ToSentence(_localisationHelper.Localise("Global.And.Text", "and")));
				}

				#endregion
			}

			if (!_job.HideCertificationsOnPosting || overrideHideSettings)
			{
				#region Certifications
				if (_jobCertificates.IsNotNullOrEmpty())
				{
					var certificates = _jobCertificates.Select(entity => entity.Certificate).ToList();

					if (_job.CertificationRequired.IsNull() || _job.CertificationRequired.GetValueOrDefault())
						postingView.CertificationRequirements = _localisationHelper.Localise("Posting.CertificationRequired.Text", "Applicants must hold {0}",
																																				certificates.ToSentence(_localisationHelper.Localise("Global.And.Text", "and")));
					else
						postingView.CertificationRequirements = _localisationHelper.Localise("Posting.CertificationPreferred.Text", "Applicants preferred to hold {0}",
																																				certificates.ToSentence(_localisationHelper.Localise("Global.And.Text", "and")));
				}

				#endregion
			}

			if (!_job.HideLanguagesOnPosting || overrideHideSettings)
			{
				#region Languages

				if (_jobLanguages.IsNotNullOrEmpty())
				{
					var languages = _jobLanguages.Select(
						entity => (entity.LanguageProficiencyId != null & entity.LanguageProficiencyId > 0)
							? entity.Language + _localisationHelper.Localise(" Global.At.Text ", " at ") +
								_lookupHelper.GetLookupText(LookupTypes.LanguageProficiencies, (long)entity.LanguageProficiencyId)
									.Split('-')
									.Last() + _localisationHelper.Localise(" Posting.LanguagesPreferredLevel.Text", " level")
							: entity.Language)
						.ToList();

					if (_job.LanguagesRequired.IsNull() || _job.LanguagesRequired.GetValueOrDefault())
						postingView.LanguagesRequirements = _localisationHelper.Localise("Posting.LanguagesRequired.Text", "Applicants are required to have language skills in {0}",
																																		languages.ToSentence(_localisationHelper.Localise("Global.And.Text", "and")));
					else
						postingView.LanguagesRequirements = _localisationHelper.Localise("Posting.LanguagesPreferred.Text", "Applicants are preferred to have language skills in {0}",
																																		languages.ToSentence(_localisationHelper.Localise("Global.And.Text", "and")));
				}

				#endregion
			}

			if (!_job.HideProgramOfStudyOnPosting || overrideHideSettings)
			{
				#region Programs of study
				if (_jobProgramsOfStudy.IsNotNullOrEmpty())
				{
					var programsOfStudy = _jobProgramsOfStudy.Where(p => p.Tier != 2).Select(entity => entity.ProgramOfStudy).ToList();
					var programOfStudySentence = programsOfStudy.ToSentence(_localisationHelper.Localise("Global.And.Text", "and"));

					if (_job.ProgramsOfStudyRequired.IsNull() || _job.ProgramsOfStudyRequired.GetValueOrDefault())
						postingView.ProgramOfStudyRequirements = _localisationHelper.Localise("Posting.ProgramsOfStudyRequired.Text", "Applicants must have studied {0}", programOfStudySentence);
					else
						postingView.ProgramOfStudyRequirements = _localisationHelper.Localise("Posting.ProgramsOfStudyPreferred.Text", "Applicants preferred to have studied {0}", programOfStudySentence);
				}

				#endregion
			}

			if (!_job.HideSpecialRequirementsOnPosting || overrideHideSettings)
			{
				#region Special requirements

				if (_jobSpecialRequirements.IsNotNullOrEmpty())
				{
					foreach (var requirement in _jobSpecialRequirements)
					{
						postingView.SpecialRequirements.Add(_localisationHelper.Localise("Posting.SpecialRequirement.Text", "Applicant must {0}", requirement.Requirement));
					}
				}

				#endregion
			}

			if ((!_job.HideWorkWeekOnPosting || overrideHideSettings) && _job.JobType == JobTypes.Job)
			{
				#region Education work week

				if (_job.WorkWeekId.HasValue && _job.WorkWeekId != 0)
				{
					postingView.WorkWeekLabel = _localisationHelper.Localise("Posting.WorkWeek.Label", "Work week");
					postingView.EducationWorkWeek = _lookupHelper.GetLookupText(LookupTypes.WorkWeeks, (long)_job.WorkWeekId);
				}

				#endregion
			}

			#endregion

			#region Details

			postingView.Location = GetJobLocations();

			if (_job.SuitableForHomeWorker.GetValueOrDefault())
				postingView.HomeBasedWorker = _localisationHelper.Localise("Posting.JobLocationVaries.Label", "Suitable for home-based job seekers");

			//if (_job.JobLocationType == JobLocationTypes.NoFixedLocation)
			//  postingView.NoFixedLocation = _localisationHelper.Localise("Posting.NoFixedLocation.Label", "No fixed location");

			if (_job.FederalContractor.IsNotNull() && _job.FederalContractor.GetValueOrDefault())
			{
				postingView.FederalContractor = _localisationHelper.Localise("Posting.FederalContractor.Text", "Federal contractor position");
				postingView.FederalContractorExpiresOn = (_job.FederalContractorExpiresOn.IsNotNull()) ? _localisationHelper.Localise("Posting.FederalContractorExpiresOn.Text", ", expires on {0}", _job.FederalContractorExpiresOn.GetValueOrDefault().ToShortDateString()) : "";
			}
			else
				postingView.FederalContractor = "";

			if (_job.ForeignLabourCertificationH2A.HasValue && (bool)_job.ForeignLabourCertificationH2A)
				postingView.ForeignLabourCertification =
					_localisationHelper.Localise("Posting.ForeignLabourCertificationH2A.Text",
																				"Foreign labor certiﬁcation H2A agriculture");

			if (_job.ForeignLabourCertificationH2B.HasValue && (bool)_job.ForeignLabourCertificationH2B)
				postingView.ForeignLabourCertification =
					_localisationHelper.Localise("Posting.ForeignLabourCertificationH2B.Text",
																				"Foreign labor certiﬁcation H2B non-agriculture");


			if (_job.ForeignLabourCertificationOther.HasValue && (bool)_job.ForeignLabourCertificationOther)
				postingView.ForeignLabourCertification =
					_localisationHelper.Localise("Posting.ForeignLabourCertificationOther.Text",
																				"Foreign labor certiﬁcation other");

			postingView.CourtOrderedAffirmativeAction = (_job.CourtOrderedAffirmativeAction.IsNotNull() && _job.CourtOrderedAffirmativeAction.GetValueOrDefault()) ? _localisationHelper.Localise("Posting.CourtOrderedAffirmativeAction.Text", "Court-ordered affirmative action") : "";

			postingView.FederalContractor = (_job.FederalContractor.IsNotNull() && _job.FederalContractor.GetValueOrDefault()) ? _localisationHelper.Localise("Posting.FederalContractor.Text", "Federal contractor") : "";

			postingView.WorkOpportunitiesTaxCreditHires = (_job.WorkOpportunitiesTaxCreditHires.IsNotNull() && _job.WorkOpportunitiesTaxCreditHires != WorkOpportunitiesTaxCreditCategories.None) ? _localisationHelper.Localise("Posting.WorkOpportunitiesTaxCreditHires.Text", "Work opportunities tax credit hires include {0}", WorkOpportunitiesTaxCreditHiresToString(_job.WorkOpportunitiesTaxCreditHires.GetValueOrDefault())) : "";

			postingView.PostingFlags = (_job.PostingFlags.IsNotNull() && _job.PostingFlags != JobPostingFlags.None) ? JobPostingFlagsToString(_job.PostingFlags.GetValueOrDefault()) : "";

			#endregion

			#region Salary and Benefits


			if ((!_job.HideSalaryOnPosting.HasValue) || (!_job.HideSalaryOnPosting.GetValueOrDefault() || overrideHideSettings))
			{
				if (_job.MinSalary.IsNotNull() && _job.MaxSalary.IsNotNull() && _job.SalaryFrequencyId.IsNotNull() && _job.SalaryFrequencyId.GetValueOrDefault() > 0)
					postingView.SalaryDetails = _localisationHelper.Localise("Posting.SalaryDetails.Text", @"Salary range ${0} - ${1} {2}", _job.MinSalary.GetValueOrDefault().ToString("F2"), _job.MaxSalary.GetValueOrDefault().ToString("F2"), _localisationHelper.GetCodeItemLocalisedValue(_job.SalaryFrequencyId.GetValueOrDefault()));
				else if (_job.MinSalary.IsNull() && _job.MaxSalary.IsNotNull() && _job.SalaryFrequencyId.IsNotNull() && _job.SalaryFrequencyId.GetValueOrDefault() > 0)
					postingView.SalaryDetails = _localisationHelper.Localise("Posting.SalaryDetails.Text", @"Actual salary ${0} {1}", _job.MaxSalary.GetValueOrDefault().ToString("F2"), _localisationHelper.GetCodeItemLocalisedValue(_job.SalaryFrequencyId.GetValueOrDefault()).ToLower());
				else
					postingView.SalaryDetails = "";

				postingView.OtherSalary = _job.OtherSalary.IsNotNullOrEmpty() && _job.OtherSalary.Trim().Length > 0
					? _localisationHelper.Localise("Posting.OtherSalary", "Other salary: {0}", _job.OtherSalary.Trim())
					: string.Empty;

				string commissionLabel;

				if (_job.IsSalaryAndCommissionBased.IsNotNull() && _job.IsSalaryAndCommissionBased.GetValueOrDefault())
					commissionLabel = _localisationHelper.Localise("Posting.CommissionBased.Text",
																													"This is a salary + commission-based position");
				else if (_job.IsCommissionBased.IsNotNull() && _job.IsCommissionBased.GetValueOrDefault())
					commissionLabel = _localisationHelper.Localise("Posting.CommissionBased.Text",
																													"This is a commission-based position");
				else
					commissionLabel = String.Empty;

				postingView.CommissionLabel = commissionLabel;
			}


			var workdays = (_job.NormalWorkDays.IsNotNull() && _job.NormalWorkDays.GetValueOrDefault() != DaysOfWeek.None)
												? WeekdaysToString(_job.NormalWorkDays.GetValueOrDefault())
												: "";

			if (_job.WorkDaysVary.IsNotNull() && _job.WorkDaysVary.GetValueOrDefault())
			{
				postingView.NormalWorkingDays = (workdays.IsNotNullOrEmpty())
																					? _localisationHelper.Localise("Posting.WorkingDaysVaryBetween.Text",
																																					"Working days vary between {0}", workdays)
																					: _localisationHelper.Localise("Posting.WorkingDaysVary.Text",
																																					"Working days vary");
			}
			else
			{
				postingView.NormalWorkingDays = (workdays.IsNotNullOrEmpty())
																					? _localisationHelper.Localise("Posting.NormalWorkingDays.Text",
																																					"Normal working days: {0}", workdays)
																					: "";
			}



			if (_job.HoursPerWeek.HasValue)
			{
				postingView.HoursPerWeek = (_appSettings.HasMinimumHoursPerWeek && _job.MinimumHoursPerWeek.HasValue)
					? _localisationHelper.Localise("Posting.HoursPerWeekRange.Text", "Between {0} and {1} hours per week", _job.MinimumHoursPerWeek.Value.ToString("#,###.##"), _job.HoursPerWeek.Value.ToString("#,###.##"))
					: _localisationHelper.Localise("Posting.HoursPerWeek.Text", "{0} hours per week", _job.HoursPerWeek.Value.ToString("#,###.##"));
			}
			else
			{
				postingView.HoursPerWeek = "";
			}

			postingView.NormalWorkShifts = (_job.NormalWorkShiftsId.IsNotNull() && _job.NormalWorkShiftsId.GetValueOrDefault() > 0) ? _localisationHelper.Localise("Posting.NormalWorkShifts.Text", "Normal work shifts: {0}", _localisationHelper.GetCodeItemLocalisedValue(_job.NormalWorkShiftsId.GetValueOrDefault())) : "";
			postingView.OvertimeRequired = (_job.OverTimeRequired.IsNotNull() && _job.OverTimeRequired.GetValueOrDefault()) ? _localisationHelper.Localise("Posting.OvertimeRequired.Text", "Overtime is required for this position") : "";
			postingView.JobStatus = (_job.JobStatusId.IsNotNull() && _job.JobStatusId.GetValueOrDefault() > 0) ? _localisationHelper.Localise("Posting.JobStatus.Text", "{0} position", _localisationHelper.GetCodeItemLocalisedValue(_job.JobStatusId.GetValueOrDefault())) : "";

			var employmentStatusText = _job.EmploymentStatusId.IsNotNullOrZero() ? _localisationHelper.GetCodeItemLocalisedValue(_job.EmploymentStatusId.GetValueOrDefault()) : "";
			var jobTypeText = _job.JobTypeId.IsNotNullOrZero() ? _localisationHelper.GetCodeItemLocalisedValue(_job.JobTypeId.GetValueOrDefault()) : "";

			postingView.EmploymentStatus = employmentStatusText.IsNotNullOrEmpty() ? _localisationHelper.Localise("Posting.EmploymentStatus.Text", "{0} position", employmentStatusText) : "";
			postingView.JobType = jobTypeText.IsNotNullOrEmpty() ? _localisationHelper.Localise("Posting.JobType.Text", "{0} position", jobTypeText) : "";

			postingView.HoursText = employmentStatusText.IsNotNullOrEmpty() && jobTypeText.IsNotNullOrEmpty()
				? _localisationHelper.Localise("Posting.Hours.Text", "The hours for this role are {0}", string.Concat(employmentStatusText, ", ", jobTypeText))
				: "";

			if (_job.DisplayBenefits || overrideHideSettings)
				postingView.BenefitsLabel = _localisationHelper.Localise("Posting.SalaryAndBenefits.Label", "Salary and Benefits");
			else
				postingView.BenefitsLabel = _localisationHelper.Localise("Posting.Salary.Label", "Salary");

			if (_job.DisplayBenefits || overrideHideSettings)
			{
				postingView.LeaveBenefits = (_job.LeaveBenefits.IsNotNull() && _job.LeaveBenefits != LeaveBenefits.None) ? _localisationHelper.Localise("Posting.LeaveBenefits.Text", "Leave benefits include {0}", LeaveBenefitsToString(_job.LeaveBenefits.GetValueOrDefault())) : "";
				postingView.RetirementBenefits = (_job.RetirementBenefits.IsNotNull() && _job.RetirementBenefits != RetirementBenefits.None) ? _localisationHelper.Localise("Posting.RetirementBenefits.Text", "Retirement benefits include {0}", RetirementBenefitsToString(_job.RetirementBenefits.GetValueOrDefault())) : "";
				postingView.InsuranceBenefits = (_job.InsuranceBenefits.IsNotNull() && _job.InsuranceBenefits != InsuranceBenefits.None) ? _localisationHelper.Localise("Posting.InsuranceBenefits.Text", "Insurance benefits include {0}", InsuranceBenefitsToString(_job.InsuranceBenefits.GetValueOrDefault())) : "";
				postingView.MiscellaneousBenefits = (_job.MiscellaneousBenefits.IsNotNull() && _job.MiscellaneousBenefits != MiscellaneousBenefits.None) ? _localisationHelper.Localise("Posting.MiscellaneousBenefits.Text", "Miscellaneous benefits include {0}", MiscellaneousBenefitsToString(_job.MiscellaneousBenefits.GetValueOrDefault(), _job.OtherBenefitsDetails)) : "";

				//if (_job.LeaveBenefits == LeaveBenefits.None && _job.RetirementBenefits == RetirementBenefits.None &&
				//		_job.InsuranceBenefits == InsuranceBenefits.None && _job.MiscellaneousBenefits == MiscellaneousBenefits.None && postingView.MiscellaneousBenefits == string.Empty)
				//{
				//	postingView.MiscellaneousBenefits = _localisationHelper.Localise("NoBenefits.Text", "No benefits are offered with this job");
				//}
			}

			#endregion region

			#region Recruitment Information

			postingView.ClosingDate = (_job.ClosingOn.IsNotNull()) ? _localisationHelper.Localise("Posting.ClosingDate.Text", "Application closing date: {0}", _job.ClosingOn.GetValueOrDefault().ToShortDateString()) : "";

			if ((!_job.HideOpeningsOnPosting.HasValue) || (!_job.HideOpeningsOnPosting.GetValueOrDefault() || overrideHideSettings))
				postingView.NumberOfOpenings = (_job.NumberOfOpenings.IsNotNull() && _job.NumberOfOpenings > 0) ? _localisationHelper.Localise("Posting.NumberOfOpenings.Text", "Number of openings: {0}", _job.NumberOfOpenings.GetValueOrDefault()) : "";

			if (!_appSettings.HideInterviewContactPreferences && (_appSettings.ShowPostingHowToApply || overrideHowToApplySetting))
			{
				postingView.HowToApplyLabel = _localisationHelper.Localise("Posting.HowToApply.Label", "How to apply");

				if ((_job.InterviewContactPreferences & ContactMethods.FocusTalent) == ContactMethods.FocusTalent)
				{
					postingView.ApplyThroughFocusCareerLabel = _localisationHelper.Localise("Posting.ApplyThroughFocusCareer.Label",
						"Log in to #FOCUSCAREER# and submit your resume");
					postingView.FocusCareerUrl = _appSettings.CareerApplicationPath;
				}

				if ((_job.InterviewContactPreferences & ContactMethods.Email) == ContactMethods.Email)
				{
					postingView.EmailApplicationLabel = _localisationHelper.Localise("Posting.EmailApplication.Label",
						"Email resume to ");
					postingView.EmailApplicationTo = (_job.InterviewEmailAddress.IsNotNullOrEmpty()) ? _job.InterviewEmailAddress : "";
				}

				if ((_job.InterviewContactPreferences & ContactMethods.Online) == ContactMethods.Online)
				{
					postingView.ApplyOnlineLabel = _localisationHelper.Localise("Posting.ApplyOnline.Label", "Apply online");
					postingView.ApplyOnlineUrl = (_job.InterviewApplicationUrl.IsNotNullOrEmpty()) ? _job.InterviewApplicationUrl : "#";
				}

				if ((_job.InterviewContactPreferences & ContactMethods.Mail) == ContactMethods.Mail)
				{
					postingView.MailApplicationLabel = _localisationHelper.Localise("Posting.MailApplication.Label", "Mail resume to ");
					postingView.MailApplicationToAddress = (_job.InterviewMailAddress.IsNotNullOrEmpty())
						? _job.InterviewMailAddress
						: "";
				}

				if ((_job.InterviewContactPreferences & ContactMethods.Fax) == ContactMethods.Fax)
				{
					postingView.FaxApplicationLabel = _localisationHelper.Localise("Posting.FaxApplication.Label", "Fax resume to ");
					postingView.FaxApplicationToNumber = (_job.InterviewFaxNumber.IsNotNullOrEmpty())
						? Regex.Replace(_job.InterviewFaxNumber, _appSettings.PhoneNumberStrictRegExPattern,
							_appSettings.PhoneNumberFormat)
						: "";
				}

				if ((_job.InterviewContactPreferences & ContactMethods.Telephone) == ContactMethods.Telephone)
				{
					postingView.TelephoneApplicationLabel = _localisationHelper.Localise("Posting.TelephoneApplication.Label",
						"Call for an appointment on ");
					postingView.TelephoneApplicationToNumber = (_job.InterviewPhoneNumber.IsNotNullOrEmpty())
						? Regex.Replace(_job.InterviewPhoneNumber, _appSettings.PhoneNumberStrictRegExPattern,
							_appSettings.PhoneNumberFormat)
						: "";
				}

				if ((_job.InterviewContactPreferences & ContactMethods.InPerson) == ContactMethods.InPerson)
				{
					postingView.InPersonApplicationLabel = _localisationHelper.Localise("Posting.OnPersonApplication.Label",
						"Apply in person at ");
					postingView.InPersonApplicationAddress = (_job.InterviewDirectApplicationDetails.IsNotNullOrEmpty())
						? _job.InterviewDirectApplicationDetails
						: "";
				}

				postingView.OtherApplicationInstructions = (_job.InterviewOtherInstructions.IsNotNullOrEmpty())
					? _job.InterviewOtherInstructions
					: "";
			}

			if (_job.CreditCheckRequired.GetValueOrDefault() || (_job.CriminalBackgroundExclusionRequired.IsNotNull() && (bool)_job.CriminalBackgroundExclusionRequired))
			{
				postingView.RequiredNoticeAboutThisJobLabel = _localisationHelper.Localise("Posting.RequiredNoticeAboutThisJob.Label", "Required notice about this job");
			}

            if (_job.CreditCheckRequired.IsNotNull())
                postingView.CreditCheckRequired = _job.CreditCheckRequired.GetValueOrDefault();

            if (_job.CriminalBackgroundExclusionRequired.IsNotNull())
                postingView.CriminalBackgroundCheckRequired = _job.CriminalBackgroundExclusionRequired.GetValueOrDefault();

			#endregion

			#region NCRC Details

			if ((!_job.HideCareerReadinessOnPosting || overrideHideSettings) && _job.CareerReadinessLevel.IsNotNullOrZero())
			{
				var careerReadinessLevel = _lookupHelper.GetLookupText(LookupTypes.NCRCLevel, _job.CareerReadinessLevel.Value);

				postingView.NCRCDetails = _job.CareerReadinessLevelRequired.GetValueOrDefault(true)
																		? _localisationHelper.Localise("Posting.NCRCDetailsRequired.Text", "Applicants must have a {0} National Career Readiness Certificate", careerReadinessLevel)
																		: _localisationHelper.Localise("Posting.NCRCDetailsRequired.Text", "Applicants preferred to have a {0} National Career Readiness Certificate", careerReadinessLevel);
			}
			else
			{
				postingView.NCRCDetails = "";
			}

			#endregion

			#region Footer

			postingView.PostingReference = string.Format("REF/{0}/{1}", _job.EmployerId, _jobId);

			#endregion

			return postingView;
		}

		/// <summary>
		/// Gets the job locations.
		/// </summary>
		/// <returns></returns>
		private string GetJobLocations()
		{
			var locations = new StringBuilder();
			if (_jobLocations.IsNotNullOrEmpty())
			{
				foreach (var location in _jobLocations)
				{
					var publicTransitAccessibleText = (location.IsPublicTransitAccessible) ? _localisationHelper.Localise("Posting.PublicTransitAccessible.Label", "(public transit accessible)") : "";
					locations.AppendFormat("{0}{1}, ", location.Location, publicTransitAccessibleText);
				}
			}
			else if (_job.JobLocationType == JobLocationTypes.NoFixedLocation)
			{
				locations.AppendFormat("{0}, ", _localisationHelper.Localise("Posting.NoFixedLocation.Label", "No fixed location"));
			}

			var locationsText = locations.ToString();
			if (locationsText.Length > 2) locationsText = locationsText.Substring(0, locationsText.Length - 2);

			return locationsText;
		}

		/// <summary>
		/// Weekdayses to string.
		/// </summary>
		/// <param name="weekdays">The weekdays.</param>
		/// <returns></returns>
		private string WeekdaysToString(DaysOfWeek weekdays)
		{
			var weekdaysList = new List<string>();

			// if weekdays is specified do not need to list individual weekdays
			if ((weekdays & DaysOfWeek.Weekdays) == DaysOfWeek.Weekdays)
			{
				weekdaysList.Add(_localisationHelper.GetEnumLocalisedText(DaysOfWeek.Weekdays, "weekdays"));
			}
			else
			{
				if ((weekdays & DaysOfWeek.Monday) == DaysOfWeek.Monday) weekdaysList.Add(_localisationHelper.GetEnumLocalisedText(DaysOfWeek.Monday, "Monday"));
				if ((weekdays & DaysOfWeek.Tuesday) == DaysOfWeek.Tuesday) weekdaysList.Add(_localisationHelper.GetEnumLocalisedText(DaysOfWeek.Tuesday, "Tuesday"));
				if ((weekdays & DaysOfWeek.Wednesday) == DaysOfWeek.Wednesday) weekdaysList.Add(_localisationHelper.GetEnumLocalisedText(DaysOfWeek.Wednesday, "Wednesday"));
				if ((weekdays & DaysOfWeek.Thursday) == DaysOfWeek.Thursday) weekdaysList.Add(_localisationHelper.GetEnumLocalisedText(DaysOfWeek.Thursday, "Thursday"));
				if ((weekdays & DaysOfWeek.Friday) == DaysOfWeek.Friday) weekdaysList.Add(_localisationHelper.GetEnumLocalisedText(DaysOfWeek.Friday, "Friday"));
			}

			// if weekends is specified do not need to list individual weekend days
			if ((weekdays & DaysOfWeek.Weekends) == DaysOfWeek.Weekends)
			{
				weekdaysList.Add(_localisationHelper.GetEnumLocalisedText(DaysOfWeek.Weekends, "weekends"));
			}
			else
			{
				if ((weekdays & DaysOfWeek.Saturday) == DaysOfWeek.Saturday)
					weekdaysList.Add(_localisationHelper.GetEnumLocalisedText(DaysOfWeek.Saturday, "Saturday"));
				if ((weekdays & DaysOfWeek.Sunday) == DaysOfWeek.Sunday)
					weekdaysList.Add(_localisationHelper.GetEnumLocalisedText(DaysOfWeek.Sunday, "Sunday"));
			}
			return weekdaysList.ToSentence(_localisationHelper.Localise("Global.And.Text", "and"));
		}

		/// <summary>
		/// Leaves the benefits to string.
		/// </summary>
		/// <param name="leaveBenefits">The leave benefits.</param>
		/// <returns></returns>
		private string LeaveBenefitsToString(LeaveBenefits leaveBenefits)
		{
			var leaveBenefitsList = new List<string>();

			if ((leaveBenefits & LeaveBenefits.PaidHolidays) == LeaveBenefits.PaidHolidays) leaveBenefitsList.Add(_localisationHelper.GetEnumLocalisedText(LeaveBenefits.PaidHolidays, "Paid holidays"));
			if ((leaveBenefits & LeaveBenefits.Vacation) == LeaveBenefits.Vacation) leaveBenefitsList.Add(_localisationHelper.GetEnumLocalisedText(LeaveBenefits.Vacation, "Vacation/paid time off"));
			if ((leaveBenefits & LeaveBenefits.LeaveSharing) == LeaveBenefits.LeaveSharing) leaveBenefitsList.Add(_localisationHelper.GetEnumLocalisedText(LeaveBenefits.LeaveSharing, "Leave sharing"));
			if ((leaveBenefits & LeaveBenefits.Sick) == LeaveBenefits.Sick) leaveBenefitsList.Add(_localisationHelper.GetEnumLocalisedText(LeaveBenefits.Sick, "Sick"));
			if ((leaveBenefits & LeaveBenefits.Medical) == LeaveBenefits.Medical) leaveBenefitsList.Add(_localisationHelper.GetEnumLocalisedText(LeaveBenefits.Medical, "Medical"));

			return leaveBenefitsList.ToSentence(_localisationHelper.Localise("Global.And.Text", "and"));
		}

		/// <summary>
		/// Retirements the benefits to string.
		/// </summary>
		/// <param name="retirementBenefits">The retirement benefits.</param>
		/// <returns></returns>
		private string RetirementBenefitsToString(RetirementBenefits retirementBenefits)
		{
			var retirementBenefitsList = new List<string>();

			if ((retirementBenefits & RetirementBenefits.PensionPlan) == RetirementBenefits.PensionPlan) retirementBenefitsList.Add(_localisationHelper.GetEnumLocalisedText(RetirementBenefits.PensionPlan, "Pension plan"));
			if ((retirementBenefits & RetirementBenefits.Four01K) == RetirementBenefits.Four01K) retirementBenefitsList.Add(_localisationHelper.GetEnumLocalisedText(RetirementBenefits.Four01K, "401K"));
			if ((retirementBenefits & RetirementBenefits.ProfitSharing) == RetirementBenefits.ProfitSharing) retirementBenefitsList.Add(_localisationHelper.GetEnumLocalisedText(RetirementBenefits.ProfitSharing, "Profit sharing"));
			if ((retirementBenefits & RetirementBenefits.DeferredCompensation) == RetirementBenefits.DeferredCompensation) retirementBenefitsList.Add(_localisationHelper.GetEnumLocalisedText(RetirementBenefits.DeferredCompensation, "Deferred Compensation"));
			if ((retirementBenefits & RetirementBenefits.Four03BPlan) == RetirementBenefits.Four03BPlan) retirementBenefitsList.Add(_localisationHelper.GetEnumLocalisedText(RetirementBenefits.Four03BPlan, "403B Plan"));


			return retirementBenefitsList.ToSentence(_localisationHelper.Localise("Global.And.Text", "and"));
		}

		/// <summary>
		/// Insurances the benefits to string.
		/// </summary>
		/// <param name="insuranceBenefits">The insurance benefits.</param>
		/// <returns></returns>
		private string InsuranceBenefitsToString(InsuranceBenefits insuranceBenefits)
		{
			var insuranceBenefitsList = new List<string>();

			if ((insuranceBenefits & InsuranceBenefits.Dental) == InsuranceBenefits.Dental) insuranceBenefitsList.Add(_localisationHelper.GetEnumLocalisedText(InsuranceBenefits.Dental, "Dental"));
			if ((insuranceBenefits & InsuranceBenefits.Health) == InsuranceBenefits.Health) insuranceBenefitsList.Add(_localisationHelper.GetEnumLocalisedText(InsuranceBenefits.Health, "Health"));
			if ((insuranceBenefits & InsuranceBenefits.Life) == InsuranceBenefits.Life) insuranceBenefitsList.Add(_localisationHelper.GetEnumLocalisedText(InsuranceBenefits.Life, "Life"));
			if ((insuranceBenefits & InsuranceBenefits.Disability) == InsuranceBenefits.Disability) insuranceBenefitsList.Add(_localisationHelper.GetEnumLocalisedText(InsuranceBenefits.Disability, "Disability"));
			if ((insuranceBenefits & InsuranceBenefits.HealthSavings) == InsuranceBenefits.HealthSavings) insuranceBenefitsList.Add(_localisationHelper.GetEnumLocalisedText(InsuranceBenefits.HealthSavings, "Health savings"));
			if ((insuranceBenefits & InsuranceBenefits.Vision) == InsuranceBenefits.Vision) insuranceBenefitsList.Add(_localisationHelper.GetEnumLocalisedText(InsuranceBenefits.Vision, "Vision"));
			if ((insuranceBenefits & InsuranceBenefits.DomesticPartnerCoverage) == InsuranceBenefits.DomesticPartnerCoverage) insuranceBenefitsList.Add(_localisationHelper.GetEnumLocalisedText(InsuranceBenefits.DomesticPartnerCoverage, "Domestic Partner Coverage"));
			return insuranceBenefitsList.ToSentence(_localisationHelper.Localise("Global.And.Text", "and"));
		}

		/// <summary>
		/// Miscellaneouses the benefits to string.
		/// </summary>
		/// <param name="miscellaneousBenefits">The miscellaneous benefits.</param>
		/// <param name="otherBenefits">The other benefits.</param>
		/// <returns></returns>
		private string MiscellaneousBenefitsToString(MiscellaneousBenefits miscellaneousBenefits, string otherBenefits)
		{
			var miscellaneousBenefitsList = new List<string>();

			if ((miscellaneousBenefits & MiscellaneousBenefits.BenefitsNegotiable) == MiscellaneousBenefits.BenefitsNegotiable) miscellaneousBenefitsList.Add(_localisationHelper.GetEnumLocalisedText(MiscellaneousBenefits.BenefitsNegotiable, "Benefits negotiable"));
			if ((miscellaneousBenefits & MiscellaneousBenefits.TuitionAssistance) == MiscellaneousBenefits.TuitionAssistance) miscellaneousBenefitsList.Add(_localisationHelper.GetEnumLocalisedText(MiscellaneousBenefits.TuitionAssistance, "Tuition assistance"));
			if ((miscellaneousBenefits & MiscellaneousBenefits.ClothingAllowance) == MiscellaneousBenefits.ClothingAllowance) miscellaneousBenefitsList.Add(_localisationHelper.GetEnumLocalisedText(MiscellaneousBenefits.ClothingAllowance, "Clothing/Uniform"));
			if ((miscellaneousBenefits & MiscellaneousBenefits.ChildCare) == MiscellaneousBenefits.ChildCare) miscellaneousBenefitsList.Add(_localisationHelper.GetEnumLocalisedText(MiscellaneousBenefits.ChildCare, "Child care"));
			if ((miscellaneousBenefits & MiscellaneousBenefits.Relocation) == MiscellaneousBenefits.Relocation) miscellaneousBenefitsList.Add(_localisationHelper.GetEnumLocalisedText(MiscellaneousBenefits.Relocation, "Relocation"));
			if ((miscellaneousBenefits & MiscellaneousBenefits.Other) == MiscellaneousBenefits.Other) miscellaneousBenefitsList.Add(otherBenefits);
			return miscellaneousBenefitsList.ToSentence(_localisationHelper.Localise("Global.And.Text", "and"));
		}

		/// <summary>
		/// Works the opportunities tax credit hires to string.
		/// </summary>
		/// <param name="wotcHires">The wotc hires.</param>
		/// <returns></returns>
		private string WorkOpportunitiesTaxCreditHiresToString(WorkOpportunitiesTaxCreditCategories wotcHires)
		{
			var wotcHiresList = new List<string>();

			if ((wotcHires & WorkOpportunitiesTaxCreditCategories.TemporaryAssistanceToNeedyFamilyRecipients) == WorkOpportunitiesTaxCreditCategories.TemporaryAssistanceToNeedyFamilyRecipients) wotcHiresList.Add(_localisationHelper.GetEnumLocalisedText(WorkOpportunitiesTaxCreditCategories.TemporaryAssistanceToNeedyFamilyRecipients, "TANF recipients"));
			if ((wotcHires & WorkOpportunitiesTaxCreditCategories.DesignatedCommunityResidents) == WorkOpportunitiesTaxCreditCategories.DesignatedCommunityResidents) wotcHiresList.Add(_localisationHelper.GetEnumLocalisedText(WorkOpportunitiesTaxCreditCategories.DesignatedCommunityResidents, "EZ and RRC"));
			if ((wotcHires & WorkOpportunitiesTaxCreditCategories.SNAPRecipients) == WorkOpportunitiesTaxCreditCategories.SNAPRecipients) wotcHiresList.Add(_localisationHelper.GetEnumLocalisedText(WorkOpportunitiesTaxCreditCategories.SNAPRecipients, "SNAP recipients"));
			if ((wotcHires & WorkOpportunitiesTaxCreditCategories.Veterans) == WorkOpportunitiesTaxCreditCategories.Veterans) wotcHiresList.Add(_localisationHelper.GetEnumLocalisedText(WorkOpportunitiesTaxCreditCategories.Veterans, "Veterans"));
			if ((wotcHires & WorkOpportunitiesTaxCreditCategories.VocationalRehabilitation) == WorkOpportunitiesTaxCreditCategories.VocationalRehabilitation) wotcHiresList.Add(_localisationHelper.GetEnumLocalisedText(WorkOpportunitiesTaxCreditCategories.VocationalRehabilitation, "Vocational rehabilitation"));
			if ((wotcHires & WorkOpportunitiesTaxCreditCategories.SupplementSecurityIncomeRecipients) == WorkOpportunitiesTaxCreditCategories.SupplementSecurityIncomeRecipients) wotcHiresList.Add(_localisationHelper.GetEnumLocalisedText(WorkOpportunitiesTaxCreditCategories.SupplementSecurityIncomeRecipients, "SSI recipients"));
			if ((wotcHires & WorkOpportunitiesTaxCreditCategories.ExFelons) == WorkOpportunitiesTaxCreditCategories.ExFelons) wotcHiresList.Add(_localisationHelper.GetEnumLocalisedText(WorkOpportunitiesTaxCreditCategories.ExFelons, "Ex-felons"));
			if ((wotcHires & WorkOpportunitiesTaxCreditCategories.SummerYouth) == WorkOpportunitiesTaxCreditCategories.SummerYouth) wotcHiresList.Add(_localisationHelper.GetEnumLocalisedText(WorkOpportunitiesTaxCreditCategories.SummerYouth, "Summer youth"));
			if ((wotcHires & WorkOpportunitiesTaxCreditCategories.LongTermFamilyAssistanceRecipient) == WorkOpportunitiesTaxCreditCategories.LongTermFamilyAssistanceRecipient) wotcHiresList.Add(_localisationHelper.GetEnumLocalisedText(WorkOpportunitiesTaxCreditCategories.LongTermFamilyAssistanceRecipient, "Long term family assistance recipients"));

			return wotcHiresList.ToSentence(_localisationHelper.Localise("Global.And.Text", "and"));
		}

		/// <summary>
		/// Drivings the licence endorsements to string.
		/// </summary>
		/// <returns></returns>
		private string DrivingLicenceEndorsementsToString()
		{
			var endorsementsList = _jobDrivingLicenceEndorsements.Select(endorsement => _lookupHelper.GetLookupText(LookupTypes.DrivingLicenceEndorsements, endorsement.DrivingLicenceEndorsementId)).Where(lookupText => lookupText.IsNotNullOrEmpty()).ToList();

			var endorsementsSentence = endorsementsList.ToSentence(_localisationHelper.Localise("Global.And.Text", "and"));

			return endorsementsSentence == "" ? endorsementsSentence : _localisationHelper.Localise("Posting.DrivingLicenceEndorsements.Text", "with {0} endorsements", endorsementsSentence);
		}

		/// <summary>
		/// Jobs the posting flags to string.
		/// </summary>
		/// <param name="postingFlags">The posting flags.</param>
		/// <returns></returns>
		private string JobPostingFlagsToString(JobPostingFlags postingFlags)
		{
			var postingFlagsList = new List<string>();

			if ((postingFlags & JobPostingFlags.HealthcareEmployer) == JobPostingFlags.HealthcareEmployer) postingFlagsList.Add(_localisationHelper.GetEnumLocalisedText(JobPostingFlags.HealthcareEmployer, "Healthcare"));
			if ((postingFlags & JobPostingFlags.TechnologyEmployer) == JobPostingFlags.TechnologyEmployer) postingFlagsList.Add(_localisationHelper.GetEnumLocalisedText(JobPostingFlags.TechnologyEmployer, "Information Technology"));
			if ((postingFlags & JobPostingFlags.AdvancedManufacturingEmployer) == JobPostingFlags.AdvancedManufacturingEmployer) postingFlagsList.Add(_localisationHelper.GetEnumLocalisedText(JobPostingFlags.AdvancedManufacturingEmployer, "Advanced manufacturing"));
			if ((postingFlags & JobPostingFlags.TransportationEmployer) == JobPostingFlags.TransportationEmployer) postingFlagsList.Add(_localisationHelper.GetEnumLocalisedText(JobPostingFlags.TransportationEmployer, "Transportation & distribution"));
			if ((postingFlags & JobPostingFlags.BiotechEmployer) == JobPostingFlags.BiotechEmployer) postingFlagsList.Add(_localisationHelper.GetEnumLocalisedText(JobPostingFlags.BiotechEmployer, "Bio Technology"));
			if ((postingFlags & JobPostingFlags.GreenJob) == JobPostingFlags.GreenJob) postingFlagsList.Add(_localisationHelper.GetEnumLocalisedText(JobPostingFlags.GreenJob, "Green"));
			if ((postingFlags & JobPostingFlags.FinancialServicesEmployer) == JobPostingFlags.FinancialServicesEmployer) postingFlagsList.Add(_localisationHelper.GetEnumLocalisedText(JobPostingFlags.FinancialServicesEmployer, "Financial services"));
			if ((postingFlags & JobPostingFlags.ITEmployer) == JobPostingFlags.ITEmployer) postingFlagsList.Add(_localisationHelper.GetEnumLocalisedText(JobPostingFlags.ITEmployer, "IT employer"));
			if ((postingFlags & JobPostingFlags.Aerospace) == JobPostingFlags.Aerospace) postingFlagsList.Add(_localisationHelper.GetEnumLocalisedText(JobPostingFlags.Aerospace, "Aerospace"));
			if ((postingFlags & JobPostingFlags.BusinessServices) == JobPostingFlags.BusinessServices) postingFlagsList.Add(_localisationHelper.GetEnumLocalisedText(JobPostingFlags.BusinessServices, "Business Services"));
			if ((postingFlags & JobPostingFlags.Energy) == JobPostingFlags.Energy) postingFlagsList.Add(_localisationHelper.GetEnumLocalisedText(JobPostingFlags.Energy, "Energy"));
			if ((postingFlags & JobPostingFlags.HealthInformatics) == JobPostingFlags.HealthInformatics) postingFlagsList.Add(_localisationHelper.GetEnumLocalisedText(JobPostingFlags.HealthInformatics, "Health Informatics"));
			if ((postingFlags & JobPostingFlags.SMART) == JobPostingFlags.SMART) postingFlagsList.Add(_localisationHelper.GetEnumLocalisedText(JobPostingFlags.SMART, "SMART"));
			if ((postingFlags & JobPostingFlags.ResearchandDevelopment) == JobPostingFlags.ResearchandDevelopment) postingFlagsList.Add(_localisationHelper.GetEnumLocalisedText(JobPostingFlags.ResearchandDevelopment, "Research and Development"));
			if ((postingFlags & JobPostingFlags.Consulting) == JobPostingFlags.Consulting) postingFlagsList.Add(_localisationHelper.GetEnumLocalisedText(JobPostingFlags.Consulting, "Consulting"));

			return postingFlagsList.ToSentence(_localisationHelper.Localise("Global.And.Text", "and"));
		}
	}

	[Serializable]
	public class PostingView
	{
		public string EmployerLogoUrl { get; set; }
		public string JobTitle { get; set; }
		public string CompanyName { get; set; }
		public string Location { get; set; }
		public string HomeBasedWorker { get; set; }
		public string NoFixedLocation { get; set; }
		public string JobDescription { get; set; }
		public EmployerDescriptionPostingPositions EmployerDescriptionPostingPosition { get; set; }
		public string BusinessUnitDescription { get; set; }
		public string EmployerDescriptionLabel { get; set; }
		public string EmployerDescription { get; set; }
		public string RequirementsLabel { get; set; }
		public string WorkWeekLabel { get; set; }
		public string MinimumEducationLevelRequirement { get; set; }
		public string MinimumCollegeYearsRequirement { get; set; }
		public string MinimumExperienceRequirement { get; set; }
		public string StudentEnrolledRequirement { get; set; }
		public string MinimumAgeRequirement { get; set; }
		public string DrivingLicenceRequirement { get; set; }
		public string LicencesRequirements { get; set; }
		public string CertificationRequirements { get; set; }
		public string LanguagesRequirements { get; set; }
		public string ProgramOfStudyRequirements { get; set; }
		public List<string> SpecialRequirements { get; set; }
		public string ClosingDate { get; set; }
		public string NumberOfOpenings { get; set; }
		public string HowToApplyLabel { get; set; }
		public string HoursText { get; set; }
		public string ApplyThroughFocusCareerLabel { get; set; }
		public string FocusCareerUrl { get; set; }
		public string EmailApplicationLabel { get; set; }
		public string EmailApplicationTo { get; set; }
		public string ApplyOnlineLabel { get; set; }
		public string ApplyOnlineUrl { get; set; }
		public string MailApplicationLabel { get; set; }
		public string MailApplicationToAddress { get; set; }
		public string FaxApplicationLabel { get; set; }
		public string FaxApplicationToNumber { get; set; }
		public string TelephoneApplicationLabel { get; set; }
		public string TelephoneApplicationToNumber { get; set; }
		public string InPersonApplicationLabel { get; set; }
		public string InPersonApplicationAddress { get; set; }
		public string OtherApplicationInstructions { get; set; }
		public string PostingReference { get; set; }
		public string SalaryDetails { get; set; }
		public string OtherSalary { get; set; }
		public string CommissionLabel { get; set; }
		public string NormalWorkingDays { get; set; }
		public string EmploymentStatus { get; set; }
		public string HoursPerWeek { get; set; }
		public string NormalWorkShifts { get; set; }
		public string OvertimeRequired { get; set; }
		public string JobType { get; set; }
		public string JobStatus { get; set; }
		public string BenefitsLabel { get; set; }
		public string LeaveBenefits { get; set; }
		public string RetirementBenefits { get; set; }
		public string InsuranceBenefits { get; set; }
		public string MiscellaneousBenefits { get; set; }
		public string FederalContractor { get; set; }
		public string FederalContractorExpiresOn { get; set; }
		public string ForeignLabourCertification { get; set; }
		public string CourtOrderedAffirmativeAction { get; set; }
		public string WorkOpportunitiesTaxCreditHires { get; set; }
		public string PostingFlags { get; set; }
		public string EducationWorkWeek { get; set; }
		public string NCRCDetails { get; set; }
		public string RequiredNoticeAboutThisJobLabel { get; set; }
		public bool? CreditCheckRequired { get; set; }
		public bool? CriminalBackgroundCheckRequired { get; set; }

		public PostingView()
		{
			SpecialRequirements = new List<string>();
		}
	}

	[Serializable]
	public class PostingGeneratorModel
	{
		public long? JobId { get; set; }
		public JobDto Job { get; set; }
		public BusinessUnitDto BusinessUnit { get; set; }
		public BusinessUnitDescriptionDto BusinessUnitDescription { get; set; }
		public List<JobLicenceDto> JobLicences { get; set; }
		public List<JobCertificateDto> JobCertificates { get; set; }
		public List<JobLanguageDto> JobLanguages { get; set; }
		public List<JobSpecialRequirementDto> JobSpecialRequirements { get; set; }
		public List<JobLocationDto> JobLocations { get; set; }
		public List<JobProgramOfStudyDto> JobProgramsOfStudy { get; set; }
		public List<JobDrivingLicenceEndorsementDto> JobDrivingLicenceEndorsements { get; set; }
		public FocusModules Module { get; set; }
	}
}
