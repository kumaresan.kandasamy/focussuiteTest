﻿using System;

using Framework.Logging;

namespace Focus.Services.Core
{
	internal class LogData : ILogData
	{
		/// <summary>
		/// Gets or sets the session id.
		/// </summary>
		/// <value>The session id.</value>
		public Guid SessionId { get; set; }

		/// <summary>
		/// Gets or sets the request id.
		/// </summary>
		/// <value>The request id.</value>
		public Guid RequestId { get; set; }

		/// <summary>
		/// Gets or sets the user id.
		/// </summary>
		/// <value>The user id.</value>
		public long UserId { get; set; }
	}
}
