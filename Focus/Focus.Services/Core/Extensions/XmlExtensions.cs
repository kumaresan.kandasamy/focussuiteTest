﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using Framework.Core;
using Framework.DataAccess;
using Newtonsoft.Json.Linq;

#endregion

namespace Focus.Services.Core.Extensions
{
	internal static class XmlExtensions
	{
		#region System.Xml Extensions

		/// <summary>
		/// Creates the element.
		/// </summary>
		/// <param name="parentNode">The parent node.</param>
		/// <param name="name">The name.</param>
		/// <returns></returns>
		public static XmlNode CreateElement(this XmlNode parentNode, string name)
		{
			if (parentNode.OwnerDocument == null)
				return null;

			XmlNode childNode = parentNode.OwnerDocument.CreateElement(name);
			parentNode.AppendChild(childNode);
			return childNode;
		}

		public static XmlNode CreateAttribute(this XmlNode parentNode, string name)
		{
			if (parentNode.OwnerDocument == null)
				return null;

			var attribute = parentNode.OwnerDocument.CreateAttribute(name);
			if (parentNode.Attributes != null) parentNode.Attributes.SetNamedItem(attribute);
			return attribute;
		}

		/// <summary>
		/// Gets or creates a new Xml element.
		/// </summary>
		/// <param name="parentNode">The parent node.</param>
		/// <param name="name">The name.</param>
		/// <returns></returns>
		public static XmlNode GetOrCreateElement(this XmlNode parentNode, string name)
		{
			return GetOrCreateElement(parentNode, name, name);
		}

		/// <summary>
		/// Gets or creates a new Xml element.
		/// </summary>
		/// <param name="parentNode">The parent node.</param>
		/// <param name="xpath">The xpath query to get the current node.</param>
		/// <param name="name">The name.</param>
		/// <returns></returns>
		public static XmlNode GetOrCreateElement(this XmlNode parentNode, string xpath, string name)
		{
			if (parentNode.OwnerDocument == null)
				throw new Exception("GetOrCreateElement: parentNode OwnerDocument is null. xpath = [" + xpath + "], name = [" + name + "]");

			var childNode = parentNode.SelectSingleNode(xpath);

			if (childNode == null)
			{
				if (parentNode.OwnerDocument != null) childNode = parentNode.OwnerDocument.CreateElement(name);
				if (childNode != null) parentNode.AppendChild(childNode);
			}

			return childNode;
		}

		/// <summary>
		/// Removes all child nodes that match the xpath.
		/// </summary>
		/// <param name="parentNode">The parent node.</param>
		/// <param name="xpath">The xpath.</param>
		public static void RemoveAllChildNodes(this XmlNode parentNode, string xpath)
		{
			// Delete all child nodes using the xpath
			var childNodes = parentNode.SelectNodes(xpath);
			if (childNodes != null && childNodes.Count > 0)
				for (var i = 0; i < childNodes.Count; i++)
					parentNode.RemoveChild(childNodes[i]);
		}

		/// <summary>
		/// Gets the element value as string.
		/// </summary>
		/// <param name="parentNode">The parent node.</param>
		/// <param name="xpath">The xpath.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <returns></returns>
		public static string AsString(this XmlNode parentNode, string xpath, string defaultValue)
		{
			var childNode = parentNode.SelectSingleNode(xpath);
			return (childNode == null ? defaultValue : childNode.InnerText);
		}

		/// <summary>
		/// Gets the element value as string.
		/// </summary>
		/// <param name="parentNode">The parent node.</param>
		/// <param name="xpath">The xpath.</param>
		/// <returns></returns>
		public static string AsString(this XmlNode parentNode, string xpath)
		{
			return AsString(parentNode, xpath, String.Empty);
		}

		/// <summary>
		/// Gets the element value as int.
		/// </summary>
		/// <param name="parentNode">The parent node.</param>
		/// <param name="xpath">The xpath.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <returns></returns>
		public static int AsInt32(this XmlNode parentNode, string xpath, int defaultValue)
		{
			int i;
			return (int.TryParse(AsString(parentNode, xpath, defaultValue.ToString()), out i) ? i : defaultValue);
		}

		/// <summary>
		/// Gets the element value as int.
		/// </summary>
		/// <param name="parentNode">The parent node.</param>
		/// <param name="xpath">The xpath.</param>
		/// <returns></returns>
		public static int AsInt32(this XmlNode parentNode, string xpath)
		{
			return AsInt32(parentNode, xpath, 0);
		}

		/// <summary>
		/// Gets the element value as date time.
		/// </summary>
		/// <param name="parentNode">The parent node.</param>
		/// <param name="xpath">The xpath.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <returns></returns>
		public static DateTime AsDateTime(this XmlNode parentNode, string xpath, DateTime defaultValue)
		{
			var dt = defaultValue;

			try { dt = DateTime.ParseExact(parentNode.AsString(xpath, defaultValue.ToString("MM/dd/yyyy")), "MM/dd/yyyy", CultureInfo.InvariantCulture); }
			catch { }

			return dt;
		}

		/// <summary>
		/// Gets the element value as date time.
		/// </summary>
		/// <param name="parentNode">The parent node.</param>
		/// <param name="xpath">The xpath.</param>
		/// <returns></returns>
		public static DateTime AsDateTime(this XmlNode parentNode, string xpath)
		{
			return AsDateTime(parentNode, xpath, DateTime.MinValue);
		}

		/// <summary>
		/// Gets the element value as decimal.
		/// </summary>
		/// <param name="parentNode">The parent node.</param>
		/// <param name="xpath">The xpath.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <returns></returns>
		public static decimal AsDecimal(this XmlNode parentNode, string xpath, Decimal defaultValue)
		{
			decimal d;
			return (decimal.TryParse(AsString(parentNode, xpath, defaultValue.ToString()), out (d)) ? d : defaultValue);
		}

		/// <summary>
		/// Gets the element value as decimal.
		/// </summary>
		/// <param name="parentNode">The parent node.</param>
		/// <param name="xpath">The xpath.</param>
		/// <returns></returns>
		public static decimal AsDecimal(this XmlNode parentNode, string xpath)
		{
			return AsDecimal(parentNode, xpath, 0M);
		}

		/// <summary>
		/// Gets the first element with a value in the list as a string.
		/// </summary>
		/// <param name="nodes">The nodes.</param>
		/// <returns></returns>
		public static string FirstAsString(this XmlNodeList nodes)
		{
			return FirstAsString(nodes, String.Empty);
		}

		/// <summary>
		/// Gets the first element with a value in the list as a string.
		/// </summary>
		/// <param name="nodes">The nodes.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <returns></returns>
		public static string FirstAsString(this XmlNodeList nodes, string defaultValue)
		{
			if (nodes.IsNotNull())
			{
				foreach (var value in nodes.Cast<XmlNode>().Select(node => node.InnerText).Where(value => value.IsNotNullOrEmpty()))
				{
					return value;
				}
			}

			return defaultValue;
		}

		/// <summary>
		/// Gets the first element with a value in the list as int32.
		/// </summary>
		/// <param name="nodes">The nodes.</param>
		/// <returns></returns>
		public static int FirstAsInt32(this XmlNodeList nodes)
		{
			return FirstAsInt32(nodes, 0);
		}

		/// <summary>
		/// Gets the first element with a value in the list as int32.
		/// </summary>
		/// <param name="nodes">The nodes.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <returns></returns>
		public static int FirstAsInt32(this XmlNodeList nodes, int defaultValue)
		{
			int i;
			return (int.TryParse(FirstAsString(nodes, defaultValue.ToString()), out i) ? i : defaultValue);
		}

		/// <summary>
		/// Gets the first element with a value in the list as a nullable decimal.
		/// </summary>
		/// <param name="nodes">The nodes.</param>
		/// <returns></returns>
		public static decimal? FirstAsNullableDecimal(this XmlNodeList nodes)
		{
			decimal d;
			return (decimal.TryParse(FirstAsString(nodes, null), out d) ? d : (decimal?)null);
		}

		/// <summary>
		/// Gets the first element with a value in the list as a nullable int32.
		/// </summary>
		/// <param name="nodes">The nodes.</param>
		/// <returns></returns>
		public static int? FirstAsNullableInt32(this XmlNodeList nodes)
		{
			int i;
			return (int.TryParse(FirstAsString(nodes, null), out i) ? i : (int?)null);
		}

		/// <summary>
		/// Gets the first element with a value in the list as a nullable double.
		/// </summary>
		/// <param name="nodes">The nodes.</param>
		/// <returns></returns>
		public static double? FirstAsNullableDouble(this XmlNodeList nodes)
		{
			double d;
			return (double.TryParse(FirstAsString(nodes, null), out d) ? d : (double?)null);
		}

		/// <summary>
		/// Gets all the values from the elements in the list as a string.
		/// </summary>
		/// <param name="nodes">The nodes.</param>
		/// <param name="joiningString">The joining string.</param>
		/// <returns></returns>
		public static string AsString(this XmlNodeList nodes, string joiningString)
		{
			return AsString(nodes, joiningString, String.Empty);
		}

		/// <summary>
		/// Gets all the values from the elements in the list as a string.
		/// </summary>
		/// <param name="nodes">The nodes.</param>
		/// <param name="joiningString">The joining string.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <returns></returns>
		public static string AsString(this XmlNodeList nodes, string joiningString, string defaultValue)
		{
			var result = new StringBuilder("");

			foreach (XmlNode node in nodes.Cast<XmlNode>().Where(node => node.InnerText.IsNotNullOrEmpty()))
			{
				result.AppendFormat("{0}{1}", node.InnerText, joiningString);
			}

			return (result.Length > joiningString.Length) ? result.ToString(0, result.Length - joiningString.Length) : defaultValue;
		}

		/// <summary>
		/// Gets the first attribute with a value in the list as a string.
		/// </summary>
		/// <param name="nodes">The nodes.</param>
		/// <param name="attributeName">Name of the attribute.</param>
		/// <returns></returns>
		public static string FirstAttributeAsString(this XmlNodeList nodes, string attributeName)
		{
			return FirstAttributeAsString(nodes, attributeName, String.Empty);
		}

		/// <summary>
		/// Gets the first attribute with a value in the list as a string.
		/// </summary>
		/// <param name="nodes">The nodes.</param>
		/// <param name="attributeName">Name of the attribute.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <returns></returns>
		public static string FirstAttributeAsString(this XmlNodeList nodes, string attributeName, string defaultValue)
		{
			if (nodes.IsNotNull())
			{
				foreach (var node in nodes.Cast<XmlNode>().Where(node => node.Attributes != null && (node.Attributes[attributeName].IsNotNull() && node.Attributes[attributeName].InnerText.IsNotNullOrEmpty())).Where(node => node.Attributes != null))
				{
					return node.Attributes[attributeName].InnerText;
				}
			}

			return defaultValue;
		}

		/// <summary>
		/// Gets the attribute as string.
		/// </summary>
		/// <param name="node">The node.</param>
		/// <param name="attributeName">Name of the attribute.</param>
		/// <returns></returns>
		public static string AttributeAsString(this XmlNode node, string attributeName)
		{
			return AttributeAsString(node, attributeName, String.Empty);
		}

		/// <summary>
		/// Gets the attribute as string.
		/// </summary>
		/// <param name="node">The node.</param>
		/// <param name="attributeName">Name of the attribute.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <returns></returns>
		public static string AttributeAsString(this XmlNode node, string attributeName, string defaultValue)
		{
			return (node.IsNotNull() && node.Attributes.IsNotNull() && node.Attributes[attributeName].IsNotNull() && node.Attributes[attributeName].InnerText.IsNotNullOrEmpty())
							? node.Attributes[attributeName].InnerText
							: defaultValue;
		}

		/// <summary>
		/// Gets the values of the nodes as a list.
		/// </summary>
		/// <param name="nodes">The nodes.</param>
		/// <returns></returns>
		public static List<string> AsStringList(this XmlNodeList nodes)
		{
			if (nodes.IsNull() || nodes.Count == 0) return null;

			var valuesList = (from XmlNode node in nodes select node.InnerText).ToList();

			return valuesList;
		}

		#endregion

		#region System.Xml.Linq Extensions

		#region XDocument Extension Methods

		/// <summary>
		/// Returns the xpath query value as an int.
		/// </summary>
		/// <param name="document">The document.</param>
		/// <param name="expression">The expression.</param>
		/// <returns></returns>
		public static int XPathAsInt(this XDocument document, string expression)
		{
			var temp = document.XPathValue(expression);
			return (temp != null ? temp.AsInt() : default(int));
		}

		/// <summary>
		/// Returns the xpath query value as a string.
		/// </summary>
		/// <param name="document">The document.</param>
		/// <param name="expression">The expression.</param>
		/// <returns></returns>
		public static string XPathAsString(this XDocument document, string expression)
		{
			return (document.XPathValue(expression) ?? string.Empty);
		}

		/// <summary>
		/// Returns the xpath query value as a date time.
		/// </summary>
		/// <param name="document">The document.</param>
		/// <param name="expression">The xpath expression.</param>
		/// <returns></returns>
		public static DateTime XPathAsDateTime(this XDocument document, string expression)
		{
			var temp = document.XPathValue(expression);
			return (temp != null ? temp.AsDateTime() : default(DateTime));
		}

		/// <summary>
		/// Returns the xpath query value as a boolean.
		/// </summary>
		/// <param name="document">The document.</param>
		/// <param name="expression">The xpath expression.</param>
		/// <returns></returns>
		public static bool XPathAsBoolean(this XDocument document, string expression)
		{
			var temp = document.XPathValue(expression);
			return (temp != null ? temp.AsBoolean() : default(bool));
		}

		#endregion

		#region XElement Extension Methods

		/// <summary>
		/// Returns the xpath query value as a int.
		/// </summary>
		/// <param name="element">The element.</param>
		/// <param name="expression">The xpath expression.</param>
		/// <returns></returns>
		public static int XPathAsInt(this XElement element, string expression)
		{
			var temp = element.XPathValue(expression);
			return (temp != null ? temp.AsInt() : default(int));
		}

		/// <summary>
		/// Returns the xpath query value as a decimal.
		/// </summary>
		/// <param name="element">The element.</param>
		/// <param name="expression">The xpath expression.</param>
		/// <returns></returns>
		public static decimal XPathAsDecimal(this XElement element, string expression)
		{
			var temp = element.XPathValue(expression);
			return (temp != null ? temp.AsDecimal() : default(decimal));
		}

		/// <summary>
		/// Returns the xpath query value as a ullable int.
		/// </summary>
		/// <param name="element">The element.</param>
		/// <param name="expression">The expression.</param>
		/// <returns></returns>
		public static int? XPathAsNullableInt(this XElement element, string expression)
		{
			var temp = element.XPathValue(expression);
			return (temp != null ? temp.AsInt() : (int?)null);
		}

        /// <summary>
        /// Returns the xpath query value as a nullable long.
        /// </summary>
        /// <param name="element">The element.</param>
        /// <param name="expression">The expression.</param>
        /// <returns></returns>
        public static long? XPathAsNullableLong(this XElement element, string expression)
        {
            var temp = element.XPathValue(expression);
            return (temp != null ? temp.ToLong() : (long?)null);
        }

		/// <summary>
		/// Returns the xpath query value as a string.
		/// </summary>
		/// <param name="element">The element.</param>
		/// <param name="expression">The xpath expression.</param>
		/// <returns></returns>
		public static string XPathAsString(this XElement element, string expression)
		{
			return (element.XPathValue(expression) ?? string.Empty);
		}

		/// <summary>
		/// Returns the xpath query value as a date time.
		/// </summary>
		/// <param name="element">The element.</param>
		/// <param name="expression">The xpath expression.</param>
		/// <returns></returns>
		public static DateTime XPathAsDateTime(this XElement element, string expression)
		{
			var temp = element.XPathValue(expression);
			return (temp != null ? temp.AsDateTime() : default(DateTime));
		}

		/// <summary>
		/// Returns the xpath query value as a boolean.
		/// </summary>
		/// <param name="element">The element.</param>
		/// <param name="expression">The xpath expression.</param>
		/// <returns></returns>
		public static bool XPathAsBoolean(this XElement element, string expression)
		{
			var temp = element.XPathValue(expression);
			return (temp != null ? temp.AsBoolean() : default(bool));
		}

		/// <summary>
		/// Xs the path as nullable GUID.
		/// </summary>
		/// <param name="element">The element.</param>
		/// <param name="expression">The expression.</param>
		/// <returns></returns>
		public static Guid? XPathAsNullableGuid(this XElement element, string expression)
		{
			var temp = element.XPathValue(expression);
			return (temp != null ? temp.AsNullableGuid() : null);
		}

		public static List<string> XPathAttributeAsListOfString(this XElement element, string expression)
		{
			var elements = ((IEnumerable)element.XPathEvaluate(expression)).Cast<XAttribute>();

			return elements.Select(matchedElement => matchedElement.Value).ToList();
		}

		/// <summary>
		/// Xml Encodes text.
		/// </summary>
		/// <param name="text">The text.</param>
		/// <returns></returns>
		public static string Encode(this string text)
		{
			if (String.IsNullOrEmpty(text))
				return text;

			return HttpUtility.HtmlEncode(HttpUtility.HtmlDecode(text));
		}

		/// <summary>
		/// Creates an element.
		/// </summary>
		/// <param name="parentElement">The parent element.</param>
		/// <param name="name">The name.</param>
		/// <param name="value">The value.</param>
		/// <returns></returns>
		public static XElement CreateElement(this XElement parentElement, string name, object value = null)
		{
			if (parentElement == null)
				return null;

			var childElement = (value == null ? new XElement(name) : new XElement(name, value));
			parentElement.Add(childElement);
			return childElement;
		}

		/// <summary>
		/// Creates an attribute.
		/// </summary>
		/// <param name="xmlElement">The XML element.</param>
		/// <param name="name">The name.</param>
		/// <param name="value">The value.</param>
		/// <returns></returns>
		public static XAttribute CreateAttribute(this XElement xmlElement, string name, object value = null)
		{
			var attribute = new XAttribute(name, value ?? "");
			xmlElement.Add(attribute);
			return attribute;
		}

		/// <summary>
		/// Gets or creates an element.
		/// </summary>
		/// <param name="parentElement">The parent element.</param>
		/// <param name="xpath">The xpath.</param>
		/// <param name="name">The name.</param>
		/// <param name="value">The value.</param>
		/// <returns></returns>
		public static XElement GetOrCreateElement(this XElement parentElement, string xpath, string name, object value = null)
		{
			var childElement = parentElement.XPathSelectElement(xpath);

			if (childElement == null)
			{
				parentElement.Add((value == null ? new XElement(name) : new XElement(name, value)));
				childElement = parentElement.XPathSelectElement(xpath);
			}
			else
				childElement.Value = (value == null ? String.Empty : value.ToString());

			return childElement;
		}

		/// <summary>
		/// Gets or creates an element.
		/// </summary>
		/// <param name="parentElement">The parent element.</param>
		/// <param name="xpath">The xpath.</param>
		/// <param name="name">The name.</param>
		/// <returns></returns>
		public static XElement GetOrCreateElement(this XElement parentElement, string xpath, string name)
		{
			var childElement = parentElement.XPathSelectElement(xpath);

			if (childElement == null)
			{
				parentElement.Add(new XElement(name));
				childElement = parentElement.XPathSelectElement(xpath);
			}

			return childElement;
		}

		/// <summary>
		/// Gets or creates an element.
		/// </summary>
		/// <param name="parentElement">The parent element.</param>
		/// <param name="name">The name.</param>
		/// <returns></returns>
		public static XElement GetOrCreateElement(this XElement parentElement, string name)
		{
			return GetOrCreateElement(parentElement, name, name);
		}

		/// <summary>
		/// Removes the elements by XPath.
		/// </summary>
		/// <param name="parentElement">The parent element.</param>
		/// <param name="xpath">The xpath.</param>
		public static void RemoveElementsByXpath(this XElement parentElement, string xpath)
		{
			var selectedElements = parentElement.XPathSelectElements(xpath);

			var xElements = selectedElements as XElement[] ?? selectedElements.ToArray();
			if (xElements.Any())
				xElements.Remove();
		}

		/// <summary>
		/// Removes the type of the elements by node.
		/// </summary>
		/// <param name="parentElement">The parent element.</param>
		/// <param name="nodeType">Type of the node.</param>
		public static void RemoveElementsByNodeType(this XElement parentElement, XmlNodeType nodeType = XmlNodeType.None)
		{
			var selectedNodes = parentElement.Nodes();

			if (nodeType == XmlNodeType.None)
			{
				selectedNodes.Remove();
				return;
			}

			var xNodes = selectedNodes as XNode[] ?? selectedNodes.ToArray();
			for (var i = 0; i < xNodes.Count(); i++)
			{
				var node = xNodes.ElementAt(i);
				if (node.NodeType == nodeType)
					node.Remove();
			}
		}

		/// <summary>
		/// Gets the value of an attribute for an element
		/// </summary>
		/// <param name="element">The element whose attribute is required</param>
		/// <param name="attributeName">The name of the attribute whose value is required</param>
		/// <param name="defaultValue">The default value to use if the attribute is not present</param>
		/// <returns>The string value of the attribute</returns>
		public static string AttributeValue(this XElement element, string attributeName, string defaultValue = "")
		{
			var attribute = element.Attribute(attributeName);

			return attribute.IsNull() ? defaultValue : attribute.Value;
		}

		/// <summary>
		/// Gets the value of an attribute for an element, converting it to the given type
		/// </summary>
		/// <typeparam name="T">The type expected</typeparam>
		/// <param name="element">The element whose attribute is required</param>
		/// <param name="attributeName">The name of the attribute whose value is required</param>
		/// <param name="defaultValue">The default value to use if the attribute is not present</param>
		/// <returns>The value of the attribute</returns>
		public static T AttributeValue<T>(this XElement element, string attributeName, T defaultValue = default(T))
		{
			var attribute = element.Attribute(attributeName);
			if (attribute.IsNull())
				return defaultValue;

			var nullableType = Nullable.GetUnderlyingType(typeof(T));

			return nullableType.IsNull()
				? (T)Convert.ChangeType(attribute.Value, typeof(T))
				: (T)Convert.ChangeType(attribute.Value, nullableType);
		}

		#endregion

		#region XPath Extension Methods

		/// <summary>
		/// Gets the first item from a set of XPath Items.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="node">The node.</param>
		/// <param name="expression">The expression.</param>
		/// <returns></returns>
		public static T XPathSelectItem<T>(this XNode node, string expression)
		{
			return ((IEnumerable)node.XPathEvaluate(expression)).Cast<T>().First();
		}

		/// <summary>
		/// Gets a set of XPath Items
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="node">The node.</param>
		/// <param name="expression">The expression.</param>
		/// <returns></returns>
		public static T XPathSelectItems<T>(this XNode node, string expression)
		{
			return (T)node.XPathEvaluate(expression);
		}

		/// <summary>
		/// Gets the XPath value.
		/// </summary>
		/// <param name="parentElement">The parent element.</param>
		/// <param name="xpath">The xpath.</param>
		/// <returns></returns>
		public static string XPathValue(this XElement parentElement, string xpath)
		{
			try
			{
				var targetElement = parentElement.XPathSelectElement(xpath);
				return targetElement != null ? targetElement.Value : null;
			}
			catch
			{
				var targetAttribute = ((IEnumerable)parentElement.XPathEvaluate(xpath)).Cast<XAttribute>().First();
				return targetAttribute != null ? targetAttribute.Value : null;
			}
		}

		/// <summary>
		/// Gets the XPath value.
		/// </summary>
		/// <param name="document">The document.</param>
		/// <param name="xpath">The xpath.</param>
		/// <returns></returns>
		public static string XPathValue(this XDocument document, string xpath)
		{
			try
			{
				var targetElement = document.XPathSelectElement(xpath);
				return targetElement != null ? targetElement.Value : null;
			}
			catch
			{
				var targetAttribute = ((IEnumerable)document.XPathEvaluate(xpath)).Cast<XAttribute>().First();
				return targetAttribute != null ? targetAttribute.Value : null;
			}
		}

		#endregion

		/// <summary>
		/// Transform object into an boolean data type.
		/// </summary>
		/// <param name="item">The object to be transformed.</param>
		/// <returns>The boolean value.</returns>
		public static bool AsBoolean(this object item)
		{
			return AsBoolean(item, default(bool));
		}

		/// <summary>
		/// Gets element value as a boolean
		/// </summary>
		/// <param name="parentNode">The parent node.</param>
		/// <param name="xpath">The xpath.</param>
		/// <returns></returns>
		public static bool AsBoolean(this XmlNode parentNode, string xpath)
		{
			return AsBoolean(parentNode, xpath, false);
		}

		/// <summary>
		/// Gets element value as a boolean
		/// </summary>
		/// <param name="parentNode">The parent node.</param>
		/// <param name="xpath">The xpath.</param>
		/// <param name="defaultValue">if set to <c>true</c> [default value].</param>
		/// <returns></returns>
		public static bool AsBoolean(this XmlNode parentNode, string xpath, bool defaultValue)
		{
			var childNode = parentNode.SelectSingleNode(xpath);
			return (childNode == null ? defaultValue : AsBoolean(childNode.InnerText, defaultValue));
		}

		/// <summary>
		/// Transform object into an boolean data type.
		/// </summary>
		/// <param name="item">The object to be transformed.</param>
		/// <param name="defaultBoolean">if set to <c>true</c> [default boolean].</param>
		/// <returns>The boolean value.</returns>
		public static bool AsBoolean(this object item, bool defaultBoolean)
		{
			if (item == null)
				return defaultBoolean;

			if (item is bool)
				return (bool)item;

			bool result;
			var value = item.ToString();

			return !bool.TryParse(value, out result) ? ((item.AsInt() != 0 || value.ToUpper() == "TRUE" || value.ToUpper() == "T" || value.ToUpper() == "YES") || defaultBoolean) : result;
		}

		/// <summary>
		/// Ases the nullable GUID.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <returns></returns>
		public static Guid? AsNullableGuid(this object item)
		{
			return AsNullableGuid(item, null);
		}

		/// <summary>
		/// Ases the nullable GUID.
		/// </summary>
		/// <param name="item">The item.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <returns></returns>
		public static Guid? AsNullableGuid(this object item, Guid? defaultValue)
		{
			if (item.IsNull())
				return defaultValue;

			if (item is Guid)
				return (Guid)item;

			Guid result;
			var value = item.ToString();

			return !Guid.TryParse(value, out result) ? defaultValue : result;
		}

		/// <summary>
		/// Ases the nullable long.
		/// </summary>
		/// <param name="parentNode">The parent node.</param>
		/// <param name="xpath">The xpath.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <returns></returns>
		public static long? AsNullableLong(this XmlNode parentNode, string xpath, long? defaultValue)
		{
			long i;
			return (long.TryParse(AsString(parentNode, xpath, defaultValue.ToString()), out i) ? i : defaultValue);
		}

		/// <summary>
		/// Ases the long.
		/// </summary>
		/// <param name="parentNode">The parent node.</param>
		/// <param name="xpath">The xpath.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <returns></returns>
		public static long AsLong(this XmlNode parentNode, string xpath, long defaultValue)
		{
			long i;
			return (long.TryParse(AsString(parentNode, xpath, defaultValue.ToString()), out i) ? i : defaultValue);
		}

		/// <summary>
		/// Ases the long.
		/// </summary>
		/// <param name="parentNode">The parent node.</param>
		/// <param name="xpath">The xpath.</param>
		/// <returns></returns>
		public static long AsLong(this XmlNode parentNode, string xpath)
		{
			return AsLong(parentNode, xpath, 0);
		}

		/// <summary>
		/// Attributes as int32.
		/// </summary>
		/// <param name="node">The node.</param>
		/// <param name="attributeName">Name of the attribute.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <returns></returns>
		public static int AttributeAsInt32(this XmlNode node, string attributeName, int defaultValue)
		{
			int i;
			return (int.TryParse(AttributeAsString(node, attributeName, defaultValue.ToString()), out i) ? i : defaultValue);
		}

		/// <summary>
		/// Gets thes the value of a child node for an element
		/// </summary>
		/// <param name="ele">The base element</param>
		/// <param name="elementXPath">The xpath to the required node</param>
		/// <returns>The string value of the required node</returns>
		public static string GetElementValueAsString(this XElement ele, string elementXPath)
		{
			return (ele.XPathGetValue(elementXPath));
		}

		/// <summary>
		/// Gets thes the value of a child node for an element
		/// </summary>
		/// <param name="ele">The base element</param>
		/// <param name="elementXPath">The xpath to the required node</param>
		/// <param name="defaultValue">The default value if the node does not exit.</param>
		/// <returns>
		/// The string value of the required node
		/// </returns>
		public static string GetElementValueAsString(this XElement ele, string elementXPath, string defaultValue)
		{
			return (ele.XPathGetValue(elementXPath) ?? defaultValue);
		}

		/// <summary>
		/// Gets the value of an element or attribute based on an xpath element
		/// </summary>
		/// <param name="parentElement">The parent element from which the xpath with be based</param>
		/// <param name="xpath">The xpath expression to select the element or node</param>
		/// <returns>The value of the selected element or attribute</returns>
		public static string XPathGetValue(this XElement parentElement, string xpath)
		{
			if (parentElement.IsNull())
				return null;

			var xObject = ((IEnumerable)parentElement.XPathEvaluate(xpath)).Cast<Object>().FirstOrDefault();

			var targetElement = xObject as XElement;
			if (targetElement != null)
				return targetElement.Value;

			var targetAttribute = xObject as XAttribute;
			return targetAttribute != null ? targetAttribute.Value : null;
			/*
				try
				{
					XElement targetElement = parentElement.XPathSelectElement(xpath);
					return targetElement != null ? targetElement.Value : null;
				}
				catch
				{
					XAttribute targetAttribute = ((IEnumerable) parentElement.XPathEvaluate(xpath)).Cast<XAttribute>().First();
					return targetAttribute != null ? targetAttribute.Value : null;
				}
			*/
		}

		/// <summary>
		/// Transform the value of an object to corresponding enum value
		/// </summary>
		/// <typeparam name="T">Input type parameter</typeparam>
		/// <param name="item">Given object</param>
		/// <param name="defaultEnum">default value for the enum</param>
		/// <returns>Apptopriate enum or null</returns>
		public static T AsEnum<T>(this object item, T defaultEnum) where T : struct
		{
			if (item.IsNull())
				return defaultEnum;

			var s = item as string;
			if (s != null)
			{
				T result;
				if (Enum.TryParse(s, true, out result))
					return result;
			}
			else if (item is int || item is char)
			{
				var val = item is char ? (char)item : (int)item;
				var temp = Enum.GetName(typeof(T), val);
				if (temp != null)
					return (T)Enum.Parse(typeof(T), temp, true);
			}
			return defaultEnum;
		}

		/// <summary>
		/// Gets the element value as int32.
		/// </summary>
		/// <param name="ele">The ele.</param>
		/// <param name="elementXPath">The element X path.</param>
		/// <returns></returns>
		public static int GetElementValueAsInt32(this XElement ele, string elementXPath)
		{
			var temp = ele.XPathGetValue(elementXPath);
			return (temp != null ? temp.AsInt32() : default(int));
		}

		/// <summary>
		/// Gets the value on an xelement as an int64.
		/// </summary>
		/// <param name="ele">The element whose value is required</param>
		/// <param name="elementXPath">The element X path.</param>
		/// <returns>The int64 value of the element</returns>
		public static long GetElementValueAsInt64(this XElement ele, string elementXPath)
		{
			var temp = ele.XPathGetValue(elementXPath);
			return (temp != null ? temp.AsInt64() : default(int));
		}

		/// <summary>
		/// Transform object into an integer data type.
		/// </summary>
		/// <param name="item">The object to be transformed.</param>
		/// <returns>The integer value.</returns>
		public static int AsInt32(this object item)
		{
			return AsInt32(item, default(int));
		}

		/// <summary>
		/// Transform object into an integer data type.
		/// </summary>
		/// <param name="item">The object to be transformed.</param>
		/// <param name="defaultInt">The default int.</param>
		/// <returns>The integer value.</returns>
		public static int AsInt32(this object item, int defaultInt)
		{
			if (item == null)
				return defaultInt;

			if (item is int)
				return (int)item;

			int result;
			return !int.TryParse(item.ToString(), out result) ? defaultInt : result;
		}

		/// <summary>
		/// Gets the element value as date time.
		/// </summary>
		/// <param name="ele">The ele.</param>
		/// <param name="elementXPath">The element X path.</param>
		/// <returns></returns>
		public static DateTime GetElementValueAsDateTime(this XElement ele, string elementXPath)
		{
			var temp = ele.XPathGetValue(elementXPath);
			return (temp != null ? temp.AsDateTime() : default(DateTime));
		}

		/// <summary>
		/// Gets the element value as nullable boolean.
		/// </summary>
		/// <param name="ele">The ele.</param>
		/// <param name="elementXPath">The element X path.</param>
		/// <returns></returns>
		public static bool? GetElementValueAsNullableBoolean(this XElement ele, string elementXPath)
		{
			bool? value = null;
			var temp = ele.XPathGetValue(elementXPath);
			if (temp != null)
				value = temp.AsBoolean();
			return value;
		}

		/// <summary>
		/// Gets the element value as boolean.
		/// </summary>
		/// <param name="ele">The ele.</param>
		/// <param name="elementXPath">The element X path.</param>
		/// <returns></returns>
		public static bool GetElementValueAsBoolean(this XElement ele, string elementXPath)
		{
			var temp = ele.XPathGetValue(elementXPath);
			return (temp != null ? temp.AsBoolean() : default(bool));
		}

		#endregion

		#region Set xml values

		/// <summary>
		/// Sets the string value.
		/// </summary>
		/// <param name="parentNode">The parent node.</param>
		/// <param name="elementName">Name of the element.</param>
		/// <param name="value">The value.</param>
		public static void SetStringValue(this XmlNode parentNode, string elementName, string value)
		{
			var node = parentNode.GetOrCreateElement(elementName);
			if (value.IsNotNullOrEmpty()) node.InnerText = value;
		}

		/// <summary>
		/// Sets the date value.
		/// </summary>
		/// <param name="parentNode">The parent node.</param>
		/// <param name="elementName">Name of the element.</param>
		/// <param name="value">The value.</param>
		public static void SetDateValue(this XmlNode parentNode, string elementName, DateTime value)
		{
			var node = parentNode.GetOrCreateElement(elementName);
			if (value.IsNotNull()) node.InnerText = value.ToString("yyyy-MM-dd");
		}

		/// <summary>
		/// Sets the date value.
		/// </summary>
		/// <param name="parentNode">The parent node.</param>
		/// <param name="elementName">Name of the element.</param>
		/// <param name="value">The value.</param>
		public static void SetDateValue(this XmlNode parentNode, string elementName, DateTime? value)
		{
			var node = parentNode.GetOrCreateElement(elementName);
			if (value.HasValue) node.InnerText = value.Value.ToString("yyyy-MM-dd");
		}

		/// <summary>
		/// Sets the flag value.
		/// </summary>
		/// <param name="parentNode">The parent node.</param>
		/// <param name="elementName">Name of the element.</param>
		/// <param name="flagSet">if set to <c>true</c> [flag set].</param>
		public static void SetFlagValue(this XmlNode parentNode, string elementName, bool flagSet)
		{
			var node = parentNode.GetOrCreateElement(elementName);
			node.InnerText = (flagSet) ? "1" : "0";
		}

		/// <summary>
		/// Sets the decimal value.
		/// </summary>
		/// <param name="parentNode">The parent node.</param>
		/// <param name="elementName">Name of the element.</param>
		/// <param name="value">The value.</param>
		/// <param name="formatSpecifier">The format specifier.</param>
		public static void SetDecimalValue(this XmlNode parentNode, string elementName, decimal? value, string formatSpecifier = "F2")
		{
			var node = parentNode.GetOrCreateElement(elementName);
			if (value.HasValue) node.InnerText = value.Value.ToString(formatSpecifier);
		}

		/// <summary>
		/// Sets the integer value.
		/// </summary>
		/// <param name="parentNode">The parent node.</param>
		/// <param name="elementName">Name of the element.</param>
		/// <param name="value">The value.</param>
		public static void SetIntegerValue(this XmlNode parentNode, string elementName, int value)
		{
			var node = parentNode.GetOrCreateElement(elementName);
			node.InnerText = value.ToString();
		}

		/// <summary>
		/// Sets the integer value.
		/// </summary>
		/// <param name="parentNode">The parent node.</param>
		/// <param name="elementName">Name of the element.</param>
		/// <param name="value">The value.</param>
		public static void SetIntegerValue(this XmlNode parentNode, string elementName, int? value)
		{
			var node = parentNode.GetOrCreateElement(elementName);
			if (value.HasValue) node.InnerText = value.Value.ToString();
		}

		/// <summary>
		/// Sets the long value.
		/// </summary>
		/// <param name="parentNode">The parent node.</param>
		/// <param name="elementName">Name of the element.</param>
		/// <param name="value">The value.</param>
		public static void SetLongValue(this XmlNode parentNode, string elementName, long value)
		{
			var node = parentNode.GetOrCreateElement(elementName);
			node.InnerText = value.ToString();
		}

		#endregion

		#region Obfuscation

		/// <summary>
		/// Obfuscates the XML document.
		/// </summary>
		/// <param name="xmlDocString">The XML document string.</param>
		/// <returns></returns>
		public static string ObfuscateXmlAndJson(this string originalString, string[] nodesAndAttributesToObfuscate)
		{
			try
			{
				var xmlDoc = XDocument.Parse(originalString);
				
				foreach (var name in nodesAndAttributesToObfuscate)
				{
					var nodes = xmlDoc.XPathSelectElements("//" + name);
					foreach (var node in nodes)
					{
						node.Value = node.Value.ObfuscateString();
					}

					var nodesWithAttribute = xmlDoc.XPathSelectElements("//*[@" + name + "]");
					foreach (var node in nodesWithAttribute)
					{
						var attributes = node.Attributes();
						var attribute = attributes.SingleOrDefault(a => a.Name == name);
						attribute.Value = attribute.Value.ObfuscateString();
					}
				}

				return xmlDoc.ToString();
			}
			catch (Exception)
			{}

			try
			{
				var json = JObject.Parse(originalString);

				foreach (var name in nodesAndAttributesToObfuscate)
				{
					var objects = json.Descendants()
						.Where(t => t.Type == JTokenType.Property && ((JProperty)t).Name == name)
						.Select(p => ((JProperty)p).Value);

					foreach (var item in objects)
					{
						item.Replace(item.ToString().ObfuscateString());
					}
				}

				return json.ToString();
			}
			catch (Exception)
			{}

			return originalString;
		}

		/// <summary>
		/// Obfuscates the string.
		/// </summary>
		/// <param name="original">The original.</param>
		/// <returns></returns>
		public static string ObfuscateString(this string original)
		{
			var obfuscated = string.Empty;
			foreach (var c in original)
			{
				obfuscated += "?";
			}
			return obfuscated;
		}

		#endregion
	}
}
