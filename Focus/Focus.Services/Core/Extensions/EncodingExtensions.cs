﻿using System;
using System.Security.Cryptography;

namespace Focus.Services.Core.Extensions
{
	internal static class EncodingExtensions
	{
		/// <summary>
		/// Generates the hash.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <param name="salt">The salt.</param>
		/// <param name="length">The length of the generated hash.</param>
		/// <returns></returns>
		internal static string GenerateHash(this string value, string salt, int length = 86)
		{
			var buffer = System.Text.Encoding.UTF8.GetBytes(value + salt);
			buffer = SHA512.Create().ComputeHash(buffer);

			return Convert.ToBase64String(buffer).Substring(0, length); 
		}
	}
}
