﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.EmailTemplate;
using Focus.Core.Messages;
using Focus.Core.Views;
using Focus.Data.Core.Entities;
using Focus.Services.Core.Posting;
using Focus.Services.DtoMappers;
using Focus.Services.Mappers;
using Focus.Services.Utilities;
using Framework.Core;

#endregion

namespace Focus.Services.Core.Extensions
{
	internal static class JobExtensions
	{
		/// <summary>
		/// Generates the posting HTML for a job.
		/// </summary>
		/// <param name="job">The job.</param>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <param name="request">The request.</param>
		/// <param name="jobDto">The job dto.</param>
		/// <param name="licences">The licences.</param>
		/// <param name="certificates">The certificates.</param>
		/// <param name="languages">The languages.</param>
		/// <param name="specialRequirements">The special requirements.</param>
		/// <param name="locations">The locations.</param>
		/// <param name="programsOfStudy">The programs of study.</param>
		/// <param name="drivingLicenceEndorsements">The driving licence endorsements.</param>
		public static void GeneratePostingHtml(
			this Job job,
			IRuntimeContext runtimeContext,
			ServiceRequest request,
			JobDto jobDto = null,
			List<JobLicenceDto> licences = null,
			List<JobCertificateDto> certificates = null,
			List<JobLanguageDto> languages = null,
			List<JobSpecialRequirementDto> specialRequirements = null,
			List<JobLocationDto> locations = null,
			List<JobProgramOfStudyDto> programsOfStudy = null,
			List<JobDrivingLicenceEndorsementDto> drivingLicenceEndorsements = null)
		{
			var postingGeneratorModel = new PostingGeneratorModel
			{
				JobId = job.Id,
				Job = jobDto ?? job.AsDto(),
				BusinessUnit = job.BusinessUnit.IsNotNull() ? job.BusinessUnit.AsDto() : null,
				BusinessUnitDescription = job.BusinessUnitDescription.IsNotNull() ? job.BusinessUnitDescription.AsDto() : null,
				JobLicences = licences ?? job.JobLicences.Select(license => license.AsDto()).ToList(),
				JobCertificates = certificates ?? job.JobCertificates.Select(certificate => certificate.AsDto()).ToList(),
				JobLanguages = languages ?? job.JobLanguages.Select(language => language.AsDto()).ToList(),
				JobSpecialRequirements = specialRequirements ?? job.JobSpecialRequirements.Select(requirement => requirement.AsDto()).ToList(),
				JobLocations = locations ?? job.JobLocations.Select(location => location.AsDto()).ToList(),
				JobProgramsOfStudy = programsOfStudy ?? job.JobProgramOfStudies.Select(programOfStudy => programOfStudy.AsDto()).ToList(),
				JobDrivingLicenceEndorsements = drivingLicenceEndorsements ?? job.JobDrivingLicenceEndorsements.Select(drivingLicenceEndorsement => drivingLicenceEndorsement.AsDto()).ToList()
			};

			job.PostingHtml = new PostingGenerator(postingGeneratorModel, request.UserContext.Culture, runtimeContext.AppSettings, runtimeContext.Helpers.Localisation, runtimeContext.Helpers.Lookup).Generate();
		}

		/// <summary>
		/// Splits the job into separate jobs for each JobLocation record
		/// </summary>
		/// <param name="job">The job.</param>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <param name="request">The original service request.</param>
		/// <param name="assignOffices">Whether to assign offices to each job.</param>
		/// <returns>A list of split jobs (May be only one if only one location specified</returns>
		public static List<Job> SplitJobByLocation(this Job job, IRuntimeContext runtimeContext, ServiceRequest request, bool assignOffices = false)
		{
			var jobList = new List<Job> { job };
			if (!runtimeContext.AppSettings.SplitJobsByLocation)
				return jobList;

			var jobLocations = job.JobLocations.ToList();
			var jobLocationCount = jobLocations.Count;

			if (jobLocationCount > 1)
			{
				// Clear existing locations from the job so these won't be clones
				job.JobLocations.Clear();

				// Clone the job for each occurence of the location (apart from the first location which will be added back to the original job)
				foreach (var jobLocation in jobLocations.Skip(1))
				{
					var newJob = job.Clone();
					newJob.UpdatedBy = job.UpdatedBy;
					newJob.DescriptionPath = job.DescriptionPath;
					newJob.Location = jobLocation.Location;
					newJob.JobLocations.Add(jobLocation);
					jobList.Add(newJob);

					newJob.GeneratePostingHtml(runtimeContext, request);
				}

				// Restore the first job location to the original job and re-generate the HTML with just that location
				var firstLocation = jobLocations.First();
				job.JobLocations.Add(firstLocation);
				job.Location = firstLocation.Location;
				job.GeneratePostingHtml(runtimeContext, request);

				runtimeContext.Repositories.Core.SaveChanges();

				if (assignOffices)
				{
					foreach (var jobToAssign in jobList)
					{
						jobToAssign.AssignOfficesToJob(runtimeContext, request);
					}
				}
			}

			return jobList;
		}

		/// <summary>
		/// Assigns the offices to job based on the location post code
		/// </summary>
		/// <param name="job">The job to which offices should be assigned.</param>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <param name="request">The original request for posting the job.</param>
		/// <param name="sendEmail">Whether to send email for screening preferences</param>
		/// <returns></returns>
		public static void AssignOfficesToJob(this Job job, IRuntimeContext runtimeContext, ServiceRequest request, bool sendEmail = true)
		{
			if (!runtimeContext.AppSettings.OfficesEnabled)
				return;

			var offices = new List<Office>();
			var officeUnassigned = (bool?)null;

			var existingOffices = runtimeContext.Repositories.Core.JobOfficeMappers.Where(x => x.JobId == job.Id).ToList();

			var jobLocations = runtimeContext.Repositories.Core.JobLocations.Where(jl => jl.JobId == job.Id).ToList();
			if (existingOffices.IsNotNullOrEmpty() && jobLocations.All(loc => loc.NewLocation == null || loc.NewLocation == false))
			{
				return;
			}

			const string pattern = @"(?<=\().+?(?=\))";

			if (jobLocations.IsNotNullOrEmpty())
			{
				foreach (var jobLocation in jobLocations)
				{
					jobLocation.NewLocation = false;

					var zip = jobLocation.Zip.IsNotNullOrEmpty() 
						? jobLocation.Zip
						: Regex.Match(jobLocation.Location, pattern).Value;

					// Get the offices that deals with the postcode of the job location
					if (zip.IsNotNullOrEmpty())
						offices.AddRange(runtimeContext.Repositories.Core.Offices.Where(x => x.AssignedPostcodeZip.Contains(MiscUtils.SafeSubstring(zip, 0, 5))).ToList());
				}
			}


			if (offices.IsNull() || offices.Count == 0)
			{
				// Get default Office
				var defaultOffice = runtimeContext.Repositories.Core.Offices.FirstOrDefault(x => (x.DefaultType & OfficeDefaultType.JobOrder) == OfficeDefaultType.JobOrder);

				if (defaultOffice.IsNotNull() && existingOffices.All(x => x.OfficeId != defaultOffice.Id))
				{
					offices.Add(defaultOffice);
					officeUnassigned = true;
				}
			}

			var doEmailCheck = ((existingOffices.IsNullOrEmpty() || existingOffices[0].OfficeUnassigned == true) && !officeUnassigned.GetValueOrDefault(false));

			if (offices.IsNotNull() && offices.Count > 0)
			{
				// Make sure there are no duplicate offices - e.g if there are 2 zips both zips could be assigned to the same office, which would mean a duplicate would occur
				offices = offices.Distinct().ToList();

				var jobOffices = from o in offices
												 select new JobOfficeMapper { OfficeId = o.Id, JobId = job.Id, OfficeUnassigned = officeUnassigned };

				var jobOfficesSaved = new List<JobOfficeMapper>();

				foreach (var jobOffice in jobOffices)
				{
					if (existingOffices.All(x => x.OfficeId != jobOffice.OfficeId))
					{
						runtimeContext.Repositories.Core.Add(jobOffice);

						jobOfficesSaved.Add(jobOffice);
					}
					else
					{
						existingOffices.RemoveAll(x => x.OfficeId == jobOffice.OfficeId);
					}
				}

				existingOffices.ForEach(x => runtimeContext.Repositories.Core.Remove(x));

				runtimeContext.Repositories.Core.SaveChanges();

				if (doEmailCheck)
				{
					var officeEmails = offices.Where(office => office.OfficeManagerMailbox.IsNotNullOrEmpty())
																		.Select(office => new OfficeView
																		{
																			OfficeName = office.OfficeName,
																			OfficeManagerMailbox = office.OfficeManagerMailbox
																		}).ToList();

					// Send an email if job requires full screening (and office is not the default office)
					if (job.ScreeningPreferences == ScreeningPreferences.JobSeekersMustBeScreened && job.PostedOn.IsNotNull() && sendEmail)
						job.SendEmailForScreeningPreference(runtimeContext, officeEmails);

					// Send an email if pre-screening is requested
					if (job.PreScreeningServiceRequest.GetValueOrDefault(false) && runtimeContext.AppSettings.PreScreeningServiceRequest)
						job.SendEmailForPreScreeningRequest(runtimeContext, officeEmails);
				}

				// Log the action
				if (jobOfficesSaved.IsNotNullOrEmpty())
				{
					if (runtimeContext.CurrentRequest.IsNull())
						runtimeContext.SetRequest(request);

					jobOfficesSaved.ForEach(ed => runtimeContext.Helpers.Logging.LogAction(ActionTypes.AddJobOffice, typeof(JobOfficeMapper).Name, ed.Id, request.UserContext.UserId));
				}
			}
		}

		/// <summary>
		/// Posts the job.
		/// </summary>
		/// <param name="job">The job.</param>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <param name="request">The request.</param>
		/// <param name="postedBy">The posted by.</param>
		/// <param name="defaultJobExpiry">The default job expiry.</param>
		/// <param name="initialLensPostingId">The existing lens posting id (for new postings only).</param>
		/// <param name="resetRepository">Whether to reset the core repository on saving</param>
		/// <param name="ignoreClient">If set to true do not perform the client services call.</param>
		/// <param name="initialOfficeId">Set the client office id initially if client call not required.</param>
		public static void PostJob(this Job job, IRuntimeContext runtimeContext, ServiceRequest request, long postedBy, int defaultJobExpiry, string initialLensPostingId = null, bool resetRepository = true, bool ignoreClient = false, string initialOfficeId = "")
		{
			var currentTime = DateTime.Now;

			//Get the clients office id
      var clientOfficeId = (ignoreClient || !runtimeContext.AppSettings.OfficesEnabled || initialOfficeId.IsNotNullOrEmpty())
				? (initialOfficeId ?? "")
				: job.GetClientOfficeId(runtimeContext, request);

			// Set the posted date to today if not set and the expriration date to the posted date plus 14 days if not set
			var postedOn = (job.PostedOn.IsNotNull()) ? job.PostedOn : currentTime;

			job.LastPostedOn = currentTime;

			if (job.ClosingOn.IsNull())
				job.ClosingOn = postedOn.Value.AddDays(defaultJobExpiry);

			// Now post the job
			var postingDto = job.ToPostingDto(runtimeContext, clientOfficeId, request.UserContext.Culture);

			// Save posting to database
			var posting = runtimeContext.Repositories.Core.Postings.FirstOrDefault(x => x.JobId == job.Id);

			var ignoreLens = false;

			if (posting.IsNotNull())
			{
				var lensPostingId = posting.LensPostingId;
				var viewedCount = posting.ViewedCount;

				posting = postingDto.CopyTo(posting);

				posting.LensPostingId = lensPostingId;
				posting.ViewedCount = viewedCount;
			}
			else
			{
				posting = new Data.Core.Entities.Posting();
				posting = postingDto.CopyTo(posting);

				if (initialLensPostingId.IsNotNullOrEmpty())
				{
					ignoreLens = true;
					posting.LensPostingId = initialLensPostingId;
				}

				runtimeContext.Repositories.Core.Add(posting);
			}

			job.PostedBy = postedBy;
			job.PostedOn = postedOn;

			runtimeContext.Repositories.Core.SaveChanges(resetRepository);

		  string officeExternalId = null;

      var employeeAddress = job.Employee.Person.PersonAddresses.FirstOrDefault(pa => pa.IsPrimary);
      if (employeeAddress.IsNotNull())
      {
        var postcodeZipToFind = employeeAddress.PostcodeZip;
        var lookup = runtimeContext.Repositories.Configuration.ExternalLookUpItems
                                                              .FirstOrDefault(e => e.ExternalLookUpType == ExternalLookUpType.IntegrationOfficeIdPerZip && e.InternalId == postcodeZipToFind);
        if (lookup.IsNotNull())
        {
          officeExternalId = lookup.ExternalId;
        }
        else
        {
          var office = runtimeContext.Repositories.Core.Offices
                                                       .FirstOrDefault(o => o.InActive == false && o.ExternalId != null && o.AssignedPostcodeZip.Contains(postcodeZipToFind));
          if (office.IsNotNull())
            officeExternalId = office.ExternalId;
        }
      }
      else
      {
        var jobOfficeMapper = job.JobOfficeMappers.FirstOrDefault(j => j.IsNotNull() && j.Office.IsNotNull() && j.Office.ExternalId.IsNotNullOrEmpty());
        if (jobOfficeMapper.IsNotNull())
          officeExternalId = jobOfficeMapper.Office.ExternalId;
      }

			if (officeExternalId.IsNotNullOrEmpty() && runtimeContext.AppSettings.Module == FocusModules.Talent && !runtimeContext.CurrentRequest.UserContext.IsShadowingUser)
		  {
		    request.ExternalOfficeId = officeExternalId;
        request.ExternalUsername = string.Format("selfreg{0}", officeExternalId.ToLower());
		  }

		  // All jobs are now priority for veterans, not just ones with tax credits hires set to Veteran
			var isVeteranJob = runtimeContext.AppSettings.VeteranPriorityServiceEnabled;
			runtimeContext.Helpers.Messaging.Publish(request.ToPostJobMessage(posting.Id, job.Id, isVeteranJob, ignoreLens, ignoreClient), updateIfExists:true, entityName: EntityTypes.Posting.ToString(), entityKey: posting.Id);
		}

		/// <summary>
		/// Sends the email for when job screening preference is set to screen all job seekers
		/// </summary>
		/// <param name="job">The job.</param>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <param name="offices">The details for the (non-default) assigned office. If left blank, it will be read from DB</param>
		public static void SendEmailForScreeningPreference(this Job job, IRuntimeContext runtimeContext, List<OfficeView> offices = null)
		{
			if (offices.IsNullOrEmpty())
			{
				offices = GetOfficesForJob(runtimeContext, job.Id);

				if (!offices.Any())
					return;
			}

			var assistPath = runtimeContext.AppSettings.AssistApplicationPath.EndsWith("/")
				? runtimeContext.AppSettings.AssistApplicationPath
				: string.Concat(runtimeContext.AppSettings.AssistApplicationPath, "/");

			var jobLinkUrl = string.Format("{0}/assist/jobview/{1}", assistPath, job.Id);

			var templateData = new EmailTemplateData
			{
				EmployerName = job.BusinessUnit.Name,
				JobTitle = job.JobTitle,
				JobLinkUrls = new List<string> { jobLinkUrl }
			};

			var email = runtimeContext.Helpers.Email.GetEmailTemplatePreview(EmailTemplateTypes.JobScreeningRequired, templateData);
			offices.ForEach(office =>
				runtimeContext.Helpers.Email.SendEmail(office.OfficeManagerMailbox, String.Empty, String.Empty, email.Subject, email.Body, detectUrl: true));
		}

		/// <summary>
		/// Sends the email for when job where pre-screening is requested
		/// </summary>
		/// <param name="job">The job.</param>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <param name="offices">The details for the (non-default) assigned office. If left blank, it will be read from DB</param>
		public static void SendEmailForPreScreeningRequest(this Job job, IRuntimeContext runtimeContext, List<OfficeView> offices = null)
		{
			if (offices.IsNullOrEmpty())
			{
				offices = GetOfficesForJob(runtimeContext, job.Id);

				if (!offices.Any())
					return;
			}

			var businessUnitName = runtimeContext.Repositories.Core.BusinessUnits.Where(bu => bu.Id == job.BusinessUnitId).Select(bu => bu.Name).FirstOrDefault();
			var employeeName = (from employee in runtimeContext.Repositories.Core.Employees
													join person in runtimeContext.Repositories.Core.Persons
														on employee.PersonId equals person.Id
													where employee.Id == job.EmployeeId
													select new
													{
														person.FirstName,
														person.LastName
													}).First();

			var emailTemplate = runtimeContext.Helpers.Email.GetEmailTemplate(EmailTemplateTypes.PreScreeningServiceRequest);

			offices.ForEach(office =>
			{
				var templateData = new EmailTemplateData
				{
					HiringManagerName = string.Concat(employeeName.FirstName, " ", employeeName.LastName),
					EmployerName = businessUnitName,
					JobId = job.Id.ToString(CultureInfo.InvariantCulture),
					JobTitle = job.JobTitle,
					ExpiryDate = job.ClosingOn.GetValueOrDefault().ToShortDateString(),
					OfficeName = office.OfficeName,
				};

				runtimeContext.Helpers.Email.SendEmailFromTemplate(EmailTemplateTypes.PreScreeningServiceRequest, templateData, office.OfficeManagerMailbox ?? emailTemplate.Recipient, String.Empty, String.Empty);
			});
		}

		/// <summary>
		/// Gets the offices for a job
		/// </summary>
		/// <param name="runtimeContext"></param>
		/// <param name="jobId"></param>
		/// <returns></returns>
		private static List<OfficeView> GetOfficesForJob(IRuntimeContext runtimeContext, long jobId)
		{
			var offices = (from mapper in runtimeContext.Repositories.Core.JobOfficeMappers
										 join office in runtimeContext.Repositories.Core.Offices
											 on mapper.OfficeId equals office.Id
										 where mapper.JobId == jobId
											 && (mapper.OfficeUnassigned == null || mapper.OfficeUnassigned == false)
										 select new OfficeView
										 {
											 OfficeName = office.OfficeName,
											 OfficeManagerMailbox = office.OfficeManagerMailbox
										 }).ToList();

			offices = offices.Where(office => office.OfficeManagerMailbox.IsNotNullOrEmpty()).ToList();

			return offices;
		}

		#region Private methods

		/// <summary>
		/// Gets the client office id.
		/// </summary>
		/// <param name="job">The job.</param>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		/// <exception cref="System.Exception">Could not get client office id for job :  + clientResponse.Message</exception>
		private static string GetClientOfficeId(this Job job, IRuntimeContext runtimeContext, ServiceRequest request)
		{
			var postalCode = "";
			if (job.JobAddresses.IsNotNullOrEmpty() && job.JobAddresses[0].PostcodeZip.IsNotNullOrEmpty())
				postalCode = job.JobAddresses[0].PostcodeZip;

			if (postalCode == "" &&
					job.Employer.IsNotNull() &&
					job.Employer.EmployerAddresses.IsNotNullOrEmpty() &&
					job.Employer.EmployerAddresses[0].PostcodeZip.IsNotNullOrEmpty())
				postalCode = job.Employer.EmployerAddresses[0].PostcodeZip;

			var assignedOffice = runtimeContext.Repositories.Core.Offices.FirstOrDefault(office => !office.InActive && office.AssignedPostcodeZip.Contains(postalCode));
			if (assignedOffice == null)
				assignedOffice = runtimeContext.Repositories.Core.Offices.FirstOrDefault(office => (office.DefaultType & OfficeDefaultType.JobOrder) == OfficeDefaultType.JobOrder);

			if (assignedOffice == null)
				throw new Exception(string.Format("Could not get client office id for job {0} using postal code '{1}", job.Id, postalCode));

			return assignedOffice.Id.ToString();
		}

		#endregion
	}
}
