﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using Framework.Core;
using Framework.Logging;
using Focus.Core.Messages;

#endregion

namespace Focus.Services.Core
{
	internal static class MessageExtensions
	{
		/// <summary>
		/// Creates a ILogData class from the service request.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public static ILogData LogData(this IServiceRequest request)
		{
			return new LogData { SessionId = request.SessionId, RequestId = request.RequestId, UserId = (request.UserContext.IsNotNull()) ? (request.UserContext.ActionerId) : 0 };
		}		
	}
}
