﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Linq;

using Focus.Core;
using Focus.Core.Models.Career;
using Focus.Core.Settings.Interfaces;
using Framework.Core;

#endregion

namespace Focus.Services.Core.Extensions
{
	public static class MappingExtensions
	{
		/// <summary>
		/// Toes the star rating.
		/// </summary>
		/// <param name="score">The score.</param>
		/// <param name="starRatingMinimumScores">The star rating minimum scores.</param>
		/// <returns></returns>
		public static int ToStarRating(this int score, int[] starRatingMinimumScores)
		{
			if (starRatingMinimumScores.IsNullOrEmpty())
				return 0;

			if (starRatingMinimumScores.Length > 5 && score >= starRatingMinimumScores[5])
				return 5;

			if (starRatingMinimumScores.Length > 4 && score >= starRatingMinimumScores[4])
				return 4;

			if (starRatingMinimumScores.Length > 3 && score >= starRatingMinimumScores[3])
				return 3;

			if (starRatingMinimumScores.Length > 2 && score >= starRatingMinimumScores[2])
				return 2;

			if (starRatingMinimumScores.Length > 1 && score >= starRatingMinimumScores[1])
				return 1;

			return 0;
		}

		/// <summary>
		/// Toes the star matching.
		/// </summary>
		/// <param name="score">The score.</param>
		/// <param name="starRatingMinimumScores">The star rating minimum scores.</param>
		/// <returns></returns>
		public static StarMatching ToStarMatching(this int score, int[] starRatingMinimumScores)
		{
			if (starRatingMinimumScores.IsNullOrEmpty())
				return StarMatching.ZeroStar;

			if (starRatingMinimumScores.Length > 5 && score >= starRatingMinimumScores[5])
				return StarMatching.FiveStar;

			if (starRatingMinimumScores.Length > 4 && score >= starRatingMinimumScores[4])
				return StarMatching.FourStar;

			if (starRatingMinimumScores.Length > 3 && score >= starRatingMinimumScores[3])
				return StarMatching.ThreeStar;

			if (starRatingMinimumScores.Length > 2 && score >= starRatingMinimumScores[2])
				return StarMatching.TwoStar;

			if (starRatingMinimumScores.Length > 1 && score >= starRatingMinimumScores[1])
				return StarMatching.OneStar;

			return StarMatching.ZeroStar;
		}

		/// <summary>
		/// To the minimum score.
		/// </summary>
		/// <param name="starMatching">The star matching.</param>
		/// <param name="starRatingMinimumScores">The star rating minimum scores.</param>
		/// <returns></returns>
		public static int ToMinimumScore(this StarMatching starMatching, int[] starRatingMinimumScores)
		{
			switch (starMatching)
			{
				case StarMatching.None:
				case StarMatching.ZeroStar:
					return starRatingMinimumScores[0];

				case StarMatching.OneStar:
					return starRatingMinimumScores[1];

				case StarMatching.TwoStar:
					return starRatingMinimumScores[2];

				case StarMatching.ThreeStar:
					return starRatingMinimumScores[3];

				case StarMatching.FourStar:
					return starRatingMinimumScores[4];

				case StarMatching.FiveStar:
					return starRatingMinimumScores[5];
			}

			return starRatingMinimumScores[0];
		}

		/// <summary>
		/// Converts the star rating into a minimum score.
		/// </summary>
		/// <param name="starRating">The star rating.</param>
		/// <param name="starRatingMinimumScores">The star rating minimum scores.</param>
		/// <returns></returns>
		public static int ToStarRatingMinimumScore(this int starRating, int[] starRatingMinimumScores)
		{
			if(starRating < 0 || starRating >= starRatingMinimumScores.Length)
				return 0;

			return starRatingMinimumScores[starRating];
		}

		/// <summary>
		/// Converts a salary to an annual salary.
		/// </summary>
		/// <param name="salary">The salary.</param>
		/// <param name="salaryFrequency">The salary frequency.</param>
		/// <returns></returns>
		public static int ToAnnualSalary(this decimal salary, Frequencies salaryFrequency)
		{
			if (salary.IsNull())
				return 0;

			if (salaryFrequency.IsNull())
				return (int) salary;

			decimal annualSalary;

			switch (salaryFrequency)
			{
				case Frequencies.Hourly:
					annualSalary = salary * 8 * 365;
					break;
				case Frequencies.Daily:
					annualSalary = salary * 365;
					break;
				case Frequencies.Weekly:
					annualSalary = salary  * 52;
					break;
				case Frequencies.Monthly:
					annualSalary = salary  * 12;
					break; 
				default:
					annualSalary = salary;
					break;
			}

			return (int) annualSalary;
		}

    /// <summary>
    /// Converts the screening value string in the posting XML to an enum
    /// </summary>
    /// <param name="externalValue">The external value</param>
    /// <returns>The Screening Preferences</returns>
    public static ScreeningPreferences? ToScreeningPreferences(this string externalValue)
    {
      if (externalValue.IsNullOrEmpty())
        return null;

      return (ScreeningPreferences) externalValue.AsInt32();
    }

		/// <summary>
		/// Determines whether job title in an intern job title.
		/// </summary>
		/// <param name="jobTitle">The job title.</param>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <returns>
		///   <c>true</c> if [is intern job title]; otherwise, <c>false</c>.
		/// </returns>
		public static bool IsInternJobTitle(this string jobTitle, IRuntimeContext runtimeContext)
		{
			// TODO: Martha (Low) - test
			jobTitle = jobTitle.ToLower();

			 var internshipWords = new List<string>
		                          {
		                            runtimeContext.Helpers.Localisation.Localise("JobTitle.Intern", "intern"),
		                            runtimeContext.Helpers.Localisation.Localise("JobTitle.Interns", "interns"),
		                            runtimeContext.Helpers.Localisation.Localise("JobTitle.Internship", "internship"),
		                            runtimeContext.Helpers.Localisation.Localise("JobTitle.Apprentice", "apprentice")
		                          };

			return internshipWords.Any(internshipWord => jobTitle.Contains(internshipWord));
		}

		/// <summary>
		/// Converts a ROnet to Lens numeric ROnet.
		/// </summary>
		/// <param name="rOnet">The ROnet.</param>
		/// <returns></returns>
		public static string AsLensROnet(this string rOnet)
		{
			return (rOnet.IsNotNullOrEmpty()) ? rOnet.Replace("-", "").Replace(".", "").Replace("S", "9").Replace("s", "9") : rOnet;		
		}

		/// <summary>
		/// Whether the Origin Id represents a Talent Job
		/// </summary>
		/// <param name="originId">The origin id</param>
		/// <param name="appSettings">Application settings</param>
		/// <returns></returns>
		public static bool IsTalentOrigin(this int originId, IAppSettings appSettings)
		{
			var longOriginId = (long?) originId;
			return longOriginId.IsTalentOrigin(appSettings);
		}

		/// <summary>
		/// Whether the Origin Id represents a Talent Job
		/// </summary>
		/// <param name="originId">The origin id</param>
		/// <param name="appSettings">Application settings</param>
		/// <returns></returns>
		public static bool IsTalentOrigin(this long? originId, IAppSettings appSettings)
		{
			return originId.GetValueOrDefault(0).IsIn(appSettings.ReviewApplicationEligibilityOriginIds);
		}
	}
}
