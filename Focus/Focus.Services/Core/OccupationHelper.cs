﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Linq;
using Focus.Core;
using Focus.Core.Criteria.Onet;
using Focus.Data.Core;
using Focus.Services.Core.Onet;
using Focus.Services.Messages;
using Focus.Services.Queries;
using Focus.Services.Repositories;
using Focus.Services.Views;
using Framework.Caching;
using Framework.Core;

#endregion

namespace Focus.Services.Core
{
	internal class OccupationHelper
	{
		private readonly ILensRepository _lensRepository;
		private readonly IFocusRepository _repository;

		public OccupationHelper(ILensRepository lensRepository, IFocusRepository repository)
		{
			_lensRepository = lensRepository;
			_repository = repository;
		}

		/// <summary>
		/// Gets the clarified words.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public List<string> GetClarifiedWords(ClarifyWordRequest request, int minScore, int maxSearchCount, string type)
		{
			var clarifiedWords = _lensRepository.Clarify(request.Text, minScore, maxSearchCount, type);
	
			return clarifiedWords;
		}

		/// <summary>
		/// Gets the onets.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public List<OnetDetailsView> GetOnets(OnetRequest request)
		{
			var onets = new List<OnetDetailsView>();

			if (request.Criteria.OnetId.HasValue)
			{
				onets.Add(GetOnetsById(request.UserContext, request.Criteria.OnetId.Value));
			}

			if (request.Criteria.OnetCode.IsNotNullOrEmpty())
			{
				onets.Add(GetOnetByCode(request.UserContext, request.Criteria.OnetCode));
			}

			// matching titles
			if (request.Criteria.JobTitle.IsNotNullOrEmpty())
			{
				var mustHaveJobTask = true;
				// Get the O*Net Codes from Integrations Search Service if we can
				var onetCodesFromIntegrations = _lensRepository.GetOnetCodes(request.Criteria.JobTitle);

				// we only get one onet from lens, if we cannot find a JobTask in our db - dump it
				if (mustHaveJobTask)
				{
					var onetWords =
						_repository.Query<OnetWordView>().Where(x => x.OnetCode == (onetCodesFromIntegrations.FirstOrDefault())).
							FirstOrDefault();
					if (onetWords.IsNull()) onetCodesFromIntegrations = null;
				}

				List<string> allOnetCodes;

				// If Integrations Search Service did not return the required amount, try the DB
				if (onetCodesFromIntegrations.IsNull() || onetCodesFromIntegrations.Count < request.Criteria.ListSize)
				{
					// Get the O*Net Codes from DB using O*Net online algorithm
					// OnetSearch has been taken from FC and needs a lot of refactoring to fit the Focus structure
					var onetSearch = new OnetSearch(_repository);
					var onetCodesFromDb = onetSearch.GetOnetCodes(request.Criteria.JobTitle, request.Criteria.ListSize, mustHaveJobTask);

					// Merge the sets of data
					allOnetCodes = onetCodesFromIntegrations.IsNotNull() ? onetCodesFromIntegrations.Union(onetCodesFromDb).ToList() : onetCodesFromDb;
				}
				else
					allOnetCodes = onetCodesFromIntegrations;

				if (allOnetCodes.IsNotNull())
				{
					var onetViews = (from o in _repository.Query<OnetView>()
													 where allOnetCodes.Contains(o.OnetCode)
													 select o);

					var culture = (onetViews.Where(x => x.Culture == request.UserContext.Culture).Count() > 0 ? request.UserContext.Culture : Constants.DefaultCulture);

					var onetCodeAndOccupations = onetViews.Where(x => x.Culture == culture).Select(onetView => new OnetDetailsView
					{
						Id = onetView.Id,
						Key = onetView.Key,
						Occupation = onetView.Occupation,
						Description = onetView.Description,
						JobFamilyId = onetView.JobFamilyId,
						OnetCode = onetView.OnetCode
					}).ToList();
					// Add to Onets but not the one that may have been added previously
					onets.AddRange(onetCodeAndOccupations.Where(x => x.Id != request.Criteria.OnetId));
				}
			}
			else if (request.Criteria.JobFamilyId.HasValue)
			{
				// Add to Onets but not the one that may have been added previously
				onets.AddRange(GetCachedOnetsByFamilyId(request.UserContext, request.Criteria.JobFamilyId.Value).Where(x => x.Id != request.Criteria.OnetId));
			}

			onets = onets.Take(request.Criteria.ListSize).ToList();
			return onets;
		}

		/// <summary>
		/// Gets the onet by code.
		/// </summary>
		/// <param name="userData">The user data.</param>
		/// <param name="onetCode">The onet code.</param>
		/// <returns></returns>
		private OnetDetailsView GetOnetByCode(UserContext userData, string onetCode)
		{
			// Could get from cache, but we need the FamilyId
			var onetView = new OnetDetailsView();

			var userCultureOnetQuery = new OnetDetailsViewQuery(_repository, new OnetViewCriteria { Culture = userData.Culture, OnetCode = onetCode });
			var userCultureOnetViews = userCultureOnetQuery.Query().ToList();

			var defaultCultureOnetQuery = new OnetDetailsViewQuery(_repository, new OnetViewCriteria { Culture = Constants.DefaultCulture, OnetCode = onetCode });
			var defaultCultureOnetViews = defaultCultureOnetQuery.Query().ToList();

			var onetViews = MergeOnets(userCultureOnetViews, defaultCultureOnetViews);

			if (onetViews.Count > 0) onetView = onetViews[0];

			return onetView;
		}

		/// <summary>
		/// Gets the cached onets.
		/// </summary>
		/// <param name="userData">The user data.</param>
		/// <param name="onetId">The onet id.</param>
		/// <returns></returns>
		private OnetDetailsView GetOnetsById(UserContext userData, long onetId)
		{
			// Could get from cache, but we need the FamilyId
			var onetView = new OnetDetailsView();

			var userCultureOnetQuery = new OnetDetailsViewQuery(_repository, new OnetViewCriteria { Culture = userData.Culture, OnetId = onetId });
			var userCultureOnetViews = userCultureOnetQuery.Query().ToList();

			var defaultCultureOnetQuery = new OnetDetailsViewQuery(_repository, new OnetViewCriteria { Culture = Constants.DefaultCulture, OnetId = onetId });
			var defaultCultureOnetViews = defaultCultureOnetQuery.Query().ToList();

			var onetViews = MergeOnets(userCultureOnetViews, defaultCultureOnetViews);

			if (onetViews.Count > 0) onetView = onetViews[0];

			return onetView;
		}

		/// <summary>
		/// Merges the onets.
		/// </summary>
		/// <param name="cultureSpecificOnets">The culture specific onets.</param>
		/// <param name="defaultCultureOnets">The default culture onets.</param>
		/// <returns></returns>
		private List<OnetDetailsView> MergeOnets(List<OnetDetailsView> cultureSpecificOnets, List<OnetDetailsView> defaultCultureOnets)
		{
			if (cultureSpecificOnets.Count > 0)
			{
				var removeSet = (from cso in cultureSpecificOnets
												 join dco in defaultCultureOnets on cso.Id equals dco.Id
												 select dco
												).ToList();

				removeSet.ForEach(x => defaultCultureOnets.Remove(x));

				return cultureSpecificOnets.Union(defaultCultureOnets).ToList();
			}

			return defaultCultureOnets;
		}

		/// <summary>
		/// Gets the cached onets by family ID.
		/// </summary>
		/// <param name="userData">The user data.</param>
		/// <param name="familyId">The family id.</param>
		/// <returns></returns>
		private IEnumerable<OnetDetailsView> GetCachedOnetsByFamilyId(Core.UserContext userData, long familyId)
		{
			var onetKey = string.Format(Constants.CacheKeys.OnetKey, userData.Culture, familyId);
			var onetViewItems = Cacher.Get<List<OnetDetailsView>>(onetKey);

			if (onetViewItems.IsNull() || onetViewItems.Count == 0)
			{
				var userCultureOnetQuery = new OnetDetailsViewQuery(_repository, new OnetViewCriteria { Culture = userData.Culture, JobFamilyId = familyId });
				var userCultureOnetViews = userCultureOnetQuery.Query().ToList();

				var defaultCultureOnetQuery = new OnetDetailsViewQuery(_repository, new OnetViewCriteria { Culture = Constants.DefaultCulture, JobFamilyId = familyId });
				var defaultCultureOnetViews = defaultCultureOnetQuery.Query().ToList();

				onetViewItems = MergeOnets(userCultureOnetViews, defaultCultureOnetViews);

				Cacher.Set(onetKey, onetViewItems);
			}

			return onetViewItems;
		}
	}
}