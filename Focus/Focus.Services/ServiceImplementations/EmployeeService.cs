﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Focus.Core.IntegrationMessages;
using Focus.Services.Messages;
using Framework.Core;
using Framework.Logging;

using Focus.Core;
using Focus.Core.Criteria;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Views;
using Focus.Data.Configuration.Entities;
using Focus.Data.Core.Entities;
using Focus.Services.Core.Extensions;
using Focus.Services.Mappers;
using Focus.Services.DtoMappers;
using Focus.Services.Core;
using Focus.Services.Queries;
using Focus.Services.ServiceContracts;
using Focus.Core.Messages.EmployeeService;
using Focus.Core.Messages.EmployerService;

#endregion

namespace Focus.Services.ServiceImplementations
{
	public class EmployeeService : ServiceBase, IEmployeeService
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="EmployeeService" /> class.
		/// </summary>
		public EmployeeService()
			: this(null)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="EmployeeService" /> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		public EmployeeService(IRuntimeContext runtimeContext)
			: base(runtimeContext)
		{ }

		/// <summary>
		/// Gets the employee.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public EmployeeResponse GetEmployee(EmployeeRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new EmployeeResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					if (request.Id.IsNotNull())
					{
						var employee = Repositories.Core.FindById<Employee>(request.Id);

						if (employee.IsNotNull())
							response.Employee = employee.AsDto();
					}

				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}


		/// <summary>
		/// Gets the employee by person.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public EmployeeResponse GetEmployeeByPerson(EmployeeRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new EmployeeResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					if (request.Id.IsNotNull())
					{
						var employee = Repositories.Core.Employees.FirstOrDefault(x => x.PersonId == request.Id);

						if (employee.IsNotNull())
							response.Employee = employee.AsDto();
					}

				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the same employer employees.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public EmployeeResponse GetEmployeesForSameEmployer(EmployeeRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new EmployeeResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					if (request.Id.IsNotNull())
					{
						var employee = Repositories.Core.FindById<Employee>(request.Id);

						if (employee.IsNotNull())
						{
							var employees = employee.Employer.Employees;
							var employeeList = employees.Select(sameCompanyEmployee => sameCompanyEmployee.Id).ToList();

							response.SameCompanyEmployeeIds = employeeList;
						}
					}
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the hiring areas.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public EmployeeResponse GetHiringAreas(EmployeeRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new EmployeeResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				var hiringAreas = new List<string>();

				try
				{
					if (request.Id.IsNotNull())
					{
						var employee = Repositories.Core.FindById<Employee>(request.Id);

						if (employee.IsNotNull())
						{
							var jobFamilies = employee.EmployeeJobFamilies;
							foreach (var employeeJobFamily in jobFamilies)
							{
								var jobFamilyId = employeeJobFamily.JobFamilyId;
								var codeItemKey = Repositories.Configuration.CodeItems.First(x => x.Id == jobFamilyId).Key;
								hiringAreas.Add(Localise(request, codeItemKey));
							}
						}
					}
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				response.HiringAreas = hiringAreas;
				return response;
			}
		}

		/// <summary>
		/// Gets the employees for a list of employers.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public EmployeesForEmployerResponse GetEmployeesForEmployers(EmployeesForEmployerRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new EmployeesForEmployerResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				if (request.Criteria.IsNull() || (request.Criteria.EmployerIds.IsNull() && request.Criteria.BusinessUnitIds.IsNull()))
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.EmployerIdsNotSpecified));
					Logger.Error(request.LogData(), response.Message);
					return response;
				}

				var query = Repositories.Core.Query<EmployeeSearchView>();

				var searchResults = new List<EmployeeSearchResultView>();
				var searchResultCount = 0;

				var startIndex = 0;
				var isEmployers = request.Criteria.EmployerIds.IsNotNull();
				var allIds = isEmployers ? request.Criteria.EmployerIds : request.Criteria.BusinessUnitIds;
				var idCount = allIds.Count;

				while (startIndex <= idCount)
				{
					var limit = (500 > (idCount - startIndex)) ? (idCount - startIndex) : 500;
					var ids = allIds.GetRange(startIndex, limit);

					var employeeQuery = isEmployers
							? query.Where(employee => ids.Contains(employee.EmployerId))
							: query.Where(employee => ids.Contains(employee.BusinessUnitId));

					if (request.Criteria.SubscribedOnly)
					{
						employeeQuery = employeeQuery.Where(employee => employee.Unsubscribed.Equals(0));
					}

					if (request.Criteria.FetchOption == CriteriaBase.FetchOptions.Count)
					{
						searchResultCount += employeeQuery.Select(r => r.EmployeeId).Distinct().Count();
					}
					else
					{
						searchResults.AddRange(employeeQuery.Select(r => new EmployeeSearchResultView
						{
							Id = r.EmployeeId,
							FirstName = r.FirstName,
							LastName = r.LastName,
							EmailAddress = r.EmailAddress,
							PhoneNumber = r.PhoneNumber,
							EmployerId = r.EmployerId,
							EmployerName = r.EmployerName,
							BusinessUnitId = isEmployers ? 0 : r.BusinessUnitId,
							BusinessUnitName = isEmployers ? null : r.BusinessUnitName,
							JobTitle = r.JobTitle,
							BusinessUnitApprovalStatus = r.BusinessUnitApprovalStatus,
							BusinessUnitApprovalStatusReason = r.BusinessUnitApprovalStatusReason,
							EmployeeApprovalStatusReason = r.EmployeeApprovalStatusReason,
							Suffix = LocaliseOrDefault(request, r.SuffixId, string.Empty)
						}).Distinct());
					}


					startIndex += 500;
				}

				if (request.Criteria.FetchOption == CriteriaBase.FetchOptions.Count)
				{
					response.EmployeesCount = searchResultCount;
				}
				else
				{
					response.EmployeesCount = searchResults.Count();

					if (request.Criteria.FetchOption == CriteriaBase.FetchOptions.PagedList)
						response.PagedEmployees = new PagedList<EmployeeSearchResultView>(
								searchResults.OrderBy(e => e.EmployerName).ThenBy(e => e.LastName).AsQueryable(),
								request.Criteria.PageIndex,
								request.Criteria.PageSize);
					else
						response.Employees = searchResults;
				}
				return response;
			}
		}

		/// <summary>
		/// Searches the specified request.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public EmployeeSearchResponse Search(EmployeeSearchRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{

				var response = new EmployeeSearchResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;
				var states = Helpers.Lookup.GetLookup(LookupTypes.States);
				//var states = GetCachedLookup(request, LookupTypes.States).ToList();
				var stateLookup = states.ToDictionary(state => state.Id, state => state.Text);

				var query = new EmployeeSearchViewQuery(Repositories.Core, request.Criteria);

				var queryResult = query.Query().Distinct();

				response.EmployeeSearchViewsPaged = queryResult.GetPagedList( result => new EmployeeSearchResultView
					{
						Id = result.EmployeeId,
						FirstName = result.FirstName,
						LastName = result.LastName,
						EmailAddress = result.EmailAddress,
						PhoneNumber = result.PhoneNumber,
						Extension = result.Extension,
						EmployerId = result.EmployerId,
						EmployerName = result.EmployerName,
						BusinessUnitName = result.BusinessUnitName,
						AccountEnabled = result.AccountEnabled,
						EmployerApprovalStatus = result.EmployerApprovalStatus,
						EmployeeApprovalStatus = result.EmployeeApprovalStatus,
						Town = result.TownCity,
						State = stateLookup[result.StateId],
						StateId = result.StateId,
						BusinessUnitId = result.BusinessUnitId,
						LastLogin = result.LastLogin,
						JobTitle = result.JobTitle,
						EmployeeBusinessUnitId = result.EmployeeBusinessUnitId,
						AccountBlocked = result.AccountBlocked,
						IsPreferred = result.EmployerIsPreferred,
						IsPrimary = result.BusinessUnitIsPrimary,
						BusinessUnitApprovalStatus = result.BusinessUnitApprovalStatus,
						BusinessUnitApprovalStatusReason = result.BusinessUnitApprovalStatusReason,
						EmployeeApprovalStatusReason = result.EmployeeApprovalStatusReason,
						Suffix = LocaliseOrDefault( request, result.SuffixId, string.Empty ),
						AccountBlockedReason = result.AccountBlockedReason
					}, request.Criteria.PageIndex, request.Criteria.PageSize );

				return response;
			}
		}

		/// <summary>
		/// Emails the employee.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public EmailEmployeeResponse EmailEmployee(EmailEmployeeRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new EmailEmployeeResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					// Find the employee so we can get their email address
					var employee = Repositories.Core.Employees.First(x => x.Id == request.EmployeeId);
					var emailAddress = employee.Person.EmailAddress;

					var bcc = (request.BccSelf) ? request.UserContext.EmailAddress : "";

					var footerUrl = string.Empty;

					if (request.FooterType == EmailFooterTypes.HiringManagerUnsubscribe)
					{
						var encryptionModel = Helpers.Encryption.GenerateVector(TargetTypes.HiringManagerUnsubscribe, EntityTypes.Employee, employee.PersonId);
						var encryptedPersonId = Helpers.Encryption.Encrypt(employee.PersonId.ToString(CultureInfo.InvariantCulture), encryptionModel.Vector, true);
						footerUrl = string.Join("", request.FooterUrl, "?", "value=", encryptedPersonId, "&key=", encryptionModel.EncryptionId);
					}

					if (request.Attachment.IsNotNullOrEmpty() && request.AttachmentName.IsNotNullOrEmpty())
						Helpers.Email.SendEmail(emailAddress, "", bcc, request.EmailSubject, request.EmailBody, attachment: request.Attachment, attachmentName: request.AttachmentName, senderAddress: request.From, footerType: request.FooterType, footerUrl: footerUrl.IsNotNullOrEmpty() ? footerUrl : null);
					else
						Helpers.Email.SendEmail(emailAddress, "", bcc, request.EmailSubject, request.EmailBody, senderAddress: request.From, footerType: request.FooterType, footerUrl: footerUrl.IsNotNullOrEmpty() ? footerUrl : null);

					// Log the action
					LogAction(request, ActionTypes.EmailEmployee, employee);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

        /// <summary>
        /// Email to list ofemployee.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public EmailEmployeesResponse EmailEmployees(EmailEmployeesRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new EmailEmployeesResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    var bcc = (request.BccSelf) ? request.UserContext.EmailAddress : "";
                    var employeeEmailView = new EmployeeEmailView()
                    {
                        employeeIds = request.EmployeeIds,
                        Subject=request.EmailSubject,
                        Body=request.EmailBody,
                        SenderAddress = request.From,
                        Bcc = bcc
                    };
                    var valueTobeSendForMessageBus = new SendBlastEmailMessages()
                    {
                        EmployeesEmails = employeeEmailView,
                        FooterType = request.FooterType,
                        SendCopy = request.SendCopy,
                        FooterUrl = request.FooterUrl,
                        IsEmployeeEmail = true
                        
                    };
                    
                    Helpers.Messaging.Publish(valueTobeSendForMessageBus, null, true);

                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }


		/// <summary>
		/// Gets the employer account referral view.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public EmployerAccountReferralViewResponse GetEmployerAccountReferralView(EmployerAccountReferralViewRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new EmployerAccountReferralViewResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					switch (request.Criteria.FetchOption)
					{
						case CriteriaBase.FetchOptions.PagedList:
							var query = new EmployerAccountReferralQuery(Repositories.Core, request.Criteria);

							response.ReferralViewsPaged = query.Query().GetPagedList( x => x.AsDto(), request.Criteria.PageIndex, request.Criteria.PageSize );
							response.ReferralViewsPaged.ForEach(e => e.Suffix = LocaliseOrDefault(request, e.SuffixId, string.Empty));
							break;

						case CriteriaBase.FetchOptions.Single:
							var referral = Repositories.Core.FindById<EmployerAccountReferralView>(request.Criteria.Id);
							if (referral != null)
							{
								response.Referral = referral.AsDto();
								response.Referral.Suffix = LocaliseOrDefault(request, response.Referral.SuffixId, string.Empty);
							}
							break;
					}
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Approves the referral.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public EmployeeResponse ApproveReferral(EmployeeRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new EmployeeResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var employee = Repositories.Core.FindById<Employee>(request.EmployeeId);

					if (employee.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.EmployeeNotFound));
						Logger.Error(request.LogData(), response.Message);
						return response;
					}

					var referral = Repositories.Core.FindById<EmployerAccountReferralView>(request.EmployeeId)
							?? Repositories.Core.FindById<EmployerAccountReferralView>(request.BusinessUnitId)
							?? Repositories.Core.FindById<EmployerAccountReferralView>(employee.EmployerId);

					if (referral.IsNull() || (request.LockVersion.IsNotNullOrEmpty() && request.LockVersion != referral.AccountLockVersion))
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.LockVersionOutOfDate));
						return response;
					}

					if (request.EmployerApprovalType == 0)
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.ApprovalTypeNotSet));
						Logger.Error(request.LogData(), response.Message);
						return response;
					}

					var user = employee.Person.User;

					// Approve Employer
					var employerApproved = false;

					var businessUnit = Repositories.Core.FindById<BusinessUnit>(request.BusinessUnitId);

					if (businessUnit.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.BusinessUnitNotFound));
						Logger.Error(request.LogData(), response.Message);
						return response;
					}

					if (request.EmployerApprovalType == ApprovalType.HiringManager)
					{
						if (employee.Employer.ApprovalStatus.IsNotIn(ApprovalStatuses.Approved, ApprovalStatuses.None) || businessUnit.ApprovalStatus.IsNotIn(ApprovalStatuses.None, ApprovalStatuses.Approved))
						{
							response.ReferralResponseType = ApproveReferralResponseType.ParentCompanyLinkedToHiringManagerNotApproved;
							return response;
						}
					}

					if (request.EmployerApprovalType == ApprovalType.Employer)
					{
						employerApproved = true;
						employee.Employer.ApprovalStatus = ApprovalStatuses.Approved;
						LogAction(request, ActionTypes.ApproveEmployerReferral, typeof(Employer).Name, employee.EmployerId);

						// Approve the primary business unit with the employer
						var businessunit = Repositories.Core.BusinessUnits.FirstOrDefault(x => x.EmployerId == employee.EmployerId && x.IsPrimary);
						businessunit.ApprovalStatus = ApprovalStatuses.Approved;
						LogAction(request, ActionTypes.ApproveEmployerReferral, typeof(BusinessUnit).Name, businessunit.Id);

					}

					if (request.EmployerApprovalType == ApprovalType.BusinessUnit)
					{
						if (employee.Employer.ApprovalStatus.IsNotIn(ApprovalStatuses.Approved, ApprovalStatuses.None))
						{
							response.ReferralResponseType = ApproveReferralResponseType.ParentCompanyLinkedToBusinessUnitNotApproved;
							return response;
						}
						businessUnit.ApprovalStatus = ApprovalStatuses.Approved;
						businessUnit.ApprovalStatusReason = ApprovalStatusReason.Default;
						businessUnit.AwaitingApprovalDate = null;
						LogAction(request, ActionTypes.ApproveEmployerReferral, typeof(BusinessUnit).Name, businessUnit.Id);
					}

					employee.ApprovalStatus = ApprovalStatuses.Approved;
					employee.ApprovalStatusReason = ApprovalStatusReason.Default;
					employee.AwaitingApprovalDate = null;
					employee.ApprovalStatusChangedBy = request.UserContext.UserId;
					employee.ApprovalStatusChangedOn = DateTime.Now;

					// If approving previously denied employer - revert username to original so user can log back in
					var username = user.UserName;

					if (username.StartsWith("DENIED"))
					{
						var originalUserName = username.Substring(25);
						user.UserName = originalUserName;
					}

					var jobsAwaitingEmployerApproval = Repositories.Core.Jobs.Where(x => x.JobStatus == JobStatuses.AwaitingEmployerApproval && x.EmployeeId == employee.Id).ToList();

                    //MIK FVN - 4482 - Checking the jobs of the employer whether it requires staff check or not . If so including the check for Specific approval and also Check application setting to see if all postings have to be approved. This check includes for both red word and yellow word filtering too.
                    List<Job> ActiveJobs = new List<Job>();
                    foreach (var job in jobsAwaitingEmployerApproval)
                    {
                        var approvalChecks = AppSettings.ApprovalChecksForCustomerCreatedJobs;
                        job.JobStatus = JobStatuses.Active;

                        if (AppSettings.ApprovalDefaultForCustomerCreatedJobs == JobApprovalOptions.ApproveAll )
                        {
                            job.ApprovalStatus = ApprovalStatuses.WaitingApproval;
                            job.AwaitingApprovalOn = DateTime.Now;
                            job.AwaitingApprovalActionedBy = job.CreatedBy;
                            job.JobStatus = JobStatuses.Draft;
                        }

                        if ((job.CourtOrderedAffirmativeAction.GetValueOrDefault() && approvalChecks.Contains(JobApprovalCheck.CourtOrdered))
                                || (job.FederalContractor.GetValueOrDefault() && approvalChecks.Contains(JobApprovalCheck.FederalContractor))
                                || (job.ForeignLabourCertificationH2A.GetValueOrDefault() && approvalChecks.Contains(JobApprovalCheck.ForeignLaborH2A))
                                || (job.ForeignLabourCertificationH2B.GetValueOrDefault() && approvalChecks.Contains(JobApprovalCheck.ForeignLaborH2B))
                                || (job.ForeignLabourCertificationOther.GetValueOrDefault() && approvalChecks.Contains(JobApprovalCheck.ForeignLaborOther))
                                || (job.IsCommissionBased.GetValueOrDefault() && approvalChecks.Contains(JobApprovalCheck.ComissionBased))
                                || (job.IsSalaryAndCommissionBased.GetValueOrDefault() && approvalChecks.Contains(JobApprovalCheck.ComissionSalary)) || (job.SuitableForHomeWorker.GetValueOrDefault() && approvalChecks.Contains(JobApprovalCheck.HomeBased))
                                || (job.JobLocationType == JobLocationTypes.NoFixedLocation && approvalChecks.Contains(JobApprovalCheck.HomeBased))  || (job.CreditCheckRequired.GetValueOrDefault() && approvalChecks.Contains(JobApprovalCheck.CreditCheck))
                                || (job.CriminalBackgroundExclusionRequired.GetValueOrDefault() && approvalChecks.Contains(JobApprovalCheck.CriminalBackground))
                                || (job.JobSpecialRequirements.IsNotNullOrEmpty() && approvalChecks.Contains(JobApprovalCheck.SpecialRequirements)) || job.MinimumAgeReasonValue.GetValueOrDefault(0) == 4)
                        {
                            job.ApprovalStatus = ApprovalStatuses.WaitingApproval;
                            job.AwaitingApprovalOn = DateTime.Now;
                            job.AwaitingApprovalActionedBy = job.CreatedBy;
                            if (job.Posting.IsNotNull() && job.Posting.LensPostingId.IsNotNullOrEmpty())
                            {
                                job.JobStatus = JobStatuses.OnHold;
                                //MIK FVN-2973 added log action to display onhold activity for waiting approval
                                LogAction(request, ActionTypes.HoldJob, job);
                                job.HeldOn = DateTime.Now;
                                job.HeldBy = request.UserContext.UserId;
                                Helpers.Messaging.Publish(request.ToUnregisterPostingMessage(job.Posting.Id), updateIfExists: true, entityName: EntityTypes.Posting.ToString(), entityKey: job.Posting.Id);
                            }
                            else
                                job.JobStatus = JobStatuses.Draft;
                        }

                        if (job.RedProfanityWords.IsNotNullOrEmpty())
                        {
                            job.ApprovalStatus = ApprovalStatuses.None;
                            job.JobStatus = JobStatuses.Draft;
                        }

                        if (job.YellowProfanityWords.IsNotNullOrEmpty())
                        {
                            job.ApprovalStatus = ApprovalStatuses.WaitingApproval;
                            job.AwaitingApprovalOn = DateTime.Now;
                            job.AwaitingApprovalActionedBy = job.CreatedBy;
                            job.JobStatus = JobStatuses.Draft;
                        }
                        if(job.JobStatus == JobStatuses.Active)
                            ActiveJobs.Add(job);
                    }

					Repositories.Core.SaveChanges();

					foreach (var job in ActiveJobs)
					{
						var jobList = job.SplitJobByLocation(RuntimeContext, request, true);
						foreach (var jobToPost in jobList)
						{
							jobToPost.PostJob(RuntimeContext, request, user.Id, AppSettings.DefaultJobExpiry, resetRepository: false);
						}

						CreateAlertMessage(Constants.AlertMessageKeys.JobPosted, user.Id, DateTime.UtcNow.AddDays(2), MessageAudiences.User, MessageTypes.General, null, job.JobTitle, employee.Person.FirstName + " " + employee.Person.LastName);
					}

					Repositories.Core.SaveChanges(true);

					ActiveJobs.ForEach(x => LogAction(request, ActionTypes.PostJob, x));

					LogAction(request, ActionTypes.ApproveEmployeeReferral, typeof(Employee).Name, request.EmployeeId);

					// Send messages to any integration client
					if (AppSettings.IntegrationClient != IntegrationClient.Standalone)
					{
						var sendEmployee = true;
						var employer = Repositories.Core.FindById<Employer>(employee.EmployerId);

						if (employerApproved || employer.ExternalId.IsNullOrEmpty())
						{
							var employerRequest = new IntegrationRequestMessage
							{
								ActionerId = request.UserContext.UserId,
								SaveEmployerRequest = new SaveEmployerRequest
								{
									UserId = user.Id,
									EmployerId = employee.Employer.Id
								},
								SaveEmployeeRequest = new SaveEmployeeRequest
								{
									UserId = request.UserContext.UserId,
									PersonId = employee.PersonId
								},
								IntegrationPoint = IntegrationPoint.SaveEmployer
							};

							Helpers.Messaging.Publish(employerRequest);

							sendEmployee = false;
						}

						if (sendEmployee)
						{
							var employeeRequest = new IntegrationRequestMessage
							{
								ActionerId = request.UserContext.UserId,
								SaveEmployeeRequest = new SaveEmployeeRequest
								{
									UserId = request.UserContext.UserId,
									PersonId = employee.PersonId
								},
								IntegrationPoint = IntegrationPoint.SaveEmployee
							};
							Helpers.Messaging.Publish(employeeRequest);
						}

						user.PublishUpdateEmployeeStatusRequest(false, RuntimeContext, employee.Employer);
					}

					response.ReferralResponseType = ApproveReferralResponseType.Success;
					response.Employee = employee.AsDto();
					response.BusinessUnitApprovalStatus = businessUnit.ApprovalStatus;
					response.EmployerApprovalStatus = employee.Employer.ApprovalStatus;
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}



		/// <summary>
		/// Denies the referral.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public EmployeeResponse DenyReferral(EmployeeRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new EmployeeResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var employee = Repositories.Core.FindById<Employee>(request.EmployeeId);

					if (employee.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.EmployeeNotFound));
						Logger.Error(request.LogData(), response.Message);
						return response;
					}

					var referral = Repositories.Core.FindById<EmployerAccountReferralView>(request.EmployeeId)
							?? Repositories.Core.FindById<EmployerAccountReferralView>(request.BusinessUnitId)
							?? Repositories.Core.FindById<EmployerAccountReferralView>(employee.EmployerId);

					if (referral.IsNull() || (request.LockVersion.IsNotNullOrEmpty() && request.LockVersion != referral.AccountLockVersion))
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.LockVersionOutOfDate));
						return response;
					}

					if (request.EmployerApprovalType == 0)
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.ApprovalTypeNotSet));
						Logger.Error(request.LogData(), response.Message);
						return response;
					}

					var errorType = request.EmployerApprovalType == ApprovalType.HiringManager ? ValidateHiringManagerApprovalDenialReasons(request.ReferralStatusReasons) : ValidateApprovalDenialReasons(request.ReferralStatusReasons);

					if (errorType != ErrorTypes.Ok)
					{
						response.SetFailure(FormatErrorMessage(request, errorType));
						Logger.Error(request.LogData(), response.Message);
						return response;
					}

					employee.ApprovalStatus = ApprovalStatuses.Rejected;
					employee.ApprovalStatusChangedBy = request.UserContext.UserId;
					employee.ApprovalStatusChangedOn = DateTime.Now;

					if (!employee.Person.User.UserName.StartsWith("DENIED"))
					{
						// Change the user name so the user can reattempt in the future - IF THIS PREFIX CHANGES THEN YOU WILL ALSO NEED TO LOOK AT THE CODE IN approveReferral() WHERE THE PREFIX IS REMOVED
						var newUserName = string.Format("DENIED_{0}_{1}", DateTime.Now.ToString("yyyyMMddHHmmssfff"), employee.Person.User.UserName);
						employee.Person.User.UserName = newUserName;
					}

					LogAction(request, ActionTypes.DenyEmployeeReferral, typeof(Employee).Name, employee.Id);
					var employerRejected = false;

					if (request.EmployerApprovalType == ApprovalType.HiringManager)
					{
						if (AppSettings.Theme == FocusThemes.Workforce && request.ReferralStatusReasons.IsNotNullOrEmpty() && request.ReferralStatusReasons.Count > 0)
						{
							request.ReferralStatusReasons.ForEach(x =>
							{
								var reason = new EmployeeReferralStatusReason { ReasonId = x.Key, OtherReasonText = x.Value, EmployeeId = employee.Id, EntityType = EntityTypes.Staff, ApprovalStatus = ApprovalStatuses.Rejected };
								Repositories.Core.Add(reason);
							});
						}
					}

                    var businessunit = Repositories.Core.FindById<BusinessUnit>(request.BusinessUnitId);

                    if (businessunit.IsNull())
                    {
                        response.SetFailure(FormatErrorMessage(request, ErrorTypes.BusinessUnitNotFound));
                        Logger.Error(request.LogData(), response.Message);
                        return response;
                    }

					if (request.EmployerApprovalType == ApprovalType.Employer)
					{
						// Deny Employer if they are awaiting approval
						if (employee.Employer.ApprovalStatus == ApprovalStatuses.WaitingApproval || employee.Employer.ApprovalStatus == ApprovalStatuses.OnHold)
						{
							employee.Employer.ApprovalStatus = ApprovalStatuses.Rejected;
							employerRejected = true;

                            // reject the primary business unit with the employer
                            businessunit.ApprovalStatus = ApprovalStatuses.Rejected;

							if (AppSettings.Theme == FocusThemes.Workforce && request.ReferralStatusReasons.IsNotNullOrEmpty() && request.ReferralStatusReasons.Count > 0)
							{
								request.ReferralStatusReasons.ForEach(x =>
								{
									var reason = new EmployerReferralStatusReason { ReasonId = x.Key, OtherReasonText = x.Value, EmployerId = employee.EmployerId, EntityType = EntityTypes.Employer, ApprovalStatus = ApprovalStatuses.Rejected };
									Repositories.Core.Add(reason);
								});
							}
						}
					}

					if (request.EmployerApprovalType == ApprovalType.BusinessUnit)
					{
						businessunit.ApprovalStatus = ApprovalStatuses.Rejected;
						LogAction(request, ActionTypes.HoldEmployerReferral, typeof(BusinessUnit).Name, businessunit.Id);

						if (AppSettings.Theme == FocusThemes.Workforce && request.ReferralStatusReasons.IsNotNullOrEmpty() && request.ReferralStatusReasons.Count > 0)
						{
							request.ReferralStatusReasons.ForEach(x =>
							{
								var reason = new BusinessUnitReferralStatusReason { ReasonId = x.Key, OtherReasonText = x.Value, BusinessUnitId = businessunit.Id, EntityType = EntityTypes.BusinessUnit, ApprovalStatus = ApprovalStatuses.Rejected };
								Repositories.Core.Add(reason);
							});
						}
					}

					Repositories.Core.SaveChanges(true);

					LogAction(request, ActionTypes.DenyEmployeeReferral, typeof(Employee).Name, employee.Id);

					if (request.EmployerApprovalType == ApprovalType.BusinessUnit)
						LogAction(request, ActionTypes.HoldEmployerReferral, typeof(BusinessUnit).Name, businessunit.Id);

					if (employerRejected)
						LogAction(request, ActionTypes.DenyEmployerReferral, typeof(Employer).Name, employee.EmployerId);

					employee.Person.User.PublishUpdateEmployeeStatusRequest(true, RuntimeContext, employee.Employer);

					response.BusinessUnitApprovalStatus = businessunit.ApprovalStatus;
					response.Employee = employee.AsDto();
					response.EmployerApprovalStatus = employee.Employer.ApprovalStatus;
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Holds the referral.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public EmployeeResponse HoldReferral(EmployeeRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new EmployeeResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var employee = Repositories.Core.FindById<Employee>(request.EmployeeId);
					if (employee.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.EmployeeNotFound));
						Logger.Error(request.LogData(), response.Message);
						return response;
					}

					var referral = Repositories.Core.FindById<EmployerAccountReferralView>(request.EmployeeId)
							?? Repositories.Core.FindById<EmployerAccountReferralView>(request.BusinessUnitId)
							?? Repositories.Core.FindById<EmployerAccountReferralView>(employee.EmployerId);

					if (referral.IsNull() || (request.LockVersion.IsNotNullOrEmpty() && request.LockVersion != referral.AccountLockVersion))
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.LockVersionOutOfDate));
						return response;
					}

					if (request.EmployerApprovalType == 0)
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.ApprovalTypeNotSet));
						Logger.Error(request.LogData(), response.Message);
						return response;
					}

					var errorType = request.EmployerApprovalType == ApprovalType.HiringManager ? ValidateHiringManagerApprovalHoldReasons(request.ReferralStatusReasons) : ValidateApprovalHoldReasons(request.ReferralStatusReasons);

					if (errorType != ErrorTypes.Ok)
					{
						response.SetFailure(FormatErrorMessage(request, errorType));
						Logger.Error(request.LogData(), response.Message);
						return response;
					}

					employee.ApprovalStatus = ApprovalStatuses.OnHold;
					employee.ApprovalStatusChangedBy = request.UserContext.UserId;
					employee.ApprovalStatusChangedOn = DateTime.Now;
					LogAction(request, ActionTypes.HoldEmployeeReferral, typeof(Employee).Name, employee.Id);

					var businessUnit = Repositories.Core.FindById<BusinessUnit>(request.BusinessUnitId);

					if (businessUnit.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.BusinessUnitNotFound));
						Logger.Error(request.LogData(), response.Message);
						return response;
					}

					if (request.EmployerApprovalType == ApprovalType.HiringManager)
					{
						if (AppSettings.Theme == FocusThemes.Workforce && request.ReferralStatusReasons.IsNotNullOrEmpty() && request.ReferralStatusReasons.Count > 0)
						{
							request.ReferralStatusReasons.ForEach(x =>
							{
								var reason = new EmployeeReferralStatusReason { ReasonId = x.Key, OtherReasonText = x.Value, EmployeeId = request.EmployeeId, EntityType = EntityTypes.Staff, ApprovalStatus = ApprovalStatuses.OnHold };
								Repositories.Core.Add(reason);
							});
						}
					}

					if (request.EmployerApprovalType == ApprovalType.BusinessUnit)
					{
						businessUnit.ApprovalStatus = ApprovalStatuses.OnHold;

						if (AppSettings.Theme == FocusThemes.Workforce && request.ReferralStatusReasons.IsNotNullOrEmpty() && request.ReferralStatusReasons.Count > 0)
						{
							request.ReferralStatusReasons.ForEach(x =>
							{
								var reason = new BusinessUnitReferralStatusReason { ReasonId = x.Key, OtherReasonText = x.Value, BusinessUnitId = businessUnit.Id, EntityType = EntityTypes.BusinessUnit, ApprovalStatus = ApprovalStatuses.OnHold };
								Repositories.Core.Add(reason);
							});
						}

						LogAction(request, ActionTypes.HoldEmployerReferral, typeof(BusinessUnit).Name, businessUnit.Id);
					}

					if (request.EmployerApprovalType == ApprovalType.Employer)
					{
						// Hold Employer if they are awaiting approval

						var employerOnHold = false;

						if (employee.Employer.ApprovalStatus.IsIn(ApprovalStatuses.WaitingApproval, ApprovalStatuses.Rejected))
						{
							employee.Employer.ApprovalStatus = ApprovalStatuses.OnHold;
							employerOnHold = true;

                            // Hold the primary business unit with the employer
                            businessUnit.ApprovalStatus = ApprovalStatuses.OnHold;
						}

						if (AppSettings.Theme == FocusThemes.Workforce && request.ReferralStatusReasons.IsNotNullOrEmpty() && request.ReferralStatusReasons.Count > 0)
						{
							request.ReferralStatusReasons.ForEach(x =>
							{
								var reason = new EmployerReferralStatusReason { ReasonId = x.Key, OtherReasonText = x.Value, EmployerId = employee.EmployerId, EntityType = EntityTypes.Employer, ApprovalStatus = ApprovalStatuses.OnHold };
								Repositories.Core.Add(reason);
							});
						}

						if (employerOnHold) LogAction(request, ActionTypes.HoldEmployerReferral, typeof(Employer).Name, employee.EmployerId);
					}

					Repositories.Core.SaveChanges(true);

					response.Employee = employee.AsDto();
					response.BusinessUnitApprovalStatus = businessUnit.ApprovalStatus;
					response.EmployerApprovalStatus = employee.ApprovalStatus;

				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Assigns the activity to multiple employees.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public AssignEmployeeActivityResponse AssignActivityToMultipleEmployees(AssignEmployeeActivityRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new AssignEmployeeActivityResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				foreach (var employeeId in request.EmployeeIds)
				{
					request.EmployeeId = employeeId;
					response = AssignActivity(request, response);

					if (response.Acknowledgement == Focus.Core.Messages.AcknowledgementType.Failure)
						return response;
				}

				return response;
			}
		}

		/// <summary>
		/// Assigns the activity.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public AssignEmployeeActivityResponse AssignActivity(AssignEmployeeActivityRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new AssignEmployeeActivityResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				response = AssignActivity(request, response);

				return response;
			}
		}

		/// <summary>
		/// Blocks the account.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public EmployeeResponse BlockAccount(EmployeeRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new EmployeeResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var employee = Repositories.Core.FindById<Employee>(request.Id);

					if (employee.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.EmployeeNotFound));
						Logger.Info(request.LogData(), string.Format("The employee '{0}' was not found.", request.Id));
						return response;
					}

					employee.Person.User.Blocked = true;

					Repositories.Core.SaveChanges(true);

					LogAction(request, ActionTypes.BlockEmployeeAccount, typeof(Employee).Name, employee.Id);

					employee.Person.User.PublishUpdateEmployeeStatusRequest(true, RuntimeContext, employee.Employer);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Unblocks the account.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public EmployeeResponse UnblockAccount(EmployeeRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new EmployeeResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var employee = Repositories.Core.FindById<Employee>(request.Id);

					if (employee.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.EmployeeNotFound));
						Logger.Info(request.LogData(), string.Format("The employee '{0}' was not found.", request.Id));
						return response;
					}

					employee.Person.User.Blocked = false;

                    //FVN-6014
                    if (employee.Person.User.BlockedReason.IsNotNull())
                    {
                        if (employee.Person.User.BlockedReason.Equals(BlockedReason.FailedPasswordReset))
                            employee.Person.User.PasswordAttempts = 0;
                        //Reset blocked reason - FVN-5922
                        employee.Person.User.BlockedReason = null;
                    }

					Repositories.Core.SaveChanges(true);

					LogAction(request, ActionTypes.UnblockEmployeeAccount, typeof(Employee).Name, employee.Id);

					employee.Person.User.PublishUpdateEmployeeStatusRequest(false, RuntimeContext, employee.Employer);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the employee user details.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public EmployeeUserDetailsResponse GetEmployeeUserDetails(EmployeeUserDetailsRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new EmployeeUserDetailsResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					if (!request.EmployeeId.HasValue)
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.InvalidEmployee));
						Logger.Info(request.LogData(), string.Format("No employee id provided."));
					}

					var employee = Repositories.Core.FindById<Employee>(request.EmployeeId);

					if (employee.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.EmployeeNotFound));
						Logger.Info(request.LogData(), string.Format("EmployeeId '{0}' not found.", request.EmployeeId));
					}

					response.UserId = employee.Person.User.Id;

					response.PersonDetails = employee.Person.AsDto();

					var address = employee.Person.PersonAddresses.Where(x => x.IsPrimary).Select(x => x.AsDto()).FirstOrDefault();
					if (address.IsNotNull()) response.AddressDetails = address;

					var phone = employee.Person.PhoneNumbers.Where(x => x.IsPrimary).Select(x => x.AsDto()).FirstOrDefault();
					if (phone.IsNotNull()) response.PrimaryPhoneNumber = phone;

					// Alternate numbers 
					var alternatePhones = employee.Person.PhoneNumbers.Where(x => x.IsPrimary == false).Select(x => x.AsDto()).ToList();
					if (alternatePhones.IsNotNullOrEmpty())
					{
						if (alternatePhones.Count > 0 && alternatePhones[0].IsNotNull())
							response.AlternatePhoneNumber1 = alternatePhones[0];

						if (alternatePhones.Count > 1 && alternatePhones[1].IsNotNull())
							response.AlternatePhoneNumber2 = alternatePhones[1];
					}
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}
				return response;
			}
		}

		/// <summary>
		/// Gets the employee business unit view.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public EmployeeBusinessUnitViewResponse GetEmployeeBusinessUnitView(EmployeeBusinessUnitViewRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new EmployeeBusinessUnitViewResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var query = Repositories.Core.EmployeeBusinessUnits;

					if (request.Criteria.Id.HasValue)
						query = query.Where(x => x.Id == request.Criteria.Id);

					if (request.Criteria.EmployeeId.HasValue)
						query = query.Where(x => x.EmployeeId == request.Criteria.EmployeeId);

					var queryResult = query.Select(result => new EmployeeBusinessUnitView
																																															{
																																																Id = result.Id,
																																																EmployeeId = result.EmployeeId,
																																																BusinessUnitId = result.BusinessUnitId,
																																																BusinessUnitName = result.BusinessUnit.Name,
																																																IsDefault = result.Default,
																																																BusinessUnitApprovalStatus = result.BusinessUnit.ApprovalStatus,
																																																EmployerApprovalStatus = result.BusinessUnit.Employer.ApprovalStatus
																																															});

					queryResult = queryResult.OrderBy(x => x.BusinessUnitName);

					switch (request.Criteria.FetchOption)
					{
						case CriteriaBase.FetchOptions.PagedList:
							response.EmployeeBusinessUnitViewsPaged = new PagedList<EmployeeBusinessUnitView>(queryResult, request.Criteria.PageIndex, request.Criteria.PageSize);
							break;

						case CriteriaBase.FetchOptions.List:
							response.EmployeeBusinessUnitViews = queryResult.ToList();
							break;

						default:
							response.EmployeeBusinessUnitView = queryResult.FirstOrDefault();
							break;
					}
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Assigns the employee to business unit.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public AssignEmployeeToBusinessUnitResponse AssignEmployeeToBusinessUnit(AssignEmployeeToBusinessUnitRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new AssignEmployeeToBusinessUnitResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var employee = Repositories.Core.FindById<Employee>(request.EmployeeId);

					// If we cant find the employee with this Id something is wrong
					if (employee.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.EmployeeNotFound));
						Logger.Info(request.LogData(),
								string.Format("The employee '{0}' was not found.", request.EmployeeId));

						return response;
					}

					// Check to see if the employee already has access to the business unit
					if (employee.EmployeeBusinessUnits.Count > 0 && employee.EmployeeBusinessUnits.Any(x => x.BusinessUnitId == request.BusinessUnitId))
					{
						// Nothing to do
						return response;
					}

					var businessUnit = Repositories.Core.FindById<BusinessUnit>(request.BusinessUnitId);

					// If we cant find the business unit with this Id something is wrong
					if (businessUnit.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.BusinessUnitNotFound));
						Logger.Info(request.LogData(),
								string.Format("The business unit '{0}' was not found.", request.BusinessUnitId));

						return response;
					}

					var employeeBusinessUnit = new EmployeeBusinessUnit
																																	{
																																		BusinessUnit = businessUnit,
																																		Default = (employee.EmployeeBusinessUnits.Count == 0)
																																	};

					employee.EmployeeBusinessUnits.Add(employeeBusinessUnit);

					Repositories.Core.SaveChanges(true);

					LogAction(request, ActionTypes.AssignBusinessUnitToEmployee, typeof(Employee).Name, employee.Id, businessUnit.Id);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Sets the business unit as default.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public SetBusinessUnitAsDefaultResponse SetBusinessUnitAsDefault(SetBusinessUnitAsDefaultRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new SetBusinessUnitAsDefaultResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var employee = Repositories.Core.FindById<Employee>(request.EmployeeId);

					// If we cant find the employee with this Id something is wrong
					if (employee.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.EmployeeNotFound));
						Logger.Info(request.LogData(),
								string.Format("The employee '{0}' was not found.", request.EmployeeId));

						return response;
					}

					// Check to see if the employee has access to the business unit
					if (employee.EmployeeBusinessUnits.Count == 0 || (employee.EmployeeBusinessUnits.All(x => x.BusinessUnitId != request.BusinessUnitId)))
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.BusinessUnitNotFound));
						Logger.Info(request.LogData(),
								string.Format("The employee '{0}' does not have access to business unit '{1}'.", request.EmployeeId, request.BusinessUnitId));

						return response;
					}


					// Reset current default business units
					var currentDefaultBusinessUnits = employee.EmployeeBusinessUnits.Where(x => x.Default);

					foreach (var currentDefaultBusinessUnit in currentDefaultBusinessUnits)
						currentDefaultBusinessUnit.Default = false;

					var newDefaultBusinessUnit = employee.EmployeeBusinessUnits.Where((x => x.BusinessUnitId == request.BusinessUnitId)).FirstOrDefault();

					if (newDefaultBusinessUnit.IsNotNull()) newDefaultBusinessUnit.Default = true;

					Repositories.Core.SaveChanges(true);

					LogAction(request, ActionTypes.DefaultBusinessUnitChanged, typeof(Employee).Name, employee.Id, request.BusinessUnitId);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the hiring manager profile.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public EmployeeBusinessUnitInfoResponse GetHiringManagerProfile(EmployeeBusinessUnitInfoRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new EmployeeBusinessUnitInfoResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var employeeBusinessUnit = Repositories.Core.FindById<EmployeeBusinessUnit>(request.Id);
					var employee = employeeBusinessUnit.Employee;
					var businessUnit = employeeBusinessUnit.BusinessUnit;

					response.EmployeePhoneNumber = employee.Person.PhoneNumbers.Select(p => p.AsDto()).ToList();
					response.EmployeeId = employee.Id;
					response.EmployeeName = employee.Person.FirstName + " " + employee.Person.LastName;
					if (employee.Person.SuffixId.HasValue) response.EmployeeName = response.EmployeeName + " " + LocaliseOrDefault(request, employee.Person.SuffixId.Value, String.Empty);
					response.EmployeeEmail = employee.Person.EmailAddress;
					response.BusinessUnitName = businessUnit.Name;
					response.BusinessUnitId = employeeBusinessUnit.BusinessUnitId;
					response.BusinessUnit = businessUnit.AsDto();
					response.BusinessUnitAddresses = businessUnit.BusinessUnitAddresses.Select(bua => bua.AsDto()).ToList();
					response.EmployerId = employee.EmployerId;

					#region Logins Panel

					var user = employee.Person.User;
					var userId = user.Id;

					response.DaysSinceRegistration = (DateTime.Now - user.CreatedOn).Days;
					response.LastLogin = user.LoggedInOn;

					var logInActionEvent = Repositories.Core.ActionTypes.FirstOrDefault(x => x.Name == ActionTypes.LogIn.ToString());
					var logInActionEventId = logInActionEvent == null ? 0 : logInActionEvent.Id;

					response.LoginsSinceRegistration = Repositories.Core.ActionEvents.Count(x => x.EntityId == user.Id && x.ActionTypeId == logInActionEventId);
					response.Logins7Days = Repositories.Core.ActionEvents.Count(x => x.EntityId == userId && x.ActionTypeId == logInActionEventId && x.ActionedOn > DateTime.Now.AddDays(-7));

					#endregion

					#region Referral Outcomes Panel

					var candidateActionEvents = Repositories.Core.ActionEvents.Where(ae => (Repositories.Core.Jobs.Where(j => j.EmployeeId == employee.Id).Select(j => j.Id)).Contains((long)ae.EntityIdAdditional01));

					response.ApplicantsDidNotApply = CountActionsByActionType(ActionTypes.UpdateApplicationStatusToDidNotApply, candidateActionEvents);
					response.ApplicantsFailedToReportToJob = CountActionsByActionType(ActionTypes.UpdateApplicationStatusToFailedToReportToJob, candidateActionEvents);
					response.ApplicantsFailedToRespondToInvitation = CountActionsByActionType(ActionTypes.UpdateApplicationStatusToFailedToRespondToInvitation, candidateActionEvents);
					response.ApplicantsFailedToShow = CountActionsByActionType(ActionTypes.UpdateApplicationStatusToFailedToShow, candidateActionEvents);
					response.ApplicantsFoundJobFromOtherSource = CountActionsByActionType(ActionTypes.UpdateApplicationStatusToFoundJobFromOtherSource, candidateActionEvents);
					response.ApplicantsHired = CountActionsByActionType(ActionTypes.UpdateApplicationStatusToHired, candidateActionEvents);
					response.ApplicantsInterviewDenied = CountActionsByActionType(ActionTypes.UpdateApplicationStatusToInterviewDenied, candidateActionEvents);
					response.ApplicantsInterviewed = CountActionsByActionType(ActionTypes.UpdateApplicationStatusToInterviewScheduled, candidateActionEvents);
					response.ApplicantsJobAlreadyFilled = CountActionsByActionType(ActionTypes.UpdateApplicationStatusToJobAlreadyFilled, candidateActionEvents);
					response.ApplicantsNewApplicant = CountActionsByActionType(ActionTypes.UpdateApplicationStatusToNewApplicant, candidateActionEvents);
					response.ApplicantsRejected = CountActionsByActionType(ActionTypes.UpdateApplicationStatusToNotHired, candidateActionEvents);
					response.ApplicantsNotQualified = CountActionsByActionType(ActionTypes.UpdateApplicationStatusToNotQualified, candidateActionEvents);
					response.ApplicantsNotYetPlaced = CountActionsByActionType(ActionTypes.UpdateApplicationStatusToNotYetPlaced, candidateActionEvents);
					response.ApplicantsRecommended = CountActionsByActionType(ActionTypes.UpdateApplicationStatusToRecommended, candidateActionEvents);
					response.ApplicantsRefused = CountActionsByActionType(ActionTypes.UpdateApplicationStatusToRefusedOffer, candidateActionEvents);
					response.ApplicantsRefusedReferral = CountActionsByActionType(ActionTypes.UpdateApplicationStatusToRefusedReferral, candidateActionEvents);
					response.ApplicantsUnderConsideration = CountActionsByActionType(ActionTypes.UpdateApplicationStatusToUnderConsideration, candidateActionEvents);

					#endregion

					#region Activity summary

					response.ApplicantsSeven = Repositories.Core.ApplicationViews
																																																	.Count(x => x.EmployeeId == employee.Id
																																																													&& x.BusinessUnitId == businessUnit.Id
																																																													&& x.CandidateApplicationReceivedOn > DateTime.Now.AddDays(-7)
																																																													&& x.CandidateApplicationApprovalStatus == ApprovalStatuses.Approved);

					response.ApplicantsThirty = Repositories.Core.ApplicationViews
																																																	 .Count(x => x.EmployeeId == employee.Id
																																																													 && x.BusinessUnitId == businessUnit.Id
																																																													 && x.CandidateApplicationReceivedOn > DateTime.Now.AddDays(-30)
																																																													 && x.CandidateApplicationApprovalStatus == ApprovalStatuses.Approved);

					//TODO: Matches/3-5 matches/3-5 matches employer did not view

					var sevenDaysAgo = DateTime.Now.Date.AddDays(-7);
					var thirtyDaysAgo = DateTime.Now.Date.AddDays(-30);
					var gracePeriodBoundary = DateTime.Now.Date.AddDays(AppSettings.NotRespondingToEmployerInvitesGracePeriodDays * -1);

					var invitesToApply = Repositories.Core.Query<InviteToApply>().Where(x => x.CreatedBy == userId && (x.CreatedOn > thirtyDaysAgo || x.UpdatedOn > thirtyDaysAgo)).Select(x => x.AsDto()).ToList();

					response.InvitationsSentThirty = invitesToApply.Count(x => x.CreatedOn > thirtyDaysAgo);
					response.InvitationsSentSeven = invitesToApply.Count(x => x.CreatedOn > sevenDaysAgo);

					response.InvitedSeekersViewedJobThirty = invitesToApply.Count(x => x.Viewed);
					response.InvitedSeekersViewedJobSeven = invitesToApply.Count(x => x.Viewed && x.UpdatedOn > sevenDaysAgo);

					response.InvitedSeekersNotViewJobThirty = invitesToApply.Count(x => !x.Viewed && x.CreatedOn > thirtyDaysAgo && x.CreatedOn < gracePeriodBoundary);
					response.InvitedSeekersNotViewJobSeven = invitesToApply.Count(x => !x.Viewed && x.CreatedOn > sevenDaysAgo && x.CreatedOn < gracePeriodBoundary);

					response.InvitedSeekersClickedHowToApplySeven = Repositories.Core.CandidateClickHowToApplyView.Count(x => (Repositories.Core.Jobs.Where(j => j.EmployeeId == employee.Id).Select(j => j.Id)).Contains((long)x.JobId) && x.ActionedOn > sevenDaysAgo);
					response.InvitedSeekersClickedHowToApplyThirty = Repositories.Core.CandidateClickHowToApplyView.Count(x => (Repositories.Core.Jobs.Where(j => j.EmployeeId == employee.Id).Select(j => j.Id)).Contains((long)x.JobId) && x.ActionedOn > thirtyDaysAgo);

					response.InvitedSeekersNotClickHowToApplySeven = Repositories.Core.CandidateDidNotClickHowToApplyView.Count(x => x.EmployeeId == employee.Id && x.ActionedOn > sevenDaysAgo && x.ActionedOn < gracePeriodBoundary);
					response.InvitedSeekersNotClickHowToApplyThirty = Repositories.Core.CandidateDidNotClickHowToApplyView.Count(x => x.EmployeeId == employee.Id && x.ActionedOn > thirtyDaysAgo && x.ActionedOn < gracePeriodBoundary);

					#endregion

					#region  Survey Panel
					var surveyResults = Repositories.Core.PostingSurveys.Where(ps => (Repositories.Core.Jobs.Where(j => j.EmployeeId == employee.Id).Select(j => j.Id)).Contains(ps.JobId));

					var lookup = Helpers.Lookup.GetLookup(LookupTypes.SatisfactionLevels);
					//GetCachedLookup(request, LookupTypes.SatisfactionLevels).ToList();

					var vsId = lookup.Single(y => y.Key == Constants.CodeItemKeys.SatisfactionLevels.VerySatisfied).Id;
					var ssId = lookup.Single(y => y.Key == Constants.CodeItemKeys.SatisfactionLevels.SomewhatSatisfied).Id;
					var sdId = lookup.Single(y => y.Key == Constants.CodeItemKeys.SatisfactionLevels.SomewhatDissatisfied).Id;
					var vdId = lookup.Single(y => y.Key == Constants.CodeItemKeys.SatisfactionLevels.VeryDissatisfied).Id;

                    response.SurveyVerySatisfied = surveyResults.Count(x => x.SatisfactionLevel == vsId);
					response.SurveyVeryDissatified = surveyResults.Count(x => x.SatisfactionLevel== vdId);
					response.SurveySomewhatSatisfied = surveyResults.Count(x => x.SatisfactionLevel == ssId);
					response.SurveySomewhatDissatified = surveyResults.Count(x => x.SatisfactionLevel == sdId);

					response.SurveyDidNotInterview = surveyResults.Count(x => x.DidInterview == false);
					response.SurveyInterviewed = surveyResults.Count(x => (bool)x.DidInterview);
					response.SurveyDidNotHire = surveyResults.Count(x => x.DidHire == false);
					response.SurveyHired = surveyResults.Count(x => (bool)x.DidHire);

					#endregion
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the hiring manager activity view.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public EmployeeBusinessUnitActivityResponse GetHiringManagerActivityView(EmployeeBusinessUnitActivityRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new EmployeeBusinessUnitActivityResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var query = Repositories.Core.Query<HiringManagerActivityView>().Where(x => x.EmployeeBuId == request.Criteria.Id);
                        
                    if(request.Criteria.DaysBack > 0)  
                        query = query.Where(x => x.ActionedOn >= DateTime.Now.Date.AddDays(request.Criteria.DaysBack * -1));

					if (request.Criteria.UserId.HasValue)
						query = query.Where(x => x.UserId == request.Criteria.UserId);

					if (request.Criteria.Action.HasValue)
						query = query.Where(x => x.ActionName == request.Criteria.Action.ToString());

					switch (request.Criteria.OrderBy)
					{
						case Constants.SortOrders.ActivityDateAsc:
							query = query.OrderBy(x => x.ActionedOn);
							break;

						case Constants.SortOrders.ActivityUsernameAsc:
							query = query.OrderBy(x => x.EmployeeLastName);
							break;

						case Constants.SortOrders.ActivityUsernameDesc:
							query = query.OrderByDescending(x => x.EmployeeLastName);
							break;

						case Constants.SortOrders.ActivityActionAsc:
							query = query.OrderBy(x => x.ActionName);
							break;

						case Constants.SortOrders.ActivityActionDesc:
							query = query.OrderByDescending(x => x.ActionName);
							break;

						default:
							query = query.OrderByDescending(x => x.ActionedOn);
							break;
					}

					var queryResults = query.Select(result => new HiringManagerActivityViewDto
					{
                        Id = result.Id,
						EmployeeFirstName = result.EmployeeFirstName,
						EmployeeLastName = result.EmployeeLastName,
						ActionName = result.ActionName,
						ActionedOn = result.ActionedOn,
						EntityIdAdditional01 = result.EntityIdAdditional01,
						JobId = result.JobId,
						JobTitle = result.JobTitle,
						AdditionalName = result.AdditionalName,
                        UserId = result.UserId
					});

					switch (request.Criteria.FetchOption)
					{
						case CriteriaBase.FetchOptions.PagedList:
							response.EmployeeBusinessUnitActivityPaged = new PagedList<HiringManagerActivityViewDto>(queryResults, request.Criteria.PageIndex, request.Criteria.PageSize);
							break;

						case CriteriaBase.FetchOptions.List:
							response.EmployeeBusinessActivityViews = new List<HiringManagerActivityViewDto>(queryResults);
							break;

						default:
							response.EmployeeBusinessUnitActivityPaged = new PagedList<HiringManagerActivityViewDto>(queryResults, request.Criteria.PageIndex, request.Criteria.PageSize);
							break;
					}
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}
				return response;
			}
		}

		/// <summary>
		/// Gets the employee business unit activity users.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public EmployeeBusinessUnitActivityResponse GetEmployeeBusinessUnitActivityUsers(EmployeeBusinessUnitActivityRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new EmployeeBusinessUnitActivityResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;
				try
				{
					response.EmployeeBusinessUnitActivityUsers = Repositories.Core.Query<HiringManagerActivityView>().Where(x => x.EmployeeBuId == request.EmployeeBuId).Select(
							x => new HiringManagerActivityViewDto { UserId = x.UserId, EmployeeLastName = x.EmployeeLastName, EmployeeFirstName = x.EmployeeFirstName }).OrderBy(x => x.EmployeeLastName).Distinct().ToList();
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}
				return response;
			}
		}

		/// <summary>
		/// Creates the hiring manager homepage alert.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public CreateHiringManagerHomepageAlertResponse CreateHiringManagerHomepageAlert(CreateHiringManagerHomepageAlertRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new CreateHiringManagerHomepageAlertResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var person = Repositories.Core.FindById<Person>(request.UserContext.PersonId);

					// Currently only deals with creating messages for ALL Hiring Managers
					CreateAlertMessage(Constants.AlertMessageKeys.HiringManagerMessage, request.HomepageAlert.IsSystemAlert, null,
						request.HomepageAlert.ExpiryDate,
						MessageAudiences.AllTalentUsers, MessageTypes.General, null,
						request.HomepageAlert.IsSystemAlert ? LocaliseOrDefault(request, "System.Text", "System") : person.FirstName,
						request.HomepageAlert.IsSystemAlert ? LocaliseOrDefault(request, "Administrator.Text", "Administrator") : person.LastName,
						LocaliseOrDefault(request, "ReminderCreatedOnDate.Format", "{0:MMM d, yyyy}", DateTime.Now),
						request.HomepageAlert.Message);

					LogAction(request, ActionTypes.CreateHiringManagerHomepageAlerts, null);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.CreateHiringManagerHomepageAlertsFailure), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Assigns the activity.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <param name="response">The response.</param>
		/// <returns></returns>
		private AssignEmployeeActivityResponse AssignActivity(AssignEmployeeActivityRequest request, AssignEmployeeActivityResponse response)
		{
			try
			{
				var employee = Repositories.Core.Query<Employee>().SingleOrDefault(x => x.Id == request.EmployeeId);

				// Create a candidate as it doesn't exist
				if (employee == null)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.EmployeeNotFound));
					Logger.Info(request.LogData(), string.Format("The employee '{0}' was not found.", request.EmployeeId));

					return response;
				}
				var activity = Repositories.Configuration.FindById<Activity>(request.ActivityId);
				if (activity.IsNull())
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.ActivityNotFound));
					Logger.Info(request.LogData(), string.Format("The activity '{0}' was not found.", request.ActivityId));

					return response;
				}


                //// validate office
                //var EemployeePersonId = Convert.ToInt64(Repositories.Core.Employees.Where(x => x.Id == request.EmployeeId).Select(y => y.PersonId).FirstOrDefault().ToString());
                //var userOffice = Repositories.Core.PersonOfficeMappers.Where(x => x.PersonId == EemployeePersonId).ToList();//.Select(mapper => mapper.OfficeId).Distinct();

                //var staffOffice = Repositories.Core.PersonOfficeMappers.Where(x => x.PersonId == request.UserContext.PersonId).ToList();
                //var staffOfficeCount = staffOffice.Count();
                //bool assighFlag = false;
                //if (staffOffice.Count > 0)
                //{
                //    if (staffOfficeCount == 1)
                //    {
                //        var isStateWide = staffOffice.Select(x => x.StateId).FirstOrDefault();
                //        var isOffice = staffOffice.Select(x => x.OfficeId).FirstOrDefault();
                //        if (isStateWide.IsNotNull() && isOffice.IsNull())
                //            assighFlag = true;
                //    }
                //    if (!assighFlag)
                //    {
                //        var isAssignedOffice = staffOffice.Where(x => userOffice.Any(y => y.OfficeId == x.OfficeId)).ToList();
                //        var isStaffcount = isAssignedOffice.Count();
                //        if (isAssignedOffice.Any())
                //            assighFlag = true;
                //    }

                //}

                //if (!assighFlag)
                //{
                //    //response.SetFailure(FormatErrorMessage(request, ErrorTypes.ValidationFailed));
                //    Logger.Info(request.LogData(), string.Format("The staff '{0}' do not have the permission.", request.UserContext.PersonId));
                //    response.Message = "-1";
                //    return response;

                //}



				if (AppSettings.IntegrationClient != IntegrationClient.Standalone)
				{
					var intRequest = new AssignEmployerActivityRequest
					{
						UserId = request.UserContext.UserId,
						EmployerId = Repositories.Core.Employers.Where(e => e.Id == employee.EmployerId).Select(e => e.ExternalId).FirstOrDefault(),
						EmployeeId = Repositories.Core.Users.Where(u => u.PersonId == employee.PersonId).Select(u => u.ExternalId).FirstOrDefault(),
						ActivityId = activity.ExternalId.ToString(CultureInfo.InvariantCulture)
					};

					Helpers.Messaging.Publish(new IntegrationRequestMessage
					{
						ActionerId = request.UserContext.UserId,
						IntegrationPoint = IntegrationPoint.AssignEmployerActivity,
						AssignEmployerActivityRequest = intRequest
					});
				}

				/*
				var integrationRequest = new Integration.Client.AssignEmployerActivityRequest
				{
						ClientTag = request.ClientTag,
						RequestId = request.RequestId,
						ExternalUserId = request.UserContext.ExternalUserId,
						ExternalUserName = request.UserContext.ExternalUserName,
						ExternalPassword = request.UserContext.ExternalPassword,
						ExternalOfficeId = request.UserContext.ExternalOfficeId,
						ActivityId = activity.ExternalId,
						ActivityDate = DateTime.Now,//??
						EmployerId = employee.Employer.Id,
						EmployeeId = employee.Id
						//, AdminId = ???
				};

				var integrationResponse = Repositories.Client.AssignEmployerActivity(integrationRequest);

				if (integrationResponse.Acknowledgement == AcknowledgementType.Failure)
				{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.IntegrationClientAssignEmployerActivityFailure), integrationResponse.Exception);
						Logger.Error(request.LogData(), integrationResponse.Message, integrationResponse.Exception);
						return response;
				}
				*/

				LogAction(request, ActionTypes.AssignActivityToEmployee, typeof(Employee).Name, employee.Id, request.ActivityId, null, null, request.ActionedDate);
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}

			return response;
		}

		/// <summary>
		/// Validates the approval denial reasons.
		/// </summary>
		/// <param name="reasons">The reasons.</param>
		/// <returns></returns>
		private ErrorTypes ValidateApprovalDenialReasons(List<KeyValuePair<long, string>> reasons)
		{
			return Helpers.Employee.ValidateReferralStatusReasons(reasons, LookupTypes.EmployerDenialReasons, "EmployerDenialReasons.OtherReason");
		}

		/// <summary>
		/// Validates the hiring manager approval denial reasons.
		/// </summary>
		/// <param name="reasons">The reasons.</param>
		/// <returns></returns>
		private ErrorTypes ValidateHiringManagerApprovalDenialReasons(List<KeyValuePair<long, string>> reasons)
		{
			return Helpers.Employee.ValidateReferralStatusReasons(reasons, LookupTypes.HiringManagerDenialReasons, "HiringManagerDenialReasons.OtherReason");
		}

		/// <summary>
		/// Validates the approval hold reasons.
		/// </summary>
		/// <param name="reasons">The reasons.</param>
		/// <returns></returns>
		private ErrorTypes ValidateApprovalHoldReasons(List<KeyValuePair<long, string>> reasons)
		{
			return Helpers.Employee.ValidateReferralStatusReasons(reasons, LookupTypes.EmployerOnHoldReasons, "EmployerOnHoldReasons.Other");
		}

		/// <summary>
		/// Validates the hiring manager approval hold reasons.
		/// </summary>
		/// <param name="reasons">The reasons.</param>
		/// <returns></returns>
		private ErrorTypes ValidateHiringManagerApprovalHoldReasons(List<KeyValuePair<long, string>> reasons)
		{
			return Helpers.Employee.ValidateReferralStatusReasons(reasons, LookupTypes.HiringManagerOnHoldReasons, "HiringManagerOnHoldReasons.OtherReason");
		}
	}
}
