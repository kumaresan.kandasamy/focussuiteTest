﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Focus.Core.Criteria.ROnet;
using Focus.Core.DataTransferObjects.FocusExplorer;
using Framework.Caching;
using Framework.Core;
using Framework.Logging;

using Focus.Core.Criteria;
using Focus.Core.Criteria.Onet;
using Focus.Core.Messages.OccupationService;
using Focus.Core.Models.Career;
using Focus.Data.Library.Entities;
using Focus.Services.DtoMappers;
using Focus.Services.Helpers;
using Focus.Core;
using Focus.Services.Core;
using Focus.Services.ServiceContracts;

#endregion

namespace Focus.Services.ServiceImplementations
{
	public class OccupationService : ServiceBase, IOccupationService
	{
    /// <summary>
		/// Initializes a new instance of the <see cref="OccupationService" /> class.
		/// </summary>
		public OccupationService() : this(null)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="OccupationService" /> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		public OccupationService(IRuntimeContext runtimeContext) : base(runtimeContext)
		{ }

		/// <summary>
		/// Gets the resume builder questions.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		/// <exception cref="System.Exception"></exception>
		public ResumeBuilderQuestionsResponse GetResumeBuilderQuestions(ResumeBuilderQuestionsRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
      {
        var response = new ResumeBuilderQuestionsResponse(request);

        // Validate request
        if (!ValidateRequest(request, response, Validate.ClientTag | Validate.AccessToken))
          return response;
          
        try
        {
        	var onetId = Repositories.Library.Onets.Where(x => x.OnetCode == request.OnetCode).Select(x => x.Id).FirstOrDefault();

					if(onetId.IsNull() || onetId <= 0)
						throw new Exception(string.Format("Could not find onet with code {0}", request.OnetCode));

        	var jobTasks = Helpers.Occupation.GetJobTasks(onetId, JobTaskScopes.Resume, request.UserContext.Culture);

					response.Questions = jobTasks.Select(jobTask => new Question
        	                                           	{
        	                                           		QuestionType = jobTask.JobTaskType, 
																												IsCertificate = jobTask.Certificate, 
																												Prompt = jobTask.Prompt, 
																												Response = jobTask.Response, 
																												Options = jobTask.MultiOptionPrompts
        	                                           	}).ToList();
          
        }
        catch (Exception ex)
        {
          response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
          Logger.Error(request.LogData(), response.Message, response.Exception);
        }

        return response;
      }
		}

		/// <summary>
		/// Gets the statements.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
	  public StatementsResponse GetStatements(StatementsRequest request)
	  {
	    using (Profiler.Profile(request.LogData(), request))
	    {
	      var response = new StatementsResponse(request);

	      // Validate request
	      if (!ValidateRequest(request, response, Validate.All))
	        return response;

	      try
	      {
					var onetId = Repositories.Library.Onets.Where(x => x.OnetCode == request.OnetCode).Select(x => x.Id).FirstOrDefault();

					if (onetId.IsNull() || onetId <= 0)
						throw new Exception(string.Format("Could not find onet with code {0}", request.OnetCode));

					var onetTaskViewList = Helpers.Occupation.GetOnetTasks(onetId, request.UserContext.Culture, 100);
	      	response.Statements =  request.Tense == StatementTense.Past ? onetTaskViewList.Select(x => x.TaskPastTense).ToList() : onetTaskViewList.Select(x => x.Task).ToList();
	      }

	      catch (Exception ex)
	      {
	        response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
	        Logger.Error(request.LogData(), response.Message, response.Exception);
	      }

	      return response;
	    }
	  }

		/// <summary>
		/// Gets the onet skills.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    public OnetSkillsResponse GetOnetSkills(OnetSkillsRequest request)
    {
      using (Profiler.Profile(request.LogData(), request))
      {
        var response = new OnetSkillsResponse(request);

        // Validate request
        if (!ValidateRequest(request, response, Validate.All))
          return response;

        try
        {
          var onetId = Repositories.Library.Onets.Where(x => x.OnetCode == request.OnetCode).Select(x => x.Id).FirstOrDefault();

          if (onetId.IsNull() || onetId <= 0)
           throw new Exception(string.Format("Could not find onet with code {0}", request.OnetCode));

					// TODO: Get SKills
          
        }

        catch (Exception ex)
        {
          response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
          Logger.Error(request.LogData(), response.Message, response.Exception);
        }

        return response;
      }
    }

		/// <summary>
		/// Gets the moc occupation codes.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public OccupationCodeResponse GetMocOccupationCodes(OccupationCodeRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new OccupationCodeResponse(request);

				// Validate request
        if (!ValidateRequest(request, response, Validate.ClientTag | Validate.AccessToken))
					return response;

				try
				{
					var mocJobTitle = "";

          if (request.OccupationCode.IsNullOrEmpty() && request.OccupationTitle.IsNotNullOrEmpty() && !request.OccupationId.HasValue)
						throw new Exception("Either an occupation code, title or id required");
					
					if (request.OccupationCode.IsNotNullOrEmpty())
						mocJobTitle = request.OccupationCode.Trim().ToLower();

					if (request.OccupationTitle.IsNotNullOrEmpty())
						mocJobTitle = mocJobTitle.IsNullOrEmpty() ? request.OccupationTitle.Trim().ToLower() : string.Format("{0} - {1}", mocJobTitle, request.OccupationTitle.Trim().ToLower());
					
					// Get MOC id from job title if id is not specifieds
          var mocId = request.OccupationId.HasValue
                        ? request.OccupationId.Value
												: Repositories.Library.MilitaryOccupationJobTitleViews.Where(x => x.JobTitle.Trim().ToLower() == mocJobTitle).Select(x => x.MilitaryOccupationId).FirstOrDefault();

					if(mocId == 0)
						mocId = Repositories.Library.MilitaryOccupationJobTitleViews.Where(x => x.JobTitle.Trim().StartsWith(request.OccupationCode.Trim().ToLower())).Select(x => x.MilitaryOccupationId).FirstOrDefault();

					if(mocId == 0)
						throw new Exception(string.Format("Could not find a military occupation matching {0}.", mocJobTitle));

					var militaryOccupation = Repositories.Library.FindById<MilitaryOccupation>(mocId);

					if (militaryOccupation.IsNotNull())
					{
						var result = new List<OnetCode>();

						// Add the 3 star ronet matches
						foreach (var rOnet in militaryOccupation.ROnets)
						{
							if (result.Any(x => x.RonetCode == rOnet.Code)) continue;
							
							var rOnetOnet = rOnet.ROnetOnets.FirstOrDefault(x => x.BestFit);

							if (rOnetOnet.IsNotNull())
							{
								var onet = Helpers.Occupation.GetOnetById(rOnetOnet.OnetId, request.UserContext.Culture);
								var localisedROnet = Helpers.Occupation.GetROnetById(rOnet.Id, request.UserContext.Culture);

								if (onet.IsNotNull() && (request.IgnoreJobTaskAvailability || !request.IgnoreJobTaskAvailability && onet.JobTasksAvailable) && localisedROnet.IsNotNull())
								{
									result.Add(new OnetCode
									{
										Occupation = localisedROnet.Occupation,
										Code = onet.OnetCode,
										RonetCode = rOnet.Code,
										StarRating = 3
									});

								}
							}
						}

						// Add the 2 star ronet matches
						if (militaryOccupation.MilitaryOccupationGroup.IsNotNull())
						{
							foreach (var rOnet in militaryOccupation.MilitaryOccupationGroup.ROnets)
							{
								if (result.Any(x => x.RonetCode == rOnet.Code)) continue;

								var rOnetOnet = rOnet.ROnetOnets.FirstOrDefault(x => x.BestFit);

								if (rOnetOnet.IsNotNull())
								{
									var onet = Helpers.Occupation.GetOnetById(rOnetOnet.OnetId, request.UserContext.Culture);
									var localisedROnet = Helpers.Occupation.GetROnetById(rOnet.Id, request.UserContext.Culture);

									if (onet.IsNotNull() && (request.IgnoreJobTaskAvailability || !request.IgnoreJobTaskAvailability && onet.JobTasksAvailable) && localisedROnet.IsNotNull())
									{
										result.Add(new OnetCode
										{
											Occupation = localisedROnet.Occupation,
											Code = onet.OnetCode,
											RonetCode = rOnet.Code,
											StarRating = 2
										});
									}
								}
							}
						}

						// Add the 1 star ronet matches
						var genericGroupName = (militaryOccupation.IsCommissioned) ? "Commissioned" : "Enlisted";

						var genericGroup = Repositories.Library.MilitaryOccupationGroups.FirstOrDefault(x => x.Name == genericGroupName);

						if (genericGroup.IsNotNull())
						{
							foreach (var rOnet in genericGroup.ROnets)
							{
								if (result.Any(x => x.RonetCode == rOnet.Code)) continue;

								var rOnetOnet = rOnet.ROnetOnets.FirstOrDefault(x => x.BestFit);

								if (rOnetOnet.IsNotNull())
								{
									var onet = Helpers.Occupation.GetOnetById(rOnetOnet.OnetId, request.UserContext.Culture);
									var localisedROnet = Helpers.Occupation.GetROnetById(rOnet.Id, request.UserContext.Culture);

									if (onet.IsNotNull() && (request.IgnoreJobTaskAvailability || !request.IgnoreJobTaskAvailability && onet.JobTasksAvailable) && localisedROnet.IsNotNull())
									{
										result.Add(new OnetCode
										{
											Occupation = localisedROnet.Occupation,
											Code = onet.OnetCode,
											RonetCode = rOnet.Code,
											StarRating = 1
										});
									}
								}
							}
						}

						response.OnetCodes = result;

					}
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the occupation codes.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public OccupationCodeResponse GetOccupationCodes(OccupationCodeRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new OccupationCodeResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.ClientTag | Validate.AccessToken))
					return response;

				try
				{
					var criteria = new OnetCriteria
					               	{
					               		JobTitle = request.OccupationTitle,
					               		FetchOption = CriteriaBase.FetchOptions.List,
					               		ListSize = (request.SOCCount.HasValue) ? request.SOCCount.Value : 100
					               	};

					var onets = Helpers.Occupation.GetOnets(criteria, request.UserContext.Culture);

					response.OnetCodes = onets.Select(onet => new OnetCode
					                                          	{
					                                          		Code = onet.OnetCode,
					                                          		Occupation = onet.Occupation
					                                          	}).ToList();
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the military occupation job titles.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public MilitaryOccupationJobTitleResponse GetMilitaryOccupationJobTitles(MilitaryOccupationJobTitleRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new MilitaryOccupationJobTitleResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.ClientTag | Validate.AccessToken))
					return response;

				try
				{
					var jobTitles = Cacher.Get<List<MilitaryOccupationJobTitleViewDto>>(Constants.CacheKeys.MilitaryOccupationJobTitlesKey);

					if (jobTitles == null)
					{
						using (Profiler.Profile(request.LogData()))
						{
							lock (SyncObject)
							{
								jobTitles = Repositories.Library.MilitaryOccupationJobTitleViews.Select(j => j.AsDto()).ToList();
								Cacher.Set(Constants.CacheKeys.MilitaryOccupationJobTitlesKey, jobTitles);
							}
						}
					}

				  if (request.MilitaryOccupationId.HasValue)
				  {
				    var jobTitle = jobTitles.FirstOrDefault(jt => jt.MilitaryOccupationId == request.MilitaryOccupationId.Value);
				    response.JobTitle = jobTitle.IsNull() ? null : jobTitle;
				  }
				  else
				  {
				    var filteredTitles = request.JobTitle.IsNotNullOrEmpty()
                                   ? (request.FullTitleSearch
                                        ? jobTitles.Where(x => x.JobTitle.ToLowerInvariant().Contains(request.JobTitle.ToLowerInvariant()))
                                        : jobTitles.Where(x => x.JobTitle.StartsWith(request.JobTitle, StringComparison.InvariantCultureIgnoreCase)))
                                   : jobTitles;

				    response.JobTitles = filteredTitles.Take(request.ListSize).ToList();
				  }

				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

	  /// <summary>
	  /// Converts the onet to ronet.
	  /// </summary>
	  /// <param name="request">The request.</param>
	  /// <returns></returns>
	  public OnetToROnetConversionResponse ConvertOnetToROnet(OnetToROnetConversionRequest request)
	  {
	    using (Profiler.Profile(request.LogData(), request))
			{
        var response = new OnetToROnetConversionResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.ClientTag | Validate.AccessToken))
					return response;

				try
				{
				 var conversionRecord =
				    Repositories.Library.OnetToROnetConversionViews.FirstOrDefault(x => x.OnetCode == request.Onet && x.BestFit);

          response.BestFitROnet = conversionRecord.IsNotNull() ? conversionRecord.AsDto() : null;
				}
        catch (Exception ex)
        {
          response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
          Logger.Error(request.LogData(), response.Message, response.Exception);
        }

        return response;
      }
	  }
	}
}
