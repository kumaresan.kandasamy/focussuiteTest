﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Aspose.Words;
using Aspose.Words.Tables;
using Focus.Core;
using Focus.Core.Criteria;
using Focus.Core.Criteria.CandidateSearch;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.EmailTemplate;
using Focus.Core.IntegrationMessages;
using Focus.Core.Messages;
using Focus.Core.Messages.JobService;
using Focus.Core.Messages.SearchService;
using Focus.Core.Models;
using Focus.Core.Models.Assist;
using Focus.Core.Models.Career;
using Focus.Core.Models.Import;
using Focus.Core.Views;
using Focus.Data.Core.Entities;
using Focus.Data.Library.Entities;
using Focus.Services.Core;
using Focus.Services.Core.Censorship;
using Focus.Services.Core.Extensions;
using Focus.Services.Core.Posting;
using Focus.Services.DtoMappers;
using Focus.Services.Mappers;
using Focus.Services.Messages;
using Focus.Services.Properties;
using Focus.Services.Queries;
using Focus.Services.ServiceContracts;
using Focus.Services.Utilities;
using Framework.Caching;
using Framework.Core;
using Framework.Logging;
using JetBrains.Annotations;
using Job = Focus.Data.Core.Entities.Job;
using JobLocationCriteria = Focus.Core.Criteria.JobLocation.JobLocationCriteria;
using OnetTaskView = Focus.Core.Views.OnetTaskView;
using SaveJobRequest = Focus.Core.Messages.JobService.SaveJobRequest;
using SaveJobResponse = Focus.Core.Messages.JobService.SaveJobResponse;
// ReSharper disable InvertIf

#endregion

namespace Focus.Services.ServiceImplementations
{
	/// <summary>
	/// 
	/// </summary>
	public class JobService : ServiceBase, IJobService
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="JobService" /> class.
		/// </summary>
		public JobService()
			: this(null)
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="JobService" /> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		public JobService(IRuntimeContext runtimeContext)
			: base(runtimeContext)
		{
		}

		/// <summary>
		/// Gets the jobView or JobsViews.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public JobViewResponse GetJobView(JobViewRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new JobViewResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var query = from j in Repositories.Core.JobViews
						select j;

					#region criteria

					var criteria = request.Criteria;

					if (criteria.EmployerId.HasValue)
					{
						query = query.Where(x => x.EmployerId == criteria.EmployerId);

						if (criteria.ValidateEmployeeId.HasValue)
						{
							var checkEmployerId = Repositories.Core.Employees.Where(e => e.Id == criteria.ValidateEmployeeId).Select(e => e.EmployerId).FirstOrDefault();

							if (criteria.EmployerId != checkEmployerId)
								throw new Exception("Invalid access to Business Unit");
						}
					}

					if (criteria.EmployeeId.HasValue)
						query = query.Where(x => x.EmployeeId == criteria.EmployeeId);

					if (criteria.BusinessUnitId.HasValue)
					{
						query = query.Where(x => x.BusinessUnitId == criteria.BusinessUnitId);

						if (criteria.ValidateEmployeeId.HasValue)
						{
							var checkBusinessUnitEmployerId = Repositories.Core.BusinessUnits.Where(bu => bu.Id == criteria.BusinessUnitId).Select(bu => bu.EmployerId).FirstOrDefault();
							var checkEmployerId = Repositories.Core.Employees.Where(e => e.Id == criteria.ValidateEmployeeId).Select(e => e.EmployerId).FirstOrDefault();

							if (checkBusinessUnitEmployerId == 0 || checkBusinessUnitEmployerId != checkEmployerId)
								throw new Exception("Invalid access to Business Unit");
						}
					}

					if (criteria.EmployeeIdForBusinessUnit.HasValue)
					{
						// Find BusinessUnitIDs available to employee
						var businessUnitList = (from bu in Repositories.Core.EmployeeBusinessUnits
							where bu.EmployeeId == criteria.EmployeeIdForBusinessUnit
							select (long?)bu.BusinessUnitId).ToList();

						query = query.Where(x => businessUnitList.Contains(x.BusinessUnitId));
					}

					if (criteria.JobId.HasValue)
            query = query.Where(x => x.Id == criteria.JobId || x.PostingId == criteria.JobId);

					if (AppSettings.Theme == FocusThemes.Education && criteria.JobType.HasValue)
						query = query.Where(x => (x.JobType & criteria.JobType) > 0);

					if (criteria.JobTitle.IsNotNullOrEmpty())
						query = query.Where(x => x.JobTitle.Contains(criteria.JobTitle));

					if (criteria.EmployerName.IsNotNullOrEmpty())
						query = query.Where(x => x.EmployerName.Contains(criteria.EmployerName) || x.BusinessUnitName.Contains(criteria.EmployerName));

					if (criteria.BusinessUnitName.IsNotNullOrEmpty())
						query = query.Where(x => x.BusinessUnitName.Contains(criteria.BusinessUnitName));

					if (criteria.HiringManagerUsername.IsNotNullOrEmpty())
						query = query.Where(x => x.UserName.Contains(criteria.HiringManagerUsername));

					if (criteria.HiringManagerFirstName.IsNotNullOrEmpty())
						query = query.Where(x => x.HiringManagerFirstName.Contains(criteria.HiringManagerFirstName));

					if (criteria.HiringManagerLastName.IsNotNullOrEmpty())
						query = query.Where(x => x.HiringManagerLastName.Contains(criteria.HiringManagerLastName));

					if (criteria.CreatedFrom.HasValue)
						query = query.Where(x => x.PostedOn >= criteria.CreatedFrom);

					if (criteria.CreatedTo.HasValue)
						query = query.Where(x => x.PostedOn <= criteria.CreatedTo);

					if (criteria.ForeignLaborJobsAll.HasValue)
						query = query.Where(x => x.ForeignLabourCertificationH2A == true ||
						                         x.ForeignLabourCertificationH2B == true ||
						                         x.ForeignLabourCertificationOther == true);

					if (criteria.ForeignLaborJobsH2A.HasValue)
						query = query.Where(x => x.ForeignLabourCertificationH2A == true);

					if (criteria.ForeignLaborJobsH2B.HasValue)
						query = query.Where(x => x.ForeignLabourCertificationH2B == true);

					if (criteria.ForeignLaborJobsOther.HasValue)
						query = query.Where(x => x.ForeignLabourCertificationOther == true);

					if (criteria.FederalContractor.HasValue)
						query = query.Where(x => x.FederalContractor == true);

					if (criteria.CourtOrderedAffirmativeAction.HasValue)
						query = query.Where(x => x.CourtOrderedAffirmativeAction == true);

					if (criteria.PostedOn.IsNotNull() && criteria.PostedOn > DateTime.MinValue)
						query = query.Where(x => x.PostedOn >=
						                         Convert.ToDateTime(String.Format("{0}-{1}-{2} 00:00:00.000", criteria.PostedOn.Year,
							                         criteria.PostedOn.Month, criteria.PostedOn.Day)));



					if (criteria.OfficeIds.IsNotNullOrEmpty() && criteria.OfficeIds.Count > 0)
					{
						query = query.Where(q => Repositories.Core.JobOfficeMappers.Where(mapper => criteria.OfficeIds.Contains(mapper.OfficeId)).Select(mapper => mapper.JobId).Distinct().Contains(q.Id));
					}

					if (criteria.StaffMemberId.IsNotNull() && criteria.StaffMemberId > 0)
						query = query.Where(x => x.AssignedToId == criteria.StaffMemberId);

					if (criteria.FetchOption != CriteriaBase.FetchOptions.Single)
					{
						if (criteria.JobStatus.IsNotNull())
						{
							if (criteria.TalentDraftJobs)
								query = query.Where(x => x.JobStatus == JobStatuses.Draft || x.JobStatus == JobStatuses.AwaitingEmployerApproval);
							else
							{
								query = query.Where(x => x.JobStatus == criteria.JobStatus);

								// FVN-3259: JLH: this code was removed after speaking with Lee as it doesn't really make sense and is also wrong as it needs a .join on the query
								//if (criteria.JobStatus == JobStatuses.Draft && request.Module == FocusModules.Assist)
								//{
								//  query =
								//    query.Where(
								//      x =>
								//        (Repositories.Core.Query<User>().Where(y => y.UserType == UserTypes.Assist).Select(y => y.Id)).Contains(x.UpdatedBy));
								//}
							}
						}
						else
						{
							query = query.Where(x => x.JobStatus != JobStatuses.AwaitingEmployerApproval);
						}
					}

					// Implement issue filters
					switch (criteria.JobIssueFilter)
					{
						case JobIssuesFilter.None: // No issue triggered, no post hire follow ups or no job issues records

							query = query.Where(q =>
								((Repositories.Core.JobIssuesViews.Where(
									x =>
										(!x.LowQualityMatches && AppSettings.JobIssueFlagLowMinimumStarMatches)
										&& (!x.LowReferralActivity && AppSettings.JobIssueFlagLowReferrals)
										&& (!x.NotViewingReferrals && AppSettings.JobIssueFlagNotClickingReferrals)
										&& (!x.SearchingButNotInviting && AppSettings.JobIssueFlagSearchingNotInviting)
										&& (!x.ClosingDateRefreshed && AppSettings.JobIssueFlagClosingDateRefreshed)
										&& (!x.EarlyJobClosing && AppSettings.JobIssueFlagJobClosedEarly)
										&& (!x.NegativeSurveyResponse && AppSettings.JobIssueFlagNegativeFeedback)
										&& (!x.PositiveSurveyResponse && AppSettings.JobIssueFlagPositiveFeedback)
										&& !x.FollowUpRequested
										&& (x.PostHireFollowUpCount == 0 && AppSettings.JobIssueFlagJobAfterHiring)).Select(x => x.JobId)).Contains(q.Id) || !(Repositories.Core.JobIssues.Select(x => x.JobId).Contains(q.Id))));

							break;
						case JobIssuesFilter.LowQualityMatches:
							query = query.Where(q => (Repositories.Core.JobIssuesViews.Where(x => x.LowQualityMatches).Select(x => x.JobId)).Contains(q.Id));
							break;
						case JobIssuesFilter.LowReferralActivity:
							query = query.Where(q => (Repositories.Core.JobIssuesViews.Where(x => x.LowReferralActivity).Select(x => x.JobId)).Contains(q.Id));
							break;
						case JobIssuesFilter.NotViewingReferrals:
							query = query.Where(q => (Repositories.Core.JobIssuesViews.Where(x => x.NotViewingReferrals).Select(x => x.JobId)).Contains(q.Id));
							break;
						case JobIssuesFilter.SearchNotInviting:
							query = query.Where(q => (Repositories.Core.JobIssuesViews.Where(x => x.SearchingButNotInviting).Select(x => x.JobId)).Contains(q.Id));
							break;
						case JobIssuesFilter.ClosingDateRefreshed:
							query = query.Where(q => (Repositories.Core.JobIssuesViews.Where(x => x.ClosingDateRefreshed).Select(x => x.JobId)).Contains(q.Id));
							break;
						case JobIssuesFilter.EarlyJobClosing:
							query = query.Where(q => (Repositories.Core.JobIssuesViews.Where(x => x.EarlyJobClosing).Select(x => x.JobId)).Contains(q.Id));
							break;
						case JobIssuesFilter.NegativeSurveyResponse:
							query = query.Where(q => (Repositories.Core.JobIssuesViews.Where(x => x.NegativeSurveyResponse).Select(x => x.JobId)).Contains(q.Id));
							break;
						case JobIssuesFilter.PositiveSurveyResponse:
							query = query.Where(q => (Repositories.Core.JobIssuesViews.Where(x => x.PositiveSurveyResponse).Select(x => x.JobId)).Contains(q.Id));
							break;
						case JobIssuesFilter.PostHireFollowUp:
							query = query.Where(q => (Repositories.Core.JobIssuesViews.Where(x => x.PostHireFollowUpCount > 0).Select(x => x.JobId)).Contains(q.Id));
							break;
						case JobIssuesFilter.FollowUpRequested:
							query = query.Where(q => (Repositories.Core.JobIssuesViews.Where(x => x.FollowUpRequested).Select(x => x.JobId)).Contains(q.Id));
							break;
						case JobIssuesFilter.Any:

							query = query.Where(q =>
								(Repositories.Core.JobIssuesViews.Where(
									x =>
										(x.LowQualityMatches && AppSettings.JobIssueFlagLowMinimumStarMatches)
										|| (x.LowReferralActivity && AppSettings.JobIssueFlagLowReferrals)
										|| (x.NotViewingReferrals && AppSettings.JobIssueFlagNotClickingReferrals)
										|| (x.SearchingButNotInviting && AppSettings.JobIssueFlagSearchingNotInviting)
										|| (x.ClosingDateRefreshed && AppSettings.JobIssueFlagClosingDateRefreshed)
										|| (x.EarlyJobClosing && AppSettings.JobIssueFlagJobClosedEarly)
										|| (x.NegativeSurveyResponse && AppSettings.JobIssueFlagNegativeFeedback)
										|| (x.PositiveSurveyResponse && AppSettings.JobIssueFlagPositiveFeedback)
										|| x.FollowUpRequested
										|| (x.PostHireFollowUpCount > 0 && AppSettings.JobIssueFlagJobAfterHiring)).Select(x => x.JobId)).Contains(q.Id));

							break;
					}

					if (criteria.OrderBy.IsNotNullOrEmpty() && criteria.OrderBy.Trim().IsNotNullOrEmpty())
						query = query.OrderBy(criteria.OrderBy);

					#endregion

					switch (request.Criteria.FetchOption)
					{
						case CriteriaBase.FetchOptions.PagedList:
						{
							response.JobViewsPaged = query.GetPagedList(x => x.AsModel(), request.Criteria.PageIndex, request.Criteria.PageSize);
							UpdateJobModelViewIssuesAndCounts(response.JobViewsPaged);
							break;
						}

						case CriteriaBase.FetchOptions.List:
						{
							response.JobViews = query.Select(jv => jv.AsDto()).ToList();
							break;
						}

						default:
						{
							var jobView = query.SingleOrDefault();
							if (jobView.IsNotNull())
								response.JobView = jobView.AsDto();
							break;
						}
					}
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Updates the job model view issues.
		/// </summary>
		/// <param name="jobViews">The job views.</param>
		private void UpdateJobModelViewIssuesAndCounts(IEnumerable<JobViewModel> jobViews)
		{
			// Get job ids we're processing for later linq queries
			var jobViewList = jobViews as List<JobViewModel> ?? jobViews.ToList();
			var jobIds = jobViewList.Select(x => x.Id).ToList();

			// Get issue records for these jobs
			var issues = (from jobIssues in Repositories.Core.JobIssues
				where jobIds.Contains(jobIssues.JobId)
				select jobIssues.AsDto()).ToList();

			// Get follow up requests for these jobs
			var followUps = (from postHireFollowUps in Repositories.Core.ApplicationViews
				where
					jobIds.Contains(postHireFollowUps.JobId) &&
					postHireFollowUps.PostHireFollowUpStatus == PostHireFollowUpStatus.Pending
				select postHireFollowUps.AsDto()).ToList();

			// Get applications
			var applications = (from posting in Repositories.Core.Postings
				join application in Repositories.Core.Applications
					on posting.Id equals application.PostingId
				join resume in Repositories.Core.Resumes
					on application.ResumeId equals resume.Id
				where jobIds.Contains(posting.JobId)
				      && application.ApprovalStatus == ApprovalStatuses.Approved
				select new
				{
					posting.JobId,
					resume.IsVeteran,
                    resume.PersonId
				}).ToList();

            //FVN-5970
            var underAgeSeekers = Repositories.Core.Persons.Where(p => p.Age <= AppSettings.UnderAgeJobSeekerRestrictionThreshold).Select(x => x.Id).ToList();
            applications.RemoveAll(a => underAgeSeekers.Contains(a.PersonId));

			var automatedUserId = jobViewList.Any(jv => jv.ClosedBy.HasValue)
				                      ? Repositories.Core.Users.Where(x => x.UserName == AppSettings.MessageBusUsername).Select(x => x.Id).FirstOrDefault()
				                      : -1;

			// Update job view model with issues
			jobViewList.ForEach(x =>
			{
				x.Issues = new JobIssuesModel
				{
					JobIssues = issues.SingleOrDefault(y => y.JobId == x.Id),
					PendingPostHireFollowUps = followUps.Where(y => y.JobId == x.Id).ToList()
				};

				x.ApplicationCount = applications.Count(y => y.JobId == x.Id);
				x.VeteranApplicationCount = applications.Count(y => y.JobId == x.Id && y.IsVeteran.GetValueOrDefault(false));
				x.AutoClosed = (x.ClosedBy == automatedUserId);
			});
		}

		/// <summary>
		/// Gets the job details.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public JobDetailsResponse GetJobDetails(JobDetailsRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new JobDetailsResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var jobDetails = GetJobDetailView(request.JobId, request.GetRecommendationCount, request.Module, request);

					if (jobDetails.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.JobNotFound));
						Logger.Info(request.LogData(), string.Format("The JobId '{0}' was not found.", request.JobId));

						return response;
					}

					response.JobDetails = jobDetails;
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the previous job data.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public JobPrevDataResponse GetJobPrevData(JobPrevDataRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new JobPrevDataResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				response.JobPrevData = null;
				if (AppSettings.JobProofingEnabled)
				{
					try
					{
						var jobPrevData = Repositories.Core.JobPrevDatas.FirstOrDefault(jpd => jpd.JobId == request.JobId);
						if (jobPrevData.IsNotNull())
						{
							response.JobPrevData = jobPrevData.AsDto();
						}
					}
					catch (Exception ex)
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
						Logger.Error(request.LogData(), response.Message, response.Exception);
					}
				}
				return response;
			}
		}

		/// <summary>
		/// Gets the job education internship skills.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public JobEducationInternshipSkillResponse GetJobEducationInternshipSkills(JobEducationInternshipSkillRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new JobEducationInternshipSkillResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var skills = Repositories.Core.JobEducationInternshipSkills.Where(x => x.JobId == request.JobId);
					response.EducationInternshipSkills = skills.Select(skill => skill.AsDto()).ToList();
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the job detail view.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <param name="getRecommendationCount">if set to <c>true</c> [get recommendation count].</param>
		/// <param name="module">The current module in which the job is being viewed.</param>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		private JobDetailsView GetJobDetailView(long jobId, bool getRecommendationCount, FocusModules module, ServiceRequest request)
		{
			JobDetailsView jobDetails = null;

			var job = (from j in Repositories.Core.Jobs
				where
					(j.EmployerId == request.UserContext.EmployerId || module == FocusModules.Assist) &&
					j.Id == jobId
				select j).SingleOrDefault();

			if (job.IsNotNull())
			{
				#region Generate posting view

				var postingGeneratorModel = GetPostingGeneratorModel(job);
				var postingView = new PostingGenerator(postingGeneratorModel, request.UserContext.Culture, AppSettings, Helpers.Localisation, Helpers.Lookup).GenerateView();

				#endregion

				var automatedUserId = job.ClosedBy.HasValue
																? Repositories.Core.Users.Where(x => x.UserName == AppSettings.MessageBusUsername).Select(x => x.Id).FirstOrDefault()
																: -1;

				jobDetails = new JobDetailsView
				{
					JobId = job.Id,
					JobTitle = job.JobTitle,
					JobType = job.JobType,
					JobDescription = job.Description,
					PositionDetails = GeneratePositionDetails(job.EmploymentStatusId, job.JobTypeId, job.JobType, request),
					NumberOfOpenings = job.NumberOfOpenings,
					JobStatus = job.JobStatus,
					ApprovalStatus = job.ApprovalStatus,
					ClosingDate = job.ClosingOn,
					PostedBy = job.PostedBy,
					HiringManagerId = job.EmployeeId,
					EmployerId = job.EmployerId,
					ExternalId = job.ExternalId,
					ApplicationCount = 0,
					VeteranApplicationCount = 0,
					RecommendedCount = (getRecommendationCount) ? GetRecommendationCount(job.Id, request) : 0,
					IsConfidential = job.IsConfidential.GetValueOrDefault(),
					CreatedOn = job.CreatedOn,
					InterviewContactPreferences = job.InterviewContactPreferences,
					HasCheckedCriminalRecordExclusion = job.HasCheckedCriminalRecordExclusion,
					ScreeningPreferences = job.ScreeningPreferences,
					PostedOn = job.PostedOn,
					LastPostedOn = job.LastPostedOn,
					LastRefreshedOn = job.LastRefreshedOn,
					UpdatedOn = job.UpdatedOn,
					ForeignLabourCertificationDetails = postingView.ForeignLabourCertification,
					CourtOrderedAffirmativeActionDetails = postingView.CourtOrderedAffirmativeAction,
					FederalContractorDetails = postingView.FederalContractor,
					JobDetails = AppSettings.Theme == FocusThemes.Workforce ? GetJobSection(postingView, JobDisplaySection.Details) : null,
					JobSalaryBenefits = AppSettings.Theme == FocusThemes.Workforce ? GetJobSection(postingView, JobDisplaySection.SalaryBenefits) : null,
					JobRequirements = GetJobSection(postingView, JobDisplaySection.Requirements),
					JobRecruitmentInformation = GetJobSection(postingView, JobDisplaySection.RecruitmentInformation),
					CriminalBackgroundExclusionRequired = job.CriminalBackgroundExclusionRequired,
					ClosedOn = job.ClosedOn,
					VeteranPriorityEndDate = job.VeteranPriorityEndDate,
					ExtendVeteranPriority = job.ExtendVeteranPriority,
					CreditCheckRequired = job.CreditCheckRequired,
					AutoClosed = job.ClosedBy == automatedUserId
				};

				var posting = job.Posting;
				if (posting.IsNotNull())
				{

                    var applications = (from application in Repositories.Core.Applications
                                        join resume in Repositories.Core.Resumes
                                            on application.ResumeId equals resume.Id
                                        where
                                            application.PostingId == posting.Id
                                            && application.ApprovalStatus == ApprovalStatuses.Approved
                                        select new { resume.IsVeteran, resume.PersonId }).ToList();

                    //FVN-5970
                    var underAgeSeekers = Repositories.Core.Persons.Where(p => p.Age <= AppSettings.UnderAgeJobSeekerRestrictionThreshold).Select(x => x.Id).ToList();
                    applications.RemoveAll(a => underAgeSeekers.Contains(a.PersonId));

					jobDetails.ApplicationCount = applications.Count;
					jobDetails.VeteranApplicationCount = applications.Count(application => application.IsVeteran == true);
				}
			}

			return jobDetails;
		}

		/// <summary>
		/// Gets the job section details.
		/// </summary>
		/// <returns></returns>
		private string GetJobSection(PostingView postingView, JobDisplaySection section)
		{
			var jobDetails = new StringBuilder();

			switch (section)
			{
				case JobDisplaySection.Details:

					#region Details

					if (postingView.FederalContractor.IsNotNullOrEmpty())
						jobDetails.AppendFormat("* {0}<br/>", postingView.FederalContractor);
					if (postingView.CourtOrderedAffirmativeAction.IsNotNullOrEmpty())
						jobDetails.AppendFormat("* {0}<br/>", postingView.CourtOrderedAffirmativeAction);
					if (postingView.WorkOpportunitiesTaxCreditHires.IsNotNullOrEmpty())
						jobDetails.AppendFormat("* {0}<br/>", postingView.WorkOpportunitiesTaxCreditHires);
					if (postingView.PostingFlags.IsNotNullOrEmpty())
						jobDetails.AppendFormat("* {0}<br/>", postingView.PostingFlags);

					#endregion

					break;

				case JobDisplaySection.SalaryBenefits:

					#region Benefits

					if (postingView.SalaryDetails.IsNotNullOrEmpty())
						jobDetails.AppendFormat("* {0}<br/>", postingView.SalaryDetails);
					if (postingView.OtherSalary.IsNotNullOrEmpty())
						jobDetails.AppendFormat("* {0}<br/>", postingView.OtherSalary);
					if (postingView.CommissionLabel.IsNotNullOrEmpty())
						jobDetails.AppendFormat("* {0}<br/>", postingView.CommissionLabel);
					if (postingView.NormalWorkingDays.IsNotNullOrEmpty())
						jobDetails.AppendFormat("* {0}<br/>", postingView.NormalWorkingDays);
					if (postingView.HoursPerWeek.IsNotNullOrEmpty())
						jobDetails.AppendFormat("* {0}<br/>", postingView.HoursPerWeek);
					if (postingView.NormalWorkShifts.IsNotNullOrEmpty())
						jobDetails.AppendFormat("* {0}<br/>", postingView.NormalWorkShifts);
					if (postingView.OvertimeRequired.IsNotNullOrEmpty())
						jobDetails.AppendFormat("* {0}<br/>", postingView.OvertimeRequired);
					if( postingView.JobType.IsNotNullOrEmpty() )
					{
						if( postingView.EmploymentStatus.IsNotNullOrEmpty() )
							jobDetails.AppendFormat( "* {0}<br/>", postingView.HoursText );
						else
							jobDetails.AppendFormat( "* {0}<br/>", postingView.JobType );
					}
					else
					{
						if( postingView.EmploymentStatus.IsNotNullOrEmpty() )
							jobDetails.AppendFormat( "* {0}<br/>", postingView.EmploymentStatus );
					}
						
					if (postingView.JobStatus.IsNotNullOrEmpty())
						jobDetails.AppendFormat("* {0}<br/>", postingView.JobStatus);
					if (postingView.LeaveBenefits.IsNotNullOrEmpty())
						jobDetails.AppendFormat("* {0}<br/>", postingView.LeaveBenefits);
					if (postingView.RetirementBenefits.IsNotNullOrEmpty())
						jobDetails.AppendFormat("* {0}<br/>", postingView.RetirementBenefits);
					if (postingView.InsuranceBenefits.IsNotNullOrEmpty())
						jobDetails.AppendFormat("* {0}<br/>", postingView.InsuranceBenefits);
					if (postingView.MiscellaneousBenefits.IsNotNullOrEmpty())
						jobDetails.AppendFormat("* {0}<br/>", postingView.MiscellaneousBenefits);

					#endregion

					break;

				case JobDisplaySection.Requirements:

					#region Requirements

					if (postingView.MinimumEducationLevelRequirement.IsNotNullOrEmpty())
						jobDetails.AppendFormat("* {0}<br/>", postingView.MinimumEducationLevelRequirement);
					if (postingView.MinimumExperienceRequirement.IsNotNullOrEmpty())
						jobDetails.AppendFormat("* {0}<br/>", postingView.MinimumExperienceRequirement);
					if (postingView.MinimumAgeRequirement.IsNotNullOrEmpty())
						jobDetails.AppendFormat("* {0}<br/>", postingView.MinimumAgeRequirement);
					if (postingView.DrivingLicenceRequirement.IsNotNullOrEmpty())
						jobDetails.AppendFormat("* {0}<br/>", postingView.DrivingLicenceRequirement);
					if (postingView.LicencesRequirements.IsNotNullOrEmpty())
						jobDetails.AppendFormat("* {0}<br/>", postingView.LicencesRequirements);
					if (postingView.CertificationRequirements.IsNotNullOrEmpty())
						jobDetails.AppendFormat("* {0}<br/>", postingView.CertificationRequirements);
					if (postingView.LanguagesRequirements.IsNotNullOrEmpty())
						jobDetails.AppendFormat("* {0}<br/>", postingView.LanguagesRequirements);
					if (postingView.SpecialRequirements.IsNotNullOrEmpty())
					{
						foreach (var requirement in postingView.SpecialRequirements)
						{
							jobDetails.AppendFormat("* {0}<br/>", requirement);
						}
					}

					if (postingView.ProgramOfStudyRequirements.IsNotNullOrEmpty())
						jobDetails.AppendFormat("* {0}<br/>", postingView.ProgramOfStudyRequirements);
					if (postingView.StudentEnrolledRequirement.IsNotNullOrEmpty())
						jobDetails.AppendFormat("* {0}<br/>", postingView.StudentEnrolledRequirement);
					if (postingView.MinimumCollegeYearsRequirement.IsNotNullOrEmpty())
						jobDetails.AppendFormat("* {0}<br/>", postingView.MinimumCollegeYearsRequirement);
					if (postingView.CreditCheckRequired.GetValueOrDefault())
						jobDetails.AppendFormat("* {0}<br/>", Helpers.Localisation.Localise("PostingView.CreditCheck.Text", "A credit check will be required on applicants"));
					if (postingView.CriminalBackgroundCheckRequired.GetValueOrDefault())
						jobDetails.AppendFormat("* {0}<br/>", Helpers.Localisation.Localise("PostingView.CriminalBackgroundCheck.Text", "A criminal background check will be required on applicants"));


					#endregion

					break;

				case JobDisplaySection.RecruitmentInformation:

					#region Recruitment Information

					if (AppSettings.Theme == FocusThemes.Education)
					{
						#region Salary And Hours

						if (postingView.SalaryDetails.IsNotNullOrEmpty())
							jobDetails.AppendFormat("* {0}<br/>", postingView.SalaryDetails);
						if (postingView.OtherSalary.IsNotNullOrEmpty())
							jobDetails.AppendFormat("* {0}<br/>", postingView.OtherSalary);
						if (postingView.CommissionLabel.IsNotNullOrEmpty())
							jobDetails.AppendFormat("* {0}<br/>", postingView.CommissionLabel);
						if (postingView.NormalWorkingDays.IsNotNullOrEmpty())
							jobDetails.AppendFormat("* {0}<br/>", postingView.NormalWorkingDays);
						if (postingView.HoursPerWeek.IsNotNullOrEmpty())
							jobDetails.AppendFormat("* {0}<br/>", postingView.HoursPerWeek);
						if (postingView.NormalWorkShifts.IsNotNullOrEmpty())
							jobDetails.AppendFormat("* {0}<br/>", postingView.NormalWorkShifts);
						if (postingView.OvertimeRequired.IsNotNullOrEmpty())
							jobDetails.AppendFormat("* {0}<br/>", postingView.OvertimeRequired);

						#endregion
					}

					#region Interview Contact Preferences

					if (!AppSettings.HideInterviewContactPreferences)
					{
						jobDetails.AppendFormat("* {0}<br />", postingView.HowToApplyLabel);
						if (postingView.ApplyThroughFocusCareerLabel.IsNotNullOrEmpty())
						{
							jobDetails.AppendFormat("&nbsp;&nbsp;{0} at {1}<br />", postingView.ApplyThroughFocusCareerLabel, postingView.FocusCareerUrl);
						}

						if (postingView.EmailApplicationLabel.IsNotNullOrEmpty())
						{
							jobDetails.AppendFormat("&nbsp;&nbsp;{0} {1}<br/>", postingView.EmailApplicationLabel, postingView.EmailApplicationTo);
						}

						if (postingView.ApplyOnlineLabel.IsNotNullOrEmpty())
						{
							jobDetails.AppendFormat("&nbsp;&nbsp;{0} at {1}<br/>", postingView.ApplyOnlineLabel, postingView.ApplyOnlineUrl);
						}

						if (postingView.MailApplicationLabel.IsNotNullOrEmpty())
						{
							jobDetails.AppendFormat("&nbsp;&nbsp;{0} {1}<br/>", postingView.MailApplicationLabel, postingView.MailApplicationToAddress);
						}

						if (postingView.FaxApplicationLabel.IsNotNullOrEmpty())
						{
							jobDetails.AppendFormat("&nbsp;&nbsp;{0} {1}<br/>", postingView.FaxApplicationLabel, postingView.FaxApplicationToNumber);
						}

						if (postingView.TelephoneApplicationLabel.IsNotNullOrEmpty())
						{
							jobDetails.AppendFormat("&nbsp;&nbsp;{0} {1}<br/>", postingView.TelephoneApplicationLabel, postingView.TelephoneApplicationToNumber);
						}

						if (postingView.InPersonApplicationLabel.IsNotNullOrEmpty())
						{
							jobDetails.AppendFormat("&nbsp;&nbsp;{0} {1}<br/>", postingView.InPersonApplicationLabel, postingView.InPersonApplicationAddress);
						}

						if (postingView.OtherApplicationInstructions.IsNotNullOrEmpty())
							jobDetails.AppendFormat("&nbsp;&nbsp;{0}<br/>", postingView.OtherApplicationInstructions = postingView.OtherApplicationInstructions.Length>800? postingView.OtherApplicationInstructions.Substring(0,800):postingView.OtherApplicationInstructions);
					}

					#endregion

					#endregion

					break;
			}

			return jobDetails.ToString();
		}

		/// <summary>
		/// Generates the postion details.
		/// </summary>
		/// <param name="employmentStatusId">The employment status id.</param>
		/// <param name="jobTypeId">The job type id.</param>
		/// <param name="jobType">Type of the job.</param>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		private string GeneratePositionDetails(long? employmentStatusId, long? jobTypeId, JobTypes jobType, ServiceRequest request)
		{
			var result = new StringBuilder("");
			if (employmentStatusId.HasValue && employmentStatusId.Value > 0)
				result.Append(Helpers.Lookup.GetLookup(LookupTypes.EmploymentStatuses).Where(x => x.Id == employmentStatusId).Select(x => x.Text).SingleOrDefault());
			//GetCachedLookup(request, LookupTypes.EmploymentStatuses).Where(x => x.Id == employmentStatusId).Select(x => x.Text)
			//  .SingleOrDefault());

			if (jobTypeId.HasValue && jobTypeId > 0)
			{
				var jobTypeFromId = Helpers.Lookup.GetLookup(LookupTypes.JobTypes).Where(x => x.Id == jobTypeId).Select(x => x.Text).SingleOrDefault();
				//GetCachedLookup(request, LookupTypes.JobTypes).Where(x => x.Id == jobTypeId).Select(x => x.Text).SingleOrDefault();

				if (jobTypeFromId.IsNotNullOrEmpty())
				{
					if (result.Length > 0)
						result.AppendFormat(", {0}", jobTypeFromId.ToLower());
					else
						result.Append(jobTypeFromId);
				}
			}

			if (jobType != JobTypes.None)
				switch (jobType)
				{
					case JobTypes.Job:
						result.Append(LocaliseOrDefault(request, "Job.Label", "Paid Job"));
						break;

					case JobTypes.InternshipPaid:
						result.Append(LocaliseOrDefault(request, "InternshipPaid.Label", "Paid Internship"));
						break;

					case JobTypes.InternshipUnpaid:
						result.Append(LocaliseOrDefault(request, "InternshipUnpaid.Label", "Unpaid Internship"));
						break;
				}

			return result.ToString();
		}

		/// <summary>
		/// Gets the recommendation count.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		private int GetRecommendationCount(long jobId, ServiceRequest request)
		{
			var resultCount = 0;

			var searchCriteria = new CandidateSearchCriteria
			{
				SearchType = CandidateSearchTypes.ByJobDescription,
				JobDetailsCriteria = new CandidateSearchJobDetailsCriteria { JobId = jobId },
				MinimumDocumentCount = AppSettings.TalentPoolSearchMaximumRecordsToReturn,
				MinimumScore = AppSettings.StarRatingMinimumScores[AppSettings.RecommendedMatchesMinimumStars]
			};

			if (AppSettings.Theme == FocusThemes.Education)
			{
				var job = Repositories.Core.FindById<Job>(jobId);
				var programsOfStudy = Repositories.Core.Find<JobProgramOfStudy>().Where(x => x.JobId == jobId).Select(x => x.AsDto()).ToList();

				JobTypes jobTypesConsidered;

				// Get job type
				switch (job.JobType)
				{
					case JobTypes.InternshipPaid:
					case JobTypes.InternshipUnpaid:
						jobTypesConsidered = JobTypes.InternshipPaid | JobTypes.InternshipUnpaid;
						break;
					default:
						jobTypesConsidered = JobTypes.Job;
						break;
				}

				searchCriteria.AdditionalCriteria = new CandidateSearchAdditionalCriteria
				{
					JobTypesConsidered = jobTypesConsidered,
					EducationCriteria = new CandidateSearchEducationCriteria()
				};

				if (jobTypesConsidered == JobTypes.Job)
					searchCriteria.AdditionalCriteria.EducationCriteria.MonthsUntilExpectedCompletion = 6;

				// Set enrollment status if this is an internship which requires a student to be enrolled
				if ((job.JobType == JobTypes.InternshipPaid || job.JobType == JobTypes.InternshipUnpaid) && job.StudentEnrolled.GetValueOrDefault())
				{
					searchCriteria.AdditionalCriteria.EducationCriteria.EnrollmentStatus = AppSettings.SchoolType == SchoolTypes.FourYear
						? SchoolStatus.Sophomore
						: SchoolStatus.SophomoreOrAbove;
				}
				// Add any programs of study associated with the job
				if (programsOfStudy.Any())
				{
					searchCriteria.AdditionalCriteria.EducationCriteria.ProgramsOfStudy = new List<KeyValuePairSerializable<string, string>>();

					foreach (var programOfStudy in programsOfStudy)
					{
						searchCriteria.AdditionalCriteria.EducationCriteria.ProgramsOfStudy.Add(
							new KeyValuePairSerializable<string, string>(programOfStudy.DegreeEducationLevelId.ToString(),
								programOfStudy.ProgramOfStudy));
					}
				}
			}

			var searchRequest = new CandidateSearchRequest
			{
				ClientTag = request.ClientTag,
				RequestId = request.RequestId,
				SessionId = request.SessionId,
				UserContext = request.UserContext,
				Criteria = searchCriteria,
				ForCount = true
			};

			var searchService = new SearchService();
			var response = searchService.SearchCandidates(searchRequest);

			if (response.IsNotNull())
				resultCount = (response.Candidates.IsNotNullOrEmpty()) ? response.Candidates.TotalCount : 0;

			return resultCount;
		}

		/// <summary>
		/// Gets the similar jobs.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public SimilarJobResponse GetSimilarJobs(SimilarJobRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new SimilarJobResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var jobTypes = new List<JobTypes>();

					switch (request.JobType)
					{
						case JobTypes.Job:
							jobTypes.Add(JobTypes.Job);
							break;

						case JobTypes.InternshipPaid:
						case JobTypes.InternshipUnpaid:
							jobTypes.Add(JobTypes.InternshipPaid);
							jobTypes.Add(JobTypes.InternshipUnpaid);
							break;

						default:
							jobTypes.Add(JobTypes.Job);
							jobTypes.Add(JobTypes.InternshipPaid);
							jobTypes.Add(JobTypes.InternshipUnpaid);
							break;
					}

					var similarJobs = (from j in Repositories.Core.Jobs
						where j.JobTitle.Contains(request.JobTitle) && jobTypes.Contains(j.JobType) && j.JobStatus != JobStatuses.Draft
						join bu in Repositories.Core.BusinessUnits on j.BusinessUnitId equals bu.Id into lbu
						from lbum in lbu.DefaultIfEmpty()
						orderby j.CreatedOn descending
						select new SimilarJobView
						{
							JobId = j.Id,
							JobTitle = j.JobTitle,
							JobDescription = j.Description,
							BusinessUnitName = (lbum != null ? lbum.Name : ""),
						}).ToList();


					if (similarJobs.Count() < AppSettings.SimilarJobsCount)
					{
						// Go get more similar jobs from Lens - much slower
						var criteria = new SearchCriteria
						{
							KeywordCriteria = new KeywordCriteria
							{
								KeywordText = request.JobTitle,
								SearchLocation = PostingKeywordScopes.JobTitle
							},
							RequiredResultCriteria = new RequiredResultCriteria
							{
								DocumentsToSearch = DocumentType.Posting,
								MaximumDocumentCount = AppSettings.SimilarJobsCount - similarJobs.Count(),
								IncludeDuties = true
							}
						};

						switch (request.JobType)
						{
							case JobTypes.Job:
								criteria.InternshipCriteria = new InternshipCriteria
								{
									Internship = FilterTypes.Exclude
								};
								break;

							case JobTypes.InternshipUnpaid:
							case JobTypes.InternshipPaid:
								criteria.InternshipCriteria = new InternshipCriteria
								{
									Internship = FilterTypes.ShowOnly
								};
								break;
						}

						var lensPostings = Repositories.Lens.SearchPostings(criteria, request.UserContext.Culture, false).Where(x => x.Id.Contains("careerjobs")).ToList(); //.Select(x => x.Id);

						var lensJobs = lensPostings.Select(posting => new SimilarJobView
						{
							JobId = 0,
							JobTitle = posting.JobTitle,
							BusinessUnitName = posting.Employer,
							JobDescription = posting.Duties.ToConcatenatedString(Environment.NewLine)
						}).ToList();

						similarJobs.AddRange(lensJobs);
					}

					response.Jobs = similarJobs.Take(AppSettings.SimilarJobsCount).ToList();
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the job.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public JobResponse GetJob(JobRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new JobResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;
				try
				{
					var query = new JobQuery(Repositories.Core, request.Criteria);

					switch (request.Criteria.FetchOption)
					{
						case CriteriaBase.FetchOptions.PagedList:
							response.JobsPaged = query.Query().GetPagedList(x => x.AsDto(), request.Criteria.PageIndex, request.Criteria.PageSize);
							break;

						case CriteriaBase.FetchOptions.List:
							response.Jobs = query.Query().Select(x => x.AsDto()).ToList();
							break;

						default:
							var job = request.Criteria.JobId.IsNotNull()
								? query.QueryEntityId()
								: query.Query().SingleOrDefault();

							response.Job = job.IsNotNull() ? job.AsDto() : null;
							break;
					}
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Refreshes the job.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public JobResponse RefreshJob(JobRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new JobResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var job = Repositories.Core.FindById<Job>(request.Criteria.JobId);

					if (job.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.JobNotFound));
						Logger.Info(request.LogData(), string.Format("The JobId '{0}' was not found.", request.Criteria.JobId));

						return response;
					}

					if (request.NewExpirationDate != DateTime.MinValue && request.NewExpirationDate != job.ClosingOn)
					{
						UpdateJobClosingDate(job, request.NewExpirationDate);

						if ((AppSettings.JobIssueFlagClosingDateRefreshed ||
						     (AppSettings.JobIssueFlagNegativeFeedback || AppSettings.JobIssueFlagPositiveFeedback)) &&
						    request.Module == FocusModules.Talent)
						{
							// Get the job's related JobIssue. 
							var jobIssue = Repositories.Core.JobIssues.SingleOrDefault(x => x.JobId == job.Id);

							// If it is null the job issue doesnt exist so create and add one.
							if (jobIssue.IsNull())
							{
								jobIssue = new JobIssues
								{
									JobId = job.Id,
								};
								Repositories.Core.Add(jobIssue);
							}
							// Flag the job refresh if flagging enabled.
							if (AppSettings.JobIssueFlagClosingDateRefreshed)
								jobIssue.ClosingDateRefreshed = true;

							// Deal with the flagging of feedback if necessary.
							if (AppSettings.JobIssueFlagNegativeFeedback || AppSettings.JobIssueFlagPositiveFeedback)
							{
								var lookup = Helpers.Lookup.GetLookup(LookupTypes.SatisfactionLevels);
								//GetCachedLookup(request, LookupTypes.SatisfactionLevels).ToList();

								var vsId = lookup.Single(y => y.Key == Constants.CodeItemKeys.SatisfactionLevels.VerySatisfied).Id;
								var ssId = lookup.Single(y => y.Key == Constants.CodeItemKeys.SatisfactionLevels.SomewhatSatisfied).Id;
								var sdId = lookup.Single(y => y.Key == Constants.CodeItemKeys.SatisfactionLevels.SomewhatDissatisfied).Id;
								var vdId = lookup.Single(y => y.Key == Constants.CodeItemKeys.SatisfactionLevels.VeryDissatisfied).Id;

                                if (request.PostingSurvey.SatisfactionLevel.IsNotNull() || request.PostingSurvey.DidHire.IsNotNull() || request.PostingSurvey.DidInterview.IsNotNull())
                                {
                                    if ((request.PostingSurvey.SatisfactionLevel.GetValueOrDefault(0) == vsId || request.PostingSurvey.SatisfactionLevel.GetValueOrDefault(0) == ssId)
                                        || request.PostingSurvey.DidHire.GetValueOrDefault() || request.PostingSurvey.DidInterview.GetValueOrDefault())
                                    {
                                        jobIssue.PositiveSurveyResponse = true;
                                    }
                                    
                                    if ((request.PostingSurvey.SatisfactionLevel.GetValueOrDefault(0) == vdId || request.PostingSurvey.SatisfactionLevel.GetValueOrDefault(0) == sdId)
                                             || !request.PostingSurvey.DidHire.GetValueOrDefault() || !request.PostingSurvey.DidInterview.GetValueOrDefault())
                                    {
                                        jobIssue.NegativeSurveyResponse = true;
                                    }
                                }
							}
						}
					}

					var jobPosting = request.PostingSurvey.CopyTo();
					job.PostingSurveys.Add(jobPosting);

					job.LastRefreshedOn = DateTime.Now;

					if (job.ClosedOn < DateTime.Now)
					{
						if (job.JobStatus == JobStatuses.Closed)
							// Set any referrals back depending on previous status
							UpdateReferralsForJob(job, new List<ApprovalStatuses> { ApprovalStatuses.OnHold });

						job.JobStatus = JobStatuses.Active;
					}

					if (request.NewNumberOfOpenings.IsNotNull())
					{
						if (request.NewNumberOfOpenings != job.NumberOfOpenings)
							job.NumberOfOpenings = request.NewNumberOfOpenings;
					}

					#region Update the posting details

					var postingGeneratorModel = GetPostingGeneratorModel(job);
					job.PostingHtml = new PostingGenerator(postingGeneratorModel, request.UserContext.Culture, AppSettings, Helpers.Localisation, Helpers.Lookup).Generate();

					#endregion

					#region Remove Previous edit data

					if (AppSettings.JobProofingEnabled)
					{
						var prevJobData = Repositories.Core.Find<JobPrevData>(jpd => jpd.JobId == job.Id).FirstOrDefault();
						if (prevJobData.IsNotNull())
						{
							Repositories.Core.Remove(prevJobData);
						}
					}

					#endregion

					Repositories.Core.SaveChanges();

					response.Job = job.AsDto();

					// Repost job to Lens
					if (job.PostedOn.IsNotNull())
						job.PostJob(RuntimeContext, request, request.UserContext.UserId, AppSettings.DefaultJobExpiry);

					LogAction(request, ActionTypes.RefreshJob, typeof(Job).Name, job.Id, null, null, job.ClosingOn.ToString());

				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Reactivate the job.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public JobResponse ReactivateJob(JobRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new JobResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var job = Repositories.Core.FindById<Job>(request.Criteria.JobId);

					if (job.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.JobNotFound));
						Logger.Info(request.LogData(), string.Format("The JobId '{0}' was not found.", request.Criteria.JobId));

						return response;
					}

					// reactivate only if job/employer is approved
					if (job.ApprovalStatus != ApprovalStatuses.Rejected && job.ApprovalStatus != ApprovalStatuses.WaitingApproval && job.JobStatus != JobStatuses.AwaitingEmployerApproval)
					{
						UpdateJobClosingDate(job, request.NewExpirationDate);

						job.JobStatus = JobStatuses.Active;

						// Set any referrals back depending on previous status
						UpdateReferralsForJob(job, new List<ApprovalStatuses> { ApprovalStatuses.OnHold });

						#region Update the posting details

						// Update the posting details
						var postingGeneratorModel = GetPostingGeneratorModel(job);

						job.PostingHtml = new PostingGenerator(postingGeneratorModel, request.UserContext.Culture, AppSettings, Helpers.Localisation, Helpers.Lookup).Generate();

						#endregion

						#region Remove Previous edit data

						if (AppSettings.JobProofingEnabled)
						{
							var prevJobData = Repositories.Core.Find<JobPrevData>(jpd => jpd.JobId == job.Id).FirstOrDefault();
							if (prevJobData.IsNotNull())
							{
								Repositories.Core.Remove(prevJobData);
							}
						}

						#endregion

						Repositories.Core.SaveChanges();

						response.Job = job.AsDto();

						// Repost job to career
						if (job.PostedOn.IsNotNull())
							job.PostJob(RuntimeContext, request, request.UserContext.UserId, AppSettings.DefaultJobExpiry);

						LogAction(request, ActionTypes.ReactivateJob, typeof(Job).Name, job.Id, null, null, job.ClosingOn.ToString());
					}
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Update on-hold referrals for a job back to their previous status if they were previously waiting for approval or under re-consideration
		/// </summary>
		/// <param name="job">The job to check for referrals</param>
		/// <param name="statusesToCheck">Only referrals with these status will be affected</param>
		private void UpdateReferralsForJob(Job job, List<ApprovalStatuses> statusesToCheck)
		{
			if (job.Posting.IsNull())
				return;

			var previousStatuses = new List<ApprovalStatuses?> { ApprovalStatuses.WaitingApproval, ApprovalStatuses.Reconsider };

			// Set any referrals back depending on previous status
			var applications = job.Posting.Applications.Where(x => statusesToCheck.Contains(x.ApprovalStatus) && previousStatuses.Contains(x.PreviousApprovalStatus)).ToList();

			foreach (var application in applications)
			{
				if (application.ApprovalStatus != ApprovalStatuses.OnHold || application.AutomaticallyOnHold == true)
				{
					application.ApprovalStatus = application.PreviousApprovalStatus.GetValueOrDefault();
				}
			}
		}

		/// <summary>
		/// Closes the job.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public JobResponse CloseJob(JobRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new JobResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var job = Repositories.Core.FindById<Job>(request.Criteria.JobId);

					if (job.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.JobNotFound));
						Logger.Info(request.LogData(), string.Format("The JobId '{0}' was not found.", request.Criteria.JobId));

						return response;
					}

					if ((AppSettings.JobIssueFlagJobClosedEarly || AppSettings.JobIssueFlagNegativeFeedback || AppSettings.JobIssueFlagPositiveFeedback)
					    && request.Module == FocusModules.Talent)
					{

						var lookup = Helpers.Lookup.GetLookup(LookupTypes.SatisfactionLevels);
						//GetCachedLookup(request, LookupTypes.SatisfactionLevels).ToList();

						var vsId = lookup.Single(y => y.Key == Constants.CodeItemKeys.SatisfactionLevels.VerySatisfied).Id;
						var ssId = lookup.Single(y => y.Key == Constants.CodeItemKeys.SatisfactionLevels.SomewhatSatisfied).Id;
						var sdId = lookup.Single(y => y.Key == Constants.CodeItemKeys.SatisfactionLevels.SomewhatDissatisfied).Id;
						var vdId = lookup.Single(y => y.Key == Constants.CodeItemKeys.SatisfactionLevels.VeryDissatisfied).Id;

						var jobClosingEarlyTriggered = job.ClosingOn != null && (job.ClosingOn.Value.AddDays(AppSettings.JobIssuePostClosedEarlyDaysThreshold * -1) > DateTime.Now && AppSettings.JobIssueFlagJobClosedEarly);

                        bool positiveFeedbackTriggered = false;
                        bool negativeFeedbackTriggered = false;

                        if (request.PostingSurvey.DidInterview.IsNotNull() || request.PostingSurvey.DidHire.IsNotNull() || request.PostingSurvey.SatisfactionLevel.IsNotNull())
                        {
                            positiveFeedbackTriggered = (request.PostingSurvey.SatisfactionLevel.GetValueOrDefault(0) == vsId || request.PostingSurvey.SatisfactionLevel.GetValueOrDefault(0) == ssId)
                                                           || request.PostingSurvey.DidHire.GetValueOrDefault() || request.PostingSurvey.DidInterview.GetValueOrDefault() && AppSettings.JobIssueFlagPositiveFeedback;

                            negativeFeedbackTriggered = (request.PostingSurvey.SatisfactionLevel.GetValueOrDefault(0) == vdId || request.PostingSurvey.SatisfactionLevel.GetValueOrDefault(0) == sdId)
                                                           || !request.PostingSurvey.DidHire.GetValueOrDefault() || !request.PostingSurvey.DidInterview.GetValueOrDefault() && AppSettings.JobIssueFlagNegativeFeedback;
                        }

						if (jobClosingEarlyTriggered || positiveFeedbackTriggered || negativeFeedbackTriggered)
						{
							// Get the related JobIssue, (if there isnt one then create one) and set the early closing field to true.
							var jobIssue = Repositories.Core.JobIssues.SingleOrDefault(x => x.JobId == job.Id);

							// If it is null the job issue doesnt exist so create and add one.
							if (jobIssue.IsNull())
							{
								jobIssue = new JobIssues
								{
									JobId = job.Id
								};
								Repositories.Core.Add(jobIssue);
							}

							if (!jobIssue.EarlyJobClosing)
								jobIssue.EarlyJobClosing = jobClosingEarlyTriggered;

							if (!jobIssue.NegativeSurveyResponse)
								jobIssue.NegativeSurveyResponse = negativeFeedbackTriggered;

							if (!jobIssue.PositiveSurveyResponse)
								jobIssue.PositiveSurveyResponse = positiveFeedbackTriggered;
						}
					}

					job.JobStatus = JobStatuses.Closed;
					job.ClosedOn = DateTime.Now;
					job.ClosedBy = request.UserContext.UserId;

					if (AppSettings.VeteranPriorityServiceEnabled && AppSettings.ExtendedVeteranPriorityofService && (job.ExtendVeteranPriority.IsNotNull() && job.ExtendVeteranPriority == ExtendVeteranPriorityOfServiceTypes.ExtendForLifeOfPosting))
						job.VeteranPriorityEndDate = job.ClosedOn;

					var jobPosting = request.PostingSurvey.CopyTo();
					job.PostingSurveys.Add(jobPosting);

					try
					{
						Helpers.Messaging.Publish(request.ToUnregisterPostingMessage(job.Posting.Id), updateIfExists: true, entityName: EntityTypes.Posting.ToString(), entityKey: job.Posting.Id);
					}
					catch (Exception ex)
					{
						Logger.Error(request.LogData(), ex.Message, ex);
					}

					// Deny the job seeker referral requests
					var postingId = job.Posting.Id;

					var statuses = new List<ApprovalStatuses> { ApprovalStatuses.Reconsider, ApprovalStatuses.OnHold, ApprovalStatuses.WaitingApproval };

					// Check if there are any pending referrals and place these On-Hold
					var applications = Repositories.Core.Applications.Where(x => x.PostingId.Equals(postingId) && statuses.Contains(x.ApprovalStatus)).ToList();

					foreach (var application in applications)
					{
						application.PreviousApprovalStatus = application.ApprovalStatus;
						application.ApprovalStatus = ApprovalStatuses.Rejected;
						application.AutomaticallyDenied = true;
						application.StatusLastChangedOn = DateTime.Now;
						application.StatusLastChangedBy = request.UserContext.UserId;

						#region Send job seeker an email informing them their referral request has been auto denied

						try
						{
							var templateValues = new EmailTemplateData
							{
								RecipientName = application.Resume.Person.FirstName,
								JobTitle = job.Posting.JobTitle,
								EmployerName = job.Employer.Name
							};

							var emailTemplate = Helpers.Email.GetEmailTemplate(EmailTemplateTypes.AutoDeniedReferralRequest);

							if (emailTemplate.IsNull())
							{
								response.SetFailure(FormatErrorMessage(request, ErrorTypes.EmailTemplateNotConfigured));
								Logger.Info(request.LogData(), string.Format("The email template '{0}' was not found.", EmailTemplateTypes.AutoDeniedReferralRequest));
								return response;
							}

							if (emailTemplate.Body.IsNull() || application.Resume.Person.EmailAddress.IsNullOrEmpty())
							{
								response.SetFailure(FormatErrorMessage(request, ErrorTypes.EmailTemplateNotConfigured));
								Logger.Info(request.LogData(), string.Format("The email template '{0}' is not configured.", EmailTemplateTypes.AutoDeniedReferralRequest));
								return response;
							}

							Helpers.Email.SendEmailFromTemplate(EmailTemplateTypes.AutoDeniedReferralRequest, templateValues, application.Resume.Person.EmailAddress, string.Empty, string.Empty);
						}
						catch (Exception ex)
						{
							Logger.Error(request.LogData(), ex.Message, ex);
						}

						#endregion
					}

					#region Remove Previous edit data

					if (AppSettings.JobProofingEnabled)
					{
						var prevJobData = Repositories.Core.Find<JobPrevData>(jpd => jpd.JobId == job.Id).FirstOrDefault();
						if (prevJobData.IsNotNull())
						{
							Repositories.Core.Remove(prevJobData);
						}
					}

					#endregion

					Repositories.Core.SaveChanges();

					response.Job = job.AsDto();

					LogAction(request, ActionTypes.CloseJob, job);

					if (applications.IsNotNullOrEmpty())
					{
						var resumeIds = applications.Select(a => a.ResumeId).Distinct().ToList();
						var resumeNames = Repositories.Core.Resumes.Where(r => resumeIds.Contains(r.Id))
																							 .Select(r => new {r.Id, r.FirstName, r.LastName})
																							 .ToDictionary(r => r.Id, r => String.Format("{0} {1}", r.FirstName, r.LastName));

						foreach (var application in applications)
						{
							Helpers.Logging.LogAction(ActionTypes.UpdateReferralStatusToAutoDenied, typeof(Application).Name, application.Id, null, null, resumeNames[application.ResumeId]);
						}
					}
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Flags a job for follow up.
		/// </summary>
		/// <param name="request">The requst.</param>
		/// <returns></returns>
		public JobResponse FlagJobForFollowUp(JobRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new JobResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var jobIssue = Repositories.Core.JobIssues.SingleOrDefault(x => x.JobId == request.Criteria.JobId);

					if (jobIssue.IsNull())
					{
						jobIssue = new JobIssues
						{
							JobId = request.Criteria.JobId ?? 0
						};
						Repositories.Core.Add(jobIssue);
					}

					jobIssue.FollowUpRequested = true;
					Repositories.Core.SaveChanges();

				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Updates the job status.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public JobResponse UpdateJobStatus(JobRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new JobResponse(request);
				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var job = Repositories.Core.FindById<Job>(request.Criteria.JobId);
					if (job.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.JobNotFound));
						Logger.Info(request.LogData(), string.Format("The JobId '{0}' was not found.", request.Criteria.JobId));
						return response;
					}

					job.JobStatus = request.JobStatus;
					if (request.ApprovalStatus.HasValue)
						job.ApprovalStatus = request.ApprovalStatus.Value;

					if (request.JobStatus == JobStatuses.OnHold)
					{
						job.HeldOn = DateTime.Now;
						job.HeldBy = request.UserContext.UserId;
						job.Posting.Applications.Where(a => a.ApprovalStatus == ApprovalStatuses.WaitingApproval || a.ApprovalStatus == ApprovalStatuses.Reconsider)
							.ToList().ForEach(a => Helpers.Candidate.HoldReferral(a.Id, ApplicationOnHoldReasons.JobPutOnHold, true, request.UserContext.UserId));
					}

					Repositories.Core.SaveChanges();
					response.Job = job.AsDto();
					// Repost job to career
					if (job.PostedOn.IsNotNull())
					{
						if (request.JobStatus.IsIn(JobStatuses.OnHold, JobStatuses.Draft))
							Helpers.Messaging.Publish(request.ToUnregisterPostingMessage(job.Posting.Id), updateIfExists: true, entityName: EntityTypes.Posting.ToString(), entityKey: job.Posting.Id);
						else
							job.PostJob(RuntimeContext, request, request.UserContext.UserId, AppSettings.DefaultJobExpiry);
					}

					switch (request.JobStatus)
					{
						case JobStatuses.Active:
							LogAction(request, ActionTypes.ReactivateJob, job);
							break;
						case JobStatuses.OnHold:
							LogAction(request, ActionTypes.HoldJob, job);
							break;
						case JobStatuses.Draft:
							break;
						default:
							response.SetFailure(FormatErrorMessage(request, ErrorTypes.InvalidStatusUpdate));
							Logger.Info(request.LogData(), string.Format("Cannot update the JobStatus '{0}' directly.", request.JobStatus));
							break;
					}
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Duplicates the job.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public JobResponse DuplicateJob(JobRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new JobResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var job = Repositories.Core.FindById<Job>(request.Criteria.JobId);

					if (job.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.JobNotFound));
						Logger.Info(request.LogData(), string.Format("The JobId '{0}' was not found.", request.Criteria.JobId));

						return response;
					}

					var newJob = job.Clone();

					newJob.ClosedOn = newJob.PostedOn = null;
					newJob.WizardStep = 0;
					newJob.JobStatus = JobStatuses.Draft;
					newJob.ClosingOn = DateTime.Today.AddDays(30).Date;
					newJob.ExternalId = null;
					// Only change the job's employee if the user doing the duplication is an actual Talent user and not an Assist user.
					if (AppSettings.Module == FocusModules.Talent && !request.UserContext.IsShadowingUser)
						newJob.Employee = Repositories.Core.Employees.FirstOrDefault(e => e.PersonId == request.UserContext.PersonId);

					Repositories.Core.Add(newJob);
					Repositories.Core.SaveChanges();

					response.Job = newJob.AsDto();

					LogAction(request, ActionTypes.DuplicateJob, typeof(Job).Name, job.Id, job.BusinessUnitId);

                    //FVN - 5727 - Added entry for Create Job action during duplication based on Gail's comments
                    LogAction(request, ActionTypes.CreateJob, typeof(Job).Name, newJob.Id, job.BusinessUnitId, overrideUserId:request.UserContext.ActionerId);
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Deletes the job.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public JobResponse DeleteJob(JobRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new JobResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var job = Repositories.Core.FindById<Job>(request.Criteria.JobId);

					if (job.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.JobNotFound));
						Logger.Info(request.LogData(), string.Format("The JobId '{0}' was not found.", request.Criteria.JobId));

						return response;
					}

					#region Remove Previous edit data

					if (AppSettings.JobProofingEnabled)
					{
						var prevJobData = Repositories.Core.Find<JobPrevData>(jpd => jpd.JobId == job.Id).FirstOrDefault();
						if (prevJobData.IsNotNull())
						{
							Repositories.Core.Remove(prevJobData);
						}
					}

					#endregion

					Repositories.Core.Remove(job);
					Repositories.Core.SaveChanges(true);

					response.Job = job.AsDto();

					LogAction(request, ActionTypes.DeleteJob, typeof(Job).Name, job.Id, job.BusinessUnitId);
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Save details from the specified job into JobPrevData for later proofing
		/// </summary>
		/// <param name="request"></param>
		/// <returns></returns>
		public JobResponse SaveBeforeImageOfJob(JobRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new JobResponse( request );
				if( AppSettings.JobProofingEnabled && request.Module == FocusModules.Assist && request.Criteria.JobId.HasValue )
				{
					// Validate request
					if (!ValidateRequest(request, response, Validate.All))
					{
						return response;
					}
					try
					{
						var jobId = request.Criteria.JobId.Value;

						var job = Repositories.Core.FindById<Job>( jobId );

						if (job.IsNull())
						{
							response.SetFailure(FormatErrorMessage(request, ErrorTypes.JobNotFound));
							Logger.Info(request.LogData(), string.Format("The JobId '{0}' was not found.", jobId));

							return response;
						}

						var prevJobData = Repositories.Core.Find<JobPrevData>(jpd => jpd.JobId == jobId).FirstOrDefault();

						if (prevJobData.IsNull())
						{
							prevJobData = new JobPrevData { JobId = jobId };
							Repositories.Core.Add(prevJobData);
						}

						#region Generate posting view

						var postingGeneratorModel = GetPostingGeneratorModel(job);
						var postingView = new PostingGenerator(postingGeneratorModel, request.UserContext.Culture, AppSettings, Helpers.Localisation, Helpers.Lookup).GenerateView();

						#endregion

						prevJobData.ClosingDate = job.ClosingOn;
						prevJobData.CourtOrderedAffirmativeActionDetails = postingView.CourtOrderedAffirmativeAction;
						prevJobData.Description = job.Description;
						prevJobData.FederalContractorDetails = postingView.FederalContractor;
						prevJobData.ForeignLabourCertificationDetails = postingView.ForeignLabourCertification;
						prevJobData.JobDetails = AppSettings.Theme == FocusThemes.Workforce ? GetJobSection( postingView, JobDisplaySection.Details ) : null;
						prevJobData.JobRequirements = GetJobSection( postingView, JobDisplaySection.Requirements );
						prevJobData.JobSalaryBenefits = AppSettings.Theme == FocusThemes.Workforce ? GetJobSection(postingView, JobDisplaySection.SalaryBenefits) : null;
						prevJobData.NumOpenings = job.NumberOfOpenings;
						prevJobData.RecruitmentInformation = GetJobSection( postingView, JobDisplaySection.RecruitmentInformation );

						var jobLocations = GetJobLocations(jobId);
						if (jobLocations.IsNotNull())
						{
							prevJobData.Locations = string.Empty;
							foreach (var jobLocation in jobLocations.Where(jobLocation => jobLocation.JobId == jobId))
							{
								prevJobData.Locations += jobLocation.Location + "<br>";
							}
						}

						Repositories.Core.SaveChanges(true);
						LogAction(request, ActionTypes.DeleteJob, typeof(Job).Name, request.Criteria.JobId, job.BusinessUnitId);
					}

					catch (Exception ex)
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
						Logger.Error(request.LogData(), response.Message, response.Exception);
					}
				}
				return response;
			}
		}

		/// <summary>
		/// Saves the job.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public SaveJobResponse SaveJob(SaveJobRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new SaveJobResponse(request);
				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					Job job;
					var newJob = false;
					bool generateJobPosting;
					if (request.Job.Id.IsNotNull())
					{
						job = Repositories.Core.FindById<Job>(request.Job.Id);
						if (job.IsNull())
						{
							response.SetFailure(FormatErrorMessage(request, ErrorTypes.JobNotFound));
							Logger.Info(request.LogData(), string.Format("The JobId '{0}' was not found.", request.Job.Id));
							return response;
						}

						// Check if employee has been validated since job was started to be edited
						if (request.Job.JobStatus == JobStatuses.AwaitingEmployerApproval)
						{
							var employee = Repositories.Core.FindById<Employee>(job.EmployeeId);
							if (employee.ApprovalStatus == ApprovalStatuses.Approved)
								request.Job.JobStatus = job.JobStatus;
						}
						if( request.Module != FocusModules.Assist || !AppSettings.JobProofingEnabled || (request.Job.JobStatus != JobStatuses.OnHold && request.Job.JobStatus != JobStatuses.Draft) )
						{
							var prevJobData = Repositories.Core.Find<JobPrevData>(jpd => jpd.JobId == request.Job.Id).FirstOrDefault();
							if (prevJobData.IsNotNull())
							{
								Repositories.Core.Remove(prevJobData);
							}
						}

						if (request.Job.InterviewPhoneNumber.IsNotNullOrEmpty())
							request.Job.InterviewPhoneNumber = new string(request.Job.InterviewPhoneNumber.Where(char.IsDigit).ToArray());

						if (request.Job.InterviewFaxNumber.IsNotNullOrEmpty())
							request.Job.InterviewFaxNumber = new string(request.Job.InterviewFaxNumber.Where(char.IsDigit).ToArray());

						var currentApprovalStatus = job.ApprovalStatus;
						request.Job.PreviousApprovalStatus = currentApprovalStatus;

						job = request.Job.CopyTo(job);
						// Update the posting details
						generateJobPosting = true;
						// VPS1: Clear down veteran search date if not veterans not specified (No longer depends on WOTC)
						//if ((job.WorkOpportunitiesTaxCreditHires & WorkOpportunitiesTaxCreditCategories.Veterans) == 0)
						//  job.VeteranPriorityEndDate = null;
					}
					else
					{
						job = new Job();
						job = request.Job.CopyTo(job);
						job.CreatedBy = request.JobCreatedBy ?? request.UserContext.UserId;
						job.JobStatus = request.InitialJobStatus ?? JobStatuses.Draft;
						Repositories.Core.Add(job);
						newJob = true;
						generateJobPosting = request.GeneratePostingForNewJob;
					}

					if (request.JobLocations.IsNotNull())
					{
						if (!request.JobLocations.Any())
						{
							var hiringManagerAddress = (from employee in Repositories.Core.Employees
								join person in Repositories.Core.Persons
									on employee.PersonId equals person.Id
								join personAddress in Repositories.Core.PersonAddresses
									on person.Id equals personAddress.PersonId
								where employee.Id == job.EmployeeId
								      && personAddress.IsPrimary
								select personAddress.AsDto()).FirstOrDefault();
							if (hiringManagerAddress.IsNotNull())
							{
								var state = Helpers.Lookup.GetLookupText(LookupTypes.States, hiringManagerAddress.StateId);
								request.JobLocations.Add(new JobLocationDto
								{
									JobId = job.Id,
									IsPublicTransitAccessible = true,
									Location = state.IsNotNullOrEmpty()
										? string.Format("{0}, {1} ({2})", hiringManagerAddress.TownCity, state, hiringManagerAddress.PostcodeZip)
										: string.Format("{0}, ({1})", hiringManagerAddress.TownCity, hiringManagerAddress.PostcodeZip)
								});
							}
						}

						SaveJobLocations(job, request.JobLocations, false, newJob);
					}

					if (request.JobAddress.IsNotNull())
						SaveJobAddress(job.Id, request.JobAddress, false, newJob);

					if (request.JobCertificates.IsNotNull())
						SaveJobCertificates(job.Id, request.JobCertificates, false, newJob);

					if (request.JobLicences.IsNotNull())
						SaveJobLicences(job.Id, request.JobLicences, false, newJob);

					if (request.JobLanguages.IsNotNull())
						SaveJobLanguages(job.Id, request.JobLanguages, false, newJob);

					if (request.JobSpecialRequirements.IsNotNull())
						SaveJobSpecialRequirements(job.Id, request.JobSpecialRequirements, false, newJob);

					if (request.JobProgramsOfStudy.IsNotNull())
						SaveJobProgramsOfStudy(job.Id, request.JobProgramsOfStudy, false, newJob);

					if (request.JobDrivingLicenceEndorsements.IsNotNull())
						SaveJobDrivingLicenceEndorsements(job.Id, request.JobDrivingLicenceEndorsements, false, newJob);

					if (generateJobPosting)
					{
						job.GeneratePostingHtml(RuntimeContext, request, request.Job, request.JobLicences, request.JobCertificates, request.JobLanguages, request.JobSpecialRequirements, request.JobLocations, request.JobProgramsOfStudy, request.JobDrivingLicenceEndorsements);
					}
					if (request.Job.AwaitingApprovalOn == null || request.Job.AwaitingApprovalOn == DateTime.MinValue || request.Job.AwaitingApprovalOn == DateTime.MaxValue)
						request.Job.AwaitingApprovalOn = request.Job.CreatedOn;

					Repositories.Core.SaveChanges(true);
					response.Job = job.AsDto();
					response.Job.PreviousApprovalStatus = request.Job.PreviousApprovalStatus;

					#region LogAction

					if (newJob)
						LogAction(request, ActionTypes.CreateJob, typeof(Job).Name, job.Id, job.BusinessUnitId, overrideUserId: request.JobCreatedBy);
					else if (job.PostedBy.IsNotNull())
						LogAction(request, ActionTypes.SaveJob, job, request.JobCreatedBy);
					else
						LogAction(request, ActionTypes.SaveJobWizardStep, typeof(Job).Name, job.Id, job.WizardStep, overrideUserId: request.JobCreatedBy);

					#endregion
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Posts the job.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public PostJobResponse PostJob(PostJobRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new PostJobResponse(request) { ContentRating = ContentRatings.Acceptable };
				Employee employee = null;
				var unregisterPosting = false;

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var censorshipRules = GetCensorshipRules(CensorshipType.Profanity);
					var censorshipCheckedSkipped = false;

					// Will this ever be a new job?
					var job = Repositories.Core.FindById<Job>(request.Job.Id);

					if (job.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.JobNotFound));
						Logger.Info(request.LogData(), string.Format("The JobId '{0}' was not found.", request.Job.Id));

						return response;
					}

					var isDraft = (job.JobStatus == JobStatuses.Draft);
					var isStaffAssisted = AppSettings.Module == FocusModules.Assist || request.UserContext.IsShadowingUser;

					var approvalDefault = isStaffAssisted
																 ? AppSettings.ApprovalDefaultForStaffCreatedJobs
																 : AppSettings.ApprovalDefaultForCustomerCreatedJobs;

					if ((request.Job.AwaitingApprovalOn == null || request.Job.AwaitingApprovalOn == DateTime.MinValue || request.Job.AwaitingApprovalOn == DateTime.MaxValue) && request.InitialApprovalStatus.GetValueOrDefault(ApprovalStatuses.None).IsNotIn(ApprovalStatuses.Approved, ApprovalStatuses.None))
						request.Job.AwaitingApprovalOn = request.Job.CreatedOn;

					job = request.Job.CopyTo(job);
					var initialApprovalStatus = request.InitialApprovalStatus.GetValueOrDefault(ApprovalStatuses.None);
					job.ApprovalStatus = initialApprovalStatus;

					if (request.InitialJobStatus.HasValue)
						job.JobStatus = request.InitialJobStatus.Value;

					if (AppSettings.OfficesEnabled && request.Module == FocusModules.Assist)
						job.AssignedToId = request.UserContext.PersonId;

					// No need to do the censorship check if this is an Assist user carrying out the posting so we shall set the status to approved
					/* FVN-3606 - Assist user changes still need approval
					if (initialApprovalStatus == ApprovalStatuses.None && (request.UserContext.IsShadowingUser || request.Module == FocusModules.Assist))
					{
						employee = Repositories.Core.FindById<Employee>(job.EmployeeId);

						// Don't approve if employee has yet to be approved
						if (employee.ApprovalStatus.IsNotIn(ApprovalStatuses.WaitingApproval, ApprovalStatuses.Rejected))
							job.ApprovalStatus = ApprovalStatuses.Approved;

						censorshipCheckedSkipped = true;
					}
					*/
					#region Approval checking

					#region Censorship checking

					// Censorship (Profanity) Check
					// Return ProfanityResult 
					// RedWordsFound, YellowWordsFound, Flag = Red/Yellow
					if (censorshipRules.IsNullOrEmpty())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown));
						Logger.Error(request.LogData(), response.Message, response.Exception);
						return response;
					}

					var postingHtml = Regex.Replace(request.Job.PostingHtml, @"<span data-nocensorship=""true"">[^<]*<\/span>", "");
					// Special requirements are shown to job seeker even if not on the posting HTML so censor check them by appending to job posting
					var censorshipCheck = !job.JobSpecialRequirements.IsNotNullOrEmpty() 
								? new CensorshipCheck(censorshipRules, string.Concat(postingHtml, " ", string.Join(",", job.JobSpecialRequirements.Select(requirement => requirement.Requirement).ToList()))) 
								: new CensorshipCheck(censorshipRules, postingHtml);

					job.RedProfanityWords = null;
					job.YellowProfanityWords = null;

					if (censorshipCheck.HasCensorships)
					{
						// Check for red words
						var redWords = censorshipCheck.FoundCensorshipRules.Where(x => x.Level == CensorshipLevel.Red).ToList();

						if (redWords.Count > 0)
						{
							job.ApprovalStatus = ApprovalStatuses.Rejected;
							var redProfanityWords = string.Join(",", redWords.Select(x => x.Phrase)).Trim(1000, true);

							job.RedProfanityWords = redProfanityWords.Length > 1000 ? redProfanityWords.Substring(0, redProfanityWords.LastIndexOf(',')) : redProfanityWords;

							response.InappropiateWords = redWords.Select(x => x.Phrase).ToList();
							response.ContentRating = ContentRatings.BlackListed;
						}
						else
						{
							// Ensure red word field is cleared down
							job.RedProfanityWords = string.Empty;

                            //FVN-5441 - checked the approval default value yellow word filtering is applied only when approval default is not set to 'NoApproval'
							// Check for yellow words
                            if (((AppSettings.Module == FocusModules.Talent && AppSettings.ActivationYellowWordFiltering) || (isStaffAssisted && AppSettings.ActivateYellowWordFilteringForStaffCreatedJobs)) && approvalDefault != JobApprovalOptions.NoApproval && initialApprovalStatus == ApprovalStatuses.None && job.ApprovalStatus != ApprovalStatuses.Approved)
							{
								var yellowWords = censorshipCheck.FoundCensorshipRules.Where(x => x.Level == CensorshipLevel.Yellow).ToList();

								if (yellowWords.Count > 0)
								{
									SetJobForApproval(job, request, ref unregisterPosting);

									var yellowProfanityWords = string.Join(",", yellowWords.Select(x => x.Phrase)).Trim(1000, true);

									job.YellowProfanityWords = yellowProfanityWords.Length > 1000 ? yellowProfanityWords.Substring(0, yellowProfanityWords.LastIndexOf(',')) : yellowProfanityWords;

									response.InappropiateWords = yellowWords.Select(x => x.Phrase).ToList();
									response.ContentRating = ContentRatings.GreyListed;
								}
							}
						}
					}

					#endregion

					#region All postings to be approved checking

					// Check application setting to see if all postings have to be approved
					if (initialApprovalStatus == ApprovalStatuses.None && job.ApprovalStatus == ApprovalStatuses.None && approvalDefault == JobApprovalOptions.ApproveAll)
					{
						SetJobForApproval(job, request, ref unregisterPosting);
					}

					#endregion

					#region Configurable approval checks

					if (approvalDefault == JobApprovalOptions.SpecifiedApproval)
					{
						var approvalChecks = isStaffAssisted
																	 ? AppSettings.ApprovalChecksForStaffCreatedJobs
																	 : AppSettings.ApprovalChecksForCustomerCreatedJobs;

						// Job conditions checking - Job needs to be approved in certain job conditions
						if (initialApprovalStatus == ApprovalStatuses.None && job.ApprovalStatus == ApprovalStatuses.None)
						{
							// Workforce jobs can have a minimum age and if the reason type is 'other' the job must be approved
							// Validation of minimum age is not configurable
							if (job.MinimumAgeReasonValue.GetValueOrDefault(0) == 4)
							{
								SetJobForApproval(job, request, ref unregisterPosting);
							}
						}

						if (initialApprovalStatus == ApprovalStatuses.None && job.ApprovalStatus == ApprovalStatuses.None)
						{
							if ((job.CourtOrderedAffirmativeAction.GetValueOrDefault() && approvalChecks.Contains(JobApprovalCheck.CourtOrdered))
								|| (job.FederalContractor.GetValueOrDefault() && approvalChecks.Contains(JobApprovalCheck.FederalContractor))
								|| (job.ForeignLabourCertificationH2A.GetValueOrDefault() && approvalChecks.Contains(JobApprovalCheck.ForeignLaborH2A))
								|| (job.ForeignLabourCertificationH2B.GetValueOrDefault() && approvalChecks.Contains(JobApprovalCheck.ForeignLaborH2B))
								|| (job.ForeignLabourCertificationOther.GetValueOrDefault() && approvalChecks.Contains(JobApprovalCheck.ForeignLaborOther))
								|| (job.IsCommissionBased.GetValueOrDefault() && approvalChecks.Contains(JobApprovalCheck.ComissionBased)) 
								|| (job.IsSalaryAndCommissionBased.GetValueOrDefault() && approvalChecks.Contains(JobApprovalCheck.ComissionSalary)))
							{
								SetJobForApproval(job, request, ref unregisterPosting);
								response.SpecialConditions = true;
							}
						}

						// Other checks
						if (initialApprovalStatus == ApprovalStatuses.None && job.ApprovalStatus == ApprovalStatuses.None)
						{
							if ((job.SuitableForHomeWorker.GetValueOrDefault() && approvalChecks.Contains(JobApprovalCheck.HomeBased))
								|| (job.JobLocationType == JobLocationTypes.NoFixedLocation && approvalChecks.Contains(JobApprovalCheck.HomeBased)) // No Fixed Location is linked to HomeBased job, so share same config
								|| (job.CreditCheckRequired.GetValueOrDefault() && approvalChecks.Contains(JobApprovalCheck.CreditCheck))
								|| (job.CriminalBackgroundExclusionRequired.GetValueOrDefault() && approvalChecks.Contains(JobApprovalCheck.CriminalBackground))
								|| (job.JobSpecialRequirements.IsNotNullOrEmpty() && approvalChecks.Contains(JobApprovalCheck.SpecialRequirements)))
							{
								SetJobForApproval(job, request, ref unregisterPosting);
							}
						}
					}

					#endregion

					#endregion
                    if (job.RedProfanityWords.IsNullOrEmpty())
                    {
                        employee = Repositories.Core.FindById<Employee>(job.EmployeeId);
                        var employer = Repositories.Core.FindById<Focus.Data.Core.Entities.Employer>(job.EmployerId);

                        if (employer.ApprovalStatus.IsIn(ApprovalStatuses.WaitingApproval, ApprovalStatuses.Rejected))
                            job.JobStatus = JobStatuses.AwaitingEmployerApproval;

                        else if (employee.ApprovalStatus.IsIn(ApprovalStatuses.WaitingApproval, ApprovalStatuses.Rejected))
                            job.JobStatus = JobStatuses.Draft;
                    }

					if (job.ApprovalStatus == ApprovalStatuses.None || job.Employee.ApprovalStatus == ApprovalStatuses.WaitingApproval || job.Employer.ApprovalStatus == ApprovalStatuses.WaitingApproval || job.BusinessUnit.ApprovalStatus == ApprovalStatuses.WaitingApproval)
					{
						// Check if employee has been validated, the current user may not be the employee so use the employeeId on the record
						if (employee.IsNull())
							employee = Repositories.Core.FindById<Employee>(job.EmployeeId);

						// Employee could be rejected if still logged in when Assist User Rejects
						if (employee.ApprovalStatus.IsIn(ApprovalStatuses.WaitingApproval, ApprovalStatuses.Rejected, ApprovalStatuses.OnHold))
						{
							job.JobStatus = JobStatuses.AwaitingEmployerApproval;
                            job.ApprovalStatus = ApprovalStatuses.None;
						}
						else
						{
							var approvalStatuses = (from e in Repositories.Core.Employers
								join bu in Repositories.Core.BusinessUnits
									on e.Id equals bu.EmployerId
								where bu.Id == job.BusinessUnitId
								select new
								{
									EmployerApprovalStatus = e.ApprovalStatus,
									BusinessUnitApprovalStatus = bu.ApprovalStatus
								}).FirstOrDefault();

							if (approvalStatuses != null && (approvalStatuses.BusinessUnitApprovalStatus == ApprovalStatuses.WaitingApproval || approvalStatuses.EmployerApprovalStatus == ApprovalStatuses.WaitingApproval))
							{
								job.JobStatus = JobStatuses.AwaitingEmployerApproval;
                                job.ApprovalStatus = ApprovalStatuses.None;
							}
						}
					}

                    if (isStaffAssisted && ((job.ApprovalStatus == ApprovalStatuses.None && job.YellowProfanityWords.IsNullOrEmpty())||(job.YellowProfanityWords.IsNotNullOrEmpty() && approvalDefault == JobApprovalOptions.NoApproval)) && request.Job.JobStatus == JobStatuses.OnHold && job.RedProfanityWords.IsNullOrEmpty())
                        job.JobStatus = JobStatuses.Active;

					var jobList = new List<Job> { job };

					// We do not want to post rejected or waiting approval jobs or jobs where the employer has not been validated
					if ((job.ApprovalStatus.IsNotIn(ApprovalStatuses.Rejected, ApprovalStatuses.WaitingApproval)
								&& (request.Job.PreviousApprovalStatus != ApprovalStatuses.Reconsider || isStaffAssisted)
								&& job.JobStatus.IsNotIn(JobStatuses.AwaitingEmployerApproval) && (request.Job.JobStatus.IsNotIn(JobStatuses.OnHold) || isStaffAssisted)) 
					|| (request.IgnoreLens && request.InitialLensPostingId.IsNotNullOrEmpty()))
					{
						if (!request.IgnoreLens || (request.InitialLensPostingId.IsNullOrEmpty() && request.InitialJobStatus.IsNull()))
							job.JobStatus = JobStatuses.Active;

						// Clone the job for each job location
						jobList = job.SplitJobByLocation(RuntimeContext, request);

						var initialLensId = request.IgnoreLens && jobList.Count == 1 ? request.InitialLensPostingId : null;

						foreach (var jobToPost in jobList)
						{
							jobToPost.PostJob(RuntimeContext, request, request.UserContext.ActionerId, AppSettings.DefaultJobExpiry, initialLensId, false, request.IgnoreClient, request.OfficeMigrationId);
						}

						Repositories.Core.SaveChanges(true);

						if (isDraft)
							CreateAlertMessage(Constants.AlertMessageKeys.JobPosted, job.Employee.Person.User.Id, DateTime.UtcNow.AddDays(2),
								MessageAudiences.User, MessageTypes.General, null, job.JobTitle,
								request.UserContext.FirstName + " " + request.UserContext.LastName);
					}

					if (job.OnetId.IsNotNull())
					{
						// add job family id to employee for populating HIRING AREAS on employer profile
						var jobFamilyId = Repositories.Library.FindById<Onet>(job.OnetId).JobFamilyId;

						if (
							!Repositories.Core.Find<EmployeeJobFamily>().Any(x => x.EmployeeId == job.EmployeeId && x.JobFamilyId == jobFamilyId))
						{
							var employeeJobFamily = new EmployeeJobFamily { Employee = job.Employee, JobFamilyId = jobFamilyId };
							Repositories.Core.Add(employeeJobFamily);
						}
					}

					var newApprovalStatus = job.ApprovalStatus == ApprovalStatuses.None
						? ApprovalStatuses.Approved
						: job.ApprovalStatus;

					if (newApprovalStatus == ApprovalStatuses.Rejected && job.JobStatus == JobStatuses.Draft && job.RedProfanityWords.IsNotNullOrEmpty())
						newApprovalStatus = ApprovalStatuses.None;

                    if ((newApprovalStatus == ApprovalStatuses.Rejected && job.RedProfanityWords.IsNotNullOrEmpty()) && (job.JobStatus == JobStatuses.Active || job.JobStatus == JobStatuses.OnHold))
                    {
                        newApprovalStatus = ApprovalStatuses.OnHold;
                        job.JobStatus = JobStatuses.OnHold;
                        unregisterPosting = true;
                        job.HeldOn = DateTime.Now;
                    }

                    if ((newApprovalStatus == ApprovalStatuses.WaitingApproval && job.YellowProfanityWords.IsNotNullOrEmpty() && job.JobLocationType != JobLocationTypes.NoFixedLocation) && (job.JobStatus == JobStatuses.Active || job.JobStatus == JobStatuses.OnHold))
                    {
                        newApprovalStatus = ApprovalStatuses.WaitingApproval;
                        job.JobStatus = JobStatuses.OnHold;
                        unregisterPosting = true;
                        job.HeldOn = DateTime.Now;
                    }

					if (((request.Job.PreviousApprovalStatus == ApprovalStatuses.Rejected && newApprovalStatus == ApprovalStatuses.WaitingApproval) || (request.Job.PreviousApprovalStatus == ApprovalStatuses.Reconsider && job.JobStatus == JobStatuses.Draft))
					&& job.RedProfanityWords.IsNullOrEmpty() && !isStaffAssisted)
						newApprovalStatus = ApprovalStatuses.Reconsider;

                    if (request.Job.JobStatus == JobStatuses.OnHold && job.RedProfanityWords.IsNullOrEmpty() && job.YellowProfanityWords.IsNullOrEmpty() && (request.Job.RedProfanityWords.IsNotNullOrEmpty()|| request.Job.YellowProfanityWords.IsNotNullOrEmpty()) && !isStaffAssisted)
                    {
                        newApprovalStatus = ApprovalStatuses.None;
                        job.JobStatus = JobStatuses.OnHold;
                        job.AwaitingApprovalOn = null;
                    }

					foreach (var jobToPost in jobList)
					{
						jobToPost.UpdatedBy = request.UserContext.UserId;
						jobToPost.ApprovalStatus = newApprovalStatus;
					}

					// Save Job and mark as posted
					Repositories.Core.SaveChanges(true);
					response.Job = job.AsDto();

					foreach (var jobToPost in jobList)
					{
						jobToPost.AssignOfficesToJob(RuntimeContext, request);
					}

					if ((!job.HiringFromTaxCreditProgramNotificationSent.HasValue ||
					     !job.HiringFromTaxCreditProgramNotificationSent.Value)
					    &&
					    (job.WorkOpportunitiesTaxCreditHires.HasValue &&
					     job.WorkOpportunitiesTaxCreditHires != WorkOpportunitiesTaxCreditCategories.None)
					    && AppSettings.SendHiringFromTaxCreditProgramNotification)
					{
						if (employee.IsNotNull())
						{
							{
								var phone = employee.Person.PhoneNumbers.Where(x => x.IsPrimary).Select(x => x.AsDto()).FirstOrDefault();
								var offices = Repositories.Core.JobOfficeMappers.Where(o => o.JobId == job.Id && (!o.OfficeUnassigned.HasValue || !o.OfficeUnassigned.Value)).Select(o => o.Office).ToList();
								var coreService = new CoreService(RuntimeContext);
								var emailResponses = new List<EmailHiringFromTaxCreditProgramNotificationResponse>();

								offices.Select(office => new EmailHiringFromTaxCreditProgramNotificationRequest
								{
									EmailTo = office.BusinessOutreachMailbox,
									WorkOpportunitiesTaxCreditHires = job.WorkOpportunitiesTaxCreditHires,
									Name = String.Format("{0} {1}", employee.Person.FirstName, employee.Person.LastName),
									HiringManagerEmailAddress = employee.Person.EmailAddress,
									PhoneNumber = phone.IsNotNull() && phone.Number.IsNotNullOrEmpty() ? phone.Number : "",
									PostedOn = job.PostedOn,
									UserContext = request.UserContext,
									ClientTag = request.ClientTag
								}).ToList()
									.ForEach(r => emailResponses.Add(coreService.SendHiringFromTaxCreditProgramNotification(r)));

								if (emailResponses.All(r => r.Acknowledgement == AcknowledgementType.Success))
								{
									foreach (var jobToPost in jobList)
									{
										jobToPost.HiringFromTaxCreditProgramNotificationSent = true;
									}
									LogAction(request, ActionTypes.HiringFromTaxCreditProgramNotificationSent, typeof(Job).Name, job.Id);
								}
							}
						}
					}

					if (unregisterPosting)
						if (job.Posting.IsNotNull())
							Helpers.Messaging.Publish(request.ToUnregisterPostingMessage(job.Posting.Id), updateIfExists: true, entityName: EntityTypes.Posting.ToString(), entityKey: job.Posting.Id);

					#region Log actions

					// Do the logging
					if (censorshipCheckedSkipped)
						LogAction(request, ActionTypes.SkipPostJobProfanityCheck, typeof(Job).Name, job.Id);

					var logActionType = ActionTypes.NoAction;

					switch (job.ApprovalStatus)
					{
						case ApprovalStatuses.Rejected:
							logActionType = ActionTypes.RejectJob;
							break;

						case ApprovalStatuses.WaitingApproval:
							logActionType = ActionTypes.ReferJob;
							break;

						case ApprovalStatuses.Approved:
							switch (job.JobStatus)
							{
								case JobStatuses.AwaitingEmployerApproval:
									logActionType = ActionTypes.HoldPostJobUntilEmployeeApproval;
									break;

								default:
									logActionType = ActionTypes.PostJob;
									break;
							}
							break;
					}

					if (logActionType != ActionTypes.NoAction)
					{
						jobList.ForEach(j => LogAction(request, logActionType, j, request.JobPostedBy));
					}

					#endregion
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Sets a job to waiting approval
		/// </summary>
		/// <param name="job">The job</param>
		/// <param name="request">The current service request</param>
		/// <param name="unregisterPosting">Whether to unregister the job from Lens</param>
		private void SetJobForApproval(Job job, ServiceRequest request, ref bool unregisterPosting)
		{
			job.ApprovalStatus = ApprovalStatuses.WaitingApproval;
			job.AwaitingApprovalOn = DateTime.Now;
			job.AwaitingApprovalActionedBy = request.UserContext.ActionerId;

			if (job.Posting.IsNotNull() && job.Posting.LensPostingId.IsNotNullOrEmpty())
			{
				job.JobStatus = JobStatuses.OnHold;
                //MIK FVN-2973 added log action to display onhold activity for waiting approval
                LogAction(request, ActionTypes.HoldJob, job);
				job.HeldOn = DateTime.Now;
				job.HeldBy = request.UserContext.UserId;
				unregisterPosting = true;
			}
			else
			{
				job.JobStatus = JobStatuses.Draft;
			}
		}

		/// <summary>
		/// Checks a job for censoredship.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public CheckCensorshipResponse CheckCensorship(CheckCensorshipRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new CheckCensorshipResponse(request) { CensoredWords = new List<string>() };

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var censorshipRules = GetCensorshipRules(request.Type);

					var fieldsToCheck = new List<string> { request.Job.PostingHtml ?? "" };

					if (request.Job.HideLicencesOnPosting)
					{
						var licencesArray = Repositories.Core.JobLicences.Where(r => r.JobId == request.Job.Id).Select(r => r.Licence);
						fieldsToCheck.AddRange(licencesArray);
					}

					if (request.Job.HideCertificationsOnPosting)
					{
						var certificatesArray = Repositories.Core.JobCertificates.Where(r => r.JobId == request.Job.Id).Select(r => r.Certificate);
						fieldsToCheck.AddRange(certificatesArray);
					}

					if (request.Job.HideLanguagesOnPosting)
					{
						var languagesArray = Repositories.Core.JobLanguages.Where(r => r.JobId == request.Job.Id).Select(r => r.Language);
						fieldsToCheck.AddRange(languagesArray);
					}

					if (request.Job.HideSpecialRequirementsOnPosting)
					{
						var requirementsArray = Repositories.Core.JobSpecialRequirements.Where(r => r.JobId == request.Job.Id).Select(r => r.Requirement);
						fieldsToCheck.AddRange(requirementsArray);
					}

					var censorshipCheck = new CensorshipCheck(censorshipRules, string.Join(", ", fieldsToCheck.Where(f => f.Length > 0).ToArray()), removeDuplicateSubsets: true);

					if (censorshipCheck.HasCensorships)
						response.CensoredWords.AddRange(censorshipCheck.FoundCensorshipRules.Select(x => x.Phrase));
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>

		/// Gets the job onet tasks.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public JobOnetTaskResponse GetJobOnetTasks(JobOnetTaskRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new JobOnetTaskResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var onetTaskKey = string.Format(Constants.CacheKeys.OnetTaskKey, request.UserContext.Culture, request.Criteria.OnetId);
					var onetTaskViews = Cacher.Get<List<OnetTaskView>>(onetTaskKey);

					if (onetTaskViews == null)
					{
						using (Profiler.Profile(request.LogData(), request))
						{
							var userCultureOnetTasks = (from ot in Repositories.Core.Query<Data.Library.Entities.OnetTaskView>()
								where ot.Culture == request.UserContext.Culture &&
								      ot.OnetId == request.Criteria.OnetId
								select new OnetTaskView
								{
									Id = ot.Id,
									OnetId = ot.OnetId,
									TaskKey = ot.TaskKey,
									Task = ot.Task
								}).ToList();

							var defaultCultureOnetTasks = (from ot in Repositories.Core.Query<Data.Library.Entities.OnetTaskView>()
								where ot.Culture == Constants.DefaultCulture &&
								      ot.OnetId == request.Criteria.OnetId
								select new OnetTaskView
								{
									Id = ot.Id,
									OnetId = ot.OnetId,
									TaskKey = ot.TaskKey,
									Task = ot.Task
								}).ToList();

							onetTaskViews = MergeOnetTasks(userCultureOnetTasks, defaultCultureOnetTasks);

							lock (SyncObject)
								Cacher.Set(onetTaskKey, onetTaskViews);
						}
					}

					response.OnetTasks = onetTaskViews.Take(request.Criteria.ListSize).ToList();
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Merges the onet tasks.
		/// </summary>
		/// <param name="cultureSpecificOnetTasks">The culture specific onet tasks.</param>
		/// <param name="defaultCultureOnetTasks">The default culture onet tasks.</param>
		/// <returns></returns>
		private static List<OnetTaskView> MergeOnetTasks(List<OnetTaskView> cultureSpecificOnetTasks, List<OnetTaskView> defaultCultureOnetTasks)
		{
			if (cultureSpecificOnetTasks.Count > 0)
			{
				var removeSet = (from csot in cultureSpecificOnetTasks
					join dcot in defaultCultureOnetTasks on csot.Id equals dcot.Id
					select dcot
					).ToList();

				removeSet.ForEach(x => defaultCultureOnetTasks.Remove(x));

				return cultureSpecificOnetTasks.Union(defaultCultureOnetTasks).ToList();
			}

			return defaultCultureOnetTasks;
		}

		private IEnumerable<JobLocationDto> GetJobLocations(long jobId)
		{
			var query = new JobLocationQuery(Repositories.Core, new JobLocationCriteria { JobId = jobId });

			return query.Query().Select(x => x.AsDto()).ToList();
		}

		/// <summary>
		/// Gets the job locations.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public JobLocationResponse GetJobLocations(JobLocationRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new JobLocationResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var query = new JobLocationQuery(Repositories.Core, request.Criteria);

					response.JobLocations = query.Query().Select(x => x.AsDto()).Take(request.Criteria.ListSize).ToList();
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Saves the job locations.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public SaveJobLocationsResponse SaveJobLocations(SaveJobLocationsRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new SaveJobLocationsResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var job = Repositories.Core.Jobs.FirstOrDefault(j => j.Id == request.Criteria.JobId.GetValueOrDefault(0));

					if (job.IsNotNull())
						response.Location = SaveJobLocations(job, request.JobLocations);

					response.JobLocations = request.JobLocations;
					LogAction(request, ActionTypes.SaveJobLocations, typeof(Job).Name, request.Criteria.JobId);
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Saves the locations for a job.
		/// </summary>
		/// <param name="job">The job to which the locations belong.</param>
		/// <param name="jobLocations">The job locations to save.</param>
		/// <param name="doSave">Whether to save the changes to the repository in this particular method</param>
		/// <param name="isNewJob">Whether it is a new job (not yet committed to DB)</param>
		private string SaveJobLocations(Job job, IEnumerable<JobLocationDto> jobLocations, bool doSave = true, bool isNewJob = false)
		{
			var existingLocations = Repositories.Core.JobLocations.Where(x => x.JobId == job.Id).ToList();

			var locationsAdded = false;
			JobLocation firstLocation = null;
			string location = null;

			if (jobLocations.IsNotNull())
			{
				var isFirst = true;
				foreach (var jobLocationDto in jobLocations)
				{
					if (isFirst)
					{
						location = jobLocationDto.Location.TruncateString(255);
						isFirst = false;
					}

					var existingLocation = existingLocations.FirstOrDefault(j => j.Zip == jobLocationDto.Zip);

					if (existingLocation.IsNull())
					{
						var jobLocation = new JobLocation();
						jobLocation = jobLocationDto.CopyTo(jobLocation);
						jobLocation.JobId = job.Id;
						jobLocation.NewLocation = true;
						Repositories.Core.Add(jobLocation);

						locationsAdded = true;
					}
					else
					{
						var newLocation = existingLocation.NewLocation;
						jobLocationDto.CopyTo(existingLocation);
						existingLocation.NewLocation = newLocation;
						existingLocations.Remove(existingLocation);

						if (firstLocation.IsNull())
						{
							firstLocation = existingLocation;
						}
					}
				}
			}

			if (!locationsAdded && existingLocations.Any() && firstLocation.IsNotNull())
			{
				firstLocation.NewLocation = true;
			}

			existingLocations.ForEach(jobLocation => Repositories.Core.Remove(jobLocation));

			job.Location = location;

			if (doSave)
				Repositories.Core.SaveChanges(true);

			return location;
		}

		/// <summary>
		/// Gets the job programs of study.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public JobProgramOfStudyResponse GetJobProgramsOfStudy(JobProgramOfStudyRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new JobProgramOfStudyResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var query = new JobProgramOfStudyQuery(Repositories.Core, request.Criteria);
					response.JobProgramsOfStudy = query.Query().Where(x => x.JobId == request.Criteria.JobId).Select(x => x.AsDto()).Take(request.Criteria.ListSize).ToList();
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Saves the job programs of study.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public SaveJobProgramsOfStudyResponse SaveJobProgramsOfStudy(SaveJobProgramsOfStudyRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new SaveJobProgramsOfStudyResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					SaveJobProgramsOfStudy(request.Criteria.JobId.GetValueOrDefault(0), request.JobProgramsOfStudy);

					response.JobProgramsOfStudy = request.JobProgramsOfStudy;
					LogAction(request, ActionTypes.SaveJobProgramsOfStudy, typeof(Job).Name, request.Criteria.JobId);
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Saves the programs of study for a job.
		/// </summary>
		/// <param name="jobId">The id of the job to which the programs of study belong.</param>
		/// <param name="jobProgramsOfStudy">The job programs of study to save.</param>
		/// <param name="doSave">Whether to save the changes to the repository in this particular method</param>
		/// <param name="isNewJob">Whether it is a new job (not yet committed to DB)</param>
		private void SaveJobProgramsOfStudy(long jobId, IEnumerable<JobProgramOfStudyDto> jobProgramsOfStudy, bool doSave = true, bool isNewJob = false)
		{
			if (!isNewJob)
				Repositories.Core.Remove<JobProgramOfStudy>(x => x.JobId == jobId);

			foreach (var jobProgramOfStudyDto in jobProgramsOfStudy)
			{
				var jobProgramOfStudy = new JobProgramOfStudy();
				jobProgramOfStudy = jobProgramOfStudyDto.CopyTo(jobProgramOfStudy);
				jobProgramOfStudy.JobId = jobId;
				Repositories.Core.Add(jobProgramOfStudy);
			}

			if (doSave)
				Repositories.Core.SaveChanges(true);
		}

		/// <summary>
		/// Gets the job description.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public JobDescriptionResponse GetJobDescription(JobDescriptionRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new JobDescriptionResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var query = new JobQuery(Repositories.Core, request.Criteria);

					response.JobDescription = query.QueryEntityId().AsDto().Description;
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the job certificates.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public JobCertificateResponse GetJobCertificates(JobCertificateRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new JobCertificateResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var query = new JobCertificateQuery(Repositories.Core, request.Criteria);

					response.JobCertificates = query.Query().Where(x => x.JobId == request.Criteria.JobId).Select(x => x.AsDto()).Take(request.Criteria.ListSize).ToList();
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Saves the job certificates.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public SaveJobCertificatesResponse SaveJobCertificates(SaveJobCertificatesRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new SaveJobCertificatesResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					SaveJobCertificates(request.Criteria.JobId.GetValueOrDefault(0), request.JobCertificates);

					response.JobCertificates = request.JobCertificates;
					LogAction(request, ActionTypes.SaveJobCertificates, typeof(Job).Name, request.Criteria.JobId);
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}
				return response;
			}
		}

		/// <summary>
		/// Saves the certificates for a job.
		/// </summary>
		/// <param name="jobId">The id of the job to which the certificates belong.</param>
		/// <param name="jobCertificates">The job certificates to save.</param>
		/// <param name="doSave">Whether to save the changes to the repository in this particular method</param>
		/// <param name="isNewJob">Whether it is a new job (not yet committed to DB)</param>
		private void SaveJobCertificates(long jobId, IEnumerable<JobCertificateDto> jobCertificates, bool doSave = true, bool isNewJob = false)
		{
			if (!isNewJob)
				Repositories.Core.Remove<JobCertificate>(x => x.JobId == jobId);

			foreach (var jobCertificateDto in jobCertificates)
			{
				var jobCertificate = new JobCertificate();
				jobCertificate = jobCertificateDto.CopyTo(jobCertificate);
				jobCertificate.JobId = jobId;
				Repositories.Core.Add(jobCertificate);
			}

			if (doSave)
				Repositories.Core.SaveChanges(true);
		}

		/// <summary>
		/// Gets the job languages.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public JobLanguageResponse GetJobLanguages(JobLanguageRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new JobLanguageResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var query = new JobLanguageQuery(Repositories.Core, request.Criteria);
					response.JobLanguages = query.Query().Where(x => x.JobId == request.Criteria.JobId).Select(x => x.AsDto()).Take(request.Criteria.ListSize).ToList();
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Saves the job languages.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public SaveJobLanguagesResponse SaveJobLanguages(SaveJobLanguagesRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new SaveJobLanguagesResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					SaveJobLanguages(request.Criteria.JobId.GetValueOrDefault(0), request.JobLanguages);

					response.JobLanguages = request.JobLanguages;
					LogAction(request, ActionTypes.SaveJobLanguages, typeof(Job).Name, request.Criteria.JobId);
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}
				return response;
			}
		}

		/// <summary>
		/// Saves the languages for a job.
		/// </summary>
		/// <param name="jobId">The id of the job to which the languages belong.</param>
		/// <param name="jobLanguages">The job languages to save.</param>
		/// <param name="doSave">Whether to save the changes to the repository in this particular method</param>
		/// <param name="isNewJob">Whether it is a new job (not yet committed to DB)</param>
		private void SaveJobLanguages(long jobId, IEnumerable<JobLanguageDto> jobLanguages, bool doSave = true, bool isNewJob = false)
		{
			if (!isNewJob)
				Repositories.Core.Remove<JobLanguage>(x => x.JobId == jobId);

			foreach (var jobLanguageDto in jobLanguages)
			{
				var jobLanguage = new JobLanguage();
				jobLanguage = jobLanguageDto.CopyTo(jobLanguage);
				jobLanguage.JobId = jobId;
				Repositories.Core.Add(jobLanguage);
			}

			if (doSave)
				Repositories.Core.SaveChanges(true);
		}

		/// <summary>
		/// Gets the job licences.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public JobLicenceResponse GetJobLicences(JobLicenceRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new JobLicenceResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var query = new JobLicenceQuery(Repositories.Core, request.Criteria);

					response.JobLicences = query.Query().Where(x => x.JobId == request.Criteria.JobId).Select(x => x.AsDto()).Take(request.Criteria.ListSize).ToList();
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Saves the job licences.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public SaveJobLicencesResponse SaveJobLicences(SaveJobLicencesRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new SaveJobLicencesResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					SaveJobLicences(request.Criteria.JobId.GetValueOrDefault(0), request.JobLicences);

					response.JobLicences = request.JobLicences;
					LogAction(request, ActionTypes.SaveJobLicences, typeof(Job).Name, request.Criteria.JobId);
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}
				return response;
			}
		}

		/// <summary>
		/// Saves the licences for a job.
		/// </summary>
		/// <param name="jobId">The id of the job to which the licences belong.</param>
		/// <param name="jobLicences">The job licences to save.</param>
		/// <param name="doSave">Whether to save the changes to the repository in this particular method</param>
		/// <param name="isNewJob">Whether it is a new job (not yet committed to DB)</param>
		private void SaveJobLicences(long jobId, IEnumerable<JobLicenceDto> jobLicences, bool doSave = true, bool isNewJob = false)
		{
			if (!isNewJob)
				Repositories.Core.Remove<JobLicence>(x => x.JobId == jobId);

			foreach (var jobLicenceDto in jobLicences)
			{
				var jobLicence = new JobLicence();
				jobLicence = jobLicenceDto.CopyTo(jobLicence);
				jobLicence.JobId = jobId;
				Repositories.Core.Add(jobLicence);
			}

			if (doSave)
				Repositories.Core.SaveChanges(true);
		}

		/// <summary>
		/// Gets the job special requirements.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public JobSpecialRequirementResponse GetJobSpecialRequirements(JobSpecialRequirementRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new JobSpecialRequirementResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var query = new JobSpecialRequirementQuery(Repositories.Core, request.Criteria);

					if (request.Criteria.FetchOption == CriteriaBase.FetchOptions.Count)
					{
						response.JobRequirementsCount = query.Query().Count(x => x.JobId == request.Criteria.JobId);
					}
					else
					{
						response.JobSpecialRequirements =
							query.Query().Where(x => x.JobId == request.Criteria.JobId).Select(x => x.AsDto()).Take(request.Criteria.ListSize)
								.ToList();
					}
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Saves the job special requirements.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public SaveJobSpecialRequirementsResponse SaveJobSpecialRequirements(SaveJobSpecialRequirementsRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new SaveJobSpecialRequirementsResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					SaveJobSpecialRequirements(request.Criteria.JobId.GetValueOrDefault(0), request.JobSpecialRequirements);

					response.JobSpecialRequirements = request.JobSpecialRequirements;
					LogAction(request, ActionTypes.SaveJobSpecialRequirements, typeof(Job).Name, request.Criteria.JobId);
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}
				return response;
			}
		}

		/// <summary>
		/// Saves the special requirements for a job.
		/// </summary>
		/// <param name="jobId">The id of the job to which the special requirements belong.</param>
		/// <param name="jobSpecialRequirements">The job special requirements to save.</param>
		/// <param name="doSave">Whether to save the changes to the repository in this particular method</param>
		/// <param name="isNewJob">Whether it is a new job (not yet committed to DB)</param>
		private void SaveJobSpecialRequirements(long jobId, List<JobSpecialRequirementDto> jobSpecialRequirements, bool doSave = true, bool isNewJob = false)
		{
			var currentSpecialRequirements = isNewJob
				? new List<JobSpecialRequirement>()
				: Repositories.Core.JobSpecialRequirements.Where(x => x.JobId == jobId).ToList();

			var currentRequirements = currentSpecialRequirements.Select(x => x.Requirement).ToList();
			var newRequirements = jobSpecialRequirements.Select(x => x.Requirement).ToList();

			var isNew = newRequirements.Except(currentRequirements).ToList().Any();

			if (!isNew)
				isNew = currentRequirements.Except(newRequirements).ToList().Any();

			currentSpecialRequirements.ForEach(x => Repositories.Core.Remove(x));

			foreach (var jobRequirementDto in jobSpecialRequirements)
			{
				jobRequirementDto.IsNew = isNew;

				var jobSpecialRequirement = new JobSpecialRequirement();
				jobSpecialRequirement = jobRequirementDto.CopyTo(jobSpecialRequirement);
				jobSpecialRequirement.JobId = jobId;
				Repositories.Core.Add(jobSpecialRequirement);
			}

			if (doSave)
				Repositories.Core.SaveChanges(true);
		}

		/// <summary>
		/// Gets the job driving licence endorsements.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public JobDrivingLicenceEndorsementResponse GetJobDrivingLicenceEndorsements(JobDrivingLicenceEndorsementRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new JobDrivingLicenceEndorsementResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var query = new JobDrivingLicenceEndorsementQuery(Repositories.Core, request.Criteria);

					response.JobDrivingLicenceEndorsements = query.Query().Where(x => x.JobId == request.Criteria.JobId).Select(x => x.AsDto()).Take(request.Criteria.ListSize).ToList();
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Saves the job driving licence endorsements.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public SaveJobDrivingLicenceEndorsementsResponse SaveJobDrivingLicenceEndorsements(SaveJobDrivingLicenceEndorsementsRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new SaveJobDrivingLicenceEndorsementsResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					SaveJobDrivingLicenceEndorsements(request.Criteria.JobId.GetValueOrDefault(0), request.JobDrivingLicenceEndorsements);

					response.JobDrivingLicenceEndorsements = request.JobDrivingLicenceEndorsements;
					LogAction(request, ActionTypes.SaveJobDrivingLicenceEndorsements, typeof(Job).Name, request.Criteria.JobId);
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}
				return response;
			}
		}

		/// <summary>
		/// Saves the job driving licence endorsements.
		/// </summary>
		/// <param name="jobId">The job identifier.</param>
		/// <param name="jobDrivingLicenceEndorsements">The job driving licence endorsements.</param>
		/// <param name="doSave">if set to <c>true</c> [do save].</param>
		/// <param name="isNewJob">if set to <c>true</c> [is new job].</param>
		private void SaveJobDrivingLicenceEndorsements(long jobId, List<JobDrivingLicenceEndorsementDto> jobDrivingLicenceEndorsements, bool doSave = true, bool isNewJob = false)
		{
			if (!isNewJob)
				Repositories.Core.Remove<JobDrivingLicenceEndorsement>(x => x.JobId == jobId);

			foreach (var jobDrivingLicenceEndorsementDto in jobDrivingLicenceEndorsements)
			{
				var jobDrivingLicenceEndorsement = new JobDrivingLicenceEndorsement();
				jobDrivingLicenceEndorsement = jobDrivingLicenceEndorsementDto.CopyTo(jobDrivingLicenceEndorsement);
				jobDrivingLicenceEndorsement.JobId = jobId;
				Repositories.Core.Add(jobDrivingLicenceEndorsement);
			}

			if (doSave)
				Repositories.Core.SaveChanges(true);
		}

		/// <summary>
		/// Gets the job tasks.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public JobTaskResponse GetJobTasks(JobTaskRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new JobTaskResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{

					var jobTaskBaseQuery = Repositories.Library.Query<JobTaskView>().Where(x => x.OnetId == request.Criteria.OnetId);
					if (request.Criteria.Scope.HasValue)
						jobTaskBaseQuery = jobTaskBaseQuery.Where(x => x.Scope == request.Criteria.Scope.Value);

					var jobTaskQuery = jobTaskBaseQuery.AsQueryable();

					var culture = (jobTaskQuery.Any(x => x.Culture == request.UserContext.Culture))
						? request.UserContext.Culture
						: Constants.DefaultCulture;

					var jobTaskViews = (from jt in jobTaskQuery.Where(x => x.Culture == culture)
						select new JobTaskDetailsView { JobTaskId = jt.Id, Prompt = jt.Prompt, Response = jt.Response, JobTaskType = jt.JobTaskType }).ToList();

					if (jobTaskViews.Count > 0)
					{
						var jobTaskMultiOptions = Repositories.Library.Query<JobTaskMultiOptionView>().Where(x => x.OnetId == request.Criteria.OnetId && x.Culture == culture).ToList();

						foreach (var jobTaskView in jobTaskViews)
						{
							var multiOptionPrompts =
								jobTaskMultiOptions.Where(x => x.JobTaskId == jobTaskView.JobTaskId).Select(x => x.Prompt).ToList();

							if (multiOptionPrompts.Count > 0)
								jobTaskView.MultiOptionPrompts.AddRange(multiOptionPrompts);
						}
					}

					response.JobTasks = jobTaskViews;
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the job posting referral view.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public JobPostingReferralViewResponse GetJobPostingReferralView(JobPostingReferralViewRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new JobPostingReferralViewResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					switch (request.Criteria.FetchOption)
					{
						case CriteriaBase.FetchOptions.PagedList:

							var query = new JobPostingReferralViewQuery(Repositories.Core, request.Criteria);

							response.ReferralViewsPaged = query.Query().GetPagedList(x => x.AsDto(), request.Criteria.PageIndex, request.Criteria.PageSize);
							break;

						case CriteriaBase.FetchOptions.Single:
							var referral = Repositories.Core.FindById<JobPostingReferralView>(request.Criteria.Id);
							if (referral.IsNotNull())
								response.Referral = referral.AsDto();
							break;
					}
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Approves the referral.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public JobResponse ApproveReferral(JobRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new JobResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var job = Repositories.Core.FindById<Job>(request.Criteria.JobId);

					if (job.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.JobNotFound));
						Logger.Info(request.LogData(), string.Format("The JobId '{0}' was not found.", request.Criteria.JobId));

						return response;
					}

					if (request.LockVersion.HasValue && job.LockVersion != request.LockVersion.Value)
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.LockVersionOutOfDate));
						return response;
					}

					var jobStatus = JobStatuses.Active;

					if (job.EmployeeId.HasValue)
					{
						var employee = Repositories.Core.FindById<Employee>(job.EmployeeId);
						if (employee.ApprovalStatus == ApprovalStatuses.Rejected)
						{
							response.ResponseType = ErrorTypes.EmployeeIsDenied;
							return response;
						}
						if (employee.ApprovalStatus == ApprovalStatuses.OnHold)
						{
							response.ResponseType = ErrorTypes.EmployeeOnHold;
							return response;
						}

						if (employee.ApprovalStatus == ApprovalStatuses.WaitingApproval)
							jobStatus = JobStatuses.AwaitingEmployerApproval;
					}

					job.JobStatus = jobStatus;
					job.ApprovalStatus = ApprovalStatuses.Approved;
					job.ApprovedOn = DateTime.Now;
					job.ApprovedBy = request.UserContext.UserId;

					#region Remove Previous edit data

					if (AppSettings.JobProofingEnabled)
					{
						var prevJobData = Repositories.Core.Find<JobPrevData>(jpd => jpd.JobId == job.Id).FirstOrDefault();
						if (prevJobData.IsNotNull())
						{
							Repositories.Core.Remove(prevJobData);
						}
					}

					#endregion

					Repositories.Core.SaveChanges(true);

					response.ResponseType = ErrorTypes.Ok;

					LogAction(request, ActionTypes.ApprovePostingReferral, typeof(Job).Name, (request.Criteria.JobId));

					// Now post the job
					var postJobRequest = new PostJobRequest
					{
						ClientTag = request.ClientTag,
						RequestId = request.RequestId,
						SessionId = request.SessionId,
						UserContext = request.UserContext,
						Job = job.AsDto(),
						Module = request.Module,
						InitialApprovalStatus = job.ApprovalStatus
					};

					var postJobResponse = PostJob(postJobRequest);

					if (postJobResponse.Acknowledgement == AcknowledgementType.Failure)
						response.SetFailure(postJobResponse.Message);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Denies the referral.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public JobResponse DenyReferral(JobRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new JobResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var job = Repositories.Core.FindById<Job>(request.Criteria.JobId);

					if (job.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.JobNotFound));
						Logger.Info(request.LogData(), string.Format("The JobId '{0}' was not found.", request.Criteria.JobId));

						return response;
					}

					if (request.LockVersion.HasValue && job.LockVersion != request.LockVersion.Value)
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.LockVersionOutOfDate));
						return response;
					}

					job.ApprovalStatus = ApprovalStatuses.Rejected;

					if (job.JobStatus != JobStatuses.OnHold)
						job.JobStatus = JobStatuses.Draft;

					job.ClosedOn = DateTime.Now;
					job.ClosedBy = request.UserContext.UserId;

					var errorType = ValidateJobDenialReasons(request.ReferralStatusReasons);

					if (errorType != ErrorTypes.Ok)
					{
						response.SetFailure(FormatErrorMessage(request, errorType));
						Logger.Error(request.LogData(), response.Message);
						return response;
					}

					if (AppSettings.Theme == FocusThemes.Workforce && request.ReferralStatusReasons.IsNotNullOrEmpty() && request.ReferralStatusReasons.Count > 0)
					{
						request.ReferralStatusReasons.ForEach(x =>
						{
							var reason = new JobReferralStatusReason
							{
								ReasonId = x.Key,
								OtherReasonText = x.Value,
								JobId = job.Id,
								EntityType = EntityTypes.Job,
								ApprovalStatus = ApprovalStatuses.Rejected
							};
							Repositories.Core.Add(reason);
						});
					}

					Repositories.Core.SaveChanges(true);

					LogAction(request, ActionTypes.DenyPostingReferral, typeof(Job).Name, (request.Criteria.JobId));
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Edits the posting and sends email to employer.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public JobResponse EditPostingReferral(JobRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new JobResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var job = Repositories.Core.FindById<Job>(request.Criteria.JobId);

					if (job.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.JobNotFound));
						Logger.Info(request.LogData(), string.Format("The JobId '{0}' was not found.", request.Criteria.JobId));

						return response;
					}

					job.ApprovalStatus = ApprovalStatuses.Approved;
					job.PostingHtml = request.NewPosting;
					job.ApprovedOn = DateTime.Now;
					job.ApprovedBy = request.UserContext.UserId;

					#region Remove Previous edit data

					if (AppSettings.JobProofingEnabled)
					{
						var prevJobData = Repositories.Core.Find<JobPrevData>(jpd => jpd.JobId == job.Id).FirstOrDefault();
						if (prevJobData.IsNotNull())
						{
							Repositories.Core.Remove(prevJobData);
						}
					}

					#endregion

					Repositories.Core.SaveChanges(true);

					LogAction(request, ActionTypes.EditPostingReferral, typeof(Job).Name, (request.Criteria.JobId));
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the job lookup.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public JobLookupResponse GetJobLookup(JobLookupRequest request)
		{
			var response = new JobLookupResponse(request);

			// Validate request
			if (!ValidateRequest(request, response, Validate.All))
				return response;

			try
			{
				var query = new JobLookupViewQuery(Repositories.Core, request.Criteria);
				var queryResult = query.Query().OrderBy("JobTitle, Employer");

				switch (request.Criteria.FetchOption)
				{
					case CriteriaBase.FetchOptions.Single:
						response.JobLookup = queryResult.SingleOrDefault();
						break;

					case CriteriaBase.FetchOptions.List:
						response.JobLookups = queryResult.ToList();
						break;

					case CriteriaBase.FetchOptions.PagedList:
						response.JobLookupsPaged = new PagedList<JobLookupView>(queryResult, request.Criteria.PageIndex, request.Criteria.PageSize);
						break;
				}
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}

			return response;
		}

		/// <summary>
		/// Gets the job URL.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public JobLensPostingIdResponse GetJobLensPostingId(JobLensPostingIdRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new JobLensPostingIdResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var lensPostingId = Repositories.Core.Postings.Where(x => x.JobId == request.JobId).Select(x => x.LensPostingId).FirstOrDefault();
					response.LensPostingId = lensPostingId;
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.JobNotFound), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the job URL.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public JobIdsResponse GetJobLensPostingId(JobIdsRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new JobIdsResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					long keyId;
					long.TryParse(request.CheckId, out keyId);
	
					var query = Repositories.Core.JobIdsViews;

					// Where the id is numeric check both external id and primary key field
					query = keyId != 0
										? query.Where(x => (x.Id == keyId || x.ExternalId == request.CheckId) && request.JobStatuses.Contains(x.JobStatus))
										: query.Where(x => x.ExternalId == request.CheckId && request.JobStatuses.Contains(x.JobStatus));

					if (request.CheckVeteranPriorityDate)
					{
						query = query.Where(j => j.VeteranPriorityEndDate == null || j.VeteranPriorityEndDate <= DateTime.Now);
					}

					var jobList = query.ToList();

					// If the requested Id is numeric, first check for an internal Id
					var job = jobList.FirstOrDefault(j => j.Id == keyId);
					if (job.IsNull())
					{
						// If no match found for Internal Id, just pick the first match for the external Id (or null)
						job = jobList.FirstOrDefault();
					}

					if (job.IsNotNull())
					{
						response.LensPostingId = job.LensPostingId;
					}
					else if (keyId > 0)
					{
		        // Attempt to get the LensPostingId directly from the posting table using the Posting ID if no job returned
						var details = Repositories.Core.Postings.Where(x => x.Id == keyId)
																										.Select(x => new
																										{
																											x.LensPostingId,
																											x.JobId
																										}
																										).FirstOrDefault();

						if (details.IsNotNull())
						{
							if (request.CheckVeteranPriorityDate && details.JobId.IsNotNullOrZero())
							{
								var veteranDate = Repositories.Core.Jobs.Where(j => j.Id == details.JobId).Select(j => j.VeteranPriorityEndDate).FirstOrDefault();
								if (veteranDate.IsNull() || veteranDate <= DateTime.Now)
								{
									response.LensPostingId = details.LensPostingId;
								}
							}
							else
							{
								response.LensPostingId = details.LensPostingId;		
							}
						}
					}
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.JobNotFound), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the job status.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public JobStatusResponse GetJobStatus(JobStatusRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new JobStatusResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var job = Repositories.Core.FindById<Job>(request.JobId);
					response.JobStatus = job.JobStatus;
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets dates for the job
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public JobDatesResponse GetJobDates(JobDatesRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new JobDatesResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var dates = Repositories.Core.Jobs
						.Where(job => job.Id == request.JobId)
						.Select(job => new JobDatesView
						{
							PostedOn = job.PostedOn,
							ClosingOn = job.ClosingOn,
							FederalContractor = job.FederalContractor
						})
						.FirstOrDefault();

					response.JobDates = dates;
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the job address.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public JobAddressResponse GetJobAddress(JobAddressRequest request)
		{
			var response = new JobAddressResponse(request);

			// Validate request
			if (!ValidateRequest(request, response, Validate.All))
				return response;

			try
			{
				response.JobAddress =
					Repositories.Core.JobAddresses.Where(x => x.JobId == request.JobId).Select(x => x.AsDto()).SingleOrDefault();
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}

			return response;
		}

		/// <summary>
		/// Saves the job address.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public SaveJobAddressResponse SaveJobAddress(SaveJobAddressRequest request)
		{
			var response = new SaveJobAddressResponse(request);

			// Validate request
			if (!ValidateRequest(request, response, Validate.All))
				return response;

			try
			{
				var jobAddress = SaveJobAddress(request.JobAddress.JobId, request.JobAddress);

				response.JobAddress = jobAddress.AsDto();

				LogAction(request, ActionTypes.SaveJobAddress, jobAddress);
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}

			return response;
		}

		/// <summary>
		/// Saves the address for a job.
		/// </summary>
		/// <param name="jobId">The id of the job to which the address belongs.</param>
		/// <param name="jobAddress">The job address to save.</param>
		/// <param name="doSave">Whether to save the changes to the repository in this particular method</param>
		/// <param name="isNewJob">Whether it is a new job (not yet committed to DB)</param>
		/// <returns>The saved address</returns>
		private JobAddress SaveJobAddress(long jobId, JobAddressDto jobAddress, bool doSave = true, bool isNewJob = false)
		{
			JobAddress savedJobAddress;

			if (jobAddress.Id.IsNotNull())
			{
				savedJobAddress = Repositories.Core.FindById<JobAddress>(jobAddress.Id);
				savedJobAddress = jobAddress.CopyTo(savedJobAddress);
			}
			else
			{
				// Delete addresses
				if (!isNewJob)
					Repositories.Core.Remove<JobAddress>(x => x.JobId == jobId);

				savedJobAddress = new JobAddress();
				savedJobAddress = jobAddress.CopyTo(savedJobAddress);
				savedJobAddress.JobId = jobId;
				Repositories.Core.Add(savedJobAddress);
			}

			if (doSave)
				Repositories.Core.SaveChanges(true);

			return savedJobAddress;
		}

		#region Education Internship Skills

		/// <summary>
		/// Saves the education internship skills.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public SaveJobEducationInternshipSkillsResponse SaveJobEducationInternshipSkills(SaveJobEducationInternshipSkillsRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new SaveJobEducationInternshipSkillsResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.ClientTag | Validate.AccessToken))
					return response;

				try
				{
					var currentInternshipSkills = Repositories.Core.Query<JobEducationInternshipSkill>().Where(x => x.JobId == request.JobId).ToList();

					currentInternshipSkills.ForEach(x => Repositories.Core.Remove(x));

					if (request.Skills.IsNotNull())
					{
						foreach (var internshipSkill in from internshipSkillDto in request.Skills
							let internshipSkill = new JobEducationInternshipSkill()
							select internshipSkillDto.CopyTo(internshipSkill))
						{
							internshipSkill.JobId = request.JobId;
							Repositories.Core.Add(internshipSkill);
						}
					}

					Repositories.Core.SaveChanges(true);
					LogAction(request, ActionTypes.SaveJobEducationInternshipSkills, typeof(JobEducationInternshipSkill).Name, request.JobId);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		#endregion

		/// <summary>
		/// Gets the application instructions.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public ApplicationInstructionsResponse GetApplicationInstructions(ApplicationInstructionsRequest request)
		{
			var response = new ApplicationInstructionsResponse(request);

			// Validate request
			if (!ValidateRequest(request, response, Validate.All))
				return response;

			try
			{
				if (!request.JobId.HasValue)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.JobNotFound));
					Logger.Info(request.LogData(), "Unable to get application instructions as no Job Id provided");
					return response;
				}

				var job = Repositories.Core.FindById<Job>(request.JobId);

				if (job.IsNull())
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.JobNotFound));
					Logger.Info(request.LogData(),
						string.Format("Unable to get application instructions as could not find Job {0}", job.Id));
					return response;
				}

				if (!job.InterviewContactPreferences.HasValue) job.InterviewContactPreferences = ContactMethods.None;

				var applicationInstructions = new StringBuilder("");

				if ((job.InterviewContactPreferences & ContactMethods.FocusTalent) == ContactMethods.FocusTalent)
				{
					applicationInstructions.Append(LocaliseOrDefault(request, "ApplicationInstructions.ApplyThroughFocusCareer.Text",
						"Log in to #FOCUSCAREER# and submit your resume {0}",
						AppSettings.CareerApplicationPath));
				}

				if ((job.InterviewContactPreferences & ContactMethods.Email) == ContactMethods.Email &&
				    job.InterviewEmailAddress.IsNotNullOrEmpty())
				{
					if (applicationInstructions.Length > 0) applicationInstructions.Append(Environment.NewLine);
					applicationInstructions.Append(LocaliseOrDefault(request, "ApplicationInstructions.EmailApplication.Text",
						"Email resume to {0}", job.InterviewEmailAddress));
				}

				if ((job.InterviewContactPreferences & ContactMethods.Online) == ContactMethods.Online &&
				    job.InterviewApplicationUrl.IsNotNullOrEmpty())
				{
					if (applicationInstructions.Length > 0) applicationInstructions.Append(Environment.NewLine);
					applicationInstructions.Append(LocaliseOrDefault(request, "ApplicationInstructions.ApplyOnline.Text",
						"Apply online {0}", job.InterviewApplicationUrl));
				}

				if ((job.InterviewContactPreferences & ContactMethods.Mail) == ContactMethods.Mail &&
				    job.InterviewMailAddress.IsNotNullOrEmpty())
				{
					if (applicationInstructions.Length > 0) applicationInstructions.Append(Environment.NewLine);
					applicationInstructions.Append(LocaliseOrDefault(request, "ApplicationInstructions.MailApplication.Text",
						"Mail resume to {0}", job.InterviewMailAddress));
				}

				if ((job.InterviewContactPreferences & ContactMethods.Fax) == ContactMethods.Fax &&
				    job.InterviewFaxNumber.IsNotNullOrEmpty())
				{
					if (applicationInstructions.Length > 0) applicationInstructions.Append(Environment.NewLine);
					applicationInstructions.Append(LocaliseOrDefault(request, "ApplicationInstructions.FaxApplication.Text",
						"Fax resume to {0}", Regex.Replace(job.InterviewFaxNumber, AppSettings.PhoneNumberStrictRegExPattern, AppSettings.PhoneNumberFormat)));
				}

				if ((job.InterviewContactPreferences & ContactMethods.Telephone) == ContactMethods.Telephone &&
				    job.InterviewPhoneNumber.IsNotNullOrEmpty())
				{
					if (applicationInstructions.Length > 0) applicationInstructions.Append(Environment.NewLine);
					applicationInstructions.Append(LocaliseOrDefault(request, "ApplicationInstructions.TelephoneApplication.Text",
						"Call for an appointment on {0}", Regex.Replace(job.InterviewPhoneNumber, AppSettings.PhoneNumberStrictRegExPattern, AppSettings.PhoneNumberFormat)));
				}

				if ((job.InterviewContactPreferences & ContactMethods.InPerson) == ContactMethods.InPerson &&
				    job.InterviewDirectApplicationDetails.IsNotNullOrEmpty())
				{
					if (applicationInstructions.Length > 0) applicationInstructions.Append(Environment.NewLine);
					applicationInstructions.Append(LocaliseOrDefault(request, "ApplicationInstructions.DirectApplication.Text",
						"Apply in person at {0}", job.InterviewDirectApplicationDetails));
				}

				if (job.InterviewOtherInstructions.IsNotNullOrEmpty())
				{
					if (applicationInstructions.Length > 0) applicationInstructions.Append(Environment.NewLine);
					applicationInstructions.Append(LocaliseOrDefault(request, "ApplicationInstructions.OtherInstructions.Text",
						"Other application instructions: {0}",
						job.InterviewOtherInstructions));
				}

				response.ApplicationInstructions = applicationInstructions.ToString();
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}

			return response;
		}

		/// <summary>
		/// Gets the job applicants.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public JobApplicantsResponse GetJobApplicants(JobApplicantsRequest request)
		{
			var response = new JobApplicantsResponse(request);

			// Validate request
			if (!ValidateRequest(request, response, Validate.All))
				return response;

			try
			{
				// We only want to return approvied applications
				response.Applicants =
					Repositories.Core.ApplicationViews.Where(
						x => x.JobId == request.JobId && x.CandidateApplicationApprovalStatus == ApprovalStatuses.Approved).Select(
							(x => x.AsDto())).ToList();
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}

			return response;
		}

		/// <summary>
		/// Generates the posting.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public GeneratePostingResponse GeneratePosting(GeneratePostingRequest request)
		{
			var response = new GeneratePostingResponse(request);

			// Validate request
			if (!ValidateRequest(request, response, Validate.All))
				return response;

			try
			{
				var businessUnit = (request.Job.BusinessUnitId.IsNotNull())
					? Repositories.Core.FindById<BusinessUnit>(request.Job.BusinessUnitId).AsDto()
					: null;
				var description = (request.Job.BusinessUnitDescriptionId.IsNotNull())
					? Repositories.Core.FindById<BusinessUnitDescription>(request.Job.BusinessUnitDescriptionId).AsDto()
					: null;

				var postingGeneratorModel = new PostingGeneratorModel
				{
					Job = request.Job,
					BusinessUnit = businessUnit,
					BusinessUnitDescription = description,
					JobLicences = request.JobLicences,
					JobCertificates = request.JobCertificates,
					JobLanguages = request.JobLanguages,
					JobSpecialRequirements = request.JobSpecialRequirements,
					JobLocations = request.JobLocations,
					JobProgramsOfStudy = request.JobProgramsOfStudy,
					JobDrivingLicenceEndorsements = request.JobDrivingLicenceEndorsements,
					Module = AppSettings.Module
				};

				response.Posting = new PostingGenerator(postingGeneratorModel, request.UserContext.Culture, AppSettings, Helpers.Localisation, Helpers.Lookup).Generate();
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}

			return response;
		}

		/// <summary>
		/// Gets the job activities.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public JobActivitiesResponse GetJobActivities(JobActivitiesRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new JobActivitiesResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					// Basic query
					var query =
						Repositories.Core.Query<JobActionEventView>().Where(x => x.JobId == request.Criteria.JobId);

					if (request.Criteria.DaysBack > 0)
						query = query.Where(x => x.ActionedOn >= DateTime.Now.Date.AddDays(request.Criteria.DaysBack * -1));

					if (request.Criteria.UserId.HasValue)
						query = query.Where(x => x.UserId == request.Criteria.UserId);
					if (request.Criteria.Action.HasValue)
						query = query.Where(x => x.ActionName == request.Criteria.Action.ToString());

					switch (request.Criteria.OrderBy)
					{
						case Constants.SortOrders.NameAsc:
							query = query.OrderBy(x => x.LastName);
							break;
						case Constants.SortOrders.NameDesc:
							query = query.OrderByDescending(x => x.LastName);
							break;
						case Constants.SortOrders.ActivityDateAsc:
							query = query.OrderBy(x => x.ActionedOn);
							break;
						default:
							query = query.OrderByDescending(x => x.ActionedOn);
							break;
					}

					var activities = query.Select(a => a.AsDto());

					switch (request.Criteria.FetchOption)
					{
						case CriteriaBase.FetchOptions.PagedList:
							response.JobActionEventViewPaged =
								new PagedList<JobActionEventViewDto>(activities, request.Criteria.PageIndex, request.Criteria.PageSize);
							break;

						case CriteriaBase.FetchOptions.List:
							response.JobActionEventsList = activities.ToList();
							break;
					}
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the job activity users.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public JobActivityUsersResponse GetJobActivityUsers(JobActivityUsersRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new JobActivityUsersResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;
				try
				{
					response.Actioners =
						Repositories.Core.Query<JobActionEventView>().Where(x => x.JobId == request.JobId).Select(
							x => new JobActionEventViewDto { UserId = x.UserId, LastName = x.LastName, FirstName = x.FirstName }).OrderBy(
								x => x.LastName).Distinct().ToList();
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}


				return response;
			}
		}

		/// <summary>
		/// Prints the job.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public PrintJobResponse PrintJob(PrintJobRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new PrintJobResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					// Create new document
					var document = new DocumentUtility();
					document.New(DocumentFileType.Pdf);
					document.Font("Calibri");

					#region Job Title

					// Draw job title then reset font
					document.FontSize(16);
					document.AppendText(request.JobDetails.JobTitle);
					document.AppendText(" - ");
					document.AppendText(request.JobView.EmployerName);
					document.FontSize(11);
					document.CarriageReturn(2);

					#endregion

					#region Applicant status

					// Display applicant count
					document.AppendBoldText(LocaliseOrDefault(request, "ApplicantStatus.Label", "Applicant status"));
					document.CarriageReturn();
					document.AppendText(LocaliseOrDefault(request, "ThereAre", "There are"));
					document.Space();
					document.AppendText(request.JobDetails.ApplicationCount.ToString());
					document.Space();
					document.AppendText(LocaliseOrDefault(request, "Applicants.Text", "applicants for this job."));
					document.CarriageReturn(2);

					#endregion

					#region Job details

					// Job details drawn in a table
					document.AppendBoldText(LocaliseOrDefault(request, "Description", "Description"));
					document.CarriageReturn();

					document.ClearTableBorders();
					var table = document.NewTable();

					document.AddCellAndBoldText(LocaliseOrDefault(request, "JobClosingDate.Label", "Job closing date") + ":");

					document.AddCellAndText(request.JobDetails.ClosingDate.HasValue
						? LocaliseOrDefault(request, "JobClosingDate.Format", "{0:MMM d, yyyy}", request.JobDetails.ClosingDate.Value)
						: LocaliseOrDefault(request, "JobClosingDate.NotDefined", "Not defined"));
					document.AddCellAndBoldText(LocaliseOrDefault(request, "PositionType.Label", "Position Type") + ":");
					document.AddCellAndText(request.JobDetails.PositionDetails.IsNotNullOrEmpty()
						? request.JobDetails.PositionDetails
						: LocaliseOrDefault(request, "PositionDetails.NotDefined", "Not defined"));

					document.NewRow();

					document.AddCellAndBoldText(LocaliseOrDefault(request, "JobReference.Label", "Job ID") + ":");
					document.AddCellAndText(request.JobId.ToString());
					document.AddCellAndBoldText(LocaliseOrDefault(request, "NumberOfOpenings.Label", "Number of openings") + ":");
					document.AddCellAndText(request.JobDetails.NumberOfOpenings.GetValueOrDefault().ToString());

					document.NewRow();
					document.AddCellAndBoldText(LocaliseOrDefault(request, "CareerJobReference.Label", "Focus Career Job ID") + ":");
					document.AddCellAndText(request.JobDetails.ExternalId ?? "");
					document.AddCellAndBoldText(LocaliseOrDefault(request, "JobStatus.Label", "Job Status") + ":");
					document.AddCellAndText(request.JobStatus);

					if (request.JobDetails.PostedBy.HasValue)
					{
						document.NewRow();
						document.AddCellAndBoldText(LocaliseOrDefault(request, "PostedBy.Label", "Posted by") + ":");
						document.AddCellAndText(request.EmployerFirstName + " " + request.EmployerLastName);

						document.NewRow();
						document.NewCell();
						document.AddCellAndText(request.Email);

						document.NewRow();
						document.NewCell();
						document.AddCellAndText(request.PhoneNumber);

					}

					document.EndTable();
					table.AutoFit(AutoFitBehavior.AutoFitToContents);

					document.CarriageReturn(2);
					document.AppendText(request.JobDetails.JobDescription);
					document.CarriageReturn(3);

					#endregion

					#region Job Activity

					// Assistance provided drawn in a table
					document.AppendBoldText(LocaliseOrDefault(request, "JobOrderActivityLog", "#EMPLOYMENTTYPE# activity log"));
					document.CarriageReturn();

					if (request.ActionEvents == null || request.ActionEvents.Count == 0)
					{
						document.AppendText(LocaliseOrDefault(request, "JobView.AssistanceProvided.NoResults", "No activity recorded for this job."));
					}
					else
					{
						table = document.NewTable();
						document.AddCellAndBoldText(LocaliseOrDefault(request, "Job.AssistanceProvided.StaffNameHeader", "STAFF NAME"));

						document.AddCellAndBoldText(LocaliseOrDefault(request, "Job.AssistanceProvided.DateHeader", "DATE"));

						document.AddCellAndBoldText(LocaliseOrDefault(request, "Job.AssistanceProvided.ServiceHeader", "ACTIVITY/SERVICE"));

						foreach (var activity in request.ActionEvents)
						{
							document.NewRow();
							document.AddCellAndText(activity.LastName + ", " + activity.FirstName);
							document.AddCellAndText(LocaliseOrDefault(request, "Activity.Format", "{0:M/d/yyyy HH:mm}", activity.ActionedOn));
							ActionTypes actionType;
							if (Enum.TryParse(activity.ActionName, out actionType))
							{
								switch (actionType)
								{
									case ActionTypes.ApproveCandidateReferral:
										document.AddCellAndText(LocaliseOrDefault(request, actionType + ".ActionDescription", "Referral Approved"));
										break;
									case ActionTypes.DenyCandidateReferral:
										document.AddCellAndText(LocaliseOrDefault(request, actionType + ".ActionDescription", "Referral denied"));
										break;
									case ActionTypes.CreateCandidateApplication:
										document.AddCellAndText(String.Format("{0} ({1})", LocaliseOrDefault(request, actionType + ".ActionDescription", "Staff referral"), activity.AdditionalDetails));
										break;
									case ActionTypes.UpdateApplicationStatusToSelfReferred:
										document.AddCellAndText(LocaliseOrDefault(request, actionType + ".ActionDescription", "Self referral submitted"));
										break;
									case ActionTypes.CreateJob:
										document.AddCellAndText(LocaliseOrDefault(request, actionType + ".ActionDescription", "#EMPLOYMENTTYPE# created"));
										break;
									case ActionTypes.PostJob:
										document.AddCellAndText(LocaliseOrDefault(request, actionType + ".ActionDescription", "#EMPLOYMENTTYPE# posted"));
										break;
									case ActionTypes.SaveJob:
										document.AddCellAndText(LocaliseOrDefault(request, actionType + ".ActionDescription", "#EMPLOYMENTTYPE# edited"));
										break;
									case ActionTypes.RefreshJob:
										document.AddCellAndText(LocaliseOrDefault(request, actionType + ".ActionDescription", "#EMPLOYMENTTYPE# refreshed"));
										break;
									case ActionTypes.ReactivateJob:
										document.AddCellAndText(LocaliseOrDefault(request, actionType + ".ActionDescription", "#EMPLOYMENTTYPE# reactivated"));
										break;
									case ActionTypes.ApprovePostingReferral:
										document.AddCellAndText(LocaliseOrDefault(request, actionType + ".ActionDescription", "#EMPLOYMENTTYPE# approved"));
										break;
									case ActionTypes.HoldJob:
										document.AddCellAndText(LocaliseOrDefault(request, actionType + ".ActionDescription", "#EMPLOYMENTTYPE# on hold"));
										break;
									case ActionTypes.CloseJob:
										document.AddCellAndText(LocaliseOrDefault(request, actionType + ".ActionDescription", "#EMPLOYMENTTYPE# closed"));
										break;
									case ActionTypes.UpdateApplicationStatusToDidNotApply:
										document.AddCellAndText(LocaliseOrDefault(request, actionType + ".ActionDescription", "#CANDIDATETYPE# did not apply"));
										break;
									case ActionTypes.UpdateApplicationStatusToFailedToShow:
										document.AddCellAndText(LocaliseOrDefault(request, actionType + ".ActionDescription", "#CANDIDATETYPE# failed to show"));
										break;
									case ActionTypes.UpdateApplicationStatusToUnderConsideration:
										document.AddCellAndText(LocaliseOrDefault(request, actionType + ".ActionDescription", "#CANDIDATETYPE# under consideration"));
										break;
									case ActionTypes.UpdateApplicationStatusToRecommended:
										document.AddCellAndText(LocaliseOrDefault(request, actionType + ".ActionDescription", "#CANDIDATETYPE# recommended"));
										break;
									case ActionTypes.UpdateApplicationStatusToHired:
										document.AddCellAndText(LocaliseOrDefault(request, actionType + ".ActionDescription", "#CANDIDATETYPE# hired"));
										break;
									case ActionTypes.UpdateApplicationStatusToInterviewScheduled:
										document.AddCellAndText(LocaliseOrDefault(request, actionType + ".ActionDescription", "#CANDIDATETYPE# interview scheduled"));
										break;
									case ActionTypes.UpdateApplicationStatusToInterviewed:
										document.AddCellAndText(LocaliseOrDefault(request, actionType + ".ActionDescription", "#CANDIDATETYPE# interviewed"));
										break;
									case ActionTypes.UpdateApplicationStatusToOfferMade:
										document.AddCellAndText(LocaliseOrDefault(request, actionType + ".ActionDescription", "#CANDIDATETYPE# offer made"));
										break;
									case ActionTypes.UpdateApplicationStatusToNotHired:
										document.AddCellAndText(LocaliseOrDefault(request, actionType + ".ActionDescription", "#CANDIDATETYPE# not hired"));
										break;
									case ActionTypes.UpdateApplicationStatusToInterviewDenied:
										document.AddCellAndText(LocaliseOrDefault(request, actionType + ".ActionDescription", "#CANDIDATETYPE# interview denied"));
										break;
									case ActionTypes.UpdateApplicationStatusToRefusedOffer:
										document.AddCellAndText(LocaliseOrDefault(request, actionType + ".ActionDescription", "#CANDIDATETYPE# refused job"));
										break;
								}
							}
							else
							{
								document.AddCellAndText("-");
							}
						}
						document.EndTable();
						table.AutoFit(AutoFitBehavior.AutoFitToContents);
						document.CarriageReturn(2);
					}

					#endregion

					#region Referral Summary

					// Referral Summary drawn in a table
					document.AppendBoldText(LocaliseOrDefault(request, "ReferralSummary", "Referral Summary"));
					document.CarriageReturn();

					if (request.Referrals == null || request.Referrals.Count == 0)
					{
						document.AppendText(LocaliseOrDefault(request, "JobView.ReferralSummary.NoResults", "No referalls for this job."));
					}
					else
					{
						table = document.NewTable();

						var cell = document.AddCellAndBoldText(LocaliseOrDefault(request, "Job.ReferralSummary.Date", "Date"));
						cell.CellFormat.PreferredWidth = PreferredWidth.FromPoints(60);
						cell = document.AddCellAndBoldText(LocaliseOrDefault(request, "Job.ReferralSummary.Rating", "Rating"));
						cell.CellFormat.PreferredWidth = PreferredWidth.Auto;
						cell = document.AddCellAndBoldText(LocaliseOrDefault(request, "Job.ReferralSummary.Name", "Name"));
						cell.CellFormat.PreferredWidth = PreferredWidth.FromPercent(20);
						cell = document.AddCellAndBoldText(LocaliseOrDefault(request, "Job.ReferralSummary.RecentEmployment", "Recent Employment"));
						cell.CellFormat.PreferredWidth = PreferredWidth.FromPercent(30);
						cell = document.AddCellAndBoldText(LocaliseOrDefault(request, "Job.ReferralSummary.YearsOfExperience", "Years of experience"));
						cell.CellFormat.PreferredWidth = PreferredWidth.FromPoints(45);
						cell = document.AddCellAndBoldText(LocaliseOrDefault(request, "Job.ReferralSummary.ReferralOutcome", "Referral outcome"));
						cell.CellFormat.PreferredWidth = PreferredWidth.FromPoints(70);

						table.AllowAutoFit = true;

						Image image = null;

						foreach (var referral in request.Referrals)
						{
							document.NewRow();
							cell = document.AddCellAndText(LocaliseOrDefault(request, "Referral.Format", "{0:M/d/yyyy}", referral.ActionedOn));
							cell.CellFormat.PreferredWidth = PreferredWidth.Auto;

							var starRating = referral.CandidateApplicationScore.ToStarRating(AppSettings.StarRatingMinimumScores);

							switch (starRating)
							{
								case 0:
									image = Resources.stars0;
									break;
								case 1:
									image = Resources.stars1;
									break;
								case 2:
									image = Resources.stars2;
									break;
								case 3:
									image = Resources.stars3;
									break;
								case 4:
									image = Resources.stars4;
									break;
								case 5:
									image = Resources.stars5;
									break;
							}
							document.AddCellAndImage(image);
							//cell.CellFormat.PreferredWidth = PreferredWidth.Auto;

							document.AddCellAndText(referral.CandidateLastName + ", " + referral.CandidateFirstName);

							var unknownText = LocaliseOrDefault(request, "Global.Unknown.Text", "Unknown");
							var employmentHistory = string.Empty;

							if (referral.LatestJobTitle.IsNotNull() || referral.LatestJobEmployer.IsNotNull() ||
							    referral.LatestJobStartYear.IsNotNull() || referral.LatestJobEndYear.IsNotNull())
							{
								employmentHistory = String.Format("{0} - {1} ({2}-{3})",
									referral.LatestJobTitle.IsNotNullOrEmpty()
										? referral.LatestJobTitle
										: unknownText,
									referral.LatestJobEmployer.IsNotNullOrEmpty()
										? referral.LatestJobEmployer
										: unknownText,
									referral.LatestJobStartYear.IsNotNullOrEmpty()
										? referral.LatestJobStartYear
										: unknownText,
									referral.LatestJobEndYear.IsNotNullOrEmpty()
										? referral.LatestJobEndYear
										: unknownText);
							}

							document.AddCellAndText(employmentHistory);
							cell = document.AddCellAndText(referral.CandidateYearsExperience.ToString());
							cell.Paragraphs[0].ParagraphFormat.Alignment = ParagraphAlignment.Center;

							var referralId = referral.CandidateApplicationStatus;
							ApplicationStatusTypes referralType;
							Enum.TryParse(referralId.ToString(), true, out referralType);

							switch (referralType)
							{
								case ApplicationStatusTypes.NotApplicable:
									cell =
										document.AddCellAndText(LocaliseOrDefault(request, referralType + ".ActionDescription", "Not applicable"));
									break;
								case ApplicationStatusTypes.SelfReferred:
									cell = document.AddCellAndText(LocaliseOrDefault(request, referralType + ".ActionDescription", "Self referred"));
									break;
								case ApplicationStatusTypes.Recommended:
									cell = document.AddCellAndText(LocaliseOrDefault(request, referralType + ".ActionDescription", "Recommended"));
									break;
								case ApplicationStatusTypes.NewApplicant:
									cell = document.AddCellAndText(LocaliseOrDefault(request, referralType + ".ActionDescription", "New applicant"));
									break;
								case ApplicationStatusTypes.UnderConsideration:
									cell =
										document.AddCellAndText(LocaliseOrDefault(request, referralType + ".ActionDescription", "Under consideration"));
									break;
								case ApplicationStatusTypes.DidNotApply:
									cell = document.AddCellAndText(LocaliseOrDefault(request, referralType + ".ActionDescription", "Did not apply"));
									break;
								case ApplicationStatusTypes.InterviewScheduled:
									cell =
										document.AddCellAndText(LocaliseOrDefault(request, referralType + ".ActionDescription", "Interview Scheduled"));
									break;
								case ApplicationStatusTypes.Interviewed:
									cell = document.AddCellAndText(LocaliseOrDefault(request, referralType + ".ActionDescription", "Interviewed"));
									break;
								case ApplicationStatusTypes.InterviewDenied:
									cell =
										document.AddCellAndText(LocaliseOrDefault(request, referralType + ".ActionDescription", "Interview denied"));
									break;
								case ApplicationStatusTypes.FailedToShow:
									cell =
										document.AddCellAndText(LocaliseOrDefault(request, referralType + ".ActionDescription", "Failed to show"));
									break;
								case ApplicationStatusTypes.OfferMade:
									cell = document.AddCellAndText(LocaliseOrDefault(request, referralType + ".ActionDescription", "Offer made"));
									break;
								case ApplicationStatusTypes.RefusedOffer:
									cell = document.AddCellAndText(LocaliseOrDefault(request, referralType + ".ActionDescription", "Refused job"));
									break;
								case ApplicationStatusTypes.Hired:
									cell = document.AddCellAndText(LocaliseOrDefault(request, referralType + ".ActionDescription", "Hired"));
									break;
								case ApplicationStatusTypes.NotHired:
									cell = document.AddCellAndText(LocaliseOrDefault(request, referralType + ".ActionDescription", "Not hired"));
									break;
							}
							cell.Paragraphs[0].ParagraphFormat.Alignment = ParagraphAlignment.Left;
						}
						document.EndTable();

					}

					#endregion

					#region Notes

					// Show notes
					document.CarriageReturn(2);
					document.AppendBoldText(LocaliseOrDefault(request, "Notes", "Notes"));
					if (request.Notes == null || request.Notes.Count == 0)
					{
						document.CarriageReturn();
						document.AppendText(LocaliseOrDefault(request, "JobView.Notes.NoResults", "No notes recorded for this job."));
					}
					else
					{
						foreach (var note in request.Notes.OrderByDescending(x => x.CreatedOn))
						{
							document.CarriageReturn();
							document.AppendItalicText(note.CreatedByFirstname + " " + note.CreatedByLastname + " - " +
							                          LocaliseOrDefault(request, "NoteDate.Format", "{0:MMM d, yyyy}", note.CreatedOn));
							document.CarriageReturn();
							document.AppendText(note.Text);
						}
					}

					#endregion

					#region Reminders

					// Show reminders
					document.CarriageReturn(2);
					document.AppendBoldText(LocaliseOrDefault(request, "Reminders", "Reminders"));
					if (request.Reminders == null || request.Reminders.Count == 0)
					{
						document.CarriageReturn();
						document.AppendText(LocaliseOrDefault(request, "JobView.Reminders.NoResults",
							"No reminders recorded for this job."));
					}
					else
					{
						foreach (var reminder in request.Reminders.OrderByDescending(x => x.CreatedOn))
						{
							document.CarriageReturn();
							document.AppendItalicText(reminder.CreatedByFirstname + " " + reminder.CreatedByLastname + " - " +
							                          LocaliseOrDefault(request, "ReminderDate.Format", "{0:MMM d, yyyy}", reminder.CreatedOn));
							document.CarriageReturn();
							document.AppendText(reminder.Text);
						}
					}

					#endregion

					// Build document name but remove illeagal characters
					response.DocumentName = Path.GetInvalidFileNameChars().Aggregate(request.JobDetails.JobTitle + ".pdf",
						(current, c) =>
							current.Replace(c.ToString(), string.Empty));
					// If no valid doc title then just use a GUID
					if (response.DocumentName.Length == 4)
					{
						response.DocumentName = Guid.NewGuid() + ".pdf";
					}
					// Convert document to array
					response.JobDocument = document.ToByteArray();

				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the job activity summary.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public JobDetailsResponse GetJobActivitySummary(JobDetailsRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new JobDetailsResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var jobActionEventsAdditional = Repositories.Core.ActionEvents.Where(ae => ae.EntityIdAdditional01 == request.JobId);

					var jobActionEventsJobEntity = Repositories.Core.ActionEvents.Where(ae => ae.EntityId == request.JobId);
					var jobActionEventsApplicationEntity = Repositories.Core.ActionEvents.Where(ae => (Repositories.Core.Applications.Where(a => a.Posting.JobId == request.JobId).Select(a => a.Id)).Contains((long)ae.EntityId));

					var actionTypes = new List<ActionTypes>
					{
						ActionTypes.CreateCandidateApplication,
						ActionTypes.UpdateApplicationStatusToInterviewScheduled,
						ActionTypes.UpdateApplicationStatusToFailedToShow,
						ActionTypes.UpdateApplicationStatusToInterviewDenied,
						ActionTypes.UpdateApplicationStatusToHired,
						ActionTypes.UpdateApplicationStatusToNotHired,
						ActionTypes.UpdateApplicationStatusToRefusedOffer,
						ActionTypes.UpdateApplicationStatusToDidNotApply
					};

					var countsForAdditional = CountActionsByActionTypes(actionTypes, jobActionEventsAdditional);

					var gracePeriodBoundary = DateTime.Now.AddDays(AppSettings.NotRespondingToEmployerInvitesGracePeriodDays * -1);
					var jobActivityDetails = new JobActivityView
					{
						StarMatches = 99,
						StarApplicants = 99,
						StarDidNotView = 99,
						StarReferrals = 99,
						StaffReferrals = countsForAdditional[ActionTypes.CreateCandidateApplication],
						SelfReferrals = CountActionsByActionType(ActionTypes.SelfReferral, jobActionEventsApplicationEntity),
						EmployerInvitationsSent = CountActionsByActionType(ActionTypes.InviteJobSeekerToApply, jobActionEventsJobEntity),
						SeekersViewed = Repositories.Core.InviteToApply.Count(i => i.JobId == request.JobId && i.Viewed),
						SeekersDidNotView = Repositories.Core.InviteToApply.Count(i => i.JobId == request.JobId && !i.Viewed && i.CreatedOn < gracePeriodBoundary),
						SeekersClicked = Repositories.Core.CandidateClickHowToApplyView.Count(x => x.JobId == request.JobId),
						SeekersDidNotClick = Repositories.Core.CandidateDidNotClickHowToApplyView.Count(x => x.JobId == request.JobId && x.ActionedOn < gracePeriodBoundary),
						ApplicantsInterviewed = countsForAdditional[ActionTypes.UpdateApplicationStatusToInterviewScheduled],
						ApplicantsFailedToShow = countsForAdditional[ActionTypes.UpdateApplicationStatusToFailedToShow],
						ApplicantsDenied = countsForAdditional[ActionTypes.UpdateApplicationStatusToInterviewDenied],
						ApplicantsHired = countsForAdditional[ActionTypes.UpdateApplicationStatusToHired],
						ApplicantsRejected = countsForAdditional[ActionTypes.UpdateApplicationStatusToNotHired],
						JobOffersRefused = countsForAdditional[ActionTypes.UpdateApplicationStatusToRefusedOffer],
						ApplicantDidNotApply = countsForAdditional[ActionTypes.UpdateApplicationStatusToDidNotApply]
					};

					//jobActivityDetails.ApplicantDidNotApply = CountActionsByActionType(ActionTypes.UpdateApplicationStatusToDidNotApply, jobActionEventsAdditional);

					response.JobActivitySummary = jobActivityDetails;
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Saves the job invite to apply.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public SaveJobInviteToReplyResponse SaveJobInviteToApply(SaveJobInviteToApplyRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new SaveJobInviteToReplyResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var invite = new InviteToApply
					{
						JobId = request.JobId,
						LensPostingId = request.LensPostingId,
						PersonId = request.PersonId,
						Viewed = false,
						CreatedBy = request.UserContext.UserId,
            QueueInvitation = request.QueueInvitation
					};
					Repositories.Core.Add(invite);

					var postingId = Repositories.Core.Postings.Where(p => p.LensPostingId == request.LensPostingId).Select(p => p.Id).FirstOrDefault();
					if (postingId > 0)
					{
						if (Repositories.Core.Query<JobPostingOfInterestSent>().Where(j => j.PersonId == request.PersonId && j.PostingId == postingId).Select(j => j.Id).FirstOrDefault() == 0)
						{
							var postingOfInterest = new JobPostingOfInterestSent
							{
								JobId = request.JobId,
								PostingId = postingId,
								PersonId = request.PersonId,
								Score = request.Score
							};
							Repositories.Core.Add(postingOfInterest);
						}
					}

					Repositories.Core.SaveChanges();

					if (request.QueueInvitation)
					{
						var person = Repositories.Core.Persons.FirstOrDefault(p => p.Id == request.PersonId);
						if (person.IsNotNull())
						{
							var jobSeekerName = string.Concat(person.FirstName, " ", person.LastName);
							Helpers.Logging.LogAction(ActionTypes.InviteJobSeekerToApplyThroughTalent, typeof(Job).Name, request.JobId, person.Id, additionalDetails: jobSeekerName, entityIdAdditional02: 1);
						}
					}
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Updates the invite to apply.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public InviteToApplyVisitedResponse UpdateInviteToApply(InviteToApplyVisitedRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new InviteToApplyVisitedResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					// Get all invites that haven't been viewed for this job for the current candidate and update the viewed property
					var invites = Repositories.Core.Query<InviteToApply>().Where(x => x.LensPostingId == request.LensPostingId && x.PersonId == request.PersonId).ToList();

					if (invites.Any())
					{
						response.IsInvitee = true;

						var viewedInvites = invites.Where(x => !x.Viewed).ToList();
						if (viewedInvites.Any())
						{
							foreach (var inviteToApply in viewedInvites)
								inviteToApply.Viewed = true;

							Repositories.Core.SaveChanges();
						}
					}

					var recommended = Repositories.Core.Query<JobPostingOfInterestSentView>().FirstOrDefault(x => x.LensPostingId == request.LensPostingId && x.PersonId == request.PersonId);
					response.IsRecommended = recommended.IsNotNull() && recommended.IsRecommendation.GetValueOrDefault();
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Updates the job assigned office.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public UpdateJobAssignedOfficeResponse UpdateJobAssignedOffice(UpdateJobAssignedOfficeRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new UpdateJobAssignedOfficeResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var existingJobOffices = (from o in Repositories.Core.JobOfficeMappers
						where o.JobId == request.JobId
						select o).ToList();

					#region Adding a job's offices

					var existingOfficeIds = existingJobOffices.Select(x => x.OfficeId).ToList();

					// Get the job offices from the request that don't already exist in the DB i.e. that need to be added
					var addJobOffices = request.JobOffices.Where(x => !existingOfficeIds.Contains(x.OfficeId));

					var jobOfficesSaved = new List<JobOfficeMapper>();

					foreach (var addJobOffice in addJobOffices)
					{
						Repositories.Core.Add(addJobOffice.CopyTo());

						jobOfficesSaved.Add(addJobOffice.CopyTo());
					}

					#endregion

					#region Deleting a job's offices

					var officeIds = request.JobOffices.Select(x => x.OfficeId).ToList();

					// Get the job's offices from the DB that don't exist in the request.  These need deleting
					var deleteJobOffices = existingJobOffices.Where(x => !officeIds.Contains(x.OfficeId)).ToList();

					deleteJobOffices.ForEach(x => Repositories.Core.Remove(x));

					#endregion

					Repositories.Core.SaveChanges(true);

					// If assigning an office where previous was the default office, an email may need to be sent if screening preferences is set to all
					if ((existingJobOffices.IsNullOrEmpty() || existingJobOffices[0].OfficeUnassigned == true) && request.JobOffices.IsNotNullOrEmpty())
					{
						var job = Repositories.Core.FindById<Job>(request.JobId);
						if (job.ScreeningPreferences == ScreeningPreferences.JobSeekersMustBeScreened && job.PostedOn.IsNotNull())
							job.SendEmailForScreeningPreference(RuntimeContext);

						if (job.PreScreeningServiceRequest.GetValueOrDefault(false) && AppSettings.PreScreeningServiceRequest)
							job.SendEmailForPreScreeningRequest(RuntimeContext);

						if (job.WorkOpportunitiesTaxCreditHires != WorkOpportunitiesTaxCreditCategories.None)
						{
							var offices = Repositories.Core.Offices.Where(o => officeIds.Contains(o.Id)).ToList();
							var phone = job.Employee.Person.PhoneNumbers.Where(x => x.IsPrimary).Select(x => x.AsDto()).FirstOrDefault();
							var coreService = new CoreService(RuntimeContext);

							offices.Select(office => new EmailHiringFromTaxCreditProgramNotificationRequest
							{
								EmailTo = office.BusinessOutreachMailbox,
								WorkOpportunitiesTaxCreditHires = job.WorkOpportunitiesTaxCreditHires,
								Name = String.Format("{0} {1}", job.Employee.Person.FirstName, job.Employee.Person.LastName),
								HiringManagerEmailAddress = job.Employee.Person.EmailAddress,
								PhoneNumber = phone != null ? phone.Number : String.Empty,
								PostedOn = job.PostedOn,
								UserContext = request.UserContext,
								ClientTag = request.ClientTag
							}).ToList()
								.ForEach(r => coreService.SendHiringFromTaxCreditProgramNotification(r));
						}
					}

					jobOfficesSaved.ForEach(ed => LogAction(request, ActionTypes.AddJobOffice, typeof(JobOfficeMapper).Name, ed.Id, request.UserContext.UserId));
					deleteJobOffices.ForEach(ed => LogAction(request, ActionTypes.DeleteJobOffice, typeof(JobOfficeMapper).Name, ed.Id, request.UserContext.UserId));

					LogAction(request, ActionTypes.UpdateJobAssignedOffice, typeof(Job).Name, request.JobId);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Updates the job assigned staff member.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public UpdateJobAssignedStaffMemberResponse UpdateJobAssignedStaffMember(UpdateJobAssignedStaffMemberRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new UpdateJobAssignedStaffMemberResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var staffMember = Repositories.Core.FindById<Person>(request.PersonId);

					if (staffMember.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.PersonNotFound));
						Logger.Info(request.LogData(), string.Format("The staff member '{0}' was not found.", request.PersonId));

						return response;
					}

					var jobs = Repositories.Core.Jobs.Where(x => request.JobIds.Contains(x.Id)).ToList();

					if (jobs.Count < request.JobIds.Count)
					{
						var jobIds = jobs.Select(x => x.Id).ToList();

						var jobsNotFound = request.JobIds.Except(jobIds);
						var jobsNotFoundString = string.Join(", ", jobsNotFound);

						response.SetFailure(FormatErrorMessage(request, ErrorTypes.PersonNotFound));
						Logger.Info(request.LogData(), string.Format("The jobs '{0}' were not found.", jobsNotFoundString));

						return response;
					}

					jobs.ForEach(j => j.AssignedToId = request.PersonId);

					Repositories.Core.SaveChanges();

					jobs.ForEach(j => LogAction(request, ActionTypes.UpdateJobAssignedStaffMember, typeof(Job).Name, j.Id, request.UserContext.UserId));

					if (AppSettings.IntegrationClient != IntegrationClient.Standalone)
					{
						var userExternalId = Repositories.Core.Users.Where(x => x.PersonId == request.PersonId).Select(u => u.ExternalId).FirstOrDefault();
						if (userExternalId.IsNotNullOrEmpty())
						{
							jobs.ForEach(job =>
							{
								if (job.ExternalId.IsNotNullOrEmpty())
								{
									Helpers.Messaging.Publish(new IntegrationRequestMessage
									{
										ActionerId = request.UserContext.UserId,
										IntegrationPoint = IntegrationPoint.AssignAdminToJobOrder,
										AssignAdminToJobOrderRequest = request.ToAssignAdminToJobOrderRequest(job.ExternalId, userExternalId)
									});
								}
							});
						}
					}
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Updates the criminal background exclusion required.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public UpdateCriminalBackgroundExclusionRequiredResponse UpdateCriminalBackgroundExclusionRequired(UpdateCriminalBackgroundExclusionRequiredRequest request)
		{
			var response = new UpdateCriminalBackgroundExclusionRequiredResponse(request);

			// Validate request
			if (!ValidateRequest(request, response, Validate.All))
				return response;

			try
			{
				if (request.JobId.IsNull())
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.JobIdNotSpecified), "Job ID not specified");
					Logger.Error(request.LogData(), response.Message, response.Exception);

					return response;
				}

				var job = Repositories.Core.FindById<Job>(request.JobId);

				if (job.IsNotNull())
				{
					job.CriminalBackgroundExclusionRequired = request.CriminalBackgroundExclusionRequired;

					Repositories.Core.SaveChanges();

					response.Job = job.AsDto();

					LogAction(request, ActionTypes.UpdateCriminalBackgroundExclusionRequired, job);
				}
				else
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.JobNotFound), "Job not found");
					Logger.Error(request.LogData(), response.Message, response.Exception);

					return response;
				}
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}

			return response;
		}

		/// <summary>
		/// Gets the criminal background exclusion required.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public CriminalBackgroundExclusionRequiredResponse GetCriminalBackgroundExclusionRequired(CriminalBackgroundExclusionRequiredRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new CriminalBackgroundExclusionRequiredResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					if (request.JobId.IsNotNull() && request.JobId > 0)
					{
						var job = Repositories.Core.FindById<Job>(request.JobId);

						if (job.IsNull())
						{
							response.SetFailure(FormatErrorMessage(request, ErrorTypes.JobNotFound));
							Logger.Info(request.LogData(), "The job was not found");

							return response;
						}

						var criminalBackgroundExclusionRequired = job.CriminalBackgroundExclusionRequired;
						response.CriminalBackgroundExclusionRequired = criminalBackgroundExclusionRequired.IsNotNull() && (bool)criminalBackgroundExclusionRequired;
					}
					else
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.JobIdNotSpecified));
						Logger.Info(request.LogData(), "The Job ID was not specified");

						return response;
					}
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the job issues.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public JobIssuesResponse GetJobIssues(JobIssuesRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new JobIssuesResponse(request);
				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var issues = Repositories.Core.JobIssues.SingleOrDefault(x => x.JobId == request.JobId);
					response.JobIssues = new JobIssuesModel
					{
						JobIssues = issues.IsNull() ? null : issues.AsDto(),
						PendingPostHireFollowUps =
							Repositories.Core.ApplicationViews.Where(
								x =>
									x.JobId == request.JobId &&
									x.PostHireFollowUpStatus == PostHireFollowUpStatus.Pending).Select(x => x.AsDto())
								.ToList()
					};
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Resolve the job's issues.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public ResolveJobIssuesResponse ResolveJobIssues(ResolveJobIssuesRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new ResolveJobIssuesResponse(request);
				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var issuesResolved = new List<JobIssuesFilter>();

					var issues = Repositories.Core.JobIssues.FirstOrDefault(x => x.JobId == request.JobId);
					if (issues.IsNull())
						return response;

					var now = DateTime.Now;
					if (request.IssuesResolved.IsNotNullOrEmpty())
					{
						if (request.IssuesResolved.Any(x => x == JobIssuesFilter.LowQualityMatches))
						{
							if (issues.LowQualityMatches)
								issuesResolved.Add(JobIssuesFilter.LowQualityMatches);

							issues.LowQualityMatches = false;
							issues.LowQualityMatchesResolvedDate = now;
						}
						if (request.IssuesResolved.Any(x => x == JobIssuesFilter.LowReferralActivity))
						{
							if (issues.LowReferralActivity)
								issuesResolved.Add(JobIssuesFilter.LowReferralActivity);

							issues.LowReferralActivity = false;
							issues.LowReferralActivityResolvedDate = now;
						}
						if (request.IssuesResolved.Any(x => x == JobIssuesFilter.LowQualityMatches))
						{
							if (issues.LowQualityMatches)
								issuesResolved.Add(JobIssuesFilter.LowQualityMatches);

							issues.LowQualityMatches = false;
							issues.LowQualityMatchesResolvedDate = now;
						}
						if (request.IssuesResolved.Any(x => x == JobIssuesFilter.NotViewingReferrals))
						{
							if (issues.NotViewingReferrals)
								issuesResolved.Add(JobIssuesFilter.NotViewingReferrals);

							issues.NotViewingReferrals = false;
							issues.NotViewingReferralsResolvedDate = now;
						}
						if (request.IssuesResolved.Any(x => x == JobIssuesFilter.SearchNotInviting))
						{
							if (issues.SearchingButNotInviting)
								issuesResolved.Add(JobIssuesFilter.SearchNotInviting);

							issues.SearchingButNotInviting = false;
							issues.SearchingButNotInvitingResolvedDate = now;
						}
						if (request.IssuesResolved.Any(x => x == JobIssuesFilter.ClosingDateRefreshed))
						{
							if (issues.ClosingDateRefreshed)
								issuesResolved.Add(JobIssuesFilter.ClosingDateRefreshed);

							issues.ClosingDateRefreshed = false;
						}
						if (request.IssuesResolved.Any(x => x == JobIssuesFilter.EarlyJobClosing))
						{
							if (issues.EarlyJobClosing)
								issuesResolved.Add(JobIssuesFilter.EarlyJobClosing);

							issues.EarlyJobClosing = false;
						}
						if (request.IssuesResolved.Any(x => x == JobIssuesFilter.NegativeSurveyResponse))
						{
							if (issues.NegativeSurveyResponse)
								issuesResolved.Add(JobIssuesFilter.NegativeSurveyResponse);

							issues.NegativeSurveyResponse = false;
						}
						if (request.IssuesResolved.Any(x => x == JobIssuesFilter.PositiveSurveyResponse))
						{
							if (issues.PositiveSurveyResponse)
								issuesResolved.Add(JobIssuesFilter.PositiveSurveyResponse);

							issues.PositiveSurveyResponse = false;
						}
						if (request.IssuesResolved.Any(x => x == JobIssuesFilter.FollowUpRequested))
						{
							if (issues.FollowUpRequested)
								issuesResolved.Add(JobIssuesFilter.FollowUpRequested);

							issues.FollowUpRequested = false;
						}
					}
					if (request.PostHireFollowUpsResolved.IsNotNullOrEmpty())
					{
						var postHireFollowUps =
							Repositories.Core.Applications.Where(
								x =>
									x.PostHireFollowUpStatus == PostHireFollowUpStatus.Pending &&
									request.PostHireFollowUpsResolved.Contains(x.Id)).ToList();
						postHireFollowUps.ForEach(x => x.PostHireFollowUpStatus = PostHireFollowUpStatus.Completed);
					}

					if (request.IsAutoResolution && !request.UserContext.IsShadowingUser)
					{
						issuesResolved.ForEach(issue =>
						{
							var additionalDetail = Helpers.Localisation.GetEnumLocalisedText(issue, true);
							LogAction(request, ActionTypes.AutoResolvedIssue, typeof(Job).Name, request.JobId, additionalDetails: additionalDetail);
						});
					}

					Repositories.Core.SaveChanges();

				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Invites the jobseekers to apply for the job.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public InviteJobseekersToApplyResponse InviteJobseekersToApply(InviteJobseekersToApplyRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new InviteJobseekersToApplyResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var sendersName = string.Format("{0} {1}", request.UserContext.FirstName, request.UserContext.LastName);
					Helpers.Messaging.Publish(request.ToInviteJobseekersToApplyMessage(request.PersonIdsWithScores, request.JobId, request.JobLink, request.LensPostingId, sendersName, request.SenderEmail));
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		public ImportJobResponse ImportJob(ImportJobRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new ImportJobResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					if (request.ExternalJobIds.IsNullOrEmpty()) return response;

					var batchId = response.ImportBatchId = (response.ImportBatchId.IsNotNull() ? response.ImportBatchId : Guid.NewGuid());
					response.Results = new List<ImportOutcome>();
					foreach (var externalJobId in request.ExternalJobIds)
					{
						var outcome = new ImportOutcome
						{
							ImportBatch = batchId,
							Entity = EntityTypes.Job,
							ExternalId = externalJobId.Item1
						};

						var integrationRequest = new GetJobRequest
						{
							JobId = externalJobId.Item1
						};

						var integrationResponse = Repositories.Integration.GetJob(integrationRequest);

						if (integrationResponse.Outcome != IntegrationOutcome.Success) continue;

						var dbJob = Repositories.Core.Query<Job>().FirstOrDefault(x => x.ExternalId == integrationResponse.Job.ExternalId);
						var job = dbJob.IsNull() ? new JobDto() : dbJob.AsDto();

						// Meta data
						job.ApprovalStatus = externalJobId.Item2;
						job.JobStatus = (job.ApprovalStatus == ApprovalStatuses.Approved) ? JobStatuses.Active : JobStatuses.Draft;
						job.BusinessUnitId = Repositories.Core.BusinessUnits.First(x => x.ExternalId == integrationResponse.Job.ExternalBusinessUnitId).Id;
						job.EmployerId = Repositories.Core.Employers.First(x => x.ExternalId == integrationResponse.Job.ExternalEmployerId).Id;
						job.ExternalId = integrationResponse.Job.ExternalId;
						if (job.EmployeeId.IsNullOrZero())
						{
							var employeeBusinessUnit = Repositories.Core.EmployeeBusinessUnits.FirstOrDefault(x => x.BusinessUnitId == job.BusinessUnitId);
							if (employeeBusinessUnit.IsNotNull())
								job.EmployeeId = employeeBusinessUnit.EmployeeId;
						}

						// Title and company
						job.JobTitle = integrationResponse.Job.Title;
						var onet = Repositories.Library.Onets.FirstOrDefault(x => x.OnetCode == integrationResponse.Job.OnetCode);
						job.OnetId = onet.IsNotNull() ? onet.Id : (long?)null;
						job.IsConfidential = integrationResponse.Job.ConfidentialEmployer;

						if (integrationResponse.Job.BusinessUnitDescription.IsNotNullOrEmpty())
						{
							// Get or save business unit description
							var descriptions = Repositories.Core.BusinessUnitDescriptions.FirstOrDefault(x => x.BusinessUnitId == job.BusinessUnitId && x.Description == integrationResponse.Job.BusinessUnitDescription);
							if (descriptions.IsNotNull())
							{
								job.BusinessUnitDescriptionId = descriptions.Id;
							}
							else
							{
								var buDesc = new BusinessUnitDescription
								{
									BusinessUnitId = job.BusinessUnitId.Value,
									Description = integrationResponse.Job.BusinessUnitDescription,
									Title = LocaliseOrDefault(request, "Import.BusinessUnitDescription", "Imported description")
								};
								Repositories.Core.Add(buDesc);
								job.BusinessUnitDescriptionId = buDesc.Id;
							}
							job.EmployerDescriptionPostingPosition = EmployerDescriptionPostingPositions.BelowJobPosting;
						}

						// Job description
						job.Description = integrationResponse.Job.Description;
						job.DescriptionPath = 22;

						// Requirements
						job.MinimumEducationLevel = integrationResponse.Job.EducationLevel;
						job.HideEducationOnPosting = false;
						job.MinimumEducationLevelRequired = false;
						job.StudentEnrolled = false;


						if (integrationResponse.Job.MinimumAge > 0)
						{
							job.MinimumAge = integrationResponse.Job.MinimumAge;
							job.MinimumAgeReason = LocaliseOrDefault(request, "Import.MinimumAgeReason", "Previously approved");
							job.MinimumAgeReasonValue = (int?)MinimumAgeReason.Other;
						}
						else
						{
							job.MinimumAge = null;
							job.MinimumAgeReason = "";
							job.MinimumAgeReasonValue = null;
						}
						job.MinimumAgeRequired = false;
						job.MinimumExperienceRequired = false;
						job.DrivingLicenceRequired = false;
						job.HideDriversLicenceOnPosting = true;
						job.CertificationRequired = false;
						job.HideCertificationsOnPosting = true;
						job.LanguagesRequired = false;
						job.HideLanguagesOnPosting = true;
						job.CriminalBackgroundExclusionRequired = false;
						job.PostingFlags = JobPostingFlags.None;
						job.IsCommissionBased = false;
						job.IsSalaryAndCommissionBased = false;
						job.LicencesRequired = false;
						job.JobStatusId = 0;
						job.SuitableForHomeWorker = false;

						job.JobLocationType = JobLocationTypes.MainSite;

						var jobLocations = integrationResponse.Job.Addresses.Select(address => new JobLocationDto
						{
							AddressLine1 = address.AddressLine1,
							City = address.City,
							Zip = address.PostCode,
							State = Helpers.Lookup.GetLookupText(LookupTypes.States, address.StateId),
							IsPublicTransitAccessible = integrationResponse.Job.PublicTransportAccessible,
							Location = address.PostCode
						}).ToList();

						job.FederalContractor = integrationResponse.Job.FederalContractor;
						job.CourtOrderedAffirmativeAction = integrationResponse.Job.CourtOrderedAffirmativeAction;
						job.ForeignLabourCertificationH2A = integrationResponse.Job.ForeignLabourCertificationH2A;
						job.ForeignLabourCertificationH2B = integrationResponse.Job.ForeignLabourCertificationH2B;
						job.ForeignLabourCertificationOther = integrationResponse.Job.ForeignLabourCertificationOther;
						job.WorkOpportunitiesTaxCreditHires = integrationResponse.Job.WorkOpportunitiesTaxCreditHires;

						// Salary and benefits
						job.MinSalary = integrationResponse.Job.MinSalary;
						job.MaxSalary = integrationResponse.Job.MaxSalary;
						job.SalaryFrequencyId = integrationResponse.Job.SalaryUnitId;
						job.MeetsMinimumWageRequirement = true;
						job.HideSalaryOnPosting = false;
						job.NormalWorkDays = integrationResponse.Job.Weekdays;
						job.MinimumHoursPerWeek = integrationResponse.Job.MinimumHoursPerWeek;
						job.HoursPerWeek = integrationResponse.Job.MaximumHoursPerWeek;
						job.NormalWorkShiftsId = integrationResponse.Job.ShiftId;
						job.JobTypeId = integrationResponse.Job.JobTypeId;
						job.EmploymentStatusId = integrationResponse.Job.EmploymentStatusId;

						job.OverTimeRequired = false;

						job.LeaveBenefits = integrationResponse.Job.LeaveBenefits;
						job.RetirementBenefits = integrationResponse.Job.RetirementBenefits;
						job.InsuranceBenefits = integrationResponse.Job.InsuranceBenefits;
						job.MiscellaneousBenefits = integrationResponse.Job.MiscellaneousBenefits;
						job.OtherBenefitsDetails = integrationResponse.Job.OtherBenefitsDetails;

						// Recruitment information
						job.ClosingOn = integrationResponse.Job.ClosingDate;
						job.NumberOfOpenings = integrationResponse.Job.Openings;
						job.InterviewContactPreferences = integrationResponse.Job.InterviewContactPreferences;
						job.InterviewEmailAddress = integrationResponse.Job.ContactEmail ?? string.Empty;
						job.InterviewApplicationUrl = integrationResponse.Job.ContactUrl ?? string.Empty;
						job.InterviewMailAddress = integrationResponse.Job.ContactAddress ?? string.Empty;
						job.InterviewFaxNumber = integrationResponse.Job.ContactFax ?? string.Empty;
						job.InterviewPhoneNumber = integrationResponse.Job.ContactPhone ?? string.Empty;
						job.InterviewDirectApplicationDetails = integrationResponse.Job.ContactInPerson ?? string.Empty;
						job.ScreeningPreferences = ScreeningPreferences.AllowUnqualifiedApplications;
						job.PreScreeningServiceRequest = false;

						job.WizardStep = 7;
						job.WizardPath = 1;

						if (job.ApprovalStatus == ApprovalStatuses.WaitingApproval && job.AwaitingApprovalOn.IsNull())
							job.AwaitingApprovalOn = DateTime.Now;

						var saveJobRequest = new SaveJobRequest
						{
							ClientTag = request.ClientTag,
							RequestId = request.RequestId,
							SessionId = request.SessionId,
							UserContext = request.UserContext,
							VersionNumber = request.VersionNumber,
							Job = job,
							JobLocations = jobLocations,
							GeneratePostingForNewJob = true,
							Module = FocusModules.General
						};
						var saveResponse = SaveJob(saveJobRequest);
						if (saveResponse.Acknowledgement == AcknowledgementType.Failure)
							throw saveResponse.Exception;

						if (saveResponse.Job.ApprovalStatus == ApprovalStatuses.Approved)
						{
							// Post job if approved
							var postJobRequest = new PostJobRequest
							{
								ClientTag = request.ClientTag,
								RequestId = request.RequestId,
								SessionId = request.SessionId,
								UserContext = request.UserContext,
								VersionNumber = request.VersionNumber,
								Job = saveResponse.Job,
								Module = FocusModules.Assist
							};

							var postJobResponse = PostJob(postJobRequest);

							if (postJobResponse.Acknowledgement == AcknowledgementType.Failure)
								throw postJobResponse.Exception;
						}

						outcome.FocusId = saveResponse.Job.Id.GetValueOrDefault();
						outcome.Success = true;
						response.Results.Add(outcome);
					}

				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the job credit check required.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public GetJobCreditCheckRequiredResponse GetJobCreditCheckRequired(GetJobCreditCheckRequiredRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new GetJobCreditCheckRequiredResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					if (request.JobId.IsNotNull() && request.JobId > 0)
					{
						response.CreditCheckRequired = Repositories.Core.FindById<Job>(request.JobId).CreditCheckRequired;
					}
					else
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.JobIdNotSpecified));
						Logger.Info(request.LogData(), "The Job ID was not specified");

						return response;
					}
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Updates the credit check required.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public UpdateCreditCheckRequiredResponse UpdateCreditCheckRequired(UpdateCreditCheckRequiredRequest request)
		{
			var response = new UpdateCreditCheckRequiredResponse(request);

			// Validate request
			if (!ValidateRequest(request, response, Validate.All))
				return response;

			try
			{
				var job = Repositories.Core.FindById<Job>(request.JobId);

				if (job.IsNotNull())
				{
					job.CreditCheckRequired = request.CreditCheckRequired;

					Repositories.Core.SaveChanges();

					response.Job = job.AsDto();

					LogAction(request, ActionTypes.UpdateCreditCheckRequired, job);
				}
				else
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.JobNotFound), "Job not found");
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}

			return response;
		}


		/// <summary>
		/// Gets the hiring manager person name email for job.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public HiringManagerPersonNameEmailForJobResponse GetHiringManagerPersonNameEmailForJob(HiringManagerPersonNameEmailForJobRequest request)
		{
			var response = new HiringManagerPersonNameEmailForJobResponse(request);

			// Validate request
			if (!ValidateRequest(request, response, Validate.All))
				return response;

			try
			{
				var job = Repositories.Core.FindById<Job>(request.JobId);

				if (job.IsNotNull())
				{
					var personNameEmail = new PersonNameEmail
					{
						Name = string.Format("{0} {1}", job.Employee.Person.FirstName, job.Employee.Person.LastName),
						Email = job.Employee.Person.EmailAddress
					};

					response.Person = personNameEmail;
				}
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}

			return response;
		}


		/// <summary>
		/// Gets the posting generator model.
		/// </summary>
		/// <param name="job">The job.</param>
		/// <returns></returns>
		private PostingGeneratorModel GetPostingGeneratorModel(Job job)
		{
			return new PostingGeneratorModel
			{
				Job = job.AsDto(),
				BusinessUnit = (job.BusinessUnit.IsNotNull()) ? job.BusinessUnit.AsDto() : null,
				BusinessUnitDescription = (job.BusinessUnitDescription.IsNotNull()) ? job.BusinessUnitDescription.AsDto() : null,
				JobLicences = (job.JobLicences.IsNotNull()) ? job.JobLicences.Select(license => license.AsDto()).ToList() : null,
				JobCertificates = (job.JobCertificates.IsNotNull()) ? job.JobCertificates.Select(certificate => certificate.AsDto()).ToList() : null,
				JobLanguages = (job.JobLanguages.IsNotNull()) ? job.JobLanguages.Select(language => language.AsDto()).ToList() : null,
				JobSpecialRequirements = (job.JobSpecialRequirements.IsNotNull()) ? job.JobSpecialRequirements.Select(requirement => requirement.AsDto()).ToList() : null,
				JobLocations = (job.JobLocations.IsNotNull()) ? job.JobLocations.Select(location => location.AsDto()).ToList() : null,
				JobProgramsOfStudy = (job.JobProgramOfStudies.IsNotNull()) ? job.JobProgramOfStudies.Select(programOfStudy => programOfStudy.AsDto()).ToList() : null,
				JobDrivingLicenceEndorsements = (job.JobDrivingLicenceEndorsements.IsNotNullOrEmpty()) ? job.JobDrivingLicenceEndorsements.Select(drivingLicenceEndorsement => drivingLicenceEndorsement.AsDto()).ToList() : null,
			};
		}

		/// <summary>
		/// Updates the job closing date.
		/// </summary>
		/// <param name="job">The job.</param>
		/// <param name="closingOn">The closing on.</param>
		private void UpdateJobClosingDate(Job job, DateTime? closingOn)
		{
			if (AppSettings.VeteranPriorityServiceEnabled && AppSettings.ExtendedVeteranPriorityofService && (job.ExtendVeteranPriority.IsNotNull() && job.ExtendVeteranPriority == ExtendVeteranPriorityOfServiceTypes.ExtendForLifeOfPosting))
			{
				var veteranPriorityEndDate = Convert.ToDateTime(closingOn);
				var ts = new TimeSpan(23, 59, 59);
				veteranPriorityEndDate = veteranPriorityEndDate.Date + ts;

				job.VeteranPriorityEndDate = veteranPriorityEndDate;
			}

			job.ClosingOn = closingOn;
			job.ClosingOnUpdated = true;
		}

		public SaveSpideredJobResponse SaveSpideredJob([NotNull] SaveSpideredJobRequest request)
		{
			if (request == null) throw new ArgumentNullException("request");

			using (Profiler.Profile(request.LogData(), request))
			{
				var postingExists = false;
				var response = new SaveSpideredJobResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					Posting posting = null;

					if (!string.IsNullOrWhiteSpace(request.LensPostingMigrationId))
					{
						posting = Repositories.Core.Postings.FirstOrDefault(x => x.LensPostingId == request.LensPostingMigrationId);
					}

					if (posting.IsNotNull())
					{
						postingExists = true;
					}
					else
					{
						// create a new posting object
						posting = new Posting();
					}

					posting.LensPostingId = request.LensPostingMigrationId;
					posting.Html = "";
					posting.ViewedCount = 0;
					posting.JobTitle = request.JobTitle;
					posting.EmployerName = request.EmployerName;
					posting.OriginId = request.OriginId;
					posting.ExternalId = request.JobMigrationId;
					posting.StatusId = (PostingStatuses)request.Status;
					//posting.OriginId = 999; /* 999 is the spidered job OriginId */

					if (!postingExists)
					{
						Repositories.Core.Add(posting);
					}
					Repositories.Core.SaveChanges(true);

					response.Posting = posting.AsDto();

					#region LogAction

					// we need

					//if (newJob)
					//  LogAction(request, ActionTypes.CreateJob, typeof(Job).Name, job.Id, job.BusinessUnitId, overrideUserId: request.JobCreatedBy);
					//else if (job.PostedBy.IsNotNull())
					//  LogAction(request, ActionTypes.SaveJob, job, request.JobCreatedBy);
					//else
					//  LogAction(request, ActionTypes.SaveJobWizardStep, typeof(Job).Name, job.Id, job.WizardStep, overrideUserId: request.JobCreatedBy);

					#endregion
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		

    /// <summary>
    /// Validates the approval hold reasons.
    /// </summary>
    /// <param name="reasons">The reasons.</param>
    /// <returns></returns>
    private ErrorTypes ValidateJobDenialReasons(List<KeyValuePair<long, string>> reasons)
    {
        return Helpers.Employee.ValidateReferralStatusReasons(reasons, LookupTypes.JobDenialReasons, "JobDenialReasons.OtherReason");
    }

	}
}
