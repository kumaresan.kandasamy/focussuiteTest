﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using Framework.Logging;

using Focus.Core;
using Focus.Core.Messages.UtilityService;
using Focus.Services.Core;
using Focus.Services.DtoMappers;
using Focus.Services.Queries;
using Focus.Services.ServiceContracts;

#endregion

namespace Focus.Services.ServiceImplementations
{
	public class UtilityService : ServiceBase, IUtilityService
	{
    /// <summary>
		/// Initializes a new instance of the <see cref="UtilityService" /> class.
		/// </summary>
		public UtilityService() : this(null)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="UtilityService" /> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		public UtilityService(IRuntimeContext runtimeContext) : base(runtimeContext)
		{ }
		/// <summary>
		/// Tags the binary document.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public BinaryDocumentResponse TagBinaryDocument(BinaryDocumentRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new BinaryDocumentResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					string html;
					response.TaggedDocument = Repositories.Lens.TagWithHtml(request.BinaryContent, request.FileExtension, request.DocumentType, out html);
					response.Html = html;
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}
		
		/// <summary>
		/// Tags the plain document.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public PlainDocumentResponse TagPlainDocument(PlainDocumentRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new PlainDocumentResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					response.TaggedDocument = Repositories.Lens.Tag(request.PlainDocument, request.DocumentType);
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}
	}
}
