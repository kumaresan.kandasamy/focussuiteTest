﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using Focus.Core.Criteria;
using Focus.Core.Messages.AnnotationService;
using Focus.Core.Models.Career;
using Focus.Data.Core.Entities;
using Focus.Services.DtoMappers;
using Focus.Core;
using Focus.Services.Core;
using Focus.Services.ServiceContracts;

using Framework.Core;
using Framework.Logging;
using Mindscape.LightSpeed;
using Mindscape.LightSpeed.Querying;

#endregion

namespace Focus.Services.ServiceImplementations
{
	public class AnnotationService : ServiceBase, IAnnotationService
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="AnnotationService" /> class.
		/// </summary>
		public AnnotationService() : this(null)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="AnnotationService" /> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		public AnnotationService(IRuntimeContext runtimeContext) : base(runtimeContext)
		{ }

    #region Bookmarks

    /// <summary>
    /// Gets a bookmark.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    public BookmarkResponse GetBookmark(BookmarkRequest request)
    {
      using (Profiler.Profile(request.LogData(), request))
      {
        var response = new BookmarkResponse(request);

        // Validate request
        if (!ValidateRequest(request, response, Validate.Explorer))
          return response;

        try
        {
          switch (request.Criteria.FetchOption)
          {
            case CriteriaBase.FetchOptions.Single:
              response.Bookmark = Repositories.Core.FindById<Bookmark>(request.Criteria.BookmarkId).AsDto();

              break;

            case CriteriaBase.FetchOptions.List:
              var query = Repositories.Core.Query<Bookmark>().Where(x => x.UserId == request.UserContext.UserId);

              if (request.Criteria.BookmarkType.HasValue)
              {
                query = query.Where(x => x.Type == request.Criteria.BookmarkType);
              }
              else
              {
                switch (AppSettings.Module)
                {
                  case FocusModules.Explorer:
                    query = query.Where(x => x.Type == BookmarkTypes.Report || x.Type == BookmarkTypes.ReportItem);
                    break;

                  case FocusModules.Career:
                  case FocusModules.CareerExplorer:
                    query = query.Where(x => x.Type != BookmarkTypes.EmailedReportItem);
                    break;
                }
              }

              if (request.Criteria.BookmarkName.IsNotNullOrEmpty())
                query = query.Where(x => x.Name == request.Criteria.BookmarkName);

              response.Bookmarks = query.Select(x => x.AsDto()).ToList();

              break;
          }
        }
        catch (Exception ex)
        {
          response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
          Logger.Error(request.LogData(), response.Message, response.Exception);
        }

        return response;
      }
    }
		
    /// <summary>
    /// Creates the bookmark.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    public CreateBookmarkResponse CreateBookmark(CreateBookmarkRequest request)
    {
      using (Profiler.Profile(request.LogData(), request))
      {
        var response = new CreateBookmarkResponse(request);

        // Validate request
        if (!ValidateRequest(request, response,Validate.All))
          return response;

        try
        {
          var bookmark = new Bookmark();
          request.Bookmark.CopyTo(bookmark);
          Repositories.Core.Add(bookmark);
					Repositories.Core.SaveChanges(true);
        }
        catch (Exception ex)
        {
          response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
          Logger.Error(request.LogData(), response.Message, response.Exception);
        }

        return response;
      }
    }

    /// <summary>
    /// Deletes the bookmark.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    public DeleteBookmarkResponse DeleteBookmark(DeleteBookmarkRequest request)
    {
      using (Profiler.Profile(request.LogData(), request))
      {
        var response = new DeleteBookmarkResponse(request);

        // Validate request
        if (!ValidateRequest(request, response, Validate.All))
          return response;

        try
        {
            var bookmark = Repositories.Core.FindById<Bookmark>(request.BookmarkId);
            // KRP 01.March.2017
            // Bug fix: Remove the bookmark only when it belongs to the current user
            if (bookmark.UserId == request.UserContext.UserId)
            {
                Repositories.Core.Remove(bookmark);
                Repositories.Core.SaveChanges(true);

            }
        }
        catch (Exception ex)
        {
          response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
          Logger.Error(request.LogData(), response.Message, response.Exception);
        }

        return response;
      }
    }

    #endregion

    #region Saved search

		/// <summary>
		/// Saves the search.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    public SaveSearchResponse SaveSearch(SaveSearchRequest request)
	  {
	    using (Profiler.Profile(request.LogData(), request))
	    {
	      var response = new SaveSearchResponse(request);

	      // Validate request
	      if (!ValidateRequest(request, response, Validate.All))
	        return response;

	      try
	      {
          var userId = request.SaveForUserId ?? request.UserContext.UserId;

          var user = Repositories.Core.Users.SingleOrDefault(x => x.Id == userId);

	        if (user.IsNull())
	        {
	          response.SetFailure(FormatErrorMessage(request, ErrorTypes.EmployeeNotFound));
            Logger.Info(request.LogData(), string.Format("Cannot find User with Id '{0}'.", userId));
	          return response;
	        }

	        SavedSearch savedSearch;
	        if (request.Id == 0)
	        {
	          savedSearch = new SavedSearch
	                          {
	                            Name = request.Name,
	                            SearchCriteria = request.SearchCriteria.Serialize(),
	                            AlertEmailAddress = request.AlertEmailAddress,
	                            AlertEmailFormat = request.AlertEmailFormat,
	                            AlertEmailFrequency = request.AlertEmailFrequency,
	                            AlertEmailRequired = request.AlertEmailRequired,
                              Type = request.SearchType
	                          };

            if (request.AlertEmailRequired) savedSearch.AlertEmailScheduledOn = GetAlertEmailScheduledDate(request.AlertEmailFrequency);

            savedSearch.Users.Add(user);
	        }
	        else
	        {
						savedSearch = Repositories.Core.FindById<SavedSearch>(request.Id);
            var currentAlertFrequecy = savedSearch.AlertEmailFrequency;
            savedSearch.Name = request.Name;
            savedSearch.SearchCriteria = request.SearchCriteria.Serialize();
            savedSearch.AlertEmailAddress = request.AlertEmailAddress;
            savedSearch.AlertEmailFormat = request.AlertEmailFormat;
            savedSearch.AlertEmailFrequency = request.AlertEmailFrequency;
            savedSearch.AlertEmailRequired = request.AlertEmailRequired;
            savedSearch.Type = request.SearchType;

            if (savedSearch.AlertEmailRequired)
            {
              if (savedSearch.AlertEmailScheduledOn.IsNull() || (savedSearch.AlertEmailFrequency != currentAlertFrequecy)) savedSearch.AlertEmailScheduledOn = GetAlertEmailScheduledDate(savedSearch.AlertEmailFrequency);
            }

            var newUser = savedSearch.Users.FirstOrDefault(x => user != null && x.Id == user.Id);

            if (newUser.IsNull())
              savedSearch.Users.Add(user);
	        }

          Repositories.Core.SaveChanges();

          if (request.OverrideCreatedOn.IsNotNull() || request.OverrideUpdatedOn.IsNotNull())
          {
            var changes = new Dictionary<string, object>();

            if (request.OverrideCreatedOn.IsNotNull())
              changes.Add("CreatedOn", request.OverrideCreatedOn);

            if (request.OverrideUpdatedOn.IsNotNull())
              changes.Add("UpdatedOn", request.OverrideUpdatedOn);

            var updateQuery = new Query(typeof(SavedSearch), Entity.Attribute("Id") == savedSearch.Id);
            Repositories.Core.Update(updateQuery, changes);
            Repositories.Core.SaveChanges();
          }

          Helpers.SelfService.PublishSelfServiceActivity(ActionTypes.SavePostingSavedSearch, request.UserContext.ActionerId, request.UserContext.UserId, user.PersonId);
          LogAction(request, ActionTypes.SavePostingSavedSearch, savedSearch);

          response.SavedSearchId = savedSearch.Id;
	      }
	      catch (Exception ex)
	      {
	        response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
	        Logger.Error(request.LogData(), response.Message, response.Exception);
	      }

	      return response;
	    }
	  }

		/// <summary>
		/// Lists the search.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
	  public ListSearchResponse ListSearch(ListSearchRequest request)
	  {
	    using (Profiler.Profile(request.LogData(), request))
	    {
	      var response = new ListSearchResponse(request);

	      // Validate request
	      if (!ValidateRequest(request, response, Validate.All))
	        return response;

	      try
	      {
          var searchDetails = new List<SearchDetails>();

          var searches = (from userSearch in Repositories.Core.SavedSearchUsers
                         join search in Repositories.Core.SavedSearches
                            on userSearch.SavedSearchId equals search.Id
                         where (userSearch.UserId == request.UserContext.UserId && search.Type == SavedSearchTypes.CareerPostingSearch)
                         select search).ToList();

          foreach (var search in searches)
	        {
						var jobAlert = new JobAlert
	                           {
                               EmailRequired = search.AlertEmailRequired,
	                             Format = search.AlertEmailFormat,
	                             Frequency = search.AlertEmailFrequency,
	                             JobSearchCriteria = null,
	                             AlertId = search.Id.ToString(),
	                             Name = search.Name,
	                             Status = AlertStatus.Active,
	                             ToEmailAddress = search.AlertEmailAddress
	                           };

	          searchDetails.Add(new SearchDetails
	                              {
	                                JobAlert = jobAlert,
	                                Name = search.Name,
	                                Notes = "",
	                                SavedTime = search.UpdatedOn,
	                                SearchCriteria = search.SearchCriteria.Deserialize(typeof (SearchCriteria)) as SearchCriteria,
	                                SearchId = search.Id
	                              });

	          response.SavedSearches = searchDetails;
	        }
	      }
        catch (Exception ex)
	      {
	        response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
	        Logger.Error(request.LogData(), response.Message, response.Exception);
	      }

	      return response;
	    }
	  }

		/// <summary>
		/// Deletes the save search.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
	  public DeleteSaveSearchResponse DeleteSaveSearch(DeleteSaveSearchRequest request)
		{
      using (Profiler.Profile(request.LogData(), request))
      {
        var response = new DeleteSaveSearchResponse(request);

        // Validate request
        if (!ValidateRequest(request, response, Validate.All))
          return response;

        try
        {
          Repositories.Core.Remove<SavedSearchUser>(x => x.SavedSearchId == request.Id);
          Repositories.Core.Remove<SavedSearch>(x => x.Id == request.Id); 
          Repositories.Core.SaveChanges(true);
        }

        catch (Exception ex)
        {
          response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
          Logger.Error(request.LogData(), response.Message, response.Exception);
        }

        return response;
      }
    }

    #endregion

		#region Helper Methods

		/// <summary>
    /// Gets the alert email scheduled date.
    /// </summary>
    /// <param name="frequency">The frequency.</param>
    /// <returns></returns>
    internal static DateTime? GetAlertEmailScheduledDate(EmailAlertFrequencies frequency)
    {
      switch (frequency)
      {
        case EmailAlertFrequencies.Weekly:
          return DateTime.Now.Date.AddDays(7);
        default:
          return DateTime.Now.Date.AddDays(1);
      }
		}

		#endregion
	}
}
