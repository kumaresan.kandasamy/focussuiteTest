﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Linq;
using Focus.Core.Messages.JobService;
using Framework.Core;

using Focus.Core.IntegrationMessages;
using Focus.Services.Messages;
using Focus.Data.Core.Entities;
using Focus.Core;
using Focus.Core.Messages.CandidateService;

using AssignEmployerToAdminRequest = Focus.Core.Messages.EmployerService.AssignEmployerToAdminRequest;
using AssignJobSeekerToAdminRequest = Focus.Core.Messages.CandidateService.AssignJobSeekerToAdminRequest;
using Employer = Focus.Data.Core.Entities.Employer;

#endregion

namespace Focus.Services.ServiceImplementations
{
  public static class IntegrationExtensions
  {
    #region New world

    public static AssignAdminToEmployerRequest ToAssignAdminToEmployerRequest(this AssignEmployerToAdminRequest request, string externalEmployerId, string externalAdminId)
    {
      var integrationRequest = new AssignAdminToEmployerRequest
                               {
                                 UserId = request.UserContext.UserId,
                                 AdminId = externalAdminId,
                                 EmployerId = externalEmployerId,
                                 ExternalAdminId = request.UserContext.ExternalUserId,
                                 ExternalOfficeId = request.UserContext.ExternalOfficeId,
                                 ExternalPassword = request.UserContext.ExternalPassword,
                                 ExternalUsername = request.UserContext.ExternalUserName
                               };
      return integrationRequest;
    }

		public static AssignAdminToJobOrderRequest ToAssignAdminToJobOrderRequest(this UpdateJobAssignedStaffMemberRequest request, string jobExternalId, string externalAdminId)
		{
			var integrationRequest = new AssignAdminToJobOrderRequest
			{
				UserId = request.UserContext.UserId,
				AdminId = externalAdminId,
				JobExternalId = jobExternalId,
				ExternalAdminId = request.UserContext.ExternalUserId,
				ExternalOfficeId = request.UserContext.ExternalOfficeId,
				ExternalPassword = request.UserContext.ExternalPassword,
				ExternalUsername = request.UserContext.ExternalUserName
			};
			return integrationRequest;
		}

    public static AssignAdminToJobSeekerRequest ToAssignAdminToJobSeekerRequest(this AssignJobSeekerToAdminRequest request, string externalJobSeekerId, string externalAdminId)
    {
      var integrationRequest = new AssignAdminToJobSeekerRequest
      {
        UserId = request.UserContext.UserId,
        AdminId = externalAdminId,
        JobSeekerId = externalJobSeekerId,
        ExternalAdminId = request.UserContext.ExternalUserId,
        ExternalOfficeId = request.UserContext.ExternalOfficeId,
        ExternalPassword = request.UserContext.ExternalPassword,
        ExternalUsername = request.UserContext.ExternalUserName
      };
      return integrationRequest;
    }

    public static AssignAdminToJobSeekerRequest ToAssignAdminToJobSeekerRequest(this UpdateAssignedStaffMemberRequest request, string externalJobSeekerId, string externalAdminId)
    {
      var integrationRequest = new AssignAdminToJobSeekerRequest
      {
        UserId = request.UserContext.UserId,
        AdminId = externalAdminId,
        JobSeekerId = externalJobSeekerId,
        ExternalAdminId = request.UserContext.ExternalUserId,
        ExternalOfficeId = request.UserContext.ExternalOfficeId,
        ExternalPassword = request.UserContext.ExternalPassword,
        ExternalUsername = request.UserContext.ExternalUserName
      };
      return integrationRequest;
    }

    public static void PublishUserUpdateIntegrationRequest(this User user, long actionerId, IRuntimeContext context, List<AssignJobSeekerActivityRequest> jobSeekerActivities = null)
    {
      if (context.AppSettings.IntegrationClient == IntegrationClient.Standalone) return;

      var publish = false;

      var intRequest = new IntegrationRequestMessage
      {
        ActionerId = actionerId,
        Culture = context.CurrentRequest.UserContext.Culture
      };

      if (user.UserType == UserTypes.Talent)
      {
        var saveEmployeeRequest = new SaveEmployeeRequest
        {
          PersonId = user.PersonId,
          UserId = actionerId
        };
        if (context.IsNotNull() && context.CurrentRequest.IsNotNull() && context.CurrentRequest.UserContext.IsNotNull())
        {
          saveEmployeeRequest.ExternalAdminId = context.CurrentRequest.UserContext.ExternalUserId;
          saveEmployeeRequest.ExternalOfficeId = context.CurrentRequest.UserContext.ExternalOfficeId;
          saveEmployeeRequest.ExternalPassword = context.CurrentRequest.UserContext.ExternalPassword;
          saveEmployeeRequest.ExternalUsername = context.CurrentRequest.UserContext.ExternalUserName;
        }

        intRequest.IntegrationPoint = IntegrationPoint.SaveEmployee;
        intRequest.SaveEmployeeRequest = saveEmployeeRequest;
        publish = true;
      }
      else if ((user.UserType & UserTypes.Career) == UserTypes.Career)
      {
        intRequest.IntegrationPoint = IntegrationPoint.SaveJobSeeker;
        var saveJobSeekerRequest = new SaveJobSeekerRequest
        {
          PersonId = user.PersonId,
          UserId = actionerId,
          IncludeSelectiveService = context.AppSettings.SelectiveServiceRegistration,
					Activities = jobSeekerActivities
        };
        if (context.IsNotNull() && context.CurrentRequest.IsNotNull() &&
                  context.CurrentRequest.UserContext.IsNotNull())
        {
          saveJobSeekerRequest.ExternalAdminId = context.CurrentRequest.UserContext.ExternalUserId;
          saveJobSeekerRequest.ExternalOfficeId = context.CurrentRequest.UserContext.ExternalOfficeId;
          saveJobSeekerRequest.ExternalUsername = context.CurrentRequest.UserContext.ExternalUserName;
          saveJobSeekerRequest.ExternalPassword = context.CurrentRequest.UserContext.ExternalPassword;
        }

        intRequest.SaveJobSeekerRequest = saveJobSeekerRequest;
        publish = true;
      }

      if (publish)
        context.Helpers.Messaging.Publish(intRequest);
    }

    /// <summary>
    /// Sends a "SaveEmployerRequest" request to any integrated client
    /// </summary>
    /// <param name="employer">The current employer entity</param>
    /// <param name="actionerId">The id of the user actioning the change to the employer</param>
    /// <param name="context">The runtime context</param>
    public static void PublishEmployerUpdateIntegrationRequest(this Employer employer, long actionerId, IRuntimeContext context)
    {
      if (context.AppSettings.IntegrationClient == IntegrationClient.Standalone)
        return;

      var saveEmployerRequest = new Focus.Core.IntegrationMessages.SaveEmployerRequest
      {
        EmployerId = employer.Id
      };
      if (employer.EmployerOfficeMappers.IsNotNullOrEmpty() &&
          employer.EmployerOfficeMappers.Any(e => e.Office.IsNotNull() && e.Office.ExternalId.IsNotNullOrEmpty()))
      {
        saveEmployerRequest.ExternalOfficeId =
          employer.EmployerOfficeMappers.First(e => e.Office.IsNotNull() && e.Office.ExternalId.IsNotNullOrEmpty())
            .Office.ExternalId;
        saveEmployerRequest.ExternalUsername = string.Format("selfreg{0}",
          saveEmployerRequest.ExternalOfficeId.ToLower());
      }

      if (context.IsNotNull() && context.CurrentRequest.IsNotNull() && context.CurrentRequest.UserContext.IsNotNull())
      {
        if (context.CurrentRequest.UserContext.ExternalUserId.IsNotNullOrEmpty())
          saveEmployerRequest.ExternalAdminId = context.CurrentRequest.UserContext.ExternalUserId;

        if (context.CurrentRequest.UserContext.ExternalOfficeId.IsNotNullOrEmpty())
          saveEmployerRequest.ExternalOfficeId = context.CurrentRequest.UserContext.ExternalOfficeId;

        if (context.CurrentRequest.UserContext.ExternalPassword.IsNotNullOrEmpty())
          saveEmployerRequest.ExternalPassword = context.CurrentRequest.UserContext.ExternalPassword;

        if (context.CurrentRequest.UserContext.ExternalUserName.IsNotNullOrEmpty())
          saveEmployerRequest.ExternalUsername = context.CurrentRequest.UserContext.ExternalUserName;
      }

      var message = new IntegrationRequestMessage
      {
        IntegrationPoint = IntegrationPoint.SaveEmployer,
        SaveEmployerRequest = saveEmployerRequest
      };
      context.Helpers.Messaging.Publish(message);
    }

    public static void PublishUpdateEmployeeStatusRequest(this User user, bool blocked, IRuntimeContext context, Employer employer)
    {
      if (context.AppSettings.IntegrationClient == IntegrationClient.Standalone)
        return;

      string externalStaffUserId = string.Empty;
      if (context.CurrentRequest.UserContext.ExternalUserId.IsNotNullOrEmpty())
        externalStaffUserId = context.CurrentRequest.UserContext.ExternalUserId;

      if (externalStaffUserId.IsNotNullOrEmpty() &&
          context.Repositories.Core.Users.Any(x => x.Id == context.CurrentRequest.UserContext.UserId))
        externalStaffUserId =
          context.Repositories.Core.Users.First(x => x.Id == context.CurrentRequest.UserContext.UserId).ExternalId;

      var updateEmployeeStatusRequest = new UpdateEmployeeStatusRequest
      {
        EmployeeUserId = user.Id,
        ExternalEmployeeUserId = user.ExternalId,
        StaffUserId = context.CurrentRequest.UserContext.UserId,
        ExternalStaffUserId = externalStaffUserId,
        Blocked = blocked
      };
      if (employer.EmployerOfficeMappers.IsNotNullOrEmpty() &&
          employer.EmployerOfficeMappers.Any(e => e.Office.IsNotNull() && e.Office.ExternalId.IsNotNullOrEmpty()))
      {
        updateEmployeeStatusRequest.ExternalOfficeId =
          employer.EmployerOfficeMappers.First(e => e.Office.IsNotNull() && e.Office.ExternalId.IsNotNullOrEmpty())
            .Office.ExternalId;
        updateEmployeeStatusRequest.ExternalUsername = string.Format("selfreg{0}",
          updateEmployeeStatusRequest.ExternalOfficeId.ToLower());
      }

      if (context.IsNotNull() && context.CurrentRequest.IsNotNull() && context.CurrentRequest.UserContext.IsNotNull())
      {
        updateEmployeeStatusRequest.ExternalAdminId = context.CurrentRequest.UserContext.ExternalUserId;
        updateEmployeeStatusRequest.ExternalOfficeId = context.CurrentRequest.UserContext.ExternalOfficeId;
        updateEmployeeStatusRequest.ExternalPassword = context.CurrentRequest.UserContext.ExternalPassword;
        updateEmployeeStatusRequest.ExternalUsername = context.CurrentRequest.UserContext.ExternalUserName;
      }

      var message = new IntegrationRequestMessage
      {
        IntegrationPoint = IntegrationPoint.UpdateEmployeeStatus,
        UpdateEmployeeStatusRequest = updateEmployeeStatusRequest
      };

      if (context.AppSettings.Module == FocusModules.Assist)
        message.UpdateEmployeeStatusRequest.ExternalStaffUserId = context.CurrentRequest.UserContext.ExternalUserId.IsNotNullOrEmpty() ? context.CurrentRequest.UserContext.ExternalUserId : context.Repositories.Core.Users.First(x => x.Id == context.CurrentRequest.UserContext.UserId).ExternalId;

      context.Helpers.Messaging.Publish(message);
    }

    #endregion
  }
}
