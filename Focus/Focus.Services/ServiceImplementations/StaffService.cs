#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using Focus.Data.Core.Entities;

using Framework.Core;
using Framework.Logging;

using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Messages.StaffService;
using Focus.Services.DtoMappers;
using Focus.Core;
using Focus.Core.Criteria;
using Focus.Services.Core;
using Focus.Services.Queries;
using Focus.Services.ServiceContracts;

#endregion

namespace Focus.Services.ServiceImplementations
{
    public class StaffService : ServiceBase, IStaffService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="StaffService" /> class.
        /// </summary>
        public StaffService()
            : this(null)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="StaffService" /> class.
        /// </summary>
        /// <param name="runtimeContext">The runtime context.</param>
        public StaffService(IRuntimeContext runtimeContext)
            : base(runtimeContext)
        { }

        /// <summary>
        /// Searches the staff.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public StaffSearchResponse SearchStaff(StaffSearchRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {

                var response = new StaffSearchResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    var query = new StaffSearchViewQuery(Repositories.Core, request.Criteria);

                    var queryOrdered = query.Query().OrderBy("LastName, FirstName, ExternalOffice");

                    response.StaffSearchViewsPaged = queryOrdered.GetPagedList(x => x.AsDto(), request.Criteria.PageIndex, request.Criteria.PageSize);
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Gets the assist user activity view.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public AssistUserActivityResponse GetAssistUserActivityView(AssistUserActivityRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new AssistUserActivityResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    var query = Repositories.Core.AssistUserActivityViews;

                    if (request.Criteria.UserId.HasValue)
                        query = query.Where(x => x.UserId == request.Criteria.UserId);
                    if (request.Criteria.DaysBack > 0)
                        query = query.Where(x => x.ActionedOn > DateTime.Now.Date.AddDays((double)request.Criteria.DaysBack * -1));

                    switch (request.Criteria.OrderBy)
                    {
                        case Constants.SortOrders.ActivityDateAsc:
                            query = query.OrderBy(x => x.ActionedOn);
                            break;

                        case Constants.SortOrders.ActivityDateDesc:
                            query = query.OrderByDescending(x => x.ActionedOn);
                            break;

                        case Constants.SortOrders.ActivityActionAsc:
                            query = query.OrderBy(x => x.ActionName);
                            break;

                        case Constants.SortOrders.ActivityActionDesc:
                            query = query.OrderByDescending(x => x.ActionName);
                            break;

                        default:
                            query = query.OrderByDescending(x => x.ActionedOn);
                            break;
                    }

                    var queryResults = query.Select(result => new AssistUserActivityViewDto
                    {
                        UserId = result.UserId,
                        ActionedOn = result.ActionedOn,
                        ActionName = result.ActionName
                    });

                    switch (request.Criteria.FetchOption)
                    {
                        case CriteriaBase.FetchOptions.PagedList:
                            response.AssistUserActivitiesPaged = new PagedList<AssistUserActivityViewDto>(queryResults, request.Criteria.PageIndex, request.Criteria.PageSize);
                            break;

                        case CriteriaBase.FetchOptions.List:
                            response.AssistUserActivitiesList = new List<AssistUserActivityViewDto>(queryResults);
                            break;

                        default:
                            response.AssistUserActivitiesPaged = new PagedList<AssistUserActivityViewDto>(queryResults, request.Criteria.PageIndex, request.Criteria.PageSize);
                            break;
                    }
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }
                return response;
            }
        }

        /// <summary>
        /// Gets the assist user employers assisted view.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public AssistUserEmployersAssistedResponse GetAssistUserEmployersAssistedView(AssistUserEmployersAssistedRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new AssistUserEmployersAssistedResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    var query = Repositories.Core.AssistUserEmployersAssistedViews;

                    if (request.Criteria.UserId.HasValue)
                        query = query.Where(x => x.UserId == request.Criteria.UserId);
                    if (request.Criteria.DaysBack > 0)
                        query = query.Where(x => x.ActionedOn > DateTime.Now.Date.AddDays((double)request.Criteria.DaysBack * -1));

                    switch (request.Criteria.OrderBy)
                    {
                        case Constants.SortOrders.ActivityDateAsc:
                            query = query.OrderBy(x => x.ActionedOn);
                            break;
                        case Constants.SortOrders.ActivityDateDesc:
                            query = query.OrderByDescending(x => x.ActionedOn);
                            break;
                        case Constants.SortOrders.NameAsc:
                            query = query.OrderBy(x => x.Name);
                            break;
                        case Constants.SortOrders.NameDesc:
                            query = query.OrderByDescending(x => x.Name);
                            break;
                        case Constants.SortOrders.BusinessUnitNameAsc:
                            query = query.OrderBy(x => x.BusinessUnitName);
                            break;
                        case Constants.SortOrders.BusinessUnitNameDesc:
                            query = query.OrderByDescending(x => x.BusinessUnitName);
                            break;
                        case Constants.SortOrders.ActivityActionAsc:
                            query = query.OrderBy(x => x.ActionName);
                            break;
                        case Constants.SortOrders.ActivityActionDesc:
                            query = query.OrderByDescending(x => x.ActionName);
                            break;
                        default:
                            query = query.OrderBy(x => x.BusinessUnitName);
                            break;
                    }

                    var queryResults = query.Select(result => new AssistUserEmployersAssistedViewDto
                    {
                        UserId = result.UserId,
                        BusinessUnitName = result.BusinessUnitName,
                        Name = result.Name,
                        ActionedOn = result.ActionedOn,
                        ActionName = result.ActionName,
                        JobTitle = result.JobTitle,
                        AdditionalDetails = result.AdditionalDetails,
                        SavedSearchName = result.SavedSearchName
                    });

                    switch (request.Criteria.FetchOption)
                    {
                        case CriteriaBase.FetchOptions.PagedList:
                            response.AssistUserEmployersAssistedViewsPaged = new PagedList<AssistUserEmployersAssistedViewDto>(queryResults, request.Criteria.PageIndex, request.Criteria.PageSize);
                            break;
                        case CriteriaBase.FetchOptions.List:
                            response.AssistUserEmployersAssistedViews = new List<AssistUserEmployersAssistedViewDto>(queryResults);
                            break;
                        default:
                            response.AssistUserEmployersAssistedViewsPaged = new PagedList<AssistUserEmployersAssistedViewDto>(queryResults, request.Criteria.PageIndex, request.Criteria.PageSize);
                            break;
                    }
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }
                return response;
            }
        }

        /// <summary>
        /// Gets the assist user employers assisted view.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public AssistUserJobSeekersAssistedResponse GetAssistUserJobSeekersAssistedView(AssistUserJobSeekersAssistedRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new AssistUserJobSeekersAssistedResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    var query = Repositories.Core.AssistUserJobSeekersAssistedViews;

                    if (request.Criteria.UserId.HasValue)
                        query = query.Where(x => x.UserId == request.Criteria.UserId);
                    if (request.Criteria.DaysBack > 0)
                        query = query.Where(x => x.ActionedOn > DateTime.Now.Date.AddDays((double)request.Criteria.DaysBack * -1));

                    switch (request.Criteria.OrderBy)
                    {
                        case Constants.SortOrders.ActivityDateAsc:
                            query = query.OrderBy(x => x.ActionedOn);
                            break;
                        case Constants.SortOrders.ActivityDateDesc:
                            query = query.OrderByDescending(x => x.ActionedOn);
                            break;
                        case Constants.SortOrders.NameAsc:
                            query = query.OrderBy(x => x.Name);
                            break;
                        case Constants.SortOrders.NameDesc:
                            query = query.OrderByDescending(x => x.Name);
                            break;
                        case Constants.SortOrders.ActivityActionAsc:
                            query = query.OrderBy(x => x.ActionName);
                            break;
                        case Constants.SortOrders.ActivityActionDesc:
                            query = query.OrderByDescending(x => x.ActionName);
                            break;
                        default:
                            query = query.OrderBy(x => x.Name);
                            break;
                    }

                    var queryResults = query.Select(result => new AssistUserJobSeekersAssistedViewDto
                    {
                        UserId = result.UserId,
                        Name = result.Name,
                        ActionedOn = result.ActionedOn,
                        ActionName = result.ActionName
                    });

                    switch (request.Criteria.FetchOption)
                    {
                        case CriteriaBase.FetchOptions.PagedList:
                            response.AssistUserJobSeekersAssistedViewsPaged = new PagedList<AssistUserJobSeekersAssistedViewDto>(queryResults, request.Criteria.PageIndex, request.Criteria.PageSize);
                            break;
                        case CriteriaBase.FetchOptions.List:
                            response.AssistUserJobSeekersAssistedViews = new List<AssistUserJobSeekersAssistedViewDto>(queryResults);
                            break;
                        default:
                            response.AssistUserJobSeekersAssistedViewsPaged = new PagedList<AssistUserJobSeekersAssistedViewDto>(queryResults, request.Criteria.PageIndex, request.Criteria.PageSize);
                            break;
                    }
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }
                return response;
            }
        }

        /// <summary>
        /// Gets the staff profile.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public StaffInfoResponse GetStaffProfile(StaffInfoRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new StaffInfoResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    var user = Repositories.Core.FindById<User>(request.Id);
                    var person = user.Person;

                    response.Person = person.AsDto();
                    response.PersonAddresses = person.PersonAddresses.Select(pa => pa.AsDto()).ToList();
                    response.PhoneNumbers = person.PhoneNumbers.Select(pn => pn.AsDto()).ToList();

                    var userActionEvents = Repositories.Core.ActionEvents.Where(x => x.UserId == request.Id);

                    // Logins Panel Data
                    var userLogins = Repositories.Core.ActionEvents.Where(x => x.EntityId == request.Id);
                    response.Logins7Days = CountActionsByActionType(ActionTypes.LogIn, userLogins, 7);
                    response.LastLogin = user.LoggedInOn;

                    // Activity Summary Panel
                    response.ActivityReferrals7Days = CountActionsByActionType(ActionTypes.CreateCandidateApplication, userActionEvents, 7);
                    response.ActivityReferrals30Days = CountActionsByActionType(ActionTypes.CreateCandidateApplication, userActionEvents, 30);

                    // TODO: count resumes created

                    response.ActivityResumesEdited7Days = CountActionsByActionType(ActionTypes.ChangeResume, userActionEvents, 7);
                    response.ActivityResumesEdited30Days = CountActionsByActionType(ActionTypes.ChangeResume, userActionEvents, 30);

                    // TODO: count jobseeker assisted

                    response.ActivityJobSeekersAssisted7Days = CountActionsByActionType(ActionTypes.AccessCandidateAccount, userActionEvents, 7);
                    response.ActivityJobSeekersAssisted30Days = CountActionsByActionType(ActionTypes.AccessCandidateAccount, userActionEvents, 30);

                    // TODO: count jobseeker unsubscribed

                    response.ActivityEmployerAccountsCreated7Days = CountActionsByActionType(ActionTypes.RegisterAccount, userActionEvents, 7);
                    response.ActivityEmployerAccountsCreated30Days = CountActionsByActionType(ActionTypes.RegisterAccount, userActionEvents, 30);

                    // when employer assists - single sign on action recorded
                    response.ActivityEmployerAccountsAssisted7Days = CountActionsByActionType(ActionTypes.ValidateSingleSignOn, userActionEvents, 7);
                    response.ActivityEmployerAccountsAssisted30Days = CountActionsByActionType(ActionTypes.ValidateSingleSignOn, userActionEvents, 30);

                    response.ActivityJobOrdersCreated7Days = CountActionsByActionType(ActionTypes.CreateJob, userActionEvents, 7);
                    response.ActivityJobOrdersCreated30Days = CountActionsByActionType(ActionTypes.CreateJob, userActionEvents, 30);

                    response.ActivityJobOrdersEdited7Days = CountActionsByActionType(ActionTypes.EditJob, userActionEvents, 7);
                    response.ActivityJobOrdersEdited30Days = CountActionsByActionType(ActionTypes.EditJob, userActionEvents, 30);

                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Gets the staffs backdate settings.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public StaffBackdateSettingsResponse GetStaffBackdateSettings(StaffBackdateSettingsRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new StaffBackdateSettingsResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    var staffBackdateSettings = Repositories.Core.Find<PersonActivityUpdateSettings>(x => x.PersonId == request.PersonId);

                    if (staffBackdateSettings.Count > 0)
                        response.StaffBackdateSettings = staffBackdateSettings.First<PersonActivityUpdateSettings>().AsDto();
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Sets the staffs Backdate settings.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public SaveStaffBackdateSettingResponse UpdateStaffBackdateSettings(SaveStaffBackdateSettingRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new SaveStaffBackdateSettingResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    //change dto to entity
                    var entity = request.StaffBackdateSettings.CopyTo();

                    //remove the old settings if exist.
                    if (Repositories.Core.Exists<PersonActivityUpdateSettings>(x => x.PersonId == request.StaffBackdateSettings.PersonId))
                        Repositories.Core.Remove<PersonActivityUpdateSettings>(x => x.PersonId == request.StaffBackdateSettings.PersonId);

                    //add the new settings
                    Repositories.Core.Add<PersonActivityUpdateSettings>(entity);
                    Repositories.Core.SaveChanges();

                    response.isUpdated = true;

                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Creates the staff homepage alert.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public CreateStaffHomepageAlertResponse CreateStaffHomepageAlert(CreateStaffHomepageAlertRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new CreateStaffHomepageAlertResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    var person = Repositories.Core.FindById<Person>(request.UserContext.PersonId);

                    if (request.HomepageAlert.AlertForAllStaff)
                    {
                        CreateAlertMessage(Constants.AlertMessageKeys.StaffMessage, request.HomepageAlert.IsSystemAlert, null, request.HomepageAlert.ExpiryDate,
                            MessageAudiences.AllAssistUsers, MessageTypes.General, null,
                            request.HomepageAlert.IsSystemAlert ? LocaliseOrDefault(request, "System.Text", "System") : person.FirstName,
                            request.HomepageAlert.IsSystemAlert ? LocaliseOrDefault(request, "Administrator.Text", "Administrator") : person.LastName,
                            LocaliseOrDefault(request, "ReminderCreatedOnDate.Format", "{0:MMM d, yyyy}", DateTime.Now), request.HomepageAlert.Message);
                    }

                    LogAction(request, ActionTypes.CreateStaffHomepageAlerts, null);
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.CreateStaffHomepageAlertsFailure), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }
    }
}