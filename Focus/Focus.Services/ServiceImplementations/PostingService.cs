﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;

using Focus.Core;
using Focus.Core.Criteria.CandidateSearch;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Messages.OrganizationService;
using Focus.Core.Messages.PostingService;
using Focus.Core.Models.Assist;
using Focus.Core.Models.Career;
using Focus.Data.Core.Entities;
using Focus.Services.DtoMappers;
using Focus.Services.Mappers;
using Focus.Services.Core;
using Focus.Services.ServiceContracts;
using Focus.Services.Core.Posting;
using AcknowledgementType = Focus.Core.Messages.AcknowledgementType;

using Framework.Core;
using Framework.Logging;

#endregion

namespace Focus.Services.ServiceImplementations
{
	public class PostingService : ServiceBase, IPostingService
	{
    /// <summary>
		/// Initializes a new instance of the <see cref="PostingService" /> class.
		/// </summary>
		public PostingService() : this(null)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="PostingService" /> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		public PostingService(IRuntimeContext runtimeContext) : base(runtimeContext)
		{ }
		
		/// <summary>
		/// Gets the posting.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public PostingResponse FetchPosting(PostingRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new PostingResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.ClientTag | Validate.AccessToken))
					return response;

				try
				{
                    bool underage = false;
					if (request.LensPostingId.IsNullOrEmpty())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.LensPostingIdNotProvided));
						Logger.Info(request.LogData(), "No lens posting id provided.");
						return response;
					}

					var postingDto = GetPosting(RuntimeContext, request.LensPostingId, true);

                    if (AppSettings.UnderAgeJobSeekerRestrictionThreshold > 0 && AppSettings.Module == FocusModules.CareerExplorer && postingDto.OriginId != 999)
                    {
                        var year = Repositories.Core.Persons.Where(x => x.Id == request.UserContext.PersonId).Select(x => x.DateOfBirth.Value.Year).FirstOrDefault();

                        if (DateTime.Now.Year - year < AppSettings.UnderAgeJobSeekerRestrictionThreshold)
                        {
                            var job = Repositories.Core.Jobs.FirstOrDefault(x => x.Id == postingDto.JobId);
                            var postingGeneratorModel = new PostingGeneratorModel
                            {
                                JobId = job.Id,
                                Job = job.AsDto(),
                                BusinessUnit = job.BusinessUnit.IsNotNull() ? job.BusinessUnit.AsDto() : null,
                                BusinessUnitDescription = job.BusinessUnitDescription.IsNotNull() ? job.BusinessUnitDescription.AsDto() : null,
                                JobLicences =  job.JobLicences.Select(license => license.AsDto()).ToList(),
                                JobCertificates =  job.JobCertificates.Select(certificate => certificate.AsDto()).ToList(),
                                JobLanguages =  job.JobLanguages.Select(language => language.AsDto()).ToList(),
                                JobSpecialRequirements = job.JobSpecialRequirements.Select(requirement => requirement.AsDto()).ToList(),
                                JobLocations = null,
                                JobProgramsOfStudy = job.JobProgramOfStudies.Select(programOfStudy => programOfStudy.AsDto()).ToList(),
                                JobDrivingLicenceEndorsements = job.JobDrivingLicenceEndorsements.Select(drivingLicenceEndorsement => drivingLicenceEndorsement.AsDto()).ToList()
                            };
                            postingDto.Html = new PostingGenerator(postingGeneratorModel, request.UserContext.Culture, RuntimeContext.AppSettings, RuntimeContext.Helpers.Localisation, RuntimeContext.Helpers.Lookup).Generate();
                        }

                    }

					if (postingDto.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.PostingNotFound));
						Logger.Info(request.LogData(), string.Format("The Posting '{0}' was not found.", request.LensPostingId));

						return response;
					}

					if (AppSettings.NotClickingLeadsEnabled && request.LogViewAction)
					{
						LogAction(request, ActionTypes.ViewedPostingFromSearch, "Posting", postingDto.Id, request.UserContext.UserId);
					}
					
					response.PostingDetails = postingDto.ToPostingEnvelope();
					response.PostingId = postingDto.Id;

					response.Acknowledgement = AcknowledgementType.Success;
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}


    /// <summary>
    /// Gets the posting's BGTOCCs
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    public PostingResponse GetPostingBGTOccs(PostingRequest request)
    {
      using (Profiler.Profile(request.LogData(), request))
      {
        var response = new PostingResponse(request);

        // Validate request
        if (!ValidateRequest(request, response, Validate.ClientTag | Validate.AccessToken))
          return response;

        try
        {
          if (request.LensPostingId.IsNullOrEmpty())
          {
            response.SetFailure(FormatErrorMessage(request, ErrorTypes.LensPostingIdNotProvided));
            Logger.Info(request.LogData(), "No lens posting id provided.");
            return response;
          }

					var postingDto = GetPosting(RuntimeContext, request.LensPostingId);

          if (postingDto.IsNull())
          {
            response.SetFailure(FormatErrorMessage(request, ErrorTypes.PostingNotFound));
            Logger.Info(request.LogData(), string.Format("The Posting '{0}' was not found.", request.LensPostingId));

            return response;
          }

          if (postingDto.JobId.IsNotNull())
          {
            var jobROnetId = Repositories.Core.Jobs.Where(x => x.Id == postingDto.JobId).Select(x => x.ROnetId).SingleOrDefault();
            
						if (jobROnetId.IsNotNull())
							response.PostingBGTOcc = Repositories.Library.ROnets.Where(x => x.Id == jobROnetId).Select(x => x.Code).SingleOrDefault();
          }

          response.Acknowledgement = AcknowledgementType.Success;
        }
        catch (Exception ex)
        {
          response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
          Logger.Error(request.LogData(), response.Message, response.Exception);
        }

        return response;
      }
    }

		/// <summary>
		/// Gets the posting id.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public PostingResponse GetPostingId(PostingRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new PostingResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.ClientTag | Validate.AccessToken))
					return response;

				try
				{
					if (request.LensPostingId.IsNullOrEmpty())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.LensPostingIdNotProvided));
						Logger.Info(request.LogData(), "No lens posting id provided.");
						return response;
					}

					var postingDto = GetPosting(RuntimeContext, request.LensPostingId);

					if (postingDto.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.PostingNotFound));
						Logger.Info(request.LogData(), string.Format("The Posting '{0}' was not found.", request.LensPostingId));

						return response;
					}

					if (AppSettings.NotClickingLeadsEnabled && request.LogViewAction)
					{
						LogAction(request, ActionTypes.ViewedPostingFromSearch, "Posting", postingDto.Id);
					}

					response.PostingId = postingDto.Id;

					response.Acknowledgement = AcknowledgementType.Success;
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Closes the posting.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public ClosePostingResponse ClosePosting(ClosePostingRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new ClosePostingResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					if (request.LensPostingId.IsNullOrEmpty())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.LensPostingIdNotProvided));
						Logger.Info(request.LogData(), "The no lens posting id provided.");
						return response;
					}

					var posting = Repositories.Core.Postings.FirstOrDefault(x => x.LensPostingId == request.LensPostingId);

					if(posting.IsNotNull())
					{
						posting.StatusId = PostingStatuses.Closed;
						Repositories.Core.SaveChanges(true);
					}

					// TODO: Martha (Low) - Should we also deregister the job in lens when closing the posting, current version of lens doesn't but it may be useful to do this
					response.Acknowledgement = AcknowledgementType.Success;

					if(posting.IsNotNull())
						LogAction(request, ActionTypes.ClosePosting, posting);
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets job feedback.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public TypicalResumesResponse GetTypicalResumes(TypicalResumesRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new TypicalResumesResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					// TODO: Martha (Medium) - Get typical resumes for a job (used on Am I a good match) Complete Service Code - I think this will use the same tables as are going to be added to Assist for it's typical resumes functionality
					response.TypicalResumes = new List<ResumeInfo>
					                          	{
					                          		new ResumeInfo
					                          			{
					                          				CurrentJob = "Driver",
																						PreviousJob = "Truck Driver Class A CDL, Certified Driver Trainer",
																						YearsOfExperience = 30,
																						DegreeLevel = "Diploma",
																						Skills = new List<string>{"Skill1", "Skill2"},
																						Certificates = new List<string>{"Certificate1", "Certificate2"}
					                          			},
																				new ResumeInfo
					                          			{
					                          				CurrentJob = "Job1",
																						PreviousJob = "Job2",
																						YearsOfExperience = 30,
																						DegreeLevel = "Diploma",
																						Skills = new List<string>{"Skill2", "Skill3"},
																						Certificates = new List<string>{"Certificate3", "Certificate4"}
					                          			}
					                          	};

					response.Acknowledgement = AcknowledgementType.Success;
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the viewed postings.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public ViewedPostingResponse GetViewedPostings(ViewedPostingRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new ViewedPostingResponse(request);

				// Validate request.
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
          var viewedPostings = (from vp in Repositories.Core.ViewedPostings
                                join p in Repositories.Core.Postings
                                   on vp.LensPostingId equals p.LensPostingId
                                into postingJoin
                                from op in postingJoin.DefaultIfEmpty()
                                where vp.UserId == request.UserId
                                select new
                                {
                                  ViewedPosting = vp,
                                  Posting = op
                                }).OrderByDescending(x => x.ViewedPosting.ViewedOn).ToList();

          var viewedPostingsModels = new List<ViewedPostingModel>();

          viewedPostings.ForEach(viewedPosting =>
          {
            var employerName = string.Empty;
            var jobTitle = string.Empty;

            if (viewedPosting.Posting.IsNull())
            {
              try
              {
                var postingDocumentText = Repositories.Document.GetDocumentText(viewedPosting.ViewedPosting.LensPostingId);
								var postingDto = postingDocumentText.ToPostingDto(viewedPosting.ViewedPosting.LensPostingId, AppSettings.JobPostingStylePath, AppSettings.SetExternalIdOnSpideredPostings);
                Repositories.Core.Add(postingDto.CopyTo());

                employerName = postingDto.EmployerName;
                jobTitle = postingDto.JobTitle;
              }
              catch (Exception ex)
              {
                if (ex.Message.StartsWith("Error getting document text from document store"))
                  Repositories.Core.Remove(viewedPosting.ViewedPosting);
                else
                  throw;
              }
            }
            else
            {
              employerName = viewedPosting.Posting.EmployerName;
              jobTitle = viewedPosting.Posting.JobTitle;
            }

            if (jobTitle.Length > 0)
            {

              viewedPostingsModels.Add(new ViewedPostingModel
              {
                EmployerName = employerName,
                LensPostingId = viewedPosting.ViewedPosting.LensPostingId,
                JobTitle = jobTitle,
                ViewedOn = viewedPosting.ViewedPosting.ViewedOn,
								FocusJobId = (viewedPosting.Posting.IsNotNull()) ? viewedPosting.Posting.JobId : null
              });
            }
          });

					Repositories.Core.SaveChanges(true);

					if (RuntimeContext.CurrentRequest.UserContext.IsShadowingUser)
					{
						var talentPostings = viewedPostingsModels.Where(vp => vp.FocusJobId.IsNotNullOrZero()).ToList();
						var jobIds = talentPostings.Select(vp => vp.FocusJobId.GetValueOrDefault(0)).ToArray();
						var employerNames = (from j in Repositories.Core.Jobs
						                     join bu in Repositories.Core.BusinessUnits
							                     on j.BusinessUnitId equals bu.Id
																 where jobIds.Contains(j.Id)
						                     select new
 							                   {
								                  JobId = j.Id,
								                  EmployerName = bu.Name
							                  }).ToDictionary(p => p.JobId, p => p.EmployerName);

						talentPostings.ForEach(tp =>
						{
							var jobId = tp.FocusJobId.GetValueOrDefault(0);
							if (employerNames.ContainsKey(jobId))
								tp.EmployerName = employerNames[jobId];
						});
					}
					else if (AppSettings.ConfidentialEmployersEnabled || AppSettings.ConfidentialEmployersDefault)
					{
						var approvalStatuses = new List<ApprovalStatuses>
						{
							ApprovalStatuses.Approved,
							ApprovalStatuses.None
						};
						var personId = Repositories.Core.Users.Where(u => u.Id == request.UserId).Select(u => u.PersonId).FirstOrDefault();
						var referralQuery = Repositories.Core.ReferralViews
																						 .Where(referral => referral.CandidateId == personId && approvalStatuses.Contains(referral.ApplicationApprovalStatus))
																						 .Select(referral => referral.AsDto());

						var referrals = new Dictionary<string, ReferralViewDto>();
						foreach (var referral in referralQuery)
						{
							if (!referrals.ContainsKey(referral.LensPostingId))
								referrals.Add(referral.LensPostingId, referral);
						}

						if (referrals.Any())
						{
							viewedPostingsModels.ForEach(posting =>
							{
								if (referrals.ContainsKey(posting.LensPostingId) && referrals[posting.LensPostingId].EmployerName.IsNotNullOrEmpty())
								{
									posting.EmployerName = referrals[posting.LensPostingId].EmployerName;
								}
							});
						}
					}

          response.ViewedPostings = viewedPostingsModels;
          response.Acknowledgement = AcknowledgementType.Success;
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Adds the viewed postings.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public AddViewedPostingResponse AddViewedPostings(AddViewedPostingRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new AddViewedPostingResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var viewedPosting = new ViewedPosting();

					// Find if this user/posting combination has already been saved so that it isn't added again.
					viewedPosting = Repositories.Core.ViewedPostings.FirstOrDefault(x => x.UserId == request.ViewedPosting.UserId && x.LensPostingId == request.ViewedPosting.LensPostingId);

					if (viewedPosting.IsNull())
					{
						viewedPosting = request.ViewedPosting.CopyTo();
						Repositories.Core.Add(viewedPosting);
					}
					else
					{
						viewedPosting.ViewedOn = request.ViewedPosting.ViewedOn;
					}

					Repositories.Core.SaveChanges(true);

					var viewedPostings = Repositories.Core.ViewedPostings.Where(x => x.UserId == request.ViewedPosting.UserId).OrderByDescending(x => x.ViewedOn).ToList();

					var maxViewedPostings = AppSettings.MaximumRecentlyViewedPostings;

					// Remove any ViewedPostings when the maximum has been reached.
					if (viewedPostings.Count() > maxViewedPostings)
					{
						var deleteViewedPostings = viewedPostings.Skip(maxViewedPostings).ToList();

						if (deleteViewedPostings.IsNotNull() && deleteViewedPostings.Count > 0)
						{
							deleteViewedPostings.ForEach(x => Repositories.Core.Remove(x));
							Repositories.Core.SaveChanges(true);
						}
					}

					var posting = Repositories.Core.Postings.FirstOrDefault(x => x.LensPostingId == request.ViewedPosting.LensPostingId);

					// Increment the viewed count on the posting record.
					if (posting.IsNotNull())
					{
						posting.ViewedCount++;
						Repositories.Core.SaveChanges(true);
						LogAction(request, ActionTypes.EditPostingViewedCount, posting);
					}

					response.ViewedPosting = viewedPosting.AsDto();

					LogAction(request, ActionTypes.AddViewedPosting, viewedPosting);
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the job id from posting.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public GetJobIdFromPostingResponse GetJobIdFromPosting(GetJobIdFromPostingRequest request)
	  {
	    using (Profiler.Profile(request.LogData(), request))
	    {
        var response = new GetJobIdFromPostingResponse(request);

	      // Validate request
	      if (!ValidateRequest(request, response, Validate.All))
	        return response;

	      try
	      {
          response.JobId = Repositories.Core.Postings.Where(x => x.LensPostingId == request.LensPostingId).Select(x => x.JobId).SingleOrDefault();
	      }
        catch (Exception ex)
        {
          response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
          Logger.Error(request.LogData(), response.Message, response.Exception);
        }

	      return response;
	    }
	  }

		/// <summary>
		/// Gets the compare posting to resume modal model.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public GetComparePostingToResumeModalModelResponse GetComparePostingToResumeModalModel(GetComparePostingToResumeModalModelRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new GetComparePostingToResumeModalModelResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					if (request.LensPostingId.IsNullOrEmpty())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.LensPostingIdNotProvided));
						Logger.Info(request.LogData(), "No Lens posting id provided.");
						return response;
					}

					var postingDto = GetPosting(RuntimeContext, request.LensPostingId);

					if (postingDto.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.PostingNotFound));
						Logger.Info(request.LogData(), string.Format("The Posting '{0}' was not found.", request.LensPostingId));

						return response;
					}
					
					var matchedPersons = new List<ComparePostingToResumePersonModel>();

					if (!request.DoLiveSearch)
					{

						var rowCount = 0;

						matchedPersons = Repositories.Core.GetPostingPersonMatches(postingDto.Id.GetValueOrDefault(), request.UserContext.UserId, 1, 6, ref rowCount).Select(x => new ComparePostingToResumePersonModel
						                                                                                                                                              	{
						                                                                                                                                              		Id = x.PersonId.GetValueOrDefault(),
						                                                                                                                                              		Name = x.FirstName + " " + x.LastName,
						                                                                                                                                              		MatchScore = x.Score.GetValueOrDefault()
						                                                                                                                                              	}).ToList();
					}
					else
					{
						var criteria = new CandidateSearchCriteria
						               	{
						               		SearchType = CandidateSearchTypes.ByJobDescription,
						               		ScopeType = CandidateSearchScopeTypes.Open,
						               		JobDetailsCriteria = new CandidateSearchJobDetailsCriteria {Posting = postingDto.Html},
						               		MinimumScore = AppSettings.StarRatingMinimumScores[AppSettings.JobDevelopmentSpideredJobSearchMatchesMinimumStars],
															MinimumDocumentCount = 10
						               	};

						var searchResult = Helpers.Search.SearchResumes(criteria, Guid.Empty);

						if(searchResult.IsNotNull() && searchResult.Resumes.IsNotNullOrEmpty())
						{
							matchedPersons.AddRange(searchResult.Resumes.Select(resume => new ComparePostingToResumePersonModel
							                                                              	{
							                                                              		Id = resume.Id, 
																																								Name = resume.Name, 
																																								MatchScore = resume.Score
							                                                              	}));
						}
					}

					response.Model = new ComparePostingToResumeModalModel
					                 	{
															PostingId = postingDto.Id.GetValueOrDefault(),
															LensPostingId = postingDto.LensPostingId,
															PostingJobTitle = postingDto.JobTitle,
															PostingHtml = postingDto.Html,
															MatchedPersons = matchedPersons
					                 	};
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the posting employer contact model.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public GetPostingEmployerContactModelResponse GetPostingEmployerContactModel(GetPostingEmployerContactModelRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new GetPostingEmployerContactModelResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					if (request.PostingId.IsNull() || request.PostingId <= 0)
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.PostingIdNotProvided));
						Logger.Info(request.LogData(), "The no posting id provided.");
						return response;
					}

					var posting = Repositories.Core.FindById<Posting>(request.PostingId);

					if (posting.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.PostingNotFound));
						Logger.Info(request.LogData(), string.Format("The posting '{0}' was not found.", request.PostingId));
						return response;
					}

					PostingDto postingDto; 

					// If we do not have a local store of the posting xml get it from the document repository
					if (posting.PostingXml.IsNullOrEmpty())
					{
						var postingDocumentText = Repositories.Document.GetDocumentText(posting.LensPostingId);

						postingDto = postingDocumentText.ToPostingDto(posting.LensPostingId, AppSettings.JobPostingStylePath, AppSettings.SetExternalIdOnSpideredPostings);

						postingDto.CopyTo(posting);

						Repositories.Core.SaveChanges();
					}
					else
					{
						postingDto = posting.AsDto();
					}

					if(postingDto.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.PostingNotFound));
						Logger.Info(request.LogData(), string.Format("The posting '{0}' was not found in the document store.", request.PostingId));
						return response; 
					}

					var model = new PostingEmployerContactModel();

					if(request.PersonId.IsNotNull())
					{
						var person = Repositories.Core.Persons.Where(x => x.Id == request.PersonId).Select(x => new {x.FirstName, x.LastName}).FirstOrDefault();

						if (person.IsNotNull())
							model.JobSeekerName = string.Format("{0} {1}", person.FirstName, person.LastName);
						else
						{
							response.SetFailure(FormatErrorMessage(request, ErrorTypes.PersonNotFound));
							Logger.Info(request.LogData(), string.Format("The person'{0}' was not found.", request.PersonId));
							return response; 
						}
					}

					model.PostingContactDetails = postingDto.GetContactDetails();

					response.Model = model;
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the spidered postings.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public SpideredPostingResponse GetSpideredPostings(SpideredPostingRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new SpideredPostingResponse(request);

				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					response.SpideredPostingsPaged = Repositories.LiveJobs.GetPostings(request.Criteria);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the spidered postings with good matches.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public SpideredPostingResponse GetSpideredPostingsWithGoodMatches(SpideredPostingRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new SpideredPostingResponse(request);

				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var rowCount = 0;

					var employerName = (request.Criteria.EmployerName.IsNotNullOrEmpty()) ? request.Criteria.EmployerName : "";
					var minimumDate = (request.Criteria.PostingAge > 0) ? DateTime.Today.AddDays(1 - request.Criteria.PostingAge.GetValueOrDefault()) : DateTime.Today.AddDays(-30);

					var pageNumber = request.Criteria.PageIndex + 1;

					IQueryable<GetSpideredPostingsWithGoodMatchesResult> spideredPostings;

					switch (request.Criteria.OrderBy)
					{
						case Constants.SortOrders.JobTitleAsc:
							spideredPostings = Repositories.Core.GetSpideredPostingsWithGoodMatchesOrderByJobTitleAsc(request.UserContext.UserId, employerName, minimumDate, pageNumber, request.Criteria.PageSize, ref rowCount);
							break;
						case Constants.SortOrders.JobTitleDesc:
							spideredPostings = Repositories.Core.GetSpideredPostingsWithGoodMatchesOrderByJobTitleDesc(request.UserContext.UserId, employerName, minimumDate, pageNumber, request.Criteria.PageSize, ref rowCount);
							break;
						case Constants.SortOrders.PostingDateAsc:
							spideredPostings = Repositories.Core.GetSpideredPostingsWithGoodMatchesOrderByPostingDateAsc(request.UserContext.UserId, employerName, minimumDate, pageNumber, request.Criteria.PageSize, ref rowCount);
							break;
						default:
							spideredPostings = Repositories.Core.GetSpideredPostingsWithGoodMatchesOrderByPostingDateDesc(request.UserContext.UserId, employerName, minimumDate, pageNumber, request.Criteria.PageSize, ref rowCount);
							break;
					}

					var spideredPostingModels = new PagedList<SpideredPostingModel>();
					spideredPostingModels.AddRange(spideredPostings.Select(spideredEmployer => spideredEmployer.ToSpideredPostingModel()));

					response.SpideredPostingsPaged = new PagedList<SpideredPostingModel>(spideredPostingModels, rowCount, request.Criteria.PageIndex, request.Criteria.PageSize, false);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		#region Private methods

		/// <summary>
		/// Gets the posting.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <param name="lensPostingId">The lens posting id.</param>
		/// <param name="checkUrl">Whether to check the URL is valid (and if not, append http:// to it)</param>
		/// <param name="isForSearch">If this is for a search, don't error if job is not in the doc store</param>
		/// <returns></returns>
		internal static PostingDto GetPosting(IRuntimeContext runtimeContext, string lensPostingId, bool checkUrl = false, bool isForSearch = false)
		{
			// Try to find latest posting by lens posting id in our database
			var postingDto = runtimeContext.Repositories.Core.Postings.Where(x => x.LensPostingId == lensPostingId).Select(x => x.AsDto()).FirstOrDefault();

			// If there is no posting xml get it from doc server and save it to database
			if (postingDto.IsNull() || postingDto.PostingXml.IsNullOrEmpty())
			{
				var postingId = (postingDto.IsNotNull()) ? postingDto.Id : null;
				postingDto = UpdatePostingFromDocumentStore(runtimeContext, lensPostingId, postingId, isForSearch);
			}

			// This is a short-term fix. See FVN-2481
			if (checkUrl && postingDto.IsNotNull() && postingDto.Url.IsNotNullOrEmpty())
			{
				Uri outUrl;
				if (!Uri.TryCreate(postingDto.Url, UriKind.Absolute, out outUrl))
				{
					postingDto.Url = !postingDto.Url.StartsWith("http://", StringComparison.OrdinalIgnoreCase)
						? string.Concat("http://", postingDto.Url)
						: string.Empty;
				}
			}

			return postingDto;
		}

		/// <summary>
		/// Updates the posting from document store.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <param name="lensPostingId">The lens posting id.</param>
		/// <param name="postingId">The posting id.</param>
		/// <param name="isForSearch">If this is for a search, don't error if job is not in the doc store</param>
		/// <returns></returns>
		private static PostingDto UpdatePostingFromDocumentStore(IRuntimeContext runtimeContext, string lensPostingId, long? postingId, bool isForSearch = false)
		{
			var postingDocumentText = runtimeContext.Repositories.Document.GetDocumentText(lensPostingId);
			if (postingDocumentText.IsNullOrEmpty() && isForSearch)
				return null;

			var postingDto = postingDocumentText.ToPostingDto(lensPostingId, runtimeContext.AppSettings.JobPostingStylePath, runtimeContext.AppSettings.SetExternalIdOnSpideredPostings);

			// Check if the posting has been added while we were getting the document from the document store
			if (!postingId.HasValue || postingId == 0)
				postingId = runtimeContext.Repositories.Core.Postings.Where(x => x.LensPostingId == lensPostingId).Select(x => x.Id).FirstOrDefault();

			Posting posting = null;

			if (postingId.HasValue && postingId > 0)
			{
				var existingPosting = runtimeContext.Repositories.Core.FindById<Posting>(postingId);

				if (existingPosting.IsNotNull())
				{
					var viewCount = existingPosting.ViewedCount;
					posting = postingDto.CopyTo(existingPosting);

					//repopulate the viewed count
					posting.ViewedCount = viewCount;
				}
			}

			if (posting.IsNull() || posting.Id == 0)
			{
				posting = postingDto.CopyTo();
				runtimeContext.Repositories.Core.Add(posting);
			}

			runtimeContext.Repositories.Core.SaveChanges();

			return (posting.IsNotNull()) ? posting.AsDto() : null;
		}

		#endregion
 
	}
}
