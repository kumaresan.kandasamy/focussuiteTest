﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;

using Framework.Core;
using Framework.Caching;
using Framework.Logging;

using Focus.Core;
using Focus.Core.Criteria;
using Focus.Core.Criteria.Explorer;
using Focus.Core.DataTransferObjects.FocusExplorer;
using Focus.Core.Messages;
using Focus.Core.Messages.ExplorerService;
using Focus.Core.Models;
using Focus.Core.Views;
using Focus.Data.Core.Entities;
using Focus.Data.Library.Entities;
using Focus.Services.Core;
using Focus.Services.Core.Onet;
using Focus.Services.DtoMappers;
using Focus.Services.ServiceContracts;

using EmployerDto = Focus.Core.DataTransferObjects.FocusExplorer.EmployerDto;
using EmployerJobRequest = Focus.Core.Messages.ExplorerService.EmployerJobRequest;
using EmployerJobResponse = Focus.Core.Messages.ExplorerService.EmployerJobResponse;
using JobDto = Focus.Core.DataTransferObjects.FocusExplorer.JobDto;
using JobTitleDto = Focus.Core.DataTransferObjects.FocusExplorer.JobTitleDto;

#endregion

namespace Focus.Services.ServiceImplementations
{
	public class ExplorerService : ServiceBase, IExplorerService
	{
		private OnetToROnet _onetToROnet;
		private Validate _validationLevel;

		/// <summary>
		/// Initializes a new instance of the <see cref="ExplorerService" /> class.
		/// </summary>
		public ExplorerService()
			: this(null)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="ExplorerService" /> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		public ExplorerService(IRuntimeContext runtimeContext)
			: base(runtimeContext)
		{
			SetValidationLevel();
		}

		/// <summary>
		/// Gets the onet to R onet.
		/// </summary>
		protected OnetToROnet OnetToROnet
		{
			get { return _onetToROnet ?? (_onetToROnet = new OnetToROnet(RuntimeContext)); }
		}

		/// <summary>
		/// Sets the validation level.
		/// </summary>
		private void SetValidationLevel()
		{
			if (AppSettings.Module == FocusModules.CareerExplorer)
				_validationLevel = Validate.License | Validate.ClientTag;
			else
				_validationLevel = Validate.License | Validate.ClientTag | Validate.SessionId;
		}

		#region Service Methods

		/// <summary>
		/// Pings the specified request.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public PingResponse Ping(PingRequest request)
		{
			var response = new PingResponse(request)
			{
				Name = request.Name,
				Server = Environment.MachineName,
				Time = DateTime.UtcNow
			};

			var lensAvailable = Repositories.Lens.IsLicensed();

			if (!lensAvailable)
			{
				response.AddMessage("Lens server unavailable");
				response.Acknowledgement = AcknowledgementType.Failure;
				return response;
			}

			try
			{
				// ReSharper disable once ReturnValueOfPureMethodIsNotUsed
				Repositories.Library.Query<StateArea>().Count();
			}
			catch (Exception)
			{
				response.AddMessage("Explorer db unavailable");
				response.Acknowledgement = AcknowledgementType.Failure;
				return response;
			}

			response.Acknowledgement = AcknowledgementType.Success;
			return response;
		}

		/// <summary>
		/// Uploads a resume.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public UploadResumeResponse UploadResume(UploadResumeRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new UploadResumeResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, _validationLevel))
					return response;

				try
				{
					if (request.ResumeModel.IsNull())
						throw new Exception("No resume model");

					if (request.ResumeModel.Resume.IsNull())
						throw new Exception("No resume");

					if (request.ResumeModel.ResumeDocument.IsNull())
						throw new Exception("No resume document");

					var resumeModel = new MyResumeModel() { ResumeModel = request.ResumeModel };

					#region parse resume using lens

					var resumeXml = Repositories.Lens.ParseResume(request.ResumeModel.ResumeDocument.DocumentBytes, Path.GetExtension(request.ResumeModel.ResumeDocument.FileName), false);

					#endregion

					#region extract onet from xml, convert to ronet and get jobid

					var onet = String.Empty;
					// ReSharper disable once NotAccessedVariable
					var jobTitle = String.Empty;

					var xmlDoc = new XmlDocument { PreserveWhitespace = true };
					xmlDoc.LoadXml(resumeXml);

					//var sw = new System.IO.StringWriter();
					//xmlDoc.Save(sw);

					var resumeNode = xmlDoc.SelectSingleNode("//resume");
					if (resumeNode.IsNotNull())
					{
						var primaryJobNode = resumeNode.SelectSingleNode("//job[@pos=1]");
						if (primaryJobNode.IsNotNull()
						    && primaryJobNode.Attributes.IsNotNull()
						    && primaryJobNode.Attributes["onet"].IsNotNull())
						{
							onet = primaryJobNode.Attributes["onet"].Value;
							// ReSharper disable StringIndexOfIsCultureSpecific.1
							if (onet.IndexOf("|") != -1)
							{
								onet = onet.Substring(0, onet.IndexOf("|"));
							}
							// ReSharper restore StringIndexOfIsCultureSpecific.1

							var primaryJobTitleNode = primaryJobNode.SelectSingleNode("//title");
							if (primaryJobTitleNode.IsNotNull())
							{
								// ReSharper disable once RedundantAssignment
								jobTitle = primaryJobTitleNode.InnerText;
							}
						}
					}

					// convert   onet to ronet

					var ronet = OnetToROnet.ConvertResumeXmlToROnet(resumeXml, request);
					//var ronet = "13-2011.S3";

					// get job id by ronet
					if (!String.IsNullOrEmpty(ronet))
					{
						var job = Repositories.Library.Query<Data.Library.Entities.Job>().SingleOrDefault(x => x.ROnet == ronet);
						if (job.IsNotNull())
						{
							resumeModel.PrimaryJobId = job.Id;
						}
					}


					request.ResumeModel.Resume.PrimaryOnet = onet;
					request.ResumeModel.Resume.PrimaryROnet = ronet;

					#endregion

					#region extract the skills from the resume

					var skills = new List<string>();
					var skillRollupNode = xmlDoc.SelectSingleNode("//skillrollup");
					if (skillRollupNode.IsNotNull())
					{
						var canonSkillNodes = skillRollupNode.SelectNodes("//canonskill");
						if (canonSkillNodes.IsNotNull() && canonSkillNodes.Count > 0)
						{
							foreach (XmlNode canonSkillNode in canonSkillNodes)
							{
								// add name attribute to skills
								if (canonSkillNode.Attributes.IsNotNull()
								    && canonSkillNode.Attributes["name"].IsNotNull())
								{
									skills.Add(canonSkillNode.Attributes["name"].Value.ToLower());
								}

								// add variants also
								var variantNodes = canonSkillNode.SelectNodes("variant");
								if (variantNodes.IsNotNull())
								{
									skills.AddRange(from XmlNode variantNode in variantNodes where !string.IsNullOrEmpty(variantNode.InnerText) select variantNode.InnerText.ToLower());
								}
							}
						}
					}

					request.ResumeModel.Resume.ResumeSkills = (skills.Count > 0 ? skills.Serialize() : null);

					#endregion

					#region if user is registered and logged in save resume

					if (request.UserContext.IsNotNull() && request.UserContext.UserId != 0)
					{
						Resume resume;

						if (request.ResumeModel.Resume.Id.IsNotNull())
						{
							resume = Repositories.Core.FindById<Resume>(request.ResumeModel.Resume.Id);
							resume = request.ResumeModel.Resume.CopyTo(resume);
							resume.IsDefault = true;
							resume.ResumeDocument = request.ResumeModel.ResumeDocument.CopyTo(resume.ResumeDocument);
						}
						else if (request.ResumeModel.Resume.PersonId.IsNotNull())
						{
							resume = Repositories.Core.Query<Resume>().SingleOrDefault(x => x.PersonId == request.ResumeModel.Resume.PersonId && x.IsDefault);

							if (resume.IsNull())
							{
								resume = new Resume { ResumeDocument = new ResumeDocument() };
								resume = request.ResumeModel.Resume.CopyTo(resume);
								resume.IsDefault = true;
								resume.ResumeDocument = request.ResumeModel.ResumeDocument.CopyTo(resume.ResumeDocument);
								Repositories.Core.Add(resume);
							}
							else
							{
								resume = request.ResumeModel.Resume.CopyTo(resume);
								resume.IsDefault = true;
								resume.ResumeDocument = request.ResumeModel.ResumeDocument.CopyTo(resume.ResumeDocument);
								resume.ResumeDocument.ResumeId = resume.Id;
							}
						}
						else
						{
							resume = new Resume();
							resume = request.ResumeModel.Resume.CopyTo(resume);
							resume.IsDefault = true;
							resume.ResumeDocument = request.ResumeModel.ResumeDocument.CopyTo(resume.ResumeDocument);
							Repositories.Core.Add(resume);
						}

						Repositories.Core.SaveChanges(true);

						resumeModel.ResumeModel = new ExplorerResumeModel();
					}

					#endregion

					response.ResumeModel = resumeModel;
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the internship jobs.
		/// </summary>
		/// <param name="request"></param>
		/// <returns></returns>
		public InternshipJobResponse GetInternshipJobs(InternshipJobRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new InternshipJobResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, _validationLevel))
					return response;

				try
				{
					response.Jobs =
						Repositories.Library.Query<InternshipJobTitle>().Where(
							x => x.InternshipCategoryId == request.Criteria.InternshipCategoryId).OrderByDescending(x => x.Frequency).Select(
								x => x.AsDto()).Take(request.Criteria.ListSize).ToList();
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the internships by skill.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public SkillInternshipsResponse GetInternshipsBySkill(SkillInternshipsRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new SkillInternshipsResponse(request);
				if (!ValidateRequest(request, response, _validationLevel))
					return response;

				try
				{
					response.Internships =
						Repositories.Library.Query<InternshipSkillView>().Where(
							x => x.SkillId == request.Criteria.SkillId).OrderBy(
								x => x.Rank).Select(x => new InternshipReportViewDto() { InternshipCategoryId = x.InternshipCategoryId, Name = x.InternshipCategoryName }).Take(request.Criteria.ListSize).ToList();
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}
				return response;
			}
		}

		/// <summary>
		/// Gets the internship categories.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public InternshipCategoryResponse GetInternshipCategories(InternshipCategoryRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new InternshipCategoryResponse(request);
				if (!ValidateRequest(request, response, _validationLevel))
					return response;

				try
				{
					var internshipCategoires = Repositories.Library.Query<InternshipCategory>();

					if (request.InternshipCategoryIds.IsNotNullOrEmpty())
						internshipCategoires = internshipCategoires.Where(x => request.InternshipCategoryIds.Contains(x.Id));

					response.InternshipCategories = internshipCategoires.OrderBy(x => x.Name).Select(x => x.AsDto()).ToList();
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}
				return response;
			}
		}

		/// <summary>
		/// Gets the employer internships.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public EmployerInternshipResponse GetEmployerInternships(EmployerInternshipRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new EmployerInternshipResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, _validationLevel))
					return response;

				try
				{
					response.Internships =
						Repositories.Library.Query<InternshipEmployerView>().Where(
							x => x.EmployerId == request.Criteria.EmployerId && x.StateAreaId == request.Criteria.StateAreaId).
							OrderByDescending(
								x => x.Frequency).Select(
									x =>
										new InternshipReportViewDto()
										{
											InternshipCategoryId = x.InternshipCategoryId,
											Name = x.InternshipCategoryName,
											NumberAvailable = x.Frequency
										}).Take(
											request.Criteria.ListSize).ToList();
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the internship employers.
		/// </summary>
		/// <param name="request"></param>
		/// <returns></returns>
		public InternshipEmployerResponse GetInternshipEmployers(InternshipEmployerRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new InternshipEmployerResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, _validationLevel))
					return response;

				try
				{
					response.Employers =
						Repositories.Library.Query<InternshipEmployerView>().Where(
							x => x.InternshipCategoryId == request.Criteria.InternshipCategoryId && x.StateAreaId == request.Criteria.StateAreaId).OrderByDescending(x => x.Frequency).Select(
								x => new EmployerJobViewDto() { EmployerId = x.EmployerId, EmployerName = x.Name, Positions = x.Frequency }).Take(
									request.Criteria.ListSize).ToList();
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the internship skills.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public InternshipSkillResponse GetInternshipSkills(InternshipSkillRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new InternshipSkillResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, _validationLevel))
					return response;

				try
				{
					var internshipSkills = new List<SkillModel>();

					var query = from its in Repositories.Library.Query<InternshipSkillView>()
						where its.InternshipCategoryId == request.Criteria.InternshipCategoryId
						select its;

					var specialisedSkills =
						query.Where(x => x.SkillType == SkillTypes.Specialized).OrderBy(x => x.Rank).Select(
							x =>
								new SkillModel()
								{
									SkillId = x.SkillId,
									SkillType = x.SkillType,
									SkillName = x.SkillName,
									Rank = x.Rank
								}).Take(request.Criteria.ListSize).ToList();

					if (specialisedSkills.Count > 0)
					{
						internshipSkills.AddRange(specialisedSkills);
					}

					var softwareSkills =
						query.Where(x => x.SkillType == SkillTypes.Software).OrderBy(x => x.Rank).Select(
							x =>
								new SkillModel()
								{
									SkillId = x.SkillId,
									SkillType = x.SkillType,
									SkillName = x.SkillName,
									Rank = x.Rank
								}).Take(request.Criteria.ListSize).ToList();

					if (softwareSkills.Count > 0)
					{
						internshipSkills.AddRange(softwareSkills);
					}

					var foundationSkills =
						query.Where(x => x.SkillType == SkillTypes.Foundation).OrderBy(x => x.Rank).Select(
							x =>
								new SkillModel()
								{
									SkillId = x.SkillId,
									SkillType = x.SkillType,
									SkillName = x.SkillName,
									Rank = x.Rank
								}).Take(request.Criteria.ListSize).ToList();

					if (foundationSkills.Count > 0)
					{
						internshipSkills.AddRange(foundationSkills);
					}

					response.Skills = internshipSkills;
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the internship.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public InternshipResponse GetInternship(InternshipRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new InternshipResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, _validationLevel))
					return response;

				try
				{
					response.Internship =
						Repositories.Library.Query<InternshipCategory>().Where(x => x.Id == request.InternshipCategoryId).Select(
							x => x.AsDto()).SingleOrDefault();
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the internship report.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public InternshipResponse GetInternshipReport(InternshipRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new InternshipResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, _validationLevel))
					return response;

				try
				{
					switch (request.Criteria.FetchOption)
					{
						case CriteriaBase.FetchOptions.Single:
							break;


						case CriteriaBase.FetchOptions.List:
							var criteria = request.Criteria.ReportCriteria;

							var query = from ir in Repositories.Library.Query<InternshipReportView>()
								select ir;

							query = query.Where(x => x.StateAreaId == criteria.StateAreaId);

							if (criteria.EmployerId.HasValue)
							{
								query =
									query.Where(
										x =>
											(Repositories.Library.Query<InternshipEmployerView>().Where(y => y.EmployerId == criteria.EmployerId).
												Select(y => y.InternshipCategoryId)).Contains(x.InternshipCategoryId));
							}

							//TODO: skills if required

							query = query.OrderByDescending(x => x.NumberAvailable);

							response.InternshipReports = query.Select(x => x.AsDto()).Take(request.Criteria.ListSize).ToList();

							break;
					}
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Sends the help email.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public SendEmailResponse SendHelpEmail(SendEmailRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new SendEmailResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, _validationLevel))
					return response;

				try
				{
					var emailBody = String.Format("<p>{0}</p>", request.EmailBody);

					Helpers.Email.SendEmail(AppSettings.ExplorerHelpEmail, null, null, request.EmailSubject, emailBody);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Creates an anonymous bookmark and sends the email.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public SendEmailResponse SendEmail(SendEmailRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new SendEmailResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, _validationLevel))
					return response;

				try
				{
					var bookmarkName = string.Format("Emailed_Bookmark_{0}_{1}_{2}", request.ReportCriteria.ReportType,
						request.ReportCriteria.ReportItemId, DateTime.Now.ToString("yyyyMMdd_HHmmss"));

					string criteria = request.ReportCriteria.SerializeJson();

					var bookmark = new Bookmark
					{
						Name = bookmarkName,
						Type = BookmarkTypes.EmailedReportItem,
						Criteria = criteria,
						UserId = null,
						UserType = UserTypes.Anonymous
					};

					Repositories.Core.Add(bookmark);
					Repositories.Core.SaveChanges(true);

					var emailBody = String.Format("<p>{0}</p><p><a href='{1}'>{1}</a></p>", request.EmailBody,
						request.BookmarkUrl.Replace("[BOOKMARKID]", bookmark.Id.ToString()));

					var emailAddresses = request.EmailAddresses.Split(',');
					foreach (var emailAddress in emailAddresses)
					{
						Helpers.Email.SendEmail(emailAddress, null, null, request.EmailSubject, emailBody);
					}
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the site search results.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public SiteSearchResponse GetSiteSearchResults(SiteSearchRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new SiteSearchResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, _validationLevel))
					return response;

				try
				{
					var siteSearchResults = new List<SiteSearchResultModel>();

					#region job

					var results =
						Repositories.Library.Query<JobStateAreaView>().Where(x => x.Name.Contains(request.SearchTerm) && (x.StateAreaId == request.StateAreaId)).Select(
							x => new SiteSearchResultModel() { ResultId = x.Id, ResultName = x.Name, ResultType = ReportTypes.Job }).ToList();

					if (results.Count > 0)
						siteSearchResults.AddRange(results);

					#endregion

					#region employer

					results =
						Repositories.Library.Query<Data.Library.Entities.Employer>().Where(x => x.Name.Contains(request.SearchTerm)).
							Select(x => new SiteSearchResultModel() { ResultId = x.Id, ResultName = x.Name, ResultType = ReportTypes.Employer })
							.ToList();

					if (results.Count > 0)
						siteSearchResults.AddRange(results);

					#endregion

					#region degree education level

					var query = from del in Repositories.Library.Query<DegreeEducationLevelView>()
						where del.DegreeEducationLevelName.Contains(request.SearchTerm)
						select del;

					switch (AppSettings.ExplorerDegreeFilteringType)
					{
						case DegreeFilteringType.Ours:
							query = query.Where(x => !x.IsClientData);
							break;
						case DegreeFilteringType.Theirs:
							query = query.Where(x => x.IsClientData && x.ClientDataTag == AppSettings.ExplorerDegreeClientDataTag);
							break;
						case DegreeFilteringType.Both:
						case DegreeFilteringType.TheirsThenOurs:
							query =
								query.Where(
									x =>
										!x.IsClientData ||
										(x.IsClientData && x.ClientDataTag == AppSettings.ExplorerDegreeClientDataTag));
							break;
					}

					results =
						query.Select(
							x =>
								new SiteSearchResultModel() { ResultId = x.Id, ResultName = x.DegreeEducationLevelName, ResultType = ReportTypes.DegreeCertification })
							.ToList();

					if (results.Count > 0)
						siteSearchResults.AddRange(results);

					#endregion

					#region Program Areas

					results =
						Repositories.Library.Query<ProgramArea>().Where(x => x.Name.Contains(request.SearchTerm)).
							Select(x => new SiteSearchResultModel() { ResultId = x.Id, ResultName = x.Name, ResultType = ReportTypes.ProgramAreaDegree })
							.ToList();

					if (results.Count > 0)
						siteSearchResults.AddRange(results);

					#endregion

					#region skill

					results =
						Repositories.Library.Query<Skill>().Where(x => x.Name.Contains(request.SearchTerm)).
							Select(x => new SiteSearchResultModel() { ResultId = x.Id, ResultName = x.Name, ResultType = ReportTypes.Skill })
							.ToList();

					if (results.Count > 0)
						siteSearchResults.AddRange(results);

					#endregion

					#region internship

					results =
						Repositories.Library.Query<InternshipCategory>().Where(x => x.Name.Contains(request.SearchTerm)).
							Select(x => new SiteSearchResultModel() { ResultId = x.Id, ResultName = x.Name, ResultType = ReportTypes.Internship })
							.ToList();

					if (results.Count > 0)
						siteSearchResults.AddRange(results);

					#endregion

					response.SiteSearchResults = siteSearchResults;
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the job experience levels.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public JobExperienceLevelResponse GetJobExperienceLevels(JobExperienceLevelRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new JobExperienceLevelResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, _validationLevel))
					return response;

				try
				{
					response.JobExperienceLevels =
						Repositories.Library.Query<JobExperienceLevel>().Where(x => x.JobId == request.JobId).Select(x => x.AsDto()).
							ToList();
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the job education requirements.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public JobEducationRequirementResponse GetJobEducationRequirements(JobEducationRequirementRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new JobEducationRequirementResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, _validationLevel))
					return response;

				try
				{
					response.JobEducationRequirements =
						Repositories.Library.Query<JobEducationRequirement>().Where(x => x.JobId == request.JobId).Select(
							x => x.AsDto()).ToList();
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the job job titles.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public JobTitleResponse GetJobJobTitles(JobTitleRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new JobTitleResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, _validationLevel))
					return response;

				try
				{
					response.JobTitles = (from jjt in Repositories.Library.Query<JobJobTitle>()
						join jt in Repositories.Library.Query<JobTitle>() on jjt.JobTitleId
							equals jt.Id
						where jjt.JobId == request.JobId
						orderby jjt.DemandPercentile descending
						select jt.AsDto()).Take(5).ToList();
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the job career paths
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public JobCareerPathResponse GetJobCareerPaths(JobCareerPathRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new JobCareerPathResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, _validationLevel))
					return response;

				try
				{
					// Create new model
					var careerPaths = new JobCareerPathModel
					{
						Job = Repositories.Library.Query<Data.Library.Entities.Job>().SingleOrDefault(
							x => x.Id == request.JobId).AsDto(),
						CareerPathFrom = new List<JobCareerPathModel>(),
						CareerPathTo = new List<JobCareerPathModel>()
					};

					// Add job details in if state has been selected
					if (request.HiringDemandStateAreaId.HasValue)
					{
						var jobReportDetails = Repositories.Library
							.Query<JobReportView>()
							.FirstOrDefault(jrv => jrv.JobId == request.JobId && jrv.StateAreaId == request.HiringDemandStateAreaId.Value);
						careerPaths.JobReportDetails = new ExplorerJobReportView(jobReportDetails.IsNull() ? null : jobReportDetails.AsDto());
					}

					// Get career paths that lead from the current job
					var query =
						Repositories.Library.Query<JobCareerPathTo>().Where(x => x.CurrentJobId == request.JobId && x.Rank <= 4);

					if (AppSettings.ExplorerOnlyShowJobsBasedOnClientDegrees)
					{
						query =
							query.Where(
								x =>
									(Repositories.Library.Query<CareerAreaJobDegreeView>().Where(
										y => y.IsClientData && y.ClientDataTag == AppSettings.ExplorerDegreeClientDataTag).Select(y => y.JobId))
										.Contains(x.Next1JobId));
						query =
							query.Where(
								x => x.Next2JobId == null ||
								     (Repositories.Library.Query<CareerAreaJobDegreeView>().Where(
									     y => y.IsClientData && y.ClientDataTag == AppSettings.ExplorerDegreeClientDataTag).Select(y => y.JobId))
									     .Contains(x.Next2JobId.Value));
					}

					var careerPathTo = query.OrderBy(y => y.Next1JobId).ThenBy(z => z.Rank).Select(a => a.AsDto()).ToList();

					if (careerPathTo.Any())
					{
						var careerPath1JobIds = careerPathTo.Select(x => x.Next1JobId).Distinct().ToList();

						var careerPath1Jobs = Repositories.Library.Query<Data.Library.Entities.Job>()
							.Where(j => careerPath1JobIds.Contains(j.Id))
							.Select(j => j.AsDto())
							.ToList();
						var careerPath1JobReports = request.HiringDemandStateAreaId.HasValue
							? Repositories.Library.Query<JobReportView>()
								.Where(j => careerPath1JobIds.Contains(j.JobId) && j.StateAreaId == request.HiringDemandStateAreaId)
								.Select(j => j.AsDto())
								.ToList()
							: new List<JobReportViewDto>();

						// Loop through all distinct next (level 1) jobs
						foreach (var jobId in careerPath1JobIds)
						{
							// Create model for this job
							var careerPathLevel1 = new JobCareerPathModel
							{
								Job = careerPath1Jobs.First(x => x.Id == jobId),
								JobReportDetails = new ExplorerJobReportView(careerPath1JobReports.FirstOrDefault(x => x.JobId == jobId)),
								Rank = 1,
								CareerPathTo = new List<JobCareerPathModel>(),
							};

							var id = jobId;

							var careerPath2JobPaths = careerPathTo.Where(x => x.Next1JobId == id).OrderBy(x => x.Rank).ToList();
							var careerPath2JobIds = careerPath2JobPaths.Select(j => j.Next2JobId).ToList();

							var careerPath2Jobs = Repositories.Library.Query<Data.Library.Entities.Job>()
								.Where(j => careerPath2JobIds.Contains(j.Id))
								.Select(j => j.AsDto())
								.ToList();
							var careerPath2JobReports = request.HiringDemandStateAreaId.HasValue
								? Repositories.Library.Query<JobReportView>()
									.Where(j => careerPath2JobIds.Contains(j.JobId) && j.StateAreaId == request.HiringDemandStateAreaId)
									.Select(j => j.AsDto())
									.ToList()
								: new List<JobReportViewDto>();

							// Loop through all level 2 jobs
							foreach (var level2JobPath in careerPath2JobPaths)
							{
								if (level2JobPath.Next2JobId.IsNotNull())
								{
									// Create model for this job
									var careerPathLevel2 = new JobCareerPathModel
									{
										Job = careerPath2Jobs.First(j => j.Id == level2JobPath.Next2JobId),
										JobReportDetails = new ExplorerJobReportView(careerPath2JobReports.FirstOrDefault(j => j.JobId == level2JobPath.Next2JobId)),
										Rank = level2JobPath.Rank
									};
									// Add model to level 1 job
									careerPathLevel1.CareerPathTo.Add(careerPathLevel2);
								}
							}
							// Add model to top level job
							careerPaths.CareerPathTo.Add(careerPathLevel1);
						}
					}
					// Get top 4 career paths that lead to the current job
					var queryFrom =
						Repositories.Library.Query<JobCareerPathFrom>().Where(x => x.ToJobId == request.JobId);

					if (AppSettings.ExplorerOnlyShowJobsBasedOnClientDegrees)
					{
						queryFrom =
							queryFrom.Where(
								x =>
									(Repositories.Library.Query<CareerAreaJobDegreeView>().Where(
										y => y.IsClientData && y.ClientDataTag == AppSettings.ExplorerDegreeClientDataTag).Select(y => y.JobId))
										.Contains(x.FromJobId));
					}

					var careerPathFrom = queryFrom.OrderBy(x => x.Rank).Select(x => x.AsDto()).Take(4).ToList();

					if (careerPathFrom.Any())
					{
						// Loop through jobs
						foreach (var job in careerPathFrom)
						{
							// Add to top level job model
							careerPaths.CareerPathFrom.Add(new JobCareerPathModel
							{
								Job =
									Repositories.Library.Query<Data.Library.Entities.Job>().Where(
										x => x.Id == job.FromJobId).Select(x => x.AsDto()).First()
							});
						}
					}

					response.CareerPaths = careerPaths;
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the skill employers.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public SkillEmployerResponse GetSkillEmployers(SkillEmployerRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new SkillEmployerResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, _validationLevel))
					return response;

				try
				{
					var query = from ej in Repositories.Library.Query<EmployerJobView>()
						select ej;

					query =
						query.Where(
							x =>
								(Repositories.Library.Query<Data.Library.Entities.JobSkill>().Where(
									y => y.SkillId == request.Criteria.SkillId).Select(y => y.JobId)).Contains(x.JobId));

					query = query.Where(x => x.StateAreaId == request.Criteria.StateAreaId);

					if (request.Criteria.CareerAreaJobId.HasValue) // Search by specific job!!
					{
						query = query.Where(x => x.JobId == request.Criteria.CareerAreaJobId);
					}
					else if (request.Criteria.CareerAreaId.HasValue) // Search by career area
					{
						query =
							query.Where(
								x =>
									(Repositories.Library.Query<JobCareerArea>().Where(y => y.CareerAreaId == request.Criteria.CareerAreaId.Value).Select(
										y => y.JobId)).Contains(x.JobId));
					}
					else if (request.Criteria.CareerAreaJobTitleId.HasValue) // Seach by job title
					{
						query =
							query.Where(
								x =>
									(Repositories.Library.Query<JobJobTitle>().Where(y => y.JobTitleId == request.Criteria.CareerAreaJobTitleId.Value)
										.Select(y => y.JobId)).Contains(x.JobId));
					}
					else if (request.Criteria.CareerAreaMilitaryOccupationROnetCodes.IsNotNullOrEmpty())
					{
						query =
							query.Where(
								x =>
									(Repositories.Library.Query<Data.Library.Entities.Job>().Where(j => request.Criteria.CareerAreaMilitaryOccupationROnetCodes.Contains(j.ROnet))
										.Select(j => j.Id)).Contains(x.JobId));
					}

					if (request.Criteria.DegreeEducationLevelId.HasValue) // Search by specific degree
					{
						query =
							query.Where(
								x =>
									(Repositories.Library.Query<JobDegreeEducationLevel>().Where(
										y => y.DegreeEducationLevelId == request.Criteria.DegreeEducationLevelId.Value).Select(
											y => y.JobId)).Contains(x.JobId));
					}
					else if (request.Criteria.DegreeLevel != DegreeLevels.AnyOrNoDegree) // Filter by degree type if necessary
					{
						query =
							query.Where(
								x =>
									(Repositories.Library.Query<JobDegreeLevel>().Where(
										y => y.DegreeLevel == request.Criteria.DegreeLevel).Select(
											y => y.JobId)).Contains(x.JobId));
					}

					var allEmployers = query.Select(x => x.AsDto()).ToList();

					response.SkillEmployers = (from ej in allEmployers
						group ej by new { ej.EmployerId, ej.EmployerName }
						into g
						orderby g.Sum(ej => ej.Positions) descending
						select new EmployerJobViewDto()
						{
							EmployerId = g.Key.EmployerId,
							EmployerName = g.Key.EmployerName,
							Positions = g.Sum(ej => ej.Positions)
						}).Take(request.Criteria.ListSize).ToList();
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the skill job report.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public JobReportResponse GetSkillJobReport(JobReportRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new JobReportResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, _validationLevel))
					return response;

				try
				{
					var criteria = request.Criteria;

					var query =
						Repositories.Library.Query<Data.Library.Entities.JobSkill>().Where(
							x => x.SkillId == criteria.SkillId && x.JobSkillType == JobSkillTypes.Demanded);

					if (request.MaximumRank.HasValue)
						query = query.Where(x => x.Rank <= request.MaximumRank.Value);

					if (request.Criteria.CareerAreaId.HasValue) // Search by career area
					{
						query =
							query.Where(
								x =>
									(Repositories.Library.Query<JobCareerArea>().Where(y => y.CareerAreaId == request.Criteria.CareerAreaId.Value).Select(
										y => y.JobId)).Contains(x.JobId));
					}
					else if (request.Criteria.CareerAreaJobTitleId.HasValue) // Seach by job title
					{
						query =
							query.Where(
								x =>
									(Repositories.Library.Query<JobJobTitle>().Where(y => y.JobTitleId == request.Criteria.CareerAreaJobTitleId.Value)
										.Select(y => y.JobId)).Contains(x.JobId));
					}
					else if (request.Criteria.CareerAreaMilitaryOccupationROnetCodes.IsNotNullOrEmpty())
					{
						query =
							query.Where(
								x =>
									(Repositories.Library.Query<Data.Library.Entities.Job>().Where(j => request.Criteria.CareerAreaMilitaryOccupationROnetCodes.Contains(j.ROnet))
										.Select(j => j.Id)).Contains(x.JobId));
					}

					// employer
					if (criteria.EmployerId.HasValue)
					{
						query =
							query.Where(
								x =>
									(Repositories.Library.Query<EmployerJob>().Where(y => y.EmployerId == criteria.EmployerId.Value).Select(
										y => y.JobId)).Contains(x.JobId));
					}

					if (request.Criteria.DegreeEducationLevelId.HasValue) // Search by specific degree
					{
						query =
							query.Where(
								x =>
									(Repositories.Library.Query<JobDegreeEducationLevel>().Where(
										y => y.DegreeEducationLevelId == request.Criteria.DegreeEducationLevelId.Value).Select(
											y => y.JobId)).Contains(x.JobId));
					}
					else if (request.Criteria.DegreeLevel != DegreeLevels.AnyOrNoDegree) // Filter by degree type if necessary
					{
						query =
							query.Where(
								x =>
									(Repositories.Library.Query<JobDegreeLevel>().Where(
										y => y.DegreeLevel == request.Criteria.DegreeLevel).Select(
											y => y.JobId)).Contains(x.JobId));
					}

					if (AppSettings.ExplorerOnlyShowJobsBasedOnClientDegrees)
					{
						query =
							query.Where(
								x =>
									(Repositories.Library.Query<CareerAreaJobDegreeView>().Where(
										y => y.IsClientData && y.ClientDataTag == AppSettings.ExplorerDegreeClientDataTag).Select(y => y.JobId))
										.Contains(x.JobId));
					}

					var jobIds = query.Select(x => x.JobId).ToList();

					response.JobReports =
						Repositories.Library.Query<JobReportView>().Where(
							x => x.StateAreaId == criteria.StateAreaId && jobIds.Contains(x.JobId)).OrderByDescending(x => x.HiringDemand).
							Select(x => new ExplorerJobReportView(x.AsDto())).Take(request.Criteria.ListSize).ToList();

					if (AppSettings.Theme == FocusThemes.Education && AppSettings.ClientTag.IsNotNullOrEmpty())
					{
						var jobsFromCurrentSchool = GetCachedCurrentSchoolsDegreeJobs(request);
						foreach (var i in response.JobReports.Where(x => jobsFromCurrentSchool.Contains(x.ReportData.JobId)))
						{
							i.RelatedDegreeAvailableAtCurrentSchool = true;
						}
					}

				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the skill.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public SkillResponse GetSkill(SkillRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new SkillResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, _validationLevel))
					return response;

				try
				{
					response.Skill =
						Repositories.Library.Query<Skill>().Where(
							x => x.Id == request.SkillId).Select(
								x => x.AsDto()).SingleOrDefault();
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the skill report.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public SkillResponse GetSkillReport(SkillRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new SkillResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, _validationLevel))
					return response;

				try
				{
					var skills = new List<SkillModel>();

					switch (request.Criteria.FetchOption)
					{
						case CriteriaBase.FetchOptions.Single:
							break;

						case CriteriaBase.FetchOptions.List:
							var criteria = request.Criteria.ReportCriteria;

							var query = from js in Repositories.Library.Query<JobSkillView>()
								join jr in Repositories.Library.Query<JobReport>() on js.JobId equals jr.JobId
								where
									js.JobSkillType == JobSkillTypes.Demanded && js.Rank <= 20 && jr.StateAreaId == criteria.StateAreaId
								select new { js.JobId, js.SkillId, js.SkillName, js.SkillType, js.DemandPercentile, jr.HiringDemand };

							if (criteria.CareerAreaId.HasValue)
							{
								query =
									query.Where(
										x =>
											(Repositories.Library.Query<JobCareerArea>().Where(y => y.CareerAreaId == criteria.CareerAreaId.Value).
												Select(y => y.JobId)).Contains(x.JobId));
							}
							else if (criteria.CareerAreaJobId.HasValue)
							{
								query = query.Where(x => x.JobId == criteria.CareerAreaJobId.Value);
							}
							else if (criteria.CareerAreaJobTitleId.HasValue)
							{
								query =
									query.Where(
										x =>
											(Repositories.Library.Query<JobJobTitle>().Where(
												y => y.JobTitleId == criteria.CareerAreaJobTitleId.Value)
												.Select(y => y.JobId)).Contains(x.JobId));
							}
							else if (criteria.CareerAreaMilitaryOccupationROnetCodes.IsNotNullOrEmpty())
							{
								query =
									query.Where(
										x =>
											(Repositories.Library.Query<Data.Library.Entities.Job>().Where(j => criteria.CareerAreaMilitaryOccupationROnetCodes.Contains(j.ROnet))
												.Select(j => j.Id)).Contains(x.JobId));
							}

							if (criteria.EducationCriteriaOption == EducationCriteriaOptions.DegreeLevel
							    && criteria.DegreeLevel != DegreeLevels.AnyOrNoDegree)
							{
								query =
									query.Where(
										x =>
											(Repositories.Library.Query<JobDegreeLevel>().Where(
												y => y.DegreeLevel == criteria.DegreeLevel).Select(
													y => y.JobId)).Contains(x.JobId));
							}
							else
							{
								if (criteria.DegreeEducationLevelId.HasValue)
								{
									query =
										query.Where(
											x =>
												(Repositories.Library.Query<JobDegreeEducationLevel>().Where(
													y => y.DegreeEducationLevelId == criteria.DegreeEducationLevelId.Value).Select(
														y => y.JobId)).Contains(x.JobId));
								}
							}

							if (criteria.EmployerId.HasValue)
							{
								query =
									query.Where(
										x =>
											(Repositories.Library.Query<EmployerJob>().Where(y => y.EmployerId == criteria.EmployerId.Value).Select(
												y => y.JobId)).Contains(x.JobId));
							}

							switch (criteria.SkillCriteriaOption)
							{
								case SkillCriteriaOptions.SkillByCategory:

									if (criteria.SkillId.HasValue)
									{
										query = query.Where(x => x.SkillId == criteria.SkillId.Value);
									}
									else if (criteria.SkillSubCategoryId.HasValue)
									{
										query =
											query.Where(
												x =>
													(Repositories.Library.Query<SkillCategorySkill>().Where(
														y => y.SkillCategoryId == criteria.SkillSubCategoryId.Value).Select(y => y.SkillId)).Contains(x.SkillId));
									}
									else if (criteria.SkillCategoryId.HasValue)
									{
										query =
											query.Where(
												x =>
													(Repositories.Library.Query<SkillCategorySkillView>().Where(
														y => y.SkillCategoryParentId == criteria.SkillCategoryId.Value).Select(y => y.SkillId)).Contains(x.SkillId));
									}

									break;

								case SkillCriteriaOptions.SkillByJob:

									if (criteria.SkillJobSkillId.HasValue)
									{
										query = query.Where(x => x.SkillId == criteria.SkillJobSkillId.Value);
									}
									else if (criteria.SkillJobId.HasValue)
									{
										query = query.Where(x => x.JobId == criteria.SkillJobId.Value);
									}

									break;
							}

							if (criteria.PrimaryJobId.HasValue)
							{
								query =
									query.Where(
										x =>
											x.JobId == criteria.PrimaryJobId.Value ||
											(Repositories.Library.Query<JobRelatedJob>().Where(y => y.JobId == criteria.PrimaryJobId.Value).Select(
												y => y.RelatedJobId)).Contains(x.JobId));
							}

							var allSkills = query.ToList();

							var specialisedSkills = (from x in allSkills
								where x.SkillType == SkillTypes.Specialized
								group x by new { x.SkillId, x.SkillName, x.SkillType }
								into g
								orderby g.Sum(x => x.HiringDemand * (x.DemandPercentile / 100)) descending
								select new SkillModel
								{
									SkillId = g.Key.SkillId,
									SkillType = g.Key.SkillType,
									SkillName = g.Key.SkillName,
									DemandPercentile = g.Sum(x => x.HiringDemand * (x.DemandPercentile / 100))
								}).Take(request.Criteria.ListSize).ToList();

							if (specialisedSkills.Count > 0)
							{
								skills.AddRange(specialisedSkills);
							}

							var softwareSkills = (from x in allSkills
								where x.SkillType == SkillTypes.Software
								group x by new { x.SkillId, x.SkillName, x.SkillType }
								into g
								orderby g.Sum(x => x.HiringDemand * (x.DemandPercentile / 100)) descending
								select new SkillModel
								{
									SkillId = g.Key.SkillId,
									SkillType = g.Key.SkillType,
									SkillName = g.Key.SkillName,
									DemandPercentile = g.Sum(x => x.HiringDemand * (x.DemandPercentile / 100))
								}).Take(request.Criteria.ListSize).ToList();

							if (softwareSkills.Count > 0)
							{
								skills.AddRange(softwareSkills);
							}

							var foundationSkills = (from x in allSkills
								where x.SkillType == SkillTypes.Foundation
								group x by new { x.SkillId, x.SkillName, x.SkillType }
								into g
								orderby g.Sum(x => x.HiringDemand * (x.DemandPercentile / 100)) descending
								select new SkillModel
								{
									SkillId = g.Key.SkillId,
									SkillType = g.Key.SkillType,
									SkillName = g.Key.SkillName,
									DemandPercentile = g.Sum(x => x.HiringDemand * (x.DemandPercentile / 100))
								}).Take(request.Criteria.ListSize).ToList();

							if (foundationSkills.Count > 0)
							{
								skills.AddRange(foundationSkills);
							}

							response.SkillReports = skills;

							break;
					}
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the degree education level exts.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public DegreeEducationLevelResponse GetDegreeEducationLevelExts(DegreeEducationLevelRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new DegreeEducationLevelResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, _validationLevel))
					return response;

				try
				{
					response.DegreeEducationLevelExts =
						Repositories.Library.Query<DegreeEducationLevelExt>().Where(
							x => x.DegreeEducationLevelId == request.SearchCriteria.DegreeEducationLevelId).Select(x => x.AsDto()).ToList();
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the degree education level study places.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public DegreeEducationLevelStudyPlaceResponse GetDegreeEducationLevelStudyPlaces(DegreeEducationLevelStudyPlaceRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new DegreeEducationLevelStudyPlaceResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, _validationLevel))
					return response;

				try
				{
					var query = from spdc in Repositories.Library.Query<StudyPlaceDegreeEducationLevelView>()
						where spdc.DegreeEducationLevelId == request.Criteria.DegreeEducationLevelId
						select spdc;

					if (request.Criteria.StateAreaId != 0)
					{
						var stateAreaRequest = new StateAreaRequest
						{
							ClientTag = request.ClientTag,
							SessionId = request.SessionId,
							RequestId = request.RequestId,
							UserContext = request.UserContext
						};

						var stateAreas = GetCachedStateAreas(stateAreaRequest);

						var stateArea = stateAreas.Single(x => x.Id == request.Criteria.StateAreaId);
						if (stateArea.AreaSortOrder == 0)
						{
							query =
								query.Where(
									x =>
										(Repositories.Library.Query<StateArea>().Where(y => y.StateId == stateArea.StateId).Select(y => y.Id)).
											Contains(x.StateAreaId) || x.StateAreaId == request.Criteria.StateAreaId);
						}
						else
						{
							query = query.Where(x => x.StateAreaId == request.Criteria.StateAreaId);
						}
					}

					switch (AppSettings.ExplorerDegreeFilteringType)
					{
						case DegreeFilteringType.Ours:
							query = query.Where(x => !x.IsClientData);
							break;
						case DegreeFilteringType.Theirs:
							query = query.Where(x => x.IsClientData && x.ClientDataTag == AppSettings.ExplorerDegreeClientDataTag);
							break;
						case DegreeFilteringType.Both:
						case DegreeFilteringType.TheirsThenOurs:
							query =
								query.Where(
									x =>
										!x.IsClientData ||
										(x.IsClientData && x.ClientDataTag == AppSettings.ExplorerDegreeClientDataTag));
							break;
					}


					response.DegreeEducationLevelStudyPlaces =
						query.OrderBy(x => x.StudyPlaceName).Select(x => x.AsDto()).ToList();
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the degree education level employers.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public DegreeEducationLevelEmployerResponse GetDegreeEducationLevelEmployers(DegreeEducationLevelEmployerRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new DegreeEducationLevelEmployerResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, _validationLevel))
					return response;

				try
				{
					var query = from ej in Repositories.Library.Query<EmployerJobView>()
						select ej;

					query =
						query.Where(
							x =>
								(Repositories.Library.Query<JobDegreeEducationLevel>().Where(
									y => y.DegreeEducationLevelId == request.Criteria.DegreeEducationLevelId).Select(y => y.JobId)).Contains(x.JobId));

					query = query.Where(x => x.StateAreaId == request.Criteria.StateAreaId);

					if (request.Criteria.CareerAreaId.HasValue) // Search by career area
					{
						query =
							query.Where(
								x =>
									(Repositories.Library.Query<JobCareerArea>().Where(y => y.CareerAreaId == request.Criteria.CareerAreaId.Value).Select(
										y => y.JobId)).Contains(x.JobId));
					}
					else if (request.Criteria.CareerAreaJobTitleId.HasValue) // Seach by job title
					{
						query =
							query.Where(
								x =>
									(Repositories.Library.Query<JobJobTitle>().Where(y => y.JobTitleId == request.Criteria.CareerAreaJobTitleId.Value)
										.Select(y => y.JobId)).Contains(x.JobId));
					}
					else if (request.Criteria.CareerAreaMilitaryOccupationROnetCodes.IsNotNullOrEmpty())
					{
						query =
							query.Where(
								x =>
									(Repositories.Library.Query<Data.Library.Entities.Job>().Where(j => request.Criteria.CareerAreaMilitaryOccupationROnetCodes.Contains(j.ROnet))
										.Select(j => j.Id)).Contains(x.JobId));
					}

					if (request.Criteria.SkillCriteriaOption == SkillCriteriaOptions.SkillByCategory)
					{
						if (request.Criteria.SkillId.HasValue) // Filter by skill
						{
							query =
								query.Where(
									x =>
										(Repositories.Library.Query<Data.Library.Entities.JobSkill>().Where(
											y => y.SkillId == request.Criteria.SkillId).Select(
												y => y.JobId)).Contains(x.JobId));
						}
						else if (request.Criteria.SkillSubCategoryId.HasValue) // Filter by skill subcategory
						{
							query = query.Where(x => (from js in Repositories.Library.Query<Data.Library.Entities.JobSkill>()
								join sks in Repositories.Library.Query<SkillCategorySkill>() on js.SkillId equals
									sks.SkillId
								where sks.SkillCategoryId == request.Criteria.SkillSubCategoryId.Value
								select js.JobId).Contains(x.JobId));
						}
						else if (request.Criteria.SkillCategoryId.HasValue) // Filter by skill category
						{
							query = query.Where(x => (from js in Repositories.Library.Query<Data.Library.Entities.JobSkill>()
								join sks in Repositories.Library.Query<SkillCategorySkillView>() on js.SkillId equals
									sks.SkillId
								where sks.SkillCategoryParentId == request.Criteria.SkillCategoryId.Value
								select js.JobId).Contains(x.JobId));
						}
					}
					else if (request.Criteria.SkillCriteriaOption == SkillCriteriaOptions.SkillByJob) // Filter by skills in current job
					{
						if (request.Criteria.SkillJobSkillId.HasValue)
						{
							query =
								query.Where(
									x =>
										(Repositories.Library.Query<Data.Library.Entities.JobSkill>().Where(
											y => y.SkillId == request.Criteria.SkillJobSkillId).Select(
												y => y.JobId)).Contains(x.JobId));
						}
						else if (request.Criteria.SkillJobId.HasValue)
						{
							query = query.Where(x => (from js in Repositories.Library.Query<Data.Library.Entities.JobSkill>()
								join jsjs in Repositories.Library.Query<Data.Library.Entities.JobSkill>() on
									js.SkillId equals jsjs.SkillId
								where jsjs.JobId == request.Criteria.SkillJobId.Value
								select js.JobId).Contains(x.JobId));
						}
					}

					var allEmployers = query.Select(x => x.AsDto()).ToList();

					response.Employers = (from ej in allEmployers
						group ej by new { ej.EmployerId, ej.EmployerName }
						into g
						orderby g.Sum(ej => ej.Positions) descending
						select new EmployerJobViewDto()
						{
							EmployerId = g.Key.EmployerId,
							EmployerName = g.Key.EmployerName,
							Positions = g.Sum(ej => ej.Positions)
						}).Take(request.Criteria.ListSize).ToList();
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the degree education level skills.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public DegreeEducationLevelSkillResponse GetDegreeEducationLevelSkills(DegreeEducationLevelSkillRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new DegreeEducationLevelSkillResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, _validationLevel))
					return response;

				try
				{
					var degreeEducationLevelSkills = new List<SkillModel>();

					var query = from dcs in Repositories.Library.Query<DegreeEducationLevelSkillView>()
						where dcs.Id == request.Criteria.DegreeEducationLevelId
						select dcs;

					query = query.Where(x => x.StateAreaId == request.Criteria.StateAreaId);

					var specialisedSkills =
						query.Where(x => x.SkillType == SkillTypes.Specialized).OrderByDescending(x => x.DemandPercentile).Select(
							x =>
								new SkillModel()
								{
									SkillId = x.SkillId,
									SkillType = x.SkillType,
									SkillName = x.SkillName,
									DemandPercentile = x.DemandPercentile.Value
								}).Take(request.Criteria.ListSize).ToList();

					if (specialisedSkills.Count > 0)
					{
						degreeEducationLevelSkills.AddRange(specialisedSkills);
					}

					var softwareSkills =
						query.Where(x => x.SkillType == SkillTypes.Software).OrderByDescending(x => x.DemandPercentile).Select(
							x =>
								new SkillModel()
								{
									SkillId = x.SkillId,
									SkillType = x.SkillType,
									SkillName = x.SkillName,
									DemandPercentile = x.DemandPercentile.Value
								}).Take(request.Criteria.ListSize).ToList();

					if (softwareSkills.Count > 0)
					{
						degreeEducationLevelSkills.AddRange(softwareSkills);
					}

					var foundationSkills =
						query.Where(x => x.SkillType == SkillTypes.Foundation).OrderByDescending(x => x.DemandPercentile).Select(
							x =>
								new SkillModel()
								{
									SkillId = x.SkillId,
									SkillType = x.SkillType,
									SkillName = x.SkillName,
									DemandPercentile = x.DemandPercentile.Value
								}).Take(request.Criteria.ListSize).ToList();

					if (foundationSkills.Count > 0)
					{
						degreeEducationLevelSkills.AddRange(foundationSkills);
					}

					response.DegreeEducationLevelSkills = degreeEducationLevelSkills;
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the degree education level job report.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public JobReportResponse GetDegreeEducationLevelJobReport(JobReportRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new JobReportResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, _validationLevel))
					return response;

				try
				{
					var criteria = request.Criteria;

					var query = from jr in Repositories.Library.Query<JobReportView>()
						where jr.StateAreaId == criteria.StateAreaId
						select jr;

					query = query.Where(jrq => jrq.StateAreaId == criteria.StateAreaId);

					if (AppSettings.ExplorerOnlyShowJobsBasedOnClientDegrees && AppSettings.ExplorerDegreeFilteringType != DegreeFilteringType.Theirs)
					{

						query =
							query.Where(
								x =>
									(Repositories.Library.Query<CareerAreaJobDegreeView>().Where(
										y => y.IsClientData && y.ClientDataTag == AppSettings.ExplorerDegreeClientDataTag).Select(y => y.JobId))
										.Contains(x.JobId));
					}

					if (request.Criteria.CareerAreaId.HasValue) // Search by career area
					{
						query =
							query.Where(
								x =>
									(Repositories.Library.Query<JobCareerArea>().Where(y => y.CareerAreaId == request.Criteria.CareerAreaId.Value).Select(
										y => y.JobId)).Contains(x.JobId));
					}
					else if (request.Criteria.CareerAreaJobTitleId.HasValue) // Seach by job title
					{
						query =
							query.Where(
								x =>
									(Repositories.Library.Query<JobJobTitle>().Where(y => y.JobTitleId == request.Criteria.CareerAreaJobTitleId.Value)
										.Select(y => y.JobId)).Contains(x.JobId));
					}
					else if (criteria.CareerAreaMilitaryOccupationROnetCodes.IsNotNullOrEmpty())
					{
						query =
							query.Where(
								x =>
									(Repositories.Library.Query<Data.Library.Entities.Job>().Where(j => criteria.CareerAreaMilitaryOccupationROnetCodes.Contains(j.ROnet))
										.Select(j => j.Id)).Contains(x.JobId));
					}

					if (request.JobDegreeTierLevel != 0)
					{
						query =
							query.Where(
								x =>
									(Repositories.Library.Query<JobDegreeEducationLevel>().Where(
										y => y.Tier == request.JobDegreeTierLevel && y.DegreeEducationLevelId == request.Criteria.DegreeEducationLevelId.GetValueOrDefault()).Select(
											y => y.JobId)).Contains(x.JobId));
					}
					else if (request.Criteria.DegreeEducationLevelId.HasValue) // Search by specific degree
					{
						query =
							query.Where(
								x =>
									(Repositories.Library.Query<JobDegreeEducationLevel>().Where(
										y => y.DegreeEducationLevelId == request.Criteria.DegreeEducationLevelId.Value).Select(
											y => y.JobId)).Contains(x.JobId));
					}
					else if (request.Criteria.DegreeLevel != DegreeLevels.AnyOrNoDegree) // Filter by degree type if necessary
					{
						query =
							query.Where(
								x =>
									(Repositories.Library.Query<JobDegreeLevel>().Where(
										y => y.DegreeLevel == request.Criteria.DegreeLevel).Select(
											y => y.JobId)).Contains(x.JobId));
					}

					if (request.Criteria.DegreeId.HasValue)
					{
						if (request.Criteria.DegreeId.Value > 0)
							query = query.Where(x => (from jdel in Repositories.Library.Query<JobDegreeEducationLevel>()
								join del in Repositories.Library.Query<DegreeEducationLevel>()
									on jdel.DegreeEducationLevelId equals del.Id
								where del.DegreeId == request.Criteria.DegreeId.Value
								select jdel.JobId).Contains(x.JobId));
						else
							query = query.Where(x => (from jdel in Repositories.Library.Query<JobDegreeEducationLevel>()
								join del in Repositories.Library.Query<DegreeEducationLevel>()
									on jdel.DegreeEducationLevelId equals del.Id
								join pad in Repositories.Library.Query<ProgramAreaDegree>()
									on del.DegreeId equals pad.DegreeId
								where pad.ProgramAreaId == request.Criteria.ProgramAreaId.Value
								select jdel.JobId).Contains(x.JobId));
					}

					if (request.Criteria.SkillCriteriaOption == SkillCriteriaOptions.SkillByCategory)
					{
						if (request.Criteria.SkillId.HasValue) // Filter by skill
						{
							query =
								query.Where(
									x =>
										(Repositories.Library.Query<Data.Library.Entities.JobSkill>().Where(
											y => y.SkillId == request.Criteria.SkillId).Select(
												y => y.JobId)).Contains(x.JobId));
						}
						else if (request.Criteria.SkillSubCategoryId.HasValue) // Filter by skill subcategory
						{
							query = query.Where(x => (from js in Repositories.Library.Query<Data.Library.Entities.JobSkill>()
								join sks in Repositories.Library.Query<SkillCategorySkill>() on js.SkillId equals
									sks.SkillId
								where sks.SkillCategoryId == request.Criteria.SkillSubCategoryId.Value
								select js.JobId).Contains(x.JobId));
						}
						else if (request.Criteria.SkillCategoryId.HasValue) // Filter by skill category
						{
							query = query.Where(x => (from js in Repositories.Library.Query<Data.Library.Entities.JobSkill>()
								join sks in Repositories.Library.Query<SkillCategorySkillView>() on js.SkillId equals
									sks.SkillId
								where sks.SkillCategoryParentId == request.Criteria.SkillCategoryId.Value
								select js.JobId).Contains(x.JobId));
						}
					}
					else if (request.Criteria.SkillCriteriaOption == SkillCriteriaOptions.SkillByJob) // Filter by skills in current job
					{
						if (request.Criteria.SkillJobSkillId.HasValue)
						{
							query =
								query.Where(
									x =>
										(Repositories.Library.Query<Data.Library.Entities.JobSkill>().Where(
											y => y.SkillId == request.Criteria.SkillJobSkillId).Select(
												y => y.JobId)).Contains(x.JobId));
						}
						else if (request.Criteria.SkillJobId.HasValue)
						{
							query = query.Where(x => (from js in Repositories.Library.Query<Data.Library.Entities.JobSkill>()
								join jsjs in Repositories.Library.Query<Data.Library.Entities.JobSkill>() on
									js.SkillId equals jsjs.SkillId
								where jsjs.JobId == request.Criteria.SkillJobId.Value
								select js.JobId).Contains(x.JobId));
						}
					}

					response.JobReports =
						query.OrderByDescending(x => x.HiringTrendPercentile).Select(x => new ExplorerJobReportView(x.AsDto())).Take(request.Criteria.ListSize).
							ToList();

					if (AppSettings.Theme == FocusThemes.Education && AppSettings.ClientTag.IsNotNullOrEmpty())
					{
						var jobsFromCurrentSchool = GetCachedCurrentSchoolsDegreeJobs(request);
						foreach (var i in response.JobReports.Where(x => jobsFromCurrentSchool.Contains(x.ReportData.JobId)))
						{
							i.RelatedDegreeAvailableAtCurrentSchool = true;
						}
					}
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the degree education level.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public DegreeEducationLevelResponse GetDegreeEducationLevel(DegreeEducationLevelRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new DegreeEducationLevelResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, _validationLevel))
					return response;

				try
				{
					response.DegreeEducationLevel =
						Repositories.Library.Query<DegreeEducationLevelView>().Where(
							x => x.Id == request.SearchCriteria.DegreeEducationLevelId).Select(
								x => x.AsDto()).SingleOrDefault();
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the degree education level report.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public DegreeEducationLevelResponse GetDegreeEducationLevelReport(DegreeEducationLevelRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new DegreeEducationLevelResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, _validationLevel))
					return response;

				try
				{
					switch (request.ReportCriteria.FetchOption)
					{
						case CriteriaBase.FetchOptions.Single:
							break;


						case CriteriaBase.FetchOptions.List:
							var criteria = request.ReportCriteria.ReportCriteria;

							var query = from jdc in Repositories.Library.Query<JobDegreeEducationLevelReportView>()
								where !jdc.ExcludeFromReport
								select jdc;

							switch (AppSettings.ExplorerDegreeFilteringType)
							{
								case DegreeFilteringType.Ours:
									query = query.Where(x => !x.IsClientData);
									break;

								case DegreeFilteringType.Theirs:
									query = query.Where(x => x.IsClientData && x.ClientDataTag == AppSettings.ExplorerDegreeClientDataTag);
									break;

								case DegreeFilteringType.Both:
								case DegreeFilteringType.TheirsThenOurs:
									query =
										query.Where(
											x =>
												!x.IsClientData ||
												(x.IsClientData && x.ClientDataTag == AppSettings.ExplorerDegreeClientDataTag));
									break;
							}

							query = query.Where(x => x.StateAreaId == criteria.StateAreaId);

							if (criteria.CareerAreaId.HasValue)
							{
								query =
									query.Where(
										x =>
											(Repositories.Library.Query<JobCareerArea>().Where(y => y.CareerAreaId == criteria.CareerAreaId.Value).Select(
												y => y.JobId)).Contains(x.JobId));
							}
							else if (criteria.CareerAreaJobId.HasValue)
							{
								query =
									query.Where(x => x.JobId == criteria.CareerAreaJobId);
							}
							else if (criteria.CareerAreaJobTitleId.HasValue)
							{
								query =
									query.Where(
										x =>
											(Repositories.Library.Query<JobJobTitle>().Where(y => y.JobTitleId == criteria.CareerAreaJobTitleId.Value)
												.Select(y => y.JobId)).Contains(x.JobId));
							}
							else if (criteria.CareerAreaMilitaryOccupationROnetCodes.IsNotNullOrEmpty())
							{
								query =
									query.Where(
										x =>
											(Repositories.Library.Query<Data.Library.Entities.Job>().Where(j => criteria.CareerAreaMilitaryOccupationROnetCodes.Contains(j.ROnet))
												.Select(j => j.Id)).Contains(x.JobId));
							}

							if (criteria.EducationCriteriaOption == EducationCriteriaOptions.DegreeLevel
							    && criteria.DegreeLevel != DegreeLevels.AnyOrNoDegree)
							{
								switch (criteria.DegreeLevel)
								{
									case DegreeLevels.LessThanBachelors:
										query = query.Where(x => (int)x.EducationLevel < (int)ExplorerEducationLevels.Bachelor);
										break;

									case DegreeLevels.BachelorsOrHigher:
										query = query.Where(x => (int)x.EducationLevel >= (int)ExplorerEducationLevels.Bachelor);
										break;
								}
							}
							else
							{
								if (criteria.DegreeEducationLevelId.HasValue)
								{
									query = query.Where(x => x.DegreeEducationLevelId == criteria.DegreeEducationLevelId.Value);
								}
							}

							if (criteria.EmployerId.HasValue)
							{
								query =
									query.Where(
										x =>
											(Repositories.Library.Query<EmployerJob>().Where(y => y.EmployerId == criteria.EmployerId.Value).Select(
												y => y.JobId)).Contains(x.JobId));
							}

							switch (criteria.SkillCriteriaOption)
							{
								case SkillCriteriaOptions.SkillByCategory:

									if (criteria.SkillId.HasValue)
									{
										query =
											query.Where(
												x =>
													(Repositories.Library.Query<Data.Library.Entities.JobSkill>().Where(y => y.SkillId == criteria.SkillId.Value).Select(
														y => y.JobId)).Contains(x.JobId));
									}
									else if (criteria.SkillSubCategoryId.HasValue)
									{
										query = query.Where(x => (from js in Repositories.Library.Query<Data.Library.Entities.JobSkill>()
											join sks in Repositories.Library.Query<SkillCategorySkill>() on js.SkillId equals
												sks.SkillId
											where sks.SkillCategoryId == criteria.SkillSubCategoryId.Value
											select js.JobId).Contains(x.JobId));

									}
									else if (criteria.SkillCategoryId.HasValue)
									{
										query = query.Where(x => (from js in Repositories.Library.Query<Data.Library.Entities.JobSkill>()
											join sks in Repositories.Library.Query<SkillCategorySkillView>() on js.SkillId equals
												sks.SkillId
											where sks.SkillCategoryParentId == criteria.SkillCategoryId.Value
											select js.JobId).Contains(x.JobId));
									}

									break;

								case SkillCriteriaOptions.SkillByJob:

									if (criteria.SkillJobSkillId.HasValue)
									{
										query =
											query.Where(
												x =>
													(Repositories.Library.Query<Data.Library.Entities.JobSkill>().Where(y => y.SkillId == criteria.SkillJobSkillId.Value).Select(
														y => y.JobId)).Contains(x.JobId));
									}
									else if (criteria.SkillJobId.HasValue)
									{
										query = query.Where(x => x.JobId == criteria.SkillJobId.Value);
									}

									break;
							}

							if (criteria.PrimaryJobId.HasValue)
							{
								query =
									query.Where(
										x => x.JobId == criteria.PrimaryJobId.Value ||
										     (Repositories.Library.Query<JobRelatedJob>().Where(y => y.JobId == criteria.PrimaryJobId.Value).Select(
											     y => y.RelatedJobId)).Contains(x.JobId));
							}

							if (request.SearchCriteria != null)
							{
								switch (request.SearchCriteria.DetailedDegreeLevel)
								{
									case DetailedDegreeLevels.CertificateOrAssociate:
										query =
											query.Where(
												x =>
													x.EducationLevel == ExplorerEducationLevels.Certificate ||
													x.EducationLevel == ExplorerEducationLevels.Associate);
										break;

									case DetailedDegreeLevels.Bachelors:
										query = query.Where(x => x.EducationLevel == ExplorerEducationLevels.Bachelor);
										break;

									case DetailedDegreeLevels.GraduateOrProfessional:
										query = query.Where(x => x.EducationLevel > ExplorerEducationLevels.Bachelor);
										break;
								}
							}

							if (criteria.DegreeId.HasValue)
							{
								if (criteria.DegreeId.Value > 0)
									query = query.Where(x => x.DegreeId == criteria.DegreeId);
								else
									query = query.Where(x => (from pad in Repositories.Library.Query<ProgramAreaDegree>()
										where pad.ProgramAreaId == criteria.ProgramAreaId.Value
										select pad.DegreeId).Contains(x.DegreeId));
							}

							var allDegreeEducationLevels = query.Select(x => x.AsDto()).ToList();
							if (AppSettings.ExplorerDegreeFilteringType == DegreeFilteringType.TheirsThenOurs && allDegreeEducationLevels.Exists(d => d.IsClientData))
								allDegreeEducationLevels.RemoveAll(d => !d.IsClientData);

							var listSize = request.ReportCriteria.ListSize == 0 ? allDegreeEducationLevels.Count : request.ReportCriteria.ListSize;

							response.DegreeEducationLevelReports = (from adc in allDegreeEducationLevels
								group adc by
									new
									{
										adc.DegreeEducationLevelId,
										adc.EducationLevel,
										adc.DegreeEducationLevelName,
										adc.DegreeId,
										adc.DegreeName,
										adc.IsDegreeArea,
										adc.IsClientData
									}
								into g
								orderby g.Sum(adc => adc.HiringDemand) descending, g.Sum(adc => adc.DegreesAwarded) descending
								select new JobDegreeEducationLevelReportViewDto()
								{
									DegreeEducationLevelId = g.Key.DegreeEducationLevelId,
									EducationLevel = g.Key.EducationLevel,
									DegreeEducationLevelName = g.Key.DegreeEducationLevelName,
									DegreeId = g.Key.DegreeId,
									DegreeName = g.Key.DegreeName,
									IsDegreeArea = g.Key.IsDegreeArea,
									IsClientData = g.Key.IsClientData,
									HiringDemand = g.Sum(adc => adc.HiringDemand),
									DegreesAwarded = g.Sum(adc => adc.DegreesAwarded)
								}).ToList();

							//Get Degree Top 10
							var degreelevels = response.DegreeEducationLevelReports.Where(x => !x.IsDegreeArea).Take(listSize).ToList();
							//Get Areas
							var degreeAreas = response.DegreeEducationLevelReports.Where(x => x.IsDegreeArea).ToList();

							response.DegreeEducationLevelReports = degreelevels.Union(degreeAreas).ToList();

							break;
					}
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the employer skills.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public EmployerSkillResponse GetEmployerSkills(EmployerSkillRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new EmployerSkillResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, _validationLevel))
					return response;

				try
				{
					response.EmployerSkills = request.Criteria.CareerAreaId.IsNull() ? GetEmployerSkills(request.Criteria) : GetJobEmployerSkills(request.Criteria);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the job employer skills.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		private List<SkillModel> GetJobEmployerSkills(ExplorerCriteria criteria)
		{
			var employerSkills = new List<SkillModel>();

			var query = from es in Repositories.Library.Query<JobEmployerSkillView>()
				where es.EmployerId == criteria.EmployerId
				select es;

			query =
				query.Where(
					x =>
						(Repositories.Library.Query<JobCareerArea>().Where(y => y.CareerAreaId == criteria.CareerAreaId).Select(
							y => y.JobId)).Contains(x.JobId));

			var specialisedSkills =
				query.Where(x => x.SkillType == SkillTypes.Specialized).OrderByDescending(x => x.SkillCount).Select(
					x =>
						new SkillModel()
						{
							SkillId = x.SkillId,
							SkillType = x.SkillType,
							SkillName = x.SkillName,
							DemandPercentile = x.SkillCount
						}).Take(criteria.ListSize).ToList();

			if (specialisedSkills.Count > 0)
			{
				employerSkills.AddRange(specialisedSkills);
			}

			var softwareSkills =
				query.Where(x => x.SkillType == SkillTypes.Software).OrderByDescending(x => x.SkillCount).Select(
					x =>
						new SkillModel
						{
							SkillId = x.SkillId,
							SkillType = x.SkillType,
							SkillName = x.SkillName,
							DemandPercentile = x.SkillCount
						}).Take(criteria.ListSize).ToList();

			if (softwareSkills.Count > 0)
			{
				employerSkills.AddRange(softwareSkills);
			}

			var foundationSkills =
				query.Where(x => x.SkillType == SkillTypes.Foundation).OrderByDescending(x => x.SkillCount).Select(
					x =>
						new SkillModel
						{
							SkillId = x.SkillId,
							SkillType = x.SkillType,
							SkillName = x.SkillName,
							DemandPercentile = x.SkillCount
						}).Take(criteria.ListSize).ToList();

			if (foundationSkills.Count > 0)
			{
				employerSkills.AddRange(foundationSkills);
			}
			return employerSkills;
		}

		/// <summary>
		/// Gets the employer skills.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		private List<SkillModel> GetEmployerSkills(ExplorerCriteria criteria)
		{
			var employerSkills = new List<SkillModel>();

			var query = from es in Repositories.Library.Query<EmployerSkillView>()
				where es.EmployerId == criteria.EmployerId
				select es;

			var specialisedSkills =
				query.Where(x => x.SkillType == SkillTypes.Specialized).OrderByDescending(x => x.Postings).Select(
					x =>
						new SkillModel
						{
							SkillId = x.SkillId,
							SkillType = x.SkillType,
							SkillName = x.SkillName,
							DemandPercentile = x.Postings
						}).Take(criteria.ListSize).ToList();

			if (specialisedSkills.Count > 0)
			{
				employerSkills.AddRange(specialisedSkills);
			}

			var softwareSkills =
				query.Where(x => x.SkillType == SkillTypes.Software).OrderByDescending(x => x.Postings).Select(
					x =>
						new SkillModel
						{
							SkillId = x.SkillId,
							SkillType = x.SkillType,
							SkillName = x.SkillName,
							DemandPercentile = x.Postings
						}).Take(criteria.ListSize).ToList();

			if (softwareSkills.Count > 0)
			{
				employerSkills.AddRange(softwareSkills);
			}

			var foundationSkills =
				query.Where(x => x.SkillType == SkillTypes.Foundation).OrderByDescending(x => x.Postings).Select(
					x =>
						new SkillModel
						{
							SkillId = x.SkillId,
							SkillType = x.SkillType,
							SkillName = x.SkillName,
							DemandPercentile = x.Postings
						}).Take(criteria.ListSize).ToList();

			if (foundationSkills.Count > 0)
			{
				employerSkills.AddRange(foundationSkills);
			}
			return employerSkills;
		}

		/// <summary>
		/// Gets the employer jobs.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public EmployerJobResponse GetEmployerJobs(EmployerJobRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new EmployerJobResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, _validationLevel))
					return response;

				try
				{
					var query = from ej in Repositories.Library.Query<EmployerJobView>()
						where ej.EmployerId == request.Criteria.EmployerId
						      && ej.StateAreaId == request.Criteria.StateAreaId
						select ej;

					if (request.Criteria.CareerAreaId.HasValue) // Search by career area
					{
						query =
							query.Where(
								x =>
									(Repositories.Library.Query<JobCareerArea>().Where(y => y.CareerAreaId == request.Criteria.CareerAreaId.Value).Select(
										y => y.JobId)).Contains(x.JobId));
					}
					else if (request.Criteria.CareerAreaJobTitleId.HasValue) // Seach by job title
					{
						query =
							query.Where(
								x =>
									(Repositories.Library.Query<JobJobTitle>().Where(y => y.JobTitleId == request.Criteria.CareerAreaJobTitleId.Value)
										.Select(y => y.JobId)).Contains(x.JobId));
					}
					else if (request.Criteria.CareerAreaMilitaryOccupationROnetCodes.IsNotNullOrEmpty())
					{
						query =
							query.Where(
								x =>
									(Repositories.Library.Query<Data.Library.Entities.Job>().Where(j => request.Criteria.CareerAreaMilitaryOccupationROnetCodes.Contains(j.ROnet))
										.Select(j => j.Id)).Contains(x.JobId));
					}

					if (request.Criteria.DegreeEducationLevelId.HasValue) // Search by specific degree
					{
						query =
							query.Where(
								x =>
									(Repositories.Library.Query<JobDegreeEducationLevel>().Where(
										y => y.DegreeEducationLevelId == request.Criteria.DegreeEducationLevelId.Value).Select(
											y => y.JobId)).Contains(x.JobId));
					}
					else if (request.Criteria.DegreeLevel != DegreeLevels.AnyOrNoDegree) // Filter by degree type if necessary
					{
						query =
							query.Where(
								x =>
									(Repositories.Library.Query<JobDegreeLevel>().Where(
										y => y.DegreeLevel == request.Criteria.DegreeLevel).Select(
											y => y.JobId)).Contains(x.JobId));
					}
					if (request.Criteria.SkillCriteriaOption == SkillCriteriaOptions.SkillByCategory)
					{
						if (request.Criteria.SkillId.HasValue) // Filter by skill
						{
							query =
								query.Where(
									x =>
										(Repositories.Library.Query<Data.Library.Entities.JobSkill>().Where(
											y => y.SkillId == request.Criteria.SkillId).Select(
												y => y.JobId)).Contains(x.JobId));
						}
						else if (request.Criteria.SkillSubCategoryId.HasValue) // Filter by skill subcategory
						{
							query = query.Where(x => (from js in Repositories.Library.Query<Data.Library.Entities.JobSkill>()
								join sks in Repositories.Library.Query<SkillCategorySkill>() on js.SkillId equals
									sks.SkillId
								where sks.SkillCategoryId == request.Criteria.SkillSubCategoryId.Value
								select js.JobId).Contains(x.JobId));
						}
						else if (request.Criteria.SkillCategoryId.HasValue) // Filter by skill category
						{
							query = query.Where(x => (from js in Repositories.Library.Query<Data.Library.Entities.JobSkill>()
								join sks in Repositories.Library.Query<SkillCategorySkillView>() on js.SkillId equals
									sks.SkillId
								where sks.SkillCategoryParentId == request.Criteria.SkillCategoryId.Value
								select js.JobId).Contains(x.JobId));
						}
					}
					else if (request.Criteria.SkillCriteriaOption == SkillCriteriaOptions.SkillByJob) // Filter by skills in current job
					{
						if (request.Criteria.SkillJobSkillId.HasValue)
						{
							query =
								query.Where(
									x =>
										(Repositories.Library.Query<Data.Library.Entities.JobSkill>().Where(
											y => y.SkillId == request.Criteria.SkillJobSkillId).Select(
												y => y.JobId)).Contains(x.JobId));
						}
						else if (request.Criteria.SkillJobId.HasValue)
						{
							query = query.Where(x => (from js in Repositories.Library.Query<Data.Library.Entities.JobSkill>()
								join jsjs in Repositories.Library.Query<Data.Library.Entities.JobSkill>() on
									js.SkillId equals jsjs.SkillId
								where jsjs.JobId == request.Criteria.SkillJobId.Value
								select js.JobId).Contains(x.JobId));
						}
					}

					if (AppSettings.ExplorerOnlyShowJobsBasedOnClientDegrees)
					{

						query =
							query.Where(
								x =>
									(Repositories.Library.Query<CareerAreaJobDegreeView>().Where(
										y => y.IsClientData && y.ClientDataTag == AppSettings.ExplorerDegreeClientDataTag).Select(y => y.JobId))
										.Contains(x.JobId));
					}

					query = query.OrderByDescending(x => x.Positions);

					response.EmployerJobs = (from q in query
						select new ExplorerEmployerJobReportView(q.AsDto())).Take(request.Criteria.ListSize).ToList();

					if (AppSettings.Theme == FocusThemes.Education && AppSettings.ClientTag.IsNotNullOrEmpty())
					{
						var jobsFromCurrentSchool = GetCachedCurrentSchoolsDegreeJobs(request);
						foreach (var i in response.EmployerJobs.Where(x => jobsFromCurrentSchool.Contains(x.ReportData.JobId)))
						{
							i.RelatedDegreeAvailableAtCurrentSchool = true;
						}
					}
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the employer.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public EmployerResponse GetEmployer(EmployerRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new EmployerResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, _validationLevel))
					return response;

				try
				{
					response.Employer =
						Repositories.Library.Query<Data.Library.Entities.Employer>().Where(x => x.Id == request.EmployerId).Select(
							x => x.AsDto()).SingleOrDefault();
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the employer report.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public EmployerResponse GetEmployerReport(EmployerRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new EmployerResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, _validationLevel))
					return response;

				try
				{
					switch (request.Criteria.FetchOption)
					{
						case CriteriaBase.FetchOptions.Single:
							break;

						case CriteriaBase.FetchOptions.List:
							var criteria = request.Criteria.ReportCriteria;

							var query = from ej in Repositories.Library.Query<EmployerJobView>()
								select ej;

							query = query.Where(x => x.StateAreaId == criteria.StateAreaId);

							if (criteria.CareerAreaId.HasValue)
							{
								query =
									query.Where(
										x =>
											(Repositories.Library.Query<JobCareerArea>().Where(y => y.CareerAreaId == criteria.CareerAreaId.Value).Select(
												y => y.JobId)).Contains(x.JobId));
							}
							else if (criteria.CareerAreaJobId.HasValue)
							{
								query = query.Where(x => x.JobId == criteria.CareerAreaJobId);
							}
							else if (criteria.CareerAreaJobTitleId.HasValue)
							{
								query =
									query.Where(
										x =>
											(Repositories.Library.Query<JobJobTitle>().Where(y => y.JobTitleId == criteria.CareerAreaJobTitleId.Value)
												.Select(y => y.JobId)).Contains(x.JobId));
							}
							else if (criteria.CareerAreaMilitaryOccupationROnetCodes.IsNotNullOrEmpty())
							{
								query =
									query.Where(
										x =>
											(Repositories.Library.Query<Data.Library.Entities.Job>().Where(j => criteria.CareerAreaMilitaryOccupationROnetCodes.Contains(j.ROnet))
												.Select(j => j.Id)).Contains(x.JobId));
							}

							if (criteria.EducationCriteriaOption == EducationCriteriaOptions.DegreeLevel
							    && criteria.DegreeLevel != DegreeLevels.AnyOrNoDegree)
							{
								query =
									query.Where(
										x =>
											(Repositories.Library.Query<JobDegreeLevel>().Where(y => y.DegreeLevel == criteria.DegreeLevel).Select(
												y => y.JobId)).Contains(x.JobId));
							}
							else
							{
								if (criteria.DegreeEducationLevelId.HasValue)
								{
									query =
										query.Where(
											x =>
												(Repositories.Library.Query<JobDegreeEducationLevel>().Where(
													y => y.DegreeEducationLevelId == criteria.DegreeEducationLevelId.Value).Select(
														y => y.JobId)).Contains(x.JobId));
								}
							}

							if (criteria.EmployerId.HasValue)
							{
								query = query.Where(x => x.EmployerId == criteria.EmployerId.Value);
							}

							switch (criteria.SkillCriteriaOption)
							{
								case SkillCriteriaOptions.SkillByCategory:

									if (criteria.SkillId.HasValue)
									{
										query =
											query.Where(
												x =>
													(Repositories.Library.Query<Data.Library.Entities.JobSkill>().Where(y => y.SkillId == criteria.SkillId.Value).Select(
														y => y.JobId)).Contains(x.JobId));
									}
									else if (criteria.SkillSubCategoryId.HasValue)
									{
										query = query.Where(x => (from js in Repositories.Library.Query<Data.Library.Entities.JobSkill>()
											join sks in Repositories.Library.Query<SkillCategorySkill>() on js.SkillId equals
												sks.SkillId
											where sks.SkillCategoryId == criteria.SkillSubCategoryId.Value
											select js.JobId).Contains(x.JobId));

									}
									else if (criteria.SkillCategoryId.HasValue)
									{
										query = query.Where(x => (from js in Repositories.Library.Query<Data.Library.Entities.JobSkill>()
											join sks in Repositories.Library.Query<SkillCategorySkillView>() on js.SkillId equals
												sks.SkillId
											where sks.SkillCategoryParentId == criteria.SkillCategoryId.Value
											select js.JobId).Contains(x.JobId));
									}

									break;

								case SkillCriteriaOptions.SkillByJob:

									if (criteria.SkillJobSkillId.HasValue)
									{
										query =
											query.Where(
												x =>
													(Repositories.Library.Query<Data.Library.Entities.JobSkill>().Where(y => y.SkillId == criteria.SkillJobSkillId.Value).Select(
														y => y.JobId)).Contains(x.JobId));
									}
									else if (criteria.SkillJobId.HasValue)
									{
										query = query.Where(x => x.JobId == criteria.SkillJobId.Value);
									}

									break;
							}

							if (criteria.PrimaryJobId.HasValue)
							{
								query =
									query.Where(
										x => x.JobId == criteria.PrimaryJobId.Value ||
										     (Repositories.Library.Query<JobRelatedJob>().Where(y => y.JobId == criteria.PrimaryJobId.Value).Select(
											     y => y.RelatedJobId)).Contains(x.JobId));
							}

							var allEmployerJobs = query.Select(x => x.AsDto()).ToList();

							response.EmployerReports = (from ae in allEmployerJobs
								group ae by new { ae.EmployerId, ae.EmployerName }
								into g
								orderby g.Sum(ae => ae.Positions) descending
								select new EmployerJobViewDto
								{
									Id = g.Key.EmployerId,
									EmployerId = g.Key.EmployerId,
									EmployerName = g.Key.EmployerName,
									Positions = g.Sum(ae => ae.Positions)
								}).Take(request.Criteria.ListSize).ToList();

							break;
					}
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the job degree education levels and certifications.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public JobDegreeCertificationResponse GetJobDegreeCertifications(JobDegreeCertificationRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new JobDegreeCertificationResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, _validationLevel))
					return response;

				try
				{
					var jobDegreeCertificationModel = new JobDegreeCertificationModel();

					var query = from jdec in Repositories.Library.Query<JobDegreeEducationLevelView>()
						where jdec.JobId == request.Criteria.CareerAreaJobId && !jdec.ExcludeFromJob
						select jdec;

					switch (AppSettings.ExplorerDegreeFilteringType)
					{
						case DegreeFilteringType.Ours:
							query = query.Where(x => !x.IsClientData);
							break;

						case DegreeFilteringType.Theirs:
							query = query.Where(x => x.IsClientData && x.ClientDataTag == AppSettings.ExplorerDegreeClientDataTag);
							break;

						case DegreeFilteringType.Both:
						case DegreeFilteringType.TheirsThenOurs:
							query =
								query.Where(
									x =>
										!x.IsClientData ||
										(x.IsClientData && x.ClientDataTag == AppSettings.ExplorerDegreeClientDataTag));
							break;
					}

					var levels = query.Select(x => x.AsDto()).ToList();
					if (AppSettings.ExplorerDegreeFilteringType == DegreeFilteringType.TheirsThenOurs && levels.Exists(l => l.IsClientData))
						levels.RemoveAll(l => !l.IsClientData);

					jobDegreeCertificationModel.JobDegreeEducationLevels = levels;

					jobDegreeCertificationModel.JobCertifications =
						Repositories.Library.Query<JobCertificationView>().Where(x => x.JobId == request.Criteria.CareerAreaJobId).Select(x => x.AsDto())
							.ToList();

					response.JobDegreeCertifications = jobDegreeCertificationModel;
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the job degree education levels and certifications.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public JobDegreeCertificationResponse GetJobDegreeReportCertifications(JobDegreeCertificationRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new JobDegreeCertificationResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, _validationLevel))
					return response;

				try
				{
					var jobDegreeCertificationModel = new JobDegreeCertificationModel();

					var jobIdList = request.Criteria.CareerAreaJobIdList.IsNullOrEmpty()
						? new List<long> { request.Criteria.CareerAreaJobId.GetValueOrDefault(0) }
						: request.Criteria.CareerAreaJobIdList;

					var query = from jdec in Repositories.Library.Query<JobDegreeEducationLevelReportView>()
						where jobIdList.Contains(jdec.JobId) && !jdec.ExcludeFromJob && jdec.StateAreaId == request.Criteria.StateAreaId
						select jdec;

					switch (AppSettings.ExplorerDegreeFilteringType)
					{
						case DegreeFilteringType.Ours:
							query = query.Where(x => !x.IsClientData);
							break;

						case DegreeFilteringType.Theirs:
							query = query.Where(x => x.IsClientData && x.ClientDataTag == AppSettings.ExplorerDegreeClientDataTag);
							break;

						case DegreeFilteringType.Both:
						case DegreeFilteringType.TheirsThenOurs:
							query =
								query.Where(
									x =>
										!x.IsClientData ||
										(x.IsClientData && x.ClientDataTag == AppSettings.ExplorerDegreeClientDataTag));
							break;
					}

					jobDegreeCertificationModel.JobDegreeEducationLevelReports = query.Select(x => x.AsDto()).ToList();

					jobDegreeCertificationModel.JobCertifications =
						Repositories.Library.Query<JobCertificationView>().Where(x => jobIdList.Contains(x.JobId)).Select(x => x.AsDto())
							.ToList();

					response.JobDegreeCertifications = jobDegreeCertificationModel;
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the job skills.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public JobSkillResponse GetJobSkills(JobSkillRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new JobSkillResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, _validationLevel))
					return response;

				try
				{
					response.Skills =
						Repositories.Library.Query<JobSkillView>().Where(x => x.JobId == request.JobId).Select(x => x.AsDto()).ToList();
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the job employers
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public JobEmployerResponse GetJobEmployers(JobEmployerRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new JobEmployerResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, _validationLevel))
					return response;

				try
				{
					var criteria = request.Criteria;
					var query = from ej in Repositories.Library.Query<EmployerJobView>()
						where ej.JobId == criteria.CareerAreaJobId
						select ej;

					query = query.Where(x => x.StateAreaId == criteria.StateAreaId);

					if (request.Criteria.EmployerId.HasValue)
					{
						query = query.Where(x => x.EmployerId == request.Criteria.EmployerId.Value);
					}

					if (request.Criteria.DegreeEducationLevelId.HasValue) // Search by specific degree
					{
						query =
							query.Where(
								x =>
									(Repositories.Library.Query<JobDegreeEducationLevel>().Where(
										y => y.DegreeEducationLevelId == request.Criteria.DegreeEducationLevelId.Value).Select(
											y => y.JobId)).Contains(x.JobId));
					}
					else if (request.Criteria.DegreeLevel != DegreeLevels.AnyOrNoDegree) // Filter by degree type if necessary
					{
						query =
							query.Where(
								x =>
									(Repositories.Library.Query<JobDegreeLevel>().Where(
										y => y.DegreeLevel == request.Criteria.DegreeLevel).Select(
											y => y.JobId)).Contains(x.JobId));
					}
					if (criteria.SkillId.HasValue)
					{
						query =
							query.Where(
								x =>
									(Repositories.Library.Query<Data.Library.Entities.JobSkill>().Where(y => y.SkillId == criteria.SkillId.Value).Select(
										y => y.JobId)).Contains(x.JobId));
					}
					else if (request.Criteria.SkillSubCategoryId.HasValue) // Filter by skill subcategory
					{
						query = query.Where(x => (from js in Repositories.Library.Query<Data.Library.Entities.JobSkill>()
							join sks in Repositories.Library.Query<SkillCategorySkill>() on js.SkillId equals
								sks.SkillId
							where sks.SkillCategoryId == request.Criteria.SkillSubCategoryId.Value
							select js.JobId).Contains(x.JobId));
					}
					else if (request.Criteria.SkillCategoryId.HasValue) // Filter by skill category
					{
						query = query.Where(x => (from js in Repositories.Library.Query<Data.Library.Entities.JobSkill>()
							join sks in Repositories.Library.Query<SkillCategorySkillView>() on js.SkillId equals
								sks.SkillId
							where sks.SkillCategoryParentId == request.Criteria.SkillCategoryId.Value
							select js.JobId).Contains(x.JobId));
					}
					if (request.Criteria.SkillJobSkillId.HasValue)
					{
						query =
							query.Where(
								x =>
									(Repositories.Library.Query<Data.Library.Entities.JobSkill>().Where(
										y => y.SkillId == request.Criteria.SkillJobSkillId).Select(
											y => y.JobId)).Contains(x.JobId));
					}
					else if (request.Criteria.SkillJobId.HasValue)
					{
						query = query.Where(x => (from js in Repositories.Library.Query<Data.Library.Entities.JobSkill>()
							join jsjs in Repositories.Library.Query<Data.Library.Entities.JobSkill>() on
								js.SkillId equals jsjs.SkillId
							where jsjs.JobId == request.Criteria.SkillJobId.Value
							select js.JobId).Contains(x.JobId));
					}

					var allEmployers = query.Select(x => x.AsDto()).ToList();

					response.Employers = (from ej in allEmployers
						group ej by new { ej.EmployerId, ej.EmployerName }
						into g
						orderby g.Sum(ej => ej.Positions) descending
						select new EmployerJobViewDto()
						{
							EmployerId = g.Key.EmployerId,
							EmployerName = g.Key.EmployerName,
							Positions = g.Sum(ej => ej.Positions)
						}).Take(request.Criteria.ListSize).ToList();
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the job report.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public JobReportResponse GetJobReport(JobReportRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new JobReportResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, _validationLevel))
					return response;

				try
				{
					switch (request.Criteria.FetchOption)
					{
						case CriteriaBase.FetchOptions.Single:
						{
							var query = from jr in Repositories.Library.Query<JobReportView>()
								where jr.JobId == request.Criteria.CareerAreaJobId
								select jr;

							query = query.Where(x => x.StateAreaId == request.Criteria.StateAreaId);

							response.JobReport = query.Select(x => new ExplorerJobReportView(x.AsDto())).SingleOrDefault();

							if (AppSettings.Theme == FocusThemes.Education && AppSettings.ClientTag.IsNotNullOrEmpty() && response.JobReport.IsNotNull())
							{
								response.JobReport.RelatedDegreeAvailableAtCurrentSchool =
									GetCachedCurrentSchoolsDegreeJobs(request).Any(x => x == request.Criteria.CareerAreaJobId);
							}

							break;
						}

						case CriteriaBase.FetchOptions.List:
						{
							var criteria = request.Criteria;

							var query = from jr in Repositories.Library.Query<JobReportView>()
								select jr;

							// state / area
							query = query.Where(x => x.StateAreaId == criteria.StateAreaId);

							// career area
							if (criteria.CareerAreaId.HasValue)
							{
								query =
									query.Where(
										x =>
											(Repositories.Library.Query<JobCareerArea>().Where(y => y.CareerAreaId == criteria.CareerAreaId.Value).Select(
												y => y.JobId)).Contains(x.JobId));
							}
							else if (criteria.CareerAreaJobId.HasValue)
							{
								query = query.Where(x => x.JobId == criteria.CareerAreaJobId);
							}
							else if (criteria.CareerAreaJobTitleId.HasValue)
							{
								query =
									query.Where(
										x =>
											(Repositories.Library.Query<JobJobTitle>().Where(y => y.JobTitleId == criteria.CareerAreaJobTitleId.Value)
												.Select(y => y.JobId)).Contains(x.JobId));
							}
							else if (criteria.CareerAreaMilitaryOccupationROnetCodes.IsNotNullOrEmpty())
							{
								var numericROnets = criteria.CareerAreaMilitaryOccupationROnetCodes.Select(rOnet => rOnet.Replace("S", "9")).ToList();

								query = query.Where(x => (Repositories.Library.Query<Data.Library.Entities.Job>().Where(j => numericROnets.Contains(j.ROnet)).Select(j => j.Id)).Contains(x.JobId));
							}

							// education
							if (criteria.EducationCriteriaOption == EducationCriteriaOptions.DegreeLevel
							    && criteria.DegreeLevel != DegreeLevels.AnyOrNoDegree)
							{
								query =
									query.Where(
										x =>
											(Repositories.Library.Query<JobDegreeLevel>().Where(y => y.DegreeLevel == criteria.DegreeLevel).Select(
												y => y.JobId)).Contains(x.JobId));
							}
							else
							{
								if (criteria.DegreeEducationLevelId.HasValue)
								{
									query =
										query.Where(
											x =>
												(Repositories.Library.Query<JobDegreeEducationLevel>().Where(
													y => y.DegreeEducationLevelId == criteria.DegreeEducationLevelId.Value).Select(
														y => y.JobId)).Contains(x.JobId));
								}
							}

							// employer
							if (criteria.EmployerId.HasValue)
							{
								query =
									query.Where(
										x =>
											(Repositories.Library.Query<EmployerJob>().Where(y => y.EmployerId == criteria.EmployerId.Value).Select(
												y => y.JobId)).Contains(x.JobId));
							}

							// skills
							switch (criteria.SkillCriteriaOption)
							{
								case SkillCriteriaOptions.SkillByCategory:

									if (criteria.SkillId.HasValue)
									{
										query =
											query.Where(
												x =>
													(Repositories.Library.Query<Data.Library.Entities.JobSkill>().Where(y => y.SkillId == criteria.SkillId.Value && y.JobSkillType == JobSkillTypes.Demanded && y.Rank <= 20).Select(
														y => y.JobId)).Contains(x.JobId));
									}
									else if (criteria.SkillSubCategoryId.HasValue)
									{
										query = query.Where(x => (from js in Repositories.Library.Query<Data.Library.Entities.JobSkill>()
											join sks in Repositories.Library.Query<SkillCategorySkill>() on js.SkillId equals
												sks.SkillId
											where sks.SkillCategoryId == criteria.SkillSubCategoryId.Value
											select js.JobId).Contains(x.JobId));

									}
									else if (criteria.SkillCategoryId.HasValue)
									{
										query = query.Where(x => (from js in Repositories.Library.Query<Data.Library.Entities.JobSkill>()
											join sks in Repositories.Library.Query<SkillCategorySkillView>() on js.SkillId equals
												sks.SkillId
											where sks.SkillCategoryParentId == criteria.SkillCategoryId.Value
											select js.JobId).Contains(x.JobId));
									}

									break;

								case SkillCriteriaOptions.SkillByJob:

									if (criteria.SkillJobSkillId.HasValue)
									{
										query =
											query.Where(
												x =>
													(Repositories.Library.Query<Data.Library.Entities.JobSkill>().Where(y => y.SkillId == criteria.SkillJobSkillId.Value).Select(
														y => y.JobId)).Contains(x.JobId));
									}
									else if (criteria.SkillJobId.HasValue)
									{
										query = query.Where(x => x.JobId == criteria.SkillJobId.Value);
									}

									break;
							}

							if (criteria.PrimaryJobId.HasValue)
							{
								query =
									query.Where(
										x => x.JobId == criteria.PrimaryJobId.Value ||
										     (Repositories.Library.Query<JobRelatedJob>().Where(y => y.JobId == criteria.PrimaryJobId.Value).Select(
											     y => y.RelatedJobId)).Contains(x.JobId));
							}

							if (AppSettings.ExplorerOnlyShowJobsBasedOnClientDegrees)
							{
								query =
									query.Where(
										x =>
											(Repositories.Library.Query<CareerAreaJobDegreeView>().Where(
												y => y.IsClientData && y.ClientDataTag == AppSettings.ExplorerDegreeClientDataTag).Select(y => y.JobId))
												.Contains(x.JobId));
							}

							response.JobReports = (from q in query
								orderby q.HiringDemand descending
								select new ExplorerJobReportView(q.AsDto())).Take(request.Criteria.ListSize).ToList();

							if (AppSettings.Theme == FocusThemes.Education && AppSettings.ClientTag.IsNotNullOrEmpty())
							{
								var jobsFromCurrentSchool = GetCachedCurrentSchoolsDegreeJobs(request);
								foreach (var i in response.JobReports.Where(x => jobsFromCurrentSchool.Contains(x.ReportData.JobId)))
								{
									i.RelatedDegreeAvailableAtCurrentSchool = true;
								}
							}

							break;
						}
					}
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the similar job report.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public JobReportResponse GetSimilarJobReport(JobReportRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new JobReportResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, _validationLevel))
					return response;

				try
				{
					var criteria = request.Criteria;

					var query = (from jrj in Repositories.Library.Query<JobRelatedJob>()
						join jr in Repositories.Library.Query<JobReportView>() on jrj.RelatedJobId equals jr.JobId
						where jrj.JobId == criteria.CareerAreaJobId && jr.StateAreaId == criteria.StateAreaId
						orderby jr.HiringTrendPercentile descending
						select new JobReportViewDto
						{
							Id = jr.Id,
							HiringDemand = jr.HiringDemand,
							GrowthTrend = jr.GrowthTrend,
							GrowthPercentile = jr.GrowthPercentile,
							SalaryTrend = jr.SalaryTrend,
							SalaryTrendPercentile = jr.SalaryTrendPercentile,
							SalaryTrendAverage = jr.SalaryTrendAverage,
							Name = jr.Name,
							JobId = jr.JobId,
							HiringTrend = jr.HiringTrend,
							HiringTrendPercentile = jr.HiringTrendPercentile,
							SalaryTrendMin = jr.SalaryTrendMin,
							SalaryTrendMax = jr.SalaryTrendMax,
							SalaryTrendRealtime = jr.SalaryTrendRealtime,
							SalaryTrendRealtimeAverage = jr.SalaryTrendRealtimeAverage,
							SalaryTrendRealtimeMin = jr.SalaryTrendRealtimeMin,
							SalaryTrendRealtimeMax = jr.SalaryTrendRealtimeMax,
							StateAreaId = jr.StateAreaId,
							DegreeIntroStatement = jr.DegreeIntroStatement,
							StarterJob = jr.StarterJob
						});

					if (AppSettings.ExplorerOnlyShowJobsBasedOnClientDegrees)
					{
						query =
							query.Where(
								x =>
									(Repositories.Library.Query<CareerAreaJobDegreeView>().Where(
										y => y.IsClientData && y.ClientDataTag == AppSettings.ExplorerDegreeClientDataTag).Select(y => y.JobId))
										.Contains(x.JobId));
					}

					var jobs = query.Take(request.Criteria.ListSize).ToList();

					response.JobReports = jobs.Select(x => new ExplorerJobReportView(x)).ToList();

					if (AppSettings.Theme == FocusThemes.Education && AppSettings.ClientTag.IsNotNullOrEmpty())
					{
						var jobsFromCurrentSchool = GetCachedCurrentSchoolsDegreeJobs(request);
						foreach (var i in response.JobReports.Where(x => jobsFromCurrentSchool.Contains(x.ReportData.JobId)))
						{
							i.RelatedDegreeAvailableAtCurrentSchool = true;
						}
					}
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the employers.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public EmployerResponse GetEmployers(EmployerRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new EmployerResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, _validationLevel))
					return response;

				try
				{
					var employers = GetCachedEmployers(request);

					if (request.StateAreaId != 0)
					{
						var employerIds = GetCachedEmployersJobIds(request);

						employers = employers.Join(employerIds, x => x.Id, id => id, (x, id) => x).ToList();

						response.Employers = employers.Where(x => x.Name.ToLower().Contains(request.SearchTerm.ToLower())).Take(20).ToList();
					}
					else
					{
						response.Employers = employers.Where(x => x.Name.ToLower().Contains(request.SearchTerm.ToLower())).ToList();
					}
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		protected List<long> GetCachedEmployersJobIds(EmployerRequest request)
		{
			var stateAreaId = Cacher.Get<long>(Constants.CacheKeys.ExplorerStateAreaKey);
			var employerIdsCacheKey = String.Format(Constants.CacheKeys.ExplorerEmployersJobsKey, request.UserContext.Culture);

			List<long> employerJobIds;

			if (stateAreaId != request.StateAreaId)
			{
				using (Profiler.Profile(request.LogData()))
				{
					lock (SyncObject)
					{
						employerJobIds = Repositories.Library.Query<EmployerJob>()
							.Where(x => x.StateAreaId == request.StateAreaId)
							.Select(x => x.EmployerId).Distinct().ToList();

						Cacher.Set(Constants.CacheKeys.ExplorerStateAreaKey, request.StateAreaId);
						Cacher.Set(employerIdsCacheKey, employerJobIds);
					}
				}
			}

			employerJobIds = Cacher.Get<List<long>>(employerIdsCacheKey);

			return employerJobIds;
		}

		/// <summary>
		/// Gets the employers from the cache.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		protected List<EmployerDto> GetCachedEmployers(EmployerRequest request)
		{
			var employersKey = String.Format(Constants.CacheKeys.ExplorerEmployersKey, request.UserContext.Culture);
			var employers = Cacher.Get<List<EmployerDto>>(employersKey);

			if (employers == null)
			{
				using (Profiler.Profile(request.LogData()))
				{
					lock (SyncObject)
					{
						employers = Repositories.Library.Query<Data.Library.Entities.Employer>().Select(x => x.AsDto()).ToList();

						Cacher.Set(employersKey, employers);
					}
				}
			}

			return employers;
		}

		/// <summary>
		/// Gets the jobs.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public JobStateAreaResponse GetJobs(JobStateAreaRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new JobStateAreaResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, _validationLevel))
					return response;

				try
				{
					var jobs = GetCachedJobs(request);

					if (request.StateAreaId != 0)
					{
						var jobIds = Repositories.Library.Query<JobReport>().Where(x => x.StateAreaId == request.StateAreaId).Select(x => x.JobId).Distinct().ToList();

						response.Jobs = jobs.Where(x => x.Id != null && jobIds.Contains(x.Id.Value) && x.Name.ToLower().Contains(request.SearchTerm.ToLower())).ToList();
					}
					else
					{
						response.Jobs =
							jobs.Where(x => x.Name.ToLower().Contains(request.SearchTerm.ToLower())).ToList();
					}
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the jobs from the cache.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		protected List<JobDto> GetCachedJobs(JobStateAreaRequest request)
		{
			var jobsKey = String.Format(Constants.CacheKeys.ExplorerJobsKey, request.UserContext.Culture);
			var jobs = Cacher.Get<List<JobDto>>(jobsKey);

			if (jobs == null)
			{
				using (Profiler.Profile(request.LogData()))
				{
					lock (SyncObject)
					{
						jobs = Repositories.Library.Query<Data.Library.Entities.Job>().Select(x => x.AsDto()).ToList();

						Cacher.Set(jobsKey, jobs);
					}
				}
			}

			return jobs;
		}


		/// <summary>
		/// Gets the state areas.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public StateAreaResponse GetStateAreas(StateAreaRequest request)
		{
			var response = new StateAreaResponse(request);

			var validationLevel = _validationLevel;

			if (!request.UserContext.IsAuthenticated)
				validationLevel = _validationLevel = Validate.License | Validate.ClientTag;

			// Validate request
			if (!ValidateRequest(request, response, validationLevel))
				return response;

			try
			{
				response.StateAreas = GetCachedStateAreas(request);
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}

			return response;
		}

		/// <summary>
		/// Gets the state areas from the cache.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		protected List<StateAreaViewDto> GetCachedStateAreas(StateAreaRequest request)
		{
			var stateAreasKey = String.Format(Constants.CacheKeys.ExplorerStateAreasKey, request.UserContext.Culture);
			var stateAreas = Cacher.Get<List<StateAreaViewDto>>(stateAreasKey);

			if (stateAreas == null)
			{
				using (Profiler.Profile(request.LogData()))
				{
					lock (SyncObject)
					{
						stateAreas = Repositories.Library.Query<StateAreaView>().Select(x => x.AsDto()).ToList();

						Cacher.Set(stateAreasKey, stateAreas);
					}
				}
			}

			return stateAreas;
		}

		/// <summary>
		/// Gets the state areas from the cache.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		protected List<long> GetCachedCurrentSchoolsDegreeJobs(ServiceRequest request)
		{
			const string currentSchoolsDegreeJobs = Constants.CacheKeys.ExplorerCurrentSchoolsDegreeJobs;
			var jobIds = Cacher.Get<List<long>>(currentSchoolsDegreeJobs);

			if (jobIds == null)
			{
				using (Profiler.Profile(request.LogData()))
				{
					lock (SyncObject)
					{
						jobIds = Repositories.Library.Query<JobDegreeEducationLevelView>().Where(x => x.ClientDataTag == AppSettings.ExplorerDegreeClientDataTag).Select(x => x.JobId).Distinct().ToList();

						Cacher.Set(currentSchoolsDegreeJobs, jobIds);
					}
				}
			}

			return jobIds;
		}

		/// <summary>
		/// Gets a career area.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public CareerAreaResponse GetCareerArea(CareerAreaRequest request)
		{
			var response = new CareerAreaResponse(request);

			// Validate request
			if (!ValidateRequest(request, response, _validationLevel))
				return response;

			try
			{
				var careerAreas = GetCachedCareerAreas(request.UserContext.Culture, request.LogData());
				response.CareerArea = careerAreas.FirstOrDefault(careerArea => careerArea.Id == request.CareerAreaId);
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}

			return response;
		}

		/// <summary>
		/// Gets the career areas.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public CareerAreaResponse GetCareerAreas(CareerAreaRequest request)
		{
			var response = new CareerAreaResponse(request);

			// Validate request
			if (!ValidateRequest(request, response, _validationLevel))
				return response;

			try
			{
				response.CareerAreas = GetCachedCareerAreas(request.UserContext.Culture, request.LogData());
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}

			return response;
		}

		/// <summary>
		/// Gets the career areas from the cache.
		/// </summary>
		/// <param name="culture">The culture.</param>
		/// <param name="logData">The logger for logging data.</param>
		/// <returns></returns>
		protected List<CareerAreaDto> GetCachedCareerAreas(string culture, ILogData logData)
		{
			var careerAreasKey = String.Format(Constants.CacheKeys.ExplorerCareerAreasKey, culture);
			var careerAreas = Cacher.Get<List<CareerAreaDto>>(careerAreasKey);

			if (careerAreas == null)
			{
				using (Profiler.Profile(logData))
				{
					lock (SyncObject)
					{
						var query = Repositories.Library.Query<CareerArea>();

						if (AppSettings.ExplorerOnlyShowJobsBasedOnClientDegrees)
						{
							query =
								query.Where(
									x =>
										(Repositories.Library.Query<CareerAreaJobDegreeView>().Where(y => y.IsClientData && y.ClientDataTag == AppSettings.ExplorerDegreeClientDataTag).Select(y => y.CareerAreaId)).Contains(x.Id));
						}

						careerAreas = query.OrderBy(x => x.Name).Select(x => x.AsDto()).ToList();

						Cacher.Set(careerAreasKey, careerAreas);
					}
				}
			}

			return careerAreas;
		}

		/// <summary>
		/// Gets the jobs by the CareerAreaId passed in.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public CareerAreaJobResponse GetCareerAreaJobs(CareerAreaJobRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new CareerAreaJobResponse(request);

				if (!ValidateRequest(request, response, _validationLevel))
					return response;

				try
				{
					var careerAreaJobsKey = String.Format(Constants.CacheKeys.CareerAreaJobsKey);
					var careerAreaJobs = Cacher.Get<List<CareerAreaJobView>>(careerAreaJobsKey);

					if (careerAreaJobs == null)
					{
						using (Profiler.Profile(request.LogData()))
						{
							lock (SyncObject)
							{
								careerAreaJobs = (from j in Repositories.Library.Query<Data.Library.Entities.Job>()
									join jca in Repositories.Library.Query<JobCareerArea>() on j.Id equals jca.JobId
									orderby j.Name
									select new CareerAreaJobView { CareerAreaId = jca.CareerAreaId, JobId = j.Id, JobName = j.Name, ROnet = j.ROnet }).ToList();

								Cacher.Set(careerAreaJobsKey, careerAreaJobs);
							}
						}
					}

					careerAreaJobs = careerAreaJobs.Where(x => x.CareerAreaId == request.CareerAreaId).ToList();

					if (!request.FilterMappedROnets)
					{
						response.CareerAreaJobs = careerAreaJobs;
					}
					else
					{
						var filteredCareerAreaJobs = (from job in careerAreaJobs
							let onetId = (from r in Repositories.Library.ROnets
								join o in Repositories.Library.Onets on r.OnetId equals o.Id
								where r.Code == job.ROnet
								select o.Id).FirstOrDefault()
							let jobTasks =
								Repositories.Library.Query<JobTaskView>()
									.FirstOrDefault(x => x.OnetId == onetId)
							where jobTasks.IsNotNull()
							select job).ToList();

						response.CareerAreaJobs = filteredCareerAreaJobs;
					}
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the total jobs by the CareerAreaId passed in.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public CareerAreaJobTotalResponse GetCareerAreaJobTotals(CareerAreaJobTotalRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new CareerAreaJobTotalResponse(request);

				if (!ValidateRequest(request, response, _validationLevel))
					return response;

				try
				{
					var criteria = request.Criteria;

					var query = Repositories.Library.Query<CareerAreaJobReportView>().Where(ca => ca.StateAreaId == criteria.StateAreaId);

					if (request.Criteria.DegreeEducationLevelId.IsNotNull())
					{
						query = query.Join(Repositories.Library.JobDegreeEducationLevelViews.Where(degree => degree.DegreeEducationLevelId == criteria.DegreeEducationLevelId),
							jobView => jobView.JobId,
							degreeView => degreeView.JobId,
							(jobView, degreeView) => jobView);
					}
					else
					{
						if (AppSettings.ExplorerOnlyShowJobsBasedOnClientDegrees)
						{
							var educationLevels = Enum.GetValues(typeof(ExplorerEducationLevels)).Cast<ExplorerEducationLevels>().ToList();
							switch (criteria.DegreeLevel)
							{
								case DegreeLevels.LessThanBachelors:
									educationLevels = educationLevels.Where(x => (int)x < (int)ExplorerEducationLevels.Bachelor).ToList();
									break;
								case DegreeLevels.BachelorsOrHigher:
									educationLevels = educationLevels.Where(x => (int)x >= (int)ExplorerEducationLevels.Bachelor).ToList();
									break;
							}
							query = query.Where(area => (Repositories.Library.JobDegreeEducationLevelViews
								.Where(degree => degree.JobId == area.JobId && educationLevels.Contains(degree.EducationLevel) && degree.IsClientData && degree.ClientDataTag == AppSettings.ExplorerDegreeClientDataTag)
								.Select(y => y.JobId)).Contains(area.JobId));

						}
						else if (criteria.EducationCriteriaOption == EducationCriteriaOptions.DegreeLevel && criteria.DegreeLevel != DegreeLevels.AnyOrNoDegree)
						{
							query = query.Where(x => (Repositories.Library.Query<JobDegreeLevel>().Where(y => y.DegreeLevel == criteria.DegreeLevel).Select(y => y.JobId)).Contains(x.JobId));
						}
					}

					var careerAreaJobs = query.ToList();
					var careerAreas = GetCachedCareerAreas(request.UserContext.Culture, request.LogData()).ToDictionary(ca => ca.Id, ca => ca.Name);

					response.CareerAreaJobTotals = careerAreaJobs.GroupBy(ca => ca.CareerAreaId)
						.Select(grp =>
							new CareerAreaJobTotalView
							{
								CareerAreaId = grp.Key,
								CareerName = careerAreas[grp.Key],
								TotalJobs = grp.Sum(ca => ca.HiringDemand)
							})
						.OrderByDescending(ca => ca.TotalJobs)
						.Take(request.Criteria.ListSize)
						.ToList();
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		public JobCareerAreaResponse GetJobCareerAreas(JobCareerAreaRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new JobCareerAreaResponse(request);

				if (!ValidateRequest(request, response, _validationLevel))
					return response;

				try
				{

					response.CareerAreas =
						Repositories.Library.Query<JobCareerArea>().Where(x => x.JobId == request.JobId).Select(x => x.CareerAreaId).ToList
							();
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the degree education levels.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public DegreeEducationLevelResponse GetDegreeAliases(DegreeEducationLevelRequest request)
		{
			var response = new DegreeEducationLevelResponse(request);

			var validationLevel = _validationLevel;

			if (!request.UserContext.IsAuthenticated)
				validationLevel = _validationLevel = Validate.License | Validate.ClientTag;

			// Validate request
			if (!ValidateRequest(request, response, validationLevel))
				return response;

			try
			{
				var degreeAliases = GetCachedDegreeAliases(request);

				var query = from del in degreeAliases
					select del;

				if (request.SearchCriteria == null)
				{
					response.DegreeAliases = query.ToList();
					return response;
				}

				query = query.Where(del => del.Alias.Contains(request.SearchCriteria.SearchTerm, StringComparison.InvariantCultureIgnoreCase));

				if (request.SearchCriteria.StateAreaId != 0)
				{
					// Get degrees specific to the area chosen
					var degreesByStateAreaIds = GetCachedDegreeEducationLevelDegreeIdsByStateArea(request);

					query = query.Join(degreesByStateAreaIds, d => d.DegreeId, degreeId => degreeId, (d, degreeId) => d);
				}

				// Search client specific data if required
				if (request.SearchCriteria.SearchClientDataOnly.HasValue && request.SearchCriteria.SearchClientDataOnly.Value)
				{
					query = query.Where(x => x.IsClientData && x.ClientDataTag == AppSettings.ExplorerDegreeClientDataTag);
				}
				else
				{
					switch (AppSettings.ExplorerDegreeFilteringType)
					{
						case DegreeFilteringType.Ours:
							query = query.Where(x => !x.IsClientData);
							break;

						case DegreeFilteringType.Theirs:
							query = query.Where(x => x.IsClientData && x.ClientDataTag == AppSettings.ExplorerDegreeClientDataTag);
							break;

						case DegreeFilteringType.Both:
						case DegreeFilteringType.TheirsThenOurs:
							query =
								query.Where(
									x =>
										!x.IsClientData ||
										(x.IsClientData && x.ClientDataTag == AppSettings.ExplorerDegreeClientDataTag));
							break;
					}
				}

				// Search by degree range
				switch (request.SearchCriteria.DetailedDegreeLevel)
				{
					case DetailedDegreeLevels.CertificateOrAssociate:
						query = query.Where(x => x.EducationLevel == ExplorerEducationLevels.Certificate || x.EducationLevel == ExplorerEducationLevels.Associate);
						break;

					case DetailedDegreeLevels.Bachelors:
						query = query.Where(x => x.EducationLevel == ExplorerEducationLevels.Bachelor);
						break;

					case DetailedDegreeLevels.GraduateOrProfessional:
						query = query.Where(x => x.EducationLevel > ExplorerEducationLevels.Bachelor);
						break;
				}

				if (request.SearchCriteria.ListSize > 0)
				{
					query = query.Take(request.SearchCriteria.ListSize);
				}

				response.DegreeAliases = query.ToList();
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}

			return response;
		}

		/// <summary>
		/// Gets the degree aliases from the cache.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		protected List<DegreeAliasViewDto> GetCachedDegreeAliases(DegreeEducationLevelRequest request)
		{
			var degreeAliasKey = String.Format(Constants.CacheKeys.ExplorerDegreeAliasKey, request.UserContext.Culture);
			var degreeAliases = Cacher.Get<List<DegreeAliasViewDto>>(degreeAliasKey);

			if (degreeAliases == null)
			{
				using (Profiler.Profile(request.LogData()))
				{
					lock (SyncObject)
					{
						var query = from del in Repositories.Library.Query<DegreeAliasView>()
							select del;

						switch (AppSettings.ExplorerDegreeFilteringType)
						{
							case DegreeFilteringType.Ours:
								query = query.Where(x => !x.IsClientData);
								break;

							case DegreeFilteringType.Theirs:
								query = query.Where(x => x.IsClientData && x.ClientDataTag == AppSettings.ExplorerDegreeClientDataTag);
								break;

							case DegreeFilteringType.Both:
							case DegreeFilteringType.TheirsThenOurs:
								query =
									query.Where(
										x =>
											!x.IsClientData ||
											(x.IsClientData && x.ClientDataTag == AppSettings.ExplorerDegreeClientDataTag));
								break;
						}

						degreeAliases = query.Select(x => x.AsDto()).ToList();

						Cacher.Set(degreeAliasKey, degreeAliases);
					}
				}
			}

			return degreeAliases;
		}

		/// <summary>
		/// Gets the degree aliases from the cache.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		protected List<long> GetCachedDegreeEducationLevelDegreeIdsByStateArea(DegreeEducationLevelRequest request)
		{
			var delStateAreaKey = String.Format(Constants.CacheKeys.ExplorerDegreeEducationLevelStateAreaViewKey, request.SearchCriteria.StateAreaId, request.SearchCriteria.DetailedDegreeLevel, request.UserContext.Culture);
			var degreeIds = Cacher.Get<List<long>>(delStateAreaKey);

			if (degreeIds == null)
			{
				using (Profiler.Profile(request.LogData()))
				{
					lock (SyncObject)
					{
						var query = Repositories.Library.Query<DegreeEducationLevelStateAreaView>()
							.Where(d => d.StateAreaId == request.SearchCriteria.StateAreaId);

						// Search by degree range
						switch (request.SearchCriteria.DetailedDegreeLevel)
						{
							case DetailedDegreeLevels.CertificateOrAssociate:
								query = query.Where(x => x.EducationLevel == ExplorerEducationLevels.Certificate || x.EducationLevel == ExplorerEducationLevels.Associate);
								break;

							case DetailedDegreeLevels.Bachelors:
								query = query.Where(x => x.EducationLevel == ExplorerEducationLevels.Bachelor);
								break;

							case DetailedDegreeLevels.GraduateOrProfessional:
								query = query.Where(x => x.EducationLevel > ExplorerEducationLevels.Bachelor);
								break;
						}
						degreeIds = query.Select(d => d.DegreeId).Distinct().ToList();
						Cacher.Set(delStateAreaKey, degreeIds);
					}
				}
			}

			return degreeIds;
		}

		/// <summary>
		/// Gets the job titles.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public JobTitleResponse GetJobTitles(JobTitleRequest request)
		{
			var response = new JobTitleResponse(request);

			// Validate request
			if (!ValidateRequest(request, response, _validationLevel))
				return response;

			try
			{
				var jobTitles = GetCachedJobTitles(request);

				if (request.JobTitleId.HasValue)
				{
					response.JobTitle = jobTitles.SingleOrDefault(x => x.Id == request.JobTitleId.Value);
				}
				else if (!String.IsNullOrEmpty(request.SearchTerm))
				{

					response.JobTitles = jobTitles.Where(x => x.Name.ToLower().Contains(request.SearchTerm.ToLower())).ToList();
				}
				else
				{
					response.JobTitles = jobTitles;
				}
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}

			return response;
		}

		/// <summary>
		/// Gets the job titles from the cache.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		protected List<JobTitleDto> GetCachedJobTitles(JobTitleRequest request)
		{
			var jobTitleKey = String.Format(Constants.CacheKeys.ExplorerJobTitlesKey, request.UserContext.Culture);
			var jobTitles = Cacher.Get<List<JobTitleDto>>(jobTitleKey);

			if (jobTitles == null)
			{
				using (Profiler.Profile(request.LogData()))
				{
					lock (SyncObject)
					{
						jobTitles = Repositories.Library.Query<JobTitle>().Select(x => x.AsDto()).ToList();

						if (AppSettings.ExplorerOnlyShowJobsBasedOnClientDegrees)
						{
							var jobTitleIds =
								Repositories.Library.Query<JobJobTitle>().Where(
									x =>
										(Repositories.Library.Query<CareerAreaJobDegreeView>().Where(
											y => y.IsClientData && y.ClientDataTag == AppSettings.ExplorerDegreeClientDataTag).Select(
												y => y.JobId)).Contains(x.JobId)).Select(x => x.JobTitleId).Distinct().ToList();

							var filtered = jobTitles.Where(jobTitle => jobTitleIds.Contains(jobTitle.Id)).ToList();
							jobTitles = filtered;
						}


						Cacher.Set(jobTitleKey, jobTitles);
					}
				}
			}

			return jobTitles;
		}

		/// <summary>
		/// Gets the skill categories.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public SkillCategoryResponse GetSkillCategories(SkillCategoryRequest request)
		{
			var response = new SkillCategoryResponse(request);

			// Validate request
			if (!ValidateRequest(request, response, _validationLevel))
				return response;

			try
			{
				response.SkillCategories = GetCachedSkillCategories(request);
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}

			return response;
		}

		/// <summary>
		/// Gets the skill categories from cache.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		protected List<SkillCategoryDto> GetCachedSkillCategories(SkillCategoryRequest request)
		{
			var skillCategoriesKey = String.Format(Constants.CacheKeys.ExplorerSkillCategoriesKey, request.UserContext.Culture);
			var skillCategories = Cacher.Get<List<SkillCategoryDto>>(skillCategoriesKey);

			if (skillCategories == null)
			{
				using (Profiler.Profile(request.LogData()))
				{
					lock (SyncObject)
					{
						skillCategories = Repositories.Library.Query<SkillCategory>().Select(x => x.AsDto()).ToList();

						Cacher.Set(skillCategoriesKey, skillCategories);
					}
				}
			}

			return skillCategories;
		}

		/// <summary>
		/// Gets the skills.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public SkillResponse GetSkills(SkillRequest request)
		{
			var response = new SkillResponse(request);

			// Validate request
			if (!ValidateRequest(request, response, _validationLevel))
				return response;

			try
			{
				response.Skills = GetCachedSkills(request);
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}

			return response;
		}

		/// <summary>
		/// Gets the skills from cache.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		protected List<SkillCategorySkillViewDto> GetCachedSkills(SkillRequest request)
		{
			var skillsKey = String.Format(Constants.CacheKeys.ExplorerSkillsKey, request.UserContext.Culture);
			var skills = Cacher.Get<List<SkillCategorySkillViewDto>>(skillsKey);

			if (skills == null)
			{
				using (Profiler.Profile(request.LogData()))
				{
					lock (SyncObject)
					{
						skills = Repositories.Library.Query<SkillCategorySkillView>().Select(x => x.AsDto()).ToList();

						Cacher.Set(skillsKey, skills);
					}
				}
			}

			return skills;
		}

		/// <summary>
		/// Gets skills for which jobs exist
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public SkillResponse GetSkillsForJobs(SkillRequest request)
		{
			var response = new SkillResponse(request);

			// Validate request
			if (!ValidateRequest(request, response, _validationLevel))
				return response;

			try
			{
				response.SkillsForJobs = GetCachedSkillsForJobs(request);
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}

			return response;
		}

		/// <summary>
		/// Gets the skills (that have jobs) from cache.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		protected List<SkillsForJobsViewDto> GetCachedSkillsForJobs(SkillRequest request)
		{
			var skillsKey = String.Format(Constants.CacheKeys.ExplorerSkillsForJobsKey, request.UserContext.Culture);
			var skills = Cacher.Get<List<SkillsForJobsViewDto>>(skillsKey);

			if (skills == null)
			{
				using (Profiler.Profile(request.LogData()))
				{
					skills = Repositories.Library.Query<SkillsForJobsView>().Select(s => s.AsDto()).ToList();

					lock (SyncObject)
					{
						if (Cacher.Get<List<SkillModel>>(skillsKey) == null)
							Cacher.Set(skillsKey, skills);
					}
				}
			}

			if (request.TermSearchCriteria != null)
			{
				var criteria = request.TermSearchCriteria;

				return skills.Where(s => s.Name.Contains(criteria.Term, StringComparison.InvariantCultureIgnoreCase) && s.Rank < criteria.MaxRank)
					.Take(criteria.ListSize)
					.OrderBy(s => s.Name)
					.ToList();
			}

			return skills;
		}

		/// <summary>
		/// Gets the my account model.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public MyAccountResponse GetMyAccountModel(MyAccountRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new MyAccountResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var user = Repositories.Core.FindById<User>(request.UserContext.UserId);
					var userSecurityQuestion1 = Repositories.Core.UserSecurityQuestions.FirstOrDefault(usq => usq.UserId == user.Id && usq.QuestionIndex == 1);
					var userSecurityQuestion = userSecurityQuestion1.IsNull() || userSecurityQuestion1.SecurityQuestion.IsNullOrWhitespace() ? string.Empty : userSecurityQuestion1.SecurityQuestion;
					var userSecurityQuestionId = userSecurityQuestion1.IsNull() ? null : userSecurityQuestion1.SecurityQuestionId;

					if (user.IsNotNull())
					{
						response.Model = new MyAccountModel
						{
							ScreenName = user.ScreenName,
							SecurityQuestion = userSecurityQuestion,
							SecurityQuestionId = userSecurityQuestionId
						};

						if (user.Person.Resumes.IsNotNullOrEmpty())
						{
							response.Model.ResumeModel = new ExplorerResumeModel
							{
								Resume = user.Person.Resumes[0].AsDto(),
								ResumeDocument = (user.Person.Resumes[0].ResumeDocument.IsNotNull()) ? user.Person.Resumes[0].ResumeDocument.AsDto() : null
							};
						}
					}
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the my resume model.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public MyResumeResponse GetMyResumeModel(MyResumeRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new MyResumeResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					// Get last uploaded resume (Explorer users only have one resume)
					var resume = Repositories.Core.Query<Resume>().OrderByDescending(x => x.UpdatedOn).FirstOrDefault(x => x.PersonId == request.UserContext.PersonId.Value && x.IsDefault);
					if (resume.IsNotNull())
					{
						response.Model = new MyResumeModel
						{
							ResumeModel = new ExplorerResumeModel
							{
								Resume = resume.AsDto(),
								ResumeDocument = (resume.ResumeDocument.IsNotNull()) ? resume.ResumeDocument.AsDto() : null
							}
						};

						if (!String.IsNullOrEmpty(resume.PrimaryROnet))
						{
							var job = Repositories.Library.Query<Data.Library.Entities.Job>().SingleOrDefault(x => x.ROnet == resume.PrimaryROnet);
							if (job.IsNotNull())
							{
								response.Model.PrimaryJobId = job.Id;
							}
						}
					}
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the related jobs.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public RelatedJobsResponse GetRelatedJobs(RelatedJobsRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new RelatedJobsResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, _validationLevel))
					return response;

				try
				{

					var jobIds = (from jjt in Repositories.Library.Query<JobJobTitle>()
						join jrj in Repositories.Library.Query<JobRelatedJob>() on jjt.JobId equals jrj.JobId
						where jjt.JobTitleId == request.JobTitleId
						select jrj.RelatedJobId).Distinct().ToList();
					jobIds.AddRange(
						Repositories.Library.Query<JobJobTitle>().Where(x => x.JobTitleId == request.JobTitleId).Select(x => x.JobId).
							ToList());

					jobIds = jobIds.Distinct().ToList();

					var jobsQuery = from j in Repositories.Library.Query<Data.Library.Entities.Job>()
						where jobIds.Contains(j.Id)
						select j;

					if (AppSettings.ExplorerOnlyShowJobsBasedOnClientDegrees)
					{
						jobsQuery =
							jobsQuery.Where(
								x => (Repositories.Library.Query<CareerAreaJobDegreeView>().Where(
									y => y.IsClientData && y.ClientDataTag == AppSettings.ExplorerDegreeClientDataTag).Select(y => y.JobId))
									.Contains(x.Id));
					}

					response.Jobs = jobsQuery.OrderBy(x => x.Name).Select(x => x.AsDto()).Distinct().ToList();

				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the job description.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public JobIntroductionResponse GetJobIntroduction(JobIntroductionRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new JobIntroductionResponse(request);
				// Validate request
				if (!ValidateRequest(request, response, _validationLevel))
					return response;

				response.JobDescription =
					Repositories.Library.Query<Data.Library.Entities.Job>().Where(x => x.Id == request.JobId).Select(
						x => x.Description).SingleOrDefault();

				return response;
			}
		}


		/// <summary>
		/// Gets the job description.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public ProgramAreaResponse GetROnetProgramsOfStudy(ProgramAreaRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new ProgramAreaResponse(request);
				// Validate request
				if (!ValidateRequest(request, response, _validationLevel))
					return response;

				//response.JobDescription =
				//  Repositories.Library.Query<Data.Library.Entities.Job>().Where(x => x.Id == request.JobId).Select(
				//    x => x.Description).SingleOrDefault();

				return response;
			}
		}

		/// <summary>
		/// Gets the program areas.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns>A response containing a list of program areas</returns>
		public ProgramAreaResponse GetProgramAreas(ProgramAreaRequest request)
		{
			var response = new ProgramAreaResponse(request);

			var validationLevel = _validationLevel;

			if (!request.UserContext.IsAuthenticated)
				validationLevel = _validationLevel = Validate.License | Validate.ClientTag;

			// Validate request
			if (!ValidateRequest(request, response, validationLevel))
				return response;

			try
			{
				var programAreas = GetCachedProgramAreas(request);

				if (request.ProgramAreaId.HasValue)
					response.ProgramArea = programAreas.First(pa => pa.Id == request.ProgramAreaId);
				else
					response.ProgramAreas = programAreas;
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}

			return response;
		}

		/// <summary>
		/// Gets the program areas from the cache.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns>A list of program areas</returns>
		protected List<ProgramAreaDto> GetCachedProgramAreas(ProgramAreaRequest request)
		{
			var programAreasKey = String.Format(Constants.CacheKeys.ExplorerProgramAreasKey, request.UserContext.Culture);
			var programAreas = Cacher.Get<List<ProgramAreaDto>>(programAreasKey);

			if (programAreas == null)
			{
				using (Profiler.Profile(request.LogData()))
				{
					lock (SyncObject)
					{
						var query = Repositories.Library.Query<ProgramArea>();

						query = AppSettings.ExplorerDegreeFilteringType == DegreeFilteringType.Ours
								? query.Where(x => !x.IsClientData)
								: query.Where(x => x.IsClientData && x.ClientDataTag == AppSettings.ExplorerDegreeClientDataTag);

						programAreas = query.OrderBy(pa => pa.Name).Select(pa => pa.AsDto()).ToList();
						Cacher.Set(programAreasKey, programAreas);
					}
				}
			}

			return programAreas;
		}

		/// <summary>
		/// Gets either a specific or list of degrees (for a program area)
		/// </summary>
		/// <param name="request">The request for the degrees.</param>
		/// <returns>The response containing either the specific degree or list of degrees for a program area</returns>
		public DegreeResponse GetDegrees(DegreeRequest request)
		{
			var response = new DegreeResponse(request);

			// Validate request
			if (!ValidateRequest(request, response, _validationLevel))
				return response;

			try
			{
				if (request.ProgramAreaId.HasValue)
				{
					var query = from pad in Repositories.Library.Query<ProgramAreaDegree>()
						join d in Repositories.Library.Query<Degree>() on pad.DegreeId equals d.Id
						where pad.ProgramAreaId == request.ProgramAreaId
						select d.AsDto();
					response.Degrees = query.ToList();
				}
				else
				{
					response.Degree = Repositories.Library.FindById<Degree>(request.DegreeId.GetValueOrDefault(0)).AsDto();
				}
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}

			return response;
		}

		/// <summary>
		/// Gets the program areas/degrees.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns>A response containing a list of program areas/degrees</returns>
		public ProgramAreaResponse GetProgramAreaDegrees(ProgramAreaRequest request)
		{
			var response = new ProgramAreaResponse(request);

			// Validate request
			if (!ValidateRequest(request, response, _validationLevel))
				return response;

			try
			{
				if (request.GetEducationLevels)
				{
					var educationLevels = GetCachedProgramAreaDegreeEducationLevels(request);

					if (request.Criteria.IsNotNull())
					{
						var criteria = request.Criteria;
						var query = Repositories.Library.Query<JobDegreeEducationLevel>();

						if (criteria.StateAreaId.IsNotNull())
						{
							query =
								query.Where(
									x =>
										(Repositories.Library.Query<JobReport>().Where(y => y.StateAreaId == criteria.StateAreaId).Select(
											y => y.JobId)).Contains(x.JobId));
						}

						if (request.Criteria.CareerAreaId.HasValue) // Search by career area
						{
							query =
								query.Where(
									x =>
										(Repositories.Library.Query<JobCareerArea>().Where(y => y.CareerAreaId == request.Criteria.CareerAreaId.Value).Select(
											y => y.JobId)).Contains(x.JobId));
						}
						else if (request.Criteria.CareerAreaJobTitleId.HasValue) // Seach by job title
						{
							query =
								query.Where(
									x =>
										(Repositories.Library.Query<JobJobTitle>().Where(y => y.JobTitleId == request.Criteria.CareerAreaJobTitleId.Value)
											.Select(y => y.JobId)).Contains(x.JobId));
						}
						else if (criteria.CareerAreaMilitaryOccupationROnetCodes.IsNotNullOrEmpty())
						{
							query =
								query.Where(
									x =>
										(Repositories.Library.Query<Data.Library.Entities.Job>().Where(j => criteria.CareerAreaMilitaryOccupationROnetCodes.Contains(j.ROnet))
											.Select(j => j.Id)).Contains(x.JobId));
						}

						if (request.Criteria.DegreeEducationLevelId.HasValue) // Search by specific degree
						{
							query =
								query.Where(
									x =>
										(Repositories.Library.Query<JobDegreeEducationLevel>().Where(
											y => y.DegreeEducationLevelId == request.Criteria.DegreeEducationLevelId.Value).Select(
												y => y.JobId)).Contains(x.JobId));
						}
						else if (request.Criteria.DegreeLevel != DegreeLevels.AnyOrNoDegree && request.CheckDegreeLevelByJob.GetValueOrDefault(true)) // Filter by degree type if necessary
						{
							query =
								query.Where(
									x =>
										(Repositories.Library.Query<JobDegreeLevel>().Where(
											y => y.DegreeLevel == request.Criteria.DegreeLevel).Select(
												y => y.JobId)).Contains(x.JobId));
						}

						if (request.Criteria.DegreeId.HasValue)
						{
							if (request.Criteria.DegreeId.Value > 0)
								query = query.Where(x => (from jdel in Repositories.Library.Query<JobDegreeEducationLevel>()
									join del in Repositories.Library.Query<DegreeEducationLevel>()
										on jdel.DegreeEducationLevelId equals del.Id
									where del.DegreeId == request.Criteria.DegreeId.Value
									select jdel.JobId).Contains(x.JobId));
							else
								query = query.Where(x => (from jdel in Repositories.Library.Query<JobDegreeEducationLevel>()
									join del in Repositories.Library.Query<DegreeEducationLevel>()
										on jdel.DegreeEducationLevelId equals del.Id
									join pad in Repositories.Library.Query<ProgramAreaDegree>()
										on del.DegreeId equals pad.DegreeId
									where pad.ProgramAreaId == request.Criteria.ProgramAreaId.Value
									select jdel.JobId).Contains(x.JobId));
						}

						if (request.Criteria.SkillCriteriaOption == SkillCriteriaOptions.SkillByCategory)
						{
							if (request.Criteria.SkillId.HasValue) // Filter by skill
							{
								query =
									query.Where(
										x =>
											(Repositories.Library.Query<Data.Library.Entities.JobSkill>().Where(
												y => y.SkillId == request.Criteria.SkillId).Select(
													y => y.JobId)).Contains(x.JobId));
							}
							else if (request.Criteria.SkillSubCategoryId.HasValue) // Filter by skill subcategory
							{
								query = query.Where(x => (from js in Repositories.Library.Query<Data.Library.Entities.JobSkill>()
									join sks in Repositories.Library.Query<SkillCategorySkill>() on js.SkillId equals
										sks.SkillId
									where sks.SkillCategoryId == request.Criteria.SkillSubCategoryId.Value
									select js.JobId).Contains(x.JobId));
							}
							else if (request.Criteria.SkillCategoryId.HasValue) // Filter by skill category
							{
								query = query.Where(x => (from js in Repositories.Library.Query<Data.Library.Entities.JobSkill>()
									join sks in Repositories.Library.Query<SkillCategorySkillView>() on js.SkillId equals
										sks.SkillId
									where sks.SkillCategoryParentId == request.Criteria.SkillCategoryId.Value
									select js.JobId).Contains(x.JobId));
							}
						}
						else if (request.Criteria.SkillCriteriaOption == SkillCriteriaOptions.SkillByJob) // Filter by skills in current job
						{
							if (request.Criteria.SkillJobSkillId.HasValue)
							{
								query =
									query.Where(
										x =>
											(Repositories.Library.Query<Data.Library.Entities.JobSkill>().Where(
												y => y.SkillId == request.Criteria.SkillJobSkillId).Select(
													y => y.JobId)).Contains(x.JobId));
							}
							else if (request.Criteria.SkillJobId.HasValue)
							{
								query = query.Where(x => (from js in Repositories.Library.Query<Data.Library.Entities.JobSkill>()
									join jsjs in Repositories.Library.Query<Data.Library.Entities.JobSkill>() on
										js.SkillId equals jsjs.SkillId
									where jsjs.JobId == request.Criteria.SkillJobId.Value
									select js.JobId).Contains(x.JobId));
							}
						}

						var filteredDegrees = query.Select(x => x.AsDto()).ToList();
						educationLevels =
							educationLevels.Where(
								x => (filteredDegrees.Select(y => y.DegreeEducationLevelId)).Contains(x.DegreeEducationLevelId)).ToList();
					}

					if (request.DetailedDegreeLevel.HasValue)
					{
						// Search by degree level
						switch (request.DetailedDegreeLevel)
						{
							case DetailedDegreeLevels.CertificateOrAssociate:
								educationLevels = educationLevels.Where(x => x.EducationLevel == ExplorerEducationLevels.Certificate || x.EducationLevel == ExplorerEducationLevels.Associate).ToList();
								break;

							case DetailedDegreeLevels.Bachelors:
								educationLevels = educationLevels.Where(x => x.EducationLevel == ExplorerEducationLevels.Bachelor).ToList();
								break;

							case DetailedDegreeLevels.GraduateOrProfessional:
								educationLevels = educationLevels.Where(x => x.EducationLevel > ExplorerEducationLevels.Bachelor).ToList();
								break;
						}
					}
					else if (request.Criteria.IsNotNull() && !request.CheckDegreeLevelByJob.GetValueOrDefault(true))
					{
						switch (request.Criteria.DegreeLevel)
						{
							case DegreeLevels.LessThanBachelors:
								educationLevels = educationLevels.Where(x => (int)x.EducationLevel < (int)ExplorerEducationLevels.Bachelor).ToList();
								break;

							case DegreeLevels.BachelorsOrHigher:
								educationLevels = educationLevels.Where(x => (int)x.EducationLevel >= (int)ExplorerEducationLevels.Bachelor).ToList();
								break;
						}
					}
					response.ProgramAreaDegreeEducationLevels = request.ProgramAreaId.HasValue
						? educationLevels.Where(p => p.ProgramAreaId == request.ProgramAreaId).ToList()
						: educationLevels;
				}
				else
				{
					var degrees = GetCachedProgramAreaDegrees(request);

					response.ProgramAreaDegrees = request.ProgramAreaId.HasValue
						? degrees.Where(p => p.ProgramAreaId == request.ProgramAreaId).ToList()
						: degrees;
				}
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}

			return response;
		}


		/// <summary>
		/// Gets the program areas/degrees.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns>A response containing a list of program areas/degrees</returns>
		public ProgramAreaResponse GetROnetProgramAreaDegrees(ProgramAreaRequest request)
		{
			var response = new ProgramAreaResponse(request);

			// Validate request
			if (!ValidateRequest(request, response, _validationLevel))
				return response;

			try
			{
				var ronetCode = (from r in Repositories.Library.ROnets
					where r.Id == request.Criteria.ROnetId
					select r.Code).SingleOrDefault();

				var jobDegreeEducationLevels = (from jdelv in Repositories.Library.JobDegreeEducationLevelViews
					where jdelv.ROnet == ronetCode && jdelv.IsClientData && jdelv.ClientDataTag == AppSettings.ExplorerDegreeClientDataTag
					select jdelv
					).ToList();

				var programAreaDegreeEducationLevels =
					jobDegreeEducationLevels.Select(jobDegreeEducationLevel => new ProgramAreaDegreeEducationLevelViewDto
					{
						DegreeEducationLevelId = jobDegreeEducationLevel.DegreeEducationLevelId,
						DegreeEducationLevelName = jobDegreeEducationLevel.DegreeEducationLevelName,
						DegreeId = jobDegreeEducationLevel.DegreeId,
						DegreeName = jobDegreeEducationLevel.DegreeName,
						Tier = jobDegreeEducationLevel.Tier
					}).ToList();
				response.ProgramAreaDegreeEducationLevels = programAreaDegreeEducationLevels;
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}

			return response;
		}

		/// <summary>
		/// Gets the degrees from the cache for program areas
		/// </summary>
		/// <param name="request">The request object</param>
		/// <returns>A list of degrees for the program area degrees</returns>
		protected List<ProgramAreaDegreeViewDto> GetCachedProgramAreaDegrees(ProgramAreaRequest request)
		{
			var degreesKey = String.Format(Constants.CacheKeys.ExplorerProgramAreaDegreesKey, request.UserContext.Culture);
			var degrees = Cacher.Get<List<ProgramAreaDegreeViewDto>>(degreesKey);

			if (degrees == null)
			{
				using (Profiler.Profile(request.LogData()))
				{
					var query = Repositories.Library.Query<ProgramAreaDegreeView>();

					switch (AppSettings.ExplorerDegreeFilteringType)
					{
						case DegreeFilteringType.Ours:
							query = query.Where(x => !x.IsClientData);
							break;

						case DegreeFilteringType.Theirs:
							query = query.Where(x => x.IsClientData && x.ClientDataTag == AppSettings.ExplorerDegreeClientDataTag);
							break;

						case DegreeFilteringType.Both:
						case DegreeFilteringType.TheirsThenOurs:
							query = query.Where(x => !x.IsClientData || x.ClientDataTag == AppSettings.ExplorerDegreeClientDataTag);
							break;
					}
					lock (SyncObject)
					{
						degrees = query.Select(d => d.AsDto()).ToList();
						Cacher.Set(degreesKey, degrees);
					}
				}
			}

			return degrees;
		}

		/// <summary>
		/// Gets the degree education levels from the cache for program areas
		/// </summary>
		/// <param name="request">The request object</param>
		/// <returns>A list of degrees for the program area/degree education levels</returns>
		protected List<ProgramAreaDegreeEducationLevelViewDto> GetCachedProgramAreaDegreeEducationLevels(ProgramAreaRequest request)
		{
			var educationLevelKey = String.Format(Constants.CacheKeys.ExplorerProgramAreaDegreeducationLevelsKey, request.UserContext.Culture);
			var educationLevels = Cacher.Get<List<ProgramAreaDegreeEducationLevelViewDto>>(educationLevelKey);

			if (educationLevels == null)
			{
				using (Profiler.Profile(request.LogData()))
				{
					var query = Repositories.Library.Query<ProgramAreaDegreeEducationLevelView>();

					query = (AppSettings.ExplorerDegreeFilteringType == DegreeFilteringType.Ours)
						? query.Where(x => !x.IsClientData)
						: query.Where(x => x.IsClientData && x.ClientDataTag == AppSettings.ExplorerDegreeClientDataTag);

					lock (SyncObject)
					{
						educationLevels = query.Select(el => el.AsDto()).ToList();
						Cacher.Set(educationLevelKey, educationLevels);
					}
				}
			}

			return educationLevels;
		}

		#region Explorer Utility Method

		/// <summary>
		/// Get the Job Id from ROnet
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns>JobId</returns>
		public JobFromROnetResponse GetJobFromROnet(JobFromROnetRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new JobFromROnetResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, _validationLevel))
					return response;

				try
				{
					if (request.ROnets.IsNullOrEmpty())
					{
						var matchingJob = Repositories.Library.Query<Data.Library.Entities.Job>().FirstOrDefault(x => x.ROnet == request.ROnet);
						response.Job = matchingJob == null ? null : matchingJob.AsDto();
					}
					else
					{
						var matchingJobs = Repositories.Library.Query<Data.Library.Entities.Job>().Where(x => request.ROnets.Contains(x.ROnet)).Select(j => j.AsDto());
						response.Jobs = matchingJobs.ToList();
					}
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Get ROnet from Resume XML
		/// </summary>
		/// <param name="request">Resume XML</param>
		/// <returns>ROnet</returns>
		public ROnetFromResumeResponse GetROnetFromResume(ROnetFromResumeRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new ROnetFromResumeResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, _validationLevel))
					return response;

				try
				{
					response.ROnet = OnetToROnet.ConvertResumeXmlToROnet(request.ResumeXml, request);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the R onets from job.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public JobIdToROnetResponse GetROnetsFromJob(JobIdToROnetRequest request)
		{

			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new JobIdToROnetResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, _validationLevel))
					return response;

				try
				{
					response.ROnet = Repositories.Library.Jobs.Where(x => x.Id == request.JobId).Select(x => x.ROnet).SingleOrDefault();
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the onet from alpha R onet.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public ROnetToOnetResponse GetOnetFromROnet(ROnetToOnetRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new ROnetToOnetResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, _validationLevel))
					return response;

				try
				{
					response.OnetDetails = new List<OnetDetailsView>();

					var onetIds = (from r in Repositories.Library.Query<ROnet>()
						join o in Repositories.Library.Query<OnetROnet>() on r.Id equals o.ROnetId
						where r.Code == request.ROnet
						select o.OnetId).ToList();

					response.OnetDetails = onetIds.Select(id => Helpers.Occupation.GetOnetById(id, request.UserContext.Culture))
						.Where(onet => onet.IsNotNull())
						.ToList();
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}
				return response;
			}
		}

		/// <summary>
		/// Gets the SOCs for an Onet
		/// </summary>
		/// <param name="request">The request containing the OnetID</param>
		/// <returns>The response containing the SOCs</returns>
		public OnetToSOCResponse GetSOCForOnet(OnetToSOCRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new OnetToSOCResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, _validationLevel))
					return response;

				try
				{
					// ReSharper disable once InconsistentNaming
					var socs = (from onetSOC in Repositories.Library.OnetSOCs
						join soc in Repositories.Library.SOCs
							on onetSOC.SocId equals soc.Id
						where onetSOC.OnetId == request.OnetId
						select soc.AsDto()).ToList();

					response.SOCs = socs;
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}
				return response;
			}
		}

		#endregion

		#endregion
	}
}
