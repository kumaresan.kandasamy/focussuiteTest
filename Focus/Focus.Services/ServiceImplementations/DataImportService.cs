﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using Focus.Core;
using Focus.Core.Criteria.JobCertificate;
using Focus.Core.Criteria.JobLanguage;
using Focus.Core.Criteria.JobLicence;
using Focus.Core.Criteria.JobLocation;
using Focus.Core.Criteria.JobProgramsOfStudy;
using Focus.Core.Criteria.JobSpecialRequirement;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Messages;
using Focus.Core.Messages.AccountService;
using Focus.Core.Messages.AnnotationService;
using Focus.Core.Messages.CandidateService;
using Focus.Core.Messages.CoreService;
using Focus.Core.Messages.EmployerService;
using Focus.Core.Messages.DataImportService;
using Focus.Core.Messages.JobService;
using Focus.Core.Messages.OrganizationService;
using Focus.Core.Messages.PostingService;
using Focus.Core.Messages.ResumeService;
using Focus.Core.Messages.SearchService;
using Focus.Core.Models;
using Focus.Data.Core.Entities;
using Focus.Data.Migration.Entities;
using Focus.Services.Core;
using Focus.Services.DtoMappers;
using Focus.Services.ServiceContracts;

using Framework.Core;
using Framework.Logging;
using JetBrains.Annotations;
using Microsoft.SqlServer.Server;
using Mindscape.LightSpeed;
using Mindscape.LightSpeed.Querying;

#endregion

namespace Focus.Services.ServiceImplementations
{
	public class DataImportService : ServiceBase, IDataImportService
	{
		protected IAccountService AccountService { get { return new AccountService(); } }
		protected IAnnotationService AnnotationService { get { return new AnnotationService(); } }
		protected ICandidateService CandidateService { get { return new CandidateService(); } }
		protected ICoreService CoreService { get { return new CoreService(); } }
		protected IOrganizationService OrganizationService { get { return new OrganizationService(); } }
		protected IEmployerService EmployerService { get { return new EmployerService(); } }
		protected IJobService JobService { get { return new JobService(); } }
		protected IResumeService ResumeService { get { return new ResumeService(); } }
		protected IPostingService PostingService { get { return new PostingService(); } }
		protected ISearchService SearchService { get { return new SearchService(); } }

		protected static readonly object SyncObject = new object();

		/// <summary>
		/// Initializes a new instance of the <see cref="DataImportService" /> class.
		/// </summary>
		public DataImportService() : this(null)
		{
			LookupEmployerRequestFunc = LookupEmployerRequest;
			DeserializeRequestFunc = DeserializeRequest;
			SaveBusinessUnitModelFunc = SaveBusinessUnitModel;
			UpdateBusinessUnitFunc = UpdateBusinessUnit;
			DeserializeSaveJobRequestFunc = DeserializeSaveJobRequest;
			GetJobOrderRequestByExternalIdFunc = GetJobOrderRequestByExternalId;
			GetEmployerRequestByEmployeeMigrationIdFunc = GetEmployerRequestByEmployeeMigrationId;
			PrepareServiceRequestPropertiesFunc = PrepareServiceRequestPropertiesWrapper;
			SaveJobFunc = SaveJob;
			PopulateSaveJobRequestFunc = PopulateSaveJobRequest;
			ProcessJobAddressAction = ProcessJobAddress;
			ProcessJobCertificateAction = ProcessJobCertificate;
			ProcessJobLanguageAction = ProcessJobLanguage;
			ProcessJobLicenceAction = ProcessJobLicence;
			ProcessJobLocationAction = ProcessJobLocation;
			ProcessJobProgramOfStudyAction = ProcessJobProgramOfStudy;
			ProcessJobSpecialRequirementAction = ProcessJobSpecialRequirement;
			LookupJobOrderRequestIdsByParentFunc = LookupJobOrderRequestIdsByParent;
			GenerateJobPostingFunc = GenerateJobPosting;
			DeserializeRegisterTalentUserRequestFunc = DeserializeRegisterTalentUserRequest;
			RegisterTalentUserFunc = RegisterTalentUser;
			CheckEmployerServiceResponseWrapperFunc = CheckEmployerServiceResponseWrapper;
			LookupEmployerRequestIdsByParentFunc = LookupEmployerRequestIdsByParent;
			ProcessBusinessUnitLogoFunc = ProcessBusinessUnitLogo;
			ProcessBusinessUnitDescriptionFunc = ProcessBusinessUnitDescription;
			ProcessEmployerTradeNamesFunc = ProcessEmployerTradeNames;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="DataImportService" /> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		public DataImportService(IRuntimeContext runtimeContext) : base(runtimeContext)
		{
			LookupEmployerRequestFunc = LookupEmployerRequest;
			DeserializeRequestFunc = DeserializeRequest;
			SaveBusinessUnitModelFunc = SaveBusinessUnitModel;
			UpdateBusinessUnitFunc = UpdateBusinessUnit;
			DeserializeSaveJobRequestFunc = DeserializeSaveJobRequest;
			GetJobOrderRequestByExternalIdFunc = GetJobOrderRequestByExternalId;
			GetEmployerRequestByEmployeeMigrationIdFunc = GetEmployerRequestByEmployeeMigrationId;
			PrepareServiceRequestPropertiesFunc = PrepareServiceRequestPropertiesWrapper;
			SaveJobFunc = SaveJob;
			PopulateSaveJobRequestFunc = PopulateSaveJobRequest;
			CheckJobOrderServiceResponseAction = CheckJobOrderServiceResponse;
			ProcessJobAddressAction = ProcessJobAddress;
			ProcessJobCertificateAction = ProcessJobCertificate;
			ProcessJobLanguageAction = ProcessJobLanguage;
			ProcessJobLicenceAction = ProcessJobLicence;
			ProcessJobLocationAction = ProcessJobLocation;
			ProcessJobProgramOfStudyAction = ProcessJobProgramOfStudy;
			ProcessJobSpecialRequirementAction = ProcessJobSpecialRequirement;
			LookupJobOrderRequestIdsByParentFunc = LookupJobOrderRequestIdsByParent;
			GenerateJobPostingFunc = GenerateJobPosting;
			DeserializeRegisterTalentUserRequestFunc = DeserializeRegisterTalentUserRequest;
			RegisterTalentUserFunc = RegisterTalentUser;
			CheckEmployerServiceResponseWrapperFunc = CheckEmployerServiceResponseWrapper;
			LookupEmployerRequestIdsByParentFunc = LookupEmployerRequestIdsByParent;
			ProcessBusinessUnitLogoFunc = ProcessBusinessUnitLogo;
			ProcessBusinessUnitDescriptionFunc = ProcessBusinessUnitDescription;
			ProcessEmployerTradeNamesFunc = ProcessEmployerTradeNames;
		}

		#region Employers

		/// <summary>
		/// Sends an employer request from the migration database to the focus database.
		/// </summary>
		/// <param name="request">The import request object.</param>
		public EmployerImportResponse ProcessEmployer([NotNull] EmployerImportRequest request)
		{
			if (request == null) throw new ArgumentNullException("request");
			var response = new EmployerImportResponse(request);

			try
			{
				RegisterTalentUserRequest employerAndEmployeeDetails;
				var migrationRequest = GetDetailsForEmployerAndEmployee(request, out employerAndEmployeeDetails);

				var callServiceLayerForMigratedEmployer = CallServiceLayerForMigratedEmployerAndEmployee(migrationRequest, employerAndEmployeeDetails);

				if (callServiceLayerForMigratedEmployer)
				{
					// A negative business unit indicates this needs to be looked up rather than add a new one
					if (employerAndEmployeeDetails.BusinessUnit.Id.HasValue && employerAndEmployeeDetails.BusinessUnit.Id < 0)
					{
						GetEmployerToMigrate(migrationRequest, employerAndEmployeeDetails);
					}

					var accountResponse = CallServiceForEmployerAndEmployeeToMigrate(request, employerAndEmployeeDetails, migrationRequest);

					CheckEmployerServiceResponseWrapperFunc(accountResponse, migrationRequest);

					if (response.Acknowledgement == AcknowledgementType.Success)
					{
						ProcessMigratedEmployerDependentObjects(request, migrationRequest, employerAndEmployeeDetails);
					}

					response.Acknowledgement = accountResponse.Acknowledgement;
				}
				else
				{
					migrationRequest.Status = MigrationStatus.Failed;
					migrationRequest.Message = "Unable to find original request to update";
				}

				migrationRequest.ProcessedBy = DateTime.UtcNow;
				Repositories.Migration.SaveChanges(true);

				SaveSourceEmployeeCreatedOnAndUpdatedOnDates(employerAndEmployeeDetails);

				SaveSourceEmployerCreatedOnAndUpdatedOnDates(employerAndEmployeeDetails);
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}
			return response;
		}

		public void SaveSourceEmployerCreatedOnAndUpdatedOnDates(RegisterTalentUserRequest employerAndEmployeeDetails)
		{
			if (employerAndEmployeeDetails.Employer.IsNotNull() &&
			    (employerAndEmployeeDetails.OverrideEmployerCreatedOn.IsNotNullOrDefault() ||
			     employerAndEmployeeDetails.OverrideEmployerUpdatedOn.IsNotNullOrDefault()))
			{
				var changes = new Dictionary<string, object>();

				if (employerAndEmployeeDetails.OverrideEmployerCreatedOn.IsNotNullOrDefault())
					changes.Add("CreatedOn", employerAndEmployeeDetails.OverrideEmployerCreatedOn);

				if (employerAndEmployeeDetails.OverrideEmployerUpdatedOn.IsNotNullOrDefault())
					changes.Add("UpdatedOn", employerAndEmployeeDetails.OverrideEmployerUpdatedOn);

				var updateQuery = new Query(typeof(Employer),
					Entity.Attribute("FederalEmployerIdentificationNumber") ==
					employerAndEmployeeDetails.Employer.FederalEmployerIdentificationNumber);
				Repositories.Core.Update(updateQuery, changes);
				Repositories.Core.SaveChanges();
			}
		}

		public void SaveSourceEmployeeCreatedOnAndUpdatedOnDates(RegisterTalentUserRequest employerAndEmployeeDetails)
		{
			if (employerAndEmployeeDetails.User.IsNotNull() &&
			    (employerAndEmployeeDetails.OverrideUserCreatedOn.IsNotNullOrDefault() ||
			     employerAndEmployeeDetails.OverrideUserUpdatedOn.IsNotNullOrDefault()))
			{
				var changes = new Dictionary<string, object>();

				if (employerAndEmployeeDetails.OverrideUserCreatedOn.IsNotNullOrDefault())
					changes.Add("CreatedOn", employerAndEmployeeDetails.OverrideUserCreatedOn);

				if (employerAndEmployeeDetails.OverrideUserUpdatedOn.IsNotNullOrDefault())
					changes.Add("UpdatedOn", employerAndEmployeeDetails.OverrideUserUpdatedOn);

				var updateQuery = new Query(typeof(User), Entity.Attribute("UserName") == employerAndEmployeeDetails.User.UserName);
				Repositories.Core.Update(updateQuery, changes);
				Repositories.Core.SaveChanges();
			}
		}

		private EmployerRequest GetDetailsForEmployerAndEmployee(EmployerImportRequest request,
			out RegisterTalentUserRequest talentRequest)
		{
			var migrationRequest = Repositories.Migration.FindById<EmployerRequest>(request.EmployerRequestId);

			talentRequest = DeserializeRegisterTalentUserRequestFunc(migrationRequest);
			return migrationRequest;
		}

		private void ProcessMigratedEmployerDependentObjects(EmployerImportRequest request, EmployerRequest migrationRequest,
			RegisterTalentUserRequest talentRequest)
		{
			migrationRequest.EmailRequired = !migrationRequest.IsUpdate;

			var originalRequest = request.EmployerRequestId;

			var unitDescriptionRequestIds = LookupEmployerRequestIdsByParentFunc(talentRequest.EmployeeMigrationId, MigrationEmployerRecordType.BusinessUnitDescription);
			if (unitDescriptionRequestIds.IsNotNullOrEmpty())
			{
				unitDescriptionRequestIds.ForEach(unitDescriptionRequestId =>
				{
					request.EmployerRequestId = unitDescriptionRequestId;
					ProcessBusinessUnitDescriptionFunc(request, false, migrationRequest.FocusId);
				});
			}

			var unitLogoRequestIds = LookupEmployerRequestIdsByParentFunc(talentRequest.EmployeeMigrationId,
				MigrationEmployerRecordType.BusinessUnitLogo);
			if (unitLogoRequestIds.IsNotNullOrEmpty())
			{
				unitLogoRequestIds.ForEach(unitLogoRequestId =>
				{
					request.EmployerRequestId = unitLogoRequestId;
					ProcessBusinessUnitLogoFunc(request, false, migrationRequest.FocusId);
				});
			}
			request.EmployerRequestId = originalRequest;
		}

		private Employer GetEmployerToMigrate(EmployerRequest migrationRequest, RegisterTalentUserRequest talentRequest)
		{
			Employer employer;
			if (migrationRequest.IsUpdate)
			{
				employer = (from employeeRecord in Repositories.Core.Employees
					join employerRecord in Repositories.Core.Employers
						on employeeRecord.EmployerId equals employerRecord.Id
					where employeeRecord.Id == talentRequest.UpdateExistingEmployeeId
					select employerRecord).FirstOrDefault();
			}
			else
			{
				employer =
					Repositories.Core.Employers.FirstOrDefault(
						e =>
							e.FederalEmployerIdentificationNumber == talentRequest.Employer.FederalEmployerIdentificationNumber &&
							(!e.Archived));
			}

			if (employer != null)
			{
				var businessUnit = employer.BusinessUnits.FirstOrDefault(bu => bu.IsPrimary);
				talentRequest.BusinessUnit.Id = (businessUnit == null) ? 0 : businessUnit.Id;
			}
			else
			{
				talentRequest.BusinessUnit.Id = 0;
			}
			return employer;
		}

		private RegisterTalentUserResponse CallServiceForEmployerAndEmployeeToMigrate(EmployerImportRequest request,
			RegisterTalentUserRequest talentRequest, EmployerRequest migrationRequest)
		{
			talentRequest.IgnoreClient = request.IgnoreClient;

			PrepareServiceRequestPropertiesFunc(talentRequest, request);

			RegisterTalentUserResponse accountResponse = null;

			var employerQuery =
				Repositories.Core.Employers.Where(
					x => x.Id == migrationRequest.FocusId && migrationRequest.Status == MigrationStatus.Processed);
			if (migrationRequest.IsUpdate || !employerQuery.Any())
			{
				lock (SyncObject)
				{
					if (migrationRequest.IsUpdate || !employerQuery.Any())
					{
						accountResponse = migrationRequest.IsUpdate
							? AccountService.UpdateTalentUser(talentRequest)
							: RegisterTalentUserFunc(talentRequest);
					}
				}
			}
			Repositories.Core.SaveChanges(true);
			return accountResponse;
		}

		private bool CallServiceLayerForMigratedEmployerAndEmployee(EmployerRequest migrationRequest, RegisterTalentUserRequest talentRequest)
		{
			var callService = true;
			if (migrationRequest.IsUpdate)
			{
				var originalRequest = LookupEmployerRequestFunc(migrationRequest.ExternalId);
				if (originalRequest.IsNotNull())
				{
					talentRequest.UpdateExistingEmployeeId = originalRequest.FocusId;

					//Password should not be updated
					talentRequest.AccountPassword = null;
				}
				else
				{
					callService = false;
				}
			}
			return callService;
		}

		public Action<RegisterTalentUserResponse, EmployerRequest> CheckEmployerServiceResponseWrapperFunc;

		private static void CheckEmployerServiceResponseWrapper(RegisterTalentUserResponse accountResponse,
			EmployerRequest migrationRequest)
		{
			CheckEmployerServiceResponse(accountResponse, migrationRequest, accountResponse.EmployeeId);
		}

		public Func<RegisterTalentUserRequest, RegisterTalentUserResponse> RegisterTalentUserFunc;

		private RegisterTalentUserResponse RegisterTalentUser(RegisterTalentUserRequest talentRequest)
		{
			return AccountService.RegisterTalentUser(talentRequest);
		}

		public Func<EmployerRequest, RegisterTalentUserRequest> DeserializeRegisterTalentUserRequestFunc;

		private static RegisterTalentUserRequest DeserializeRegisterTalentUserRequest(EmployerRequest migrationRequest)
		{
			return migrationRequest.Request.DataContractDeserialize<RegisterTalentUserRequest>();
		}

		public Func<EmployerRequest, EmployerTradeNamesRequest> DeserializeRequestFunc;

		public EmployerTradeNamesRequest DeserializeRequest(EmployerRequest migrationRequest)
		{
			return migrationRequest.Request.DataContractDeserialize<EmployerTradeNamesRequest>();
		}

		public Func<SaveBusinessUnitModelRequest, SaveBusinessUnitModelResponse> SaveBusinessUnitModelFunc;

		private SaveBusinessUnitModelResponse SaveBusinessUnitModel(SaveBusinessUnitModelRequest bumRequest)
		{
			return EmployerService.SaveBusinessUnitModel(bumRequest);
		}

		public Func<EmployerImportRequest, bool, long, EmployerImportResponse> ProcessEmployerTradeNamesFunc;

		public EmployerImportResponse ProcessEmployerTradeNames([NotNull] EmployerImportRequest request, bool resetRepository = true, long employeeId = 0)
		{
			if (request == null) throw new ArgumentNullException("request");

			var response = new EmployerImportResponse(request);
			long employerId = 0;
			EmployerRequest migrationRequest = null;

			try
			{
				// get the employer trade name details to use for the employerid and the business unit name
				var descriptionRequest = GetEmployerTradeNamesRequest(request, ref migrationRequest);

				var employer = new Employer();
				employer = GetEmployer(employeeId, descriptionRequest, employerId, employer, migrationRequest, response);

				// update the business unit if specified
				if (migrationRequest.IsUpdate)
				{
					if (UpdateBusinessUnitFunc(employer, descriptionRequest))
					{
						migrationRequest.Status = MigrationStatus.Processed;
					}
					else
					{
						migrationRequest.Status = MigrationStatus.Failed;
						response.Acknowledgement = AcknowledgementType.Failure;
					}
				}
				else
				{
					var bumRequest = CreateBusinessUnitModelRequest(request, employer, descriptionRequest);

					var bumResponse = SaveBusinessUnitModelFunc(bumRequest);

					if (bumResponse.Acknowledgement == AcknowledgementType.Success)
					{
						migrationRequest.FocusId = bumResponse.Model.BusinessUnit.Id.HasValue ? (long)bumResponse.Model.BusinessUnit.Id : -1;
						migrationRequest.Status = MigrationStatus.Processed;
						response.Acknowledgement = bumResponse.Acknowledgement;
					}
					else
					{
						migrationRequest.Status = MigrationStatus.Failed;
						migrationRequest.Message = "Unable to save Business Unit Address";
						response.Acknowledgement = bumResponse.Acknowledgement;
					}
				}
				migrationRequest.ProcessedBy = DateTime.UtcNow;
				Repositories.Migration.SaveChanges(true);
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}
			return response;
		}

		private static SaveBusinessUnitModelRequest CreateBusinessUnitModelRequest(EmployerImportRequest request,
			Employer employer, EmployerTradeNamesRequest descriptionRequest)
		{
			var bu = new BusinessUnitDto
			{
				AccountTypeId = null,
				EmployerId = employer.Id,
				AlternatePhone1 = employer.AlternatePhone1,
				AlternatePhone1Type = employer.AlternatePhone1Type,
				AlternatePhone2 = employer.AlternatePhone2,
				AlternatePhone2Type = employer.AlternatePhone2Type,
				IndustrialClassification = employer.IndustrialClassification,
				Name =
					descriptionRequest.TradeName.IsNotNullOrEmpty()
						? descriptionRequest.TradeName
						: descriptionRequest.EmployerLegalName,
				LegalName = descriptionRequest.TradeName,
				PrimaryPhoneType = employer.PrimaryPhoneType,
				PrimaryPhone = employer.PrimaryPhone,
				PrimaryPhoneExtension = employer.PrimaryPhoneExtension,
			};

			var buRequest = new SaveBusinessUnitRequest
			{
			  BusinessUnit = bu,
			  ClientTag = request.ClientTag,
			  SessionId = request.SessionId,
			  RequestId = request.RequestId,
			  UserContext = request.UserContext,
			};

			var bua = new BusinessUnitAddressDto
			{
				Line1 = descriptionRequest.Line1,
				Line2 = descriptionRequest.Line2,
				PostcodeZip = descriptionRequest.PostalCode,
				TownCity = descriptionRequest.City,
				IsPrimary = true,
				/* Only one address is relevant to employer tradenames, so must be primary */
			};

			var bumRequest = new SaveBusinessUnitModelRequest
			{
				ClientTag = request.ClientTag,
				SessionId = request.SessionId,
				RequestId = request.RequestId,
				UserContext = request.UserContext,
				Model = new BusinessUnitModel { BusinessUnit = bu, BusinessUnitAddress = bua }
			};
			return bumRequest;
		}

		private Employer GetEmployer(long employeeId, EmployerTradeNamesRequest descriptionRequest, long employerId,
			Employer employer, EmployerRequest migrationRequest, EmployerImportResponse response)
		{
			if (employeeId == 0)
			{
				var employerRequest = LookupEmployerRequestFunc("FC01:" + descriptionRequest.EmployeeMigrationId);
				if (employerRequest != null && employerRequest.FocusId != 0)
				{
					employer = GetEmployerFromEmployeeDetails(employeeId, descriptionRequest, employerId, migrationRequest, response, employerRequest);
				}
				else
				{
					employer = GetEmployerFromFEIN(descriptionRequest, migrationRequest, response);
				}
			}
			return employer;
		}

		private Employer GetEmployerFromFEIN(EmployerTradeNamesRequest descriptionRequest, EmployerRequest migrationRequest,
			EmployerImportResponse response)
		{
			Employer employer;
			// FEIN in v3 has a dash as the 3rd character
			descriptionRequest.Fein = descriptionRequest.Fein.Insert(2, "-");

			// find the employer by fein if the employee id is not populated.  Can occur if there are no employees for an employer associated with an employer tradename
			employer = Repositories.Core.Employers.FirstOrDefault(x => x.FederalEmployerIdentificationNumber == descriptionRequest.Fein);

			if (employer == null)
			{
				migrationRequest.Status = MigrationStatus.Failed;
				migrationRequest.Message = "Unable to save Business Unit Address";

				response.Acknowledgement = AcknowledgementType.Failure;
			  Repositories.Migration.SaveChanges();

				throw new Exception(String.Format("Could not identify employer with employee id {0} or fein {1}",
					descriptionRequest.EmployeeMigrationId, descriptionRequest.Fein));
			}
			return employer;
		}

		private Employer GetEmployerFromEmployeeDetails(long employeeId, EmployerTradeNamesRequest descriptionRequest,
			long employerId, EmployerRequest migrationRequest, EmployerImportResponse response, EmployerRequest employerRequest)
		{
			Employer employer;
			employeeId = employerRequest.FocusId;

			// lookup the employer from the employerid to get the default values
			var employee = Repositories.Core.Employees.SingleOrDefault(x => x.Id == employeeId);

			if (employeeId <= 0 || employee == null || employee.EmployerId <= 0)
				throw new InvalidDataException(String.Format("employeeId:{0}, employerId:{1}", employeeId.ToString(),
					employee == null ? "employee is null:{2}" : employee.EmployerId.ToString()));

			if (employerId <= 0)
				employerId = employee.EmployerId;

			// get\use the employerid to get the employer values
			employer = (Repositories.Core.Employers.Where(employerRecord => employerRecord.Id == employerId)).FirstOrDefault();

			if (employer == null)
			{
				migrationRequest.Status = MigrationStatus.Failed;
				migrationRequest.Message = "Unable to save Business Unit Address";

				response.Acknowledgement = AcknowledgementType.Failure;
				throw new Exception(String.Format("Could not identify employer with employee id {0} or fein {1}",
					descriptionRequest.EmployeeMigrationId, descriptionRequest.Fein));
			}
			return employer;
		}

		private EmployerTradeNamesRequest GetEmployerTradeNamesRequest(EmployerImportRequest request,
			ref EmployerRequest migrationRequest)
		{
			migrationRequest = Repositories.Migration.FindById<EmployerRequest>(request.EmployerRequestId);
			var descriptionRequest = DeserializeRequestFunc(migrationRequest);
			return descriptionRequest;
		}

		public Func<Employer, EmployerTradeNamesRequest, bool> UpdateBusinessUnitFunc;

		public bool UpdateBusinessUnit([NotNull] Employer employer, [NotNull] EmployerTradeNamesRequest descriptionRequest)
		{
			if (employer == null) throw new ArgumentNullException("employer");
			if (descriptionRequest == null) throw new ArgumentNullException("descriptionRequest");

			// get the matching business unit from the database
			var businessUnitToUpdate =
				Repositories.Core.BusinessUnits.FirstOrDefault(
					x => x.EmployerId == employer.Id && x.Name == descriptionRequest.TradeName);

			// update the business unit values
			if (businessUnitToUpdate != null)
			{
				businessUnitToUpdate.AccountTypeId = null;
				businessUnitToUpdate.EmployerId = employer.Id;
				businessUnitToUpdate.AlternatePhone1 = employer.AlternatePhone1;
				businessUnitToUpdate.AlternatePhone1Type = employer.AlternatePhone1Type;
				businessUnitToUpdate.AlternatePhone2 = employer.AlternatePhone2;
				businessUnitToUpdate.AlternatePhone2Type = employer.AlternatePhone2Type;
				businessUnitToUpdate.IndustrialClassification = employer.IndustrialClassification;
				businessUnitToUpdate.Name =
					descriptionRequest.TradeName.IsNotNullOrEmpty()
						? descriptionRequest.TradeName
						: descriptionRequest.EmployerLegalName;
				businessUnitToUpdate.PrimaryPhoneType = employer.PrimaryPhoneType;
				businessUnitToUpdate.PrimaryPhone = employer.PrimaryPhone;
				businessUnitToUpdate.PrimaryPhoneExtension = employer.PrimaryPhoneExtension;

			  BusinessUnitAddress businessUnitAddressToUpdate =
				  Repositories.Core.BusinessUnitAddresses.FirstOrDefault(x => x.BusinessUnitId == businessUnitToUpdate.Id);

			  if (businessUnitAddressToUpdate != null)
			  {
				  businessUnitAddressToUpdate.Line1 = descriptionRequest.Line1;
				  businessUnitAddressToUpdate.Line2 = descriptionRequest.Line2;
				  businessUnitAddressToUpdate.PostcodeZip = descriptionRequest.PostalCode;
				  businessUnitAddressToUpdate.TownCity = descriptionRequest.City;
				  businessUnitAddressToUpdate.IsPrimary = true; /* Business unit address is always primary for employer tradenames */
			  }
			}

			// save the entity
			Repositories.Core.SaveChanges();

			return true;
		}

		public Func<EmployerImportRequest, bool, long, EmployerImportResponse> ProcessBusinessUnitDescriptionFunc;

		/// <summary>
		/// Sends an business unit description request from the migration database to the focus database.
		/// </summary>
		/// <param name="request">The import request object.</param>
		/// <param name="resetRepository">Whether to reset the migration repository</param>
		/// <param name="employeeId">The employee identifier.</param>
		public EmployerImportResponse ProcessBusinessUnitDescription(EmployerImportRequest request, bool resetRepository = true, long employeeId = 0)
		{
			var response = new EmployerImportResponse(request);

			try
			{
				var migrationRequest = Repositories.Migration.FindById<EmployerRequest>(request.EmployerRequestId);

				SaveBusinessUnitDescriptionRequest descriptionRequest;
				descriptionRequest = migrationRequest.Request.DataContractDeserialize<SaveBusinessUnitDescriptionRequest>();

				if (employeeId == 0)
				{
					var employerRequest = LookupEmployerRequest(descriptionRequest.EmployeeMigrationId);
					if (employerRequest != null)
						employeeId = employerRequest.FocusId;
				}

				if (employeeId > 0)
				{
					PrepareServiceRequestPropertiesWrapper(descriptionRequest, request);

					var employeeBusinessUnit = LookupDefaultBusinessUnitForEmployee(employeeId);
					descriptionRequest.BusinessUnitDescription.BusinessUnitId = employeeBusinessUnit.BusinessUnitId;

					var descriptionResponse = UpdateOrAddBusinessUnitDescriptionResponse(descriptionRequest, migrationRequest);

					var focusId = (descriptionResponse.BusinessUnitDescription != null && descriptionResponse.BusinessUnitDescription.Id.HasValue) ? descriptionResponse.BusinessUnitDescription.Id.Value : 0;
					CheckEmployerServiceResponse(descriptionResponse, migrationRequest, focusId);

					response.Acknowledgement = descriptionResponse.Acknowledgement;
				}
				else
				{
					migrationRequest.Status = MigrationStatus.Failed;
					migrationRequest.Message = "EmployerRequest record not found";

					response.Acknowledgement = AcknowledgementType.Failure;
				}

				migrationRequest.ProcessedBy = DateTime.UtcNow;
				Repositories.Migration.SaveChanges(resetRepository);
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}
			return response;
		}

		public Func<EmployerImportRequest, bool, long, EmployerImportResponse> ProcessBusinessUnitLogoFunc;

		/// <summary>
		/// Sends an business unit logo request from the migration database to the focus database.
		/// </summary>
		/// <param name="request">The import request object.</param>
		/// <param name="resetRepository">Whether to reset the migration repository</param>
		/// <param name="employeeId">The employee identifier.</param>
		/// <returns>The response</returns>
		public EmployerImportResponse ProcessBusinessUnitLogo(EmployerImportRequest request, bool resetRepository = true, long employeeId = 0)
		{
			var response = new EmployerImportResponse(request);

			try
			{
				var migrationRequest = Repositories.Migration.FindById<EmployerRequest>(request.EmployerRequestId);

				var logoRequest = migrationRequest.Request.DataContractDeserialize<SaveBusinessUnitLogoRequest>();

				if (employeeId == 0)
				{
					var employerRequest = LookupEmployerRequest(logoRequest.EmployeeMigrationId, MigrationEmployerRecordType.Employee);
					if (employerRequest != null)
						employeeId = employerRequest.FocusId;
				}

				if (employeeId > 0)
				{
					PrepareServiceRequestPropertiesWrapper(logoRequest, request);

					var employeeBusinessUnit = LookupDefaultBusinessUnitForEmployee(employeeId);
					logoRequest.BusinessUnitLogo.BusinessUnitId = employeeBusinessUnit.BusinessUnitId;

					var logoResponse = UpdateOrAddBusinessUnitLogoResponse(logoRequest, migrationRequest);

					CheckEmployerServiceResponse(logoResponse, migrationRequest, logoResponse.BusinessUnitLogoId);

					response.Acknowledgement = logoResponse.Acknowledgement;
				}
				else
				{
					migrationRequest.Status = MigrationStatus.Failed;
					migrationRequest.Message = "EmployerRequest record not found";

					response.Acknowledgement = AcknowledgementType.Failure;
				}

				migrationRequest.ProcessedBy = DateTime.UtcNow;
				Repositories.Migration.SaveChanges(resetRepository);
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}
			return response;
		}

		private SaveBusinessUnitDescriptionResponse UpdateOrAddBusinessUnitDescriptionResponse(SaveBusinessUnitDescriptionRequest descriptionRequest, EmployerRequest migrationRequest)
		{
			var originalBusinessUnitDescriptionRequest = Repositories.Migration.EmployerRequests
				.FirstOrDefault(x => x.ExternalId == migrationRequest.ExternalId
				                     && migrationRequest.IsUpdate == false
				                     && x.RecordType == MigrationEmployerRecordType.BusinessUnitDescription);

			// if the business unit description already in the v3 application table and parent successfuly processed
			SaveBusinessUnitDescriptionResponse descriptionResponse;
			if (originalBusinessUnitDescriptionRequest != null && originalBusinessUnitDescriptionRequest.Status == MigrationStatus.Processed)
			{
				// update the business unit description information

				descriptionResponse = EmployerService.UpdateBusinessUnitDescription(descriptionRequest, originalBusinessUnitDescriptionRequest);
			}
			// else if the business unit description is not in the v3 application table or not successfully processed
			else
			{
				// add the business unit description
				descriptionResponse = EmployerService.SaveBusinessUnitDescription(descriptionRequest);
			}
			return descriptionResponse;
		}


		private SaveBusinessUnitLogoResponse UpdateOrAddBusinessUnitLogoResponse(SaveBusinessUnitLogoRequest logoRequest, EmployerRequest migrationRequest)
		{
			var originalBusinessUnitLogoRequest = Repositories.Migration.EmployerRequests
				.FirstOrDefault(x => x.ExternalId == migrationRequest.ExternalId
				                     && migrationRequest.IsUpdate == false
				                     && x.RecordType == MigrationEmployerRecordType.BusinessUnitDescription);

			// if the business unit description already in the v3 application table and parent successfuly processed
			SaveBusinessUnitLogoResponse logoResponse;
			if (originalBusinessUnitLogoRequest != null && originalBusinessUnitLogoRequest.Status == MigrationStatus.Processed)
			{
				// update the business unit description information

				logoResponse = EmployerService.UpdateBusinessUnitLogo(logoRequest, originalBusinessUnitLogoRequest);
			}
			// else if the business unit description is not in the v3 application table or not successfully processed
			else
			{
				// add the business unit description
				logoResponse = EmployerService.SaveBusinessUnitLogo(logoRequest);
			}
			return logoResponse;
		}

		/// <summary>
		/// Sends an resume search request from the migration database to the focus database.
		/// </summary>
		/// <param name="request">The import request object.</param>
		/// <param name="recordType">Type of the record.</param>
		/// <param name="resetRepository">Whether to reset the migration repository</param>
		/// <returns>The response</returns>
		public EmployerImportResponse ProcessResumeSearch(EmployerImportRequest request, MigrationEmployerRecordType recordType, bool resetRepository = true)
		{
			var response = new EmployerImportResponse(request);

			EmployerRequest migrationRequest = null;

			try
			{
				migrationRequest = Repositories.Migration.FindById<EmployerRequest>(request.EmployerRequestId);
				var talentRequest = migrationRequest.Request.DataContractDeserialize<SaveCandidateSavedSearchRequest>();

				var callService = true;
				if (migrationRequest.IsUpdate)
				{
					var originalRequest = LookupEmployerRequest(migrationRequest.ExternalId, recordType);
					if (originalRequest.IsNotNull())
						talentRequest.Id = originalRequest.FocusId;
					else
						callService = false;
				}

				if (callService)
				{
					var employeeRequest = LookupEmployerRequest(talentRequest.EmployeeMigrationId, MigrationEmployerRecordType.Employee);
					if (employeeRequest != null)
					{
						PrepareServiceRequestPropertiesWrapper(talentRequest, request);

						var employee = Repositories.Core.Employees.FirstOrDefault(e => e.Id == employeeRequest.FocusId);

						if (employee != null)
						{
							var person = employee.Person;
							var user = person.User;

							talentRequest.SaveForUserId = user.Id;
							if (talentRequest.AlertEmailRequired && talentRequest.AlertEmailAddress.IsNullOrEmpty())
								talentRequest.AlertEmailAddress = employee.Person.EmailAddress;

							// Map posting Id from V1 database to a Job Id in V3
							var jobDetailsCriteria = talentRequest.Criteria.JobDetailsCriteria;
							if (jobDetailsCriteria.IsNotNull() && jobDetailsCriteria.MigrationPostingId.IsNotNullOrEmpty())
							{
								var jobRequest = LookupJobOrderRequest(jobDetailsCriteria.MigrationPostingId);
								if (jobRequest.IsNotNull())
								{
									var posting = Repositories.Core.Postings.FirstOrDefault(p => p.Id == jobRequest.FocusId);
									if (posting.IsNotNull() && posting.JobId.IsNotNull())
									{
										jobDetailsCriteria.JobId = posting.JobId.Value;
										jobDetailsCriteria.JobTitle = posting.JobTitle;
									}
								}
								jobDetailsCriteria.MigrationPostingId = null;
							}

							SaveCandidateSavedSearchResponse careerResponse = null;

							var resumeSearchQuery = Repositories.Core.SavedSearches.Where(x => x.Id == migrationRequest.FocusId && migrationRequest.Status == MigrationStatus.Processed);
							if (migrationRequest.IsUpdate || !resumeSearchQuery.Any())
							{
								lock (SyncObject)
								{
									if (migrationRequest.IsUpdate || !resumeSearchQuery.Any())
									{
										careerResponse = SearchService.SaveCandidateSearch(talentRequest);
									}
								}
							}
							if( careerResponse != null && careerResponse.Acknowledgement == AcknowledgementType.Success )
							{
								migrationRequest.FocusId = careerResponse.SavedSearchId;
								migrationRequest.Status = MigrationStatus.Processed;
							}
							else
							{
								migrationRequest.Status = MigrationStatus.Failed;
								migrationRequest.Message = FormatResponseMessage(careerResponse);
							}
						}
						else
						{
							migrationRequest.Status = MigrationStatus.Failed;
							migrationRequest.Message = string.Format("Employee record not found for {0}", recordType);
						}
					}
					else
					{
						migrationRequest.Status = MigrationStatus.Failed;
						migrationRequest.Message = string.Format("EmployerRequest ({0}) record not found", recordType);
					}
				}
				else
				{
					migrationRequest.Status = MigrationStatus.Failed;
					migrationRequest.Message = "Unable to find original request to update";
				}

				migrationRequest.ProcessedBy = DateTime.UtcNow;
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);

				if (migrationRequest.IsNotNull())
				{
					migrationRequest.Status = MigrationStatus.Failed;
					migrationRequest.Message = string.Concat("EXCEPTION: ", ex.Message);
				}
			}

			Repositories.Migration.SaveChanges(true);

			return response;
		}

		#endregion

		#region Job Seekers

		/// <summary>
		/// Sends a job seeker request from the migration database to the focus database.
		/// </summary>
		/// <param name="request">The import request object.</param>
		public JobSeekerImportResponse ProcessJobSeeker(JobSeekerImportRequest request)
		{
			var response = new JobSeekerImportResponse(request);

			try
			{
				var migrationRequest = Repositories.Migration.FindById<JobSeekerRequest>(request.JobSeekerRequestId);
				var careerRequest = migrationRequest.Request.DataContractDeserialize<RegisterCareerUserRequest>();

				PrepareServiceRequestPropertiesWrapper(careerRequest, request);

				var callService = true;
				if (migrationRequest.IsUpdate)
				{
					var originalRequest = LookupJobSeekerRequest(migrationRequest.ExternalId, MigrationJobSeekerRecordType.JobSeeker);
					if (originalRequest.IsNotNull())
					{
						careerRequest.UpdatePersonId = originalRequest.FocusId;

						//Password should not be updated
						careerRequest.AccountPassword = null;
					}
					else
					{
						callService = false;
					}
				}

				var processedBy = DateTime.UtcNow;
				var emailRequired = migrationRequest.EmailRequired;
				var focusId = migrationRequest.FocusId;
				var message = migrationRequest.Message;

				MigrationStatus status;

				if (callService)
				{
					careerRequest.IgnoreClient = request.IgnoreClient;

					RegisterCareerUserResponse careerResponse = null;
					var jobSeekerQuery = Repositories.Core.Persons.Where(x => x.Id == migrationRequest.FocusId && migrationRequest.Status == MigrationStatus.Processed);
					if (migrationRequest.IsUpdate || !jobSeekerQuery.Any())
					{
						lock (SyncObject)
						{
							if (migrationRequest.IsUpdate || !jobSeekerQuery.Any())
							{
								careerResponse = migrationRequest.IsUpdate
									? AccountService.UpdateUser(careerRequest)
									: AccountService.RegisterCareerUser(careerRequest);
							}
						}
					}
					if( careerResponse != null && careerResponse.Acknowledgement == AcknowledgementType.Success )
					{
						//migrationRequest.EmailRequired = !migrationRequest.IsUpdate;
						//migrationRequest.FocusId = careerResponse.PersonId;
						//migrationRequest.Status = MigrationStatus.Processed;

						emailRequired = !migrationRequest.IsUpdate;
						focusId = careerResponse.PersonId;
						status = MigrationStatus.Processed;
					}
					else
					{
						//migrationRequest.Status = MigrationStatus.Failed;
						//migrationRequest.Message = FormatResponseMessage(careerResponse);
						status = MigrationStatus.Failed;
						message = FormatResponseMessage(careerResponse);
					}
				}
				else
				{
					//migrationRequest.Status = MigrationStatus.Failed;
					//migrationRequest.Message = "Unable to find original request to update";
					status = MigrationStatus.Failed;
					message = "Unable to find original request to update";
				}

				//migrationRequest.ProcessedBy = DateTime.UtcNow;

				var updateQuery = new Query(typeof(JobSeekerRequest), Entity.Attribute("Id") == request.JobSeekerRequestId);
				Repositories.Migration.Update(updateQuery, new
				{
					ProcessedBy = processedBy,
					EmailRequired = emailRequired,
					FocusId = focusId,
					Status = status,
					Message = message
				});

				Repositories.Migration.SaveChanges(true);
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}
			return response;
		}

		/// <summary>
		/// Sends a job seeker resume request from the migration database to the focus database.
		/// </summary>
		/// <param name="request">The import request object.</param>
		public JobSeekerImportResponse ProcessJobSeekerResume(JobSeekerImportRequest request)
		{
			var response = new JobSeekerImportResponse(request);

			try
			{
				var migrationRequest = Repositories.Migration.FindById<JobSeekerRequest>(request.JobSeekerRequestId);
				var careerRequest = migrationRequest.Request.DataContractDeserialize<SaveResumeRequest>();

				var callService = true;
				if (migrationRequest.IsUpdate)
				{
					var originalRequest = LookupJobSeekerRequest(migrationRequest.ExternalId, MigrationJobSeekerRecordType.Resume);
					if (originalRequest.IsNotNull())
						careerRequest.SeekerResume.ResumeMetaInfo.ResumeId = originalRequest.FocusId;
					else
						callService = false;
				}

				var processedBy = DateTime.UtcNow;
				var emailRequired = migrationRequest.EmailRequired;
				var focusId = migrationRequest.FocusId;
				var message = migrationRequest.Message;

				var status = MigrationStatus.Failed;

				if (callService)
				{
					var jobSeekerRequest = LookupJobSeekerRequest(careerRequest.JobSeekerMigrationId, MigrationJobSeekerRecordType.JobSeeker);
					if (jobSeekerRequest != null)
					{
						PrepareServiceRequestPropertiesWrapper(careerRequest, request);

						careerRequest.PersonId = jobSeekerRequest.FocusId;

						var registerCareerUserRequest = jobSeekerRequest.Request.DataContractDeserialize<RegisterCareerUserRequest>();
						var emailAddress = registerCareerUserRequest.EmailAddress;
						if (jobSeekerRequest.ExternalId.EndsWith(".tag") && emailAddress.StartsWith("Test") && emailAddress.EndsWith("@burning-glass.com"))
						{
							if (careerRequest.SeekerResume.ResumeContent.SeekerContactDetails != null)
								careerRequest.SeekerResume.ResumeContent.SeekerContactDetails.EmailAddress = emailAddress;

							if (careerRequest.SeekerResume.ResumeContent.Profile != null)
								careerRequest.SeekerResume.ResumeContent.Profile.EmailAddress = emailAddress;
						}

						careerRequest.IgnoreLens = request.IgnoreLens;
						careerRequest.IgnoreClient = request.IgnoreClient;

// check if resume exists and if the update mode is false, then skip it
// if not lock it and save the resume

						var resumeQuery = Repositories.Core.Resumes.Where(x => x.PersonId == careerRequest.PersonId && x.ExternalId == careerRequest.SeekerResume.ResumeMetaInfo.ResumeId.ToString());
						if (migrationRequest.IsUpdate || !resumeQuery.Any())
						{
							lock (SyncObject)
							{
								if (migrationRequest.IsUpdate || !resumeQuery.Any())
								{
									careerRequest.ExternalResumeId = careerRequest.SeekerResume.ResumeMetaInfo.ResumeId.ToString();
									var careerResponse = ResumeService.SaveResume(careerRequest);

									if (careerResponse.Acknowledgement == AcknowledgementType.Success)
									{
										focusId = careerResponse.SeekerResume.ResumeMetaInfo.ResumeId.GetValueOrDefault(0);
										status = MigrationStatus.Processed;

										var resumeDocumentRequestId = LookupJobSeekerRequestIdsByParent(careerRequest.ResumeMigrationId,
											MigrationJobSeekerRecordType.ResumeDocument).FirstOrDefault();
										if (resumeDocumentRequestId.IsNotNull() && resumeDocumentRequestId > 0)
										{
											var originalRequest = request.JobSeekerRequestId;
											var originalResumeId = request.ResumeId;

											//request.ResumeId = migrationRequest.FocusId;
											request.ResumeId = focusId;
											request.JobSeekerRequestId = resumeDocumentRequestId;

											ProcessJobSeekerResumeDocument(request, false);

											request.JobSeekerRequestId = originalRequest;
											request.ResumeId = originalResumeId;
										}
									}
								}
							}
						}
						else
						{
							status = MigrationStatus.Failed;
							message = "Resume already exists in target database";
						}
					}
					else
					{
						status = MigrationStatus.Failed;
						message = "JobSeekerRequest record not found";
					}
				}
				else
				{
					status = MigrationStatus.Failed;
					message = "Unable to find original request to update";
				}

				var updateQuery = new Query(typeof(JobSeekerRequest), Entity.Attribute("Id") == request.JobSeekerRequestId);
				Repositories.Migration.Update(updateQuery, new
				{
					ProcessedBy = processedBy,
					EmailRequired = emailRequired,
					FocusId = focusId,
					Status = status,
					Message = message
				});

				Repositories.Migration.SaveChanges(true);
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}

			return response;
		}

		/// <summary>
		/// Sends a job seeker resume document request from the migration database to the focus database.
		/// </summary>
		/// <param name="request">The import request object.</param>
		/// <param name="resetRepository">Whether to reset the migration repository</param>
		/// <returns>The response</returns>
		public JobSeekerImportResponse ProcessJobSeekerResumeDocument(JobSeekerImportRequest request, bool resetRepository = true)
		{
			var response = new JobSeekerImportResponse(request);

			try
			{
				var migrationRequest = Repositories.Migration.FindById<JobSeekerRequest>(request.JobSeekerRequestId);

				var careerRequest = migrationRequest.Request.DataContractDeserialize<SaveResumeDocumentRequest>();

				var resumeId = request.ResumeId;
				if (resumeId == null)
				{
					var resumeRequest = LookupJobSeekerRequest(careerRequest.ResumeMigrationId, MigrationJobSeekerRecordType.Resume);
					if (resumeRequest != null)
						resumeId = resumeRequest.FocusId;
				}

				var processedBy = DateTime.UtcNow;
				var emailRequired = migrationRequest.EmailRequired;
				var focusId = migrationRequest.FocusId;
				var message = migrationRequest.Message;

				MigrationStatus status;

				if (resumeId.HasValue)
				{
					PrepareServiceRequestPropertiesWrapper(careerRequest, request);

					careerRequest.ResumeDocument.ResumeId = resumeId.Value;
					var careerResponse = ResumeService.SaveResumeDocument(careerRequest);

					if (careerResponse.Acknowledgement == AcknowledgementType.Success)
					{
						//migrationRequest.FocusId = careerResponse.ResumeDocument.Id.GetValueOrDefault(0);
						//migrationRequest.Status = MigrationStatus.Processed;

						focusId = careerResponse.ResumeDocument.Id.GetValueOrDefault(0);
						status = MigrationStatus.Processed;
					}
					else
					{
						//migrationRequest.Status = MigrationStatus.Failed;
						//migrationRequest.Message = FormatResponseMessage(careerResponse);

						status = MigrationStatus.Failed;
						message = FormatResponseMessage(careerResponse);
					}
				}
				else
				{
					//migrationRequest.Status = MigrationStatus.Failed;
					//migrationRequest.Message = "JobSeekerRequest (resume) record not found";

					status = MigrationStatus.Failed;
					message = "JobSeekerRequest (resume) record not found";
				}

				//migrationRequest.ProcessedBy = DateTime.UtcNow;

				var updateQuery = new Query(typeof(JobSeekerRequest), Entity.Attribute("Id") == request.JobSeekerRequestId);
				Repositories.Migration.Update(updateQuery, new
				{
					ProcessedBy = processedBy,
					EmailRequired = emailRequired,
					FocusId = focusId,
					Status = status,
					Message = message
				});

				Repositories.Migration.SaveChanges(resetRepository);
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}
			return response;
		}

		/// <summary>
		/// Sends a viewed posting request from the migration database to the focus database.
		/// </summary>
		/// <param name="request">The import request object.</param>
		public JobSeekerImportResponse ProcessJobSeekerViewedPosting(JobSeekerImportRequest request)
		{
			var response = new JobSeekerImportResponse(request);

			try
			{
				var migrationRequest = Repositories.Migration.FindById<JobSeekerRequest>(request.JobSeekerRequestId);
				var careerRequest = migrationRequest.Request.DataContractDeserialize<AddViewedPostingRequest>();

				var jobSeekerRequest = LookupJobSeekerRequest(careerRequest.JobSeekerMigrationId, MigrationJobSeekerRecordType.JobSeeker);
				if (jobSeekerRequest != null)
				{
					PrepareServiceRequestPropertiesWrapper(careerRequest, request);

					var user = Repositories.Core.Users.FirstOrDefault(u => u.PersonId == jobSeekerRequest.FocusId);

					if (user != null)
					{
						careerRequest.ViewedPosting.UserId = user.Id;

						var careerResponse = PostingService.AddViewedPostings(careerRequest);

						if (careerResponse.Acknowledgement == AcknowledgementType.Success)
						{
							migrationRequest.FocusId = careerResponse.ViewedPosting.Id.GetValueOrDefault(0);
							migrationRequest.Status = MigrationStatus.Processed;
						}
						else
						{
							migrationRequest.Status = MigrationStatus.Failed;
							migrationRequest.Message = FormatResponseMessage(careerResponse);
						}
					}
					else
					{
						migrationRequest.Status = MigrationStatus.Failed;
						migrationRequest.Message = "User record not found for ViewedPosting";
					}
				}
				else
				{
					migrationRequest.Status = MigrationStatus.Failed;
					migrationRequest.Message = "JobSeekerRequest (ViewedPosting) record not found";
				}

				migrationRequest.ProcessedBy = DateTime.UtcNow;
				Repositories.Migration.SaveChanges(true);
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}
			return response;
		}

		/// <summary>
		/// Sends a saved search request from the migration database to the focus database.
		/// </summary>
		/// <param name="request">The import request object.</param>
		/// <param name="recordType">Type of the record.</param>
		/// <returns></returns>
		public JobSeekerImportResponse ProcessJobSeekerSavedSearch(JobSeekerImportRequest request, MigrationJobSeekerRecordType recordType)
		{
			var response = new JobSeekerImportResponse(request);

			try
			{
				var migrationRequest = Repositories.Migration.FindById<JobSeekerRequest>(request.JobSeekerRequestId);
				var careerRequest = migrationRequest.Request.DataContractDeserialize<SaveSearchRequest>();

				var callService = true;
				if (migrationRequest.IsUpdate)
				{
					var originalRequest = LookupJobSeekerRequest(migrationRequest.ExternalId, recordType);
					if (originalRequest.IsNotNull())
						careerRequest.Id = originalRequest.FocusId;
					else
						callService = false;
				}

				if (callService)
				{
					var jobSeekerRequest = LookupJobSeekerRequest(careerRequest.JobSeekerMigrationId, MigrationJobSeekerRecordType.JobSeeker);
					if (jobSeekerRequest != null)
					{
						PrepareServiceRequestPropertiesWrapper(careerRequest, request);

						var user = Repositories.Core.Users.FirstOrDefault(u => u.PersonId == jobSeekerRequest.FocusId);

						if (user != null)
						{
							careerRequest.SaveForUserId = user.Id;
							if (careerRequest.AlertEmailRequired && careerRequest.AlertEmailAddress.IsNullOrEmpty())
								careerRequest.AlertEmailAddress = user.Person.EmailAddress;


							SaveSearchResponse careerResponse = null;

							var savedSearchQuery =
								Repositories.Core.SavedSearches.Where(
									x => x.Id == migrationRequest.FocusId
									     && migrationRequest.Status != MigrationStatus.Processed);
							if (migrationRequest.IsUpdate || !savedSearchQuery.Any())
							{
								lock (SyncObject)
								{
									if (migrationRequest.IsUpdate || !savedSearchQuery.Any())
									{
										careerResponse = AnnotationService.SaveSearch(careerRequest);
									}
								}
								if( careerResponse != null && careerResponse.Acknowledgement == AcknowledgementType.Success )
								{
									migrationRequest.FocusId = careerResponse.SavedSearchId;
									migrationRequest.Status = MigrationStatus.Processed;
								}
								else
								{
									migrationRequest.Status = MigrationStatus.Failed;
									migrationRequest.Message = FormatResponseMessage(careerResponse);
								}
							}
						}
						else
						{
							migrationRequest.Status = MigrationStatus.Failed;
							migrationRequest.Message = string.Format("User record not found for {0}", recordType);
						}
					}
					else
					{
						migrationRequest.Status = MigrationStatus.Failed;
						migrationRequest.Message = string.Format("JobSeekerRequest ({0}) record not found", recordType);
					}
				}
				else
				{
					migrationRequest.Status = MigrationStatus.Failed;
					migrationRequest.Message = "Unable to find original request to update";
				}

				migrationRequest.ProcessedBy = DateTime.UtcNow;
				Repositories.Migration.SaveChanges(true);
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}
			return response;
		}

		/// <summary>
		/// Sends a viewed posting request from the migration database to the focus database.
		/// </summary>
		/// <param name="request">The import request object.</param>
		/// <param name="recordType">Type of the record.</param>
		public JobSeekerImportResponse ProcessReferral(JobSeekerImportRequest request, MigrationJobSeekerRecordType recordType)
		{
			var response = new JobSeekerImportResponse(request);

			try
			{
				var migrationRequest = Repositories.Migration.FindById<JobSeekerRequest>(request.JobSeekerRequestId);
				var candidateRequest = migrationRequest.Request.DataContractDeserialize<ReferCandidateRequest>();

				var callService = true;
				if (migrationRequest.IsUpdate)
				{
					var originalRequest = LookupJobSeekerRequest(migrationRequest.ExternalId, recordType);
					if (originalRequest.IsNull())
						callService = false;
				}

				if (callService)
				{
					var jobSeekerRequest = LookupJobSeekerRequest(candidateRequest.JobSeekerMigrationId, MigrationJobSeekerRecordType.JobSeeker);
					var jobOrderRequest = LookupJobOrderRequest(candidateRequest.PostingMigrationId);

					if (jobSeekerRequest.IsNotNull() && jobOrderRequest.IsNotNull())
					{
						PrepareServiceRequestPropertiesWrapper(candidateRequest, request);

						candidateRequest.PersonId = jobSeekerRequest.FocusId;
						candidateRequest.JobId = jobOrderRequest.FocusId;

						ReferCandidateResponse candidateResponse = null;

						var referralQuery = Repositories.Core.Resumes.Where(x => x.Id == migrationRequest.FocusId && migrationRequest.Status == MigrationStatus.Processed);
						if (migrationRequest.IsUpdate || !referralQuery.Any())
						{
							lock (SyncObject)
							{
								if (migrationRequest.IsUpdate || !referralQuery.Any())
								{
									candidateResponse = CandidateService.ReferCandidate(candidateRequest);
								}
							}
						}
						if( candidateResponse != null && candidateResponse.Acknowledgement == AcknowledgementType.Success )
						{
							migrationRequest.FocusId = candidateResponse.ApplicationId;
							migrationRequest.Status = MigrationStatus.Processed;
						}
						else
						{
							migrationRequest.Status = MigrationStatus.Failed;
							migrationRequest.Message = FormatResponseMessage(candidateResponse);
						}
					}
					else
					{
						migrationRequest.Status = MigrationStatus.Failed;
						migrationRequest.Message = jobSeekerRequest.IsNull()
							? string.Format("JobSeekerRequest ({0}) record not found", recordType)
							: string.Format("JobOrderRequest ({0}) record not found", recordType);
					}
				}
				else
				{
					migrationRequest.Status = MigrationStatus.Failed;
					migrationRequest.Message = "Unable to find original request to update";
				}

				migrationRequest.ProcessedBy = DateTime.UtcNow;
				Repositories.Migration.SaveChanges(true);
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}
			return response;
		}

		/// <summary>
		/// Sends an activity assignment from the migration database to the focus database.
		/// </summary>
		/// <param name="request">The import request object.</param>
		public JobSeekerImportResponse ProcessJobSeekerActivity(JobSeekerImportRequest request)
		{
			var response = new JobSeekerImportResponse(request);

			JobSeekerRequest migrationRequest = null;

			try
			{
				migrationRequest = Repositories.Migration.FindById<JobSeekerRequest>(request.JobSeekerRequestId);

				var candidateRequest = migrationRequest.Request.DataContractDeserialize<AssignActivityOrActionRequest>();
				var jobSeekerRequest = LookupJobSeekerRequest(candidateRequest.JobSeekerMigrationId, MigrationJobSeekerRecordType.JobSeeker);

				if (jobSeekerRequest.IsNotNull())
				{
					var jobSeeker = Repositories.Core.JobSeekerProfileViews.FirstOrDefault(js => js.Id == jobSeekerRequest.FocusId);
					if (jobSeeker.IsNotNull())
					{
						PrepareServiceRequestPropertiesWrapper(candidateRequest, request);

						candidateRequest.PersonId = jobSeeker.Id;
						candidateRequest.ClientId = jobSeeker.ClientId ?? jobSeeker.Id.ToString(CultureInfo.InvariantCulture);
						candidateRequest.FirstName = jobSeeker.FirstName;
						candidateRequest.LastName = jobSeeker.LastName;
						candidateRequest.PhoneNumber = jobSeeker.HomePhone;
						candidateRequest.EmailAddress = jobSeeker.EmailAddress;

						candidateRequest.IgnoreClient = request.IgnoreClient;
						if (candidateRequest.StaffMigrationId.IsNotNull())
						{
							var userRequest = LookupUserRequest(candidateRequest.StaffMigrationId, MigrationAssistUserRecordType.AssistUser);
							if (userRequest != null)
							{
								candidateRequest.AssigningUserId = userRequest.FocusId;
							}
						}
						else if (candidateRequest.StaffSecondaryMigrationId.IsNotNull())
						{
							var userRequest = LookupUserRequestBySecondary(candidateRequest.StaffSecondaryMigrationId, MigrationAssistUserRecordType.AssistUser);
							if (userRequest != null)
							{
								candidateRequest.AssigningUserId = userRequest.FocusId;
							}
						}
						else
						{
							candidateRequest.AssigningUserId = jobSeeker.UserId;
						}

						AssignActivityOrActionResponse candidateResponse = null;

						if (migrationRequest.IsUpdate || (!DoesStoredDuplicateActivityExist(candidateRequest)))
						{
							lock (SyncObject)
							{

								if (!DoesStoredDuplicateActivityExist(candidateRequest))
								{
									if (candidateRequest.ActivityId != -1)
									{
										candidateResponse = CandidateService.AssignActivity(candidateRequest);
									}
									else if (candidateRequest.ActionTypeId != -1)
									{
										candidateResponse = CoreService.AssignAction(candidateRequest);
									}
									else
									{
										throw new Exception("Cannot Assign Activity, ActivityId and ActionTypeId are both invalid");
									}
								}
							}
						}

						if (candidateRequest.OverrideCreatedOn != null)
	          {
		          var changes = new Dictionary<string, object>();

							changes.Add("ActionedOn", candidateRequest.OverrideCreatedOn);

		          var updateQuery = new Query(typeof (ActionEvent), Entity.Attribute("Id") == candidateResponse.FocusId);
		          Repositories.Core.Update(updateQuery, changes);
		          Repositories.Core.SaveChanges();
	          }
						if (candidateResponse == null)
						{
							migrationRequest.FocusId = -1;
							migrationRequest.Status = MigrationStatus.Failed;
							migrationRequest.Message = "Duplicate activity or action found";
						}
						else if (candidateResponse.Acknowledgement == AcknowledgementType.Success)
	          {
		          migrationRequest.FocusId = candidateResponse.FocusId ?? -2;
							migrationRequest.Status = MigrationStatus.Processed;
						}
						else
						{
							migrationRequest.Status = MigrationStatus.Failed;
							migrationRequest.Message = FormatResponseMessage(candidateResponse);
						}
					}
					else
					{
						migrationRequest.Status = MigrationStatus.Failed;
						migrationRequest.Message = "JobSeeker (Activity) record not found";
					}
				}
				else
				{
					migrationRequest.Status = MigrationStatus.Failed;
					migrationRequest.Message = "JobSeekerRequest (Activity) record not found";
				}

				migrationRequest.ProcessedBy = DateTime.UtcNow;
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);

				if (migrationRequest != null)
				{
					migrationRequest.Status = MigrationStatus.Failed;
					migrationRequest.Message = string.Concat("EXCEPTION: ", ex.Message);
				}
			}

			Repositories.Migration.SaveChanges(true);

			return response;
		}

		private bool DoesStoredDuplicateActivityExist(AssignActivityOrActionRequest candidateRequest)
		{
			var assignActivityToCandidateAction = Repositories.Core.ActionTypes.FirstOrDefault(at => at.Name == ActionTypes.AssignActivityToCandidate.ToString());
			Logger.Error(string.Format("assignActivityToCandidateAction is null: {0}", assignActivityToCandidateAction == null));
			var personEntity = Repositories.Core.EntityTypes.FirstOrDefault(et => et.Name == typeof(Person).Name);
			Logger.Error(string.Format("personEntity is null: {0}", personEntity == null));

			IQueryable<ActionEvent> storedDuplicateActivityQuery = null;

			long actionId;
			if (assignActivityToCandidateAction == null)
			{
				actionId = -1;
			}
			else
			{
				actionId = assignActivityToCandidateAction.Id;
			}

			if (personEntity != null)
			{
				var entityType = personEntity;

				Logger.Error(string.Format("candidateRequest.PersonId: {0}, candidateRequest.ActivityId: {1}, entityType.Id: {2}, actionId{3}", candidateRequest.PersonId, candidateRequest.ActivityId, entityType.Id, actionId));


				storedDuplicateActivityQuery = Repositories.Core.ActionEvents.Where(
					x =>
						x.EntityId == candidateRequest.PersonId &&
						x.EntityIdAdditional01 == candidateRequest.ActivityId &&
						x.EntityTypeId == entityType.Id &&
						x.ActionTypeId == actionId &&
						x.ActionedOn == candidateRequest.OverrideCreatedOn
					);
			}
			return storedDuplicateActivityQuery != null && storedDuplicateActivityQuery.Any();
		}

		/// <summary>
		/// Sends an toggle job seeker flag request from the migration database to the focus database.
		/// </summary>
		/// <param name="request">The import request object.</param>
		public JobSeekerImportResponse ProcessFlagJobSeeker(JobSeekerImportRequest request)
		{
			var response = new JobSeekerImportResponse(request);

			try
			{
				var migrationRequest = Repositories.Migration.FindById<JobSeekerRequest>(request.JobSeekerRequestId);

				var candidateRequest = migrationRequest.Request.DataContractDeserialize<ToggleCandidateFlagRequest>();

				var jobSeekerRequest = LookupJobSeekerRequest(candidateRequest.JobSeekerMigrationId, MigrationJobSeekerRecordType.JobSeeker);
				var employeeRequest = LookupEmployerRequest(candidateRequest.EmployeeMigrationId, MigrationEmployerRecordType.Employee);
				if (jobSeekerRequest.IsNotNull() && employeeRequest.IsNotNull())
				{
					var employeePersonId = Repositories.Core.Employees.Where(emp => emp.Id == employeeRequest.FocusId).Select(emp => emp.PersonId).FirstOrDefault();
					if (employeePersonId > 0)
					{
						PrepareServiceRequestPropertiesWrapper(candidateRequest, request);

						candidateRequest.PersonId = jobSeekerRequest.FocusId;
						candidateRequest.FlaggingPersonId = employeePersonId;

						var candidateResponse = CandidateService.ToggleCandidateFlag(candidateRequest);

						if (candidateResponse.Acknowledgement == AcknowledgementType.Success)
						{
							migrationRequest.FocusId = candidateResponse.FlaggedApplicantsListId;
							migrationRequest.Status = MigrationStatus.Processed;
						}
						else
						{
							migrationRequest.Status = MigrationStatus.Failed;
							migrationRequest.Message = FormatResponseMessage(candidateResponse);
						}
					}
					else
					{
						migrationRequest.Status = MigrationStatus.Failed;
						migrationRequest.Message = "Employee (Toggle Flag) record not found";
					}
				}
				else
				{
					migrationRequest.Status = MigrationStatus.Failed;
					migrationRequest.Message = jobSeekerRequest.IsNull()
						? "JobSeekerRequest (Toggle Flag) record not found"
						: "EmployerRequest (Toggle Flag) record not found";
				}

				migrationRequest.ProcessedBy = DateTime.UtcNow;
				Repositories.Migration.SaveChanges(true);
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}
			return response;
		}

		#endregion

		#region Job Orders

		/// <summary>
		/// Sends a job order request from the migration database to the focus database.
		/// </summary>
		/// <param name="request">The import request object.</param>
		public JobOrderImportResponse ProcessJobOrder([NotNull] JobOrderImportRequest request)
		{
			if (request == null) throw new ArgumentNullException("request");

			var response = new JobOrderImportResponse(request);

			try
			{
				var migrationRequest = Repositories.Migration.FindById<JobOrderRequest>(request.JobOrderRequestId);

				var saveJobRequest = DeserializeSaveJobRequestFunc(migrationRequest);

				var callService = true;
				if (migrationRequest.IsUpdate)
				{
					var originalRequest = GetJobOrderRequestByExternalIdFunc(migrationRequest);
					if (originalRequest.IsNotNull())
						saveJobRequest.Job.Id = originalRequest.FocusId;
					else
						callService = false;
				}

				if (callService)
				{
					var employerRequest = GetEmployerRequestByEmployeeMigrationIdFunc(saveJobRequest);

					if (employerRequest.IsNotNull())
					{
						PrepareServiceRequestPropertiesFunc(saveJobRequest, request);

						var employee = Repositories.Core.Employees.FirstOrDefault(e => e.Id == employerRequest.FocusId);
						if (employee == null)
						{
							migrationRequest.Status = MigrationStatus.Failed;
							migrationRequest.Message = "Employee record not found";
						}
						else
						{
							SaveJobResponse saveJobResponse = null;

							var jobQuery = Repositories.Core.Resumes.Where(x => x.Id == migrationRequest.FocusId && migrationRequest.Status == MigrationStatus.Processed);
							if (migrationRequest.IsUpdate || !jobQuery.Any())
							{
								lock (SyncObject)
								{
									if (migrationRequest.IsUpdate || !jobQuery.Any())
									{
										PopulateSaveJobRequestFunc(saveJobRequest, employee);
										saveJobResponse = SaveJobFunc(saveJobRequest);
									}
								}
							}


							var focusId = (saveJobResponse != null && saveJobResponse.Job != null && saveJobResponse.Job.Id.HasValue)
								? saveJobResponse.Job.Id.Value
								: 0;

							CheckJobOrderServiceResponseAction(saveJobResponse, migrationRequest, focusId);

							if( saveJobResponse != null && saveJobResponse.Acknowledgement == AcknowledgementType.Success )
							{
								var originalJobId = request.JobId;
								var originalRequest = request.JobOrderRequestId;
								var originalLensPostingId = request.InitialLensPostingId;

								var childElementsSaved = false;

								request.JobId = focusId;

								var jobLocationRequestIds = LookupJobOrderRequestIdsByParentFunc(saveJobRequest.JobMigrationId,
									MigrationJobOrderRecordType.Location);

								if (jobLocationRequestIds.IsNotNullOrEmpty())
								{
									jobLocationRequestIds.ForEach(jobLocationRequestId =>
									{
										request.JobOrderRequestId = jobLocationRequestId;

										ProcessJobLocationAction(request);
									});
									childElementsSaved = true;
								}

								var jobAddressRequestIds = LookupJobOrderRequestIdsByParentFunc(saveJobRequest.JobMigrationId,
									MigrationJobOrderRecordType.Address);
								if (jobAddressRequestIds.IsNotNullOrEmpty())
								{
									jobAddressRequestIds.ForEach(jobAddressRequestId =>
									{
										request.JobOrderRequestId = jobAddressRequestId;
										ProcessJobAddressAction(request);
									});
									childElementsSaved = true;
								}

								var jobCertificateRequestIds = LookupJobOrderRequestIdsByParentFunc(saveJobRequest.JobMigrationId,
									MigrationJobOrderRecordType.Certificate);
								if (jobCertificateRequestIds.IsNotNullOrEmpty())
								{
									jobCertificateRequestIds.ForEach(jobCertificateRequestId =>
									{
										request.JobOrderRequestId = jobCertificateRequestId;

										ProcessJobCertificateAction(request);
									});
									childElementsSaved = true;
								}

								var jobLicenceRequestIds = LookupJobOrderRequestIdsByParentFunc(saveJobRequest.JobMigrationId,
									MigrationJobOrderRecordType.Licence);
								if (jobLicenceRequestIds.IsNotNullOrEmpty())
								{
									jobLicenceRequestIds.ForEach(jobLicenceRequestId =>
									{
										request.JobOrderRequestId = jobLicenceRequestId;

										ProcessJobLicenceAction(request);
									});
									childElementsSaved = true;
								}

								var jobLanguageRequestIds = LookupJobOrderRequestIdsByParentFunc(saveJobRequest.JobMigrationId,
									MigrationJobOrderRecordType.Language);
								if (jobLanguageRequestIds.IsNotNullOrEmpty())
								{
									jobLanguageRequestIds.ForEach(jobLanguageRequestId =>
									{
										request.JobOrderRequestId = jobLanguageRequestId;
										ProcessJobLanguageAction(request);
									});
									childElementsSaved = true;
								}

								var jobSpecialRequirementRequestIds = LookupJobOrderRequestIdsByParentFunc(saveJobRequest.JobMigrationId,
									MigrationJobOrderRecordType.SpecialRequirement);
								if (jobSpecialRequirementRequestIds.IsNotNullOrEmpty())
								{
									jobSpecialRequirementRequestIds.ForEach(jobSpecialRequirementRequestId =>
									{
										request.JobOrderRequestId = jobSpecialRequirementRequestId;
										ProcessJobSpecialRequirementAction(request);
									});
									childElementsSaved = true;
								}

								var jobProgramOfStudyRequestIds = LookupJobOrderRequestIdsByParentFunc(saveJobRequest.JobMigrationId,
									MigrationJobOrderRecordType.ProgramOfStudy);
								if (jobProgramOfStudyRequestIds.IsNotNullOrEmpty())
								{
									jobProgramOfStudyRequestIds.ForEach(jobProgramOfStudyRequestId =>
									{
										request.JobOrderRequestId = jobProgramOfStudyRequestId;
										ProcessJobProgramOfStudyAction(request);
									});
									childElementsSaved = true;
								}

								request.JobOrderRequestId = originalRequest;
								request.InitialLensPostingId = saveJobRequest.LensPostingMigrationId;
								request.IgnoreClient = request.IgnoreClient;
								request.SavedJobWithPostingHtml = childElementsSaved ? null : saveJobResponse.Job;

								GenerateJobPostingFunc(request);

								request.JobId = originalJobId;
								request.InitialLensPostingId = originalLensPostingId;
							}
							else
							{
								migrationRequest.Status = MigrationStatus.Failed;
								if (migrationRequest.Message.IsNullOrEmpty())
									migrationRequest.Message = "SaveJobResponse not saved";
							}
						}
					}

				}
				else
				{
					migrationRequest.Status = MigrationStatus.Failed;
					migrationRequest.Message = "Unable to find original request to update";
				}

				migrationRequest.ProcessedBy = DateTime.UtcNow;
				Repositories.Migration.SaveChanges(true);
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}
			return response;
		}

		public Action<JobOrderImportRequest> ProcessJobProgramOfStudyAction;

		private void ProcessJobProgramOfStudy(JobOrderImportRequest request)
		{
			ProcessJobProgramOfStudy(request, false);
		}

		public Action<JobOrderImportRequest> ProcessJobSpecialRequirementAction;

		private void ProcessJobSpecialRequirement(JobOrderImportRequest request)
		{
			ProcessJobSpecialRequirement(request, false);
		}

		public Action<JobOrderImportRequest> ProcessJobLanguageAction;

		private void ProcessJobLanguage(JobOrderImportRequest request)
		{
			ProcessJobLanguage(request, false);
		}

		public Action<JobOrderImportRequest> ProcessJobLicenceAction;

		private void ProcessJobLicence(JobOrderImportRequest request)
		{
			ProcessJobLicence(request, false);
		}

		public Action<JobOrderImportRequest> ProcessJobCertificateAction;

		private void ProcessJobCertificate(JobOrderImportRequest request)
		{
			ProcessJobCertificate(request, false);
		}

		public Action<JobOrderImportRequest> ProcessJobAddressAction;

		private void ProcessJobAddress(JobOrderImportRequest request)
		{
			ProcessJobAddress(request, false);
		}

		public Action<JobOrderImportRequest> ProcessJobLocationAction;

		private void ProcessJobLocation(JobOrderImportRequest request)
		{
			ProcessJobLocation(request, false);
		}

		public Action<ServiceResponse, JobOrderRequest, long> CheckJobOrderServiceResponseAction;

		private void CheckJobOrderServiceResponse(SaveJobResponse saveJobResponse, JobOrderRequest migrationRequest, long focusId)
		{
			CheckJobOrderServiceResponse((ServiceResponse)saveJobResponse, migrationRequest, focusId);
		}

		public Action<SaveJobRequest, Employee> PopulateSaveJobRequestFunc;

		private void PopulateSaveJobRequest(SaveJobRequest saveJobRequest, Employee employee)
		{
			saveJobRequest.Job.EmployerId = employee.EmployerId;
			saveJobRequest.Job.EmployeeId = employee.Id;

			var employeeBusinessUnit =
				Repositories.Core.EmployeeBusinessUnits.First(ebu => ebu.EmployeeId == employee.Id && ebu.Default);
			saveJobRequest.Job.BusinessUnitId = employeeBusinessUnit.BusinessUnitId;

			var employeeBusinessUnitDescription =
				employeeBusinessUnit.BusinessUnit.BusinessUnitDescriptions.FirstOrDefault(bud => bud.IsPrimary);
			saveJobRequest.Job.BusinessUnitDescriptionId = (employeeBusinessUnitDescription == null
				? (long?)null
				: employeeBusinessUnitDescription.Id);

			if (!string.IsNullOrEmpty(saveJobRequest.EmployerMigrationLogoId))
			{
				var employerLogoRequest = LookupEmployerRequest(saveJobRequest.EmployerMigrationLogoId,
					MigrationEmployerRecordType.BusinessUnitLogo);
				if (employerLogoRequest != null)
					saveJobRequest.Job.BusinessUnitLogoId = employerLogoRequest.FocusId;
			}

			saveJobRequest.InitialJobStatus = saveJobRequest.Job.JobStatus;
		}

		public Func<SaveJobRequest, SaveJobResponse> SaveJobFunc;

		private SaveJobResponse SaveJob(SaveJobRequest saveJobRequest)
		{
			var saveJobResponse = JobService.SaveJob(saveJobRequest);
			return saveJobResponse;
		}

		public Action<ServiceRequest, ServiceRequest> PrepareServiceRequestPropertiesFunc;

		private static void PrepareServiceRequestPropertiesWrapper(ServiceRequest request, ServiceRequest serviceRequest)
		{
			PrepareServiceRequestProperties(request, serviceRequest);
		}

		public Func<SaveJobRequest, EmployerRequest> GetEmployerRequestByEmployeeMigrationIdFunc;

		private EmployerRequest GetEmployerRequestByEmployeeMigrationId(SaveJobRequest saveJobRequest)
		{
			var employerRequest = LookupEmployerRequest(saveJobRequest.EmployeeMigrationId);
			return employerRequest;
		}

		public Func<JobOrderRequest, JobOrderRequest> GetJobOrderRequestByExternalIdFunc;

		private JobOrderRequest GetJobOrderRequestByExternalId(JobOrderRequest migrationRequest)
		{
			var originalRequest = LookupJobOrderRequest(migrationRequest.ExternalId);
			return originalRequest;
		}

		public Func<JobOrderRequest, SaveJobRequest> DeserializeSaveJobRequestFunc;

		private static SaveJobRequest DeserializeSaveJobRequest(JobOrderRequest migrationRequest)
		{
			var saveJobRequest = migrationRequest.Request.DataContractDeserialize<SaveJobRequest>();
			return saveJobRequest;
		}

		/// <summary>
		/// Sends a job location request from the migration database to the focus database.
		/// </summary>
		/// <param name="request">The import request object.</param>
		/// <param name="resetRepository">Whether to reset the migration repository</param>
		/// <returns>The response</returns>
		public JobOrderImportResponse ProcessJobLocation(JobOrderImportRequest request, bool resetRepository = true)
		{
			var response = new JobOrderImportResponse(request);

			try
			{
				var migrationRequest = Repositories.Migration.FindById<JobOrderRequest>(request.JobOrderRequestId);

				var locationRequest = migrationRequest.Request.DataContractDeserialize<SaveJobLocationsRequest>();

				var jobId = request.JobId;
				if (jobId == null)
				{
					var jobRequest = LookupJobOrderRequest(locationRequest.JobMigrationId);
					if (jobRequest != null)
						jobId = jobRequest.FocusId;
				}

				if (jobId.HasValue)
				{
					PrepareServiceRequestPropertiesWrapper(locationRequest, request);

					locationRequest.JobLocations.ForEach(jobLocation =>
					{
						jobLocation.JobId = jobId.Value;

						// If the address id is -1, then this indicates the job address should be the business unit address
						if (jobLocation.Id == -1)
						{
							jobLocation.Id = null;
							//var address = Repositories.Core.BusinessUnitAddresses.First(bua => bua.BusinessUnitId == job.BusinessUnitId);

							var address = (from j in Repositories.Core.Jobs
								join a in Repositories.Core.BusinessUnitAddresses
									on j.BusinessUnitId equals a.BusinessUnitId
								where j.Id == jobId
								select a.AsDto()).First();

							var state = RuntimeContext.Helpers.Lookup.GetLookup(LookupTypes.States).FirstOrDefault(l => l.Id == address.StateId);
							jobLocation.Location = string.Format("{0}, {1} ({2})", address.TownCity, state == null ? "" : state.ExternalId, address.PostcodeZip);
						}
					});
					locationRequest.Criteria = new JobLocationCriteria
					{
						JobId = jobId
					};

					var saveResponse = JobService.SaveJobLocations(locationRequest);

					CheckJobOrderServiceResponse(saveResponse, migrationRequest, jobId.Value);
				}
				else
				{
					migrationRequest.Status = MigrationStatus.Failed;
					migrationRequest.Message = "JobOrderRequest record not found";
				}

				migrationRequest.ProcessedBy = DateTime.UtcNow;
				Repositories.Migration.SaveChanges(resetRepository);
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}
			return response;
		}

		/// <summary>
		/// Sends a job address request from the migration database to the focus database.
		/// </summary>
		/// <param name="request">The import request object.</param>
		/// <param name="resetRepository">Whether to reset the migration repository</param>
		/// <returns>The response</returns>
		public JobOrderImportResponse ProcessJobAddress(JobOrderImportRequest request, bool resetRepository = true)
		{
			var response = new JobOrderImportResponse(request);

			try
			{
				var migrationRequest = Repositories.Migration.FindById<JobOrderRequest>(request.JobOrderRequestId);

				var addressRequest = migrationRequest.Request.DataContractDeserialize<SaveJobAddressRequest>();

				var jobId = request.JobId;
				if (jobId == null)
				{
					var jobRequest = LookupJobOrderRequest(addressRequest.JobMigrationId);
					if (jobRequest != null)
						jobId = jobRequest.FocusId;
				}

				if (jobId.HasValue)
				{
					PrepareServiceRequestPropertiesWrapper(addressRequest, request);

					// If the address id is -1, then this indicates the job address should be the business unit address
					if (addressRequest.JobAddress.Id == -1)
					{
						addressRequest.JobAddress.Id = null;

						var address = (from j in Repositories.Core.Jobs
							join a in Repositories.Core.BusinessUnitAddresses
								on j.BusinessUnitId equals a.BusinessUnitId
							where j.Id == jobId
							select a.AsDto()).First();

						addressRequest.JobAddress = new JobAddressDto
						{
							Line1 = address.Line1,
							Line2 = address.Line2,
							Line3 = address.Line3,
							TownCity = address.TownCity,
							CountyId = address.CountyId,
							CountryId = address.CountryId,
							StateId = address.StateId,
							PostcodeZip = address.PostcodeZip,
							IsPrimary = true
						};
					}

					addressRequest.JobAddress.JobId = jobId.Value;

					var saveResponse = JobService.SaveJobAddress(addressRequest);

					CheckJobOrderServiceResponse(saveResponse, migrationRequest, jobId.Value);
				}
				else
				{
					migrationRequest.Status = MigrationStatus.Failed;
					migrationRequest.Message = "JobOrderRequest record not found";
				}

				migrationRequest.ProcessedBy = DateTime.UtcNow;
				Repositories.Migration.SaveChanges(resetRepository);
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}
			return response;
		}

		/// <summary>
		/// Sends a job certificate request from the migration database to the focus database.
		/// </summary>
		/// <param name="request">The import request object.</param>
		/// <param name="resetRepository">Whether to reset the migration repository</param>
		/// <returns>The response</returns>
		public JobOrderImportResponse ProcessJobCertificate(JobOrderImportRequest request, bool resetRepository = true)
		{
			var response = new JobOrderImportResponse(request);

			try
			{
				var migrationRequest = Repositories.Migration.FindById<JobOrderRequest>(request.JobOrderRequestId);

				var certificateRequest = migrationRequest.Request.DataContractDeserialize<SaveJobCertificatesRequest>();

				var jobId = request.JobId;
				if (jobId == null)
				{
					var jobRequest = LookupJobOrderRequest(certificateRequest.JobMigrationId);
					if (jobRequest != null)
						jobId = jobRequest.FocusId;
				}

				if (jobId.HasValue)
				{
					PrepareServiceRequestPropertiesWrapper(certificateRequest, request);

					certificateRequest.JobCertificates.ForEach(jc => jc.JobId = jobId.Value);
					certificateRequest.Criteria = new JobCertificateCriteria
					{
						JobId = jobId
					};

					var saveResponse = JobService.SaveJobCertificates(certificateRequest);

					CheckJobOrderServiceResponse(saveResponse, migrationRequest, jobId.Value);
				}
				else
				{
					migrationRequest.Status = MigrationStatus.Failed;
					migrationRequest.Message = "JobOrderRequest record not found";
				}

				migrationRequest.ProcessedBy = DateTime.UtcNow;
				Repositories.Migration.SaveChanges(resetRepository);
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}
			return response;
		}

		/// <summary>
		/// Sends a job licence request from the migration database to the focus database.
		/// </summary>
		/// <param name="request">The import request object.</param>
		/// <param name="resetRepository">Whether to reset the migration repository</param>
		/// <returns>The response</returns>
		public JobOrderImportResponse ProcessJobLicence(JobOrderImportRequest request, bool resetRepository = true)
		{
			var response = new JobOrderImportResponse(request);

			try
			{
				var migrationRequest = Repositories.Migration.FindById<JobOrderRequest>(request.JobOrderRequestId);

				var licenceRequest = migrationRequest.Request.DataContractDeserialize<SaveJobLicencesRequest>();

				var jobId = request.JobId;
				if (jobId == null)
				{
					var jobRequest = LookupJobOrderRequest(licenceRequest.JobMigrationId);
					if (jobRequest != null)
						jobId = jobRequest.FocusId;
				}

				if (jobId.HasValue)
				{
					PrepareServiceRequestPropertiesWrapper(licenceRequest, request);

					licenceRequest.JobLicences.ForEach(jl => jl.JobId = jobId.Value);
					licenceRequest.Criteria = new JobLicenceCriteria
					{
						JobId = jobId
					};

					var saveResponse = JobService.SaveJobLicences(licenceRequest);

					CheckJobOrderServiceResponse(saveResponse, migrationRequest, jobId.Value);
				}
				else
				{
					migrationRequest.Status = MigrationStatus.Failed;
					migrationRequest.Message = "JobOrderRequest record not found";
				}

				migrationRequest.ProcessedBy = DateTime.UtcNow;
				Repositories.Migration.SaveChanges(resetRepository);
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}
			return response;
		}

		/// <summary>
		/// Sends a job language request from the migration database to the focus database.
		/// </summary>
		/// <param name="request">The import request object.</param>
		/// <param name="resetRepository">Whether to reset the migration repository</param>
		/// <returns>The response</returns>
		public JobOrderImportResponse ProcessJobLanguage(JobOrderImportRequest request, bool resetRepository = true)
		{
			var response = new JobOrderImportResponse(request);

			try
			{
				var migrationRequest = Repositories.Migration.FindById<JobOrderRequest>(request.JobOrderRequestId);

				var languageRequest = migrationRequest.Request.DataContractDeserialize<SaveJobLanguagesRequest>();

				var jobId = request.JobId;
				if (jobId == null)
				{
					var jobRequest = LookupJobOrderRequest(languageRequest.JobMigrationId);
					if (jobRequest != null)
						jobId = jobRequest.FocusId;
				}

				if (jobId.HasValue)
				{
					PrepareServiceRequestPropertiesWrapper(languageRequest, request);

					languageRequest.JobLanguages.ForEach(jl => jl.JobId = jobId.Value);
					languageRequest.Criteria = new JobLanguageCriteria
					{
						JobId = jobId
					};

					var saveResponse = JobService.SaveJobLanguages(languageRequest);

					CheckJobOrderServiceResponse(saveResponse, migrationRequest, jobId.Value);
				}
				else
				{
					migrationRequest.Status = MigrationStatus.Failed;
					migrationRequest.Message = "JobOrderRequest record not found";
				}

				migrationRequest.ProcessedBy = DateTime.UtcNow;
				Repositories.Migration.SaveChanges(resetRepository);
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}
			return response;
		}

		/// <summary>
		/// Sends a job special requirement request from the migration database to the focus database.
		/// </summary>
		/// <param name="request">The import request object.</param>
		/// <param name="resetRepository">Whether to reset the migration repository</param>
		/// <returns>The response</returns>
		public JobOrderImportResponse ProcessJobSpecialRequirement(JobOrderImportRequest request, bool resetRepository = true)
		{
			var response = new JobOrderImportResponse(request);

			try
			{
				var migrationRequest = Repositories.Migration.FindById<JobOrderRequest>(request.JobOrderRequestId);

				var requirementRequest = migrationRequest.Request.DataContractDeserialize<SaveJobSpecialRequirementsRequest>();

				var jobId = request.JobId;
				if (jobId == null)
				{
					var jobRequest = LookupJobOrderRequest(requirementRequest.JobMigrationId);
					if (jobRequest != null)
						jobId = jobRequest.FocusId;
				}

				if (jobId.HasValue)
				{
					PrepareServiceRequestPropertiesWrapper(requirementRequest, request);

					requirementRequest.JobSpecialRequirements.ForEach(jsr => jsr.JobId = jobId.Value);
					requirementRequest.Criteria = new JobSpecialRequirementCriteria
					{
						JobId = jobId
					};

					var saveResponse = JobService.SaveJobSpecialRequirements(requirementRequest);

					CheckJobOrderServiceResponse(saveResponse, migrationRequest, jobId.Value);
				}
				else
				{
					migrationRequest.Status = MigrationStatus.Failed;
					migrationRequest.Message = "JobOrderRequest record not found";
				}

				migrationRequest.ProcessedBy = DateTime.UtcNow;
				Repositories.Migration.SaveChanges(resetRepository);
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}
			return response;
		}

		/// <summary>
		/// Sends a job program of study request from the migration database to the focus database.
		/// </summary>
		/// <param name="importRequest">The import request object.</param>
		/// <param name="resetRepository">Whether to reset the migration repository</param>
		/// <returns>The response</returns>
		public JobOrderImportResponse ProcessJobProgramOfStudy(JobOrderImportRequest importRequest, bool resetRepository = true)
		{
			var importResponse = new JobOrderImportResponse(importRequest);

			try
			{
				var migrationRequest = Repositories.Migration.FindById<JobOrderRequest>(importRequest.JobOrderRequestId);

				var programOfStudyRequest = migrationRequest.Request.DataContractDeserialize<SaveJobProgramsOfStudyRequest>();

				var jobId = importRequest.JobId;
				if (jobId == null)
				{
					var jobRequest = LookupJobOrderRequest(programOfStudyRequest.JobMigrationId);
					if (jobRequest != null)
						jobId = jobRequest.FocusId;
				}

				if (jobId.HasValue)
				{
					PrepareServiceRequestPropertiesWrapper(programOfStudyRequest, importRequest);

					programOfStudyRequest.JobProgramsOfStudy.ForEach(jp => jp.JobId = jobId.Value);
					programOfStudyRequest.Criteria = new JobProgramsOfStudyCriteria
					{
						JobId = jobId
					};

					var saveResponse = JobService.SaveJobProgramsOfStudy(programOfStudyRequest);

					CheckJobOrderServiceResponse(saveResponse, migrationRequest, jobId.Value);
				}
				else
				{
					migrationRequest.Status = MigrationStatus.Failed;
					migrationRequest.Message = "JobOrderRequest record not found";
				}

				migrationRequest.ProcessedBy = DateTime.UtcNow;
				Repositories.Migration.SaveChanges(resetRepository);
			}
			catch (Exception ex)
			{
				importResponse.SetFailure(FormatErrorMessage(importRequest, ErrorTypes.Unknown), ex);
				Logger.Error(importRequest.LogData(), importResponse.Message, importResponse.Exception);
			}
			return importResponse;
		}

		public Func<JobOrderImportRequest, JobOrderImportResponse> GenerateJobPostingFunc;

		/// <summary>
		/// Updates a job so that the Posting field is generated.
		/// </summary>
		/// <param name="request">The request object for the import service.</param>
		public JobOrderImportResponse GenerateJobPosting(JobOrderImportRequest request)
		{
			var response = new JobOrderImportResponse(request);

			try
			{
				var savedJob = request.SavedJobWithPostingHtml;

				if (savedJob.IsNull())
				{
					// Job must be saved, as posting Html is only generated when saving an existing job
					var saveRequest = new SaveJobRequest
					{
						ClientTag = request.ClientTag,
						SessionId = request.SessionId,
						RequestId = request.RequestId,
						UserContext = request.UserContext,
						Module = FocusModules.General,
						Job = Repositories.Core.Jobs.First(j => j.Id == request.JobId).AsDto()
					};

					var saveResponse = JobService.SaveJob(saveRequest);

					if (saveResponse.Acknowledgement == AcknowledgementType.Success && saveResponse.Job.JobStatus != JobStatuses.Draft && (saveResponse.Job.JobStatus == JobStatuses.Active || request.IgnoreLens))
						savedJob = saveResponse.Job;
				}
				if (savedJob.IsNotNull() && (savedJob.JobStatus != JobStatuses.Draft || request.InitialLensPostingId.IsNotNullOrEmpty()))
				{
					var postRequest = new PostJobRequest
					{
						ClientTag = request.ClientTag,
						SessionId = request.SessionId,
						RequestId = request.RequestId,
						UserContext = request.UserContext,
						Job = savedJob,
						Module = FocusModules.Assist,
						IgnoreLens = request.IgnoreLens,
						IgnoreClient = request.IgnoreClient,
						InitialLensPostingId = request.IgnoreLens ? request.InitialLensPostingId : null,
						InitialApprovalStatus = savedJob.ApprovalStatus
					};

					JobService.PostJob(postRequest);
				}
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}
			return response;
		}

		/// <summary>
		/// Processes a spidered job to from the migration table to the import table.
		/// </summary>
		/// <param name="request">The request object for the import service.</param>
		public JobOrderImportResponse ProcessSpideredJob(JobOrderImportRequest request)
		{
			var response = new JobOrderImportResponse(request);

			try
			{
				var migrationRequest = Repositories.Migration.FindById<JobOrderRequest>(request.JobOrderRequestId);

				var saveSpideredJobRequest = migrationRequest.Request.DataContractDeserialize<SaveSpideredJobRequest>();

				var callService = true;
				if (migrationRequest.IsUpdate)
				{
					var originalRequest = LookupJobOrderRequest(migrationRequest.ExternalId);
					if (originalRequest.IsNotNull())
						saveSpideredJobRequest.JobId = originalRequest.FocusId;
					else
						callService = false;
				}

				if (callService)
				{
					PrepareServiceRequestProperties(saveSpideredJobRequest, request);

					SaveSpideredJobResponse saveSpideredJobResponse = null;
					var spideredJobQuery =
						Repositories.Core.Resumes.Where(
							x => x.Id == migrationRequest.FocusId && migrationRequest.Status == MigrationStatus.Processed);

					if (migrationRequest.IsUpdate || !spideredJobQuery.Any())
					{
						lock (SyncObject)
						{
							if (migrationRequest.IsUpdate || !spideredJobQuery.Any())
							{
								saveSpideredJobResponse = JobService.SaveSpideredJob(saveSpideredJobRequest);
							}
						}
					}

					var focusId = (saveSpideredJobResponse != null && saveSpideredJobResponse.Posting != null && saveSpideredJobResponse.Posting.Id.HasValue)
						? saveSpideredJobResponse.Posting.Id.Value
						: 0;

					CheckJobOrderServiceResponse(saveSpideredJobResponse, migrationRequest, focusId);

					if( saveSpideredJobResponse != null && saveSpideredJobResponse.Acknowledgement == AcknowledgementType.Success )
					{
						var originalJobId = request.JobId;
						var originalRequest = request.JobOrderRequestId;
						var originalLensPostingId = request.InitialLensPostingId;

						var childElementsSaved = false;

						request.JobId = focusId;

						request.JobOrderRequestId = originalRequest;
						request.InitialLensPostingId = saveSpideredJobRequest.LensPostingMigrationId;
						request.OfficeMigrationId = saveSpideredJobRequest.OfficeMigrationId;
						request.IgnoreClient = request.IgnoreClient;
						request.SavedSpideredJobWithPostingHtml = childElementsSaved ? null : saveSpideredJobResponse.Posting;

						request.JobId = originalJobId;
						request.InitialLensPostingId = originalLensPostingId;

						migrationRequest.Status = MigrationStatus.Processed;
						migrationRequest.FocusId = focusId;
						migrationRequest.ProcessedBy = DateTime.UtcNow;
						Repositories.Migration.SaveChanges(true);

						var spideredJobReferrals = LookupJobOrderRequestIdsByParent(saveSpideredJobRequest.LensPostingMigrationId, MigrationJobOrderRecordType.SpideredJobReferral);

						if (spideredJobReferrals.IsNotNullOrEmpty())
						{
							spideredJobReferrals.ForEach(spideredJobReferral =>
							{
								request.JobOrderRequestId = spideredJobReferral;
								ProcessSpideredJobReferral(request);
							});
							childElementsSaved = true;
						}
					}
				}
				else
				{
					migrationRequest.Status = MigrationStatus.Failed;
					migrationRequest.Message = "Unable to find original request to update";
				}

				migrationRequest.ProcessedBy = DateTime.UtcNow;
				Repositories.Migration.SaveChanges(true);
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}
			return response;

		}

		public JobOrderImportResponse ProcessSpideredJobReferral([NotNull] JobOrderImportRequest request)
		{
			if (request == null) throw new ArgumentNullException("request");

			var response = new JobOrderImportResponse(request);

			try
			{
				var migrationRequest = Repositories.Migration.FindById<JobOrderRequest>(request.JobOrderRequestId);
				var candidateRequest = migrationRequest.Request.DataContractDeserialize<ReferCandidateRequest>();

				var callService = true;
				if (migrationRequest.IsUpdate)
				{
					var originalRequest = LookupJobOrderRequest(migrationRequest.ExternalId, MigrationJobOrderRecordType.SpideredJobReferral);
					if (originalRequest.IsNull())
						callService = false;
				}

				if (callService)
				{
					var jobSeekerRequest = LookupJobSeekerRequest(candidateRequest.JobSeekerMigrationId, MigrationJobSeekerRecordType.JobSeeker);
					var jobOrderRequest = LookupJobOrderRequest(candidateRequest.JobMigrationId, MigrationJobOrderRecordType.SpideredJob);

					if (jobSeekerRequest.IsNotNull() && jobOrderRequest.IsNotNull())
					{
						PrepareServiceRequestProperties(candidateRequest, request);

						candidateRequest.PersonId = jobSeekerRequest.FocusId;
						candidateRequest.JobId = jobOrderRequest.FocusId;

						var applyForJobRequest = new ApplyForJobRequest();
						applyForJobRequest.LensPostingId = candidateRequest.LensPostingId;

						var resume = LookupJobSeekerRequest("FC01:" + candidateRequest.ResumeMigrationId, MigrationJobSeekerRecordType.Resume);
						applyForJobRequest.ResumeId = resume.FocusId;
						applyForJobRequest.ReviewApplication = false; /* do not want this to be reviewed */
						applyForJobRequest.IsMigratedReferral = true;
						//applyForJobRequest.IsEligible =;
						//applyForJobRequest.Notes =;
						//applyForJobRequest.MatchingScore =;
						//applyForJobRequest.UserContext.UserId =;
						//applyForJobRequest.UserContext.IsShadowingUser =;

						PrepareServiceRequestProperties(applyForJobRequest, request);

						ApplyForJobResponse organizationServiceResponse = null;

						var referralQuery = Repositories.Core.Applications.Where(x => x.Id == migrationRequest.FocusId && migrationRequest.Status == MigrationStatus.Processed);
						if (migrationRequest.IsUpdate || !referralQuery.Any())
						{
							lock (SyncObject)
							{
								if (migrationRequest.IsUpdate || !referralQuery.Any())
								{
									organizationServiceResponse = OrganizationService.ApplyForJob(applyForJobRequest);
								}
							}
						}

						if (organizationServiceResponse != null && organizationServiceResponse.Acknowledgement == AcknowledgementType.Success)
						{
							migrationRequest.FocusId = organizationServiceResponse.ApplicationId.GetValueOrDefault(0);
							migrationRequest.Status = MigrationStatus.Processed;
						}
						else
						{
							migrationRequest.Status = MigrationStatus.Failed;
							migrationRequest.Message = FormatResponseMessage(organizationServiceResponse);
						}
					}
					else
					{
						migrationRequest.Status = MigrationStatus.Failed;
						migrationRequest.Message = jobSeekerRequest.IsNull()
							? string.Format("JobSeekerRequest ({0}) record not found", MigrationJobSeekerRecordType.JobSeeker)
							: string.Format("JobOrderRequest ({0}) record not found", MigrationJobOrderRecordType.SpideredJobReferral);
					}
				}
				else
				{
					migrationRequest.Status = MigrationStatus.Failed;
					migrationRequest.Message = "Unable to find original request to update";
				}

				migrationRequest.ProcessedBy = DateTime.UtcNow;
				Repositories.Migration.SaveChanges(true);
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}
			return response;
		}

		public JobOrderImportResponse ProcessSavedJob([NotNull] JobOrderImportRequest request)
		{
			if (request == null) throw new ArgumentNullException("request");

			var response = new JobOrderImportResponse(request);
			JobOrderRequest migrationRequest = null;

			try
			{
				migrationRequest = Repositories.Migration.FindById<JobOrderRequest>(request.JobOrderRequestId);

				var saveSavedJobRequest = migrationRequest.Request.DataContractDeserialize<SaveSavedJobRequest>();

				var bookmark = new Bookmark();

				if (migrationRequest.IsUpdate)
				{
					var originalRequest = LookupJobOrderRequest(migrationRequest.ExternalId);
					if (originalRequest.IsNotNull())
					{
						bookmark = Repositories.Core.FindById<Bookmark>(migrationRequest.FocusId);
						// TODO update the migration table record with the id of the record migrated
						// talentRequest.UpdateExistingEmployeeId = originalRequest.FocusId;
					}
				}

				// TODO set the postingid on the job viewed - this is done by the lenspostingid
				// bookmark = savedJob.CopyTo(bookmark);

				bookmark.Criteria = saveSavedJobRequest.LensPostingMigrationId;
				bookmark.EntityId = null;
				bookmark.Name = saveSavedJobRequest.BookmarkName;

				var jobSeekerRequest = LookupJobSeekerRequest(saveSavedJobRequest.JobSeekerMigrationId,
					MigrationJobSeekerRecordType.JobSeeker);
				if (jobSeekerRequest != null)
				{
					bookmark.UserId = jobSeekerRequest.FocusId;
				}
				else
				{
					throw new InvalidDataException(string.Format("ProcessSavedJob():JobSeekerMigrationId:{0} JobSeeker record not found", saveSavedJobRequest.JobSeekerMigrationId));
				}

				/* need to make sure the jobseekerid is populated and use it to lookup the userid */
				bookmark.UserType = UserTypes.Talent; /* need to identify the usertype. is talent right for spidered jobs? */
				bookmark.Type = BookmarkTypes.Posting;
				/* need to identify the type. Type  4 is a posting, so should be set to this for all */

				Repositories.Core.Add(bookmark);

				Repositories.Core.SaveChanges(true);

				migrationRequest.Status = MigrationStatus.Processed;
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				if (migrationRequest != null)
				{
					migrationRequest.Status = MigrationStatus.Failed;
					migrationRequest.Message = ex.Message;
				}
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}
			if (migrationRequest != null)
			{
				migrationRequest.ProcessedBy = DateTime.UtcNow;
			}
			Repositories.Migration.SaveChanges(true);

			return response;
		}

		#endregion

		#region Assist

		/// <summary>
		/// Sends an Create Assist User request from the migration database to the focus database
		/// </summary>
		/// <param name="request">The Assist U</param>
		/// <returns>A response indicating success or failure</returns>
		public AssistImportResponse ProcessCreateAssistUser(AssistImportRequest request)
		{
			var response = new AssistImportResponse(request);

			try
			{
				var migrationRequest = Repositories.Migration.FindById<UserRequest>(request.AssistRequestId);

				var assistRequest = migrationRequest.Request.DataContractDeserialize<CreateAssistUserRequest>();

				PrepareServiceRequestPropertiesWrapper(assistRequest, request);

				var assistResponse = AccountService.CreateAssistUser(assistRequest);

				if (assistResponse.Acknowledgement == AcknowledgementType.Success)
				{
					migrationRequest.FocusId = assistResponse.UserId;
					migrationRequest.Status = MigrationStatus.Processed;
				}
				else
				{
					migrationRequest.Status = MigrationStatus.Failed;
					migrationRequest.Message = FormatResponseMessage(assistResponse);
				}

				migrationRequest.ProcessedBy = DateTime.UtcNow;
				Repositories.Migration.SaveChanges(true);
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}
			return response;
		}

		/// <summary>
		/// Sends a note/reminder request from the migration database to the focus database
		/// </summary>
		/// <param name="importRequest"></param>
		/// <returns>A response indicating success or failure</returns>
		public AssistImportResponse ProcessNoteReminder(AssistImportRequest importRequest)
		{
			var importResponse = new AssistImportResponse(importRequest);

			try
			{
				var migrationRequest = Repositories.Migration.FindById<UserRequest>(importRequest.AssistRequestId);

				var assistRequest = migrationRequest.Request.DataContractDeserialize<SaveNoteReminderRequest>();

				var jobSeekerRequest = LookupJobSeekerRequest(assistRequest.JobSeekerMigrationId, MigrationJobSeekerRecordType.JobSeeker);
				if (jobSeekerRequest != null)
				{
					var userRequest = LookupUserRequest(assistRequest.StaffMigrationId, MigrationAssistUserRecordType.AssistUser) ??
					                  LookupUserRequestBySecondary(assistRequest.StaffSecondaryMigrationId, MigrationAssistUserRecordType.AssistUser);

					if (userRequest != null)
					{
						PrepareServiceRequestPropertiesWrapper(assistRequest, importRequest);

						assistRequest.NoteReminder.EntityId = jobSeekerRequest.FocusId;
						assistRequest.NoteReminder.CreatedBy = userRequest.FocusId;

						if (assistRequest.ReminderRecipients.IsNotNullOrEmpty())
						{
							assistRequest.ReminderRecipients.ForEach(
								r => r.RecipientEntityId = (r.RecipientEntityType == EntityTypes.JobSeeker ? jobSeekerRequest.FocusId : userRequest.FocusId));
						}

						SaveNoteReminderResponse assistResponse = null;

						var noteQuery = Repositories.Core.Notes.Where(x => x.Id == migrationRequest.FocusId && migrationRequest.Status == MigrationStatus.Processed);
						if (migrationRequest.IsUpdate || !noteQuery.Any())
						{
							lock (SyncObject)
							{
								if (migrationRequest.IsUpdate || !noteQuery.Any())
								{
									assistResponse = CoreService.SaveNoteReminder(assistRequest);
								}
							}
						}

						if( assistResponse != null && assistResponse.Acknowledgement == AcknowledgementType.Success )
						{
							migrationRequest.FocusId = assistResponse.NoteReminder.Id.GetValueOrDefault(0);
							migrationRequest.Status = MigrationStatus.Processed;
						}
						else
						{
							migrationRequest.Status = MigrationStatus.Failed;
							migrationRequest.Message = FormatResponseMessage(assistResponse);
						}
					}
					else
					{
						migrationRequest.Status = MigrationStatus.Failed;
						migrationRequest.Message = "UserRequest record not found";
					}
				}
				else
				{
					migrationRequest.Status = MigrationStatus.Failed;
					migrationRequest.Message = "JobSeekerRequest record not found";
				}

				migrationRequest.ProcessedBy = DateTime.UtcNow;
				Repositories.Migration.SaveChanges(true);
			}
			catch (Exception ex)
			{
				importResponse.SetFailure(FormatErrorMessage(importRequest, ErrorTypes.Unknown), ex);
				Logger.Error(importRequest.LogData(), importResponse.Message, importResponse.Exception);
			}
			return importResponse;
		}

		/// <summary>
		/// Sends a Save Office request from the migration database to the focus database
		/// </summary>
		/// <param name="request">The request object for the import service.</param>
		/// <returns></returns>
		public AssistImportResponse ProcessSaveOffice(AssistImportRequest request)
		{
			var response = new AssistImportResponse(request);

			try
			{
				var migrationRequest = Repositories.Migration.FindById<UserRequest>(request.AssistRequestId);
				var assistRequest = migrationRequest.Request.DataContractDeserialize<SaveOfficeRequest>();

				PrepareServiceRequestPropertiesWrapper(assistRequest, request);

				var callService = true;
				if (migrationRequest.IsUpdate)
				{
					var originalRequest = LookupUserRequest(migrationRequest.ExternalId, MigrationAssistUserRecordType.Office);
					if (originalRequest.IsNotNull())
						assistRequest.Office.Id = originalRequest.FocusId;
					else
						callService = false;
				}

				if (callService)
				{
					var assistResponse = EmployerService.SaveOffice(assistRequest);

					if (assistResponse.Acknowledgement == AcknowledgementType.Success)
					{
						migrationRequest.FocusId = assistResponse.Office.Id.GetValueOrDefault(0);
						migrationRequest.Status = MigrationStatus.Processed;
					}
					else
					{
						migrationRequest.Status = MigrationStatus.Failed;
						migrationRequest.Message = FormatResponseMessage(assistResponse);
					}
				}
				else
				{
					migrationRequest.Status = MigrationStatus.Failed;
					migrationRequest.Message = "Unable to find original request to update";
				}

				migrationRequest.ProcessedBy = DateTime.UtcNow;
				Repositories.Migration.SaveChanges(true);
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}
			return response;
		}

		/// <summary>
		/// Sends a Person Office Mapping request from the migration database to the focus database
		/// </summary>
		/// <param name="request">The request object for the import service.</param>
		/// <returns></returns>
		public AssistImportResponse ProcessOfficeMappings(AssistImportRequest request)
		{
			var response = new AssistImportResponse(request);

			try
			{
				var migrationRequest = Repositories.Migration.FindById<UserRequest>(request.AssistRequestId);

				var assistRequest = migrationRequest.Request.DataContractDeserialize<PersonOfficeMapperRequest>();

				PrepareServiceRequestPropertiesWrapper(assistRequest, request);

				var userRequest = LookupUserRequest(assistRequest.UserMigrationId, MigrationAssistUserRecordType.AssistUser);

				if (userRequest.IsNotNull())
				{
					var officeRequests = LookupUserRequests(assistRequest.OfficeMigrationIds, MigrationAssistUserRecordType.Office);

					var personId = Repositories.Core.Users.Where(u => u.Id == userRequest.FocusId).Select(u => u.PersonId).FirstOrDefault();

					assistRequest.PersonId = personId;
					assistRequest.PersonOffices = officeRequests.Values.Select(officeRequest => new PersonOfficeMapperDto
					{
						OfficeId = officeRequest.FocusId,
						PersonId = personId
					}).ToList();

					var assistResponse = EmployerService.SavePersonOffices(assistRequest);

					if (assistResponse.Acknowledgement == AcknowledgementType.Success)
					{
						migrationRequest.FocusId = personId;
						migrationRequest.Status = MigrationStatus.Processed;
					}
					else
					{
						migrationRequest.Status = MigrationStatus.Failed;
						migrationRequest.Message = FormatResponseMessage(assistResponse);
					}
				}
				else
				{
					migrationRequest.Status = MigrationStatus.Failed;
					migrationRequest.Message = "AssistUser request record not found";
				}

				migrationRequest.ProcessedBy = DateTime.UtcNow;
				Repositories.Migration.SaveChanges(true);
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}
			return response;
		}

		#endregion

		#region Helpers

		#region Employer Helpers

		/// <summary>
		/// Checks the response from a call to an employer service.
		/// </summary>
		/// <param name="saveResponse">The save response from the employer service.</param>
		/// <param name="migrationRequest">The employer migration request record.</param>
		/// <param name="focusId">The id of any record saved in Focus Assist/Talent.</param>
		private static void CheckEmployerServiceResponse(ServiceResponse saveResponse, EmployerRequest migrationRequest, long focusId)
		{
			if (saveResponse.Acknowledgement == AcknowledgementType.Success)
			{
				migrationRequest.FocusId = focusId;
				migrationRequest.Status = MigrationStatus.Processed;
			}
			else
			{
				migrationRequest.Status = MigrationStatus.Failed;
				migrationRequest.Message = FormatResponseMessage(saveResponse);
			}
		}


		public Func<string, EmployerRequest> LookupEmployerRequestFunc;

		/// <summary>
		/// Looks up the original migration request for an employer.
		/// </summary>
		/// <param name="externalId">The ID of the employer from the source system.</param>
		/// <returns>The original migration request.</returns>
		private EmployerRequest LookupEmployerRequest(string externalId)
		{
			// We need to refactor this. We would only have one or the other, so it makes sense that we have an OR in the query
			// Time is against us so I am making the TalentUser the dominate value to search by as this is what we are workign with
			//var employerRequest = LookupEmployerRequest(externalId, MigrationEmployerRecordType.TalentUser);

			var employerRequest = LookupEmployerRequest(externalId, MigrationEmployerRecordType.Employee);
			return employerRequest;
		}

		/// <summary>
		/// Looks up the original migration request for an employer.
		/// </summary>
		/// <param name="type">The type of migration request (employer, business unit description or logo)</param>
		/// <param name="externalId">The ID of the employer from the source system.</param>
		/// <returns>The original migration request.</returns>
		private EmployerRequest LookupEmployerRequest(string externalId, MigrationEmployerRecordType type)
		{
			return Repositories.Migration.EmployerRequests.FirstOrDefault(er => er.RecordType == type && er.ExternalId == externalId);
		}

		public Func<string, MigrationEmployerRecordType, List<long>> LookupEmployerRequestIdsByParentFunc;

		/// <summary>
		/// Looks up a child requests for an employer.
		/// </summary>
		/// <param name="parentExternalId">The ID of the employer from the source system.</param>
		/// <param name="type">The type of migration request (employer, business unit description or logo)</param>
		/// <returns>The original migration request.</returns>    
		private List<long> LookupEmployerRequestIdsByParent(string parentExternalId, MigrationEmployerRecordType type)
		{
			return Repositories.Migration.EmployerRequests
				.Where(er => er.RecordType == type && er.ParentExternalId == parentExternalId && er.Status == MigrationStatus.ToBeProcessed)
				.Select(js => js.Id)
				.ToList();
		}

		/// <summary>
		/// Looks up the default business unit for an employee.
		/// </summary>
		/// <param name="employeeId">The id of the employee to look up.</param>
		/// <returns>The employee's default business unit.</returns>
		private EmployeeBusinessUnit LookupDefaultBusinessUnitForEmployee(long employeeId)
		{
			var employee = Repositories.Core.Employees.First(e => e.Id == employeeId);
			var employeeBusinessUnit = Repositories.Core.EmployeeBusinessUnits.First(ebu => ebu.EmployeeId == employee.Id && ebu.Default);

			return employeeBusinessUnit;
		}

		#endregion

		#region Job Seeker Helpers

		/// <summary>
		/// Looks up the original migration request for a job seeker.
		/// </summary>
		/// <param name="externalId">The ID of the job seeker from the source system.</param>
		/// <param name="type">The type of request</param>
		/// <param name="checkFocusId">Only return records where the focus ID is set</param>
		/// <returns>The original migration request.</returns>
		private JobSeekerRequest LookupJobSeekerRequest(string externalId, MigrationJobSeekerRecordType type, bool checkFocusId = true)
		{
			return checkFocusId
				? Repositories.Migration.JobSeekerRequests.FirstOrDefault(js => js.RecordType == type && js.ExternalId == externalId && js.FocusId != 0)
				: Repositories.Migration.JobSeekerRequests.FirstOrDefault(js => js.RecordType == type && js.ExternalId == externalId);
		}


		/// <summary>
		/// Looks up the original migration id request for a job seeker request
		/// </summary>
		/// <param name="parentExternalId">The ID of the job seeker from the source system.</param>
		/// <param name="type">The type of request</param>
		/// <returns>The original migration request id.</returns>
		private IEnumerable<long> LookupJobSeekerRequestIdsByParent(string parentExternalId, MigrationJobSeekerRecordType type)
		{
			return Repositories.Migration.JobSeekerRequests
				.Where(js => js.RecordType == type && js.ParentExternalId == parentExternalId && js.Status == MigrationStatus.ToBeProcessed)
				.Select(js => js.Id)
				.ToList();
		}

		#endregion

		#region Job Order Helpers

		/// <summary>
		/// Checks the response from a call to an job service.
		/// </summary>
		/// <param name="saveResponse">The save response from the job service.</param>
		/// <param name="migrationRequest">The job migration request record.</param>
		/// <param name="focusId">The id of any record saved in Focus Assist/Talent.</param>
		private void CheckJobOrderServiceResponse(ServiceResponse saveResponse, JobOrderRequest migrationRequest, long focusId)
		{
			if (saveResponse.Acknowledgement == AcknowledgementType.Success)
			{
				migrationRequest.FocusId = focusId;
				migrationRequest.Status = MigrationStatus.Processed;
			}
			else
			{
				migrationRequest.Status = MigrationStatus.Failed;
				migrationRequest.Message = FormatResponseMessage(saveResponse);
			}
		}

		/// <summary>
		/// Looks up the original migration request for a job.
		/// </summary>
		/// <param name="externalId">The ID of the job order from the source system.</param>
		/// <param name="type">The type of request</param>
		/// <param name="checkFocusId">Only return records where the focus ID is set</param>
		/// <returns>The original migration request.</returns>
		private JobOrderRequest LookupJobOrderRequest(string externalId, MigrationJobOrderRecordType type = MigrationJobOrderRecordType.Job, bool checkFocusId = true)
		{
			return checkFocusId
				? Repositories.Migration.JobOrderRequests.FirstOrDefault(jo => jo.RecordType == type && jo.ExternalId == externalId && jo.FocusId != 0)
				: Repositories.Migration.JobOrderRequests.FirstOrDefault(jo => jo.RecordType == type && jo.ExternalId == externalId);
		}

		public Func<string, MigrationJobOrderRecordType, List<long>> LookupJobOrderRequestIdsByParentFunc;

		/// <summary>
		/// Looks up the original migration id request for a job.
		/// </summary>
		/// <param name="parentExternalId">The ID of the job order from the source system.</param>
		/// <param name="type">The type of request</param>
		/// <returns>The original migration request id.</returns>
		private List<long> LookupJobOrderRequestIdsByParent(string parentExternalId, MigrationJobOrderRecordType type)
		{
			return Repositories.Migration.JobOrderRequests
				.Where(jo => jo.RecordType == type && jo.ParentExternalId == parentExternalId && jo.Status == MigrationStatus.ToBeProcessed)
				.Select(jo => jo.Id)
				.ToList();
		}

		#endregion

		#region Assist Helpers

		/// <summary>
		/// Looks up the original migration request for a user.
		/// </summary>
		/// <param name="externalId">The ID of the user from the source system.</param>
		/// <param name="type">The type of request</param>
		/// <param name="checkFocusId">Only return records where the focus ID is set</param>
		/// <returns>The original migration request.</returns>
		private UserRequest LookupUserRequest(string externalId, MigrationAssistUserRecordType type, bool checkFocusId = true)
		{
			return checkFocusId
				? Repositories.Migration.UserRequests.FirstOrDefault(u => u.RecordType == type && u.ExternalId == externalId && u.FocusId != 0)
				: Repositories.Migration.UserRequests.FirstOrDefault(u => u.RecordType == type && u.ExternalId == externalId);
		}

		/// <summary>
		/// Looks up the original migration request for a user using a secondary id
		/// </summary>
		/// <param name="externalId">The ID of the user from the source system.</param>
		/// <param name="type">The type of request</param>
		/// <param name="checkFocusId">Only return records where the focus ID is set</param>
		/// <returns>The original migration request.</returns>
		private UserRequest LookupUserRequestBySecondary(string externalId, MigrationAssistUserRecordType type, bool checkFocusId = true)
		{
			return checkFocusId
				? Repositories.Migration.UserRequests.FirstOrDefault(u => u.RecordType == type && u.SecondaryExternalId == externalId && u.FocusId != 0)
				: Repositories.Migration.UserRequests.FirstOrDefault(u => u.RecordType == type && u.SecondaryExternalId == externalId);
		}

		/// <summary>
		/// Looks up the original migration requests for a user.
		/// </summary>
		/// <param name="externalIds">The IDs of the user from the source system.</param>
		/// <param name="type">The type of request</param>
		/// <param name="checkFocusId">Only return records where the focus ID is set</param>
		/// <returns>A list of original migration requests.</returns>
		private Dictionary<string, UserRequest> LookupUserRequests(List<string> externalIds, MigrationAssistUserRecordType type, bool checkFocusId = true)
		{
			var requests = checkFocusId
				? Repositories.Migration.UserRequests.Where(u => u.RecordType == type && externalIds.Contains(u.ExternalId) && u.FocusId != 0)
				: Repositories.Migration.UserRequests.Where(u => u.RecordType == type && externalIds.Contains(u.ExternalId));

			return requests.ToDictionary(u => u.ExternalId, u => u);
		}

		#endregion

		#region Shared Helpers

		/// <summary>
		/// Sets the standard properties of a service request.
		/// </summary>
		/// <param name="serviceRequest">The service request.</param>
		/// <param name="currentRequest">The import request from which the properties can be copied.</param>
		private static void PrepareServiceRequestProperties(IServiceRequest serviceRequest, ServiceRequest currentRequest)
		{
			serviceRequest.ClientTag = currentRequest.ClientTag;
			serviceRequest.SessionId = currentRequest.SessionId;
			serviceRequest.RequestId = currentRequest.RequestId;
			serviceRequest.UserContext = currentRequest.UserContext;
			serviceRequest.VersionNumber = currentRequest.VersionNumber;
		}

		/// <summary>
		/// Formats the message from a service 'save' response.
		/// </summary>
		/// <param name="saveResponse">The response from the service.</param>
		/// <returns>The formatted message.</returns>
		private static string FormatResponseMessage(ServiceResponse saveResponse)
		{
			return (saveResponse.HasRaisedException)
				? string.Format("{0}. EXCEPTION: {1}", saveResponse.Message, saveResponse.Exception.Message)
				: saveResponse.Message;
		}

		#endregion

		#endregion
	}
}
