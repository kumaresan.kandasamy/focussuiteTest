﻿#region Copyright © 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;
using System.Xml.Xsl;

using Focus.Core;
using Focus.Core.Criteria;
using Focus.Core.Criteria.CandidateApplication;
using Focus.Core.Criteria.CandidateSearch;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.EmailTemplate;
using Focus.Core.IntegrationMessages;
using Focus.Core.Messages;
using Focus.Core.Messages.CandidateService;
using Focus.Core.Messages.EmployerService;
using Focus.Core.Messages.ResumeService;
using Focus.Core.Models;
using Focus.Core.Models.Career;
using Focus.Core.Settings;
using Focus.Core.Views;
using Focus.Data.Configuration.Entities;
using Focus.Data.Core.Entities;
using Focus.Services.Core;
using Focus.Services.Core.Extensions;
using Focus.Services.DtoMappers;
using Focus.Services.Helpers;
using Focus.Services.Integration;
using Focus.Services.Mappers;
using Focus.Services.Messages;
using Focus.Services.Queries;
using Focus.Services.ServiceContracts;
using Job = Focus.Data.Core.Entities.Job;

using Framework.Core;
using Framework.Logging;

using Mindscape.LightSpeed;
using Query = Mindscape.LightSpeed.Querying.Query;

#endregion

namespace Focus.Services.ServiceImplementations
{
    public class CandidateService : ServiceBase, ICandidateService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CandidateService" /> class.
        /// </summary>
        public CandidateService()
            : this(null)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="CandidateService" /> class.
        /// </summary>
        /// <param name="runtimeContext">The runtime context.</param>
        public CandidateService(IRuntimeContext runtimeContext)
            : base(runtimeContext)
        { }

        /// <summary>
        /// Sends candidate emails.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public SendCandidateEmailsResponse SendCandidateEmails(SendCandidateEmailsRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new SendCandidateEmailsResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    var bcc = (request.UserContext.EmailAddress.IsNotNullOrEmpty()) ? request.UserContext.EmailAddress : "";
                    var unsubscribedCount = 0;
                    var emailsCount = 0;
                    emailsCount = request.Emails.Count();
                    if (emailsCount == 1)
                    {
                        foreach (var candidateEmail in request.Emails)
                        {
                            var person = Repositories.Core.Persons.Where(x => x.Id == candidateEmail.PersonId)
                                .Select(x => new { x.EmailAddress, x.Unsubscribed })
                                .FirstOrDefault();

                            var devices = Repositories.Core.PersonMobileDevices.Where(x => x.PersonId == candidateEmail.PersonId).ToList();
                            var emailAddress = person.IsNotNull() ? person.EmailAddress : null;
                            var isUnsubcribed = person.IsNotNull() && person.Unsubscribed.GetValueOrDefault(false);
                            if (request.CheckSubscribed && isUnsubcribed)
                                unsubscribedCount++;

                            if (emailAddress.IsNotNullOrEmpty() && !(request.CheckSubscribed && isUnsubcribed))
                            {
                                emailsCount++;

                                var footerUrl = string.Empty;
                                candidateEmail.To = emailAddress;

                                if (request.FooterType == EmailFooterTypes.JobSeekerUnsubscribe)
                                {
                                    var encryptionModel = Helpers.Encryption.GenerateVector(TargetTypes.JobSeekerUnsubscribe, EntityTypes.JobSeeker, candidateEmail.PersonId);
                                    var encryptedPersonId = Helpers.Encryption.Encrypt(candidateEmail.PersonId.ToString(CultureInfo.InvariantCulture), encryptionModel.Vector, true);
                                    footerUrl = string.Join("", request.FooterUrl, "?", "value=", encryptedPersonId, "&key=", encryptionModel.EncryptionId);
                                }

                                Helpers.Email.SendEmail(emailAddress, "", candidateEmail.BccRequestor ? bcc : "", candidateEmail.Subject, candidateEmail.Body, true, detectUrl: request.DetectUrl, senderAddress: candidateEmail.SenderAddress, footerType: request.FooterType, footerUrl: footerUrl.IsNotNullOrEmpty() ? footerUrl : null);

                                // Send message as push notification if person has mobile device
                                if (devices.Any())
                                {
                                    foreach (var device in devices.Where(x => x.Token != null))
                                    {
                                        Helpers.PushNotification.SendNotification(device.DeviceType, device.Token, device.Id, candidateEmail.Subject, candidateEmail.Body);
                                    }
                                }

                                if (!request.Emails[0].IsInviteToApply)
                                {
                                    LogAction(request, ActionTypes.JobSeekerEmailed, typeof(Person).Name, candidateEmail.PersonId, request.UserContext.UserId);
                                }
                            }
                        }
                    }
                    else
                    {
                        var valueTobeSendForMessageBus = new SendBlastEmailMessages()
                        {
                            Emails = request.Emails,
                            DetectUrl = request.DetectUrl,
                            FooterType = request.FooterType,
                            SendCopy = request.SendCopy,
                            FooterUrl = request.FooterUrl,
                            IsEmployeeEmail = false,
                            CheckSubscribed = request.CheckSubscribed
                        };

                        Helpers.Messaging.Publish(valueTobeSendForMessageBus, null, true);
                        emailsCount = request.Emails.Count();
                        foreach (var e in request.Emails)
                        {
                            var person = Repositories.Core.Persons.Where(x => x.Id == e.PersonId).FirstOrDefault();
                            if (person.IsNotNull() && person.Unsubscribed.IsNotNull() && person.Unsubscribed == true)
                                unsubscribedCount++;
                        }
                    }

                    if (emailsCount > 0)
                    {
                        if (request.SendCopy)
                        {
                            var ccEmail = AppSettings.UseOfficeAddressToSendMessages
                                ? Helpers.Office.GetHiringManagerOfficeEmail(
                                    Convert.ToInt64(request.UserContext.PersonId))
                                : request.UserContext.EmailAddress;
                            var emailList = string.Join("; ",
                                request.Emails.Where(r => r.To.IsNotNullOrEmpty()).Select(r => r.To));
                            var newBody = string.Format("<p>Sent to: {0}</p>{1}", emailList,
                                request.Emails[0].Body);
                            Helpers.Email.SendEmail(ccEmail, "", "", request.Emails[0].Subject, newBody, true,
                                detectUrl: request.DetectUrl, senderAddress: request.Emails[0].SenderAddress);
                        }

                        if (request.Emails[0].IsInviteToApply)
                        {
                            LogAction(request,
                                (request.UserContext.EmployerId.IsNull() || request.UserContext.EmployerId == 0 ||
                                 request.UserContext.IsShadowingUser || AppSettings.Module == FocusModules.Assist)
                                    ? ActionTypes.InviteJobSeekerToApply
                                    : ActionTypes.InviteJobSeekerToApplyThroughTalent, typeof(Job).Name,
                                request.Emails[0].JobId,
                                additionalDetails: request.Emails[0].CandidateName,
                                entityIdAdditional01: request.Emails[0].PersonId);
                        }
                        else if (request.Emails[0].JobId.HasValue && request.Emails[0].JobId != 0)
                        {
                            LogAction(request, ActionTypes.SendCandidateEmails, typeof(User).Name,
                                request.Emails[0].JobId, additionalDetails: request.Emails[0].CandidateName);
                        }
                        else
                        {
                            if (request.Emails[0].ContactRequest)
                            {
                                LogAction(request, ActionTypes.SendContactRequest, typeof(Person).Name,
                                    request.Emails[0].PersonId, additionalDetails: request.Emails[0].CandidateName);
                            }

                            LogAction(request, ActionTypes.SendCandidateEmails, typeof(User).Name,
                                request.UserContext.UserId);
                        }
                    }

                    response.UnsubscribedCount = unsubscribedCount;
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.IntegrationCandidateSendCandidateEmailFailure), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Gets the resume.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public GetResumeAsHtmlResponse GetResumeAsHtml(GetResumeAsHtmlRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new GetResumeAsHtmlResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    if ((!request.PersonId.HasValue) && (!request.ResumeId.HasValue))
                    {
                        response.SetFailure(ErrorTypes.PersonDetailsNotSupplied, FormatErrorMessage(request, ErrorTypes.PersonDetailsNotSupplied));
                        Logger.Info(request.LogData(), "No person ID provided.");
                        return response;
                    }

                    Tuple<long?, string> resumeTuple;

                    if (request.ResumeId.HasValue)
                    {
                        resumeTuple = Tuple.Create(request.ResumeId, Repositories.Core.FindById<Resume>(request.ResumeId.Value).ResumeXml);
                    }
                    else if (request.ApplicationJobId > 0 && !request.ResumeId.HasValue)
                    {
                        var applicationId = Repositories.Core.ApplicationViews.Where(a => a.CandidateId.Equals(request.PersonId) && a.JobId.Equals(request.ApplicationJobId)).Select(a => a.Id).FirstOrDefault();

                        if (applicationId != 0)
                        {
                            //get resume id from application table
                            long? resumeId = Repositories.Core.Applications.Where(x => x.Id.Equals(applicationId)).Select(a => a.ResumeId).FirstOrDefault();
                            resumeTuple = Tuple.Create(resumeId, Repositories.Core.FindById<Resume>(resumeId).ResumeXml);
                        }
                        else
                        {
                            resumeTuple = (from r in Repositories.Core.Resumes
                                           where r.PersonId == request.PersonId.Value && r.IsDefault
                                           select new Tuple<long?, string>(r.Id, r.ResumeXml)).FirstOrDefault();


                            if (resumeTuple.IsNull() || resumeTuple.Item2.IsNullOrEmpty())
                            {
                                // Final resort get latest resume
                                resumeTuple = (from r in Repositories.Core.Resumes
                                               where r.PersonId == request.PersonId.Value && r.StatusId == ResumeStatuses.Active
                                               select new Tuple<long?, string>(r.Id, r.ResumeXml)).FirstOrDefault();
                            }
                        }
                    }
                    else
                    {
                        resumeTuple = (from r in Repositories.Core.Resumes
                                       where r.PersonId == request.PersonId.Value && r.IsDefault
                                       select new Tuple<long?, string>(r.Id, r.ResumeXml)).FirstOrDefault();


                        if (resumeTuple.IsNull() || resumeTuple.Item2.IsNullOrEmpty())
                        {
                            // Final resort get latest resume
                            resumeTuple = (from r in Repositories.Core.Resumes
                                           where r.PersonId == request.PersonId.Value && r.StatusId == ResumeStatuses.Active
                                           select new Tuple<long?, string>(r.Id, r.ResumeXml)).FirstOrDefault();
                        }
                    }

                    var resume = Repositories.Core.Resumes.Where(x => x.Id == resumeTuple.Item1).FirstOrDefault();
                    var resumemodel = resume.ResumeXml.ToResumeModel(RuntimeContext, request.UserContext.Culture);
                    if (resumemodel.IsNotNull())
                    {
                        XmlDocument XmlDoc = new XmlDocument();
                        if (resumemodel.Special.HideInfo.IsNotNull())
                        {
                            var hideResumeInfo = resumemodel.Special.HideInfo;
                            XmlDoc.LoadXml(resume.ResumeXml);

                            #region Hide - All Dates, Education dates, Work history dates
                            if (hideResumeInfo.HideDateRange)
                            {
                                foreach (XmlNode dateRangeNode in XmlDoc.SelectNodes("//daterange"))
                                    dateRangeNode.ParentNode.RemoveChild(dateRangeNode);

                                foreach (XmlNode dateRangeNode in XmlDoc.SelectNodes("//completiondate"))
                                    dateRangeNode.ParentNode.RemoveChild(dateRangeNode);

                                foreach (XmlNode dateRangeNode in XmlDoc.SelectNodes("//completion_date"))
                                    dateRangeNode.ParentNode.RemoveChild(dateRangeNode);


                                foreach (XmlNode dateRangeNode in XmlDoc.SelectNodes("//vet_start_date"))
                                    dateRangeNode.ParentNode.RemoveChild(dateRangeNode);

                                foreach (XmlNode dateRangeNode in XmlDoc.SelectNodes("//vet_end_date"))
                                    dateRangeNode.ParentNode.RemoveChild(dateRangeNode);
                            }
                            else if (hideResumeInfo.HideWorkDates || hideResumeInfo.HideEducationDates)
                            {
                                if (hideResumeInfo.HideWorkDates)
                                {
                                    foreach (XmlNode dateRangeNode in XmlDoc.SelectNodes("//daterange"))
                                        dateRangeNode.ParentNode.RemoveChild(dateRangeNode);
                                }

                                if (hideResumeInfo.HideEducationDates)
                                {
                                    foreach (XmlNode dateRangeNode in XmlDoc.SelectNodes("//completiondate"))
                                        dateRangeNode.ParentNode.RemoveChild(dateRangeNode);
                                }
                            }

                            #endregion

                            #region Hide - Military service dates.

                            if (hideResumeInfo.HideMilitaryServiceDates)
                            {
                                foreach (XmlNode dateRangeNode in XmlDoc.SelectNodes("//statements/personal/veteran//vet_start_date"))
                                    dateRangeNode.ParentNode.RemoveChild(dateRangeNode);

                                foreach (XmlNode dateRangeNode in XmlDoc.SelectNodes("//statements/personal/veteran//vet_end_date"))
                                    dateRangeNode.ParentNode.RemoveChild(dateRangeNode);
                            }

                            #endregion

                            #region Hide - Contact

                            if (hideResumeInfo.HideContact)
                            {
                                if (hideResumeInfo.HideName)
                                {
                                    foreach (XmlNode contactNode in XmlDoc.SelectNodes("//resume/contact"))
                                        contactNode.ParentNode.RemoveChild(contactNode);
                                }
                                else
                                {
                                    foreach (XmlNode address in XmlDoc.SelectNodes("//resume/contact/address"))
                                        address.ParentNode.RemoveChild(address);

                                    XmlNode contact = XmlDoc.SelectSingleNode("//resume/contact");
                                    if (contact != null)
                                    {
                                        if (contact.SelectNodes("phone").Count > 0)
                                        {
                                            XmlNodeList phones = contact.SelectNodes("phone");
                                            for (int i = 0; i < phones.Count; i++)
                                                contact.RemoveChild(phones.Item(i));

                                            contact.InnerXml = contact.InnerXml.Replace("home:", "").Replace("cell:", "").Replace("work:", "").Replace("fax:", "").Replace("NonUS:", "");
                                        }
                                    }

                                    XmlNode email = XmlDoc.SelectSingleNode("//resume/contact/email");
                                    if (email != null)
                                        email.ParentNode.RemoveChild(email);
                                }
                            }

                            #endregion

                            #region Hide - Name

                            if (hideResumeInfo.HideName && !hideResumeInfo.HideContact)
                            {
                                foreach (XmlNode nameNode in XmlDoc.SelectNodes("//resume/contact/name"))
                                    nameNode.ParentNode.RemoveChild(nameNode);
                            }

                            #endregion

                            #region Hide individual contact details if the contact section is visible

                            if (!hideResumeInfo.HideContact)
                            {
                                XmlNode contact = XmlDoc.SelectSingleNode("//resume/contact");

                                #region Hide - Email address

                                if (hideResumeInfo.HideEmail)
                                {
                                    XmlNode email = XmlDoc.SelectSingleNode("//resume/contact/email");
                                    if (email != null)
                                        email.ParentNode.RemoveChild(email);
                                }

                                #endregion

                                #region Hide - Home Phone Number

                                if (hideResumeInfo.HideHomePhoneNumber)
                                {
                                    XmlNodeList homePhone = XmlDoc.SelectNodes("//resume/contact/phone[@type='home']");
                                    foreach (XmlNode hPhome in homePhone)
                                        hPhome.ParentNode.RemoveChild(hPhome);

                                    contact.InnerXml = contact.InnerXml.Replace("home:", "");
                                }

                                #endregion

                                #region Hide - Work Phone Number

                                if (hideResumeInfo.HideWorkPhoneNumber)
                                {
                                    XmlNodeList workPhone = XmlDoc.SelectNodes("//resume/contact/phone[@type='work']");
                                    foreach (XmlNode wPhome in workPhone)
                                        wPhome.ParentNode.RemoveChild(wPhome);

                                    contact.InnerXml = contact.InnerXml.Replace("work:", "");
                                }

                                #endregion

                                #region Hide - Cell Phone Number

                                if (hideResumeInfo.HideCellPhoneNumber)
                                {
                                    XmlNodeList cellPhone = XmlDoc.SelectNodes("//resume/contact/phone[@type='cell']");
                                    foreach (XmlNode cPhome in cellPhone)
                                        cPhome.ParentNode.RemoveChild(cPhome);

                                    contact.InnerXml = contact.InnerXml.Replace("cell:", "");
                                }

                                #endregion

                                #region Hide - Fax Phone Number

                                if (hideResumeInfo.HideFaxPhoneNumber)
                                {
                                    XmlNodeList faxPhone = XmlDoc.SelectNodes("//resume/contact/phone[@type='fax']");
                                    foreach (XmlNode fPhome in faxPhone)
                                        fPhome.ParentNode.RemoveChild(fPhome);

                                    contact.InnerXml = contact.InnerXml.Replace("fax:", "");
                                }

                                #endregion

                                #region Hide - NonUS Phone Number

                                if (hideResumeInfo.HideNonUSPhoneNumber)
                                {
                                    XmlNodeList nonUSPhone = XmlDoc.SelectNodes("//resume/contact/phone[@type='nonus']");
                                    foreach (XmlNode nUSPhome in nonUSPhone)
                                        nUSPhome.ParentNode.RemoveChild(nUSPhome);

                                    contact.InnerXml = contact.InnerXml.Replace("NonUS:", "");
                                }

                                #endregion
                            }

                            #endregion

                            #region Hide - Affiliations

                            if (hideResumeInfo.HideAffiliations)
                            {
                                foreach (XmlNode affiliationsNode in XmlDoc.SelectNodes("//professional/affiliations"))
                                    affiliationsNode.ParentNode.RemoveChild(affiliationsNode);
                            }

                            #endregion

                            #region Hide - Certification and Professional License

                            if (hideResumeInfo.HideCertificationsAndProfessionalLicenses)
                            {
                                foreach (XmlNode certificationsNode in XmlDoc.SelectNodes("//skills/courses/certifications/certification"))
                                    certificationsNode.ParentNode.RemoveChild(certificationsNode);
                            }

                            #endregion

                            #region Hide - Driver's license

                            if (!hideResumeInfo.UnHideDriverLicense || hideResumeInfo.HideDriverLicense)
                            {
                                foreach (XmlNode driversLicense in XmlDoc.SelectNodes("//personal/license/driver_class"))
                                    driversLicense.InnerText = "-1";
                            }

                            #endregion

                            #region Hide - Honors

                            if (hideResumeInfo.HideHonors)
                            {
                                foreach (XmlNode honorsNode in XmlDoc.SelectNodes("//special/honors"))
                                    honorsNode.ParentNode.RemoveChild(honorsNode);
                            }

                            #endregion

                            #region Hide - Interests

                            if (hideResumeInfo.HideInterests)
                            {
                                foreach (XmlNode interestsNode in XmlDoc.SelectNodes("//special/interests"))
                                    interestsNode.ParentNode.RemoveChild(interestsNode);
                            }

                            #endregion

                            #region Hide - Internships

                            if (hideResumeInfo.HideInternships)
                            {
                                foreach (XmlNode internshipsNode in XmlDoc.SelectNodes("//special/internships"))
                                    internshipsNode.ParentNode.RemoveChild(internshipsNode);
                            }

                            #endregion

                            #region Hide - Languages

                            if (hideResumeInfo.HideLanguages)
                            {
                                foreach (XmlNode languagesNode in XmlDoc.SelectNodes("//skills/languages/languages_profiency"))
                                    languagesNode.ParentNode.RemoveChild(languagesNode);

                                foreach (XmlNode languagesNode in XmlDoc.SelectNodes("//skills/languages/language"))
                                    languagesNode.ParentNode.RemoveChild(languagesNode);
                            }

                            #endregion

                            #region Hide - Military services

                            if (hideResumeInfo.HideVeteran)
                            {
                                foreach (XmlNode veteranNode in XmlDoc.SelectNodes("//personal/veteran"))
                                    veteranNode.ParentNode.RemoveChild(veteranNode);
                            }

                            #endregion

                            #region Hide - Objectives

                            if (hideResumeInfo.HideObjective || AppSettings.HideObjectives)
                            {
                                foreach (XmlNode objectiveNode in XmlDoc.SelectNodes("//summary/objective"))
                                    objectiveNode.ParentNode.RemoveChild(objectiveNode);
                            }

                            #endregion

                            #region Hide - Personal Information

                            if (hideResumeInfo.HidePersonalInformation)
                            {
                                foreach (XmlNode personallNode in XmlDoc.SelectNodes("//special/personal"))
                                    personallNode.ParentNode.RemoveChild(personallNode);
                            }

                            #endregion

                            #region Hide - Profession Development

                            if (hideResumeInfo.HideProfessionalDevelopment)
                            {
                                foreach (XmlNode professionalNode in XmlDoc.SelectNodes("//professional/description"))
                                    professionalNode.ParentNode.RemoveChild(professionalNode);
                            }

                            #endregion

                            #region Hide Publications

                            if (hideResumeInfo.HidePublications)
                            {
                                foreach (XmlNode publicationsNode in XmlDoc.SelectNodes("//professional/publications"))
                                    publicationsNode.ParentNode.RemoveChild(publicationsNode);
                            }

                            #endregion

                            #region Hide References

                            if (hideResumeInfo.HideReferences)
                            {
                                foreach (XmlNode publicationsNode in XmlDoc.SelectNodes("//references"))
                                    publicationsNode.ParentNode.RemoveChild(publicationsNode);
                            }

                            #endregion

                            #region Hide Skills

                            if (hideResumeInfo.HideSkills)
                            {
                                foreach (XmlNode skillsNode in XmlDoc.SelectNodes("//skills/skills"))
                                    skillsNode.ParentNode.RemoveChild(skillsNode);
                            }

                            #endregion

                            #region Hide Technical skills

                            if (hideResumeInfo.HideTechnicalSkills)
                            {
                                foreach (XmlNode technicalSkillsNode in XmlDoc.SelectNodes("//special/technicalskills"))
                                    technicalSkillsNode.ParentNode.RemoveChild(technicalSkillsNode);
                            }

                            #endregion

                            #region Hide Volunteer activities

                            if (hideResumeInfo.HideVolunteerActivities)
                            {
                                foreach (XmlNode volunteerNode in XmlDoc.SelectNodes("//special/volunteeractivities"))
                                    volunteerNode.ParentNode.RemoveChild(volunteerNode);
                            }

                            #endregion
                            #region Thesis/Major projects

                            if (hideResumeInfo.HideThesisMajorProjects)
                            {
                                foreach (XmlNode thesisMajorProjectNode in XmlDoc.SelectNodes("//special/thesismajorprojects"))
                                    thesisMajorProjectNode.ParentNode.RemoveChild(thesisMajorProjectNode);
                            }

                            #endregion

                            foreach (XmlNode job in XmlDoc.SelectNodes("//job"))
                            {
                                if (job.SelectSingleNode("jobid") != null && hideResumeInfo.HiddenJobs.Contains(job.SelectSingleNode("jobid").InnerText))
                                    job.ParentNode.RemoveChild(job);
                            }

                            resumeTuple = Tuple.Create((long?)resume.Id, XmlDoc.InnerXml);
                        }
                    }

                    if (resumeTuple.IsNull() || resumeTuple.Item2.IsNullOrEmpty())
                    {
                        response.SetFailure(FormatErrorMessage(request, ErrorTypes.ResumeNotFound));

                        Logger.Info(request.LogData(), request.ResumeId.HasValue ? string.Format("The resume '{0}' was not found.", request.ResumeId)
                            : string.Format("The defaut resume for person '{0}' was not found.", request.PersonId));

                        return response;
                    }


                    //TODO: this needs reviewing
                    var xDoc = new XmlDocument();
                    xDoc.LoadXml(resumeTuple.Item2);

                    string preservedformat = null;
                    var xsltName = request.ReturnFullResume ? AppSettings.FullResumeStylesheet : AppSettings.PartialResumeStylesheet;
                    if (xDoc.SelectSingleNode("//special/preserved_format").IsNotNull())
                    {
                        var preservedformatnode = xDoc.SelectSingleNode("//special/preserved_format");
                        preservedformat = preservedformatnode.InnerText.ToString();
                        if (preservedformat != null && !preservedformat.Equals("Original"))
                            xsltName = preservedformat + "Format.xsl";

                        else if (preservedformat != null && preservedformat.Equals("Original"))
                        {
                            var originalResume = Repositories.Core.ResumeDocuments.Where(x => x.ResumeId == resume.Id).FirstOrDefault();
                            if (originalResume.IsNotNull() && !originalResume.Html.ToUpper().StartsWith("BGTEC022:"))
                            {
                                response.ResumeHtml = originalResume.Html;

                                if (request.LogView)
                                    LogAction(request, ActionTypes.ViewResume, "Resume", resumeTuple.Item1, request.UserContext.UserId);

                                return response;

                            }

                        }

                        if (!request.ReturnFullResume)
                        {
                            foreach (XmlNode contactNode in xDoc.SelectNodes("//resume/contact"))
                                contactNode.ParentNode.RemoveChild(contactNode);
                        }
                    }

                    var xslTransform = GetXsltTransform(xsltName);
                    var xsltArguments = GetXsltArguments(request);

                    if (request.IncludeAge)
                    {
                        var personAge = Repositories.Core.Persons.Where(p => p.Id == request.PersonId).Select(p => p.Age).FirstOrDefault();
                        if (personAge.IsNotNull() && personAge > 0)
                        {
                            var personalNode = xDoc.SelectSingleNode("//personal");
                            if (personalNode.IsNotNull())
                            {
                                personalNode.SetStringValue("age", personAge.ToString());
                            }
                        }
                    }

                    var veteran = xDoc.SelectSingleNode("//personal/veteran");
                    if (veteran != null)
                    {
                        var vetFlagNode = veteran.SelectSingleNode("vet_flag");
                        var vetFlag = vetFlagNode != null && Convert.ToBoolean(vetFlagNode.InnerText.ToInt());

                        if (vetFlag)
                        {
                            var historyList = new List<XmlNode>();
                            var historyNodes = veteran.SelectNodes("history");
                            if (historyNodes.IsNotNull())
                                historyList.AddRange(historyNodes.Cast<XmlNode>());

                            if (!historyList.Any())
                                historyList.Add(veteran);

                            foreach (var historyNode in historyList)
                            {

                                #region Branch of service

                                var branchNode = historyNode.SelectSingleNode("branch_of_service");
                                var serviceBranch = branchNode.IsNotNull() ? branchNode.InnerText : null;
                                var serviceBranchName = serviceBranch.IsNotNull()
                                    ? Helpers.Lookup.GetLookup(LookupTypes.MilitaryBranchesOfService).FirstOrDefault(x => x.ExternalId == serviceBranch.ToString(CultureInfo.InvariantCulture))
                                    : null;

                                #endregion

                                if (serviceBranchName != null)
                                {
                                    var serviceBranchKey = serviceBranchName.Key;

                                    #region Rank

                                    var rank = historyNode.SelectSingleNode("rank");
                                    if (rank != null)
                                    {
                                        var rankId = rank.InnerText;
                                        var rankName =
                                            Helpers.Lookup.GetLookup(LookupTypes.MilitaryRanks).FirstOrDefault(x => x.ExternalId == rankId.ToString() && x.ParentKey == serviceBranchKey);

                                        if (rankName != null) rank.InnerText = rankName.Text;
                                    }

                                    #endregion
                                }

                            }
                        }
                    }

                    foreach (XmlNode dateRangeNode in xDoc.SelectNodes("//daterange/start"))
                    {
                        if (dateRangeNode.InnerText.Contains("-"))
                        {
                            if (dateRangeNode.InnerText.Length <= 7)
                                dateRangeNode.InnerText = dateRangeNode.InnerText.Insert(3, "01-");
                            dateRangeNode.InnerText = dateRangeNode.InnerText.Replace('-', '/');
                            dateRangeNode.InnerText = dateRangeNode.InnerText.Remove(3, 3);
                        }
                    }

                    foreach (XmlNode dateRangeNode in xDoc.SelectNodes("//daterange/end"))
                    {
                        if (dateRangeNode.InnerText.Contains("-"))
                        {
                            if (dateRangeNode.InnerText.Length <= 7)
                                dateRangeNode.InnerText = dateRangeNode.InnerText.Insert(3, "01-");
                            dateRangeNode.InnerText = dateRangeNode.InnerText.Replace('-', '/');
                            dateRangeNode.InnerText = dateRangeNode.InnerText.Remove(3, 3);
                        }
                    }

                    if (xDoc.SelectSingleNode("//ResDoc") != null)
                        xDoc.InnerXml = xDoc.SelectSingleNode("//ResDoc").OuterXml.Trim().Replace("\r\n", "\n").Replace("\n", "FC_CRLF").Replace("FC_CRLF*	", "FC_CRLF* ");
                    else if (xDoc.SelectSingleNode("//resume") != null)
                        xDoc.InnerXml = xDoc.SelectSingleNode("//resume").OuterXml.Trim().Replace("\r\n", "\n").Replace("\n", "FC_CRLF").Replace("FC_CRLF*	", "FC_CRLF* ");

                    response.ResumeHtml = Transform(xDoc.InnerXml, xslTransform, xsltArguments);
                    response.ResumeHtml = response.ResumeHtml.ToString().Replace("FC_CRLF", "<br/>");

                    if (request.LogView)
                        LogAction(request, ActionTypes.ViewResume, "Resume", resumeTuple.Item1, request.UserContext.UserId);
                }

                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Gets the resume.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public GetResumeResponse GetResume(GetResumeRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new GetResumeResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    if (!request.PersonId.HasValue)
                    {
                        response.SetFailure(ErrorTypes.PersonDetailsNotSupplied, FormatErrorMessage(request, ErrorTypes.PersonDetailsNotSupplied));
                        Logger.Info(request.LogData(), "No person ID provided.");
                        return response;
                    }

                    var resume = Repositories.Core.Resumes.FirstOrDefault(x => x.PersonId == request.PersonId.Value && x.IsDefault);

                    if (resume.IsNull())
                    {
                        response.SetFailure(FormatErrorMessage(request, ErrorTypes.CandidateNotFound));
                        Logger.Info(request.LogData(), string.Format("No resume for '{0}' was returned.", request.PersonId));
                        return response;
                    }

                    var xDoc = new XmlDocument();
                    xDoc.LoadXml(resume.ResumeXml);

                    string preservedformat = null;
                    var xsltName = request.ReturnFullResume ? AppSettings.FullResumeStylesheet : AppSettings.PartialResumeStylesheet;
                    if (xDoc.SelectSingleNode("//special/preserved_format").IsNotNull())
                    {
                        var preservedformatnode = xDoc.SelectSingleNode("//special/preserved_format");
                        preservedformat = preservedformatnode.InnerText.ToString();
                        if (preservedformat != null && !preservedformat.Equals("Original"))
                            xsltName = preservedformat + "Format.xsl";
                    }

                    var xslTransform = GetXsltTransform(xsltName);
                    var xsltArguments = GetXsltArguments(request);

                    var resumeView = new ResumeView
                    {

                        Resume = Transform(resume.ResumeXml, xslTransform, xsltArguments),
                        Name = String.Format("{0} {1}", resume.FirstName, resume.LastName),
                    };

                    response.ResumeDetails = resumeView;
                }

                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Gets the resume by id.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public GetResumeResponse GetResumeById(GetResumeRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new GetResumeResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    if (!request.ResumeId.HasValue)
                    {
                        response.SetFailure(ErrorTypes.PersonDetailsNotSupplied, FormatErrorMessage(request, ErrorTypes.PersonDetailsNotSupplied));
                        Logger.Info(request.LogData(), "No resume ID provided.");
                        return response;
                    }

                    var resume = Repositories.Core.Resumes.Single(x => x.Id == request.ResumeId.Value);

                    if (resume.IsNull())
                    {
                        response.SetFailure(FormatErrorMessage(request, ErrorTypes.CandidateNotFound));
                        Logger.Info(request.LogData(), string.Format("No resume for '{0}' was returned.", request.ResumeId));
                        return response;
                    }

                    var xDoc = new XmlDocument();
                    xDoc.LoadXml(resume.ResumeXml);

                    string preservedformat = null;
                    var xsltName = request.ReturnFullResume ? AppSettings.FullResumeStylesheet : AppSettings.PartialResumeStylesheet;
                    if (xDoc.SelectSingleNode("//special/preserved_format").IsNotNull())
                    {
                        var preservedformatnode = xDoc.SelectSingleNode("//special/preserved_format");
                        preservedformat = preservedformatnode.InnerText.ToString();
                        if (preservedformat != null && !preservedformat.Equals("Original"))
                            xsltName = preservedformat + "Format.xsl";
                    }

                    var xslTransform = GetXsltTransform(xsltName);
                    var xsltArguments = GetXsltArguments(request);

                    var resumeView = new ResumeView { Id = resume.PersonId, Resume = Transform(resume.ResumeXml, xslTransform, xsltArguments), Name = String.Format("{0} {1}", resume.Person.FirstName, resume.Person.LastName), IsVeteran = resume.IsVeteran.IsNotNull() && (bool)resume.IsVeteran };

                    response.ResumeDetails = resumeView;
                }

                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Gets the resume.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public GetResumesResponse GetResumes(GetResumesRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new GetResumesResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    var candidateResumes = Repositories.Core.Resumes.Where(x => x.PersonId == request.PersonId && x.StatusId.Value.Equals(1)).OrderByDescending(x => x.IsDefault).ThenByDescending(x => x.UpdatedOn).Select(x => new ResumeSummary
                    {
                        ResumeDate = x.UpdatedOn,
                        IsDefault = x.IsDefault,
                        ResumeID = x.Id,
                        ResumeName = x.ResumeName,
                    }).ToList();

                    foreach (var candidateResume in candidateResumes)
                    {
                        if (String.IsNullOrWhiteSpace(candidateResume.ResumeName))
                            candidateResume.ResumeName = candidateResume.IsDefault ? LocaliseOrDefault(request, "MainResume", "Main resume") : LocaliseOrDefault(request, "AnotherOne", "Another one");
                    }

                    response.CandidateResumes = candidateResumes;
                }

                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Toggles the candidate flag.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public ToggleCandidateFlagResponse ToggleCandidateFlag(ToggleCandidateFlagRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new ToggleCandidateFlagResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    if (!request.PersonId.HasValue)
                    {
                        response.SetFailure(ErrorTypes.PersonDetailsNotSupplied, FormatErrorMessage(request, ErrorTypes.PersonDetailsNotSupplied));
                        Logger.Info(request.LogData(), "No person ID provided.");
                        return response;
                    }

                    var person = Repositories.Core.FindById<Person>(request.PersonId.Value);

                    // If there isn't an candidate with the relevant candidate id then bomb out!
                    if (person.IsNull())
                    {
                        response.SetFailure(FormatErrorMessage(request, ErrorTypes.PersonNotFound));
                        Logger.Info(request.LogData(), string.Format("The person '{0}' was not found.", (request.PersonId ?? 0)));

                        return response;
                    }

                    var flaggingPersonId = request.FlaggingPersonId ?? request.UserContext.PersonId;

                    // Get the employee's flagged resume list, if one doesn't exist create one
                    var flaggedApplicantsList = Repositories.Core.PersonLists.FirstOrDefault(x => x.PersonId == flaggingPersonId && x.ListType == ListTypes.FlaggedJobSeeker);

                    if (flaggedApplicantsList.IsNull())
                    {
                        // Get the person
                        var flaggingPerson = Repositories.Core.FindById<Person>(flaggingPersonId);

                        // Create the person list
                        flaggedApplicantsList = new PersonList { Name = "Flagged applicants", Person = flaggingPerson, ListType = ListTypes.FlaggedJobSeeker };
                        Repositories.Core.Add(flaggedApplicantsList);
                    }

                    // See if the candidate already exists on the list
                    var flaggedPerson = flaggedApplicantsList.People.SingleOrDefault(x => x.Id == person.Id);

                    if (flaggedPerson.IsNull())
                        flaggedApplicantsList.People.Add(person);
                    else
                        flaggedApplicantsList.People.Remove(flaggedPerson);

                    LogAction(request, ActionTypes.ToggleCandidateFlag, typeof(Person).Name, request.PersonId);

                    Repositories.Core.SaveChanges();

                    response.FlaggedApplicantsListId = flaggedApplicantsList.Id;
                }

                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Gets the job seeker referral view.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public JobSeekerReferralViewResponse GetJobSeekerReferralView(JobSeekerReferralViewRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new JobSeekerReferralViewResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    var query = new JobSeekerReferralQuery(Repositories.Core, request.Criteria, AppSettings);

                    var states = Helpers.Lookup.GetLookup(LookupTypes.States);
                    var stateLookup = states.ToDictionary(state => state.Id, state => state.Text);

                    switch (request.Criteria.FetchOption)
                    {
                        case CriteriaBase.FetchOptions.PagedList:
                            //FVN-5970

                            var orderedQuery = query.Query();

                            var underAgeSeekers = Repositories.Core.Persons.Where(p => p.Age <= AppSettings.UnderAgeJobSeekerRestrictionThreshold).Select(x => x.Id).ToList();

                            response.ReferralViewsPaged = orderedQuery.GetPagedList(x => x.AsDto(), request.Criteria.PageIndex, request.Criteria.PageSize);
                            response.ReferralViewsPaged.RemoveAll(r => underAgeSeekers.Contains(r.CandidateId));
                            //response.ReferralViewsPaged.TotalCount = response.ReferralViewsPaged.Count;


                            var jobIds = response.ReferralViewsPaged.Select(v => v.JobId).ToList();
                            var jobLocationLookup = Repositories.Core.JobLocations.Where(jl => jobIds.Contains(jl.JobId)).ToLookup(jl => jl.JobId, jl => jl);

                            response.ReferralViewsPaged.ForEach(r =>
                            {
                                r.State = stateLookup.ContainsKey(r.StateId) ? stateLookup[r.StateId] : "UNKNOWN";
                                r.TimeInQueue = r.ReferralDate.GetBusinessDays(DateTime.Now);

                                // Posting Location(s)
                                r.PostingLocations = jobLocationLookup.Contains(r.JobId)
                                                                             ? string.Join("<br />", jobLocationLookup[r.JobId].Select(joblocation => string.Concat(joblocation.City, ", ", joblocation.State)))
                                                                             : "UNKNOWN";

                                // Jobseeker Address
                                var jobseekerState = stateLookup.ContainsKey(r.JobseekerStateId) ? stateLookup[r.JobseekerStateId] : "UNKNOWN";
                                r.JobSeekerAddress = string.Concat(r.JobseekerTown, ", ", jobseekerState);
                            });
                            break;

                        case CriteriaBase.FetchOptions.Single:
                            var referral = Repositories.Core.FindById<JobSeekerReferralAllStatusView>(request.Criteria.Id);
                            if (referral.IsNotNull())
                            {
                                response.Referral = referral.AsDto();
                                response.Referral.State = stateLookup.ContainsKey(response.Referral.StateId) ? stateLookup[response.Referral.StateId] : "Unknown";
                            }
                            break;
                    }
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Searches the job seekers.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public JobSeekerSearchResponse SearchJobSeekers(JobSeekerSearchRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new JobSeekerSearchResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    var validUserTypes = new List<UserTypes?>
					{
						UserTypes.Career,
						UserTypes.Explorer,
						UserTypes.Career | UserTypes.Explorer
					};

                    var query = from p in Repositories.Core.Persons
                                join u in Repositories.Core.Users on p.Id equals u.PersonId
                                where (validUserTypes.Contains(u.UserType))
                                select new JobSeekerView
                                {
                                    PersonId = p.Id,
                                    EmailAddress = p.EmailAddress,
                                    FirstName = p.FirstName,
                                    LastName = p.LastName,
                                    Ssn = p.SocialSecurityNumber,
                                    UserName = u.UserName,
                                    PhoneNumber = ""
                                };
                    if (request.Criteria.PhoneNumber.IsNotNullOrEmpty())
                    {
                        var phoneNumbers = (from ph in Repositories.Core.PhoneNumbers
                                            where ph.Number.Contains(request.Criteria.PhoneNumber)
                                            select (long?)ph.PersonId).ToList();

                        query = query.Where(x => phoneNumbers.Contains(x.PersonId));
                    }

                    if (request.Criteria.Firstname.IsNotNullOrEmpty())
                        query = query.Where(x => x.FirstName.StartsWith(request.Criteria.Firstname, StringComparison.OrdinalIgnoreCase));

                    if (request.Criteria.Lastname.IsNotNullOrEmpty())
                        query = query.Where(x => x.LastName.Contains(request.Criteria.Lastname));

                    if (request.Criteria.Ssn.IsNotNullOrEmpty())
                        query = query.Where(x => x.Ssn == request.Criteria.Ssn);

                    if (request.Criteria.SsnList.IsNotNullOrEmpty())
                        query = query.Where(x => request.Criteria.SsnList.Contains(x.Ssn));

                    if (request.Criteria.EmailAddress.IsNotNullOrEmpty())
                        query = query.Where(x => x.EmailAddress.Contains(request.Criteria.EmailAddress));

                    var jobSeekers = new PagedList<JobSeekerView>(query.OrderBy(x => x.LastName), query.Count(), request.Criteria.PageIndex, request.Criteria.PageSize);

                    var jobSeekerIds = jobSeekers.Select(seeker => seeker.PersonId).ToList();
                    var seekerNumbers = Repositories.Core.PhoneNumbers
                        .Where(seeker => seeker.IsPrimary && jobSeekerIds.Contains(seeker.PersonId))
                        .ToLookup(seeker => seeker.PersonId, seeker => seeker.Number);
                    foreach (var seeker in jobSeekers)
                    {
                        seeker.PhoneNumber = seekerNumbers.Contains(seeker.PersonId) ? seekerNumbers[seeker.PersonId].First() : string.Empty;
                    }
                    response.SearchResults = jobSeekers;
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Approves the referral.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public CandidateApplicationResponse ApproveReferral(CandidateApplicationRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new CandidateApplicationResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    var application = Repositories.Core.FindById<Application>(request.Id);

                    if (application.IsNull())
                    {
                        response.SetFailure(FormatErrorMessage(request, ErrorTypes.CandidateApplicationNotFound));
                        Logger.Info(request.LogData(), string.Format("Cannot approve referral as application {0} cannot be found.", request.Id));
                        return response;
                    }

                    if (request.LockVersion.HasValue && application.LockVersion != request.LockVersion.Value)
                    {
                        response.SetFailure(FormatErrorMessage(request, ErrorTypes.LockVersionOutOfDate));
                        return response;
                    }

                    var job = application.Posting.Job;
                    if (job.IsNotNull() && job.JobStatus != JobStatuses.Active)
                    {
                        response.SetFailure(FormatErrorMessage(request, ErrorTypes.JobNotActive));
                        return response;
                    }

                    if (application.ApprovalStatus != ApprovalStatuses.Approved)
                    {
                        // Save application in client system
                        application.PreviousApprovalStatus = application.ApprovalStatus;
                        application.ApprovalStatus = ApprovalStatuses.Approved;
                        application.ApplicationStatus = ApplicationStatusTypes.NewApplicant;
                        application.StatusLastChangedOn = DateTime.Now;
                        application.StatusLastChangedBy = request.UserContext.UserId;
                        Repositories.Core.SaveChanges(true);

                        // Save application in client system
                        if (AppSettings.IntegrationClient != IntegrationClient.Standalone)
                        {
                            var integrationRequest = new ReferJobSeekerRequest
                            {
                                ApplicationId = application.Id,
                            };
                            if (RuntimeContext.IsNotNull() && RuntimeContext.CurrentRequest.IsNotNull() &&
                                    RuntimeContext.CurrentRequest.UserContext.IsNotNull())
                            {
                                integrationRequest.ExternalAdminId = RuntimeContext.CurrentRequest.UserContext.ExternalUserId;
                                integrationRequest.ExternalOfficeId = RuntimeContext.CurrentRequest.UserContext.ExternalOfficeId;
                                integrationRequest.ExternalPassword = RuntimeContext.CurrentRequest.UserContext.ExternalPassword;
                                integrationRequest.ExternalUsername = RuntimeContext.CurrentRequest.UserContext.ExternalUserName;
                            }

                            Helpers.Messaging.Publish(new IntegrationRequestMessage
                            {
                                ActionerId = request.UserContext.UserId,
                                ReferJobSeekerRequest = integrationRequest,
                                IntegrationPoint = IntegrationPoint.ReferJobSeeker
                            });
                        }

                        LogStatusChange(request, typeof(Application).Name, application.Id, (long)ApprovalStatuses.Approved, (long)application.ApplicationStatus);
                        LogAction(request, request.AutoApproval ? ActionTypes.AutoApprovedReferralBypass : ActionTypes.ApproveCandidateReferral, typeof(Application).Name, request.Id, null, null, string.Format("{0} {1}", application.Resume.FirstName, application.Resume.LastName));
                    }
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Denies the referral.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public CandidateApplicationResponse DenyReferral(CandidateApplicationRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new CandidateApplicationResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    var errorType = ValidateReferralStatusReasons(request.ReferralStatusReasons);

                    if (errorType != ErrorTypes.Ok)
                    {
                        response.SetFailure(FormatErrorMessage(request, errorType));
                        Logger.Error(request.LogData(), response.Message);
                        return response;
                    }

                    if (AppSettings.Theme == FocusThemes.Workforce && request.ReferralStatusReasons.IsNotNullOrEmpty() && request.ReferralStatusReasons.Count > 0)
                    {
                        request.ReferralStatusReasons.ForEach(x =>
                        {
                            var reason = new ApplicationReferralStatusReason { ReasonId = x.Key, OtherReasonText = x.Value, ApplicationId = request.Id, EntityType = EntityTypes.Application, ApprovalStatus = ApprovalStatuses.Rejected };
                            Repositories.Core.Add(reason);
                        });
                    }

                    RuntimeContext.Helpers.Candidate.DenyReferral(request.Id, request.UserContext.UserId, request.LockVersion);
                }
                catch (ArgumentException)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.CandidateApplicationNotFound));
                    Logger.Info(request.LogData(), string.Format("Cannot deny referral as application {0} cannot be found.", request.Id));
                    return response;
                }
                catch (OptimisticConcurrencyException)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.LockVersionOutOfDate));
                    return response;
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Puts the referral on hold
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public CandidateApplicationResponse HoldReferral(CandidateApplicationRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new CandidateApplicationResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    var reason = request.OnHoldReason ?? ApplicationOnHoldReasons.PutOnHoldByStaff;
                    RuntimeContext.Helpers.Candidate.HoldReferral(request.Id, reason, false, request.UserContext.UserId, request.LockVersion);
                }
                catch (ArgumentException)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.CandidateApplicationNotFound));
                    Logger.Info(request.LogData(), string.Format("Cannot put referral on hold as application {0} cannot be found.", request.Id));
                    return response;
                }
                catch (OptimisticConcurrencyException)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.LockVersionOutOfDate));
                    return response;
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Assigns the activity.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public AssignActivityOrActionResponse AssignActivity(AssignActivityOrActionRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new AssignActivityOrActionResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                return AssignActivity(request, response);
            }
        }

        /// <summary>
        /// Assigns the activity to multiple users.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public AssignActivityOrActionResponse AssignActivityToMultipleUsers(AssignActivityOrActionRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new AssignActivityOrActionResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                foreach (var jobSeeker in request.Jobseekers)
                {
                    request.PersonId = jobSeeker.PersonId;

                    request.ClientId = jobSeeker.ClientId ?? jobSeeker.PersonId.ToString();
                    request.UserName = jobSeeker.UserName;
                    request.FirstName = jobSeeker.FirstName;
                    request.LastName = jobSeeker.LastName;
                    request.PhoneNumber = jobSeeker.PhoneNumber;
                    request.EmailAddress = jobSeeker.EmailAddress;

                    response = AssignActivity(request, response);

                    if (response.Acknowledgement == AcknowledgementType.Failure)
                        return response;
                }

                return response;
            }
        }

        /// <summary>
        /// Gets the job seeker lists for a user.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public GetJobSeekerListsResponse GetJobSeekerLists(GetJobSeekerListsRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new GetJobSeekerListsResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    response.Lists = new List<SelectView>();

                    foreach (var personList in Repositories.Core.Query<PersonList>().Where(x => x.PersonId == request.UserContext.PersonId &&
                                                                                                x.ListType == request.ListType).OrderBy(x => x.Name).ToList())
                        response.Lists.Add(new SelectView { Id = personList.Id, Text = personList.Name });
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Adds the job seeker(s) to a list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public AddJobSeekersToListResponse AddJobSeekersToList(AddJobSeekersToListRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new AddJobSeekersToListResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    if (request.ListId.IsNull() && request.NewListName.IsNullOrEmpty())
                    {
                        response.SetFailure(FormatErrorMessage(request, ErrorTypes.JobSeekerListNotSpecified));
                        Logger.Error(request.LogData(), response.Message);
                        return response;
                    }

                    if (request.JobSeekers.Any(x => x.PersonId.IsNull()))
                    {
                        response.SetFailure(FormatErrorMessage(request, ErrorTypes.JobSeekerListCandidateNotSpecified));
                        Logger.Error(request.LogData(), response.Message);
                        return response;
                    }

                    // Get the person list by list id or new name
                    PersonList personList;

                    if (request.ListId.IsNotNull())
                        personList = Repositories.Core.FindById<PersonList>(request.ListId);
                    else
                        personList = Repositories.Core.Query<PersonList>().SingleOrDefault(x => x.PersonId == request.UserContext.PersonId &&
                                                                                                x.ListType == request.ListType &&
                                                                                                x.Name == request.NewListName);

                    if (personList == null)
                    {
                        // Get the person
                        var person = Repositories.Core.FindById<Person>(request.UserContext.PersonId);

                        // Create the person list
                        personList = new PersonList { Name = request.NewListName, Person = person, ListType = ListTypes.AssistJobSeeker };
                        Repositories.Core.Add(personList);
                    }

                    var listPeopleIds = new List<long>();

                    foreach (var jobSeeker in request.JobSeekers)
                    {
                        // See if the candidate already exists on the list
                        var listPerson = personList.People.SingleOrDefault(x => x.Id == jobSeeker.PersonId);

                        if (listPerson == null)
                        {
                            // See if the candidate exists anywhere else on any other list
                            listPerson = Repositories.Core.Query<Person>().SingleOrDefault(x => x.Id == jobSeeker.PersonId);

                            if (listPerson.IsNotNull())
                            {
                                // Add the candidate to the person list
                                personList.People.Add(listPerson);
                                listPeopleIds.Add(listPerson.Id);
                            }
                        }
                    }

                    Repositories.Core.SaveChanges();

                    // Log it.
                    LogActions(request, ActionTypes.AddJobSeekersToList, typeof(PersonList).Name, personList.Id, listPeopleIds.ToArray());
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Gets the job seekers for list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public GetJobSeekersForListResponse GetJobSeekersForList(GetJobSeekersForListRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new GetJobSeekersForListResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    if (request.Criteria.IsNull() || request.Criteria.ListId.IsNull())
                    {
                        response.SetFailure(FormatErrorMessage(request, ErrorTypes.JobSeekerListNotSpecified));
                        Logger.Error(request.LogData(), response.Message);
                        return response;
                    }

                    var jobSeekers = (from plc in Repositories.Core.Query<PersonListPerson>()
                                      join p in Repositories.Core.Query<Person>() on plc.PersonId equals p.Id
                                      join u in Repositories.Core.Users on p.Id equals u.PersonId
                                      where plc.PersonListId == request.Criteria.ListId
                                      orderby p.LastName, p.FirstName
                                      select
                                          new JobSeekerView
                                          {
                                              PersonId = p.Id,
                                              FirstName = p.FirstName,
                                              LastName = p.LastName,
                                              EmailAddress = p.EmailAddress,
                                              Ssn = p.SocialSecurityNumber,
                                              UserName = u.UserName,
                                              PhoneNumber = ""
                                          });

                    IEnumerable<JobSeekerView> jobSeekersList;

                    if (request.Criteria.FetchOption == CriteriaBase.FetchOptions.PagedList)
                    {
                        response.PagedJobSeekers = new PagedList<JobSeekerView>(jobSeekers, request.Criteria.PageIndex, request.Criteria.PageSize);
                        jobSeekersList = response.PagedJobSeekers;
                    }
                    else
                    {
                        response.JobSeekers = new List<JobSeekerView>(jobSeekers);
                        jobSeekersList = response.JobSeekers;
                    }

                    var phones = (from plp in Repositories.Core.PersonListPeople
                                  join pn in Repositories.Core.PhoneNumbers
                                      on plp.PersonId equals pn.PersonId
                                  where plp.PersonListId == request.Criteria.ListId
                                        && pn.IsPrimary
                                  select pn).ToDictionary(pn => pn.PersonId, pn => pn.Number);

                    foreach (var seeker in jobSeekersList.Where(seeker => phones.ContainsKey(seeker.PersonId)))
                    {
                        seeker.PhoneNumber = phones[seeker.PersonId];
                    }

                    var offices = (from plc in Repositories.Core.PersonListPeople
                                   join pom in Repositories.Core.PersonOfficeMappers
                                       on plc.PersonId equals pom.PersonId
                                   join office in Repositories.Core.Offices
                                       on pom.OfficeId equals office.Id
                                   where plc.PersonListId == request.Criteria.ListId
                                       && (pom.OfficeUnassigned == null || pom.OfficeUnassigned == false)
                                   select new { plc.PersonId, office.OfficeName }).ToLookup(p => p.PersonId, p => p.OfficeName);

                    foreach (var seeker in jobSeekersList.Where(seeker => offices.Contains(seeker.PersonId)))
                    {
                        seeker.Offices = string.Join(", ", offices[seeker.PersonId]);
                    }

                    var defaultOffices = (from jsl in jobSeekersList
                                          join pom in Repositories.Core.PersonOfficeMappers
                                              on jsl.PersonId equals pom.PersonId
                                          join office in Repositories.Core.Offices
                                              on pom.OfficeId equals office.Id
                                          where pom.OfficeUnassigned == true && jsl.Offices == null
                                          select new { jsl.PersonId, office.OfficeName }).ToList();

                    if (defaultOffices.Count > 1)
                    {
                        foreach (var seeker in jobSeekersList.Where(seeker => seeker.Offices == null))
                        {
                            var seekerId = seeker.PersonId;
                            var defaultOffice = defaultOffices.Where(o => o.PersonId == seekerId).Select(o => o.OfficeName);
                            seeker.Offices = string.Join(", ", defaultOffice);
                        }
                    }
                }

                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Removes the job seekers from list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public RemoveJobSeekersFromListResponse RemoveJobSeekersFromList(RemoveJobSeekersFromListRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new RemoveJobSeekersFromListResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    var personList = Repositories.Core.FindById<PersonList>(request.ListId);

                    if (personList == null)
                    {
                        response.SetFailure(FormatErrorMessage(request, ErrorTypes.JobSeekerListNotFound));
                        Logger.Error(request.LogData(), response.Message);
                        return response;
                    }

                    var peopleRemoved = new List<Person>();

                    foreach (var jobSeeker in request.JobSeekers)
                    {
                        var listPerson = personList.People.SingleOrDefault(x => x.Id == jobSeeker.PersonId);

                        if (listPerson != null)
                        {
                            personList.People.Remove(listPerson);
                            peopleRemoved.Add(listPerson);
                        }
                    }

                    Repositories.Core.SaveChanges(true);

                    // Log removals
                    peopleRemoved.ForEach(cr => LogAction(request, ActionTypes.RemovedJobSeekerFromList, cr));

                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Deletes the job seeker list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public DeleteJobSeekerListResponse DeleteJobSeekerList(DeleteJobSeekerListRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new DeleteJobSeekerListResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    var personList = Repositories.Core.FindById<PersonList>(request.ListId);

                    if (personList == null)
                    {
                        response.SetFailure(FormatErrorMessage(request, ErrorTypes.JobSeekerListNotFound));
                        Logger.Error(request.LogData(), response.Message);
                        return response;
                    }

                    Repositories.Core.Remove(personList);

                    Repositories.Core.SaveChanges(true);

                    LogAction(request, ActionTypes.DeletedJobSeekerList, personList);
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Assigns the job seeker to admin.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public AssignJobSeekerToAdminResponse AssignJobSeekerToAdmin(AssignJobSeekerToAdminRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new AssignJobSeekerToAdminResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    var user = Repositories.Core.Users.FirstOrDefault(x => x.Id == request.AdminId);

                    if (user.IsNull())
                    {
                        response.SetFailure(FormatErrorMessage(request, ErrorTypes.UserNotFound));
                        Logger.Error(request.LogData(), "User not found");
                        return response;
                    }

                    if (user.ExternalId.IsNullOrEmpty())
                    {
                        response.SetFailure(FormatErrorMessage(request, ErrorTypes.UserMissingExternalId));
                        Logger.Error(request.LogData(), "User does not have an ExternalId");
                        return response;
                    }

                    request.ExternalAdminId = user.ExternalId;

                    var person = Repositories.Core.Persons.SingleOrDefault(x => x.Id == request.PersonId);

                    if (person == null)
                    {
                        response.SetFailure(FormatErrorMessage(request, ErrorTypes.PersonNotFound));
                        Logger.Info(request.LogData(), string.Format("The person '{0}' was not found.", request.PersonId));

                        return response;
                    }

                    if (AppSettings.IntegrationClient != IntegrationClient.Standalone)
                    {
                        var adminExternalId = Repositories.Core.FindById<User>(request.AdminId).ExternalId;
                        var jobSeekerExternalId = person.User.ExternalId;

                        Helpers.Messaging.Publish(new IntegrationRequestMessage
                        {
                            ActionerId = request.UserContext.UserId,
                            IntegrationPoint = IntegrationPoint.AssignAdminToJobSeeker,
                            AssignAdminToJobSeekerRequest = request.ToAssignAdminToJobSeekerRequest(jobSeekerExternalId, adminExternalId)
                        });
                    }

                    /*
                    var integrationResponse = Repositories.Client.AssignJobSeekerToAdmin(request.ToIntegrationAssignJobSeekerToAdminRequest(request.UserName, adminUserName));

                    if (integrationResponse.Acknowledgement == AcknowledgementType.Failure)
                    {
                        response.SetFailure(FormatErrorMessage(request, ErrorTypes.IntegrationAssignJobSeekerToAdmin), integrationResponse.Exception);
                        Logger.Error(request.LogData(), integrationResponse.Message, integrationResponse.Exception);
                        return response;
                    }
                    */

                    LogAction(request, ActionTypes.AssignJobSeekerToAdmin, typeof(Person).Name, person.Id, request.AdminId, additionalDetails: request.PersonId.ToString());
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Saves the candidate application.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public ReferCandidateResponse ReferCandidate(ReferCandidateRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new ReferCandidateResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    var job = Repositories.Core.Query<Job>().SingleOrDefault(x => x.Id == request.JobId);
                    if (job == null)
                    {
                        response.SetFailure(FormatErrorMessage(request, ErrorTypes.InvalidJob, request.JobId));
                        Logger.Error(request.LogData(), response.Message, response.Exception);
                        return response;
                    }

                    if (job.Posting.IsNull())
                    {
                        response.SetFailure(FormatErrorMessage(request, ErrorTypes.InvalidJob, request.JobId));
                        Logger.Error(request.LogData(), string.Format("Job {0} has not been posted.", request.JobId));
                        return response;
                    }

                    var person = Repositories.Core.FindById<Person>(request.PersonId);

                    // If there isn't an person with the relevant id then bomb out!
                    if (person.IsNull())
                    {
                        response.SetFailure(FormatErrorMessage(request, ErrorTypes.PersonNotFound));
                        Logger.Info(request.LogData(), string.Format("The person '{0}' was not found.", request.PersonId));

                        return response;
                    }

                    // Get the Application
                    var application =
                        Repositories.Core.Query<Application>()
                            .SingleOrDefault(x => x.Resume.PersonId == person.Id && x.PostingId == job.Posting.Id);

                    var currentStatus = ApplicationStatusTypes.NotApplicable;
                    var applicationQueued = false;
                    var newApplication = false;

                    if (application == null)
                    {
                        newApplication = true;
                        var resume = person.Resumes.FirstOrDefault(x => x.IsDefault && x.StatusId == ResumeStatuses.Active && x.ResumeCompletionStatusId == ResumeCompletionStatuses.Completed);
                        if (resume.IsNotNull())
                            applicationQueued = (!resume.IsVeteran.GetValueOrDefault(false) && job.VeteranPriorityEndDate > DateTime.Now);

                        if (resume.IsNull())
                        {
                            response.SetFailure(ErrorTypes.ResumeNotFound, FormatErrorMessage(request, ErrorTypes.ResumeNotFound));
                            Logger.Info(request.LogData(),
                                string.Format("The default resume for person '{0}' was not found.", request.PersonId));

                            return response;
                        }

                        application = new Application
                        {
                            Resume = resume,
                            Posting = job.Posting,
                            ApprovalStatus = ApprovalStatuses.Approved,
                            ApplicationStatus = request.InitialApplicationStatus ?? ApplicationStatusTypes.NewApplicant,
                            ApplicationScore = request.ApplicationScore,
                            StatusLastChangedOn = request.DateApplied ?? DateTime.Now
                        };

                        Repositories.Core.Add(application);

                    }
                    else
                        currentStatus = application.ApplicationStatus;

                    application.PreviousApprovalStatus = application.ApprovalStatus;
                    application.ApplicationScore = request.ApplicationScore;

                    Repositories.Core.SaveChanges();

                    if (request.DateReferred.IsNotNull())
                    {
                        var updateQuery = new Query(typeof(Application), Entity.Attribute("Id") == application.Id);
                        Repositories.Core.Update(updateQuery, new { CreatedOn = request.DateReferred });
                        Repositories.Core.SaveChanges();
                    }

                    LogStatusChange(request, typeof(Application).Name, application.Id, (long)currentStatus,
                        (long)application.ApplicationStatus, request.UserId);

                    LogAction(request, ActionTypes.CreateCandidateApplication, typeof(Application).Name, application.Id, job.Id,
                        person.Id, String.Format("{0} {1}", person.FirstName, person.LastName));

                    if (request.WaivedRequirements.IsNotNullOrEmpty())
                        LogAction(request, ActionTypes.WaivedRequirements, typeof(Application).Name, application.Id, job.Id, person.Id, string.Join(", ", request.WaivedRequirements));

                    // See FVN-3139
                    // if (request.StaffReferral)
                    //	 LogAction(request, ActionTypes.StaffReferralDeprecated, typeof(Person).Name, person.Id, job.Id, job.EmployerId, string.Format("{0}, {1}", job.JobTitle, job.Employer.Name));

                    response.ApplicationQueued = applicationQueued;
                    response.ApplicationId = application.Id;

                    // Save application in client system
                    if (AppSettings.IntegrationClient != IntegrationClient.Standalone &&
                        newApplication &&
                        application.Resume.Person.User.ExternalId.IsNotNullOrEmpty() &&
                        application.Posting.ExternalId.IsNotNullOrEmpty())
                    {
                        var integrationRequest = new ReferJobSeekerRequest
                        {
                            ApplicationId = application.Id
                        };
                        if (RuntimeContext.IsNotNull() && RuntimeContext.CurrentRequest.IsNotNull() &&
                            RuntimeContext.CurrentRequest.UserContext.IsNotNull())
                        {
                            integrationRequest.ExternalAdminId = RuntimeContext.CurrentRequest.UserContext.ExternalUserId;

                            var currentOffice = (from pco in Repositories.Core.PersonsCurrentOffices
                                                 join o in Repositories.Core.Offices
                                                     on pco.OfficeId equals o.Id
                                                 where pco.PersonId == RuntimeContext.CurrentRequest.UserContext.PersonId
                                                 orderby pco.StartTime descending
                                                 select o.ExternalId).FirstOrDefault();

                            integrationRequest.ExternalOfficeId = currentOffice.IsNotNullOrEmpty() ? currentOffice : RuntimeContext.CurrentRequest.UserContext.ExternalOfficeId;

                            integrationRequest.ExternalPassword = RuntimeContext.CurrentRequest.UserContext.ExternalPassword;
                            integrationRequest.ExternalUsername = RuntimeContext.CurrentRequest.UserContext.ExternalUserName;
                        }

                        Helpers.Messaging.Publish(new IntegrationRequestMessage
                        {
                            ActionerId = request.UserContext.UserId,
                            ReferJobSeekerRequest = integrationRequest,
                            IntegrationPoint = IntegrationPoint.ReferJobSeeker
                        });
                    }

                    if (response.ApplicationQueued && job.VeteranPriorityEndDate.IsNotNull() && job.VeteranPriorityEndDate > DateTime.Now)
                        Helpers.Candidate.SendQueuedStaffReferralEmail(request.PersonId, job.Posting.AsDto(), request.UserContext, job.VeteranPriorityEndDate.GetValueOrDefault());
                    else
                        Helpers.Candidate.SendStaffReferralEmail(request.PersonId, job.Posting.AsDto(), request.UserContext);
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Gets the candidate system defaults.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public GetCandidateSystemDefaultsResponse GetCandidateSystemDefaults(GetCandidateSystemDefaultsRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new GetCandidateSystemDefaultsResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    // TODO: Martha (Low) - Direct method replacement call for -> _focusCareerRepositories.Core.GetSystemDefaults();

                    #region Activity code setup

                    var activityCodes = new List<ActivityCodeView>();

                    var activityCode0 = new ActivityCodeView { Name = "Phone call", CodeNumber = 10001 };
                    var activityCode1 = new ActivityCodeView { Name = "Sent email", CodeNumber = 10002 };

                    activityCodes.Add(activityCode0);
                    activityCodes.Add(activityCode1);

                    #endregion

                    #region Messages setups

                    var defaultMessages = new List<ApplicationMessageView>();
                    var messageTags1 = new List<MessageVariable>();

                    var messageTag0 = new MessageVariable { Name = "Variable 1", Tag = "#TAG1#" };
                    var messageTag1 = new MessageVariable { Name = "Variable 2", Tag = "#TAG2#" };

                    messageTags1.Add(messageTag0);
                    messageTags1.Add(messageTag1);

                    var defaultMessage0 = new ApplicationMessageView { Name = "Default message 1", Text = "Default message 1 text", AvailableVariables = messageTags1 };
                    var defaultMessage1 = new ApplicationMessageView { Name = "Default message 2", Text = "Default message 2 text" };

                    defaultMessages.Add(defaultMessage0);
                    defaultMessages.Add(defaultMessage1);

                    #endregion

                    response.SystemDefaults = new CandidateSystemDefaultsView
                    {
                        AlertDefaults = new AlertDefaultsView
                        {
                            JobAlertsOption = JobAlertsOptions.OnButCanUnsubscribe,
                            AlertFrequency = Frequencies.Weekly,
                            SendOtherJobsEmail = true
                        },
                        ApprovalDefaults = new ApprovalDefaultsView
                        {
                            NewResumeApprovalsOption = NewResumeApprovalsOptions.ApprovalSystemAvailable,
                            ResumeReferralApprovalsOption = ResumeReferralApprovalsOptions.SelfReferralRequires3StarPlusMatch
                        },
                        ActivityCodes = activityCodes,
                        ColourDefaults = new ColourDefaultsView
                        {
                            LightColour = "bfbf00",
                            DarkColour = "ff0000"
                        },
                        MessageDefaults = defaultMessages
                    };
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.IntegrationCandidateGetSystemDefaultsFailure), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Updates the candidate system defaults.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public UpdateCandidateSystemDefaultsResponse UpdateCandidateSystemDefaults(UpdateCandidateSystemDefaultsRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new UpdateCandidateSystemDefaultsResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    // TODO: Martha (Low) - Direct method replacement call for -> _focusCareerRepositories.Core.UpdateSystemDefaults(request.SystemDefaults);

                    LogAction(request, ActionTypes.UpdateCandidateSystemDefaults, null);
                }

                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.IntegrationCandidateUpdateSystemDefaultsFailure), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Creates the job seeker homepage alert.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public CreateJobSeekerHomepageAlertResponse CreateJobSeekerHomepageAlert(CreateJobSeekerHomepageAlertRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new CreateJobSeekerHomepageAlertResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    var person = Repositories.Core.FindById<Person>(request.UserContext.PersonId);

                    foreach (var homepageAlert in request.HomepageAlerts)
                    {
                        if (homepageAlert.AlertForAllCandidates && homepageAlert.CandidateId == 0)
                        {
                            CreateAlertMessage(Constants.AlertMessageKeys.JobSeekerMessage, homepageAlert.IsSystemAlert, null, homepageAlert.ExpiryDate,
                                MessageAudiences.AllJobSeekers, MessageTypes.General, null,
                                homepageAlert.IsSystemAlert ? LocaliseOrDefault(request, "System.Text", "System") : person.FirstName,
                                homepageAlert.IsSystemAlert ? LocaliseOrDefault(request, "Administrator.Text", "Administrator") : person.LastName,
                                LocaliseOrDefault(request, "ReminderCreatedOnDate.Format", "{0:MMM d, yyyy}", DateTime.Now), homepageAlert.Message);
                        }
                        else
                        {
                            var userId = Repositories.Core.FindById<Person>(homepageAlert.CandidateId).User.Id;

                            CreateAlertMessage(Constants.AlertMessageKeys.JobSeekerMessage, homepageAlert.IsSystemAlert, userId, homepageAlert.ExpiryDate,
                                MessageAudiences.User, MessageTypes.General, homepageAlert.CandidateId,
                                person.FirstName,
                                person.LastName,
                                LocaliseOrDefault(request, "ReminderCreatedOnDate.Format", "{0:MMM d, yyyy}", DateTime.Now), homepageAlert.Message);
                        }
                    }

                    LogAction(request, ActionTypes.CreateCandidateHomepageAlerts, null);
                }

                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.IntegrationCandidateCreateHomepageAlertFailure), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Saves the person note.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public SavePersonNoteResponse SavePersonNote(SavePersonNoteRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new SavePersonNoteResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    var employer = Repositories.Core.FindById<Employer>(request.EmployerId);
                    if (employer == null)
                    {
                        response.SetFailure(FormatErrorMessage(request, ErrorTypes.InvalidEmployer, request.EmployerId));
                        Logger.Error(request.LogData(), response.Message, response.Exception);
                        return response;
                    }

                    var person = Repositories.Core.Query<Person>().SingleOrDefault(x => x.Id == request.PersonId);

                    if (person.IsNull())
                    {
                        response.SetFailure(FormatErrorMessage(request, ErrorTypes.PersonNotFound, request.PersonId));
                        Logger.Error(request.LogData(), response.Message, response.Exception);
                        return response;
                    }

                    // Get the candidate note
                    var personNote = Repositories.Core.PersonNotes.SingleOrDefault(x => x.PersonId == person.Id && x.EmployerId == request.EmployerId);

                    if (personNote == null)
                    {
                        personNote = new PersonNote
                        {
                            Person = person,
                            Employer = employer,
                        };

                        Repositories.Core.Add(personNote);
                    }

                    personNote.Note = request.Note;
                    personNote.DateAdded = DateTime.Now;

                    Repositories.Core.SaveChanges();

                    LogAction(request, ActionTypes.SavePersonNote, typeof(PersonNote).Name, personNote.Id, employer.Id, person.Id);
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Gets the person note.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public GetPersonNoteResponse GetPersonNote(GetPersonNoteRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new GetPersonNoteResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    var person = Repositories.Core.Query<Person>().SingleOrDefault(x => x.Id == request.PersonId);

                    PersonNoteDto candidateNote = null;

                    if (person.IsNotNull())
                    {
                        candidateNote = Repositories.Core.PersonNotes.Where(x => x.PersonId == person.Id && x.EmployerId == request.EmployerId).Select(x => x.AsDto()).SingleOrDefault();
                    }

                    if (candidateNote.IsNotNull()) response.PersonNote = candidateNote;

                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Updates the application status.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public UpdateApplicationStatusResponse UpdateApplicationStatus(UpdateApplicationStatusRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new UpdateApplicationStatusResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    Application application;
                    // If we have a candidate application id use that, if not we have to use a combination of person id and job id
                    if (request.ApplicationId.HasValue)
                    {
                        application = Repositories.Core.FindById<Application>(request.ApplicationId);

                        if (application == null)
                        {
                            response.SetFailure(FormatErrorMessage(request, ErrorTypes.CandidateApplicationNotFound, request.ApplicationId));
                            Logger.Error(request.LogData(), response.Message, response.Exception);
                            return response;
                        }
                    }
                    else
                    {
                        if (!request.PersonId.HasValue)
                        {
                            response.SetFailure(ErrorTypes.PersonDetailsNotSupplied, FormatErrorMessage(request, ErrorTypes.PersonDetailsNotSupplied));
                            Logger.Info(request.LogData(), "No person ID provided.");
                            return response;
                        }

                        var person = Repositories.Core.Persons.SingleOrDefault(x => x.Id == request.PersonId);

                        if (person.IsNull())
                        {
                            response.SetFailure(FormatErrorMessage(request, ErrorTypes.PersonNotFound));
                            Logger.Info(request.LogData(), string.Format("The person '{0}' was not found.", (request.PersonId ?? 0)));

                            return response;
                        }

                        application = Repositories.Core.Applications.FirstOrDefault(x => x.Posting.JobId == request.JobId && x.Resume.PersonId == person.Id);

                        if (application == null)
                        {
                            response.SetFailure(FormatErrorMessage(request, ErrorTypes.CandidateApplicationNotFound, person.Id, request.JobId));
                            Logger.Error(request.LogData(), response.Message, response.Exception);
                            return response;
                        }
                    }

                    var currentStatus = application.ApplicationStatus.IsNotNull() ? application.ApplicationStatus : ApplicationStatusTypes.NotApplicable;

                    if (currentStatus != request.NewStatus)
                    {
                        application.ApplicationStatus = request.NewStatus;
                        application.StatusLastChangedOn = DateTime.Now;
                        // Status changed so set to null, overnight process will reset when time is right
                        application.PostHireFollowUpStatus = null;

                        Repositories.Core.SaveChanges(true);

                        #region Log action

                        var logActionType = ActionTypes.NoAction;

                        switch (application.ApplicationStatus)
                        {
                            case ApplicationStatusTypes.SelfReferred:
                                logActionType = ActionTypes.UpdateApplicationStatusToSelfReferred;
                                break;
                            case ApplicationStatusTypes.Recommended:
                                logActionType = ActionTypes.UpdateApplicationStatusToRecommended;
                                break;
                            case ApplicationStatusTypes.FailedToShow:
                                logActionType = ActionTypes.UpdateApplicationStatusToFailedToShow;
                                break;
                            case ApplicationStatusTypes.Hired:
                                logActionType = ActionTypes.UpdateApplicationStatusToHired;
                                break;
                            case ApplicationStatusTypes.InterviewScheduled:
                                logActionType = ActionTypes.UpdateApplicationStatusToInterviewScheduled;
                                break;
                            case ApplicationStatusTypes.Interviewed:
                                logActionType = ActionTypes.UpdateApplicationStatusToInterviewed;
                                break;
                            case ApplicationStatusTypes.NewApplicant:
                                logActionType = ActionTypes.UpdateApplicationStatusToNewApplicant;
                                break;
                            case ApplicationStatusTypes.NotApplicable:
                                logActionType = ActionTypes.UpdateApplicationStatusToNotApplicable;
                                break;
                            case ApplicationStatusTypes.OfferMade:
                                logActionType = ActionTypes.UpdateApplicationStatusToOfferMade;
                                break;
                            case ApplicationStatusTypes.NotHired:
                                logActionType = ActionTypes.UpdateApplicationStatusToNotHired;
                                break;
                            case ApplicationStatusTypes.UnderConsideration:
                                logActionType = ActionTypes.UpdateApplicationStatusToUnderConsideration;
                                break;
                            case ApplicationStatusTypes.DidNotApply:
                                logActionType = ActionTypes.UpdateApplicationStatusToDidNotApply;
                                break;
                            case ApplicationStatusTypes.InterviewDenied:
                                logActionType = ActionTypes.UpdateApplicationStatusToInterviewDenied;
                                break;
                            case ApplicationStatusTypes.RefusedOffer:
                                logActionType = ActionTypes.UpdateApplicationStatusToRefusedOffer;
                                break;
                            case ApplicationStatusTypes.FailedToReportToJob:
                                logActionType = ActionTypes.UpdateApplicationStatusToFailedToReportToJob;
                                break;
                            case ApplicationStatusTypes.FailedToRespondToInvitation:
                                logActionType = ActionTypes.UpdateApplicationStatusToFailedToRespondToInvitation;
                                break;
                            case ApplicationStatusTypes.FoundJobFromOtherSource:
                                logActionType = ActionTypes.UpdateApplicationStatusToFoundJobFromOtherSource;
                                break;
                            case ApplicationStatusTypes.JobAlreadyFilled:
                                logActionType = ActionTypes.UpdateApplicationStatusToJobAlreadyFilled;
                                break;
                            case ApplicationStatusTypes.NotQualified:
                                logActionType = ActionTypes.UpdateApplicationStatusToNotQualified;
                                break;
                            case ApplicationStatusTypes.NotYetPlaced:
                                logActionType = ActionTypes.UpdateApplicationStatusToNotYetPlaced;
                                break;
                            case ApplicationStatusTypes.RefusedReferral:
                                logActionType = ActionTypes.UpdateApplicationStatusToRefusedReferral;
                                break;
                        }

                        LogStatusChange(request, typeof(Application).Name, application.Id, (long)currentStatus, (long)application.ApplicationStatus);

                        if (logActionType != ActionTypes.NoAction)
                            LogAction(request, logActionType, typeof(Application).Name, application.Id, application.Posting.JobId, application.PostingId);

                        if (logActionType == ActionTypes.UpdateApplicationStatusToHired)
                            Helpers.Messaging.Publish(request.ToApplicantHiredMessage(application.Id, application.Resume.PersonId));

                        if (AppSettings.IntegrationClient != IntegrationClient.Standalone)
                        {
                            var integrationRequest = new SetApplicationStatusRequest
                            {
                                ApplicationId = application.Id
                            };
                            if (RuntimeContext.IsNotNull() && RuntimeContext.CurrentRequest.IsNotNull() &&
                                RuntimeContext.CurrentRequest.UserContext.IsNotNull())
                            {
                                integrationRequest.ExternalAdminId = RuntimeContext.CurrentRequest.UserContext.ExternalUserId;
                                integrationRequest.ExternalOfficeId = RuntimeContext.CurrentRequest.UserContext.ExternalOfficeId;
                                integrationRequest.ExternalPassword = RuntimeContext.CurrentRequest.UserContext.ExternalPassword;
                                integrationRequest.ExternalUsername = RuntimeContext.CurrentRequest.UserContext.ExternalUserName;
                            }

                            Helpers.Messaging.Publish(new IntegrationRequestMessage
                            {
                                ActionerId = request.UserContext.UserId,
                                SetApplicationStatusRequest = integrationRequest,
                                IntegrationPoint = IntegrationPoint.SetApplicationStatus
                            });
                        }

                        #endregion
                    }
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Gets the flagged candidates.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public GetFlaggedCandidatesResponse GetFlaggedCandidates(GetFlaggedCandidatesRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new GetFlaggedCandidatesResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    var states = Helpers.Lookup.GetLookup(LookupTypes.States);

                    // TODO: Rework PersonListCandidateView as naming of fields is confusing
                    var candidates = Repositories.Core.PersonListCandidateViews.Where(plcv => plcv.ListType == ListTypes.FlaggedJobSeeker && plcv.PersonId == request.UserContext.PersonId).
                        OrderByDescending(plcv => plcv.CandidateIsVeteran).
                        ThenBy(plcv => plcv.CandidateLastName).
                        ThenBy(plcv => plcv.CandidateFirstName);

                    var pagedCandidates = candidates.GetPagedList(plcv => new ResumeView
                            {
                                Id = plcv.CandidatePersonId,
                                Name = plcv.CandidateFirstName + " " + plcv.CandidateLastName,
                                Flagged = true,
                                IsVeteran = plcv.CandidateIsVeteran,
                                CandidateLocation = plcv.CandidateTownCity + (plcv.CandidateTownCity.IsNotNullOrEmpty() ? ", " : "") + states.Where(s => s.Id == plcv.CandidateStateId).Select(s => s.ExternalId).FirstOrDefault(),
                                ContactInfoVisible = plcv.CandidateIsContactInfoVisible,
                                NcrcLevelId = plcv.CandidateNcrcLevelId
                            }, request.Criteria.PageIndex, request.Criteria.PageSize);

                    var jobId = request.Criteria.JobId ?? 0;

                    // Get a list of all applicants for any job the employer has had
                    var candidateIds = pagedCandidates.Select(candidate => candidate.Id).ToList();
                    var employerApplicantIds = Repositories.Core.ApplicationViews
                        .Where(x => x.EmployerId == request.UserContext.EmployerId && x.CandidateApplicationApprovalStatus == ApprovalStatuses.Approved && candidateIds.Contains(x.CandidateId))
                        .Select(x => x.CandidateId)
                        .ToList();

                    // Get a list of all applicants for the job in scope
                    var statuses = new List<ApprovalStatuses> { ApprovalStatuses.Approved, ApprovalStatuses.WaitingApproval, ApprovalStatuses.Reconsider, ApprovalStatuses.Rejected };
                    var jobApplicants = (jobId > 0) ? Repositories.Core.ApplicationViews.Where(x => x.JobId == jobId && statuses.Contains(x.CandidateApplicationApprovalStatus)).ToList() : new List<ApplicationView>();

                    foreach (var pagedCandidate in pagedCandidates)
                    {
                        var person = Repositories.Core.Persons.FirstOrDefault(x => x.Id == pagedCandidate.Id);

                        if (person.IsNotNull())
                        {
                            var resume = person.Resumes.FirstOrDefault(x => x.IsDefault);

                            if (resume.IsNotNull())
                            {
                                // Get years experience
                                pagedCandidate.YearsExperience = resume.YearsExperience ?? 0;

                                CandidateHelper.UpdateResumeViewName(pagedCandidate, employerApplicantIds, RuntimeContext);

                                /*
                                if (AppSettings.ShowContactDetails != ShowContactDetails.Partial)
                                {
                                    // Mask name if has not applied for any employer jobs
                                    if ((employerApplicants.IsNullOrEmpty() || employerApplicants.Count > 0 || (!employerApplicants.Exists(x => x.CandidateId == pagedCandidate.Id) || request.UserContext.IsShadowingUser)) && (!pagedCandidate.ContactInfoVisible && AppSettings.ShowContactDetails == ShowContactDetails.Full || AppSettings.ShowContactDetails == ShowContactDetails.None))
                                        pagedCandidate.Name = pagedCandidate.Id.ToString();
                                }
                                */

                                // Get date application received on and application status
                                if (jobApplicants.IsNotNullOrEmpty() && jobApplicants.Exists(x => x.CandidateId == pagedCandidate.Id))
                                {
                                    var application = jobApplicants.SingleOrDefault(x => x.CandidateId == pagedCandidate.Id);
                                    if (application.IsNotNull())
                                    {
                                        pagedCandidate.ApplicationReceivedOn = application.CandidateApplicationReceivedOn;
                                        pagedCandidate.Status = application.CandidateApplicationStatus;
                                        pagedCandidate.ApplicationApprovalStatus = application.CandidateApplicationApprovalStatus;
                                        pagedCandidate.ApplicationId = application.Id;
                                        pagedCandidate.ApplicationLockVersion = application.LockVersion;
                                    }
                                }
                                else
                                {
                                    pagedCandidate.Status = ApplicationStatusTypes.NotApplicable;
                                    pagedCandidate.ApplicationApprovalStatus = ApprovalStatuses.None;
                                }

                                if (AppSettings.ShowBrandingStatement)
                                {
                                    var resumeModel = resume.ResumeXml.ToResumeModel(RuntimeContext, request.UserContext.Culture);
                                    pagedCandidate.Branding = resumeModel.Special.Branding;
                                }
                                // Employer notes
                                pagedCandidate.EmployerNote = person.PersonNotes.Where(pn => pn.EmployerId == request.UserContext.EmployerId).Select(pn => pn.Note).FirstOrDefault();

                                // Work history
                                if (resume.ResumeJobs.IsNotNullOrEmpty())
                                {
                                    foreach (var job in resume.ResumeJobs)
                                    {
                                        pagedCandidate.EmploymentHistories.Add(new ResumeView.ResumeJobView
                                        {
                                            JobTitle = job.JobTitle,
                                            Employer = job.Employer,
                                            YearStart = job.StartYear,
                                            YearEnd = job.EndYear
                                        });
                                    }
                                }
                            }
                        }
                    }

                    response.Candidates = pagedCandidates;
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Gets the candidate.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public GetApplicationResponse GetApplication(GetApplicationRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new GetApplicationResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    if (request.PersonId.IsNotNull())
                    {
                        var application = (from a in Repositories.Core.Applications
                                           join r in Repositories.Core.Resumes
                                               on a.ResumeId equals r.Id
                                           join p in Repositories.Core.Postings
                                               on a.PostingId equals p.Id
                                           where r.PersonId == request.PersonId
                                                 && ((request.JobId > 0 && p.JobId == request.JobId) || (request.PostingId > 0 && p.Id == request.PostingId))
                                           select a).FirstOrDefault();

                        if (application.IsNotNull())
                            response.Application = application.AsDto();
                    }
                    else if (request.PersonIds.IsNotNullOrEmpty())
                    {
                        var applications = (from a in Repositories.Core.Applications
                                            join r in Repositories.Core.Resumes
                                                on a.ResumeId equals r.Id
                                            join p in Repositories.Core.Postings
                                                on a.PostingId equals p.Id
                                            where request.PersonIds.Contains(r.PersonId)
                                                  && ((request.JobId > 0 && p.JobId == request.JobId) || (request.PostingId > 0 && p.Id == request.PostingId))
                                            select new
                                            {
                                                r.PersonId,
                                                Application = a
                                            }).ToDictionary(r => r.PersonId, r => r.Application.AsDto());

                        response.Applications = applications;
                    }
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Gets the candidate's referrals
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>A list of referrals</returns>
        public GetApplicationViewResponse GetApplicationViews(GetApplicationViewRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new GetApplicationViewResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    var dateRange = DateTime.Now.Date.AddDays(-30);
                    var applications = Repositories.Core.ApplicationViews
                        .Where(app => app.CandidateId == request.PersonId && app.JobClosingOn >= dateRange)
                        .OrderByDescending(app => app.CandidateApplicationReceivedOn)
                        .Select(app => app.AsDto());

                    if (request.ReferralsRequired.HasValue)
                        applications = applications.Take(request.ReferralsRequired.Value);

                    response.ApplicationViews = applications.ToList();
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Gets the applicant candidate view.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public ResumeViewResponse GetApplicantResumeView(ResumeViewRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new ResumeViewResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    var query = new ResumeViewQuery(Repositories.Core, request.Criteria);

                    var flaggedCandidates =
                        Repositories.Core.PersonListCandidateViews.Where(
                            x => x.PersonId == request.Criteria.FlagPersonId && x.ListType == ListTypes.FlaggedJobSeeker).ToList();

                    switch (request.Criteria.FetchOption)
                    {
                        case CriteriaBase.FetchOptions.PagedList:
                            var candidatesPaged = new PagedList<ResumeView>(query.Query(true, true), request.Criteria.PageIndex, request.Criteria.PageSize);

                            // Get other required data
                            foreach (var pagedCandidate in candidatesPaged)
                            {
                                var otherCandidateViewInfo = GetOtherRequiredCandidateViewInformation(pagedCandidate, flaggedCandidates, request.UserContext.EmployerId);
                                pagedCandidate.Flagged = otherCandidateViewInfo.Flagged;
                                pagedCandidate.EmployerNote = otherCandidateViewInfo.EmployerNote;
                                pagedCandidate.EmploymentHistories = otherCandidateViewInfo.EmploymentHistories;
                                pagedCandidate.IsApplicantForEmployer = true;
                            }

                            GetAssistNotesAndRemindersCount(request.Criteria, candidatesPaged);
                            response.ResumeViewsPaged = candidatesPaged;
                            //FVN-5970
                            var underAgeSeekers = Repositories.Core.Persons.Where(p => p.Age <= AppSettings.UnderAgeJobSeekerRestrictionThreshold).Select(x => x.Id).ToList();
                            response.ResumeViewsPaged.RemoveAll(r => underAgeSeekers.Contains(r.Id));
                            //response.ResumeViewsPaged.TotalCount = response.ResumeViewsPaged.Count;
                            break;
                        case CriteriaBase.FetchOptions.List:
                            var candidatesList = query.Query(true, true).ToList();

                            // Get other required data
                            foreach (var listCandidate in candidatesList)
                            {
                                var otherCandidateViewInfo = GetOtherRequiredCandidateViewInformation(listCandidate, flaggedCandidates, request.UserContext.EmployerId);
                                listCandidate.Flagged = otherCandidateViewInfo.Flagged;
                                listCandidate.EmployerNote = otherCandidateViewInfo.EmployerNote;
                                listCandidate.EmploymentHistories = otherCandidateViewInfo.EmploymentHistories;
                                listCandidate.IsApplicantForEmployer = true;
                            }

                            GetAssistNotesAndRemindersCount(request.Criteria, candidatesList);
                            response.ResumeViews = candidatesList;
                            break;
                        default:
                            var candidate = request.Criteria.ApplicationId.IsNotNull() ? query.QueryEntityId() : query.Query(false, false).SingleOrDefault();
                            if (candidate.IsNotNull())
                                candidate.IsApplicantForEmployer = true;
                            response.ResumeView = GetOtherRequiredCandidateViewInformation(candidate, flaggedCandidates, request.UserContext.EmployerId);
                            break;
                    }
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }
                return response;
            }
        }

        /// <summary>
        /// Updates the resumes with the count of any Assist notes and reminds
        /// </summary>
        /// <param name="criteria">The search critera</param>
        /// <param name="resumes">The list of resumes</param>
        private void GetAssistNotesAndRemindersCount(ResumeViewCriteria criteria, List<ResumeView> resumes)
        {
            if (criteria.GetNotesAndRemindersCount)
            {
                var candidateIds = resumes.Select(r => r.Id).ToList();

                var notesCounts = Repositories.Core.NoteReminderViews
                    .Where(n => candidateIds.Contains(n.EntityId) && n.EntityType == EntityTypes.JobSeeker)
                    .GroupBy(n => n.EntityId)
                    .Select(group => new
                    {
                        Id = group.Key,
                        Count = group.Count()
                    })
                    .ToDictionary(n => n.Id, n => n.Count);

                resumes.ForEach(r => r.NotesAndRemindersCount = notesCounts.Keys.Contains(r.Id) ? notesCounts[r.Id] : r.NotesAndRemindersCount);
            }
        }

        /// <summary>
        /// Gets the other required candidate view information.
        /// </summary>
        /// <param name="resumeView">The resume view.</param>
        /// <param name="flaggedCandidates">The flagged candidates.</param>
        /// <param name="employerId">The employer id.</param>
        /// <returns></returns>
        private ResumeView GetOtherRequiredCandidateViewInformation(ResumeView resumeView, List<PersonListCandidateView> flaggedCandidates, long? employerId)
        {
            var person = Repositories.Core.Persons.FirstOrDefault(x => x.Id == resumeView.Id);

            if (person.IsNull()) return resumeView;

            // See if they are flagged
            resumeView.Flagged = (flaggedCandidates.IsNotNullOrEmpty() && flaggedCandidates.Exists(x => x.CandidatePersonId == person.Id));

            // Employer notes
            if (employerId.HasValue)
                resumeView.EmployerNote = person.PersonNotes.Where(pn => pn.EmployerId == employerId.Value).Select(pn => pn.Note).FirstOrDefault();

            // Work history
            var resume = person.Resumes.FirstOrDefault(x => x.Id == Repositories.Core.Applications.Where(a => a.Id.Equals(resumeView.ApplicationId)).Select(a => a.ResumeId).FirstOrDefault());

            if (resume.IsNotNull())
            {
                resumeView.CandidateLocation = GetCandidateLocation(resume.TownCity, resume.StateId);

                if (resume.ResumeJobs.IsNotNullOrEmpty())
                {
                    foreach (var job in resume.ResumeJobs)
                    {
                        resumeView.EmploymentHistories.Add(new ResumeView.ResumeJobView
                        {
                            JobTitle = job.JobTitle,
                            Employer = job.Employer,
                            YearStart = job.StartYear,
                            YearEnd = job.EndYear
                        });
                    }
                }
            }

            return resumeView;
        }

        /// <summary>
        /// Gets the candidate location.
        /// </summary>
        /// <param name="town">The town.</param>
        /// <param name="stateId">The state identifier.</param>
        /// <returns></returns>
        private string GetCandidateLocation(string town, long? stateId)
        {
            return (town.IsNotNullOrEmpty() && stateId.HasValue) ? string.Format("{0}, {1}", town, Helpers.Lookup.GetLookupExternalId(LookupTypes.States, stateId.Value)) : string.Empty;
        }

        /// <summary>
        /// Gets the application status log view.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public ApplicationStatusLogViewResponse GetApplicationStatusLogView(ApplicationStatusLogViewRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new ApplicationStatusLogViewResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    var query = new ApplicationStatusLogViewQuery(Repositories.Core, request.Criteria);

                    switch (request.Criteria.FetchOption)
                    {
                        case CriteriaBase.FetchOptions.PagedList:
                            response.ApplicationStatusLogViewsPaged = query.Query().GetPagedList(x => x.AsDto(), request.Criteria.PageIndex, request.Criteria.PageSize);
                            break;

                        case CriteriaBase.FetchOptions.List:
                            response.ApplicationStatusLogViews = query.Query().Select(x => x.AsDto()).ToList();
                            break;

                        case CriteriaBase.FetchOptions.Grouped:
                            var employerJobReferralListItems = new List<EmployerJobReferralListItem>();

                            var totalJobs = query.Query().Select(a => a.JobId).Distinct().Count();
                            var pagedJobIds = query.Query().Select(a => a.JobId).Distinct().Skip(request.Criteria.PageIndex * request.Criteria.PageSize).Take(request.Criteria.PageSize).ToList();

                            var applicationStatusLogViews = query.Query().Where(x => pagedJobIds.Contains(x.JobId)).Select(x => x.AsDto()).ToList();

                            var mostRecentChangeForEachCandidate = from view in applicationStatusLogViews
                                                                   orderby view.ActionedOn descending
                                                                   group view by view.CandidateApplicationId
                                                                       into appgp
                                                                       select appgp.First();

                            var referralsGroupedByJob = (from view in mostRecentChangeForEachCandidate
                                                         group view by view.JobId
                                                             into viewGroup
                                                             select viewGroup.ToList());

                            foreach (var viewGroup in referralsGroupedByJob)
                            {
                                var viewGroupOrdered = viewGroup.OrderByDescending(x => x.ActionedOn).ToList();
                                var employerJobReferralListItem = new EmployerJobReferralListItem
                                {
                                    NumReferrals = viewGroupOrdered.Count(),
                                    MostRecentReferral = viewGroupOrdered[0],
                                    OtherReferrals = viewGroupOrdered,
                                    Id = viewGroupOrdered[0].JobId
                                };

                                employerJobReferralListItem.OtherReferrals.RemoveAt(0);

                                employerJobReferralListItems.Add(employerJobReferralListItem);

                                if (request.Criteria.OrderBy == Constants.SortOrders.DateReceivedAsc)
                                    employerJobReferralListItems = employerJobReferralListItems.OrderBy(x => x.MostRecentReferral.ActionedOn).ToList();

                                if (request.Criteria.OrderBy == Constants.SortOrders.DateReceivedDesc)
                                    employerJobReferralListItems = employerJobReferralListItems.OrderByDescending(x => x.MostRecentReferral.ActionedOn).ToList();

                                if (request.Criteria.OrderBy == Constants.SortOrders.JobTitleAsc)
                                    employerJobReferralListItems = employerJobReferralListItems.OrderBy(x => x.MostRecentReferral.JobTitle).ToList();

                                if (request.Criteria.OrderBy == Constants.SortOrders.JobTitleDesc)
                                    employerJobReferralListItems = employerJobReferralListItems.OrderByDescending(x => x.MostRecentReferral.JobTitle).ToList();
                            }

                            response.EmployerJobReferralListItems = new PagedList<EmployerJobReferralListItem>(employerJobReferralListItems, totalJobs, request.Criteria.PageIndex, request.Criteria.PageSize, false);
                            break;
                        default:
                            response.ApplicationStatusLogView = request.Criteria.Id.IsNotNull() ? query.QueryEntityId().AsDto() : query.Query().SingleOrDefault().AsDto();
                            break;
                    }
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Gets the job seeker activity view.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public JobSeekerActivityViewResponse GetJobSeekerActivityView(JobSeekerActivityViewRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new JobSeekerActivityViewResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                var jobSeekerId = request.Criteria.JobSeekerId;
                var person = Repositories.Core.FindById<Person>(jobSeekerId);
                var user = person.User;

                var actionEventsByUserId = Repositories.Core.ActionEvents.Where(x => x.UserId == user.Id);
                var actionEventsByEntityId = Repositories.Core.ActionEvents.Where(x => x.EntityId == user.Id);
                var actionEventsByAdd02 = Repositories.Core.ActionEvents.Where(x => x.EntityIdAdditional02 == jobSeekerId);

                var daysBack = request.Criteria.DaysBack;

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    var actionTypes = new List<ActionTypes>
					{
						ActionTypes.JobSearchHighGrowth,
						ActionTypes.SaveResume,
						ActionTypes.ShowNotWhatYoureLookingFor,
						ActionTypes.ViewJobDetails,
						ActionTypes.ReferralRequest,
						ActionTypes.SelfReferral,
						ActionTypes.JobSearch
					};

                    var countTypes = CountActionsByActionTypes(actionTypes, actionEventsByUserId, daysBack);

                    var staffReferrals = request.GetStaffReferrals
                        ? CountActionsByActionType(ActionTypes.CreateCandidateApplication, actionEventsByAdd02, daysBack)
                        : 0;

                    var highGrowthSectors = request.GetHighGrowthSectors
                        ? countTypes[ActionTypes.JobSearchHighGrowth]
                        : 0;

                    response.JobSeekerActivityView = new JobSeekerActivityView
                    {
                        DaysSinceRegistration = (DateTime.Now - user.CreatedOn).Days,
                        FocusCareerSignins = CountActionsByActionType(ActionTypes.LogIn, actionEventsByEntityId, daysBack),
                        LastLogin = user.LoggedInOn,
                        ResumeEdited = countTypes[ActionTypes.SaveResume],
                        OnlineResumeHelpUsed = countTypes[ActionTypes.ShowNotWhatYoureLookingFor],
                        HighScoreMatches = Repositories.Core.PersonPostingMatches.Count(x => x.PersonId == person.Id && x.Score >= AppSettings.StarRatingMinimumScores[3]),
                        MatchesViewed = countTypes[ActionTypes.ViewJobDetails],
                        ReferralsRequested = countTypes[ActionTypes.ReferralRequest],
                        StaffReferrals = staffReferrals,
                        SelfReferrals = countTypes[ActionTypes.SelfReferral],
                        JobSearchesPerformed = countTypes[ActionTypes.JobSearch],
                        JobSearchesByHighGrowthSectors = highGrowthSectors,
                        JobSearchesSavedForJobAlerts = Repositories.Core.SavedSearchUsers.Count(x => x.UserId == user.Id && x.SavedSearch.Type == SavedSearchTypes.CareerPostingSearch)
                    };
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Gets the job seeker profile.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public JobSeekerProfileResponse GetJobSeekerProfile(JobSeekerProfileRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new JobSeekerProfileResponse(request);

                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    // Get basic job seeker data
                    var jobSeekerProfileView = Repositories.Core.JobSeekerProfileViews.Single(x => x.Id == request.JobSeekerId);

                    if (jobSeekerProfileView != null)
                        response.JobSeekerProfileView = jobSeekerProfileView.AsDto();

                    //should be resume id
                    var resumeXml = response.JobSeekerProfileView.DefaultResumeId.HasValue
                        ? Repositories.Core.Resumes.Single(x => x.Id == response.JobSeekerProfileView.DefaultResumeId).ResumeXml
                        : string.Empty;
                    var resumeModel = resumeXml.Length > 0
                        ? resumeXml.ToResumeModel(RuntimeContext, request.UserContext.Culture)
                        : null;

                    // DATA FOR PROFILE PANEL
                    response.UnemploymentInfo = ""; // Empty for now as unable to see where set in Career

                    if (resumeModel != null)
                    {
                        response.VeteranInfo = resumeModel.ResumeContent.Profile.Veteran;
                        response.ResumeId = resumeModel.ResumeMetaInfo.ResumeId;
                        response.PrimaryPhone = resumeModel.ResumeContent.Profile.PrimaryPhone;

                        if (resumeModel.ResumeContent.Profile.DisabilityStatus != null)
                            response.DisabilityStatus = (DisabilityStatus)resumeModel.ResumeContent.Profile.DisabilityStatus;

                        response.DisabilityCategoryId = resumeModel.ResumeContent.Profile.DisabilityCategoryId;
                        response.DisabilityCategoryIds = resumeModel.ResumeContent.Profile.DisabilityCategoryIds;

                        // Migrant/Seasonal Workers
                        response.IsMSFW = resumeModel.ResumeContent.Profile.IsMSFW;
                        response.MSFWQuestions = resumeModel.ResumeContent.Profile.MSFWQuestions;

                        //Language/Culture Issues
                        response.CulturalBarriers = resumeModel.ResumeContent.Profile.CulturalBarriers;
                        response.LowLevelLiteracy = resumeModel.ResumeContent.Profile.LowLevelLiteracy;
                        response.PreferredLanguage = resumeModel.ResumeContent.Profile.PreferredLanguage;
                        response.NativeLanguage = resumeModel.ResumeContent.Profile.LowLevelLiteracyIssues.NativeLanguage;
                        response.CommonLanguage = resumeModel.ResumeContent.Profile.LowLevelLiteracyIssues.CommonLanguage;

                        //Income Issues
                        response.NoOfDependents = resumeModel.ResumeContent.Profile.NoOfDependents;
                        response.EstMonthlyIncome = resumeModel.ResumeContent.Profile.EstMonthlyIncome;
                        response.DisplacedHomemaker = resumeModel.ResumeContent.Profile.DisplacedHomemaker;
                        response.SingleParent = resumeModel.ResumeContent.Profile.SingleParent;
                        response.LowIncomeStatus = resumeModel.ResumeContent.Profile.LowIncomeStatus;
                        //HouseIssue Panel

                        response.HomelessWithoutShelter = (bool)resumeModel.ResumeContent.Profile.IsHomeless;//homelessWithoutShelter
                        response.HomelessWithShelter = (bool)resumeModel.ResumeContent.Profile.IsStaying;//homelessWithShelter
                        response.RunawayYouth = (bool)resumeModel.ResumeContent.Profile.IsUnderYouth;//Runaway under age 18

                        //LegalIssue Panel
                        response.ExOffender = (bool)resumeModel.ResumeContent.Profile.IsExOffender;//Ex-Offender

                        // DATA FOR RESUME PANEL
                        response.ResumeLength = Helpers.Resume.GetNumberOfWords(resumeXml);

                        if (resumeModel.ResumeContent.Skills.Skills != null)
                            response.SkillsCodedFromResume = resumeModel.ResumeContent.Skills.Skills.Count();

                        response.NumberOfResumes = Repositories.Core.Resumes.Count(x => x.PersonId == request.JobSeekerId && x.StatusId.Equals(1));
                        response.NumberOfIncompleteResumes = Repositories.Core.Resumes.Count(x => x.PersonId == request.JobSeekerId && x.StatusId.Equals(1) && x.ResumeCompletionStatusId != ResumeCompletionStatuses.Completed);

                        response.EducationLevel = resumeModel.ResumeContent.EducationInfo.EducationLevel;

                        var person = Repositories.Core.Persons.FirstOrDefault(x => x.Id == request.JobSeekerId);
                        if (person.IsNotNull())
                            response.EnrollmentStatus = person.EnrollmentStatus;

                        response.EmploymentStatus = resumeModel.ResumeContent.ExperienceInfo.EmploymentStatus;

                        if (resumeModel.ResumeMetaInfo.CompletionStatus < ResumeCompletionStatuses.Contact && jobSeekerProfileView != null)
                        {
                            SetContactDetails(response, request.JobSeekerId, true);
                        }

                        //check that the resume counts only the active resumes
                        if (resumeModel.ResumeMetaInfo.CompletionStatus >= ResumeCompletionStatuses.Preferences && jobSeekerProfileView != null)
                        {
                            response.ResumeIsSearchable = jobSeekerProfileView.IsSearchable;
                            response.WillingToRelocate = resumeModel.Special.Preferences.WillingToRelocate;
                            response.WillingToWorkOvertime = resumeModel.Special.Preferences.ShiftDetail.WorkOverTime;
                        }
                        else
                        {
                            response.ResumeIsSearchable = null;
                            response.WillingToRelocate = null;
                            response.WillingToWorkOvertime = null;
                        }
                    }
                    else
                    {
                        SetContactDetails(response, request.JobSeekerId, true);
                    }

                    var resumes = Repositories.Core.FindById<Person>(request.JobSeekerId).Resumes;

                    var applicationIds = (from r in resumes
                                          from a in r.Applications
                                          select (long?)a.Id).ToList();

                    var actionEvents = from ae in Repositories.Core.ActionEvents
                                       where applicationIds.Contains(ae.EntityId)
                                       select ae;

                    var countTypes = new List<ActionTypes>
					{
						ActionTypes.UpdateApplicationStatusToDidNotApply,
						ActionTypes.UpdateApplicationStatusToFailedToReportToJob,
						ActionTypes.UpdateApplicationStatusToFailedToRespondToInvitation,
						ActionTypes.UpdateApplicationStatusToFailedToShow,
						ActionTypes.UpdateApplicationStatusToFoundJobFromOtherSource,
						ActionTypes.UpdateApplicationStatusToHired,
						ActionTypes.UpdateApplicationStatusToInterviewDenied,
						ActionTypes.UpdateApplicationStatusToInterviewScheduled,
						ActionTypes.UpdateApplicationStatusToJobAlreadyFilled,
						ActionTypes.UpdateApplicationStatusToNewApplicant,
						ActionTypes.UpdateApplicationStatusToNotHired,
						ActionTypes.UpdateApplicationStatusToNotQualified,
						ActionTypes.UpdateApplicationStatusToNotYetPlaced,
						ActionTypes.UpdateApplicationStatusToRecommended,
						ActionTypes.UpdateApplicationStatusToRefusedOffer,
						ActionTypes.UpdateApplicationStatusToRefusedReferral,
						ActionTypes.UpdateApplicationStatusToUnderConsideration,
						ActionTypes.SelfReferral,
						ActionTypes.ApproveCandidateReferral,
						ActionTypes.AutoApprovedReferralBypass,
						ActionTypes.CreateCandidateApplication
					};

                    var allCounts = CountActionsByActionTypes(countTypes, actionEvents);

                    // DATA FOR REFERRAL OUTCOME PANEL
                    response.ApplicantsDidNotApply = allCounts[ActionTypes.UpdateApplicationStatusToDidNotApply];
                    response.ApplicantsFailedToReportToJob = allCounts[ActionTypes.UpdateApplicationStatusToFailedToReportToJob];
                    response.ApplicantsFailedToRespondToInvitation = allCounts[ActionTypes.UpdateApplicationStatusToFailedToRespondToInvitation];
                    response.ApplicantsFailedToShow = allCounts[ActionTypes.UpdateApplicationStatusToFailedToShow];
                    response.ApplicantsFoundJobFromOtherSource = allCounts[ActionTypes.UpdateApplicationStatusToFoundJobFromOtherSource];
                    response.ApplicantsHired = allCounts[ActionTypes.UpdateApplicationStatusToHired];
                    response.ApplicantsInterviewDenied = allCounts[ActionTypes.UpdateApplicationStatusToInterviewDenied];
                    response.ApplicantsInterviewed = allCounts[ActionTypes.UpdateApplicationStatusToInterviewScheduled];
                    response.ApplicantsJobAlreadyFilled = allCounts[ActionTypes.UpdateApplicationStatusToJobAlreadyFilled];
                    response.ApplicantsRejected = allCounts[ActionTypes.UpdateApplicationStatusToNotHired];
                    response.ApplicantsNotQualified = allCounts[ActionTypes.UpdateApplicationStatusToNotQualified];
                    response.ApplicantsNotYetPlaced = allCounts[ActionTypes.UpdateApplicationStatusToNotYetPlaced];
                    response.ApplicantsRecommended = allCounts[ActionTypes.UpdateApplicationStatusToRecommended];
                    response.ApplicantsRefused = allCounts[ActionTypes.UpdateApplicationStatusToRefusedOffer];
                    response.ApplicantsRefusedReferral = allCounts[ActionTypes.UpdateApplicationStatusToRefusedReferral];
                    response.ApplicantsUnderConsideration = allCounts[ActionTypes.UpdateApplicationStatusToUnderConsideration];

                    var newApplicantTypes = new List<ActionTypes> { ActionTypes.UpdateApplicationStatusToNewApplicant, ActionTypes.SelfReferral, ActionTypes.ApproveCandidateReferral, ActionTypes.AutoApprovedReferralBypass, ActionTypes.CreateCandidateApplication };

                    var newApplicantsCount = allCounts.Where(x => newApplicantTypes.Contains(x.Key)).Sum(x => x.Value);

                    response.ApplicantsNewApplicant = newApplicantsCount;

                    // SURVEY RESULTS PANEL
                    var totals = Repositories.Core.JobSeekerSurveyTotals(request.JobSeekerId).ToList();

                    if (totals.Count == 1)
                    {
                        response.MatchQualityDissatisfied = totals[0].Dissatisfied;
                        response.MatchQualitySomewhatDissatisfied = totals[0].SomewhatDissatisfied;
                        response.MatchQualitySomewhatSatisfied = totals[0].SomewhatSatisfied;
                        response.MatchQualitySatisfied = totals[0].VerySatisfied;
                        response.NotReceiveUnexpectedMatches = totals[0].NoUnanticipatedMatches;
                        response.ReceivedUnexpectedResults = totals[0].UnanticipatedMatches;
                        response.NotEmployerInvitationsReceive = totals[0].WasNotInvitedOrDidNotApply;
                        response.ReceivedEmployerInvitations = totals[0].WasInvitedDidApply;
                    }

                    response.OfficeIds = Repositories.Core.PersonOfficeMappers.Where(x => x.PersonId == request.JobSeekerId).Select(x => x.OfficeId).ToList();

                    if (request.ShowContactDetails)
                    {
                        LogAction(request, ActionTypes.ViewedJobSeekerContactInfo, typeof(Person).Name, request.JobSeekerId);
                    }
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Gets the candidate.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public GetCandidateResponse GetCandidate(GetCandidateRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new GetCandidateResponse(request);

                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    if (request.GetPersonRecord.GetValueOrDefault(false))
                        response.Person = Repositories.Core.FindById<Person>(request.PersonId).AsDto();
                    else
                        response.Candidate = Repositories.Core.FindById<CandidateView>(request.PersonId).AsDto();
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Resolves the candidate issues.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="autoresolve"></param>
        /// <returns></returns>
        public ResolveCandidateIssuesResponse ResolveCandidateIssues(ResolveCandidateIssuesRequest request, bool autoresolve = false)
        {

            using (Profiler.Profile(request.LogData(), request))
            {

                var response = new ResolveCandidateIssuesResponse(request);

                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    ResolveCandidateIssue(request, request.PersonId, request.UserContext.UserId, request.IssuesResolved, autoresolve);
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Resolves one or more issues for a candidate
        /// </summary>
        /// <param name="request">The current service request</param>
        /// <param name="personId">The Id of the job seeker</param>
        /// <param name="resolverUserId">The id of the user resolving the issue</param>
        /// <param name="issuesResolved">A list of issues to resolve</param>
        /// <param name="autoresolve">Was the issue auto-resolved</param>
        private void ResolveCandidateIssue(ServiceRequest request, long personId, long resolverUserId, List<CandidateIssueType> issuesResolved, bool autoresolve = false)
        {
            var logAction = true;

            var user = Repositories.Core.Users.First(x => x.Id == request.UserContext.UserId);
            var issues = GetCandidateIssues(personId);
            foreach (var issue in issuesResolved)
            {
                switch (issue)
                {
                    case CandidateIssueType.NotLoggingIn:
                        if (!issues.NoLoginTriggered)
                        {
                            logAction = false;
                        }
                        else
                        {
                            ResolveLoginIssue(issues);
                            //Updating LoggedInOn value of the user to fix the issue jobseeker re-activated by staff is made inactive for the next day
                            user.LoggedInOn = DateTime.Now;
                        }
                        break;

                    case CandidateIssueType.NotClickingLeads:
                        if (!issues.NotClickingOnLeadsTriggered)
                        {
                            logAction = false;
                        }
                        else
                        {
                            ResolveNotClickingOnLeadsIssue(issues);
                        }
                        break;

                    case CandidateIssueType.RejectingJobOffers:
                        ResolveJobOfferRejectionIssue(issues);
                        break;

                    case CandidateIssueType.NotReportingForInterviews:
                        ResolveNotReportingToInterviewIssue(issues);
                        break;

                    case CandidateIssueType.NotRespondingToEmployerInvites:
                        if (!issues.NotRespondingToEmployerInvitesTriggered)
                        {
                            logAction = false;
                        }
                        else
                        {
                            ResolveNotRespondingToEmployerInvitesIssue(issues);
                        }
                        break;

                    case CandidateIssueType.PoorQualityResume:
                        if (!issues.PostingLowQualityResumeTriggered)
                        {
                            logAction = false;
                        }
                        else
                        {
                            ResolvePoorQualityResumeIssue(personId, issues);
                        }
                        break;

                    case CandidateIssueType.ShowingLowQualityMatches:
                        ResolveShowingLowQualityMatchesIssue(issues);
                        break;

                    case CandidateIssueType.SuggestingPostHireFollowUp:
                        ResolvePostHireFollowUpIssue(issues);
                        break;

                    case CandidateIssueType.RequiringFollowUp:
                        ResolveFollowUpRequestIssue(issues);
                        break;

                    case CandidateIssueType.NotSearchingJobs:
                        if (!issues.NotSearchingJobsTriggered)
                        {
                            logAction = false;
                        }
                        else
                        {
                            ResolveNotSearchingJobsIssue(issues);
                        }
                        break;

                    case CandidateIssueType.InappropriateEmailAddress:
                        if (issues.InappropriateEmailAddress ?? false)
                        {
                            ResolveInappropriateEmailAddressIssue(issues);
                        }
                        else
                        {
                            logAction = false;
                        }
                        break;

                    case CandidateIssueType.GivingPositiveFeedback:
                        ResolveGivingPositiveFeedbackIssue(issues);
                        break;

                    case CandidateIssueType.GivingNegativeFeedback:
                        ResolveGivingNegativeFeedbackIssue(issues);
                        break;

                    case CandidateIssueType.PotentialMSFW:
                        if (!issues.MigrantSeasonalFarmWorkerTriggered)
                        {
                            logAction = false;
                        }
                        else
                        {
                            ResolvePotentialMSFWIssue(issues);
                        }
                        break;

                    case CandidateIssueType.ExpiredAlienCertification:
                        if (issues.HasExpiredAlienCertificationRegistration ?? false)
                        {
                            ResolveExpiredAlienCertificationIssue(issues);
                        }
                        else
                        {
                            logAction = false;
                        }
                        break;
                }
            }
            Repositories.Core.SaveChanges();

            if (logAction)
            {
                if (autoresolve)
                {
                    LogAction(request, ActionTypes.AutoResolvedIssue, typeof(Person).Name, personId,
                        resolverUserId, null, issuesResolved[0].ToString());
                }
                else
                {
                    foreach (var issue in issuesResolved)
                        LogAction(request, ActionTypes.MarkCandidateIssuesResolved, typeof(Person).Name, personId,
                            resolverUserId, null, issue.ToString());
                }
            }
        }

        /// <summary>
        /// Flags the candidate for follow up.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public FlagCandidateForFollowUpResponse FlagCandidateForFollowUp(FlagCandidateForFollowUpRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new FlagCandidateForFollowUpResponse(request);

                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    var issues = GetCandidateIssues(request.PersonId);
                    issues.FollowUpRequested = true;
                    Repositories.Core.SaveChanges();

                    LogAction(request, ActionTypes.RequestFollowUp, typeof(Person).Name, request.PersonId, request.UserContext.UserId);
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Gets the job seeker activities.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public JobSeekerActivityActionViewResponse GetJobSeekerActivities(JobSeekerActivityActionViewRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new JobSeekerActivityActionViewResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    // Basic query
                    var query = Repositories.Core.JobSeekerActivityActionViews.Where(x => x.JobSeekerId == request.Criteria.JobSeekerId && x.ActionType != ActionTypes.JobSearch.ToString());

                    if (request.Criteria.DaysBack > 0)
                        query = query.Where(x => x.ActionedOn >= DateTime.Now.Date.AddDays(request.Criteria.DaysBack * -1));


                    if (request.Criteria.UserId.HasValue)
                        query = query.Where(x => x.UserId == request.Criteria.UserId);

                    if (request.Criteria.Action.HasValue)
                    {
                        // Using UpdateApplicationStatusToNotApplicable to catch all updates to application status
                        query = request.Criteria.Action == ActionTypes.UpdateApplicationStatusToNotApplicable
                            ? query.Where(x => x.ActionType.Contains("UpdateApplicationStatusTo"))
                            : query.Where(x => x.ActionType == request.Criteria.Action.ToString());
                    }

                    switch (request.Criteria.OrderBy)
                    {
                        case Constants.SortOrders.ActivityUsernameAsc:
                            query = query.OrderBy(x => x.UserName);
                            break;

                        case Constants.SortOrders.ActivityUsernameDesc:
                            query = query.OrderByDescending(x => x.UserName);
                            break;

                        case Constants.SortOrders.ActivityDateAsc:
                            query = query.OrderBy(x => x.ActionedOn);
                            break;

                        default:
                            query = query.OrderByDescending(x => x.ActionedOn);
                            break;
                    }

                    var activities = query.Select(a => a.AsDto());

                    List<JobSeekerActivityActionViewDto> updateActivities;
                    switch (request.Criteria.FetchOption)
                    {
                        case CriteriaBase.FetchOptions.PagedList:
                            response.JobSeekerActivityActionViewPaged = new PagedList<JobSeekerActivityActionViewDto>(activities, request.Criteria.PageIndex, request.Criteria.PageSize);
                            updateActivities = response.JobSeekerActivityActionViewPaged;
                            break;

                        default:
                            updateActivities = activities.ToList();
                            break;
                    }

                    if (updateActivities.IsNotNullOrEmpty())
                    {
                        var automatedUserId = Repositories.Core.Users.Where(x => x.UserName == AppSettings.MessageBusUsername).Select(x => x.Id).FirstOrDefault();
                        foreach (var activity in updateActivities.Where(a => a.UserId == automatedUserId))
                        {
                            activity.UserName = Helpers.Localisation.Localise("Automated.Update", "Administrator, System");
                        }
                    }
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Gets the job seeker activity users.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public JobSeekerActivityActionViewResponse GetJobSeekerActivityUsers(JobSeekerActivityActionViewRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new JobSeekerActivityActionViewResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;
                try
                {
                    var views = Repositories.Core.JobSeekerActivityActionViews
                                                   .Where(x => x.JobSeekerId == request.Criteria.JobSeekerId)
                                                                             .Select(x => new UserView
                                                                             {
                                                                                 Id = x.UserId,
                                                                                 UserName = x.UserName
                                                                             })
                                                                             .Distinct()
                                                                             .OrderBy(x => x.UserName);

                    response.JobSeekerActivityActionViewUserList = views.ToList();
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }
                return response;
            }
        }

        /// <summary>
        /// Gets the job seekers and their issues.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public GetCanididatesAndIssuesResponse GetCandidatesAndIssues(GetCandidatesAndIssuesRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new GetCanididatesAndIssuesResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    var query = new CandidateIssueQuery(Repositories.Core, request.Criteria, AppSettings);

                    switch (request.Criteria.FetchOption)
                    {
                        case CriteriaBase.FetchOptions.PagedList:
                            response.CandidatesAndIssuesPaged = query.Query().GetPagedList(x => x.AsModel(), request.Criteria.PageIndex, request.Criteria.PageSize);
                            break;

                        case CriteriaBase.FetchOptions.List:
                            response.CandidatesAndIssues = query.Query().Select(x => x.AsDto()).ToList();
                            break;

                        case CriteriaBase.FetchOptions.Single:
                            response.CandidateAndIssues = query.QueryEntityId().AsDto();
                            break;
                    }
                }
                catch (Exception ex)
                {

                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }
                return response;
            }
        }

        /// <summary>
        /// Gets the candidate school status.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public GetStudentAlumniIssuesResponse GetStudentAlumniIssues(GetStudentAlumniIssuesRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new GetStudentAlumniIssuesResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    var query = new StudentAlumniIssuesQuery(Repositories.Core, request.Criteria, AppSettings);

                    switch (request.Criteria.FetchOption)
                    {
                        case CriteriaBase.FetchOptions.PagedList:
                            response.StudentAlumniIssuesPaged = query.Query().GetPagedList(x => x.AsDto(), request.Criteria.PageIndex, request.Criteria.PageSize);
                            break;

                        case CriteriaBase.FetchOptions.List:
                            response.StudentAlumniIssues = query.Query().Select(x => x.AsDto()).ToList();
                            break;

                        case CriteriaBase.FetchOptions.Single:
                            response.StudentsAlumniIssues = query.QueryEntityId().AsDto();
                            break;

                        default:
                            response.StudentAlumniIssuesPaged = query.Query().GetPagedList(x => x.AsDto(), request.Criteria.PageIndex, request.Criteria.PageSize);
                            break;
                    }
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Gets the open position jobs.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public CandidateOpenPositionMatchesResponse GetOpenPositionJobs(CandidateOpenPositionMatchesRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new CandidateOpenPositionMatchesResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    #region Get all the open positions for the candidate minus the ignored jobs

                    var query = Repositories.Core.OpenPositionMatchViews.Where(x => x.CandidateId == request.CandidateId && x.EmployerId == request.EmployerId);

                    var ignoredMatches = Repositories.Core.PersonPostingMatchesToIgnore.Where(x => x.UserId == request.UserContext.UserId).Select(x => x.PersonId + '|' + x.PostingId).ToList();

                    if (ignoredMatches.Any())
                        query = query.Where(x => !ignoredMatches.Contains(x.CandidateId + '|' + x.PostingId));

                    #endregion

                    response.CandidateOpenPositionMatches = query.OrderByDescending(x => x.JobScore).Select(x => x.AsDto()).ToList();
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Ignores the person to posting match.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public IgnorePersonPostingMatchResponse IgnorePersonPostingMatch(IgnorePersonPostingMatchRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new IgnorePersonPostingMatchResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    // Check if the match is already ignored
                    if (!Repositories.Core.PersonPostingMatchesToIgnore.Any(x => x.PersonId == request.PersonId && x.PostingId == request.PostingId && x.UserId == request.UserContext.UserId))
                    {
                        // No ignore entry so add it
                        var ignoreMatch = new PersonPostingMatchToIgnore { PersonId = request.PersonId, IgnoredOn = DateTime.Now, UserId = request.UserContext.UserId, PostingId = request.PostingId };

                        Repositories.Core.Add(ignoreMatch);

                        Repositories.Core.SaveChanges(true);
                    }

                    LogAction(request, ActionTypes.IgnorePersonPostingMatch, typeof(Person).Name, request.PersonId, request.PostingId, request.UserContext.UserId);
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Allows the person to posting match.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public AllowPersonPostingMatchResponse AllowPersonPostingMatch(AllowPersonPostingMatchRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new AllowPersonPostingMatchResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    // Check if the match is already ignored
                    var ignoreMatch = Repositories.Core.PersonPostingMatchesToIgnore.FirstOrDefault(x => x.PersonId == request.PersonId && x.PostingId == request.PostingId && x.UserId == request.UserContext.UserId);

                    if (ignoreMatch.IsNotNull())
                    {
                        Repositories.Core.Remove(ignoreMatch);

                        Repositories.Core.SaveChanges(true);

                        LogAction(request, ActionTypes.AllowPersonPostingMatch, typeof(Person).Name, request.PersonId, request.PostingId, request.UserContext.UserId);
                    }
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Registers action when an Assist user ran a search for jobs for a seeker
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public RegisterFindJobsForSeekerResponse RegisterFindJobsForSeeker(RegisterFindJobsForSeekerRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new RegisterFindJobsForSeekerResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    LogAction(request, ActionTypes.FindJobsForSeeker, typeof(Person).Name, request.CandidatePersonId, request.UserContext.UserId);
                    Helpers.SelfService.PublishSelfServiceActivity(ActionTypes.FindJobsForSeeker, request.UserContext.UserId, jobSeekerId: request.CandidatePersonId);
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Registers action when job seeker click how to apply.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public RegisterHowToApplyResponse RegisterHowToApply(RegisterHowToApplyRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new RegisterHowToApplyResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    // Convert lensPostingId to jobId
                    var posting = Repositories.Core.Postings
                        .Where(x => x.LensPostingId == request.LensPostingId)
                        .Select(x => new { x.OriginId, x.JobId })
                        .SingleOrDefault();

                    if (posting.IsNotNull())
                    {
                        var originId = posting.OriginId;
                        var jobId = posting.JobId;

                        if (originId.IsNotNull() && originId.IsTalentOrigin(AppSettings))
                            LogAction(request, ActionTypes.RegisterHowToApply, typeof(Job).Name, jobId, request.CandidatePersonId, request.UserContext.UserId);
                        else
                            LogAction(request, ActionTypes.RegisterHowToApplySpidered, typeof(Job).Name, jobId, request.CandidatePersonId, request.UserContext.UserId);
                    }
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Registers the self referral.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public RegisterSelfReferralResponse RegisterSelfReferral(RegisterSelfReferralRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new RegisterSelfReferralResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    var posting = Repositories.Core.Postings.SingleOrDefault(x => x.Id == request.PostingId).AsDto();
                    var originId = posting.OriginId;

                    // If OriginId > 998 then this is a spidered job.
                    if (originId.IsNull() || !originId.IsTalentOrigin(AppSettings))
                    {
                        if (!Repositories.Core.Referrals.Any(x => x.PostingId == request.PostingId && x.PersonId == request.CandidatePersonId))
                        {
                            var actionType = AppSettings.Module == FocusModules.Assist || RuntimeContext.CurrentRequest.UserContext.IsShadowingUser
                                                 ? ActionTypes.ExternalStaffReferral
                                                 : ActionTypes.ExternalReferral;

                            if (actionType == ActionTypes.ExternalStaffReferral)
                                Helpers.Candidate.SendStaffReferralEmail(request.CandidatePersonId, posting, request.UserContext);

                            CreateReferral(request, actionType, request.PostingId, request.CandidatePersonId, request.UserContext.UserId, request.ReferralScore);
                            Helpers.SelfService.PublishSelfServiceActivity(actionType, request.UserContext.ActionerId, request.UserContext.UserId, request.CandidatePersonId);
                            var integrationRequest = new ReferJobSeekerRequest
                            {
                                PostingId = posting.Id.GetValueOrDefault(),
                                PersonId = request.CandidatePersonId
                            };
                            if (RuntimeContext.IsNotNull() && RuntimeContext.CurrentRequest.IsNotNull() &&
                                RuntimeContext.CurrentRequest.UserContext.IsNotNull())
                            {
                                integrationRequest.ExternalAdminId = RuntimeContext.CurrentRequest.UserContext.ExternalUserId;
                                integrationRequest.ExternalOfficeId = RuntimeContext.CurrentRequest.UserContext.ExternalOfficeId;
                                integrationRequest.ExternalPassword = RuntimeContext.CurrentRequest.UserContext.ExternalPassword;
                                integrationRequest.ExternalUsername = RuntimeContext.CurrentRequest.UserContext.ExternalUserName;
                            }

                            Helpers.Messaging.Publish(new IntegrationRequestMessage
                            {
                                IntegrationPoint = IntegrationPoint.ReferJobSeeker,
                                ReferJobSeekerRequest = integrationRequest

                            });
                        }
                        else
                            response.AlreadyLogged = true;
                    }
                    else
                    {
                        if (!Repositories.Core.Referrals.Any(x => x.PostingId == request.PostingId && x.PersonId == request.CandidatePersonId))
                        {
                            var hasApplication = (from application in Repositories.Core.Applications
                                                  join resume in Repositories.Core.Resumes
                                                      on application.ResumeId equals resume.Id
                                                  where application.PostingId == request.PostingId
                                                        && resume.PersonId == request.CandidatePersonId
                                                  select application.Id).Any();

                            if (!hasApplication)
                            {
                                CreateReferral(request, ActionTypes.SelfReferral, request.PostingId, request.CandidatePersonId, request.UserContext.UserId, request.ReferralScore);
                                Helpers.SelfService.PublishSelfServiceActivity(ActionTypes.SelfReferral, request.UserContext.ActionerId, request.UserContext.UserId, request.CandidatePersonId);
                            }
                            else
                                response.AlreadyLogged = true;
                        }
                    }

                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Creates a referral (with no associated application)
        /// </summary>
        /// <param name="request">The current service request</param>
        /// <param name="actionType">SelfReferral or ExternalReferral</param>
        /// <param name="postingId">The id of the posting being referred to</param>
        /// <param name="candidateId">The id of the job seeker</param>
        /// <param name="actionerId">The user id of the user making the referral</param>
        /// <param name="referralScore">The referral score.</param>
        private void CreateReferral(ServiceRequest request, ActionTypes actionType, long postingId, long candidateId, long actionerId, int referralScore)
        {
            var userId = Repositories.Core.Users.Where(u => u.PersonId == candidateId).Select(u => u.Id).FirstOrDefault();
            var actionTypeId = Helpers.Logging.GetActionType(actionType).Id;
            var referral = new Referral
            {
                PostingId = postingId,
                PersonId = candidateId,
                UserId = userId,
                ActionerId = actionerId,
                ReferralDate = DateTime.Now,
                ActionTypeId = actionTypeId,
                ReferralScore = referralScore
            };
            Repositories.Core.Add(referral);

            var postingOfInterestSent = Repositories.Core.Query<JobPostingOfInterestSent>().FirstOrDefault(x => x.PersonId == candidateId && x.PostingId == postingId);
            if (postingOfInterestSent.IsNotNull())
            {
                postingOfInterestSent.Applied = true;
            }

            Repositories.Core.SaveChanges(true);

            LogAction(request, actionType, typeof(Posting).Name, postingId, candidateId, actionerId);
        }

        /// <summary>
        /// Shows the not what your looking for.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public LogUIActionResponse ShowNotWhatYourLookingFor(LogUIActionRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new LogUIActionResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    LogAction(request, ActionTypes.ShowNotWhatYoureLookingFor, typeof(Person).Name, request.PersonId);
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Logs candidate viewing the job details.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public LogUIActionResponse ViewJobDetails(LogUIActionRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new LogUIActionResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.ClientTag | Validate.SessionId | Validate.License | Validate.AccessToken))
                    return response;

                try
                {
                    // Convert lensPostingId to jobId
                    var jobId = Repositories.Core.Postings.Where(x => x.LensPostingId == request.LensPostingId).Select(x => x.JobId).FirstOrDefault();

                    LogAction(request, ActionTypes.ViewJobDetails, typeof(Person).Name, request.PersonId, jobId);
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Updates the assigned staff member.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public UpdateAssignedStaffMemberResponse UpdateAssignedStaffMember(UpdateAssignedStaffMemberRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new UpdateAssignedStaffMemberResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    var staffMember = Repositories.Core.FindById<Person>(request.PersonId);

                    if (staffMember.IsNull())
                    {
                        response.SetFailure(FormatErrorMessage(request, ErrorTypes.PersonNotFound));
                        Logger.Info(request.LogData(), string.Format("The staff member '{0}' was not found.", request.PersonId));

                        return response;
                    }

                    var jobSeekers = Repositories.Core.Persons.Where(x => request.JobSeekerIds.Contains(x.Id)).ToList();

                    if (jobSeekers.Count < request.JobSeekerIds.Count)
                    {
                        var jobSeekerIds = jobSeekers.Select(x => x.Id).ToList();

                        var jobSeekersNotFound = request.JobSeekerIds.Except(jobSeekerIds);
                        var jobSeekersNotFoundString = string.Join(", ", jobSeekersNotFound);

                        response.SetFailure(FormatErrorMessage(request, ErrorTypes.PersonNotFound));
                        Logger.Info(request.LogData(), string.Format("The job seekers '{0}' were not found.", jobSeekersNotFoundString));

                        return response;
                    }

                    jobSeekers.ForEach(j => j.AssignedToId = request.PersonId);

                    if (AppSettings.IntegrationClient != IntegrationClient.Standalone)
                    {
                        var adminExternalId = staffMember.User.ExternalId;
                        var jobSeekerUserIds = Repositories.Core.Users.Where(js => request.JobSeekerIds.Contains(js.PersonId)).Select(js => js.ExternalId).ToList();

                        jobSeekerUserIds.ForEach(externalId => Helpers.Messaging.Publish(new IntegrationRequestMessage
                        {
                            ActionerId = request.UserContext.UserId,
                            IntegrationPoint = IntegrationPoint.AssignAdminToJobSeeker,
                            AssignAdminToJobSeekerRequest = request.ToAssignAdminToJobSeekerRequest(externalId, adminExternalId)
                        }));
                    }

                    Repositories.Core.SaveChanges();

                    jobSeekers.ForEach(j => LogAction(request, ActionTypes.UpdateJobSeekerAssignedStaffMember, typeof(Person).Name, j.Id, request.UserContext.UserId, request.PersonId));
                }

                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        public RegisterJobPostingOfInterestSentResponse RegisterJobPostingOfInterestSent(RegisterJobPostingOfInterestSentRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new RegisterJobPostingOfInterestSentResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    var posting = Repositories.Core.Postings
                                                     .Where(x => x.LensPostingId == request.LensPostingId)
                                                     .Select(x => new
                                                       {
                                                           x.JobId,
                                                           x.Id
                                                       })
                                                                                 .SingleOrDefault();

                    if (posting.IsNotNull())
                    {
                        var existingRecord = Repositories.Core.Query<JobPostingOfInterestSent>().FirstOrDefault(j => j.PersonId == request.CandidatePersonId && j.PostingId == posting.Id);
                        if (existingRecord.IsNull())
                        {
                            Repositories.Core.Add(new JobPostingOfInterestSent
                            {
                                JobId = posting.JobId,
                                PostingId = posting.Id,
                                PersonId = request.CandidatePersonId,
                                Score = request.Score,
                                IsRecommendation = request.IsRecommendation,
                                QueueRecommendation = request.QueueRecommendation,
                                CreatedBy = request.CreatedBy
                            });
                        }
                        else
                        {
                            if (request.IsRecommendation && !existingRecord.IsRecommendation.GetValueOrDefault(false))
                            {
                                existingRecord.IsRecommendation = true;
                            }
                            if (request.QueueRecommendation && !existingRecord.QueueRecommendation.GetValueOrDefault(false))
                            {
                                existingRecord.QueueRecommendation = true;
                            }
                        }
                        Repositories.Core.SaveChanges(true);

                        if (request.QueueRecommendation && (existingRecord.IsNull() || !existingRecord.QueueRecommendation.GetValueOrDefault(false)))
                        {
                            var person = Repositories.Core.Persons.FirstOrDefault(p => p.Id == request.CandidatePersonId);
                            if (person.IsNotNull())
                            {
                                var jobSeekerName = string.Concat(person.FirstName, " ", person.LastName);
                                Helpers.Logging.LogAction(ActionTypes.InviteJobSeekerToApply, typeof(Job).Name, posting.JobId, person.Id, additionalDetails: jobSeekerName, entityIdAdditional02: 1);
                            }
                        }
                    }
                }

                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Gets the postings of interest received.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public GetPostingsOfInterestReceivedResponse GetPostingsOfInterestReceived(GetPostingsOfInterestReceivedRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new GetPostingsOfInterestReceivedResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    var personId = request.PersonId ?? request.UserContext.PersonId;
                    var query = Repositories.Core.Query<JobPostingOfInterestSentView>().Where(x => x.PersonId == personId);
                    if (request.LensPostingId.IsNotNullOrEmpty())
                    {
                        query = query.Where(x => x.LensPostingId == request.LensPostingId && x.SentOn <= DateTime.Now);
                        response.PostingSent = query.Select(x => x.AsDto()).FirstOrDefault();
                    }
                    else
                    {
                        var startdate = request.NumberOfDays == 0 ? new DateTime(2000, 1, 1) : DateTime.Now.Date.AddDays(0 - request.NumberOfDays);
                        query = query.Where(x => x.SentOn >= startdate && x.Applied == false).OrderByDescending(x => x.SentOn);
                        response.PostingsSent = query.Select(x => x.AsDto()).ToList();
                        //FVN-6011
                        if (response.PostingsSent.IsNotNullOrEmpty())
                        {
                            var queuedInvitees = Repositories.Core.InviteToApply.Where(i => i.PersonId == personId && (i.QueueInvitation ?? false)).Select(invitees => invitees.LensPostingId).ToList();
                            response.PostingsSent.RemoveAll(p => queuedInvitees.Contains(p.LensPostingId));
                            var recommendedPostings = Repositories.Core.Query<JobPostingOfInterestSent>().Where(x => x.PersonId == personId && (x.QueueRecommendation ?? false)).Select(r => r.Id).ToList();
                            response.PostingsSent.RemoveAll(p => recommendedPostings.Contains(p.Id ?? 0));
                        }
                    }
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Marks the application viewed. This should only be called when a Talent user views the application
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public MarkApplicationViewedResponse MarkApplicationViewed(MarkApplicationViewedRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new MarkApplicationViewedResponse();

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    // Get application view record using request object parameters (These aren't available in the application object so use the view)
                    var applicationView =
                        Repositories.Core.ApplicationViews.SingleOrDefault(
                            x => x.JobId == request.JobId && x.CandidateId == request.CandidateId);

                    // If for some reason we don't have a record leave now
                    if (applicationView.IsNull()) return response;

                    // Get actual application record
                    var application = Repositories.Core.FindById<Application>(applicationView.Id);
                    // Update
                    application.Viewed = true;
                    //Save
                    Repositories.Core.SaveChanges();

                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Gets the referral action type view.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public NewApplicantReferralViewResponse GetNewApplicantReferralView(NewApplicantReferralViewRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new NewApplicantReferralViewResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    switch (request.Criteria.FetchOption)
                    {
                        case CriteriaBase.FetchOptions.PagedList:
                            var referrals = Repositories.Core.NewApplicantReferralViews;

                            if (request.Criteria.PersonId.IsNotNull() && request.Criteria.PersonId > 0)
                            {
                                if (request.Criteria.ExcludeVeteranPriorityJobs)
                                    referrals = (from r in referrals
                                                 where r.PersonId == request.Criteria.PersonId
                                                       && (r.IsVeteran == true || r.VeteranPriorityEndDate == null || r.VeteranPriorityEndDate < DateTime.Now)
                                                 select r);
                                else
                                    referrals = (from r in referrals
                                                 where r.PersonId == request.Criteria.PersonId
                                                 select r);
                            }

                            response.NewApplicantReferralViewPaged = new PagedList<NewApplicantReferralViewDto>(referrals.Select(x => x.AsDto()), referrals.Count(), request.Criteria.PageIndex, request.Criteria.PageSize);
                            break;
                    }
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Gets the latest job seeker survey.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public JobSeekerSurveyResponse GetLatestSurvey(JobSeekerSurveyRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new JobSeekerSurveyResponse(request);

                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    if (request.PersonId.IsNotNull())
                    {
                        var latestSurvey = Repositories.Core.JobSeekerSurveys
                            .Where(survey => survey.PersonId == request.PersonId)
                            .OrderByDescending(survey => survey.CreatedOn)
                            .FirstOrDefault();

                        response.JobSeekerSurvey = latestSurvey.IsNull() ? null : latestSurvey.AsDto();
                    }
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Saves the job seeker survey.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public JobSeekerSurveyResponse SaveJobSeekerSurvey(JobSeekerSurveyRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new JobSeekerSurveyResponse(request);

                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    if (request.JobSeekerSurvey.IsNotNull())
                    {
                        var survey = request.JobSeekerSurvey.CopyTo();
                        Repositories.Core.Add(survey);

                        if (AppSettings.PositiveFeedbackCheckEnabled || AppSettings.NegativeFeedbackCheckEnabled)
                        {
                            if (request.JobSeekerSurvey.PersonId > 0)
                            {
                                // Get repository issue record for this person (a new one is created and added to the repository if one does not exist).
                                var issue = GetCandidateIssues(request.JobSeekerSurvey.PersonId);

                                if ((request.JobSeekerSurvey.SatisfactionLevel == SatisfactionLevels.SomewhatSatisfied || request.JobSeekerSurvey.SatisfactionLevel == SatisfactionLevels.VerySatisfied)
                                    || request.JobSeekerSurvey.UnanticipatedMatches.GetValueOrDefault() || request.JobSeekerSurvey.WasInvitedDidApply.GetValueOrDefault())
                                    issue.GivingPositiveFeedback = true;

                                if ((request.JobSeekerSurvey.SatisfactionLevel == SatisfactionLevels.SomewhatDissatisfied || request.JobSeekerSurvey.SatisfactionLevel == SatisfactionLevels.VeryDissatisfied)
                                    || !request.JobSeekerSurvey.UnanticipatedMatches.GetValueOrDefault() || !request.JobSeekerSurvey.WasInvitedDidApply.GetValueOrDefault())
                                    issue.GivingNegativeFeedback = true;
                            }
                        }

                        Repositories.Core.SaveChanges();
                        LogAction(request, ActionTypes.SaveJobSeekerSurvey, typeof(JobSeekerSurvey).Name, survey.Id, survey.PersonId);

                        response.JobSeekerSurvey = survey.AsDto();
                    }
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Gets the referral view.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public GetReferralViewResponse GetReferralViews(GetReferralViewRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new GetReferralViewResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    var referrals = Repositories.Core.ReferralViews.Where(r => r.CandidateId == request.PersonId);

                    if (request.PostingId.HasValue)
                        referrals = referrals.Where(r => r.Id == request.PostingId);

                    if (request.DayLimit.HasValue)
                    {
                        var dateRange = DateTime.Now.Date.AddDays(-30);
                        referrals = referrals.Where(r => r.JobClosingOn >= dateRange || (r.JobClosingOn == null && r.PostingCreatedOn >= dateRange));
                    }

                    if (!request.IgnoreVeteranPriorityService && AppSettings.VeteranPriorityServiceEnabled)
                        referrals = referrals.Where(r => r.VeteranPriorityEndDate == null || r.VeteranPriorityEndDate <= DateTime.Now || r.IsVeteran == true);

                    referrals = referrals.OrderByDescending(r => r.ApplicationDate);

                    if (request.NumberOfReferrals.HasValue)
                        referrals = referrals.Take(request.NumberOfReferrals.Value);

                    response.ReferralViews = referrals.Select(r => r.AsDto()).ToList();
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Gets the campuses.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public GetCampusesResponse GetCampuses(GetCampusesRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new GetCampusesResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.ClientTag | Validate.License | Validate.AccessToken))
                    return response;

                try
                {
                    var campuses = Repositories.Core.Campuses;

                    if (request.CampusId.HasValue)
                    {
                        response.Campus = campuses.Where(campus => campus.Id == request.CampusId).Select(campus => campus.AsDto()).FirstOrDefault();
                    }
                    else
                    {
                        if (!AppSettings.SupportsProspectiveStudents)
                            campuses = campuses.Where(campus => !campus.IsNonCampus);

                        response.Campuses = campuses.Select(campus => campus.AsDto()).ToList();
                    }
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Gets the UI claimant status.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public UiClaimantStatusResponse GetUiClaimantStatus(UiClaimantStatusRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new UiClaimantStatusResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.ClientTag | Validate.License | Validate.AccessToken))
                    return response;

                try
                {
                    var person = Repositories.Core.FindById<Person>(request.PersonId);

                    var isClaimant = IsUiClaimant(request.CheckExternalSystem ? Repositories.Integration : null, person);
                    response.IsUiClaimant = isClaimant;

                    // Only update if the value has changed
                    if (person.IsUiClaimant != isClaimant)
                    {
                        person.IsUiClaimant = isClaimant;

                        Repositories.Core.SaveChanges();
                        LogAction(request, ActionTypes.SaveUiClaimantStatus, typeof(Person).Name, person.Id);
                    }
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Checks if a job seeker is a UI Claimant
        /// </summary>
        /// <param name="integrationRepositories">The integration repository</param>
        /// <param name="person">The job seeker entity</param>
        /// <returns>A boolean indicating if they are a claimant</returns>
        internal static bool IsUiClaimant(IIntegrationRepository integrationRepositories, Person person)
        {
            var savedStatus = person.IsUiClaimant.GetValueOrDefault(false);
            if (integrationRepositories.IsNull())
                return savedStatus;

            var isUiClaimantRequest = new IsUIClaimantRequest { JobSeekerId = person.Id, ExternalJobSeekerId = person.User.ExternalId, SocialSecurityNumber = person.SocialSecurityNumber };
            var isUiClaimantResponse = integrationRepositories.IsUIClaimant(isUiClaimantRequest);

            return isUiClaimantResponse.Outcome == IntegrationOutcome.Success
                ? isUiClaimantResponse.IsUIClaimant
                : savedStatus;
        }

        /// <summary>
        /// Gets the invitees for job.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>InviteesForJobResponse.</returns>
        public InviteesForJobResponse GetInviteesForJob(InviteesForJobRequest request)
        {
            var response = new InviteesForJobResponse(request);

            if (!ValidateRequest(request, response, Validate.All))
                return response;

            try
            {
                if (request.InviteesCriteria.JobId.IsNotNull() && request.InviteesCriteria.JobId > 0)
                {
                    response.InviteesResumeViews = RuntimeContext.Helpers.Candidate.GetInviteesForJob(request.InviteesCriteria);

                    if (response.InviteesResumeViews.Any())
                    {
                        var flaggedCandidates = Repositories.Core.PersonListCandidateViews
                            .Where(x => x.PersonId == request.UserContext.PersonId && x.ListType == ListTypes.FlaggedJobSeeker)
                            .ToList();

                        response.InviteesResumeViews.ForEach(resumeView => GetOtherRequiredCandidateViewInformation(resumeView, flaggedCandidates, request.UserContext.EmployerId));
                    }
                }
                else
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.ArgumentInvalid));
                    Logger.Info(request.LogData(), "Job Id cannot be null or 0.");
                }
            }
            catch (Exception)
            {
                response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown));
                Logger.Error(request.LogData(), response.Message, response.Exception);
            }

            return response;
        }

        /// <summary>
        /// Determines whether [is candidate exisiting job invitee] [the specified request].
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        /// <exception cref="System.ArgumentNullException">
        /// jobId;@Job ID required
        /// or
        /// candidateId;@Candidate ID required
        /// </exception>
        public IsCandidateExistingInviteeOrApplicantResponse IsCandidateExisitingJobInviteeOrApplicant(IsCandidateExistingInviteeOrApplicantRequest request)
        {
            var response = new IsCandidateExistingInviteeOrApplicantResponse(request);

            if (!ValidateRequest(request, response, Validate.All))
                return response;

            // Validate parameters in request
            // ReSharper disable NotResolvedInText
            if (request.IsCandidateInviteeCriteria.JobId.IsNull())
                throw new ArgumentNullException("jobId", @"Job ID required");

            if (request.IsCandidateInviteeCriteria.CandidateId.IsNull())
                throw new ArgumentNullException("candidateId", @"Candidate ID required");

            if (request.IsCandidateInviteeCriteria.JobId < 1)
                throw new ArgumentOutOfRangeException("jobId", @"Job Id cannot be 0");

            if (request.IsCandidateInviteeCriteria.CandidateId < 1)
                throw new ArgumentOutOfRangeException("candidateId", @"Candidate Id cannot be 0");
            // ReSharper enable NotResolvedInText

            try
            {
                var jobId = request.IsCandidateInviteeCriteria.JobId;
                var candidateId = request.IsCandidateInviteeCriteria.CandidateId;

                var jobInviteesCriteria = new InviteesResumeViewCriteria { JobId = jobId };

                // Get the invitees for the job.
                var invitees = RuntimeContext.Helpers.Candidate.GetInviteesForJob(jobInviteesCriteria);

                // Check if they are a current invitee
                var isCandidateExisitngJobInvitee = invitees.Any(x => x.Id == candidateId);

                response.IsCandidateExistingJobInviteeOrApplicant = isCandidateExisitngJobInvitee;

                if (!isCandidateExisitngJobInvitee)
                {
                    //check applications
                    var existingApplication = Repositories.Core.ApplicationBasicViews.FirstOrDefault(x => x.JobId == jobId && x.CandidateId == candidateId);
                    response.IsCandidateExistingJobInviteeOrApplicant = existingApplication.IsNotNull();

                    if (response.IsCandidateExistingJobInviteeOrApplicant)
                    {
                        var application = Repositories.Core.FindById<Application>(existingApplication == null ? 0 : existingApplication.Id);
                        response.IsCandidateExistingApplicantToScreenedJobPendingApproval =
                                application.ApprovalStatus.IsIn(ApprovalStatuses.WaitingApproval, ApprovalStatuses.Reconsider)
                                && application.Posting.Job.ScreeningPreferences == ScreeningPreferences.JobSeekersMustBeScreened;
                    }
                    else
                    {
                        response.IsCandidateExistingApplicantToScreenedJobPendingApproval = false;
                    }
                }
            }
            catch (Exception)
            {
                response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown));
                Logger.Error(request.LogData(), response.Message, response.Exception);
            }
            return response;
        }

        public bool IsCandidateInvitedToApplyForThisJob(long jobId, long personId)
        {
            try
            {

                return RuntimeContext.Helpers.Candidate.IsCandidateInvitedToApplyForThisJob(jobId, personId);

            }
            catch (Exception)
            {

                return false;
            }
        }

        /// <summary>
        /// Checks if this candidate has been invited to apply for specified job
        /// </summary>
        /// <param name="jobId"></param>
        /// <param name="personId"></param>
        /// <returns></returns>
        public bool IsCandidateInvitedForThisJob(long jobId, long personId)
        {
            try
            {
                return Repositories.Core.InviteToApplyBasicViews.FirstOrDefault(x => x.JobId == jobId && x.PersonId == personId).IsNotNull();
            }
            catch (Exception)
            {
                return false;
            }

        }

        /// <summary>
        /// Sets the MSFW verified status.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>VerifyMSFWStatusResponse.</returns>
        public VerifyMSFWStatusResponse VerifyMSFWStatus(VerifyMSFWStatusRequest request)
        {
            var response = new VerifyMSFWStatusResponse(request);

            if (!ValidateRequest(request, response, Validate.All))
                return response;

            try
            {
                var person = Repositories.Core.FindById<Person>(request.PersonId);
                person.MigrantSeasonalFarmWorkerVerified = request.IsVerified;

                var resumeQuery = Repositories.Core.Resumes.Where(r => r.PersonId == request.PersonId && r.StatusId == ResumeStatuses.Active);
                if (request.IsVerified)
                    resumeQuery = resumeQuery.Where(r => r.IsDefault && r.ResumeCompletionStatusId == ResumeCompletionStatuses.Completed);

                var resumes = resumeQuery.ToList();

                resumes.ForEach(resume =>
                {
                    var resumeModel = resume.ResumeXml.ToResumeModel(RuntimeContext, request.UserContext.Culture);
                    if (resumeModel.ResumeContent.IsNotNull())
                    {
                        var profile = resumeModel.ResumeContent.Profile;
                        if (profile.IsNotNull())
                        {
                            if (request.IsVerified)
                            {
                                profile.MSFWQuestions = request.MSFWQuestions;
                            }
                            else
                            {
                                profile.IsMSFW = false;
                                if (profile.MSFWQuestions.IsNotNullOrEmpty())
                                    profile.MSFWQuestions.Clear();
                            }
                            resume.ResumeXml = resumeModel.ToResumeDto(RuntimeContext, request.UserContext.Culture, AppSettings.DefaultCountryKey).ResumeXml;
                            resume.MigrantSeasonalFarmWorker = request.IsVerified;

                            LogAction(request, ActionTypes.SaveResume, resume);
                        }
                    }
                });

                ResolveCandidateIssue(request, request.PersonId, request.UserContext.UserId, new List<CandidateIssueType> { CandidateIssueType.PotentialMSFW });

                Repositories.Core.SaveChanges(true);
            }
            catch (Exception ex)
            {
                response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                Logger.Error(request.LogData(), response.Message, response.Exception);
            }

            return response;
        }

        #region Helper Methods

        /// <summary>
        /// Gets the compiled transform for an XSLT file name
        /// </summary>
        /// <param name="xsltName">Name of the XSLT.</param>
        /// <returns>The XSLT compiled transform</returns>
        /// <exception cref="System.Exception">Unable to get ResumeText xslt</exception>
        private XslCompiledTransform GetXsltTransform(string xsltName)
        {
            var xslTransform = new XslCompiledTransform();

            try
            {

                var resourceName = string.Concat("Focus.Services.Assets.Xslts.", xsltName);

                var assembly = Assembly.GetExecutingAssembly();
                var stream = assembly.GetManifestResourceStream(resourceName);

                if (stream == null)
                    throw new Exception("Unable to get ResumeText xslt");

                var reader = new StreamReader(stream);
                var xmlReader = XmlReader.Create(reader);

                xslTransform.Load(xmlReader);

                xmlReader.Close();
                reader.Close();
                stream.Close();

            }
            catch
            {
                // ignored
            }

            return xslTransform;
        }

        /// <summary>
        /// Transforms the specified XML.
        /// </summary>
        /// <param name="xml">The XML.</param>
        /// <param name="xslTransform">The compiled xslt transform.</param>
        /// <param name="arguments">The arguments</param>
        /// <returns></returns>
        private string Transform(string xml, XslCompiledTransform xslTransform, XsltArgumentList arguments)
        {
            var stringWriter = new StringWriter();

            try
            {
                var xmlDocument = new XmlDocument { PreserveWhitespace = true };
                xmlDocument.LoadXml(xml);

                if (xmlDocument.IsNotNull() && xmlDocument.DocumentElement.IsNotNull())
                {
                    var languageProficiencyNodes = xmlDocument.DocumentElement.SelectNodes("resume/skills/languages/language/proficiency");
                    if (languageProficiencyNodes.IsNotNull() && languageProficiencyNodes.Count > 0)
                    {
                        var proficiencyLookup = Helpers.Lookup.GetLookup(LookupTypes.LanguageProficiencies).ToDictionary(l => l.ExternalId, l => l.Text);
                        foreach (XmlElement proficienyNode in languageProficiencyNodes)
                        {
                            if (!proficienyNode.HasChildNodes)
                            {
                                proficienyNode.AppendChild(xmlDocument.CreateTextNode(""));
                            }

                            var externalId = proficienyNode.FirstChild.Value;
                            var proficienyText = proficiencyLookup.ContainsKey(externalId) ? proficiencyLookup[externalId] : "";
                            if (proficienyText.Length > 0)
                            {
                                var checkPrefix = string.Concat(externalId, " - ");
                                if (proficienyText.StartsWith(checkPrefix))
                                {
                                    proficienyText = proficienyText.Substring(checkPrefix.Length);
                                }
                                proficienyText = string.Concat(" at ", proficienyText.ToLower());
                            }
                            proficienyNode.FirstChild.Value = proficienyText;
                        }
                    }
                }

                xslTransform.Transform(xmlDocument.CreateNavigator(), arguments, stringWriter);
            }
            catch
            {
                // ignored
            }

            return stringWriter.ToString();
        }

        /// <summary>
        /// Gets the xslt argument list
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        private XsltArgumentList GetXsltArguments(ServiceRequest request)
        {
            var argsList = new XsltArgumentList();
            argsList.AddParam("ProfessionalSummary", "", LocaliseOrDefault(request, "ProfessionalSummary", "Professional Summary"));
            argsList.AddParam("TechnicalSkills", "", LocaliseOrDefault(request, "ResumeSectionType.TechnicalSkills.NoEdit", "Technical Skills"));
            argsList.AddParam("Skills", "", LocaliseOrDefault(request, "Skills", "Skills"));
            argsList.AddParam("Experience", "", LocaliseOrDefault(request, "Experience", "Experience"));
            argsList.AddParam("Education", "", LocaliseOrDefault(request, "Education", "Education"));
            argsList.AddParam("MilitaryService", "", LocaliseOrDefault(request, "MilitaryService", "Military Service"));
            argsList.AddParam("Licenses", "", LocaliseOrDefault(request, "Licenses", "Licenses"));
            argsList.AddParam("Certifications", "", LocaliseOrDefault(request, "Certifications", "Certifications"));
            argsList.AddParam("Languages", "", LocaliseOrDefault(request, "Languages", "Languages"));
            argsList.AddParam("VolunteerActivities", "", LocaliseOrDefault(request, "ResumeSectionType.VolunteerActivities.NoEdit", "Volunteer Activities"));
            argsList.AddParam("Affiliations", "", LocaliseOrDefault(request, "ResumeSectionType.Affiliations.NoEdit", "Affiliations"));
            argsList.AddParam("Honors", "", LocaliseOrDefault(request, "ResumeSectionType.Honors.NoEdit", "Honors"));
            argsList.AddParam("ProfessionalDevelopment", "", LocaliseOrDefault(request, "ResumeSectionType.ProfessionalDevelopment.NoEdit", "Professional Development"));
            argsList.AddParam("Interests", "", LocaliseOrDefault(request, "ResumeSectionType.Interests.NoEdit", "Interests"));
            argsList.AddParam("Personal", "", LocaliseOrDefault(request, "Personal", "Personal"));
            argsList.AddParam("References", "", LocaliseOrDefault(request, "ResumeSectionType.References.NoEdit", "References"));
            argsList.AddParam("Internships", "", LocaliseOrDefault(request, "ResumeSectionType.Internships.NoEdit", "Internships"));
            argsList.AddParam("Objectives", "", LocaliseOrDefault(request, "ResumeSectionType.Objective.NoEdit", "Objectives"));
            argsList.AddParam("PersonalInformation", "", LocaliseOrDefault(request, "ResumeSectionType.Personals.NoEdit", "Personal Information"));
            argsList.AddParam("Publications", "", LocaliseOrDefault(request, "ResumeSectionType.Publications.NoEdit", "Publications"));

            return argsList;
        }

        /// <summary>
        /// Gets the star ratings and sort.
        /// </summary>
        /// <param name="candidateViews">The candidate views.</param>
        /// <param name="sortOrder">The sort order.</param>
        /// <param name="excludeNonVeterans">Whether to exclude applications queued for non-veterans (for non-veteran jobs only).</param>
        /// <returns></returns>
        private List<ResumeView> GetStarRatingsAndSort(List<ResumeView> candidateViews, string sortOrder, bool excludeNonVeterans)
        {
            if (excludeNonVeterans)
                candidateViews = candidateViews
                    .Where(candidate => candidate.IsVeteran || candidate.ApplicationVeteranPriorityEndDate.GetValueOrDefault(DateTime.MinValue) <= DateTime.Now)
                    .ToList();

            foreach (var candidateView in candidateViews)
            {
                candidateView.StarRating = candidateView.Score.ToStarRating(AppSettings.StarRatingMinimumScores);
            }

            #region Sort

            var orderBy = (sortOrder.IsNotNullOrEmpty()) ? sortOrder : "PoolResultSortBys.Score";

            switch (orderBy.ToLower())
            {
                case "poolresultsortbys.status":
                    candidateViews = candidateViews.OrderBy(x => x.Status).ThenByDescending(x => x.IsVeteran).ThenByDescending(x => x.Score).ToList();
                    break;
                case "poolresultsortbys.applicationreceivedon":
                    candidateViews = candidateViews.OrderByDescending(x => x.ApplicationReceivedOn).ThenByDescending(x => x.IsVeteran).ThenByDescending(x => x.Score).ToList();
                    break;
                default:
                    candidateViews = candidateViews.OrderByDescending(x => x.StarRating).ThenByDescending(x => x.IsVeteran).ThenByDescending(x => x.Score).ToList();
                    break;
            }

            #endregion

            return candidateViews;
        }

        /// <summary>
        /// Gets the job seeker issue.
        /// </summary>
        /// <param name="personId">The person id.</param>
        /// <returns></returns>
        private Issues GetCandidateIssues(long personId)
        {
            // Get repository issue record for this person
            var issue = Repositories.Core.Query<Issues>().SingleOrDefault(x => x.PersonId == personId);
            // If we don't have one, create one and add to repository
            if (issue.IsNull())
            {
                issue = new Issues { PersonId = personId };
                Repositories.Core.Add(issue);
            }
            return issue;
        }

        /// <summary>
        /// Assigns the activity.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="response">The response.</param>
        /// <returns></returns>
        private AssignActivityOrActionResponse AssignActivity(AssignActivityOrActionRequest request, AssignActivityOrActionResponse response)
        {
            try
            {
                if (request.PersonId == 0)
                {
                    response.SetFailure(ErrorTypes.PersonDetailsNotSupplied, FormatErrorMessage(request, ErrorTypes.PersonDetailsNotSupplied));
                    Logger.Info(request.LogData(), "No person ID provided.");
                    return response;
                }

                var person = Repositories.Core.FindById<Person>(request.PersonId);

                if (person.IsNull())
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.PersonNotFound));
                    Logger.Info(request.LogData(), string.Format("The person '{0}' was not found.", request.PersonId));

                    return response;
                }

                var activityUserId = person.User.Id;

                var activity = Repositories.Configuration.FindById<Activity>(request.ActivityId);
                if (activity.IsNull())
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.ActivityNotFound));
                    Logger.Info(request.LogData(), string.Format("The activity '{0}' was not found.", request.ActivityId));

                    return response;
                }

                //// validate office
                //var userOffice = Repositories.Core.PersonOfficeMappers.Where(x => x.PersonId == request.PersonId).ToList();//.Select(mapper => mapper.OfficeId).Distinct();

                //var staffOffice = Repositories.Core.PersonOfficeMappers.Where(x => x.PersonId == request.UserContext.PersonId).ToList();
                //var staffOfficeCount = staffOffice.Count();
                //bool assighFlag = false;
                //if (staffOffice.Count > 0)
                //{
                //    if (staffOfficeCount == 1)
                //    {
                //        var isStateWide = staffOffice.Select(x => x.StateId).FirstOrDefault();
                //        var isOffice = staffOffice.Select(x => x.OfficeId).FirstOrDefault();
                //        if (isStateWide.IsNotNull() && isOffice.IsNull())
                //            assighFlag = true;
                //    }
                //    if (!assighFlag)
                //    {
                //        var isAssignedOffice = staffOffice.Where(x => userOffice.Any(y => y.OfficeId == x.OfficeId)).ToList();
                //        var isStaffcount = isAssignedOffice.Count();
                //        if (isAssignedOffice.Any())
                //            assighFlag = true;
                //    }

                //}

                //if (!assighFlag)
                //{
                //    //response.SetFailure(FormatErrorMessage(request, ErrorTypes.ValidationFailed));
                //    Logger.Info(request.LogData(), string.Format("The staff '{0}' do not have the permission.", request.UserContext.PersonId));
                    
                //    return response;

                //}


                if (AppSettings.IntegrationClient != IntegrationClient.Standalone && !request.IgnoreClient)
                {
                    var integrationRequest = new AssignJobSeekerActivityRequest
                    {
                        UserId = request.UserContext.UserId,
                        PersonId = person.Id,
                        JobSeekerId = person.User.ExternalId,
                        ActivityId = activity.ExternalId.ToString(CultureInfo.InvariantCulture),
                        ActivityUserId = activityUserId,
                        IsSelfAssigned = request.IsSelfAssigned,
                        SelfServicedActivity = request.IsSelfAssigned,
                        ActionerId = request.UserContext.UserId,
                        IsManual = true
                    };

                    if (!request.IsSelfAssigned && RuntimeContext.IsNotNull() && RuntimeContext.CurrentRequest.IsNotNull() && RuntimeContext.CurrentRequest.UserContext.IsNotNull())
                    {
                        integrationRequest.ExternalAdminId = RuntimeContext.CurrentRequest.UserContext.ExternalUserId;
                        integrationRequest.ExternalOfficeId = RuntimeContext.CurrentRequest.UserContext.ExternalOfficeId;
                        integrationRequest.ExternalPassword = RuntimeContext.CurrentRequest.UserContext.ExternalPassword;
                        integrationRequest.ExternalUsername = RuntimeContext.CurrentRequest.UserContext.ExternalUserName;
                    }

                    Helpers.Messaging.Publish(new IntegrationRequestMessage
                    {
                        ActionerId = request.UserContext.UserId,
                        IntegrationPoint = IntegrationPoint.AssignJobSeekerActivity,
                        AssignJobSeekerActivityRequest = integrationRequest
                    });
                }

                //// If candidate does not have a client id do not send it to client system
                //if (request.ClientId.IsNotNullOrEmpty() && !request.IgnoreClient)
                //{
                //  var integrationResponse = Repositories.Client.AssignJobSeekerActivity(request.ToIntegrationJobSeekerAssignActivity(activity.ExternalId));

                //  if (integrationResponse.Acknowledgement == AcknowledgementType.Failure)
                //  {
                //    response.SetFailure(FormatErrorMessage(request, ErrorTypes.IntegrationClientAssignJobSeekerActivityFailure), integrationResponse.Exception);
                //    Logger.Error(request.LogData(), integrationResponse.Message, integrationResponse.Exception);
                //    return response;
                //  }
                //}

                response.FocusId = LogAction(request, ActionTypes.AssignActivityToCandidate, typeof(Person).Name, person.Id, activity.Id, actionedOn: request.ActivityTime, overrideUserId: request.AssigningUserId, overrideCreatedOn: request.OverrideCreatedOn);
            }
            catch (Exception ex)
            {
                response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                Logger.Error(request.LogData(), response.Message, response.Exception);
            }

            return response;
        }

        private void SetContactDetails(JobSeekerProfileResponse jobSeekerProfileResponse, long personId, bool isPrimary)
        {
            var address = Repositories.Core.PersonAddresses.FirstOrDefault(x => x.PersonId == personId && x.IsPrimary == isPrimary);
            var phoneNumber = Repositories.Core.PhoneNumbers.FirstOrDefault(x => x.PersonId == personId && x.IsPrimary == isPrimary);

            jobSeekerProfileResponse.JobSeekerProfileView.AddressLine1 = (address != null ? address.Line1 : "");
            jobSeekerProfileResponse.JobSeekerProfileView.TownCity = (address != null ? address.TownCity : "");
            jobSeekerProfileResponse.JobSeekerProfileView.StateId = (address != null ? address.StateId : 1);
            jobSeekerProfileResponse.JobSeekerProfileView.PostcodeZip = (address != null ? address.PostcodeZip : "");
            jobSeekerProfileResponse.PrimaryPhone = new Phone
            {
                PhoneType = PhoneType.Home,
                PhoneNumber = (phoneNumber != null ? phoneNumber.Number : "")
            };
            //return jobSeekerProfileResponse;
        }

        #region Resolve Issues

        /// <summary>
        /// Resolves the login issue.
        /// </summary>
        /// <param name="issues">The issues.</param>
        private void ResolveLoginIssue(Issues issues)
        {
            issues.NoLoginResolvedDate = DateTime.Now;
            issues.NoLoginTriggered = false;
        }

        /// <summary>
        /// Resolves the job offer rejection issue.
        /// </summary>
        /// <param name="issues">The issues.</param>
        private void ResolveJobOfferRejectionIssue(Issues issues)
        {
            issues.JobOfferRejectionCount = 0;
            issues.JobOfferRejectionResolvedDate = DateTime.Now;
            issues.JobOfferRejectionTriggered = false;
        }

        /// <summary>
        /// Resolves the not reporting to interview issue.
        /// </summary>
        /// <param name="issues">The issues.</param>
        private void ResolveNotReportingToInterviewIssue(Issues issues)
        {
            issues.NotReportingToInterviewCount = 0;
            issues.NotReportingToInterviewResolvedDate = DateTime.Now;
            issues.NotReportingToInterviewTriggered = false;
        }

        /// <summary>
        /// Resolves the not clicking on leads issue.
        /// </summary>
        /// <param name="issues">The issues.</param>
        private void ResolveNotClickingOnLeadsIssue(Issues issues)
        {
            issues.NotClickingOnLeadsCount = 0;
            issues.NotClickingOnLeadsResolvedDate = DateTime.Now;
            issues.NotClickingOnLeadsTriggered = false;
        }

        /// <summary>
        /// Resolves the not responding to employer invites issue.
        /// </summary>
        /// <param name="issues">The issues.</param>
        private void ResolveNotRespondingToEmployerInvitesIssue(Issues issues)
        {
            issues.NotRespondingToEmployerInvitesCount = 0;
            issues.NotRespondingToEmployerInvitesResolvedDate = DateTime.Now;
            issues.NotRespondingToEmployerInvitesTriggered = false;
        }

        /// <summary>
        /// Resolves the showing low quality matches issue.
        /// </summary>
        /// <param name="issues">The issues.</param>
        private void ResolveShowingLowQualityMatchesIssue(Issues issues)
        {
            issues.ShowingLowQualityMatchesCount = 0;
            issues.ShowingLowQualityMatchesResolvedDate = DateTime.Now;
            issues.ShowingLowQualityMatchesTriggered = false;
        }

        /// <summary>
        /// Resolves the poor quality resume issue.
        /// </summary>
        /// <param name="personId"></param>
        /// <param name="issues">The issues.</param>
        private void ResolvePoorQualityResumeIssue(long personId, Issues issues)
        {
            issues.PostingLowQualityResumeCount = 0;
            issues.PostingLowQualityResumeResolvedDate = DateTime.Now;
            issues.PostingLowQualityResumeTriggered = false;

            var user = Repositories.Core.Users.FirstOrDefault(x => x.PersonId == personId);
            // Get message that hasn't been dismissed/resolved

            var messageId = Repositories.Core.Messages.Where(m => !m.DismissedMessages.Any() && m.UserId.Equals(user.Id)).Select(m => m.Id).FirstOrDefault();

            if (messageId.IsNotNull() && messageId != 0 && user.IsNotNull())
            {
                var dismissedMessage = new DismissedMessage
                {
                    MessageId = messageId,
                    UserId = user.Id
                };

                Repositories.Core.Add(dismissedMessage);
            }
        }

        /// <summary>
        /// Resolves the post hire follow up issue.
        /// </summary>
        /// <param name="issues">The issues.</param>
        private void ResolvePostHireFollowUpIssue(Issues issues)
        {
            issues.PostHireFollowUpResolvedDate = DateTime.Now;
            issues.PostHireFollowUpTriggered = false;
        }

        /// <summary>
        /// Resolves the follow up request issue.
        /// </summary>
        /// <param name="issues">The issues.</param>
        private void ResolveFollowUpRequestIssue(Issues issues)
        {
            issues.FollowUpRequested = false;
        }

        /// <summary>
        /// Resolves the inappropriate email issue.
        /// </summary>
        /// <param name="issues">The issues.</param>
        private void ResolveInappropriateEmailAddressIssue(Issues issues)
        {
            issues.InappropriateEmailAddress = false;
        }

        /// <summary>
        /// Resolves the giving positive feedback issue.
        /// </summary>
        /// <param name="issues">The issues.</param>
        private void ResolveGivingPositiveFeedbackIssue(Issues issues)
        {
            issues.GivingPositiveFeedback = false;
        }

        /// <summary>
        /// Resolves the giving negative feedback issue.
        /// </summary>
        /// <param name="issues">The issues.</param>
        private void ResolveGivingNegativeFeedbackIssue(Issues issues)
        {
            issues.GivingNegativeFeedback = false;
        }

        private void ResolveExpiredAlienCertificationIssue(Issues issues)
        {
            issues.HasExpiredAlienCertificationRegistration = false;
        }

        /// <summary>
        /// Resolves the not searching jobs issue.
        /// </summary>
        /// <param name="issues">The issues.</param>
        private void ResolveNotSearchingJobsIssue(Issues issues)
        {
            issues.NotSearchingJobsResolvedDate = DateTime.Now;
            issues.NotSearchingJobsTriggered = false;
        }


        /// <summary>
        /// Resolves the Potential MSFW issue.
        /// </summary>
        /// <param name="issues">The issues.</param>
        private void ResolvePotentialMSFWIssue(Issues issues)
        {
            issues.MigrantSeasonalFarmWorkerTriggered = false;
        }

        #endregion

        #endregion

        /// <summary>
        /// Validates the referral status reasons.
        /// </summary>
        /// <param name="reasons">The reasons.</param>
        /// <returns></returns>
        private ErrorTypes ValidateReferralStatusReasons(List<KeyValuePair<long, string>> reasons)
        {
            if (AppSettings.Theme == FocusThemes.Workforce)
            {
                if (reasons.IsNullOrEmpty())
                    return ErrorTypes.ReferralStatusReasonsNotSpecified;

                var otherReasonId = Helpers.Lookup.GetLookup(LookupTypes.JobSeekerDenialReasons).Where(x => x.Key.Equals("JobSeekerDenialReasons.Other")).Select(x => x.Id).FirstOrDefault();

                if (reasons.Any(x => x.Key.Equals(otherReasonId) && string.IsNullOrEmpty(x.Value)))
                    return ErrorTypes.ReferralStatusReasonsOtherReasonTextNotSpecified;

                if (reasons.Any(x => !x.Key.Equals(otherReasonId) && !string.IsNullOrEmpty(x.Value)))
                    return ErrorTypes.ReferralStatusReasonsOtherReasonTextNotValidWithSpecifiedReasonId;
            }

            return ErrorTypes.Ok;
        }

        /// <summary>
        /// Gets the push notification list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public GetPushNotificationListResponse GetPushNotificationList(GetPushNotificationListRequest request)
        {
            var response = new GetPushNotificationListResponse(request);

            if (!ValidateRequest(request, response, Validate.All))
                return response;

            try
            {
                if (request.DeviceToken.IsNotNullOrEmpty())
                {
                    var pushNotifications = (from pn in Repositories.Core.PushNotifications
                                             where pn.PersonMobileDeviceToken == request.DeviceToken
                                             orderby pn.CreatedOn descending
                                             select new PushNotificationDto
                                             {
                                                 Id = pn.Id,
                                                 Title = pn.Title,
                                                 Text = pn.Text,
                                                 Sent = pn.Sent,
                                                 PersonMobileDeviceToken = pn.PersonMobileDeviceToken,
                                                 CreatedOn = pn.CreatedOn
                                             }).ToList();

                    response.PushNotifications = pushNotifications;
                }
                else
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.ArgumentInvalid));
                    Logger.Info(request.LogData(), "DeviceToken cannot be null or Empty.");
                }
            }
            catch (Exception ex)
            {
                response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                Logger.Error(request.LogData(), response.Message, response.Exception);
            }
            return response;
        }

        /// <summary>
        /// Sets the push notification as having been viewed.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public ViewedPushNotificationResponse ViewedPushNotification(ViewedPushNotificationRequest request)
        {
            var response = new ViewedPushNotificationResponse(request);

            if (!ValidateRequest(request, response, Validate.All))
                return response;

            try
            {
                if (request.Id > 0)
                {
                    var pushNotification = Repositories.Core.FindById<PushNotification>(request.Id);
                    if (pushNotification != null)
                    {
                        pushNotification.Viewed = true;
                        Repositories.Core.SaveChanges();
                    }
                    else
                    {
                        response.SetFailure(FormatErrorMessage(request, ErrorTypes.UnableToChangeSsn));
                        Logger.Info(request.LogData(), string.Format("Unable to find push notification for'{0}'", request.Id));
                    }
                }
                else
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.ArgumentInvalid));
                    Logger.Info(request.LogData(), string.Format("Invalid Push Notification Id : {0}", request.Id));
                }
            }
            catch (Exception ex)
            {
                response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                Logger.Error(request.LogData(), response.Message, response.Exception);
            }
            return response;
        }


        /// <summary>
        /// Gets the push notification.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public GetPushNotificationResponse GetPushNotification(GetPushNotificationRequest request)
        {
            var response = new GetPushNotificationResponse(request);

            if (!ValidateRequest(request, response, Validate.All))
                return response;

            try
            {
                var pushNotification = (from pn in Repositories.Core.PushNotifications
                                        where pn.Id == request.Id
                                        select new PushNotificationDto
                                        {
                                            Id = pn.Id,
                                            Title = pn.Title,
                                            Text = pn.Text,
                                            Sent = pn.Sent,
                                            PersonMobileDeviceToken = pn.PersonMobileDeviceToken,
                                            CreatedOn = pn.CreatedOn,
                                            Viewed = pn.Viewed
                                        }).FirstOrDefault();

                if (pushNotification.IsNotNull())
                {
                    response.PushNotification = pushNotification;
                }
                else
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.ArgumentInvalid));
                    Logger.Info(request.LogData(), "Push Notification not found.");
                }
            }
            catch (Exception ex)
            {
                response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                Logger.Error(request.LogData(), response.Message, response.Exception);
            }
            return response;
        }
    }
}
