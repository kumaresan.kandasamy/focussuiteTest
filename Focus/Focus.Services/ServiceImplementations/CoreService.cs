﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Globalization;
using System.Text;
using System.Web.Script.Serialization;
using Focus.Core;
using Focus.Core.Criteria;
using Focus.Core.Criteria.Message;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.DataTransferObjects.FocusExplorer;
using Focus.Core.EmailTemplate;
using Focus.Core.Messages;
using Focus.Core.Messages.CandidateService;
using Focus.Core.Messages.CoreService;
using Focus.Core.Messages.JobService;
using Focus.Core.Settings;
using Focus.Core.Settings.Interfaces;
using Focus.Core.Views;
using Focus.Data.Configuration.Entities;
using Focus.Data.Core.Entities;
using Focus.Data.Library.Entities;
using Focus.Data.Repositories.Contracts;
using Focus.Services.DtoMappers;
using Focus.Services.Core;
using Focus.Services.Core.Extensions;
using Focus.Services.Messages;
using Focus.Services.ServiceContracts;
using Focus.Services.Queries;
using Focus.Services.Helpers;
using Focus.Core.Messages.EmployeeService;
using Framework.Messaging;
using Localisation = Focus.Data.Configuration.Entities.Localisation;

using Framework.Caching;
using Framework.Core;
using Framework.Logging;

using Mindscape.LightSpeed;
using Mindscape.LightSpeed.Querying;
using Message = Focus.Data.Core.Entities.Message;
using Focus.Core.IntegrationMessages;
using Focus.Core.Models.Assist;
using Newtonsoft.Json;

#endregion

namespace Focus.Services.ServiceImplementations
{
    public class CoreService : ServiceBase, ICoreService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CoreService" /> class.
        /// </summary>
        public CoreService()
            : this(null)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="CoreService" /> class.
        /// </summary>
        /// <param name="runtimeContext">The runtime context.</param>
        public CoreService(IRuntimeContext runtimeContext)
            : base(runtimeContext)
        { }

        #region General

        /// <summary>
        /// Gets the build info.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public GetVersionInfoResponse GetVersionInfo(GetVersionInfoRequest request)
        {
            var response = new GetVersionInfoResponse(request);

            // Validate request
            if (!ValidateRequest(request, response, Validate.ClientTag | Validate.AccessToken))
                return response;

            try
            {
                var creationTime = File.GetLastWriteTime(Assembly.GetExecutingAssembly().Location);
                response.VersionInfo = string.Format("{0} @ {1:MMM dd, yyyy HH:mm}", Constants.SystemVersion, creationTime);
            }
            catch (Exception ex)
            {
                response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                Logger.Error(request.LogData(), response.Message, response.Exception);
            }

            return response;
        }

        #endregion

        #region Application Settings

        /// <summary>
        /// Gets external only configurations.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>A response containing all configuration items</returns>
        public GetConfigurationResponse GetExternalConfigurationItems(GetConfigurationRequest request)
        {
            return GetConfigurationItems(request, true);
        }

        /// <summary>
        /// Gets all configurations.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>A response containing configuration items</returns>
        public GetConfigurationResponse GetConfigurationItems(GetConfigurationRequest request)
        {
            return GetConfigurationItems(request, false);
        }

        /// <summary>
        /// Gets all configurations.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="externalOnly">if set to <c>true</c> then only return external configuration items</param>
        /// <returns>
        /// A response containing configuration items
        /// </returns>
        private GetConfigurationResponse GetConfigurationItems(GetConfigurationRequest request, bool externalOnly)
        {
            var response = new GetConfigurationResponse(request);

            if (request.ClientTag.IsNullOrEmpty())
                request.ClientTag = ConfigurationManager.AppSettings[Constants.WebConfigKeys.ClientTag];

            // Validate request
            if (!ValidateRequest(request, response, Validate.ClientTag | Validate.AccessToken))
                return response;

            try
            {
                response.ConfigurationItems = externalOnly
                                                                                ? Helpers.Configuration.ExternalConfigurationItems
                                                                                : Helpers.Configuration.ConfigurationItems;
            }
            catch (Exception ex)
            {
                response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                Logger.Error(request.LogData(), response.Message, response.Exception);
            }

            return response;
        }

        /// <summary>
        /// Saves the configuration items.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public SaveConfigurationItemsResponse SaveExternalConfigurationItems(SaveConfigurationItemsRequest request)
        {
            return SaveConfigurationItems(request, true);
        }

        /// <summary>
        /// Saves the configuration items.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public SaveConfigurationItemsResponse SaveConfigurationItems(SaveConfigurationItemsRequest request)
        {
            return SaveConfigurationItems(request, false);
        }

        /// <summary>
        /// Gets the bool configuration items.
        /// </summary>
        /// <returns></returns>
        public ConfigurationItemsResponse GetBoolConfigItems(ConfigurationItemsRequest request)
        {
            var response = new ConfigurationItemsResponse(request);

            var appSettings = new AppSettings();

            response.ConfigurationItems = typeof(IAppSettings).GetProperties()
                .Where(p => p.PropertyType == typeof(bool) && p.GetCustomAttributes(typeof(DbConfigSettingAttribute), true).Any())
                .ToDictionary(p => (p.GetCustomAttributes(typeof(DbConfigSettingAttribute), true).Single() as DbConfigSettingAttribute).Key, p => (p.GetValue(appSettings, null) as bool?).Value);

            return response;
        }

        /// <summary>
        /// Saves the configuration items.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="externalOnly">if set to <c>true</c> then only return external configuration items</param>
        /// <returns></returns>
        private SaveConfigurationItemsResponse SaveConfigurationItems(SaveConfigurationItemsRequest request, bool externalOnly)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new SaveConfigurationItemsResponse(request);

                // Validate 
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    foreach (var configurationItemDto in request.ConfigurationItems.Where(ci => !ci.Id.HasValue && ci.Key.IsNotNullOrEmpty()))
                    {
                        // Resharper suggests copying to local variable
                        var dto = configurationItemDto;

                        // Remove existing setting and add new one
                        var configurationItemsForDeletion = Repositories.Configuration.ConfigurationItems.Where(x => x.Key == dto.Key).ToList();

                        var isInternalUpdate = configurationItemsForDeletion.Any(ci => ci.InternalOnly);
                        if (externalOnly && isInternalUpdate)
                            throw new Exception("Invalid update of internal item");

                        configurationItemsForDeletion.ForEach(x => Repositories.Configuration.Remove(x));

                        var configurationItem = new ConfigurationItem();
                        configurationItem = configurationItemDto.CopyTo(configurationItem);
                        configurationItem.InternalOnly = isInternalUpdate;
                        Repositories.Configuration.Add(configurationItem);

                        Repositories.Configuration.SaveChanges(true);

                        LogAction(request, ActionTypes.UpdateConfigurationItem, configurationItem);
                    }

                    foreach (var configurationItemDto in request.ConfigurationItems.Where(ci => ci.Id.HasValue))
                    {
                        // Get existing setting
                        var configurationItem = Repositories.Configuration.ConfigurationItems.Single(x => x.Id == configurationItemDto.Id);

                        if (externalOnly && configurationItem.InternalOnly)
                            throw new Exception("Invalid update of internal item");

                        configurationItem.Value = configurationItemDto.Value;

                        Repositories.Configuration.SaveChanges(true);

                        LogAction(request, ActionTypes.UpdateConfigurationItem, configurationItem);
                    }

                    // Reload the new settings into the cache
                    Helpers.Configuration.RefreshAppSettings();
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        // TEMP TESTING METHOD
        public void RefreshSettings()
        {
            Helpers.Configuration.RefreshAppSettings();
        }

        #endregion

        #region Localisation

        /// <summary>
        /// Gets the localisation dictionary.
        /// Note: We aren't profiling this.  The cached method is profiled instead
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public GetLocalisationDictionaryResponse GetLocalisationDictionary(GetLocalisationDictionaryRequest request)
        {
            var response = new GetLocalisationDictionaryResponse(request);

            // Validate client tag
            if (!ValidateRequest(request, response, Validate.ClientTag | Validate.AccessToken))
                return response;

            try
            {
                response.LocalisationDictionary = GetCachedLocalisationDictionary(request);
            }
            catch (Exception ex)
            {
                response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                Logger.Error(request.LogData(), response.Message, response.Exception);
            }

            return response;
        }

        /// <summary>
        /// Gets the non default localisations.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public GetLocalisationResponse GetLocalisation(GetLocalisationRequest request)
        {
            var response = new GetLocalisationResponse(request);

            // Validate
            if (!ValidateRequest(request, response, Validate.All))
                return response;

            try
            {
                response.Localisations = Repositories.Configuration.Localisations.Select(x => x.AsDto()).ToList();
            }
            catch (Exception ex)
            {
                response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                Logger.Error(request.LogData(), response.Message, response.Exception);
            }

            return response;
        }

        /// <summary>
        /// Updates a localisation item.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The save response</returns>
        public SaveLocalisationItemsResponse SaveLocalisationItems(SaveLocalisationItemsRequest request)
        {
            var response = new SaveLocalisationItemsResponse(request);

            // Validate client tag
            if (!ValidateRequest(request, response, Validate.ClientTag | Validate.AccessToken))
                return response;

            try
            {
                var culture = request.Culture ?? Constants.DefaultCulture;
                var localisation = Repositories.Configuration.Localisations.FirstOrDefault(l => l.Culture == culture);

                if (localisation.IsNull())
                {
                    localisation = new Localisation { Culture = culture };
                    Repositories.Configuration.Add(localisation);
                    Repositories.Configuration.SaveChanges(true);
                }

                var keys = request.Items.Select(i => i.Key).ToList();

                var localisationItems = Repositories.Configuration.LocalisationItems
                                                                                                                    .Where(li => li.LocalisationId == localisation.Id && keys.Contains(li.Key))
                                                                                                                    .ToDictionary(li => li.Key, li => li);

                foreach (var requestItem in request.Items)
                {
                    if (localisationItems.ContainsKey(requestItem.Key))
                    {
                        var localisationItem = localisationItems[requestItem.Key];
                        if (requestItem.Value.IsNullOrEmpty())
                        {
                            Repositories.Configuration.Remove(localisationItem);
                        }
                        else
                        {
                            localisationItem.Value = requestItem.Value;
                        }
                    }
                    else
                    {
                        var localisationItem = new Data.Configuration.Entities.LocalisationItem
                        {
                            LocalisationId = localisation.Id,
                            Key = requestItem.Key,
                            Value = requestItem.Value,
                            Localised = false,
                            ContextKey = string.Empty
                        };
                        Repositories.Configuration.Add(localisationItem);
                    }
                }

                Repositories.Configuration.SaveChanges(true);

                response.LocalisationDictionary = RefreshCachedLocalisationDictionary(request);
            }
            catch (Exception ex)
            {
                response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                Logger.Error(request.LogData(), response.Message, response.Exception);
            }

            return response;
        }

        /// <summary>
        /// Saves the default localisation items.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public SaveDefaultLocalisationItemsResponse SaveDefaultLocalisationItems(SaveDefaultLocalisationItemsRequest request)
        {
            var response = new SaveDefaultLocalisationItemsResponse(request);

            if (AppSettings.SaveDefaultLocalisationItems)
            {
                // Validate client tag
                if (!ValidateRequest(request, response, Validate.ClientTag | Validate.AccessToken))
                    return response;

                try
                {
                    var keys = request.Items.Select(i => i.Key).ToList();
                    var keysThatExist = Repositories.Configuration.DefaultLocalisationItems.Where(x => keys.Contains(x.Key)).Select(x => x.Key);
                    var keysToAdd = keys.Except(keysThatExist).ToArray();

                    if (keysToAdd.Length > 0)
                    {
                        foreach (var item in request.Items.Where(x => keysToAdd.Contains(x.Key)))
                            Repositories.Configuration.Add(new DefaultLocalisationItem { Key = item.Key, DefaultValue = item.Value ?? "" });

                        Repositories.Configuration.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }
            }

            return response;
        }

        #endregion

        #region Job Titles

        /// <summary>
        /// Gets the generic job titles.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public GetGenericJobTitlesResponse GetGenericJobTitles(GetGenericJobTitlesRequest request)
        {
            var response = new GetGenericJobTitlesResponse(request);

            // Validate request
            if (!ValidateRequest(request, response, Validate.ClientTag | Validate.AccessToken))
                return response;

            try
            {
                var jobTitles = Cacher.Get<List<string>>(Constants.CacheKeys.JobTitlesKey);

                if (jobTitles == null)
                {
                    using (Profiler.Profile(request.LogData()))
                    {
                        lock (SyncObject)
                        {
                            jobTitles = Repositories.Library.Query<GenericJobTitle>().OrderBy(x => x.Value).Select(x => x.Value).ToList();
                            Cacher.Set(Constants.CacheKeys.JobTitlesKey, jobTitles);
                        }
                    }
                }

                response.JobTitles = (request.JobTitle.IsNotNullOrEmpty()
                                                                ? jobTitles.Where(x => x.ToLower().Contains(request.JobTitle.ToLower())).Take(request.ListSize).ToList()
                                                                : jobTitles.Take(request.ListSize).ToList());

            }
            catch (Exception ex)
            {
                response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                Logger.Error(request.LogData(), response.Message, response.Exception);
            }

            return response;
        }

        #endregion

        #region NAICS

        /// <summary>
        /// Gets the naics.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public GetNaicsResponse GetNaics(GetNaicsRequest request)
        {
            var response = new GetNaicsResponse(request);

            // Validate request
            if (!ValidateRequest(request, response, Validate.ClientTag | Validate.AccessToken))
                return response;

            try
            {
                var allNaics = GetAllNaics(request).AsQueryable();

                if (request.ChildrenOnly)
                    allNaics = allNaics.Where(naics => naics.IsChild);

                var prefix = request.Prefix.TrimStart();

                response.Naics = (request.Prefix.IsNotNullOrEmpty()
                                                        ? allNaics
                                                                .Where(
                                                                    x => x.Code.StartsWith(prefix) ||
                                                                             x.Name.StartsWith(prefix, StringComparison.InvariantCultureIgnoreCase) ||
                                                                             x.Name.Contains(" " + prefix, StringComparison.InvariantCultureIgnoreCase))
                                                                .Take(request.ListSize)
                                                                .ToList()
                                                        : allNaics.Take(request.ListSize).ToList());

            }
            catch (Exception ex)
            {
                response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                Logger.Error(request.LogData(), response.Message, response.Exception);
            }

            return response;
        }

        #endregion

        #region Postal Code

        /// <summary>
        /// Gets the state and county for a postal code.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public GetStateCityAndCountyForPostalCodeResponse GetStateCityAndCountyForPostalCode(GetStateCityAndCountyForPostalCodeRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new GetStateCityAndCountyForPostalCodeResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.ClientTag | Validate.AccessToken))
                    return response;

                try
                {
                    var postalCode = (request.PostalCode.Length > 5 ? request.PostalCode.Substring(0, 5) : request.PostalCode);
                    var postalCodeView = Repositories.Library.Query<PostalCodeView>().FirstOrDefault(x => x.Code == postalCode);

                    if (postalCodeView != null)
                    {
                        response.StateLookupId = GetLookup(LookupTypes.States)
                                .Where(x => x.Key == postalCodeView.StateKey)
                                .Select(x => x.Id)
                                .FirstOrDefault();
                        response.CountyLookupId = GetLookup(LookupTypes.Counties)
                            .Where(x => x.Key == postalCodeView.CountyKey)
                            .Select(x => x.Id)
                            .FirstOrDefault();
                        response.CityName = postalCodeView.CityName;
                    }
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Establishes the location of a postal code or city and state.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public EstablishLocationResponse EstablishLocation(EstablishLocationRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new EstablishLocationResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.ClientTag | Validate.AccessToken))
                    return response;

                try
                {
                    PostalCodeView postalCodeView = null;

                    if (request.PostalCode.IsNotNullOrEmpty())
                    {
                        var postalCode = (request.PostalCode.Length > 5 ? request.PostalCode.Substring(0, 5) : request.PostalCode);
                        postalCodeView = Repositories.Library.Query<PostalCodeView>().FirstOrDefault(x => x.Code == postalCode);
                    }
                    else
                    {
                        if (request.State.IsNotNullOrEmpty()) request.State = request.State.Trim();
                        if (request.City.IsNotNullOrEmpty()) request.City = request.City.Trim();

                        if (request.State.IsNotNullOrEmpty() && request.City.IsNotNullOrEmpty())
                        {
                            postalCodeView =
                                Repositories.Library.Query<PostalCodeView>()
                                                        .FirstOrDefault(x => x.StateName == request.State && x.CityName == request.City) ??
                                Repositories.Library.Query<PostalCodeView>()
                                                        .FirstOrDefault(x => x.StateKey == "State." + request.State && x.CityName == request.City);
                        }
                    }

                    if (postalCodeView != null)
                        response.Location = postalCodeView.AsDto();
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        #endregion

        #region Languages

        /// <summary>
        /// Gets the language.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public GetLanguageResponse GetLanguage(GetLanguageRequest request)
        {
            var response = new GetLanguageResponse(request);

            // Validate request
            if (!ValidateRequest(request, response, Validate.All))
                return response;

            try
            {
                response.Languages = GetLookup(LookupTypes.Languages)
                        .Where(x => x.Text.StartsWith(request.Criteria.LanguageLike, StringComparison.OrdinalIgnoreCase))
                        .OrderBy(x => x.DisplayOrder).ThenBy(x => x.Text).ToList();
            }
            catch (Exception ex)
            {
                response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                Logger.Error(request.LogData(), response.Message, response.Exception);
            }

            return response;
        }

        #endregion

        #region Certificate & License

        /// <summary>
        /// Gets a certificate licenses.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public GetCertificateLicenseResponse GetCertificateLicenses(GetCertificateLicenseRequest request)
        {
            var response = new GetCertificateLicenseResponse(request);

            // Validate  request
            if (!ValidateRequest(request, response, Validate.All))
                return response;

            try
            {
                var certificateLicenseKey = string.Format(Constants.CacheKeys.CertificateLicenseKey, request.UserContext.Culture);
                var certificateLicenses = Cacher.Get<List<CertificateLicenseView>>(certificateLicenseKey);

                if (certificateLicenses == null)
                {
                    using (Profiler.Profile(request.LogData(), request))
                    {
                        var userCultureCertificateLicenses = (from cl in Repositories.Configuration.CertificateLicenses
                                                              join clli in Repositories.Configuration.CertificateLicenceLocalisationItems on cl.Key
                                                                  equals clli.Key
                                                              join l in Repositories.Configuration.Localisations on clli.LocalisationId equals l.Id
                                                              where l.Culture == request.UserContext.Culture
                                                              select new CertificateLicenseView
                                                              {
                                                                  Id = cl.Id,
                                                                  NameKey = cl.Key,
                                                                  Name = clli.Value,
                                                                  CertificateLicenseType = cl.CertificateLicenseType,
                                                                  OnetParentCode = cl.OnetParentCode,
                                                                  IsLicence = (cl.IsLicence == "1"),
                                                                  IsCertificate = (cl.IsCertificate == "1")
                                                              }).ToList();

                        var defaultCultureCertificateLicenses = (from cl in Repositories.Configuration.CertificateLicenses
                                                                 join clli in Repositories.Configuration.CertificateLicenceLocalisationItems on
                                                                     cl.Key equals clli.Key
                                                                 join l in Repositories.Configuration.Localisations on clli.LocalisationId equals
                                                                     l.Id
                                                                 where l.Culture == Constants.DefaultCulture
                                                                 select new CertificateLicenseView
                                                                 {
                                                                     Id = cl.Id,
                                                                     NameKey = cl.Key,
                                                                     Name = clli.Value,
                                                                     CertificateLicenseType = cl.CertificateLicenseType,
                                                                     OnetParentCode = cl.OnetParentCode,
                                                                     IsLicence = (cl.IsLicence == "1"),
                                                                     IsCertificate = (cl.IsCertificate == "1")
                                                                 }).ToList();

                        certificateLicenses = MergeCertificateLicenses(userCultureCertificateLicenses, defaultCultureCertificateLicenses);

                        lock (SyncObject)
                            Cacher.Set(certificateLicenseKey, certificateLicenses);
                    }
                }

                var query = certificateLicenses.Where(x => x.Name.ToLower().Contains(request.Criteria.LikeName.ToLower()));

                if (request.Criteria.IsCertificate)
                    query = query.Where(x => request.Criteria.IsCertificate);
                if (request.Criteria.IsLicence)
                    query = query.Where(x => request.Criteria.IsLicence);

                response.CertificateLicenses = query.Take(request.Criteria.ListSize).ToList();
            }
            catch (Exception ex)
            {
                response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                Logger.Error(request.LogData(), response.Message, response.Exception);
            }

            return response;
        }

        /// <summary>
        /// Merges the certificate licenses.
        /// </summary>
        /// <param name="cultureSpecificCertificateLicenses">The culture specific certificate licenses.</param>
        /// <param name="defaultCultureCertificateLicenses">The default culture certificate licenses.</param>
        /// <returns></returns>
        private List<CertificateLicenseView> MergeCertificateLicenses(List<CertificateLicenseView> cultureSpecificCertificateLicenses, List<CertificateLicenseView> defaultCultureCertificateLicenses)
        {
            if (cultureSpecificCertificateLicenses.Count > 0)
            {
                var removeSet = (from cscl in cultureSpecificCertificateLicenses
                                 join dccl in defaultCultureCertificateLicenses on cscl.Id equals dccl.Id
                                 select dccl
                                                ).ToList();

                removeSet.ForEach(x => defaultCultureCertificateLicenses.Remove(x));

                return cultureSpecificCertificateLicenses.Union(defaultCultureCertificateLicenses).ToList();
            }

            return defaultCultureCertificateLicenses;
        }

        #endregion

        #region Emails


        /// <summary>
        /// Sends the email.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public SendEmailResponse SendEmail(SendEmailRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new SendEmailResponse(request);

                // Validate
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    if (request.SendOnFutureDate.IsNotNull() && request.SendOnFutureDate > DateTime.Now)
                        Helpers.Email.SendQueuedEmail(request.EmailToAddresses, null, null, request.EmailSubject, request.EmailBody, SendEmailOn: request.SendOnFutureDate ?? DateTime.Now, isBodyHtml: request.IsHtml, attachment: request.Attachment, attachmentName: request.AttachmentName, detectUrl: request.DetectUrl);
                    else
                        Helpers.Email.SendEmail(request.EmailToAddresses, null, null, request.EmailSubject, request.EmailBody, isBodyHtml: request.IsHtml, attachment: request.Attachment, attachmentName: request.AttachmentName, detectUrl: request.DetectUrl);
                }

                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        public SendEmailFromTemplateResponse SendEmailFromTemplate(SendEmailFromTemplateRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new SendEmailFromTemplateResponse(request);

                // Validate
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    Helpers.Email.SendEmailFromTemplate(request.TemplateType, request.TemplateData, request.EmailToAddresses, request.CC, request.BCC, request.SendAsync, request.IsHtml, request.Attachment, request.AttachmentName, request.DetectUrl);
                }

                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Gets an email template for preview.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public EmailTemplateResponse GetEmailTemplatePreview(EmailTemplateRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new EmailTemplateResponse(request);

                // Validate
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    response.EmailTemplatePreview = Helpers.Email.GetEmailTemplatePreview(request.EmailTemplateType, request.TemplateValues);
                }

                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Gets the email template.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public EmailTemplateResponse GetEmailTemplate(EmailTemplateRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new EmailTemplateResponse(request);

                // Validate
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    response.EmailTemplate = Helpers.Email.GetEmailTemplate(request.EmailTemplateType);
                }

                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Gets the localisation item.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public GetLocalisationDictionaryResponse GetLocalisationItem(GetLocalisationDictionaryRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new GetLocalisationDictionaryResponse(request);

                // Validate
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    response.LocalisationDictionaryEntry = Helpers.Localisation.GetLocalisationItem(request.Key);
                }

                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Saves the email template.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public SaveEmailTemplateResponse SaveEmailTemplate(SaveEmailTemplateRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new SaveEmailTemplateResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    EmailTemplate emailTemplate;

                    if (request.EmailTemplate.Id.IsNotNull())
                    {
                        emailTemplate = Repositories.Configuration.FindById<EmailTemplate>(request.EmailTemplate.Id);
                        emailTemplate = request.EmailTemplate.CopyTo(emailTemplate);
                    }
                    else
                    {
                        emailTemplate = new EmailTemplate();
                        emailTemplate = request.EmailTemplate.CopyTo(emailTemplate);
                        Repositories.Configuration.Add(emailTemplate);
                    }

                    Repositories.Configuration.SaveChanges(true);

                    response.EmailTemplate = emailTemplate.AsDto();

                    LogAction(request, ActionTypes.SaveEmailTemplate, emailTemplate);
                }

                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Gets emails sent regarding referrals.
        /// </summary>
        /// <param name="request">The request containing the relevant ids.</param>
        /// <returns>A list of emails sent</returns>
        public GetReferralEmailResponse GetReferralEmails(GetReferralEmailRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new GetReferralEmailResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    // Find the employee so we can get their email address
                    var emailQuery = Repositories.Core.Query<ReferralEmail>();

                    if (request.EmployeeId.HasValue)
                        emailQuery = emailQuery.Where(email => email.EmployeeId == request.EmployeeId);

                    if (request.ApplicationId.HasValue)
                        emailQuery = emailQuery.Where(email => email.ApplicationId == request.ApplicationId);

                    if (request.ApprovalStatus.HasValue)
                        emailQuery = emailQuery.Where(email => email.ApprovalStatus == request.ApprovalStatus.Value);

                    emailQuery = emailQuery.OrderByDescending(email => email.EmailSentOn);

                    if (request.EmailsRequired.HasValue)
                        emailQuery = emailQuery.Take(request.EmailsRequired.Value);

                    if (request.JobId.HasValue)
                        emailQuery = emailQuery.Where(email => email.JobId == request.JobId.Value);

                    response.EmailsSent = emailQuery.Select(email => email.AsDto()).ToList();
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Saves an email sent on referral
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public SaveReferralEmailResponse SaveReferralEmail(SaveReferralEmailRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new SaveReferralEmailResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    var personId = request.EmployeeId.IsNotNull()
                                                     ? Repositories.Core.Employees.Where(x => x.Id == request.EmployeeId).Select(x => x.PersonId).FirstOrDefault()
                                                     : Repositories.Core.Query<ApplicationBasicView>().Where(x => x.Id == request.ApplicationId).Select(x => x.CandidateId).FirstOrDefault();

                    var emailToSave = new ReferralEmail
                    {
                        PersonId = personId,
                        EmployeeId = request.EmployeeId,
                        ApplicationId = request.ApplicationId,
                        EmailText = request.EmailBody,
                        EmailSentOn = DateTime.Now,
                        EmailSentBy = request.UserContext.UserId,
                        ApprovalStatus = request.ApprovalStatus,
                        JobId = request.JobId
                    };

                    Repositories.Core.Add(emailToSave);
                    Repositories.Core.SaveChanges(true);
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        #endregion

        #region Profile Event

        /// <summary>
        /// Profiles an event.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public ProfilerEventResponse ProfileEvent(ProfilerEventRequest request)
        {
            var response = new ProfilerEventResponse(request);

            // Validate Client Tag
            if (!ValidateRequest(request, response, Validate.ClientTag | Validate.AccessToken))
                return response;

            // Update the session in the database
            Helpers.Session.UpdateSession(request.SessionId, request.UserContext.UserId, true);

            if (AppSettings.ProfilingEnabled)
                Profiler.Instance.OnProfile(new ProfileEventArgs(request.SessionId, request.RequestId,
                                                                                                                 request.UserContext.ActionerId,
                                                                                                                 request.Server, request.InTime,
                                                                                                                 request.OutTime, request.Milliseconds, request.DeclaringType,
                                                                                                                 request.Method, request.Parameters));

            return response;
        }

        #endregion

        #region Messages

        /// <summary>
        /// Gets the message.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public GetMessageResponse GetMessage(GetMessageRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new GetMessageResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    if (request.Module.IsNull())
                    {
                        response.SetFailure(FormatErrorMessage(request, ErrorTypes.ModuleNotProvided));
                        Logger.Error(request.LogData(), response.Message, response.Exception);
                    }

                    // Get messages in the users culture
                    var usersCultureQuery = new MessageViewQuery(Repositories.Core, Repositories.Configuration, new MessageViewCriteria
                    {
                        UserId = request.UserContext.UserId,
                        EmployerId = request.UserContext.EmployerId,
                        Module = request.Module,
                        EmployeeId = request.UserContext.EmployeeId,
                        Culture = request.UserContext.Culture
                    });

                    var userCultureMessages = usersCultureQuery.Query().ToList();

                    // Get messages for default culture
                    var defaultCultureQuery = new MessageViewQuery(Repositories.Core, Repositories.Configuration, new MessageViewCriteria
                    {
                        UserId = request.UserContext.UserId,
                        EmployerId = request.UserContext.EmployerId,
                        Module = request.Module,
                        EmployeeId = request.UserContext.EmployeeId,
                        Culture = Constants.DefaultCulture
                    });


                    var defaultCultureMessages = defaultCultureQuery.Query().ToList();

                    // Merge the messages
                    var messages = MergeMessages(userCultureMessages, defaultCultureMessages);

                    // Add the sort order
                    var query = from m in messages
                                orderby m.IsSystemAlert descending, m.CreatedOn descending
                                select m;

                    response.Messages = query.ToList();
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Gets recent messages.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public GetMessageResponse GetRecentMessages(GetMessageRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new GetMessageResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    var query = from m in Repositories.Core.Messages
                                join mt in Repositories.Core.MessageTexts
                                    on m.Id equals mt.MessageId
                                join u in Repositories.Core.Users
                                    on m.CreatedBy equals u.Id into ug
                                from u2 in ug.DefaultIfEmpty()
                                join p in Repositories.Core.Persons
                                    on u2.PersonId equals p.Id into pg
                                from p2 in pg.DefaultIfEmpty()
                                where m.IsSystemAlert
                                    && m.ExpiresOn > DateTime.Now
                                select new MessageView
                                {
                                    Id = m.Id,
                                    IsSystemAlert = m.IsSystemAlert,
                                    Text = mt.Text,
                                    CreatedOn = m.CreatedOn,
                                    Audience = m.Audience,
                                    EmployerId = m.EmployerId,
                                    UserId = m.UserId,
                                    BusinessUnitId = m.BusinessUnitId,
                                    CreatedByFirstName = p2.FirstName,
                                    CreatedByLastName = p2.LastName
                                };

                    if (request.OrderBy.IsNotNullOrEmpty())
                        query = query.OrderBy(request.OrderBy);

                    response.Messages = query.ToList();
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }
        /// <summary>
        /// Expires a message.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public DismissMessageResponse ExpireMessage(DismissMessageRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new DismissMessageResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    var message = Repositories.Core.Messages.FirstOrDefault(m => m.Id == request.MessageId);
                    if (message.IsNotNull())
                    {
                        message.ExpiresOn = DateTime.Now;

                        LogAction(request, ActionTypes.ExpireMessage, message);

                        Repositories.Core.SaveChanges(true);
                    }
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Dismisses the message.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public DismissMessageResponse DismissMessage(DismissMessageRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new DismissMessageResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    var dismissedMessage = new DismissedMessage
                    {
                        MessageId = request.MessageId,
                        UserId = request.UserContext.UserId
                    };

                    Repositories.Core.Add(dismissedMessage);

                    LogAction(request, ActionTypes.DismissMessage, dismissedMessage);

                    Repositories.Core.SaveChanges(true);
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Creates the homepage alert.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public CreateHomepageAlertResponse CreateHomepageAlert(CreateHomepageAlertRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new CreateHomepageAlertResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    var defaultLocalisationId = Repositories.Configuration.Localisations.FirstOrDefault(x => x.Culture == Constants.DefaultCulture).Id;
                    var messagesAdded = new List<Message>();

                    foreach (var homepageAlert in request.HomepageAlerts)
                    {
                        var message = new Message();
                        message = homepageAlert.Message.CopyTo(message);
                        Repositories.Core.Add(message);

                        foreach (var messageTextDto in homepageAlert.MessageTexts)
                        {
                            var messageText = new MessageText();
                            messageText = messageTextDto.CopyTo(messageText);
                            messageText.MessageId = message.Id;

                            // If the localisation Id is 0 we will add the text to the default localisation
                            if (messageText.LocalisationId == 0)
                                messageText.LocalisationId = defaultLocalisationId;

                            Repositories.Core.Add(messageText);
                        }

                        messagesAdded.Add(message);
                    }

                    Repositories.Core.SaveChanges(true);

                    messagesAdded.ForEach(m => LogAction(request, ActionTypes.CreateHomepageAlert, m));
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Saves the message text.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public SaveMessageTextsResponse SaveMessageTexts(SaveMessageTextsRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new SaveMessageTextsResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    SavedMessage message;
                    var isEdit = false;

                    if (request.Message.Id.IsNotNull())
                    {
                        message = Repositories.Core.FindById<SavedMessage>(request.Message.Id);
                        message = request.Message.CopyTo(message);
                        isEdit = true;
                    }
                    else
                    {
                        message = new SavedMessage();
                        message = request.Message.CopyTo(message);
                        Repositories.Core.Add(message);
                    }

                    // If this is an edit clear out previous message texts
                    if (isEdit)
                    {
                        var currentSavedMessageTexts = Repositories.Core.SavedMessageTexts.Where(x => x.SavedMessageId == message.Id).ToList();
                        currentSavedMessageTexts.ForEach(x => Repositories.Core.Remove(x));
                    }

                    foreach (var messageTextDto in request.MessageTexts)
                    {
                        var messageText = new SavedMessageText();
                        messageText = messageTextDto.CopyTo(messageText);
                        messageText.SavedMessageId = message.Id;

                        // If the localisation Id is 0 we will add the text to the default localisation
                        if (messageText.LocalisationId == 0)
                            messageText.LocalisationId = Repositories.Configuration.Localisations.FirstOrDefault(x => x.Culture == Constants.DefaultCulture).Id;

                        Repositories.Core.Add(messageText);
                    }

                    Repositories.Core.SaveChanges(true);

                    LogAction(request, ActionTypes.SaveMessageText, message);
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Gets the saved message.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public GetSavedMessageResponse GetSavedMessage(GetSavedMessageRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new GetSavedMessageResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    if (request.Id.IsNotNull())
                        response.SavedMessage = Repositories.Core.SavedMessages.FirstOrDefault(x => x.Id == request.Id && x.UserId == request.UserContext.UserId).AsDto();
                    else
                        response.SavedMessages = Repositories.Core.SavedMessages.OrderBy(x => x.Name).Where(x => x.Audience == request.Audience && x.UserId == request.UserContext.UserId)
                                                 .Select(x => x.AsDto()).ToList();
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Gets the saved message texts.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public GetSavedMessageTextsResponse GetSavedMessageTexts(GetSavedMessageTextsRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new GetSavedMessageTextsResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    response.SavedMessageTexts = Repositories.Core.SavedMessageTexts.Where(x => x.SavedMessageId == request.SavedMessageId)
                                             .Select(x => x.AsDto()).ToList();
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Deletes the saved message.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public DeleteSavedMessageResponse DeleteSavedMessage(DeleteSavedMessageRequest request)
        {
            var response = new DeleteSavedMessageResponse(request);

            // Validate request
            if (!ValidateRequest(request, response, Validate.All))
                return response;

            try
            {
                var message = Repositories.Core.FindById<SavedMessage>(request.SavedMessageId);

                Repositories.Core.Remove(message);
                Repositories.Core.SaveChanges(true);

                LogAction(request, ActionTypes.DeleteSavedMessage, message);
            }
            catch (Exception ex)
            {
                response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                Logger.Error(request.LogData(), response.Message, response.Exception);
            }

            return response;
        }

        #region message helpers

        /// <summary>
        /// Merges the messages.
        /// </summary>
        /// <param name="cultureSpecificMessages">The culture specific messages.</param>
        /// <param name="defaultCultureMessages">The default culture messages.</param>
        /// <returns></returns>
        private List<MessageView> MergeMessages(List<MessageView> cultureSpecificMessages, List<MessageView> defaultCultureMessages)
        {
            if (cultureSpecificMessages.Count > 0)
            {
                var removeSet = (from csm in cultureSpecificMessages
                                 join dcm in defaultCultureMessages on csm.Id equals dcm.Id
                                 select dcm
                                                ).ToList();

                removeSet.ForEach(x => defaultCultureMessages.Remove(x));

                return cultureSpecificMessages.Union(defaultCultureMessages).ToList();
            }

            return defaultCultureMessages;
        }

        #endregion

        #endregion

        #region Application Images

        /// <summary>
        /// Gets the application image.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public ApplicationImageResponse GetApplicationImage(ApplicationImageRequest request)
        {
            var response = new ApplicationImageResponse(request);

            // Validate Client Tag as a user may view an image without being logged in
            if (!ValidateRequest(request, response, Validate.ClientTag | Validate.AccessToken))
                return response;

            try
            {
                // Try get from Cache first
                var cacheKey = string.Format(Constants.CacheKeys.ApplicationImage, request.Type);
                response.ApplicationImage = Cacher.Get<ApplicationImageDto>(cacheKey);

                if (response.ApplicationImage.IsNull())
                {
                    using (Profiler.Profile(request.LogData(), request))
                    {
                        response.ApplicationImage = Repositories.Configuration.ApplicationImages.Where(x => x.Type == request.Type).Select(x => x.AsDto()).FirstOrDefault();

                        lock (SyncObject)
                        {
                            Cacher.Set(cacheKey, response.ApplicationImage);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                Logger.Error(request.LogData(), response.Message, response.Exception);
            }
            return response;
        }

        /// <summary>
        /// Saves the application image.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public SaveApplicationImageResponse SaveApplicationImage(SaveApplicationImageRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new SaveApplicationImageResponse(request);

                // Validate 
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    ApplicationImage applicationImage;

                    // Remove previous image with same type
                    var applicationImagesForDeletion = Repositories.Configuration.ApplicationImages.Where(x => x.Type == request.ApplicationImage.Type).ToList();
                    applicationImagesForDeletion.ForEach(x => Repositories.Configuration.Remove(x));

                    if (request.ApplicationImage.Id.IsNotNull())
                    {
                        applicationImage = Repositories.Core.FindById<ApplicationImage>(request.ApplicationImage.Id);
                        applicationImage = request.ApplicationImage.CopyTo(applicationImage);
                    }
                    else
                    {
                        applicationImage = new ApplicationImage();
                        applicationImage = request.ApplicationImage.CopyTo(applicationImage);
                        Repositories.Configuration.Add(applicationImage);
                    }

                    Repositories.Configuration.SaveChanges(true);

                    response.ApplicationImage = applicationImage.AsDto();

                    // Update the cache
                    var cacheKey = string.Format(Constants.CacheKeys.ApplicationImage, request.ApplicationImage.Type);

                    lock (SyncObject)
                    {
                        Cacher.Set(cacheKey, response.ApplicationImage);
                    }

                    LogAction(request, ActionTypes.UpdateApplicationImage, applicationImage);
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }
                return response;
            }
        }

        #endregion

        #region Feedback

        /// <summary>
        /// Sends the feedback.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public FeedbackResponse SendFeedback(FeedbackRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new FeedbackResponse(request);

                // Validate 
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    // Add some details about the person who sent the email
                    var emailbody = String.Format(@"{0}

Sent by: {1} {2}
Email Address: {3}", request.Detail, request.UserContext.FirstName, request.UserContext.LastName,
                                                                                request.UserContext.EmailAddress);

                    Helpers.Email.SendEmail(AppSettings.FeedbackEmailAddress, "", "", request.Subject, emailbody);
                    LogAction(request, ActionTypes.EmailFeedback, "None", null);
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }
                return response;
            }
        }

        #endregion

        #region Notes and Reminders

        /// <summary>
        /// Gets the note reminder view.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public NoteReminderViewResponse GetNoteReminder(NoteReminderViewRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new NoteReminderViewResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    var query = new NoteReminderViewQuery(Repositories.Core, request.Criteria);

                    switch (request.Criteria.FetchOption)
                    {
                        case CriteriaBase.FetchOptions.PagedList:
                            response.NoteRemindersPaged = query.Query().GetPagedList(x => x.AsDto(), request.Criteria.PageIndex, request.Criteria.PageSize);
                            break;

                        case CriteriaBase.FetchOptions.List:
                            response.NoteReminders = query.Query().Select(x => x.AsDto()).ToList();
                            break;

                        default:
                            response.NoteReminder = request.Criteria.Id.IsNotNull() ? query.QueryEntityId().AsDto() : query.Query().SingleOrDefault().AsDto();
                            break;
                    }
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Saves the note reminder.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public SaveNoteReminderResponse SaveNoteReminder(SaveNoteReminderRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new SaveNoteReminderResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    NoteReminder noteReminder;

                    if (request.NoteReminder.Id.IsNotNull())
                    {
                        noteReminder = Repositories.Core.FindById<NoteReminder>(request.NoteReminder.Id);
                        noteReminder = request.NoteReminder.CopyTo(noteReminder);
                    }
                    else
                    {
                        noteReminder = new NoteReminder();
                        noteReminder = request.NoteReminder.CopyTo(noteReminder);
                        Repositories.Core.Add(noteReminder);
                    }


                    if (noteReminder.NoteReminderType == NoteReminderTypes.Reminder)
                    {
                        // Remove existing reminder recipients
                        var noteReminderRecipients = Repositories.Core.Query<NoteReminderRecipient>().Where(x => x.NoteReminderId == noteReminder.Id).ToList();
                        noteReminderRecipients.ForEach(x => Repositories.Core.Remove(x));

                        // Add new recipients
                        foreach (var reminderRecipient in request.ReminderRecipients)
                        {
                            var recipient = new NoteReminderRecipient();
                            reminderRecipient.NoteReminderId = noteReminder.Id;
                            recipient = reminderRecipient.CopyTo(recipient);
                            Repositories.Core.Add(recipient);
                        }
                    }

                    Repositories.Core.SaveChanges(true);

                    response.NoteReminder = noteReminder.AsDto();

                    if (request.OverrideCreatedOnDate.HasValue)
                    {
                        var updateQuery = new Query(typeof(NoteReminder), Entity.Attribute("Id") == noteReminder.Id);
                        Repositories.Core.Update(updateQuery, new { CreatedOn = request.OverrideCreatedOnDate.Value });
                        Repositories.Core.SaveChanges(true);

                        response.NoteReminder.CreatedOn = request.OverrideCreatedOnDate.Value;
                    }

                    var action = (noteReminder.NoteReminderType == NoteReminderTypes.Reminder)
                                                 ? ActionTypes.SaveReminder
                                                 : ActionTypes.SaveNote;

                    LogAction(request, action, typeof(NoteReminder).Name, noteReminder.Id, noteReminder.EntityId);
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Sends the reminder.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public SendReminderResponse SendReminder(SendReminderRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new SendReminderResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    Helpers.Alerts.SendReminder(request.Id);
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }


        #endregion

        #region Industry Classification

        /// <summary>
        /// Gets the industry classification.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public IndustryClassificationResponse GetIndustryClassification(IndustryClassificationRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new IndustryClassificationResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.ClientTag | Validate.AccessToken))
                    return response;

                try
                {
                    var query = new IndustryClassificationQuery(Repositories.Library, request.Criteria);

                    switch (request.Criteria.FetchOption)
                    {
                        case CriteriaBase.FetchOptions.PagedList:
                            response.IndustryClassificationsPaged = query.Query().GetPagedList(x => x.AsDto(), request.Criteria.PageIndex, request.Criteria.PageSize);
                            break;

                        case CriteriaBase.FetchOptions.List:
                            response.IndustryClassifications = query.Query().Select(x => x.AsDto()).ToList();
                            break;

                        case CriteriaBase.FetchOptions.Lookup:
                            var industryClassifications = query.Query().Select(x => x.AsDto()).ToList();
                            response.IndustryClassificationLookup = industryClassifications.ToDictionary(industryClassification => industryClassification.Id.ToString(), industryClassification => industryClassification.Name);
                            break;

                        default:
                            var industrialClassification = request.Criteria.Id.IsNotNull() ? query.QueryEntityId() : query.Query().SingleOrDefault();
                            response.IndustryClassification = industrialClassification.IsNotNull() ? industrialClassification.AsDto() : null;
                            break;
                    }
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }
        #endregion

        #region Activities

        /// <summary>
        /// Gets the activity categories.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public GetActivityCategoriesResponse GetActivityCategories(GetActivityCategoriesRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new GetActivityCategoriesResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.ClientTag | Validate.AccessToken))
                    return response;

                try
                {
                    var visibleActivities = Repositories.Core.Query<Activity>().Where(a => a.EnableDisable).Select(a => a.ActivityCategoryId).Distinct();

                    response.ActivityCategories = Repositories.Core.Query<ActivityCategory>()
                                                                                                                 .Where(x => x.Visible
                                                                                                                                            && x.Administrable
                                                                                                                                            && x.ActivityType == request.ActivityType
                                                                                                                                            && visibleActivities.Contains(x.Id))
                                                                                                                 .Select(x => x.AsDto())
                                                                                                                 .ToList();
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Saves an activity category.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response (containing the save category DTO)</returns>
        public SaveActivityCategoryResponse SaveActivityCategory(SaveActivityCategoryRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new SaveActivityCategoryResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.ClientTag | Validate.AccessToken))
                    return response;

                try
                {
                    ActivityCategory category;
                    var saved = false;

                    if (request.Id.HasValue)
                    {
                        category = Repositories.Core.Query<ActivityCategory>().FirstOrDefault(c => c.Id != request.Id && c.Name == request.Name);
                        if (category.IsNull())
                        {
                            category = Repositories.Core.FindById<ActivityCategory>(request.Id);
                            category.Name = request.Name;

                            saved = true;
                        }
                    }
                    else
                    {
                        category = Repositories.Core.Query<ActivityCategory>().FirstOrDefault(c => c.Name == request.Name);
                        if (category.IsNull())
                        {
                            var categoryCount = Repositories.Core.Count<ActivityCategory>();
                            var newKey = (categoryCount + 1).ToString("000");

                            category = new ActivityCategory
                            {
                                LocalisationKey = string.Concat("ActivityCategory.", newKey),
                                Name = request.Name,
                                Visible = true,
                                Administrable = true,
                                ActivityType = request.ActivityType
                            };
                            Repositories.Core.Add(category);

                            saved = true;
                        }
                    }

                    if (saved)
                    {
                        Repositories.Core.SaveChanges(true);
                        response.ActivityCategory = category.AsDto();
                    }
                    response.Saved = saved;
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Deletes an activity category.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        public DeleteActivityCategoryResponse DeleteActivityCategory(DeleteActivityCategoryRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new DeleteActivityCategoryResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.ClientTag | Validate.AccessToken))
                    return response;

                try
                {
                    var deleted = false;
                    if (!Repositories.Core.Query<Activity>().Any(a => a.ActivityCategoryId == request.Id))
                    {
                        var category = Repositories.Core.Query<ActivityCategory>().Single(c => c.Id == request.Id);
                        Repositories.Core.Remove(category);

                        Repositories.Core.SaveChanges(true);
                        deleted = true;
                    }

                    response.Deleted = deleted;
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Gets the activities.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public GetActivitiesResponse GetActivities(GetActivitiesRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new GetActivitiesResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.ClientTag | Validate.AccessToken))
                    return response;

                try
                {
                    var query = Repositories.Core.Query<ActivityView>();

                    if (request.Criteria.FetchOption != CriteriaBase.FetchOptions.Single)
                    {
                        // For reporting purposes we may want to show hidden activities
                        query = query.Where(x => x.ActivityAdministrable && x.CategoryAdministrable);
                    }

                    if (request.Criteria.ActivityCategoryId.HasValue)
                        query = query.Where(x => x.ActivityCategoryId == request.Criteria.ActivityCategoryId.Value);

                    if (request.Criteria.ActivityType.IsNotNull())
                        query = query.Where(x => x.ActivityType == request.Criteria.ActivityType);

                    if (request.Criteria.VisibleOnly)
                        query = query.Where(x => x.ActivityVisible);

                    if (request.Criteria.ActivityId.HasValue)
                        query = query.Where(x => x.Id == request.Criteria.ActivityId.GetValueOrDefault());

                    switch (request.Criteria.OrderBy)
                    {
                        case Constants.SortOrders.ActivityCategoryAsc:
                            query = query.OrderBy(x => x.CategoryName);
                            break;

                        case Constants.SortOrders.ActivityCategoryDesc:
                            query = query.OrderByDescending(x => x.CategoryName);
                            break;

                        case Constants.SortOrders.ActivityAsc:
                            query = query.OrderBy(x => x.ActivityName);
                            break;

                        case Constants.SortOrders.ActivityDesc:
                            query = query.OrderByDescending(x => x.ActivityName);
                            break;

                        default:
                            query = query.OrderBy(x => x.SortOrder).ThenBy(x => x.ActivityName);
                            break;
                    }

                    switch (request.Criteria.FetchOption)
                    {
                        case CriteriaBase.FetchOptions.PagedList:
                            response.ActivitiesPaged = query.GetPagedList(x => x.AsDto(), request.Criteria.PageIndex, request.Criteria.PageSize);
                            break;

                        case CriteriaBase.FetchOptions.Lookup:
                            response.ActivitiesLookUp = query.Select(x => x.AsDto()).ToDictionary(x => x.Id.Value, x => x.ActivityName);
                            break;

                        case CriteriaBase.FetchOptions.Single:
                            response.Activity = query.Select(x => x.AsDto()).SingleOrDefault();
                            break;

                        default:
                            response.Activities = query.Select(x => x.AsDto()).ToList();
                            break;
                    }
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Saves an activity.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response</returns>
        public SaveActivitiesResponse SaveActivity(SaveActivitiesRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new SaveActivitiesResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.ClientTag | Validate.AccessToken))
                    return response;

                try
                {
                    Activity activity;
                    var saved = false;

                    if (request.Id.HasValue)
                    {
                        activity = Repositories.Core.Query<Activity>().FirstOrDefault(a => a.Id != request.Id && a.Name == request.Name);
                        if (activity.IsNull())
                        {
                            activity = Repositories.Core.FindById<Activity>(request.Id);
                            activity.Name = request.Name;

                            saved = true;
                        }
                    }
                    else
                    {
                        activity = Repositories.Core.Query<Activity>().FirstOrDefault(c => c.Name == request.Name);
                        if (activity.IsNull())
                        {
                            var activityCount = Repositories.Core.Count<Activity>();
                            var newKey = (activityCount + 1).ToString("000");

                            activity = new Activity
                            {
                                LocalisationKey = string.Concat("Activity.", newKey),
                                Name = request.Name,
                                EnableDisable = request.Visible,
                                Administrable = true,
                                ActivityCategoryId = request.ActivityCategoryId,
                                ExternalId = request.ExternalId,
                                SortOrder = 0,
                                Used = false
                            };
                            Repositories.Core.Add(activity);

                            saved = true;
                        }
                    }

                    if (saved)
                    {
                        Repositories.Core.SaveChanges(true);
                        response.Activity = activity.AsDto();
                    }
                    response.Saved = saved;
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Saves the activities.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public SaveActivitiesResponse SaveActivities(SaveActivitiesRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new SaveActivitiesResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.ClientTag | Validate.AccessToken))
                    return response;

                try
                {
                    //var activities = request.Activities.Where(a => a.ActivityCategoryId == id).ToList();
                    if (request.Activities.Any(a => a.ActivityVisible && a.ActivityName.IsNullOrEmpty()))
                    {
                        response.Acknowledgement = AcknowledgementType.Failure;
                        response.Message = LocaliseOrDefault(request, "SaveActivitiesErrors.VisibleActivityWithBlankName", "Activities not saved. A visible activity has a blank name.");
                        return response;
                    }

                    if (request.Activities.Any(a => a.ActivityVisible && a.CategoryName.IsNullOrEmpty()))
                    {
                        response.Acknowledgement = AcknowledgementType.Failure;
                        response.Message = LocaliseOrDefault(request, "SaveActivitiesErrors.CategoryWithBlankName", "Activities not saved. A visible category has a blank name.");
                        return response;
                    }

                    var categoryIds = request.Activities.Select(a => a.ActivityCategoryId).Distinct();

                    foreach (var id in categoryIds)
                    {
                        var dbActivityCategory = Repositories.Configuration.FindById<ActivityCategory>(id);
                        var activities = request.Activities.Where(a => a.ActivityCategoryId == id).ToList();

                        if (activities.Any())
                        {
                            var firstActivity = activities.First();
                            if (Repositories.Configuration.Query<ActivityCategory>().Any(ac => ac.Name == firstActivity.CategoryName && ac.ActivityType == firstActivity.ActivityType && ac.Id != id))
                            {
                                response.Acknowledgement = AcknowledgementType.Failure;
                                response.Message = LocaliseOrDefault(request, "SaveActivitiesErrors.DuplicateCategoryName", "Activities not saved. Category {0} already exists.", firstActivity.CategoryName);
                                return response;
                            }

                            var dbActivities = Repositories.Configuration.Query<Activity>().Where(a => a.ActivityCategoryId == id).ToList();
                            activities.ForEach(activity =>
                            {
                                var dbActivity = dbActivities.FirstOrDefault(dbAcitivity => dbAcitivity.Id == activity.Id);
                                if (dbActivity.IsNotNull())
                                {
                                    dbActivity.EnableDisable = activity.ActivityVisible;
                                    dbActivity.Name = activity.ActivityName;
                                }
                            });

                            if (dbActivities.Select(a => a.Name).Distinct().Count(x => x.IsNotNullOrEmpty()) != dbActivities.Count(x => x.Name.IsNotNullOrEmpty()))
                            {
                                response.Acknowledgement = AcknowledgementType.Failure;
                                response.Message = LocaliseOrDefault(request, "SaveActivitiesErrors.DuplicateActivityNameInCategory", "Activities not saved. Category {0} contains a duplicate activity name.", activities.First().CategoryName);
                                return response;
                            }

                            if (dbActivityCategory.IsNotNull())
                            {
                                dbActivityCategory.Visible = dbActivities.Any(a => a.EnableDisable);
                                dbActivityCategory.Name = activities.First().CategoryName;
                            }
                        }
                    }

                    Repositories.Configuration.SaveChanges();
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                    return response;
                }

                response.Acknowledgement = AcknowledgementType.Success;
                response.Message = LocaliseOrDefault(request, "SaveActivitiesSuccessMessage", "Activities successfully saved.");
                return response;
            }
        }

        /// <summary>
        /// Deletes an activity.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        public DeleteActivityResponse DeleteActivity(DeleteActivityRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new DeleteActivityResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.ClientTag | Validate.AccessToken))
                    return response;

                try
                {
                    var deleted = false;

                    var activity = Repositories.Core.Query<Activity>().Single(c => c.Id == request.Id);
                    if (!activity.Used)
                    {
                        Repositories.Core.Remove(activity);
                        Repositories.Core.SaveChanges(true);

                        deleted = true;
                    }

                    response.Deleted = deleted;
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Saves the self service.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public SaveSelfServiceResponse SaveSelfService(SaveSelfServiceRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new SaveSelfServiceResponse(request);
                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;
                try
                {
                    var selfServiceUser = Repositories.Core.Users.FirstOrDefault(x => x.Id == (request.ActingOnBehalfOfUserId.IsNull() ? request.UserContext.UserId : request.ActingOnBehalfOfUserId));

                    if (selfServiceUser.IsNotNull())
                    {
                        Helpers.SelfService.PublishSelfServiceActivity(request.Action, request.UserContext.ActionerId, request.ActingOnBehalfOfUserId ?? request.UserContext.UserId, request.ActingOnBehalfOfPersonId.GetValueOrDefault());
                    }
                    else
                        response.SetFailure(FormatErrorMessage(request, ErrorTypes.UserNotFound));
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        #endregion

        #region ActionEvents
        /// <summary>
        /// Gets the action event.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public GetActionEventResponse GetActionEvent(GetActionEventRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new GetActionEventResponse(request);

                //Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;
                try
                {
                    var actionEvent = Repositories.Core.FindById<ActionEvent>(request.ActionEventId);
                    response.ActionEvent = actionEvent.AsDto();
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        ///<summary>
        ///Checks the last action before activating an account
        ///</summary>
        ///<param name="request">The request</param>
        ///<returns></returns>
        public CheckLastActionResponse CheckLastAction(CheckLastActionRequest request)
        {
            var response = new CheckLastActionResponse(request);
            //get ActivateAccount And InactivateAccount TypeId
            var AccountActivateTypeId = Repositories.Core.Find<ActionType>(x => x.Name.Equals(ActionTypes.ActivateAccount.ToString())).First();
            var InactivateAccountTypeId = Repositories.Core.Find<ActionType>(x => x.Name.Equals(ActionTypes.InactivateAccount.ToString())).First();

            var InActivateAccountActionTableIdForEnityId = Repositories.Core.Query<ActionEvent>().Where(x => x.EntityId == request.PersonId && x.ActionTypeId == InactivateAccountTypeId.Id).OrderByDescending(x => x.Id).Select(x => new { Entityid = x.EntityId, Actiontypid = x.ActionTypeId, Id = x.Id }).FirstOrDefault();

            if (InActivateAccountActionTableIdForEnityId != null)
            {
                var action = Repositories.Core.Query<ActionEvent>().Where(x => x.Id > InActivateAccountActionTableIdForEnityId.Id && x.EntityId == request.PersonId).OrderByDescending(x => x.Id).Select(x => new { Actiontypeid = x.ActionTypeId }).ToList();


                if (action.Count == 0)
                {
                    response.IsAccountActivated = true;
                }

                if (action.Count != 0)
                {
                    foreach (var actionArray in action)
                    {
                        if (actionArray.Actiontypeid == AccountActivateTypeId.Id)
                        {
                            response.IsAccountActivated = false;
                        }
                        else
                        {
                            response.IsAccountActivated = true;
                        }
                    }
                }
            }
            else
            {
                response.IsAccountActivated = false;
            }
            return response;

        }

        /// <summary>
        /// Backdates the action event.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public BackdateActionEventResponse BackdateActionEvent(BackdateActionEventRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new BackdateActionEventResponse(request);

                //validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    #region Preserve Original Activity Log
                    var originalActionEvent = Repositories.Core.FindById<ActionEvent>(request.activityId);
                    var backdatedActionEvent = new BackdatedActionEvents
                    {
                        ActionId = originalActionEvent.Id,
                        SessionId = originalActionEvent.SessionId,
                        RequestId = originalActionEvent.RequestId,
                        CreatedOn = originalActionEvent.CreatedOn,
                        ActionTypeId = originalActionEvent.ActionTypeId,
                        EntityTypeId = originalActionEvent.EntityTypeId,
                        UserId = originalActionEvent.UserId,
                        EntityId = originalActionEvent.EntityId,
                        AdditionalDetails = originalActionEvent.AdditionalDetails,
                        EntityIdAdditional01 = originalActionEvent.EntityIdAdditional01,
                        EntityIdAdditional02 = originalActionEvent.EntityIdAdditional02,
                        ActionedOn = originalActionEvent.ActionedOn,
                        ActionBackdatedOn = DateTime.Now
                    };

                    Repositories.Core.Add<BackdatedActionEvents>(backdatedActionEvent);

                    Repositories.Core.SaveChanges();

                    backdatedActionEvent.CreatedOn = originalActionEvent.CreatedOn;

                    #endregion

                    var updateQuery = new Query(typeof(ActionEvent), Entity.Attribute("Id") == request.activityId);
                    Repositories.Core.Update(updateQuery, new
                    {
                        ActivityStatusId = ActionEventStatus.Backdated,
                        ActionedOn = request.backdateActivityTo,
                        CreatedOn = DateTime.Now,
                        //FVN-5974
                        //UserId = request.UserContext.ActionerId,
                        ActivityUpdatedBy = request.UserContext.ActionerId
                    });
                    Repositories.Core.SaveChanges();

                    var activity = Repositories.Core.FindById<ActionEvent>(request.activityId);

                    var activityType = Repositories.Configuration.FindById<Activity>(request.activityTypeId);


                    if (AppSettings.IntegrationClient != IntegrationClient.Standalone)
                    {
                        if (!activity.ActionType.Name.Equals("AssignActivityToEmployee"))
                        {
                            var person = Repositories.Core.FindById<Person>(activity.EntityId);

                            var logrequest = new AssignActivityOrActionRequest
                            {

                                ActivityId = activityType.Id,
                                PersonId = person.Id,
                                ClientId = person.Id.ToString(),
                                UserName = person.User.UserName,
                                FirstName = person.FirstName,
                                LastName = person.LastName,
                                EmailAddress = person.EmailAddress,
                                UserContext = request.UserContext,
                                ActivityTime = activity.ActionedOn
                            };

                            new LoggingHelper(RuntimeContext).LogActionToIntegration(logrequest, ActionTypes.AssignActivityToCandidate, activity.Id, activity.EntityId);

                        }
                        else
                        {
                            var employee = Repositories.Core.FindById<Employee>(activity.EntityId);
                            var intRequest = new AssignEmployeeActivityRequest
                            {
                                ActionedDate = activity.ActionedOn,
                                EmployeeId = employee.Id,
                                ActivityId = activityType.Id,
                                UserContext = request.UserContext
                            };

                            new LoggingHelper(RuntimeContext).LogActionToIntegration(intRequest, ActionTypes.AssignActivityToEmployee, activity.Id, activity.EntityId);
                        }
                    }
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Deletes the action event
        /// </summary>
        /// <param name="request">The request</param>
        /// <returns></returns>
        public DeleteActionEventResponse DeleteActionEvent(DeleteActionEventRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new DeleteActionEventResponse(request);

                //validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    var deleteQuery = new Query(typeof(ActionEvent), Entity.Attribute("Id") == request.actionId);
                    Repositories.Core.Update(deleteQuery, new { ActivityStatusId = ActionEventStatus.Deleted, ActivityDeletedOn = DateTime.Now, ActivityUpdatedBy = request.UserContext.ActionerId });
                    Repositories.Core.SaveChanges();

                    response.isDeleted = true;

                    var activity = Repositories.Core.FindById<ActionEvent>(request.actionId);

                    var activityType = Repositories.Core.FindById<Activity>(request.activityTypeId);

                    if (AppSettings.IntegrationClient != IntegrationClient.Standalone)
                    {
                        if (!activity.ActionType.Name.Equals("AssignActivityToEmployee"))
                        {
                            var person = Repositories.Core.FindById<Person>(activity.EntityId);
                            var logrequest = new AssignActivityOrActionRequest
                            {

                                ActivityId = activityType.Id,
                                PersonId = person.Id,
                                ClientId = person.Id.ToString(),
                                UserName = person.User.UserName,
                                FirstName = person.FirstName,
                                LastName = person.LastName,
                                EmailAddress = person.EmailAddress,
                                UserContext = request.UserContext,
                                ActivityTime = activity.ActionedOn
                            };

                            new LoggingHelper(RuntimeContext).LogActionToIntegration(logrequest, ActionTypes.AssignActivityToCandidate, activity.Id, activity.EntityId);
                        }
                        else
                        {
                            var employee = Repositories.Core.FindById<Employee>(activity.EntityId);
                            var intRequest = new AssignEmployeeActivityRequest
                            {
                                ActionedDate = activity.ActionedOn,
                                EmployeeId = employee.Id,
                                ActivityId = activityType.Id,
                                UserContext = request.UserContext
                            };

                            new LoggingHelper(RuntimeContext).LogActionToIntegration(intRequest, ActionTypes.AssignActivityToEmployee, activity.Id, activity.EntityId);
                        }


                    }
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }
        #endregion

        #region Education Internship Categories

        /// <summary>
        /// Gets the education internship categories.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public GetEducationInternshipCategoriesResponse GetEducationInternshipCategories(GetEducationInternshipCategoriesRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new GetEducationInternshipCategoriesResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.ClientTag | Validate.AccessToken))
                    return response;

                try
                {
                    var categories = Repositories.Library.Query<EducationInternshipCategory>().Select(x => x.AsDto()).ToList();
                    categories.ForEach(c => c.CategorySkills = new List<EducationInternshipSkillDto>());

                    var categoryDictionary = categories.ToDictionary(c => c.Id, c => c);

                    // Get skills in one go, and link them to relevant category, to avoid calling db for each category individually
                    var skills = Repositories.Library.Query<EducationInternshipSkill>().Select(x => x.AsDto()).ToList();
                    skills.ForEach(sk => categoryDictionary[sk.EducationInternshipCategoryId].CategorySkills.Add(sk));

                    response.EducationInternshipCategories = categories.ToList();
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        #endregion

        #region Education Internship Statements

        /// <summary>
        /// Gets the education internship statements.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public GetEducationInternshipStatementsResponse GetEducationInternshipStatements(GetEducationInternshipStatementsRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new GetEducationInternshipStatementsResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                var statements = new List<EducationInternshipStatementDto>();

                try
                {
                    var softwareCategories = (from eis in Repositories.Library.EducationInternshipSkills
                                              join eic in Repositories.Library.EducationInternshipCategories
                                                  on eis.EducationInternshipCategoryId equals eic.Id
                                              where request.Criteria.EducationInternshipSkillIds.Contains(eis.Id)
                                                          && eic.CategoryType == EducationSkillCategories.Software
                                              select eic
                                         ).Distinct().ToList();

                    var generalStatements = (from x in Repositories.Library.EducationInternshipStatements
                                             where request.Criteria.EducationInternshipSkillIds.Contains(x.EducationInternshipSkillId)
                                             select x).ToList();

                    statements.AddRange(generalStatements.Select(statement => new EducationInternshipStatementDto
                    {
                        PastTenseStatement = statement.PastTenseStatement,
                        PresentTenseStatement = statement.PresentTenseStatement
                    }));

                    statements.AddRange(from sc in softwareCategories
                                        let skills = (from x in sc.EducationInternshipSkills
                                                      where request.Criteria.EducationInternshipSkillIds.Contains(x.Id)
                                                      select x.Name).ToList()
                                        let skillsText = string.Join(",", skills)
                                        select new EducationInternshipStatementDto
                                        {
                                            PastTenseStatement = sc.PastTenseStatement.Replace("#SKILLS#", skillsText),
                                            PresentTenseStatement = sc.PresentTenseStatement.Replace("#SKILLS#", skillsText)
                                        });

                    response.EducationInternshipStatements = statements;
                }

                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        #endregion

        #region Job Name From Onet

        /// <summary>
        /// Gets the job name from ROnet.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public JobNameResponse GetJobNameFromROnet(JobNameRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new JobNameResponse(request);

                if (!ValidateRequest(request, response, Validate.ClientTag | Validate.SessionId | Validate.License | Validate.AccessToken))
                    return response;

                response.JobName =
                    Repositories.Library.Query<Data.Library.Entities.Job>().Where(x => x.ROnet == request.RONet).Select(
                        x => x.Name).SingleOrDefault();

                return response;
            }
        }

        /// <summary>
        /// Gets the name of the career area.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public CareerAreaNameResponse GetCareerAreaName(CareerAreaNameRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new CareerAreaNameResponse(request);

                if (!ValidateRequest(request, response, Validate.ClientTag | Validate.SessionId | Validate.License | Validate.AccessToken))
                    return response;

                response.CareerAreaName = Repositories.Library.Query<Data.Library.Entities.CareerArea>().Where(x => x.Id == request.CareerAreaId).Select(
                        x => x.Name).SingleOrDefault();

                return response;
            }
        }

        #endregion

        #region Send Hiring From Tax Credit Email

        /// <summary>
        /// Sends the hiring from tax credit program notification reminder.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public EmailHiringFromTaxCreditProgramNotificationResponse SendHiringFromTaxCreditProgramNotification(EmailHiringFromTaxCreditProgramNotificationRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new EmailHiringFromTaxCreditProgramNotificationResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    const EmailTemplateTypes emailTemplateType = EmailTemplateTypes.HiringFromTaxCreditProgramNotification;

                    var emailTemplate = Helpers.Email.GetEmailTemplate(emailTemplateType);

                    if (emailTemplate.IsNull())
                    {
                        response.SetFailure(FormatErrorMessage(request, ErrorTypes.EmailTemplateNotConfigured));
                        Logger.Info(request.LogData(), string.Format("The email template '{0}' was not found.", emailTemplateType));
                        return response;
                    }

                    if (emailTemplate.Body.IsNull() || (request.EmailTo.IsNullOrEmpty() && emailTemplate.Recipient.IsNullOrEmpty()))
                    {
                        response.SetFailure(FormatErrorMessage(request, ErrorTypes.EmailTemplateNotConfigured));
                        Logger.Info(request.LogData(), string.Format("The email template '{0}' is not configured.", emailTemplateType));
                        return response;
                    }


                    #region build work opportunities tax credit category details

                    var workOpportunitiesTaxCreditHiresDetails = new StringBuilder();

                    if ((request.WorkOpportunitiesTaxCreditHires &
                             WorkOpportunitiesTaxCreditCategories.DesignatedCommunityResidents) ==
                            WorkOpportunitiesTaxCreditCategories.DesignatedCommunityResidents)
                    {
                        workOpportunitiesTaxCreditHiresDetails.Append(LocaliseOrDefault(request,
                                                                                                                                                        "WorkOpportunitiesTaxCreditCategories.DesignatedCommunityResidents",
                                                                                                                                                        "EZ and RRC"));
                    }

                    if ((request.WorkOpportunitiesTaxCreditHires & WorkOpportunitiesTaxCreditCategories.ExFelons) ==
                            WorkOpportunitiesTaxCreditCategories.ExFelons)
                    {
                        if (workOpportunitiesTaxCreditHiresDetails.Length > 0)
                            workOpportunitiesTaxCreditHiresDetails.Append(",");
                        workOpportunitiesTaxCreditHiresDetails.Append(LocaliseOrDefault(request,
                                                                                                                                                        "WorkOpportunitiesTaxCreditCategories.ExFelons",
                                                                                                                                                        "Ex-felons"));
                    }

                    if ((request.WorkOpportunitiesTaxCreditHires &
                             WorkOpportunitiesTaxCreditCategories.LongTermFamilyAssistanceRecipient) ==
                            WorkOpportunitiesTaxCreditCategories.LongTermFamilyAssistanceRecipient)
                    {
                        if (workOpportunitiesTaxCreditHiresDetails.Length > 0)
                            workOpportunitiesTaxCreditHiresDetails.Append(",");
                        workOpportunitiesTaxCreditHiresDetails.Append(LocaliseOrDefault(request,
                                                                                                                                                        "WorkOpportunitiesTaxCreditCategories.LongTermFamilyAssistanceRecipient",
                                                                                                                                                        "Long term family assistance recipients"));
                    }

                    if ((request.WorkOpportunitiesTaxCreditHires & WorkOpportunitiesTaxCreditCategories.SNAPRecipients) ==
                            WorkOpportunitiesTaxCreditCategories.SNAPRecipients)
                    {
                        if (workOpportunitiesTaxCreditHiresDetails.Length > 0)
                            workOpportunitiesTaxCreditHiresDetails.Append(",");
                        workOpportunitiesTaxCreditHiresDetails.Append(LocaliseOrDefault(request,
                                                                                                                                                        "WorkOpportunitiesTaxCreditCategories.SNAPRecipients",
                                                                                                                                                        "SNAP recipients"));
                    }

                    if ((request.WorkOpportunitiesTaxCreditHires & WorkOpportunitiesTaxCreditCategories.SummerYouth) ==
                            WorkOpportunitiesTaxCreditCategories.SummerYouth)
                    {
                        if (workOpportunitiesTaxCreditHiresDetails.Length > 0)
                            workOpportunitiesTaxCreditHiresDetails.Append(",");
                        workOpportunitiesTaxCreditHiresDetails.Append(LocaliseOrDefault(request,
                                                                                                                                                        "WorkOpportunitiesTaxCreditCategories.SummerYouth",
                                                                                                                                                        "Summer youth"));
                    }

                    if ((request.WorkOpportunitiesTaxCreditHires &
                             WorkOpportunitiesTaxCreditCategories.SupplementSecurityIncomeRecipients) ==
                            WorkOpportunitiesTaxCreditCategories.SupplementSecurityIncomeRecipients)
                    {
                        if (workOpportunitiesTaxCreditHiresDetails.Length > 0)
                            workOpportunitiesTaxCreditHiresDetails.Append(",");
                        workOpportunitiesTaxCreditHiresDetails.Append(LocaliseOrDefault(request,
                                                                                                                                                        "WorkOpportunitiesTaxCreditCategories.SupplementSecurityIncomeRecipients",
                                                                                                                                                        "SSI recipients"));
                    }

                    if ((request.WorkOpportunitiesTaxCreditHires &
                             WorkOpportunitiesTaxCreditCategories.TemporaryAssistanceToNeedyFamilyRecipients) ==
                            WorkOpportunitiesTaxCreditCategories.TemporaryAssistanceToNeedyFamilyRecipients)
                    {
                        if (workOpportunitiesTaxCreditHiresDetails.Length > 0)
                            workOpportunitiesTaxCreditHiresDetails.Append(",");
                        workOpportunitiesTaxCreditHiresDetails.Append(LocaliseOrDefault(request,
                                                                                                                                                        "WorkOpportunitiesTaxCreditCategories.TemporaryAssistanceToNeedyFamilyRecipients",
                                                                                                                                                        "TANF recipients"));
                    }

                    if ((request.WorkOpportunitiesTaxCreditHires & WorkOpportunitiesTaxCreditCategories.Veterans) ==
                            WorkOpportunitiesTaxCreditCategories.Veterans)
                    {
                        if (workOpportunitiesTaxCreditHiresDetails.Length > 0)
                            workOpportunitiesTaxCreditHiresDetails.Append(",");
                        workOpportunitiesTaxCreditHiresDetails.Append(LocaliseOrDefault(request,
                                                                                                                                                        "WorkOpportunitiesTaxCreditCategories.Veterans",
                                                                                                                                                        "Veterans"));
                    }

                    if ((request.WorkOpportunitiesTaxCreditHires & WorkOpportunitiesTaxCreditCategories.VocationalRehabilitation) ==
                            WorkOpportunitiesTaxCreditCategories.VocationalRehabilitation)
                    {
                        if (workOpportunitiesTaxCreditHiresDetails.Length > 0)
                            workOpportunitiesTaxCreditHiresDetails.Append(",");
                        workOpportunitiesTaxCreditHiresDetails.Append(LocaliseOrDefault(request,
                                                                                                                                                        "WorkOpportunitiesTaxCreditCategories.VocationalRehabilitation",
                                                                                                                                                        "Vocational rehabilitation"));
                    }

                    #endregion

                    var templateData = new EmailTemplateData
                    {
                        HiringManagerName = request.Name,
                        HiringManagerPhoneNumber = request.PhoneNumber,
                        HiringManagerEmail = request.HiringManagerEmailAddress,
                        JobPostingDate = request.PostedOn.ToString(),
                        WOTC = workOpportunitiesTaxCreditHiresDetails.ToString()
                    };

                    //var email = Helpers.Email.GetEmailTemplatePreview(emailTemplate, templateData);
                    Helpers.Email.SendEmailFromTemplate(emailTemplateType, templateData, request.EmailTo ?? emailTemplate.Recipient, String.Empty, String.Empty);
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        #endregion

        #region Session Services

        public SessionResponse<T> GetSessionValue<T>(SessionRequest<T> request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new SessionResponse<T>(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.SessionId))
                    return response;

                try
                {
                    response.Value = Helpers.Session.GetSessionState(request.SessionId, request.Key, request.UseValueAsDefault ? request.Value : default(T), request.KnownTypes);
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        public SessionResponse<T> SetSessionValue<T>(SessionRequest<T> request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new SessionResponse<T>(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.SessionId))
                    return response;

                try
                {
                    if (request.Value.IsNull())
                        Helpers.Session.RemoveSessionState(request.SessionId, request.Key);
                    else
                        Helpers.Session.SetSessionState(request.SessionId, request.Key, request.Value, request.KnownTypes);

                    response.Value = request.Value;
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }

        }

        public SessionResponse RemoveSessionValue(SessionRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new SessionResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.SessionId))
                    return response;

                try
                {
                    Helpers.Session.RemoveSessionState(request.SessionId, request.Key);
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        public SessionResponse ClearSession(SessionRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new SessionResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.SessionId))
                    return response;

                try
                {
                    Helpers.Session.ClearSessionState(request.SessionId);
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }
        #endregion

        #region Upload Services

        /// <summary>
        /// Gets the validation schema for a specific type
        /// </summary>
        /// <param name="request">The service request contain the schema type</param>
        /// <returns>The response containing the schema Xml</returns>
        public ValidationSchemaResponse GetValidationSchema(ValidationSchemaRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new ValidationSchemaResponse(request);

                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    response.ValidationSchema = Helpers.Validation.GetValidationSchema(request.SchemaType);
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Gets the csv template for a specific schema type
        /// </summary>
        /// <param name="request">The service request contain the schema type</param>
        /// <returns>The response containing the csv template</returns>
        public ValidationSchemaResponse GetCSVTemplateForSchema(ValidationSchemaRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new ValidationSchemaResponse(request);

                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    var validationSchema = Helpers.Validation.GetValidationSchema(request.SchemaType);
                    var root = validationSchema.Root;

                    if (root.IsNotNull())
                    {
                        var template = string.Join(",", root.Descendants("field").Select(element => element.Attribute("displayname").Value));
                        response.Template = string.Concat(template, "\n");
                    }
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Validates a file
        /// </summary>
        /// <param name="request">The request containing the file</param>
        /// <returns>The validation status of the file</returns>
        public UploadFileValidationResponse ValidateUploadFile(UploadFileValidationRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new UploadFileValidationResponse(request);

                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    var userId = request.UserContext.ActionerId;
                    var schemaType = request.SchemaType;

                    var previousIds = Repositories.Core.UploadFiles
                                                                                         .Where(file => file.UserId == userId && file.SchemaType == schemaType && file.ProcessingState != UploadFileProcessingState.Committed && file.UpdatedOn <= DateTime.Now.AddHours(-24))
                                                                                         .Select(file => file.Id)
                                                                                         .ToList();

                    previousIds.ForEach(id => DeleteUploadFile(Repositories.Core, id));

                    var recordType = Helpers.Validation.GetRecordType(request.SchemaType);

                    var uploadFile = new UploadFile
                    {
                        UserId = userId,
                        SchemaType = request.SchemaType,
                        FileType = request.FileType,
                        RecordTypeName = recordType.AssemblyQualifiedName,
                        Status = ValidationRecordStatus.Success,
                        ProcessingState = UploadFileProcessingState.RequiresValidation,
                        FileName = request.FileName,
                        FileData = request.FileBytes,
                        CustomInformation = request.CustomInformation.SerializeJson()
                    };

                    Repositories.Core.Add(uploadFile);
                    Repositories.Core.SaveChanges(true);

                    response.UploadFileId = uploadFile.Id;

                    var message = new UploadFileMessage
                    {
                        UploadFileId = uploadFile.Id,
                        ProcessingState = UploadFileProcessingState.RequiresValidation,
                        IgnoreHeader = request.IgnoreHeader
                    };
                    Helpers.Messaging.Publish(message);
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Gets the processing state of the upload file
        /// </summary>
        /// <param name="request">The request containing the file Id</param>
        /// <returns></returns>
        public UploadFileDetailsResponse GetUploadFileProcessingState(UploadFileDetailsRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new UploadFileDetailsResponse(request);

                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    if (request.UploadFileId.IsNull())
                        throw new Exception("UploadFileId not specificed");

                    var userId = request.UserContext.ActionerId;
                    var fileId = request.UploadFileId.Value;

                    var uploadFile = Repositories.Core.FindById<UploadFile>(fileId);
                    if (uploadFile.UserId != userId)
                        throw new Exception("Invalid access to validation file");

                    response.ProcessingState = uploadFile.ProcessingState;
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Gets the details of a validation file
        /// </summary>
        /// <param name="request">The request containing the file id</param>
        /// <returns>The validation status of the file</returns>
        public UploadFileDetailsResponse GetUploadFileDetails(UploadFileDetailsRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new UploadFileDetailsResponse(request);

                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    if (request.UploadFileId.IsNull())
                        throw new Exception("UploadFileId not specificed");

                    var userId = request.UserContext.ActionerId;
                    var fileId = request.UploadFileId.Value;

                    var uploadFile = Repositories.Core.FindById<UploadFile>(fileId);
                    if (uploadFile.UserId != userId)
                        throw new Exception("Invalid access to validation file");

                    var resultsQuery = uploadFile.UploadRecords.AsQueryable();
                    if (request.ExceptionsOnly)
                        resultsQuery = resultsQuery.Where(record => record.Status != ValidationRecordStatus.Success);

                    resultsQuery = resultsQuery.OrderBy(log => log.RecordNumber);

                    response.ValidationResults = resultsQuery.OrderBy(log => log.RecordNumber).Select(log => log.AsDto()).ToList();

                    if (request.BasicDataOnly)
                    {
                        response.ValidationResults = resultsQuery.Select(record => new UploadRecordDto
                        {
                            Id = record.Id,
                            RecordNumber = record.RecordNumber,
                            Status = record.Status,
                            ValidationMessage = record.ValidationMessage,
                            UploadFileId = record.UploadFileId
                        }).ToList();
                    }
                    else
                    {
                        response.ValidationResults = resultsQuery.Select(record => record.AsDto()).ToList();
                    }
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Commits the file after validation
        /// </summary>
        /// <param name="request">The request containing the file id</param>
        /// <returns>The outcome of the commit</returns>
        public UploadFileCommitResponse CommitUploadFile(UploadFileCommitRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new UploadFileCommitResponse(request);

                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    var userId = request.UserContext.ActionerId;
                    var fileId = request.UploadFileId;

                    var uploadFile = Repositories.Core.FindById<UploadFile>(fileId);
                    if (uploadFile.UserId != userId)
                        throw new Exception("Invalid access to validation file");

                    uploadFile.ProcessingState = UploadFileProcessingState.RequiresCommitting;
                    Repositories.Core.SaveChanges(true);

                    var message = new UploadFileMessage
                    {
                        UploadFileId = uploadFile.Id,
                        ProcessingState = UploadFileProcessingState.RequiresCommitting
                    };
                    Helpers.Messaging.Publish(message);
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Deletes an upload file
        /// </summary>
        /// <param name="coreRepository">The core repository.</param>
        /// <param name="uploadFileId">The id of the file to delete</param>
        private static void DeleteUploadFile(ICoreRepository coreRepository, long uploadFileId)
        {
            var uploadFile = coreRepository.FindById<UploadFile>(uploadFileId);
            var recordQuery = new Query(typeof(UploadRecord), Entity.Attribute("UploadFileId") == uploadFileId);

            coreRepository.Remove(recordQuery);
            coreRepository.Remove(uploadFile);
            coreRepository.SaveChanges();
        }

        #endregion

        #region Integration

        /// <summary>
        /// Gets the case management record.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public CaseManagementResponse GetCaseManagementRecord(CaseManagementRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new CaseManagementResponse(request);
                if (!ValidateRequest(request, response, Validate.All))
                    return response;
                try
                {
                    FailedIntegrationMessage item =
                        Repositories.Core.FindById<FailedIntegrationMessage>(request.FailedIntegrationMessageId);
                    response.CaseManagementMessage = item.AsDto();
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Gets the case management errors.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public CaseManagementResponse GetCaseManagementRecords(CaseManagementRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new CaseManagementResponse(request);

                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    var criteria = request.Criteria;

                    var query = Repositories.Core.Query<FailedIntegrationMessage>();

                    if (criteria.Resent)
                        query = query.Where(x => x.DateResent.HasValue);

                    if (criteria.ErrorDescription.IsNotNullOrEmpty())
                        query = query.Where(x => x.ErrorDescription.Contains(criteria.ErrorDescription));

                    if (criteria.MessageType.IsNotNull())
                    {
                        query = query.Where(x => x.IntegrationPoint == criteria.MessageType || x.MessageType == criteria.MessageType.ToString());
                    }

                    if (criteria.IncludeIgnored)
                        query = query.Where(log => log.IsIgnored);

                    if (criteria.Pending)
                        query = query.Where(x => !x.IsIgnored && x.DateResent == null);

                    if (criteria.DateSubmittedFrom.IsNotNull() && criteria.DateSubmittedTo.IsNotNull())
                        query = query.Where(x => x.DateSubmitted >= criteria.DateSubmittedFrom && x.DateSubmitted <= criteria.DateSubmittedTo);

                    if (criteria.OrderBy.IsNotNullOrEmpty() && criteria.OrderBy.Trim().IsNotNullOrEmpty())
                        query = query.OrderBy(criteria.OrderBy);
                    else
                        query = query.OrderBy(x => x.DateSubmitted);

                    switch (request.Criteria.FetchOption)
                    {
                        case CriteriaBase.FetchOptions.PagedList:
                            response.CaseManagementMessagesPaged = query.GetPagedList(x => x.AsDto(), request.Criteria.PageIndex, request.Criteria.PageSize);
                            break;
                        case CriteriaBase.FetchOptions.List:
                            response.CaseManagementMessages = query.Select(x => x.AsDto()).ToList();
                            break;
                        default:
                            var record = query.SingleOrDefault();
                            if (record.IsNotNull())
                                response.CaseManagementMessage = record.AsDto();
                            break;
                    }
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Sends a command to the case management system to ignore a message
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public CaseManagementCommandResponse IgnoreCaseManagementMessage(CaseManagementCommandRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new CaseManagementCommandResponse(request);

                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    var failedRecord = Repositories.Core.FindById<FailedIntegrationMessage>(request.Id);
                    if (failedRecord.IsIgnored)
                    {
                        response.Success = false;
                    }
                    else
                    {
                        failedRecord.IsIgnored = true;
                        Repositories.Core.SaveChanges(true);
                        response.Success = true;
                    }
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Sends a command to the case management system to resend a message
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public CaseManagementCommandResponse ResendCaseManagementMessage(CaseManagementCommandRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new CaseManagementCommandResponse(request);

                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    var failedRecord = Repositories.Core.FindById<FailedIntegrationMessage>(request.Id);
                    if (failedRecord.DateResent.IsNotNull())
                    {
                        response.Success = false;
                    }
                    else
                    {
                        var message = failedRecord.Request.DeserializeJson<IntegrationRequestMessage>();
                        message.Id = Framework.Messaging.Message.NewId();

                        var integrationRequest = GetNonNullRequest(message);
                        integrationRequest.RequestId = Guid.NewGuid();
                        var integrationRequestProperty = message.GetType().GetProperties().SingleOrDefault(p => p.PropertyType == integrationRequest.GetType());
                        integrationRequestProperty.SetValue(message, integrationRequest, null);

                        Helpers.Messaging.Publish(message, MessagePriority.High);

                        response.Success = true;
                        failedRecord.DateResent = DateTime.Now;
                    }

                    Repositories.Core.SaveChanges(true);
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Gets the integration request responses.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public GetIntegrationRequestResponsesResponse GetIntegrationRequestResponses(GetIntegrationRequestResponsesRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new GetIntegrationRequestResponsesResponse(request);

                try
                {
                    var caseManagementError = Repositories.Core.FindById<FailedIntegrationMessage>(request.CaseManagementErrorId);
                    var failedRequestJson = caseManagementError.Request;
                    var message = JsonConvert.DeserializeObject<IntegrationRequestMessage>(failedRequestJson);
                    var integrationRequest = GetNonNullRequest(message);
                    var requestId = integrationRequest.IsNotNull() && integrationRequest.RequestId.HasValue ? integrationRequest.RequestId.Value : Guid.Empty;
                    var log = Repositories.Core.Query<IntegrationLog>().FirstOrDefault(i => i.RequestId == requestId);
                    if (log.IsNull()) return response;
                    var integrationLogId = log.Id;

                    response.RequestResponseMessages =
                        Repositories.Core.Query<IntegrationRequestResponse>()
                        .Where(x => x.IntegrationLogId == integrationLogId)
                        .OrderByDescending(x => x.RequestSentOn)
                        .Join(Repositories.Core.Query<IntegrationRequestResponseMessages>(), x => x.Id, x => x.IntegrationRequestResponseId, (rr, rrm) => rrm)
                        .Select(ObfuscateRequestResponse).ToList();
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Obfuscates the request response.
        /// </summary>
        /// <param name="original">The original.</param>
        /// <returns></returns>
        private IntegrationRequestResponseMessagesDto ObfuscateRequestResponse(IntegrationRequestResponseMessages original)
        {
            if (AppSettings.IntegrationNodesAndAttributesToObfuscate.IsNullOrEmpty()) return original.AsDto();
            var obfuscated = original.AsDto();
            obfuscated.RequestMessage = obfuscated.RequestMessage.ObfuscateXmlAndJson(AppSettings.IntegrationNodesAndAttributesToObfuscate.Split(';'));
            obfuscated.ResponseMessage = obfuscated.ResponseMessage.ObfuscateXmlAndJson(AppSettings.IntegrationNodesAndAttributesToObfuscate.Split(';'));
            return obfuscated;
        }

        /// <summary>
        /// Gets the non null request.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <returns></returns>
        private IntegrationRequest GetNonNullRequest(IntegrationRequestMessage message)
        {
            var properties = message.GetType().GetProperties();
            IntegrationRequest request = null;
            foreach (var prop in properties)
            {
                request = prop.GetValue(message, null) as IntegrationRequest;
                if (request != null) break;
            }
            return request;
        }

        #endregion

        #region Action
        /// <summary>
        /// Assigns the actiontype.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public AssignActivityOrActionResponse AssignAction(AssignActivityOrActionRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new AssignActivityOrActionResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                return AssignAction(request, response);
            }
        }

        public AssignActivityOrActionResponse AssignAction(AssignActivityOrActionRequest request, AssignActivityOrActionResponse response)
        {
            if (request == null) throw new ArgumentNullException("candidateRequest");
            if (response == null) throw new ArgumentNullException("response");

            //TODO: The line below will never throw since ActionTypeId is a value type and can never be null. Should we be checking this value for 0?
            //if (request.ActionTypeId == null) throw new ArgumentNullException("candidateRequest.ActionTypeId");

            try
            {
                if (request.PersonId == 0)
                {
                    response.SetFailure(ErrorTypes.PersonDetailsNotSupplied,
                        FormatErrorMessage(request, ErrorTypes.PersonDetailsNotSupplied));
                    Logger.Info(request.LogData(), "No person ID provided.");
                    return response;
                }

                var person = Repositories.Core.FindById<Person>(request.PersonId);

                if (person.IsNull())
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.PersonNotFound));
                    Logger.Info(request.LogData(), string.Format("The person '{0}' was not found.", request.PersonId));

                    return response;
                }

                response.FocusId = LogAction(request, (ActionTypes)request.ActionTypeId, typeof(Person).Name, person.Id,
                actionedOn: request.ActivityTime, overrideUserId: request.AssigningUserId, overrideCreatedOn: request.OverrideCreatedOn);
            }
            catch (Exception ex)
            {
                response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                Logger.Error(request.LogData(), response.Message, response.Exception);
            }

            return response;
        }
        #endregion

        #region Documents

        /// <summary>
        /// Gets the documents.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public GetDocumentsResponse GetDocuments(GetDocumentsRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new GetDocumentsResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    if (request.Criteria.IsNull())
                    {
                        response.SetFailure(FormatErrorMessage(request, ErrorTypes.DocumentCriteriaNull));
                        Logger.Info(request.LogData(), "Document criteria object is null");

                        return response;
                    }

                    var query = new DocumentQuery(Repositories.Core, request.Criteria);

                    if (!request.Grouped && request.Criteria.FetchOption != CriteriaBase.FetchOptions.Single)
                        response.Documents = query.Query().Select(x => x.AsDto()).ToList();
                    else if (request.Grouped && request.Criteria.FetchOption != CriteriaBase.FetchOptions.Single)
                    {
                        var list = query.Query().Select(x => x.AsDto()).ToList();

                        response.GroupedDocuments = list.GroupBy(doc => doc.Group,
                                            doc => doc,
                                             (key, grp) => new GroupedDocumentsModel
                                             {
                                                 Group = key,
                                                 GroupName = Helpers.Lookup.GetLookupText(LookupTypes.DocumentGroups, key),
                                                 Documents = grp.ToList()
                                             }).ToList();
                    }
                    else if (request.Criteria.FetchOption == CriteriaBase.FetchOptions.Single)
                    {
                        response.Document = query.Query().Select(x => x.AsDto()).FirstOrDefault();
                    }
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Uploads the document.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public GetDocumentsResponse UploadDocument(GetDocumentsRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new GetDocumentsResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    #region Validation

                    var errors = new List<ErrorTypes>();

                    if (request.Document.Id.IsNullOrZero() && request.Document.File.IsNullOrEmpty())
                        errors.Add(ErrorTypes.FileNotSpecified);

                    if (request.Document.Title.IsNullOrEmpty())
                        errors.Add(ErrorTypes.TitleNotSpecified);

                    if (request.Document.Description.IsNotNullOrEmpty() && request.Document.Description.Length > 2000)
                        errors.Add(ErrorTypes.DescriptionTooLong);

                    if (request.Document.Category.IsNull())
                        errors.Add(ErrorTypes.CategoryNotSpecified);

                    if (request.Document.Group.IsNull())
                        errors.Add(ErrorTypes.GroupNotSpecified);

                    if (request.Document.Module.IsNull())
                        errors.Add(ErrorTypes.ModuleNotSpecified);

                    if (errors.Count > 0)
                    {
                        response.SetFailure(FormatErrorMessage(request, ErrorTypes.ValidationErrorsOccurred));
                        Logger.Info(request.LogData(), "Document validation errors occurred; unable to upload document");

                        response.Errors = errors;

                        return response;
                    }

                    #endregion

                    var doc = new Document();

                    // If editing an existing document
                    if (request.Document.Id.IsNotNullOrZero())
                    {
                        doc = Repositories.Core.FindById<Document>(request.Document.Id);

                        doc.Title = request.Document.Title;
                        doc.Description = request.Document.Description;
                        doc.Group = request.Document.Group;
                        doc.Category = request.Document.Category;
                        doc.Module = request.Document.Module;

                        // Replacing the document
                        if (request.Document.File.IsNotNullOrEmpty() && request.Document.FileName.IsNotNullOrEmpty())
                        {
                            doc.File = request.Document.File;
                            doc.FileName = request.Document.FileName;
                        }
                    }
                    // Get the next order number if it's a new document being added
                    if ((doc.IsNull() || doc.Id == 0) && (request.Document.Order.IsNull() || request.Document.Order == 0))
                    {
                        var maxOrder = Repositories.Core.Documents.Where(x => x.Group == request.Document.Group).OrderByDescending(x => x.Order).Select(x => x.Order).FirstOrDefault();

                        request.Document.Order = maxOrder + 1;
                    }

                    if (request.Document.Id.IsNullOrZero())
                        Repositories.Core.Add(request.Document.CopyTo());

                    Repositories.Core.SaveChanges();
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Deletes the document.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public GetDocumentsResponse DeleteDocument(GetDocumentsRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new GetDocumentsResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    if (request.Criteria.IsNull())
                    {
                        response.SetFailure(FormatErrorMessage(request, ErrorTypes.DocumentCriteriaNull));
                        Logger.Info(request.LogData(), "Document criteria object is null");

                        return response;
                    }

                    if (request.Criteria.DocumentId.IsNullOrZero())
                    {
                        response.SetFailure(FormatErrorMessage(request, ErrorTypes.DocumentIdNotSpecified));
                        Logger.Info(request.LogData(), "Document ID not specified");
                        return response;
                    }

                    var document = Repositories.Core.FindById<Document>(request.Criteria.DocumentId);

                    if (document.IsNull())
                    {
                        response.SetFailure(FormatErrorMessage(request, ErrorTypes.DocumentNotFound));
                        Logger.Info(request.LogData(), string.Format("Document with ID {0} not found", request.Criteria.DocumentId));
                        return response;
                    }

                    Repositories.Core.Remove(document);
                    Repositories.Core.SaveChanges();
                }
                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        #endregion
    }
}
