﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;

using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.IntegrationMessages;
using Focus.Core.Messages.OrganizationService;
using Focus.Core.Models.Career;
using Focus.Data.Core.Entities;
using Focus.Services.DtoMappers;
using Focus.Services.Mappers;
using Focus.Core;
using Focus.Services.Core;
using Focus.Services.Messages;
using Focus.Services.ServiceContracts;

using Framework.Core;
using Framework.Logging;

using AcknowledgementType = Focus.Core.Messages.AcknowledgementType;

#endregion

namespace Focus.Services.ServiceImplementations
{
    public class OrganizationService : ServiceBase, IOrganizationService
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="OrganizationService" /> class.
        /// </summary>
        public OrganizationService()
            : this(null)
        { }

        /// <summary>
        /// Initializes a new instance of the <see cref="OrganizationService" /> class.
        /// </summary>
        /// <param name="runtimeContext">The runtime context.</param>
        public OrganizationService(IRuntimeContext runtimeContext)
            : base(runtimeContext)
        { }

        /// <summary>
        /// Dont show the specified job in the list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public DontDisplayJobResponse DontShowJob(DontDisplayJobRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new DontDisplayJobResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    var excludedJob = new ExcludedLensPosting
                                        {
                                            LensPostingId = request.LensPostingId,
                                            PersonId = Convert.ToInt64(request.UserContext.PersonId)
                                        };

                    Repositories.Core.Add(excludedJob);
                    Repositories.Core.SaveChanges(true);

                    LogAction(request, ActionTypes.AddExcludedPosting, excludedJob);
                    response.Acknowledgement = AcknowledgementType.Success;
                }

                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Refers the specified request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException"></exception>
        public ApplyForJobResponse ApplyForJob(ApplyForJobRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new ApplyForJobResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    if (request.LensPostingId.IsNullOrEmpty())
                    {
                        response.SetFailure(FormatErrorMessage(request, ErrorTypes.LensPostingIdNotProvided));
                        Logger.Info(request.LogData(), "The no lens posting id provided.");
                        return response;
                    }

                    if (request.ResumeId.IsNull())
                    {
                        response.SetFailure(FormatErrorMessage(request, ErrorTypes.ResumeIdNotProvided));
                        Logger.Info(request.LogData(), "The no resume id provided.");
                        return response;
                    }

                    var posting = Repositories.Core.Postings.FirstOrDefault(x => x.LensPostingId == request.LensPostingId);

                    if (posting.IsNull())
                    {
                        response.SetFailure(FormatErrorMessage(request, ErrorTypes.PostingNotFound));
                        Logger.Info(request.LogData(), string.Format("The posting with lens id {0} could not be found.", request.LensPostingId));
                        return response;
                    }

                    var resume = Repositories.Core.FindById<Resume>(request.ResumeId);

                    if (resume.IsNull())
                    {
                        response.SetFailure(FormatErrorMessage(request, ErrorTypes.ResumeNotFound));
                        Logger.Info(request.LogData(), string.Format("The resume {0} could not be found.", request.ResumeId));
                        return response;
                    }

                    if (posting.StatusId != PostingStatuses.Active && !request.IsMigratedReferral)
                    {
                        response.SetFailure(FormatErrorMessage(request, ErrorTypes.PostingNotActive));
                        Logger.Info(request.LogData(), string.Format("The posting {0} is not active.", posting.Id));
                        return response;
                    }

                    ApplicationDto application = null;
                    var reconsiderApplication = false;

                    // Only allow an application to be updated if the original one has been denied
                    if (posting.Applications.IsNotNullOrEmpty())
                    {
                        application = posting.Applications.Where(x => x.Resume.PersonId == resume.PersonId).Select(x => x.AsDto()).FirstOrDefault();

                        if (application.IsNotNull() && application.ApprovalStatus != ApprovalStatuses.Rejected)
                        {
                            response.SetFailure(FormatErrorMessage(request, ErrorTypes.ApplicationAlreadyMade));
                            Logger.Info(request.LogData(), string.Format("Application already made for posting {0} by person {1}.", posting.Id, resume.PersonId));
                            return response;
                        }

                        if (application.IsNotNull() && application.ApprovalStatus == ApprovalStatuses.Rejected)
                            reconsiderApplication = true;
                    }


                    if (request.ReviewApplication && (!request.UserContext.IsShadowingUser))
                    {
                        request.IsEligible = true;

                        if (AppSettings.ReviewApplicationEligibilityOriginIds.IsNotNullOrEmpty() && AppSettings.ReviewApplicationEligibilityOriginIds.Any(x => x == posting.OriginId))
                        {
                            // Check the resume is complete
                            if (resume.ResumeCompletionStatusId != ResumeCompletionStatuses.Completed)
                            {
                                request.IsEligible = false;
                                request.Notes = "Resume incomplete";
                            }

                            // Check the match score
                            if (AppSettings.ReferralMinStars.IsNotNull() && AppSettings.StarRatingMinimumScores.IsNotNullOrEmpty() && (AppSettings.StarRatingMinimumScores.Count() > AppSettings.ReferralMinStars))
                            {
                                request.MatchingScore = Repositories.Lens.Match(resume.ResumeXml, posting.PostingXml.GetJobDoc());

                                var minimumMatchScore = AppSettings.StarRatingMinimumScores[AppSettings.ReferralMinStars];

                                if (request.MatchingScore < minimumMatchScore)
                                {
                                    request.IsEligible = false;
                                    request.Notes = string.Format("Matching score should be at least {0}.", minimumMatchScore);
                                }
                            }
                        }
                    }

                    // Save the application
                    if (application.IsNull())
                        application = new ApplicationDto();

                    var currentStatus = (application.ApplicationStatus.IsNotNull()) ? application.ApplicationStatus : ApplicationStatusTypes.NotApplicable;

                    application.PostingId = posting.Id;
                    application.ResumeId = resume.Id;

                    // Assumption : Only for talent jobs will jobseekers enter the approval queue
                    application.ApprovalStatus = ((!request.IsEligible) && posting.JobId.IsNotNull() && AppSettings.JobSeekerReferralsEnabled) ? ApprovalStatuses.WaitingApproval : ApprovalStatuses.Approved;
                    application.ApprovalRequiredReason = request.Notes;

                    if (reconsiderApplication)
                    {
                        if (request.UserContext.IsShadowingUser)
                        {
                            application.ApprovalStatus = ApprovalStatuses.Approved;
                            application.ApplicationStatus = ApplicationStatusTypes.NewApplicant;
                        }
                        else
                        {
                            application.ApprovalStatus = ApprovalStatuses.Reconsider;
                            application.ApplicationStatus = ApplicationStatusTypes.ReconsiderReferral;
                        }
                    }
                    else
                        application.ApplicationStatus = (request.IsEligible) ? ApplicationStatusTypes.NewApplicant : ApplicationStatusTypes.SelfReferred;

										if (request.MatchingScore.HasValue)
											application.ApplicationScore = request.MatchingScore.Value;
                    application.StatusLastChangedOn = DateTime.Now;
                    application.AutomaticallyApproved = application.ApprovalStatus == ApprovalStatuses.Approved;

                    Application applicationEntity;
                    var newApplication = false;

                    if (application.Id.HasValue)
                    {
                        applicationEntity = Repositories.Core.FindById<Application>(application.Id);
                        applicationEntity = application.CopyTo(applicationEntity);
                    }
                    else
                    {
                        newApplication = true;

                        applicationEntity = new Application();
                        applicationEntity = application.CopyTo(applicationEntity);
                        Repositories.Core.Add(applicationEntity);
                    }

										var postingOfInterestSent = Repositories.Core.Query<JobPostingOfInterestSent>().FirstOrDefault(x => x.PersonId == resume.PersonId && x.PostingId == posting.Id);
										if (postingOfInterestSent.IsNotNull())
										{
											postingOfInterestSent.Applied = true;
										}

                    Repositories.Core.SaveChanges(true);

                    response.ApplicationId = applicationEntity.Id;

                    // Save application in client system
                    if (application.ApprovalStatus == ApprovalStatuses.Approved && AppSettings.IntegrationClient != IntegrationClient.Standalone)
                    {
                        var integrationRequest = new ReferJobSeekerRequest
                        {
                            ExternalJobId = posting.ExternalId,
                            PostingId = posting.Id,
                            JobSeekerId = resume.Person.User.ExternalId,
														ExternalAdminId = RuntimeContext.CurrentRequest.UserContext.ExternalUserId,
														ExternalOfficeId = RuntimeContext.CurrentRequest.UserContext.ExternalOfficeId,
														ExternalPassword = RuntimeContext.CurrentRequest.UserContext.ExternalPassword,
														ExternalUsername = RuntimeContext.CurrentRequest.UserContext.ExternalUserName
                        };

                        Helpers.Messaging.Publish(new IntegrationRequestMessage
                        {
                            ActionerId = request.UserContext.UserId,
                            ReferJobSeekerRequest = integrationRequest,
                            IntegrationPoint = IntegrationPoint.ReferJobSeeker
                        });

                        //var clientResponse = Repositories.Client.ReferJobSeekerForPosting(request.ToReferJobSeekerForPostingRequest(resume.Person.User.ExternalId, posting.ExternalId));

                        //if (clientResponse.Acknowledgement == Integration.Client.AcknowledgementType.Failure)
                        //  throw new Exception("Updating referal in client system, failed : " + clientResponse.Message, clientResponse.Exception);

                        //if (!clientResponse.ReferralSuccesful)
                        //{
                        //  response.SetFailure(FormatErrorMessage(request, ErrorTypes.IntegrationReferJobSeekerForJobFailure));
                        //  Logger.Info(request.LogData(), string.Format("The resume {0} could not be referred for posting {1} in the client system.", resume.Id, posting.Id));
                        //  return response;
                        //}
                    }

                    // Log action and statuses
                    LogStatusChange(request, typeof(Application).Name, applicationEntity.Id, (long)currentStatus, (long)applicationEntity.ApplicationStatus);

                    LogAction(request, ActionTypes.SaveApplication, typeof(Application).Name, applicationEntity.Id, posting.Id, resume.Id, application.ApplicationStatus.ToString());

                    if (request.WaivedRequirements.IsNotNullOrEmpty())
                        LogAction(request, ActionTypes.WaivedRequirements, typeof(Application).Name, applicationEntity.Id, posting.JobId, resume.PersonId, string.Join(", ", request.WaivedRequirements));

                    if (reconsiderApplication)
                    {
                        LogAction(request, ActionTypes.ReapplyReferralRequest, applicationEntity);
                    }
                    if (applicationEntity.ApprovalStatus == ApprovalStatuses.WaitingApproval)
                    {
                        LogAction(request, ActionTypes.ReferralRequest, applicationEntity);
                    }
										else if (newApplication)
										{
											if (request.UserContext.IsShadowingUser)
											{
												var person = resume.Person;
												LogAction(request, ActionTypes.CreateCandidateApplication, typeof(Application).Name, applicationEntity.Id, posting.JobId, person.Id, String.Format("{0} {1}", person.FirstName, person.LastName));
											}
											else
											{
												var postingEntityType = Repositories.Core.EntityTypes.Where(e => e.Name == "Posting").Select(e => e.Id).FirstOrDefault();
												if (!Repositories.Core.ActionEvents.Any(x => x.ActionType.Name == ActionTypes.SelfReferral.ToString() && x.EntityTypeId == postingEntityType && x.EntityId == posting.Id && x.EntityIdAdditional01 == resume.PersonId))
													LogAction(request, ActionTypes.SelfReferral, applicationEntity);
											}
										}
                    Helpers.SelfService.PublishSelfServiceActivity(ActionTypes.SelfReferral, request.UserContext.ActionerId, request.UserContext.UserId);
                    response.Acknowledgement = AcknowledgementType.Success;
                }

                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Gets the Application list.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public ApplicationListResponse GetApplicationList(ApplicationListRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new ApplicationListResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    // Get user's applcations
                    var applications = (from a in Repositories.Core.Applications
                                        join p in Repositories.Core.Postings on a.PostingId equals p.Id
                                        join r in Repositories.Core.Resumes on a.ResumeId equals r.Id
                                        where a.CreatedOn >= request.FromDate &&
                                        a.CreatedOn <= request.ToDate &&
                                        r.PersonId == request.UserContext.PersonId
                                        select new ReferralPosting { Id = a.Id, LensPostingId = p.LensPostingId, PostingStatus = p.StatusId, ApprovalStatus = a.ApprovalStatus }).ToList();

                    response.ReferralPostings = applications;
                    response.Acknowledgement = AcknowledgementType.Success;
                }

                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Matches the resume to posting.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public MatchResponse MatchResumeToPosting(MatchRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new MatchResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    if (request.LensPostingId.IsNullOrEmpty())
                    {
                        response.SetFailure(FormatErrorMessage(request, ErrorTypes.LensPostingIdNotProvided));
                        Logger.Info(request.LogData(), "The no lens posting id provided.");
                        return response;
                    }

                    if (request.ResumeId.IsNull())
                    {
                        response.SetFailure(FormatErrorMessage(request, ErrorTypes.ResumeIdNotProvided));
                        Logger.Info(request.LogData(), "The no resume id provided.");
                        return response;
                    }

                    var posting = Repositories.Core.Postings.FirstOrDefault(x => x.LensPostingId == request.LensPostingId);

                    if (posting.IsNull())
                    {
                        response.SetFailure(FormatErrorMessage(request, ErrorTypes.PostingNotFound));
                        Logger.Info(request.LogData(), string.Format("The posting with lens id {0} could not be found.", request.LensPostingId));
                        return response;
                    }

                    var resume = Repositories.Core.FindById<Resume>(request.ResumeId);

                    if (resume.IsNull())
                    {
                        response.SetFailure(FormatErrorMessage(request, ErrorTypes.ResumeNotFound));
                        Logger.Info(request.LogData(), string.Format("The resume {0} could not be found.", request.ResumeId));
                        return response;
                    }

                    response.Score = Repositories.Lens.Match(resume.ResumeXml, posting.PostingXml.GetJobDoc());
                    response.Acknowledgement = AcknowledgementType.Success;
                }

                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }

        /// <summary>
        /// Jobs the information.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        public JobInformationResponse JobInformation(JobInformationRequest request)
        {
            using (Profiler.Profile(request.LogData(), request))
            {
                var response = new JobInformationResponse(request);

                // Validate request
                if (!ValidateRequest(request, response, Validate.All))
                    return response;

                try
                {
                    var postingDto = Repositories.Core.Postings.Where(x => x.StatusId == PostingStatuses.Active && x.LensPostingId == request.JobLensId).Select(x => x.AsDto()).FirstOrDefault();

                    if (postingDto.IsNull())
                    {
                        response.SetFailure(FormatErrorMessage(request, ErrorTypes.PostingNotFound));
                        Logger.Info(request.LogData(), string.Format("The Posting '{0}' was not found.", request.JobLensId));

                        return response;
                    }

                    response.JobInformation = postingDto.ToPostingEnvelope(RuntimeContext);

                    response.Acknowledgement = AcknowledgementType.Success;
                }

                catch (Exception ex)
                {
                    response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
                    Logger.Error(request.LogData(), response.Message, response.Exception);
                }

                return response;
            }
        }
    }
}
