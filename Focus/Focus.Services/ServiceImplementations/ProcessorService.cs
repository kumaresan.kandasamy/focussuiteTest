﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Focus.Core;
using Focus.Core.EmailTemplate;
using Focus.Core.Messages.ProcessorService;
using Focus.Data.Configuration.Entities;
using Focus.Data.Migration.Entities;
using Focus.Data.Report.Entities;
using Focus.Services.Core;
using Focus.Services.Messages;
using Focus.Services.ServiceContracts;
using IRepository = Focus.Data.Repositories.Contracts.IRepository;

using Framework.Caching;
using Framework.Core;
using Framework.Encryption;
using Framework.Logging;

using Mindscape.LightSpeed;
using Mindscape.LightSpeed.Querying;

#endregion

namespace Focus.Services.ServiceImplementations
{
  public class ProcessorService : ServiceBase, IProcessorService
  {
    private delegate void ProcessFieldFunction(AESEncryption encryptor, IRepository repository, string tableName, string fieldName, bool decrypt);

    private class ImportedUserDetails
    {
      public long MigrationId { get; set; }
      public long PersonId { get; set; }
    }

    /// <summary>
		/// Initializes a new instance of the <see cref="ProcessorService" /> class.
		/// </summary>
		public ProcessorService() : this(null)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="ProcessorService" /> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		public ProcessorService(IRuntimeContext runtimeContext) : base(runtimeContext)
		{ }

		/// <summary>
		/// Enqueues the batch process.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public EnqueueBatchResponse EnqueueBatch(EnqueueBatchRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new EnqueueBatchResponse(request);

				// Validate request
        if (!ValidateRequest(request, response, Validate.ClientTag | Validate.AccessToken))
					return response;

				try
				{
          if (request.Identifier.IsNullOrEmpty() && !request.Process.HasValue)
					{
						response.SetFailure("Bad request.");
						return response;
					}

				  BatchProcesses batchProcess;

          if (request.Process.HasValue)
          {
            batchProcess = request.Process.Value;
          }
          else
          {
            var batchSettings = AppSettings.BatchProcessSettings.FirstOrDefault(x => x.Identifier == request.Identifier);

            if (batchSettings.IsNull())
            {
              response.SetFailure("Bad request.");
              return response;
            }

            batchProcess = batchSettings.Process;
          }

					ProcessMessage message;

          switch (batchProcess)
					{
						case BatchProcesses.AboutToExpiredJobs:
							message = new ProcessAboutToExpireJobsMessage();
							break;
						case BatchProcesses.CandidateIssues:
							message = new ProcessCandidateIssuesMessage();
							break;
						case BatchProcesses.EmailAlerts:
							message = new ProcessEmailAlertsMessage();
							break;
						case BatchProcesses.ExpiredJobs:
							message = new ProcessExpiredJobsMessage{ExpiryDays = 1};
							break;
						case BatchProcesses.Reminders:
							message = new ProcessRemindersMessage();
							break;
						case BatchProcesses.SearchAlerts:
							message = new ProcessSearchAlertsMessage();
							break;
            case BatchProcesses.JobIssues:
              message = new ProcessJobIssuesMessage();
					    break;
            case BatchProcesses.JobSeekerReportAges:
              message = new ProcessJobSeekerReportAgesMessage();
              break;
						case BatchProcesses.DenyReferralRequest:
							message = new DenyReferralRequestMessage();
							break;
						case BatchProcesses.Statistics:
							message = new ProcessStatisticsMessage();
							break;
            case BatchProcesses.SessionMaintenance:
              message = new ProcessSessionMaintenanceMessage();
              break;
						case BatchProcesses.MatchesToRecentPlacements:
							message = new ProcessMatchesToRecentPlacementsMessage();
							break;
            case BatchProcesses.QueuedInvitesAndRecommendations:
              message = new ProcessQueuedInvitesAndRecommendationsMessage();
              break;
						default:
							response.SetFailure("Bad request.");
							return response;
					}

				  if (request.Process.HasValue)
				  {
            if (request.Schedule.Interval < 0)
              response.SetFailure("Interval must be greater than zero");
            else if (request.Schedule.Interval == 0)
              Helpers.Messaging.Publish(message);
            else
				      Helpers.Messaging.ScheduleMessage(message, request.Schedule);
				  }
				  else
				  {
				    var messageType = Helpers.Messaging.GetMessageType(message);

            if (messageType.IsNotNull() && messageType.SchedulePlan.IsNotNull())
              response.SetFailure("Process is now a scheduled task on the message bus");
            else
				      Helpers.Messaging.Publish(message);
				  }
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

    /// <summary>
    /// Enqueues the import process.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns>The response</returns>
    public EnqueueImportResponse EnqueueImport(EnqueueImportRequest request)
    {
			using (Profiler.Profile(request.LogData(), request))
			{
        var response = new EnqueueImportResponse(request);

				// Validate request
        if (!ValidateRequest(request, response, Validate.ClientTag | Validate.AccessToken))
					return response;

				try
				{
				  var message = new IntegrationImportMessage
				  {
				    RecordType = request.ImportType
				  };

          Helpers.Messaging.Publish(message);
				}
        catch (Exception ex)
        {
          response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
          Logger.Error(request.LogData(), response.Message, response.Exception);
        }

        return response;
      }
    }

    /// <summary>
    /// Enqueues the report population request
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns>The response</returns>
    public EnqueueReportPopulationResponse EnqueueReportPopulation(EnqueueReportPopulationRequest request)
    {
      using (Profiler.Profile(request.LogData(), request))
      {
        var response = new EnqueueReportPopulationResponse(request);

        // Validate request
        if (!ValidateRequest(request, response, Validate.All))
          return response;

        try
        {
          if (request.EntityType.IsNotIn(
            ReportEntityType.Employer,
            ReportEntityType.EmployerOffice,
            ReportEntityType.JobOrder,
            ReportEntityType.JobOffice,
            ReportEntityType.JobSeeker,
            ReportEntityType.JobSeekerOffice,
            ReportEntityType.StaffMember,
            ReportEntityType.StaffMemberOffice,
            ReportEntityType.PersonsCurrentOffice,
            ReportEntityType.JobSeekerAction,
            ReportEntityType.JobSeekerActivityAssignment,
            ReportEntityType.JobOrderAction,
            ReportEntityType.EmployerAction))
          {
            response.SetFailure("Unhandled Type.");
            return response;
          }

          if (request.EntityType == ReportEntityType.EmployerOffice)
            request.EntityType = ReportEntityType.BusinessUnitOffice;

          var message = new UpdateReportingMessage
          {
            EntityType = request.EntityType,
            EntityDeletion = false,
            ResetAll = request.ResetAll
          };

          if (request.EntityIds.IsNotNull())
            message.EntityIds = request.EntityIds.Select(id => Tuple.Create(id, (long?) null)).ToList();

          Helpers.Messaging.Publish(message);

          if (request.EntityIds.IsNotNull())
          {
            if (request.EntityType == ReportEntityType.StaffMember)
            {
              message = new UpdateReportingMessage
              {
                EntityType = ReportEntityType.StaffMemberOffice,
                EntityIds = message.EntityIds,
                EntityDeletion = false,
                ResetAll = request.ResetAll
              };

              Helpers.Messaging.Publish(message);

              message.EntityIds = request.EntityIds.Select(id => Tuple.Create((long)0, (long?)id)).ToList();

              message = new UpdateReportingMessage
              {
                EntityType = ReportEntityType.PersonsCurrentOffice,
                EntityIds = message.EntityIds,
                EntityDeletion = false,
                ResetAll = request.ResetAll
              };

              Helpers.Messaging.Publish(message);
            }
            else if (request.EntityType == ReportEntityType.JobSeeker)
            {
              message = new UpdateReportingMessage
              {
                EntityType = ReportEntityType.JobSeekerOffice,
                EntityIds = message.EntityIds,
                EntityDeletion = false,
                ResetAll = request.ResetAll
              };

              Helpers.Messaging.Publish(message);

              message = new UpdateReportingMessage
              {
                EntityType = ReportEntityType.JobSeekerAction,
                EntityIds = message.EntityIds,
                EntityDeletion = false,
                ResetAll = request.ResetAll
              };

              Helpers.Messaging.Publish(message);

              message = new UpdateReportingMessage
              {
                EntityType = ReportEntityType.JobSeekerActivityAssignment,
                EntityIds = message.EntityIds,
                EntityDeletion = false,
                ResetAll = request.ResetAll
              };

              Helpers.Messaging.Publish(message);
            }
            else if (request.EntityType == ReportEntityType.JobOrder)
            {
              message = new UpdateReportingMessage
              {
                EntityType = ReportEntityType.JobOffice,
                EntityIds = message.EntityIds,
                EntityDeletion = false,
                ResetAll = request.ResetAll
              };

              Helpers.Messaging.Publish(message);

              message = new UpdateReportingMessage
              {
                EntityType = ReportEntityType.JobOrderAction,
                EntityIds = message.EntityIds,
                EntityDeletion = false,
                ResetAll = request.ResetAll
              };

              Helpers.Messaging.Publish(message);
            }
            else if (request.EntityType == ReportEntityType.Employer)
            {
              message = new UpdateReportingMessage
              {
                EntityType = ReportEntityType.BusinessUnitOffice,
                EntityIds = message.EntityIds,
                EntityDeletion = false,
                ResetAll = request.ResetAll
              };

              Helpers.Messaging.Publish(message);

              message = new UpdateReportingMessage
              {
                EntityType = ReportEntityType.EmployerAction,
                EntityIds = message.EntityIds,
                EntityDeletion = false,
                ResetAll = request.ResetAll
              };

              Helpers.Messaging.Publish(message);
            }
          }
        }
        catch (Exception ex)
        {
          response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
          Logger.Error(request.LogData(), response.Message, response.Exception);
        }

        return response;
      }
    }

    /// <summary>
    /// Resets the cache.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    public ResetCacheResponse ResetCache(ResetCacheRequest request)
    {
      using (Profiler.Profile(request.LogData(), request))
      {
        var response = new ResetCacheResponse(request);

        // Validate request
        if (!ValidateRequest(request, response, Validate.All))
          return response;

        try
        {
          Cacher.Clear();
        }
        catch (Exception ex)
        {
          response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
          Logger.Error(request.LogData(), response.Message, response.Exception);
        }

        return response;
      }
    }

    /// <summary>
    /// Sends emails to imported users.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    public ImportedUsersResponse SendEmailsToImportedUsers(ImportedUsersRequest request)
    {
      using (Profiler.Profile(request.LogData(), request))
      {
        var response = new ImportedUsersResponse(request);

        // Validate request
        if (!ValidateRequest(request, response, Validate.All))
          return response;

        try
        {
          List<ImportedUserDetails> entityIds = null;

          switch (request.RecordType)
          {
            case MigrationEmailRecordType.JobSeeker:
              entityIds = Repositories.Migration.JobSeekerRequests
                                      .Where(js => js.RecordType == MigrationJobSeekerRecordType.JobSeeker && js.Status == MigrationStatus.Processed && js.EmailRequired)
                                      .Take(request.EmailsToSend)
                                      .Select(js => new ImportedUserDetails { MigrationId = js.Id, PersonId = js.FocusId })
                                      .ToList();
              break;

            case MigrationEmailRecordType.Employee:
              entityIds = Repositories.Migration.EmployerRequests
                                      .Where(js => js.RecordType == MigrationEmployerRecordType.Employee && js.Status == MigrationStatus.Processed && js.EmailRequired)
                                      .Take(request.EmailsToSend)
                                      .Select(js => new ImportedUserDetails { MigrationId = js.Id, PersonId = js.FocusId })
                                      .ToList();
              break;
          }

          if (entityIds.IsNotNullOrEmpty())
          {
            var emailsSent = 0;

            var template = new EmailTemplateData
            {
              FreeText = request.RecordType == MigrationEmailRecordType.JobSeeker ? AppSettings.FocusCareerApplicationName : AppSettings.FocusTalentApplicationName,
              Url = request.ResetPasswordUrl
            };

            const int batchSize = 500;
            for (var index = 0; index < entityIds.Count; index += batchSize)
            {
              var batchEntityIds = entityIds.Where((details, checkIndex) => checkIndex >= index && checkIndex < index + batchSize).ToList();

              var peopleIds = batchEntityIds.Select(details => details.PersonId);
              var emailAddresses = Repositories.Core.Persons.Where(person => peopleIds.Contains(person.Id)).Select(person => person.EmailAddress).ToList();
              emailAddresses.ForEach(email =>
              {
                template.EmailAddress = email;
                var preview = Helpers.Email.GetEmailTemplatePreview(EmailTemplateTypes.ImportUserPasswordReset, template);
                //Helpers.Email.SendEmail(email, null, null, preview.Subject, preview.Body);
              });

              emailsSent += emailAddresses.Count;

              var migrationIds = batchEntityIds.Select(details => details.MigrationId).Cast<object>().ToArray();
              switch (request.RecordType)
              {
                case MigrationEmailRecordType.JobSeeker:
                  UpdateMigrationRecords<JobSeekerRequest>(migrationIds);
                  break;
                case MigrationEmailRecordType.Employee:
                  UpdateMigrationRecords<EmployerRequest>(migrationIds);
                  break;
              }
            }

            response.EmailsSent = emailsSent;
          }
        }
        catch (Exception ex)
        {
          response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
          Logger.Error(request.LogData(), response.Message, response.Exception);
        }

        return response;
      }
    }

    /// <summary>
    /// Updates the email required flag of the migration records
    /// </summary>
    /// <typeparam name="T">The type of record to update</typeparam>
    /// <param name="migrationIds">The ids of record to update.</param>
    private void UpdateMigrationRecords<T>(object[] migrationIds)
    {
      var resetQuery = new Query(typeof(T), Entity.Attribute("Id").In(migrationIds));
      Repositories.Migration.Update(resetQuery, new { EmailRequired = false });
      Repositories.Migration.SaveChanges(true);
    }

    /// <summary>
    /// Encrypts fields in the database (when encryption has been enabled).
    /// </summary>
    /// <param name="request">The request containing the field to encrypt.</param>
    /// <returns>A response containing a boolean indicating if the field had already been encrypted</returns>
    public EncryptionResponse EncryptFields(EncryptionRequest request)
    {
      using (Profiler.Profile(request.LogData(), request))
      {
        var response = new EncryptionResponse(request);

        // Validate request
        if (!ValidateRequest(request, response, Validate.License | Validate.ClientTag))
          return response;

        try
        {
          var config = Repositories.Configuration.ConfigurationItems.FirstOrDefault(item => item.Key == "EncryptedFields");
          if (config.IsNull())
          {
            config = new ConfigurationItem
            {
              Key = "EncryptedFields",
              Value = ""
            };
            Repositories.Configuration.Add(config);
          }
          var checkList = config.Value.Split(new[] {','}, StringSplitOptions.RemoveEmptyEntries).ToList();

          var encryptor = new AESEncryption(AppSettings.EncryptionKey, AppSettings.EncryptionVector);

          EncryptField(encryptor, Repositories.Core, "Data.Application.Person", "SocialSecurityNumber", checkList, "person-ssn", ProcessStringField);
					EncryptField(encryptor, Repositories.Core, "Report.JobSeeker", "SocialSecurityNumber", checkList, "jobseeker-ssn", ProcessStringField);

          var newConfigValue = string.Join(",", checkList);
          if (config.Value != newConfigValue)
            config.Value = newConfigValue;
          else
            response.AlreadyProcessed = true;

          Repositories.Configuration.SaveChanges(true);
        }
        catch (Exception ex)
        {
          response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
          Logger.Error(request.LogData(), response.Message, response.Exception);
        }

        return response;
      }
    }

    /// <summary>
    /// Decrypts fields in the database (when encryption has been disabled).
    /// </summary>
    /// <param name="request">The request containing the field to decrypt.</param>
    /// <returns>A response containing a boolean indicating if the field had already been decrypted</returns>
    public EncryptionResponse DecryptFields(EncryptionRequest request)
    {
      using (Profiler.Profile(request.LogData(), request))
      {
        var response = new EncryptionResponse(request);

        // Validate request
        if (!ValidateRequest(request, response, Validate.License | Validate.ClientTag))
          return response;

        try
        {
          var config = Repositories.Configuration.ConfigurationItems.FirstOrDefault(item => item.Key == "EncryptedFields");
          if (config.IsNull())
          {
            response.AlreadyProcessed = true;
            return response;
          }

          var checkList = config.Value.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();

          var encryptor = new AESEncryption(AppSettings.EncryptionKey, AppSettings.EncryptionVector);

          DecryptField(encryptor, Repositories.Core, "Data.Application.Person", "SocialSecurityNumber", checkList, "person-ssn", ProcessStringField);
					DecryptField(encryptor, Repositories.Core, "Report.JobSeeker", "SocialSecurityNumber", checkList, "jobseeker-ssn", ProcessStringField);

          var newConfigValue = string.Join(",", checkList);
          if (config.Value != newConfigValue)
          {
            if (newConfigValue.Length == 0)
              Repositories.Configuration.Remove(config);
            else
              config.Value = newConfigValue;
          }
          else
            response.AlreadyProcessed = true;

          Repositories.Configuration.SaveChanges(true);
        }
        catch (Exception ex)
        {
          response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
          Logger.Error(request.LogData(), response.Message, response.Exception);
        }

        return response;
      }
    }

    /// <summary>
    /// Encrypts a field for a table
    /// </summary>
    /// <param name="encryptor">The object to use to encrypt the data</param>
    /// <param name="repository">The repository in which the date is being encrypted.</param>
    /// <param name="tableName">The name of the table with field</param>
    /// <param name="fieldName">The name of the field</param>
    /// <param name="checkList">A list of fields already processed.</param>
    /// <param name="checkCode">The code of the field.</param>
    /// <param name="processFunction">The process function.</param>
    private void EncryptField(AESEncryption encryptor, IRepository repository, string tableName, string fieldName, List<string> checkList, string checkCode, ProcessFieldFunction processFunction)
    {
      if (!checkList.Contains(checkCode))
      {
        processFunction(encryptor, repository, tableName, fieldName, false);
        checkList.Add(checkCode);
      }
    }


    /// <summary>
    /// Decrypts a field for a table
    /// </summary>
    /// <param name="encryptor">The object to use to encrypt the data</param>
    /// <param name="repository">The repository in which the date is being encrypted.</param>
    /// <param name="tableName">The name of the table with field</param>
    /// <param name="fieldName">The name of the field</param>
    /// <param name="checkList">A list of fields already processed.</param>
    /// <param name="checkCode">The code of the field.</param>
    /// <param name="processFunction">The process function.</param>
    private void DecryptField(AESEncryption encryptor, IRepository repository, string tableName, string fieldName, List<string> checkList, string checkCode, ProcessFieldFunction processFunction)
    {
      if (checkList.Contains(checkCode))
      {
        processFunction(encryptor, repository, tableName, fieldName, true);
        checkList.Remove(checkCode);
      }
    }
    /// <summary>
    /// Encrypts or decrypts a date field for a table
    /// </summary>
    /// <param name="encryptor">The object to use to encrypt the data</param>
    /// <param name="repository">The repository in which the date is being encrypted.</param>
    /// <param name="tableName">The name of the table with a date field</param>
    /// <param name="fieldName">The name of the date field</param>
    /// <param name="decrypt">Whether to decrypt or encrypt</param>
    private void ProcessDateField(AESEncryption encryptor, IRepository repository, string tableName, string fieldName, bool decrypt)
    {
      var sql = new StringBuilder();
      sql.AppendLine("CREATE TABLE #Dates ( OriginalDate VARCHAR(75), EncryptedDate VARCHAR(75) )");

      var datesSql = string.Format("SELECT DISTINCT {0} FROM [{1}] WHERE {0} IS NOT NULL", fieldName, tableName);
      using (var reader = repository.ExecuteDataReader(datesSql))
      {
        while (reader.Read())
        {
          string date, encryptedDate;

          if (decrypt)
          {
            encryptedDate = reader.GetString(0);
            date = encryptor.DecryptString(encryptedDate);
          }
          else
          {
            date = reader.GetString(0);
            encryptedDate = encryptor.EncryptToString(DateTime.Parse(date));
          }

          sql.AppendFormat("INSERT INTO #Dates ( OriginalDate, EncryptedDate ) VALUES ('{0}', '{1}')",
                    date.Replace("'", "''"),
                    encryptedDate.Replace("'", "''"));
          sql.AppendLine();
        }
        reader.Close();
      }

      string dateFieldFrom, dateFieldTo;
      if (decrypt)
      {
        dateFieldFrom = "EncryptedDate";
        dateFieldTo = "OriginalDate";
      }
      else
      {
        dateFieldFrom = "OriginalDate";
        dateFieldTo = "EncryptedDate"; 
      }
      sql.AppendFormat(@"UPDATE P SET {0} = TD.{1}
                          FROM [{2}] P
                          INNER JOIN #Dates TD
                            ON TD.{3} = P.{0}
                          WHERE P.{0} IS NOT NULL", fieldName, dateFieldTo, tableName, dateFieldFrom);

      sql.AppendLine();
      sql.Append("DROP TABLE #Dates");

      repository.ExecuteNonQuery(sql.ToString());
      repository.SaveChanges(true);
    }

    /// <summary>
    /// Encrypts or decrypts a string field for a table
    /// </summary>
    /// <param name="encryptor">The object to use to encrypt the data</param>
    /// <param name="repository">The repository in which the date is being encrypted.</param>
    /// <param name="tableName">The name of the table with the field to encrypt</param>
    /// <param name="fieldName">The name of the field</param>
    /// <param name="decrypt">Whether to decrypt or encrypt</param>
    private void ProcessStringField(AESEncryption encryptor, IRepository repository, string tableName, string fieldName, bool decrypt)
    {
      var sql = new StringBuilder();
      sql.AppendLine("CREATE TABLE #Values ( OriginalValue VARCHAR(75), EncryptedValue VARCHAR(75) )");

      var valuesSql = string.Format("SELECT DISTINCT {0} FROM [{1}] WHERE {0} IS NOT NULL AND {0} <> ''", fieldName, tableName);
      using (var reader = repository.ExecuteDataReader(valuesSql))
      {
        while (reader.Read())
        {
          string value, encryptedValue;

          if (decrypt)
          {
            encryptedValue = reader.GetString(0);
            value = encryptor.DecryptString(encryptedValue);
          }
          else
          {
            value = reader.GetString(0);
            encryptedValue = encryptor.EncryptToString(value);
          }

          sql.AppendFormat("INSERT INTO #Values ( OriginalValue, EncryptedValue ) VALUES ('{0}', '{1}')",
                    value.Replace("'", "''"),
                    encryptedValue.Replace("'", "''"));
          sql.AppendLine();
        }
        reader.Close();
      }

      string valueFieldFrom, valueFieldTo;
      if (decrypt)
      {
        valueFieldFrom = "EncryptedValue";
        valueFieldTo = "OriginalValue";
      }
      else
      {
        valueFieldFrom = "OriginalValue";
        valueFieldTo = "EncryptedValue";
      }
      sql.AppendFormat(@"UPDATE P SET {0} = TD.{1}
                          FROM [{2}] P
                          INNER JOIN #Values TD
                            ON TD.{3} = P.{0}
                          WHERE P.{0} IS NOT NULL AND P.{0} <> ''", fieldName, valueFieldTo, tableName, valueFieldFrom);

      sql.AppendLine();
      sql.Append("DROP TABLE #Values");

      repository.ExecuteNonQuery(sql.ToString());
      repository.SaveChanges(true);
    }
  }
}
