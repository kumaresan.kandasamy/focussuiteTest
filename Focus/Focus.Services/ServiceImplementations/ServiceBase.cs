﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using Focus.Services.Views;
using Framework.Exceptions;
using Focus.Core.Settings.Interfaces;
using Mindscape.LightSpeed;

using Framework.Caching;
using Framework.Core;
using Framework.Core.Attributes;
using Framework.Logging;
using Framework.ServiceLocation;

using Focus.Common.Models;
using Focus.Core;
using Focus.Core.IntegrationMessages;
using Focus.Core.Messages;
using Focus.Core.Messages.CoreService;
using Focus.Core.Models.Career;
using Focus.Core.Views;
using Focus.Data.Configuration.Entities;
using Focus.Data.Core.Entities;
using Focus.Data.Library.Entities;
using Focus.Services.Core;
using Focus.Services.Core.Censorship;
using Focus.Services.Core.ServiceHost;
using Focus.Services.Messages;
using Focus.Services.Repositories;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using CensorshipRule = Focus.Data.Configuration.Entities.CensorshipRule;

#endregion

namespace Focus.Services.ServiceImplementations
{
	[ServiceHostOperations]
	public abstract class ServiceBase
	{
		protected static readonly object SyncObject = new object();

		/// <summary>
		/// Initializes a new instance of the <see cref="ServiceBase"/> class.
		/// </summary>
		protected ServiceBase() : this(null)
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ServiceBase" /> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		protected ServiceBase(IRuntimeContext runtimeContext)
		{
			RuntimeContext = runtimeContext ?? ServiceLocator.Current.Resolve<IRuntimeContext>();
		}

		internal IRuntimeContext RuntimeContext { get; private set; }

		internal IAppSettings AppSettings
		{
			get { return RuntimeContext.AppSettings; }
		}

		internal IRepositories Repositories
		{
			get { return RuntimeContext.Repositories; }
		}

		internal IHelpers Helpers
		{
			get { return RuntimeContext.Helpers; }
		}

		#region Validate Request Functionality

		/// <summary>
		/// Validate 4 security levels for a request: License, ClientTag, SessionId
		/// </summary>
		/// <param name="request">The request message.</param>
		/// <param name="response">The response message.</param>
		/// <param name="validate">The validation that needs to take place.</param>
		/// <returns></returns>
		protected bool ValidateRequest(ServiceRequest request, ServiceResponse response, Validate validate)
		{
			// Poke the current request into the RuntimeContext!
			RuntimeContext.SetRequest(request);

			#region Validate Version Number

			if (AppSettings.ValidateServiceVersion && request.VersionNumber != Constants.SystemVersion)
			{
				response.Acknowledgement = AcknowledgementType.Failure;
				response.Message = FormatErrorMessage(request, ErrorTypes.IncorrectVersionNumber);
				return false;
			}

			#endregion

			#region Validate Access Token

			if ((Validate.AccessToken & validate) == Validate.AccessToken && AppSettings.UsesAccessToken)
			{
				var token = request.AccessToken;
				if (token.IsNull() || !token.IsValid(AppSettings.AccessTokenKey, AppSettings.AccessTokenSecret))
				{
					response.Acknowledgement = AcknowledgementType.Failure;
					response.Message = FormatErrorMessage(request, ErrorTypes.InvalidAccessToken);
					return false;
				}
			}

			#endregion

			#region Validate License.

			if ((Validate.License & validate) == Validate.License)
			{
				var isLicensed = Cacher.Get<bool?>(Constants.CacheKeys.IsLicensedKey);

				if (isLicensed == null || !isLicensed.Value)
				{
					try
					{
						isLicensed = true;
						//isLicensed = LensRepository.IsLicensed();
					}
					catch (Exception ex)
					{
						response.Acknowledgement = AcknowledgementType.Failure;
						response.Message = FormatErrorMessage(request, ErrorTypes.LensNotAvailable);
						response.Exception = ex;
						return false;
					}

					if (isLicensed.Value)
						lock (SyncObject)
						{
							Cacher.Set(Constants.CacheKeys.IsLicensedKey, isLicensed.Value,
								DateTime.Now.AddMinutes(AppSettings.LicenseExpiration));
						}
				}

				if (!isLicensed.Value)
				{
					response.Acknowledgement = AcknowledgementType.Failure;
					response.Message = FormatErrorMessage(request, ErrorTypes.NotLicensedToUseProduct);
					return false;
				}
			}

			#endregion

			#region Validate Client Tag.

			if ((Validate.ClientTag & validate) == Validate.ClientTag)
			{
				if (!AppSettings.ValidClientTags.Contains(request.ClientTag))
				{
					response.Acknowledgement = AcknowledgementType.Failure;
					response.Message = FormatErrorMessage(request, ErrorTypes.UnknownClientTag);
					return false;
				}
			}

			#endregion

			#region Validate session id

			if ((Validate.SessionId & validate) == Validate.SessionId)
			{
				try
				{
					Helpers.Session.ValidateSession(request.SessionId, request.UserContext.UserId);
				}
				catch (SessionException ex)
				{
					response.Acknowledgement = AcknowledgementType.Failure;
					response.Message = ex.Message;
					response.Exception = ex;
					return false;
				}
				catch (Exception ex)
				{
					Logger.Debug("Invalid session id inner exception: " + ex.Message);
					response.Acknowledgement = AcknowledgementType.Failure;
					response.Message = FormatErrorMessage(request, ErrorTypes.InvalidSessionId);
					response.Exception = ex;
					return false;
				}
			}

			#endregion

			#region Validate user credentials

			if ((Validate.UserCredentials & validate) == Validate.UserCredentials)
			{
				var userIdValidationResponse = ValidateUserId(request.UserContext.ActionerId);
				if (!userIdValidationResponse.Valid)
				{
					response.Message = FormatErrorMessage(request, ErrorTypes.LogInRequired);

					switch (userIdValidationResponse.ValidationFailureReason)
					{
						case UserIdValidationFailureReasons.AccountNotEnabled:
							response.SetFailure(ErrorTypes.UserNotEnabled);
							break;

						case UserIdValidationFailureReasons.AccountBlocked:
							response.SetFailure(ErrorTypes.UserBlocked);
							response.Message = FormatErrorMessage(request, ErrorTypes.UserBlocked);
							break;

						default:
							response.SetFailure(ErrorTypes.LogInRequired);
							break;
					}

					return false;
				}

				// If the user has single signed on check the single signed on check the Actioner account is valid
				if (request.UserContext.ActionerId != request.UserContext.UserId)
				{
					var actionerIdValidationResponse = ValidateUserId(request.UserContext.ActionerId);
					if (!actionerIdValidationResponse.Valid)
					{
						response.Acknowledgement = AcknowledgementType.Failure;
						response.Message = FormatErrorMessage(request, ErrorTypes.LogInRequired);
						return false;
					}
				}
			}

			#endregion

			return true;
		}

		/// <summary>
		/// Validates the user id.
		/// </summary>
		/// <param name="userId">The user id.</param>
		/// <returns></returns>
		protected ValidateUserResponse ValidateUserId(long userId)
		{
			var response = new ValidateUserResponse {Valid = true};

			if (userId < 0)
			{
				response.Valid = false;
				response.ValidationFailureReason = UserIdValidationFailureReasons.InvalidUserId;
				return response;
			}

			var user = Repositories.Core.FindById<User>(userId);

			if (user == null)
			{
				response.Valid = false;
				response.ValidationFailureReason = UserIdValidationFailureReasons.AccountDoesNotExist;
				return response;
			}

			if (!user.Enabled)
			{
				response.Valid = false;
				response.ValidationFailureReason = UserIdValidationFailureReasons.AccountNotEnabled;
				return response;
			}

			// For now this has been removed as there is a requirement to be able to access blocked accounts (REL-194).
			//if (user.Blocked)
			//{
			//  response.Valid = false;
			//  response.ValidationFailureReason = UserIdValidationFailureReasons.AccountBlocked;
			//  return response;
			//}

			return response;
		}

		protected class ValidateUserResponse
		{
			public bool Valid { get; set; }
			public UserIdValidationFailureReasons ValidationFailureReason { get; set; }
		}

		#endregion

		#region Error & Logging methods

		/// <summary>
		/// Formats an error message.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <param name="error">The error.</param>
		/// <param name="args">The args.</param>
		/// <returns></returns>
		public string FormatErrorMessage(ServiceRequest request, ErrorTypes error, params object[] args)
		{
			// If there are any args then use them in the message.
			return LocaliseOrDefault(request, "ErrorType." + error, error.GetAttributeValue<EnumLocalisationDefaultAttribute>(),
				args);
		}

		/// <summary>
		/// Logs an action.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <param name="actionType">Type of the action.</param>
		/// <param name="entityName">Name of the entity.</param>
		/// <param name="entityId">The entity id.</param>
		/// <param name="entityIdAdditional01">The entity id additional01.</param>
		/// <param name="entityIdAdditional02">The entity id additional02.</param>
		/// <param name="additionalDetails">The additional details.</param>
		/// <param name="actionedOn">Override the ActionedOn date</param>
		/// <param name="overrideUserId">Whether to override the id of the user performing the action</param>
		protected long LogAction(ServiceRequest request, ActionTypes actionType, string entityName, long? entityId,
			long? entityIdAdditional01 = null, long? entityIdAdditional02 = null, string additionalDetails = null,
			DateTime? actionedOn = null, long? overrideUserId = null, DateTime? overrideCreatedOn = null)
		{
			if (RuntimeContext.CurrentRequest.IsNull())
				RuntimeContext.SetRequest(request);

			var actionId = Helpers.Logging.LogAction(actionType, entityName, entityId, entityIdAdditional01, entityIdAdditional02,
				additionalDetails, actionedOn: actionedOn, overrideUserId: overrideUserId, overrideCreatedOn: overrideCreatedOn);

			return actionId;
		}

		/// <summary>
		/// Logs the actions.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <param name="actionType">Type of the action.</param>
		/// <param name="entityName">Name of the entity.</param>
		/// <param name="entityId">The entity id.</param>
		/// <param name="entityIdAdditionals">The entity id additional01s.</param>
		protected void LogActions(ServiceRequest request, ActionTypes actionType, string entityName, long entityId,
			long[] entityIdAdditionals)
		{
			if (RuntimeContext.CurrentRequest.IsNull())
				RuntimeContext.SetRequest(request);

			Helpers.Logging.LogActions(actionType, entityName, entityId, entityIdAdditionals);
		}

		/// <summary>
		/// Logs the action.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <param name="actionType">Type of the action.</param>
		/// <param name="entityName">Name of the entity.</param>
		/// <param name="entityId">The entity id.</param>
		protected void LogAction(ServiceRequest request, ActionTypes actionType, string entityName, long? entityId)
		{
			if (RuntimeContext.CurrentRequest.IsNull())
				RuntimeContext.SetRequest(request);

			Helpers.Logging.LogAction(actionType, entityName, entityId, null);
		}

		/// <summary>
		/// Logs an action.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <param name="actionType">Type of the action.</param>
		/// <param name="entity">The entity.</param>
		/// <param name="overrideUserId">Override the id of the user for which the action is logged</param>
		/// <param name="actionedOn">Override the ActionedOn date</param>
		protected void LogAction(ServiceRequest request, ActionTypes actionType, Entity<long> entity,
			long? overrideUserId = null, DateTime? actionedOn = null)
		{
			if (RuntimeContext.CurrentRequest.IsNull())
				RuntimeContext.SetRequest(request);

			Helpers.Logging.LogAction(actionType, entity, overrideUserId: overrideUserId, actionedOn: actionedOn);
		}

		/// <summary>
		/// Logs the status change.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <param name="entityName">Name of the entity.</param>
		/// <param name="entityId">The entity id.</param>
		/// <param name="originalStatus">The original status.</param>
		/// <param name="newStatus">The new status.</param>
		/// <param name="overrideUserId">Override the id of the user for which the action is logged</param>
		protected void LogStatusChange(ServiceRequest request, string entityName, long entityId, long? originalStatus,
			long newStatus, long? overrideUserId = null)
		{
			if (RuntimeContext.CurrentRequest.IsNull())
				RuntimeContext.SetRequest(request);

			Helpers.Logging.LogStatusChange(entityName, entityId, originalStatus, newStatus, overrideUserId);
		}

		#endregion

		#region Localisation

		/// <summary>
		/// Localises the specified key.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		public string Localise(ServiceRequest request, string key)
		{
			if (RuntimeContext.CurrentRequest.IsNull())
				RuntimeContext.SetRequest(request);

			var value = Helpers.Localisation.Localise(key);

			return (value.IsNotNullOrEmpty()) ? value : string.Format("[{0}]", key);
		}

		/// <summary>
		/// Localises the specified key.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <param name="key">The key.</param>
		/// <param name="args">The args.</param>
		/// <returns></returns>
		public string Localise(ServiceRequest request, string key, params object[] args)
		{
			if (RuntimeContext.CurrentRequest.IsNull())
				RuntimeContext.SetRequest(request);

			var value = Helpers.Localisation.Localise(key, string.Empty, args);

			return (value.IsNotNullOrEmpty()) ? value : string.Format("[{0}]", key);
		}

		/// <summary>
		/// Localises the specified key or returns default.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <param name="key">The key.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <returns></returns>
		public string LocaliseOrDefault(ServiceRequest request, string key, string defaultValue)
		{
			if (RuntimeContext.CurrentRequest.IsNull())
				RuntimeContext.SetRequest(request);

			var value = Helpers.Localisation.Localise(key, defaultValue);

			return (value.IsNotNullOrEmpty()) ? value : string.Format("[{0}]", key);
		}

		/// <summary>
		/// Localises the or default.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <param name="id">The identifier.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <param name="args">The arguments.</param>
		/// <returns></returns>
		public string LocaliseOrDefault(ServiceRequest request, long? id, string defaultValue, params object[] args)
		{
			if (RuntimeContext.CurrentRequest.IsNull())
				RuntimeContext.SetRequest(request);

			return Helpers.Localisation.Localise(id, defaultValue, args);
		}

		/// <summary>
		/// Localises the specified key or returns default.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <param name="key">The key.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <param name="args">The args.</param>
		/// <returns></returns>
		public string LocaliseOrDefault(ServiceRequest request, string key, string defaultValue, params object[] args)
		{
			if (RuntimeContext.CurrentRequest.IsNull())
				RuntimeContext.SetRequest(request);

			var value = Helpers.Localisation.Localise(key, defaultValue, args);

			return (value.IsNotNullOrEmpty()) ? value : string.Format("[{0}]", key);
		}

		/// <summary>
		/// Gets the localisation dictionary from the cache.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		protected LocalisationDictionary GetCachedLocalisationDictionary(ServiceRequest request)
		{
			if (RuntimeContext.CurrentRequest.IsNull())
				RuntimeContext.SetRequest(request);

			return Helpers.Localisation.GetLocalisationDictionary();
		}

		/// <summary>
		/// Refreshes the localisation dictionary in the cache.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns>The refreshed cache</returns>
		protected LocalisationDictionary RefreshCachedLocalisationDictionary(ServiceRequest request)
		{
			if (RuntimeContext.CurrentRequest.IsNull())
				RuntimeContext.SetRequest(request);

			return Helpers.Localisation.RefreshLocalisationDictionary();
		}

		#endregion

		#region Lookups

		/// <summary>
		/// Gets a lookup.
		/// Note: We aren't profiling this.  The cached method is profiled instead
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public GetLookupResponse GetLookup(GetLookupRequest request)
		{
			var response = new GetLookupResponse(request);

			// Validate request
			if (!ValidateRequest(request, response, Validate.License))
				return response;

			try
			{
				response.LookupItems = GetLookup(request.LookupType, request.ParentId, request.Key, request.Prefix);
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}

			return response;
		}

		/// <summary>
		/// Gets the look up.
		/// </summary>
		/// <param name="lookupType">Type of the lookup.</param>
		/// <param name="parentId">The parent id.</param>
		/// <param name="key">The key.</param>
		/// <param name="prefix">An optional prefix</param>
		/// <returns></returns>
		public List<LookupItemView> GetLookup(LookupTypes lookupType, long parentId = 0, string key = null,
			string prefix = null)
		{
			return Helpers.Lookup.GetLookup(lookupType, parentId, key, prefix);
		}

		///// <summary>
		///// Gets a cached lookup.
		///// </summary>
		///// <param name="request">The request.</param>
		///// <param name="lookupType">Type of the lookup.</param>
		///// <returns></returns>
		//private IEnumerable<LookupItemView> GetCachedLookup(ServiceRequest request, LookupTypes lookupType)
		//{
		//  var lookupKey = string.Format(Constants.CacheKeys.LookupKey, request.UserContext.Culture);
		//  var lookup = Cacher.Get<ILookup<LookupTypes, LookupItemView>>(lookupKey);

		//  if (lookup == null)
		//  {
		//    using (Profiler.Profile(request.LogData()))
		//    {
		//      lock (SyncObject)
		//      {
		//        // Users and culture
		//        var keySet1 = GetLookupItems(request.UserContext.Culture);

		//        // Users config and default culture
		//        var rootSet = new List<LookupItemView>();
		//        if (request.UserContext.Culture != Constants.DefaultCulture)
		//          rootSet = GetLookupItems(Constants.DefaultCulture);

		//        keySet1 = MergeLists(rootSet, keySet1);
		//        lookup = keySet1.ToLookup(x => x.LookupType, x => x);

		//        Cacher.Set(lookupKey, lookup);
		//      }
		//    }
		//  }

		//  // return the requested set of data
		//  return lookup[lookupType].ToList();
		//}

		/// <summary>
		/// Merges the lists.
		/// </summary>
		/// <param name="rootSet">The root set.</param>
		/// <param name="keySet">The key set.</param>
		/// <returns></returns>
		private static List<LookupItemView> MergeLists(IList<LookupItemView> rootSet, IEnumerable<LookupItemView> keySet)
		{
			var removeSet = (from rs in rootSet
				join ks in keySet on
					new {rs.Key, rs.LookupType}
					equals
					new {ks.Key, ks.LookupType}
				select rs).ToList();

			removeSet.ForEach(x => rootSet.Remove(x));
			return keySet.Union(rootSet).ToList();
		}

		/// <summary>
		/// Gets the lookup items.
		/// </summary>
		/// <param name="culture">The culture.</param>
		/// <returns></returns>
		private List<LookupItemView> GetLookupItems(string culture)
		{
			return (
				from lu in Repositories.Configuration.LookupItemsViews
				where lu.Culture == culture
				select
					new LookupItemView(lu.Id, lu.Key, lu.LookupType, lu.Value, lu.DisplayOrder, lu.ParentId, lu.ParentKey,
						lu.ExternalId, Convert.ToInt32(lu.CustomFilterId))
				).ToList();
		}

		#endregion

		#region NAICS 

		/// <summary>
		/// Gets all the naics.
		/// </summary>
		/// <returns></returns>
		protected List<NaicsView> GetAllNaics(ServiceRequest request)
		{
			var naics = Cacher.Get<List<NaicsView>>(Constants.CacheKeys.NaicsNamesKey);

			if (naics == null)
			{
				using (Profiler.Profile(request.LogData()))
				{
					lock (SyncObject)
					{
						naics =
							Repositories.Library.Query<NAICS>()
								.OrderBy(x => x.Code)
								.Select(x => new NaicsView {Code = x.Code, Name = x.Name, IsChild = x.ParentId != 0})
								.ToList();
						Cacher.Set(Constants.CacheKeys.NaicsNamesKey, naics);
					}
				}
			}

			return naics;
		}

		#endregion

		#region Alert Messages

		/// <summary>
		/// Creates the alert message.
		/// </summary>
		/// <param name="alertMessageKey">The alert message key.</param>
		/// <param name="audienceEntityId">The audience entity id.</param>
		/// <param name="expiryDate">The expiry date.</param>
		/// <param name="audience">The audience.</param>
		/// <param name="messageType">Type of the message.</param>
		/// <param name="entityId">The entity id.</param>
		/// <param name="messageProperties">The message properties.</param>
		protected void CreateAlertMessage(string alertMessageKey, long? audienceEntityId, DateTime expiryDate,
			MessageAudiences audience, MessageTypes messageType, long? entityId, params object[] messageProperties)
		{
			Helpers.Alerts.CreateAlertMessage(alertMessageKey, false, audienceEntityId, expiryDate, audience, messageType,
				entityId, messageProperties);
		}

		/// <summary>
		/// Creates the alert message.
		/// </summary>
		/// <param name="alertMessageKey">The alert message key.</param>
		/// <param name="isSystemAlert">Whether this is a system alert</param>
		/// <param name="audienceEntityId">The audience entity id.</param>
		/// <param name="expiryDate">The expiry date.</param>
		/// <param name="audience">The audience.</param>
		/// <param name="messageType">Type of the message.</param>
		/// <param name="entityId">The entity id.</param>
		/// <param name="messageProperties">The message properties.</param>
		protected void CreateAlertMessage(string alertMessageKey, bool isSystemAlert, long? audienceEntityId,
			DateTime expiryDate, MessageAudiences audience, MessageTypes messageType, long? entityId,
			params object[] messageProperties)
		{
			Helpers.Alerts.CreateAlertMessage(alertMessageKey, isSystemAlert, audienceEntityId, expiryDate, audience, messageType,
				entityId, messageProperties);
		}

		#endregion

		#region Helper Methods

		/// <summary>
		/// Gets the encrypted GUID.
		/// </summary>
		/// <returns></returns>
		public static string GenerateRandomHash()
		{
			var buffer = Encoding.UTF8.GetBytes(Guid.NewGuid().ToString().Replace("-", ""));
			buffer = SHA512.Create().ComputeHash(buffer);

			return Convert.ToBase64String(buffer).Substring(0, 86);
		}

		/// <summary>
		/// Validates the user has access to module.
		/// </summary>
		/// <param name="user">The user.</param>
		/// <param name="module">The module.</param>
		/// <returns></returns>
		protected bool UserHasAccessToModule(User user, FocusModules module)
		{
			switch (module)
			{
				case FocusModules.Talent:
					return user.UserType == UserTypes.Talent;

				case FocusModules.Assist:
					return user.UserType == UserTypes.Assist;

				case FocusModules.Explorer:
					return (user.UserType & UserTypes.Explorer) == UserTypes.Explorer;

				case FocusModules.Career:
					return user.UserType == UserTypes.Career;

				case FocusModules.CareerExplorer:
					return ((user.UserType & UserTypes.Career) == UserTypes.Career &&
					        (user.UserType & UserTypes.Explorer) == UserTypes.Explorer);

				default:
					return false;
			}
		}

		/// <summary>
		/// Gets the user type for module.
		/// </summary>
		/// <param name="module">The module.</param>
		/// <returns></returns>
		protected UserTypes GetUserTypeForModule(FocusModules module)
		{
			switch (module)
			{
				case FocusModules.Talent:
					return UserTypes.Talent;

				case FocusModules.Assist:
					return UserTypes.Assist;

				case FocusModules.Explorer:
					return UserTypes.Explorer;

				case FocusModules.Career:
					return UserTypes.Career;

				case FocusModules.CareerExplorer:
					return UserTypes.Career | UserTypes.Explorer;

				default:
					return UserTypes.Anonymous;
			}
		}

		/// <summary>
		/// Counts the actions for job.
		/// </summary>
		/// <param name="actionType">Type of the action.</param>
		/// <param name="query">The query.</param>
		/// <param name="daysBack">The number of historic days to check</param>
		/// <returns></returns>
		public int CountActionsByActionType(ActionTypes actionType, IQueryable<ActionEvent> query, int daysBack = 0)
		{
			var actionEvent = Repositories.Core.ActionTypes.FirstOrDefault((x => x.Name == actionType.ToString()));

			if (daysBack != 0)
				query = query.Where(x => x.ActionedOn > DateTime.Now.AddDays(-1*daysBack));

			return actionEvent == null ? 0 : query.Count(x => x.ActionTypeId == actionEvent.Id);
		}

		/// <summary>
		/// Counts the number of actions for a list of action types
		/// </summary>
		/// <param name="actionTypes">A list of action types.</param>
		/// <param name="query">The query.</param>
		/// <param name="daysBack">The number of historic days to check</param>
		/// <returns></returns>
		public Dictionary<ActionTypes, int> CountActionsByActionTypes(List<ActionTypes> actionTypes,
			IQueryable<ActionEvent> query, int daysBack = 0)
		{
			var actionTypeNames = actionTypes.Select(at => at.ToString()).ToList();
			var actionTypeLookup = Repositories.Core.ActionTypes
				.Where(at => actionTypeNames.Contains(at.Name))
				.ToDictionary(at => at.Id, at => (ActionTypes) Enum.Parse(typeof (ActionTypes), at.Name, true));

			if (daysBack != 0)
				query = query.Where(ae => ae.ActionedOn > DateTime.Now.AddDays(-1*daysBack));

			var actionTypeIds = actionTypeLookup.Keys.ToList();
			var actionEventTypes =
				query.Where(ae => actionTypeIds.Contains(ae.ActionTypeId)).Select(ae => ae.ActionTypeId).ToList();

			var counts = actionTypes.ToDictionary(at => at, at => 0);
			actionTypeIds.ForEach(id => counts[actionTypeLookup[id]] = actionEventTypes.Count(aet => aet == id));

			return counts;
		}

		/// <summary>
		/// Gets the censorship rules.
		/// </summary>
		/// <param name="type">The type.</param>
		/// <returns></returns>
		protected CensorshipRuleList GetCensorshipRules(CensorshipType type)
		{
			var censorshipRulesKey = string.Format(Constants.CacheKeys.CensorshipRulesKey);
			var censorshipRules = Cacher.Get<CensorshipRuleList>(censorshipRulesKey);

			if (censorshipRules.IsNull())
			{
				censorshipRules = new CensorshipRuleList();
				censorshipRules.AddRange(
					Repositories.Core.Query<CensorshipRule>()
						.Select(censorshipRule => new Core.Censorship.CensorshipRule(censorshipRule))
						.ToList());

				lock (SyncObject)
					Cacher.Set(censorshipRulesKey, censorshipRules);
			}

			return censorshipRules.Filter(type);
		}

		/// <summary>
		/// Performs a censorship check on the email address for any profane words.
		/// </summary>
		/// <param name="emailAddress">The email address to check.</param>
		/// <param name="personId">The id of the person being check.</param>
		/// <param name="issues">Any existing issues record</param>
    protected void CensorshipCheckEmail(string emailAddress, long personId, Issues issues = null)
		{
			if (!AppSettings.InappropriateEmailAddressCheckEnabled)
				return;

			var censorshipRules = GetCensorshipRules(CensorshipType.Profanity);
			var censorshipCheck = new CensorshipCheck(censorshipRules, emailAddress);

			if (censorshipCheck.HasCensorships)
			{
				var badWords = censorshipCheck.FoundCensorshipRules.Count(x => x.Level == CensorshipLevel.Red);

				badWords += censorshipCheck.FoundCensorshipRules.Count(x => x.Level == CensorshipLevel.Yellow);

				if (badWords > 0)
				{
					if (issues.IsNull())
						issues = Repositories.Core.Issues.FirstOrDefault(issue => issue.PersonId == personId);

					if (issues.IsNull())
					{
						issues = new Issues
						{
							PersonId = personId,
							InappropriateEmailAddress = true
						};
						Repositories.Core.Add(issues);
					}
					else
					{
						issues.InappropriateEmailAddress = true;
					}
				}
			}
		}

		/// <summary>
		/// Checks text for profanities
		/// </summary>
		/// <param name="text">The text to check</param>
		/// <returns>Information on red and yellow words</returns>
		protected CensorshipModel ProfanityCheck(string text)
		{
			var censorshipRules = GetCensorshipRules(CensorshipType.Profanity);
			var censorshipCheck = new CensorshipCheck(censorshipRules, text);
			var censorshipModel = new CensorshipModel();

			// Check for red words
			var redWords = censorshipCheck.FoundCensorshipRules.Where(x => x.Level == CensorshipLevel.Red).ToList();

			if (redWords.Count > 0)
				censorshipModel.RedWords = string.Join(",", redWords.Select(x => x.Phrase));

			// Check for yellow words
			if (AppSettings.ActivationYellowWordFiltering)
			{
				var yellowWords = censorshipCheck.FoundCensorshipRules.Where(x => x.Level == CensorshipLevel.Yellow).ToList();

				if (yellowWords.Count > 0)
					censorshipModel.YellowWords = string.Join(",", yellowWords.Select(x => x.Phrase));
			}

			return censorshipModel;
		}

		#endregion

		#region Enums

		/// <summary>
		/// Validation options enum. Used in validation of messages.
		/// </summary>
		[Flags]
		protected enum Validate
		{
			License = 0x0001,
			ClientTag = 0x0002,
			SessionId = 0x0004,
			UserCredentials = 0x0008,
			AccessToken = 0x0010,
			Explorer = License | ClientTag | SessionId | AccessToken,
			All = License | ClientTag | SessionId | UserCredentials | AccessToken
		}

		/// <summary>
		/// 
		/// </summary>
		protected enum UserIdValidationFailureReasons
		{
			InvalidUserId,
			AccountDoesNotExist,
			AccountNotEnabled,
			AccountBlocked
		}

		#endregion

		
	}
}
