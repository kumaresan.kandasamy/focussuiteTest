﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using Focus.Core;
using Focus.Core.Criteria.Onet;
using Focus.Core.Criteria.Report;
using Focus.Core.Messages.ReportService;
using Focus.Core.Messages.ResumeService;
using Focus.Core.Models;
using Focus.Core.Models.Career;
using Focus.Core.Models.Report;
using Focus.Core.Views;
using Focus.Data.Configuration.Entities;
using Focus.Data.Core.Entities;
using Focus.Data.Library.Entities;
using Focus.Data.Report.Entities;
using Focus.Services.Core;
using Focus.Services.DtoMappers;
using Focus.Services.Messages;
using Focus.Services.Repositories.LaborInsight;
using Focus.Services.Repositories.LaborInsight.Criteria;
using Focus.Services.ServiceContracts;
using Framework.Caching;
using Framework.Core;
using Framework.Logging;

using Aspose.Cells;
using Aspose.Cells.Charts;
using Aspose.Cells.Rendering;

using Mindscape.LightSpeed;
using Query = Mindscape.LightSpeed.Querying.Query;

#endregion

namespace Focus.Services.ServiceImplementations
{
	public class ReportService : ServiceBase, IReportService
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="ReportService" /> class.
		/// </summary>
		public ReportService()
			: this(null)
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ReportService" /> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		public ReportService(IRuntimeContext runtimeContext)
			: base(runtimeContext)
		{
		}

		#region Job Seeker Service Methods

		/// <summary>
		/// Gets a list of job seekers matching the criteria
		/// </summary>
		/// <param name="request">The request containing the matching criteria</param>
		/// <returns>A response containing matching job seekers</returns>
		public JobSeekersReportResponse GetJobSeekers(JobSeekersReportRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new JobSeekersReportResponse(request);

				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var criteria = request.ReportCriteria;

					var countSql = new StringBuilder();
					var reportSql = GetJobSeekersSql(criteria, countSql, true);

					var totalJobSeekers = Repositories.Report.ExecuteScalar<int>(countSql.ToString());

					var jobSeekers = new List<JobSeekersReportView>();
					using (var reader = Repositories.Report.ExecuteDataReader(reportSql.ToString()))
					{
						var ordinalLookup = GetJobSeekerFieldsOrdinalLookup(reader);

						while (reader.Read())
						{
							jobSeekers.Add(new JobSeekersReportView
							{
								Id = reader.GetInt64(ordinalLookup["Id"]),
								FirstName =
									reader.IsDBNull(ordinalLookup["FirstName"])
										? string.Empty
										: reader.GetString(ordinalLookup["FirstName"]),
								MiddleInitial =
									reader.IsDBNull(ordinalLookup["MiddleInitial"])
										? string.Empty
										: reader.GetString(ordinalLookup["MiddleInitial"]),
								LastName =
									reader.IsDBNull(ordinalLookup["LastName"])
										? string.Empty
										: reader.GetString(ordinalLookup["LastName"]),
								EmailAddress =
									reader.IsDBNull(ordinalLookup["EmailAddress"])
										? string.Empty
										: reader.GetString(ordinalLookup["EmailAddress"]),
								County =
									reader.IsDBNull(ordinalLookup["County"])
										? string.Empty
										: reader.GetString(ordinalLookup["County"]),
								State =
									reader.IsDBNull(ordinalLookup["State"])
										? string.Empty
										: reader.GetString(ordinalLookup["State"]),
								Office =
									reader.IsDBNull(ordinalLookup["Office"])
										? string.Empty
										: reader.GetString(ordinalLookup["Office"]),
								EmploymentStatus =
									reader.IsDBNull(ordinalLookup["EmploymentStatus"])
										? null
										: (EmploymentStatus?)reader.GetInt32(ordinalLookup["EmploymentStatus"]),
								VeteranType =
									reader.IsDBNull(ordinalLookup["VeteranType"])
										? null
										: (VeteranEra?)reader.GetInt32(ordinalLookup["VeteranType"]),
								VeteranTransitionType =
									reader.IsDBNull(ordinalLookup["VeteranTransitionType"])
										? null
										: (VeteranTransitionType?)reader.GetInt32(ordinalLookup["VeteranTransitionType"]),
								VeteranMilitaryDischarge =
									reader.IsDBNull(ordinalLookup["VeteranMilitaryDischarge"])
										? null
										: (ReportMilitaryDischargeType?)
											reader.GetInt32(ordinalLookup["VeteranMilitaryDischarge"]),
								VeteranBranchOfService =
									reader.IsDBNull(ordinalLookup["VeteranBranchOfService"])
										? null
										: (ReportMilitaryBranchOfService?)
											reader.GetInt32(ordinalLookup["VeteranBranchOfService"]),
								FocusPersonId = reader.GetInt64(ordinalLookup["FocusPersonId"]),
								Totals = new JobSeekerActionView
								{
									Logins = reader.GetInt32(ordinalLookup["Logins"]),
									PostingsViewed = reader.GetInt32(ordinalLookup["PostingsViewed"]),
									ReferralRequests = reader.GetInt32(ordinalLookup["ReferralRequests"]),
									SelfReferrals = reader.GetInt32(ordinalLookup["SelfReferrals"]),
									StaffReferrals = reader.GetInt32(ordinalLookup["StaffReferrals"]),
									NotesAdded = reader.GetInt32(ordinalLookup["NotesAdded"]),
									AddedToLists = reader.GetInt32(ordinalLookup["AddedToLists"]),
									ActivitiesAssigned = reader.GetInt32(ordinalLookup["ActivitiesAssigned"]),
									StaffAssignments = reader.GetInt32(ordinalLookup["StaffAssignments"]),
									EmailsSent = reader.GetInt32(ordinalLookup["EmailsSent"]),
									FollowUpIssues = reader.GetInt32(ordinalLookup["FollowUpIssues"]),
									IssuesResolved = reader.GetInt32(ordinalLookup["IssuesResolved"]),
									Hired = reader.GetInt32(ordinalLookup["Hired"]),
									NotHired = reader.GetInt32(ordinalLookup["NotHired"]),
									FailedToApply = reader.GetInt32(ordinalLookup["FailedToApply"]),
									FailedToReportToInterview =
										reader.GetInt32(ordinalLookup["FailedToReportToInterview"]),
									InterviewDenied = reader.GetInt32(ordinalLookup["InterviewDenied"]),
									InterviewScheduled = reader.GetInt32(ordinalLookup["InterviewScheduled"]),
									NewApplicant = reader.GetInt32(ordinalLookup["NewApplicant"]),
									Recommended = reader.GetInt32(ordinalLookup["Recommended"]),
									RefusedOffer = reader.GetInt32(ordinalLookup["RefusedOffer"]),
									UnderConsideration = reader.GetInt32(ordinalLookup["UnderConsideration"]),
									UsedOnlineResumeHelp =
										reader.GetInt32(ordinalLookup["UsedOnlineResumeHelp"]),
									SavedJobAlerts = reader.GetInt32(ordinalLookup["SavedJobAlerts"]),
									TargetingHighGrowthSectors =
										reader.GetInt32(ordinalLookup["TargetingHighGrowthSectors"]),
									SelfReferralsExternal =
										reader.GetInt32(ordinalLookup["SelfReferralsExternal"]),
									StaffReferralsExternal =
										reader.GetInt32(ordinalLookup["StaffReferralsExternal"]),
									ReferralsApproved = reader.GetInt32(ordinalLookup["ReferralsApproved"]),
									FindJobsForSeeker = reader.GetInt32(ordinalLookup["FindJobsForSeeker"]),
									FailedToReportToJob =
										reader.GetInt32(ordinalLookup["FailedToReportToJob"]),
									FailedToRespondToInvitation =
										reader.GetInt32(ordinalLookup["FailedToRespondToInvitation"]),
									FoundJobFromOtherSource =
										reader.GetInt32(ordinalLookup["FoundJobFromOtherSource"]),
									JobAlreadyFilled = reader.GetInt32(ordinalLookup["JobAlreadyFilled"]),
									NotQualified = reader.GetInt32(ordinalLookup["NotQualified"]),
									NotYetPlaced = reader.GetInt32(ordinalLookup["NotYetPlaced"]),
									RefusedReferral = reader.GetInt32(ordinalLookup["RefusedReferral"]),
									SurveyVerySatisfied =
										reader.GetInt32(ordinalLookup["SurveyVerySatisfied"]),
									SurveySatisfied = reader.GetInt32(ordinalLookup["SurveySatisfied"]),
									SurveyDissatisfied = reader.GetInt32(ordinalLookup["SurveyDissatisfied"]),
									SurveyVeryDissatisfied =
										reader.GetInt32(ordinalLookup["SurveyVeryDissatisfied"]),
									SurveyNoUnexpectedMatches =
										reader.GetInt32(ordinalLookup["SurveyNoUnexpectedMatches"]),
									SurveyUnexpectedMatches =
										reader.GetInt32(ordinalLookup["SurveyUnexpectedMatches"]),
									SurveyReceivedInvitations =
										reader.GetInt32(ordinalLookup["SurveyReceivedInvitations"]),
									SurveyDidNotReceiveInvitations =
										reader.GetInt32(ordinalLookup["SurveyDidNotReceiveInvitations"]),
									ActivityServices = reader.GetInt32(ordinalLookup["ActivityServices"])
								}
							});
						}
						reader.Close();
					}

					response.JobSeekers = new PagedList<JobSeekersReportView>(jobSeekers, totalJobSeekers, criteria.PageIndex,
																																		criteria.PageSize, false);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets a data table job seekers corresponding to the criteria
		/// </summary>
		/// <param name="request">The request containing the corresponding criteria</param>
		/// <returns>A response containing a data table job seekers</returns>
		public JobSeekersDataTableResponse GetJobSeekersDataTable(JobSeekersDataTableRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new JobSeekersDataTableResponse(request);

				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var criteria = request.ReportCriteria;

					var countSql = new StringBuilder();
					var reportSql = GetJobSeekersSql(criteria, countSql, false);

					response.ReportData = GetJobSeekersDataTable(reportSql, request.ReportName, criteria);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets an Excel or PDF file of job seekers for export
		/// </summary>
		/// <param name="request">The request containing criteria or a list of job seekers</param>
		/// <returns>The response containing the bytes making up the file</returns>
		public JobSeekersExportResponse GetJobSeekersExportBytes(JobSeekersExportRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new JobSeekersExportResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.ClientTag | Validate.AccessToken))
					return response;

				try
				{
					var criteria = request.ReportCriteria;

					criteria.PageIndex = 0;
					criteria.PageSize = 50000;

					var countSql = new StringBuilder();
					var reportSql = GetJobSeekersSql(criteria, countSql, false);

					var reportData = GetJobSeekersDataTable(reportSql, request.ReportName, criteria);

					var criteriaDisplay =
						request.ReportCriteria.BuildCriteriaForDisplay(Helpers.Localisation.Localise,
																													 Helpers.Localisation.GetEnumLocalisedText,
																													 Helpers.Localisation.GetEnumLocalisedText, false)[0];

					response.ExportBytes = GetExportBytes(reportData, criteriaDisplay, request.ExportType);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}
				return response;
			}
		}

		#endregion

		#region Job Order Methods

		/// <summary>
		/// Gets a list of job orders matching the criteria
		/// </summary>
		/// <param name="request">The request containing the matching criteria</param>
		/// <returns>A response containing matching job orders</returns>
		public JobOrdersReportResponse GetJobOrders(JobOrdersReportRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new JobOrdersReportResponse(request);

				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var criteria = request.ReportCriteria;

					var countSql = new StringBuilder();
					var reportSql = GetJobOrdersSql(criteria, countSql, true);

					var totalJobOrders = Repositories.Report.ExecuteScalar<int>(countSql.ToString());

					var jobOrders = new List<JobOrdersReportView>();
					using (var reader = Repositories.Report.ExecuteDataReader(reportSql.ToString()))
					{
						var ordinalLookup = GetJobOrderFieldsOrdinalLookup(reader);

						while (reader.Read())
						{
							jobOrders.Add(new JobOrdersReportView
							{
								Id = reader.GetInt64(ordinalLookup["Id"]),
								JobTitle = reader.GetString(ordinalLookup["JobTitle"]),
								County =
									reader.IsDBNull(ordinalLookup["County"])
										? string.Empty
										: reader.GetString(ordinalLookup["County"]),
								State =
									reader.IsDBNull(ordinalLookup["State"])
										? string.Empty
										: reader.GetString(ordinalLookup["State"]),
								Office =
									reader.IsDBNull(ordinalLookup["Office"])
										? string.Empty
										: reader.GetString(ordinalLookup["Office"]),
								Employer = reader.GetString(ordinalLookup["EmployerName"]),
								FocusJobId = reader.GetInt64(ordinalLookup["FocusJobId"]),
								Totals = new JobOrderActionView
								{
									StaffReferrals = reader.GetInt32(ordinalLookup["StaffReferrals"]),
									SelfReferrals = reader.GetInt32(ordinalLookup["SelfReferrals"]),
									EmployerInvitationsSent =
										reader.GetInt32(ordinalLookup["EmployerInvitationsSent"]),
									ApplicantsInterviewed =
										reader.GetInt32(ordinalLookup["ApplicantsInterviewed"]),
									ApplicantsFailedToShow =
										reader.GetInt32(ordinalLookup["ApplicantsFailedToShow"]),
									ApplicantsDeniedInterviews =
										reader.GetInt32(ordinalLookup["ApplicantsDeniedInterviews"]),
									ApplicantsHired = reader.GetInt32(ordinalLookup["ApplicantsHired"]),
									JobOffersRefused = reader.GetInt32(ordinalLookup["JobOffersRefused"]),
									ApplicantsDidNotApply =
										reader.GetInt32(ordinalLookup["ApplicantsDidNotApply"]),
									InvitedJobSeekerViewed =
										reader.GetInt32(ordinalLookup["InvitedJobSeekerViewed"]),
									InvitedJobSeekerClicked =
										reader.GetInt32(ordinalLookup["InvitedJobSeekerClicked"]),
									ReferralsRequested = reader.GetInt32(ordinalLookup["ReferralsRequested"]),
									ApplicantsNotHired = reader.GetInt32(ordinalLookup["ApplicantsNotHired"]),
									ApplicantsNotYetPlaced =
										reader.GetInt32(ordinalLookup["ApplicantsNotYetPlaced"]),
									FailedToRespondToInvitation =
										reader.GetInt32(ordinalLookup["FailedToRespondToInvitation"]),
									FailedToReportToJob = reader.GetInt32(ordinalLookup["FailedToReportToJob"]),
									FoundJobFromOtherSource =
										reader.GetInt32(ordinalLookup["FoundJobFromOtherSource"]),
									JobAlreadyFilled = reader.GetInt32(ordinalLookup["JobAlreadyFilled"]),
									NewApplicant = reader.GetInt32(ordinalLookup["NewApplicant"]),
									NotQualified = reader.GetInt32(ordinalLookup["NotQualified"]),
									ApplicantRecommended =
										reader.GetInt32(ordinalLookup["ApplicantRecommended"]),
									RefusedReferral = reader.GetInt32(ordinalLookup["RefusedReferral"]),
									UnderConsideration = reader.GetInt32(ordinalLookup["UnderConsideration"])
								}
							});
						}
						reader.Close();
					}

					response.JobOrders = new PagedList<JobOrdersReportView>(jobOrders, totalJobOrders, criteria.PageIndex,
																																	criteria.PageSize, false);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets a data table job orders corresponding to the criteria
		/// </summary>
		/// <param name="request">The request containing the corresponding criteria</param>
		/// <returns>A response containing a data table job orders</returns>
		public JobOrdersDataTableResponse GetJobOrdersDataTable(JobOrdersDataTableRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new JobOrdersDataTableResponse(request);

				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var criteria = request.ReportCriteria;

					var countSql = new StringBuilder();
					var reportSql = GetJobOrdersSql(criteria, countSql, false);

					response.ReportData = GetJobOrdersDataTable(reportSql, request.ReportName, criteria);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets an Excel or PDF file of job orders for export
		/// </summary>
		/// <param name="request">The request containing criteria or a list of job orders</param>
		/// <returns>The response containing the bytes making up the file</returns>
		public JobOrdersExportResponse GetJobOrdersExportBytes(JobOrdersExportRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new JobOrdersExportResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.ClientTag | Validate.AccessToken))
					return response;

				try
				{
					var criteria = request.ReportCriteria;

					criteria.PageIndex = 0;
					criteria.PageSize = 50000;

					var countSql = new StringBuilder();
					var reportSql = GetJobOrdersSql(criteria, countSql, false);

					var reportData = GetJobOrdersDataTable(reportSql, request.ReportName, criteria);

					var criteriaDisplay =
						request.ReportCriteria.BuildCriteriaForDisplay(Helpers.Localisation.Localise,
																													 Helpers.Localisation.GetEnumLocalisedText,
																													 Helpers.Localisation.GetEnumLocalisedText, false)[0];

					response.ExportBytes = GetExportBytes(reportData, criteriaDisplay, request.ExportType);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}
				return response;
			}
		}

		#endregion

		#region Supply Demand Methods

		/// <summary>
		/// Gets a supply demand view matching the criteria
		/// </summary>
		/// <param name="request">The request containing the matching criteria</param>
		/// <returns>A response containing matching job orders</returns>
		public SupplyDemandReportResponse GetSupplyDemand(SupplyDemandReportRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new SupplyDemandReportResponse(request);

				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					response.SupplyDemand = GetSupplyDemandReport(request.ReportCriteria);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the supply demand report.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		private PagedList<SupplyDemandReportView> GetSupplyDemandReport(SupplyDemandReportCriteria criteria)
		{
			var jobReportCriteria = GetJobReportCriteria(criteria);
			var supplyDemand = Repositories.LaborInsight.GetCachedSupplyDemand(jobReportCriteria.Criteria);

			if (supplyDemand.IsNull())
			{
				supplyDemand = new PagedList<SupplyDemandReportView>();

				var laborInsightReport =
					Repositories.LaborInsight.GetJobReport(jobReportCriteria);

				if (laborInsightReport.IsNull())
					return new PagedList<SupplyDemandReportView>();

				if (laborInsightReport.JobData.Count == 0)
				{
					#region return empty report
					supplyDemand.Add(new SupplyDemandReportView
														 {
															 SupplyCount =
																 0,
															 DemandCount = 0,
															 GroupBy =
																 string.Empty,
															 OnetCode =
																 jobReportCriteria.Criteria.OnetIsFamily
																	 ? criteria.OccupationInfo.OnetFamily.ToString()
																	 : criteria.OccupationInfo.OnetId,
														 });
					#endregion
				}

				supplyDemand.AddRange(laborInsightReport.JobData.Select(jobData => GetSupplyDemand(jobData, criteria, jobReportCriteria)));
				Repositories.LaborInsight.SetCachedSupplyDemand(supplyDemand);
			}

			if (criteria.DisplayType == ReportDisplayType.BarChart)
			{
				return new PagedList<SupplyDemandReportView>(supplyDemand, supplyDemand.Count, 0, supplyDemand.Count, false);
			}

			return new PagedList<SupplyDemandReportView>(
				supplyDemand.Skip(criteria.PageIndex * criteria.PageSize).Take(criteria.PageSize), supplyDemand.Count,
				criteria.PageIndex,
				criteria.PageSize, false);
		}

		/// <summary>
		/// Gets a data table job orders corresponding to the criteria
		/// </summary>
		/// <param name="request">The request containing the corresponding criteria</param>
		/// <returns>A response containing a data table supply demand</returns>
		public SupplyDemandDataTableResponse GetSupplyDemandDataTable(SupplyDemandDataTableRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new SupplyDemandDataTableResponse(request);

				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var criteria = request.ReportCriteria;
					response.ReportData = GetSupplyDemandDataTable(request.ReportName, criteria);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets an Excel or PDF file of job orders for export
		/// </summary>
		/// <param name="request">The request containing criteria or a list of job orders</param>
		/// <returns>The response containing the bytes making up the file</returns>
		public SupplyDemandExportResponse GetSupplyDemandExportBytes(SupplyDemandExportRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new SupplyDemandExportResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.ClientTag | Validate.AccessToken))
					return response;

				try
				{
					var criteria = request.ReportCriteria;

					criteria.PageIndex = 0;
					criteria.PageSize = 50000;

					var reportData = GetSupplyDemandDataTable(request.ReportName, criteria, true);

					var criteriaDisplay = request.ReportCriteria.BuildCriteriaForDisplay(Helpers.Localisation.Localise, Helpers.Localisation.GetEnumLocalisedText, Helpers.Localisation.GetEnumLocalisedText, false)[0];

					response.ExportBytes = GetExportBytes(reportData, criteriaDisplay, request.ExportType);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}
				return response;
			}
		}

		#endregion

		#region Employer Methods

		/// <summary>
		/// Gets a list of job orders matching the criteria
		/// </summary>
		/// <param name="request">The request containing the matching criteria</param>
		/// <returns>A response containing matching job orders</returns>
		public EmployersReportResponse GetEmployers(EmployersReportRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new EmployersReportResponse(request);

				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var criteria = request.ReportCriteria;

					var countSql = new StringBuilder();
					var reportSql = GetEmployersSql(criteria, countSql, true);

					var totalEmployers = Repositories.Report.ExecuteScalar<int>(countSql.ToString());

					var employers = new List<EmployersReportView>();
					using (var reader = Repositories.Report.ExecuteDataReader(reportSql.ToString()))
					{
						var ordinalLookup = GetEmployerFieldsOrdinalLookup(reader);

						while (reader.Read())
						{
							employers.Add(new EmployersReportView
							{
								Id = reader.GetInt64(ordinalLookup["Id"]),
								Name = reader.GetString(ordinalLookup["Name"]),
								State = reader.IsDBNull(ordinalLookup["State"]) ? string.Empty : reader.GetString(ordinalLookup["State"]),
								County = reader.IsDBNull(ordinalLookup["County"]) ? string.Empty : reader.GetString(ordinalLookup["County"]),
								Office = reader.IsDBNull(ordinalLookup["Office"]) ? string.Empty : reader.GetString(ordinalLookup["Office"]),
								FocusEmployerId = reader.IsDBNull(ordinalLookup["FocusEmployerId"]) ? (long?)null : reader.GetInt64(ordinalLookup["FocusEmployerId"]),
								FocusBusinessUnitId = reader.GetInt64(ordinalLookup["FocusBusinessUnitId"]),
								Totals = new EmployerActionView
								{
									JobOrdersEdited = reader.GetInt32(ordinalLookup["JobOrdersEdited"]),
									JobSeekersInterviewed = reader.GetInt32(ordinalLookup["JobSeekersInterviewed"]),
									JobSeekersHired = reader.GetInt32(ordinalLookup["JobSeekersHired"]),
									JobOrdersCreated = reader.GetInt32(ordinalLookup["JobOrdersCreated"]),
									JobOrdersPosted = reader.GetInt32(ordinalLookup["JobOrdersPosted"]),
									JobOrdersPutOnHold = reader.GetInt32(ordinalLookup["JobOrdersPutOnHold"]),
									JobOrdersRefreshed = reader.GetInt32(ordinalLookup["JobOrdersRefreshed"]),
									JobOrdersClosed = reader.GetInt32(ordinalLookup["JobOrdersClosed"]),
									InvitationsSent = reader.GetInt32(ordinalLookup["InvitationsSent"]),
									JobSeekersNotHired = reader.GetInt32(ordinalLookup["JobSeekersNotHired"]),
									SelfReferrals = reader.GetInt32(ordinalLookup["SelfReferrals"]),
									StaffReferrals = reader.GetInt32(ordinalLookup["StaffReferrals"]),
								}
							});
						}
						reader.Close();
					}

					response.Employers = new PagedList<EmployersReportView>(employers, totalEmployers, criteria.PageIndex, criteria.PageSize, false);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets a data table job orders corresponding to the criteria
		/// </summary>
		/// <param name="request">The request containing the corresponding criteria</param>
		/// <returns>A response containing a data table job orders</returns>
		public EmployersDataTableResponse GetEmployersDataTable(EmployersDataTableRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new EmployersDataTableResponse(request);

				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var criteria = request.ReportCriteria;

					var countSql = new StringBuilder();
					var reportSql = GetEmployersSql(criteria, countSql, false);

					response.ReportData = GetEmployersDataTable(reportSql, request.ReportName, criteria);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets an Excel or PDF file of job orders for export
		/// </summary>
		/// <param name="request">The request containing criteria or a list of job orders</param>
		/// <returns>The response containing the bytes making up the file</returns>
		public EmployersExportResponse GetEmployersExportBytes(EmployersExportRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new EmployersExportResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.ClientTag | Validate.AccessToken))
					return response;

				try
				{
					var criteria = request.ReportCriteria;

					criteria.PageIndex = 0;
					criteria.PageSize = 50000;

					var countSql = new StringBuilder();
					var reportSql = GetEmployersSql(criteria, countSql, false);

					var reportData = GetEmployersDataTable(reportSql, request.ReportName, criteria);

					var criteriaDisplay = request.ReportCriteria.BuildCriteriaForDisplay(Helpers.Localisation.Localise, Helpers.Localisation.GetEnumLocalisedText, Helpers.Localisation.GetEnumLocalisedText, false)[0];

					response.ExportBytes = GetExportBytes(reportData, criteriaDisplay, request.ExportType);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}
				return response;
			}
		}

		#endregion

		#region Shared Service Methods

		/// <summary>
		/// Converts a data table to a PDF or Excel file for export
		/// </summary>
		/// <param name="request">he request containing the data table</param>
		/// <returns>The response containing the bytes making up the file</returns>
		public DataTableExportResponse ConvertDataTableToExportBytes(DataTableExportRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new DataTableExportResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.ClientTag | Validate.AccessToken))
					return response;

				try
				{
					response.ExportBytes = GetExportBytes(request.ReportData, request.CriteriaDisplay, request.ExportType);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}
				return response;
			}
		}

		#endregion

		#region Look-ups and drop-downs

		/// <summary>
		/// Gets the reporting specific look up.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns>The lookup data response</returns>
		public LookupReportDataResponse LookUpReportData(LookupReportDataRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new LookupReportDataResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.ClientTag | Validate.AccessToken))
					return response;

				try
				{
					var lookupKey = string.Format(Constants.CacheKeys.ReportLookupKey, request.LookUpType);
					var lookup = Cacher.Get<List<string>>(lookupKey);

					if (lookup.IsNull())
					{
						IQueryable<string> lookupQuery = null;

						switch (request.LookUpType)
						{
							case ReportLookUpType.CertificationLicenses:
								lookupQuery = Repositories.Report.Query<LookupJobSeekerCertificateView>().Select(data => data.Description);
								break;
							case ReportLookUpType.Skills:
								lookupQuery = Repositories.Report.Query<LookupJobSeekerSkillView>().Select(data => data.Description);
								break;
							case ReportLookUpType.Internships:
								lookupQuery = Repositories.Report.Query<LookupJobSeekerInternshipView>().Select(data => data.Description);
								break;
							case ReportLookUpType.EducationQualifications:
								lookupQuery = Repositories.Report.Query<LookupJobSeekerEducationView>().Select(data => data.Description);
								break;
							case ReportLookUpType.MilitaryOccupationCodes:
								lookupQuery = Repositories.Report.Query<LookupJobSeekerMilitaryOccupationCodeView>().Select(data => data.MilitaryOccupationCode);
								break;
							case ReportLookUpType.MilitaryOccupationTitles:
								lookupQuery = Repositories.Report.Query<LookupJobSeekerMilitaryOccupationTitleView>().Select(data => data.MilitaryOccupationTitle);
								break;
							case ReportLookUpType.EmployerNames:
								lookupQuery = Repositories.Report.Query<LookupEmployerNameView>().Select(data => data.Name);
								break;
							case ReportLookUpType.EmployerFEIN:
								lookupQuery = Repositories.Report.Query<LookupEmployerFEINView>().Select(data => data.FederalEmployerIdentificationNumber);
								break;
							case ReportLookUpType.JobOrderTitles:
								lookupQuery = Repositories.Report.Query<LookupJobOrderTitleView>().Select(data => data.JobTitle);
								break;
							case ReportLookUpType.EmployerCounties:
								lookupQuery = Repositories.Report.Query<Data.Report.Entities.Employer>().Select(data => data.County).Distinct();
								break;
							case ReportLookUpType.JobOrderCounties:
								lookupQuery = Repositories.Report.Query<JobOrder>().Select(data => data.County).Distinct();
								break;
							case ReportLookUpType.JobSeekerCounties:
								lookupQuery = Repositories.Report.Query<JobSeeker>().Select(data => data.County).Distinct();
								break;
						}

						if (lookupQuery.IsNotNull())
						{
							lock (SyncObject)
							{
								lookup = lookupQuery.ToList();
								Cacher.Set(lookupKey, lookup, DateTime.Now.AddMinutes(10));
							}
						}
					}

					// Search relevant look up for items required.
					response.LookupData = lookup.Where(text => text.StartsWith(request.SearchString, StringComparison.OrdinalIgnoreCase))
																			.OrderBy(text => text)
																			.ToArray();
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}
				return response;
			}
		}

		/// <summary>
		/// Gets the reporting specific look up.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns>The lookup data response</returns>
		public OfficeLookupResponse OfficeLookup(OfficeLookupRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new OfficeLookupResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.ClientTag | Validate.AccessToken))
					return response;

				try
				{
					switch (request.ReportType)
					{
						case ReportType.JobSeeker:
							response.Offices = Repositories.Report.Query<JobSeekerOffice>()
																										 .Select(office => new ReportOffice { Id = office.OfficeId, Name = office.OfficeName })
																										 .Distinct()
																										 .ToList();
							break;
						case ReportType.JobOrder:
							response.Offices = Repositories.Report.Query<JobOrderOffice>()
																										 .Select(office => new ReportOffice { Id = office.OfficeId, Name = office.OfficeName })
																										 .Distinct()
																										 .ToList();
							break;
						case ReportType.Employer:
							response.Offices = Repositories.Report.Query<EmployerOffice>()
																										 .Select(office => new ReportOffice { Id = office.OfficeId, Name = office.OfficeName })
																										 .Distinct()
																										 .ToList();
							break;
					}

					response.Offices = response.Offices.OrderBy(office => office.Name).ToList();
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}
				return response;
			}
		}

		#endregion

		#region Report Saving

		/// <summary>
		/// Saves the report
		/// </summary>
		/// <param name="request">The request holding the report criteria</param>
		/// <returns>The response</returns>
		public SaveReportResponse SaveReport(SaveReportRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new SaveReportResponse(request);

				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					if (request.IsSessionReport)
					{
						var deleteReportQuery = new Query(typeof(SavedReport), Entity.Attribute("UserId") == request.UserContext.UserId && Entity.Attribute("IsSessionReport") == true);
						Repositories.Report.Remove(deleteReportQuery);
						Repositories.Report.SaveChanges();
					}

					var savedReport = new SavedReport
					{
						ReportType = request.ReportCriteria.ReportType,
						ReportDisplayType = request.ReportCriteria.DisplayType,
						Name = request.ReportName,
						Criteria = request.ReportCriteria.DataContractSerializeRunTime(),
						UserId = request.UserContext.UserId,
						ReportDate = DateTime.Now,
						DisplayOnDashboard = request.DisplayOnDashboard,
						IsSessionReport = request.IsSessionReport
					};

					Repositories.Report.Add(savedReport);
					Repositories.Report.SaveChanges();

					response.ReportId = savedReport.Id;
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets a saved report
		/// </summary>
		/// <param name="request">The request holding the session key and report type</param>
		/// <returns>The response</returns>
		public GetSavedReportResponse GetSavedReport(GetSavedReportRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new GetSavedReportResponse(request);

				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var savedReport = Repositories.Report.SavedReports
																							 .First(report => report.Id == request.ReportId &&
																																report.IsSessionReport == request.IsSessionReport &&
																																report.UserId == request.UserContext.UserId);

					switch (savedReport.ReportType)
					{
						case ReportType.JobSeeker:
							response.ReportCriteria = savedReport.Criteria.DataContractDeserialize<JobSeekersReportCriteria>();
							break;
						case ReportType.JobOrder:
							response.ReportCriteria = savedReport.Criteria.DataContractDeserialize<JobOrdersReportCriteria>();
							break;
						case ReportType.Employer:
							response.ReportCriteria = savedReport.Criteria.DataContractDeserialize<EmployersReportCriteria>();
							break;
						case ReportType.SupplyDemand:
							response.ReportCriteria = savedReport.Criteria.DataContractDeserialize<SupplyDemandReportCriteria>();
							break;
						default:
							throw new Exception("Unsupported saved report type");
					}
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets all saved reports for the use
		/// </summary>
		/// <param name="request">The request</param>
		/// <returns>The response containing the saved report</returns>
		public GetSavedReportListResponse GetSavedReportList(GetSavedReportListRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new GetSavedReportListResponse(request);

				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var savedReportsQuery = Repositories.Report.SavedReports.Where(report => !report.IsSessionReport && report.UserId == request.UserContext.UserId);

					if (request.DashboardReports.HasValue)
						savedReportsQuery = savedReportsQuery.Where(report => report.DisplayOnDashboard == request.DashboardReports);

					if (request.ReportType.HasValue)
						savedReportsQuery = savedReportsQuery.Where(report => report.ReportType == request.ReportType);

					response.SavedReports = savedReportsQuery.Select(report => report.AsDto()).ToList();
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Deletes a report for a user
		/// </summary>
		/// <param name="request">The request containing the report id</param>
		/// <returns>The response</returns>
		public DeleteReportResponse DeleteReport(DeleteReportRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new DeleteReportResponse(request);

				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var reportToDelete = Repositories.Report.SavedReports.FirstOrDefault(report => report.Id == request.ReportId && report.UserId == request.UserContext.UserId);

					if (reportToDelete.IsNotNull())
					{
						Repositories.Report.Remove(reportToDelete);
						Repositories.Report.SaveChanges(true);
					}
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		#endregion

		#region Dashboard Report

		/// <summary>
		/// Gets the approval queue status report.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public ApprovalQueueStatusReportResponse GetApprovalQueueStatusReport(ApprovalQueueStatusReportRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new ApprovalQueueStatusReportResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var newPostingsSinceLastLoginCount = 0;
					int flaggedPostingsCount;
					var newEmployeeAccountsSinceLastLoginCount = 0;
					int outstandingEmployeeAccountsCount;
					var newReferralRequestsSinceLastLoginCount = 0;
					List<DateTime> referralDates;

					var lastLogin = Repositories.Core.FindById<User>(request.UserId).LastLoggedInOn;
					var businessDate = DateTime.Now.Date.AddWorkingDays(-1);

					// Get user's offices
					var offices = AppSettings.OfficesEnabled
						? Repositories.Core.PersonOfficeMappers.Where(x => x.PersonId == request.UserContext.PersonId).Select(x => x.OfficeId).ToArray()
						: new long?[] { null };

					var isStateWide = (offices.IsNotNullOrEmpty() && offices.Length == 1 && offices[0] == null);

					if (lastLogin.IsNotNull())
					{
						newEmployeeAccountsSinceLastLoginCount = (from e in Repositories.Core.Employees
																											join u in Repositories.Core.Users on e.PersonId equals u.PersonId
																											where u.CreatedOn >= lastLogin
																											select u.Id).Count();
					}

					if (isStateWide)
					{
						if (lastLogin.IsNotNull())
							newPostingsSinceLastLoginCount = (from j in Repositories.Core.Jobs
																								where j.PostedOn >= lastLogin
																								select j.Id).Count();

						flaggedPostingsCount = (from j in Repositories.Core.Jobs
																		where j.ApprovalStatus == ApprovalStatuses.WaitingApproval
																		select j.Id).Count();

						outstandingEmployeeAccountsCount = (from e in Repositories.Core.EmployerAccountReferralViews
																								where e.AwaitingApprovalDate <= businessDate &&
																								(
																									(e.EmployerApprovalStatus == (int)ApprovalStatuses.WaitingApproval && e.TypeOfApproval == (int)ApprovalType.Employer) ||
																									(e.EmployeeApprovalStatus == (int)ApprovalStatuses.WaitingApproval && e.TypeOfApproval == (int)ApprovalType.HiringManager) ||
																									(e.BusinessUnitApprovalStatus == (int)ApprovalStatuses.WaitingApproval && e.TypeOfApproval == (int)ApprovalType.BusinessUnit)
																								)
																								select e.Id).Count();

						referralDates = (from p in Repositories.Core.Persons
														 join r in Repositories.Core.Resumes
															 on p.Id equals r.PersonId
														 join a in Repositories.Core.Applications
															 on r.Id equals a.ResumeId
														 where a.ApplicationStatus == ApplicationStatusTypes.SelfReferred
																	 && a.ApprovalStatus == ApprovalStatuses.WaitingApproval
														 select a.CreatedOn).ToList();
					}
					else
					{
						if (lastLogin.IsNotNull())
							newPostingsSinceLastLoginCount = (from j in Repositories.Core.Jobs
																								join jom in Repositories.Core.JobOfficeMappers on j.Id equals jom.JobId
																								where offices.Contains(jom.OfficeId) &&
																											j.PostedOn >= lastLogin
																								select j.Id).Distinct().Count();

						flaggedPostingsCount = (from j in Repositories.Core.Jobs
																		join jom in Repositories.Core.JobOfficeMappers on j.Id equals jom.JobId
																		where j.ApprovalStatus == ApprovalStatuses.WaitingApproval && offices.Contains(jom.OfficeId)
																		select j.Id).Distinct().Count();

						outstandingEmployeeAccountsCount = (from e in Repositories.Core.EmployerAccountReferralViews
																								join eom in Repositories.Core.EmployerOfficeMappers on e.EmployerId equals eom.EmployerId
																								where offices.Contains(eom.OfficeId) &&
																								e.AwaitingApprovalDate <= businessDate &&
																								(
																									(e.EmployerApprovalStatus == (int)ApprovalStatuses.WaitingApproval && e.TypeOfApproval == (int)ApprovalType.Employer) ||
																									(e.EmployeeApprovalStatus == (int)ApprovalStatuses.WaitingApproval && e.TypeOfApproval == (int)ApprovalType.HiringManager) ||
																									(e.BusinessUnitApprovalStatus == (int)ApprovalStatuses.WaitingApproval && e.TypeOfApproval == (int)ApprovalType.BusinessUnit)
																								)
																								select e.Id).Distinct().Count();

						referralDates = (from p in Repositories.Core.Persons
														 join r in Repositories.Core.Resumes
															 on p.Id equals r.PersonId
														 join a in Repositories.Core.Applications
															 on r.Id equals a.ResumeId
														 where p.PersonOfficeMappers.Any(pom => offices.Contains(pom.OfficeId))
																	 && a.ApplicationStatus == ApplicationStatusTypes.SelfReferred
																	 && a.ApprovalStatus == ApprovalStatuses.WaitingApproval
														 select a.CreatedOn).ToList();
					}

					if (lastLogin.IsNotNull())
						newReferralRequestsSinceLastLoginCount = referralDates.Count(date => date >= lastLogin);

					var outstandingReferralRequestsCount = referralDates.Count(date => date.Date <= businessDate);

					response.ReportView = new ApprovalQueueStatusReportView
					{
						NewPostingsSinceLastLoginCount = newPostingsSinceLastLoginCount,
						FlaggedPostingsCount = flaggedPostingsCount,
						NewEmployeeAccountsSinceLastLoginCount = newEmployeeAccountsSinceLastLoginCount,
						OutstandingEmployeeAccountsCount = outstandingEmployeeAccountsCount,
						NewReferralRequestsSinceLastLoginCount = newReferralRequestsSinceLastLoginCount,
						OutstandingReferralRequestsCount = outstandingReferralRequestsCount
					};
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		#endregion

		#region Generate Employer Activity Report

		public ActivityReportResponse GenerateActivityReport(ActivityReportRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new ActivityReportResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					if (request.FromDate.IsNull() || request.FromDate == DateTime.MinValue)
					{
						if (response.Errors.IsNullOrEmpty())
							response.Errors = new List<ErrorTypes>();

						response.Errors.Add(ErrorTypes.FromDateNotSupplied);
					}

					if (request.ToDate.IsNull() || request.ToDate == DateTime.MinValue)
					{
						if (response.Errors.IsNullOrEmpty())
							response.Errors = new List<ErrorTypes>();

						response.Errors.Add(ErrorTypes.ToDateNotSupplied);
					}

					if (request.FromDate > request.ToDate)
					{
						if (response.Errors.IsNullOrEmpty())
							response.Errors = new List<ErrorTypes>();

						response.Errors.Add(ErrorTypes.FromDateAfterToDate);
					}

					if (request.Recipients.IsNullOrEmpty())
					{
						if (response.Errors.IsNullOrEmpty())
							response.Errors = new List<ErrorTypes>();

						response.Errors.Add(ErrorTypes.EmailsNotSupplied);
					}

					var recipients = request.Recipients.Split(',').ToList();

					foreach (var recipient in from recipient in recipients let emailRegEx = new Regex(AppSettings.EmailAddressRegExPattern) where recipient.IsNotNullOrEmpty() && !emailRegEx.IsMatch(recipient) select recipient)
					{
						if (response.Errors.IsNullOrEmpty())
							response.Errors = new List<ErrorTypes> { ErrorTypes.InvalidEmail };
						else if (!response.Errors.Any(x => x.Equals(ErrorTypes.InvalidEmail)))
							response.Errors.Add(ErrorTypes.InvalidEmail);

						if (response.InvalidEmails.IsNullOrEmpty())
							response.InvalidEmails = new List<string> { recipient };
						else
							response.InvalidEmails.Add(recipient);
					}

					if (response.Errors.IsNotNullOrEmpty() && response.Errors.Count > 0)
					{
						response.SetFailure(ErrorTypes.ValidationFailed);
						return response;
					}

					Helpers.Messaging.Publish(new GenerateReportMessage
					{
						ReportType = request.ReportType,
						ReportFromDate = request.FromDate,
						ReportToDate = request.ToDate,
						Emails = recipients
					});
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}


		#endregion

		#region Job Seeker Helper Methods

		/// <summary>
		/// Gets the SQL for getting the job seekers from the database matching the criteria
		/// </summary>
		/// <param name="criteria">The criteria</param>
		/// <param name="countSql">Will be set to a SQL specific for counting the total</param>
		/// <param name="forScreen">Whether the data is needed for the screen, or export</param>
		/// <returns>The SQL to get the matching job seekers</returns>
		private StringBuilder GetJobSeekersSql(JobSeekersReportCriteria criteria, StringBuilder countSql, bool forScreen)
		{
			const string selectClauseFull = @"
        JS.[Id],
        JS.[FirstName],
        JS.[LastName],
        JS.[MiddleInitial],
        JS.[EmailAddress],
        JS.[County],
        JS.[State],
        JS.[Office],
        JS.[EmploymentStatus],
				JS.[AccountType],
        JS.[VeteranType],
        JS.[VeteranTransitionType],
        JS.[VeteranMilitaryDischarge],
        JS.[VeteranBranchOfService],
        JS.[FocusPersonId]";

			const string selectClauseForExport = @"
        JS.[FirstName] + ' ' + ISNULL(JS.[MiddleInitial] + ' ', '')  + JS.[LastName] AS [Name],
        JS.[EmailAddress],
        ISNULL(JS.[County] + ', ' + JS.[State], '') AS [Location],
        JS.[Office],
        CAST(JS.[EmploymentStatus] AS VARCHAR(50)) AS [EmploymentStatus],
        CAST(JS.[VeteranType] AS VARCHAR(50)) AS [VeteranType],
        CAST(JS.[VeteranTransitionType] AS VARCHAR(50)) AS [VeteranTransitionType],
        CAST(JS.[VeteranMilitaryDischarge] AS VARCHAR(50)) AS [VeteranMilitaryDischarge],
        CAST(JS.[VeteranBranchOfService] AS VARCHAR(50)) AS [VeteranBranchOfService]";

			var chartSelectField = string.Empty;

			var whereClause = new StringBuilder();
			var orderClause = new StringBuilder("ORDER BY ");
			var groupByClause = new StringBuilder("GROUP BY JS.[Id]");
			var havingClause = new StringBuilder();

			var whereStep = " WHERE";
			var havingStep = " HAVING";

			var jobSeekerStatus = criteria.JobSeekerStatusInfo;
			if (jobSeekerStatus.IsNotNull())
			{
				if (jobSeekerStatus.EmploymentStatuses.IsNotNullOrEmpty())
				{
					whereClause.Append(whereStep).AppendFormat(" JS.EmploymentStatus IN ({0})", jobSeekerStatus.EmploymentStatuses.AsCommaListForSql());
					whereStep = " AND";
				}
				if (jobSeekerStatus.CareerAccountTypes.IsNotNullOrEmpty())
				{
					whereClause.Append(whereStep).AppendFormat(" JS.AccountType IN ({0})", jobSeekerStatus.CareerAccountTypes.AsCommaListForSql());
					whereStep = " AND";
				}
                if (jobSeekerStatus.JobSeekerEnabledStatus.IsNotNullOrEmpty())
                {
                    whereClause.Append(whereStep).AppendFormat(" JS.Enabled IN ({0})", jobSeekerStatus.JobSeekerEnabledStatus.AsCommaListForSql());
                    whereStep = " AND";
                }
                if (jobSeekerStatus.JobSeekerBlockedStatus)
                {
                    whereClause.Append(whereStep).AppendFormat(" JS.Blocked = 1");
                    whereStep = " AND";
                }
                
			}

			var veteranInfo = criteria.VeteranInfo;
			if (veteranInfo.IsNotNull())
			{
				var veteranWhereClause = new StringBuilder();

				if (veteranInfo.Types.IsNotNullOrEmpty())
				{
					var types = new List<VeteranEra?>();
					types.AddRange(veteranInfo.Types);

					if (types.Contains(VeteranEra.TransitioningServiceMember))
						types.Add(VeteranEra.TransitioningServiceMemberSpouse);

					if (types.Contains(VeteranEra.OtherVet))
						types.Add(VeteranEra.Vietnam);

					veteranWhereClause.AppendFormat(" AND JSMH.VeteranType IN ({0})", types.AsCommaListForSql());
				}

				if (veteranInfo.TransitionTypes.IsNotNullOrEmpty())
					veteranWhereClause.AppendFormat(" AND JSMH.VeteranTransitionType IN ({0})", veteranInfo.TransitionTypes.AsCommaListForSql());

				if (veteranInfo.MilitaryDischarges.IsNotNullOrEmpty())
					veteranWhereClause.AppendFormat(" AND JSMH.VeteranMilitaryDischarge IN ({0})", veteranInfo.MilitaryDischarges.AsCommaListForSql());

				if (veteranInfo.BranchOfServices.IsNotNullOrEmpty())
					veteranWhereClause.AppendFormat(" AND JSMH.VeteranBranchOfService IN ({0})", veteranInfo.BranchOfServices.AsCommaListForSql());

				if (veteranInfo.DisabilityStatuses.IsNotNullOrEmpty())
					veteranWhereClause.AppendFormat(" AND JSMH.VeteranDisability IN ({0})", veteranInfo.DisabilityStatuses.AsCommaListForSql());

				if (veteranInfo.CampaignVeteran.HasValue)
					veteranWhereClause.AppendFormat(" AND JSMH.CampaignVeteran = {0}", veteranInfo.CampaignVeteran.Value.AsOneOrZero());

				if (veteranInfo.HomelessVeteran.HasValue)
					veteranWhereClause.AppendFormat(" AND JS.IsHomelessVeteran = {0}", veteranInfo.HomelessVeteran.Value.AsOneOrZero());

				if (veteranWhereClause.Length > 0)
				{
					whereClause.Append(whereStep)
										 .Append(" EXISTS(SELECT TOP 1 1 FROM [Report.JobSeekerMilitaryHistory] JSMH WHERE JSMH.JobSeekerId = JS.Id")
										 .Append(veteranWhereClause)
										 .Append(")");
					whereStep = " AND";
				}
			}

			var characteristicInfo = criteria.CharacteristicsInfo;
			if (characteristicInfo.IsNotNull())
			{
				if (characteristicInfo.MinimumAge.HasValue)
				{
					whereClause.Append(whereStep).AppendFormat(" JS.Age >= {0}", (characteristicInfo.MinimumAge.Value));
					whereStep = " AND";
				}

				if (characteristicInfo.MaximumAge.HasValue)
				{
					whereClause.Append(whereStep).AppendFormat(" JS.Age <= {0}", (characteristicInfo.MaximumAge.Value));
					whereStep = " AND";
				}

				if (characteristicInfo.Genders.IsNotNullOrEmpty())
				{
					whereClause.Append(whereStep).AppendFormat(" JS.Gender IN ({0})", characteristicInfo.Genders.AsCommaListForSql());
					whereStep = " AND";
				}

				if (characteristicInfo.EthnicHeritages.IsNotNullOrEmpty())
				{
					whereClause.Append(whereStep).AppendFormat(" JS.EthnicHeritage IN ({0})", characteristicInfo.EthnicHeritages.AsCommaListForSql());
					whereStep = " AND";
				}

				if (characteristicInfo.ReportRaces.IsNotNullOrEmpty())
				{
					var raceBitMask = ReportRace.NA;
					characteristicInfo.ReportRaces.ForEach(race => raceBitMask |= race ?? raceBitMask);

					whereClause.Append(whereStep).AppendFormat(" JS.Race & {0} > 0", (int)(raceBitMask));
					whereStep = " AND";
				}

				if (characteristicInfo.DisabilityStatuses.IsNotNullOrEmpty())
				{
					whereClause.Append(whereStep).AppendFormat(" JS.DisabilityStatus IN ({0})", characteristicInfo.DisabilityStatuses.AsCommaListForSql());
					whereStep = " AND";
				}

				if (characteristicInfo.DisabilityTypes.IsNotNullOrEmpty())
				{
				    var disabilityBitMask = ReportDisabilityType.NA;
                    characteristicInfo.DisabilityTypes.ForEach(disability => disabilityBitMask |= disability?? disabilityBitMask);

					whereClause.Append(whereStep).AppendFormat(" JS.DisabilityType & {0} > 0", (int)(disabilityBitMask));
					whereStep = " AND";
				}
			}

			var individualCharacteristicInfo = criteria.IndividualCharacteristicsInfo;
			if (individualCharacteristicInfo.IsNotNull())
			{
				if (individualCharacteristicInfo.FirstName.IsNotNullOrEmpty())
				{
					whereClause.Append(whereStep).AppendFormat(" JS.FirstName LIKE '%{0}%'", (individualCharacteristicInfo.FirstName.Trim().EscapeSql()));
					whereStep = " AND";
				}

				if (individualCharacteristicInfo.LastName.IsNotNullOrEmpty())
				{
					whereClause.Append(whereStep).AppendFormat(" JS.LastName LIKE '%{0}%'", (individualCharacteristicInfo.LastName.Trim().EscapeSql()));
					whereStep = " AND";
				}

				if (individualCharacteristicInfo.Email.IsNotNullOrEmpty())
				{
					whereClause.Append(whereStep).AppendFormat(" JS.EmailAddress LIKE '%{0}%'", (individualCharacteristicInfo.Email.Trim())); whereStep = " AND";
				}

				if (individualCharacteristicInfo.Ssns.IsNotNullOrEmpty())
				{
					whereClause.Append(whereStep).AppendFormat(" JS.SocialSecurityNumber IN ({0})", individualCharacteristicInfo.Ssns.AsCommaListForSql());
					whereStep = " AND";
				}
			}

			var ncrcInfo = criteria.NcrcInfo;
			if (ncrcInfo.IsNotNull() && ncrcInfo.NcrcLevels.Any())
			{
				whereClause.Append(whereStep).Append("(");
				whereClause.AppendFormat(" JS.NcrcLevel IN ({0}) ", ncrcInfo.NcrcLevels.AsCommaListForSql());
				whereClause.Append(")");
				whereStep = " AND";
			}

			var qualificationsInfo = criteria.QualificationsInfo;
			if (qualificationsInfo.IsNotNull())
			{
				if (qualificationsInfo.MinimumEducationLevels.IsNotNullOrEmpty())
				{
					var mappedLevels = MapEducationLevels(qualificationsInfo.MinimumEducationLevels);

					whereClause.Append(whereStep).AppendFormat(" JS.MinimumEducationLevel IN ({0})", mappedLevels.AsCommaListForSql());
					whereStep = " AND";
				}

				if (qualificationsInfo.CertificationAndLicences.IsNotNullOrEmpty())
				{
					string[] certs = qualificationsInfo.CertificationAndLicences.ToArray();

					whereClause.Append(whereStep)
											.AppendFormat(" EXISTS(SELECT TOP 1 1 FROM [Report.JobSeekerData] JSD WHERE JSD.JobSeekerId = JS.Id AND JSD.DataType = {0} AND JSD.[Description] IN ({1}))", (int)ReportJobSeekerDataType.CertificationLicence, certs.AsCommaListForSql());
					whereStep = " AND";
				}

				if (qualificationsInfo.Skills.IsNotNullOrEmpty())
				{
					string[] skills = qualificationsInfo.Skills.ToArray();

					whereClause.Append(whereStep)
											.AppendFormat(" EXISTS(SELECT TOP 1 1 FROM [Report.JobSeekerData] JSD WHERE JSD.JobSeekerId = JS.Id AND JSD.DataType = {0} AND JSD.[Description] IN ({1}))", (int)ReportJobSeekerDataType.Skill, skills.AsCommaListForSql());
					whereStep = " AND";
				}
			}

			var salaryInfo = criteria.SalaryInfo;
			if (salaryInfo.IsNotNull())
			{
				if (salaryInfo.MinimumSalary.HasValue)
				{
					whereClause.Append(whereStep).AppendFormat(" JS.Salary >= {0}", salaryInfo.MinimumSalary.Value);
					whereStep = " AND";
				}

				if (salaryInfo.MaximumSalary.HasValue)
				{
					whereClause.Append(whereStep).AppendFormat(" JS.Salary <= {0}", salaryInfo.MaximumSalary.Value);
					whereStep = " AND";
				}

				if (salaryInfo.SalaryFrequency.HasValue)
				{
					whereClause.Append(whereStep).AppendFormat(" JS.SalaryFrequency = {0}", (int)(salaryInfo.SalaryFrequency.Value));
					whereStep = " AND";
				}
			}

			var occupationsInfo = criteria.OccupationInfo;
			if (occupationsInfo.IsNotNull())
			{
				if (occupationsInfo.MilitaryOccupationCodes.IsNotNullOrEmpty())
				{
					string[] moCodes = occupationsInfo.MilitaryOccupationCodes.ToArray();

					whereClause.Append(whereStep)
											.AppendFormat(" EXISTS(SELECT TOP 1 1 FROM [Report.JobSeekerData] JSD WHERE JSD.JobSeekerId = JS.Id AND JSD.DataType = {0} AND JSD.[Description] IN ({1}))", (int)ReportJobSeekerDataType.MilitaryOccupationCode, moCodes.AsCommaListForSql());
					whereStep = " AND";
				}

				if (occupationsInfo.MilitaryOccupationTitles.IsNotNullOrEmpty())
				{
					string[] moTitles = occupationsInfo.MilitaryOccupationTitles.ToArray();

					whereClause.Append(whereStep)
											.AppendFormat(" EXISTS(SELECT TOP 1 1 FROM [Report.JobSeekerData] JSD WHERE JSD.JobSeekerId = JS.Id AND JSD.DataType = {0} AND JSD.[Description] IN ({1}))", (int)ReportJobSeekerDataType.MilitaryOccupationTitle, moTitles.AsCommaListForSql());
					whereStep = " AND";
				}

				if (occupationsInfo.OccupationCodes.IsNotNullOrEmpty())
				{
					string[] ronets = occupationsInfo.OccupationCodes.ToArray();

					whereClause.Append(whereStep)
										 .AppendFormat(" EXISTS(SELECT TOP 1 1 FROM [Report.JobSeekerData] JSD WHERE JSD.JobSeekerId = JS.Id AND JSD.DataType = {0} AND JSD.[Description] IN ({1}))", (int)ReportJobSeekerDataType.JobROnet, ronets.AsCommaListForSql());
					whereStep = " AND";
				}
			}

			var industriesInfo = criteria.IndustriesInfo;
			if (industriesInfo.IsNotNull())
			{
				if (industriesInfo.MostExperienceIndustriesCodes.IsNotNullOrEmpty())
				{
					var industries = industriesInfo.MostExperienceIndustriesCodes.ToArray();

					whereClause.Append(whereStep).AppendFormat(" EXISTS(SELECT TOP 1 1 FROM [Report.JobSeekerData] JSD WHERE JSD.JobSeekerId = JS.Id AND JSD.DataType = {0} AND JSD.[Description] IN ({1}))", (int)ReportJobSeekerDataType.MostExperienceIndustry, industries.AsCommaListForSql());
					whereStep = " AND";
				}

				if (industriesInfo.TargetIndustriesCodes.IsNotNullOrEmpty())
				{
					var industries = industriesInfo.TargetIndustriesCodes.ToArray();

					whereClause.Append(whereStep).AppendFormat(" EXISTS(SELECT TOP 1 1 FROM [Report.JobSeekerData] JSD WHERE JSD.JobSeekerId = JS.Id AND JSD.DataType = {0} AND JSD.[Description] IN ({1}))", (int)ReportJobSeekerDataType.TargetIndustry, industries.AsCommaListForSql());
					whereStep = " AND";
				}
			}

			var resumeInfo = criteria.ResumeInfo;
			if (resumeInfo.IsNotNull())
			{
				if (resumeInfo.HasResume.GetValueOrDefault(false) || resumeInfo.HasMultipleResumes.GetValueOrDefault(false))
				{
					var minValue = resumeInfo.HasMultipleResumes.GetValueOrDefault(false) ? 2 : 1;

					whereClause.Append(whereStep).AppendFormat(" JS.ResumeCount >= {0}", minValue);
					whereStep = " AND";
				}

				if (resumeInfo.HasNoResume.GetValueOrDefault(false))
				{
					whereClause.Append(whereStep).Append(" JS.ResumeCount = 0");
					whereStep = " AND";
				}

				if (resumeInfo.HasPendingResume.GetValueOrDefault(false))
				{
					whereClause.Append(whereStep).AppendFormat(" JS.HasPendingResume = {0}", resumeInfo.HasPendingResume.Value.AsOneOrZero());
					whereStep = " AND";
				}

				if (resumeInfo.HasSearchableResume.GetValueOrDefault(false))
				{
					whereClause.Append(whereStep).AppendFormat(" JS.ResumeSearchable = {0}", resumeInfo.HasSearchableResume.Value.AsOneOrZero());
					whereStep = " AND";
				}

				if (resumeInfo.WillingToWorkOvertime.GetValueOrDefault(false))
				{
					whereClause.Append(whereStep).AppendFormat(" JS.WillingToWorkOvertime = {0}", resumeInfo.WillingToWorkOvertime.Value.AsOneOrZero());
					whereStep = " AND";
				}

				if (resumeInfo.WillingToRelocate.GetValueOrDefault(false))
				{
					whereClause.Append(whereStep).AppendFormat(" JS.WillingToRelocate = {0}", resumeInfo.WillingToRelocate.Value.AsOneOrZero());
					whereStep = " AND";
				}
			}

			var locationInfo = criteria.LocationInfo;
			if (locationInfo.IsNotNull())
			{
				if (locationInfo.Offices.IsNotNullOrEmpty())
				{
					var offices = locationInfo.Offices.ToArray();

					whereClause.Append(whereStep)
											.AppendFormat(" EXISTS(SELECT TOP 1 1 FROM [Report.JobSeekerOffice] JSO WHERE JSO.JobSeekerId = JS.Id AND JSO.OfficeName IN ({0}))", offices.AsCommaListForSql());
					whereStep = " AND";
				}

				if (locationInfo.Counties.IsNotNullOrEmpty())
				{
					whereClause.Append(whereStep).AppendFormat(" JS.County IN ({0})", locationInfo.Counties.AsCommaListForSql());
					whereStep = " AND";
				}

				if (locationInfo.PostalCode.IsNotNullOrEmpty() && locationInfo.DistanceFromPostalCode.IsNotNull())
				{
					var postalCodePoint = Repositories.Library.Query<PostalCode>().FirstOrDefault(postalCode => postalCode.Code == locationInfo.PostalCode);
					if (postalCodePoint.IsNotNull())
					{
						// See www.movable-type.co.uk/scripts/latlong.html for explanation of calculation (Spherical law of cosiness)

						var latitude = Math.PI * postalCodePoint.Latitude / 180.0;
						var longitude = Math.PI * postalCodePoint.Longitude / 180.0;
						var distanceInKm = (double)locationInfo.DistanceFromPostalCode / 0.62137;
						/*
						query =
							query.Where(
								jobSeeker => !string.IsNullOrEmpty(jobSeeker.PostalCode) &&
								Math.Acos(Math.Sin(latitude) * Math.Sin(jobSeeker.LatitudeRadians) +
													Math.Cos(latitude) * Math.Cos(jobSeeker.LatitudeRadians) *
													Math.Cos(jobSeeker.LongitudeRadians - longitude)) * 6371 < distanceInKm);
						*/
						whereClause.Append(whereStep).AppendFormat(@" JS.PostalCode IS NOT NULL 
              AND ACOS(ROUND({0} * SIN(JS.LatitudeRadians) +
                       {1} * COS(JS.LatitudeRadians) *
                       COS(JS.LongitudeRadians - {2}), 9)) * 6371 < {3}", Math.Round(Math.Sin(latitude), 10), Math.Round(Math.Cos(latitude), 10), Math.Round(longitude, 10), Math.Round(distanceInKm, 10));
						whereStep = " AND";
					}
				}
			}

			if (AppSettings.UnsubscribeFromJSMessages && criteria.SubscribedOnly)
			{
				whereClause.Append(whereStep).AppendFormat(" JS.Unsubscribed = 0");
				whereStep = " AND";
			}

			var keywordInfo = criteria.KeywordInfo;
			if (keywordInfo.IsNotNull())
			{
				if (keywordInfo.KeyWordSearch.IsNotNullOrEmpty() && keywordInfo.KeyWords.IsNotNullOrEmpty() && keywordInfo.SearchType.HasValue)
				{
					List<int> typesToCheck;
					if (keywordInfo.KeyWordSearch.Any(x => x.HasValue && x.Value == KeyWord.FullResume))
					{
						// Build up list of keywords represented by FullResume
						var collatedKeyWords = new List<KeyWord?>
                                   {
                                     KeyWord.EmployerName,
                                     KeyWord.JobTitle,
                                     KeyWord.JobDescription,
                                     KeyWord.EducationQualifications,
                                     KeyWord.Skills,
                                     KeyWord.Internships
                                   };
						typesToCheck = collatedKeyWords.Select(MapKeyWordTypeToJobSeekerDataType).ToList();
					}
					else
						typesToCheck = keywordInfo.KeyWordSearch.Select(MapKeyWordTypeToJobSeekerDataType).ToList();

					if (keywordInfo.SearchType == KeywordSearchType.All)
					{
						keywordInfo.KeyWords.ForEach(word =>
						{
							whereClause.Append(whereStep)
												 .AppendFormat(" EXISTS(SELECT TOP 1 1 FROM [Report.JobSeekerData] JSD WHERE JSD.JobSeekerId = JS.Id AND JSD.DataType IN ({0}) AND JSD.[Description] LIKE '%{1}%')", typesToCheck.AsCommaListForSql(), word.EscapeSql());
							whereStep = " AND";
						});
					}
					else
					{
						whereClause.Append(whereStep)
											 .AppendFormat(" EXISTS(SELECT TOP 1 1 FROM [Report.JobSeekerData] JSD WHERE JSD.JobSeekerId = JS.Id AND JSD.DataType IN ({0}) AND (", typesToCheck.AsCommaListForSql());
						whereStep = string.Empty;
						keywordInfo.KeyWords.ForEach(word =>
						{
							whereClause.Append(whereStep).AppendFormat(" JSD.[Description] LIKE '%{0}%'", word.EscapeSql());
							whereStep = " OR";
						});
						whereClause.Append("))");
					}
				}
			}

			if (resumeInfo.IsNotNull() && resumeInfo.UsedOnlineResumeHelp.GetValueOrDefault(false))
			{
				havingClause.Append(havingStep).Append(" SUM(JSA.UsedOnlineResumeHelp) > 0");
				havingStep = " AND";
			}

			string categoryKey = null;
			string activityKey = null;

			var activityCriteria = criteria.JobSeekerActivityLogInfo;
			if (activityCriteria.IsNotNull() && activityCriteria.ActivityCategory.IsNotNull())
			{
				categoryKey = Repositories.Configuration.Query<ActivityCategory>()
																	.Where(category => category.Id == activityCriteria.ActivityCategory.Item1)
																	.Select(category => category.LocalisationKey)
																	.FirstOrDefault();

				if (activityCriteria.Activity.IsNotNull())
					activityKey = Repositories.Configuration.Query<Activity>()
																		.Where(activity => activity.Id == activityCriteria.Activity.Item1)
																		.Select(activity => activity.LocalisationKey)
																		.FirstOrDefault();

				groupByClause.Append(", TJS.AssignmentsMade");
			}

			var summaryInfo = criteria.JobSeekerActivitySummaryInfo;
			var groupInfo = criteria.ChartGroupInfo;

			var showTotalCount = (summaryInfo.IsNotNull() && summaryInfo.TotalCount);
			var isGrouped = groupInfo.IsNotNull() || showTotalCount;

			// Ordering on 'text' fields can be done in SQL
			var orderBy = ReportJobSeekerOrderBy.LastName;
			var sqlOrderDirection = "ASC";
			var orderingByAction = false;

			var orderInfo = criteria.OrderInfo;
			if (orderInfo.IsNotNull() && (summaryInfo.IsNull() || !summaryInfo.TotalCount))
			{
				orderBy = orderInfo.OrderBy;
				sqlOrderDirection = orderInfo.Direction == ReportOrder.Ascending ? "ASC" : "DESC";
			}

			if (groupInfo.IsNotNull())
			{
				switch (groupInfo.GroupBy)
				{
					case ReportJobSeekerGroupBy.State:
						orderBy = ReportJobSeekerOrderBy.State;
						chartSelectField = "State";
						break;
					case ReportJobSeekerGroupBy.Office:
						orderBy = ReportJobSeekerOrderBy.Office;
						chartSelectField = "Office";
						break;
					case ReportJobSeekerGroupBy.EmploymentStatus:
						orderBy = ReportJobSeekerOrderBy.EmploymentStatus;
						chartSelectField = "EmploymentStatus";
						break;
					case ReportJobSeekerGroupBy.EducationLevel:
						orderBy = ReportJobSeekerOrderBy.EducationLevel;
						chartSelectField = "MinimumEducationLevel";
						break;
				}
				sqlOrderDirection = "ASC";
			}

			switch (orderBy)
			{
				case ReportJobSeekerOrderBy.LastName:
					orderClause.AppendFormat("JS.LastName {0}, JS.FirstName {0}", sqlOrderDirection);
					groupByClause.Append(", JS.LastName, JS.FirstName");
					break;

				case ReportJobSeekerOrderBy.FirstName:
				case ReportJobSeekerOrderBy.EmailAddress:
				case ReportJobSeekerOrderBy.County:
				case ReportJobSeekerOrderBy.State:
				case ReportJobSeekerOrderBy.EmploymentStatus:
				case ReportJobSeekerOrderBy.VeteranType:
				case ReportJobSeekerOrderBy.VeteranTransitionType:
				case ReportJobSeekerOrderBy.VeteranMilitaryDischarge:
				case ReportJobSeekerOrderBy.VeteranBranchOfService:
				case ReportJobSeekerOrderBy.Office:
					orderClause.AppendFormat("JS.{0} {1}", orderBy, sqlOrderDirection);
					groupByClause.AppendFormat(", JS.{0}", orderBy);
					break;

				case ReportJobSeekerOrderBy.EducationLevel:
					orderClause.AppendFormat("JS.MinimumEducationLevel {0}", sqlOrderDirection);
					groupByClause.Append(", JS.MinimumEducationLevel");
					break;

				case ReportJobSeekerOrderBy.Logins:
				case ReportJobSeekerOrderBy.PostingsViewed:
				case ReportJobSeekerOrderBy.ReferralRequests:
				case ReportJobSeekerOrderBy.StaffReferrals:
				case ReportJobSeekerOrderBy.SelfReferrals:
				case ReportJobSeekerOrderBy.StaffReferralsExternal:
				case ReportJobSeekerOrderBy.SelfReferralsExternal:
				case ReportJobSeekerOrderBy.ReferralsApproved:
				case ReportJobSeekerOrderBy.TargetingHighGrowthSectors:
				case ReportJobSeekerOrderBy.SavedJobAlerts:
				case ReportJobSeekerOrderBy.SurveyVerySatisfied:
				case ReportJobSeekerOrderBy.SurveySatisfied:
				case ReportJobSeekerOrderBy.SurveyDissatisfied:
				case ReportJobSeekerOrderBy.SurveyVeryDissatisfied:
				case ReportJobSeekerOrderBy.SurveyNoUnexpectedMatches:
				case ReportJobSeekerOrderBy.SurveyUnexpectedMatches:
				case ReportJobSeekerOrderBy.SurveyReceivedInvitations:
				case ReportJobSeekerOrderBy.SurveyDidNotReceiveInvitations:
				case ReportJobSeekerOrderBy.NotesAdded:
				case ReportJobSeekerOrderBy.AddedToLists:
				case ReportJobSeekerOrderBy.ActivitiesAssigned:
				case ReportJobSeekerOrderBy.StaffAssignments:
				case ReportJobSeekerOrderBy.FindJobsForSeeker:
				case ReportJobSeekerOrderBy.EmailsSent:
				case ReportJobSeekerOrderBy.IssuesResolved:
				case ReportJobSeekerOrderBy.FollowUpIssues:
				case ReportJobSeekerOrderBy.Hired:
				case ReportJobSeekerOrderBy.NotHired:
				case ReportJobSeekerOrderBy.FailedToApply:
				case ReportJobSeekerOrderBy.FailedToReportToInterview:
				case ReportJobSeekerOrderBy.InterviewDenied:
				case ReportJobSeekerOrderBy.InterviewScheduled:
				case ReportJobSeekerOrderBy.NewApplicant:
				case ReportJobSeekerOrderBy.Recommended:
				case ReportJobSeekerOrderBy.RefusedOffer:
				case ReportJobSeekerOrderBy.UnderConsideration:
				case ReportJobSeekerOrderBy.FailedToReportToJob:
				case ReportJobSeekerOrderBy.FailedToRespondToInvitation:
				case ReportJobSeekerOrderBy.FoundJobFromOtherSource:
				case ReportJobSeekerOrderBy.JobAlreadyFilled:
				case ReportJobSeekerOrderBy.NotQualified:
				case ReportJobSeekerOrderBy.NotYetPlaced:
				case ReportJobSeekerOrderBy.RefusedReferral:
					orderingByAction = true;
					orderClause.AppendFormat("SUM(JSA.{0}) {1}", orderBy, sqlOrderDirection);
					break;

				case ReportJobSeekerOrderBy.ActivityServices:
					orderingByAction = true;
					orderClause.AppendFormat("TJS.AssignmentsMade {0}", sqlOrderDirection);
					break;

				case ReportJobSeekerOrderBy.TotalReferrals:
					orderingByAction = true;
					orderClause.AppendFormat("SUM(JSA.SelfReferrals + JSA.SelfReferralsExternal + JSA.StaffReferrals + JSA.StaffReferralsExternal) {0}", sqlOrderDirection);
					break;
			}

			var actionToDate = DateTime.Now.Date;
			var actionFromDate = new DateTime(1900, 01, 01);

			var dateInfo = criteria.DateRangeInfo;
			if (dateInfo.IsNotNull())
			{
				if (dateInfo.FromDate.HasValue || dateInfo.Days.HasValue)
					actionFromDate = dateInfo.FromDate.HasValue ? dateInfo.FromDate.Value : actionToDate.AddDays(0 - dateInfo.Days.Value);

				if (dateInfo.ToDate.HasValue)
					actionToDate = dateInfo.ToDate.Value;
			}

			var startRow = criteria.PageIndex * criteria.PageSize + 1;
			var endRow = startRow + criteria.PageSize - 1;

			var actionTotalFields = GetJobSeekersRequiredActions(criteria.JobSeekerActivitySummaryInfo, criteria.JobSeekerActivityLogInfo, criteria.JobSeekerReferralOutcomesInfo);

			if (criteria.ZeroesHandling != ReportZeroesHandling.GetAll && actionTotalFields.Any())
			{
				havingClause.Append(havingStep).Append("(");
				havingStep = string.Empty;

				actionTotalFields.ForEach(field =>
				{
					havingClause.Append(havingStep).AppendFormat(" SUM(JSA.{0}) > 0", field);
					havingStep = criteria.ZeroesHandling == ReportZeroesHandling.ExcludeAllZeroes ? " OR" : " AND";
				});

				havingClause.Append(")");
			}

			var withSql = new StringBuilder();

			withSql.Append(@"
WITH FilteredJobSeekers AS 
(
  SELECT
    JS.[Id]");

			if (categoryKey.IsNotNull())
				withSql.Append(", ").AppendLine().Append("TJS.AssignmentsMade");

			if (!isGrouped)
				withSql.Append(", ").AppendLine().AppendFormat(@"ROW_NUMBER() OVER ({0}) AS RowNumber", orderClause);

			withSql.AppendLine().Append(@"FROM [Report.JobSeeker] JS");

			var includeGroupAndHavingClause = false;
			if (actionTotalFields.Any() && (orderingByAction || criteria.ZeroesHandling != ReportZeroesHandling.GetAll))
			{
				withSql.AppendFormat(@"
  LEFT JOIN [Report.JobSeekerAction] JSA
	  ON JSA.JobSeekerId = JS.Id
    AND JSA.ActionDate BETWEEN '{0}' AND '{1}'",
					actionFromDate.ToString("dd MMMM yyyy"),
					actionToDate.ToString("dd MMMM yyyy"));

				includeGroupAndHavingClause = true;
			}

			if (categoryKey.IsNotNull())
			{
				withSql.AppendFormat(@"
  INNER JOIN 
  (
	  SELECT 
		  JS2.Id,
		  SUM(JSAA.AssignmentsMade) AS AssignmentsMade
	  FROM [Report.JobSeeker] JS2
	  INNER JOIN [Report.JobSeekerActivityAssignment] JSAA
	    ON JSAA.JobSeekerId = JS2.Id
	    AND JSAA.AssignDate BETWEEN '{0}' AND '{1}'
      AND JSAA.ActivityCategory = '{2}'
      {3}
	  GROUP BY
		  JS2.Id
  ) TJS
	ON TJS.Id = JS.Id",
					actionFromDate.ToString("dd MMMM yyyy"),
					actionToDate.ToString("dd MMMM yyyy"),
					categoryKey.EscapeSql(),
					(activityKey.IsNotNull() ? string.Concat("AND JSAA.Activity = '", activityKey.EscapeSql(), "'") : ""));
			}

			withSql.AppendLine().Append(whereClause);
			if (includeGroupAndHavingClause)
			{
				withSql.AppendLine().Append(groupByClause);
				withSql.AppendLine().Append(havingClause);
			}
			withSql.AppendLine().Append(")");

			countSql.Append(withSql);
			countSql.Replace(string.Format("ROW_NUMBER() OVER ({0})", orderClause), "NULL");
			countSql.AppendLine().Append("SELECT COUNT(1) FROM FilteredJobSeekers");

			var reportSql = new StringBuilder();
			reportSql.Append(withSql);

			var selectDivider = "";

			if (!isGrouped)
			{
				reportSql.AppendLine().Append("SELECT 0 AS DummyTotal,");
				reportSql.AppendLine().Append(forScreen ? selectClauseFull : selectClauseForExport);

				selectDivider = ",";
			}
			else if (chartSelectField.IsNotNullOrEmpty())
			{
				reportSql.AppendLine().AppendFormat("SELECT CAST(JS.[{0}] AS VARCHAR(400)) AS [{0}]", chartSelectField);

				selectDivider = ",";
			}
			else
			{
				reportSql.AppendLine().Append("SELECT ");
			}

			if (actionTotalFields.Any())
			{
				reportSql.Append(selectDivider);
				reportSql.AppendLine().Append(string.Join(",\r\n", actionTotalFields.Select(field => string.Format("ISNULL(SUM(JSA.[{0}]), 0) AS {0}", field))));

				selectDivider = ",";
			}

			if (showTotalCount || (isGrouped && !actionTotalFields.Any() && categoryKey.IsNull()))
			{
				reportSql.Append(selectDivider);
				reportSql.AppendLine().Append("COUNT(1) AS TotalAccounts");

				selectDivider = ",";
			}

			if (categoryKey.IsNotNull())
			{
				reportSql.Append(selectDivider);
				reportSql.AppendLine();
				reportSql.Append(!isGrouped
													 ? "ISNULL(FJS.AssignmentsMade, 0) AS ActivityServices"
													 : "ISNULL(SUM(FJS.AssignmentsMade), 0) AS ActivityServices");
			}

			reportSql.AppendLine().Append(@"
FROM FilteredJobSeekers FJS 
INNER JOIN [Report.JobSeeker] JS
ON JS.Id = FJS.Id");

			if (actionTotalFields.Any())
			{
				reportSql.AppendLine().AppendFormat(@"
LEFT JOIN [Report.JobSeekerAction] JSA
ON JSA.JobSeekerId = JS.Id
AND JSA.ActionDate BETWEEN '{0}' AND '{1}'",
																						actionFromDate.ToString("dd MMMM yyyy"),
																						actionToDate.ToString("dd MMMM yyyy"));
			}

			if (!isGrouped)
			{
				reportSql.AppendLine().AppendFormat("WHERE FJS.RowNumber BETWEEN {0} AND {1}", startRow, endRow);
				reportSql.AppendLine().Append("GROUP BY");
				reportSql.AppendLine().Append(selectClauseFull);
				if (categoryKey.IsNotNull())
				{
					reportSql.Append(",");
					reportSql.AppendLine().Append("FJS.AssignmentsMade");
				}
				reportSql.Append(",");
				reportSql.AppendLine().Append("FJS.RowNumber");
				reportSql.AppendLine().Append("ORDER BY FJS.RowNumber");
			}
			else
			{
				if (chartSelectField.IsNotNullOrEmpty())
				{
					reportSql.AppendLine().AppendFormat("GROUP BY JS.[{0}]", chartSelectField);
					reportSql.AppendLine().AppendFormat("ORDER BY JS.[{0}]", chartSelectField);
				}
			}

			return reportSql;
		}

		/// <summary>
		/// Gets the list of actions to total on the report
		/// </summary>
		/// <param name="summaryCriteria">The summary criteria.</param>
		/// <param name="activityCriteria">The activity criteria.</param>
		/// <param name="referralCriteria">The referral criteria.</param>
		/// <returns>The list to populate.</returns>
		private static List<string> GetJobSeekersRequiredActions(JobSeekerActivitySummaryCriteria summaryCriteria, JobSeekerActivityLogCriteria activityCriteria, ReferralOutcomesCriteria referralCriteria)
		{
			var actions = new List<string>();

			if (summaryCriteria.IsNotNull())
			{
				if (summaryCriteria.Logins)
					actions.Add("Logins");

				if (summaryCriteria.PostingsViewed)
					actions.Add("PostingsViewed");

				if (summaryCriteria.ReferralRequests)
					actions.Add("ReferralRequests");

				if (summaryCriteria.SelfReferrals || summaryCriteria.TotalReferrals)
					actions.Add("SelfReferrals");

				if (summaryCriteria.StaffReferrals || summaryCriteria.TotalReferrals)
					actions.Add("StaffReferrals");

				if (summaryCriteria.SelfReferralsExternal || summaryCriteria.TotalReferrals)
					actions.Add("SelfReferralsExternal");

				if (summaryCriteria.StaffReferralsExternal || summaryCriteria.TotalReferrals)
					actions.Add("StaffReferralsExternal");

				if (summaryCriteria.ReferralsApproved)
					actions.Add("ReferralsApproved");

				if (summaryCriteria.TargetingHighGrowthSectors)
					actions.Add("TargetingHighGrowthSectors");

				if (summaryCriteria.SavedJobAlerts)
					actions.Add("SavedJobAlerts");

				if (summaryCriteria.SurveyVerySatisfied)
					actions.Add("SurveyVerySatisfied");

				if (summaryCriteria.SurveySatisfied)
					actions.Add("SurveySatisfied");

				if (summaryCriteria.SurveyDissatisfied)
					actions.Add("SurveyDissatisfied");

				if (summaryCriteria.SurveyVeryDissatisfied)
					actions.Add("SurveyVeryDissatisfied");

				if (summaryCriteria.SurveyNoUnexpectedMatches)
					actions.Add("SurveyNoUnexpectedMatches");

				if (summaryCriteria.SurveyUnexpectedMatches)
					actions.Add("SurveyUnexpectedMatches");

				if (summaryCriteria.SurveyReceivedInvitations)
					actions.Add("SurveyReceivedInvitations");

				if (summaryCriteria.SurveyDidNotReceiveInvitations)
					actions.Add("SurveyDidNotReceiveInvitations");
			}

			if (activityCriteria.IsNotNull())
			{
				if (activityCriteria.ActivitiesAssigned)
					actions.Add("ActivitiesAssigned");

				if (activityCriteria.AddedToLists)
					actions.Add("AddedToLists");

				if (activityCriteria.NotesAdded)
					actions.Add("NotesAdded");

				if (activityCriteria.StaffAssignments)
					actions.Add("StaffAssignments");

				if (activityCriteria.FindJobsForSeeker)
					actions.Add("FindJobsForSeeker");

				if (activityCriteria.EmailsSent)
					actions.Add("EmailsSent");

				if (activityCriteria.FollowUpIssues)
					actions.Add("FollowUpIssues");

				if (activityCriteria.IssuesResolved)
					actions.Add("IssuesResolved");
			}

			if (referralCriteria.IsNotNull())
			{
				if (referralCriteria.FailedToApply)
					actions.Add("FailedToApply");

				if (referralCriteria.FailedToReportToInterview)
					actions.Add("FailedToReportToInterview");

				if (referralCriteria.FailedToReportToJob)
					actions.Add("FailedToReportToJob");

				if (referralCriteria.FailedToRespondToInvitation)
					actions.Add("FailedToRespondToInvitation");

				if (referralCriteria.FoundJobFromOtherSource)
					actions.Add("FoundJobFromOtherSource");

				if (referralCriteria.Hired)
					actions.Add("Hired");

				if (referralCriteria.InterviewDenied)
					actions.Add("InterviewDenied");

				if (referralCriteria.InterviewScheduled)
					actions.Add("InterviewScheduled");

				if (referralCriteria.JobAlreadyFilled)
					actions.Add("JobAlreadyFilled");

				if (referralCriteria.NewApplicant)
					actions.Add("NewApplicant");

				if (referralCriteria.NotHired)
					actions.Add("NotHired");

				if (referralCriteria.NotQualified)
					actions.Add("NotQualified");

				if (referralCriteria.NotYetPlaced)
					actions.Add("NotYetPlaced");

				if (referralCriteria.Recommended)
					actions.Add("Recommended");

				if (referralCriteria.RefusedOffer)
					actions.Add("RefusedOffer");

				if (referralCriteria.RefusedReferral)
					actions.Add("RefusedReferral");

				if (referralCriteria.UnderConsideration)
					actions.Add("UnderConsideration");
			}

			return actions;
		}

		/// <summary>
		/// Gets the ordinals for looking up job seeker fields from a data reader
		/// </summary>
		/// <param name="reader">The reader.</param>
		/// <returns></returns>
		private static Dictionary<string, int> GetJobSeekerFieldsOrdinalLookup(IDataReader reader)
		{
			var allActionTotalFields = new List<string>
      {
        "Logins",  
        "PostingsViewed",  
        "ReferralRequests",  
        "SelfReferrals",  
        "StaffReferrals",  
        "NotesAdded",  
        "AddedToLists",  
        "ActivitiesAssigned",  
        "StaffAssignments",  
        "EmailsSent",  
        "FollowUpIssues",  
        "IssuesResolved",  
        "Hired",  
        "NotHired",  
        "FailedToApply",  
        "FailedToReportToInterview",  
        "InterviewDenied",  
        "InterviewScheduled",  
        "NewApplicant",  
        "Recommended",  
        "RefusedOffer",  
        "UnderConsideration",  
        "UsedOnlineResumeHelp",  
        "SavedJobAlerts",  
        "TargetingHighGrowthSectors",  
        "SelfReferralsExternal",  
        "StaffReferralsExternal",  
        "ReferralsApproved",  
        "FindJobsForSeeker",  
        "FailedToReportToJob",  
        "FailedToRespondToInvitation",  
        "FoundJobFromOtherSource",  
        "JobAlreadyFilled",  
        "NotQualified",  
        "NotYetPlaced",  
        "RefusedReferral",  
        "SurveyVerySatisfied",  
        "SurveySatisfied",  
        "SurveyDissatisfied",  
        "SurveyVeryDissatisfied",  
        "SurveyNoUnexpectedMatches",  
        "SurveyUnexpectedMatches",  
        "SurveyReceivedInvitations",  
        "SurveyDidNotReceiveInvitations",
        "ActivityServices"
      };

			var ordinalLookup = new Dictionary<string, int>();
			for (var ordinal = 0; ordinal < reader.FieldCount; ordinal++)
			{
				ordinalLookup.Add(reader.GetName(ordinal), ordinal);
			}

			allActionTotalFields.Where(field => !ordinalLookup.ContainsKey(field))
													.ToList()
													.ForEach(field => ordinalLookup.Add(field, reader.GetOrdinal("DummyTotal")));

			return ordinalLookup;
		}

		/// <summary>
		/// Runs a SQL to return a data table used in either charts or export
		/// </summary>
		/// <param name="reportSql">The report SQL.</param>
		/// <param name="reportName">Name of the report.</param>
		/// <param name="criteria">The criteria.</param>
		/// <returns>The data table</returns>
		private ReportDataTableModel GetJobSeekersDataTable(StringBuilder reportSql, string reportName, JobSeekersReportCriteria criteria)
		{
			var reportData = new ReportDataTableModel
			{
				FirstDataColumnIndex = 1,
				Data = new DataTable(reportName),
				DisplayType = criteria.DisplayType,
				IsGrouped = true
			};

			if (criteria.ChartGroupInfo.IsNull() || criteria.ChartGroupInfo.GroupBy.IsNull())
			{
				if (criteria.JobSeekerActivitySummaryInfo.IsNotNull() && criteria.JobSeekerActivitySummaryInfo.TotalCount)
				{
					reportData.FirstDataColumnIndex = 0;
				}
				else
				{
					reportData.FirstDataColumnIndex = 5;
					reportData.IsGrouped = false;
				}
			}

			using (var reader = Repositories.Report.ExecuteDataReader(reportSql.ToString()))
			{
				reportData.Data.Load(reader);
				reader.Close();
			}

			if (criteria.ExtraColumns.IsNull())
			{

				if (reportData.Data.Columns.Contains("VeteranType")) reportData.Data.Columns.Remove("VeteranType");
				if (reportData.Data.Columns.Contains("VeteranTransitionType")) reportData.Data.Columns.Remove("VeteranTransitionType");
				if (reportData.Data.Columns.Contains("VeteranMilitaryDischarge")) reportData.Data.Columns.Remove("VeteranMilitaryDischarge");
				if (reportData.Data.Columns.Contains("VeteranBranchOfService")) reportData.Data.Columns.Remove("VeteranBranchOfService");
			}
			else
			{
				if (!criteria.ExtraColumns.VeteranType)
					if (reportData.Data.Columns.Contains("VeteranType")) reportData.Data.Columns.Remove("VeteranType");

				if (!criteria.ExtraColumns.VeteranTransitionType)
					if (reportData.Data.Columns.Contains("VeteranTransitionType")) reportData.Data.Columns.Remove("VeteranTransitionType");

				if (!criteria.ExtraColumns.VeteranMilitaryDischarge)
					if (reportData.Data.Columns.Contains("VeteranMilitaryDischarge")) reportData.Data.Columns.Remove("VeteranMilitaryDischarge");

				if (!criteria.ExtraColumns.VeteranBranchOfService)
					if (reportData.Data.Columns.Contains("VeteranBranchOfService")) reportData.Data.Columns.Remove("VeteranBranchOfService");
			}

			var columns = reportData.Data.Columns.Cast<DataColumn>().ToList();
			columns.ForEach(column => column.ReadOnly = false);

			var columnNames = columns.Select(x => x.ColumnName).ToList();

			// Split office into comma delimited list
			if (columnNames.Contains("Office"))
			{
				var columnIndex = reportData.Data.Columns.IndexOf("Office");

				foreach (DataRow row in reportData.Data.Rows)
				{
					if (row[columnIndex].IsDBNull() || row[columnIndex].IsNull())
					{
						row[columnIndex] = null;
					}
					else
					{
						row[columnIndex] = ((string)row[columnIndex]).Replace("||", ", ");
					}
				}
			}

			// Format employment status
			if (columnNames.Contains("EmploymentStatus"))
			{
				var columnIndex = reportData.Data.Columns.IndexOf("EmploymentStatus");

				foreach (DataRow row in reportData.Data.Rows)
				{
					if (row[columnIndex].IsDBNull())
					{
						row[columnIndex] = "(Unspecified)";
					}
					else
					{
						var status = (EmploymentStatus?)(Convert.ToInt32(row[columnIndex]));
						row[columnIndex] = Helpers.Localisation.GetEnumLocalisedText(status, true);
					}
				}
			}

			// Format veteran type
			if (columnNames.Contains("VeteranType"))
			{
				var columnIndex = reportData.Data.Columns.IndexOf("VeteranType");

				foreach (DataRow row in reportData.Data.Rows)
				{
					if (row[columnIndex].IsDBNull())
					{
						row[columnIndex] = "(Unspecified)";
					}
					else
					{
						var status = (VeteranEra?)(Convert.ToInt32(row[columnIndex]));
						row[columnIndex] = Helpers.Localisation.GetEnumLocalisedText(status, true);
					}
				}
			}

			// Format transition type
			if (columnNames.Contains("VeteranTransitionType"))
			{
				var columnIndex = reportData.Data.Columns.IndexOf("VeteranTransitionType");

				foreach (DataRow row in reportData.Data.Rows)
				{
					if (row[columnIndex].IsDBNull())
					{
						row[columnIndex] = "(Unspecified)";
					}
					else
					{
						var status = (VeteranTransitionType?)(Convert.ToInt32(row[columnIndex]));
						row[columnIndex] = Helpers.Localisation.GetEnumLocalisedText(status, true);
					}
				}
			}

			// Format military dischage
			if (columnNames.Contains("VeteranMilitaryDischarge"))
			{
				var columnIndex = reportData.Data.Columns.IndexOf("VeteranMilitaryDischarge");

				foreach (DataRow row in reportData.Data.Rows)
				{
					if (row[columnIndex].IsDBNull())
					{
						row[columnIndex] = "(Unspecified)";
					}
					else
					{
						var status = (ReportMilitaryDischargeType?)(Convert.ToInt32(row[columnIndex]));
						row[columnIndex] = Helpers.Localisation.GetEnumLocalisedText(status, true);
					}
				}
			}

			// Format branch of service
			if (columnNames.Contains("VeteranBranchOfService"))
			{
				var columnIndex = reportData.Data.Columns.IndexOf("VeteranBranchOfService");

				foreach (DataRow row in reportData.Data.Rows)
				{
					if (row[columnIndex].IsDBNull())
					{
						row[columnIndex] = "(Unspecified)";
					}
					else
					{
						var status = (ReportMilitaryBranchOfService?)(Convert.ToInt32(row[columnIndex]));
						row[columnIndex] = Helpers.Localisation.GetEnumLocalisedText(status, true);
					}
				}
			}

			// Format education level
			if (columnNames.Contains("MinimumEducationLevel"))
			{
				var columnIndex = reportData.Data.Columns.IndexOf("MinimumEducationLevel");

				foreach (DataRow row in reportData.Data.Rows)
				{
					if (row[columnIndex].IsDBNull())
					{
						row[columnIndex] = "(Unspecified)";
					}
					else
					{
						var status = (EducationLevel?)(Convert.ToInt32(row[columnIndex]));
						row[columnIndex] = Helpers.Localisation.GetEnumLocalisedText(status, true);
					}
				}
			}

			var dummyIndex = -1;
			if (columnNames.Contains("DummyTotal"))
				dummyIndex = reportData.Data.Columns.IndexOf("DummyTotal");

			// Update column headings to proper text
			columns.ForEach(column =>
			{
				var localisationKey = string.Concat("Global.JobSeekers.Report.", column.ColumnName);
				var defaultText = column.ColumnName.SplitByPascalCase();
				switch (column.ColumnName)
				{
					case "EmploymentStatus":
						defaultText = "Status";
						break;
					case "VeteranTransitionType":
					case "VeteranMilitaryDischarge":
					case "VeteranBranchOfService":
						defaultText = defaultText.SubstringAfter("Veteran ");
						break;
					case "EmailAddress":
						defaultText = "Email";
						break;
				}
				column.ColumnName = Helpers.Localisation.Localise(localisationKey, defaultText);
			});

			// Remove the 'dummy total' which was purely used for setting action totals to zero when not required
			if (dummyIndex >= 0)
				reportData.Data.Columns.RemoveAt(dummyIndex);

			return reportData;
		}

		/// <summary>
		/// Maps generic education levels to lists of more specific levels
		/// </summary>
		/// <param name="minimumEducationLevels">The generic education levels</param>
		/// <returns>A list of specific levels</returns>
		private static List<EducationLevel?> MapEducationLevels(List<ReportJobSeekerEducationLevels?> minimumEducationLevels)
		{
			var mappedEducationLevels = new List<EducationLevel?>();

			minimumEducationLevels.ForEach(level =>
			{
				switch (level)
				{
					case ReportJobSeekerEducationLevels.NoDiploma:
						mappedEducationLevels.AddItems(
							EducationLevel.Grade_1_01,
							EducationLevel.Grade_2_02,
							EducationLevel.Grade_3_03,
							EducationLevel.Grade_4_04,
							EducationLevel.Grade_5_05,
							EducationLevel.Grade_6_06,
							EducationLevel.Grade_7_07,
							EducationLevel.Grade_8_08,
							EducationLevel.Grade_9_09,
							EducationLevel.Grade_10_10,
							EducationLevel.Grade_11_11,
							EducationLevel.Grade_12_No_Diploma_12
						);
						break;

					case ReportJobSeekerEducationLevels.HighSchoolDiplomaOrEquivalent:
						mappedEducationLevels.AddItems(
							EducationLevel.GED_88,
							EducationLevel.Grade_12_HS_Graduate_13,
							EducationLevel.Disabled_w_Cert_IEP_14);
						break;

					case ReportJobSeekerEducationLevels.HighSchoolDiplomaOnly:
						mappedEducationLevels.AddItems(EducationLevel.Grade_12_HS_Graduate_13);
						break;

					case ReportJobSeekerEducationLevels.HighSchoolEquivalencyDiplomaOnly:
						mappedEducationLevels.AddItems(EducationLevel.GED_88);
						break;

					case ReportJobSeekerEducationLevels.DisabledWithCertOnly:
						mappedEducationLevels.AddItems(EducationLevel.Disabled_w_Cert_IEP_14);
						break;


					case ReportJobSeekerEducationLevels.HighSchoolDiploma:
						mappedEducationLevels.AddItems(
							EducationLevel.GED_88,
							EducationLevel.Grade_12_HS_Graduate_13,
							EducationLevel.Disabled_w_Cert_IEP_14,
							EducationLevel.HS_1_Year_College_OR_VOC_Tech_No_Degree_15,
							EducationLevel.HS_2_Year_College_OR_VOC_Tech_No_Degree_16,
							EducationLevel.HS_3_Year_College_OR_VOC_Tech_No_Degree_17);
						break;

					case ReportJobSeekerEducationLevels.SomeCollegeNoDegree:
						mappedEducationLevels.AddItems(
							EducationLevel.HS_1_Year_College_OR_VOC_Tech_No_Degree_15,
							EducationLevel.HS_2_Year_College_OR_VOC_Tech_No_Degree_16,
							EducationLevel.HS_3_Year_College_OR_VOC_Tech_No_Degree_17);
						break;

					case ReportJobSeekerEducationLevels.AssociatesDegree:
						mappedEducationLevels.AddItems(
							EducationLevel.HS_1_Year_Vocational_Degree_18,
							EducationLevel.HS_2_Year_Vocational_Degree_19,
							EducationLevel.HS_3_Year_Vocational_Degree_20,
							EducationLevel.HS_1_Year_Associates_Degree_21,
							EducationLevel.HS_2_Year_Associates_Degree_22,
							EducationLevel.HS_3_Year_Associates_Degree_23);
						break;

					case ReportJobSeekerEducationLevels.BachelorsDegree:
						mappedEducationLevels.Add(EducationLevel.Bachelors_OR_Equivalent_24);
						break;

					case ReportJobSeekerEducationLevels.GraduateDegree:
						mappedEducationLevels.Add(EducationLevel.Masters_Degree_25);
						mappedEducationLevels.Add(EducationLevel.Doctorate_Degree_26);
						break;

					case ReportJobSeekerEducationLevels.MastersDegree:
						mappedEducationLevels.Add(EducationLevel.Masters_Degree_25);
						break;

					case ReportJobSeekerEducationLevels.DoctorateDegree:
						mappedEducationLevels.Add(EducationLevel.Doctorate_Degree_26);
						break;
				}
			});

			return mappedEducationLevels;
		}

		/// <summary>
		/// Maps old education levels to lists of current education levels
		/// </summary>
		/// <param name="educationLevels">list of education levels</param>
		/// <returns>List of mapped education levels</returns>
		private static List<EducationLevels?> MapMinimumEducationLevels(List<EducationLevels?> educationLevels)
		{
			var mappedEducationLevels = new List<EducationLevels?>(educationLevels);

			if (mappedEducationLevels.Contains(EducationLevels.GraduateDegree))
			{
				// Graduate option now replace by separate Masters and Doctorate options
				mappedEducationLevels.Remove(EducationLevels.GraduateDegree);
				if (!mappedEducationLevels.Contains(EducationLevels.MastersDegree))
					mappedEducationLevels.Add(EducationLevels.MastersDegree);
				if (!mappedEducationLevels.Contains(EducationLevels.DoctorateDegree))
					mappedEducationLevels.Add(EducationLevels.DoctorateDegree);
			}

			if (mappedEducationLevels.Contains(EducationLevels.HighSchoolDiploma))
			{
				// HighSchoolDiploma option now replace by separate NoDiploma,HighSchoolDiplomaOrEquivalent and SomeCollegeNoDegree options
				mappedEducationLevels.Remove(EducationLevels.HighSchoolDiploma);
				mappedEducationLevels.Add(EducationLevels.NoDiploma);
				mappedEducationLevels.Add(EducationLevels.HighSchoolDiplomaOrEquivalent);
				mappedEducationLevels.Add(EducationLevels.SomeCollegeNoDegree);
			}

			//add HighSchoolDiploma option for not to miss old jobs with HighSchoolDiploma option
			if (mappedEducationLevels.Contains(EducationLevels.NoDiploma) || mappedEducationLevels.Contains(EducationLevels.HighSchoolDiplomaOrEquivalent) || mappedEducationLevels.Contains(EducationLevels.SomeCollegeNoDegree))
				mappedEducationLevels.Add(EducationLevels.HighSchoolDiploma);
			//add Graduate option for not to miss old jobs with Graduate option
			if (mappedEducationLevels.Contains(EducationLevels.MastersDegree) || mappedEducationLevels.Contains(EducationLevels.DoctorateDegree))
				mappedEducationLevels.Add(EducationLevels.GraduateDegree);

			return mappedEducationLevels;
		}


		/// <summary>
		/// Converts a keyword criteria to a report data type
		/// </summary>
		/// <param name="keywordType">The keyword</param>
		/// <returns>The report data type</returns>
		private static int MapKeyWordTypeToJobSeekerDataType(KeyWord? keywordType)
		{
			switch (keywordType)
			{
				case KeyWord.Skills:
					return (int)ReportJobSeekerDataType.Skill;

				case KeyWord.Internships:
					return (int)ReportJobSeekerDataType.Internship;

				case KeyWord.JobTitle:
					return (int)ReportJobSeekerDataType.JobTitle;

				case KeyWord.JobDescription:
					return (int)ReportJobSeekerDataType.JobDescription;

				case KeyWord.EmployerName:
					return (int)ReportJobSeekerDataType.JobEmployer;

				case KeyWord.EducationQualifications:
					return (int)ReportJobSeekerDataType.EducationQualification;
			}

			return 0;
		}

		#endregion

		#region Job Order Helper Methods

		/// <summary>
		/// Gets the SQL for getting the job orders from the database matching the criteria
		/// </summary>
		/// <param name="criteria">The criteria</param>
		/// <param name="countSql">Will be set to a SQL specific for counting the total</param>
		/// <param name="forScreen">Whether the data is needed for the screen, or export</param>
		/// <returns>The SQL to get the matching job orders</returns>
		private StringBuilder GetJobOrdersSql(JobOrdersReportCriteria criteria, StringBuilder countSql, bool forScreen)
		{
			const string selectClauseFull = @"
        JO.[Id],
        JO.[JobTitle],
        JO.[EmployerName],
        JO.[County],
        JO.[State],
        JO.[Office],
        JO.[FocusJobId]";

			const string selectClauseForExport = @"
        JO.[JobTitle],
        JO.[EmployerName],
        JO.[County],
        JO.[State],
        JO.[Office]";

			var chartSelectField = string.Empty;

			var whereClause = new StringBuilder();
			var orderClause = new StringBuilder("ORDER BY ");
			var groupByClause = new StringBuilder("GROUP BY JO.[Id]");
			var havingClause = new StringBuilder();

			var whereStep = " WHERE";
			var havingStep = " HAVING";

			var salaryInfo = criteria.SalaryInfo;
			if (salaryInfo.IsNotNull())
			{
				if (salaryInfo.MinimumSalary.HasValue)
				{
					whereClause.Append(whereStep).AppendFormat(" JO.MinSalary >= {0}", salaryInfo.MinimumSalary.Value);
					whereStep = " AND";
				}

				if (salaryInfo.MaximumSalary.HasValue)
				{
					whereClause.Append(whereStep).AppendFormat(" JO.MaxSalary <= {0}", salaryInfo.MaximumSalary.Value);
					whereStep = " AND";
				}

				if (salaryInfo.SalaryFrequency.HasValue)
				{
					whereClause.Append(whereStep).AppendFormat(" JO.SalaryFrequency = {0}", (int)(salaryInfo.SalaryFrequency.Value));
					whereStep = " AND";
				}
			}

			var jobOrdercharacteristicInfo = criteria.JobOrderCharacteristicsInfo;
			if (jobOrdercharacteristicInfo.IsNotNull())
			{
				if (jobOrdercharacteristicInfo.JobStatuses.IsNotNullOrEmpty())
				{
					whereClause.Append(whereStep).AppendFormat(" JO.JobStatus IN ({0})", jobOrdercharacteristicInfo.JobStatuses.AsCommaListForSql());
					whereStep = " AND";
				}

				if (jobOrdercharacteristicInfo.EducationLevels.IsNotNullOrEmpty())
				{
					whereClause.Append(whereStep).AppendFormat(" JO.MinimumEducationLevel IN ({0})", MapMinimumEducationLevels(jobOrdercharacteristicInfo.EducationLevels).AsCommaListForSql());
					whereStep = " AND";
				}
			}

			var employerCharacterticsInfo = criteria.EmployerCharacteristicsInfo;
			if (employerCharacterticsInfo.IsNotNull())
			{
				if (employerCharacterticsInfo.WorkOpportunitiesTaxCreditInterests.IsNotNullOrEmpty())
				{
					var wotcBitMask = 0;
					employerCharacterticsInfo.WorkOpportunitiesTaxCreditInterests.ForEach(wotc =>
					{
						if (wotc.IsNotNull())
							wotcBitMask = wotcBitMask | (int)wotc;
					});

					whereClause.Append(whereStep).AppendFormat(" JO.WorkOpportunitiesTaxCreditHires & {0} != 0", wotcBitMask);
					whereStep = " AND";
				}

				if (employerCharacterticsInfo.AccountTypesList.IsNotNullOrEmpty())
				{
					// The not indicated account type maps to null in the AccountTypeId field
					// 0 is not a valid state for account type id but to make the reporting robust treat as not indicated

					// This if statement needs to produce the following depending on the contents of the accounttypeslist:
					// where (E.AccountTypeId is null OR E.AccountTypeId = 0 OR E.AccountTypeId in ( <Id for Direct Employer>, <Id for StaffingAgency> ))
					if (employerCharacterticsInfo.AccountTypesList.Contains(AccountTypes.NotIndicated))
					{
						whereClause.Append(whereStep).AppendFormat(" (JO.AccountTypeId is null OR JO.AccountTypeId = 0 ");
						if (employerCharacterticsInfo.AccountTypesList.Count > 1)
						{
							whereStep = "OR";

							AppendSpecifiedAccountTypesToWhereClause(employerCharacterticsInfo, whereClause, whereStep, "JO");

						}
						whereClause.Append(")");
					}

					// This else statement needs to produce the following depending on the contents of the accounttypeslist:
					// where E.AccountTypeId in ( <Id for Direct Employer>, <Id for StaffingAgency> )
					else
					{
						AppendSpecifiedAccountTypesToWhereClause(employerCharacterticsInfo, whereClause, whereStep, "JO");
					}

					whereStep = " AND";
				}

				if (employerCharacterticsInfo.FEINs.IsNotNullOrEmpty())
				{
					whereClause.Append(whereStep).AppendFormat(" JO.FederalEmployerIdentificationNumber IN ({0})", employerCharacterticsInfo.FEINs.AsCommaListForSql());
					whereStep = " AND";
				}

				if (employerCharacterticsInfo.EmployerNames.IsNotNullOrEmpty())
				{
					whereClause.Append(whereStep).AppendFormat(" JO.EmployerName IN ({0})", employerCharacterticsInfo.EmployerNames.AsCommaListForSql());
					whereStep = " AND";
				}

				if (employerCharacterticsInfo.JobTitles.IsNotNullOrEmpty())
				{
					whereClause.Append(whereStep).AppendFormat(" JO.JobTitle IN ({0})", employerCharacterticsInfo.JobTitles.AsCommaListForSql());
					whereStep = " AND";
				}
			}

			var complianceCriteria = criteria.JobOrderComplianceInfo;
			if (complianceCriteria.IsNotNull())
			{
				if (complianceCriteria.ForeignLabourTypes.IsNotNullOrEmpty())
				{
					whereClause.Append(whereStep).AppendFormat(" JO.ForeignLabourType IN ({0})", complianceCriteria.ForeignLabourTypes.AsCommaListForSql());
					whereStep = " AND";
				}

				if (complianceCriteria.NonForeignLabourCertification.HasValue)
				{
					whereClause.Append(whereStep).AppendFormat(" JO.ForeignLabourCertification = {0}", (!complianceCriteria.NonForeignLabourCertification.Value).AsOneOrZero());
					whereStep = " AND";
				}

				if (complianceCriteria.FederalContractor.HasValue)
				{
					whereClause.Append(whereStep).AppendFormat(" JO.FederalContractor = {0}", complianceCriteria.FederalContractor.Value.AsOneOrZero());
					whereStep = " AND";
				}

				if (complianceCriteria.CourtOrderedAffirmativeAction.HasValue)
				{
					whereClause.Append(whereStep).AppendFormat(" JO.CourtOrderedAffirmativeAction = {0}", complianceCriteria.CourtOrderedAffirmativeAction.Value.AsOneOrZero());
					whereStep = " AND";
				}
			}

			var locationInfo = criteria.LocationInfo;
			if (locationInfo.IsNotNull())
			{
				if (locationInfo.Offices.IsNotNullOrEmpty())
				{
					var offices = locationInfo.Offices.ToArray();

					whereClause.Append(whereStep)
											.AppendFormat(" EXISTS(SELECT TOP 1 1 FROM [Report.JobOrderOffice] JOO WHERE JOO.JobOrderId = JO.Id AND JOO.OfficeName IN ({0}))", offices.AsCommaListForSql());
					whereStep = " AND";
				}

				if (locationInfo.Counties.IsNotNullOrEmpty())
				{
					whereClause.Append(whereStep).AppendFormat(" JO.County IN ({0})", locationInfo.Counties.AsCommaListForSql());
					whereStep = " AND";
				}

				if (locationInfo.PostalCode.IsNotNullOrEmpty() && locationInfo.DistanceFromPostalCode.IsNotNull())
				{
					var postalCodePoint = Repositories.Library.Query<PostalCode>().FirstOrDefault(postalCode => postalCode.Code == locationInfo.PostalCode);
					if (postalCodePoint.IsNotNull())
					{
						// See www.movable-type.co.uk/scripts/latlong.html for explanation of calculation (Spherical law of cosiness)

						var latitude = Math.PI * postalCodePoint.Latitude / 180.0;
						var longitude = Math.PI * postalCodePoint.Longitude / 180.0;
						var distanceInKm = (double)locationInfo.DistanceFromPostalCode / 0.62137;
						/*
						query =
							query.Where(
								jobOrder => !string.IsNullOrEmpty(jobOrder.PostalCode) &&
								Math.Acos(Math.Sin(latitude) * Math.Sin(jobOrder.LatitudeRadians) +
													Math.Cos(latitude) * Math.Cos(jobOrder.LatitudeRadians) *
													Math.Cos(jobOrder.LongitudeRadians - longitude)) * 6371 < distanceInKm);
						*/
						whereClause.Append(whereStep).AppendFormat(@" JO.PostalCode IS NOT NULL 
              AND ACOS(ROUND({0} * SIN(JO.LatitudeRadians) +
                       {1} * COS(JO.LatitudeRadians) *
                       COS(JO.LongitudeRadians - {2}), 9)) * 6371 < {3}", Math.Round(Math.Sin(latitude), 10), Math.Round(Math.Cos(latitude), 10), Math.Round(longitude, 10), Math.Round(distanceInKm, 10));
						whereStep = " AND";
					}
				}
			}

			var keywordInfo = criteria.KeywordInfo;
			if (keywordInfo.IsNotNull())
			{
				if (keywordInfo.KeyWordSearch.IsNotNullOrEmpty() && keywordInfo.KeyWords.IsNotNullOrEmpty() && keywordInfo.SearchType.HasValue)
				{
					var checkAll = keywordInfo.KeyWordSearch.Contains(KeyWord.FullJob);

					var checkTitle = checkAll || keywordInfo.KeyWordSearch.Contains(KeyWord.JobTitle);
					var checkDescription = checkAll || keywordInfo.KeyWordSearch.Contains(KeyWord.JobDescription);
					var checkEmployer = checkAll || keywordInfo.KeyWordSearch.Contains(KeyWord.EmployerName);

					whereClause.Append(whereStep).Append("(");

					var innerWhereStep = "";
					keywordInfo.KeyWords.ForEach(word =>
					{
						whereClause.Append(innerWhereStep).Append(" (");

						var innerClause = new List<string>();

						if (checkTitle)
							innerClause.Add(string.Format("JO.JobTitle LIKE '%{0}%'", word.EscapeSql()));

						if (checkDescription)
							innerClause.Add(string.Format("JO.[Description] LIKE '%{0}%'", word.EscapeSql()));

						if (checkEmployer)
							innerClause.Add(string.Format("JO.EmployerName LIKE '%{0}%'", word.EscapeSql()));

						whereClause.Append(string.Join(" OR ", innerClause));
						whereClause.Append(")");

						innerWhereStep = (keywordInfo.SearchType == KeywordSearchType.All) ? " AND" : " OR";
					});

					whereClause.Append(")");
				}
			}

			var ncrcInfo = criteria.NcrcInfo;
			if (ncrcInfo.IsNotNull() && ncrcInfo.NcrcLevels.Any() && (ncrcInfo.Required || ncrcInfo.Preferred))
			{
				whereClause.Append(whereStep).Append(" (");
				var firstRun = true;
				if (ncrcInfo.Required && !ncrcInfo.Preferred)
				{
					ncrcInfo.NcrcLevels.ForEach(ncrcLevel =>
					{
						if (!firstRun)
						{
							whereClause.Append(" OR ");
						}

						whereClause.AppendFormat("(JO.NCRCLevel = '{0}' AND JO.NCRCLevelRequired = 1)", ncrcLevel);
						firstRun = false;
					});
				}
				else if (!ncrcInfo.Required && ncrcInfo.Preferred)
				{
					ncrcInfo.NcrcLevels.ForEach(ncrcLevel =>
					{
						if (!firstRun)
						{
							whereClause.Append(" OR ");
						}

						whereClause.AppendFormat("(JO.NCRCLevel = '{0}' AND JO.NCRCLevelRequired = 0)", ncrcLevel);
						firstRun = false;
					});
				}
				else
				{
					ncrcInfo.NcrcLevels.ForEach(ncrcLevel =>
					{
						if (!firstRun)
						{
							whereClause.Append(" OR ");
						}

						whereClause.AppendFormat("(JO.NCRCLevel = '{0}')", ncrcLevel);
						firstRun = false;
					});
				}

				whereClause.Append(")");
				whereStep = " AND";
			}

			var summaryInfo = criteria.JobOrdersActivitySummaryInfo;
			var groupInfo = criteria.ChartGroupInfo;

			var showTotalCount = (summaryInfo.IsNotNull() && summaryInfo.TotalCount);
			var isGrouped = groupInfo.IsNotNull() || showTotalCount;

			// Ordering on 'text' fields can be done in SQL
			var orderBy = ReportJobOrderOrderBy.JobTitle;
			var sqlOrderDirection = "ASC";
			var orderingByAction = false;

			var orderInfo = criteria.OrderInfo;
			if (orderInfo.IsNotNull() && (summaryInfo.IsNull() || !summaryInfo.TotalCount))
			{
				orderBy = orderInfo.OrderBy;
				sqlOrderDirection = orderInfo.Direction == ReportOrder.Ascending ? "ASC" : "DESC";
			}

			if (groupInfo.IsNotNull())
			{
				switch (groupInfo.GroupBy)
				{
					case ReportJobOrderGroupBy.State:
						orderBy = ReportJobOrderOrderBy.State;
						chartSelectField = "State";
						break;
					case ReportJobOrderGroupBy.Office:
						orderBy = ReportJobOrderOrderBy.Office;
						chartSelectField = "Office";
						break;
				}
				sqlOrderDirection = "ASC";
			}

			switch (orderBy)
			{
				case ReportJobOrderOrderBy.JobTitle:
				case ReportJobOrderOrderBy.County:
				case ReportJobOrderOrderBy.State:
				case ReportJobOrderOrderBy.Office:
					orderClause.AppendFormat("JO.{0} {1}", orderBy, sqlOrderDirection);
					groupByClause.AppendFormat(", JO.{0}", orderBy);
					break;

				case ReportJobOrderOrderBy.Employer:
					orderClause.AppendFormat("JO.EmployerName {0}", sqlOrderDirection);
					groupByClause.Append(", JO.EmployerName");
					break;

				case ReportJobOrderOrderBy.StaffReferrals:
				case ReportJobOrderOrderBy.SelfReferrals:
				case ReportJobOrderOrderBy.ReferralsRequested:
				case ReportJobOrderOrderBy.EmployerInvitationsSent:
				case ReportJobOrderOrderBy.InvitedJobSeekerViewed:
				case ReportJobOrderOrderBy.InvitedJobSeekerClicked:
				case ReportJobOrderOrderBy.FailedToRespondToInvitation:
				case ReportJobOrderOrderBy.FailedToReportToJob:
				case ReportJobOrderOrderBy.FoundJobFromOtherSource:
				case ReportJobOrderOrderBy.JobAlreadyFilled:
				case ReportJobOrderOrderBy.NewApplicant:
				case ReportJobOrderOrderBy.NotQualified:
				case ReportJobOrderOrderBy.RefusedReferral:
				case ReportJobOrderOrderBy.UnderConsideration:
					orderingByAction = true;
					orderClause.AppendFormat("SUM(JOA.{0}) {1}", orderBy, sqlOrderDirection);
					break;
				case ReportJobOrderOrderBy.Hired:
				case ReportJobOrderOrderBy.NotHired:
				case ReportJobOrderOrderBy.NotYetPlaced:
					orderingByAction = true;
					orderClause.AppendFormat("SUM(JOA.Applicants{0}) {1}", orderBy, sqlOrderDirection);
					break;
				case ReportJobOrderOrderBy.FailedToApply:
					orderingByAction = true;
					orderClause.AppendFormat("SUM(JOA.ApplicantsDidNotApply) {0}", sqlOrderDirection);
					break;
				case ReportJobOrderOrderBy.FailedToReportToInterview:
					orderingByAction = true;
					orderClause.AppendFormat("SUM(JOA.ApplicantsFailedToShow) {0}", sqlOrderDirection);
					break;
				case ReportJobOrderOrderBy.InterviewDenied:
					orderingByAction = true;
					orderClause.AppendFormat("SUM(JOA.ApplicantsDeniedInterviews) {0}", sqlOrderDirection);
					break;
				case ReportJobOrderOrderBy.InterviewScheduled:
					orderingByAction = true;
					orderClause.AppendFormat("SUM(JOA.ApplicantsInterviewed) {0}", sqlOrderDirection);
					break;
				case ReportJobOrderOrderBy.Recommended:
					orderingByAction = true;
					orderClause.AppendFormat("SUM(JOA.ApplicantRecommended) {0}", sqlOrderDirection);
					break;
				case ReportJobOrderOrderBy.RefusedOffer:
					orderingByAction = true;
					orderClause.AppendFormat("SUM(JOA.JobOffersRefused) {0}", sqlOrderDirection);
					break;
				case ReportJobOrderOrderBy.TotalReferrals:
					orderingByAction = true;
					orderClause.AppendFormat("SUM(JOA.SelfReferrals + JOA.StaffReferrals + JOA.ReferralsRequested) {0}", sqlOrderDirection);
					break;
			}

			var actionToDate = DateTime.Now.Date;
			var actionFromDate = new DateTime(1900, 01, 01);

			var dateInfo = criteria.DateRangeInfo;
			if (dateInfo.IsNotNull())
			{
				if (dateInfo.FromDate.HasValue || dateInfo.Days.HasValue)
					actionFromDate = dateInfo.FromDate.HasValue ? dateInfo.FromDate.Value : actionToDate.AddDays(0 - dateInfo.Days.Value);

				if (dateInfo.ToDate.HasValue)
					actionToDate = dateInfo.ToDate.Value;
			}

			var startRow = criteria.PageIndex * criteria.PageSize + 1;
			var endRow = startRow + criteria.PageSize - 1;

			var actionTotalFields = GetJobOrdersRequiredActions(criteria.JobOrdersActivitySummaryInfo, criteria.JobOrdersReferralOutcomesInfo);

			if (criteria.ZeroesHandling != ReportZeroesHandling.GetAll)
			{
				havingClause.Append(havingStep).Append("(");
				havingStep = string.Empty;

				actionTotalFields.ForEach(field =>
				{
					havingClause.Append(havingStep).AppendFormat(" SUM(JOA.{0}) > 0", field);
					havingStep = criteria.ZeroesHandling == ReportZeroesHandling.ExcludeAllZeroes ? " OR" : " AND";
				});

				havingClause.Append(")");
			}

			var withSql = new StringBuilder();

			withSql.Append(@"
WITH FilteredJobOrders AS 
(
  SELECT
    JO.[Id]");

			if (!isGrouped)
				withSql.Append(", ").AppendLine().AppendFormat(@"ROW_NUMBER() OVER ({0}) AS RowNumber", orderClause);

			withSql.AppendLine().Append(@"FROM [Report.JobOrderView] JO");

			var includeHavingClause = false;
			if (actionTotalFields.Any() && (orderingByAction || criteria.ZeroesHandling != ReportZeroesHandling.GetAll))
			{
				withSql.AppendFormat(@"
  LEFT JOIN [Report.JobOrderAction] JOA
	  ON JOA.JobOrderId = JO.Id
    AND JOA.ActionDate BETWEEN '{0}' AND '{1}'",
					actionFromDate.ToString("dd MMMM yyyy"),
					actionToDate.ToString("dd MMMM yyyy"));

				includeHavingClause = true;
			}

			withSql.AppendLine().Append(whereClause);
			withSql.AppendLine().Append(groupByClause);
			if (includeHavingClause)
				withSql.AppendLine().Append(havingClause);

			withSql.AppendLine().Append(")");

			countSql.Append(withSql);
			countSql.Replace(string.Format("ROW_NUMBER() OVER ({0})", orderClause), "NULL");
			countSql.AppendLine().Append("SELECT COUNT(1) FROM FilteredJobOrders");

			var reportSql = new StringBuilder();
			reportSql.Append(withSql);

			var selectDivider = "";

			if (!isGrouped)
			{
				reportSql.AppendLine().Append("SELECT 0 AS DummyTotal,");
				reportSql.AppendLine().Append(forScreen ? selectClauseFull : selectClauseForExport);

				selectDivider = ",";
			}
			else if (chartSelectField.IsNotNullOrEmpty())
			{
				reportSql.AppendLine().AppendFormat("SELECT CAST(JO.[{0}] AS VARCHAR(400)) AS [{0}]", chartSelectField);

				selectDivider = ",";
			}
			else
			{
				reportSql.AppendLine().Append("SELECT ");
			}

			if (actionTotalFields.Any())
			{
				reportSql.Append(selectDivider);
				reportSql.AppendLine().Append(string.Join(",\r\n", actionTotalFields.Select(field => string.Format("ISNULL(SUM(JOA.[{0}]), 0) AS {0}", field))));

				selectDivider = ",";
			}

			if (showTotalCount || (isGrouped && !actionTotalFields.Any()))
			{
				reportSql.Append(selectDivider);
				reportSql.AppendLine().Append("COUNT(1) AS TotalJobs");
			}

			reportSql.AppendLine().Append(@"
FROM FilteredJobOrders FJO
INNER JOIN [Report.JobOrderView] JO
ON JO.Id = FJO.Id");

			if (actionTotalFields.Any())
			{
				reportSql.AppendLine().AppendFormat(@"
LEFT JOIN [Report.JobOrderAction] JOA
ON JOA.JobOrderId = JO.Id
AND JOA.ActionDate BETWEEN '{0}' AND '{1}'",
				actionFromDate.ToString("dd MMMM yyyy"),
				actionToDate.ToString("dd MMMM yyyy"));
			}

			if (!isGrouped)
			{
				reportSql.AppendLine().AppendFormat("WHERE FJO.RowNumber BETWEEN {0} AND {1}", startRow, endRow);
				reportSql.AppendLine().Append("GROUP BY");
				reportSql.AppendLine().Append(selectClauseFull);
				reportSql.Append(",");
				reportSql.AppendLine().Append("FJO.RowNumber");
				reportSql.AppendLine().Append("ORDER BY FJO.RowNumber");
			}
			else
			{
				if (chartSelectField.IsNotNullOrEmpty())
				{
					reportSql.AppendLine().AppendFormat("GROUP BY JO.[{0}]", chartSelectField);
					reportSql.AppendLine().AppendFormat("ORDER BY JO.[{0}]", chartSelectField);
				}
			}

			return reportSql;
		}

		/// <summary>
		/// Gets the list of actions to total on the job orders report
		/// </summary>
		/// <param name="activityCriteria">The activity criteria.</param>
		/// <param name="referralCriteria">The referral criteria.</param>
		/// <returns>The list to populate.</returns>
		private static List<string> GetJobOrdersRequiredActions(JobOrdersActivitySummaryCriteria activityCriteria, ReferralOutcomesCriteria referralCriteria)
		{
			var actions = new List<string>();

			if (activityCriteria.IsNotNull())
			{
				if (activityCriteria.InvitedJobSeekerViewed)
					actions.Add("InvitedJobSeekerViewed");

				if (activityCriteria.InvitedJobSeekerClicked)
					actions.Add("InvitedJobSeekerClicked");

				if (activityCriteria.EmployerInvitationsSent)
					actions.Add("EmployerInvitationsSent");

				if (activityCriteria.SelfReferrals || activityCriteria.TotalReferrals)
					actions.Add("SelfReferrals");

				if (activityCriteria.StaffReferrals || activityCriteria.TotalReferrals)
					actions.Add("StaffReferrals");

				if (activityCriteria.ReferralsRequested || activityCriteria.TotalReferrals)
					actions.Add("ReferralsRequested");
			}

			if (referralCriteria.IsNotNull())
			{
				if (referralCriteria.FailedToApply)
					actions.Add("ApplicantsDidNotApply");

				if (referralCriteria.FailedToReportToInterview)
					actions.Add("ApplicantsFailedToShow");

				if (referralCriteria.FailedToReportToJob)
					actions.Add("FailedToReportToJob");

				if (referralCriteria.FailedToRespondToInvitation)
					actions.Add("FailedToRespondToInvitation");

				if (referralCriteria.FoundJobFromOtherSource)
					actions.Add("FoundJobFromOtherSource");

				if (referralCriteria.Hired)
					actions.Add("ApplicantsHired");

				if (referralCriteria.InterviewDenied)
					actions.Add("ApplicantsDeniedInterviews");

				if (referralCriteria.InterviewScheduled)
					actions.Add("ApplicantsInterviewed");

				if (referralCriteria.JobAlreadyFilled)
					actions.Add("JobAlreadyFilled");

				if (referralCriteria.NewApplicant)
					actions.Add("NewApplicant");

				if (referralCriteria.NotHired)
					actions.Add("ApplicantsNotHired");

				if (referralCriteria.NotQualified)
					actions.Add("NotQualified");

				if (referralCriteria.NotYetPlaced)
					actions.Add("ApplicantsNotYetPlaced");

				if (referralCriteria.Recommended)
					actions.Add("ApplicantRecommended");

				if (referralCriteria.RefusedOffer)
					actions.Add("JobOffersRefused");

				if (referralCriteria.RefusedReferral)
					actions.Add("RefusedReferral");

				if (referralCriteria.UnderConsideration)
					actions.Add("UnderConsideration");
			}

			return actions;
		}

		/// <summary>
		/// Gets the ordinals for looking up job order fields from a data reader
		/// </summary>
		/// <param name="reader">The reader.</param>
		/// <returns></returns>
		private static Dictionary<string, int> GetJobOrderFieldsOrdinalLookup(IDataReader reader)
		{
			var allActionTotalFields = new List<string>
      {
        "StaffReferrals",
        "SelfReferrals",
        "EmployerInvitationsSent",
        "ApplicantsInterviewed",
        "ApplicantsFailedToShow",
        "ApplicantsDeniedInterviews",
        "ApplicantsHired",
        "JobOffersRefused",
        "ApplicantsDidNotApply",
        "InvitedJobSeekerViewed",
        "InvitedJobSeekerClicked",
        "ReferralsRequested",
        "ApplicantsNotHired",
        "ApplicantsNotYetPlaced",
        "FailedToRespondToInvitation",
        "FailedToReportToJob",
        "FoundJobFromOtherSource",
        "JobAlreadyFilled",
        "NewApplicant",
        "NotQualified",
        "ApplicantRecommended",
        "RefusedReferral",
        "UnderConsideration"
      };

			var ordinalLookup = new Dictionary<string, int>();
			for (var ordinal = 0; ordinal < reader.FieldCount; ordinal++)
			{
				ordinalLookup.Add(reader.GetName(ordinal), ordinal);
			}

			allActionTotalFields.Where(field => !ordinalLookup.ContainsKey(field))
													.ToList()
													.ForEach(field => ordinalLookup.Add(field, reader.GetOrdinal("DummyTotal")));

			return ordinalLookup;
		}

		/// <summary>
		/// Runs a SQL to return a data table used in either charts or export
		/// </summary>
		/// <param name="reportSql">The report SQL.</param>
		/// <param name="reportName">Name of the report.</param>
		/// <param name="criteria">The criteria.</param>
		/// <returns>The data table</returns>
		private ReportDataTableModel GetJobOrdersDataTable(StringBuilder reportSql, string reportName, JobOrdersReportCriteria criteria)
		{
			var reportData = new ReportDataTableModel
			{
				FirstDataColumnIndex = 1,
				Data = new DataTable(reportName),
				DisplayType = criteria.DisplayType,
				IsGrouped = true
			};

			if (criteria.ChartGroupInfo.IsNull() || criteria.ChartGroupInfo.GroupBy.IsNull())
			{
				if (criteria.JobOrdersActivitySummaryInfo.IsNotNull() && criteria.JobOrdersActivitySummaryInfo.TotalCount)
				{
					reportData.FirstDataColumnIndex = 0;
				}
				else
				{
					reportData.FirstDataColumnIndex = 5;
					reportData.IsGrouped = false;
				}
			}

			using (var reader = Repositories.Report.ExecuteDataReader(reportSql.ToString()))
			{
				reportData.Data.Load(reader);
				reader.Close();
			}

			var columns = reportData.Data.Columns.Cast<DataColumn>().ToList();
			columns.ForEach(column => column.ReadOnly = false);

			var columnNames = columns.Select(x => x.ColumnName).ToList();

			// Split office into comma delimited list
			if (columnNames.Contains("Office"))
			{
				var columnIndex = reportData.Data.Columns.IndexOf("Office");

				foreach (DataRow row in reportData.Data.Rows)
				{
					if (row[columnIndex].IsDBNull() || row[columnIndex].IsNull())
					{
						row[columnIndex] = null;
					}
					else
					{
						row[columnIndex] = ((string)row[columnIndex]).Replace("||", ", ");
					}
				}
			}

			var dummyIndex = -1;
			if (columnNames.Contains("DummyTotal"))
				dummyIndex = reportData.Data.Columns.IndexOf("DummyTotal");

			// Update column headings to proper text
			columns.ForEach(column =>
			{
				var localisationKey = string.Concat("Global.JobOrders.Report.", column.ColumnName);
				var defaultText = column.ColumnName.SplitByPascalCase();
				column.ColumnName = Helpers.Localisation.Localise(localisationKey, defaultText);
			});

			// Remove the 'dummy total' which was purely used for setting action totals to zero when not required
			if (dummyIndex >= 0)
				reportData.Data.Columns.RemoveAt(dummyIndex);

			return reportData;
		}

		/*
		/// <summary>
		/// Gets a list of job orders from the database matching the criteria
		/// </summary>
		/// <param name="criteria">The criteria</param>
		/// <returns>A list of matching job orders</returns>
		private List<JobOrdersReportView> GetJobOrders(JobOrdersReportCriteria criteria)
		{
			var query = Repositories.Report.JobOrderViews;

			var salaryInfo = criteria.SalaryInfo;
			if (salaryInfo.IsNotNull())
			{
				if (salaryInfo.MinimumSalary.HasValue)
					query = query.Where(jobOrder => jobOrder.MinSalary >= salaryInfo.MinimumSalary);

				if (salaryInfo.MaximumSalary.HasValue)
					query = query.Where(jobOrder => jobOrder.MaxSalary <= salaryInfo.MaximumSalary);

				if (salaryInfo.SalaryFrequency.HasValue)
					query = query.Where(jobOrder => jobOrder.SalaryFrequency == salaryInfo.SalaryFrequency);
			}

			var jobOrdercharacteristicInfo = criteria.JobOrderCharacteristicsInfo;
			if (jobOrdercharacteristicInfo.IsNotNull())
			{
				if (jobOrdercharacteristicInfo.JobStatuses.IsNotNullOrEmpty())
					query = query.Where(jobOrder => jobOrdercharacteristicInfo.JobStatuses.Contains(jobOrder.JobStatus));

				if (jobOrdercharacteristicInfo.EducationLevels.IsNotNullOrEmpty())
					query = query.Where(jobOrder => jobOrdercharacteristicInfo.EducationLevels.Contains(jobOrder.MinimumEducationLevel));
			}

			var employerCharacterticsInfo = criteria.EmployerCharacteristicsInfo;
			if (employerCharacterticsInfo.IsNotNull())
			{
				if (employerCharacterticsInfo.WorkOpportunitiesTaxCreditInterests.IsNotNullOrEmpty())
				{
					var wotcBitMask = (WorkOpportunitiesTaxCreditCategories?) 0;
					employerCharacterticsInfo.WorkOpportunitiesTaxCreditInterests.ForEach(wotc => wotcBitMask = wotcBitMask | wotc);
					query = query.Where(jobOrder => (jobOrder.WorkOpportunitiesTaxCreditHires & wotcBitMask) != 0);
				}

				if (employerCharacterticsInfo.FEINs.IsNotNullOrEmpty())
					query = query.Where(jobOrder => employerCharacterticsInfo.FEINs.Contains(jobOrder.FederalEmployerIdentificationNumber));

				if (employerCharacterticsInfo.EmployerNames.IsNotNullOrEmpty())
					query = query.Where(jobOrder => employerCharacterticsInfo.EmployerNames.Contains(jobOrder.EmployerName));

				if (employerCharacterticsInfo.JobTitles.IsNotNullOrEmpty())
					query = query.Where(jobOrder => employerCharacterticsInfo.JobTitles.Contains(jobOrder.JobTitle));
			}

			var complianceCriteria = criteria.JobOrderComplianceInfo;
			if (complianceCriteria.IsNotNull())
			{
				if (complianceCriteria.ForeignLabourTypes.IsNotNullOrEmpty())
					query = query.Where(jobOrder => complianceCriteria.ForeignLabourTypes.Contains(jobOrder.ForeignLabourType));

				if (complianceCriteria.NonForeignLabourCertification.HasValue)
					query = query.Where(jobOrder => jobOrder.ForeignLabourCertification == !(complianceCriteria.NonForeignLabourCertification));

				if (complianceCriteria.FederalContractor.HasValue)
					query = query.Where(jobOrder => jobOrder.FederalContractor == complianceCriteria.FederalContractor);

				if (complianceCriteria.CourtOrderedAffirmativeAction.HasValue)
					query = query.Where(jobOrder => jobOrder.CourtOrderedAffirmativeAction == complianceCriteria.CourtOrderedAffirmativeAction);
			}

			var locationInfo = criteria.LocationInfo;
			if (locationInfo.IsNotNull())
			{
				if (locationInfo.Offices.IsNotNullOrEmpty())
				{
					var offices = locationInfo.Offices.ToArray();
					query = query.Where(jobOrder => jobOrder.JobOrderOffices.Any(office => offices.Contains(office.OfficeName)));
				}

				if (locationInfo.Counties.IsNotNullOrEmpty())
					query = query.Where(jobOrder => locationInfo.Counties.Contains(jobOrder.County));

				if (locationInfo.PostalCode.IsNotNullOrEmpty() && locationInfo.DistanceFromPostalCode.IsNotNull())
				{
					var postalCodePoint = Repositories.Library.Query<PostalCode>().FirstOrDefault(postalCode => postalCode.Code == locationInfo.PostalCode);
					if (postalCodePoint.IsNotNull())
					{
						// See www.movable-type.co.uk/scripts/latlong.html for explanation of calculation (Spherical law of cosiness)

						var latitude = Math.PI * postalCodePoint.Latitude / 180.0;
						var longitude = Math.PI * postalCodePoint.Longitude / 180.0;
						var distanceInKm = (double)locationInfo.DistanceFromPostalCode / 0.62137;

						query =
							query.Where(
								jobOrder => !string.IsNullOrEmpty(jobOrder.PostalCode) &&
								Math.Acos(Math.Sin(latitude) * Math.Sin(jobOrder.LatitudeRadians) +
													Math.Cos(latitude) * Math.Cos(jobOrder.LatitudeRadians) *
													Math.Cos(jobOrder.LongitudeRadians - longitude)) * 6371 < distanceInKm);
					}
				}
			}

			List<long> filteredJobOrderIds = null;
			var filterByKeyword = false;

			var keywordInfo = criteria.KeywordInfo;
			if (keywordInfo.IsNotNull())
			{
				if (keywordInfo.KeyWordSearch.IsNotNullOrEmpty() && keywordInfo.KeyWords.IsNotNullOrEmpty() && keywordInfo.SearchType.HasValue)
				{
					var checkAll = keywordInfo.KeyWordSearch.Contains(KeyWord.FullJob);

					var checkTitle = checkAll || keywordInfo.KeyWordSearch.Contains(KeyWord.JobTitle);
					var checkDescription = checkAll || keywordInfo.KeyWordSearch.Contains(KeyWord.JobDescription);
					var checkEmployer = checkAll || keywordInfo.KeyWordSearch.Contains(KeyWord.EmployerName);

					filteredJobOrderIds = Repositories.Report.JobOrderKeywordFilter(keywordInfo.KeyWords, checkTitle, checkDescription, checkEmployer, keywordInfo.SearchType == KeywordSearchType.All)
																										.Select(filter => filter.Id)
																										.ToList();

					filterByKeyword = true;
				}
			}

			// If less that 100 job orders match the keyword filter, the ids can be added to the main filter
			if (filterByKeyword && filteredJobOrderIds.Count <= MaxIdsToFilterInProc)
			{
				query = query.Where(jobOrder => filteredJobOrderIds.Contains(jobOrder.Id));
				filterByKeyword = false;
			}

			// Ordering on 'text' fields can be done in SQL
			ReportJobOrderOrderBy? orderBy = null;
			ReportOrder? orderDirection = null;

			var orderInfo = criteria.OrderInfo;
			if (orderInfo.IsNotNull())
			{
				orderBy = orderInfo.OrderBy;
				orderDirection = orderInfo.Direction;
			}

			var groupInfo = criteria.ChartGroupInfo;
			if (groupInfo.IsNotNull())
			{
				switch (groupInfo.GroupBy)
				{
					case ReportJobOrderGroupBy.State:
						orderBy = ReportJobOrderOrderBy.State;
						break;
					case ReportJobOrderGroupBy.Office:
						orderBy = ReportJobOrderOrderBy.Office;
						break;
				}
				orderDirection = ReportOrder.Ascending;
			}

			switch (orderBy)
			{
				case ReportJobOrderOrderBy.JobTitle:
					query = orderDirection == ReportOrder.Ascending ? query.OrderBy(jobOrder => jobOrder.JobTitle) : query.OrderByDescending(jobOrder => jobOrder.JobTitle);
					break;

				case ReportJobOrderOrderBy.Employer:
					query = orderDirection == ReportOrder.Ascending ? query.OrderBy(jobOrder => jobOrder.EmployerName) : query.OrderByDescending(jobOrder => jobOrder.EmployerName);
					break;

				case ReportJobOrderOrderBy.County:
					query = orderDirection == ReportOrder.Ascending ? query.OrderBy(jobOrder => jobOrder.County) : query.OrderByDescending(jobOrder => jobOrder.County);
					break;

				case ReportJobOrderOrderBy.State:
					query = orderDirection == ReportOrder.Ascending ? query.OrderBy(jobOrder => jobOrder.State) : query.OrderByDescending(jobOrder => jobOrder.State);
					break;

				case ReportJobOrderOrderBy.Office:
					query = orderDirection == ReportOrder.Ascending ? query.OrderBy(jobOrder => jobOrder.Office) : query.OrderByDescending(jobOrder => jobOrder.Office);
					break;
			}

			var jobOrders = query.Select(jobOrder => new JobOrdersReportView
																									 {
																										 Id = jobOrder.Id,
																										 JobTitle = jobOrder.JobTitle,
																										 //CountyId = jobOrder.CountyId,
																										 County = jobOrder.County,
																										 //StateId = jobOrder.StateId,
																										 State = jobOrder.State,
																										 Office = jobOrder.Office,
																										 Employer = jobOrder.EmployerName,
																										 FocusJobId = jobOrder.FocusJobId
																									 }).ToList();

			// If there were more than 100 filtered job order ids, filter the results now
			if (filterByKeyword)
				jobOrders = jobOrders.Where(jobOrder => filteredJobOrderIds.Contains(jobOrder.Id)).ToList();

			Dictionary<long, JobOrderActionDto> jobOrderTotalsLookup;

			if (jobOrders.Any())
			{
				var toDate = DateTime.Now.Date;
				var fromDate = new DateTime(1900, 01, 01);

				var dateRangeInfo = criteria.DateRangeInfo;
				if (dateRangeInfo.IsNotNull())
				{
					if (dateRangeInfo.FromDate.HasValue || dateRangeInfo.Days.HasValue)
						fromDate = dateRangeInfo.FromDate.HasValue ? dateRangeInfo.FromDate.Value : toDate.AddDays(0 - dateRangeInfo.Days.Value);

					if (dateRangeInfo.ToDate.HasValue)
						toDate = dateRangeInfo.ToDate.Value;
				}

				var jobOrderTotals = jobOrders.Count <= MaxIdsToFilterInProc
					? Repositories.Report.JobOrderActionTotals(fromDate, toDate, jobOrders.Select(jobOrder => jobOrder.Id).ToList())
					: Repositories.Report.JobOrderActionTotals(fromDate, toDate);

				jobOrderTotalsLookup = jobOrderTotals.ToDictionary(total => total.Id, total => total.AsDto());
			}
			else
			{
				jobOrderTotalsLookup = new Dictionary<long, JobOrderActionDto>();
			}

			var jobOrderTotalsLookup = new Dictionary<long, JobOrderActionView>();

			jobOrders.ForEach(order =>
			{
				order.Totals = jobOrderTotalsLookup.ContainsKey(order.Id)
													? jobOrderTotalsLookup[order.Id]
													: new JobOrderActionView();
			});

			// When ordering by data total, this is done directly on the returned data set, not in SQL
			if (orderInfo.IsNotNull())
			{
				switch (orderInfo.OrderBy)
				{
					case ReportJobOrderOrderBy.StaffReferrals:
						jobOrders = orderInfo.Direction == ReportOrder.Ascending ? jobOrders.OrderBy(jobOrder => jobOrder.Totals.StaffReferrals).ToList() : jobOrders.OrderByDescending(jobOrder => jobOrder.Totals.StaffReferrals).ToList();
						break;
					case ReportJobOrderOrderBy.SelfReferrals:
						jobOrders = orderInfo.Direction == ReportOrder.Ascending ? jobOrders.OrderBy(jobOrder => jobOrder.Totals.SelfReferrals).ToList() : jobOrders.OrderByDescending(jobOrder => jobOrder.Totals.SelfReferrals).ToList();
						break;
					case ReportJobOrderOrderBy.ReferralsRequested:
						jobOrders = orderInfo.Direction == ReportOrder.Ascending ? jobOrders.OrderBy(jobOrder => jobOrder.Totals.ReferralsRequested).ToList() : jobOrders.OrderByDescending(jobOrder => jobOrder.Totals.ReferralsRequested).ToList();
						break;
					case ReportJobOrderOrderBy.TotalReferrals:
						jobOrders = orderInfo.Direction == ReportOrder.Ascending ? jobOrders.OrderBy(jobOrder => jobOrder.Totals.TotalReferrals).ToList() : jobOrders.OrderByDescending(jobOrder => jobOrder.Totals.TotalReferrals).ToList();
						break;
					case ReportJobOrderOrderBy.EmployerInvitationsSent:
						jobOrders = orderInfo.Direction == ReportOrder.Ascending ? jobOrders.OrderBy(jobOrder => jobOrder.Totals.EmployerInvitationsSent).ToList() : jobOrders.OrderByDescending(jobOrder => jobOrder.Totals.EmployerInvitationsSent).ToList();
						break;
					case ReportJobOrderOrderBy.InvitedJobSeekerViewed:
						jobOrders = orderInfo.Direction == ReportOrder.Ascending ? jobOrders.OrderBy(jobOrder => jobOrder.Totals.InvitedJobSeekerViewed).ToList() : jobOrders.OrderByDescending(jobOrder => jobOrder.Totals.InvitedJobSeekerViewed).ToList();
						break;
					case ReportJobOrderOrderBy.InvitedJobSeekerClicked:
						jobOrders = orderInfo.Direction == ReportOrder.Ascending ? jobOrders.OrderBy(jobOrder => jobOrder.Totals.InvitedJobSeekerClicked).ToList() : jobOrders.OrderByDescending(jobOrder => jobOrder.Totals.InvitedJobSeekerClicked).ToList();
						break;
					case ReportJobOrderOrderBy.InterviewScheduled:
						jobOrders = orderInfo.Direction == ReportOrder.Ascending ? jobOrders.OrderBy(jobOrder => jobOrder.Totals.ApplicantsInterviewed).ToList() : jobOrders.OrderByDescending(jobOrder => jobOrder.Totals.ApplicantsInterviewed).ToList();
						break;
					case ReportJobOrderOrderBy.FailedToReportToInterview:
						jobOrders = orderInfo.Direction == ReportOrder.Ascending ? jobOrders.OrderBy(jobOrder => jobOrder.Totals.ApplicantsFailedToShow).ToList() : jobOrders.OrderByDescending(jobOrder => jobOrder.Totals.ApplicantsFailedToShow).ToList();
						break;
					case ReportJobOrderOrderBy.InterviewDenied:
						jobOrders = orderInfo.Direction == ReportOrder.Ascending ? jobOrders.OrderBy(jobOrder => jobOrder.Totals.ApplicantsDeniedInterviews).ToList() : jobOrders.OrderByDescending(jobOrder => jobOrder.Totals.ApplicantsDeniedInterviews).ToList();
						break;
					case ReportJobOrderOrderBy.Hired:
						jobOrders = orderInfo.Direction == ReportOrder.Ascending ? jobOrders.OrderBy(jobOrder => jobOrder.Totals.ApplicantsHired).ToList() : jobOrders.OrderByDescending(jobOrder => jobOrder.Totals.ApplicantsHired).ToList();
						break;
					case ReportJobOrderOrderBy.RefusedOffer:
						jobOrders = orderInfo.Direction == ReportOrder.Ascending ? jobOrders.OrderBy(jobOrder => jobOrder.Totals.JobOffersRefused).ToList() : jobOrders.OrderByDescending(jobOrder => jobOrder.Totals.JobOffersRefused).ToList();
						break;
					case ReportJobOrderOrderBy.FailedToApply:
						jobOrders = orderInfo.Direction == ReportOrder.Ascending ? jobOrders.OrderBy(jobOrder => jobOrder.Totals.ApplicantsDidNotApply).ToList() : jobOrders.OrderByDescending(jobOrder => jobOrder.Totals.ApplicantsDidNotApply).ToList();
						break;
					case ReportJobOrderOrderBy.NotHired:
						jobOrders = orderInfo.Direction == ReportOrder.Ascending ? jobOrders.OrderBy(jobOrder => jobOrder.Totals.ApplicantsNotHired).ToList() : jobOrders.OrderByDescending(jobOrder => jobOrder.Totals.ApplicantsNotHired).ToList();
						break;
					case ReportJobOrderOrderBy.NotYetPlaced:
						jobOrders = orderInfo.Direction == ReportOrder.Ascending ? jobOrders.OrderBy(jobOrder => jobOrder.Totals.ApplicantsNotYetPlaced).ToList() : jobOrders.OrderByDescending(jobOrder => jobOrder.Totals.ApplicantsNotYetPlaced).ToList();
						break;
					case ReportJobOrderOrderBy.FailedToRespondToInvitation:
						jobOrders = orderInfo.Direction == ReportOrder.Ascending ? jobOrders.OrderBy(jobOrder => jobOrder.Totals.FailedToRespondToInvitation).ToList() : jobOrders.OrderByDescending(jobOrder => jobOrder.Totals.FailedToRespondToInvitation).ToList();
						break;
					case ReportJobOrderOrderBy.FailedToReportToJob:
						jobOrders = orderInfo.Direction == ReportOrder.Ascending ? jobOrders.OrderBy(jobOrder => jobOrder.Totals.FailedToReportToJob).ToList() : jobOrders.OrderByDescending(jobOrder => jobOrder.Totals.FailedToReportToJob).ToList();
						break;
					case ReportJobOrderOrderBy.FoundJobFromOtherSource:
						jobOrders = orderInfo.Direction == ReportOrder.Ascending ? jobOrders.OrderBy(jobOrder => jobOrder.Totals.FoundJobFromOtherSource).ToList() : jobOrders.OrderByDescending(jobOrder => jobOrder.Totals.FoundJobFromOtherSource).ToList();
						break;
					case ReportJobOrderOrderBy.JobAlreadyFilled:
						jobOrders = orderInfo.Direction == ReportOrder.Ascending ? jobOrders.OrderBy(jobOrder => jobOrder.Totals.JobAlreadyFilled).ToList() : jobOrders.OrderByDescending(jobOrder => jobOrder.Totals.JobAlreadyFilled).ToList();
						break;
					case ReportJobOrderOrderBy.NewApplicant:
						jobOrders = orderInfo.Direction == ReportOrder.Ascending ? jobOrders.OrderBy(jobOrder => jobOrder.Totals.NewApplicant).ToList() : jobOrders.OrderByDescending(jobOrder => jobOrder.Totals.NewApplicant).ToList();
						break;
					case ReportJobOrderOrderBy.NotQualified:
						jobOrders = orderInfo.Direction == ReportOrder.Ascending ? jobOrders.OrderBy(jobOrder => jobOrder.Totals.NotQualified).ToList() : jobOrders.OrderByDescending(jobOrder => jobOrder.Totals.NotQualified).ToList();
						break;
					case ReportJobOrderOrderBy.Recommended:
						jobOrders = orderInfo.Direction == ReportOrder.Ascending ? jobOrders.OrderBy(jobOrder => jobOrder.Totals.ApplicantRecommended).ToList() : jobOrders.OrderByDescending(jobOrder => jobOrder.Totals.ApplicantRecommended).ToList();
						break;
					case ReportJobOrderOrderBy.RefusedReferral:
						jobOrders = orderInfo.Direction == ReportOrder.Ascending ? jobOrders.OrderBy(jobOrder => jobOrder.Totals.RefusedReferral).ToList() : jobOrders.OrderByDescending(jobOrder => jobOrder.Totals.RefusedReferral).ToList();
						break;
					case ReportJobOrderOrderBy.UnderConsideration:
						jobOrders = orderInfo.Direction == ReportOrder.Ascending ? jobOrders.OrderBy(jobOrder => jobOrder.Totals.UnderConsideration).ToList() : jobOrders.OrderByDescending(jobOrder => jobOrder.Totals.UnderConsideration).ToList();
						break;
				}
			}

			return jobOrders;
		}
		*/

		/*
		/// <summary>
		/// Converts a list of job orders to a datatable
		/// </summary>
		/// <param name="jobOrders">The list of job orders</param>
		/// <param name="reportName">The name of the datatable</param>
		/// <param name="criteria">The criteria to determine which columns are shown</param>
		/// <returns>The data table</returns>
		private ReportDataTableModel ConvertJobOrdersToDataTable(IEnumerable<JobOrdersReportView> jobOrders, string reportName, JobOrdersReportCriteria criteria)
		{
			var reportData = new ReportDataTableModel
			{
				FirstDataColumnIndex = 1,
				Data = new DataTable(reportName),
				DisplayType = criteria.DisplayType,
				IsGrouped = true
			};

			var groupInfo = criteria.ChartGroupInfo.IsNull() ? null : criteria.ChartGroupInfo.GroupBy;

			switch (groupInfo)
			{
				case ReportJobOrderGroupBy.State:
					AddColumnToDataTable<string>(reportData, "Global.JobOrderReport.State", Constants.Reporting.JobOrderReportDataColumns.State);
					break;

				case ReportJobOrderGroupBy.Office:
					AddColumnToDataTable<string>(reportData, "Global.JobOrderReport.Office", Constants.Reporting.JobOrderReportDataColumns.Office);
					break;

				default:
					reportData.FirstDataColumnIndex = 5;
					reportData.IsGrouped = false;
					AddColumnToDataTable<string>(reportData, "Global.JobOrderReport.JobTitle", Constants.Reporting.JobOrderReportDataColumns.JobTitle);
					AddColumnToDataTable<string>(reportData, "Global.JobOrderReport.Employer", Constants.Reporting.JobOrderReportDataColumns.Employer);
					AddColumnToDataTable<string>(reportData, "Global.JobOrderReport.County", Constants.Reporting.JobOrderReportDataColumns.County);
					AddColumnToDataTable<string>(reportData, "Global.JobOrderReport.State", Constants.Reporting.JobOrderReportDataColumns.State);
					AddColumnToDataTable<string>(reportData, "Global.JobOrderReport.Office", Constants.Reporting.JobOrderReportDataColumns.Office);
					break;
			}

			var activityCriteria = criteria.JobOrdersActivitySummaryInfo;
			var referralCriteria = criteria.JobOrdersReferralOutcomesInfo;

			if (activityCriteria.IsNotNull())
			{
				if (activityCriteria.InvitedJobSeekerViewed)
					AddColumnToDataTable<int>(reportData, "Global.JobOrderReport.InvitedJobSeekerViewed", Constants.Reporting.JobOrderReportDataColumns.InvitedJobSeekerViewed);

				if (activityCriteria.InvitedJobSeekerClicked)
					AddColumnToDataTable<int>(reportData, "Global.JobOrderReport.InvitedJobSeekerClicked", Constants.Reporting.JobOrderReportDataColumns.InvitedJobSeekerClicked);

				if (activityCriteria.EmployerInvitationsSent)
					AddColumnToDataTable<int>(reportData, "Global.JobOrderReport.EmployerInvitationsSent", Constants.Reporting.JobOrderReportDataColumns.EmployerInvitationsSent);

				if (activityCriteria.SelfReferrals)
					AddColumnToDataTable<int>(reportData, "Global.JobOrderReport.SelfReferrals", Constants.Reporting.JobOrderReportDataColumns.SelfReferrals);

				if (activityCriteria.StaffReferrals)
					AddColumnToDataTable<int>(reportData, "Global.JobOrderReport.StaffReferrals", Constants.Reporting.JobOrderReportDataColumns.StaffReferrals);

				if (activityCriteria.ReferralsRequested)
					AddColumnToDataTable<int>(reportData, "Global.JobOrderReport.ReferralsRequested", Constants.Reporting.JobOrderReportDataColumns.ReferralsRequested);

				if (activityCriteria.TotalReferrals)
					AddColumnToDataTable<int>(reportData, "Global.JobOrderReport.TotalReferrals", Constants.Reporting.JobOrderReportDataColumns.TotalReferrals);
			}

			if (referralCriteria.IsNotNull())
			{
				if (referralCriteria.FailedToApply)
					AddColumnToDataTable<int>(reportData, "Global.SharedReport.FailedToApply", Constants.Reporting.SharedReportDataColumns.FailedToApply);

				if (referralCriteria.FailedToReportToInterview)
					AddColumnToDataTable<int>(reportData, "Global.SharedReport.FailedToReportToInterview", Constants.Reporting.SharedReportDataColumns.FailedToReportToInterview);

				if (referralCriteria.FailedToReportToJob)
					AddColumnToDataTable<int>(reportData, "Global.SharedReport.FailedToReportToJob", Constants.Reporting.SharedReportDataColumns.FailedToReportToJob);

				if (referralCriteria.FailedToRespondToInvitation)
					AddColumnToDataTable<int>(reportData, "Global.SharedReport.FailedToRespondToInvitation", Constants.Reporting.SharedReportDataColumns.FailedToRespondToInvitation);

				if (referralCriteria.FoundJobFromOtherSource)
					AddColumnToDataTable<int>(reportData, "Global.SharedReport.FoundJobFromOtherSource", Constants.Reporting.SharedReportDataColumns.FoundJobFromOtherSource);

				if (referralCriteria.Hired)
					AddColumnToDataTable<int>(reportData, "Global.SharedReport.Hired", Constants.Reporting.SharedReportDataColumns.Hired);

				if (referralCriteria.InterviewDenied)
					AddColumnToDataTable<int>(reportData, "Global.SharedReport.InterviewDenied", Constants.Reporting.SharedReportDataColumns.InterviewDenied);

				if (referralCriteria.InterviewScheduled)
					AddColumnToDataTable<int>(reportData, "Global.SharedReport.InterviewScheduled", Constants.Reporting.SharedReportDataColumns.InterviewScheduled);

				if (referralCriteria.JobAlreadyFilled)
					AddColumnToDataTable<int>(reportData, "Global.SharedReport.JobAlreadyFilled", Constants.Reporting.SharedReportDataColumns.JobAlreadyFilled);

				if (referralCriteria.NewApplicant)
					AddColumnToDataTable<int>(reportData, "Global.SharedReport.NewApplicant", Constants.Reporting.SharedReportDataColumns.NewApplicant);

				if (referralCriteria.NotHired)
					AddColumnToDataTable<int>(reportData, "Global.SharedReport.NotHired", Constants.Reporting.SharedReportDataColumns.NotHired);

				if (referralCriteria.NotQualified)
					AddColumnToDataTable<int>(reportData, "Global.SharedReport.NotQualified", Constants.Reporting.SharedReportDataColumns.NotQualified);

				if (referralCriteria.NotYetPlaced)
					AddColumnToDataTable<int>(reportData, "Global.SharedReport.NotYetPlaced", Constants.Reporting.SharedReportDataColumns.NotYetPlaced);

				if (referralCriteria.Recommended)
					AddColumnToDataTable<int>(reportData, "Global.SharedReport.Recommended", Constants.Reporting.SharedReportDataColumns.Recommended);

				if (referralCriteria.RefusedOffer)
					AddColumnToDataTable<int>(reportData, "Global.SharedReport.RefusedOffer", Constants.Reporting.SharedReportDataColumns.RefusedOffer);

				if (referralCriteria.RefusedReferral)
					AddColumnToDataTable<int>(reportData, "Global.SharedReport.RefusedReferral", Constants.Reporting.SharedReportDataColumns.RefusedReferral);

				if (referralCriteria.UnderConsideration)
					AddColumnToDataTable<int>(reportData, "Global.SharedReport.UnderConsideration", Constants.Reporting.SharedReportDataColumns.UnderConsideration);
			}

			var totalColumns = reportData.Data.Columns.Count;

			var showTotalCount = false;
			if (groupInfo.IsNotNull())
			{
				if (totalColumns == reportData.FirstDataColumnIndex || (activityCriteria.IsNotNull() && activityCriteria.TotalCount))
				{
					AddColumnToDataTable<int>(reportData, "Global.JobOrderReport.TotalAccounts", Constants.Reporting.JobOrderReportDataColumns.TotalAccounts);
					showTotalCount = true;
					totalColumns++;
				}
			}

			var dataTableRows = new Dictionary<string, DataRow>();

			foreach (var jobOrderData in jobOrders)
			{
				DataRow row;
				int colIndex;

				var rowKey = "";
				var rowTitle = "(Unspecified)";

				switch (groupInfo)
				{
					case ReportJobOrderGroupBy.State:
						if (jobOrderData.StateId.HasValue)
						{
							rowKey = jobOrderData.StateId.ToString();
							rowTitle = jobOrderData.State;
						}
						break;
					case ReportJobOrderGroupBy.Office:
						rowKey = jobOrderData.Office;
						rowTitle = jobOrderData.Office;
						break;

					default:
						rowKey = jobOrderData.Id.ToString(CultureInfo.InvariantCulture);
						rowTitle = jobOrderData.JobTitle;
						break;
				}

				if (dataTableRows.ContainsKey(rowKey))
				{
					row = dataTableRows[rowKey];
				}
				else
				{
					row = reportData.Data.NewRow();
					row[0] = rowTitle;

					if (reportData.FirstDataColumnIndex > 1)
					{
						row[1] = jobOrderData.Employer;
						row[2] = jobOrderData.County;
						row[3] = jobOrderData.State;
						row[4] = jobOrderData.Office.Replace("||", ", ");
					}

					for (colIndex = reportData.FirstDataColumnIndex; colIndex < totalColumns; colIndex++)
					{
						row[colIndex] = 0;
					}
					dataTableRows.Add(rowKey, row);
					reportData.Data.Rows.Add(row);
				}

				colIndex = reportData.FirstDataColumnIndex;

				if (activityCriteria.IsNotNull())
				{
					if (activityCriteria.TotalCount)
						UpdateColumnValue(row, ref colIndex, 1);

					if (activityCriteria.InvitedJobSeekerViewed)
						UpdateColumnValue(row, ref colIndex, jobOrderData.Totals.InvitedJobSeekerViewed);

					if (activityCriteria.InvitedJobSeekerClicked)
						UpdateColumnValue(row, ref colIndex, jobOrderData.Totals.InvitedJobSeekerClicked);

					if (activityCriteria.EmployerInvitationsSent)
						UpdateColumnValue(row, ref colIndex, jobOrderData.Totals.EmployerInvitationsSent);

					if (activityCriteria.SelfReferrals)
						UpdateColumnValue(row, ref colIndex, jobOrderData.Totals.SelfReferrals);

					if (activityCriteria.StaffReferrals)
						UpdateColumnValue(row, ref colIndex, jobOrderData.Totals.StaffReferrals);

					if (activityCriteria.ReferralsRequested)
						UpdateColumnValue(row, ref colIndex, jobOrderData.Totals.ReferralsRequested);

					if (activityCriteria.TotalReferrals)
						UpdateColumnValue(row, ref colIndex, jobOrderData.Totals.TotalReferrals);
				}

				if (referralCriteria.IsNotNull())
				{
					if (referralCriteria.FailedToApply)
						UpdateColumnValue(row, ref colIndex, jobOrderData.Totals.ApplicantsDidNotApply);

					if (referralCriteria.FailedToReportToInterview)
						UpdateColumnValue(row, ref colIndex, jobOrderData.Totals.ApplicantsFailedToShow);

					if (referralCriteria.FailedToReportToJob)
						UpdateColumnValue(row, ref colIndex, jobOrderData.Totals.FailedToReportToJob);

					if (referralCriteria.FailedToRespondToInvitation)
						UpdateColumnValue(row, ref colIndex, jobOrderData.Totals.FailedToRespondToInvitation);

					if (referralCriteria.FoundJobFromOtherSource)
						UpdateColumnValue(row, ref colIndex, jobOrderData.Totals.FoundJobFromOtherSource);

					if (referralCriteria.Hired)
						UpdateColumnValue(row, ref colIndex, jobOrderData.Totals.ApplicantsHired);

					if (referralCriteria.InterviewDenied)
						UpdateColumnValue(row, ref colIndex, jobOrderData.Totals.ApplicantsDeniedInterviews);

					if (referralCriteria.InterviewScheduled)
						UpdateColumnValue(row, ref colIndex, jobOrderData.Totals.ApplicantsInterviewed);

					if (referralCriteria.JobAlreadyFilled)
						UpdateColumnValue(row, ref colIndex, jobOrderData.Totals.JobAlreadyFilled);

					if (referralCriteria.NewApplicant)
						UpdateColumnValue(row, ref colIndex, jobOrderData.Totals.NewApplicant);

					if (referralCriteria.NotHired)
						UpdateColumnValue(row, ref colIndex, jobOrderData.Totals.ApplicantsNotHired);

					if (referralCriteria.NotQualified)
						UpdateColumnValue(row, ref colIndex, jobOrderData.Totals.NotQualified);

					if (referralCriteria.NotYetPlaced)
						UpdateColumnValue(row, ref colIndex, jobOrderData.Totals.ApplicantsNotYetPlaced);

					if (referralCriteria.Recommended)
						UpdateColumnValue(row, ref colIndex, jobOrderData.Totals.ApplicantRecommended);

					if (referralCriteria.RefusedOffer)
						UpdateColumnValue(row, ref colIndex, jobOrderData.Totals.JobOffersRefused);

					if (referralCriteria.RefusedReferral)
						UpdateColumnValue(row, ref colIndex, jobOrderData.Totals.RefusedReferral);

					if (referralCriteria.UnderConsideration)
						UpdateColumnValue(row, ref colIndex, jobOrderData.Totals.UnderConsideration);
				}

				if (showTotalCount)
					UpdateColumnValue(row, ref colIndex, 1);
			}

			return reportData;
		}
		*/
		#endregion

		#region Employer Helper Methods

		/// <summary>
		/// Gets the SQL for getting the employers from the database matching the criteria
		/// </summary>
		/// <param name="criteria">The criteria</param>
		/// <param name="countSql">Will be set to a SQL specific for counting the total</param>
		/// <param name="forScreen">Whether the data is needed for the screen, or export</param>
		/// <returns>The SQL to get the matching employers</returns>
		private StringBuilder GetEmployersSql(EmployersReportCriteria criteria, StringBuilder countSql, bool forScreen)
		{
			// This method is currently 440 lines approx.
			// TODO: break each statement out into smaller methods and unit test

			const string selectClauseFull = @"
        E.[Id],
        E.[Name],
        E.[County],
        E.[State],
        E.[Office],
        E.[FocusEmployerId],
        E.[FocusBusinessUnitId]";

			const string selectClauseForExport = @"
        E.[Name],
        E.[County],
        E.[State],
        E.[Office]";

			var chartSelectField = string.Empty;

			var whereClause = new StringBuilder();
			var orderClause = new StringBuilder("ORDER BY ");
			var groupByClause = new StringBuilder("GROUP BY E.[Id]");
			var havingClause = new StringBuilder();

			var whereStep = " WHERE";
			var havingStep = " HAVING";

			var salaryInfo = criteria.SalaryInfo;
			if (salaryInfo.IsNotNull())
			{
				if (salaryInfo.MinimumSalary.HasValue)
				{
					whereClause.Append(whereStep).AppendFormat(" E.MinSalary >= {0}", salaryInfo.MinimumSalary.Value);
					whereStep = " AND";
				}

				if (salaryInfo.MaximumSalary.HasValue)
				{
					whereClause.Append(whereStep).AppendFormat(" E.MaxSalary <= {0}", salaryInfo.MaximumSalary.Value);
					whereStep = " AND";
				}

				if (salaryInfo.SalaryFrequency.HasValue)
				{
					whereClause.Append(whereStep).AppendFormat(" E.SalaryFrequency = {0}", (int)(salaryInfo.SalaryFrequency.Value));
					whereStep = " AND";
				}
			}

			var jobOrdercharacteristicInfo = criteria.JobOrderCharacteristicsInfo;
			if (jobOrdercharacteristicInfo.IsNotNull())
			{
				if (jobOrdercharacteristicInfo.JobStatuses.IsNotNullOrEmpty())
				{
					whereClause.Append(whereStep).AppendFormat(" E.JobStatus IN ({0})", jobOrdercharacteristicInfo.JobStatuses.AsCommaListForSql());
					whereStep = " AND";
				}

				if (jobOrdercharacteristicInfo.EducationLevels.IsNotNullOrEmpty())
				{
					whereClause.Append(whereStep).AppendFormat(" E.MinimumEducationLevel IN ({0})", MapMinimumEducationLevels(jobOrdercharacteristicInfo.EducationLevels).AsCommaListForSql());
					whereStep = " AND";
				}
			}

			var employerCharacterticsInfo = criteria.EmployerCharacteristicsInfo;
			if (employerCharacterticsInfo.IsNotNull())
			{
				if (employerCharacterticsInfo.WorkOpportunitiesTaxCreditInterests.IsNotNullOrEmpty())
				{
					var wotcBitMask = 0;
					employerCharacterticsInfo.WorkOpportunitiesTaxCreditInterests.ForEach(wotc =>
					{
						if (wotc.IsNotNull())
							wotcBitMask = wotcBitMask | (int)wotc;
					});

					whereClause.Append(whereStep).AppendFormat(" E.WorkOpportunitiesTaxCreditHires & {0} != 0", wotcBitMask);
					whereStep = " AND";
				}

				if (employerCharacterticsInfo.AccountTypesList.IsNotNullOrEmpty())
				{
					// The not indicated account type maps to null in the AccountTypeId field
					// 0 is not a valid state for account type id but to make the reporting robust treat as not indicated

					// This if statement needs to produce the following depending on the contents of the accounttypeslist:
					// where (E.AccountTypeId is null OR E.AccountTypeId = 0 OR E.AccountTypeId in ( <Id for Direct Employer>, <Id for StaffingAgency> ))
					if (employerCharacterticsInfo.AccountTypesList.Contains(AccountTypes.NotIndicated))
					{
						whereClause.Append(whereStep).AppendFormat(" (E.AccountTypeId is null OR E.AccountTypeId = 0 ");
						if (employerCharacterticsInfo.AccountTypesList.Count > 1)
						{
							whereStep = "OR";

							AppendSpecifiedAccountTypesToWhereClause(employerCharacterticsInfo, whereClause, whereStep);

						}
						whereClause.Append(")");
					}

					// This else statement needs to produce the following depending on the contents of the accounttypeslist:
					// where E.AccountTypeId in ( <Id for Direct Employer>, <Id for StaffingAgency> )
					else
					{
						AppendSpecifiedAccountTypesToWhereClause(employerCharacterticsInfo, whereClause, whereStep);
					}

					whereStep = " AND";

				}

				if (employerCharacterticsInfo.FEINs.IsNotNullOrEmpty())
				{
					whereClause.Append(whereStep).AppendFormat(" E.FederalEmployerIdentificationNumber IN ({0})", employerCharacterticsInfo.FEINs.AsCommaListForSql());
					whereStep = " AND";
				}

				if (employerCharacterticsInfo.EmployerNames.IsNotNullOrEmpty())
				{
					whereClause.Append(whereStep).AppendFormat(" E.[Name] IN ({0})", employerCharacterticsInfo.EmployerNames.AsCommaListForSql());
					whereStep = " AND";
				}

				if (employerCharacterticsInfo.JobTitles.IsNotNullOrEmpty())
				{
					whereClause.Append(whereStep).AppendFormat(" E.JobTitle IN ({0})", employerCharacterticsInfo.JobTitles.AsCommaListForSql());
					whereStep = " AND";
				}
				if (employerCharacterticsInfo.NoOfEmployees.IsNotNull() && employerCharacterticsInfo.NoOfEmployees != 0)
				{
					whereClause.Append(whereStep).AppendFormat(" E.NoOfEmployees IN ({0})", employerCharacterticsInfo.NoOfEmployees);
					whereStep = " AND";
				}
			}

			var complianceCriteria = criteria.JobOrderComplianceInfo;
			if (complianceCriteria.IsNotNull())
			{
				if (complianceCriteria.ForeignLabourTypes.IsNotNullOrEmpty())
				{
					whereClause.Append(whereStep).AppendFormat(" E.ForeignLabourType IN ({0})", complianceCriteria.ForeignLabourTypes.AsCommaListForSql());
					whereStep = " AND";
				}

				if (complianceCriteria.NonForeignLabourCertification.HasValue)
				{
					whereClause.Append(whereStep).AppendFormat(" E.ForeignLabourCertification = {0}", (!complianceCriteria.NonForeignLabourCertification.Value).AsOneOrZero());
					whereStep = " AND";
				}

				if (complianceCriteria.FederalContractor.HasValue)
				{
					whereClause.Append(whereStep).AppendFormat(" E.FederalContractor = {0}", complianceCriteria.FederalContractor.Value.AsOneOrZero());
					whereStep = " AND";
				}

				if (complianceCriteria.CourtOrderedAffirmativeAction.HasValue)
				{
					whereClause.Append(whereStep).AppendFormat(" E.CourtOrderedAffirmativeAction = {0}", complianceCriteria.CourtOrderedAffirmativeAction.Value.AsOneOrZero());
					whereStep = " AND";
				}
			}

			var locationInfo = criteria.LocationInfo;
			if (locationInfo.IsNotNull())
			{
				if (locationInfo.Offices.IsNotNullOrEmpty())
				{
					var offices = locationInfo.Offices.ToArray();

					whereClause.Append(whereStep)
											.AppendFormat(" EXISTS(SELECT TOP 1 1 FROM [Report.EmployerOffice] EO WHERE EO.EmployerId = E.Id AND EO.OfficeName IN ({0}))", offices.AsCommaListForSql());
					whereStep = " AND";
				}

				if (locationInfo.Counties.IsNotNullOrEmpty())
				{
					whereClause.Append(whereStep).AppendFormat(" E.County IN ({0})", locationInfo.Counties.AsCommaListForSql());
					whereStep = " AND";
				}

				if (locationInfo.PostalCode.IsNotNullOrEmpty() && locationInfo.DistanceFromPostalCode.IsNotNull())
				{
					var postalCodePoint = Repositories.Library.Query<PostalCode>().FirstOrDefault(postalCode => postalCode.Code == locationInfo.PostalCode);
					if (postalCodePoint.IsNotNull())
					{
						// See www.movable-type.co.uk/scripts/latlong.html for explanation of calculation (Spherical law of cosiness)

						var latitude = Math.PI * postalCodePoint.Latitude / 180.0;
						var longitude = Math.PI * postalCodePoint.Longitude / 180.0;
						var distanceInKm = (double)locationInfo.DistanceFromPostalCode / 0.62137;
						/*
						query =
							query.Where(
								employer => !string.IsNullOrEmpty(employer.PostalCode) &&
								Math.Acos(Math.Sin(latitude) * Math.Sin(employer.LatitudeRadians) +
													Math.Cos(latitude) * Math.Cos(employer.LatitudeRadians) *
													Math.Cos(employer.LongitudeRadians - longitude)) * 6371 < distanceInKm);
						*/
						whereClause.Append(whereStep).AppendFormat(@" E.PostalCode IS NOT NULL 
              AND ACOS(ROUND({0} * SIN(E.LatitudeRadians) +
                       {1} * COS(E.LatitudeRadians) *
                       COS(E.LongitudeRadians - {2}), 9)) * 6371 < {3}", Math.Round(Math.Sin(latitude), 10), Math.Round(Math.Cos(latitude), 10), Math.Round(longitude, 10), Math.Round(distanceInKm, 10));
						whereStep = " AND";
					}
				}
			}

			var keywordInfo = criteria.KeywordInfo;
			if (keywordInfo.IsNotNull())
			{
				if (keywordInfo.KeyWordSearch.IsNotNullOrEmpty() && keywordInfo.KeyWords.IsNotNullOrEmpty() && keywordInfo.SearchType.HasValue)
				{
					var checkAll = keywordInfo.KeyWordSearch.Contains(KeyWord.FullJob);

					var checkTitle = checkAll || keywordInfo.KeyWordSearch.Contains(KeyWord.JobTitle);
					var checkDescription = checkAll || keywordInfo.KeyWordSearch.Contains(KeyWord.JobDescription);
					var checkEmployer = checkAll || keywordInfo.KeyWordSearch.Contains(KeyWord.EmployerName);

					whereClause.Append(whereStep).Append("(");

					var innerWhereStep = "";
					keywordInfo.KeyWords.ForEach(word =>
					{
						whereClause.Append(innerWhereStep).Append(" (");

						var innerClause = new List<string>();

						if (checkTitle)
							innerClause.Add(string.Format("E.JobTitle LIKE '%{0}%'", word.EscapeSql()));

						if (checkDescription)
							innerClause.Add(string.Format("E.JobDescription LIKE '%{0}%'", word.EscapeSql()));

						if (checkEmployer)
							innerClause.Add(string.Format("E.[Name] LIKE '%{0}%'", word.EscapeSql()));

						whereClause.Append(string.Join(" OR ", innerClause));
						whereClause.Append(")");

						innerWhereStep = (keywordInfo.SearchType == KeywordSearchType.All) ? " AND" : " OR";
					});

					whereClause.Append(")");
				}
			}

			var groupInfo = criteria.ChartGroupInfo;
			var isGrouped = groupInfo.IsNotNull();

			// Ordering on 'text' fields can be done in SQL
			var orderBy = ReportEmployerOrderBy.Name;
			var sqlOrderDirection = "ASC";
			var orderingByAction = false;

			var orderInfo = criteria.OrderInfo;
			if (orderInfo.IsNotNull())
			{
				orderBy = orderInfo.OrderBy;
				sqlOrderDirection = orderInfo.Direction == ReportOrder.Ascending ? "ASC" : "DESC";
			}

			if (groupInfo.IsNotNull())
			{
				switch (groupInfo.GroupBy)
				{
					case ReportEmployerGroupBy.State:
						orderBy = ReportEmployerOrderBy.State;
						chartSelectField = "State";
						break;
					case ReportEmployerGroupBy.Office:
						orderBy = ReportEmployerOrderBy.Office;
						chartSelectField = "Office";
						break;
				}
				sqlOrderDirection = "ASC";
			}

			switch (orderBy)
			{
				case ReportEmployerOrderBy.Name:
				case ReportEmployerOrderBy.County:
				case ReportEmployerOrderBy.State:
				case ReportEmployerOrderBy.Office:
					orderClause.AppendFormat("E.{0} {1}", orderBy, sqlOrderDirection);
					groupByClause.AppendFormat(", E.{0}", orderBy);
					break;

				case ReportEmployerOrderBy.JobOrdersEdited:
				case ReportEmployerOrderBy.JobSeekersInterviewed:
				case ReportEmployerOrderBy.JobSeekersHired:
				case ReportEmployerOrderBy.JobOrdersCreated:
				case ReportEmployerOrderBy.JobOrdersPosted:
				case ReportEmployerOrderBy.JobOrdersPutOnHold:
				case ReportEmployerOrderBy.JobOrdersRefreshed:
				case ReportEmployerOrderBy.JobOrdersClosed:
				case ReportEmployerOrderBy.InvitationsSent:
				case ReportEmployerOrderBy.JobSeekersNotHired:
				case ReportEmployerOrderBy.SelfReferrals:
				case ReportEmployerOrderBy.StaffReferrals:
					orderingByAction = true;
					orderClause.AppendFormat("SUM(EA.{0}) {1}", orderBy, sqlOrderDirection);
					break;
			}

			var actionToDate = DateTime.Now.Date;
			var actionFromDate = new DateTime(1900, 01, 01);

			var dateInfo = criteria.DateRangeInfo;
			if (dateInfo.IsNotNull())
			{
				if (dateInfo.FromDate.HasValue || dateInfo.Days.HasValue)
					actionFromDate = dateInfo.FromDate.HasValue ? dateInfo.FromDate.Value : actionToDate.AddDays(0 - dateInfo.Days.Value);

				if (dateInfo.ToDate.HasValue)
					actionToDate = dateInfo.ToDate.Value;
			}

			var startRow = criteria.PageIndex * criteria.PageSize + 1;
			var endRow = startRow + criteria.PageSize - 1;

			var actionTotalFields = GetEmployersRequiredActions(criteria.EmployersEmployerActivityInfo);

			if (criteria.ZeroesHandling != ReportZeroesHandling.GetAll)
			{
				havingClause.Append(havingStep).Append("(");
				havingStep = string.Empty;

				actionTotalFields.ForEach(field =>
				{
					havingClause.Append(havingStep).AppendFormat(" SUM(EA.{0}) > 0", field);
					havingStep = criteria.ZeroesHandling == ReportZeroesHandling.ExcludeAllZeroes ? " OR" : " AND";
				});

				havingClause.Append(")");
			}

			var withSql = new StringBuilder();

			withSql.Append(@"
WITH FilteredEmployers AS 
(
  SELECT
    E.[Id]");

			if (!isGrouped)
				withSql.Append(", ").AppendLine().AppendFormat(@"ROW_NUMBER() OVER ({0}) AS RowNumber", orderClause);

			withSql.AppendLine().Append(@"FROM [Report.EmployerView] E");

			var includeHavingClause = false;
			if (actionTotalFields.Any() && (orderingByAction || criteria.ZeroesHandling != ReportZeroesHandling.GetAll))
			{
				withSql.AppendFormat(@"
  LEFT JOIN [Report.EmployerAction] EA
	  ON EA.EmployerId = E.Id
    AND EA.ActionDate BETWEEN '{0}' AND '{1}'",
					actionFromDate.ToString("dd MMMM yyyy"),
					actionToDate.ToString("dd MMMM yyyy"));
				includeHavingClause = true;
			}

			withSql.AppendLine().Append(whereClause);
			withSql.AppendLine().Append(groupByClause);
			if (includeHavingClause)
				withSql.AppendLine().Append(havingClause);

			withSql.AppendLine().Append(")");

			countSql.Append(withSql);
			countSql.Replace(string.Format("ROW_NUMBER() OVER ({0})", orderClause), "NULL");
			countSql.AppendLine().Append("SELECT COUNT(1) FROM FilteredEmployers");

			var reportSql = new StringBuilder();
			reportSql.Append(withSql);

			var selectDivider = "";

			if (!isGrouped)
			{
				reportSql.AppendLine().Append("SELECT 0 AS DummyTotal,");
				reportSql.AppendLine().Append(forScreen ? selectClauseFull : selectClauseForExport);

				selectDivider = ",";
			}
			else if (chartSelectField.IsNotNullOrEmpty())
			{
				reportSql.AppendLine().AppendFormat("SELECT CAST(E.[{0}] AS VARCHAR(400)) AS [{0}]", chartSelectField);

				selectDivider = ",";
			}
			else
			{
				reportSql.AppendLine().Append("SELECT ");
			}

			if (actionTotalFields.Any())
			{
				reportSql.Append(selectDivider);
				reportSql.AppendLine().Append(string.Join(",\r\n", actionTotalFields.Select(field => string.Format("ISNULL(SUM(EA.[{0}]), 0) AS {0}", field))));

				selectDivider = ",";
			}

			if (isGrouped && !actionTotalFields.Any())
			{
				reportSql.Append(selectDivider);
				reportSql.AppendLine().Append("COUNT(1) AS TotalEmployers");
			}

			reportSql.AppendLine().Append(@"
FROM FilteredEmployers FE
INNER JOIN [Report.Employer] E
ON E.Id = FE.Id");

			if (actionTotalFields.Any())
			{
				reportSql.AppendLine().AppendFormat(@"
LEFT JOIN [Report.EmployerAction] EA
ON EA.EmployerId = E.Id
AND EA.ActionDate BETWEEN '{0}' AND '{1}'",
				actionFromDate.ToString("dd MMMM yyyy"),
				actionToDate.ToString("dd MMMM yyyy"));
			}

			if (!isGrouped)
			{
				reportSql.AppendLine().AppendFormat("WHERE FE.RowNumber BETWEEN {0} AND {1}", startRow, endRow);
				reportSql.AppendLine().Append("GROUP BY");
				reportSql.AppendLine().Append(selectClauseFull);
				reportSql.Append(",");
				reportSql.AppendLine().Append("FE.RowNumber");
				reportSql.AppendLine().Append("ORDER BY FE.RowNumber");
			}
			else
			{
				if (chartSelectField.IsNotNullOrEmpty())
				{
					reportSql.AppendLine().AppendFormat("GROUP BY E.[{0}]", chartSelectField);
					reportSql.AppendLine().AppendFormat("ORDER BY E.[{0}]", chartSelectField);
				}
			}

			return reportSql;
		}


		/// <summary>
		/// Appends the specified account types to the where clause for the report SQL
		/// </summary>
		/// <param name="employerCharacterticsInfo">The employer charactertics information.</param>
		/// <param name="whereClause">The where clause.</param>
		/// <param name="whereStep">The where step (either AND or OR)</param>
		/// <param name="tablePrefix">The report table prefix.</param>
		private void AppendSpecifiedAccountTypesToWhereClause(EmployerCharacteristicsReportCriteria employerCharacterticsInfo, StringBuilder whereClause, string whereStep, string tablePrefix = "E")
		{
			var accountTypes = GetAccountTypeCodeItems(employerCharacterticsInfo);
			whereClause.Append(whereStep).AppendFormat(" {0}.AccountTypeId IN ({1})", tablePrefix, accountTypes.AsCommaListForSql());
		}

		private List<long> GetAccountTypeCodeItems(EmployerCharacteristicsReportCriteria employerCharacterticsInfo)
		{
			var accountTypes = new List<long>();
			foreach (var item in employerCharacterticsInfo.AccountTypesList)
			{
				var accountCodeItem = Helpers.Lookup.GetLookup(LookupTypes.AccountTypes).FirstOrDefault(x => x.Key.ToLower().EndsWith(item.ToString().ToLower()));
				if (accountCodeItem != null)
					accountTypes.Add(accountCodeItem.Id);
			}
			return accountTypes;
		}

		/// <summary>
		/// Gets the list of actions to total on the employers report
		/// </summary>
		/// <param name="activityCriteria">The activity criteria.</param>
		/// <returns>The list to populate.</returns>
		private static List<string> GetEmployersRequiredActions(EmployersActivityCriteria activityCriteria)
		{
			var actions = new List<string>();

			if (activityCriteria.IsNotNull())
			{
				if (activityCriteria.JobOrdersEdited)
					actions.Add("JobOrdersEdited");

				if (activityCriteria.JobSeekersInterviewed)
					actions.Add("JobSeekersInterviewed");

				if (activityCriteria.JobSeekersHired)
					actions.Add("JobSeekersHired");

				if (activityCriteria.JobOrdersCreated)
					actions.Add("JobOrdersCreated");

				if (activityCriteria.JobOrdersPosted)
					actions.Add("JobOrdersPosted");

				if (activityCriteria.JobOrdersPutOnHold)
					actions.Add("JobOrdersPutOnHold");

				if (activityCriteria.JobOrdersRefreshed)
					actions.Add("JobOrdersRefreshed");

				if (activityCriteria.JobOrdersClosed)
					actions.Add("JobOrdersClosed");

				if (activityCriteria.InvitationsSent)
					actions.Add("InvitationsSent");

				if (activityCriteria.JobSeekersNotHired)
					actions.Add("JobSeekersNotHired");

				if (activityCriteria.SelfReferrals)
					actions.Add("SelfReferrals");

				if (activityCriteria.StaffReferrals)
					actions.Add("StaffReferrals");
			}

			return actions;
		}

		/// <summary>
		/// Gets the ordinals for looking up job order fields from a data reader
		/// </summary>
		/// <param name="reader">The reader.</param>
		/// <returns></returns>
		private static Dictionary<string, int> GetEmployerFieldsOrdinalLookup(IDataReader reader)
		{
			var allActionTotalFields = new List<string>
      {
        "JobOrdersEdited",
        "JobSeekersInterviewed",
        "JobSeekersHired",
        "JobOrdersCreated",
        "JobOrdersPosted",
        "JobOrdersPutOnHold",
        "JobOrdersRefreshed",
        "JobOrdersClosed",
        "InvitationsSent",
        "JobSeekersNotHired",
        "SelfReferrals",
        "StaffReferrals"
      };

			var ordinalLookup = new Dictionary<string, int>();
			for (var ordinal = 0; ordinal < reader.FieldCount; ordinal++)
			{
				ordinalLookup.Add(reader.GetName(ordinal), ordinal);
			}

			allActionTotalFields.Where(field => !ordinalLookup.ContainsKey(field))
													.ToList()
													.ForEach(field => ordinalLookup.Add(field, reader.GetOrdinal("DummyTotal")));

			return ordinalLookup;
		}

		/// <summary>
		/// Runs a SQL to return a data table used in either charts or export
		/// </summary>
		/// <param name="reportSql">The report SQL.</param>
		/// <param name="reportName">Name of the report.</param>
		/// <param name="criteria">The criteria.</param>
		/// <returns>The data table</returns>
		private ReportDataTableModel GetEmployersDataTable(StringBuilder reportSql, string reportName, EmployersReportCriteria criteria)
		{
			var reportData = new ReportDataTableModel
			{
				FirstDataColumnIndex = 1,
				Data = new DataTable(reportName),
				DisplayType = criteria.DisplayType,
				IsGrouped = true
			};

			if (criteria.ChartGroupInfo.IsNull() || criteria.ChartGroupInfo.GroupBy.IsNull())
			{
				reportData.FirstDataColumnIndex = 5;
				reportData.IsGrouped = false;
			}

			using (var reader = Repositories.Report.ExecuteDataReader(reportSql.ToString()))
			{
				reportData.Data.Load(reader);
				reader.Close();
			}

			var columns = reportData.Data.Columns.Cast<DataColumn>().ToList();
			columns.ForEach(column => column.ReadOnly = false);

			var columnNames = columns.Select(x => x.ColumnName).ToList();

			// Split office into comma delimited list
			if (columnNames.Contains("Office"))
			{
				var columnIndex = reportData.Data.Columns.IndexOf("Office");

				foreach (DataRow row in reportData.Data.Rows)
				{
					if (row[columnIndex].IsDBNull() || row[columnIndex].IsNull())
					{
						row[columnIndex] = null;
					}
					else
					{
						row[columnIndex] = ((string)row[columnIndex]).Replace("||", ", ");
					}
				}
			}

			var dummyIndex = -1;
			if (columnNames.Contains("DummyTotal"))
				dummyIndex = reportData.Data.Columns.IndexOf("DummyTotal");

			// Update column headings to proper text
			columns.ForEach(column =>
			{
				var localisationKey = string.Concat("Global.Employers.Report.", column.ColumnName);
				var defaultText = column.ColumnName.SplitByPascalCase();
				column.ColumnName = Helpers.Localisation.Localise(localisationKey, defaultText);
			});

			// Remove the 'dummy total' which was purely used for setting action totals to zero when not required
			if (dummyIndex >= 0)
				reportData.Data.Columns.RemoveAt(dummyIndex);

			return reportData;
		}


		/// <summary>
		/// Gets the data table from objects.
		/// </summary>
		/// <param name="supplyDemand">The supply demand.</param>
		/// <returns></returns>
		public static DataTable GetDataTableFromObjects(SupplyDemandReportView[] supplyDemand)
		{
			if (supplyDemand != null && supplyDemand.Length > 0)
			{
				var t = supplyDemand[0].GetType();
				var dt = new DataTable(t.Name);
				foreach (var pi in t.GetProperties())
				{
					dt.Columns.Add(new DataColumn(pi.Name));
				}
				foreach (var o in supplyDemand)
				{
					DataRow dr = dt.NewRow();
					foreach (DataColumn dc in dt.Columns)
					{
						dr[dc.ColumnName] = o.GetType().GetProperty(dc.ColumnName).GetValue(o, null);
					}
					dt.Rows.Add(dr);
				}
				return dt;
			}
			return null;
		}

		/// <summary>
		/// Gets the supply demand data table.
		/// </summary>
		/// <param name="reportName">Name of the report.</param>
		/// <param name="criteria">The criteria.</param>
		/// <param name="showFullInfo">Whether this is for export to Excel or PDF.</param>
		/// <returns>
		/// The data table
		/// </returns>
		private ReportDataTableModel GetSupplyDemandDataTable(string reportName, SupplyDemandReportCriteria criteria, bool isForExport = false)
		{
			var reportData = new ReportDataTableModel
			{
				FirstDataColumnIndex = 1,
				Data = new DataTable(reportName),
				DisplayType = criteria.DisplayType,
				IsGrouped = true
			};

			if (criteria.ChartGroupInfo.IsNull() || criteria.ChartGroupInfo.GroupBy.IsNull())
			{
				reportData.FirstDataColumnIndex = 5;
				reportData.IsGrouped = false;
			}

			var supplyDemand = GetSupplyDemandReport(criteria);

			var dataTable = GetDataTableFromObjects(supplyDemand.ToArray());

			reportData.Data = dataTable;

			reportData.Data.Columns.RemoveAt(reportData.Data.Columns.IndexOf("Id"));

			if (!isForExport)
			{
				reportData.Data.Columns.RemoveAt(reportData.Data.Columns.IndexOf("OnetCode"));
				reportData.Data.Columns.RemoveAt(reportData.Data.Columns.IndexOf("Gap"));

				var columns = reportData.Data.Columns.Cast<DataColumn>().ToList();

				// Update column headings to proper text
				columns.ForEach(column =>
				{
					var localisationKey = string.Concat("Global.SupplyDemandReport.Report.", column.ColumnName);
					var defaultText = column.ColumnName.SplitByPascalCase();
					column.ColumnName = Helpers.Localisation.Localise(localisationKey, defaultText);
				});
			}
			else
			{
				dataTable.Columns["OnetCode"].SetOrdinal(0);
				dataTable.Columns[0].ColumnName = "Onet";
				dataTable.Columns[1].ColumnName = "Group By Detail";
				dataTable.Columns[2].ColumnName = "Supply Count";
				dataTable.Columns[3].ColumnName = "Demand Count";
			}

			return reportData;
		}

		/*
		/// <summary>
		/// Gets a list of employers from the database matching the criteria
		/// </summary>
		/// <param name="criteria">The criteria</param>
		/// <returns>A list of matching employers</returns>
		private List<EmployersReportView> GetEmployers(EmployersReportCriteria criteria)
		{
			var query = Repositories.Report.EmployerViews;

			var salaryInfo = criteria.SalaryInfo;
			if (salaryInfo.IsNotNull())
			{
				if (salaryInfo.MinimumSalary.HasValue)
					query = query.Where(jobOrder => jobOrder.MinSalary >= salaryInfo.MinimumSalary);

				if (salaryInfo.MaximumSalary.HasValue)
					query = query.Where(jobOrder => jobOrder.MaxSalary <= salaryInfo.MaximumSalary);

				if (salaryInfo.SalaryFrequency.HasValue)
					query = query.Where(jobOrder => jobOrder.SalaryFrequency == salaryInfo.SalaryFrequency);
			}

			var jobOrderCharacteristicInfo = criteria.JobOrderCharacteristicsInfo;
			if (jobOrderCharacteristicInfo.IsNotNull())
			{
				if (jobOrderCharacteristicInfo.JobStatuses.IsNotNullOrEmpty())
					query = query.Where(jobOrder => jobOrderCharacteristicInfo.JobStatuses.Contains(jobOrder.JobStatus));

				if (jobOrderCharacteristicInfo.EducationLevels.IsNotNullOrEmpty())
					query = query.Where(jobOrder => jobOrderCharacteristicInfo.EducationLevels.Contains(jobOrder.MinimumEducationLevel));
			}

			var employerCharacterticsInfo = criteria.EmployerCharacteristicsInfo;
			if (employerCharacterticsInfo.IsNotNull())
			{
				if (employerCharacterticsInfo.WorkOpportunitiesTaxCreditInterests.IsNotNullOrEmpty())
				{
					var wotcBitMask = (WorkOpportunitiesTaxCreditCategories?)0;
					employerCharacterticsInfo.WorkOpportunitiesTaxCreditInterests.ForEach(wotc => wotcBitMask = wotcBitMask | wotc);
					query = query.Where(jobOrder => (jobOrder.WorkOpportunitiesTaxCreditHires & wotcBitMask) != 0);
				}

				if (employerCharacterticsInfo.FEINs.IsNotNullOrEmpty())
					query = query.Where(employer => employerCharacterticsInfo.FEINs.Contains(employer.FederalEmployerIdentificationNumber));

				if (employerCharacterticsInfo.EmployerNames.IsNotNullOrEmpty())
					query = query.Where(employer => employerCharacterticsInfo.EmployerNames.Contains(employer.Name));

				if (employerCharacterticsInfo.JobTitles.IsNotNullOrEmpty())
					query = query.Where(employer => employerCharacterticsInfo.JobTitles.Contains(employer.JobTitle));
			}

			var complianceCriteria = criteria.JobOrderComplianceInfo;
			if (complianceCriteria.IsNotNull())
			{
				if (complianceCriteria.ForeignLabourTypes.IsNotNullOrEmpty())
					query = query.Where(employer => complianceCriteria.ForeignLabourTypes.Contains(employer.ForeignLabourType));

				if (complianceCriteria.NonForeignLabourCertification.HasValue)
					query = query.Where(employer => employer.ForeignLabourCertification == !(complianceCriteria.NonForeignLabourCertification));

				if (complianceCriteria.FederalContractor.HasValue)
					query = query.Where(employer => employer.FederalContractor == complianceCriteria.FederalContractor);

				if (complianceCriteria.CourtOrderedAffirmativeAction.HasValue)
					query = query.Where(employer => employer.CourtOrderedAffirmativeAction == complianceCriteria.CourtOrderedAffirmativeAction);
			}

			var locationInfo = criteria.LocationInfo;
			if (locationInfo.IsNotNull())
			{
				if (locationInfo.Offices.IsNotNullOrEmpty())
				{
					var offices = locationInfo.Offices.ToArray();
					query = query.Where(employer => employer.EmployerOffices.Any(office => offices.Contains(office.OfficeName)));
				}

				if (locationInfo.Counties.IsNotNullOrEmpty())
					query = query.Where(employer => locationInfo.Counties.Contains(employer.County));

				if (locationInfo.PostalCode.IsNotNullOrEmpty() && locationInfo.DistanceFromPostalCode.IsNotNull())
				{
					var postalCodePoint = Repositories.Library.Query<PostalCode>().FirstOrDefault(postalCode => postalCode.Code == locationInfo.PostalCode);
					if (postalCodePoint.IsNotNull())
					{
						// See www.movable-type.co.uk/scripts/latlong.html for explanation of calculation (Spherical law of cosiness)

						var latitude = Math.PI * postalCodePoint.Latitude / 180.0;
						var longitude = Math.PI * postalCodePoint.Longitude / 180.0;
						var distanceInKm = (double)locationInfo.DistanceFromPostalCode / 0.62137;

						query =
							query.Where(
								employer => !string.IsNullOrEmpty(employer.PostalCode) &&
								Math.Acos(Math.Sin(latitude) * Math.Sin(employer.LatitudeRadians) +
													Math.Cos(latitude) * Math.Cos(employer.LatitudeRadians) *
													Math.Cos(employer.LongitudeRadians - longitude)) * 6371 < distanceInKm);
					}
				}
			}

			List<long> filteredEmployerIds = null;
			var filterByKeyword = false;

			var keywordInfo = criteria.KeywordInfo;
			if (keywordInfo.IsNotNull())
			{
				if (keywordInfo.KeyWordSearch.IsNotNullOrEmpty() && keywordInfo.KeyWords.IsNotNullOrEmpty() && keywordInfo.SearchType.HasValue)
				{
					var checkAll = keywordInfo.KeyWordSearch.Contains(KeyWord.FullJob);

					var checkName = checkAll || keywordInfo.KeyWordSearch.Contains(KeyWord.EmployerName);
					var checkJobTitle = checkAll || keywordInfo.KeyWordSearch.Contains(KeyWord.JobTitle);
					var checkJobDescription = checkAll || keywordInfo.KeyWordSearch.Contains(KeyWord.JobDescription);

					filteredEmployerIds = Repositories.Report.EmployerKeywordFilter(keywordInfo.KeyWords, checkName, checkJobTitle, checkJobDescription, keywordInfo.SearchType == KeywordSearchType.All)
																									 .Select(filter => filter.Id)
																									 .ToList();

					filterByKeyword = true;
				}
			}

			// If less that 100 job orders match the keyword filter, the ids can be added to the main filter
			if (filterByKeyword && filteredEmployerIds.Count <= MaxIdsToFilterInProc)
			{
				query = query.Where(employer => filteredEmployerIds.Contains(employer.Id));
				filterByKeyword = false;
			}

			// Ordering on 'text' fields can be done in SQL
			ReportEmployerOrderBy? orderBy = null;
			ReportOrder? orderDirection = null;

			var orderInfo = criteria.OrderInfo;
			if (orderInfo.IsNotNull())
			{
				orderBy = orderInfo.OrderBy;
				orderDirection = orderInfo.Direction;
			}

			var groupInfo = criteria.ChartGroupInfo;
			if (groupInfo.IsNotNull())
			{
				switch (groupInfo.GroupBy)
				{
					case ReportEmployerGroupBy.State:
						orderBy = ReportEmployerOrderBy.State;
						break;
					case ReportEmployerGroupBy.Office:
						orderBy = ReportEmployerOrderBy.Office;
						break;
				}
				orderDirection = ReportOrder.Ascending;
			}

			switch (orderBy)
			{
				case ReportEmployerOrderBy.Name:
					query = orderDirection == ReportOrder.Ascending ? query.OrderBy(employer => employer.Name) : query.OrderByDescending(employer => employer.Name);
					break;

				case ReportEmployerOrderBy.County:
					query = orderDirection == ReportOrder.Ascending ? query.OrderBy(employer => employer.County) : query.OrderByDescending(employer => employer.County);
					break;

				case ReportEmployerOrderBy.State:
					query = orderDirection == ReportOrder.Ascending ? query.OrderBy(employer => employer.State) : query.OrderByDescending(employer => employer.State);
					break;

				case ReportEmployerOrderBy.Office:
					query = orderDirection == ReportOrder.Ascending ? query.OrderBy(employer => employer.Office) : query.OrderByDescending(employer => employer.Office);
					break;
			}

			var employers = query.Select(employer => new EmployersReportView
																									 {
																										 Id = employer.Id,
																										 Name = employer.Name,
																										 //StateId = employer.StateId,
																										 State = employer.State,
																										 Office = employer.Office,
																										 FocusBusinessUnitId = employer.FocusBusinessUnitId,
																										 FocusEmployerId = employer.FocusEmployerId
																									 }).Distinct().ToList();

			// If there were more than 100 filtered employer ids, filter the results now
			if (filterByKeyword)
				employers = employers.Where(employer => filteredEmployerIds.Contains(employer.Id)).ToList();
      
			Dictionary<long, EmployerActionDto> employerTotalsLookup;

			if (employers.Any())
			{
				var toDate = DateTime.Now.Date;
				var fromDate = new DateTime(1900, 01, 01);

				var dateRangeInfo = criteria.DateRangeInfo;
				if (dateRangeInfo.IsNotNull())
				{
					if (dateRangeInfo.FromDate.HasValue || dateRangeInfo.Days.HasValue)
						fromDate = dateRangeInfo.FromDate.HasValue ? dateRangeInfo.FromDate.Value : toDate.AddDays(0 - dateRangeInfo.Days.Value);

					if (dateRangeInfo.ToDate.HasValue)
						toDate = dateRangeInfo.ToDate.Value;
				}

				var employerTotals = employers.Count <= MaxIdsToFilterInProc
					? Repositories.Report.EmployerActionTotals(fromDate, toDate, employers.Select(employer => employer.Id).ToList())
					: Repositories.Report.EmployerActionTotals(fromDate, toDate);

				employerTotalsLookup = employerTotals.ToDictionary(total => total.Id, total => total.AsDto());
			}
			else
			{
				employerTotalsLookup = new Dictionary<long, EmployerActionDto>();
			}

			employers.ForEach(order =>
			{
				order.Totals = employerTotalsLookup.ContainsKey(order.Id)
													? employerTotalsLookup[order.Id]
													: new EmployerActionDto();
			});

			if (criteria.ZeroesHandling != ReportZeroesHandling.GetAll)
			{
				var employerCriteria = criteria.EmployersEmployerActivityInfo;

				employers = employers.Where(employer => !ExcludeEmployerRow(employer.Totals, criteria.ZeroesHandling, employerCriteria))
															 .ToList();
			}

			// When ordering by data total, this is done directly on the returned data set, not in SQL
			if (orderInfo.IsNotNull())
			{
				switch (orderInfo.OrderBy)
				{
					case ReportEmployerOrderBy.JobOrdersEdited:
						employers = orderInfo.Direction == ReportOrder.Ascending ? employers.OrderBy(employer => employer.Totals.JobOrdersEdited).ToList() : employers.OrderByDescending(employer => employer.Totals.JobOrdersEdited).ToList();
						break;
					case ReportEmployerOrderBy.JobSeekersInterviewed:
						employers = orderInfo.Direction == ReportOrder.Ascending ? employers.OrderBy(employer => employer.Totals.JobSeekersInterviewed).ToList() : employers.OrderByDescending(employer => employer.Totals.JobSeekersInterviewed).ToList();
						break;
					case ReportEmployerOrderBy.JobSeekersHired:
						employers = orderInfo.Direction == ReportOrder.Ascending ? employers.OrderBy(employer => employer.Totals.JobSeekersHired).ToList() : employers.OrderByDescending(employer => employer.Totals.JobSeekersHired).ToList();
						break;
					case ReportEmployerOrderBy.JobOrdersCreated:
						employers = orderInfo.Direction == ReportOrder.Ascending ? employers.OrderBy(employer => employer.Totals.JobOrdersCreated).ToList() : employers.OrderByDescending(employer => employer.Totals.JobOrdersCreated).ToList();
						break;
					case ReportEmployerOrderBy.JobOrdersPosted:
						employers = orderInfo.Direction == ReportOrder.Ascending ? employers.OrderBy(employer => employer.Totals.JobOrdersPosted).ToList() : employers.OrderByDescending(employer => employer.Totals.JobOrdersPosted).ToList();
						break;
					case ReportEmployerOrderBy.JobOrdersPutOnHold:
						employers = orderInfo.Direction == ReportOrder.Ascending ? employers.OrderBy(employer => employer.Totals.JobOrdersPutOnHold).ToList() : employers.OrderByDescending(employer => employer.Totals.JobOrdersPutOnHold).ToList();
						break;
					case ReportEmployerOrderBy.JobOrdersRefreshed:
						employers = orderInfo.Direction == ReportOrder.Ascending ? employers.OrderBy(employer => employer.Totals.JobOrdersRefreshed).ToList() : employers.OrderByDescending(employer => employer.Totals.JobOrdersRefreshed).ToList();
						break;
					case ReportEmployerOrderBy.JobOrdersClosed:
						employers = orderInfo.Direction == ReportOrder.Ascending ? employers.OrderBy(employer => employer.Totals.JobOrdersClosed).ToList() : employers.OrderByDescending(employer => employer.Totals.JobOrdersClosed).ToList();
						break;
					case ReportEmployerOrderBy.InvitationsSent:
						employers = orderInfo.Direction == ReportOrder.Ascending ? employers.OrderBy(employer => employer.Totals.InvitationsSent).ToList() : employers.OrderByDescending(employer => employer.Totals.InvitationsSent).ToList();
						break;
					case ReportEmployerOrderBy.JobSeekersNotHired:
						employers = orderInfo.Direction == ReportOrder.Ascending ? employers.OrderBy(employer => employer.Totals.JobSeekersNotHired).ToList() : employers.OrderByDescending(employer => employer.Totals.JobSeekersNotHired).ToList();
						break;
					case ReportEmployerOrderBy.SelfReferrals:
						employers = orderInfo.Direction == ReportOrder.Ascending ? employers.OrderBy(employer => employer.Totals.SelfReferrals).ToList() : employers.OrderByDescending(employer => employer.Totals.SelfReferrals).ToList();
						break;
					case ReportEmployerOrderBy.StaffReferrals:
						employers = orderInfo.Direction == ReportOrder.Ascending ? employers.OrderBy(employer => employer.Totals.StaffReferrals).ToList() : employers.OrderByDescending(employer => employer.Totals.StaffReferrals).ToList();
						break;
				}
			}

			return employers;
		}
		*/

		/*
		/// <summary>
		/// Returns whether an employer row needs to be excluded due to zero totals
		/// </summary>
		/// <param name="totals">The employer totals.</param>
		/// <param name="zeroesHandling">How to handle zeroes found.</param>
		/// <param name="employerCriteria">The activity criteria.</param>
		/// <returns></returns>
		private bool ExcludeEmployerRow(EmployerActionDto totals, ReportZeroesHandling zeroesHandling, EmployersActivityCriteria employerCriteria)
		{
			var fields = new List<int>();
			if (employerCriteria.IsNotNull())
			{
				if (employerCriteria.JobOrdersEdited)
					fields.Add(totals.JobOrdersEdited);

				if (employerCriteria.JobSeekersInterviewed)
					fields.Add(totals.JobSeekersInterviewed);

				if (employerCriteria.JobSeekersHired)
					fields.Add(totals.JobSeekersHired);

				if (employerCriteria.JobOrdersCreated)
					fields.Add(totals.JobOrdersCreated);

				if (employerCriteria.JobOrdersPosted)
					fields.Add(totals.JobOrdersPosted);

				if (employerCriteria.JobOrdersPutOnHold)
					fields.Add(totals.JobOrdersPutOnHold);

				if (employerCriteria.JobOrdersRefreshed)
					fields.Add(totals.JobOrdersRefreshed);

				if (employerCriteria.JobOrdersClosed)
					fields.Add(totals.JobOrdersClosed);

				if (employerCriteria.InvitationsSent)
					fields.Add(totals.InvitationsSent);

				if (employerCriteria.JobSeekersNotHired)
					fields.Add(totals.JobSeekersNotHired);

				if (employerCriteria.SelfReferrals)
					fields.Add(totals.SelfReferrals);

				if (employerCriteria.StaffReferrals)
					fields.Add(totals.StaffReferrals);
			}

			if (fields.Count == 0)
				return false;

			if (zeroesHandling == ReportZeroesHandling.ExcludeAllZeroes)
				return !fields.Any(field => field > 0);

			return fields.Any(field => field == 0);
		}
		*/

		/*
		/// <summary>
		/// Converts a list of employers to a datatable
		/// </summary>
		/// <param name="employers">The list of employers</param>
		/// <param name="reportName">The name of the datatable</param>
		/// <param name="criteria">The criteria to determine which columns are shown</param>
		/// <returns>The data table</returns>
		private ReportDataTableModel ConvertEmployersToDataTable(IEnumerable<EmployersReportView> employers, string reportName, EmployersReportCriteria criteria)
		{
			var reportData = new ReportDataTableModel
			{
				FirstDataColumnIndex = 1,
				Data = new DataTable(reportName),
				DisplayType = criteria.DisplayType,
				IsGrouped = true
			};

			var groupInfo = criteria.ChartGroupInfo.IsNull() ? null : criteria.ChartGroupInfo.GroupBy;

			switch (groupInfo)
			{
				case ReportEmployerGroupBy.State:
					AddColumnToDataTable<string>(reportData, "Global.EmployerReport.State", Constants.Reporting.EmployerReportDataColumns.State);
					break;

				case ReportEmployerGroupBy.Office:
					AddColumnToDataTable<string>(reportData, "Global.EmployerReport.Office", Constants.Reporting.EmployerReportDataColumns.Office);
					break;

				default:
					reportData.FirstDataColumnIndex = 3;
					reportData.IsGrouped = false;
					AddColumnToDataTable<string>(reportData, "Global.EmployerReport.Employer", Constants.Reporting.EmployerReportDataColumns.Name);
					AddColumnToDataTable<string>(reportData, "Global.EmployerReport.State", Constants.Reporting.EmployerReportDataColumns.State);
					AddColumnToDataTable<string>(reportData, "Global.EmployerReport.Office", Constants.Reporting.EmployerReportDataColumns.Office);
					break;
			}

			var employerCriteria = criteria.EmployersEmployerActivityInfo;

			if (employerCriteria.IsNotNull())
			{
				if (employerCriteria.JobOrdersClosed)
					AddColumnToDataTable<int>(reportData, "Global.EmployerReport.JobOrdersClosed", Constants.Reporting.EmployerReportDataColumns.JobOrdersClosed);

				if (employerCriteria.JobOrdersCreated)
					AddColumnToDataTable<int>(reportData, "Global.EmployerReport.JobOrdersCreated", Constants.Reporting.EmployerReportDataColumns.JobOrdersCreated);

				if (employerCriteria.JobOrdersEdited)
					AddColumnToDataTable<int>(reportData, "Global.EmployerReport.JobOrdersEdited", Constants.Reporting.EmployerReportDataColumns.JobOrdersEdited);

				if (employerCriteria.InvitationsSent)
					AddColumnToDataTable<int>(reportData, "Global.EmployerReport.InvitationsSent", Constants.Reporting.EmployerReportDataColumns.InvitationsSent);

				if (employerCriteria.JobOrdersPutOnHold)
					AddColumnToDataTable<int>(reportData, "Global.EmployerReport.JobOrdersPutOnHold", Constants.Reporting.EmployerReportDataColumns.JobOrdersPutOnHold);

				if (employerCriteria.JobOrdersPosted)
					AddColumnToDataTable<int>(reportData, "Global.EmployerReport.JobOrdersPosted", Constants.Reporting.EmployerReportDataColumns.JobOrdersPosted);

				if (employerCriteria.JobOrdersRefreshed)
					AddColumnToDataTable<int>(reportData, "Global.EmployerReport.JobOrdersRefreshed", Constants.Reporting.EmployerReportDataColumns.JobOrdersRefreshed);

				if (employerCriteria.SelfReferrals)
					AddColumnToDataTable<int>(reportData, "Global.EmployerReport.SelfReferrals", Constants.Reporting.EmployerReportDataColumns.SelfReferrals);

				if (employerCriteria.StaffReferrals)
					AddColumnToDataTable<int>(reportData, "Global.EmployerReport.StaffReferrals", Constants.Reporting.EmployerReportDataColumns.StaffReferrals);

				if (employerCriteria.JobSeekersInterviewed)
					AddColumnToDataTable<int>(reportData, "Global.EmployerReport.JobSeekersInterviewed", Constants.Reporting.EmployerReportDataColumns.JobSeekersInterviewed);

				if (employerCriteria.JobSeekersHired)
					AddColumnToDataTable<int>(reportData, "Global.EmployerReport.JobSeekersHired", Constants.Reporting.EmployerReportDataColumns.JobSeekersHired);

				if (employerCriteria.JobSeekersNotHired)
					AddColumnToDataTable<int>(reportData, "Global.EmployerReport.JobSeekersNotHired", Constants.Reporting.EmployerReportDataColumns.JobSeekersNotHired);
			}

			var totalColumns = reportData.Data.Columns.Count;

			var showTotalCount = false;
			if (groupInfo.IsNotNull())
			{
				if (totalColumns == reportData.FirstDataColumnIndex || (employerCriteria.IsNotNull() && employerCriteria.TotalCount))
				{
					AddColumnToDataTable<int>(reportData, "Global.EmployerReport.TotalAccounts", Constants.Reporting.EmployerReportDataColumns.TotalAccounts);
					showTotalCount = true;
					totalColumns++;
				}
			}

			var dataTableRows = new Dictionary<string, DataRow>();

			foreach (var employerData in employers)
			{
				DataRow row;
				int colIndex;

				var rowKey = "";
				var rowTitle = "(Unspecified)";

				switch (groupInfo)
				{
					case ReportEmployerGroupBy.State:
						if (employerData.StateId.HasValue)
						{
							rowKey = employerData.StateId.ToString();
							rowTitle = employerData.State;
						}
						break;
					case ReportEmployerGroupBy.Office:
						rowKey = employerData.Office;
						rowTitle = employerData.Office;
						break;

					default:
						rowKey = employerData.Id.ToString(CultureInfo.InvariantCulture);
						rowTitle = employerData.Name;
						break;
				}

				if (dataTableRows.ContainsKey(rowKey))
				{
					row = dataTableRows[rowKey];
				}
				else
				{
					row = reportData.Data.NewRow();
					row[0] = rowTitle;

					if (reportData.FirstDataColumnIndex > 1)
					{
						row[1] = employerData.State;
						row[2] = employerData.Office.Replace("||", ", ");
					}

					for (colIndex = reportData.FirstDataColumnIndex; colIndex < totalColumns; colIndex++)
					{
						row[colIndex] = 0;
					}
					dataTableRows.Add(rowKey, row);
					reportData.Data.Rows.Add(row);
				}

				colIndex = reportData.FirstDataColumnIndex;

				if (employerCriteria.IsNotNull())
				{
					if (employerCriteria.TotalCount)
						UpdateColumnValue(row, ref colIndex, 1);

					if (employerCriteria.JobOrdersClosed)
						UpdateColumnValue(row, ref colIndex, employerData.Totals.JobOrdersClosed);

					if (employerCriteria.JobOrdersCreated)
						UpdateColumnValue(row, ref colIndex, employerData.Totals.JobOrdersCreated);

					if (employerCriteria.JobOrdersEdited)
						UpdateColumnValue(row, ref colIndex, employerData.Totals.JobOrdersEdited);

					if (employerCriteria.InvitationsSent)
						UpdateColumnValue(row, ref colIndex, employerData.Totals.InvitationsSent);

					if (employerCriteria.JobOrdersPutOnHold)
						UpdateColumnValue(row, ref colIndex, employerData.Totals.JobOrdersPutOnHold);

					if (employerCriteria.JobOrdersPosted)
						UpdateColumnValue(row, ref colIndex, employerData.Totals.JobOrdersPosted);

					if (employerCriteria.JobOrdersRefreshed)
						UpdateColumnValue(row, ref colIndex, employerData.Totals.JobOrdersRefreshed);

					if (employerCriteria.SelfReferrals)
						UpdateColumnValue(row, ref colIndex, employerData.Totals.SelfReferrals);

					if (employerCriteria.StaffReferrals)
						UpdateColumnValue(row, ref colIndex, employerData.Totals.StaffReferrals);

					if (employerCriteria.JobSeekersInterviewed)
						UpdateColumnValue(row, ref colIndex, employerData.Totals.JobSeekersInterviewed);

					if (employerCriteria.JobSeekersHired)
						UpdateColumnValue(row, ref colIndex, employerData.Totals.JobSeekersHired);

					if (employerCriteria.JobSeekersNotHired)
						UpdateColumnValue(row, ref colIndex, employerData.Totals.JobSeekersNotHired);
				}

				if (showTotalCount)
					UpdateColumnValue(row, ref colIndex, 1);
			}

			return reportData;
		}
		*/

		#endregion

		#region Supply Demand Helper Methods

		/// <summary>
		/// Gets the supply demand.
		/// </summary>
		/// <param name="jobData">The job data.</param>
		/// <param name="criteria">The criteria.</param>
		/// <param name="reportCriteria">The report criteria.</param>
		/// <returns></returns>
		private SupplyDemandReportView GetSupplyDemand(LaborInsightGroupedResult jobData, SupplyDemandReportCriteria criteria,
															 JobReportCriteria reportCriteria)
		{
			var onetCodes = new List<string>();
			var onetCode = string.Empty;

			switch (criteria.ChartGroupInfo.GroupBy)
			{
				case ReportSupplyDemandGroupBy.Onet:
					onetCode = Helpers.Occupation.GetOnetCodeByOccupation(jobData.Group,
																																		RuntimeContext.CurrentRequest.UserContext.Culture);
					if (onetCode.IsNotNullOrEmpty()) onetCodes.Add(onetCode);
					break;
				default:
					if (reportCriteria.Criteria.OnetIsFamily)
					{
						var onetViews = Helpers.Occupation.GetOnets(
							new OnetCriteria { JobFamilyId = Convert.ToInt64(reportCriteria.Criteria.OnetFamily) },
							RuntimeContext.CurrentRequest.UserContext.Culture);

						onetCodes.AddRange(onetViews.Select(onetDetailsView => onetDetailsView.OnetCode));

						var mapping =
							Repositories.Configuration.LaborInsightMappingsView.SingleOrDefault(
								x => x.CodeGroupItemId == reportCriteria.Criteria.OnetFamily);

						if (mapping.IsNotNull()) onetCode = mapping.LaborInsightValue;
					}
					else
					{
						var onetView = Helpers.Occupation.GetOnetById(Convert.ToInt64(reportCriteria.Criteria.OnetID),
																													RuntimeContext.CurrentRequest.UserContext.Culture);
						onetCodes.Add(onetView.OnetCode);
						onetCode = onetView.OnetCode;
					}
					break;
			}

			var supplyCount = (from r in Repositories.Core.Resumes
												 join p in Repositories.Core.Persons on r.PersonId equals p.Id
												 where p.User.LastLoggedInOn >= DateTime.Today.AddDays(-30) &&
															 r.StatusId == ResumeStatuses.Active && onetCodes.Contains(r.PrimaryOnet)
												 select p.Id).Count();

			return new SupplyDemandReportView
			{
				SupplyCount = supplyCount,
				DemandCount = jobData.Count,
				Gap = jobData.Count - supplyCount,
				GroupBy = jobData.Group,
				OnetCode = onetCode,
			};
		}

		/// <summary>
		/// Gets the job report criteria.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		private JobReportCriteria GetJobReportCriteria(SupplyDemandReportCriteria criteria)
		{
			var jobReportCriteria = new JobReportCriteria { Criteria = new JobReportBaseCriteria() };

			jobReportCriteria.Criteria.OnetID = criteria.OccupationInfo.OnetId;
			jobReportCriteria.Criteria.OnetFamily = Convert.ToInt64(criteria.OccupationInfo.OnetFamily);
			jobReportCriteria.Criteria.State = criteria.LocationInfo.State.Item1;
			jobReportCriteria.Criteria.MSA = criteria.LocationInfo.MSA.Item1;
			jobReportCriteria.Criteria.Limit = 100;
			jobReportCriteria.Criteria.IncludeTotalClassifiedPostings = true;
			jobReportCriteria.Criteria.IncludeTotalUnclassifiedPostings = true;
			jobReportCriteria.Criteria.GroupBy = criteria.ChartGroupInfo.GroupBy.ToString();
			jobReportCriteria.QueryParameters = new List<QueryStringParams>
                                            {
                                              #region query string params
                                              new QueryStringParams
                                                {
                                                  Filter = FilterName.State,
                                                  Value = criteria.LocationInfo.State.Item1.ToString()
                                                },
                                              new QueryStringParams
                                                {
                                                  Filter = FilterName.Msa,
                                                  Value = criteria.LocationInfo.MSA.Item1.ToString()
                                                },
                                              #endregion
                                            };

			if (criteria.OccupationInfo.OnetId.IsNullOrEmpty())
			{
				jobReportCriteria.Criteria.OnetIsFamily = true;
				jobReportCriteria.QueryParameters.Add(new QueryStringParams
				{
					Filter = FilterName.Onet,
					Value = jobReportCriteria.Criteria.OnetFamily.ToString()
				});
			}
			else
			{
				var onetView = Helpers.Occupation.GetOnetById(Convert.ToInt64(jobReportCriteria.Criteria.OnetID),
																											RuntimeContext.CurrentRequest.UserContext.Culture);

				if (onetView.IsNotNull())
					jobReportCriteria.QueryParameters.Add(new QueryStringParams
					{
						Filter = FilterName.Onet,
						Value = onetView.OnetCode.RemoveNonNumericCharacters()
					});
			}

			if (criteria.DateRangeInfo.IsNotNull())
			{
				if (criteria.DateRangeInfo.Days.IsNotNull())
				{
					jobReportCriteria.Criteria.FromDate = DateTime.Today.AddDays(-criteria.DateRangeInfo.Days.Value);
					jobReportCriteria.Criteria.ToDate = DateTime.Today.Date;
				}

				if (criteria.DateRangeInfo.FromDate.IsNotNull())
					jobReportCriteria.Criteria.FromDate = criteria.DateRangeInfo.FromDate.Value;

				if (criteria.DateRangeInfo.ToDate.IsNotNull())
					jobReportCriteria.Criteria.ToDate = criteria.DateRangeInfo.ToDate.Value;
			}

			return jobReportCriteria;
		}

		#endregion

		#region Common Helper Methods

		/// <summary>
		/// Converts a data table into an array of bytes to output (as Excel or PDF)
		/// </summary>
		/// <param name="reportData">The report data table structure</param>
		/// <param name="criteriaDisplay">A displayable list of text describing the criteria</param>
		/// <param name="exportType">Whether it is required as Excel or PDF</param>
		/// <returns>The report bytes</returns>
		private static byte[] GetExportBytes(ReportDataTableModel reportData, List<string> criteriaDisplay, ReportExportType exportType)
		{
			var workbook = BuildExcelReport(reportData, criteriaDisplay);

			SaveOptions saveOptions;

			if (exportType == ReportExportType.Excel)
				saveOptions = new XlsSaveOptions(SaveFormat.Xlsx);
			else
				saveOptions = new PdfSaveOptions { Compliance = PdfCompliance.PdfA1b };

			var stream = new MemoryStream();
			workbook.Save(stream, saveOptions);
			stream.Close();

			return stream.ToArray();
		}

		/// <summary>
		/// Builds the workbook containing the report
		/// <param name="criteriaDisplay">A displayable list of text describing the criteria</param>
		/// </summary>
		/// <returns>The workbook</returns>
		private static Workbook BuildExcelReport(ReportDataTableModel reportData, List<string> criteriaDisplay)
		{
			var license = new License();
			license.SetLicense("Aspose.Cells.lic");

			var reportName = reportData.Data.TableName;

			if (reportData.Data.Columns.Count == reportData.FirstDataColumnIndex)
			{
				if (reportData.DisplayType == ReportDisplayType.BarChart)
					reportData.DisplayType = ReportDisplayType.Table;
			}

			var workbook = new Workbook();

			workbook.DefaultStyle.Font.Name = "Tahoma";

			var headerStyle = workbook.Styles[workbook.Styles.Add()];
			headerStyle.Font.IsBold = true;
			headerStyle.HorizontalAlignment = TextAlignmentType.Center;

			var reportSheet = reportData.DisplayType == ReportDisplayType.Table
													? workbook.Worksheets[0]
													: workbook.Worksheets[workbook.Worksheets.Add()];

			reportSheet.Name = reportName;
			reportSheet.PageSetup.Orientation = PageOrientationType.Landscape;

			var cells = reportSheet.Cells;

			var columnCount = reportData.Data.Columns.Count;

			var colIndex = 0;
			foreach (DataColumn column in reportData.Data.Columns)
			{
				var headerCell = cells[0, colIndex];
				headerCell.SetStyle(headerStyle);
				headerCell.PutValue(column.Caption);

				colIndex++;
			}

			var rowCount = 0;
			foreach (DataRow row in reportData.Data.Rows)
			{
				rowCount++;

				for (colIndex = 0; colIndex < columnCount; colIndex++)
				{
					cells[rowCount, colIndex].PutValue(row[colIndex]);
				}
			}

			if (reportData.DisplayType == ReportDisplayType.BarChart)
			{
				var chartSheet = workbook.Worksheets[0];
				chartSheet.Name = "Chart";
				chartSheet.PageSetup.Orientation = PageOrientationType.Landscape;
				chartSheet.PageSetup.RightMarginInch = 0.5;
				chartSheet.PageSetup.LeftMarginInch = 0.5;

				var lastRowIndexOfChart = 8 + rowCount * 2;

				var indexOfChart = chartSheet.Charts.Add(ChartType.Bar, 0, 0, lastRowIndexOfChart, 14);
				var chart = chartSheet.Charts[indexOfChart];

				chart.PlotArea.Border.IsVisible = false;
				chart.CategoryAxis.IsPlotOrderReversed = !reportData.IsGrouped;

				chart.CategoryAxis.MajorGridLines.IsVisible = false;

				//Set properties of chart title
				chart.Title.Text = reportName;
				chart.Title.Font.Color = Color.Black;
				chart.Title.Font.IsBold = true;
				chart.Title.Font.Size = 12;

				//Set properties of nseries
				var nSeriesRange = string.Format("'{0}'!{1}2:{2}{3}",
																				 reportName,
																				 ConvertColumnIndexToLetter(reportData.FirstDataColumnIndex),
																				 ConvertColumnIndexToLetter(columnCount - 1),
																				 rowCount + 1);
				chart.NSeries.Add(nSeriesRange, true);
				chart.NSeries.CategoryData = string.Format("'{0}'!A2:A{1}", reportName, rowCount + 1);

				if (chart.NSeries.Count > 1)
					chart.NSeries.IsColorVaried = true;

				//loop over the Chart's Nseries and Assign Name from Cell Values
				for (var i = 0; i < chart.NSeries.Count; i++)
				{
					chart.NSeries[i].Name = cells[0, i + reportData.FirstDataColumnIndex].Value.ToString();
				}

				//Set properties of categoryaxis title
				chart.CategoryAxis.Title.Text = reportName;
				chart.CategoryAxis.Title.Font.Color = Color.Black;
				chart.CategoryAxis.Title.Font.IsBold = true;
				chart.CategoryAxis.Title.Font.Size = 10;
				chart.CategoryAxis.Title.RotationAngle = 90;

				//Set properties of legend to show on Top
				if (chart.NSeries.Count > 1)
				{
					chart.Legend.Position = LegendPositionType.Top;
				}
				else
				{
					chart.Title.Text = string.Concat(chart.Title.Text, "\n", chart.NSeries[0].Name);
					chart.ShowLegend = false;
				}

				AppendCriteriaToSheet(criteriaDisplay, chartSheet, lastRowIndexOfChart + 1);
				chartSheet.AutoFitColumns();
			}
			else
			{
				AppendCriteriaToSheet(criteriaDisplay, reportSheet, rowCount + 2);
			}

			reportSheet.AutoFitColumns();

			return workbook;
		}

		/// <summary>
		/// Converts a cell-index (zero-based) to an Excel column letter (A, B, C, etc)
		/// </summary>
		/// <param name="columnIndex">The datatable column index</param>
		/// <returns>The Excel column letter</returns>
		private static string ConvertColumnIndexToLetter(int columnIndex)
		{
			if (columnIndex >= 26)
			{
				var extraColumnIndex = columnIndex / 26;
				columnIndex = columnIndex % 26;

				return string.Concat((char)(extraColumnIndex + 'A'), (char)(columnIndex + 'A'));
			}

			return ((char)(columnIndex + 'A')).ToString(CultureInfo.InvariantCulture);
		}

		/// <summary>
		/// Adds the displayable criteria to the work sheet
		/// </summary>
		/// <param name="criteriaDisplay">The criteria to display</param>
		/// <param name="sheet">The worksheet to which the criteria should be appended</param>
		/// <param name="startRowIndex">The row index from which the criteria should be displayed</param>
		private static void AppendCriteriaToSheet(List<string> criteriaDisplay, Worksheet sheet, int startRowIndex)
		{
			if (criteriaDisplay.IsNullOrEmpty())
				return;

			var headerStyle = sheet.Workbook.Styles[sheet.Workbook.Styles.Add()];
			headerStyle.Font.IsBold = true;
			headerStyle.HorizontalAlignment = TextAlignmentType.Left;

			var cells = sheet.Cells;

			var headerCell = cells[startRowIndex, 0];
			headerCell.PutValue("Report Criteria");
			headerCell.SetStyle(headerStyle);

			startRowIndex += 1;

			foreach (var criteria in criteriaDisplay)
			{
				var splitCriteria = criteria.Split(new[] { " - " }, 2, StringSplitOptions.None);

				cells[startRowIndex, 0].PutValue(splitCriteria[0]);
				if (splitCriteria.Length == 2)
					cells[startRowIndex, 1].PutValue(splitCriteria[1]);

				startRowIndex++;
			}
		}

		#endregion

		#region Statistics Methods

		public GetStatisticsResponse GetStatistics(GetStatisticsRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new GetStatisticsResponse(request);

				if (!ValidateRequest(request, response, Validate.All))
					return response;

				if (request.StatisticTypes.IsNull() || request.StatisticTypes.Count == 0 || request.StatisticDate.IsNull() || request.StatisticDate == DateTime.MinValue)
					return response;

				try
				{
					var date = request.StatisticDate;
					var startTs = new TimeSpan(00, 00, 00);
					var startDate = date.Date + startTs;

					var endTs = new TimeSpan(23, 59, 59);
					var endDate = date.Date + endTs;

					response.Statistics = Repositories.Report.Statistics.Where(s => request.StatisticTypes.Contains(s.StatisticType) && s.CreatedOn >= startDate && s.CreatedOn <= endDate)
						.OrderByDescending(x => x.CreatedOn)
						.Take(request.StatisticTypes.Count)
						.Select(x => new KeyValuePair<StatisticType, string>(x.StatisticType, x.Value)).ToList();
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		#endregion
	}
}
