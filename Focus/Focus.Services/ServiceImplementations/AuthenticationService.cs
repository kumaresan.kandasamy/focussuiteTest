﻿#region Copyright © 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

using Focus.Common.Models;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.EmailTemplate;
using Focus.Core.IntegrationMessages;
using Focus.Core.Messages;
using Focus.Core.Messages.AccountService;
using Focus.Core.Messages.AuthenticationService;
using Focus.Core.Messages.ResumeService;
using Focus.Core.Models;
using Focus.Core.Models.Career;
using Focus.Core.Models.Integration;
using Focus.Core.Views;
using Focus.Data.Core.Entities;
using Focus.Services.Core;
using Focus.Services.Core.Extensions;
using Focus.Services.Core.Login;
using Focus.Services.DtoMappers;
using Focus.Services.Mappers;
using Focus.Services.Messages;
using Focus.Services.Repositories.Integration;
using Focus.Services.ServiceContracts;
using JetBrains.Annotations;
using AcknowledgementType = Focus.Core.Messages.AcknowledgementType;
using Employee = Focus.Data.Core.Entities.Employee;

using Framework.Authentication;
using Framework.Core;
using Framework.Logging;

using Mindscape.LightSpeed;
using Focus.Data.Configuration.Entities;

#endregion

namespace Focus.Services.ServiceImplementations
{
	public class AuthenticationService : ServiceBase, IAuthenticationService
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="AuthenticationService" /> class.
		/// </summary>
		public AuthenticationService()
			: this(null)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="AuthenticationService" /> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		public AuthenticationService(IRuntimeContext runtimeContext)
			: base(runtimeContext)
		{ }

		/// <summary>
		/// Validates a session for a user / client application.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public SessionResponse ValidateSession(SessionRequest request)
		{
			var response = new SessionResponse(request);

			ValidateRequest(request, response, Validate.SessionId | Validate.UserCredentials);

			return response;
		}

		/// <summary>
		/// Starts a session for a user / client application.
		/// 
		/// This is a key security feature to ensure that no application can pick up
		/// the dll's and start using them.  Only official client applications should be able to call them
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public SessionResponse StartSession(SessionRequest request)
		{
			var response = new SessionResponse(request) { SessionId = Guid.NewGuid() };

			// Validate client tag only
			if (!ValidateRequest(request, response, Validate.ClientTag | Validate.AccessToken))
				return response;

			// If it is ok then create a session record with relevant access token so we can validate it each time a request comes in
			try
			{
				response.SessionId = Helpers.Session.CreateSession();
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}

			return response;
		}

		/// <summary>
		/// Gets the security questions.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public SecurityQuestionResponse GetSecurityQuestions(SecurityQuestionRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new SecurityQuestionResponse(request);

				// Validate client tag & security token only
				if (!ValidateRequest(request, response, Validate.ClientTag | Validate.License | Validate.AccessToken))
					return response;

				try
				{
					var userType = GetUserTypeForModule(request.Module);

					var user = request.UserName.IsNotNullOrEmpty()
						? Repositories.Core.Users.SingleOrDefault(u => u.UserName == request.UserName && u.UserType == userType)
						: Repositories.Core.Users.SingleOrDefault(u => u.Person.EmailAddress == request.EmailAddress && u.UserType == userType);

					if (user == null)
					{
						response.SetFailure(ErrorTypes.UserNotFound);
						return response;
					}

					var securityQuestions = Repositories.Core.UserSecurityQuestions.Where(usq => usq.UserId == user.Id).ToList();

					response.SecurityQuestionIndex1 = response.SecurityQuestionIndex2 = 0;

					if (securityQuestions.Count == 0)
					{
						return response;
					}

					var randomNumbers = new List<int>();
					var random = new Random();
					for( var i = 0; i < 2 && i < securityQuestions.Count; i++ )
					{
						int number;
						do
						{
							number = random.Next(1, securityQuestions.Max(usq => usq.QuestionIndex)+1);
						} while (randomNumbers.Contains(number));

						randomNumbers.Add(number);
					}

					var userSecurityQuestion = securityQuestions.FirstOrDefault(usq => usq.QuestionIndex == randomNumbers[0]);
					if (userSecurityQuestion != null)
					{
						response.SecurityQuestion1 = userSecurityQuestion.SecurityQuestion;
						response.SecurityQuestionId1 = userSecurityQuestion.SecurityQuestionId;
						response.SecurityAnswer1 = userSecurityQuestion.SecurityAnswerEncrypted.IsNull() ? string.Empty : Helpers.Encryption.Decrypt( userSecurityQuestion.SecurityAnswerEncrypted, TargetTypes.SecurityAnswer, EntityTypes.User, user.Id );
						response.SecurityQuestionIndex1 = randomNumbers[0];

						if (securityQuestions.Count > 1)
						{
							userSecurityQuestion = securityQuestions.FirstOrDefault(usq => usq.QuestionIndex == randomNumbers[1]);
							if (userSecurityQuestion != null)
							{
								response.SecurityQuestion2 = userSecurityQuestion.SecurityQuestion;
								response.SecurityQuestionId2 = userSecurityQuestion.SecurityQuestionId;
								response.SecurityAnswer2 = userSecurityQuestion.SecurityAnswerEncrypted.IsNull() ? string.Empty : Helpers.Encryption.Decrypt( userSecurityQuestion.SecurityAnswerEncrypted, TargetTypes.SecurityAnswer, EntityTypes.User, user.Id );
								response.SecurityQuestionIndex2 = randomNumbers[1];
							}
						}
					}
				}
				catch (Exception ex)
				{
					response.SetFailure(ErrorTypes.InvalidUserNameOrPassword, FormatErrorMessage(request, ErrorTypes.InvalidUserNameOrPassword), ex);
					Logger.Error(request.LogData(), ex.Message, ex);
				}

				return response;
			}
		}

		public ProcessSecurityQuestionsResponse ProcessSecurityQuestions(ProcessSecurityQuestionsRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new ProcessSecurityQuestionsResponse(request);

				// Validate client tag & security token only
				if (!ValidateRequest(request, response, Validate.ClientTag | Validate.SessionId | Validate.License | Validate.AccessToken))
					return response;

				try
				{
					// Username or Email Address must be supplied
					if (request.UserName.IsNullOrEmpty() && request.EmailAddress.IsNullOrEmpty())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.MustSupplyUserNameOrEmailAddress));
						Logger.Info(request.LogData(),
							"Unable to process forgotten password as no username or email address was supplied.");

						return response;
					}

					// Work out the user type we are processing forgotten password for
					var userType = GetUserTypeForModule(request.Module);

					// Get the user by username or if that returns no results email address
					var user = request.UserName.IsNotNullOrEmpty()
						? Repositories.Core.Users.FirstOrDefault(x => x.UserName == request.UserName && x.UserType == userType)
						: Repositories.Core.Users.FirstOrDefault(x => x.Person.EmailAddress == request.EmailAddress && x.UserType == userType);

					// If no user found, and this is Career/Explorer, attempt to send email with original PIN
					if (user == null && request.Module.IsIn(FocusModules.Career, FocusModules.CareerExplorer, FocusModules.Explorer) && AppSettings.Theme == FocusThemes.Education)
					{
						var pinRecord = Repositories.Core.RegistrationPins.FirstOrDefault(pin => pin.EmailAddress == request.EmailAddress);
						if (pinRecord.IsNotNull())
						{
							var template = new EmailTemplateData
							{
								EmailAddress = request.EmailAddress,
								PinNumber = pinRecord.Pin,
								AuthenticateUrl = string.Format(request.PinRegistrationUrl, pinRecord.Pin)
							};
							var preview = Helpers.Email.GetEmailTemplatePreview(EmailTemplateTypes.CareerAccountRegistration, template);

							Helpers.Email.SendEmail(request.EmailAddress, "", "", preview.Subject, preview.Body, true);

							return response;
						}
					}

					if (user == null)
					{
						response.SetFailure(ErrorTypes.UnableToProcessForgottenPassword, FormatErrorMessage(request, ErrorTypes.UnableToProcessForgottenPassword));
						Logger.Info(request.LogData(),
							string.Format(
								"Unable to process forgotten password for username / email address '{0}' as was unable to locate user.",
								request.UserContext.UserId));

						return response;
					}

					if (!user.Enabled)
					{
						response.SetFailure(ErrorTypes.UserNotEnabled, FormatErrorMessage(request, ErrorTypes.UserNotEnabled));
						Logger.Info(request.LogData(), "Inactive user attempting to reset their password.");
						return response;
					}

					if (user.Blocked)
					{
						response.SetFailure(ErrorTypes.UserBlocked, FormatErrorMessage(request, ErrorTypes.UserBlocked));
						Logger.Info(request.LogData(), "Blocked user attempting to reset their password.");
						return response;
					}

					user.ValidationKey = GenerateRandomHash();
					user.ValidationKeyExpiry = DateTime.Now.AddHours(AppSettings.PasswordValidationKeyExpiryHours);
					user.UserValidationKeyLogs.Add(new UserValidationKeyLog
					{
						Status = UserValidationKeyStatus.Sent,
						ValidationKey = user.ValidationKey,
						LogDate = DateTime.Now
					});

					var person = user.Person;
					var userEmail = person.EmailAddress;
					bool failed1 = false;
					bool failed2 = false;

					if (request.SecurityQuestionIndex1 != 0)
					{
						if (GetAnswer(request.SecurityQuestionIndex1, user) != request.SecurityAnswer1)
							failed1 = true;
					}

					if (request.SecurityQuestionIndex2 != 0)
					{
						if (GetAnswer(request.SecurityQuestionIndex2, user) != request.SecurityAnswer2)
							failed2 = true;
					}

					if (failed1 || failed2)
					{
						user.PasswordAttempts += 1;
						response.AttemptsLeft = Defaults.ConfigurationItemDefaults.MaximumFailedPasswordAttempts - user.PasswordAttempts;
						response.AttemptsFailed = true;

						if (user.PasswordAttempts == Defaults.ConfigurationItemDefaults.MaximumFailedPasswordAttempts)
						{
							user.Blocked = true;
							user.BlockedReason = BlockedReason.FailedPasswordReset;
						}

						Repositories.Core.SaveChanges();

						LogAction(request, ActionTypes.FailedPasswordReset, user);

						if (user.Blocked)
						{
							response.SetFailure(ErrorTypes.ValidationFailed, FormatErrorMessage(request, ErrorTypes.ValidationFailed));
							Logger.Info(request.LogData(), "User's security detials could not be validated.");
						}

						return response;
					}

					user.PasswordAttempts = 0;
					Repositories.Core.SaveChanges();


					if (userEmail.IsNullOrEmpty())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.UserEmailAddressNotFound));
						Logger.Info(request.LogData(), "User email address not found.");

						return response;
					}

					var resetPasswordUrl = string.Format("{0}?{1}", request.ResetPasswordUrl, HttpUtility.UrlEncode(user.ValidationKey));

					if (request.Module == FocusModules.Talent)
					{
						var templateValues = new EmailTemplateData
						{
							Url = resetPasswordUrl
						};
						var emailTemplate = Helpers.Email.GetEmailTemplatePreview(EmailTemplateTypes.TalentPasswordReset, templateValues);

						Helpers.Email.SendEmail(userEmail, "", "", emailTemplate.Subject, emailTemplate.Body, true, detectUrl: true);
					}
					else if (request.Module == FocusModules.Assist)
					{
						Helpers.Email.SendEmail(userEmail, "", "",
							Localise(request, "System.Email.ResetPassword.Subject"),
							Localise(request, "System.Email.ResetPassword.Body", person.FirstName, resetPasswordUrl),
							true,
							detectUrl: true);
					}
					else
					{
						var template = new EmailTemplateData
						{
							EmailAddress = request.EmailAddress,
							Url = resetPasswordUrl,
							RecipientName = person.FirstName
						};
						var preview = Helpers.Email.GetEmailTemplatePreview(EmailTemplateTypes.CareerPasswordReset, template);

						Helpers.Email.SendEmail(userEmail, "", "", preview.Subject, preview.Body, true, detectUrl: true);
					}
					Repositories.Core.SaveChanges();

					// Log the action
					LogAction(request, ActionTypes.ProcessForgottenPassword, user);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.UnableToProcessForgottenPassword), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		private string GetAnswer(int questionIndex, User user)
		{
			var userSecurityQuestionRecord = Repositories.Core.UserSecurityQuestions.FirstOrDefault(usq => usq.UserId == user.Id && usq.QuestionIndex == questionIndex);

			return userSecurityQuestionRecord.IsNull() ? string.Empty : Helpers.Encryption.Decrypt(userSecurityQuestionRecord.SecurityAnswerEncrypted, TargetTypes.SecurityAnswer, EntityTypes.User, user.Id);
		}

		/// <summary>
		/// Logs the user in using the specific provider.
		/// </summary>
		/// <param name="request">The login request.</param>
		/// <returns>A response with details of success or failure</returns>
		public LogInResponse LogIn(LogInRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new LogInResponse(request);

				// Validate client tag & security token only
				if (!ValidateRequest(request, response, Validate.ClientTag | Validate.License | Validate.AccessToken))
					return response;

				try
				{
					var loginProvider = new LoginProviderFactory(RuntimeContext, request.ModuleToLoginTo, request.Type).Provider;
					var loginModel = loginProvider.Authenticate(request.UserName, request.Password, request.ExternalId, request.FirstName, request.LastName, request.ScreenName, request.EmailAddress);

					// If authentication failed, fall back to Focus Authentication if required
					if (loginModel.UserView.IsNull() && loginModel.FallbackToFocus)
					{
						var focusLoginProvider = new LoginProviderFactory(RuntimeContext, request.ModuleToLoginTo, LoginType.Standard).Provider;
						loginModel = focusLoginProvider.Authenticate(request.UserName, request.Password, request.ExternalId, request.FirstName, request.LastName, request.ScreenName, request.EmailAddress);
					}
					
					// If user model is still null, the login has failed
					if (loginModel.UserView.IsNull())
					{
						if (loginModel.FailureType != ErrorTypes.Ok)
						{
							response.SetFailure(loginModel.FailureType, FormatErrorMessage(request, loginModel.FailureType));
							Logger.Info(request.LogData(), loginModel.FailureMessage);
						}
						else
						{
							response.SetFailure(ErrorTypes.InvalidUserNameOrPassword, FormatErrorMessage(request, ErrorTypes.InvalidUserNameOrPassword));
							Logger.Info(request.LogData(), string.Format("Unable to locate user using username '{0}'", request.UserName));
						}
						return response;
					}

					// Check if user existed in Focus
					User user = null;
					var userView = loginModel.UserView;
					var userId = userView.FocusUserId;
					if (userId == 0)
					{
						// Add user to Focus based on user model
						if (loginModel.UpdateFocus)
						{
							var createRoles = (request.CreateRoles ?? String.Empty).Split(';').Where(r => !String.IsNullOrWhiteSpace(r)).Distinct().ToList();
							user = CreateUserFromModel(userView, request.ModuleToLoginTo, createRoles);

							if (loginModel.UserLogAction != ActionTypes.NoAction)
							{
								LogAction(request, loginModel.UserLogAction, user);
							}
						}
						else if (request.ModuleToLoginTo.IsIn(FocusModules.Explorer, FocusModules.CareerExplorer, FocusModules.Career))
						{
							// In the case of Career/Explorer, the user details can be passed back to the web page to auto-populate the registration page (Currently only supported in Career/Explorer)
							response.IsIntegrationUser = true;
							response.IntegrationDetails = userView;
							return response;
						}
						else
						{
							response.SetFailure(ErrorTypes.InvalidUserNameOrPassword, FormatErrorMessage(request, ErrorTypes.InvalidUserNameOrPassword));
							Logger.Info(request.LogData(), string.Format("Unable to locate user using username '{0}'", request.UserName));
							return response;
						}
					}

					if (user.IsNull())
					{
						user = Repositories.Core.FindById<User>(userId);
					}

					// Update user details from model if specified (and if user hasn't been newly created)
					if (loginModel.UpdateFocus && userView.FocusUserId > 0)
					{
						var updateRoles = (request.UpdateRoles ?? String.Empty).Split(';').Where(r => !String.IsNullOrWhiteSpace(r)).Distinct().ToList();
						UpdateUserFromUserModel(user, userView, updateRoles);
					}

					// Do an integration call if necessary to update more user details
					if (loginModel.UpdateFocus && loginModel.UpdateFromIntegration)
					{
						var isNew = userView.FocusUserId == 0;
						var errorCheck = UpdateUserFromIntegration(request, user, null, isNew);
						if (errorCheck != ErrorTypes.Ok)
						{
							response.SetFailure(errorCheck, FormatErrorMessage(request, errorCheck));
							var message = errorCheck == ErrorTypes.ResumeNotSaved
								              ? string.Format("Unable to save resume for integrated job seeker '{0}'", request.UserName)
								              : string.Format("Unable to update user for integrated job seeker '{0}'", request.UserName);
							Logger.Info(request.LogData(), message);

							return response;
						}
					}

					// Validate the user isn't blocked
					if (user.Blocked)
					{
						response.SetFailure(ErrorTypes.UserBlocked, FormatErrorMessage(request, ErrorTypes.UserBlocked));
						Logger.Info(request.LogData(), string.Format("User '{0}' has been blocked", request.UserName));

						return response;
					}

					// Validate the user is enabled
					if (!user.Enabled)
					{
						// FVN-328 User is inactive (A Jobseeker cannot be 'disabled' they can only be blocked or 'inactivated'.  The enabled flag is set to false when the jobseeker is inactived.)
						if ((user.UserType & UserTypes.Career) == UserTypes.Career && user.AccountDisabledReason.IsNotNull())
						{
							response.AccountReactivateRequest = true;
						}
						else if (!loginModel.BypassEnabledCheck)
						{
							response.SetFailure(ErrorTypes.UserNotEnabled, FormatErrorMessage(request, ErrorTypes.UserNotEnabled));
							Logger.Info(request.LogData(), string.Format("User '{0}' is not enabled", request.UserName));

							return response;
						}
					}

					// Validate the user can log into the module
					if (!UserHasAccessToModule(user, request.ModuleToLoginTo))
					{
						response.SetFailure(ErrorTypes.InvalidUserTypeForModule, FormatErrorMessage(request, ErrorTypes.InvalidUserTypeForModule));
						Logger.Info(request.LogData(), string.Format("User '{0}' isn't a valid {1} user.  It is a {2} user.", request.UserName, request.ModuleToLoginTo, user.UserType));
						return response;
					}

					var newSession = false;

					// Validate the session
					try
					{
						Helpers.Session.ValidateSession(request.SessionId, user.Id);
					}
					catch (Exception ex)
					{
						if (!request.AllowResetSessionId)
						{
							response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
							Logger.Error(request.LogData(), response.Message, response.Exception);
							return response;
						}

						Logger.Error(request.LogData(), FormatErrorMessage(request, ErrorTypes.InvalidSessionId), ex);
						newSession = true;
					}

					// This is a workaround to the "Invalid Session Id" error. User gets given a new session in this case
					if (newSession)
					{
						try
						{
							var newSessionId = Helpers.Session.CreateSession();
							response.ResetSessionId = request.SessionId = newSessionId;
						}
						catch (Exception ex)
						{
							response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
							Logger.Error(request.LogData(), response.Message, response.Exception);
							return response;
						}
					}

					// Re-get the user because the ValidateSession method does SaveChanges which clears down the entities from Lightspeed, so lightspeed no longer tracks them unless they are read from the DB again
					user = Repositories.Core.FindById<User>(user.Id);

					response.FirstName = user.Person.FirstName;
					response.LastName = user.Person.LastName;
					response.EmailAddress = user.Person.EmailAddress;
					response.PersonId = user.Person.Id;
					if (user.Person.Employee.IsNotNull())
					{
						response.EmployerId = user.Person.Employee.EmployerId;
						response.EmployeeId = user.Person.Employee.Id;
					}

					response.UserId = user.Id;
					response.IsEnabled = user.Enabled;
					response.ScreenName = user.ScreenName;
					response.ExternalUserId = userView.ExternalUserId ?? string.Empty;
					response.ExternalUserName = userView.ExternalUserName ?? string.Empty;
					response.ExternalPassword = userView.ExternalPassword ?? string.Empty;
					response.ExternalOfficeId = userView.ExternalOfficeId ?? string.Empty;
					response.UserName = request.UserName;
					response.IsMigrated = user.IsMigrated;
					response.IsEnabled = user.Enabled;
					response.RegulationsConsent = user.RegulationsConsent;

					if (request.ModuleToLoginTo.IsIn(FocusModules.Career, FocusModules.CareerExplorer))
					{
						// Retrieve data required for App.UserData
						UpdateCareerUserData(user, request.UserContext.Culture, response);

						response.IsMigrated = user.IsMigrated;
						response.ProgramAreaId = user.Person.ProgramAreaId;
						response.DegreeId = user.Person.DegreeId;
						response.EnrollmentStatus = user.Person.EnrollmentStatus;
						response.CampusId = user.Person.CampusId;

						// Log login as self service
                        var loginRequest = Helpers.SelfService.PublishSelfServiceActivity(ActionTypes.LogIn, user.Id, jobSeekerId: user.PersonId);

						if (AppSettings.IntegrationClient != IntegrationClient.Standalone)
						{
							// Required for WI
							var integrationRequest = new IntegrationRequestMessage
							{
								ActionerId = request.UserContext.UserId,
								IntegrationPoint = IntegrationPoint.JobSeekerLogin,
								JobSeekerLoginRequest = new JobSeekerLoginRequest
								{
									UserId = request.UserContext.UserId,
									PersonId = user.PersonId,
									Active = true,
                                    Activities = new List<AssignJobSeekerActivityRequest> { loginRequest }
								}
							};

							Helpers.Messaging.Publish(integrationRequest);
						}
					}

					if (user.LastLoggedInOn == null && user.LoggedInOn == null)
						user.FirstLoggedInOn = DateTime.Now;

					// Update the LoggedInOn and LastLoggedInOn UpdateSession will commit to database
					user.LastLoggedInOn = user.LoggedInOn;
					user.LoggedInOn = DateTime.Now;
					user.PasswordAttempts = 0;

					response.LastLoggedInOn = user.LastLoggedInOn;

					Helpers.Session.UpdateSession(request.SessionId, user.Id);

					// Log the action
					LogAction(request, ActionTypes.LogIn, typeof(User).Name, user.Id);
				}
				catch (Exception ex)
				{
					response.SetFailure(ErrorTypes.InvalidUserNameOrPassword, FormatErrorMessage(request, ErrorTypes.InvalidUserNameOrPassword), ex);
					Logger.Error(request.LogData(), ex.Message, ex);
				}

				return response;
			}
		}

		/// <summary>
		/// Automatically create user from a login model
		/// </summary>
		/// <param name="userModel">The user model</param>
		/// <param name="module">The current module</param>
		/// <param name="createRoles">Override default roles added for user</param>
		/// <returns>The user record</returns>
		private User CreateUserFromModel(ValidatedUserView userModel, FocusModules module, List<string> createRoles)
		{
			var password = new Password(AppSettings, userModel.Password, false);
			var userType = UserTypes.Anonymous;
			var userRoles = new List<string>();

			switch (module)
			{
				case FocusModules.Assist:
					userType = UserTypes.Assist;
					userRoles.Add(Constants.RoleKeys.AssistUser);
					break;
				case FocusModules.Talent:
					userType = UserTypes.Talent;
					userRoles.Add(Constants.RoleKeys.TalentUser);
					break;
				case FocusModules.Career:
					userType = UserTypes.Career;
					userRoles.Add(Constants.RoleKeys.CareerUser);
					break;
				case FocusModules.Explorer:
					userType = UserTypes.Explorer;
					userRoles.Add(Constants.RoleKeys.ExplorerUser);
					break;
				case FocusModules.CareerExplorer:
					userType = UserTypes.Career | UserTypes.Explorer;
					userRoles.Add(Constants.RoleKeys.CareerUser);
					userRoles.Add(Constants.RoleKeys.ExplorerUser);
					break;
			}

			var user = new User
			{
				UserName = userModel.UserName,
				ScreenName = userModel.ScreenName.IsNotNullOrEmpty() ? userModel.ScreenName : string.Concat(userModel.FirstName, " ", userModel.LastName),
				PasswordSalt = password.Salt,
				PasswordHash = password.Hash,
				UserType = userType,
				ExternalId = userModel.ExternalId,
				Enabled = true,
				IsMigrated = userModel.IsMigrated,
				IsClientAuthenticated = userModel.IsClientAuthenticated,
			};

            if ((AppSettings.SamlEnabled || AppSettings.SamlEnabledForCareer) && AppSettings.Module == FocusModules.CareerExplorer)
                user.RegulationsConsent = true;

			var person = new Person
			{
				TitleId = 0,
				FirstName = userModel.FirstName,
				MiddleInitial = userModel.MiddleInitial,
				LastName = userModel.LastName,
				EmailAddress = userModel.EmailAddress,
				ExternalOffice = userModel.ExternalOfficeId,
				User = user
			};

			var address = new PersonAddress
			{
				IsPrimary = true,
				Line1 = userModel.AddressLine1 ?? String.Empty,
				Line2 = string.Empty,
				TownCity = userModel.AddressTownCity ?? String.Empty,
				PostcodeZip = userModel.AddressPostcodeZip ?? String.Empty
			};
			person.PersonAddresses.Add(address);

			if (userModel.PhoneNumber.IsNotNullOrEmpty())
			{
				person.PhoneNumbers.Add(new PhoneNumber
				{
					PhoneType = PhoneTypes.Phone,
					IsPrimary = true,
					Number = userModel.PhoneNumber
				});
			}

            Repositories.Core.Add(person);

			var issues = new Issues { PersonId = person.Id };
			Repositories.Core.Add(issues);

			// If there were no create roles in the assertion then read them out of model
			if (createRoles.IsNullOrEmpty())
			{
				createRoles = userModel.Roles ?? new List<string>();
			}

			// Remove any standard roles, other than the ones specific for the user type
			var standardUserRoles = new List<string>
			{
				Constants.RoleKeys.TalentUser,
				Constants.RoleKeys.AssistUser,
				Constants.RoleKeys.CareerUser,
				Constants.RoleKeys.ExplorerUser
			};

			createRoles.RemoveAll(standardUserRoles.Contains);
			createRoles.AddRange(userRoles);

			// Select the roles that exist in the database and add to the user.
			var validRoles = createRoles.Select(r => Repositories.Core.Roles.SingleOrDefault(x => x.Key == r)).Where(r => r.IsNotNull()).ToList();
			validRoles.ForEach(user.Roles.Add);

			Repositories.Core.SaveChanges();

			return user;
		}

		/// <summary>
		/// Automatically create user from a login model
		/// </summary>
		/// <param name="user">The current user</param>
		/// <param name="userModel">The user model</param>
		/// <param name="updateRoles">Any new roles to be added for the user</param>
		/// <returns>The user record</returns>
		private void UpdateUserFromUserModel(User user, ValidatedUserView userModel, List<string> updateRoles)
		{
			var person = user.Person;

			if (person.EmailAddress != userModel.EmailAddress && userModel.EmailAddress.IsNotNullOrEmpty())
				person.EmailAddress = userModel.EmailAddress;

			if (person.FirstName != userModel.FirstName && userModel.FirstName.IsNotNullOrEmpty())
				person.FirstName = userModel.FirstName;

			if (person.LastName != userModel.LastName && userModel.LastName.IsNotNullOrEmpty())
				person.LastName = userModel.LastName;

			if (user.ScreenName != userModel.ScreenName && userModel.ScreenName.IsNotNullOrEmpty())
				user.ScreenName = userModel.ScreenName;

			if (userModel.Password.IsNotNullOrEmpty())
			{
				var password = new Password(AppSettings, userModel.Password, user.PasswordSalt, false);
				user.MigratedPasswordHash = null;
				user.PasswordHash = password.Hash;
			}

			if (updateRoles.IsNullOrEmpty())
			{
				updateRoles = userModel.Roles;
			}

			if (updateRoles.IsNotNullOrEmpty())
			{
				var standardUserRoles = new List<string>
				{
					Constants.RoleKeys.TalentUser,
					Constants.RoleKeys.AssistUser,
					Constants.RoleKeys.CareerUser,
					Constants.RoleKeys.ExplorerUser
				};

				// Remove any roles that the user already has except for the standard set
				var existingRoles = user.Roles.Where(r => !standardUserRoles.Contains(r.Key)).ToList();

				foreach (var existingRole in existingRoles)
				{
					user.Roles.Remove(existingRole);
				}

				// Select the roles that exist in the database and add to the user.
				var validRoles = updateRoles
					.Where(r => !standardUserRoles.Contains(r))
					.Select(r => Repositories.Core.Roles.SingleOrDefault(x => x.Key == r))
					.Where(r => r.IsNotNull())
					.ToList();

				validRoles.ForEach(user.Roles.Add);
			}

			Repositories.Core.SaveChanges();
		}

		/// <summary>
		/// Performs a call to any integration client to get details that can be used to update the user
		/// </summary>
		/// <param name="request">The current service request</param>
		/// <param name="user">The current user</param>
		/// <param name="person">The associated person record</param>
		/// <param name="isNew">Whether the user was newly added by the login</param>
		/// <returns>Any error that was raised (ErrorTypes.OK means no error)</returns>
		private ErrorTypes UpdateUserFromIntegration(ServiceRequest request, User user, Person person, bool isNew)
		{
			if (AppSettings.IntegrationClient == IntegrationClient.Standalone)
				return ErrorTypes.Ok;

			if ((user.UserType & UserTypes.Career) != UserTypes.Career && (user.UserType & UserTypes.Explorer) != UserTypes.Explorer)
				return ErrorTypes.Ok;

			if (person.IsNull())
				person = user.Person;

			var jobSeeker = person.ToIntegrationJobSeeker(RuntimeContext);
			var getJobSeekerResponse = Repositories.Integration.GetJobSeeker(new GetJobSeekerRequest { JobSeeker = jobSeeker, IsNew = isNew });

			// If we have a successful outcome and there is a job seeker plus a resume then process it
			if (getJobSeekerResponse.Outcome == IntegrationOutcome.Success && getJobSeekerResponse.JobSeeker != null && getJobSeekerResponse.JobSeeker.Resume != null)
			{
				var personAddress = new PersonAddress
				{
					Line1 = getJobSeekerResponse.JobSeeker.AddressLine1,
					Line2 = getJobSeekerResponse.JobSeeker.AddressLine2,
					TownCity = getJobSeekerResponse.JobSeeker.AddressTownCity,
					PostcodeZip = getJobSeekerResponse.JobSeeker.AddressPostcodeZip,
					StateId = getJobSeekerResponse.JobSeeker.AddressStateId ?? 0,
					CountryId = getJobSeekerResponse.JobSeeker.AddressCountryId ?? 0
				};
				person.PersonAddresses.Add(personAddress);
				var saveResumeRequest = new SaveResumeRequest
				{
					ClientTag = request.ClientTag,
					RequestId = request.RequestId,
					SessionId = request.SessionId,
					UserContext = new UserContext { ActionerId = user.Id, UserId = user.Id }, // As we are logging in there is no user context for this request so we need to create a new one for the save resume request.
					PersonId = person.Id,
					SeekerResume = getJobSeekerResponse.JobSeeker.Resume,
					IgnoreLens = true,
				};

				var resumeService = new ResumeService(RuntimeContext);
				var saveResumeResponse = resumeService.SaveResume(saveResumeRequest);

				if (saveResumeResponse.Acknowledgement == AcknowledgementType.Failure)
				{
					return ErrorTypes.ResumeNotSaved;
				}

				// If we have successfully saved the resume then mark the user as migrated
				user.IsMigrated = true;
				Repositories.Core.SaveChanges();
			}

			return ErrorTypes.Ok;
		}

		/*
		/// <summary>
		/// Logs the user in.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public LogInResponse LogInOld(LogInRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new LogInResponse(request);

				// Validate client tag & security token only
				if (!ValidateRequest(request, response, Validate.ClientTag | Validate.License | Validate.AccessToken))
					return response;

				// ReSharper disable once InconsistentNaming
				var isSSO = request.Type == LoginType.SSO;
				try
				{
					var userId = (long)-1;
					User user = null;
					var externalUserId = string.Empty;
					var externalUserName = string.Empty;
					var externalPassword = string.Empty;
					var externalOfficeId = string.Empty;
					var userTableChecked = false;
					var authenticatedAssistViaClient = false;

					if (isSSO)
					{
						// TODO: Check what we need to create for a new SSO user.
						user = Repositories.Core.Users.SingleOrDefault(x => x.ExternalId == request.ExternalId);
						var isNew = (user == null);
						Person person;
						if (isNew)
						{
							#region Add the oAuth user to the system

							#region Validate Fields

							if (request.ExternalId.IsNullOrEmpty())
							{
								response.SetFailure(ErrorTypes.RegistrationFieldRequired, FormatErrorMessage(request, ErrorTypes.RegistrationFieldRequired, "ExternalId"));
								return response;
							}

							if (request.ScreenName.IsNullOrEmpty())
							{
								response.SetFailure(ErrorTypes.RegistrationFieldRequired, FormatErrorMessage(request, ErrorTypes.RegistrationFieldRequired, "ScreenName"));
								return response;
							}

							if (request.EmailAddress.IsNullOrEmpty())
							{
								response.SetFailure(ErrorTypes.RegistrationFieldRequired, FormatErrorMessage(request, ErrorTypes.RegistrationFieldRequired, "EmailAddress"));
								return response;
							}

							if (request.FirstName.IsNullOrEmpty())
							{
								response.SetFailure(ErrorTypes.RegistrationFieldRequired, FormatErrorMessage(request, ErrorTypes.RegistrationFieldRequired, "FirstName"));
								return response;
							}

							if (request.LastName.IsNullOrEmpty())
							{
								response.SetFailure(ErrorTypes.RegistrationFieldRequired, FormatErrorMessage(request, ErrorTypes.RegistrationFieldRequired, "LastName"));
								return response;
							}

							#endregion

							try
							{
								UserTypes? userType = null;

								#region Determine the type of user to create

								if (request.ModuleToLoginTo == FocusModules.Assist)
									userType = UserTypes.Assist;

								if (request.ModuleToLoginTo == FocusModules.Talent)
									userType = UserTypes.Talent;

								if (request.ModuleToLoginTo == FocusModules.Career || request.ModuleToLoginTo == FocusModules.CareerExplorer)
									userType = UserTypes.Career;

								if (request.ModuleToLoginTo == FocusModules.Explorer || request.ModuleToLoginTo == FocusModules.CareerExplorer)
									userType = userType.IsNull() ? UserTypes.Explorer : userType | UserTypes.Explorer;

								#endregion

								#region Create the User record

								var password = new Password(AppSettings, String.Format("Pasty99{0}", request.ExternalId), false);
								user = new User
								{
									UserName = String.Format("sso-{0}@ci.bgt.com", request.ExternalId),
									PasswordSalt = password.Salt,
									PasswordHash = password.Hash,
									UserType = userType,
									ExternalId = request.ExternalId,
									Enabled = true,
									IsMigrated = (userType == UserTypes.Explorer)
								};

								#endregion

								#region Create the person record

								person = new Person
								{
									TitleId = 0,
									FirstName = (request.FirstName.IsNotNullOrEmpty()) ? request.FirstName : "Unknown",
									MiddleInitial = "",
									LastName = (request.LastName.IsNotNullOrEmpty()) ? request.LastName : "Unknown",
									EmailAddress = request.EmailAddress,
									User = user
								};
								// Set screen name
								user.ScreenName = person.FirstName + " " + person.LastName;

								#endregion

								#region Add roles

								var standardUserRoles = new List<string>
								{
									Constants.RoleKeys.TalentUser,
									Constants.RoleKeys.AssistUser,
									Constants.RoleKeys.CareerUser,
									Constants.RoleKeys.ExplorerUser
								};

								// Get roles out of assertion
								var createRoles = (request.CreateRoles ?? String.Empty).Split(';').Where(r => !String.IsNullOrWhiteSpace(r)).Distinct().ToList();

								// If there were no create roles in the assertion then read them out of config
								if (!createRoles.Any() && request.ModuleToLoginTo == FocusModules.Assist) createRoles = AppSettings.SamlDefaultAssistRoles.Split(';').Distinct().ToList();

								// Remove any roles that are in the standard list of user roles
								createRoles.RemoveAll(standardUserRoles.Contains);

								// Select the roles that exist in the database and add to the user.
								var validRoles = createRoles.Select(r => Repositories.Core.Roles.SingleOrDefault(x => x.Key == r)).Where(r => r.IsNotNull()).ToList();
								validRoles.ForEach(user.Roles.Add);

								#endregion

								#region Process additional data for each module

								switch (request.ModuleToLoginTo)
								{
									case FocusModules.Talent:
										var talentUserRole = Repositories.Core.Roles.Single(x => x.Key == Constants.RoleKeys.TalentUser);
										user.Roles.Add(talentUserRole);
										break;

									case FocusModules.Assist:
										var assistUserRole = Repositories.Core.Roles.Single(x => x.Key == Constants.RoleKeys.AssistUser);
										user.Roles.Add(assistUserRole);
										break;

									case FocusModules.Career:
									case FocusModules.Explorer:
									case FocusModules.CareerExplorer:
										var careerUserRole = Repositories.Core.Roles.SingleOrDefault(x => x.Key == Constants.RoleKeys.CareerUser);
										var explorerUserRole = Repositories.Core.Roles.SingleOrDefault(x => x.Key == Constants.RoleKeys.ExplorerUser);

										if ((userType & UserTypes.Career) == UserTypes.Career)
											user.Roles.Add(careerUserRole);

										if ((userType & UserTypes.Explorer) == UserTypes.Explorer)
											user.Roles.Add(explorerUserRole);

										// Add a default issues record
										// We do this here as an SSO user will not go throught the normal registration process
										var issues = new Issues { PersonId = person.Id };
										Repositories.Core.Add(issues);

										// We must add a blank primary address for the new jobseeker otherwise it will not appear in the CandidateIssueView
										// This address will be overwritten by a  SaveResume call with a meaningful address.
										var address = new PersonAddress
										{
											IsPrimary = true,
											Line1 = String.Empty,
											Line2 = String.Empty,
											TownCity = String.Empty,
											PostcodeZip = String.Empty
										};
										person.PersonAddresses.Add(address);

										break;
								}

								#endregion

								Repositories.Core.Add(person);
								Repositories.Core.SaveChanges();

								// Log the action
								LogAction(request, ActionTypes.RegisterOAuthAccount, user);
							}
							catch (Exception ex)
							{
								response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
								Logger.Error(request.LogData(), response.Message, response.Exception);

								return response;
							}

							#endregion
						}
						else
						{
							#region Update the users details if the profile data has changed

							person = user.Person;

							if (person.EmailAddress != request.EmailAddress && request.EmailAddress.IsNotNullOrEmpty())
								person.EmailAddress = request.EmailAddress;

							if (person.FirstName != request.FirstName && request.FirstName.IsNotNullOrEmpty())
								person.FirstName = request.FirstName;

							if (person.LastName != request.LastName && request.LastName.IsNotNullOrEmpty())
								person.LastName = request.LastName;

							if (user.ScreenName != request.ScreenName && request.ScreenName.IsNotNullOrEmpty())
								user.ScreenName = request.ScreenName;

							if (user.EntityState == EntityState.Modified || person.EntityState == EntityState.Modified)
								Repositories.Core.SaveChanges();

							// Get roles out of assertion, excluding any repeated roles.
							var updateRoles = (request.UpdateRoles ?? String.Empty).Split(';').Where(r => !String.IsNullOrWhiteSpace(r)).Distinct().ToList();

							if (updateRoles.Any())
							{
								var standardUserRoles = new List<string>
								{
									Constants.RoleKeys.TalentUser,
									Constants.RoleKeys.AssistUser,
									Constants.RoleKeys.CareerUser,
									Constants.RoleKeys.ExplorerUser
								};

								// Remove any roles that the user already has except for the standard set
								var existingRoles = user.Roles.Where(r => !standardUserRoles.Contains(r.Key)).ToList();

								foreach (var existingRole in existingRoles)
								{
									user.Roles.Remove(existingRole);
								}

								// Select the roles that exist in the database and add to the user.
								var validRoles = updateRoles
									.Where(r => !standardUserRoles.Contains(r))
									.Select(r => Repositories.Core.Roles.SingleOrDefault(x => x.Key == r))
									.Where(r => r.IsNotNull())
									.ToList();

								validRoles.ForEach(user.Roles.Add);
							}

							#endregion
						}

						userId = user.Id;

						#region Get job seeker data from client integration and update if applicable

						if (AppSettings.IntegrationClient != IntegrationClient.Standalone &&
						    ((user.UserType & UserTypes.Career) == UserTypes.Career || (user.UserType & UserTypes.Explorer) == UserTypes.Explorer))
						{
							var jobSeeker = person.ToIntegrationJobSeeker(RuntimeContext);
							var getJobSeekerResponse = Repositories.Integration.GetJobSeeker(new GetJobSeekerRequest { JobSeeker = jobSeeker, IsNew = isNew });

							// If we have a successful outcome and there is a job seeker plus a resume then process it
							if (getJobSeekerResponse.Outcome == IntegrationOutcome.Success && getJobSeekerResponse.JobSeeker != null && getJobSeekerResponse.JobSeeker.Resume != null)
							{
								var personAddress = new PersonAddress
								{
									Line1 = getJobSeekerResponse.JobSeeker.AddressLine1,
									Line2 = getJobSeekerResponse.JobSeeker.AddressLine2,
									TownCity = getJobSeekerResponse.JobSeeker.AddressTownCity,
									PostcodeZip = getJobSeekerResponse.JobSeeker.AddressPostcodeZip,
									StateId = getJobSeekerResponse.JobSeeker.AddressStateId ?? 0,
									CountryId = getJobSeekerResponse.JobSeeker.AddressCountryId ?? 0
								};
								person.PersonAddresses.Add(personAddress);
								var saveResumeRequest = new SaveResumeRequest
								{
									ClientTag = request.ClientTag,
									RequestId = request.RequestId,
									SessionId = request.SessionId,
									UserContext = new UserContext { ActionerId = userId, UserId = user.Id }, // As we are logging in there is no user context for this request so we need to create a new one for the save resume request.
									PersonId = person.Id,
									SeekerResume = getJobSeekerResponse.JobSeeker.Resume,
									IgnoreLens = true,
								};

								var resumeService = new ResumeService(RuntimeContext);
								var saveResumeResponse = resumeService.SaveResume(saveResumeRequest);

								if (saveResumeResponse.Acknowledgement == AcknowledgementType.Failure)
								{
									response.SetFailure(ErrorTypes.ResumeNotSaved, FormatErrorMessage(request, ErrorTypes.ResumeNotSaved));
									Logger.Info(request.LogData(), string.Format("Unable to save resume for integrated job seeker '{0}'", request.UserName));

									return response;
								}

								// If we have successfully saved the resume then mark the user as migrated
								user.IsMigrated = true;
								Repositories.Core.SaveChanges();
							}
						}

						#endregion
					}
					else
					{
						#region Assist

						if (request.ModuleToLoginTo == FocusModules.Assist)
						{
							if (AppSettings.AuthenticateAssistUserViaClient)
							{
								#region Authenticate assist user against client system

								var authenticationResponse = Repositories.Integration.AuthenticateStaffUser(new AuthenticateStaffRequest { Username = request.UserName, Password = request.Password });

								if (authenticationResponse.StaffUserInfo.IsNotNull() && authenticationResponse.StaffUserInfo.ExternalId.IsNotNull())
								{
	                authenticatedAssistViaClient = true;

									externalUserId = authenticationResponse.StaffUserInfo.ExternalId;
									externalUserName = request.UserName;
									externalPassword = request.Password;
									if (authenticationResponse.StaffUserInfo.AuthModel.IsNotNull())
										externalOfficeId = (authenticationResponse.StaffUserInfo.AuthModel.ExternalOfficeId.IsNotNullOrEmpty()) ? authenticationResponse.StaffUserInfo.AuthModel.ExternalOfficeId : "Undefined";

									user = Repositories.Core.Users.SingleOrDefault(x => x.ExternalId == externalUserId);

									if (user.IsNull())
									{
										#region Create assist user account

										var userPerson = new PersonDto
										{
											FirstName = (authenticationResponse.StaffUserInfo.FirstName.IsNotNullOrEmpty()) ? authenticationResponse.StaffUserInfo.FirstName : "Undefined",
											LastName = (authenticationResponse.StaffUserInfo.Surname.IsNotNullOrEmpty()) ? authenticationResponse.StaffUserInfo.Surname : "Undefined"
										};

										// Create dummy username so user cannot access system directly
										var dummyUserName = string.Format(AppSettings.DummyUsernameFormat, request.UserName);
	                  var dummyEmailName = dummyUserName.Contains("@") ? dummyUserName : string.Concat(dummyUserName, "@ci.bgt.com");

										var model = new CreateAssistUserModel
										{
											AccountUserName = dummyUserName,
											AccountPassword = request.Password,
											AccountExternalId = externalUserId,
											UserPerson = userPerson,
											UserEmailAddress = (authenticationResponse.StaffUserInfo.Email.IsNotNullOrEmpty()) ? authenticationResponse.StaffUserInfo.Email : dummyEmailName,
											UserPhone = (authenticationResponse.StaffUserInfo.PhoneNumber.IsNotNullOrEmpty()) ? authenticationResponse.StaffUserInfo.PhoneNumber : "Undefined",
											UserExternalOffice = externalOfficeId,
											IsClientAuthenticated = true,
											IsEnabled = true
										};

										var accountRequest = new CreateAssistUserRequest
										{
											ClientTag = request.ClientTag,
											RequestId = request.RequestId,
											SessionId = request.SessionId,
											UserContext = request.UserContext,
											Models = new List<CreateAssistUserModel> { model }
										};

										var accountService = new AccountService();
										var accountResponse = accountService.CreateAssistUser(accountRequest);

										if (accountResponse.Acknowledgement == AcknowledgementType.Failure)
										{
											response.SetFailure(ErrorTypes.InvalidUserNameOrPassword, FormatErrorMessage(request, ErrorTypes.InvalidUserNameOrPassword));
											Logger.Info(request.LogData(), string.Format("Unable to create a dummy account for '{0}'", request.UserName));

											return response;
										}

										user = Repositories.Core.Users.SingleOrDefault(x => x.ExternalId == authenticationResponse.StaffUserInfo.ExternalId);

										#endregion
									}
								}
								else
								{
									response.SetFailure(ErrorTypes.InvalidUserNameOrPassword, FormatErrorMessage(request, ErrorTypes.InvalidUserNameOrPassword));
									Logger.Info(request.LogData(), string.Format("Unable to access EKOS for user '{0}'", request.UserName));
									return response;
								}

								#endregion
							}
							else
							{
								#region Check our db and then the client repository

								user = Repositories.Core.Users.FirstOrDefault(x => x.UserName == request.UserName);

								if (user.IsNull())
								{
									userTableChecked = true;

									#region Check for AssistUser in with the ClientService

									var getAssistUserResponse = Repositories.Integration.AuthenticateStaffUser(new AuthenticateStaffRequest { Username = request.UserName, Password = request.Password });

									if (getAssistUserResponse.Outcome == IntegrationOutcome.Failure)
									{
										response.SetFailure(ErrorTypes.InvalidUserNameOrPassword, FormatErrorMessage(request, ErrorTypes.InvalidUserNameOrPassword));
										Logger.Info(request.LogData(), string.Format("Unable to create user account for '{0}' without assist user data.", request.UserName));

										return response;
									}

									#endregion

									if (getAssistUserResponse.Outcome == IntegrationOutcome.Success)
									{
										#region Create AssistUser

										var userPerson = new PersonDto
											{
												FirstName = (getAssistUserResponse.StaffUserInfo.FirstName.IsNotNullOrEmpty()) ? getAssistUserResponse.StaffUserInfo.FirstName : "Undefined",
												LastName = (getAssistUserResponse.StaffUserInfo.Surname.IsNotNullOrEmpty()) ? getAssistUserResponse.StaffUserInfo.Surname : "Undefined",
												JobTitle = (getAssistUserResponse.StaffUserInfo.JobTitle.IsNotNullOrEmpty()) ? getAssistUserResponse.StaffUserInfo.JobTitle : "Undefined"
											};

										var model = new CreateAssistUserModel
											{
												AccountUserName = request.UserName,
												AccountPassword = request.Password,
												AccountExternalId = getAssistUserResponse.StaffUserInfo.ExternalId,
												UserPerson = userPerson,
												UserEmailAddress = getAssistUserResponse.StaffUserInfo.Email,
												UserPhone =
													(getAssistUserResponse.StaffUserInfo.PhoneNumber.IsNotNullOrEmpty())
														? getAssistUserResponse.StaffUserInfo.PhoneNumber
														: "Undefined",
												IsClientAuthenticated = false,
												IsEnabled = false,
												UserExternalOffice = "Undefined"
											};

										var accountRequest = new CreateAssistUserRequest
											{
												ClientTag = request.ClientTag,
												RequestId = request.RequestId,
												SessionId = request.SessionId,
												UserContext = request.UserContext,
												Models = new List<CreateAssistUserModel>
													{
														model
													}
											};

										var accountService = new AccountService();
										var accountResponse = accountService.CreateAssistUser(accountRequest);

										if (accountResponse.Acknowledgement == AcknowledgementType.Failure)
										{
											response.SetFailure(ErrorTypes.InvalidUserNameOrPassword, FormatErrorMessage(request, ErrorTypes.InvalidUserNameOrPassword));
											Logger.Info(request.LogData(), string.Format("Unable to create a Assist account for '{0}'", request.UserName));

											return response;
										}

										#endregion

										#region Email the user so they can validate their account

										user = Repositories.Core.Users.FirstOrDefault(x => x.UserName == request.UserName);

										SendAccountActivationEmail(request, user);

										response.SetFailure(Localise(request, "System.Email.AssistUserActivation.EmailWarning"));

										return response;

										#endregion
									}
								}

								#endregion
							}
						}
						else if (request.ModuleToLoginTo.IsIn(FocusModules.Explorer, FocusModules.CareerExplorer, FocusModules.Career))
						{
							if (AppSettings.IntegrationClient != IntegrationClient.Standalone)
							{
								user = Repositories.Core.Users.FirstOrDefault(x => x.UserName == request.UserName);

								if (user.IsNull())
								{
									// We may need to go to the client to get this user as it doesn't exist in Focus currently
									var authResponse = Repositories.Integration.AuthenticateJobSeeker(new AuthenticateJobSeekerRequest { UserName = request.UserName, Password = request.Password });
									if (authResponse.Outcome == IntegrationOutcome.Success && authResponse.JobSeeker.IsNotNull())
									{
										response.IsIntegrationUser = true;

										var jobSeeker = authResponse.JobSeeker;
										response.IntegrationDetails = new ValidatedJobSeekerView
										{
											ExternalId = jobSeeker.ExternalId,
											EmailAddress = jobSeeker.EmailAddress,
											FirstName = jobSeeker.FirstName,
											MiddleInitial = jobSeeker.MiddleInitial,
											LastName = jobSeeker.LastName,
											DateOfBirth = jobSeeker.DateOfBirth.GetValueOrDefault(DateTime.MinValue) != DateTime.MinValue ? jobSeeker.DateOfBirth : null,
											AddressLine1 = (jobSeeker.AddressLine1.IsNotNullOrEmpty() ? jobSeeker.AddressLine1 + Environment.NewLine : String.Empty) + jobSeeker.AddressLine2,
											AddressTownCity = jobSeeker.AddressTownCity,
											AddressStateId = jobSeeker.AddressStateId,
											AddressCountyId = jobSeeker.AddressCountyId,
											AddressCountryId = jobSeeker.AddressCountryId,
											AddressPostcodeZip = jobSeeker.AddressPostcodeZip,
											PrimaryPhone = jobSeeker.PrimaryPhone,
											SocialSecurityNumber = jobSeeker.SocialSecurityNumber
										};

										return response;
									}
								}
							}
						}

						#endregion
					}

					#region Validate against user table

					if (user.IsNull())
					{
						var userNameId = userTableChecked ? 0 : Repositories.Core.Users.Where(x => x.UserName == request.UserName).Select(x => x.Id).FirstOrDefault();
						if (userNameId != 0)
							userId = userNameId;
					}
					else
					{
						userId = user.Id;
					}

					// Does the user not exist?
					if (userId == -1)
					{
						//if (request.ModuleToLoginTo == FocusModules.CareerExplorer && AppSettings.Theme == FocusThemes.Education)
						//{
						//  // Check client repository
						//  if (CheckCareerClientRepository(request))
						//  {
						//    // User has been created but they need to validate their account so don't continue with the log in, just return
						//    return response;
						//  }
						//}

						// If this is Talent then check whether we haven't found the user because the username has been changed when denying the user
						if (request.ModuleToLoginTo == FocusModules.Talent)
						{
							if (Repositories.Core.Users.Any(u => u.UserType == UserTypes.Talent
							                                     && u.UserName.StartsWith("DENIED_")
							                                     && u.UserName.EndsWith("_" + request.UserName)
							                                     && u.Person.Employee != null
							                                     && u.Person.Employee.ApprovalStatus == ApprovalStatuses.Rejected))
							{
								response.SetFailure(ErrorTypes.EmployeeIsDenied, FormatErrorMessage(request, ErrorTypes.EmployeeIsDenied));
								Logger.Info(request.LogData(), string.Format("Talent user '{0}' has been denied", request.UserName));

								return response;
							}
						}

						response.SetFailure(ErrorTypes.InvalidUserNameOrPassword, FormatErrorMessage(request, ErrorTypes.InvalidUserNameOrPassword));
						Logger.Info(request.LogData(), string.Format("Unable to locate user using username '{0}'", request.UserName));

						return response;
					}

					user = Repositories.Core.FindById<User>(userId);

					#endregion

					#region Validate Talent user is not denied (must come before username and password)

					// Must come before username and password because denied Talent users have an altered username that will fail that validation.
					// This validation failure will give an invalid username or password error that is misleading in this situation.

					if (request.ModuleToLoginTo == FocusModules.Talent && user.Person.Employee.IsNotNull() && user.Person.Employee.ApprovalStatus == ApprovalStatuses.Rejected)
					{
						response.SetFailure(ErrorTypes.EmployeeIsDenied, FormatErrorMessage(request, ErrorTypes.EmployeeIsDenied));
						Logger.Info(request.LogData(), string.Format("Talent user '{0}' has been denied", request.UserName));

						return response;
					}

					#endregion

					#region Validate username and password

					if (!isSSO && !(authenticatedAssistViaClient && request.ModuleToLoginTo == FocusModules.Assist))
					{
						// Is the password valid
						if (user.MigratedPasswordHash.IsNullOrEmpty())
						{
							var password = new Password(AppSettings, request.Password, user.PasswordSalt, false);

							if (user.PasswordHash != password.Hash)
							{
								response.SetFailure(ErrorTypes.InvalidUserNameOrPassword, FormatErrorMessage(request, ErrorTypes.InvalidUserNameOrPassword));
								Logger.Info(request.LogData(), string.Format("Invalid password for user '{0}'", request.UserName));

								return response;
							}
						}
						else
						{
							if (user.MigratedPasswordHash != Password.GetMD5Hash(request.Password))
							{
								response.SetFailure(ErrorTypes.InvalidUserNameOrPassword, FormatErrorMessage(request, ErrorTypes.InvalidUserNameOrPassword));
								Logger.Info(request.LogData(), string.Format("Invalid password for user '{0}'", request.UserName));

								return response;
							}

							var password = new Password(AppSettings, request.Password, user.PasswordSalt, false);
							user.MigratedPasswordHash = null;
							user.PasswordHash = password.Hash;
						}
					}

					#endregion

					#region Validate the user isn't blocked

					if (user.Blocked)
					{
						response.SetFailure(ErrorTypes.UserBlocked, FormatErrorMessage(request, ErrorTypes.UserBlocked));
						Logger.Info(request.LogData(), string.Format("User '{0}' has been blocked", request.UserName));

						return response;
					}

					#endregion

					#region Validate the user is enabled

					if (!user.Enabled)
					{
						// FVN-328 User is inactive (A Jobseeker cannot be 'disabled' they can only be blocked or 'inactivated'.  The enabled flag is set to false when the jobseeker is inactived.)
						if ((user.UserType & UserTypes.Career) == UserTypes.Career && user.AccountDisabledReason.IsNotNull())
						{
							response.AccountReactivateRequest = true;
						}
						else if (!isSSO)
						{
							// Patch for Oklahoma as account verification emails being blocked RF 28.09.12
							user = Repositories.Core.Users.FirstOrDefault(x => x.UserName == request.UserName);

							Password password;

							try
							{
								password = new Password(AppSettings, request.Password, false);
							}
							catch (Exception)
							{
								response.SetFailure(ErrorTypes.InvalidPassword, FormatErrorMessage(request, ErrorTypes.InvalidPassword));
								Logger.Info(request.LogData(), "Invalid password.");

								return response;
							}

							if (user != null)
							{
								user.PasswordHash = password.Hash;
								user.PasswordSalt = password.Salt;
							}

							Repositories.Core.SaveChanges();

							// Removed due to change so that user has to request activation email resend (BZ75)
							// SendAccountActivationEmail(request, user);

							response.SetFailure(ErrorTypes.UserNotEnabled, FormatErrorMessage(request, ErrorTypes.UserNotEnabled));
							Logger.Info(request.LogData(), string.Format("User '{0}' is not enabled", request.UserName));

							return response;
						}
					}

					#endregion

					#region Validate the user can log into the module

					if (!UserHasAccessToModule(user, request.ModuleToLoginTo))
					{
						response.SetFailure(ErrorTypes.InvalidUserTypeForModule, FormatErrorMessage(request, ErrorTypes.InvalidUserTypeForModule));
						Logger.Info(request.LogData(), string.Format("User '{0}' isn't a valid {1} user.  It is a {2} user.", request.UserName, request.ModuleToLoginTo, user.UserType));
						return response;
					}

					#endregion

					#region For Talent Users, check employer is waiting approval

					if (request.ModuleToLoginTo == FocusModules.Talent && !(isSSO && user.Person.Employee == null))
					{
						var employee = user.Person.Employee;
						var employerId = employee.EmployerId;
						var employer = Repositories.Core.Employers.First(e => e.Id == employerId);
						var businessunit = employee.EmployeeBusinessUnits.FirstOrDefault(b => b.EmployeeId == employee.Id);

						if (employer.ApprovalStatus == ApprovalStatuses.WaitingApproval && AppSettings.NewEmployerApproval == TalentApprovalOptions.ApprovalSystemNotAvailable)
						{
							response.SetFailure(ErrorTypes.EmployerNotApproved, FormatErrorMessage(request, ErrorTypes.EmployerNotApproved));
							Logger.Info(request.LogData(), string.Format("Employer '{0}' is not approved", request.UserName));

							return response;
						}
						// Commented out - See FVN-3690
						// else if (employer.ApprovalStatus == ApprovalStatuses.OnHold)
						// {
						// 	 response.SetFailure(ErrorTypes.EmployerOnHold, FormatErrorMessage(request, ErrorTypes.EmployerOnHold));
						// 	 Logger.Info(request.LogData(), string.Format("Employer '{0}' is on hold", request.UserName));
						//	 return response;
						// }
						else if (businessunit.BusinessUnit.ApprovalStatus == ApprovalStatuses.WaitingApproval && AppSettings.NewBusinessUnitApproval == TalentApprovalOptions.ApprovalSystemNotAvailable)
						{
							response.SetFailure(ErrorTypes.EmployerNotApproved, FormatErrorMessage(request, ErrorTypes.EmployerNotApproved));
							Logger.Info(request.LogData(), string.Format("Business unit '{0}' is not approved", request.UserName));
							return response;

						}
						else if (employee.ApprovalStatus == ApprovalStatuses.WaitingApproval && AppSettings.NewHiringManagerApproval == TalentApprovalOptions.ApprovalSystemNotAvailable)
						{
							response.SetFailure(ErrorTypes.EmployerNotApproved, FormatErrorMessage(request, ErrorTypes.EmployerNotApproved));
							Logger.Info(request.LogData(), string.Format("Hiring Manager '{0}' is not approved", request.UserName));
							return response;
						}
					}

					#endregion

					var newSession = false;

					try
					{
						Helpers.Session.ValidateSession(request.SessionId, user.Id);
					}
					catch (Exception ex)
					{
						if (!request.AllowResetSessionId)
						{
							response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
							Logger.Error(request.LogData(), response.Message, response.Exception);
							return response;
						}

						Logger.Error(request.LogData(), FormatErrorMessage(request, ErrorTypes.InvalidSessionId), ex);
						newSession = true;
					}

					if (newSession)
					{
						try
						{
							var newSessionId = Helpers.Session.CreateSession();
							response.ResetSessionId = request.SessionId = newSessionId;
						}
						catch (Exception ex)
						{
							response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
							Logger.Error(request.LogData(), response.Message, response.Exception);
							return response;
						}
					}

					// Re-get the user because the ValidateSession method does SaveChanges which clears down the entities from Lightspeed, so lightspeed no longer tracks them unless they are read from the DB again
					user = Repositories.Core.FindById<User>(user.Id);

					if (request.ModuleToLoginTo == FocusModules.Assist && !authenticatedAssistViaClient && AppSettings.AuthenticateAssistUserViaClient)
					{
						if (user.ExternalId.IsNotNullOrEmpty())
						{
							externalUserId = user.ExternalId;
							externalUserName = user.UserName;
							externalPassword = request.Password;
							externalOfficeId = (from pco in Repositories.Core.PersonsCurrentOffices
																	join o in Repositories.Core.Offices
																	 on pco.OfficeId equals o.Id
																	where pco.PersonId == user.PersonId
																	orderby pco.StartTime descending
																	select o.ExternalId).FirstOrDefault();

							if (externalOfficeId.IsNullOrEmpty())
							{
								externalOfficeId = Repositories.Core.Offices.Where(o => (o.DefaultType & OfficeDefaultType.JobSeeker) == OfficeDefaultType.JobSeeker)
																														.Select(o => o.ExternalId)
																														.FirstOrDefault();
							}
						}
					}

					if (user.Person.IsNotNull())
					{
						response.FirstName = user.Person.FirstName;
						response.LastName = user.Person.LastName;
						response.EmailAddress = user.Person.EmailAddress;
						response.PersonId = user.Person.Id;
						if (user.Person.Employee.IsNotNull())
						{
							response.EmployerId = user.Person.Employee.EmployerId;
							response.EmployeeId = user.Person.Employee.Id;
						}

						if (user.UserType != UserTypes.Assist)
						{
							var postCodeZip = user.Person.PersonAddresses.IsNotNullOrEmpty()
								? user.Person.PersonAddresses.Any(pa => pa.IsPrimary)
									? user.Person.PersonAddresses.First(pa => pa.IsPrimary).PostcodeZip
									: user.Person.PersonAddresses[0].PostcodeZip
								: string.Empty;
							if (postCodeZip.IsNotNullOrEmpty())
							{
								var lookup =
									Repositories.Configuration.ExternalLookUpItems.FirstOrDefault(
										e => e.ExternalLookUpType == ExternalLookUpType.IntegrationOfficeIdPerZip && e.InternalId == postCodeZip);
								if (lookup.IsNotNull())
								{
									externalOfficeId = lookup.ExternalId;
									externalUserName = string.Format("selfreg{0}", externalOfficeId.ToLower());
								}
							}

							if (externalOfficeId.IsNullOrEmpty())
							{
								if (user.Person.Employee.IsNotNull())
								{
									if (user.Person.Employee.Employer.IsNotNull() &&
											user.Person.Employee.Employer.EmployerOfficeMappers.IsNotNullOrEmpty() &&
											user.Person.Employee.Employer.EmployerOfficeMappers[0].Office.IsNotNull() &&
											user.Person.Employee.Employer.EmployerOfficeMappers[0].Office.ExternalId.IsNotNullOrEmpty())
									{
										externalOfficeId = user.Person.Employee.Employer.EmployerOfficeMappers[0].Office.ExternalId;
										externalUserName = string.Format("selfreg{0}", externalOfficeId.ToLower());
									}
								}

								if (user.Person.PersonOfficeMappers.IsNotNullOrEmpty() &&
										user.Person.PersonOfficeMappers[0].IsNotNull() &&
										user.Person.PersonOfficeMappers[0].Office.IsNotNull() &&
										user.Person.PersonOfficeMappers[0].Office.ExternalId.IsNotNullOrEmpty())
								{
									externalOfficeId = user.Person.PersonOfficeMappers[0].Office.ExternalId;
									externalUserName = string.Format("selfreg{0}", externalOfficeId.ToLower());
								}
							}
						}
					}

					response.UserId = user.Id;
					response.IsEnabled = user.Enabled;
					response.ScreenName = user.ScreenName;

					// Set the external ids
					response.ExternalUserId = externalUserId;
					response.ExternalUserName = externalUserName;
					response.ExternalPassword = externalPassword;
					response.ExternalOfficeId = externalOfficeId;

					// Set the user name
					response.UserName = request.UserName;
					response.IsMigrated = user.IsMigrated;
					response.IsEnabled = user.Enabled;
					response.RegulationsConsent = user.RegulationsConsent;
					if (request.ModuleToLoginTo.IsIn(FocusModules.Career, FocusModules.CareerExplorer))
					{
						#region Retrieve data required for App.UserData

						UpdateCareerUserData(user, request.UserContext.Culture, response);

						response.IsMigrated = user.IsMigrated;

						#endregion

						response.ProgramAreaId = user.Person.ProgramAreaId;
						response.DegreeId = user.Person.DegreeId;
						response.EnrollmentStatus = user.Person.EnrollmentStatus;
						response.CampusId = user.Person.CampusId;

						// Log login as self service
						Helpers.SelfService.PublishSelfServiceActivity(ActionTypes.LogIn, user.Id, jobSeekerId: user.PersonId);

						if (AppSettings.IntegrationClient != IntegrationClient.Standalone)
						{
							// Required for WI
							var integrationRequest = new IntegrationRequestMessage
							{
								ActionerId = request.UserContext.UserId,
								IntegrationPoint = IntegrationPoint.JobSeekerLogin,
								JobSeekerLoginRequest = new JobSeekerLoginRequest
								{
									UserId = request.UserContext.UserId,
									PersonId = user.PersonId,
									Active = true
								}
							};

							Helpers.Messaging.Publish(integrationRequest);
						}
					}

					if (user.LastLoggedInOn == null && user.LoggedInOn == null)
						user.FirstLoggedInOn = DateTime.Now;

					// Update the LoggedInOn and LastLoggedInOn UpdateSession will commit to database
					user.LastLoggedInOn = user.LoggedInOn;
					user.LoggedInOn = DateTime.Now;
					user.PasswordAttempts = 0;

					response.LastLoggedInOn = user.LastLoggedInOn;

					Helpers.Session.UpdateSession(request.SessionId, user.Id);

					// Log the action
					LogAction(request, ActionTypes.LogIn, typeof(User).Name, user.Id);
				}
				catch (Exception ex)
				{
					response.SetFailure(ErrorTypes.InvalidUserNameOrPassword, FormatErrorMessage(request, ErrorTypes.InvalidUserNameOrPassword), ex);
					Logger.Error(request.LogData(), ex.Message, ex);
				}

				return response;
			}
		}
		*/

		/// <summary>
		/// Gets the career user data for the authenticated user
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public CareerUserDataResponse GetCareerUserData(CareerUserDataRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new CareerUserDataResponse(request);

				// Validate client tag & security token only
				if (!ValidateRequest(request, response, Validate.ClientTag | Validate.SessionId | Validate.License | Validate.AccessToken))
					return response;

				try
				{
					// Get the user
					var user = Repositories.Core.FindById<User>(request.UserContext.UserId);

					UpdateCareerUserData(user, request.UserContext.Culture, response);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.UnableToGetCareerData), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Updates are career user data for the user
		/// </summary>
		/// <param name="user">The authenticated user whose career data is required</param>
		/// <param name="culture">The current culture.</param>
		/// <param name="response">The response.</param>
		private void UpdateCareerUserData(User user, string culture, ICareerUserData response)
		{
			var defaultResume = user.Person.Resumes.Where(x => x.IsDefault && x.StatusId == ResumeStatuses.Active).Select(x => x.AsDto()).FirstOrDefault();

			if (defaultResume.IsNotNull())
			{
				var resumeModel = defaultResume.ToResumeModel(RuntimeContext, culture);
				response.DefaultResumeId = defaultResume.Id;
				response.DefaultResumeCompletionStatus = (defaultResume.ResumeCompletionStatusId.HasValue) ? defaultResume.ResumeCompletionStatusId.GetValueOrDefault() : ResumeCompletionStatuses.None;
				response.Profile = resumeModel.ResumeContent.Profile;
			}
			else
			{
				if (response.Profile.IsNull())
					response.Profile = new UserProfile();

				response.Profile.EmailAddress = user.Person.EmailAddress;
				response.Profile.UserGivenName = new Name { FirstName = user.Person.FirstName, LastName = user.Person.LastName };

				var address = Repositories.Core.PersonAddresses.FirstOrDefault(x => x.PersonId == user.Person.Id);

				if (address.IsNotNull())
					response.Profile.PostalAddress = new Address
					{
						Street1 = address.Line1,
						Street2 = address.Line2,
						City = address.TownCity,
						CountyId = address.CountyId,
						StateId = address.StateId,
						CountryId = address.CountryId,
						Zip = address.PostcodeZip
					};

				var phone = Repositories.Core.PhoneNumbers.FirstOrDefault(x => x.PersonId == user.Person.Id && x.IsPrimary);

				if (phone.IsNotNull())
					response.Profile.PrimaryPhone = new Phone { PhoneNumber = phone.Number, Extention = phone.Extension };
			}
		}

		/// <summary>
		/// Sends account activation email
		/// </summary>
		/// <param name="request">The request.</param>
		/// <param name="user">The user.</param>
		/// <returns></returns>
		private void SendAccountActivationEmail(LogInRequest request, User user)
		{
			var singleSignOn = CreateSingleSignOn(request.UserContext.UserId, user.Id);

			var validationUrl = string.Format(request.ValidationUrl, singleSignOn.Id);

			switch (AppSettings.Module)
			{
				case FocusModules.Assist:
					Helpers.Email.SendEmail(user.Person.EmailAddress, "", "",
						Localise(request, "System.Email.AssistUserActivation.Subject"),
						Localise(request, "System.Email.AssistUserActivation.Body", user.Person.FirstName, validationUrl), true);
					break;

				case FocusModules.Talent:
					Helpers.Email.SendEmail(user.Person.EmailAddress, "", "",
						Localise(request, "System.Email.TalentUserActivation.Subject"),
						Localise(request, "System.Email.TalentUserActivation.Body", user.Person.FirstName, validationUrl), true);
					break;

				case FocusModules.Career:
				case FocusModules.CareerExplorer:
					Helpers.Email.SendEmail(user.Person.EmailAddress, "", "",
						Localise(request, "System.Email.CareerUserActivation.Subject"),
						Localise(request, "System.Email.CareerUserActivation.Body", user.Person.FirstName, validationUrl, request.Password), true);
					break;
			}
		}

		/// <summary>
		/// Logs the user out.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public LogOutResponse LogOut(LogOutRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new LogOutResponse(request);

				// Validate client tag & session only
				if (!ValidateRequest(request, response, Validate.ClientTag | Validate.SessionId | Validate.AccessToken))
					return response;

				try
				{
					// Get the user
					var user = Repositories.Core.FindById<User>(request.UserContext.UserId);

					// Do some cleanup of the user account ??

					// Log the action
					LogAction(request, ActionTypes.LogOut, user);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Processes a forgotten password request for the user.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public ProcessForgottenPasswordResponse ProcessForgottenPassword(ProcessForgottenPasswordRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new ProcessForgottenPasswordResponse(request);

				// Validate client tag & security token only
				if (!ValidateRequest(request, response, Validate.ClientTag | Validate.SessionId | Validate.License | Validate.AccessToken))
					return response;

				try
				{
					// Username or Email Address must be supplied
					if (request.UserName.IsNullOrEmpty() && request.EmailAddress.IsNullOrEmpty())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.MustSupplyUserNameOrEmailAddress));
						Logger.Info(request.LogData(),
							"Unable to process forgotten password as no username or email address was supplied.");

						return response;
					}

					// Work out the user type we are processing forgotten password for
					var userType = GetUserTypeForModule(request.Module);

					// Get the user by username or if that returns no results email address
					var user = request.UserName.IsNotNullOrEmpty()
						? Repositories.Core.Users.FirstOrDefault(x => x.UserName == request.UserName && x.UserType == userType)
						: Repositories.Core.Users.FirstOrDefault(x => x.Person.EmailAddress == request.EmailAddress && x.UserType == userType);

					// If no user found, and this is Career/Explorer, attempt to send email with original PIN
					if (user == null && request.Module.IsIn(FocusModules.Career, FocusModules.CareerExplorer, FocusModules.Explorer) && AppSettings.Theme == FocusThemes.Education)
					{
						var pinRecord = Repositories.Core.RegistrationPins.FirstOrDefault(pin => pin.EmailAddress == request.EmailAddress);
						if (pinRecord.IsNotNull())
						{
							var template = new EmailTemplateData
							{
								EmailAddress = request.EmailAddress,
								PinNumber = pinRecord.Pin,
								AuthenticateUrl = string.Format(request.PinRegistrationUrl, pinRecord.Pin)
							};
							var preview = Helpers.Email.GetEmailTemplatePreview(EmailTemplateTypes.CareerAccountRegistration, template);

							Helpers.Email.SendEmail(request.EmailAddress, "", "", preview.Subject, preview.Body, true);

							return response;
						}
					}

					if (user == null)
					{
						response.SetFailure(ErrorTypes.UnableToProcessForgottenPassword, FormatErrorMessage(request, ErrorTypes.UnableToProcessForgottenPassword));
						Logger.Info(request.LogData(),
							string.Format(
								"Unable to process forgotten password for username / email address '{0}' as was unable to locate user.",
								request.UserContext.UserId));

						return response;
					}

					if (!user.Enabled && !user.AccountDisabledReason.IsIn(AccountDisabledReason.Inactivity, AccountDisabledReason.InactivatedByStaffRequest))
					{
						response.SetFailure(ErrorTypes.UserNotEnabled, FormatErrorMessage(request, ErrorTypes.UserNotEnabled));
						Logger.Info(request.LogData(), "Inactive user attempting to reset their password.");
						return response;
					}

					if (user.Blocked)
					{
						response.SetFailure(ErrorTypes.UserBlocked, FormatErrorMessage(request, ErrorTypes.UserBlocked));
						Logger.Info(request.LogData(), "Blocked user attempting to reset their password.");
						return response;
					}

					user.ValidationKey = GenerateRandomHash();
					user.ValidationKeyExpiry = DateTime.Now.AddHours(AppSettings.PasswordValidationKeyExpiryHours);
					user.UserValidationKeyLogs.Add(new UserValidationKeyLog
					{
						Status = UserValidationKeyStatus.Sent,
						ValidationKey = user.ValidationKey,
						LogDate = DateTime.Now
					});

					var person = user.Person;
					var userEmail = person.EmailAddress;

					var userSecurityQuestion1 = Repositories.Core.UserSecurityQuestions.FirstOrDefault(usq => usq.UserId == user.Id && usq.QuestionIndex == 1);
					var userSecurityQuestion = userSecurityQuestion1.IsNull() || userSecurityQuestion1.SecurityQuestion.IsNullOrWhitespace() ? null : userSecurityQuestion1.SecurityQuestion;
					var userSecurityQuestionId = userSecurityQuestion1.IsNull() ? null : userSecurityQuestion1.SecurityQuestionId;
					var userSecurityAnswerEncrypted = userSecurityQuestion1.IsNull() || userSecurityQuestion1.SecurityAnswerEncrypted.IsNullOrWhitespace() ? null : userSecurityQuestion1.SecurityAnswerEncrypted;
					var userSecurityAnswerHash = userSecurityQuestion1.IsNull() || userSecurityQuestion1.SecurityAnswerHash.IsNullOrWhitespace() ? null : userSecurityQuestion1.SecurityAnswerHash;

					if (request.SecurityQuestion.IsNotNullOrEmpty() || request.SecurityQuestionId.HasValue)
					{
						var failed = (userSecurityQuestion != request.SecurityQuestion && userSecurityQuestionId != request.SecurityQuestionId)
						             || (person.DateOfBirth.HasValue && person.DateOfBirth.Value.Day != request.DayOfBirth)
						             || (person.DateOfBirth.HasValue && person.DateOfBirth.Value.Month != request.MonthOfBirth);

						if (!failed && userSecurityAnswerEncrypted.IsNotNull())
						{
							var securityAnswer = Helpers.Encryption.Decrypt(userSecurityAnswerEncrypted, TargetTypes.SecurityAnswer, EntityTypes.User, user.Id);
							failed = securityAnswer != request.SecurityAnswer;
						}
						else if (!failed && userSecurityAnswerHash.IsNotNull())
						{
							failed = request.SecurityAnswer.GenerateHash(user.PasswordSalt) != userSecurityAnswerHash;
						}

						if (failed)
						{
							user.PasswordAttempts += 1;
							response.AttemptsLeft = Defaults.ConfigurationItemDefaults.MaximumFailedPasswordAttempts - user.PasswordAttempts;
							response.AttemptsFailed = true;

							if (user.PasswordAttempts == Defaults.ConfigurationItemDefaults.MaximumFailedPasswordAttempts)
							{
								user.Blocked = true;
								user.BlockedReason = BlockedReason.FailedPasswordReset;
							}

							Repositories.Core.SaveChanges();

							LogAction(request, ActionTypes.FailedPasswordReset, user);

							if (user.Blocked)
							{
								LogAction(request, ActionTypes.BlockUser, typeof(Person).Name, user.PersonId, user.Id, (long)BlockedReason.FailedPasswordReset, overrideUserId: user.Id);

								response.SetFailure(ErrorTypes.ValidationFailed, FormatErrorMessage(request, ErrorTypes.ValidationFailed));
								Logger.Info(request.LogData(), "User's security details could not be validated.");
							}

							return response;
						}

						user.PasswordAttempts = 0;
						Repositories.Core.SaveChanges();
					}

					if (userEmail.IsNullOrEmpty())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.UserEmailAddressNotFound));
						Logger.Info(request.LogData(), "User email address not found.");

						return response;
					}

					var resetPasswordUrl = string.Format("{0}?{1}", request.ResetPasswordUrl, HttpUtility.UrlEncode(user.ValidationKey));

					if (request.Module == FocusModules.Talent)
					{
						var templateValues = new EmailTemplateData
						{
							Url = resetPasswordUrl
						};
						var emailTemplate = Helpers.Email.GetEmailTemplatePreview(EmailTemplateTypes.TalentPasswordReset, templateValues);

						Helpers.Email.SendEmail(userEmail, "", "", emailTemplate.Subject, emailTemplate.Body, true, detectUrl: true);
					}
					else if (request.Module == FocusModules.Assist)
					{
						Helpers.Email.SendEmail(userEmail, "", "",
							Localise(request, "System.Email.ResetPassword.Subject"),
							Localise(request, "System.Email.ResetPassword.Body", person.FirstName, resetPasswordUrl),
							true,
							detectUrl: true);
					}
					else
					{
						var template = new EmailTemplateData
						{
							EmailAddress = request.EmailAddress,
							Url = resetPasswordUrl,
							RecipientName = person.FirstName
						};
						var preview = Helpers.Email.GetEmailTemplatePreview(EmailTemplateTypes.CareerPasswordReset, template);

						Helpers.Email.SendEmail(userEmail, "", "", preview.Subject, preview.Body, true, detectUrl: true);
					}
					Repositories.Core.SaveChanges();

					// Log the action
					LogAction(request, ActionTypes.ProcessForgottenPassword, user);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.UnableToProcessForgottenPassword), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		public string CheckUserHasASavedQuestion(string username, string emailAddress)
		{
			if (string.IsNullOrWhiteSpace(username) && string.IsNullOrWhiteSpace(emailAddress)) throw new ArgumentException("Username and EmailAddress cannot both be null");

			User user = Repositories.Core.Users.FirstOrDefault(u => u.UserName == username);

			if (user != null)
			{
				var person =
					Repositories.Core.Persons.Where(p => user != null && (p.EmailAddress == emailAddress || p.Id == user.PersonId));
				var userSecurityQuestion =
					Repositories.Core.UserSecurityQuestions.FirstOrDefault(q => user != null && q.UserId == user.Id);
				CodeItem codeItem =
					Repositories.Configuration.CodeItems.FirstOrDefault(
						x => userSecurityQuestion != null && x.Id == userSecurityQuestion.SecurityQuestionId);

				var question =
					Repositories.Configuration.LocalisationItems.FirstOrDefault(x => codeItem != null && x.Key == codeItem.Key);

				if (question != null)
				{
					return question.Value;
				}
			}
			throw new InvalidDataException("User or security question was not found");
		}

		/// <summary>
		/// Creates the single sign on.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public CreateSingleSignOnResponse CreateSingleSignOn(CreateSingleSignOnRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new CreateSingleSignOnResponse(request);

				// Validate all
				if (!ValidateRequest(request, response, Validate.ClientTag | Validate.SessionId | Validate.License | Validate.AccessToken))
					return response;

				try
				{
					long? accountUserId = null;

					if (request.AccountUserId.HasValue) accountUserId = request.AccountUserId.Value;
					else if (request.AccountEmployeeId.HasValue)
					{
						var employee = Repositories.Core.FindById<Employee>(request.AccountEmployeeId.Value);
						if (employee.IsNull())
						{
							response.SetFailure(FormatErrorMessage(request, ErrorTypes.UnableToCreateSingleSignIn));
							Logger.Info(request.LogData(), String.Format("Unable to create single sign in for employee '{0}' as was unable to locate employee.", request.AccountEmployeeId));
							return response;
						}

						accountUserId = employee.Person.User.Id;
					}
					else if (request.AccountPersonId.HasValue)
					{
						accountUserId = Repositories.Core.Persons.Where(x => x.Id == request.AccountPersonId).Select(x => x.User.Id).FirstOrDefault();

						if (accountUserId.IsNull() || accountUserId == 0)
						{
							response.SetFailure(FormatErrorMessage(request, ErrorTypes.UnableToCreateSingleSignIn));
							Logger.Info(request.LogData(), String.Format("Unable to create single sign in for person '{0}' as was unable to locate person.", request.AccountPersonId));
							return response;
						}
					}
					else if (request.UserName.IsNotNullOrEmpty())
					{
						var user = Repositories.Core.Users.FirstOrDefault(x => x.UserName == request.UserName);
						if (user.IsNotNull())
							accountUserId = user.Id;
					}

					if (!accountUserId.HasValue)
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.UnableToCreateSingleSignIn));
						Logger.Info(request.LogData(), "Unable to create single sign in as no user or employee id found.");
						return response;
					}

					// Validate Account To Sign Into
					var userIdValidationResponse = ValidateUserId(accountUserId.Value);

					// Allow account not enabled for creating SSO (User needs SSO to enable their account)
					if (!userIdValidationResponse.Valid && userIdValidationResponse.ValidationFailureReason != UserIdValidationFailureReasons.AccountNotEnabled)
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.UnableToCreateSingleSignInInvalidAccount));
						Logger.Info(request.LogData(), String.Format("Unable to create single sign in for user '{0}' as was unable to validate user. ({1}).", accountUserId.Value, userIdValidationResponse.ValidationFailureReason));
						return response;
					}

					// Create SingleSignOn

					var singleSignOn = CreateSingleSignOn(request.UserContext.UserId, accountUserId.Value);

					response.SingleSignOnKey = singleSignOn.Id;

					// Log the action
					LogAction(request, ActionTypes.CreateSingleSignOn, typeof(User).Name, accountUserId.Value);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.UnableToCreateSingleSignIn), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Validates the single sign on.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public ValidateSingleSignOnResponse ValidateSingleSignOn(ValidateSingleSignOnRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new ValidateSingleSignOnResponse(request);

				// Validate client tag & security token only
				if (!ValidateRequest(request, response, Validate.ClientTag | Validate.SessionId | Validate.License | Validate.AccessToken))
					return response;

				try
				{
					// Validate guid / Fetch record
					if (request.SingleSignOnKey.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.UnableToValidateSingleSignIn));
						Logger.Info(request.LogData(), "Unable to validate single sign in as no key.");
						return response;
					}

					var singleSignOn = Repositories.Core.FindById<SingleSignOn>(request.SingleSignOnKey);

					if (singleSignOn.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.UnableToValidateSingleSignIn));
						Logger.Info(request.LogData(), String.Format("Unable to validate single sign in as no single sign on found for '{0}'.", request.SingleSignOnKey));
						return response;
					}

					// Validate returned user id
					var userIdValidationResponse = ValidateUserId(singleSignOn.UserId);
					if (!userIdValidationResponse.Valid && userIdValidationResponse.ValidationFailureReason != UserIdValidationFailureReasons.AccountNotEnabled)
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.UnableToValidateSingleSignIn));
						Logger.Info(request.LogData(), String.Format("Unable to validate single sign in {0} as was unable to validate user '{1}'. ({2}).", singleSignOn.Id, singleSignOn.UserId, userIdValidationResponse.ValidationFailureReason));
						return response;
					}

					// Validate returned actioner id (unless 0 in which case this is registration validation)
					if (singleSignOn.ActionerId != 0)
					{
						var actionerIdValidationResponse = ValidateUserId(singleSignOn.ActionerId);
						if (!actionerIdValidationResponse.Valid)
						{
							response.SetFailure(FormatErrorMessage(request, ErrorTypes.UnableToValidateSingleSignIn));
							Logger.Info(request.LogData(),
								String.Format(
									"Unable to validate single sign in {0} as was unable to validate actioner '{1}'. ({2}).",
									singleSignOn.Id, singleSignOn.ActionerId,
									actionerIdValidationResponse.ValidationFailureReason));
							return response;
						}
					}

					var user = Repositories.Core.FindById<User>(singleSignOn.UserId);
					response.ExternalAdminId = singleSignOn.ExternalAdminId;
					response.ExternalOfficeId = singleSignOn.ExternalOfficeId;
					response.ExternalPassword = singleSignOn.ExternalPassword;
					response.ExternalUsername = singleSignOn.ExternalUsername;

					// Log user in
					response.UserId = user.Id;
					response.ActionerId = singleSignOn.ActionerId;
					response.FirstName = user.Person.FirstName;
					response.LastName = user.Person.LastName;

					// Get the primary email or any possible email
					response.EmailAddress = user.Person.EmailAddress;

					// Set PersonId
					response.PersonId = user.Person.Id;

					// Set EmployerId and employeeid if we have one
					if (user.Person.Employee.IsNotNull())
					{
						response.EmployerId = user.Person.Employee.EmployerId;
						response.EmployeeId = user.Person.Employee.Id;
					}

					response.ScreenName = user.ScreenName;

					if (AppSettings.Module == FocusModules.Career || AppSettings.Module == FocusModules.CareerExplorer)
					{
						#region retrieve data required for App.UserData

						UpdateCareerUserData(user, request.UserContext.Culture, response);

						#endregion

						if (AppSettings.Theme == FocusThemes.Education)
						{
							response.ProgramAreaId = user.Person.ProgramAreaId;
							response.DegreeId = user.Person.DegreeId;
							response.EnrollmentStatus = user.Person.EnrollmentStatus;
							response.CampusId = user.Person.CampusId;
						}
					}

					response.UserName = user.UserName;

					// NOTE: Update to LastLoggedInOn & LoggedInOn removed so that User table reflects last time actual user logged in

					// Update the LoggedInOn and LastLoggedInOn UpdateSession will commit to database
					// Maintain change to LastLoggedInOn so that Assist User sees what user would see in terms of info since last log in.
					user.LastLoggedInOn = user.LoggedInOn;

					Helpers.Session.UpdateSession(request.SessionId, user.Id);

					// Delete SingleSignOn
					Repositories.Core.Remove(singleSignOn);

					Repositories.Core.SaveChanges(true);

					// Log the action
					LogAction(request, ActionTypes.ValidateSingleSignOn, typeof(User).Name, singleSignOn.UserId, singleSignOn.ActionerId);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.UnableToValidateSingleSignIn), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Validates whether (encrypted) user details exist in an external system
		/// </summary>
		/// <param name="request"></param>
		/// <returns></returns>
		public ExternalAuthenticationResponse ValidateExternalAuthentication(ExternalAuthenticationRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new ExternalAuthenticationResponse(request);

				// Validate client tag & security token only
				if (!ValidateRequest(request, response, Validate.ClientTag | Validate.SessionId | Validate.License | Validate.AccessToken))
					return response;

				try
				{
					JobSeekerModel jobSeeker = null;

					if (request.AuthenticationDetails.IsNotNullOrEmpty())
					{
						var authRequest = new AuthenticateJobSeekerRequest
						{
							AuthenticationDetails = request.AuthenticationDetails
						};
						var authResponse = Repositories.Integration.AuthenticateJobSeeker(authRequest);

						if (authResponse.Outcome == IntegrationOutcome.Success && authResponse.JobSeeker.IsNotNull())
							jobSeeker = authResponse.JobSeeker;
					}
					else if (request.ExternalId.IsNotNullOrEmpty() || request.SocialSecurityNumber.IsNotNullOrEmpty())
					{
						var checkEkos = true;

						if (request.CheckFocus)
						{
							if (request.ExternalId.IsNotNullOrEmpty())
							{
								var userId = Repositories.Core.Users.Where(u => u.ExternalId == request.ExternalId).Select(u => u.Id).ToList().FirstOrDefault();
								if (userId > 0)
									checkEkos = false;
							}
							else
							{
								var personId = Repositories.Core.Persons.Where(p => p.SocialSecurityNumber == request.SocialSecurityNumber).Select(p => p.Id).ToList().FirstOrDefault();
								if (personId > 0)
									checkEkos = false;
							}
						}

						if (checkEkos)
						{
							var getRequest = new GetJobSeekerRequest
							{
								JobSeeker = new JobSeekerModel
								{
									ExternalId = request.ExternalId,
									SocialSecurityNumber = request.SocialSecurityNumber
								}
							};
							var getResponse = Repositories.Integration.GetJobSeeker(getRequest);

							if (getResponse.Outcome == IntegrationOutcome.Success && getResponse.JobSeeker.IsNotNull())
								jobSeeker = getResponse.JobSeeker;
						}
					}

					if (jobSeeker.IsNotNull())
					{
						var focusUserName = Repositories.Core.Users.Where(u => u.ExternalId == jobSeeker.ExternalId)
							.Select(u => u.UserName)
							.FirstOrDefault();

						response.ExternalsDetails = new ValidatedUserView
						{
							ExternalId = jobSeeker.ExternalId,
							EmailAddress = jobSeeker.EmailAddress,
							FirstName = jobSeeker.FirstName,
							MiddleInitial = jobSeeker.MiddleInitial,
							LastName = jobSeeker.LastName,
							DateOfBirth = jobSeeker.DateOfBirth.GetValueOrDefault(DateTime.MinValue) != DateTime.MinValue ? jobSeeker.DateOfBirth : null,
							AddressLine1 = (jobSeeker.AddressLine1.IsNotNullOrEmpty() ? jobSeeker.AddressLine1 + Environment.NewLine : String.Empty) + jobSeeker.AddressLine2,
							AddressTownCity = jobSeeker.AddressTownCity,
							AddressStateId = jobSeeker.AddressStateId,
							AddressCountyId = jobSeeker.AddressCountyId,
							AddressCountryId = jobSeeker.AddressCountryId,
							AddressPostcodeZip = jobSeeker.AddressPostcodeZip,
							PrimaryPhone = jobSeeker.PrimaryPhone,
							SocialSecurityNumber = jobSeeker.SocialSecurityNumber,
							FocusUserName = focusUserName
						};
					}

					return response;
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.UnableToAuthenticateExternally), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Validates the password reset validation key.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public ValidatePasswordResetValidationKeyResponse ValidatePasswordResetValidationKey(ValidatePasswordResetValidationKeyRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new ValidatePasswordResetValidationKeyResponse(request);

				// Validate client tag
				if (!ValidateRequest(request, response, Validate.ClientTag | Validate.License | Validate.AccessToken))
					return response;

				try
				{
					// Validate Key is not provided
					if (request.ValidationKey.IsNullOrEmpty())
					{
						response.SetFailure(ErrorTypes.UserValidationKeyNotProvided, FormatErrorMessage(request, ErrorTypes.UserValidationKeyNotProvided));
						Logger.Info(request.LogData(), "Unable to validate password reset validation key as no key provided");
						return response;
					}

					var user = Repositories.Core.Users.SingleOrDefault(x => x.ValidationKey == request.ValidationKey);

					// Log the validation of the key
					Repositories.Core.Add(new UserValidationKeyLog
					{
						UserId = user.IsNull() ? (long?)null : user.Id,
						Status = UserValidationKeyStatus.Validated,
						ValidationKey = request.ValidationKey,
						LogDate = DateTime.Now
					});

					if (user.IsNull())
					{
						var keyLog = Repositories.Core.Query<UserValidationKeyLog>().FirstOrDefault(x => x.ValidationKey == request.ValidationKey && x.UserId != null);

						// Check if key has ever been sent out
						if (keyLog.IsNotNull())
						{
							response.SetFailure(ErrorTypes.UserValidationKeyAlreadyUsed, FormatErrorMessage(request, ErrorTypes.UserValidationKeyAlreadyUsed));
							Logger.Info(request.LogData(), "Unable to validate password reset validation key as it has already been used");
							return response;
						}

						response.SetFailure(ErrorTypes.UserValidationKeyNotFound, FormatErrorMessage(request, ErrorTypes.UserValidationKeyNotFound));
						Logger.Info(request.LogData(), "Unable to validate password reset validation key as no user found for supplied key");
						return response;
					}

					// Check expiry date
					if (user.ValidationKeyExpiry <= DateTime.Now)
					{
						response.SetFailure(ErrorTypes.UserValidationKeyExpired, FormatErrorMessage(request, ErrorTypes.UserValidationKeyExpired));
						Logger.Info(request.LogData(), "Unable to validate password reset validation key as has expired");
						return response;
					}

					if (!UserHasAccessToModule(user, AppSettings.Module))
					{
						response.SetFailure(ErrorTypes.InvalidUserTypeForModule, FormatErrorMessage(request, ErrorTypes.InvalidUserTypeForModule));
						Logger.Info(request.LogData(), string.Format("User '{0}' isn't a valid {1} user.  It is a {2} user.", user.UserName, AppSettings.Module, user.UserType));
						return response;
					}

					response.UserId = user.Id;

					// Log the action
					LogAction(request, ActionTypes.ValidatePasswordResetValidationKey, typeof(User).Name, user.Id);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.UnableToValidatePasswordResetValidationKey), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		public ValidatePinResponse ValidatePinRegistration(ValidatePinRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new ValidatePinResponse(request);

				// Validate client tag & security token only
				if (!ValidateRequest(request, response, Validate.ClientTag | Validate.SessionId | Validate.License | Validate.AccessToken))
					return response;

				try
				{
					// Validate PIN / Fetch record
					if (request.Pin.IsNullOrEmpty() || request.EmailAddress.IsNullOrEmpty())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.UnableToValidatePin));
						Logger.Info(request.LogData(), "Unable to validate PIN as there is no PIN and/or email address.");
						return response;
					}

					var registrationPin = Repositories.Core.RegistrationPins.SingleOrDefault(x => x.Pin == request.Pin && x.EmailAddress == request.EmailAddress);

					if (registrationPin.IsNull())
					{
						#region Check that a user hasn't already registered with the pin/email

						var user = Repositories.Core.Users.SingleOrDefault(x => x.Pin == request.Pin || x.UserName == request.EmailAddress);
						var person = Repositories.Core.Persons.SingleOrDefault(x => x.EmailAddress == request.EmailAddress);

						if (user.IsNotNull() || person.IsNotNull())
						{
							response.SetFailure(ErrorTypes.EmailAlreadyInUse, FormatErrorMessage(request, ErrorTypes.EmailAlreadyInUse));
							Logger.Info(request.LogData(), "There is already an account registered with the details entered.");

							return response;
						}

						#endregion

						response.SetFailure(FormatErrorMessage(request, ErrorTypes.UnableToValidatePin));
						Logger.Info(request.LogData(), string.Format("Unable to validate PIN as no RegistrationPin found for '{0}' and '{1}'.", request.Pin, request.EmailAddress));
						return response;
					}
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.UnableToValidatePin), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		#region Helper Methods

		/// <summary>
		/// Creates the single sign on.
		/// </summary>
		/// <param name="actionUserId">The action user id.</param>
		/// <param name="userId">The user id.</param>
		/// <returns></returns>
		private SingleSignOn CreateSingleSignOn(long actionUserId, long userId)
		{
			// Create SingleSignOn
			var singleSignOn = new SingleSignOn
			{
				ActionerId = actionUserId,
				UserId = userId
			};
			if (RuntimeContext.IsNotNull() &&
			    RuntimeContext.CurrentRequest.IsNotNull() &&
			    RuntimeContext.CurrentRequest.UserContext.IsNotNull())
			{
				singleSignOn.ExternalAdminId = RuntimeContext.CurrentRequest.UserContext.ExternalUserId;
				singleSignOn.ExternalOfficeId = RuntimeContext.CurrentRequest.UserContext.ExternalOfficeId;
				singleSignOn.ExternalPassword = RuntimeContext.CurrentRequest.UserContext.ExternalPassword;
				singleSignOn.ExternalUsername = RuntimeContext.CurrentRequest.UserContext.ExternalUserName;
			}

			Repositories.Core.Add(singleSignOn);
			Repositories.Core.SaveChanges(true);

			return singleSignOn;
		}

		#endregion
	}
}
