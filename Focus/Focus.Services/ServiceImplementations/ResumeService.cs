﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;

using Focus.Core;
using Focus.Core.Criteria;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.IntegrationMessages;
using Focus.Core.Messages;
using Focus.Core.Messages.ResumeService;
using Focus.Core.Models.Career;
using Focus.Core.Models.Integration;
using Focus.Data.Configuration.Entities;
using Focus.Data.Core.Entities;
using Focus.Services.DtoMappers;
using Focus.Services.Helpers;
using Focus.Services.Mappers;
using Focus.Services.Queries;
using Focus.Services.Core;
using Focus.Services.ServiceContracts;

using Framework.Core;
using Framework.DataAccess;
using Framework.Logging;
using Mindscape.LightSpeed;
using Mindscape.LightSpeed.Querying;
using AcknowledgementType = Focus.Core.Messages.AcknowledgementType;

#endregion

namespace Focus.Services.ServiceImplementations
{
	public class ResumeService : ServiceBase, IResumeService
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="ResumeService" /> class.
		/// </summary>
		public ResumeService() : this(null)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="ResumeService" /> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		public ResumeService(IRuntimeContext runtimeContext) : base(runtimeContext)
		{ }

		/// <summary>
		/// Gets the resume.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public GetResumeResponse GetResume(GetResumeRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new GetResumeResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					if (request.ResumeId.IsNotNull())
					{
						var resume = Repositories.Core.FindById<Resume>(request.ResumeId);
						response.SeekerResume = (resume.IsNotNull()) ? resume.AsDto().ToResumeModel(RuntimeContext, request.UserContext.Culture) : null;
					}
					else
					{
						// As far as I can see Career used to get the first resume after get all resumes for the user
						// I am going to get the default as this makes more sense to me
						var resume = Repositories.Core.Resumes.FirstOrDefault(x => x.PersonId == request.UserContext.PersonId && x.IsDefault);

						// If there is no default then I will get any one like it use to
						if (resume.IsNull())
							resume = Repositories.Core.Resumes.FirstOrDefault(x => x.PersonId == request.UserContext.PersonId);

						response.SeekerResume = (resume.IsNotNull()) ? resume.AsDto().ToResumeModel(RuntimeContext, request.UserContext.Culture) : null;
					}
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Re-Regisers the default resume for a person.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns>The response</returns>
		public RegisterResumeResponse RegisterDefaultResume(RegisterResumeRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new RegisterResumeResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var personId = request.PersonId ?? request.UserContext.PersonId;
					var resume = Repositories.Core.Resumes.FirstOrDefault(x => x.PersonId == personId && x.IsDefault);

					if (resume.IsNotNull())
					{
						if (resume.IsSearchable && resume.ResumeCompletionStatusId == ResumeCompletionStatuses.Completed)
						{
							Helpers.Messaging.Publish(request.ToRegisterResumeMessage(resume.Id, resume.PersonId), updateIfExists: true, entityName: EntityTypes.Resume.ToString(), entityKey: personId);
							LogAction(request, ActionTypes.SearchableResume, resume);
						}
					}

					response.Acknowledgement = AcknowledgementType.Success;

					if (resume.IsNotNull())
						LogAction(request, ActionTypes.RegisterDefaultResume, resume);
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		public GetResumeTemplateResponse GetResumeTemplate(GetResumeTemplateRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new GetResumeTemplateResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var personId = request.PersonId ?? request.UserContext.PersonId;

					var resumeTemplate = Repositories.Core.ResumeTemplates.FirstOrDefault(x => x.PersonId == personId);
					var currentLogIn = DateTime.MinValue;
					var person = Repositories.Core.Users.FirstOrDefault(x => x.PersonId == personId);
					if (person.IsNotNull())
						currentLogIn = person.LoggedInOn.GetValueOrDefault();

					// If no resume template available or it was updated before last log in then get the latest
					if (resumeTemplate.IsNull() || (resumeTemplate.IsNotNull() && resumeTemplate.UpdatedOn < currentLogIn))
					{
						Repositories.Integration.GetJobSeeker(new GetJobSeekerRequest
						{
							PersonId = personId.GetValueOrDefault(),
							ForImport = false,
							JobSeeker = new JobSeekerModel { ExternalId = person.ExternalId }
						});
						resumeTemplate = Repositories.Core.ResumeTemplates.FirstOrDefault(x => x.PersonId == personId);
					}

					if (resumeTemplate.IsNotNull() && resumeTemplate.ResumeXml.IsNotNullOrEmpty())
					{
						response.Resume = resumeTemplate.ResumeXml.ToResumeModel(RuntimeContext, request.UserContext.Culture);
						response.Resume.ResumeMetaInfo.ResumeName = string.Empty;
						if (response.Resume.ResumeContent.Profile.IsNull())
							response.Resume.ResumeContent.Profile = new UserProfile();
					}

				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Parses the resume.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public ParseResumeResponse ParseResume(ParseResumeRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{

				var response = new ParseResumeResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var resumeModel = request.ResumeXML.ToResumeModel(RuntimeContext, request.UserContext.Culture);
					response.SeekerResume = resumeModel;
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.ResumeNotFound), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Saves the resume.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public SaveResumeResponse SaveResume(SaveResumeRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var isNewResume = false;

				var isAlreadyCompleted = false;
				var wasMSFW = false;

				var response = new SaveResumeResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var personId = request.PersonId ?? request.UserContext.PersonId;

					// Check the person we are adding the resume for exists
					if (!personId.HasValue)
					{
						response.SetFailure(ErrorTypes.PersonDetailsNotSupplied, FormatErrorMessage(request, ErrorTypes.PersonDetailsNotSupplied));
						Logger.Info(request.LogData(), "No person ID provided.");
						return response;
					}

					var person = Repositories.Core.FindById<Person>(personId);

					if (person.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.PersonNotFound));
						Logger.Info(request.LogData(), string.Format("The person '{0}' was not found.", personId));
						return response;
					}

					if (request.SeekerResume.IsNull())
					{
						response.SetFailure(ErrorTypes.ResumeDetailsNotProvided, FormatErrorMessage(request, ErrorTypes.ResumeDetailsNotProvided));
						Logger.Info(request.LogData(), "No resume provided.");
						return response;
					}

					#region Canon resume

					ResumeModel canonResumeModel;
					string canonResumeXml;

					ResumeDto resumeDto;

					if (!request.IgnoreLens)
					{
						// Get skills before xray parsing
						List<string> skillsBeforeParse;
						List<InternshipSkill> internshipSkills;

						if (request.SeekerResume.ResumeContent.IsNotNull() && request.SeekerResume.ResumeContent.Skills.IsNotNull())
						{
							skillsBeforeParse = request.SeekerResume.ResumeContent.Skills.Skills.IsNotNullOrEmpty()
								? request.SeekerResume.ResumeContent.Skills.Skills
								: new List<string>();

							// Temporarily remove internship skills as the should not be auto-extracted.
							internshipSkills = request.SeekerResume.ResumeContent.Skills.InternshipSkills;
							request.SeekerResume.ResumeContent.Skills.InternshipSkills = null;
						}
						else
						{
							skillsBeforeParse = new List<string>();
							internshipSkills = null;
						}

						var resumeBeforeCanonDto = request.SeekerResume.ToResumeDto(RuntimeContext, request.UserContext.Culture, AppSettings.DefaultCountryKey);
						canonResumeXml = Repositories.Lens.Canon(resumeBeforeCanonDto.ResumeXml);
						canonResumeModel = canonResumeXml.ToResumeModel(RuntimeContext, request.UserContext.Culture);

						var skillsAfterParse = GetCanonSkills(canonResumeXml);

						// Remove hidden skills
						if (request.SeekerResume.Special.IsNotNull() && request.SeekerResume.Special.HideInfo.IsNotNull() && request.SeekerResume.Special.HideInfo.HiddenSkills.IsNotNullOrEmpty())
							skillsAfterParse.RemoveAll(s => request.SeekerResume.Special.HideInfo.HiddenSkills.Contains(s, StringComparer.OrdinalIgnoreCase));

						// Concatenate the skills removing duplicates
						var skills = new List<string>();

						if (skillsAfterParse.IsNotNullOrEmpty() && skillsBeforeParse.IsNotNullOrEmpty())
							skills = skillsBeforeParse.Union(skillsAfterParse, CoreExtensions.IgnoreSpaceComparer).ToList();
						else if (skillsAfterParse.IsNotNullOrEmpty())
							skills = skillsAfterParse;
						else if (skillsBeforeParse.IsNotNullOrEmpty())
							skills = skillsBeforeParse;

						// Assign the skills back to the resume
						canonResumeModel.ResumeContent.Skills.Skills = skills;

						// Re-add back internship skills
						if (internshipSkills.IsNotNull())
							canonResumeModel.ResumeContent.Skills.InternshipSkills = internshipSkills;

						resumeDto = canonResumeModel.ToResumeDto(RuntimeContext, request.UserContext.Culture, AppSettings.DefaultCountryKey);
						resumeDto.DateOfBirth = resumeBeforeCanonDto.DateOfBirth;
						resumeDto.CountyId = resumeBeforeCanonDto.CountyId;
					}
					else
					{
						resumeDto = request.SeekerResume.ToResumeDto(RuntimeContext, request.UserContext.Culture, AppSettings.DefaultCountryKey);

						canonResumeModel = resumeDto.ToResumeModel(RuntimeContext, request.UserContext.Culture);
						canonResumeXml = resumeDto.ResumeXml;
					}
					var resumeWasCompleted = resumeDto.ResumeCompletionStatusId == ResumeCompletionStatuses.Completed;

					#endregion

					// Calculate years experience
					if (canonResumeXml.IsNotNullOrEmpty())
						resumeDto.YearsExperience = ResumeHelper.GetYearsOfExperience(canonResumeXml);

					var existingResumes = person.Resumes.Where(r => r.StatusId == ResumeStatuses.Active).ToList();
                   
					var isFirstResume = existingResumes.All(existing => existing.ResumeCompletionStatusId != ResumeCompletionStatuses.Completed);

					//If this is the users first resume set it as default
                    if (existingResumes.IsNullOrEmpty() || (isFirstResume && resumeDto.ResumeCompletionStatusId == ResumeCompletionStatuses.Completed))
                    {
                        resumeDto.IsDefault = true;
                        //FVN-6643 AIP It removes other default resumes. Multiple default resumes was causing system error in assist when accessing jobseeker account
                        var resumesToReset = person.Resumes.Where(x => x.IsDefault && x.Id != resumeDto.Id).ToList();

                        if (resumesToReset.IsNotNullOrEmpty())
                            resumesToReset.ForEach(x => x.IsDefault = false);
                        ////////////////////////////////////
                    }
                    else
                    {
                        // If the current resume has not been previously set to the default, check if the current resume is already the default
                        if (!resumeDto.IsDefault && resumeDto.Id.HasValue)
                            resumeDto.IsDefault = existingResumes.FirstOrDefault(x => x.IsDefault && x.Id == resumeDto.Id).IsNotNull();

                        // We still havent set a default, so check if any existing are already default and if none exist set current to be default
                        if (!resumeDto.IsDefault)
                            resumeDto.IsDefault = existingResumes.All(x => x.IsDefault).IsNull();
                    }

                    if (resumeDto.IsDefault)
                    {
                        var ExistingDefaultResume = existingResumes.Where(x => x.IsDefault && x.Id != resumeDto.Id && x.ResumeCompletionStatusId == ResumeCompletionStatuses.Completed && x.StatusId == ResumeStatuses.Active).ToList();
                        if (ExistingDefaultResume.IsNotNullOrEmpty() && (resumeDto.ResumeCompletionStatusId != ResumeCompletionStatuses.Completed))
                            resumeDto.IsDefault = false;
                    }

					// Save Resume.
					Resume resume = null;

					if (resumeDto.Id.HasValue)
					{
						resume = Repositories.Core.FindById<Resume>(resumeDto.Id);
					}

					if (resume != null)
					{
						isAlreadyCompleted = resume.ResumeCompletionStatusId == ResumeCompletionStatuses.Completed;
						wasMSFW = isAlreadyCompleted && resume.MigrantSeasonalFarmWorker.GetValueOrDefault(false);

						// Maintain external Id
						if (resume.IsNotNull())
							resumeDto.ExternalId = resume.ExternalId;

						//if (resumeDto.EmailAddress != resume.EmailAddress)
						//  CensorshipCheckEmail(resumeDto.EmailAddress, person.Id, false);

						resume = resumeDto.CopyTo(resume);
					}
					else
					{
						resume = new Resume();
						resume = resumeDto.CopyTo(resume);
						Repositories.Core.Add(resume);
						isNewResume = true;

						//CensorshipCheckEmail(resume.EmailAddress, person.Id, false);
					}

					// Set External Id (Only reset if we are provided with one)
					if (request.ExternalResumeId.IsNotNullOrEmpty())
						resume.ExternalId = request.ExternalResumeId;

					// If resume is default then ensure all other resumes are set as not default
					if (resume.IsDefault)
					{
						var resumesToReset = existingResumes.Where(x => x.IsDefault && x.Id != resume.Id).ToList();

						if (resumesToReset.IsNotNullOrEmpty())
							resumesToReset.ForEach(x => x.IsDefault = false);
					}

					// Set resume name
					if (resume.ResumeName.IsNullOrEmpty())
						resume.ResumeName = String.Format(LocaliseOrDefault(request, "ResumeService.SaveResume.DefaultResumeName", "Resume on {0}"), (resume.CreatedOn.IsNotNull() && resume.CreatedOn != DateTime.MinValue) ? resume.CreatedOn.ToString("g") : DateTime.Now.ToString("g"));

					// Save Resume Jobs
					if (resume.ResumeJobs.IsNotNullOrEmpty())
					{
						var resumeJobs = resume.ResumeJobs.ToList();
						resumeJobs.ForEach(x => Repositories.Core.Remove(x));
					}

					if (canonResumeModel.ResumeContent.IsNotNull() && canonResumeModel.ResumeContent.ExperienceInfo.IsNotNull() && canonResumeModel.ResumeContent.ExperienceInfo.Jobs.IsNotNullOrEmpty())
					{
						foreach (var job in canonResumeModel.ResumeContent.ExperienceInfo.Jobs)
						{
							resume.ResumeJobs.Add(new ResumeJob
							{
								JobTitle = job.Title.TruncateString(255),
								Employer = job.Employer.TruncateString(255),
								StartYear = (job.JobStartDate.HasValue) ? job.JobStartDate.Value.Year.ToString() : null,
								EndYear = (job.JobEndDate.HasValue) ? job.JobEndDate.Value.Year.ToString() : null,
								LensId = (job.JobId.HasValue) ? job.JobId : null,
                                ReasonForLeaving = job.JobReasonForLeaving,
                                JobOnet = job.OnetCode.IsNotNullOrEmpty() ? (job.OnetCode.Contains('|') ? job.OnetCode.Split('|').FirstOrDefault() : job.OnetCode) : null
							});
						}
					}

					// Save ResumeAddIns
					var existingSections = resume.ResumeAddIns.ToList();

					var sections = canonResumeModel.ResumeContent.AdditionalResumeSections ?? new List<AdditionalResumeSection>();
					sections.ForEach(section =>
					{
						var existingSection = existingSections.FirstOrDefault(existing => existing.ResumeSectionType == section.SectionType);

						// Insert new if it doesn't exist
						if (existingSection.IsNull())
						{
							resume.ResumeAddIns.Add(new ResumeAddIn
							{
								ResumeSectionType = section.SectionType,
								Description = section.SectionContent.ToString()
							});
						}
						else
						{
							// Update existing section
							existingSection.Description = section.SectionContent.ToString();

							// Remove it from the list so that all that remains will be existing sections that need to be removed from the database
							existingSections.Remove(existingSection);
						}
					});

					// Remove any existing section that remains
					foreach (var removeSection in existingSections)
					{
						Repositories.Core.Remove(removeSection);
					}

					// Save selected data
					var existingSelections = resume.ResumeSelectedData.ToList();
					var newSelections = new List<ResumeSelectedDataDto>();

					// Currently only MSFW questions use this table
					var msfwSelections = canonResumeModel.ResumeContent.Profile.MSFWQuestions;
					if (msfwSelections.IsNotNullOrEmpty())
					{
						newSelections.AddRange(msfwSelections.Select(selectionId => new ResumeSelectedDataDto
						{
							LookupId = selectionId,
							LookupType = LookupTypes.MSFWQuestions.ToString()
						}));
					}
					var existingMSFWIds = existingSelections.Where(e => e.LookupType == LookupTypes.MSFWQuestions.ToString()).Select(e => e.LookupId).ToList();

					newSelections.ForEach(selection =>
					{
						var existingSelection = existingSelections.FirstOrDefault(e => e.LookupType == selection.LookupType && e.LookupId == selection.LookupId);
						if (existingSelection.IsNull())
						{
							var newSelectedData = new ResumeSelectedData();
							selection.CopyTo(newSelectedData);
							resume.ResumeSelectedData.Add(newSelectedData);
						}
						else
						{
							existingSelections.Remove(existingSelection);
						}
					});

					// Remove any existing selection that remains
					foreach (var removeSelection in existingSelections)
					{
						Repositories.Core.Remove(removeSelection);
					}

					// Assign Resume to the current person
					resume.Person = person;

					// Update properties in xml that may have been updated
					resume.ResumeXml = resume.UpdateResumeXml();

					#region Update Person, PersonAddress and User records

					if (resume.IsDefault && resume.ResumeCompletionStatusId == ResumeCompletionStatuses.Completed)
					{
						SetPersonDetailsFromDefaultResume(request, resume, person, request.SeekerResume, true);

						if (isFirstResume && !request.IgnoreClient)
						{
							if (AppSettings.MandatoryJSJobAlerts || CandidateService.IsUiClaimant(Repositories.Integration, person))
							{
								var postingAge = GetLookup(LookupTypes.PostingAges, 0, "PostingAge.MostRecent7Days").Select(x => x.ExternalId).FirstOrDefault();

								var defaultSearchCriteria = new SearchCriteria
								{
									RequiredResultCriteria = new RequiredResultCriteria
									{
										DocumentsToSearch = DocumentType.Posting,
										MinimumStarMatch = StarMatching.ThreeStar,
										MaximumDocumentCount = AppSettings.MaximumNoDocumentToReturnInSearch
									},
									ReferenceDocumentCriteria = new ReferenceDocumentCriteria
									{
										DocumentId = resume.Id.ToString(),
										DocumentType = DocumentType.Resume	
									},
									JobLocationCriteria = new JobLocationCriteria
									{
										Radius = new RadiusCriteria
										{
											Distance = 100,
											DistanceUnits = Focus.Core.Models.Career.DistanceUnits.Miles,
											PostalCode = request.SeekerResume.ResumeContent.SeekerContactDetails.PostalAddress.Zip
										}
									},
									PostingAgeCriteria = new PostingAgeCriteria
									{
										PostingAgeInDays = postingAge.AsInt()
									}
								};

								var defaultSavedSearch = new SavedSearch
								{
									Name = "Default job alert",
									SearchCriteria = defaultSearchCriteria.Serialize(),
									AlertEmailAddress = person.EmailAddress,
									AlertEmailFormat = EmailFormats.HTML,
									AlertEmailFrequency = EmailAlertFrequencies.Daily,
									AlertEmailRequired = true,
									Type = SavedSearchTypes.CareerPostingSearch,
									AlertEmailScheduledOn = AnnotationService.GetAlertEmailScheduledDate(EmailAlertFrequencies.Daily)
								};

								defaultSavedSearch.Users.Add(person.User);
							}
						}
					}

					#endregion

					resume.StatusId = ResumeStatuses.Active;

					// Handle primary onet with more than primary onet !!!!
					if (resume.PrimaryOnet.Contains("|"))
					{
						var primaryOnet = resume.PrimaryOnet.Split('|');
						resume.PrimaryOnet = primaryOnet[0];
					}

					if (resume.IsDefault && resume.ResumeCompletionStatusId == ResumeCompletionStatuses.Completed)
						CheckMSFWIssue(request, person, resume, wasMSFW, existingMSFWIds, msfwSelections);

					if (resume.IsDefault && resume.ResumeCompletionStatusId == ResumeCompletionStatuses.Completed && resumeWasCompleted)
						CheckAlientRegistration(request, person, request.SeekerResume.ResumeContent.Profile.IsUSCitizen ?? false, request.SeekerResume.ResumeContent.Profile.AlienExpires ?? DateTime.MinValue);

					Repositories.Core.SaveChanges(true);

					if (request.OverrideCreationDate.IsNotNull())
					{
						var updateQuery = new Query(typeof(Resume), Entity.Attribute("Id") == resume.Id);
						Repositories.Core.Update(updateQuery, new { CreatedOn = request.OverrideCreationDate });
						Repositories.Core.SaveChanges(true);
					}

					if (request.ResumeDocument.IsNotNull())
						SaveResumeDocument(resume, request.ResumeDocument, true);

					if (resume.IsDefault && resume.ResumeCompletionStatusId == ResumeCompletionStatuses.Completed)
					{
						var actionTypesToPublish = new List<ActionTypes>();
                        if(resume.IsVeteran.GetValueOrDefault(false))
                        {
						if ((!resume.VeteranPriorityServiceAlertsSubscription && !isAlreadyCompleted) || request.SeekerResume.ResumeContent.Profile.Veteran.VeteranPriorityServiceAlertsSubscriptionChanged)
						{
							request.SeekerResume.ResumeContent.Profile.Veteran.VeteranPriorityServiceAlertsSubscriptionChanged = false;
							LogAction(request, ActionTypes.UnsubscribeEmail, typeof (Person).Name, person.Id, person.User.Id,(long) UnsubscribeTypes.VeteranPriorityEmails, overrideUserId: person.User.Id);
						}
                        }

						if (request.SeekerResume.Special.Preferences.BridgestoOppsInterest.GetValueOrDefault() && (request.SeekerResume.Special.Preferences.BridgestoOppsInterestChanged || !isAlreadyCompleted))
						{
							actionTypesToPublish.Add(ActionTypes.BridgesToOpportunity);

							var activityId = Repositories.Configuration.Query<ActivityView>()
								.Where(a => a.ActivityExternalId == AppSettings.BridgestoOppsInterestActivityExternalId && a.ActivityType == ActivityType.JobSeeker)
								.Select(a => a.Id)
								.FirstOrDefault();
							LogAction(request, ActionTypes.AssignActivityToCandidate, typeof(Person).Name, person.Id, activityId, actionedOn: DateTime.Now);
						}

						actionTypesToPublish.Add(!isAlreadyCompleted ? ActionTypes.CompleteResume : ActionTypes.EditDefaultCompletedResume);

						// This should probably be avoided.
						if (resume.IsSearchable)
						{
							Helpers.Messaging.Publish(request.ToRegisterResumeMessage(resume.Id, resume.PersonId, request.IgnoreLens, request.IgnoreClient, actionTypesToPublish), updateIfExists: true, entityName: EntityTypes.Resume.ToString(), entityKey: personId);
							LogAction(request, ActionTypes.SearchableResume, resume);
						}
						else
						{
							Helpers.Messaging.Publish(request.ToUnregisterResumeMessage(resume.Id, resume.PersonId, request.IgnoreLens, request.IgnoreClient, actionTypesToPublish), updateIfExists: true, entityName: EntityTypes.Resume.ToString(), entityKey: personId);
						}
					}

					response.Acknowledgement = AcknowledgementType.Success;
					response.SeekerResume = resume.AsDto().ToResumeModel(RuntimeContext, request.UserContext.Culture);
					response.SeekerResume.ResumeMetaInfo.ResumeName = resume.ResumeName;

					if (resume.IsNotNull())
					{
						LogAction(request, ActionTypes.SaveResume, resume);
						if (isNewResume) LogAction(request, ActionTypes.CreateNewResume, resume, actionedOn: request.OverrideCreationDate);
						if (resume.ResumeCompletionStatusId == ResumeCompletionStatuses.Completed)
						{
							if (!isAlreadyCompleted)
							{
								LogAction(request, ActionTypes.CompleteResume, resume);
								//if (request.UserContext.IsNotNull())
								//	Helpers.SelfService.PublishSelfServiceActivity(ActionTypes.CompleteResume, request.UserContext.ActionerId, request.UserContext.UserId, request.UserContext.PersonId ?? 0, true);
								if (!resume.IsDefault)
									LogAction(request, ActionTypes.CompleteNonDefaultResume, resume);
							}
							else if (resume.IsDefault)
							{
								LogAction(request, ActionTypes.EditDefaultCompletedResume, resume);
								//if (request.UserContext.IsNotNull())
								//	Helpers.SelfService.PublishSelfServiceActivity(ActionTypes.EditDefaultCompletedResume, request.UserContext.ActionerId, request.UserContext.UserId, request.UserContext.PersonId ?? 0);
							}
						}
						else
						{
							if (isAlreadyCompleted)
							{
								Helpers.Messaging.Publish(request.ToUnregisterResumeMessage(resume.Id, resume.PersonId, request.IgnoreLens, request.IgnoreClient, null), updateIfExists: true, entityName: EntityTypes.Resume.ToString(), entityKey: personId);
							}
						}
					}
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Saves the resume document.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public SaveResumeDocumentResponse SaveResumeDocument(SaveResumeDocumentRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new SaveResumeDocumentResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					if (request.ResumeDocument.ResumeId.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.ResumeIdNotProvided));
						Logger.Info(request.LogData(), "Resume id was not provided.");
						return response;
					}

					var resume = Repositories.Core.FindById<Resume>(request.ResumeDocument.ResumeId);

					if (resume.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.ResumeNotFound));
						Logger.Info(request.LogData(), string.Format("Resume {0} was not found.", request.ResumeDocument.ResumeId));
						return response;
					}

					var resumeDocument = SaveResumeDocument(resume, request.ResumeDocument, true);
					/*
					// Check if we have a resume to replace
        	var resumeDocument = (resume.IsNotNull() && resume.ResumeDocument.IsNotNull()) ? resume.ResumeDocument : null;

					if(resumeDocument.IsNotNull())
					{
						request.ResumeDocument.Id = resumeDocument.Id;
					}
					
					// Save ResumeDocument
					resumeDocument = request.ResumeDocument.CopyTo();
					resume.ResumeDocument = resumeDocument;

					Repositories.Core.SaveChanges(true);
          */
					response.ResumeDocument = resumeDocument.AsDto();

					LogAction(request, ActionTypes.SaveResumeDocument, resumeDocument);
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Saves a resume document
		/// </summary>
		/// <param name="resume">The parent Resume object</param>
		/// <param name="documentToSave">The resume document to save</param>
		/// <param name="doSave">Whether to perform a save in the repository</param>
		/// <returns>The saved resume document entity</returns>
		private Data.Core.Entities.ResumeDocument SaveResumeDocument(Resume resume, ResumeDocumentDto documentToSave, bool doSave)
		{
			// Check if we have a resume to replace
			var resumeDocument = (resume.IsNotNull() && resume.ResumeDocument.IsNotNull()) ? resume.ResumeDocument : null;

			if (resumeDocument.IsNotNull())
				documentToSave.Id = resumeDocument.Id;

			// Save ResumeDocument
			resumeDocument = documentToSave.CopyTo();
			resume.ResumeDocument = resumeDocument;

			if (doSave)
				Repositories.Core.SaveChanges(true);

			return resumeDocument;
		}

		/// <summary>
		/// Gets the resume info.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public ResumeMetaInfoResponse GetResumeInfo(ResumeMetaInfoRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new ResumeMetaInfoResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var query = new ResumeQuery(Repositories.Core, request.Criteria);

					switch (request.Criteria.FetchOption)
					{
						case CriteriaBase.FetchOptions.Lookup:
							var resumes = query.Query().Select(x => new { x.Id, x.ResumeName }).ToList();
							response.ResumeInfoLookup = resumes.ToDictionary(x => x.Id.ToString(), x => x.ResumeName);
							break;

						case CriteriaBase.FetchOptions.Single:
							response.ResumeInfo = query.Query().Select(x => new ResumeEnvelope
							{
								ResumeId = x.Id,
								ResumeName = x.ResumeName,
								LastUpdated = x.UpdatedOn,
								CompletionStatus = (x.ResumeCompletionStatusId != null) ? x.ResumeCompletionStatusId.Value : ResumeCompletionStatuses.None,
								IsDefault = x.IsDefault,
								ResumeOnet = x.PrimaryOnet,
								ResumeROnet = x.PrimaryROnet
							}).FirstOrDefault();
							break;

						case CriteriaBase.FetchOptions.Count:
							response.ResumeCount = query.Query().Count();
							break;

						case CriteriaBase.FetchOptions.List:
							response.AllResumeInfo = query.Query().Select(x => new ResumeEnvelope
							{
								ResumeId = x.Id,
								ResumeName = x.ResumeName,
								LastUpdated = x.UpdatedOn,
								CompletionStatus = (x.ResumeCompletionStatusId != null) ? x.ResumeCompletionStatusId.Value : ResumeCompletionStatuses.None,
								IsDefault = x.IsDefault,
								ResumeOnet = x.PrimaryOnet,
								ResumeROnet = x.PrimaryROnet
							}).ToList();
							break;
					}
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the automated summary.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public SummaryResponse GetAutomatedSummary(SummaryRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new SummaryResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					if (request.SeekerResume.IsNull())
					{
						response.SetFailure(ErrorTypes.ResumeDetailsNotProvided, FormatErrorMessage(request, ErrorTypes.ResumeDetailsNotProvided));
						Logger.Info(request.LogData(), "No resume provided.");
						return response;
					}

					var resume = request.SeekerResume.ToResumeDto(RuntimeContext, request.UserContext.Culture, AppSettings.DefaultCountryKey);
					var canonnedResume = Repositories.Lens.Canon(resume.ResumeXml);

					response.Summary = Helpers.Resume.GenerateAutomatedSummary(canonnedResume, AppSettings.FilterSkillsInAutomatedSummary, AppSettings.RemoveSpacesFromAutomatedSummary, request.EscapeText);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Deletes the resume.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public DeleteResumeResponse DeleteResume(DeleteResumeRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new DeleteResumeResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					if (request.ResumeId.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.ResumeIdNotProvided));
						Logger.Info(request.LogData(), "Resume id was not provided.");
						return response;
					}

					var resume = Repositories.Core.FindById<Resume>(request.ResumeId);

					if (resume.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.ResumeNotFound));
						Logger.Info(request.LogData(), string.Format("Resume {0} was not found.", request.ResumeId));
						return response;
					}

					var personId = resume.PersonId;
					var resumeId = resume.Id;

					// Defensive code to check for other resumes. If no other resumes, the assume the resume being deleted is always the default
					var otherResumeId = Repositories.Core.Resumes
						.Where(other => other.PersonId == personId && other.Id != resumeId && other.StatusId == ResumeStatuses.Active)
						.Select(other => other.Id)
						.FirstOrDefault();

					var wasDefaultResume = resume.IsDefault || otherResumeId == 0;

					resume.StatusId = ResumeStatuses.Deleted;
					resume.IsDefault = false;
					resume.IsActive = false;

					var resumeDoc = resume.ResumeDocument;

					if (resumeDoc.IsNotNull())
						Repositories.Core.Remove(resumeDoc);

					Resume newDefault = null;

					if (wasDefaultResume)
					{
						if (request.NewDefaultResumeId.IsNotNull())
							newDefault = Repositories.Core.Resumes.FirstOrDefault(x => x.Id == request.NewDefaultResumeId);

						if (newDefault.IsNull())
							newDefault = Repositories.Core.Resumes.Where(x => x.PersonId == request.UserContext.PersonId && x.Id != resume.Id && x.StatusId == ResumeStatuses.Active && x.ResumeCompletionStatusId == ResumeCompletionStatuses.Completed).FirstOrDefault();
                            if(newDefault.IsNull())
                            {
                                newDefault = Repositories.Core.Resumes.Where(x => x.PersonId == request.UserContext.PersonId && x.Id != resume.Id).FirstOrDefault();
                            }


						if (newDefault.IsNotNull())
						{
                            
                            newDefault.IsDefault = true;
							newDefault.ResumeXml = newDefault.UpdateResumeXml();

							if (newDefault.ResumeCompletionStatusId == ResumeCompletionStatuses.Completed)
							{
								SetPersonDetailsFromDefaultResume(request, newDefault);

								if (newDefault.IsSearchable)
								{
									Helpers.Messaging.Publish(request.ToRegisterResumeMessage(newDefault.Id, resume.PersonId), updateIfExists: true, entityName: EntityTypes.Resume.ToString(), entityKey: personId);
									LogAction(request, ActionTypes.SearchableResume, newDefault);
								}
								else
								{
									Helpers.Messaging.Publish((request.ToUnregisterResumeMessage(resume.Id, resume.PersonId)), updateIfExists: true, entityName: EntityTypes.Resume.ToString(), entityKey: personId);
								}
							}
							else
							{
								Helpers.Messaging.Publish((request.ToUnregisterResumeMessage(resume.Id, resume.PersonId)), updateIfExists: true, entityName: EntityTypes.Resume.ToString(), entityKey: personId);
							}

							var person = Repositories.Core.FindById<Person>(resume.PersonId);
							var oldQuestions = resume.ResumeSelectedData.Where(s => s.LookupType == LookupTypes.MSFWQuestions.ToString()).Select(s => s.LookupId).ToList();
							CheckMSFWIssue(request, person, newDefault, resume.MigrantSeasonalFarmWorker.GetValueOrDefault(false), oldQuestions);

							CheckAlientRegistration(request, person, resume.IsUSCitizen ?? false, resume.AlienRegExpiryDate ?? DateTime.MinValue);
						}
						else
						{
							var person = resume.Person;
							person.IsVeteran = null;

							Helpers.Messaging.Publish((request.ToUnregisterResumeMessage(resume.Id, resume.PersonId)), updateIfExists: true, entityName: EntityTypes.Resume.ToString(), entityKey: personId);
						}
					}

					Repositories.Core.SaveChanges(true);

					response.Acknowledgement = AcknowledgementType.Success;

					LogAction(request, ActionTypes.DeleteResume, resume);

					if (resumeDoc.IsNotNull())
						LogAction(request, ActionTypes.DeleteResumeDocument, resumeDoc);

					if (newDefault.IsNotNull())
						LogAction(request, ActionTypes.SetDefaultResume, newDefault);

					// Update integration client
					if (AppSettings.IntegrationClient != IntegrationClient.Standalone && resume.ResumeCompletionStatusId == ResumeCompletionStatuses.Completed)
					{
						resume.Person.User.PublishUserUpdateIntegrationRequest(request.UserContext.UserId, RuntimeContext);
					}
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the structured resume.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public ResumeConvertionResponse GetStructuredResume(ResumeConvertionRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new ResumeConvertionResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var taggedResume = request.TaggedResume;

					if (request.TaggedResume.IsNull() && (!request.ResumeId.HasValue))
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.ResumeNotFound));
						Logger.Info(request.LogData(), "Neither tagged resume or resume id was provided.");
						return response;
					}

					if (request.TaggedResume.IsNull())
					{
						var resume = Repositories.Core.FindById<Resume>(request.ResumeId);

						if (resume.IsNull())
						{
							response.SetFailure(FormatErrorMessage(request, ErrorTypes.ResumeNotFound));
							Logger.Info(request.LogData(), string.Format("The resume '{0}' was not found.", request.ResumeId));
							return response;
						}

						taggedResume = (resume.ResumeXml.IsNotNullOrEmpty()) ? resume.ResumeXml : "";
					}

					if (request.Canonize)
					{
						taggedResume = Repositories.Lens.Canon(taggedResume);
					}

					response.SeekerResume = taggedResume.ToResumeModel(RuntimeContext, request.UserContext.Culture);
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Returns HTML formated resume.
		/// </summary>
		/// <param name="request"></param>
		/// <returns></returns>
		public OriginalResumeResponse GetOriginalResume(OriginalResumeRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new OriginalResumeResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					if (request.ResumeId.IsNotNull())
					{
						var resumeDocument = Repositories.Core.ResumeDocuments.FirstOrDefault(x => x.ResumeId == request.ResumeId);

						if (resumeDocument.IsNotNull())
						{
							switch (request.ResumeType)
							{
								case DocumentContentType.HTML:
									response.HTMLResume = resumeDocument.Html;
								    //FVN-6792 Foreign character AIP
								    if (!response.HTMLResume.EndsWith("</html>"))
								    {

                                        Regex regex = new Regex("</html>.+");
								        string result = regex.Replace(response.HTMLResume, "</html>");
								        response.HTMLResume = result;
								    }
									break;
								case DocumentContentType.Binary:
									response.FileName = resumeDocument.FileName;
									response.MimeType = resumeDocument.ContentType;
									response.ResumeContent = resumeDocument.DocumentBytes;
									response.HTMLResume = resumeDocument.Html;
								    //FVN-6792 Foreign character AIP
								    if (!response.HTMLResume.EndsWith("</html>"))
								    {
                                        Regex regex = new Regex("</html>.+");
                                        string result = regex.Replace(response.HTMLResume, "</html>");
								        response.HTMLResume = result;
								    }
									break;
							}
						}
					}
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the tagged resume.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public ResumeConvertionResponse GetTaggedResume(ResumeConvertionRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new ResumeConvertionResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					if (request.SeekerResume.IsNull() && (!request.ResumeId.HasValue) && request.ResumeBytes.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.ResumeNotFound));
						Logger.Info(request.LogData(), "Neither seeker resume nor resume id was provided.");
						return response;
					}

					var taggedResume = request.TaggedResume;

					// Get the resume XML
					if (request.SeekerResume.IsNotNull())
					{
						var resume = request.SeekerResume.ToResumeDto(RuntimeContext, request.UserContext.Culture, AppSettings.DefaultCountryKey);

						if (resume.IsNull())
						{
							response.SetFailure(FormatErrorMessage(request, ErrorTypes.ResumeNotFound));
							Logger.Info(request.LogData(), string.Format("The resume '{0}' was not found.", request.ResumeId));
							return response;
						}

						taggedResume = resume.ResumeXml;
					}
					else if (request.ResumeId.HasValue)
					{
						var resume = Repositories.Core.FindById<Resume>(request.ResumeId);

						if (resume.IsNull())
						{
							response.SetFailure(FormatErrorMessage(request, ErrorTypes.ResumeNotFound));
							Logger.Info(request.LogData(), string.Format("The resume '{0}' was not found.", request.ResumeId));
							return response;
						}

						taggedResume = resume.ResumeXml;
					}
					else if (request.ResumeBytes.IsNotNull())
					{
						taggedResume = Repositories.Lens.ParseResume(request.ResumeBytes, request.ResumeFileExtension, false);
					}

					if (taggedResume.IsNotNullOrEmpty())
					{
						if (request.Canonize)
							taggedResume = Repositories.Lens.Canon(taggedResume);

						response.TaggedResume = taggedResume;
					}

					response.TaggedResume = taggedResume;
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the resume jobs.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public ResumeJobResponse GetResumeJobs(ResumeJobRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new ResumeJobResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var query = new ResumeJobQuery(Repositories.Core, request.Criteria);

					switch (request.Criteria.FetchOption)
					{
						case CriteriaBase.FetchOptions.Lookup:
							var resumeJobs = query.Query().Select(x => x.AsDto()).ToList();
							response.ResumeJobLookup = resumeJobs.ToDictionary(x => x.LensId.ToString(), x => string.Format("{0} - {1}", x.JobTitle, x.Employer));
							break;
					}
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the default resume.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public DefaultResumeResponse GetDefaultResume(DefaultResumeRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{

				var response = new DefaultResumeResponse(request);

				// Validate request
				var validate = request.FetchDefaultResume ? Validate.All : (Validate.ClientTag | Validate.SessionId | Validate.License | Validate.AccessToken);
				if (!ValidateRequest(request, response, validate))
					return response;

				try
				{
					var personId = request.PersonId ?? request.UserContext.PersonId;
					var query = Repositories.Core.Resumes.Where(x => x.PersonId == personId && x.IsDefault && x.StatusId == ResumeStatuses.Active);

					if (request.CompletedResumesOnly)
						query = query.Where(x => x.ResumeCompletionStatusId == ResumeCompletionStatuses.Completed);

					if (request.FetchDefaultResume)
					{
						var resume = query.Select(x => x.AsDto()).FirstOrDefault();
						response.Resume = (resume.IsNotNull()) ? resume.ToResumeModel(RuntimeContext, request.UserContext.Culture) : null;
					}
					else
					{
						response.ResumeId = query.Select(x => x.Id).FirstOrDefault();
					}
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Sets the default resume.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public DefaultResumeResponse SetDefaultResume(DefaultResumeRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new DefaultResumeResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					if (request.ResumeId.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.ResumeIdNotProvided));
						Logger.Info(request.LogData(), "Resume id was not provided.");
						return response;
					}

					var resume = Repositories.Core.FindById<Resume>(request.ResumeId);

					if (resume.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.ResumeNotFound));
						Logger.Info(request.LogData(), string.Format("Resume {0} was not found.", request.ResumeId));
						return response;
					}

					if (resume.ResumeCompletionStatusId.GetValueOrDefault(ResumeCompletionStatuses.None) != ResumeCompletionStatuses.Completed)
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.ResumeIncomplete));
						Logger.Info(request.LogData(), string.Format("Resume {0} is incomplete.", request.ResumeId));
						return response;
					}

					var wasMSFW = false;
					var existingMSFW = new List<long>();
					if (request.UserContext.PersonId.IsNotNull())
					{
						var existingResumes = Repositories.Core.Resumes.Where(x => x.PersonId == request.UserContext.PersonId).ToList();
						foreach (var existingResume in existingResumes)
						{
							if (existingResume.IsDefault)
							{
								existingResume.IsDefault = false;
								existingResume.ResumeXml = existingResume.UpdateResumeXml();

								wasMSFW = existingResume.MigrantSeasonalFarmWorker.GetValueOrDefault(false);
								existingMSFW = existingResume.ResumeSelectedData.Where(s => s.LookupType == LookupTypes.MSFWQuestions.ToString()).Select(s => s.LookupId).ToList();
							}
						}
					}

					resume.IsDefault = true;

					var person = Repositories.Core.FindById<Person>(resume.PersonId);
					CheckMSFWIssue(request, person, resume, wasMSFW, existingMSFW);

					CheckAlientRegistration(request, person, resume.IsUSCitizen ?? false, resume.AlienRegExpiryDate ?? DateTime.MinValue);

					resume.ResumeXml = resume.UpdateResumeXml();

					if (resume.ResumeCompletionStatusId == ResumeCompletionStatuses.Completed)
					{
						SetPersonDetailsFromDefaultResume(request, resume);
					}

					Repositories.Core.SaveChanges(true);

					if (resume.IsSearchable && (resume.ResumeCompletionStatusId == ResumeCompletionStatuses.Completed))
					{
						Helpers.Messaging.Publish(request.ToRegisterResumeMessage(resume.Id, resume.PersonId), updateIfExists: true, entityName: EntityTypes.Resume.ToString(), entityKey: resume.PersonId);
						LogAction(request, ActionTypes.SearchableResume, resume);
					}
					else
					{
						Helpers.Messaging.Publish(request.ToUnregisterResumeMessage(resume.Id, resume.PersonId), updateIfExists: true, entityName: EntityTypes.Resume.ToString(), entityKey: resume.PersonId);
					}

					// only send to integration clients if this is an update to a resume. New resumes will be only inserted at the end of the job seeker registration process.
					if (AppSettings.IntegrationClient != IntegrationClient.Standalone &&
					    resume.ResumeCompletionStatusId == ResumeCompletionStatuses.Completed)
					{
						resume.Person.User.PublishUserUpdateIntegrationRequest(request.UserContext.UserId, RuntimeContext);
					}

					LogAction(request, ActionTypes.SetDefaultResume, resume);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Returns HTML formated resume.
		/// </summary>
		/// <param name="request"></param>
		/// <returns></returns>
		public HTMLResumeResponse GetHTMLResume(HTMLResumeRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new HTMLResumeResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var html = String.Empty;

					var resumeDocument = Repositories.Core.ResumeDocuments.FirstOrDefault(x => x.ResumeId == request.ResumeId);

					if (resumeDocument.IsNotNull())
						html = resumeDocument.Html;

					response.HTMLResume = html;
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}
				return response;
			}
		}

		/// <summary>
		/// Gets the resume view count.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public ResumeViewCountResponse GetResumeViewCount(ResumeViewCountRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new ResumeViewCountResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var query = Repositories.Core.ViewedResumeViews;

					if (request.PersonId.HasValue)
						query = query.Where(x => x.ResumePersonId == request.PersonId);

					if (request.UserType.IsNotNull())
						query = query.Where(x => x.ViewerUserType == request.UserType);

					if (request.FromDate.IsNotNull())
						query = query.Where(x => x.ActionedOn >= request.FromDate);

					response.ViewCount = query.Select(x => x.ViewerUserId).Distinct().Count();
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}
				return response;
			}
		}

		#region Private helpers

		/// <summary>
		/// Sets the person details from default resume.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <param name="resume">The resume.</param>
		/// <param name="person">The person.</param>
		/// <param name="resumeModel">The resume model</param>
		/// <param name="updateEmailAddress">Whether to update the person's email address</param>
		private void SetPersonDetailsFromDefaultResume(ServiceRequest request, Resume resume, Person person = null, ResumeModel resumeModel = null, bool updateEmailAddress = false)
		{
			if (person.IsNull())
				person = resume.Person;

			#region update basic person details based on data they've inputted in the resume

			person.FirstName = resume.FirstName;
			person.LastName = resume.LastName;
			person.MiddleInitial = resume.MiddleName.TruncateString(5);
			person.SuffixId = resume.SuffixId;
			person.IsVeteran = resume.IsVeteran;

			person.DateOfBirth = resume.DateOfBirth;

			if (updateEmailAddress && !AccountService.IsEmailInUse(RuntimeContext, resume.EmailAddress, person.Id))
			{
				person.EmailAddress = resume.EmailAddress;
			}

			#endregion

			#region Update address details based on data they've inputted in the resume

			var address = person.PersonAddresses.FirstOrDefault(x => x.IsPrimary);

			if (address.IsNull())
			{
				address = new PersonAddress { IsPrimary = true };
				person.PersonAddresses.Add(address);
			}

			address.Line1 = resume.AddressLine1;
			address.CountyId = resume.CountyId;
			address.CountryId = Convert.ToInt64(resume.CountryId);
			address.PostcodeZip = resume.PostcodeZip;
			address.StateId = Convert.ToInt64(resume.StateId);
			address.TownCity = resume.TownCity.TruncateString(100);

			#endregion

			#region Update user details details based on data they've inputted in the resume

			person.User.ScreenName = string.Format("{0} {1}", resume.FirstName, resume.LastName);

			if (person.User.ScreenName.Length > 50)
				person.User.ScreenName = person.User.ScreenName.Substring(0, 50);

			#endregion

			#region Enrollment Status

			if (resumeModel.IsNull())
				resumeModel = resume.ResumeXml.ToResumeModel(RuntimeContext, request.UserContext.Culture);

			if (resumeModel.ResumeContent.EducationInfo.IsNotNull())
				person.EnrollmentStatus = resumeModel.ResumeContent.EducationInfo.SchoolStatus;

			#endregion

			#region Check for "Posting Low Quality Resume" issue

			if (AppSettings.PostingLowQualityResumeEnabled)
			{
				var resumeWordCount = resume.WordCount;
				if (resumeWordCount.IsNull())
				{
					resumeWordCount = Helpers.Resume.GetNumberOfWords(resume.ResumeXml);
					resume.WordCount = resumeWordCount;
				}

				var resumeSkillCount = resume.SkillCount;
				if (resumeSkillCount.IsNull())
				{
					var resumeSkills = resume.ResumeSkills ?? "";
					resumeSkillCount = resumeSkills.IsNullOrEmpty() ? 0 : resumeSkills.Split(new[] { ',' }).Count();
					resume.SkillCount = resumeSkillCount;
				}

				// underage job seekers shouldn't have issues.
				var isUnderage = AppSettings.UnderAgeJobSeekerRestrictionThreshold > 0 && person.Age.HasValue &&
				                 person.Age.Value < AppSettings.UnderAgeJobSeekerRestrictionThreshold;
				if (!isUnderage &&
				    ((resumeWordCount.IsNotNull() && resumeWordCount < AppSettings.PostingLowQualityResumeWordCountThreshold)
				     ||
				     (resumeSkillCount.IsNotNull() && resumeSkillCount < AppSettings.PostingLowQualityResumeSkillCountThreshold)))
				{
					var issues = Repositories.Core.Issues.FirstOrDefault(issue => issue.PersonId == person.Id);

					if (issues.IsNull())
					{
						issues = new Issues
						{
							PersonId = person.Id,
							PostingLowQualityResumeTriggered = true
						};
						Repositories.Core.Add(issues);
					}
					else
					{
						issues.PostingLowQualityResumeTriggered = true;
					}
					if (AppSettings.DisplayPoorResumeAnnouncement && resume.IsDefault)
					{
						// Check if message already exists
						var userid = person.User.Id;

						var activePoorResumeAlerts = Repositories.Core.Messages.Where(x => x.UserId == userid
						                                                                   && x.MessageType == MessageTypes.CareerDefaultResumePoor
						                                                                   && x.ExpiresOn > DateTime.Now
						                                                                   && !x.DismissedMessages.Any()).Select(x => x.Id).Any();

						if (!activePoorResumeAlerts)
						{
							CreateAlertMessage("PostingLowQualityResume.Text", true, userid, DateTime.Now.AddDays(7), MessageAudiences.User, MessageTypes.CareerDefaultResumePoor, null);
						}
					}
				}
				else
				{
					var issues = Repositories.Core.Issues.FirstOrDefault(issue => issue.PersonId == person.Id && issue.PostingLowQualityResumeTriggered);

					if (issues.IsNotNull())
					{
						issues.PostingLowQualityResumeCount = 0;
						issues.PostingLowQualityResumeResolvedDate = DateTime.Now;
						issues.PostingLowQualityResumeTriggered = false;

						Repositories.Core.SaveChanges();

						LogAction(request, ActionTypes.AutoResolvedIssue, typeof(Person).Name, person.Id,
							request.UserContext.UserId, null, CandidateIssueType.PoorQualityResume.ToString());

					}
				}
			}


			#endregion
		}

		/// <summary>
		/// Gets the canon skills.
		/// </summary>
		/// <param name="canonedResumeXml">The canoned resume XML.</param>
		/// <returns></returns>
		private static List<string> GetCanonSkills(string canonedResumeXml)
		{
			if (canonedResumeXml.IsNullOrEmpty()) return new List<string>();

			var outSkills = new List<string>();
			try
			{
				var xDoc = new XmlDocument();
				xDoc.LoadXml(canonedResumeXml);

				outSkills.AddRange(from XmlElement canonskill in xDoc.SelectNodes("//canonskill") where canonskill.Attributes["name"] != null select canonskill.Attributes["name"].Value.ToTitleCase());

				return outSkills;
			}
			catch
			{
				return new List<string>();
			}
		}

		/// <summary>
		/// Checks if alien registration expiry issue needs to be resolved
		/// </summary>
		/// <param name="request"></param>
		/// <param name="person">The person</param>
		/// <param name="isUSCitizen"></param>
		/// <param name="alienExpires"></param>
		protected void CheckAlientRegistration(ServiceRequest request, Person person, bool isUSCitizen, DateTime alienExpires)
		{
			if (AppSettings.Theme == FocusThemes.Workforce)
			{
				if (isUSCitizen) return;

				var issues = Repositories.Core.Issues.FirstOrDefault(issue => issue.PersonId == person.Id);			

				if (alienExpires < new DateTime())
				{
					if (issues.IsNull())
					{
						issues = new Issues
						{
							PersonId = person.Id
						};
						Repositories.Core.Add(issues);
					}
					issues.HasExpiredAlienCertificationRegistration = true;
					Repositories.Core.SaveChanges();
				}
				else
				{
					if (!issues.IsNotNull() || issues.HasExpiredAlienCertificationRegistration != true) return;
					issues.HasExpiredAlienCertificationRegistration = false;
					Repositories.Core.SaveChanges();
					LogAction(request, ActionTypes.AutoResolvedIssue, typeof(Person).Name, person.Id, request.UserContext.UserId, null, CandidateIssueType.ExpiredAlienCertification.ToString());
				}
			}
		}

		/// <summary>
		/// Checks is MSFW issue needs to be raised
		/// </summary>
		/// <param name="request"></param>
		/// <param name="person">The person.</param>
		/// <param name="resume">The resume.</param>
		/// <param name="wasMSFW">Whether the person was original a migrant/seasonal farm worker</param>
		/// <param name="oldQuestions">The old questions.</param>
		/// <param name="newQuestions">The new questions.</param>
		protected void CheckMSFWIssue(ServiceRequest request, Person person, Resume resume, bool wasMSFW, List<long> oldQuestions, List<long> newQuestions = null)
		{
			if (!AppSettings.JobIssueFlagPotentialMSFW)
				return;

			if (resume.IsNotNull())
			{
				var isMSFW = resume.MigrantSeasonalFarmWorker.GetValueOrDefault();
				// Raise an issue if MSFW has switched from No to Yes
				var raiseIssue = !wasMSFW && isMSFW;

				if (AppSettings.Theme == FocusThemes.Workforce && !isMSFW)
				{
					var issues = Repositories.Core.Issues.FirstOrDefault(issue => issue.PersonId == person.Id && issue.MigrantSeasonalFarmWorkerTriggered);

					if (issues.IsNotNull())
					{
						issues.MigrantSeasonalFarmWorkerTriggered = false;

						Repositories.Core.SaveChanges();

						LogAction(request, ActionTypes.AutoResolvedIssue, typeof(Person).Name, person.Id,
							request.UserContext.UserId, null, CandidateIssueType.PotentialMSFW.ToString());
					}
				}

				// If not switched, and resume is Yes, check the questions
				if (!raiseIssue && isMSFW)
				{
					if (newQuestions.IsNull())
						newQuestions = resume.ResumeSelectedData.Where(s => s.LookupType == LookupTypes.MSFWQuestions.ToString()).Select(s => s.LookupId).ToList();

					raiseIssue = !oldQuestions.EqualsList(newQuestions);
				}

				if (raiseIssue)
				{
					person.MigrantSeasonalFarmWorkerVerified = null;

					var issues = Repositories.Core.Issues.FirstOrDefault(issue => issue.PersonId == person.Id);
					if (issues.IsNull())
					{
						issues = new Issues
						{
							PersonId = person.Id,
							MigrantSeasonalFarmWorkerTriggered = true
						};
						Repositories.Core.Add(issues);
					}
					else
					{
						issues.MigrantSeasonalFarmWorkerTriggered = true;
					}
				}


			}
		}

		#endregion
	}
}
