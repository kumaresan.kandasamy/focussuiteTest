﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;

using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Messages.SearchService;
using Focus.Core.Views;
using Focus.Data.Core.Entities;
using Focus.Services.DtoMappers;
using Focus.Core;
using Focus.Core.Criteria;
using Focus.Core.Criteria.CandidateSearch;
using Focus.Services.Queries;
using Focus.Services.Core;
using Focus.Services.ServiceContracts;

using Framework.Core;
using Framework.Logging;

#endregion

namespace Focus.Services.ServiceImplementations
{
	public class SearchService : ServiceBase, ISearchService
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="SearchService" /> class.
		/// </summary>
		public SearchService() : this(null)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="SearchService" /> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		public SearchService(IRuntimeContext runtimeContext) : base(runtimeContext)
		{ }

		/// <summary>
		/// Searches the specified request.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public CandidateSearchResponse SearchCandidates(CandidateSearchRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new CandidateSearchResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var result = Helpers.Search.SearchResumes(request.Criteria, request.SearchId, request.ForCount);
					response.SearchId = result.SearchId;
					response.Candidates = result.Resumes;
					response.HighestStarRating = result.HighestStarRating;

          // If this search isn't for a count, is not cached (searchid changes during search), has job details and is an open search then record the results!
          if (!request.ForCount && request.SearchId != response.SearchId && request.Criteria.ScopeType == CandidateSearchScopeTypes.Open  && request.Criteria.JobDetailsCriteria.IsNotNull() && request.Criteria.JobDetailsCriteria.JobId != 0)
          {
            var history = new CandidateSearchHistory
                            {
                              JobId = request.Criteria.JobDetailsCriteria.JobId,
                              CreatedBy = request.UserContext.UserId
                            };

            Repositories.Core.Add(history);

            result.SummarisedSearchResults.ForEach(x => Repositories.Core.Add(new CandidateSearchResult { CandidateSearchHistoryId = history.Id, PersonId = x.Item1, Score = x.Item2 }));
            
            Repositories.Core.SaveChanges();
          }
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Saves the candidate search.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public SaveCandidateSavedSearchResponse SaveCandidateSearch(SaveCandidateSavedSearchRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new SaveCandidateSavedSearchResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
          var userId = request.SaveForUserId.IsNull() || request.SaveForUserId == 0
                ? request.UserContext.UserId
                : request.SaveForUserId;

					var user = Repositories.Core.Users.SingleOrDefault(x => x.Id == userId);
          if (user.IsNull())
          {
            response.SetFailure(FormatErrorMessage(request, ErrorTypes.EmployeeNotFound));
            Logger.Info(request.LogData(), string.Format("Cannot find User with Id '{0}'.", userId));
            return response;
          }

					SavedSearch savedSearch;
					if (request.Id == 0)
					{
						savedSearch = new SavedSearch
						                  	{
						                  		Name = request.Name,
						                  		SearchCriteria = request.Criteria.Serialize(),
						                  		AlertEmailAddress = request.AlertEmailAddress,
						                  		AlertEmailFormat = request.AlertEmailFormat,
						                  		AlertEmailFrequency = request.AlertEmailFrequency,
						                  		AlertEmailRequired = request.AlertEmailRequired,
																	Type = request.SearchType
						                  	};

						if (request.AlertEmailRequired) savedSearch.AlertEmailScheduledOn = GetAlertEmailScheduledDate(request.AlertEmailFrequency);
						
						savedSearch.Users.Add(user);
					}
					else
					{
						savedSearch = Repositories.Core.FindById<SavedSearch>(request.Id);

						var currentAlertFrequecy = savedSearch.AlertEmailFrequency;
						
						savedSearch.Name = request.Name;
						savedSearch.SearchCriteria = request.Criteria.Serialize();
						savedSearch.AlertEmailAddress = request.AlertEmailAddress;
						savedSearch.AlertEmailFormat = request.AlertEmailFormat;
						savedSearch.AlertEmailFrequency = request.AlertEmailFrequency;
						savedSearch.AlertEmailRequired = request.AlertEmailRequired;
						savedSearch.Type = request.SearchType;

						if(savedSearch.AlertEmailRequired)
						{
							if (savedSearch.AlertEmailScheduledOn.IsNull() || (savedSearch.AlertEmailFrequency != currentAlertFrequecy)) savedSearch.AlertEmailScheduledOn = GetAlertEmailScheduledDate(savedSearch.AlertEmailFrequency);
						}

						var newUser = savedSearch.Users.FirstOrDefault(x => x.Id == user.Id);

            if (newUser.IsNull())
							savedSearch.Users.Add(user);
					}

					Repositories.Core.SaveChanges();
					LogAction(request, ActionTypes.SaveCandidateSavedSearch, savedSearch);

				  response.SavedSearchId = savedSearch.Id;
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the alert email scheduled date.
		/// </summary>
		/// <param name="frequency">The frequency.</param>
		/// <returns></returns>
		private static DateTime? GetAlertEmailScheduledDate(EmailAlertFrequencies frequency)
		{
			switch (frequency)
			{
				case EmailAlertFrequencies.Weekly:
					return DateTime.Now.Date.AddDays(7);

				default:
					return DateTime.Now.Date.AddDays(1);
			}
		}

		/// <summary>
		/// Gets the candidate saved search.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public GetCandidateSavedSearchResponse GetCandidateSavedSearch(GetCandidateSavedSearchRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new GetCandidateSavedSearchResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
          switch (request.Criteria.FetchOption)
				  {
				    case CriteriaBase.FetchOptions.PagedList:
				      {
								var savedSearchQuery = new SavedSearchQuery(Repositories.Core, request.Criteria);
								response.SavedSearchesPaged = savedSearchQuery.Query().GetPagedList(x => x.AsDto(), request.Criteria.PageIndex, request.Criteria.PageSize);
								break;
				      }

            case CriteriaBase.FetchOptions.Single:
							var savedSearch = new SavedSearchQuery(Repositories.Core, request.Criteria).QueryEntityId();

						  if (savedSearch.IsNull())
						  {
							  response.SetFailure(FormatErrorMessage(request, ErrorTypes.SavedSearchNotFound));
							  Logger.Info(request.LogData(), string.Format("Cannot find SavedSearch with Id '{0}'.", request.UserContext.PersonId));
						  }

						  response.SavedSearch = savedSearch.AsDto();

				      break;
				  }
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the candidate saved search.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public GetCandidateSavedSearchViewResponse GetCandidateSavedSearchView(GetCandidateSavedSearchViewRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new GetCandidateSavedSearchViewResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var savedSearch = new SavedSearchQuery(Repositories.Core, request.Criteria).Query();

					response.SavedSearches = (
						                        from ssv in savedSearch
						                        select new SavedSearchView {Id = ssv.Id, Name = ssv.Name}
						                        ).ToList();
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

    /// <summary>
    /// Gets the R onet code by id.
    /// </summary>
    /// <param name="ronetId">The ronet id.</param>
    /// <returns></returns>
    public string GetROnetCodeById(long ronetId)
    {
      return Helpers.Occupation.GetROnetCodeById(ronetId);
    }

		/// <summary>
		/// Gets Onets.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public OnetResponse GetOnets(OnetRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new OnetResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.ClientTag | Validate.AccessToken))
					return response;

				try
				{
					response.Onets = Helpers.Occupation.GetOnets(request.Criteria, request.UserContext.Culture);
				}
				catch(Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(),response.Message,response.Exception);
				}
				return response;
			}
		}

    /// <summary>
    /// Gets Onet.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    public OnetResponse GetOnet(OnetRequest request)
    {
      using (Profiler.Profile(request.LogData(), request))
      {
        var response = new OnetResponse(request);

        // Validate request
        if (!ValidateRequest(request, response, Validate.ClientTag | Validate.AccessToken))
          return response;

        try
        {
          response.OnetId = Helpers.Occupation.GetOnet(request.Criteria);
        }
        catch (Exception ex)
        {
          response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
          Logger.Error(request.LogData(), response.Message, response.Exception);
        }
        return response;
      }
    }

    /// <summary>
    /// Gets ROnet.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    public ROnetResponse GetROnet(ROnetRequest request)
    {
      using (Profiler.Profile(request.LogData(), request))
      {
        var response = new ROnetResponse(request);

        // Validate request
        if (!ValidateRequest(request, response, Validate.ClientTag | Validate.AccessToken))
          return response;

        try
        {
          response.ROnetId = Helpers.Occupation.GetROnet(request.ROnetCriteria);
        }
        catch (Exception ex)
        {
          response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
          Logger.Error(request.LogData(), response.Message, response.Exception);
        }
        return response;
      }
    }


    /// <summary>
    /// Gets ROnets.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    public ROnetResponse GetROnets(ROnetRequest request)
    {
      using (Profiler.Profile(request.LogData(), request))
      {
        var response = new ROnetResponse(request);

        // Validate request
        if (!ValidateRequest(request, response, Validate.ClientTag | Validate.AccessToken))
          return response;

        try
        {
          response.ROnets = Helpers.Occupation.GetROnets(request.Criteria, request.UserContext.Culture);
        }
        catch (Exception ex)
        {
          response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
          Logger.Error(request.LogData(), response.Message, response.Exception);
        }
        return response;
      }
    }

    /// <summary>
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    public DeleteSavedSearchResponse DeleteSavedSearch(DeleteSavedSearchRequest request)
    {
      var response = new DeleteSavedSearchResponse(request);

      // Validate request
      if (!ValidateRequest(request, response, Validate.All))
        return response;

      try
      {
          /* KRP 10.May.2016
          * Check whether the saved search id is for the current user and delete it
           * */
          var sSUser = Repositories.Core.SavedSearchUsers.Where(u => u.SavedSearchId == request.SavedSearchId).Select(u => u.UserId).FirstOrDefault();
          if (sSUser != request.UserContext.UserId)
          {
              return response;
          }
        var search = Repositories.Core.FindById<SavedSearch>(request.SavedSearchId);

        Repositories.Core.Remove(search);
        Repositories.Core.SaveChanges(true);

        LogAction(request, ActionTypes.DeleteSavedSearch, search);
      }
      catch (Exception ex)
      {
        response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
        Logger.Error(request.LogData(), response.Message, response.Exception);
      }

      return response;
    }

		/// <summary>
		/// Gets the clarified words.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <param name="minScore">The min score.</param>
		/// <param name="maxSearchCount">The max search count.</param>
		/// <param name="type">The type.</param>
		/// <returns></returns>
		public ClarifyWordResponse GetClarifiedWords(ClarifyWordRequest request, int minScore, int maxSearchCount, string type)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new ClarifyWordResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					response.ClarifiedWords = Helpers.Occupation.GetClarifiedWords(request.Text, minScore, maxSearchCount, type);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Converts the binary data to ASCII text.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    public ConvertBinaryDataResponse ConvertBinaryDataToText(ConvertBinaryDataRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new ConvertBinaryDataResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					response.Text = Repositories.Lens.CovertBinaryData(request.BinaryFileData, request.ContentType);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the onet keywords.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public OnetKeywordsResponse GetOnetKeywords(OnetKeywordsRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new OnetKeywordsResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					response.Keywords = Helpers.Occupation.GetOnetKeywords(request.OnetCode);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the onet tasks.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public OnetTaskResponse GetOnetTasks(OnetTaskRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new OnetTaskResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					response.OnetTasks = Helpers.Occupation.GetOnetTasks(Convert.ToInt64(request.Criteria.OnetId), request.UserContext.Culture, request.Criteria.ListSize);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Search Lens.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public LensSearchResponse LensSearch(LensSearchRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new LensSearchResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.ClientTag | Validate.SessionId | Validate.License | Validate.AccessToken))
					return response;

				try
				{
					response.Postings = Helpers.Search.SearchPostings(request.Criteria, manualSearch: request.ManualSearch);		
					if (AppSettings.Module.IsIn(FocusModules.Career, FocusModules.CareerExplorer))
					{
						if (request.ManualSearch)
						{
							LogAction(request, ActionTypes.LensJobSearch, typeof (Person).Name, RuntimeContext.CurrentRequest.UserContext.PersonId);
						}
					}
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the resume search session.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public GetResumeSearchSessionResponse GetResumeSearchSession(GetResumeSearchSessionRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new GetResumeSearchSessionResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					response.Resumes = Helpers.Search.GetResumeSearchSession(request.SearchId, request.StarRating);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}
	}
}
