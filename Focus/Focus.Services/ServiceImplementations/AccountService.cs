﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

using Focus.Common.Models;
using Focus.Core;
using Focus.Core.Criteria;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.EmailTemplate;
using Focus.Core.IntegrationMessages;
using Focus.Core.Messages.AccountService;
using Focus.Core.Messages.EmployerService;
using Focus.Core.Messages.JobService;
using Focus.Core.Messages.ResumeService;
using Focus.Core.Messages.UtilityService;
using Focus.Core.Models;
using Focus.Core.Models.Career;
using Focus.Core.Models.Integration;
using Focus.Core.Views;
using Focus.Data.Core.Entities;
using Focus.Services.Core;
using Focus.Services.DtoMappers;
using Focus.Services.Messages;
using Focus.Services.Queries;
using Focus.Services.ServiceContracts;
using Focus.Services.Mappers;
using Focus.Services.Utilities;
using Focus.Services.Views;
using Mindscape.LightSpeed;
using Mindscape.LightSpeed.Querying;

using AcknowledgementType = Focus.Core.Messages.AcknowledgementType;
using Employee = Focus.Data.Core.Entities.Employee;
using Employer = Focus.Data.Core.Entities.Employer;
using ServiceRequest = Focus.Core.Messages.ServiceRequest;
using ServiceResponse = Focus.Core.Messages.ServiceResponse;

using Framework.Authentication;
using Framework.Core;
using Framework.Logging;

#endregion

namespace Focus.Services.ServiceImplementations
{
	public class AccountService : ServiceBase, IAccountService
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="AccountService" /> class.
		/// </summary>
		public AccountService()
			: this(null)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="AccountService" /> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		public AccountService(IRuntimeContext runtimeContext)
			: base(runtimeContext)
		{ }

		/// <summary>
		/// Blocks or unblocks a person
		/// </summary>
		/// <param name="request">The request indicating the person and whether to block/unblock them.</param>
		/// <returns>A response indicating whether they were successfully blocked/unblocked</returns>
		public BlockUnblockPersonResponse BlockUnblockPerson(BlockUnblockPersonRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new BlockUnblockPersonResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var user = Repositories.Core.Users.FirstOrDefault(u => u.PersonId == request.PersonId);

					if (user.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.UserNotFound));
						Logger.Info(request.LogData(), string.Format("The User was not found for person id '{0}'.", request.PersonId));
						return response;
					}

					if (request.DeactivateJobSeeker && (user.UserType & UserTypes.Career) != UserTypes.Career)
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.InvalidUserType));
						Logger.Info(request.LogData(), string.Format("Person '{0}' is not a job seeker", request.PersonId));
						return response;
					}

					if (user.Blocked != request.Block)
					{
						user.Blocked = request.Block;

						// Reset blocked reason and password attempts
						if (!request.Block)
						{
							user.BlockedReason = null;
							user.PasswordAttempts = 0;
						}

						if (request.DeactivateJobSeeker && (user.UserType & UserTypes.Career) == UserTypes.Career)
						{
							user.ExternalId = null;
							user.Person.EmailAddress = string.Format("{0}-DEACTIVATEDON{1}", user.Person.EmailAddress, DateTime.Now.ToString("dMMMyyyy"));
						}

						// If the staff member being deactivated is the default administrator of any offices then remove them as default admin
						var staffIsDefaultAdmin = Repositories.Core.Offices.Where(x => x.DefaultAdministratorId.Equals(request.PersonId));

						if (staffIsDefaultAdmin.Any())
						{
							var changes = new Dictionary<string, object> { { "DefaultAdministratorId", null } };

							var updateQuery = new Query(typeof(Office), Entity.Attribute("DefaultAdministratorId") == request.PersonId);
							Repositories.Core.Update(updateQuery, changes);
						}

						// Check if this is a job seeker
						if ((user.UserType & UserTypes.Career) == UserTypes.Career)
						{
							if (request.Block)
							{
								// This method sets enabled = false, unregisters the default resume from lens and marks the resume as not searchable on the table and in the xml
								InactivateJobSeeker(request, request.PersonId, AccountDisabledReason.BlockedByStaffRequest, request.DeactivateJobSeeker, false);
							}
							else
							{
								Helpers.Resume.ResetSearchableDefaultResumeForPerson(request, user.PersonId);
							}
						}

						Repositories.Core.SaveChanges(true);

						var actionType = request.Block ? ActionTypes.BlockUser : ActionTypes.UnblockUser;
						var blockReason = request.Block ? request.ReasonText : null;
						LogAction(request, actionType, typeof(Person).Name, request.PersonId, user.Id, additionalDetails:blockReason);

						response.Success = true;
					}
					else
					{
						response.Success = false;
					}
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Changes the password for the user.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public ChangePasswordResponse ChangePassword(ChangePasswordRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new ChangePasswordResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var user = Repositories.Core.FindById<User>(request.UserContext.UserId);
					if (user == null)
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.UnableToChangePassword));
						Logger.Info(request.LogData(), string.Format("Unable to change password for user '{0}' as was unable to locate user.", request.UserContext.UserId));

						return response;
					}

				  if (user.IsClientAuthenticated)
				  {
				    // Change password via client
				    var changeUserPasswordRequest = new ChangeUserPasswordRequest()
				    {
				      NewPassword = request.NewPassword,
				      OldPassword = request.CurrentPassword
				    };
				    if (request.UserContext.IsNotNull())
				    {
				      changeUserPasswordRequest.ExternalAdminId = request.UserContext.ExternalUserId;
				      changeUserPasswordRequest.ExternalOfficeId = request.UserContext.ExternalOfficeId;
				      changeUserPasswordRequest.ExternalPassword = request.UserContext.ExternalPassword;
				      changeUserPasswordRequest.ExternalUsername = request.UserContext.ExternalUserName;
				      changeUserPasswordRequest.UserName = request.UserContext.ExternalUserName;
				    }

				    var authenticationResponse = Repositories.Integration.ChangeUserPassword(changeUserPasswordRequest);

				    if (authenticationResponse.Outcome != IntegrationOutcome.Success)
				    {
				      response.SetFailure(FormatErrorMessage(request, ErrorTypes.UnableToChangePassword));
				      Logger.Info(request.LogData(),
				        string.Format("Unable to change password for user {0}. Client application reported : {1}.",
				          request.UserContext.UserId, authenticationResponse.Exception));

				      return response;
				    }
				  }
				  else
				  {
				    if (!request.UserContext.IsShadowingUser)
				    {
				      var currentPassword = new Password(AppSettings, request.CurrentPassword, user.PasswordSalt, false);
				      if (user.PasswordHash != currentPassword.Hash)
				      {
				        response.SetFailure(ErrorTypes.CurrentPasswordDoesNotMatch,
				          FormatErrorMessage(request, ErrorTypes.CurrentPasswordDoesNotMatch));
				        Logger.Info(request.LogData(),
				          string.Format("Unable to change password for user {0}. Current password doesn't match.",
				            request.UserContext.UserId));

				        return response;
				      }
				    }
				  }

				  var newPassword = new Password(AppSettings, request.NewPassword, user.PasswordSalt);
					user.PasswordHash = newPassword.Hash;
	        user.MigratedPasswordHash = null;

					Repositories.Core.SaveChanges();

					if ((user.UserType & UserTypes.Career) == UserTypes.Career)
						Helpers.SelfService.PublishSelfServiceActivity(ActionTypes.ChangePassword, request.UserContext.ActionerId, user.Id, user.PersonId);

					// Log the action
					LogAction(request, ActionTypes.ChangePassword, user);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.UnableToChangePassword), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Validates an Employer.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public ValidateEmployerResponse ValidateEmployer(ValidateEmployerRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new ValidateEmployerResponse(request);

				// Validate client tag, session only & license
				if (!ValidateRequest(request, response, Validate.ClientTag | Validate.SessionId | Validate.License | Validate.AccessToken))
					return response;

				try
				{
					// Search to see if we already have the employer in the Focus database for this client and fein (or use EmployerId for Education)

					var employer = request.EmployerId.HasValue
						? Repositories.Core.Employers.FirstOrDefault(x => x.Id == request.EmployerId)
						: Repositories.Core.Employers.FirstOrDefault(x => x.FederalEmployerIdentificationNumber == request.FederalEmployerIdentificationNumber && (!x.Archived));

					ValidatedEmployerView validatedEmployerView = null;
					// check client
					if (employer.IsNotNull())
					{
						// Set the Employer
						validatedEmployerView = new ValidatedEmployerView
						{
							Id = employer.Id,
							Name = employer.Name,
							ApprovalStatus = employer.ApprovalStatus,
							Url = employer.Url,
							PrimaryPhone = employer.PrimaryPhone,
							PrimaryPhoneExtension = employer.PrimaryPhoneExtension,
							PrimaryPhoneType = employer.PrimaryPhoneType,
							AlternatePhone1 = employer.AlternatePhone1,
							AlternatePhone1Type = employer.AlternatePhone1Type,
							AlternatePhone2 = employer.AlternatePhone2,
							AlternatePhone2Type = employer.AlternatePhone2Type,
							FederalEmployerIdentificationNumber = employer.FederalEmployerIdentificationNumber,
																						LegalName = employer.LegalName,
							IsValidFederalEmployerIdentificationNumber = employer.IsValidFederalEmployerIdentificationNumber,
							StateEmployerIdentificationNumber = employer.StateEmployerIdentificationNumber,
							OwnershipTypeId = employer.OwnershipTypeId,
							AccountTypeId = employer.AccountTypeId,
							IndustrialClassification = employer.IndustrialClassification,
							IsRegistrationComplete = employer.IsRegistrationComplete,
							NoOfEmployees = employer.NoOfEmployees,
							CompleteDataFromIntegrationClient = false,
							CompleteDataFromFocus = true
						};

						// Set the Employer Address
						if (employer.EmployerAddresses.Count > 0)
						{
							validatedEmployerView.AddressId = employer.EmployerAddresses[0].Id;
							validatedEmployerView.AddressLine1 = employer.EmployerAddresses[0].Line1;
							validatedEmployerView.AddressLine2 = employer.EmployerAddresses[0].Line1;
							validatedEmployerView.AddressTownCity = employer.EmployerAddresses[0].TownCity;
							validatedEmployerView.AddressStateId = employer.EmployerAddresses[0].StateId;
							validatedEmployerView.AddressCountyId = employer.EmployerAddresses[0].CountyId;
							validatedEmployerView.AddressPostcodeZip = employer.EmployerAddresses[0].PostcodeZip;
							validatedEmployerView.AddressCountryId = employer.EmployerAddresses[0].CountryId;
						}

						if (employer.BusinessUnits.Count > 0)
						{
							var primaryBusinessUnit = (employer.BusinessUnits.Count(x => x.IsPrimary) > 0) ? employer.BusinessUnits.First(x => x.IsPrimary) : employer.BusinessUnits[0];
							validatedEmployerView.BusinessUnitId = primaryBusinessUnit.Id;
							validatedEmployerView.BusinessUnit = primaryBusinessUnit.Name;

							if (primaryBusinessUnit.BusinessUnitDescriptions.Count(x => x.IsPrimary) > 0)
							{
								var description = primaryBusinessUnit.BusinessUnitDescriptions.FirstOrDefault(x => x.IsPrimary);
								if (description.IsNotNull())
								{
									validatedEmployerView.DescriptionId = description.Id;
									validatedEmployerView.Description = description.Description;
								}
							}
						}
					}
					else
					{
						// Search the external FEIN database
						ValidatedEmployerView clientValidatedEmployerView = null;
						var employerRequest = new GetEmployerRequest
						{
							FEIN = request.FederalEmployerIdentificationNumber,
							UserId = request.UserContext.UserId
						};
						var employerResponse = Repositories.Integration.GetEmployer(employerRequest);


						if (employerResponse.Employer.IsNotNull())
						{
							var naics = employerResponse.Employer.Naics;
							if (naics.IsNotNullOrEmpty())
							{
								var lookupNaics = Repositories.Library.NAICS.FirstOrDefault(n => n.Code == naics);
								if (lookupNaics.IsNotNull())
									naics = string.Concat(naics, " - ", lookupNaics.Name);
							}

							clientValidatedEmployerView = new ValidatedEmployerView
							{
								Name = employerResponse.Employer.Name,
																								LegalName = employerResponse.Employer.LegalName,
								Description = employerResponse.Employer.BusinessDescription,
								FederalEmployerIdentificationNumber = employerResponse.Employer.FederalEmployerIdentificationNumber,
								IsValidFederalEmployerIdentificationNumber = employerResponse.Employer.IsValidFederalEmployerIdentificationNumber,
								StateEmployerIdentificationNumber = employerResponse.Employer.StateEmployerIdentificationNumber,
								Url = employerResponse.Employer.Url,
								PrimaryPhone = employerResponse.Employer.Phone,
								PrimaryPhoneExtension = employerResponse.Employer.PhoneExt,
								PrimaryPhoneType = "Phone",
								AlternatePhone1 = employerResponse.Employer.AltPhone,
								AlternatePhone1Type = "Phone",
								IndustrialClassification = naics,
								IsRegistrationComplete = employerResponse.Employer.IsRegistrationComplete,
								ExternalId = employerResponse.Employer.ExternalId,
								OwnershipTypeId = employerResponse.Employer.Owner,
								AccountTypeId = employerResponse.Employer.AccountType,
								NoOfEmployees = employerResponse.Employer.NoOfEmployees,
								CompleteDataFromIntegrationClient = !employerResponse.Employer.MoreDetailsRequired
							};

							if (employerResponse.Employer.Address.IsNotNull())
							{
								clientValidatedEmployerView.AddressLine1 = employerResponse.Employer.Address.AddressLine1;
								clientValidatedEmployerView.AddressLine2 = employerResponse.Employer.Address.AddressLine2;
								clientValidatedEmployerView.AddressTownCity = employerResponse.Employer.Address.City;
								clientValidatedEmployerView.AddressPostcodeZip = employerResponse.Employer.Address.PostCode;
								clientValidatedEmployerView.AddressCountyId = employerResponse.Employer.Address.CountyId != 0
									? employerResponse.Employer.Address.CountyId
									: Helpers.Lookup.GetLookup(LookupTypes.Counties).Where(s => s.ExternalId == employerResponse.Employer.Address.ExternalCountyId).Select(s => s.Id).FirstOrDefault();
								clientValidatedEmployerView.AddressStateId = employerResponse.Employer.Address.StateId != 0
									? employerResponse.Employer.Address.StateId
									: Helpers.Lookup.GetLookup(LookupTypes.States).Where(s => s.ExternalId == employerResponse.Employer.Address.ExternalStateId).Select(s => s.Id).FirstOrDefault();
								clientValidatedEmployerView.AddressCountryId = employerResponse.Employer.Address.CountryId != 0
									? employerResponse.Employer.Address.CountryId
									: Helpers.Lookup.GetLookup(LookupTypes.Countries).Where(s => s.ExternalId == employerResponse.Employer.Address.ExternalCountryId).Select(s => s.Id).FirstOrDefault();
							}
						}

						if (validatedEmployerView.IsNotNull() && clientValidatedEmployerView.IsNotNull())
						{
							validatedEmployerView.Name = clientValidatedEmployerView.Name;
							validatedEmployerView.ExternalId = clientValidatedEmployerView.ExternalId;
							validatedEmployerView.StateEmployerIdentificationNumber = clientValidatedEmployerView.StateEmployerIdentificationNumber;
						}

						if (validatedEmployerView.IsNull() && clientValidatedEmployerView.IsNotNull())
							validatedEmployerView = clientValidatedEmployerView;
					}
					if (validatedEmployerView.IsNotNull())
						response.ValidatedEmployer = validatedEmployerView;
					else
					{
						// Log the fact that we couldn't find one and return nothing
						Logger.Warning(request.LogData(), string.Format("Unable to validate employer with an FEIN of '{0}'. ", request.FederalEmployerIdentificationNumber));
					}

					LogAction(request, ActionTypes.ValidateEmployer, "Employer(FEIN)", null, null, null, "FEIN : " + request.FederalEmployerIdentificationNumber);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Registers a new Talent user
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public RegisterTalentUserResponse RegisterTalentUser(RegisterTalentUserRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new RegisterTalentUserResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.ClientTag | Validate.SessionId | Validate.License | Validate.AccessToken))
					return response;

				// 
				try
				{
					#region Check user doesn't exist

					// This is the SSO username will b
					if (request.AccountUserName.IsNotNullOrEmpty())
					{
						if (Repositories.Core.Users.Count(x => x.UserName == request.AccountUserName) > 0)
						{
							response.SetFailure(ErrorTypes.UserNameAlreadyExists, FormatErrorMessage(request, ErrorTypes.UserNameAlreadyExists));
							Logger.Info(request.LogData(), string.Format("A user with user name '{0}' already exists.", request.AccountUserName));

							return response;
						}
					}

					#endregion

					var initialApprovalStatus = request.Module == FocusModules.Assist ? ApprovalStatuses.Approved : request.InitialApprovalStatus;

					#region Find the client employer for which the account is being created

					if (request.Employer.FederalEmployerIdentificationNumber.IsNullOrEmpty())
						request.Employer.FederalEmployerIdentificationNumber = Guid.NewGuid().ToString();

					Employer employer;

					if (request.Employer.Id.IsNotNull() && request.Employer.Id > 0)
					{
						employer = Repositories.Core.Employers.FirstOrDefault(x => x.Id == request.Employer.Id);

						// If we cant find the employer with these Id's something is wrong
						if (employer.IsNull())
						{
							response.SetFailure(FormatErrorMessage(request, ErrorTypes.EmployerNotFound));
							Logger.Info(request.LogData(), string.Format("The employer '{0}' was not found.", request.Employer.Name));

							return response;
						}
					}
					else
					{
						employer = Repositories.Core.Employers.FirstOrDefault(x => x.FederalEmployerIdentificationNumber == request.Employer.FederalEmployerIdentificationNumber && (!x.Archived));
					}

					#endregion

					#region This means that the employer cannot be found by that FEIN / Id so create one

					var isNewEmployer = false;
					if (employer.IsNull())
					{
						isNewEmployer = true;

						#region Remove any formatting of the phone numbers

						if (request.Employer.PrimaryPhone.IsNotNullOrEmpty())
							request.Employer.PrimaryPhone = new string(request.Employer.PrimaryPhone.Where(char.IsDigit).ToArray());

						if (request.Employer.AlternatePhone1.IsNotNullOrEmpty())
							request.Employer.AlternatePhone1 = new string(request.Employer.AlternatePhone1.Where(char.IsDigit).ToArray());

						if (request.Employer.AlternatePhone2.IsNotNullOrEmpty())
							request.Employer.AlternatePhone2 = new string(request.Employer.AlternatePhone2.Where(char.IsDigit).ToArray());

						#endregion

						employer = request.Employer.CopyTo();
						employer.LegalName = request.Employer.Name;
						employer.ExternalId = request.EmployerExternalId;
						employer.ApprovalStatus = initialApprovalStatus
						                          ?? (request.Employer.IsValidFederalEmployerIdentificationNumber ? ApprovalStatuses.Approved : ApprovalStatuses.WaitingApproval);

					}

					// we may have some data from the client so we override our values
					if (request.Employer.StateEmployerIdentificationNumber.IsNotNullOrEmpty())
					{
						employer.StateEmployerIdentificationNumber = request.Employer.StateEmployerIdentificationNumber;
						employer.Name = request.Employer.Name;
					}

					#endregion

					#region Censorship check

					var stringBuilder = new StringBuilder();
					var censoredRedWords = new StringBuilder();
					var censoredYellowWords = new StringBuilder();

					if (request.Module == FocusModules.Talent && AppSettings.NewEmployerCensorshipFiltering)
					{
						// Censorship check employer only if it's new
						CensorshipModel censorshipModel;
						if (employer.ApprovalStatus == ApprovalStatuses.WaitingApproval)
						{
							#region Employer

							var employerProperties = employer.GetType().GetProperties(BindingFlags.DeclaredOnly |
							                                                          BindingFlags.Public |
							                                                          BindingFlags.Instance);

							foreach (var propertyInfo in employerProperties)
							{
								stringBuilder.Append(" " + propertyInfo.GetValue(employer, null));
							}

							var employerText = stringBuilder.ToString();
							censorshipModel = ProfanityCheck(employerText);

							if (censorshipModel.RedWords.IsNotNullOrEmpty())
							{
								censoredRedWords.Append(censorshipModel.RedWords);
								response.FailedCensorshipCheck = true;
							}

							if (censorshipModel.YellowWords.IsNotNullOrEmpty())
							{
								censoredYellowWords.Append(censorshipModel.YellowWords);
								response.FailedCensorshipCheck = true;
							}

							#endregion
						}

						#region BusinessUnit

						if (request.BusinessUnit.IsNotNull())
						{
							if (request.BusinessUnit.Url.IsNullOrEmpty()) request.BusinessUnit.Url = null;
							stringBuilder = new StringBuilder();

							var buProperties = request.BusinessUnit.GetType().GetProperties(BindingFlags.DeclaredOnly |
							                                                                BindingFlags.Public |
							                                                                BindingFlags.Instance);

							foreach (var propertyInfo in buProperties)
							{
								stringBuilder.Append(" " + propertyInfo.GetValue(request.BusinessUnit, null));
							}

							var buText = stringBuilder.ToString();
							censorshipModel = ProfanityCheck(buText);

							if (censorshipModel.RedWords.IsNotNullOrEmpty())
							{
								var separator = censoredRedWords.Length > 0 ? "," : "";
								censoredRedWords.Append(separator + censorshipModel.RedWords);
								response.FailedCensorshipCheck = true;
							}

							// Check for yellow words
							if (censorshipModel.YellowWords.IsNotNullOrEmpty())
							{
								var separator = censoredYellowWords.Length > 0 ? "," : "";
								censoredYellowWords.Append(separator + censorshipModel.YellowWords);
								response.FailedCensorshipCheck = true;
							}
						}

						#endregion

						#region Business unit address

						if (request.BusinessUnitAddress.IsNotNull())
						{
							stringBuilder = new StringBuilder();

							var buAddressProperties = request.BusinessUnitAddress.GetType().GetProperties(BindingFlags.DeclaredOnly |
							                                                                              BindingFlags.Public |
							                                                                              BindingFlags.Instance);

							foreach (var propertyInfo in buAddressProperties)
							{
								stringBuilder.Append(" " + propertyInfo.GetValue(request.BusinessUnitAddress, null));
							}

							var buAddressText = stringBuilder.ToString();
							censorshipModel = ProfanityCheck(buAddressText);

							if (censorshipModel.RedWords.IsNotNullOrEmpty())
							{
								var separator = censoredRedWords.Length > 0 ? "," : "";
								censoredRedWords.Append(separator + censorshipModel.RedWords);
								response.FailedCensorshipCheck = true;
							}

							if (censorshipModel.YellowWords.IsNotNullOrEmpty())
							{
								var separator = censoredYellowWords.Length > 0 ? "," : "";
								censoredYellowWords.Append(separator + censorshipModel.YellowWords);
								response.FailedCensorshipCheck = true;
							}
						}

						#endregion

						#region Business unit description

						if (request.BusinessUnitDescription.IsNotNull())
						{
							stringBuilder = new StringBuilder();

							var buDescriptionProperties = request.BusinessUnitDescription.GetType().GetProperties(BindingFlags.DeclaredOnly |
							                                                                                      BindingFlags.Public |
							                                                                                      BindingFlags.Instance);

							foreach (var propertyInfo in buDescriptionProperties)
							{
								stringBuilder.Append(" " + propertyInfo.GetValue(request.BusinessUnitDescription, null));
							}

							var buDescriptionText = stringBuilder.ToString();
							censorshipModel = ProfanityCheck(buDescriptionText);

							if (censorshipModel.RedWords.IsNotNullOrEmpty())
							{
								var separator = censoredRedWords.Length > 0 ? "," : "";
								censoredRedWords.Append(separator + censorshipModel.RedWords);
								response.FailedCensorshipCheck = true;
							}

							if (censorshipModel.YellowWords.IsNotNullOrEmpty())
							{
								var separator = censoredYellowWords.Length > 0 ? "," : "";
								censoredYellowWords.Append(separator + censorshipModel.YellowWords);
								response.FailedCensorshipCheck = true;
							}
						}

						#endregion

						#region Hiring Manager

						if (request.EmployeePerson.IsNotNull())
						{
							stringBuilder = new StringBuilder();

							var personProperties = request.EmployeePerson.GetType().GetProperties(BindingFlags.DeclaredOnly |
							                                                                      BindingFlags.Public |
							                                                                      BindingFlags.Instance);

							foreach (var propertyInfo in personProperties)
							{
								stringBuilder.Append(" " + propertyInfo.GetValue(request.EmployeePerson, null));
							}

							var personText = stringBuilder.ToString();
							censorshipModel = ProfanityCheck(personText);

							if (censorshipModel.RedWords.IsNotNullOrEmpty())
							{
								var separator = censoredRedWords.Length > 0 ? "," : "";
								censoredRedWords.Append(separator + censorshipModel.RedWords);
								response.FailedCensorshipCheck = true;
							}

							if (censorshipModel.YellowWords.IsNotNullOrEmpty())
							{
								var separator = censoredYellowWords.Length > 0 ? "," : "";
								censoredYellowWords.Append(separator + censorshipModel.YellowWords);
								response.FailedCensorshipCheck = true;
							}
						}

						if (request.EmployeeAddress.IsNotNull())
						{
							stringBuilder = new StringBuilder();

							var personAddressProperties = request.EmployeeAddress.GetType().GetProperties(BindingFlags.DeclaredOnly |
							                                                                              BindingFlags.Public |
							                                                                              BindingFlags.Instance);

							foreach (var propertyInfo in personAddressProperties)
							{
								stringBuilder.Append(" " + propertyInfo.GetValue(request.EmployeeAddress, null));
							}

							var personAddressText = stringBuilder.ToString();
							censorshipModel = ProfanityCheck(personAddressText);

							if (censorshipModel.RedWords.IsNotNullOrEmpty())
							{
								var separator = censoredRedWords.Length > 0 ? "," : "";
								censoredRedWords.Append(separator + censorshipModel.RedWords);
								response.FailedCensorshipCheck = true;
							}

							if (censorshipModel.YellowWords.IsNotNullOrEmpty())
							{
								var separator = censoredYellowWords.Length > 0 ? "," : "";
								censoredYellowWords.Append(separator + censorshipModel.YellowWords);
								response.FailedCensorshipCheck = true;
							}
						}

						#endregion
					}

					string redProfanityWords = null;
					if (censoredRedWords.Length > 0)
					{
						var distinctRedWords = censoredRedWords.ToString().Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Distinct();
						redProfanityWords = string.Join(",", distinctRedWords);
					}

					string yellowProfanityWords = null;
					if (censoredYellowWords.Length > 0)
					{
						var distinctYellowWords = censoredYellowWords.ToString().Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Distinct();
						yellowProfanityWords = string.Join(",", distinctYellowWords);
					}

					#endregion

					#region Add / update the employer address

					if (request.EmployerAddress.IsNotNull())
					{
						EmployerAddress employerAddress = null;

						// Try and get the primary address and If this employer has any addresses then we need to check them
						if (employer.EmployerAddresses.Count > 0)
						{
							if (request.EmployerAddress.Id.IsNotNull())
								employerAddress = employer.EmployerAddresses.SingleOrDefault(x => x.Id == request.EmployerAddress.Id);

							if (employerAddress.IsNull())
								employerAddress = employer.EmployerAddresses.FirstOrDefault(x => x.IsPrimary);

							if (employerAddress.IsNull())
								employerAddress = employer.EmployerAddresses.SingleOrDefault(x => x.Line1 == request.EmployerAddress.Line1 &&
								                                                                  x.Line2 == request.EmployerAddress.Line2 &&
								                                                                  x.Line3 == request.EmployerAddress.Line3 &&
								                                                                  x.TownCity == request.EmployerAddress.TownCity &&
								                                                                  x.PostcodeZip == request.EmployerAddress.PostcodeZip &&
								                                                                  x.CountyId == request.EmployerAddress.CountyId &&
								                                                                  x.CountryId == request.EmployerAddress.CountryId &&
								                                                                  x.StateId == request.EmployerAddress.StateId);
						}

						// We now need to create an address if one doesn't exist
						if (employerAddress.IsNull())
						{
							employerAddress = request.EmployerAddress.CopyTo();
						}
						else
						{
							employerAddress.IsPrimary = true;
							employerAddress.Line1 = request.EmployerAddress.Line1;
							employerAddress.Line2 = request.EmployerAddress.Line2;
							employerAddress.Line3 = request.EmployerAddress.Line3;
							employerAddress.TownCity = request.EmployerAddress.TownCity;
							employerAddress.CountyId = request.EmployerAddress.CountyId;
							employerAddress.StateId = request.EmployerAddress.StateId;
							employerAddress.CountryId = request.EmployerAddress.CountryId;
							employerAddress.PostcodeZip = request.EmployerAddress.PostcodeZip;
							employerAddress.PublicTransitAccessible = request.EmployerAddress.PublicTransitAccessible;
						}

						employer.EmployerAddresses.Add(employerAddress);
					}
					else
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.EmployerAddressMissing));
						Logger.Info(request.LogData(), "The employer must have an Employer Address.");

						return response;
					}

					#endregion

					#region Add the business unit

					BusinessUnit businessUnit = null;
					bool isNewBusinessUnit = false;
					if (request.BusinessUnit.IsNotNull())
					{
						if (request.BusinessUnit.Url.IsNullOrEmpty())
							request.BusinessUnit.Url = null;

						if (request.BusinessUnit.Id.HasValue && request.BusinessUnit.Id > 0)
						{
							businessUnit = Repositories.Core.FindById<BusinessUnit>(request.BusinessUnit.Id);

							// If we cant find the business unit with this Id something is wrong
							if (businessUnit.IsNull())
							{
								response.SetFailure(FormatErrorMessage(request, ErrorTypes.BusinessUnitNotFound));
								Logger.Info(request.LogData(), string.Format("The business unit '{0}' was not found.", request.BusinessUnit.Id));

								return response;
							}
						}

						if (businessUnit.IsNull())
						{
							isNewBusinessUnit = true;
							// Add one
							var businessUnitAddress = request.BusinessUnitAddress.CopyTo();
							businessUnitAddress.IsPrimary = true;

							var businessUnitDescription = request.BusinessUnitDescription.CopyTo();
							businessUnitDescription.IsPrimary = true;

							#region Remove any formatting of the phone numbers

							if (request.BusinessUnit.PrimaryPhone.IsNotNullOrEmpty())
								request.BusinessUnit.PrimaryPhone = new string(request.BusinessUnit.PrimaryPhone.Where(char.IsDigit).ToArray());

							if (request.BusinessUnit.AlternatePhone1.IsNotNullOrEmpty())
								request.BusinessUnit.AlternatePhone1 = new string(request.BusinessUnit.AlternatePhone1.Where(char.IsDigit).ToArray());

							if (request.BusinessUnit.AlternatePhone2.IsNotNullOrEmpty())
								request.BusinessUnit.AlternatePhone2 = new string(request.BusinessUnit.AlternatePhone2.Where(char.IsDigit).ToArray());

							#endregion

							businessUnit = request.BusinessUnit.CopyTo();
							if (businessUnit.Name.IsNullOrEmpty()) businessUnit.Name = employer.Name;
							businessUnit.LegalName = businessUnit.LegalName ?? businessUnit.Name;
							businessUnit.IsPrimary = employer.BusinessUnits.IsNullOrEmpty();
							businessUnit.BusinessUnitAddresses.Add(businessUnitAddress);

							var businessUnitApprovalStatus = (AppSettings.NewBusinessUnitApproval == TalentApprovalOptions.NoApproval && employer.ApprovalStatus.IsIn(ApprovalStatuses.None, ApprovalStatuses.Approved))
								? ApprovalStatuses.Approved
								: ApprovalStatuses.WaitingApproval;
							businessUnit.ApprovalStatus = initialApprovalStatus ?? businessUnitApprovalStatus;

							if (businessUnitDescription.IsNotNull() && businessUnitDescription.Description.IsNotNullOrEmpty())
								businessUnit.BusinessUnitDescriptions.Add(businessUnitDescription);

						}

						#region Assign the business unit to the employer

						if (employer.BusinessUnits.IsNullOrEmpty() || (employer.BusinessUnits.All(x => x.Id != businessUnit.Id)))
							employer.BusinessUnits.Add(businessUnit);

						#endregion
					}

					#endregion

					#region Add User and Roles

					User user;

					if (request.User.IsNotNull() && request.User.Id.IsNotNull())
					{
						user = Repositories.Core.FindById<User>(request.User.Id);
                        user.Enabled =
							!(AppSettings.RegistrationCompleteAction == RegistrationCompleteAction.EmailLink &&
							  request.Employer.IsValidFederalEmployerIdentificationNumber &&
							  request.Employer.ApprovalStatus == ApprovalStatuses.Approved &&
							  redProfanityWords.IsNullOrEmpty() && yellowProfanityWords.IsNullOrEmpty()) ||
							request.Module.IsIn(FocusModules.Assist, FocusModules.MigrationService);
						user.IsMigrated = true;
					}
					else
					{

						user = new User
						{
							UserName = request.AccountUserName ?? String.Format("sso-{0}@ci.bgt.com", request.User.ExternalId),
							MigratedPasswordHash = request.MigratedPasswordHash,
							UserType = UserTypes.Talent,
							Enabled = !(AppSettings.RegistrationCompleteAction == RegistrationCompleteAction.EmailLink &&
								  request.Employer.IsValidFederalEmployerIdentificationNumber &&
								  request.Employer.ApprovalStatus == ApprovalStatuses.Approved &&
								  redProfanityWords.IsNullOrEmpty() && yellowProfanityWords.IsNullOrEmpty()) ||
								request.Module.IsIn(FocusModules.Assist, FocusModules.MigrationService),
							IsMigrated = true
						};

                        //FVN-4929
                        //commented since causing issue during saml sso.
                        //issue : replaces the request.User.ExternalId in request with the new user.ExternalId. the "request.User" is updated unwanted while the old values must be retained.
						//request.User = user.AsDto();

						Password password;

						if (AppSettings.SamlEnabled || AppSettings.SamlEnabledForTalent || AppSettings.OAuthEnabled)
						{
							password = new Password(AppSettings, "Pasty99", false);
							user.PasswordSalt = password.Salt;
							user.PasswordHash = password.Hash;
							user.ExternalId = request.User.ExternalId;
							user.ScreenName = request.User.ScreenName;
                            user.RegulationsConsent = true;
						}
						else
						{
							password = new Password(AppSettings, request.AccountPassword);
							user.PasswordSalt = password.Salt;
							user.PasswordHash = password.Hash;
						}

						var talentUserRole = Repositories.Core.Roles.Single(x => x.Key == Constants.RoleKeys.TalentUser);
						user.Roles.Add(talentUserRole);
					}

					if (request.EmployeeExternalId.IsNotNullOrEmpty())
						user.ExternalId = request.EmployeeExternalId;

					#endregion

					#region Create the person record

					Person person;

					if (request.EmployeePerson.Id.IsNotNull())
					{
						person = Repositories.Core.FindById<Person>(request.EmployeePerson.Id);
						person.AssignedToId = request.EmployeePerson.AssignedToId;
						person.EmailAddress = request.EmployeePerson.EmailAddress;
						person.DateOfBirth = request.EmployeePerson.DateOfBirth;
						person.SocialSecurityNumber = request.EmployeePerson.SocialSecurityNumber;
						person.JobTitle = request.EmployeePerson.JobTitle;
						person.EnrollmentStatus = request.EmployeePerson.EnrollmentStatus;
						person.ProgramAreaId = request.EmployeePerson.ProgramAreaId;
						person.DegreeId = request.EmployeePerson.DegreeId;
						person.Manager = request.EmployeePerson.Manager;
						person.ExternalOffice = request.EmployeePerson.ExternalOffice;
					}
					else
					{
						person = request.EmployeePerson.CopyTo();

						if (request.EmployeeEmailAddress.IsNullOrEmpty())
						{
							response.SetFailure(FormatErrorMessage(request, ErrorTypes.PersonEmailAddressMissing));
							Logger.Info(request.LogData(), "Account registration requires an email address.");

							return response;
						}

						person.EmailAddress = request.EmployeeEmailAddress;
					}

					person.User = user;

					#endregion

					#region Check contact details

					var missingContactDetail = "";

					if (request.EmployeePhone.IsNotNullOrEmpty())
					{
						request.EmployeePhone = new string(request.EmployeePhone.Where(char.IsDigit).ToArray());
						person.PhoneNumbers.Add(new PhoneNumber { PhoneType = (PhoneTypes)Enum.Parse(typeof(PhoneTypes), request.EmployeePhoneType, true), IsPrimary = true, Number = request.EmployeePhone, Extension = request.EmployeePhoneExtension });
					}

					else
						missingContactDetail = "Phone number";

					if (request.EmployeeAlternatePhone1.IsNotNullOrEmpty())
					{
						request.EmployeeAlternatePhone1 = new string(request.EmployeeAlternatePhone1.Where(char.IsDigit).ToArray());
						person.PhoneNumbers.Add(new PhoneNumber { PhoneType = (PhoneTypes)Enum.Parse(typeof(PhoneTypes), request.EmployeeAlternatePhone1Type, true), IsPrimary = false, Number = request.EmployeeAlternatePhone1 });
					}

					if (request.EmployeeAlternatePhone2.IsNotNullOrEmpty())
					{
						request.EmployeeAlternatePhone2 = new string(request.EmployeeAlternatePhone2.Where(char.IsDigit).ToArray());
						person.PhoneNumbers.Add(new PhoneNumber { PhoneType = (PhoneTypes)Enum.Parse(typeof(PhoneTypes), request.EmployeeAlternatePhone2Type, true), IsPrimary = false, Number = request.EmployeeAlternatePhone2 });
					}

					if (missingContactDetail.IsNotNullOrEmpty())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.PhoneNumberMissing));
						Logger.Info(request.LogData(), string.Format("Account registration requires a phone and fax numbers, missing {0}.", missingContactDetail));

						return response;
					}

					#endregion

					#region Add the users address and create the employee

					if (request.EmployeeAddress.IsNotNull())
						person.PersonAddresses.Add(request.EmployeeAddress.CopyTo());

					var employee = new Employee
					{
						Person = person,
						Employer = employer,
						ApprovalStatus = initialApprovalStatus
						                 ?? (request.Employer.IsValidFederalEmployerIdentificationNumber && request.Employer.ApprovalStatus == ApprovalStatuses.Approved && redProfanityWords.IsNullOrEmpty() && yellowProfanityWords.IsNullOrEmpty()
							                 ? ApprovalStatuses.Approved
							                 : ApprovalStatuses.WaitingApproval),
						RedProfanityWords = redProfanityWords,
						YellowProfanityWords = yellowProfanityWords
					};

					// if its already waiting approval we dont need to set
					// checks for business units and hiring managers need to be set to waiting approval
					if (initialApprovalStatus.IsNull())
					{
						if (isNewEmployer && AppSettings.NewEmployerApproval != TalentApprovalOptions.NoApproval)
						{
							employer.ApprovalStatus = ApprovalStatuses.WaitingApproval;

							if (AppSettings.NewEmployerApproval == TalentApprovalOptions.ApprovalSystemNotAvailable)
								user.Enabled = false;
						}

						if (employer.ApprovalStatus == ApprovalStatuses.WaitingApproval)
						{
							employee.ApprovalStatus = ApprovalStatuses.WaitingApproval;
						}
						else
						{
							if (!isNewBusinessUnit && !isNewEmployer && AppSettings.NewHiringManagerApproval == TalentApprovalOptions.NoApproval)
							{
								if (redProfanityWords.IsNullOrEmpty() && yellowProfanityWords.IsNullOrEmpty())
									employee.ApprovalStatus = ApprovalStatuses.Approved;
							}
							else if (AppSettings.NewHiringManagerApproval != TalentApprovalOptions.NoApproval)
							{
								employee.ApprovalStatus = ApprovalStatuses.WaitingApproval;

								if (AppSettings.NewHiringManagerApproval == TalentApprovalOptions.ApprovalSystemNotAvailable)
									user.Enabled = false;
								else if (AppSettings.NewHiringManagerApproval == TalentApprovalOptions.ApprovalSystemAvailable)
									user.Enabled = true;
							}
						}

						if (isNewBusinessUnit && AppSettings.NewBusinessUnitApproval != TalentApprovalOptions.NoApproval && businessUnit.IsNotNull())
						{
							employee.ApprovalStatus = ApprovalStatuses.WaitingApproval;
							businessUnit.ApprovalStatus = ApprovalStatuses.WaitingApproval;

							if (AppSettings.NewBusinessUnitApproval == TalentApprovalOptions.ApprovalSystemNotAvailable)
								user.Enabled = false;
						}
						else if (!isNewEmployer && employer.ApprovalStatus.IsIn(ApprovalStatuses.None, ApprovalStatuses.Approved) && isNewBusinessUnit && AppSettings.NewBusinessUnitApproval == TalentApprovalOptions.NoApproval)
						{
							employee.ApprovalStatus = ApprovalStatuses.Approved;
							businessUnit.ApprovalStatus = ApprovalStatuses.Approved;
						}
					}

					#region Assign business unit to employee

					if (businessUnit.IsNotNull())
					{
						var employeeBusinessUnit = new EmployeeBusinessUnit
						{
							BusinessUnit = businessUnit,
							Default = true
						};

						employee.EmployeeBusinessUnits.Add(employeeBusinessUnit);
					}

					#endregion

					#endregion

					// Send data to Client System
					//var employerRequest = new SaveEmployerRequest
					//{
					//  RequestId = request.RequestId,
					//  ClientTag = request.ClientTag,
					//  ExternalUserId = request.UserContext.ExternalUserId,
					//  ExternalUserName = request.UserContext.ExternalUserName,
					//  ExternalPassword = request.UserContext.ExternalPassword,
					//  ExternalOfficeId = request.UserContext.ExternalOfficeId,
					//  Employer = CreateRegistrationIntegrationEmployer(employee, request.UserContext.UserId, request.UserContext.ExternalUserId, request.UserContext.ExternalOfficeId)
					//};

					//employerRequest.Employer.ExternalId = request.EmployerExternalId;

					//if (!request.IgnoreClient && request.Employer.ApprovalStatus == ApprovalStatuses.Approved)
					//{
					//  var employerResponse = Repositories.Client.SaveEmployer(employerRequest);

					//  if (employerResponse.Acknowledgement == Integration.Client.AcknowledgementType.Failure)
					//  {
					//    response.SetFailure(FormatErrorMessage(request, ErrorTypes.SaveEmployerFailed), employerResponse.Exception);
					//    Logger.Error(request.LogData(), response.Message, response.Exception);

					//    return response;
					//  }
					//}

					// Create the employee record
					Repositories.Core.Add(employee);
					Repositories.Core.SaveChanges();

					if (user.Id.IsNotNull())
					{
						Helpers.User.UpdateSecurityQuestions(user.Id, request.SecurityQuestions, false);
					}

					//// update the user createdon and updated on values to match those in the source data
					//if (request.User.IsNotNull() && (request.OverrideUserCreatedOn.IsNotNullOrDefault() || request.OverrideUserUpdatedOn.IsNotNullOrDefault()))
					//{
					//  var changes = new Dictionary<string, object>();

					//  if (request.OverrideUserCreatedOn.IsNotNullOrDefault())
					//    changes.Add("CreatedOn", request.User.CreatedOn);

					//  if (request.OverrideUserUpdatedOn.IsNotNullOrDefault())
					//    changes.Add("UpdatedOn", request.User.UpdatedOn);

					//  if (request.User.CreatedOn == DateTime.MinValue)
					//  {
					//    request.User.CreatedOn = new DateTime(1753, 01, 01);
					//  }

					//  var updateQuery = new Query(typeof(User), Entity.Attribute("Id") == user.Id);
					//  Repositories.Core.Update(updateQuery, changes);
					//  Repositories.Core.SaveChanges();
					//}

					//// update the employer createdon and updated on values to match those in the source data
					//if (request.Employer.IsNotNull() && (request.OverrideEmployerCreatedOn.IsNotNullOrDefault() || request.OverrideEmployerUpdatedOn.IsNotNullOrDefault()))
					//{
					//  var changes = new Dictionary<string, object>();

					//  if (request.OverrideEmployerCreatedOn.IsNotNullOrDefault())
					//    changes.Add("CreatedOn", request.Employer.CreatedOn);

					//  if (request.OverrideEmployerUpdatedOn.IsNotNullOrDefault())
					//    changes.Add("UpdatedOn", request.Employer.UpdatedOn);

					//  var updateQuery = new Query(typeof(Employer), Entity.Attribute("Id") == employer.Id);
					//  Repositories.Core.Update(updateQuery, changes);
					//  Repositories.Core.SaveChanges();
					//}


					#region Get the Office this employer is to be assigned to

					if (AppSettings.OfficesEnabled && request.EmployerAddress.IsNotNull())
					{
						Helpers.Employer.AssignDefaultOffices(employer.Id, request.EmployerAddress.PostcodeZip);
						Repositories.Core.SaveChanges();
					}

					#endregion

					response.EmployeeId = employee.Id;
					response.UserId = user.Id;
					response.EmployerId = employer.Id;
					response.LastLoggedInOn = user.LastLoggedInOn;
					response.IsMigrated = user.IsMigrated;
					response.RegulationsConsent = user.RegulationsConsent;
					response.EmployeeApprovalStatus = employee.ApprovalStatus;

					// Save employer in any integrated client
					if (employer.ApprovalStatus == ApprovalStatuses.Approved)
					{
						employer.PublishEmployerUpdateIntegrationRequest(request.UserContext.ActionerId, RuntimeContext);
						if (employee.ApprovalStatus == ApprovalStatuses.Approved)
							user.PublishUpdateEmployeeStatusRequest(false, RuntimeContext, employer);
					}

					// Log the action
					LogAction(request, ActionTypes.RegisterAccount, user);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Updates an existing Talent user (along with associated BU and employer). Created mainly for the import process
		/// </summary>
		/// <param name="request">The request holding details of the Talent user.</param>
		/// <returns></returns>
		public RegisterTalentUserResponse UpdateTalentUser(RegisterTalentUserRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new RegisterTalentUserResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.ClientTag | Validate.SessionId | Validate.License | Validate.AccessToken))
					return response;

				try
				{
					if (request.UpdateExistingEmployeeId.GetValueOrDefault(0) == 0)
					{
						response.SetFailure(ErrorTypes.EmployerIdsNotSpecified, FormatErrorMessage(request, ErrorTypes.EmployerIdsNotSpecified));
						Logger.Info(request.LogData(), @"Id not specified for updating Employee");

						return response;
					}

					var employee = Repositories.Core.Employees.FirstOrDefault(e => e.Id == request.UpdateExistingEmployeeId);

					if (employee.IsNull())
					{
						response.SetFailure(ErrorTypes.EmployeeNotFound, FormatErrorMessage(request, ErrorTypes.EmployeeNotFound));
						Logger.Info(request.LogData(), string.Format("Employee not found for Id '{0}' already exists.", request.UpdateExistingEmployeeId));

						return response;
					}

					// Check user doesn't exist
					if (!(AppSettings.OAuthEnabled || AppSettings.SamlEnabled || AppSettings.SamlEnabledForTalent))
					{
						if (Repositories.Core.Users.Any(x => x.UserName == request.AccountUserName && x.PersonId != employee.PersonId))
						{
							response.SetFailure(ErrorTypes.UserNameAlreadyExists, FormatErrorMessage(request, ErrorTypes.UserNameAlreadyExists));
							Logger.Info(request.LogData(), string.Format("A user with user name '{0}' already exists.", request.AccountUserName));

							return response;
						}
					}

					// Find the employer for which the account is being updated
					Employer employer;

					if (request.Employer.Id.GetValueOrDefault(0) > 0)
					{
						employer = Repositories.Core.Employers.FirstOrDefault(employerRecord => employerRecord.Id == request.Employer.Id);
					}
					else
					{
						employer = (from personRecord in Repositories.Core.Persons
							join employeePersonRecord in Repositories.Core.Employees
								on personRecord.Id equals employeePersonRecord.PersonId
							join employeeRecord in Repositories.Core.Employees
								on employeePersonRecord.Id equals employeeRecord.Id
							join employerRecord in Repositories.Core.Employers
								on employeeRecord.EmployerId equals employerRecord.Id
							where personRecord.Id == employee.PersonId
							select employerRecord).FirstOrDefault();
					}

					if (employer.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.EmployerNotFound));
						Logger.Info(request.LogData(), string.Format("The employer '{0}' was not found.", request.Employer.Name));

						return response;
					}

					// Remove any formatting of the phone numbers
					if (request.Employer.PrimaryPhone.IsNotNullOrEmpty())
						request.Employer.PrimaryPhone = new string(request.Employer.PrimaryPhone.Where(char.IsDigit).ToArray());

					if (request.Employer.AlternatePhone1.IsNotNullOrEmpty())
						request.Employer.AlternatePhone1 = new string(request.Employer.AlternatePhone1.Where(char.IsDigit).ToArray());

					if (request.Employer.AlternatePhone2.IsNotNullOrEmpty())
						request.Employer.AlternatePhone2 = new string(request.Employer.AlternatePhone2.Where(char.IsDigit).ToArray());

					// Amend the Employer fields
					request.Employer.CopyTo(employer);

					// Update the employer address
					if (request.EmployerAddress.IsNotNull())
					{
						EmployerAddress employerAddress = null;

						// Try and get the primary address
						if (request.EmployerAddress.Id.IsNotNull())
							employerAddress = employer.EmployerAddresses.SingleOrDefault(x => x.Id == request.EmployerAddress.Id);

						if (employerAddress.IsNull())
							employerAddress = employer.EmployerAddresses.FirstOrDefault(x => x.IsPrimary);

						if (employerAddress.IsNull())
						{
							response.SetFailure(FormatErrorMessage(request, ErrorTypes.EmployerAddressMissing));
							Logger.Info(request.LogData(), string.Format("The address for employer '{0}' was not found.", request.Employer.Name));

							return response;
						}

						// Update the address
						request.EmployerAddress.CopyTo(employerAddress);
						employerAddress.EmployerId = employer.Id;
					}

					// Update the business unit
					if (request.BusinessUnit.IsNotNull())
					{
						BusinessUnit businessUnit = null;

						// Try and get the primary address
						if (request.BusinessUnit.Id.IsNotNull())
							businessUnit = employer.BusinessUnits.SingleOrDefault(x => x.Id == request.BusinessUnit.Id);

						if (businessUnit.IsNull())
							businessUnit = employer.BusinessUnits.FirstOrDefault(x => x.IsPrimary);

						if (businessUnit.IsNull())
						{
							response.SetFailure(FormatErrorMessage(request, ErrorTypes.BusinessUnitNotFound));
							Logger.Info(request.LogData(), string.Format("The business unit for employer '{0}' was not found.", request.Employer.Name));

							return response;
						}

						// Remove any formatting of the phone numbers
						if (request.BusinessUnit.PrimaryPhone.IsNotNullOrEmpty())
							request.BusinessUnit.PrimaryPhone = new string(request.BusinessUnit.PrimaryPhone.Where(char.IsDigit).ToArray());

						if (request.BusinessUnit.AlternatePhone1.IsNotNullOrEmpty())
							request.BusinessUnit.AlternatePhone1 = new string(request.BusinessUnit.AlternatePhone1.Where(char.IsDigit).ToArray());

						if (request.BusinessUnit.AlternatePhone2.IsNotNullOrEmpty())
							request.BusinessUnit.AlternatePhone2 = new string(request.BusinessUnit.AlternatePhone2.Where(char.IsDigit).ToArray());

						// Update the address
						request.BusinessUnit.CopyTo(businessUnit);
						businessUnit.EmployerId = employer.Id;

						// Update the business unit description
						if (request.BusinessUnitDescription.IsNotNull())
						{
							var businessUnitDescription = businessUnit.BusinessUnitDescriptions.FirstOrDefault(bud => bud.IsPrimary);
							if (businessUnitDescription.IsNotNull() && businessUnitDescription.Description != request.BusinessUnitDescription.Description)
								businessUnitDescription.Description = request.BusinessUnitDescription.Description;
						}
					}

					// Update User
					var user = Repositories.Core.Users.Single(u => u.PersonId == employee.PersonId);
					user.UserName = request.AccountUserName;

					if (request.EmployeeExternalId.IsNotNullOrEmpty())
						user.ExternalId = request.EmployeeExternalId;

					if (request.AccountPassword.IsNotNullOrEmpty())
					{
						var password = new Password(AppSettings, request.AccountPassword);

						user.PasswordSalt = password.Salt;
						user.PasswordHash = password.Hash;
					}

					if (request.MigratedPasswordHash.IsNotNullOrEmpty())
						user.MigratedPasswordHash = request.MigratedPasswordHash;

					// Update the person record
					var person = Repositories.Core.FindById<Person>(employee.PersonId);
					person.FirstName = request.EmployeePerson.FirstName;
					person.MiddleInitial = request.EmployeePerson.MiddleInitial;
					person.LastName = request.EmployeePerson.LastName;
					person.AssignedToId = request.EmployeePerson.AssignedToId;
					person.EmailAddress = request.EmployeePerson.EmailAddress;
					person.DateOfBirth = request.EmployeePerson.DateOfBirth;
					person.SocialSecurityNumber = request.EmployeePerson.SocialSecurityNumber;
					person.JobTitle = request.EmployeePerson.JobTitle;
					person.EnrollmentStatus = request.EmployeePerson.EnrollmentStatus;
					person.ProgramAreaId = request.EmployeePerson.ProgramAreaId;
					person.DegreeId = request.EmployeePerson.DegreeId;
					person.Manager = request.EmployeePerson.Manager;
					person.ExternalOffice = request.EmployeePerson.ExternalOffice;

					// Update contact phone numbers
					if (request.EmployeePhone.IsNotNullOrEmpty())
					{
						Repositories.Core.Remove<PhoneNumber>(pn => pn.PersonId == person.Id);

						request.EmployeePhone = new string(request.EmployeePhone.Where(char.IsDigit).ToArray());
						person.PhoneNumbers.Add(new PhoneNumber { PhoneType = (PhoneTypes)Enum.Parse(typeof(PhoneTypes), request.EmployeePhoneType, true), IsPrimary = true, Number = request.EmployeePhone, Extension = request.EmployeePhoneExtension });

						if (request.EmployeeAlternatePhone1.IsNotNullOrEmpty())
						{
							request.EmployeeAlternatePhone1 = new string(request.EmployeeAlternatePhone1.Where(char.IsDigit).ToArray());
							person.PhoneNumbers.Add(new PhoneNumber { PhoneType = (PhoneTypes)Enum.Parse(typeof(PhoneTypes), request.EmployeeAlternatePhone1Type, true), IsPrimary = false, Number = request.EmployeeAlternatePhone1 });
						}

						if (request.EmployeeAlternatePhone2.IsNotNullOrEmpty())
						{
							request.EmployeeAlternatePhone2 = new string(request.EmployeeAlternatePhone2.Where(char.IsDigit).ToArray());
							person.PhoneNumbers.Add(new PhoneNumber { PhoneType = (PhoneTypes)Enum.Parse(typeof(PhoneTypes), request.EmployeeAlternatePhone2Type, true), IsPrimary = false, Number = request.EmployeeAlternatePhone2 });
						}
					}

					// Update contact address
					if (request.EmployeeAddress.IsNotNull())
					{
						Repositories.Core.Remove<PersonAddress>(pn => pn.PersonId == person.Id);
						person.PersonAddresses.Add(request.EmployeeAddress.CopyTo());
					}


					// Create the employee record
					Repositories.Core.SaveChanges(true);

					// Get the Office this employer is to be assigned to
					var offices = new List<Office>();
					var officeUnassigned = (bool?)null;

					// Get the office that deals with the postcode of the employer
					if (AppSettings.OfficesEnabled && request.EmployerAddress.IsNotNull())
						offices = Repositories.Core.Offices.Where(x => x.AssignedPostcodeZip.Contains(MiscUtils.SafeSubstring(request.EmployerAddress.PostcodeZip, 0, 5))).ToList();

					if (offices.IsNull() || offices.Count == 0)
					{
						// Get default Office
						var defaultOffice = Repositories.Core.Offices.FirstOrDefault(x => (x.DefaultType & OfficeDefaultType.Employer) == OfficeDefaultType.Employer);

						if (defaultOffice.IsNotNull())
						{
							offices.Add(defaultOffice);
							officeUnassigned = true;
						}
					}

					if (offices.IsNotNull() && offices.Count > 0)
					{
						Repositories.Core.Remove<EmployerOfficeMapper>(e => e.EmployerId == employer.Id);

						var employerOffices = from o in offices
							select new EmployerOfficeMapper { OfficeId = o.Id, EmployerId = employer.Id, OfficeUnassigned = officeUnassigned };

						var employerOfficesSaved = new List<EmployerOfficeMapper>();

						foreach (var employerOffice in employerOffices)
						{
							Repositories.Core.Add(employerOffice);

							employerOfficesSaved.Add(employerOffice);
						}

						Repositories.Core.SaveChanges(true);

						// Log the action
						employerOfficesSaved.ForEach(ed => LogAction(request, ActionTypes.AddEmployerOffice, typeof(EmployerOfficeMapper).Name, ed.Id, request.UserContext.UserId));
					}

					response.EmployeeId = employee.Id;
					response.UserId = user.Id;
					response.EmployerId = employer.Id;
					response.LastLoggedInOn = user.LastLoggedInOn;
					response.IsMigrated = user.IsMigrated;
					response.RegulationsConsent = user.RegulationsConsent;
					response.EmployeeApprovalStatus = employee.ApprovalStatus;

					user.PublishUserUpdateIntegrationRequest(request.UserContext.UserId, RuntimeContext);

					// Log the action
					LogAction(request, ActionTypes.RegisterAccount, user);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Sends the account activation email.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public SendAccountActivationEmailResponse SendAccountActivationEmail(SendAccountActivationEmailRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new SendAccountActivationEmailResponse(request);

				// Validate client tag & session only
				if (!ValidateRequest(request, response, Validate.ClientTag | Validate.SessionId | Validate.License | Validate.AccessToken))
					return response;

				var email = request.Email;
				var name = request.Name;

				if (request.UserName.IsNotNullOrEmpty())
				{
					var user = Repositories.Core.Users.FirstOrDefault(x => x.UserName == request.UserName);

					if (user.IsNotNull())
					{
						email = user.Person.EmailAddress;
						name = user.Person.FirstName;
					}
				}

				if (email.IsNotNullOrEmpty() && name.IsNotNullOrEmpty())
				{
					switch (request.Module)
					{
						case FocusModules.Assist:
							Helpers.Email.SendEmail(email, "", "",
								Localise(request, "System.Email.AssistUserActivation.Subject"),
								Localise(request, "System.Email.AssistUserActivation.Body", name, request.SingleSignOnURL), true);
							break;

						default:
							Helpers.Email.SendEmail(email, "", "",
								Localise(request, "System.Email.AccountActivation.Subject"),
								Localise(request, "System.Email.AccountActivation.Body", name, request.SingleSignOnURL),
								true);
							break;
					}
				}

				return response;
			}
		}

		public CheckSocialSecurityNumberInUseResponse CheckSocialSecurityNumberInUse(CheckSocialSecurityNumberInUseRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
        var response = new CheckSocialSecurityNumberInUseResponse();
				if (!ValidateRequest(request, response, Validate.ClientTag | Validate.SessionId | Validate.License | Validate.AccessToken))
					return response;
				try
				{
					var personExists = Repositories.Core.Persons.Any(p => p.SocialSecurityNumber == request.SocialSecurityNumber);
					if (personExists)
					{
						response.Exists = true;
						return response;
					}

          var jobSeekerRequest = new GetJobSeekerRequest()
					{
						JobSeeker = new JobSeekerModel
						{
							SocialSecurityNumber = request.SocialSecurityNumber
						}
					};
          var getJobSeekerResponse = Repositories.Integration.GetJobSeekerBySocialSecurityNumber(jobSeekerRequest);
					response.Exists = getJobSeekerResponse.Outcome == IntegrationOutcome.Success;
					return response;
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
					return response;
				}
			}
		}

		/// <summary>
		/// Checks the user exists.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public CheckUserExistsResponse CheckUserExists(CheckUserExistsRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new CheckUserExistsResponse(request);

				// Validate client tag & session only
				if (!ValidateRequest(request, response, Validate.ClientTag | Validate.SessionId | Validate.License | Validate.AccessToken))
					return response;

				try
				{
					if (request.UserName.IsNullOrEmpty())
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.UserNameIsNull));
					else
					{

						var user = Repositories.Core.Users.FirstOrDefault(x => x.UserName == request.UserName);

                        if (user.IsNull())
                        {
                            var deniedUsers = Repositories.Core.Users.Where(u => u.UserName.StartsWith("DENIED") && u.UserName.Contains(request.UserName)).ToList();
                            deniedUsers.ForEach(x => {
                                user = x.UserName.Substring(25) == request.UserName ? x : null;
                            });
                        }

						if (user.IsNotNull() && user.Id != request.ExistingUserId.GetValueOrDefault(0))
							response.Exists = true;

						var existingPersonId = user.IsNotNull() ? user.PersonId : 0;

						#region Career/CareerExplorer checking

						if (request.ModuleToCheck == FocusModules.Career || request.ModuleToCheck == FocusModules.CareerExplorer)
						{
							if (Repositories.Core.Persons.Any(x => x.Id != existingPersonId && (x.EmailAddress.ToLower() == request.EmailAddress.ToLower() || (request.SSN != null && x.SocialSecurityNumber == request.SSN))))
							{
								response.Exists = true;
                                if (Repositories.Core.Persons.Any(x =>
							        x.Id != existingPersonId && (request.SSN != null && x.SocialSecurityNumber == request.SSN)))
                                {
                                    response.ExistenceReason = ExtenalUserPreExistenceReason.SsnExists;
                                }
							}
							else
							{
								var firstName = request.FirstName.IsNotNullOrEmpty() ? request.FirstName.ToLower() : "";
								var lastName = request.LastName.IsNotNullOrEmpty() ? request.LastName.ToLower() : "";

								var person = (firstName.IsNotNullOrEmpty() || lastName.IsNotNullOrEmpty() || request.DateOfBirth.HasValue || request.PostalAddress.IsNotNull())
									? Repositories.Core.Persons.Where(x => x.Id != existingPersonId && x.FirstName.ToLower() == firstName && x.LastName.ToLower() == lastName &&
									                                       x.DateOfBirth == request.DateOfBirth).SingleOrDefault(
										                                       p =>
											                                       p.PersonAddresses.Any(
												                                       a => request.PostalAddress.Street1 == a.Line1 &&
												                                            request.PostalAddress.Zip == a.PostcodeZip &&
												                                            request.PostalAddress.StateId == a.StateId))
									: null;

								if (person != null)
									response.Exists = person.PhoneNumbers.FindAll(p => p.Number == request.PrimaryPhone.PhoneNumber).Count > 0;


							}
							// Need to check if user exists externally if created in Assist
							if (!request.CheckLocalOnly && !response.Exists && AppSettings.IntegrationClient != IntegrationClient.Standalone)
							{
								// Check by username first
								var getJobSeekerResponse = Repositories.Integration.GetJobSeeker(new GetJobSeekerRequest
								{
									UserId = request.UserContext.UserId,
									JobSeeker = new JobSeekerModel()
									{
										Username = request.UserName
									}
								});
								if (getJobSeekerResponse.Outcome == IntegrationOutcome.Success && getJobSeekerResponse.JobSeeker.IsNotNull())
								{
									response.Exists = true;
									response.ExistenceReason = ExtenalUserPreExistenceReason.UsernameExists;
								}
								else
								{
									// Next check by SSN if provided
									if (request.SSN.IsNotNullOrEmpty())
									{
										var getJobSeekerBySsnResponse = Repositories.Integration.GetJobSeekerBySocialSecurityNumber(new GetJobSeekerRequest
										{
											UserId = request.UserContext.UserId,
											JobSeeker = new JobSeekerModel
											{
												SocialSecurityNumber = request.SSN
											}
										});
										if (getJobSeekerBySsnResponse.Outcome == IntegrationOutcome.Success && getJobSeekerBySsnResponse.JobSeeker.IsNotNull())
										{
											response.Exists = true;
											response.ExistenceReason = ExtenalUserPreExistenceReason.SsnExists;
										}
									}
								}

							}
						}

						#endregion
					}
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Creates the assist account.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public CreateAssistUserResponse CreateAssistUser(CreateAssistUserRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new CreateAssistUserResponse(request);

				// Validate client tag
				if (!ValidateRequest(request, response, Validate.ClientTag | Validate.License | Validate.AccessToken))
					return response;

				#region Validate request

				try
				{
					var error = false;

					foreach (var model in request.Models)
					{
						var existingUserId = Repositories.Core.Users.Where(x => x.UserName == model.AccountUserName).Select(x => x.Id).FirstOrDefault();

						if (existingUserId > 0)
						{
							if (request.BulkUpload)
							{
								model.AccountUserId = existingUserId;

								if (model.RegistrationError.IsNull())
									model.RegistrationError = new List<ErrorTypes> { ErrorTypes.UserNameAlreadyExists };
								else
									model.RegistrationError.Add(ErrorTypes.UserNameAlreadyExists);

								error = true;
								response.SetFailure(ErrorTypes.BulkRegistrationValidationFailed, FormatErrorMessage(request, ErrorTypes.BulkRegistrationValidationFailed));
							}
							else
							{
								response.SetFailure(ErrorTypes.UserNameAlreadyExists, FormatErrorMessage(request, ErrorTypes.UserNameAlreadyExists));
								Logger.Info(request.LogData(), string.Format("A user with user name '{0}' already exists.", model.UserEmailAddress));

								return response;
							}
						}

						existingUserId = Repositories.Core.Users.Where(x => x.Person.FirstName == model.UserPerson.FirstName && x.Person.LastName == model.UserPerson.LastName && x.Person.EmailAddress == model.UserEmailAddress)
							.Select(x => x.Id).FirstOrDefault();

						if (existingUserId > 0)
						{
							if (request.BulkUpload)
							{
								if (model.RegistrationError.IsNull())
									model.RegistrationError = new List<ErrorTypes> { ErrorTypes.UserAlreadyExists };
								else
									model.RegistrationError.Add(ErrorTypes.UserAlreadyExists);

								error = true;
								response.SetFailure(ErrorTypes.BulkRegistrationValidationFailed, FormatErrorMessage(request, ErrorTypes.BulkRegistrationValidationFailed));
							}
							else
							{
								response.SetFailure(ErrorTypes.UserAlreadyExists, FormatErrorMessage(request, ErrorTypes.UserAlreadyExists));
								Logger.Info(request.LogData(), string.Format("A user with firstname '{0}', lastname '{1}' and email address '{2}' already exists.", model.UserPerson.FirstName, model.UserPerson.LastName, model.UserEmailAddress));

								return response;
							}
						}

						if (model.UserEmailAddress.IsNullOrEmpty())
						{
							if (request.BulkUpload)
							{
								if (model.RegistrationError.IsNull())
									model.RegistrationError = new List<ErrorTypes> { ErrorTypes.InvalidEmail };
								else
									model.RegistrationError.Add(ErrorTypes.InvalidEmail);

								error = true;
								response.SetFailure(ErrorTypes.BulkRegistrationValidationFailed, FormatErrorMessage(request, ErrorTypes.BulkRegistrationValidationFailed));
							}
							else
							{
								response.SetFailure(FormatErrorMessage(request, ErrorTypes.PersonEmailAddressMissing));
								Logger.Info(request.LogData(), "Account registration requires an email address.");

								return response;
							}
						}

						if (model.UserExternalOffice.IsNullOrEmpty())
						{
							if (request.BulkUpload)
							{
								model.ErrorText = new List<string> { "Account registration requires an External Office." };
								error = true;
								response.SetFailure(ErrorTypes.BulkRegistrationValidationFailed, FormatErrorMessage(request, ErrorTypes.BulkRegistrationValidationFailed));
							}
							else
							{
								response.SetFailure(FormatErrorMessage(request, ErrorTypes.PersonOfficeMissing));
								Logger.Info(request.LogData(), "Account registration requires an External Office.");

								return response;
							}
						}

						// Remove any formatting on the number
						if (model.UserPhone.IsNotNullOrEmpty())
						{
							model.UserPhone = new string(model.UserPhone.Where(char.IsDigit).ToArray());
						}

						if (model.UserPhone.IsNullOrEmpty())
						{
							if (request.BulkUpload)
							{
								if (model.RegistrationError.IsNull())
									model.RegistrationError = new List<ErrorTypes> { ErrorTypes.PhoneNumberMissing };
								else
									model.RegistrationError.Add(ErrorTypes.PhoneNumberMissing);

								model.ErrorText = new List<string> { "Account registration requires a phone number." };
								error = true;
								response.SetFailure(ErrorTypes.BulkRegistrationValidationFailed, FormatErrorMessage(request, ErrorTypes.BulkRegistrationValidationFailed));
							}
							else
							{
								response.SetFailure(FormatErrorMessage(request, ErrorTypes.PhoneNumberMissing));
								Logger.Info(request.LogData(), "Account registration requires a phone number.");

								return response;
							}
						}
					}

					#endregion

					if (error)
					{
						response.Models = request.Models;
						return response;
					}

					var users = new List<User>();
					var templateDataList = new List<EmailTemplateData>();

					#region Create users

					foreach (var model in request.Models)
					{
						Password password;

						if (request.GeneratePassword)
						{
							model.AccountPassword = Guid.NewGuid().ToString().Replace("-", "").Substring(0, 10);
							model.ValidatePasswordStrength = false;
						}

						try
						{
							password = new Password(AppSettings, model.AccountPassword, model.ValidatePasswordStrength);
						}
						catch (Exception)
						{
							response.SetFailure(ErrorTypes.InvalidPassword, FormatErrorMessage(request, ErrorTypes.InvalidPassword));
							Logger.Info(request.LogData(), "Invalid password.");

							return response;
						}

						var user = new User
						{
							UserName = model.AccountUserName,
							PasswordSalt = password.Salt,
							PasswordHash = password.Hash,
							ExternalId = model.AccountExternalId,
							IsClientAuthenticated = model.IsClientAuthenticated,
							Enabled = model.IsEnabled,
							UserType = UserTypes.Assist
						};

						// Give the new user read-only access
						var roleNames = new List<string>
						{
							Constants.RoleKeys.AssistUser,
							Constants.RoleKeys.AssistEmployersReadOnly,
							Constants.RoleKeys.AssistJobSeekersReadOnly,
							Constants.RoleKeys.AssistJobSeekersAdministrator
						};

						if (request.ExtraRoles.IsNotNullOrEmpty())
						{
							request.ExtraRoles.ForEach(roleName =>
							{
								if (!roleNames.Contains(roleName))
									roleNames.Add(roleName);
							});
						}

						var roles = Repositories.Core.Roles.Where(x => roleNames.Contains(x.Key)).ToList();
						roles.ForEach(role => user.Roles.Add(role));

						var person = model.UserPerson.CopyTo();
						person.User = user;

						person.EmailAddress = model.UserEmailAddress;
						person.ExternalOffice = model.UserExternalOffice;

						#region Contact Details

						person.PhoneNumbers.Add(new PhoneNumber { PhoneType = PhoneTypes.Phone, IsPrimary = true, Number = model.UserPhone });

						if (model.UserAlternatePhone.IsNotNullOrEmpty())
						{
							// Remove any formatting on the number
							model.UserAlternatePhone = new string(model.UserAlternatePhone.Where(char.IsDigit).ToArray());
							person.PhoneNumbers.Add(new PhoneNumber { PhoneType = PhoneTypes.Phone, IsPrimary = false, Number = model.UserAlternatePhone });
						}

						if (model.UserFax.IsNotNullOrEmpty())
						{
							// Remove any formatting on the number
							model.UserFax = new string(model.UserFax.Where(char.IsDigit).ToArray());
							person.PhoneNumbers.Add(new PhoneNumber { PhoneType = PhoneTypes.Fax, IsPrimary = true, Number = model.UserFax });
						}

						#endregion

						if (model.UserAddress.IsNotNull())
							person.PersonAddresses.Add(model.UserAddress.CopyTo());

						var office = Repositories.Core.Offices.FirstOrDefault(o => o.ExternalId == model.UserExternalOffice);
						if (office.IsNotNull())
						{
							person.PersonOfficeMappers.Add(new PersonOfficeMapper
							{
								OfficeId = office.Id
							});

							person.CurrentOffices.Add(new PersonsCurrentOffice
							{
								OfficeId = office.Id,
								StartTime = DateTime.Now
							});
						}

						if (!request.BulkUpload)
							response.UserId = user.Id;

						if (request.SendEmail)
						{
							templateDataList.Add(new EmailTemplateData
							{
								RecipientName = model.UserPerson.FirstName,
								Username = model.UserEmailAddress,
								Password = model.AccountPassword,
								Url = AppSettings.AssistApplicationPath,
								SenderName = String.Format("{0} {1}", request.UserContext.FirstName, request.UserContext.LastName)
							});
						}

						users.Add(user);
					}

					#endregion

					// If we've created all the users successfully save them and send them all emails
					Repositories.Core.SaveChanges();

					#region Send emails to all newly created users

					if (request.SendEmail)
					{
						foreach (var templateData in templateDataList)
						{
							var email = Helpers.Email.GetEmailTemplatePreview(EmailTemplateTypes.AssistAccountCreation, templateData);

							Helpers.Email.SendEmail(templateData.Username, "", "", email.Subject, email.Body);
						}
					}

					#endregion

					// Log the create request
					foreach (var user in users)
					{
						LogAction(request, ActionTypes.CreateUser, user);
					}

					response.Models = request.Models;
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Finds the user.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public FindUserResponse FindUser(FindUserRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new FindUserResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.License | Validate.ClientTag | Validate.SessionId | Validate.AccessToken))
					return response;

				try
				{
					response.Users = FindUsers(request);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		public FindUserResponse FindUserByExternalId(FindUserRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new FindUserResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.License | Validate.ClientTag | Validate.SessionId))
					return response;

				try
				{
					response.Users = new PagedList<UserView>(
						Repositories.Core.Users.Where(u => u.ExternalId == request.SearchExternalId).Select(u => new UserView
						{
							ExternalId = u.ExternalId,
							EmailAddress = u.Person.EmailAddress,
							FirstName = u.Person.FirstName,
							LastName = u.Person.LastName,
							ScreenName = u.ScreenName,
							IsMigrated = u.IsMigrated
						}), 2, 0, 2);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Deletes the assist account.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public DeleteUserResponse DeleteUser(DeleteUserRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new DeleteUserResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var user = Repositories.Core.FindById<User>(request.DeleteUserId);

					// Check user doesn't exist)
					if (user.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.UserNotFound));
						Logger.Info(request.LogData(), string.Format("An user of username '{0} {1} ({2})' already exists.", request.DeleteUserFirstName, request.DeleteUserLastName, request.DeleteUserId));

						return response;
					}

					// Remove the user and log the event
					Repositories.Core.Remove(user);
					Repositories.Core.SaveChanges();

					// Log the action
					LogAction(request, ActionTypes.DeleteUser, user);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Requests the user account password reset.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public ResetUsersPasswordResponse ResetUsersPassword(ResetUsersPasswordRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new ResetUsersPasswordResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var user = Repositories.Core.FindById<User>(request.ResetUserId);

					// Check user doesn't exist)
					if (user.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.UserNotFound));
						Logger.Info(request.LogData(), string.Format("UserId '{0}' not found.", request.ResetUserId));

						return response;
					}

					user.ValidationKey = GenerateRandomHash();
					var userEmail = user.Person.EmailAddress;

					if (userEmail.IsNullOrEmpty())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.UserEmailAddressNotFound));
						Logger.Info(request.LogData(), "User email address not found.");

						return response;
					}

					Helpers.Email.SendEmail(userEmail, "", "",
            Localise(request, "System.Email.ResetPassword.Subject"),
						Localise(request, "System.Email.ResetPassword.Body", user.Person.FirstName, string.Format("{0}?{1}", request.ResetPasswordUrl, HttpUtility.UrlEncode(user.ValidationKey))), true);

					// Generate a new password
					var newPassword = Password.GenerateRandomPassword();

					user.PasswordHash = newPassword.Hash;
					user.PasswordSalt = newPassword.Salt;

					Repositories.Core.SaveChanges();

					// Log the action
					LogAction(request, ActionTypes.RequestPasswordReset, user);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Activates the user.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns>Response object</returns>
		public ActivateUserResponse ActivateUser(ActivateUserRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new ActivateUserResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.ClientTag | Validate.License | Validate.AccessToken))
					return response;

				try
				{
					var user = Repositories.Core.FindById<User>(request.UserId);

					// Check user doesn't exist)
					if (user.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.UserNotFound));
						Logger.Info(request.LogData(), string.Format("UserId '{0}' not found.", request.UserId));

						return response;
					}

					user.Enabled = true;

					Repositories.Core.SaveChanges(true);

					if (request.ResetResume)
						Helpers.Resume.ResetSearchableDefaultResumeForPerson(request, user.PersonId);

					// Log the action
					LogAction(request, ActionTypes.ActivateAccount, typeof(Person).Name, user.PersonId, user.Id);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Deactivates the user.
		/// THIS METHOD IS CURRENTLY NOT BEING USED ANYWHERE
		/// IF YOU ARE LOOKING TO DEACTIVATE A USER REFER TO THE AccountService.InactivateJobSeeker METHOD FOR A POSSIBLE ALTERNATIVE
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public DeactivateUserResponse DeactivateUser(DeactivateUserRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new DeactivateUserResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var user = Repositories.Core.FindById<User>(request.UserId);

					// Check user doesn't exist)
					if (user.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.UserNotFound));
						Logger.Info(request.LogData(), string.Format("UserId '{0}' not found.", request.UserId));

						return response;
					}

					user.Enabled = false;

					Repositories.Core.SaveChanges(true);

					// Log the action
					LogAction(request, ActionTypes.DeactivateAccount, user);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the user type of a user
		/// </summary>
		/// <param name="request">The request containing the user id</param>
		/// <returns>The response containing the user type</returns>
		public GetUserTypeResponse GetUserType(GetUserTypeRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new GetUserTypeResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var userType = Repositories.Core.Users.Where(u => u.Id == request.UserId).Select(u => u.UserType).FirstOrDefault();
					if (userType.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.UserNotFound));
						Logger.Info(request.LogData(), string.Format("UserId '{0}' not found.", request.UserId));

						return response;
					}

					response.UserType = userType.Value;
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}
				return response;
			}
		}

		/// <summary>
		/// Gets the user details.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public GetUserDetailsResponse GetUserDetails(GetUserDetailsRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new GetUserDetailsResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var userQuery = new UserQuery(Repositories.Core, request.Criteria);

					if (userQuery.Query().IsNull())
						return response;

					var userId = request.Criteria.UserId;

					if (request.Criteria.PersonId.IsNotNull() && request.Criteria.PersonId > 0 && (userId.IsNotNull() && userId == 0))
						userId = (from u in userQuery.Query()
							where u.PersonId == request.Criteria.PersonId
							select u.Id).FirstOrDefault();

					var user = (from u in userQuery.Query()
						where u.Id == userId
						select u).FirstOrDefault();

					if (user.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.UserNotFound));
						Logger.Info(request.LogData(), string.Format("UserId '{0}' not found.", userId));

						return response;
					}

					response.UserDetails = user.AsDto();

					var person = (from u in userQuery.Query()
						join p in Repositories.Core.Persons on u.PersonId equals p.Id
						where u.Id == userId
						select p).FirstOrDefault();

					if (person.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.UserNotFound));
						Logger.Info(request.LogData(), string.Format("UserId '{0}' not found.", userId));
					}
					else
					{
						response.PersonDetails = person.AsDto();

						var address = person.PersonAddresses.Where(x => x.IsPrimary).Select(x => x.AsDto()).FirstOrDefault();
						if (address.IsNotNull()) response.AddressDetails = address;

						var phone = person.PhoneNumbers.Where(x => x.IsPrimary).Select(x => x.AsDto()).FirstOrDefault();
						if (phone.IsNotNull()) response.PrimaryPhoneNumber = phone;

						// Alternate numbers 
						var alternatePhones = person.PhoneNumbers.Where(x => x.IsPrimary == false).Select(x => x.AsDto()).ToList();
						if (alternatePhones.IsNotNullOrEmpty())
						{
							if (alternatePhones.Count > 0 && alternatePhones[0].IsNotNull())
								response.AlternatePhoneNumber1 = alternatePhones[0];

							if (alternatePhones.Count > 1 && alternatePhones[1].IsNotNull())
								response.AlternatePhoneNumber2 = alternatePhones[1];
						}

						// Office roles
						response.OfficeRoles = person.PersonOfficeMappers.Select(x => x.AsDto()).ToList();
						if (response.OfficeRoles.IsNullOrEmpty())
							response.OfficeRoles = new List<PersonOfficeMapperDto>();

						if (request.Criteria.GetSecurityAnswer)
						{
							var userSecurityQuestion1 = Repositories.Core.UserSecurityQuestions.FirstOrDefault(usq => usq.UserId == user.Id && usq.QuestionIndex == 1);
							response.SecurityQuestion = userSecurityQuestion1.IsNull() ? null : userSecurityQuestion1.SecurityQuestion;
							response.SecurityQuestionId = userSecurityQuestion1.IsNull() ? null : userSecurityQuestion1.SecurityQuestionId;
							response.SecurityAnswer = userSecurityQuestion1.IsNotNull() && userSecurityQuestion1.SecurityAnswerEncrypted.IsNotNullOrEmpty()
								? Helpers.Encryption.Decrypt(userSecurityQuestion1.SecurityAnswerEncrypted, TargetTypes.SecurityAnswer, EntityTypes.User, user.Id)
								: string.Empty;
						}
					if (response.UserDetails.UserType == UserTypes.Talent && person.Employee.IsNotNull())
						{
							response.EmployeeLockNumber = person.Employee.LockVersion;
						}
					}
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}
				return response;
			}
		}

		/// <summary>
		/// Get Security Questions for user
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public SecurityQuestionDetailsResponse GetUserSecurityQuestion( SecurityQuestionDetailsRequest request )
		{
			var response = new SecurityQuestionDetailsResponse( request );
			try
			{
				response.UserSecurityQuestions = from usq in Repositories.Core.UserSecurityQuestions.Where(usq => usq.UserId == request.UserId && (request.QuestionIndex == 0 || usq.QuestionIndex == request.QuestionIndex))
					select new UserSecurityQuestionDto
					{
						QuestionIndex = usq.QuestionIndex,
						SecurityQuestion = usq.SecurityQuestion,
						SecurityQuestionId = usq.SecurityQuestionId,
						SecurityAnswerHash = usq.SecurityAnswerHash,
						UserId = usq.UserId,
						Id = usq.Id,
						SecurityAnswerEncrypted = usq.SecurityAnswerEncrypted.IsNotNullOrEmpty()
							? Helpers.Encryption.Decrypt(usq.SecurityAnswerEncrypted, TargetTypes.SecurityAnswer, EntityTypes.User, request.UserId, false)
							: string.Empty
					};
			}
			catch( Exception ex )
			{
				response.SetFailure( FormatErrorMessage( request, ErrorTypes.Unknown ), ex );
				Logger.Error( request.LogData(), response.Message, response.Exception );
			}
			return response;
		}

		/// <summary>
		/// Checks if the user has questions
		/// </summary>
		/// <param name="request"></param>
		/// <returns></returns>
		public SecurityQuestionDetailsResponse HasUserSecurityQuestions(SecurityQuestionDetailsRequest request)
		{
			var response = new SecurityQuestionDetailsResponse(request);
			try
			{
				response.HasQuestions = (Repositories.Core.UserSecurityQuestions.Where(usq => usq.UserId == request.UserId).Select(usq => usq.Id).FirstOrDefault()) > 0;
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}
			return response;
		}

		/// <summary>
		/// Gets the role.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public RoleResponse GetRole(RoleRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new RoleResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.ClientTag | Validate.SessionId | Validate.License | Validate.AccessToken))
					return response;

				try
				{
					var query = Repositories.Core.Roles;

					if (request.Criteria.UserId.HasValue)
					{
						query = (from q in query
							join ur in Repositories.Core.UserRoles on q.Id equals ur.RoleId
							where ur.UserId == request.Criteria.UserId.Value
							select q);
					}

					switch (request.Criteria.FetchOption)
					{
						case CriteriaBase.FetchOptions.Single:
							response.Role = query.Select(x => x.AsDto()).SingleOrDefault();
							break;

						case CriteriaBase.FetchOptions.List:
							response.Roles = query.Select(x => x.AsDto()).ToList();
							break;

						case CriteriaBase.FetchOptions.PagedList:
							response.RolesPaged = query.GetPagedList( x => x.AsDto(), request.Criteria.PageIndex, request.Criteria.PageSize);
							break;

						case CriteriaBase.FetchOptions.Lookup:
							response.RolesLookup = query.ToDictionary(x => x.Id, x => x.Key);
							break;
					}
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}
				return response;
			}
		}

		/// <summary>
		/// Updates the users roles.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public UpdateUsersRolesResponse UpdateUsersRoles(UpdateUsersRolesRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new UpdateUsersRolesResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					// Two objects to store User Roles removed and added
					var rolesRemoved = new List<UserRole>();
					var rolesAdded = new List<UserRole>();

					// Get users current roles
					var currentRoles = (from r in Repositories.Core.Roles
						join ur in Repositories.Core.UserRoles on r.Id equals ur.RoleId
						where ur.UserId == request.UserId
						select r).ToList();

					// Now remove any roles no longer required
					foreach (var currentRole in currentRoles)
					{
						if (request.Roles.All(r => r.Key != currentRole.Key))
						{
							// Below suggested by Resharper
							var role = currentRole;

							var userRolesToBeRemoved = Repositories.Core.UserRoles.Where(x => x.UserId == request.UserId && x.RoleId == role.Id).ToList();
							userRolesToBeRemoved.ForEach(x => Repositories.Core.Remove(x));

							userRolesToBeRemoved.ForEach(rolesRemoved.Add);
						}
					}

					//Now add the new roles
					foreach (var role in request.Roles)
					{
						// Below suggested by Resharper
						var roleDto = role;

						if (currentRoles.All(r => r.Key != roleDto.Key))
						{
							var roleObj = Repositories.Core.Roles.FirstOrDefault(x => x.Key == roleDto.Key);
							if (roleObj.IsNotNull())
							{
								var roleId = roleObj.Id;

								var newUserRole = new UserRole
								{
									RoleId = roleId,
									UserId = request.UserId
								};

								Repositories.Core.Add(newUserRole);

								rolesAdded.Add(newUserRole);
							}
						}
					}

					Repositories.Core.SaveChanges(true);

					// Log the removals
					rolesRemoved.ForEach(ur => LogAction(request, ActionTypes.RemovedUserFromRole, ur));
					// Log the additions
					rolesAdded.ForEach(ur => LogAction(request, ActionTypes.AddedUserToRole, ur));
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Changes the username.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public ChangeUsernameResponse ChangeUsername(ChangeUsernameRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new ChangeUsernameResponse(request);
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var user = Repositories.Core.FindById<User>(request.UserContext.UserId);
					if (user == null)
					{
						response.SetFailure(ErrorTypes.UnableToChangeUserName, FormatErrorMessage(request, ErrorTypes.UnableToChangeUserName));
						Logger.Info(request.LogData(), string.Format("Unable to change user name for user '{0}': unable to locate user.", request.CurrentUsername));
						return response;
					}

					if (user.UserName != request.CurrentUsername)
					{
            response.SetFailure(ErrorTypes.CurrentUsernameDoesNotMatch, FormatErrorMessage(request, ErrorTypes.CurrentUsernameDoesNotMatch));
            Logger.Info(request.LogData(), string.Format("Unable to change user name for user {0}. Current username doesn't match.", request.UserContext.UserId));
						return response;
					}

					if (Repositories.Core.Users.Count(x => x.UserName == request.NewUsername && x.Id != user.Id) > 0)
					{
						response.SetFailure(ErrorTypes.UserNameAlreadyExists, FormatErrorMessage(request, ErrorTypes.UserNameAlreadyExists));
						Logger.Info(request.LogData(), string.Format("An user with email address '{0}' already exists.", request.NewUsername));
						return response;
					}

					if ((user.UserType & UserTypes.Career) == UserTypes.Career && AppSettings.IntegrationClient != IntegrationClient.Standalone)
					{
						var getJobSeekerResponse = Repositories.Integration.GetJobSeeker(new GetJobSeekerRequest()
						{
							JobSeeker = new JobSeekerModel()
							{
								Username = request.NewUsername
							}
						});
						if (getJobSeekerResponse.Outcome == IntegrationOutcome.Success)
						{
							response.SetFailure(ErrorTypes.UserNameAlreadyExists, FormatErrorMessage(request, ErrorTypes.UserNameAlreadyExists));
							Logger.Info(request.LogData(), string.Format("An user with email address '{0}' already exists.", request.NewUsername));
							return response;
						}
					}

					user.UserName = request.NewUsername;
					Repositories.Core.SaveChanges();
					response.User = user.AsDto();
					if ((user.UserType & UserTypes.Career) == UserTypes.Career)
					{
						Helpers.SelfService.PublishSelfServiceActivity(ActionTypes.ChangeUserName, request.UserContext.ActionerId,
							user.Id, user.PersonId);
						if (AppSettings.IntegrationClient != IntegrationClient.Standalone)
						{
							SaveJobSeekerRequest saveJobSeekerRequest = new SaveJobSeekerRequest()
							{
								IntegrationPoint = IntegrationPoint.SaveJobSeeker,
								PersonId = user.PersonId,
								UserId = user.Id
							};
							if (RuntimeContext.IsNotNull() && RuntimeContext.CurrentRequest.IsNotNull() &&
							    RuntimeContext.CurrentRequest.UserContext.IsNotNull())
							{
								saveJobSeekerRequest.ExternalAdminId = RuntimeContext.CurrentRequest.UserContext.ExternalUserId;
								saveJobSeekerRequest.ExternalOfficeId = RuntimeContext.CurrentRequest.UserContext.ExternalOfficeId;
								saveJobSeekerRequest.ExternalUsername = RuntimeContext.CurrentRequest.UserContext.ExternalUserName;
								saveJobSeekerRequest.ExternalPassword = RuntimeContext.CurrentRequest.UserContext.ExternalPassword;
							}

							IntegrationRequestMessage integrationRequest = new IntegrationRequestMessage
							{
								ActionerId = request.UserContext.UserId,
								IntegrationPoint = IntegrationPoint.SaveJobSeeker,
								SaveJobSeekerRequest = saveJobSeekerRequest
							};
							Helpers.Messaging.Publish(integrationRequest);
						}
					}

					LogAction(request, ActionTypes.ChangeUserName, user);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Checks if an email address is in use by another user
		/// </summary>
		/// <param name="request">The request with the email details.</param>
		/// <returns>A response indicating if the email address is in use</returns>
		public CheckEmailAddressResponse CheckEmailAddress(CheckEmailAddressRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new CheckEmailAddressResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					//Check another user hasn't got that email address
					var personId = request.PersonId ?? request.UserContext.PersonId;

					response.EmailInUse = IsEmailInUse(RuntimeContext, request.EmailAddress, personId.GetValueOrDefault(0));
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Checks if an email addrss is in use by another person
		/// </summary>
		/// <param name="runtimeContext">The Runtime Context</param>
		/// <param name="emailAddress">The email address to check</param>
		/// <param name="personId">The id of the current person</param>
		/// <returns></returns>
		internal static bool IsEmailInUse(IRuntimeContext runtimeContext, string emailAddress, long personId)
		{
			return runtimeContext.Repositories.Core.Persons.Any(x => x.EmailAddress == emailAddress && x.Id != personId);
		}

		/// <summary>
		/// Updates the email address of the current user
		/// </summary>
		/// <param name="request">The request with the email details.</param>
		/// <returns>A response indicating if the change was successful or not</returns>
		public ChangeEmailAddressResponse ChangeEmailAddress(ChangeEmailAddressRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new ChangeEmailAddressResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					// user details
					var user = Repositories.Core.FindById<User>(request.UserContext.UserId);

					// User must exist
					if (user.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.UserNotFound));
						Logger.Info(request.LogData(), string.Format("UserId '{0}' not found.", request.UserContext.UserId));
					}

					// Check existing email address matches
					if (user.Person.EmailAddress != request.CurrentEmailAddress)
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.UnableToChangeEmailAddress));
						Logger.Info(request.LogData(), string.Format("Unable to change email address for user {0}. Current email address doesn't match.", request.UserContext.UserId));

						return response;
					}

					//Check another user hasn't got that email address
					if (Repositories.Core.Persons.Any(x => x.EmailAddress == request.NewEmailAddress && x.Id != user.PersonId))
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.EmailAddressAlreadyExists));
						Logger.Info(request.LogData(), string.Format("An user with email address '{0}' already exists.", request.NewEmailAddress));

						return response;
					}

					// Perform censorship check on email address
					if (user.Person.EmailAddress != request.NewEmailAddress)
            CensorshipCheckEmail(request.NewEmailAddress, user.Person.Id);

					// Update person record
					user.Person.EmailAddress = request.NewEmailAddress;

					Repositories.Core.SaveChanges();

					if ((user.UserType & UserTypes.Career) == UserTypes.Career)
					{
						Helpers.Resume.UpdateDefaultResumeEmailField(request, user.PersonId, request.NewEmailAddress);
					}

					// Log the action
					LogAction(request, ActionTypes.UpdateUser, user);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Updates the contact details. Note: that if the value of a contact detail is null or empty it is deleted.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public UpdateContactDetailsResponse UpdateContactDetails(UpdateContactDetailsRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new UpdateContactDetailsResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					// user details
					var user = Repositories.Core.FindById<User>(request.Criteria.UserId);

					if (user.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.UserNotFound));
						Logger.Info(request.LogData(), string.Format("UserId '{0}' not found.", request.Criteria.UserId));
					}

					long personId = user.PersonId;
					var employee = Repositories.Core.Employees.FirstOrDefault(e => e.PersonId == personId);

					if (employee.IsNotNull() && employee.LockVersion != request.LockVersion.GetValueOrDefault())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.LockVersionOutOfDate));
						return response;
					}



					if (request.PersonDetails.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.PersonDetailsNotSupplied));
						Logger.Info(request.LogData(), string.Format("UserId '{0}' not found.", request.Criteria.UserId));
					}

					if (personId.IsNull() || personId == 0)
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.PersonNotFound));
						Logger.Info(request.LogData(), string.Format("UserId '{0}' not found.", request.Criteria.UserId));
					}

					// person
					user.Person.FirstName = request.PersonDetails.FirstName;
					user.Person.LastName = request.PersonDetails.LastName;
					user.Person.MiddleInitial = request.PersonDetails.MiddleInitial;
					user.Person.TitleId = request.PersonDetails.TitleId;
					user.Person.SuffixId = request.PersonDetails.SuffixId;
					user.Person.JobTitle = request.PersonDetails.JobTitle;
					user.Person.EmailAddress = request.PersonDetails.EmailAddress;
					if (request.UserDetails.IsNotNull())
					{
						if (request.UserDetails.ExternalId.IsNotNullOrEmpty())
							user.ExternalId = request.UserDetails.ExternalId;
					}

					// contact details
					PhoneNumber primaryNumber = null;
					PhoneNumber alternateNumber1 = null;
					PhoneNumber alternateNumber2 = null;

					if (request.PrimaryContactDetails.IsNotNull())
					{
						request.PrimaryContactDetails.PersonId = personId;
						response = (UpdateContactDetailsResponse)UpdatePhoneNumber(request, response, user.Person, request.PrimaryContactDetails, false, out primaryNumber);

						if (response.Acknowledgement == AcknowledgementType.Failure)
							return response;
					}

					if (request.Alternate1ContactDetails.IsNotNull())
					{
						request.Alternate1ContactDetails.PersonId = personId;
						response = (UpdateContactDetailsResponse)UpdatePhoneNumber(request, response, user.Person, request.Alternate1ContactDetails, false, out alternateNumber1);

						if (response.Acknowledgement == AcknowledgementType.Failure)
							return response;
					}

					if (request.Alternate2ContactDetails.IsNotNull())
					{
						request.Alternate2ContactDetails.PersonId = personId;
						response = (UpdateContactDetailsResponse)UpdatePhoneNumber(request, response, user.Person, request.Alternate2ContactDetails, false, out alternateNumber2);

						if (response.Acknowledgement == AcknowledgementType.Failure)
							return response;
					}

					if (request.UserDetails.IsNotNull())
					{
						if (request.UserDetails.UserName.IsNotNullOrEmpty())
						{
							// if user does not already exist
							var checkUserId = Repositories.Core.Users.Where(x => x.UserName == request.UserDetails.UserName).Select(x => x.Id).FirstOrDefault();
							if (checkUserId.IsNull() || checkUserId == 0)
							{
								user.UserName = request.UserDetails.UserName;
							}
							else if (checkUserId != user.Id)
							{
								response.SetFailure(FormatErrorMessage(request, ErrorTypes.UserAlreadyExists));
								Logger.Info(request.LogData(), string.Format("Username '{0}' already exists.", request.UserDetails.UserName));
								return response;
							}
						}
					}

					var address = new PersonAddress();

					if (request.AddressDetails.IsNotNull())
					{
						var primaryAddress = user.Person.PersonAddresses.FirstOrDefault(x => x.IsPrimary = true);
						if (primaryAddress.IsNotNull())
						{
							address = Repositories.Core.FindById<PersonAddress>( Convert.ToInt64( primaryAddress.Id ) );							
						}

						if (address.IsNotNull())
						{
							address.Line1 = request.AddressDetails.Line1;
							address.Line2 = request.AddressDetails.Line2;
							address.TownCity = request.AddressDetails.TownCity;
							address.StateId = request.AddressDetails.StateId;
							address.CountyId = request.AddressDetails.CountyId;
							address.CountryId = request.AddressDetails.CountryId;
							address.PostcodeZip = request.AddressDetails.PostcodeZip;

							if (user.Person.PersonAddresses.Count == 0)
							{
								address.IsPrimary = true;
								address.PersonId = personId;
								Repositories.Core.Add(address);
							}
						}
						else
						{
							response.SetFailure(FormatErrorMessage(request, ErrorTypes.PersonAddressNotFound));
							Logger.Info(request.LogData(), string.Format("Address '{0}' not found.", request.AddressDetails.Id));
						}
					}

					response.FailedCensorshipCheck = false;
					if (request.CheckCensorship && AppSettings.NewEmployerCensorshipFiltering && !request.UserContext.IsShadowingUser)
					{
						var stringBuilder = new StringBuilder();

						var personProperties = user.Person.GetType().GetProperties(BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance);
						foreach (var propertyInfo in personProperties)
						{
							stringBuilder.Append(" ").Append(propertyInfo.GetValue(user.Person, null));
						}

						var personAddressProperties = address.GetType().GetProperties(BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance);
						foreach (var propertyInfo in personAddressProperties)
						{
							stringBuilder.Append(" ").Append(propertyInfo.GetValue(address, null));
						}

						var personText = stringBuilder.ToString();
						var censorshipModel = ProfanityCheck(personText);


						if (censorshipModel.RedWords.IsNotNullOrEmpty() || censorshipModel.YellowWords.IsNotNullOrEmpty())
						{
							response.FailedCensorshipCheck = true;
							if (employee.ApprovalStatus != ApprovalStatuses.WaitingApproval)
							{
								employee.ApprovalStatusReason = ApprovalStatusReason.CensorshipFailed;
								employee.ApprovalStatus = ApprovalStatuses.WaitingApproval;
								employee.AwaitingApprovalDate = DateTime.Now;
							}
							else if (employee.ApprovalStatusReason != ApprovalStatusReason.Default)
							{
								employee.ApprovalStatusReason = (employee.ApprovalStatusReason | ApprovalStatusReason.CensorshipFailed);
							}

							employee.RedProfanityWords = censorshipModel.RedWords.NullIfEmpty();
							employee.YellowProfanityWords = censorshipModel.YellowWords.NullIfEmpty();
						}
						else
						{
							// Reset the approval status if it was original set due to censorship failure on employee record
							if (employee.ApprovalStatus == ApprovalStatuses.WaitingApproval && (employee.ApprovalStatusReason & ApprovalStatusReason.CensorshipFailed) == ApprovalStatusReason.CensorshipFailed)
							{
								employee.ApprovalStatusReason = (employee.ApprovalStatusReason & ~ApprovalStatusReason.CensorshipFailed);
								if (employee.ApprovalStatusReason == ApprovalStatusReason.Default)
								{
									employee.ApprovalStatus = ApprovalStatuses.None;
									employee.AwaitingApprovalDate = null;
								}
							}
						}
					}


					if (employee.IsNotNull())
					{
						var newLockVersion = employee.LockVersion + 1;
						var changes = new Dictionary<string, object> { { "LockVersion", newLockVersion } };
						var updateQuery = new Query(typeof(Employee), Entity.Attribute("PersonId") == personId);
						Repositories.Core.Update(updateQuery, changes);

						response.LockVersion = newLockVersion;
					}
					Repositories.Core.SaveChanges();

					//temporary solution for implementing locking policy on person entity


					if (AppSettings.IntegrationClient != IntegrationClient.Standalone)
					{
						var integrationRequest = new IntegrationRequestMessage { ActionerId = request.UserContext.UserId };
						if (user.UserType == UserTypes.Talent)
						{
	            var employerExternalId = Repositories.Core.Employers.Where(e => e.Id == employee.EmployerId).Select(e => e.ExternalId).FirstOrDefault();
	            if (employerExternalId.IsNotNullOrEmpty())
	            {
							integrationRequest.IntegrationPoint = IntegrationPoint.SaveEmployee;
							integrationRequest.SaveEmployeeRequest = new SaveEmployeeRequest { UserId = request.UserContext.UserId, PersonId = user.PersonId };
						}
            }
						else if (((user.UserType & UserTypes.Career) == UserTypes.Career))
						{
							integrationRequest.IntegrationPoint = IntegrationPoint.SaveJobSeeker;
							var saveJobSeekerRequest = new SaveJobSeekerRequest
							{
								UserId = request.UserContext.UserId,
								PersonId = user.PersonId
							};
							if (RuntimeContext.IsNotNull() && RuntimeContext.CurrentRequest.IsNotNull() &&
							    RuntimeContext.CurrentRequest.UserContext.IsNotNull())
							{
								saveJobSeekerRequest.ExternalAdminId = RuntimeContext.CurrentRequest.UserContext.ExternalUserId;
								saveJobSeekerRequest.ExternalOfficeId = RuntimeContext.CurrentRequest.UserContext.ExternalOfficeId;
								saveJobSeekerRequest.ExternalUsername = RuntimeContext.CurrentRequest.UserContext.ExternalUserName;
								saveJobSeekerRequest.ExternalPassword = RuntimeContext.CurrentRequest.UserContext.ExternalPassword;
							}

							integrationRequest.SaveJobSeekerRequest = saveJobSeekerRequest;
						}

						if (integrationRequest.IntegrationPoint != default(IntegrationPoint))
						{
							Helpers.Messaging.Publish(integrationRequest);
						}
					}

					if (primaryNumber.IsNotNull())
						response.PrimaryPhoneNumber = primaryNumber.AsDto();

					if (alternateNumber1.IsNotNull())
						response.AlternatePhoneNumber1 = alternateNumber1.AsDto();

					if (alternateNumber2.IsNotNull())
						response.AlternatePhoneNumber2 = alternateNumber2.AsDto();

					if (address.IsNotNull())
						response.AddressDetails = address.AsDto();

					if (user.IsNotNull())
						response.User = user.AsDto();

					if (user.Person.IsNotNull())
						response.Person = user.Person.AsDto();

					// Log the action
					LogAction(request, ActionTypes.UpdateUser, user);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets a job seeker's SSN.
		/// </summary>
		/// <param name="request">The request containing the job seeker details.</param>
		/// <returns>A response containing the SSN</returns>
		public GetSSNResponse GetSSN(GetSSNRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new GetSSNResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					response.SSN = Repositories.Core.Persons.Where(p => p.Id == request.PersonId).Select(p => p.SocialSecurityNumber).FirstOrDefault();
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

    /// <summary>
		/// Updates the SSN.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public UpdateSsnResponse UpdateSsn(UpdateSsnRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new UpdateSsnResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var person = Repositories.Core.FindById<Person>(request.Id);
					if (person == null)
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.UnableToChangeSsn));
						Logger.Info(request.LogData(), string.Format("Unable to change SSN for person ID '{0}': unable to locate person.", request.Id));

						return response;
					}

					#region Check current SSN is correct

					if (person.SocialSecurityNumber.IsNotNullOrEmpty() && request.CurrentSsn.IsNotNullOrEmpty())
					{
						if (person.SocialSecurityNumber != request.CurrentSsn)
						{
							response.UiError = ErrorTypes.CurrentSsnDoesNotMatch;
							response.SetFailure(FormatErrorMessage(request, ErrorTypes.CurrentSsnDoesNotMatch));
							var error = string.Format("Unable to change SSN for person ID {0}. Current SSN doesn't match.", request.Id);
							Logger.Info(request.LogData(), error);
							response.ErrorText = error;

							return response;
						}
					}

					#endregion

					#region Check another user hasn't got the new SSN

					if (Repositories.Core.Persons.Count(x => x.SocialSecurityNumber == request.NewSsn && x.Id != person.Id) > 0)
					{
						response.UiError = ErrorTypes.SsnAlreadyExists;
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.SsnAlreadyExists));
						var error = string.Format("A user with SSN '{0}' already exists.", request.NewSsn);
						Logger.Info(request.LogData(), error);
						response.ErrorText = error;

						return response;
					}

					#endregion

					person.SocialSecurityNumber = request.NewSsn;

					Repositories.Core.SaveChanges();

					response.Person = person.AsDto();

					person.User.PublishUserUpdateIntegrationRequest(request.UserContext.UserId, RuntimeContext);

					// Log the action
					LogAction(request, ActionTypes.ChangeJobSeekerSsn, person);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}


		/// <summary>
		/// Updates the user.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public RegisterCareerUserResponse UpdateUser(RegisterCareerUserRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				//TODO: Could this method be refactored to update all users not just Job seekers?

				var response = new RegisterCareerUserResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					// user details
					var user = request.UpdateUserId.HasValue
						? Repositories.Core.FindById<User>(request.UpdateUserId.Value)
						: Repositories.Core.Users.FirstOrDefault(u => u.PersonId == request.UpdatePersonId.Value);

					if (user.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.UserNotFound));
						Logger.Info(request.LogData(), string.Format("UserId '{0}' not found.", request.UserContext.UserId));

						return response;
					}

					#region User

					if (request.AccountUserName.IsNotNullOrEmpty() && user.UserName != request.AccountUserName)
					{
						if (Repositories.Core.Users.Any(u => u.UserName == request.AccountUserName && u.Id != user.Id))
						{
							response.SetFailure(ErrorTypes.UserNameAlreadyExists, FormatErrorMessage(request, ErrorTypes.UserNameAlreadyExists));
							Logger.Info(request.LogData(), string.Format("An user with username '{0}' already exists.", request.EmailAddress));

							return response;
						}

						user.UserName = request.AccountUserName;
					}

					if (request.AccountPassword.IsNotNullOrEmpty())
					{
						var password = new Password(AppSettings, request.AccountPassword);
						user.PasswordSalt = password.Salt;
						user.PasswordHash = password.Hash;
					}

					if (request.MigratedPasswordHash.IsNotNullOrEmpty())
						user.MigratedPasswordHash = request.MigratedPasswordHash;

					var securityQuestions = new List<UserSecurityQuestionDto>();
					if( request.SecurityQuestion.IsNotNullOrEmpty() || request.SecurityQuestionId.HasValue )
					{
						securityQuestions.Add(new UserSecurityQuestionDto
						{
							QuestionIndex = 1,
							SecurityQuestion = request.SecurityQuestion,
							SecurityQuestionId = request.SecurityQuestionId,
							SecurityAnswerEncrypted = request.SecurityAnswer.IsNotNullOrEmpty()
								? request.SecurityAnswer
								: null
						});
					}

					Helpers.User.UpdateSecurityQuestions(user.Id, securityQuestions.ToArray(), true);

					if (request.LastLoggedIn.IsNotNull())
						user.LoggedInOn = request.LastLoggedIn;

					if (AppSettings.OAuthEnabled || AppSettings.SamlEnabled || AppSettings.SamlEnabledForCareer)
					{
						user.IsMigrated = true;
						user.RegulationsConsent = true;
					}

					#endregion

					#region Person

					var person = Repositories.Core.Persons.FirstOrDefault(p => p.Id == user.PersonId);
					if (person.IsNull())
					{
						response.SetFailure(ErrorTypes.PersonNotFound, FormatErrorMessage(request, ErrorTypes.PersonNotFound));
						Logger.Info(request.LogData(), string.Format("Person with Id {0} does not exist.", user.PersonId));

						return response;
					}

					if (request.EmailAddress.IsNotNullOrEmpty() && person.EmailAddress != request.EmailAddress)
					{
						if (Repositories.Core.Persons.Any(p => p.EmailAddress == request.EmailAddress && p.Id != person.Id))
						{
							response.SetFailure(ErrorTypes.EmailAddressAlreadyExists, FormatErrorMessage(request, ErrorTypes.EmailAddressAlreadyExists));
							Logger.Info(request.LogData(), string.Format("Email address '{0}' already exists.", request.EmailAddress));

							return response;
						}

						person.EmailAddress = request.EmailAddress;
					}

					if (request.SocialSecurityNumber.IsNotNullOrEmpty() && person.SocialSecurityNumber != request.SocialSecurityNumber)
					{
						if (Repositories.Core.Persons.Any(p => p.SocialSecurityNumber == request.SocialSecurityNumber && p.Id != person.Id))
						{
							var maskNumber = request.SocialSecurityNumber.Length > 3
								? string.Concat(request.SocialSecurityNumber.Substring(0, 3), Regex.Replace(request.SocialSecurityNumber.Substring(3), @"\d", "X"))
								: request.SocialSecurityNumber;

							response.SetFailure(ErrorTypes.SsnAlreadyExists, FormatErrorMessage(request, ErrorTypes.SsnAlreadyExists));
							Logger.Info(request.LogData(), string.Format("SSN '{0}' already exists.", maskNumber));

							return response;
						}

						person.SocialSecurityNumber = request.SocialSecurityNumber;
					}

					if (request.FirstName.IsNotNullOrEmpty())
						person.FirstName = request.FirstName;

					if (request.LastName.IsNotNullOrEmpty())
						person.LastName = request.LastName;

					if (request.MiddleInitial.IsNotNull())
						person.MiddleInitial = request.MiddleInitial;

					if (request.DateOfBirth.IsNotNull())
						person.DateOfBirth = request.DateOfBirth;

					if (request.EnrollmentStatus.IsNotNull())
						person.EnrollmentStatus = request.EnrollmentStatus;

					if (request.CampusId.HasValue)
						person.CampusId = request.CampusId;

					if (request.ProgramOfStudyId.IsNotNull() && request.ProgramOfStudyId != 0)
						person.ProgramAreaId = request.ProgramOfStudyId;

					if (request.DegreeId.IsNotNull() && request.DegreeId != 0)
						person.DegreeId = request.DegreeId;

					// Update user screen name accordingly
					user.ScreenName = (person.FirstName.IsNullOrEmpty() || person.LastName.IsNullOrEmpty()) ? person.EmailAddress : string.Format("{0} {1}", person.FirstName, person.LastName);
					if (user.ScreenName.Length > 50)
						user.ScreenName = user.ScreenName.Substring(0, 50);

					#endregion

					#region Phone number

					if (request.PrimaryPhone.IsNotNull() && request.PrimaryPhone.PhoneNumber.IsNotNullOrEmpty() && request.UpdatePhoneNumber.IsNull())
					{
						request.UpdatePhoneNumber = new PhoneNumberDto
						{
							IsPrimary = true,
							Number = request.PrimaryPhone.PhoneNumber,
							Extension = request.PrimaryPhone.Extention,
							ProviderId = request.PrimaryPhone.Provider
						};

						switch (request.PrimaryPhone.PhoneType)
						{
							case PhoneType.Cell:
								request.UpdatePhoneNumber.PhoneType = PhoneTypes.Mobile;
								break;
							case PhoneType.NonUS:
							case PhoneType.Home:
							case PhoneType.Work:
								request.UpdatePhoneNumber.PhoneType = PhoneTypes.Phone;
								break;
							case PhoneType.Fax:
								request.UpdatePhoneNumber.PhoneType = PhoneTypes.Fax;
								break;
							default:
								request.UpdatePhoneNumber.PhoneType = PhoneTypes.Phone;
								break;
						}
					}

					if (request.UpdatePhoneNumber.IsNotNull())
					{
						var phone = (request.UpdatePhoneNumber.Id.IsNotNull() && request.UpdatePhoneNumber.Id > 0)
							? person.PhoneNumbers.FirstOrDefault(x => x.Id == request.UpdatePhoneNumber.Id)
							: person.PhoneNumbers.FirstOrDefault(pn => pn.IsPrimary);

						if (phone.IsNull())
						{
							if (request.UpdatePhoneNumber.Id.IsNotNull() && request.UpdatePhoneNumber.Id > 0)
							{
								response.SetFailure(FormatErrorMessage(request, ErrorTypes.UnableToLocatePhoneRecord));
								Logger.Info(request.LogData(), string.Format("Phone number record not found for ID '{0}'.", request.UpdatePhoneNumber.Id));
								return response;
							}

							phone = new PhoneNumber();
							Repositories.Core.Add(phone);
						}

						phone.Number = new string(request.UpdatePhoneNumber.Number.Where(char.IsDigit).ToArray());
						phone.Extension = request.UpdatePhoneNumber.Extension;
						phone.PhoneType = request.UpdatePhoneNumber.PhoneType;
						phone.ProviderId = request.UpdatePhoneNumber.ProviderId;
						phone.PersonId = user.PersonId;
						phone.IsPrimary = request.UpdatePhoneNumber.IsPrimary;
					}

					#endregion

					#region Address

					if (request.PostalAddress.IsNotNull())
					{
						var address = person.PersonAddresses.FirstOrDefault(x => x.IsPrimary);

						if (address.IsNull())
						{
							address = new PersonAddress();
							Repositories.Core.Add(address);
						}

						address.Line1 = request.PostalAddress.Street1;
						address.TownCity = request.PostalAddress.City;
						address.StateId = request.PostalAddress.StateId ?? 0;
						address.PostcodeZip = request.PostalAddress.Zip;
						address.PersonId = user.PersonId;
					}

					#endregion

					Repositories.Core.SaveChanges();

					response.UserId = user.Id;
					response.PersonId = user.PersonId;

					if (!request.IgnoreClient)
						user.PublishUserUpdateIntegrationRequest(request.UserContext.UserId, RuntimeContext);

					// Log the action
					LogAction(request, ActionTypes.UpdateUser, user);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the users for look up.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public UserLookupResponse GetUserLookup(UserLookupRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new UserLookupResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var query = new UserQuery(Repositories.Core, request.Criteria).Query();

					response.Users = (from q in query
						join p in Repositories.Core.Persons on q.PersonId equals p.Id
						orderby p.LastName, p.FirstName
						select new UserLookup { Id = q.Id, Name = string.Format("{0}, {1}", p.LastName, p.FirstName) }).ToList();
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Resets the password.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public ResetPasswordResponse ResetPassword(ResetPasswordRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new ResetPasswordResponse(request);

				// Validate client tag
				if (!ValidateRequest(request, response, Validate.ClientTag | Validate.License | Validate.AccessToken))
					return response;

				try
				{
					// Validate user id
					if (!request.UserId.HasValue)
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.UnableToResetPassword));
						Logger.Info(request.LogData(), "Unable to reset password as no user id provided.");
						return response;
					}

					var user = Repositories.Core.FindById<User>(request.UserId);

					if (user.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.UnableToResetPassword));
						Logger.Info(request.LogData(), string.Format("Unable to reset password as no user found for id {0}.", request.UserId));
						return response;
					}

					// Confirm validation key
					if (user.ValidationKey != request.ValidationKey)
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.UnableToResetPassword));
						Logger.Info(request.LogData(), string.Format("Unable to reset password for user id {0} as validation key does not match.", request.UserId));
						return response;
					}

					// Confirm expiry date
					if (user.ValidationKeyExpiry <= DateTime.Now)
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.UnableToResetPassword));
						Logger.Info(request.LogData(), string.Format("Unable to reset password for user id {0} as validation key has expired.", request.UserId));
						return response;
					}

					// Validate new password
					if (request.NewPassword.IsNullOrEmpty())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.UnableToResetPassword));
						Logger.Info(request.LogData(), "Unable to reset password as no new password provided.");
						return response;
					}

					var newPassword = new Password(AppSettings, request.NewPassword, user.PasswordSalt);
					user.PasswordHash = newPassword.Hash;
	        user.MigratedPasswordHash = null;

					// Log the use of the key
					user.UserValidationKeyLogs.Add(new UserValidationKeyLog
					{
						Status = UserValidationKeyStatus.Used,
						ValidationKey = user.ValidationKey,
						LogDate = DateTime.Now
					});

					// Clear validation key so it cannot be reused
					user.ValidationKey = null;
					user.ValidationKeyExpiry = null;

					Repositories.Core.SaveChanges();

					// Log Reset Password and Change Password
					LogAction(request, ActionTypes.ResetPassword, user);
					LogAction(request, ActionTypes.ChangePassword, user);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.UnableToValidatePasswordResetValidationKey), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Registers the explorer user.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public RegisterExplorerUserResponse RegisterExplorerUser(RegisterExplorerUserRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new RegisterExplorerUserResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.ClientTag | Validate.SessionId | Validate.License | Validate.AccessToken))
					return response;

				try
				{
					#region Check user doesn't exist

					var user = (AppSettings.SSOEnabled || AppSettings.SamlEnabledForCareer)
						? Repositories.Core.Users.FirstOrDefault(x => x.ExternalId == request.ExternalId)
						: Repositories.Core.Users.FirstOrDefault(x => x.UserName == request.EmailAddress);

					if (user.IsNotNull() && (!(AppSettings.SSOEnabled || AppSettings.SamlEnabledForCareer)))
					{
						response.SetFailure(ErrorTypes.UserNameAlreadyExists, FormatErrorMessage(request, ErrorTypes.UserNameAlreadyExists));
						Logger.Info(request.LogData(), string.Format("An user with username '{0}' already exists.", request.EmailAddress));

						return response;
					}

					user = user ?? new User();

					#endregion

					#region Add User and Roles

					var explorerUserRole = Repositories.Core.Roles.SingleOrDefault(x => x.Key == Constants.RoleKeys.ExplorerUser);

					if (explorerUserRole.IsNull())
					{
						response.SetFailure(ErrorTypes.UserRoleDoesNotExist, FormatErrorMessage(request, ErrorTypes.UserRoleDoesNotExist));
						Logger.Info(request.LogData(), "The ExplorerUser role could not be found in the database");

						return response;
					}

					var password = (AppSettings.SSOEnabled || AppSettings.SamlEnabledForCareer)
						? new Password(AppSettings, "Pasty" + request.ExternalId, false)
						: new Password(AppSettings, request.Password);

					user.UserName = request.EmailAddress;
					user.PasswordSalt = password.Salt;
					user.PasswordHash = password.Hash;
					user.UserType = UserTypes.Explorer;
					user.Enabled = true;
					user.ScreenName = request.ScreenName;
					user.RegulationsConsent = request.TandCConsentGiven;

					if (AppSettings.SSOEnabled || AppSettings.SamlEnabledForCareer)
					{
						user.ExternalId = request.ExternalId;
						user.UserName = String.Format("sso-{0}@ci.bgt.com", request.ExternalId);
						user.IsMigrated = true;
					}

					if (user.Roles.All(r => r.Id != explorerUserRole.Id))
						user.Roles.Add(explorerUserRole);

					#endregion

					#region Create the person record

					var person = new Person
					{
						TitleId = 0,
						FirstName = (request.FirstName.IsNotNullOrEmpty()) ? request.FirstName : "Unknown",
						MiddleInitial = request.MiddleInitial,
						LastName = (request.LastName.IsNotNullOrEmpty()) ? request.LastName : "Unknown",
						DateOfBirth = request.DateOfBirth,
						SocialSecurityNumber = request.SocialSecurityNumber,
						EmailAddress = request.EmailAddress,
						User = user
					};

					#endregion

					#region Add the phone number if provided

					if (request.PhoneNumber.IsNotNullOrEmpty())
						person.PhoneNumbers.Add(new PhoneNumber
						{
							PhoneType = PhoneTypes.Phone,
							IsPrimary = true,
							Number = request.PhoneNumber
						});

					#endregion

					Repositories.Core.Add(person);
					Repositories.Core.SaveChanges();

					var securityQuestions = new List<UserSecurityQuestionDto>();
					if( request.SecurityQuestion.IsNotNullOrEmpty() || request.SecurityQuestionId.HasValue )
					{
						securityQuestions.Add(new UserSecurityQuestionDto
						{
							QuestionIndex = 1,
							SecurityQuestion = request.SecurityQuestion,
							SecurityQuestionId = request.SecurityQuestionId,
							SecurityAnswerEncrypted = request.SecurityAnswer.IsNotNullOrEmpty()
								? request.SecurityAnswer
								: null
						});
					}

					Helpers.User.UpdateSecurityQuestions(user.Id, securityQuestions.ToArray(), true);

					// Log the action
					LogAction(request, ActionTypes.RegisterAccount, user);

					// Save the resume if one has been passed up
					if (request.ResumeModel != null)
						response.ResumeModel = UpdateResume(request, request.ResumeModel);

					// Email out registration details to notification email account(s)
					if (AppSettings.ExplorerRegistrationNotificationEmails.IsNotNullOrEmpty())
					{
						var emailSubject = Localise(request, "System.Email.ExplorerRegistration.Subject").Replace(Constants.PlaceHolders.FullName, (person.FirstName + " " + person.LastName));
						var emailBody = Localise(request, "System.Email.ExplorerRegistration.Body").
							Replace(Constants.PlaceHolders.FirstName, person.FirstName).
							Replace(Constants.PlaceHolders.LastName, person.LastName).
							Replace(Constants.PlaceHolders.EmailAddress, person.EmailAddress).
							Replace(Constants.PlaceHolders.PhoneNumber,
								(request.PhoneNumber.IsNotNullOrEmpty()
									? Regex.Replace(request.PhoneNumber, AppSettings.PhoneNumberStrictRegExPattern,
										AppSettings.PhoneNumberFormat)
									: "")).
							Replace(Constants.PlaceHolders.NowDateTime, DateTime.Now.ToString("dd-MMM-yyyy hh:mm"));

						Helpers.Email.SendEmail(AppSettings.ExplorerRegistrationNotificationEmails, "", "", emailSubject, emailBody, true);
					}

				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Updates the screen name and email address.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public UpdateScreenNameAndEmailAddressResponse UpdateScreenNameAndEmailAddress(UpdateScreenNameAndEmailAddressRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new UpdateScreenNameAndEmailAddressResponse(request);

				// Validate client tag
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var user = Repositories.Core.FindById<User>(request.UserContext.UserId);

					if (user.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.UnableToUpdateScreenNameAndEmailAddress));
						Logger.Info(request.LogData(), string.Format("Unable to update user data as no user found for id {0}.", request.UserContext.UserId));
						return response;
					}

					user.ScreenName = request.ScreenName;
					user.Person.EmailAddress = request.EmailAddress;

					Repositories.Core.SaveChanges();

					user.PublishUserUpdateIntegrationRequest(request.UserContext.UserId, RuntimeContext);

					// Log Reset Password and Change Password
					LogAction(request, ActionTypes.UpdateScreenNameAndEmailAddress, user);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.UnableToUpdateScreenNameAndEmailAddress), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Updates the screen name and email address.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public UpdateManagerFlagResponse UpdateManagerFlag(UpdateManagerFlagRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new UpdateManagerFlagResponse(request);

				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var person = Repositories.Core.FindById<Person>(request.PersonId);

					if (person.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.UnableToChangeManagerFlag));
						Logger.Info(request.LogData(), string.Format("Unable to update person data as no user found for id {0}.", request.PersonId));
						return response;
					}

					person.Manager = request.Manager;

					Repositories.Core.SaveChanges();

					LogAction(request, ActionTypes.UpdateManagerFlag, person);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.UnableToUpdateScreenNameAndEmailAddress), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Changes the security question.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public ChangeSecurityQuestionResponse ChangeSecurityQuestion(ChangeSecurityQuestionRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new ChangeSecurityQuestionResponse(request);

				// Validate client tag
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var user = Repositories.Core.FindById<User>(request.UserContext.UserId);

					if (user.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.UnableToChangeSecurityQuestion));
						Logger.Info(request.LogData(), string.Format("Unable to change security question as no user found for id {0}.", request.UserContext.UserId));
						return response;
					}

					if (request.Questions.IsNotNullOrEmpty())
					{
						Helpers.User.UpdateSecurityQuestions(user.Id, request.Questions.ToArray(), request.ChangeSingleQuestion);
					}

					if ((user.UserType & UserTypes.Career) == UserTypes.Career)
						Helpers.SelfService.PublishSelfServiceActivity(ActionTypes.ChangeSecurityQuestion, request.UserContext.ActionerId, user.Id, user.PersonId);

					// Log Reset Password and Change Password
					LogAction(request, ActionTypes.ChangeSecurityQuestion, user);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.UnableToChangeSecurityQuestion), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Registers the career user.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		/// <exception cref="System.NotImplementedException"></exception>
		public RegisterCareerUserResponse RegisterCareerUser(RegisterCareerUserRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new RegisterCareerUserResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.ClientTag | Validate.SessionId | Validate.License | Validate.AccessToken))
					return response;

				try
				{

					var user = (AppSettings.SSOEnabled || AppSettings.SamlEnabledForCareer)
						? Repositories.Core.Users.FirstOrDefault(x => x.ExternalId == request.ExternalId)
						: null;

					user = user ?? new User();

					#region Add User and Roles

					var careerUserRole = Repositories.Core.Roles.SingleOrDefault(x => x.Key == Constants.RoleKeys.CareerUser);

					if (careerUserRole.IsNull())
					{
						response.SetFailure(ErrorTypes.UserRoleDoesNotExist, FormatErrorMessage(request, ErrorTypes.UserRoleDoesNotExist));
						Logger.Info(request.LogData(), "The CareerUser role could not be found in the database");

						return response;
					}

					var explorerUserRole = Repositories.Core.Roles.SingleOrDefault(x => x.Key == Constants.RoleKeys.ExplorerUser);

					if (explorerUserRole.IsNull())
					{
						response.SetFailure(ErrorTypes.UserRoleDoesNotExist, FormatErrorMessage(request, ErrorTypes.UserRoleDoesNotExist));
						Logger.Info(request.LogData(), "The ExplorerUser role could not be found in the database");

						return response;
					}

					var password = (AppSettings.SSOEnabled|| AppSettings.SamlEnabledForCareer)
						? new Password(AppSettings, "Pasty" + request.ExternalId, false)
						: new Password(AppSettings, request.AccountPassword);

					#region Determine the type of user to create (career / careerexplorer)

					UserTypes? userType = null;

					if (AppSettings.CareerExplorerModule == FocusModules.Career || AppSettings.CareerExplorerModule == FocusModules.CareerExplorer)
					{
						userType = UserTypes.Career;
					}

					if (AppSettings.CareerExplorerModule == FocusModules.Explorer || AppSettings.CareerExplorerModule == FocusModules.CareerExplorer)
					{
						userType = userType.IsNull() ? UserTypes.Explorer : userType | UserTypes.Explorer;
					}

					#endregion

					var enabledUser = true;

					if (request.Enabled.HasValue)
						enabledUser = (bool)request.Enabled;

					user.UserName = request.EmailAddress;
					user.PasswordSalt = password.Salt;
					user.PasswordHash = password.Hash;
					user.MigratedPasswordHash = request.MigratedPasswordHash;
					user.UserType = userType;
					user.Enabled = enabledUser;
					user.ScreenName = (request.FirstName.IsNullOrEmpty() || request.LastName.IsNullOrEmpty()) ? request.EmailAddress : string.Format("{0} {1}", request.FirstName, request.LastName);
					user.IsMigrated = !request.IsMigrated.HasValue || (bool)request.IsMigrated;
					user.RegulationsConsent = !request.RegulationsConsent.HasValue || (bool)request.RegulationsConsent;
					user.ExternalId = request.ExternalId.IsNotNullOrEmpty() ? request.ExternalId : null;
					user.Pin = request.Pin.IsNotNullOrEmpty() ? request.Pin : null;
					user.LoggedInOn = request.LastLoggedIn;

					if (AppSettings.SSOEnabled || AppSettings.SamlEnabledForCareer)
					{
						if (request.ScreenName.IsNotNullOrEmpty()) user.ScreenName = request.ScreenName;
						user.UserName = String.Format("sso-{0}@ci.bgt.com", request.ExternalId);
					}

					if (user.ScreenName.Length > 50)
						user.ScreenName = user.ScreenName.Substring(0, 50);

					if ((userType & UserTypes.Career) == UserTypes.Career && user.Roles.All(r => r.Id != careerUserRole.Id))
						user.Roles.Add(careerUserRole);

					if ((userType & UserTypes.Explorer) == UserTypes.Explorer && user.Roles.All(r => r.Id != explorerUserRole.Id))
						user.Roles.Add(explorerUserRole);

					#endregion

					#region Create the PersonAddress record

					var personAddress = (request.PostalAddress.IsNotNull())
						? new PersonAddress
						{
							Line1 = request.PostalAddress.Street1,
							Line2 = request.PostalAddress.Street2,
							TownCity = (request.PostalAddress.City.IsNotNullOrEmpty()) ? request.PostalAddress.City.TruncateString(100) : String.Empty,
							StateId = request.PostalAddress.StateId ?? 0,
							CountyId = request.PostalAddress.CountyId,
							CountryId = request.PostalAddress.CountryId ?? 0,
							PostcodeZip = (request.PostalAddress.Zip.IsNotNull()) ? request.PostalAddress.Zip : String.Empty,
							IsPrimary = true
						}
						: new PersonAddress
						{
							Line1 = null,
							Line2 = null,
							TownCity = String.Empty,
							StateId = 0,
							CountyId = null,
							CountryId = 0,
							PostcodeZip = String.Empty,
							IsPrimary = true
						};

					#endregion

					#region Create the person record

					var person = new Person
					{
						Id = request.InsertPersonId ?? 0,
						TitleId = 0,
						FirstName = (request.FirstName.IsNotNullOrEmpty()) ? request.FirstName : "Unknown",
						MiddleInitial = request.MiddleInitial.IsNotNullOrEmpty() ? request.MiddleInitial.TruncateString(5) : request.MiddleInitial,
						LastName = (request.LastName.IsNotNullOrEmpty()) ? request.LastName : "Unknown",
						SuffixId = request.SuffixId,
						DateOfBirth = request.DateOfBirth,
						SocialSecurityNumber = request.SocialSecurityNumber,
						EmailAddress = request.EmailAddress,
						User = user,
						EnrollmentStatus = request.EnrollmentStatus,
						ProgramAreaId = request.ProgramOfStudyId,
						CampusId = request.CampusId,
						DegreeId = request.DegreeId,
						AccountType = request.AccountType
					};

					if (personAddress.IsNotNull())
						person.PersonAddresses.Add(personAddress);

					// Add a default issues record
					var issues = new Issues { PersonId = person.Id };
					Repositories.Core.Add(issues);

					#endregion

					#region Add the phone number if provided

					if (request.PrimaryPhone.IsNotNull() && request.PrimaryPhone.PhoneNumber.IsNotNullOrEmpty())
					{
						PhoneTypes phoneType;

						switch (request.PrimaryPhone.PhoneType)
						{
							case PhoneType.Cell:
								phoneType = PhoneTypes.Mobile;
								break;
							case PhoneType.NonUS:
							case PhoneType.Home:
							case PhoneType.Work:
								phoneType = PhoneTypes.Phone;
								break;
							case PhoneType.Fax:
								phoneType = PhoneTypes.Fax;
								break;
							default:
								phoneType = PhoneTypes.Phone;
								break;
						}

						request.PrimaryPhone.PhoneNumber = new string(request.PrimaryPhone.PhoneNumber.Where(char.IsDigit).ToArray());

						person.PhoneNumbers.Add(new PhoneNumber
						{
							PhoneType = phoneType,
							IsPrimary = true,
							Number = request.PrimaryPhone.PhoneNumber,
							ProviderId = request.PrimaryPhone.Provider
						});
					}

					#endregion

					#region Remove pin from RegistrationPin table

					if (request.Pin.IsNotNullOrEmpty())
					{
						var registrationPin = Repositories.Core.RegistrationPins.FirstOrDefault(x => x.Pin == request.Pin && x.EmailAddress == request.PinRegisteredEmailAddress);

						if (registrationPin.IsNotNull())
						{
							Repositories.Core.Remove(registrationPin);
						}
						else
						{
							response.SetFailure(ErrorTypes.RegistrationPinNotFound, FormatErrorMessage(request, ErrorTypes.RegistrationPinNotFound));
							Logger.Info(request.LogData(), string.Format("Unable to find a registration pin with email address '{0}' and PIN '{1}' already exists.", request.EmailAddress, request.Pin));

							return response;
						}
					}

					#endregion

          CensorshipCheckEmail(person.EmailAddress, person.Id, issues);

					#region Check user doesn't exist

					// Check if user name exists (Done as last possible point should service method being called multiple times using WCF)
					if (!(AppSettings.SSOEnabled || AppSettings.SamlEnabledForCareer))
					{
						var existsUserId = Repositories.Core.Users.Where(x => x.UserName == request.EmailAddress).Select(x => x.Id).FirstOrDefault();
						if (existsUserId > 0)
						{
							response.SetFailure(ErrorTypes.UserNameAlreadyExists, FormatErrorMessage(request, ErrorTypes.UserNameAlreadyExists));
							Logger.Info(request.LogData(), string.Format("An user with username '{0}' already exists.", request.EmailAddress));

							return response;
						}
					}

					#endregion

					#region Create PersonOfficeMapper record

					var offices = new List<Office>();
					var officeUnassigned = (bool?)null;

					// Get the office that deals with the postcode of the jobseeker
					if (AppSettings.OfficesEnabled)
					{
						var hasPostalCode = (request.PostalAddress.IsNotNull() && request.PostalAddress.Zip.IsNotNullOrEmpty());

						if (hasPostalCode)
							offices = Repositories.Core.Offices.Where(x => x.AssignedPostcodeZip.Contains(MiscUtils.SafeSubstring(request.PostalAddress.Zip, 0, 5))).ToList();

						// FVN-185 : Code temporarily commented out as logic may not be what was actually required by the business
						/*
            // When being set up by an Assist user, the office the Assist is currently working on must be assigned to the job seeker
            if (request.OverrideOfficeId.IsNotNull())
            {
              var overrideOffice = Repositories.Core.FindById<Office>(request.OverrideOfficeId);
              offices.Add(overrideOffice);

              // Also, the job seekers zip is added to the list of postal codes for the current office
              if (hasPostalCode && (overrideOffice.AssignedPostcodeZip.IsNull() || !overrideOffice.AssignedPostcodeZip.Contains(request.PostalAddress.Zip)))
              {
                overrideOffice.AssignedPostcodeZip = overrideOffice.AssignedPostcodeZip.IsNullOrEmpty()
                                                       ? request.PostalAddress.Zip
                                                       : string.Concat(overrideOffice.AssignedPostcodeZip, ",", request.PostalAddress.Zip);
              }
            }
            */
					}

					if (offices.IsNull() || offices.Count == 0)
					{
						// Get default Office
						var defaultOffice = Repositories.Core.Offices.FirstOrDefault(x => (x.DefaultType & OfficeDefaultType.JobSeeker) == OfficeDefaultType.JobSeeker);

						if (defaultOffice.IsNotNull())
						{
							offices.Add(defaultOffice);
							officeUnassigned = true;
						}
					}

					var personOfficesSaved = new List<PersonOfficeMapper>();

					if (offices.IsNotNull() && offices.Count > 0)
					{
						var personOffices = from o in offices
																select new PersonOfficeMapper { OfficeId = o.Id, PersonId = person.Id, OfficeUnassigned = officeUnassigned };


						foreach (var personOffice in personOffices)
						{
							Repositories.Core.Add(personOffice);

							personOfficesSaved.Add(personOffice);
						}
					}

					#endregion

					Repositories.Core.SaveChanges();

					var securityQuestions = new List<UserSecurityQuestionDto>();
					if( request.SecurityQuestion.IsNotNullOrEmpty() || request.SecurityQuestionId.HasValue )
					{
						securityQuestions.Add(new UserSecurityQuestionDto
						{
							QuestionIndex = 1,
							SecurityQuestion = request.SecurityQuestion,
							SecurityQuestionId = request.SecurityQuestionId,
							SecurityAnswerEncrypted = request.SecurityAnswer.IsNotNullOrEmpty()
								? request.SecurityAnswer
								: null
						});
					}

					Helpers.User.UpdateSecurityQuestions(user.Id, securityQuestions.ToArray(), false);

					if (request.OverrideCreationDate.IsNotNull())
					{
						var updateQuery = new Query(typeof(User), Entity.Attribute("Id") == user.Id);
						Repositories.Core.Update(updateQuery, new { CreatedOn = request.OverrideCreationDate });
						Repositories.Core.SaveChanges();
					}

					response.PersonId = person.Id;

					// Log the action
                    //AIM-FVN-6822 Fixed the UserId issue in ActionEvent Table
		            LogAction(request, ActionTypes.RegisterAccount, user, request.UserContext.UserId, actionedOn: request.OverrideCreationDate);

					// Log the action for person office mappers
					personOfficesSaved.ForEach(ed => LogAction(request, ActionTypes.AddPersonOffice, typeof(PersonOfficeMapper).Name, ed.Id, request.UserContext.UserId));

					// Email out registration details to notification email account(s)
					if (AppSettings.Theme != FocusThemes.Education && AppSettings.CareerRegistrationNotificationEmails.IsNotNullOrEmpty())
					{
						var emailSubject = Localise(request, "System.Email.CareerRegistration.Subject")
							.Replace(Constants.PlaceHolders.FullName, (person.FirstName + " " + person.LastName));
						var emailBody = Localise(request, "System.Email.CareerRegistration.Body")
							.Replace(Constants.PlaceHolders.FirstName, person.FirstName)
							.Replace(Constants.PlaceHolders.PhoneNumber, (request.PrimaryPhone.PhoneNumber.IsNotNullOrEmpty() ? request.PrimaryPhone.PhoneNumber : ""))
							.Replace(Constants.PlaceHolders.NowDateTime, DateTime.Now.ToString("dd-MMM-yyyy hh:mm"));
						Helpers.Email.SendEmail(AppSettings.CareerRegistrationNotificationEmails, "", "", emailSubject, emailBody, true);
					}
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the user alert settings.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns>The user alert response</returns>
		public GetUserAlertResponse GetUserAlert(GetUserAlertRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new GetUserAlertResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var userAlert = (from ua in Repositories.Core.UserAlerts
						where ua.UserId == request.Criteria.UserId
						select ua).FirstOrDefault();

					// Records may not initially exist in the database
					response.UserAlert = userAlert.IsNotNull() ? userAlert.AsDto() : new UserAlertDto { UserId = request.Criteria.UserId };
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}
				return response;
			}
		}

		/// <summary>
		/// Updates the contact details. Note: that if the value of a contact detail is null or empty it is deleted.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public UpdateUserAlertResponse UpdateUserAlert(UpdateUserAlertRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new UpdateUserAlertResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					// Check a record already exists
					var userAlert = (from ua in Repositories.Core.UserAlerts
						where ua.UserId == request.UserAlert.UserId
						select ua).FirstOrDefault();

					// If not, a new one needs to be added
					if (userAlert == null)
					{
						userAlert = new UserAlert { UserId = request.UserAlert.UserId };
						Repositories.Core.Add(userAlert);
					}

					// Update the fields on the record
					userAlert.Frequency = request.UserAlert.Frequency;
					userAlert.NewReferralRequests = request.UserAlert.NewReferralRequests;
					userAlert.EmployerAccountRequests = request.UserAlert.EmployerAccountRequests;
					userAlert.NewJobPostings = request.UserAlert.NewJobPostings;

					Repositories.Core.SaveChanges();

					response.UserAlert = userAlert.AsDto();

					// Log the action
					LogAction(request, ActionTypes.UpdateUserAlert, userAlert);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Changes a phone number
		/// </summary>
		/// <param name="request">The request to change the phone number</param>
		/// <returns>A response with details of the change</returns>
		public ChangePhoneNumberResponse ChangePhoneNumber(ChangePhoneNumberRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new ChangePhoneNumberResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				if (request.PhoneNumber.PersonId == 0)
					return response;

				try
				{
					PhoneNumber phoneNumber = null;

					if (request.PhoneNumber.IsNotNull() && request.PhoneNumber.Number.IsNotNullOrEmpty())
						request.PhoneNumber.Number = new string(request.PhoneNumber.Number.Where(char.IsDigit).ToArray());

					if (request.PhoneNumber.Id.IsNull() || request.PhoneNumber.Id == 0)
					{
						if (request.PhoneNumber.Number.Length > 0 || request.PhoneNumber.ProviderId.GetValueOrDefault(0) != 0)
						{
							phoneNumber = new PhoneNumber();
							request.PhoneNumber.CopyTo(phoneNumber);
							Repositories.Core.Add(phoneNumber);

							Repositories.Core.SaveChanges();
						}
					}
					else
					{
						phoneNumber = Repositories.Core.PhoneNumbers.First(pn => pn.Id == request.PhoneNumber.Id);
						if (request.PhoneNumber.Number.Length == 0 && request.PhoneNumber.ProviderId.GetValueOrDefault(0) == 0)
							Repositories.Core.Remove(phoneNumber);
						else
							request.PhoneNumber.CopyTo(phoneNumber);

						Repositories.Core.SaveChanges();
					}

					// Log the action
					if (phoneNumber != null)
						LogAction(request, ActionTypes.ChangePhoneNumber, phoneNumber);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}


				return response;
			}
		}

		/// <summary>
		/// Change Migrated Status
		/// </summary>
		/// <param name="request">ChangeMigratedStatusRequest contains migrated user status</param>
		/// <returns>status</returns>
		public ChangeMigratedStatusResponse ChangeMigratedStatus(ChangeMigratedStatusRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new ChangeMigratedStatusResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var user = (from u in Repositories.Core.Users
						where u.Id == request.UserContext.UserId
						select u).FirstOrDefault();

					if (user.IsNotNull())
					{
						user.IsMigrated = request.IsMigrated;

						Repositories.Core.SaveChanges();

						// Log the action
						LogAction(request, ActionTypes.ChangeMigratedStatus, user);
					}
					else
					{
						throw new Exception("Unable to find user by ID");
					}
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}
				return response;
			}
		}

		/// <summary>
		/// Change Consent Status
		/// </summary>
		/// <param name="request">ChangeConsentStatusRequest contains Consent status</param>
		/// <returns>status</returns>
		public ChangeConsentStatusResponse ChangeConsentStatus(ChangeConsentStatusRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new ChangeConsentStatusResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var user = (from u in Repositories.Core.Users
						where u.Id == request.UserContext.UserId
						select u).FirstOrDefault();

					if (user.IsNotNull())
					{
						user.RegulationsConsent = request.IsConsentGiven;

						Repositories.Core.SaveChanges();

						// Log the action
						LogAction(request, ActionTypes.ChangeRegulationsConsentStatus, user);
					}
					else
					{
						throw new Exception("Unable to find user by ID");
					}
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}
				return response;
			}

		}

		/// <summary>
		/// Changes the enrollment status.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public ChangeEnrollmentStatusResponse ChangeEnrollmentStatus(ChangeEnrollmentStatusRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new ChangeEnrollmentStatusResponse(request);

				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var user = Repositories.Core.FindById<User>(request.UserContext.UserId);

					if (user.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.UnableToChangeEnrollmentStatus));
						Logger.Info(request.LogData(), string.Format("Unable to change enrollment status as no user found for id {0}.", request.UserContext.UserId));
						return response;
					}

					user.Person.EnrollmentStatus = request.EnrollmentStatus;

					Repositories.Core.SaveChanges();

					// Log Change Enrollment Status
					LogAction(request, ActionTypes.ChangeEnrollmentStatus, user);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.UnableToChangeEnrollmentStatus), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Changes the campus location.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public ChangeCampusResponse ChangeCampus(ChangeCampusRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new ChangeCampusResponse(request);

				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var userId = request.ChangeForUserId ?? request.UserContext.UserId;

					var user = Repositories.Core.FindById<User>(userId);

					if (user.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.UnableToChangeCampus));
						Logger.Info(request.LogData(), string.Format("Unable to change campus location as no user found for id {0}.", userId));
						return response;
					}

					user.Person.CampusId = request.CampusId;

					Repositories.Core.SaveChanges();

					// Log Change Campust
					LogAction(request, ActionTypes.ChangeCampus, user);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.UnableToChangeCampus), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		#region Program Of Study

		/// <summary>
		/// Changes the program of study.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public ChangeProgramOfStudyResponse ChangeProgramOfStudy(ChangeProgramOfStudyRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new ChangeProgramOfStudyResponse(request);

				// Validate client tag
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var user = Repositories.Core.FindById<User>(request.UserContext.UserId);

					if (user.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.UnableToChangeProgramOfStudy));
						Logger.Info(request.LogData(), string.Format("Unable to change program of study as no user found for id {0}.", request.UserContext.UserId));
						return response;
					}

					user.Person.DegreeId = request.StudyProgramDegreeId;
					user.Person.ProgramAreaId = request.StudyProgramAreaId;

					Repositories.Core.SaveChanges();

					// Log Change Program of Study
					LogAction(request, ActionTypes.ChangeProgramOfStudy, user);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.UnableToChangeProgramOfStudy), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		#endregion

		public ValidateEmailResponse ValidateEmail(ValidateEmailRequest request)
		{
			var response = new ValidateEmailResponse(request);

			if (!ValidateRequest(request, response, Validate.All))
				return response;

			if (request.Emails.IsNullOrEmpty())
				return response;

			try
			{
				var trimChars = new[] { ' ', ',', ';' };

				var pattern = AppSettings.EmailAddressRegExPattern;

				// Ensure matching on the whole string
				if (!pattern.StartsWith("^"))
					pattern = string.Concat("^", pattern);
				if (!pattern.EndsWith("$"))
					pattern = string.Concat(pattern, "$");

				var regEx = new Regex(pattern);

				foreach (var email in request.Emails)
				{
					var trimmedEmail = email.Trim(trimChars);

					if (!regEx.IsMatch(email))
					{
						#region Validate email is in an email format

						if (response.InvalidEmailFormat.IsNull())
							response.InvalidEmailFormat = new List<string>();

						response.InvalidEmailFormat.Add(email);

						#endregion
					}
					else if (Regex.Matches(email, "@").Count > 1)
					{
						#region Validate only one email address is contained on each element

						if (response.MultipleEmailError.IsNull())
							response.MultipleEmailError = new List<string>();

						response.MultipleEmailError.Add(email);

						#endregion
					}
					else if (response.ValidEmail.IsNotNull() && response.ValidEmail.Any(e => String.Compare(e, trimmedEmail, StringComparison.OrdinalIgnoreCase) == 0))
					{
						#region Validate email address has not already been entered

						if (response.DuplicateEmailError.IsNull())
							response.DuplicateEmailError = new List<string>();

						response.DuplicateEmailError.Add(email);

						#endregion
					}
					else if (Repositories.Core.Users.Any(x => x.UserName == email && x.UserType != request.UserType))
					{
						#region Validate Email Address is not being used as a user name in another Module

						if (response.UsernameInUse.IsNull())
							response.UsernameInUse = new List<string>();

						response.UsernameInUse.Add(email);

						#endregion
					}
					else
					{
						if (response.ValidEmail.IsNull())
							response.ValidEmail = new List<string>();

						response.ValidEmail.Add(trimmedEmail);
					}
				}
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.UnableToValidateEmail), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}

			return response;
		}

		/// <summary>
		/// Sends the registration pin email.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public SendRegistrationPinEmailResponse SendRegistrationPinEmail(SendRegistrationPinEmailRequest request)
		{
			var response = new SendRegistrationPinEmailResponse(request);

			if (!ValidateRequest(request, response, Validate.All))
				return response;

			if (request.Email.IsNullOrEmpty())
				return response;

			try
			{
				Helpers.Messaging.Publish(request.ToProcessRegistrationPinEmailMessage(request.Email, request.PinRegistrationUrl, request.ResetPasswordUrl, request.TargetModule));
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.UnableToValidateEmail), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}

			return response;
		}

		/// <summary>
		/// Updates the last surveyed date.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public UpdateLastSurveyedDateResponse UpdateLastSurveyedDate(UpdateLastSurveyedDateRequest request)
		{
			var response = new UpdateLastSurveyedDateResponse(request);

			if (!ValidateRequest(request, response, Validate.All))
				return response;

			if (request.PersonId.IsNull() || request.PersonId <= 0)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.PersonNotFound));
				Logger.Info(request.LogData(), string.Format("Invalid Person ID: {0}.", request.PersonId));
				return response;
			}

			var person = Repositories.Core.FindById<Person>(request.PersonId);

			if (person.IsNull())
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.PersonNotFound));
				Logger.Info(request.LogData(), string.Format("Invalid Person ID: {0}.", request.PersonId));
				return response;
			}

			person.LastSurveyedOn = DateTime.Now;
			Repositories.Core.SaveChanges();

			return response;
		}

		/// <summary>
		/// Exports the assist users.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public ExportAssistUsersResponse ExportAssistUsers(ExportAssistUsersRequest request)
		{
			var response = new ExportAssistUsersResponse(request);

			if (!ValidateRequest(request, response, Validate.All))
				return response;

			response.ValidationErrors = false;

			#region Export user data from file

			var modelList = new List<CreateAssistUserModel>();

			var documentFileType = DocumentFileType.Xlsx;

			var extension = Path.GetExtension(request.FileName);

			if (extension.IsNotNullOrEmpty())
			{
				switch (extension.ToLower())
				{
					case ".xlsx":
						documentFileType = DocumentFileType.Xlsx;
						break;

					case ".xls":
						documentFileType = DocumentFileType.Xls;
						break;

					case ".csv":
						documentFileType = DocumentFileType.Csv;
						break;

					default:
						var error = string.Format("The file format provided {0} is not supported. Please convert to .xlsx, .xls or .csv and try again.", extension);

						modelList.Add(new CreateAssistUserModel { RegistrationError = new List<ErrorTypes> { ErrorTypes.InvalidDocumentType }, ErrorText = new List<string> { error } });
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.InvalidDocumentType), error);
						Logger.Info(request.LogData(), error);
						response.ValidationErrors = true;
						response.ModelList = modelList;
						return response;
				}
			}

			var document = new DocumentUtility();
			var exportedData = document.ExportToDataTable(documentFileType, request.File);

			#endregion

			#region Populate CreateAssistUserModel with the data in the spreadsheet

			var columnCount = exportedData.Columns.Count;

			foreach (DataRow row in exportedData.Rows)
			{
				if (exportedData.Rows.IndexOf(row) != 0)
				{
					var model = new CreateAssistUserModel();

					int colIndex;
					for (colIndex = 0; colIndex < columnCount; colIndex++)
					{
						var value = row[colIndex].ToString();
						if (value.IsNotNullOrEmpty())
							value = value.Trim();

						switch (colIndex)
						{
								#region Column order definition

								// 1.  Email Address
							// 2.  First Name
							// 3.  Last Name
							// 4.  Title
							// 5.  Phone Number
							// 6.  Middle Initial
							// 7.  Job title
							// 8.  External Id

								#endregion

							case 0:
								var emailRegEx = new Regex(AppSettings.EmailAddressRegExPattern);

								if (value.IsNotNullOrEmpty() && !emailRegEx.IsMatch(value))
								{
									if (model.RegistrationError.IsNull())
										model.RegistrationError = new List<ErrorTypes> { ErrorTypes.InvalidEmail };
									else
										model.RegistrationError.Add(ErrorTypes.InvalidEmail);

									response.ValidationErrors = true;
									response.SetFailure(ErrorTypes.InvalidEmail);
									Logger.Info(request.LogData(), "Invalid email");
								}

								model.UserEmailAddress = value;
								break;
							case 1:
								if (model.UserPerson.IsNull()) model.UserPerson = new PersonDto();
								model.UserPerson.FirstName = value;
								break;
							case 2:
								if (model.UserPerson.IsNull()) model.UserPerson = new PersonDto();
								model.UserPerson.LastName = value;
								break;
							case 3:
								if (value.IsNotNullOrEmpty())
								{
									var titleId = GetLookup(LookupTypes.Titles).Where(x => x.Key.Contains(value))
										.Select(x => x.Id)
										.FirstOrDefault();

									if (titleId.Equals(0))
									{
										if (model.RegistrationError.IsNull())
											model.RegistrationError = new List<ErrorTypes> { ErrorTypes.InvalidTitle };
										else
											model.RegistrationError.Add(ErrorTypes.InvalidTitle);

										response.ValidationErrors = true;
										response.SetFailure(ErrorTypes.InvalidTitle);
										Logger.Info(request.LogData(), "Invalid title");
									}
									else
									{
										if (model.UserPerson.IsNull()) model.UserPerson = new PersonDto();
										model.UserPerson.TitleId = titleId;
									}
								}

								break;
							case 4:
								value = new string(value.Where(char.IsDigit).ToArray());

								long test;
								var isNumber = long.TryParse(value, out test);

								if (!isNumber || value.Length != 10)
								{
									if (model.RegistrationError.IsNull())
										model.RegistrationError = new List<ErrorTypes> { ErrorTypes.InvalidPhoneNumber };
									else
										model.RegistrationError.Add(ErrorTypes.InvalidPhoneNumber);

									response.ValidationErrors = true;
									response.SetFailure(ErrorTypes.InvalidPhoneNumber);
									Logger.Info(request.LogData(), "Invalid phone number");
								}

								model.UserPhone = value;
								break;
							case 5:
								if (model.UserPerson.IsNull()) model.UserPerson = new PersonDto();
								model.UserPerson.MiddleInitial = value;
								break;
							case 6:
								if (model.UserPerson.IsNull()) model.UserPerson = new PersonDto();
								model.UserPerson.JobTitle = value;
								break;
							case 7:
								if (model.UserPerson.IsNull()) model.UserPerson = new PersonDto();
								model.AccountExternalId = string.IsNullOrWhiteSpace(value) ? null : value;
								break;
						}
					}

					if (model.UserEmailAddress.IsNullOrEmpty() || model.UserPerson.FirstName.IsNullOrEmpty() || model.UserPerson.LastName.IsNullOrEmpty() || (model.UserPerson.TitleId.Equals(0) && (model.RegistrationError.IsNull() || !model.RegistrationError.Contains(ErrorTypes.InvalidTitle))) || model.UserPhone.IsNullOrEmpty())
					{
						if (model.RegistrationError.IsNull())
							model.RegistrationError = new List<ErrorTypes> { ErrorTypes.RequiredDataNotFound };
						else
							model.RegistrationError.Add(ErrorTypes.RequiredDataNotFound);

						if (model.ErrorText.IsNull()) model.ErrorText = new List<string>();
						var error = string.Format("One or more mandatory fields are not populated on row {0} in the uploaded file. Please correct and try again", exportedData.Rows.IndexOf(row) + 1);
						response.ValidationErrors = true;
						response.SetFailure(ErrorTypes.RequiredDataNotFound);
						Logger.Info(request.LogData(), error);
					}

					modelList.Add(model);
				}
			}

			#endregion

			response.ModelList = modelList;

			return response;
		}

		/// <summary>
		/// Imports the job seeker and resumes from an external source
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public ImportJobSeekerAndResumesResponse ImportJobSeekerAndResumes(ImportJobSeekerAndResumesRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new ImportJobSeekerAndResumesResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var integrationRequest = new GetJobSeekerRequest { JobSeeker = new JobSeekerModel { ExternalId = request.JobSeekerExternalId }, ForImport = true, RequestId = request.RequestId };
					var integrationResponse = Repositories.Integration.GetJobSeeker(integrationRequest);

					if (integrationResponse.Outcome == IntegrationOutcome.Failure)
						throw new Exception("Unable to import job seeker", integrationResponse.Exception);
					if (integrationResponse.Outcome == IntegrationOutcome.NotImplemented)
						return response;

					var user = Repositories.Core.Users.FirstOrDefault(x => x.ExternalId == integrationResponse.JobSeeker.ExternalId);
					Person person;

					if (user.IsNull())
					{
						// Create basic user record
						UserTypes userType;
						switch (AppSettings.CareerExplorerModule)
						{
							case FocusModules.CareerExplorer:
								userType = UserTypes.Career | UserTypes.Explorer;
								break;
							case FocusModules.Career:
								userType = UserTypes.Career;
								break;
							case FocusModules.Explorer:
								userType = UserTypes.Explorer;
								break;
							default:
								throw new Exception("Unable to determine Career/Explorer module when importing job seekers");
						}

						var password = new Password(AppSettings, String.Format("Pasty99{0}", request.JobSeekerExternalId), false);
						user = new User
						{
							ExternalId = integrationResponse.JobSeeker.ExternalId,
							PasswordHash = password.Hash,
							PasswordSalt = password.Salt,
							UserType = userType,
							UserName = ((AppSettings.SamlEnabled || AppSettings.SamlEnabledForCareer || AppSettings.OAuthEnabled) ? DateTime.Now.Ticks.ToString() : integrationResponse.JobSeeker.ExternalId) + integrationResponse.JobSeeker.EmailAddress, // Append some value to make username invalid for SSO
							Enabled = true,
							IsMigrated = true
						};

						person = new Person
						{
							TitleId = 0,
							FirstName = integrationResponse.JobSeeker.FirstName,
							MiddleInitial = integrationResponse.JobSeeker.MiddleInitial,
							LastName = integrationResponse.JobSeeker.LastName,
							EmailAddress = integrationResponse.JobSeeker.EmailAddress,
							User = user
						};

						var careerUserRole = Repositories.Core.Roles.SingleOrDefault(x => x.Key == Constants.RoleKeys.CareerUser);
						var explorerUserRole = Repositories.Core.Roles.SingleOrDefault(x => x.Key == Constants.RoleKeys.ExplorerUser);

						if ((userType & UserTypes.Career) == UserTypes.Career)
							user.Roles.Add(careerUserRole);

						if ((userType & UserTypes.Explorer) == UserTypes.Explorer)
							user.Roles.Add(explorerUserRole);



						Repositories.Core.Add(person);
						Repositories.Core.SaveChanges();
					}
					else
					{
						person = user.Person;
					}

					response.FocusId = person.Id;

					// No issues currently for this person?
					if (!Repositories.Core.Issues.Any(issue => issue.PersonId == person.Id))
					{
						// Add a default issues record
						var issues = new Issues { PersonId = person.Id };
						Repositories.Core.Add(issues);
						Repositories.Core.SaveChanges();
					}

					// Now update the existing data. Some of this will be repeasted from above but as we have mandatory fields that we need to prime it can't be helped.
					person.TitleId = 0;
					person.FirstName = integrationResponse.JobSeeker.FirstName;
					person.MiddleInitial = integrationResponse.JobSeeker.MiddleInitial;
					person.LastName = integrationResponse.JobSeeker.LastName;
					person.EmailAddress = integrationResponse.JobSeeker.EmailAddress;
					person.DateOfBirth = integrationResponse.JobSeeker.DateOfBirth;
					person.IsUiClaimant = integrationResponse.JobSeeker.UiClaimant;
					if (integrationResponse.JobSeeker.Resumes.IsNotNullOrEmpty())
						person.IsVeteran = integrationResponse.JobSeeker.Resumes.Any(x =>(bool) x.Resume.ResumeContent.Profile.Veteran.IsVeteran);
					person.EnrollmentStatus = integrationResponse.JobSeeker.EnrollmentStatus;

					var addresses = person.PersonAddresses;
					PersonAddress address = null;
					if (addresses.IsNotNullOrEmpty())
						address = addresses.FirstOrDefault(x => x.IsPrimary);
					if (address.IsNull())
					{
						address = new PersonAddress { IsPrimary = true };
						addresses.Add(address);
					}

					address.Line1 = integrationResponse.JobSeeker.AddressLine1;
					address.Line2 = integrationResponse.JobSeeker.AddressLine2;
					address.TownCity = integrationResponse.JobSeeker.AddressTownCity;
					address.PostcodeZip = integrationResponse.JobSeeker.AddressPostcodeZip;
					address.CountyId = integrationResponse.JobSeeker.AddressCountyId;
					address.StateId = integrationResponse.JobSeeker.AddressStateId.GetValueOrDefault();
					address.CountryId = integrationResponse.JobSeeker.AddressCountryId.GetValueOrDefault();

					var offices = Repositories.Core.Offices.Where(x => x.AssignedPostcodeZip.Contains(MiscUtils.SafeSubstring(integrationResponse.JobSeeker.AddressPostcodeZip, 0, 5))).Select(x => x.Id).ToList();
					if (offices.IsNullOrEmpty())
					{
						var defaultOffice = Helpers.Office.GetDefaultOfficeId(OfficeDefaultType.JobSeeker);
						if (defaultOffice.HasValue)
							offices = new List<long>
							{
								defaultOffice.Value
							};
					}

					if (offices.IsNotNullOrEmpty())
					{
						var employerService = new EmployerService(RuntimeContext);
						var personOfficeMappers = offices.Select(office => new PersonOfficeMapperDto
						{
							PersonId = person.Id,
							OfficeId = office
						}).ToList();

						var personOfficeMapperRequest = new PersonOfficeMapperRequest
						{
							ClientTag = request.ClientTag,
							RequestId = request.RequestId,
							SessionId = request.SessionId,
							UserContext = request.UserContext,
							VersionNumber = request.VersionNumber,
							PersonId = person.Id,
							PersonOffices = personOfficeMappers
						};

						var personOfficeMapperResponse = employerService.SavePersonOffices(personOfficeMapperRequest);

						if (personOfficeMapperResponse.Acknowledgement == AcknowledgementType.Failure)
							throw personOfficeMapperResponse.Exception;
					}

					if (integrationResponse.JobSeeker.Resumes.IsNotNullOrEmpty())
					{
						var resumeService = new ResumeService(RuntimeContext);
						var resumes = person.Resumes.Where(x => x.StatusId != ResumeStatuses.Archived && x.StatusId != ResumeStatuses.Deleted).Select(x => x.AsDto()).ToList();
						// Now resumes
						foreach (var importedResume in integrationResponse.JobSeeker.Resumes)
						{

							var resume = resumes.FirstOrDefault(x => x.ExternalId == importedResume.ExternalId);
							if (resume.IsNotNull())
							{
								importedResume.Resume.ResumeMetaInfo.ResumeId = resume.Id;
							}

							var saveResumeRequest = new SaveResumeRequest
							{
								ClientTag = request.ClientTag,
								RequestId = request.RequestId,
								SessionId = request.SessionId,
								UserContext = request.UserContext,
								VersionNumber = request.VersionNumber,
								SeekerResume = importedResume.Resume,
								PersonId = person.Id,
								ExternalResumeId = importedResume.ExternalId,
								IgnoreClient = true
							};

							var saveResumeResponse = resumeService.SaveResume(saveResumeRequest);
							if (saveResumeResponse.Acknowledgement == AcknowledgementType.Failure)
								throw saveResumeResponse.Exception;


							var importedResumeAvailable = false;
							var resumeDocument = Repositories.Core.ResumeDocuments.FirstOrDefault(x => x.ResumeId == saveResumeResponse.SeekerResume.ResumeMetaInfo.ResumeId.GetValueOrDefault());
							if (importedResume.DocumentBytes.IsNotNullOrEmpty())
							{
								var binaryDocumentRequest = new BinaryDocumentRequest
								{

									ClientTag = request.ClientTag,
									RequestId = request.RequestId,
									SessionId = request.SessionId,
									UserContext = request.UserContext,
									VersionNumber = request.VersionNumber,
									DocumentType = DocumentType.Resume,
									FileExtension = Path.GetExtension(importedResume.FileName),
									BinaryContent = importedResume.DocumentBytes
								};
								var binaryDocumentResponse = new UtilityService(RuntimeContext).TagBinaryDocument(binaryDocumentRequest);

								if (resumeDocument.IsNull())
								{
									resumeDocument = new Data.Core.Entities.ResumeDocument
									{
										ResumeId = saveResumeResponse.SeekerResume.ResumeMetaInfo.ResumeId.Value
									};
									Repositories.Core.Add(resumeDocument);
								}
								resumeDocument.FileName = importedResume.FileName;
								resumeDocument.Html = binaryDocumentResponse.Html;
								resumeDocument.ContentType = importedResume.ContentType;
								resumeDocument.DocumentBytes = importedResume.DocumentBytes;
								Repositories.Core.SaveChanges();
								importedResumeAvailable = true;
							}
							// If we are importing a resume without an associated document the delete any uploaded resume we already store
							if (!importedResumeAvailable && resumeDocument.IsNotNull())
							{
								Repositories.Core.Remove(resumeDocument);
								Repositories.Core.SaveChanges();
							}
						}
					}
					user.IsMigrated = true;
					Repositories.Core.SaveChanges();

				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);

					response.Exceptions = SetImportResponseExceptions(response);

					return response;
				}

				return response;
			}
		}

		public ImportEmployerAndArtifactsResponse ImportEmployerAndArtifacts(ImportEmployerAndArtifactsRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new ImportEmployerAndArtifactsResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var integrationRequest = new GetEmployerRequest
					{
						EmployerId = request.EmployerExternalId,
						ForImport = true,
						RequestId = request.RequestId
					};


					if (request.EmployerExternalId.IsNull()) request.EmployerExternalId = Guid.NewGuid().ToString();

					var integrationResponse = Repositories.Integration.GetEmployer(integrationRequest);

					if (integrationResponse.Outcome == IntegrationOutcome.Failure)
						throw new Exception("Unable to import employer", integrationResponse.Exception);
					if (integrationResponse.Outcome == IntegrationOutcome.NotImplemented)
						return response;

					// Add/Update employer
					var employer = Repositories.Core.Employers.FirstOrDefault(x => x.ExternalId == request.EmployerExternalId);

					if (employer.IsNull())
					{
						employer = new Employer
						{
							ExternalId = request.EmployerExternalId
						};
					}

					employer.Name = integrationResponse.Employer.Name;
					employer.FederalEmployerIdentificationNumber = integrationResponse.Employer.FederalEmployerIdentificationNumber;
					employer.IsValidFederalEmployerIdentificationNumber = true; // Validation should have been carried out by importer
					employer.ApprovalStatus = ApprovalStatuses.Approved;
					employer.Url = integrationResponse.Employer.Url.IsNullOrEmpty() ? null : integrationResponse.Employer.Url;
					employer.OwnershipTypeId = integrationResponse.Employer.Owner;
					var naics = Repositories.Library.NAICS.FirstOrDefault(x => x.Code == integrationResponse.Employer.Naics);
					if (naics.IsNotNull())
						employer.IndustrialClassification = naics.Code + " - " + naics.Name;
					employer.TermsAccepted = true;
					employer.PrimaryPhone = integrationResponse.Employer.Phone;
					employer.PrimaryPhoneExtension = integrationResponse.Employer.PhoneExt ?? string.Empty;
					employer.PrimaryPhoneType = integrationResponse.Employer.PhoneType.ToString();
					employer.AlternatePhone1 = integrationResponse.Employer.AltPhone;
					employer.AlternatePhone1Type = integrationResponse.Employer.AltPhoneType.ToString();
					employer.AlternatePhone2 = string.Empty;
					employer.AlternatePhone1Type = PhoneTypes.Phone.ToString();
					employer.IsRegistrationComplete = true; // Assume true if imported
					employer.AccountTypeId = integrationResponse.Employer.AccountType;
					employer.StateEmployerIdentificationNumber = integrationResponse.Employer.StateEmployerIdentificationNumber ?? string.Empty;

					if (employer.Id == 0)
						Repositories.Core.Add(employer);

					Repositories.Core.SaveChanges();

					response.FocusId = employer.Id;

					// Add/Update employer address
					var employerAddresses = employer.EmployerAddresses;
					EmployerAddress employerAddress;
					if (employerAddresses.IsNullOrEmpty())
					{
						employer.EmployerAddresses.Add(new EmployerAddress());
						employerAddress = employer.EmployerAddresses[0];
					}
					else
					{
						employerAddress = employerAddresses.FirstOrDefault(x => x.PostcodeZip == integrationResponse.Employer.Address.PostCode);
						if (employerAddress.IsNull())
						{
							employerAddress = new EmployerAddress();
							employer.EmployerAddresses.Add(employerAddress);
							foreach (var address in employerAddresses)
							{
								address.IsPrimary = false;
							}
						}
					}
					employerAddress.IsPrimary = true;
					employerAddress.Line1 = integrationResponse.Employer.Address.AddressLine1;
					employerAddress.Line2 = integrationResponse.Employer.Address.AddressLine2;
					employerAddress.Line3 = integrationResponse.Employer.Address.AddressLine3;
					employerAddress.TownCity = integrationResponse.Employer.Address.City;
					employerAddress.PostcodeZip = integrationResponse.Employer.Address.PostCode;
					employerAddress.CountyId = integrationResponse.Employer.Address.CountyId;
					employerAddress.StateId = integrationResponse.Employer.Address.StateId;
					employerAddress.CountryId = integrationResponse.Employer.Address.CountryId;
					employerAddress.EmployerId = employer.Id;

					Repositories.Core.SaveChanges();

					var businessUnits = new List<long>();
					long defaultBu = 0;
					// Add/update business units
					foreach (var importBu in integrationResponse.Employer.BusinessUnits)
					{
						BusinessUnit bu;
						if (employer.BusinessUnits.IsNullOrEmpty())
						{
							bu = new BusinessUnit();
							employer.BusinessUnits.Add(bu);
						}
						else
						{
							bu = employer.BusinessUnits.FirstOrDefault(x => x.ExternalId == importBu.BusinessUnit.ExternalId);
							if (bu.IsNull())
							{
								bu = new BusinessUnit();
								employer.BusinessUnits.Add(bu);
							}
						}

						bu.Name = importBu.BusinessUnit.Name;
						bu.IsPrimary = importBu.IsDefault;
						bu.OwnershipTypeId = importBu.BusinessUnit.Owner;

						naics = Repositories.Library.NAICS.FirstOrDefault(x => x.Code == integrationResponse.Employer.Naics);
						if (naics.IsNotNull())
							bu.IndustrialClassification = naics.Code + " - " + naics.Name;

						bu.PrimaryPhone = importBu.BusinessUnit.Phone;
						bu.PrimaryPhoneExtension = importBu.BusinessUnit.PhoneExt;
						bu.PrimaryPhoneType = importBu.BusinessUnit.PhoneType.ToString();
						bu.AlternatePhone1 = importBu.BusinessUnit.AltPhone;
						bu.AlternatePhone1Type = importBu.BusinessUnit.AltPhoneType.ToString();
						bu.AlternatePhone2 = string.Empty;
						bu.AlternatePhone2Type = PhoneTypes.Phone.ToString();
						bu.EmployerId = employer.Id;
						bu.IsPreferred = true;
						bu.AccountTypeId = importBu.BusinessUnit.AccountType;
						bu.ExternalId = importBu.BusinessUnit.ExternalId;
						bu.Url = importBu.BusinessUnit.Url.IsNullOrEmpty() ? null : importBu.BusinessUnit.Url;
						bu.ApprovalStatus = ApprovalStatuses.Approved;

						BusinessUnitAddress bua;
						if (bu.BusinessUnitAddresses.IsNullOrEmpty() || bu.BusinessUnitAddresses.All(x => x.PostcodeZip != importBu.BusinessUnit.Address.PostCode))
						{
							bua = new BusinessUnitAddress { IsPrimary = true };
							bu.BusinessUnitAddresses.Add(bua);
						}
						else
							bua = bu.BusinessUnitAddresses.First(x => x.PostcodeZip == importBu.BusinessUnit.Address.PostCode);

						bua.Line1 = importBu.BusinessUnit.Address.AddressLine1;
						bua.Line2 = importBu.BusinessUnit.Address.AddressLine2;
						bua.Line3 = importBu.BusinessUnit.Address.AddressLine3;
						bua.TownCity = importBu.BusinessUnit.Address.City;
						bua.CountyId = importBu.BusinessUnit.Address.CountyId;
						bua.StateId = importBu.BusinessUnit.Address.StateId;
						bua.CountryId = importBu.BusinessUnit.Address.CountryId;
						bua.PostcodeZip = importBu.BusinessUnit.Address.PostCode;

						if (bu.Id == 0)
							Repositories.Core.Add(bu);
						Repositories.Core.SaveChanges();

						businessUnits.Add(bu.Id);
						if (bu.IsPrimary) defaultBu = bu.Id;
					}

					var employees = new List<long>();
					// And finally the users
					foreach (var employee in integrationResponse.Employer.Employees)
					{
						var user = Repositories.Core.Users.FirstOrDefault(x => x.MigrationId == employee.MigrationId && x.UserType == UserTypes.Talent);
						if (user.IsNull())
						{
							user = new User();
							var password = new Password(AppSettings, Guid.NewGuid().ToString(), false);
							user.PasswordHash = password.Hash;
							user.PasswordSalt = password.Salt;
							user.UserName = ((AppSettings.SamlEnabled || AppSettings.SamlEnabledForTalent || AppSettings.OAuthEnabled) ? DateTime.Now.Ticks.ToString() : employee.ExternalId) + employee.Email; // Append some value to make username invalid for SSO
							var talentUserRole = Repositories.Core.Roles.Single(x => x.Key == Constants.RoleKeys.TalentUser);
							user.Roles.Add(talentUserRole);
						}

						user.Enabled = true;
						user.UserType = UserTypes.Talent;
						user.ExternalId = employee.ExternalId;
						user.MigrationId = employee.MigrationId;
						user.ScreenName = employee.FirstName + " " + employee.LastName;
						user.IsMigrated = true;

						var person = user.Person;
						if (person.IsNull())
							person = user.Person = new Person();

						person.TitleId = employee.SalutationId;
						person.FirstName = employee.FirstName;
						person.LastName = employee.LastName;
						person.EmailAddress = employee.Email;
						var phone = person.PhoneNumbers;
						PhoneNumber phoneNumber = null;
						if (phone.IsNotNullOrEmpty())
						{
							phoneNumber = person.PhoneNumbers.FirstOrDefault(x => x.IsPrimary);
						}

						if (phoneNumber.IsNull())
						{
							phoneNumber = new PhoneNumber { IsPrimary = true };
							person.PhoneNumbers.Add(phoneNumber);
						}
						phoneNumber.Number = employee.Phone;
						phoneNumber.Extension = employee.PhoneExt;
						phoneNumber.PhoneType = PhoneTypes.Phone;

						// Employee Address
						var employeeAddress = person.PersonAddresses.FirstOrDefault();
						if (employeeAddress.IsNull())
						{
							employeeAddress = new PersonAddress();
							person.PersonAddresses.Add(employeeAddress);
						}
						employeeAddress.Line1 = employee.Address.AddressLine1;
						employeeAddress.Line2 = employee.Address.AddressLine2;
						employeeAddress.Line3 = employee.Address.AddressLine3;
						employeeAddress.TownCity = employee.Address.City;
						employeeAddress.CountyId = employee.Address.CountyId;
						employeeAddress.PostcodeZip = employee.Address.PostCode;
						employeeAddress.StateId = employee.Address.StateId;
						employeeAddress.CountryId = employee.Address.CountryId;
						employeeAddress.IsPrimary = true;

						var employeeRecord = person.Employee;
						if (employeeRecord.IsNull())
							person.Employee = new Employee
							{
								ApprovalStatus = ApprovalStatuses.Approved,
								PersonId = person.Id,
								EmployerId = employer.Id
							};

						if (user.Id == 0)
							Repositories.Core.Add(user);

						Repositories.Core.SaveChanges();
						employees.Add(person.Employee.Id);
					}

					var employeeBUs = (from e in Repositories.Core.Query<Employer>() join bu in Repositories.Core.Query<BusinessUnit>() on e.Id equals bu.EmployerId join ebu in Repositories.Core.EmployeeBusinessUnits on bu.Id equals ebu.BusinessUnitId where e.Id == employer.Id select ebu.AsDto()).ToList();
					foreach (var businessUnit in businessUnits)
					{
						foreach (var employee in employees.Where(employee => !employeeBUs.Any(x => x.BusinessUnitId == businessUnit && x.EmployeeId == employee)))
						{
							Repositories.Core.Add(new EmployeeBusinessUnit { BusinessUnitId = businessUnit, EmployeeId = employee, Default = (businessUnit == defaultBu) });
						}
					}
					Repositories.Core.SaveChanges();

					// Employer offices (WI employers get assigned to the default regardless of zip)
					var offices = new List<long>();
					var defaultOffice = Helpers.Office.GetDefaultOfficeId(OfficeDefaultType.Employer);
					if (defaultOffice.HasValue)
						offices = new List<long>
						{
							defaultOffice.Value
						};

					if (offices.IsNotNullOrEmpty())
					{
						var updateEmployerAssignedOfficeRequest = new UpdateEmployerAssignedOfficeRequest
						{
							ClientTag = request.ClientTag,
							RequestId = request.RequestId,
							SessionId = request.SessionId,
							UserContext = request.UserContext,
							VersionNumber = request.VersionNumber,
							EmployerId = employer.Id,
							EmployerOffices = offices.Select(office => new EmployerOfficeMapperDto
							{
								EmployerId = employer.Id,
								OfficeId = office
							}).ToList()
						};
						var updateEmployerAssignedOfficeResponse = new EmployerService(RuntimeContext).UpdateEmployerAssignedOffice(updateEmployerAssignedOfficeRequest);
						if (updateEmployerAssignedOfficeResponse.Acknowledgement == AcknowledgementType.Failure)
							throw updateEmployerAssignedOfficeResponse.Exception;
					}


					if (integrationResponse.JobsToImport.IsNotNullOrEmpty())
					{
						var jobService = new JobService(RuntimeContext);

						var importJobRequest = new ImportJobRequest
						{
							ClientTag = request.ClientTag,
							RequestId = request.RequestId,
							SessionId = request.SessionId,
							UserContext = request.UserContext,
							VersionNumber = request.VersionNumber,
							ExternalJobIds = integrationResponse.JobsToImport
						};
						var saveJobResponse = jobService.ImportJob(importJobRequest);
						if (saveJobResponse.Acknowledgement == AcknowledgementType.Failure)
							throw saveJobResponse.Exception;
					}
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);

					response.Exceptions = SetImportResponseExceptions(response);

					return response;
				}

				return response;
			}
		}

		/// <summary>
		/// Unsubscribes the user from emails.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public UnsubscribeUserFromEmailsResponse UnsubscribeUserFromEmails(UnsubscribeUserFromEmailsRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new UnsubscribeUserFromEmailsResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.ClientTag | Validate.License | Validate.SessionId | Validate.AccessToken))
					return response;

				try
				{
					if (request.EncryptedPersonId.IsNullOrEmpty())
					{
						response.SetFailure(ErrorTypes.EncryptedValueNotSupplied, FormatErrorMessage(request, ErrorTypes.EncryptedValueNotSupplied));
						Logger.Error(request.LogData(), response.Message, response.Exception);

						return response;
					}

					// Need to get the vector by encryption ID because the entity ID is what is encrypted
					var encrpytionModel = Helpers.Encryption.GetVector(request.EncryptionId);
					var decryptedPersonId = Helpers.Encryption.Decrypt(request.EncryptedPersonId, encrpytionModel.Vector, request.UrlEncoded);

					if (decryptedPersonId.IsNullOrEmpty())
					{
						response.SetFailure(ErrorTypes.DecryptedValueNotFound, FormatErrorMessage(request, ErrorTypes.DecryptedValueNotFound));
						Logger.Error(request.LogData(), response.Message, response.Exception);

						return response;
					}

					var personId = Convert.ToInt64(decryptedPersonId);

					var person = Repositories.Core.FindById<Person>(personId);

					if (person.IsNull())
					{
						response.SetFailure(ErrorTypes.PersonNotFound, FormatErrorMessage(request, ErrorTypes.PersonNotFound));
						Logger.Error(request.LogData(), response.Message, response.Exception);

						return response;
					}

					person.Unsubscribed = true;

					Repositories.Core.SaveChanges(true);

					LogAction(request, ActionTypes.UnsubscribeEmail, typeof(Person).Name, person.Id, person.User.Id, (long)UnsubscribeTypes.BlastEmails, overrideUserId: person.User.Id);
				}
				catch (Exception ex)
				{
					response.SetFailure(ErrorTypes.Unknown, FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);

					return response;
				}

				return response;
			}
		}

		/// <summary>
		/// Inactivates the job seeker.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public InactivateJobSeekerResponse InactivateJobSeeker(InactivateJobSeekerRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new InactivateJobSeekerResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					#region Request validation

					if (request.PersonId.IsNull() || request.PersonId <= 0)
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.PersonIdNotSupplied));
						Logger.Info(request.LogData(), string.Format("Person ID not specified {0}", request.PersonId));
						return response;
					}

					if (request.AccountDisabledReason == AccountDisabledReason.Unknown)
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.AccountDisabledReasonNotSpecified));
						Logger.Info(request.LogData(), "Account disabled reason not specified");
						return response;
					}

					#endregion

					InactivateJobSeeker(request, request.PersonId, request.AccountDisabledReason, true);

					response.Success = true;
				}
				catch (Exception ex)
				{
					response.SetFailure(ErrorTypes.Unknown, FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);

					return response;
				}

				return response;
			}
		}

		/// <summary>
		/// Get a list of exceptions for the import request
		/// </summary>
		/// <param name="response">The import response</param>
		/// <returns>A list of exceptions</returns>
		private List<string> SetImportResponseExceptions(ServiceResponse response)
		{
			var ex = response.Exception;
			response.Exception = null;

			var exceptions = new List<string> { ex.Message };
			while (ex.InnerException.IsNotNull())
			{
				exceptions.Add(ex.InnerException.Message);
				ex = ex.InnerException;
			}

			return exceptions;
		}

		public CreatePersonMobileDeviceResponse CreatePersonMobileDevice(CreatePersonMobileDeviceRequest request)
		{
			var response = new CreatePersonMobileDeviceResponse(request);

			// Validate request
			if (
				!ValidateRequest(request, response, Validate.License | Validate.ClientTag | Validate.SessionId | Validate.AccessToken) ||
				string.IsNullOrEmpty(request.Token) || request.PersonId < 1)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown));
				return response;
			}
			try
			{
				// Check if already exists
				var query = Repositories.Core.PersonMobileDevices.Where(m => m.Token == request.Token);
				var device = query.FirstOrDefault();
				if (device != null)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown));
					return response;
				}

				// Create 
				device = new PersonMobileDevice
				{
					PersonId = request.PersonId,
					Token = request.Token,
					ApiKey = request.ApiKey,
					DeviceType = request.DeviceType
				};

				Repositories.Core.Add(device);
				Repositories.Core.SaveChanges(true);
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}

			//TODO: LogAction(request, ActionTypes.CreatePersonMobileDevice)

			return response;
		}


		public FindPersonMobileDeviceResponse FindPersonMobileDevice(FindPersonMobileDeviceRequest request)
		{
			var response = new FindPersonMobileDeviceResponse(request);

			// Validate request
			if (!ValidateRequest(request, response, Validate.License | Validate.ClientTag | Validate.SessionId | Validate.AccessToken))
				return response;

			try
			{
				if (!string.IsNullOrEmpty(request.Token))
				{
					var query = Repositories.Core.PersonMobileDevices.Where(m => m.Token == request.Token);
					var device = query.FirstOrDefault();

					if (device == null)
						return response;

					response.Token = device.Token;
					response.Id = device.Id;
					response.ApiKey = device.ApiKey;
					response.DeviceType = device.DeviceType;
					response.CreatedOn = device.CreatedOn;
					response.PersonId = device.PersonId;

					return response;
				}
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}
			//TODO: LogAction(request, ActionTypes.FindPersonMobileDeviceUser, device);

			return response;
		}


		public DeletePersonMobileDeviceResponse DeletePersonMobileDevice(DeletePersonMobileDeviceRequest request)
		{
			var response = new DeletePersonMobileDeviceResponse(request);

			// Validate request
			if (!ValidateRequest(request, response, Validate.License | Validate.ClientTag | Validate.SessionId | Validate.AccessToken) || string.IsNullOrEmpty(request.Token))
				return response;

			try
			{
				// Check if already exists
				var query = Repositories.Core.PersonMobileDevices.Where(m => m.Token == request.Token);
				var device = query.FirstOrDefault();
				if (device == null)
				{
					//TODO: message for already exists?
					// eg. Logger.Info(request.LogData(), string.Format("UserId '{0}' not found.", User.Id));

					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown));
					return response;
				}

				Repositories.Core.Remove(device);
				Repositories.Core.SaveChanges(true);
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}

			//TODO: LogAction(request, ActionTypes.CreatePersonMobileDevice)

			return response;
		}

		/// <summary>
		/// Updates the assist user.
		/// 
		/// At the moment this method is only updating 2 properties as that's all that I need for now, but this method can and should be extended as more properties need to be saved
		/// 
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public UpdateAssistUserResponse UpdateAssistUser(UpdateAssistUserRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new UpdateAssistUserResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					if (request.UserId.IsNull())
					{
						response.SetFailure(ErrorTypes.EncryptedValueNotSupplied, FormatErrorMessage(request, ErrorTypes.UserIdNotSupplied));
						Logger.Error(request.LogData(), response.Message, response.Exception);

						return response;
					}

					var user = Repositories.Core.FindById<User>(request.UserId);

					if (user.IsNull())
					{
						response.SetFailure(ErrorTypes.EncryptedValueNotSupplied, FormatErrorMessage(request, ErrorTypes.UserNotFound));
						Logger.Error(request.LogData(), response.Message, response.Exception);

						return response;
					}

					#region Person

					if (request.LocalVeteranEmploymentRepresentative.IsNotNull())
						user.Person.LocalVeteranEmploymentRepresentative = request.LocalVeteranEmploymentRepresentative;

					if (request.DisabledVeteransOutreachProgramSpecialist.IsNotNull())
						user.Person.DisabledVeteransOutreachProgramSpecialist = request.DisabledVeteransOutreachProgramSpecialist;

					#endregion

					Repositories.Core.SaveChanges(true);

					response.User = user.AsDto();
				}
				catch (Exception ex)
				{
					response.SetFailure(ErrorTypes.Unknown, FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);

					return response;
				}

				return response;
			}
		}

		public ChangeCareerAccountTypeResponse ChangeCareerAccountType(ChangeCareerAccountTypeRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new ChangeCareerAccountTypeResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					// person details
					var person = Repositories.Core.FindById<Person>(request.UserContext.PersonId);

					// person must exist
					if (person.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.PersonNotFound));
						Logger.Info(request.LogData(), string.Format("PersonId '{0}' not found.", request.UserContext.PersonId));
					}

					// Update person record
					person.AccountType = request.NewAccountType;

					Repositories.Core.SaveChanges();

					// Log the action
					LogAction(request, ActionTypes.ChangeAccountType, person);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		#region Helper Methods

		private PagedList<UserView> FindUsers(FindUserRequest request)
		{
			// This causes 2 queries per row found which is:
			//
			// 1 Count Query
			// 1 Main query
			// 2 * N email address / office queries
			//
			// With this in mind we need to change this so that it does 2 queries only
			var roleId = Repositories.Core.Roles.Where(r => r.Key == request.SearchRole).Select(r => r.Id).FirstOrDefault();

			var query = from p in Repositories.Core.Persons
									join u in Repositories.Core.Users on p.Id equals u.PersonId
									join ur in Repositories.Core.UserRoles on u.Id equals ur.UserId
									join r in Repositories.Core.Roles on ur.RoleId equals r.Id
									where r.Id == roleId
									select new UserView
									{
										Id = u.Id,
										ExternalId = u.ExternalId,
										EmailAddress = p.EmailAddress,
										FirstName = p.FirstName,
										LastName = p.LastName,
										ExternalOffice = p.ExternalOffice,
										UserName = u.UserName,
										Enabled = u.Enabled,
										PersonId = u.PersonId,
									};

			switch (request.FindBy)
			{
				case FindUserRequest.UserFindBy.Name:
					{
						if (request.SearchFirstName.IsNotNullOrEmpty())
							query = query.Where(x => x.FirstName == request.SearchFirstName);

						if (request.SearchLastName.IsNotNullOrEmpty())
							query = query.Where(x => x.LastName == request.SearchLastName);

						break;
					}

				case FindUserRequest.UserFindBy.Email:
					query = query.Where(x => x.EmailAddress == request.SearchEmailAddress);
					break;


				case FindUserRequest.UserFindBy.ExternalOffice:
					query = query.Where(x => x.ExternalOffice == request.SearchExternalOffice);
					break;

				case FindUserRequest.UserFindBy.ExternalId:
					query = query.Where(x => x.ExternalId == request.SearchExternalId);
					break;
			}

			return new PagedList<UserView>(query, request.PageIndex, request.PageSize);
		}

		/// <summary>
		/// Updates the phone number.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <param name="response">The response.</param>
		/// <param name="person">The person.</param>
		/// <param name="phoneNumber">The phone number.</param>
	  /// <param name="doPublish">Whether to publish a message to integration</param>
		/// <param name="savedPhoneNumber">The saved phone number.</param>
		/// <returns></returns>
		private ServiceResponse UpdatePhoneNumber(ServiceRequest request, ServiceResponse response, Person person, PhoneNumberDto phoneNumber, bool doPublish, out PhoneNumber savedPhoneNumber)
		{
			// Validate request
			if (!ValidateRequest(request, response, Validate.All))
			{
				savedPhoneNumber = new PhoneNumber();
				return response;
			}

			if (phoneNumber.IsNull())
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.PhoneNumberNotFound));
				Logger.Info(request.LogData(), string.Format("UserId '{0}' not found.", person.User.Id));
			}

			phoneNumber.Number = new string(phoneNumber.Number.Where(char.IsDigit).ToArray());

			// Do nothing
			if (phoneNumber.Number.IsNullOrEmpty() && phoneNumber.Id == 0)
			{
				savedPhoneNumber = new PhoneNumber();
				return response;
			}

			if (phoneNumber.PersonId.IsNull() || phoneNumber.PersonId == 0)
				phoneNumber.PersonId = person.Id;

			// If we have a primary for this contact type we need to reset it(them just incase)
			if (phoneNumber.IsPrimary)
			{
				var existingPrimaries = new List<PhoneNumber>();
				if (phoneNumber.Number.IsNotNullOrEmpty())
				{
					existingPrimaries.AddRange(Repositories.Core.PhoneNumbers.Where(x => x.Person.Id == phoneNumber.PersonId && x.IsPrimary).ToList());
				}

				foreach (var existingPrimary in existingPrimaries.Where(existingPrimary => existingPrimary.Id != phoneNumber.Id))
					existingPrimary.IsPrimary = false;
			}

			if (phoneNumber.Id == 0)
			{
				savedPhoneNumber = new PhoneNumber();
				savedPhoneNumber = phoneNumber.CopyTo(savedPhoneNumber);
				Repositories.Core.Add(savedPhoneNumber);
			}
			else
			{
				savedPhoneNumber = person.PhoneNumbers.FirstOrDefault(x => x.Id == phoneNumber.Id);
				if (savedPhoneNumber.IsNull())
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.PhoneNumberNotFound));
					Logger.Info(request.LogData(), string.Format("UserId '{0}' not found.", person.User.Id));
				}

				// if the value is empty we must delete it
				if (phoneNumber.Number.IsNullOrEmpty())
					Repositories.Core.Remove(savedPhoneNumber);

				savedPhoneNumber = phoneNumber.CopyTo(savedPhoneNumber);
			}

			if (doPublish)
				person.User.PublishUserUpdateIntegrationRequest(request.UserContext.UserId, RuntimeContext);

			return response;
		}


		/// <summary>
		/// Updates the resume.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <param name="resumeModel">The resume model.</param>
		/// <returns></returns>
		private ExplorerResumeModel UpdateResume(ServiceRequest request, ExplorerResumeModel resumeModel)
		{
			Resume resume;

			if (resumeModel.IsNull())
				throw new Exception("No resume model");

			if (resumeModel.Resume.IsNull())
				throw new Exception("No resume");

			if (resumeModel.ResumeDocument.IsNull())
				throw new Exception("No resume document");

			if (resumeModel.Resume.Id.IsNotNull())
			{
				resume = Repositories.Core.FindById<Resume>(resumeModel.Resume.Id);
				resume = resumeModel.Resume.CopyTo(resume);
				resume.ResumeDocument = resumeModel.ResumeDocument.CopyTo(resume.ResumeDocument);
			}
			else
			{
				resume = new Resume();
				resume = resumeModel.Resume.CopyTo(resume);
				resume.ResumeDocument = resumeModel.ResumeDocument.CopyTo(resume.ResumeDocument);
				Repositories.Core.Add(resume);
			}

			Repositories.Core.SaveChanges(true);

			LogAction(request, ActionTypes.ChangeResume, resume);

			return new ExplorerResumeModel
			{
				Resume = resume.AsDto(),
				ResumeDocument = resume.ResumeDocument.AsDto()
			};
		}


		/// <summary>
		/// Inactivates the job seeker.
		/// </summary>
		/// <param name="request">The current service request.</param>
		/// <param name="personId">The person id.</param>
		/// <param name="inactivationReason">The inactivation reason.</param>
		/// <param name="deactivateUser">disable user if deactivated</param>
		/// <param name="reset">if set to <c>true</c> [reset].</param>
		/// <exception cref="System.Exception">
		/// </exception>
		private void InactivateJobSeeker(ServiceRequest request, long personId, AccountDisabledReason inactivationReason,bool deactivateUser, bool reset = true)
		{
			var jobseeker = Repositories.Core.FindById<Person>(personId);

			if (jobseeker.IsNull())
				throw new Exception(string.Format("Could not find person details for person {0}.", personId));

			var user = jobseeker.User;

			if (user.UserType != UserTypes.Career && user.UserType != UserTypes.Explorer && user.UserType != (UserTypes.Career | UserTypes.Explorer))
				throw new Exception(string.Format("Person ID: {0} is not a job seeker", personId));

			// Set the enabled flag to false and set the disabled reason
			// we dont want to disable the user if blocked FVN-2523
			if (deactivateUser)
			{
				user.Enabled = false;
				user.AccountDisabledReason = inactivationReason;

				Repositories.Core.SaveChanges(reset);

				LogAction(request, ActionTypes.InactivateAccount, typeof(Person).Name, jobseeker.Id, user.Id);
			}
			
			// Unregister the job seeker's resume and make it non-searchable
			Helpers.Resume.FullUnregisterResume(jobseeker.User.Id);
		}

		#endregion
	}
}
