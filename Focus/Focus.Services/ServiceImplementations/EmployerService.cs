﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Focus.Core;
using Focus.Core.Criteria;
using Focus.Core.Criteria.Employer;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Messages;
using Focus.Core.Messages.EmployerService;
using Focus.Core.Models;
using Focus.Core.Models.Assist;
using Focus.Core.Views;
using Focus.Data.Core.Entities;
using Focus.Data.Migration.Entities;
using Focus.Services.Core;
using Focus.Services.DtoMappers;
using Focus.Services.Mappers;
using Focus.Services.Messages;
using Focus.Services.Queries;
using Focus.Services.ServiceContracts;
using Employer = Focus.Data.Core.Entities.Employer;
using EmployerJobRequest = Focus.Core.Messages.EmployerService.EmployerJobRequest;
using EmployerJobResponse = Focus.Core.Messages.EmployerService.EmployerJobResponse;

using Framework.Core;
using Framework.Logging;

using Mindscape.LightSpeed;
using Mindscape.LightSpeed.Querying;

#endregion

namespace Focus.Services.ServiceImplementations
{
	public class EmployerService : ServiceBase, IEmployerService
	{
 		/// <summary>
		/// Initializes a new instance of the <see cref="EmployerService" /> class.
		/// </summary>
		public EmployerService() : this(null)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="EmployerService" /> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		public EmployerService(IRuntimeContext runtimeContext) : base(runtimeContext)
		{ }

		/// <summary>
		/// Gets the business units.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public BusinessUnitResponse GetBusinessUnits(BusinessUnitRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new BusinessUnitResponse(request);

				// Validate request (List requireed during registration so validate client and session but not user)
				if (!ValidateRequest(request, response, Validate.ClientTag | Validate.SessionId | Validate.AccessToken))
					return response;

				try
				{
					var query = new BusinessUnitQuery(Repositories.Core, request.Criteria);
				  List<long> businessUnitIds = null;

					switch (request.Criteria.FetchOption)
					{
						case CriteriaBase.FetchOptions.PagedList:
							response.BusinessUnitsPaged = query.Query().GetPagedList(x => x.AsDto(), request.Criteria.PageIndex, request.Criteria.PageSize);

              if (request.IncludeEmployeeIds)
                businessUnitIds = response.BusinessUnitsPaged.Select(bu => bu.Id.GetValueOrDefault()).ToList();

							break;
						case CriteriaBase.FetchOptions.List:
							response.BusinessUnits = query.Query().Select(x => x.AsDto()).ToList();
					    if (request.IncludeEmployeeIds)
					      businessUnitIds = response.BusinessUnits.Select(bu => bu.Id.GetValueOrDefault()).ToList();

							break;

						default:
							var businessUnit = request.Criteria.BusinessUnitId.IsNotNull()
																				? query.QueryEntityId()
																				: query.Query().SingleOrDefault();
							response.BusinessUnit = businessUnit.IsNotNull() ? businessUnit.AsDto() : null;

							if (request.IncludeEmployeeIds && response.BusinessUnit.IsNotNull())
                businessUnitIds = new List<long> { response.BusinessUnit.Id.GetValueOrDefault() };

							break;
					}

          if (businessUnitIds.IsNotNull())
          {
            response.EmployeeIds = Repositories.Core.EmployeeBusinessUnits
                                               .Where(ebu => businessUnitIds.Contains(ebu.BusinessUnitId))
                                               .ToLookup(ebu => ebu.BusinessUnitId, ebu => ebu.EmployeeId);
          }
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the employer profile.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public BusinessUnitProfileResponse GetBusinessUnitProfile(BusinessUnitProfileRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new BusinessUnitProfileResponse(request);

				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
          var date7DaysAgo = DateTime.Now.AddDays(-7);
          var date30DaysAgo = DateTime.Now.AddDays(-30);

					var businessUnit = Repositories.Core.FindById<BusinessUnit>(request.BusinessUnitId);
				  response.BusinessUnit = businessUnit.AsDto();
				  response.BusinessUnitAddresses = businessUnit.BusinessUnitAddresses.Select(bua => bua.AsDto()).ToList();
					response.EmployerLockVersion = Repositories.Core.Employers.Where(e => e.Id == businessUnit.EmployerId).Select(e => e.LockVersion).FirstOrDefault();
					var employeeBusinessUnits = businessUnit.EmployeeBusinessUnits;

					// set up counter to be incremented when iterating through hiring managers

					// get the action ids needed to calculate activity counts
          var actionTypeNames = new List<string>
				  {
            ActionTypes.LogIn.ToString(),
            ActionTypes.PostJob.ToString(),
				    ActionTypes.SaveJob.ToString(),
				    ActionTypes.SelfReferral.ToString(),
            ActionTypes.ApproveCandidateReferral.ToString(),
				    ActionTypes.ReferralRequest.ToString(),
						ActionTypes.CreateCandidateApplication.ToString()
				  };

          var actionTypes = Repositories.Core.ActionTypes.Where(t => actionTypeNames.Contains(t.Name)).ToList();

          var logInActionEvent = actionTypes.FirstOrDefault(x => x.Name == ActionTypes.LogIn.ToString());
					var logInActionEventId = logInActionEvent == null ? 0 : logInActionEvent.Id;

          var jobOrderOpenActionEvent = actionTypes.FirstOrDefault(x => x.Name == ActionTypes.PostJob.ToString());
					var jobOrderOpenActionEventId = jobOrderOpenActionEvent == null ? 0 : jobOrderOpenActionEvent.Id;

          var jobOrderEditedActionEvent = actionTypes.FirstOrDefault(x => x.Name == ActionTypes.SaveJob.ToString());
					var jobOrderEditedActionEventId = jobOrderEditedActionEvent == null ? 0 : jobOrderEditedActionEvent.Id;

          var selfReferralActionEvent = actionTypes.FirstOrDefault(x => x.Name == ActionTypes.SelfReferral.ToString());
          var selfReferralActionEventId = selfReferralActionEvent == null ? 0 : selfReferralActionEvent.Id;

          var approveCandidateReferralActionEvent = actionTypes.FirstOrDefault(x => x.Name == ActionTypes.ApproveCandidateReferral.ToString());
          var approveCandidateReferralActionEventId = approveCandidateReferralActionEvent == null ? 0 : approveCandidateReferralActionEvent.Id;

					var createCandidateApplicationActionEvent = actionTypes.FirstOrDefault(x => x.Name == ActionTypes.CreateCandidateApplication.ToString());
					var createCandidateApplicationActionEventId = createCandidateApplicationActionEvent == null ? 0 : createCandidateApplicationActionEvent.Id;

					var countLogIns = employeeBusinessUnits.Select(employeeBusinessUnit => employeeBusinessUnit.Employee.Person.User.Id).Select(userId => Repositories.Core.ActionEvents.Count(x => x.EntityId == userId && x.ActionTypeId == logInActionEventId && x.ActionedOn > date7DaysAgo)).Sum();

					response.Logins7Days = countLogIns;

          // Opened and Edited jobs
          var actionTypeIds = new List<long> {jobOrderOpenActionEventId, jobOrderEditedActionEventId};

          var editEvents = (from ae in Repositories.Core.ActionEvents
                            join j in Repositories.Core.Jobs on ae.EntityId equals j.Id
                            where
                            (
                              j.BusinessUnitId == request.BusinessUnitId &&
                              actionTypeIds.Contains(ae.ActionTypeId) &&
                              ae.ActionedOn >= date30DaysAgo
                            )
                            select ae).ToList();

          response.NoOfJobOrdersOpen30Days = editEvents.Where(editEvent => editEvent.ActionTypeId == jobOrderOpenActionEventId)
                                               .Select(item => item.EntityId)
				                                       .Distinct()
				                                       .Count();

          response.NoOfJobOrdersOpen7Days = editEvents.Where(editEvent => editEvent.ActionTypeId == jobOrderOpenActionEventId && editEvent.ActionedOn >= date7DaysAgo)
                                              .Select(item => item.EntityId)
                                              .Distinct()
                                              .Count();

          response.NoOfOrdersEditedByStaff30Days = editEvents.Where(editEvent => editEvent.ActionTypeId == jobOrderEditedActionEventId)
                                                             .Select(item => item.EntityId)
                                                             .Distinct()
                                                             .Count();

          response.NoOfOrdersEditedByStaff7Days = editEvents.Where(editEvent => editEvent.ActionTypeId == jobOrderEditedActionEventId && editEvent.ActionedOn >= date7DaysAgo)
                                                            .Select(item => item.EntityId)
                                                            .Distinct()
                                                            .Count();

          actionTypeIds = new List<long> { selfReferralActionEventId, approveCandidateReferralActionEventId, createCandidateApplicationActionEventId };

          var referrals = (from ae in Repositories.Core.ActionEvents
				                   join a in Repositories.Core.Applications on ae.EntityId equals a.Id
                           join p in Repositories.Core.Postings on a.PostingId equals p.Id
                           join j in Repositories.Core.Jobs on p.JobId equals j.Id
				                   where 
                           (
                             j.BusinessUnitId == request.BusinessUnitId && 
                             actionTypeIds.Contains(ae.ActionTypeId) && 
                             ae.ActionedOn >= date30DaysAgo
                           )
				                   select ae).ToList();

          response.NoOfResumesReferred30Days = referrals.Select(item => item.EntityId)
                                                        .Distinct()
                                                        .Count();

          response.NoOfResumesReferred7Days = referrals.Where(referralEvent => referralEvent.ActionedOn >= date7DaysAgo)
                                                       .Select(item => item.EntityId)
                                                       .Distinct()
                                                       .Count();

          var candidateActionEvents = Repositories.Core.ActionEvents.Where(ae => (Repositories.Core.Jobs.Where(j => j.BusinessUnitId == request.BusinessUnitId).Select(j => j.Id)).Contains((long)ae.EntityIdAdditional01));

					response.NoOfJobSeekersHired30Days = CountActionsByActionType(ActionTypes.UpdateApplicationStatusToHired, candidateActionEvents, 30);
					response.NoOfJobSeekersHired7Days = CountActionsByActionType(ActionTypes.UpdateApplicationStatusToHired, candidateActionEvents, 7);
					response.NoOfJobSeekersInterviewed30Days = CountActionsByActionType(ActionTypes.UpdateApplicationStatusToInterviewScheduled, candidateActionEvents, 30);
					response.NoOfJobSeekersInterviewed7Days = CountActionsByActionType(ActionTypes.UpdateApplicationStatusToInterviewScheduled, candidateActionEvents, 7);
					response.NoOfJobSeekersRejected30Days = CountActionsByActionType(ActionTypes.UpdateApplicationStatusToNotHired, candidateActionEvents, 30);
					response.NoOfJobSeekersRejected7Days = CountActionsByActionType(ActionTypes.UpdateApplicationStatusToNotHired, candidateActionEvents, 7);


					var lastLoggedInUser = (from ebu in employeeBusinessUnits
					                       orderby ebu.Employee.Person.User.LoggedInOn descending
					                       select ebu.Employee.Person).FirstOrDefault();

					if (lastLoggedInUser != null)
					{
						response.LastLogin = lastLoggedInUser.User.LoggedInOn;
						response.LastLoginEmail = lastLoggedInUser.EmailAddress;
					}

					response.OfficeIds = Repositories.Core.EmployerOfficeMappers.Where(x => x.EmployerId == businessUnit.EmployerId).Select(x => x.OfficeId).ToList();
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the linked companies for a business unit.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public BusinessUnitLinkedCompaniesResponse GetBusinessUnitLinkedCompanies(BusinessUnitLinkedCompaniesRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new BusinessUnitLinkedCompaniesResponse(request);

				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var employerId =
						Repositories.Core.BusinessUnits.Where(x => x.Id == request.Criteria.BusinessUnitId).Select(bu => bu.EmployerId).FirstOrDefault();
					var linkedBusinessUnits =
						Repositories.Core.BusinessUnits.Where(x => x.EmployerId == employerId && x.Id != request.Criteria.BusinessUnitId).OrderBy(ord => ord.Name);
					if (linkedBusinessUnits.Any())
					{
						var linkedCompanies = linkedBusinessUnits.Select(linkedBusinessUnit => new BusinessUnitLinkedCompanyDto
						                                                                       {
							                                                                       BusinessUnit = linkedBusinessUnit.AsDto(), 
																																										 BusinessUnitAddress = Repositories.Core.BusinessUnitAddresses.Single(x => x.BusinessUnitId == linkedBusinessUnit.Id && x.IsPrimary).AsDto(), 
																																										 HiringManagers = Repositories.Core.EmployeeBusinessUnits.Where(ebu => ebu.BusinessUnitId == linkedBusinessUnit.Id).Select(x => new HiringManagerSummaryDto
							                                                                                                                                                                                                              {
								                                                                                                                                                                                                              PersonId = x.Employee.PersonId, 
																																																																																																															EmployeeId = x.EmployeeId, 
																																																																																																															EmployeeBusinessUnitId = x.Employee.EmployeeBusinessUnits.Where(y => y.BusinessUnitId == linkedBusinessUnit.Id).Select(z => z.Id).FirstOrDefault(), 
																																																																																																															FirstName = x.Employee.Person.FirstName, 
																																																																																																															LastName = x.Employee.Person.LastName
							                                                                                                                                                                                                              }).ToList()
						                                                                       }).ToList();

						switch (request.Criteria.OrderBy)
						{
							case Constants.SortOrders.BusinessUnitNameAsc:
								response.LinkedCompanies = linkedCompanies.OrderBy(x => x.BusinessUnit.Name).ToList();
								break;
							case Constants.SortOrders.BusinessUnitNameDesc:
								response.LinkedCompanies = linkedCompanies.OrderByDescending(x => x.BusinessUnit.Name).ToList();
								break;
							case Constants.SortOrders.LocationAsc:
								response.LinkedCompanies = linkedCompanies.OrderBy(x => x.BusinessUnitAddress.TownCity).ToList();
								break;
							case Constants.SortOrders.LocationDesc:
								response.LinkedCompanies = linkedCompanies.OrderByDescending(x => x.BusinessUnitAddress.TownCity).ToList();
								break;
							default:
								response.LinkedCompanies = linkedCompanies.OrderBy(x => x.BusinessUnit.Name).ToList();
								break;
						}
					}
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the employer descriptions.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public BusinessUnitDescriptionResponse GetBusinessUnitDescriptions(BusinessUnitDescriptionRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new BusinessUnitDescriptionResponse(request);

				// Validate request (List requireed during registration so validate client and session but not user)
				if (!ValidateRequest(request, response, Validate.ClientTag | Validate.SessionId | Validate.AccessToken))
					return response;

				try
				{
					var query = new BusinessUnitDescriptionQuery(Repositories.Core, request.Criteria);

					switch (request.Criteria.FetchOption)
					{
						case CriteriaBase.FetchOptions.PagedList:
							response.BusinessUnitDescriptionsPaged = query.Query().GetPagedList(x => x.AsDto(), request.Criteria.PageIndex, request.Criteria.PageSize);
							break;

						case CriteriaBase.FetchOptions.List:
							response.BusinessUnitDescriptions = query.Query().Select(x => x.AsDto()).ToList();
							break;

						default:
              response.BusinessUnitDescription = request.Criteria.BusinessUnitDescriptionId.IsNotNull()
                                                  ? query.QueryEntityId().AsDto()
                                                  : query.Query().Select(x => x.AsDto()).FirstOrDefault();
							break;
					}
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		public SaveBusinessUnitDescriptionResponse UpdateBusinessUnitDescription(SaveBusinessUnitDescriptionRequest request, EmployerRequest originalBusinessUnitDescriptionRequest)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new SaveBusinessUnitDescriptionResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					BusinessUnitDescription originalBusinessUnitDescription = Repositories.Core.BusinessUnitDescriptions.FirstOrDefault(x => x.Id == originalBusinessUnitDescriptionRequest.FocusId);
					if (originalBusinessUnitDescription != null)
					{
						originalBusinessUnitDescription.Title = request.BusinessUnitDescription.Title;
						originalBusinessUnitDescription.Description = request.BusinessUnitDescription.Description;
						originalBusinessUnitDescription.IsPrimary = request.BusinessUnitDescription.IsPrimary;
						originalBusinessUnitDescription.RedProfanityWords = request.BusinessUnitDescription.RedProfanityWords;
						originalBusinessUnitDescription.YellowProfanityWords = request.BusinessUnitDescription.YellowProfanityWords;
						Repositories.Core.SaveChanges();


						// Return employer Description id
						response.BusinessUnitDescription.Id = originalBusinessUnitDescription.Id;

						LogAction(request, ActionTypes.SaveBusinessUnitDescription, typeof(BusinessUnitDescription).Name, originalBusinessUnitDescription.Id,
							request.UserContext.UserId);
					}
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}

		}

		/// <summary>
		/// Saves the employer description.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public SaveBusinessUnitDescriptionResponse SaveBusinessUnitDescription(SaveBusinessUnitDescriptionRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new SaveBusinessUnitDescriptionResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var businessUnitDescription = request.BusinessUnitDescription.CopyTo();

					Repositories.Core.Add(businessUnitDescription);

					Repositories.Core.SaveChanges(true);

					// Update the Business Unit Description
					response.BusinessUnitDescription = businessUnitDescription.AsDto();
										
					LogAction(request, ActionTypes.SaveBusinessUnitDescription, typeof(BusinessUnitDescription).Name, businessUnitDescription.Id, request.UserContext.UserId);
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

	public SaveBusinessUnitLogoResponse UpdateBusinessUnitLogo(SaveBusinessUnitLogoRequest request, EmployerRequest originalBusinessUnitLogoRequest)
	{
		using (Profiler.Profile(request.LogData(), request))
		{
			var response = new SaveBusinessUnitLogoResponse(request);

			// Validate request
			if (!ValidateRequest(request, response, Validate.All))
				return response;

			try
			{
				BusinessUnitLogo originalBusinessUnitLogo =
					Repositories.Core.BusinessUnitLogos.FirstOrDefault(x => x.Id == originalBusinessUnitLogoRequest.FocusId);
				if (originalBusinessUnitLogo != null)
				{
					originalBusinessUnitLogo.Name = request.BusinessUnitLogo.Name;
					originalBusinessUnitLogo.Logo = request.BusinessUnitLogo.Logo;
					Repositories.Core.SaveChanges();


					// Return employer logo id
					response.BusinessUnitLogoId = originalBusinessUnitLogo.Id;

					LogAction(request, ActionTypes.SaveBusinessUnitLogo, typeof (BusinessUnitLogo).Name, originalBusinessUnitLogo.Id,
						request.UserContext.UserId);
				}
			}

			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}

			return response;
		}

	}

		/// <summary>
		/// Saves the business unit logo.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public SaveBusinessUnitLogoResponse SaveBusinessUnitLogo(SaveBusinessUnitLogoRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new SaveBusinessUnitLogoResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					

					var businessUnitLogo = request.BusinessUnitLogo.CopyTo();
					Repositories.Core.Add(businessUnitLogo);

					Repositories.Core.SaveChanges(true);

					// Return employer logo id
					response.BusinessUnitLogoId = businessUnitLogo.Id;
					
					LogAction(request, ActionTypes.SaveBusinessUnitLogo, typeof(BusinessUnitLogo).Name, businessUnitLogo.Id, request.UserContext.UserId);
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the employer job titles.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public EmployerJobTitleResponse GetEmployerJobTitles(EmployerJobTitleRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new EmployerJobTitleResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var query = new JobQuery(Repositories.Core, request.Criteria);
					response.JobTitles = query.Query().Select(x => x.JobTitle).Distinct().ToList();
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the employer placements.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public EmployerPlacementsViewResponse GetEmployerPlacements(EmployerPlacementsViewRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new EmployerPlacementsViewResponse(request);

				// Validate request
				if (!ValidateRequest(request,response,Validate.All))
					return response;

				try
				{
					if(request.Criteria.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.CriteriaRequired));
						Logger.Info(request.LogData(), "The no employer placement view criteria passed.");
						return response;	
					}

					if (!request.Criteria.NumberOfDays.HasValue) request.Criteria.NumberOfDays = AppSettings.JobDevelopmentEmployerPlacementsNoOfDays;

					if (request.Criteria.OfficeGroup == OfficeGroup.MyOffices) request.Criteria.PersonId = request.UserContext.PersonId.GetValueOrDefault();

					var query = new EmployerPlacementsViewQuery(Repositories.Core, request.Criteria);

					var postings = query.Query().Select(x => x.AsDto()).ToList();
					
					var employerPostingsViews = postings.GroupBy(x => x.EmployerName)
																			.Select(employer => employer.ToList())
																			.Select(jobs => new Focus.Core.Views.EmployerPlacementsView { Placements = jobs })
																			.AsQueryable();

					var count = employerPostingsViews.Count();

					employerPostingsViews = employerPostingsViews.Skip(request.Criteria.PageIndex * request.Criteria.PageSize).Take(request.Criteria.PageSize);

					response.EmployerPlacementsViewsPaged = new PagedList<Focus.Core.Views.EmployerPlacementsView>(employerPostingsViews, count, request.Criteria.PageIndex, request.Criteria.PageSize,false);

					response.Acknowledgement = AcknowledgementType.Success;

				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}
				return response;
			}
		}

		/// <summary>
		/// Gets the employer approval status.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public GetEmployerDetailsResponse GetEmployerApprovalStatus(GetEmployerDetailsRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new GetEmployerDetailsResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.Explorer))
					return response;

				try
				{
					response.ApprovalStatus = Repositories.Core.Employers.Where(x => x.Id == request.Criteria.EmployerId).Select(x => x.ApprovalStatus).FirstOrDefault();
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the employer.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public GetEmployerDetailsResponse GetEmployerDetails(GetEmployerDetailsRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new GetEmployerDetailsResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.Explorer))
					return response;

				try
				{
					var employer = Repositories.Core.Query<Employer>().SingleOrDefault(x => x.Id == request.Criteria.EmployerId);

					if (employer.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.EmployerNotFound));
						Logger.Info(request.LogData(), string.Format("EmployerId '{0}' not found.", request.Criteria.EmployerId));
					}

					var primaryAddress = Repositories.Core.EmployerAddresses.Where(x => x.EmployerId == request.Criteria.EmployerId
																																				&& x.IsPrimary).Select(x => x.AsDto()).
						SingleOrDefault();

					if (primaryAddress != null)
					{
						response.PrimaryAddress = primaryAddress;
					}

					var primaryPhoneNumber = new PhoneNumberDto { Number = employer.PrimaryPhone };
					PhoneNumberDto alternatePhoneNumber1 = null;
					PhoneNumberDto alternatePhoneNumber2 = null;

					if (employer.PrimaryPhoneExtension.IsNotNullOrEmpty())
						primaryPhoneNumber.Extension = employer.PrimaryPhoneExtension;

					if (employer.PrimaryPhoneType.IsNotNullOrEmpty())
					{
						var phoneType = (PhoneTypes)Enum.Parse(typeof(PhoneTypes), employer.PrimaryPhoneType);
						primaryPhoneNumber.PhoneType = phoneType;
					}

					if (employer.AlternatePhone1.IsNotNullOrEmpty())
					{
						alternatePhoneNumber1 = new PhoneNumberDto { Number = employer.AlternatePhone1 };
						var phoneType = (PhoneTypes)Enum.Parse(typeof(PhoneTypes), employer.AlternatePhone1Type);
						alternatePhoneNumber1.PhoneType = phoneType;
					}

					if (employer.AlternatePhone2.IsNotNullOrEmpty())
					{
						alternatePhoneNumber2 = new PhoneNumberDto { Number = employer.AlternatePhone2 };
						var phoneType = (PhoneTypes)Enum.Parse(typeof(PhoneTypes), employer.AlternatePhone2Type);
						alternatePhoneNumber2.PhoneType = phoneType;
					}

					response.ApprovalStatus = employer.ApprovalStatus;
					if (employer.ApprovalStatus == ApprovalStatuses.Approved)
						response.IsAlreadyApproved = true;

					response.Name = employer.Name;
					response.FederalEmployerIdentificationNumber = employer.FederalEmployerIdentificationNumber;
					response.StateEmployerIdentificationNumber = employer.StateEmployerIdentificationNumber;
					response.IndustrialClassification = employer.IndustrialClassification;

					response.AlternatePhoneNumber1 = alternatePhoneNumber1;
					response.AlternatePhoneNumber2 = alternatePhoneNumber2;
					response.PrimaryPhoneNumber = primaryPhoneNumber;
					response.LockVersion = employer.LockVersion;
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the employer legal name.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public GetEmployerDetailsResponse GetEmployerLegalName(GetEmployerDetailsRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new GetEmployerDetailsResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.Explorer))
					return response;

				try
				{
					response.LegalName = Repositories.Core.Query<Employer>().Where(x => x.Id == request.Criteria.EmployerId).Select(x => x.LegalName).FirstOrDefault();
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Updates the employer details. 
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public SaveEmployerDetailsResponse SaveEmployerDetails(SaveEmployerDetailsRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new SaveEmployerDetailsResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					// Employer details
					var employer = Repositories.Core.FindById<Employer>(request.Criteria.EmployerId);

					if (employer.IsNull())
					{

						response.SetFailure(FormatErrorMessage(request, ErrorTypes.EmployerNotFound));
						Logger.Info(request.LogData(), string.Format("EmployerId '{0}' not found.", request.Criteria.EmployerId));
					}

					if (employer.LockVersion != request.LockVersion)
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.LockVersionOutOfDate));
						return response;
					}

					if (employer.FederalEmployerIdentificationNumber != request.FederalEmployerIdentificationNumber
						  || employer.StateEmployerIdentificationNumber != request.StateEmployerIdentificationNumber
							|| employer.Name != request.Name)
					{
						// Update the employer details
						employer.FederalEmployerIdentificationNumber = request.FederalEmployerIdentificationNumber;
						employer.StateEmployerIdentificationNumber = request.StateEmployerIdentificationNumber;
						employer.Name = request.Name;
					}
					else
					{
						var changes = new Dictionary<string, object> { { "LockVersion", employer.LockVersion + 1 } };
						var updateQuery = new Query(typeof(Employer), Entity.Attribute("Id") == employer.Id);
						Repositories.Core.Update(updateQuery, changes);
					}

					Repositories.Core.SaveChanges(true);

					// Save employer in any integrated client
					if (employer.ApprovalStatus == ApprovalStatuses.Approved)
						employer.PublishEmployerUpdateIntegrationRequest(request.UserContext.ActionerId, RuntimeContext);

					// Log the action
					LogAction(request, ActionTypes.SaveEmployerDetails, employer);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the employer address.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public EmployerAddressResponse GetEmployerAddress(EmployerAddressRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new EmployerAddressResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					if (request.Criteria.IsPrimary.HasValue && request.Criteria.IsPrimary.Value)
					{
						response.PrimaryEmployerAddress = Repositories.Core.EmployerAddresses.Where(
							x => x.EmployerId == request.Criteria.EmployerId
									 && x.IsPrimary == request.Criteria.IsPrimary).Select(x => x.AsDto()).SingleOrDefault();
					}
					else if (request.Criteria.IsPrimary.HasValue && !request.Criteria.IsPrimary.Value)
					{

						response.OtherEmployerAddresses = Repositories.Core.EmployerAddresses.Where(
							x => x.EmployerId == request.Criteria.EmployerId
									 && x.IsPrimary == request.Criteria.IsPrimary).Select(x => x.AsDto()).ToList();

					}
					else
					{
						response.AllEmployerAddresses = Repositories.Core.EmployerAddresses.Where(
							x => x.EmployerId == request.Criteria.EmployerId).Select(x => x.AsDto()).ToList();
					}
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the business unit address.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public BusinessUnitAddressResponse GetBusinessUnitAddress(BusinessUnitAddressRequest request)
		{ 
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new BusinessUnitAddressResponse(request);

				// Validate request (List requireed during registration so validate client and session but not user)
				if (!ValidateRequest(request, response, Validate.ClientTag | Validate.SessionId | Validate.AccessToken))
					return response;

				try
				{
					var query = new BusinessUnitAddressQuery(Repositories.Core, request.Criteria);

					switch (request.Criteria.FetchOption)
					{
						case CriteriaBase.FetchOptions.PagedList:
							response.BusinessUnitAddressesPaged = query.Query().GetPagedList(x => x.AsDto(), request.Criteria.PageIndex, request.Criteria.PageSize);
							break;
						case CriteriaBase.FetchOptions.List:
							response.BusinessUnitAddresses = query.Query().Select(x => x.AsDto()).ToList();
							break;
						default:
							response.BusinessUnitAddress = request.Criteria.Id.IsNotNull()
																							? query.QueryEntityId().AsDto()
																							: query.Query().SingleOrDefault().AsDto();
							break;
					}
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}
		
		/// <summary>
		/// Saves the business unit address
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public SaveBusinessUnitAddressResponse SaveBusinessUnitAddress(SaveBusinessUnitAddressRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new SaveBusinessUnitAddressResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var businessUnitAddress = request.BusinessUnitAddress.CopyTo();
					Repositories.Core.Add(businessUnitAddress);

					Repositories.Core.SaveChanges(true);

					// Update the Business Unit Description
					response.BusinessUnitAddress = businessUnitAddress.AsDto();

					LogAction(request, ActionTypes.SaveBusinessUnitDescription, businessUnitAddress);
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the employer jobs.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public EmployerJobResponse GetEmployerJobs(EmployerJobRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new EmployerJobResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
				  if (request.Criteria.EmployerId.HasValue)
				  {
            var query = Repositories.Core.Jobs.Where(x => x.EmployerId == request.Criteria.EmployerId && x.JobStatus != JobStatuses.Draft);

				    if (request.Criteria.JobTypes.IsNotNullOrEmpty())
				      query = query.Where(x => request.Criteria.JobTypes.Contains(x.JobType));

            response.Jobs = query.Select(x => new SelectView {Id = x.Id, Text = x.JobTitle})
				                         .Take(request.Criteria.ListSize)
				                         .ToList();
				  }
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}
		
		/// <summary>
		/// Saves the business unit
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public SaveBusinessUnitResponse SaveBusinessUnit(SaveBusinessUnitRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new SaveBusinessUnitResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var businessUnit = request.BusinessUnit.CopyTo();
					Repositories.Core.Add(businessUnit);

					Repositories.Core.SaveChanges(true);

					// Update the Business Unit Description
					response.BusinessUnit = businessUnit.AsDto();

					LogAction(request, ActionTypes.SaveBusinessUnitDescription, businessUnit);
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Saves a business unit's descriptions.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public SaveBusinessUnitDescriptionsResponse SaveBusinessUnitDescriptions(SaveBusinessUnitDescriptionsRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new SaveBusinessUnitDescriptionsResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					// Find descriptions by business unit
					var currentdescriptions =
						Repositories.Core.BusinessUnitDescriptions.Where(x => x.BusinessUnitId == request.BusinessUnitId).ToList();

					// Find descriptions associated with jobs and therefore can not be deleted
					var descriptionsAssociatedWithJob = (from bud in Repositories.Core.BusinessUnitDescriptions
																							 join j in Repositories.Core.Jobs on bud.Id equals j.BusinessUnitDescriptionId
																							 where bud.BusinessUnitId == request.BusinessUnitId
																							 select bud).Distinct().ToList();

					// get list of descriptions to delete
					var descriptionForDeletion =
						currentdescriptions.Where(bud => request.Descriptions.FirstOrDefault(x => x.Id == bud.Id).IsNull()).ToList
							();

					// Find if descriptions is blocked from being deleted because it is on a job
					var blockedDeletion =
						descriptionForDeletion.Where(
							bud => descriptionsAssociatedWithJob.FirstOrDefault(x => x.Id == bud.Id).IsNotNull()).ToList();

					// We cannot delete descriptions associated with a job
					if (blockedDeletion.Count > 0)
					{
						response.SetFailure(FormatErrorMessage(request,
																									 ErrorTypes.BusinessUnitDescriptionCannotBeDeletedWithJobAssociation));
						response.DescriptionsAssociatedWithJob = blockedDeletion.Select(x => x.AsDto()).ToList();
						Logger.Info(request.LogData(), "Business Unit Descriptions marked for deletion found to be associated with a job");
						return response;
					}

					// Delete Descriptions's
					descriptionForDeletion.ForEach(x => Repositories.Core.Remove(x));

					var businessUnitDescripionsSaved = new List<BusinessUnitDescription>();
					foreach (var description in request.Descriptions)
					{

						// Validate required properties
						if (description.Id.IsNull())
						{
							response.SetFailure(FormatErrorMessage(request, ErrorTypes.BusinessUnitDescriptionIdRequired));
							Logger.Info(request.LogData(), "Business unit description id required");
							return response;
						}

						if (description.Description.IsNull())
						{
							response.SetFailure(FormatErrorMessage(request, ErrorTypes.BusinessUnitDescriptionDescriptionRequired));
							Logger.Info(request.LogData(), "Business unit description required");
							return response;
						}

						// Save
						BusinessUnitDescription saveBusinessUnitDescription;
						if (description.Id == 0)
						{
							saveBusinessUnitDescription = new BusinessUnitDescription();
							saveBusinessUnitDescription = description.CopyTo(saveBusinessUnitDescription);
							Repositories.Core.Add(saveBusinessUnitDescription);
						}
						else
						{
							saveBusinessUnitDescription = currentdescriptions.FirstOrDefault(x => x.Id == description.Id);
							saveBusinessUnitDescription = description.CopyTo(saveBusinessUnitDescription);
						}

						businessUnitDescripionsSaved.Add(saveBusinessUnitDescription);
					}

          #region Censorship Check

          response.FailedCensorshipCheck = false;
          if (request.CheckCensorship && AppSettings.NewEmployerCensorshipFiltering && !request.UserContext.IsShadowingUser)
          {
            var businessUnit = Repositories.Core.BusinessUnits.First(bu => bu.Id == request.BusinessUnitId);
            var employee = Repositories.Core.FindById<Employee>(request.UserContext.EmployeeId);

            foreach (var description in businessUnitDescripionsSaved)
            {
              response.FailedCensorshipCheck |= BusinessUnitCensorshipUpdates(description.Description, businessUnit, employee, ApprovalStatusReason.BusinessUnitDescriptionCensorshipFailed, description);
            }
          }

          #endregion

					Repositories.Core.SaveChanges(true);

					response.Descriptions = new List<BusinessUnitDescriptionDto>();
					response.Descriptions.AddRange(businessUnitDescripionsSaved.Select(x => x.AsDto()));

					// log changes
					businessUnitDescripionsSaved.ForEach(ed => LogAction(request, ActionTypes.SaveBusinessUnitDescription, typeof(BusinessUnitDescription).Name, ed.Id, request.UserContext.UserId));

					descriptionForDeletion.ForEach(ed => LogAction(request, ActionTypes.DeleteBusinessUnitDescription, typeof(BusinessUnitDescription).Name, ed.Id, request.UserContext.UserId));
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}
				return response;
			}
		}

    /// <summary>
    /// Updates fields on the business unit and employee based on censorship checking
    /// </summary>
    /// <param name="checkText">The text to censorship check</param>
    /// <param name="businessUnit">The business unit</param>
    /// <param name="employee">The employee updating the business unit</param>
    /// <param name="reason">The reason the business unit needs checking</param>
    /// <param name="description">The business unit description.</param>
    /// <returns></returns>
    private bool BusinessUnitCensorshipUpdates(string checkText, BusinessUnit businessUnit, Employee employee, ApprovalStatusReason reason, BusinessUnitDescription description = null)
    {
      var failed = false;
      var censorshipModel = ProfanityCheck(checkText);

      if (censorshipModel.RedWords.IsNotNullOrEmpty() || censorshipModel.YellowWords.IsNotNullOrEmpty())
      {
        failed = true;
        if (businessUnit.ApprovalStatus != ApprovalStatuses.WaitingApproval)
        {
          businessUnit.ApprovalStatusReason = reason;
          businessUnit.ApprovalStatus = ApprovalStatuses.WaitingApproval;
          businessUnit.AwaitingApprovalDate = DateTime.Now;
        }
        else if (businessUnit.ApprovalStatusReason != ApprovalStatusReason.Default)
        {
          businessUnit.ApprovalStatusReason = (businessUnit.ApprovalStatusReason | reason);
        }

        // Update Red/Yellow Words
        if (description.IsNull())
        {
          businessUnit.RedProfanityWords = censorshipModel.RedWords;
          businessUnit.YellowProfanityWords = censorshipModel.YellowWords;
        }
        else
        {
          description.RedProfanityWords = censorshipModel.RedWords;
          description.YellowProfanityWords = censorshipModel.YellowWords;
        }

        if (employee != null)
        {
          if (employee.ApprovalStatus != ApprovalStatuses.WaitingApproval)
          {
            employee.ApprovalStatusReason = reason;
            employee.ApprovalStatus = ApprovalStatuses.WaitingApproval;
            employee.AwaitingApprovalDate = DateTime.Now;
          }
          else if (employee.ApprovalStatusReason != ApprovalStatusReason.Default)
          {
            employee.ApprovalStatusReason = (employee.ApprovalStatusReason | reason);
          }
        }
      }
      else
      {
        // Reset the approval status if it was original set due to censorship failure on business unit record
        if (businessUnit.ApprovalStatus == ApprovalStatuses.WaitingApproval && (businessUnit.ApprovalStatusReason & reason) == reason)
        {
          businessUnit.ApprovalStatusReason = (businessUnit.ApprovalStatusReason & ~reason);
          if (businessUnit.ApprovalStatusReason == ApprovalStatusReason.Default)
          {
            businessUnit.ApprovalStatus = ApprovalStatuses.None;
            businessUnit.AwaitingApprovalDate = null;
          }
        }

        // Reset Red/Yellow Words
        if (description.IsNull())
        {
          businessUnit.RedProfanityWords = null;
          businessUnit.YellowProfanityWords = null;
        }
        else
        {
          description.RedProfanityWords = null;
          description.YellowProfanityWords = null;
        }

        if (employee.IsNotNull())
        {
          // Reset the approval status if it was original set due to censorship failure on employee record
          if (employee.ApprovalStatus == ApprovalStatuses.WaitingApproval && (employee.ApprovalStatusReason & reason) == reason)
          {
            employee.ApprovalStatusReason = (employee.ApprovalStatusReason & ~reason);
            if (employee.ApprovalStatusReason == ApprovalStatusReason.Default)
            {
              employee.ApprovalStatus = ApprovalStatuses.None;
              employee.AwaitingApprovalDate = null;
            }
          }
        }
      }

      return failed;
    }
		
		/// <summary>
		/// Gets the business unit logos.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public BusinessUnitLogoResponse GetBusinessUnitLogos(BusinessUnitLogoRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new BusinessUnitLogoResponse(request);

				// Validate Client Tag as a user may view a logo without being logged in
				if (!ValidateRequest(request, response, Validate.ClientTag | Validate.AccessToken))
					return response;

				try
				{
					var query = new BusinessUnitLogoQuery(Repositories.Core, request.Criteria);

					switch (request.Criteria.FetchOption)
					{

						case CriteriaBase.FetchOptions.PagedList:
							response.BusinessUnitLogosPaged = query.Query().GetPagedList(x => x.AsDto(), request.Criteria.PageIndex, request.Criteria.PageSize);
							break;

						case CriteriaBase.FetchOptions.List:
							response.BusinessUnitLogos = query.Query().Select(x => x.AsDto()).ToList();
							break;
						default:

							response.BusinessUnitLogo = request.Criteria.Id.IsNotNull()
																						? query.QueryEntityId().AsDto()
																						: query.Query().SingleOrDefault().AsDto();
							break;
					}
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}
				return response;
			}
		}

		/// <summary>
		/// Saves a business unit's logos.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public SaveBusinessUnitLogosResponse SaveBusinessUnitLogos(SaveBusinessUnitLogosRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new SaveBusinessUnitLogosResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					// Find logos by business unit
					var currentLogos = Repositories.Core.BusinessUnitLogos.Where(x => x.BusinessUnitId == request.BusinessUnitId).ToList();

					// Find logos associated with jobs and therefore can not be deleted
					var logosAssociatedWithJob = (from bul in Repositories.Core.BusinessUnitLogos
																				join j in Repositories.Core.Jobs on bul.Id equals j.BusinessUnitLogoId
																				where bul.BusinessUnitId == request.BusinessUnitId
																				select bul).Distinct().ToList();

					// get list of logos to delete
					var logosForDeletion =
						currentLogos.Where(bul => request.BusinessUnitLogos.FirstOrDefault(x => x.Id == bul.Id).IsNull()).ToList();

					// Find if logos is blocked from being deleted because it is on a job
					var blockedDeletion =
						logosForDeletion.Where(bul => logosAssociatedWithJob.FirstOrDefault(x => x.Id == bul.Id).IsNotNull()).
							ToList();

					// We cannot delete logos associated with a job
					if (blockedDeletion.Count > 0)
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.BusinessUnitLogoCannotBeDeletedWithJobAssociation));
						response.BusinessUnitLogosAssociatedWithJob = blockedDeletion.Select(x => x.AsDto()).ToList();
						Logger.Info(request.LogData(), "Business Unit Logos marked for deletion found to be associated with a job");
						return response;
					}

					// Delete logos's
					logosForDeletion.ForEach(x => Repositories.Core.Remove(x));

					var logosSaved = new List<BusinessUnitLogo>();
					foreach (var logo in request.BusinessUnitLogos)
					{
						BusinessUnitLogo saveLogo;
						if (logo.Id == 0)
						{
							saveLogo = new BusinessUnitLogo();
							saveLogo = logo.CopyTo(saveLogo);
							Repositories.Core.Add(saveLogo);
						}
						else
						{
							saveLogo = currentLogos.FirstOrDefault(x => x.Id == logo.Id);
							saveLogo = logo.CopyTo(saveLogo);
						}

						logosSaved.Add(saveLogo);
					}

					Repositories.Core.SaveChanges(true);

					response.BusinessUnitLogos = new List<BusinessUnitLogoDto>();
					response.BusinessUnitLogos.AddRange(logosSaved.Select(x => x.AsDto()));

					// log changes
					logosSaved.ForEach(ed => LogAction(request, ActionTypes.SaveBusinessUnitLogo, typeof(BusinessUnitLogo).Name, ed.Id, request.UserContext.UserId));
					logosForDeletion.ForEach(ed => LogAction(request, ActionTypes.DeleteBusinessUnitLogo, typeof(BusinessUnitLogo).Name, ed.Id, request.UserContext.UserId));
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}
				return response;
			}
		}

		/// <summary>
		/// Assigns the employer to admin. REFERS TO HIRING MANAGER (EmployeeId)
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public AssignEmployerToAdminResponse AssignEmployerToAdmin(AssignEmployerToAdminRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new AssignEmployerToAdminResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var user = Repositories.Core.Users.FirstOrDefault(x => x.Id == request.AdminId);

					if (user.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.UserNotFound));
						Logger.Error(request.LogData(), "User not found");
						return response;
					}

          var employee = Repositories.Core.FindById<Employee>(request.EmployeeId);

          request.ExternalAdminId = user.ExternalId;

          if (AppSettings.IntegrationClient != IntegrationClient.Standalone)
          {
            if (user.ExternalId.IsNotNullOrEmpty())
            {
              var employerExternalId = employee.Employer.ExternalId;

              Helpers.Messaging.Publish(new IntegrationRequestMessage
              {
                ActionerId = request.UserContext.UserId,
                IntegrationPoint = IntegrationPoint.AssignAdminToEmployer,
                AssignAdminToEmployerRequest = request.ToAssignAdminToEmployerRequest(employerExternalId, user.ExternalId)
              });

              response.AssignSuccessful = true;
            }
          }
          else
          {
            response.AssignSuccessful = true;
          }
            
					employee.StaffUserId = request.AdminId;

					Repositories.Core.SaveChanges();

					LogAction(request, ActionTypes.AssignEmployeeToStaff, typeof(Employee).Name, request.EmployeeId, request.AdminId);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Checks the naics exists.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public CheckNaicsExistsResponse CheckNaicsExists(CheckNaicsExistsRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new CheckNaicsExistsResponse(request);

				// Validate client tag & session only
				if (!ValidateRequest(request, response, Validate.ClientTag | Validate.SessionId | Validate.AccessToken))
					return response;

				try
				{
				  if (request.Naics.IsNullOrEmpty())
				  {
				    response.SetFailure(FormatErrorMessage(request, ErrorTypes.EmployerNotFound));
				  }
				  else
				  {
				    var code = request.Naics;

				    var index = code.IndexOf(" - ", StringComparison.Ordinal);
				    if (index > -1)
				      code = code.Substring(0, index);

				    code = string.Concat("[", code, "]");

				    var query = GetAllNaics(request).AsQueryable();
				    if (request.ChildrenOnly)
				      query = query.Where(naics => naics.IsChild);

				    response.Exists = query.Count(x => string.Concat("[", x.Code, "]") == code) > 0;
				  }
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Blocks the employees accounts.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public BlockEmployeesAccountsResponse BlockEmployeesAccounts(BlockEmployeesAccountsRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new BlockEmployeesAccountsResponse(request);
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var businessUnit = Repositories.Core.FindById<BusinessUnit>(request.BusinessUnitId);
					if (businessUnit.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.BusinessUnitNotFound));
						Logger.Info(request.LogData(), string.Format("The business unit '{0}' was not found.", request.BusinessUnitId));
						return response;
					}

					var businessUnitEmployeeIds = businessUnit.EmployeeBusinessUnits.Select(x => x.EmployeeId).ToList();
					var employees = Repositories.Core.Employees.Where(x => businessUnitEmployeeIds.Contains(x.Id));
          foreach (var employee in employees)
          {
            employee.Person.User.Blocked = true;
            employee.Person.User.PublishUpdateEmployeeStatusRequest(true, RuntimeContext, employee.Employer);
            LogAction(request, ActionTypes.BlockAllEmployerEmployeesAccount, typeof(Employee).Name, employee.Id);
          }

				  Repositories.Core.SaveChanges(true);
					LogAction(request, ActionTypes.BlockAllEmployerEmployeesAccount, typeof(Employer).Name, request.BusinessUnitId);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Unblocks the employees accounts.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public UnblockEmployeesAccountsResponse UnblockEmployeesAccounts(UnblockEmployeesAccountsRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new UnblockEmployeesAccountsResponse(request);
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var businessUnit = Repositories.Core.FindById<BusinessUnit>(request.BusinessUnitId);
					if (businessUnit.IsNull())
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.EmployerNotFound));
						Logger.Info(request.LogData(), string.Format("The business unit '{0}' was not found.", request.BusinessUnitId));
						return response;
					}

					var businessUnitEmployeeIds = businessUnit.EmployeeBusinessUnits.Select(x => x.EmployeeId).ToList();
					var employees = Repositories.Core.Employees.Where(x => businessUnitEmployeeIds.Contains(x.Id));
					foreach (var employee in employees)
					{
						employee.Person.User.Blocked = false;
                        //FVN-6014
                        if (employee.Person.User.BlockedReason.IsNotNull())
                        {
                            if (employee.Person.User.BlockedReason.Equals(BlockedReason.FailedPasswordReset))
                                employee.Person.User.PasswordAttempts = 0;
                            //Reset Blocked reason - FVN-5922
                            employee.Person.User.BlockedReason = null;
                        }
            employee.Person.User.PublishUpdateEmployeeStatusRequest(false, RuntimeContext, employee.Employer);
            LogAction(request, ActionTypes.UnblockAllEmployerEmployeesAccount, typeof(Employee).Name, employee.Id);
					}

					Repositories.Core.SaveChanges(true);
					LogAction(request, ActionTypes.UnblockAllEmployerEmployeesAccount, typeof(Employer).Name, request.BusinessUnitId);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Saves the business unit model.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public SaveBusinessUnitModelResponse SaveBusinessUnitModel(SaveBusinessUnitModelRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new SaveBusinessUnitModelResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var currentBusinessUnit = Repositories.Core.FindById<BusinessUnit>(request.Model.BusinessUnit.Id??0);
					
					if (currentBusinessUnit.IsNotNull())
					{
						if (request.Model.LockVersion != currentBusinessUnit.LockVersion)
						{
							response.SetFailure(FormatErrorMessage(request, ErrorTypes.LockVersionOutOfDate));
							return response;
						}
					}

					if (request.Model.BusinessUnit.IsNull())
						throw new Exception("Business unit is required.");

					BusinessUnit businessUnit;

					if (request.Model.BusinessUnit.Id.IsNotNull() && request.Model.BusinessUnit.Id > 0)
					{
						businessUnit = Repositories.Core.FindById<BusinessUnit>(request.Model.BusinessUnit.Id);

						// If we cant find the business unit with this Id something is wrong
						if (businessUnit.IsNull())
						{
							response.SetFailure(FormatErrorMessage(request, ErrorTypes.BusinessUnitNotFound));
							Logger.Info(request.LogData(), string.Format("The business unit '{0}' was not found.", request.Model.BusinessUnit.Id));

							return response;
						}

						// Don't update approval status at this point
						request.Model.BusinessUnit.ApprovalStatus = businessUnit.ApprovalStatus;
						request.Model.BusinessUnit.AwaitingApprovalDate = businessUnit.AwaitingApprovalDate;
						request.Model.BusinessUnit.ApprovalStatusReason = businessUnit.ApprovalStatusReason;

						businessUnit = request.Model.BusinessUnit.CopyTo(businessUnit);
                        //businessUnit.LegalName = request.Model.BusinessUnit.LegalName; - commented since updated in next step for all business units

                        //update legal name for all business units of the employer
                        var businessUnits = Repositories.Core.Find<BusinessUnit>(x => x.EmployerId == businessUnit.EmployerId);
                        businessUnits.ToList().ForEach(bu =>
                        {
                            bu.LegalName = businessUnit.LegalName;
                        });

                        //Update Employer legal name
                        var businessUnitEmployer = Repositories.Core.FindById<Employer>(businessUnit.EmployerId);
                        businessUnitEmployer.LegalName = businessUnit.LegalName;

					}
					else
					{
						businessUnit = request.Model.BusinessUnit.CopyTo();
						Repositories.Core.Add(businessUnit);
					}

          if (businessUnit.IsPrimary && businessUnit.EmployerId > 0)
          {
            var employer = Repositories.Core.FindById<Employer>(businessUnit.EmployerId);
            //employer.LegalName = businessUnit.LegalName; - commented since employer legal name is updated even if the business unit is not primary
						employer.Name = businessUnit.Name;
            if (employer.Url == string.Empty)
              employer.Url = null;
          }

					#region Business Unit Address

					if (request.Model.BusinessUnitAddress.IsNull())
						throw new Exception("Business unit address is required.");

					BusinessUnitAddress businessUnitAddress;

					if (request.Model.BusinessUnitAddress.Id.IsNotNull() && request.Model.BusinessUnitAddress.Id > 0)
					{
						businessUnitAddress = Repositories.Core.FindById<BusinessUnitAddress>(request.Model.BusinessUnitAddress.Id);

						// If we cant find the business unit address with this Id something is wrong
						if (businessUnitAddress.IsNull())
						{
							response.SetFailure(FormatErrorMessage(request, ErrorTypes.BusinessUnitAddressNotFound));
							Logger.Info(request.LogData(),
													string.Format("The business unit address '{0}' was not found.", request.Model.BusinessUnitAddress.Id));

							return response;
						}

						businessUnitAddress = request.Model.BusinessUnitAddress.CopyTo(businessUnitAddress);
					}
					else
					{
						businessUnitAddress = request.Model.BusinessUnitAddress.CopyTo();
						Repositories.Core.Add(businessUnitAddress);
					}

					if (businessUnit.BusinessUnitAddresses.IsNullOrEmpty() ||
							(businessUnit.BusinessUnitAddresses.All(x => x.Id != businessUnitAddress.Id)))
						businessUnit.BusinessUnitAddresses.Add(businessUnitAddress);

					#endregion

          #region Business Unit Description

          BusinessUnitDescription businessUnitDescription = null;
          
          if (request.Model.BusinessUnitDescription.IsNotNull())
          {

            if (request.Model.BusinessUnitDescription.Id.IsNotNull() && request.Model.BusinessUnitDescription.Id > 0)
            {
              businessUnitDescription = Repositories.Core.FindById<BusinessUnitDescription>(request.Model.BusinessUnitDescription.Id);

              // If we cant find the business unit description with this Id something is wrong
              if (businessUnitDescription.IsNull())
              {
                response.SetFailure(FormatErrorMessage(request, ErrorTypes.BusinessUnitDescriptionNotFound));
                Logger.Info(request.LogData(),
                            string.Format("The business unit description '{0}' was not found.",
                                          request.Model.BusinessUnitDescription.Id));

                return response;
              }

              businessUnitDescription = request.Model.BusinessUnitDescription.CopyTo(businessUnitDescription);
            }
            else
            {
              businessUnitDescription = request.Model.BusinessUnitDescription.CopyTo();
              Repositories.Core.Add(businessUnitDescription);
            }

            if (businessUnit.BusinessUnitDescriptions.IsNullOrEmpty() ||
                (businessUnit.BusinessUnitDescriptions.All(x => x.Id != businessUnitDescription.Id)))
              businessUnit.BusinessUnitDescriptions.Add(businessUnitDescription);
          }

          #endregion

          #region Censorship Check

          response.FailedCensorshipCheck = false;
          if ((request.CheckCensorshipBusinessUnit || request.CheckCensorshipDescription) && AppSettings.NewEmployerCensorshipFiltering && !request.UserContext.IsShadowingUser)
          {
            var stringBuilder = new StringBuilder();
            var employee = Repositories.Core.FindById<Employee>(request.UserContext.EmployeeId);

            if (request.CheckCensorshipBusinessUnit)
            {
              var unitProperties = request.Model.BusinessUnit.GetType().GetProperties(BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance);
              foreach (var propertyInfo in unitProperties)
              {
                if (!propertyInfo.Name.EndsWith("ProfanityWords"))
                  stringBuilder.Append(" ").Append(propertyInfo.GetValue(request.Model.BusinessUnit, null));
              }

              var addressProperties = request.Model.BusinessUnitAddress.GetType().GetProperties(BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance);
              foreach (var propertyInfo in addressProperties)
              {
                stringBuilder.Append(" ").Append(propertyInfo.GetValue(request.Model.BusinessUnitAddress, null));
              }

              var checkText = stringBuilder.ToString();
              response.FailedCensorshipCheck = BusinessUnitCensorshipUpdates(checkText, businessUnit, employee, ApprovalStatusReason.BusinessUnitCensorshipFailed);
            }

            if (request.CheckCensorshipDescription && businessUnitDescription.IsNotNull())
              response.FailedCensorshipCheck |= BusinessUnitCensorshipUpdates(businessUnitDescription.Description, businessUnit, employee, ApprovalStatusReason.BusinessUnitDescriptionCensorshipFailed, businessUnitDescription);
          }

          #endregion

          Repositories.Core.SaveChanges(true);

					// Update the Business Unit Model
					response.Model = new BusinessUnitModel
					{
						BusinessUnit = businessUnit.AsDto(),
						BusinessUnitAddress = businessUnitAddress.AsDto(),
            BusinessUnitDescription = businessUnitDescription.IsNotNull() ? businessUnitDescription.AsDto() : null
					};

          if (businessUnit.IsPrimary && AppSettings.IntegrationClient != IntegrationClient.Standalone)
          {
            var employer = businessUnit.Employer;
            if (employer.ApprovalStatus == ApprovalStatuses.Approved)
              employer.PublishEmployerUpdateIntegrationRequest(request.UserContext.ActionerId, RuntimeContext);
          }

					LogAction(request, ActionTypes.SaveBusinessUnit, typeof(BusinessUnit).Name, businessUnit.Id, request.UserContext.UserId);
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}
		
		/// <summary>
		/// Gets the business unit activities.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public BusinessUnitActivityResponse GetBusinessUnitActivities(BusinessUnitActivityRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new BusinessUnitActivityResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;
			   
			    try
				{
					var query = Repositories.Core.BusinessUnitActivity; 

					if (request.Criteria.BusinessUnitId.HasValue)
						query = query.Where(x => x.BuId == request.Criteria.BusinessUnitId);
          if (request.Criteria.DaysBack > 0)
              query = query.Where(x => x.ActionedOn <= DateTime.Now && x.ActionedOn >= DateTime.Now.Date.AddDays(request.Criteria.DaysBack > 0 ? request.Criteria.DaysBack * -1 : -30));

          switch (request.Criteria.OrderBy)
          {
              case Constants.SortOrders.ActivityDateAsc:
                  query = query.OrderBy(x => x.ActionedOn);
                  break;

              case Constants.SortOrders.ActivityDateDesc:
                  query = query.OrderByDescending(x => x.ActionedOn);
                  break;

              case Constants.SortOrders.ActivityUsernameAsc:
                  query = query.OrderBy(x => x.UserName);
                  break;

              case Constants.SortOrders.ActivityUsernameDesc:
                  query = query.OrderByDescending(x => x.UserName);
                  break;  

              case Constants.SortOrders.ActivityActionAsc:
                  query = query.OrderBy(x => x.ActionName);
                  break;

              case Constants.SortOrders.ActivityActionDesc:
                  query = query.OrderByDescending(x => x.ActionName);
                  break;

              default:
                  query = query.OrderByDescending(x => x.ActionedOn);
                  break;
          }

					var queryResults = query.Select(result => new BusinessUnitActivityViewDto
					{
						BuId = result.BuId,
						BuName = result.BuName,
						UserName = result.UserName,
						ActionName = result.ActionName,
						ActionedOn = result.ActionedOn,
						JobId = result.JobId,
						JobTitle =result.JobTitle
					});

          switch (request.Criteria.FetchOption)
          {
              case CriteriaBase.FetchOptions.PagedList:
                  response.BusinessUnitActivitiesPaged = new PagedList<BusinessUnitActivityViewDto>(queryResults, request.Criteria.PageIndex, request.Criteria.PageSize);
                  break;

              case CriteriaBase.FetchOptions.List:
                  response.BusinessUnitActivitiesList = new List<BusinessUnitActivityViewDto>(queryResults);
                  break;

              default:
                  response.BusinessUnitActivitiesPaged = new PagedList<BusinessUnitActivityViewDto>(queryResults, request.Criteria.PageIndex, request.Criteria.PageSize);
                  break;
          }
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}
			    return response;
			}
		}
	
		/// <summary>
		/// Gets the business unit placements.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public BusinessUnitPlacementsResponse GetBusinessUnitPlacements(BusinessUnitPlacementsRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new BusinessUnitPlacementsResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var query = Repositories.Core.BusinessUnitPlacements;

          if (request.Criteria.BusinessUnitId.HasValue)
            query = query.Where(x => x.BuId == request.Criteria.BusinessUnitId);

          switch (request.Criteria.OrderBy)
          {
            case Constants.SortOrders.PlacementNameAsc:
              query = query.OrderBy(x => x.LastName);
              query = query.OrderBy(x => x.FirstName);
              break;

            case Constants.SortOrders.PlacementNameDesc:
              query = query.OrderByDescending(x => x.LastName);
              query = query.OrderByDescending(x => x.FirstName);
              break;

            case Constants.SortOrders.PlacementJobTitleAsc:
              query = query.OrderBy(x => x.JobTitle);
              break;

            case Constants.SortOrders.PlacementJobTitleDesc:
              query = query.OrderByDescending(x => x.JobTitle);
              break;

            case Constants.SortOrders.PlacementStartDateAsc:
              // TODO:
              break;

            case Constants.SortOrders.PlacementStartDateDesc:
              // TODO:
              break;

            default:
              query = query.OrderByDescending(x => x.LastName);
              break;
          }

          var queryResults = query.Select(result => new BusinessUnitPlacementsViewDto
          {
            BuId = result.BuId,
            BuName = result.BuName,
            JobSeekerId = result.JobSeekerId,
            JobTitle = result.JobTitle,
            FirstName = result.FirstName,
            LastName = result.LastName
          });


          switch (request.Criteria.FetchOption)
          {
            case CriteriaBase.FetchOptions.PagedList:
							response.BusinessUnitPlacementsViewsPaged = new PagedList<BusinessUnitPlacementsViewDto>(queryResults, request.Criteria.PageIndex, request.Criteria.PageSize);
              break;

            case CriteriaBase.FetchOptions.List:
              response.BusinessUnitPlacementsViews = new List<BusinessUnitPlacementsViewDto>(queryResults);
              break;

            default:
              response.BusinessUnitPlacementsViewsPaged = new PagedList<BusinessUnitPlacementsViewDto>(queryResults, request.Criteria.PageIndex, request.Criteria.PageSize);
              break;
          }
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}
				return response;
			}
		}

		/// <summary>
		/// Gets employer recently placed matches.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public EmployerRecentlyPlacedMatchResponse GetEmployerRecentlyPlacedMatches(EmployerRecentlyPlacedMatchRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new EmployerRecentlyPlacedMatchResponse(request);

				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
          var rowCount = 0;

					var businessUnitName = (request.Criteria.BusinessUnitName.IsNotNullOrEmpty()) ? request.Criteria.BusinessUnitName : "";
					var pageNumber = request.Criteria.PageIndex + 1;
					var filterByOffices = (request.Criteria.OfficeGroup == OfficeGroup.MyOffices);

					List<RecentlyPlacedMatchView> matches;
					
					switch(request.Criteria.OrderBy)
					{
						case Constants.SortOrders.BusinessUnitNameDesc:
							matches = Repositories.Core.GetRecentlyPlacedPersonMatchesOrderByBusinessUnitNameDesc(request.UserContext.UserId, request.Criteria.BusinessUnitId, businessUnitName, filterByOffices, 
																																																		request.Criteria.OfficeId, 6, pageNumber, request.Criteria.PageSize, ref rowCount).ToList();
							break;
						case Constants.SortOrders.RecentPlacementNameAsc:
							matches = Repositories.Core.GetRecentlyPlacedPersonMatchesOrderByPlacedPersonNameAsc(request.UserContext.UserId, request.Criteria.BusinessUnitId, businessUnitName, filterByOffices, 
																																																		request.Criteria.OfficeId, 6, pageNumber, request.Criteria.PageSize, ref rowCount).ToList();
							break;
						case Constants.SortOrders.RecentPlacementNameDesc:
							matches = Repositories.Core.GetRecentlyPlacedPersonMatchesOrderByPlacedPersonNameDesc(request.UserContext.UserId, request.Criteria.BusinessUnitId, businessUnitName, filterByOffices, 
																																																		request.Criteria.OfficeId, 6, pageNumber, request.Criteria.PageSize, ref rowCount).ToList();
							break;
						default:
							matches = Repositories.Core.GetRecentlyPlacedPersonMatchesOrderByBusinessUnitNameAsc(request.UserContext.UserId, request.Criteria.BusinessUnitId, businessUnitName, filterByOffices, 
																																																		request.Criteria.OfficeId, 6, pageNumber, request.Criteria.PageSize, ref rowCount).ToList();
							break;
					}
					
					var distinctRecentlyPlaced = matches.GroupBy(x => x.RecentlyPlacedId).Select(x => x.First()).Select(x => new EmployerRecentlyPlacedView
																																																										{
																																																											Id = x.RecentlyPlacedId,
																																																											EmployerName = x.EmployerName,
																																																											BusinessUnitId = x.BusinessUnitId,
																																																											BusinessUnitName = x.BusinessUnitName,
																																																											PersonId = x.PlacedPersonId,
																																																											CandidateName = x.PlacedCandidateName,
																																																											PlacementDate = x.PlacementDate,
																																																											JobId = x.JobId,
																																																											JobTitle = x.JobTitle,
																																																											EmployerId = x.EmployerId
																																																										}).ToList();

					var pagedPlacements = new PagedList<EmployerRecentlyPlacedView>(distinctRecentlyPlaced, rowCount, request.Criteria.PageIndex, request.Criteria.PageSize, false);

          // We've got all recent placements, now get all matches
          foreach (var employerRecentlyPlacedView in pagedPlacements)
          {
						var matchCollection = matches.Where(x => x.RecentlyPlacedId == employerRecentlyPlacedView.Id).OrderByDescending(x => x.Score).Select(match => new EmployerRecentlyPlacedView.Candidate
																																																																													 {
																																																																														CandidateName = match.MatchedCandidateName,
																																																																														Score = match.Score,
																																																																														HasMatchesToOpenPositions = match.HasMatchesToOpenPositions.GetValueOrDefault(),
																																																																														PersonId = match.MatchedPersonId,
																																																																														Id = match.Id,
																																																																														RecentlyPlacedId = match.RecentlyPlacedId
																																																																													}).ToList();

            employerRecentlyPlacedView.Candidates = matchCollection;
          }

          response.RecentlyPlacedPaged = pagedPlacements;

        }
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}
		
		/// <summary>
		/// Ignores the recently placed matches.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public IgnoreRecentlyPlacedMatchResponse IgnoreRecentlyPlacedMatch(IgnoreRecentlyPlacedMatchRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new IgnoreRecentlyPlacedMatchResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var recentlyPlacedMatch = Repositories.Core.FindById<RecentlyPlacedMatch>(request.RecentlyPlacedMatchId);

					if(recentlyPlacedMatch.IsNotNull())
					{
						// Check if the match is already ignored
						if (!Repositories.Core.RecentlyPlacedMatchesToIgnore.Any(x => x.PersonId == recentlyPlacedMatch.PersonId && x.RecentlyPlacedId == recentlyPlacedMatch.RecentlyPlacedId && x.UserId == request.UserContext.UserId))
						{
							// No ignore entry so add it
							var recentlyPlacedMatchToIgnore = new RecentlyPlacedMatchesToIgnore
							                                  	{
							                                  		PersonId = recentlyPlacedMatch.PersonId,
																										RecentlyPlacedId = recentlyPlacedMatch.RecentlyPlacedId, 
																										IgnoredOn = DateTime.Now, 
																										UserId = request.UserContext.UserId
							                                  	};

							Repositories.Core.Add(recentlyPlacedMatchToIgnore);

							Repositories.Core.SaveChanges(true);
						}

						LogAction(request, ActionTypes.IgnoreRecentlyPlacedMatch, typeof(RecentlyPlaced).Name, recentlyPlacedMatch.RecentlyPlacedId, recentlyPlacedMatch.PersonId, request.UserContext.UserId);
					}
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Allows the recently placed matches.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public AllowRecentlyPlacedMatchResponse AllowRecentlyPlacedMatch(AllowRecentlyPlacedMatchRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new AllowRecentlyPlacedMatchResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var recentlyPlacedMatch = Repositories.Core.FindById<RecentlyPlacedMatch>(request.RecentlyPlacedMatchId);

					if (recentlyPlacedMatch.IsNotNull())
					{
						var ignoreMatch = Repositories.Core.RecentlyPlacedMatchesToIgnore.Where(x => x.PersonId == recentlyPlacedMatch.PersonId && x.RecentlyPlacedId == recentlyPlacedMatch.RecentlyPlacedId && x.UserId == request.UserContext.UserId).FirstOrDefault();

						if (ignoreMatch.IsNotNull())
						{
							Repositories.Core.Remove(ignoreMatch);

							Repositories.Core.SaveChanges(true);

							LogAction(request, ActionTypes.AllowRecentlyPlacedMatch, typeof(RecentlyPlaced).Name, recentlyPlacedMatch.RecentlyPlacedId, recentlyPlacedMatch.PersonId, request.UserContext.UserId);
						}
					}
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

	  /// <summary>
	  /// Searches the employers.
	  /// </summary>
	  /// <param name="request">The request.</param>
	  /// <returns></returns>
    public SearchBusinessUnitsResponse SearchBusinessUnits(SearchBusinessUnitsRequest request)
	  {
	    using (Profiler.Profile(request.LogData(), request))
			{
        var response = new SearchBusinessUnitsResponse(request);

				// Validate request (Search available before log in so don't validate user
				if (!ValidateRequest(request, response, Validate.ClientTag | Validate.SessionId | Validate.License | Validate.AccessToken))
					return response;

				try
				{
          // Simple query for now. Create query class when/if query gets more complicated
				  response.BusinessUnits =
						Repositories.Core.BusinessUnitSearchViews.Where(x => x.PostcodeZip == request.SearchCriteria.ZipCode ||
							x.PostcodeZip.Contains(request.SearchCriteria.ZipCode)).OrderBy(
				      x => x.BusinessUnitName).Select(x => x.AsDto()).ToList();
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the offices.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public OfficeResponse GetOffices(OfficeRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new OfficeResponse(request);

				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					if (request.Criteria.PersonId.IsNotNull())
					{
						var person = Repositories.Core.FindById<Person>(request.Criteria.PersonId);

						if (person.IsNotNull())
						{
							var lookup = Helpers.Lookup.GetLookup(LookupTypes.States).FirstOrDefault(x => x.Key == AppSettings.DefaultStateKey);
							request.Criteria.StateId = lookup.Id;
						}
						else
						{
							response.SetFailure(FormatErrorMessage(request, ErrorTypes.PersonNotFound));
							Logger.Info(request.LogData(), string.Format("The person '{0}' was not found.", request.Criteria.PersonId));

							return response;
						}
					}

					var query = new OfficeQuery(Repositories.Core, request.Criteria);

					switch (request.Criteria.FetchOption)
					{
						case CriteriaBase.FetchOptions.PagedList:
							response.OfficesPaged = query.Query().GetPagedList(x => x.AsDto(), request.Criteria.PageIndex, request.Criteria.PageSize);
							break;
						case CriteriaBase.FetchOptions.List:
							response.Offices = query.Query().Select(x => x.AsDto()).ToList();
							break;
						default:
							var office = query.Query().SingleOrDefault();
							if (office.IsNotNull())
								response.Office = office.AsDto();
							break;
					}
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Saves the office.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public SaveOfficeResponse SaveOffice(SaveOfficeRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new SaveOfficeResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
          Office office;

          if (request.Office.ExternalId.IsNotNullOrEmpty())
          {
            var checkOffice = Repositories.Core.Offices.FirstOrDefault(o => o.ExternalId == request.Office.ExternalId);
            if (checkOffice.IsNotNull() && (request.Office.Id.IsNull() || checkOffice.Id != request.Office.Id))
            {
              response.SetFailure(ErrorTypes.DuplicateOfficeExternalId);
              return response;
            }
          }

					#region Update

					if (request.Office.Id.HasValue)
					{
						office = (from o in Repositories.Core.Offices
											where o.Id == request.Office.Id
											select o).FirstOrDefault();

						if (office.IsNotNull())
						{
							office.OfficeName = request.Office.OfficeName;
							office.Line1 = request.Office.Line1;
							office.Line2 = request.Office.Line2;
							office.PostcodeZip = request.Office.PostcodeZip;
							office.TownCity = request.Office.TownCity;
							office.CountyId = request.Office.CountyId;
							office.StateId = request.Office.StateId;
							office.CountryId = request.Office.CountryId;
							office.AssignedPostcodeZip = request.Office.AssignedPostcodeZip;
              office.OfficeManagerMailbox = request.Office.OfficeManagerMailbox;
						  office.BusinessOutreachMailbox = request.Office.BusinessOutreachMailbox;
							office.ExternalId = request.Office.ExternalId;
							office.DefaultAdministratorId = request.Office.DefaultAdministratorId;
						}
						else
						{
							response.SetFailure(FormatErrorMessage(request, ErrorTypes.OfficeNotFound));
							Logger.Info(request.LogData(), string.Format("The office '{0}' was not found.", request.Office.Id));

							return response;
						}
					}
					
					#endregion

					#region Insert

					else
					{
						office = request.Office.CopyTo();
						Repositories.Core.Add(office);
					}
	
					#endregion

					Repositories.Core.SaveChanges(true);

					response.Office = office.AsDto();

					LogAction(request, ActionTypes.SaveOffice, office);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the office staff members.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public StaffMemberResponse GetOfficeStaffMembers(StaffMemberRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new StaffMemberResponse(request);

				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var staff = Helpers.Office.GetOfficeStaffMembers(request.Criteria);

					switch (request.Criteria.FetchOption)
					{
						case CriteriaBase.FetchOptions.PagedList:
							response.StaffMembersPaged = staff.GetPagedList(x => x.AsModel(), request.Criteria.PageIndex, request.Criteria.PageSize);
							break;

						case CriteriaBase.FetchOptions.List:
              if (request.Criteria.CheckNames.IsNull() || !(bool)request.Criteria.CheckNames)
							{
								var staffMembersList = staff.Select(person => person.AsModel()).ToList();

								response.StaffMembers = staffMembersList;
							}
							else
							{
								// This is for when this method is used on a type ahead control
							  if (request.Criteria.Name.IsNotNullOrEmpty())
							  {
							    var query = staff.Where(s => string.Concat(s.FirstName, " ", s.LastName).ToLower().Contains(request.Criteria.Name.ToLower()));
							    if (request.Criteria.ListSize > 0)
							      query = query.Take(request.Criteria.ListSize);

                  response.StaffMembers = query.Select(s => s.AsModel()).ToList();
							  }
							  else if (request.Criteria.ListSize > 0)
							    response.StaffMembers = staff.Take(request.Criteria.ListSize).Select(s => s.AsModel()).ToList();
							}
							
							break;
					}
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the office managers.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public StaffMemberResponse GetOfficeManagers(StaffMemberRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new StaffMemberResponse(request);

				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var query = new OfficeStaffMemberQuery(Repositories.Core, request.Criteria);

					response.StaffMembers = query.Query().Select(x => x.AsModel()).ToList();
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the staff member count.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public StaffMemberResponse GetStaffMemberCount(StaffMemberRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new StaffMemberResponse(request);

				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var query = new OfficeStaffMemberQuery(Repositories.Core, request.Criteria);

					response.StaffMemberCount = query.Query().Select(x => x.AsDto()).Count();
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Determines whether [has assigned postcodes] [the specified request].
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public OfficeResponse HasAssignedPostcodes(OfficeRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new OfficeResponse(request);

				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
          var assignedZips = Helpers.Office.GetAssignedZips(request.Criteria);
          response.HasAssignedPostcodes = assignedZips.IsNotNull() && assignedZips.Any();
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Saves the person offices.
		/// This method is used to assign multiple offices to one staff member/job seeker
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public PersonOfficeMapperResponse SavePersonOffices(PersonOfficeMapperRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new PersonOfficeMapperResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var existingPersonOffices = (from o in Repositories.Core.PersonOfficeMappers
																			 where o.PersonId == request.PersonId
																			 select o).ToList();

					#region Adding a person's offices

					var existingOfficeIds = existingPersonOffices.Select(x => x.OfficeId).ToList();

					// Get the person offices from the request that don't already exist in the DB i.e. that need to be added
					var addPersonOffices = request.PersonOffices.Where(x => !existingOfficeIds.Contains(x.OfficeId));

					var personOfficesSaved = new List<PersonOfficeMapper>();

					foreach (var addPersonOffice in addPersonOffices)
					{
						Repositories.Core.Add(addPersonOffice.CopyTo());

						personOfficesSaved.Add(addPersonOffice.CopyTo());
					}

					#endregion

					#region Deleting a person's offices

					var officeIds = request.PersonOffices.Select(x => x.OfficeId).ToList();

					// Get the person's offices from the DB that don't exist in the request.  These need deleting
					var deletePersonOffices = existingPersonOffices.Where(x => !officeIds.Contains(x.OfficeId)).ToList();

					foreach (var deletePersonOffice in deletePersonOffices)
					{
						// Delete the PersonOfficeMapper record
						Repositories.Core.Remove(deletePersonOffice);
					}

					var isStatewide = (officeIds.Count == 1 && officeIds[0] == null);
					if (!isStatewide)
					{
						var officeIdList = string.Join(",", officeIds);
						if (!officeIdList.Any())
							officeIdList = "0";

						// The below code is not ideal however I considered both an SP and Mindscape bulk updates.  Updating in an SP is not ideal and I didn't 
						// manage to implement bulk updates with joins without having to use a linq statement with joins anyway.

						// Remove this person from any jobs they are assigned to that are assigned to the Office they are being removed from
						var jobUpdateSql = string.Format(@"UPDATE job 
                                 SET job.AssignedToId = null  
                                 FROM  
                                 [Data.Application.Job] job WITH (NOLOCK) 
                                 INNER JOIN [Data.Application.JobOfficeMapper] jom WITH (NOLOCK) ON job.Id = jom.JobId 
                                 WHERE 
                                 jom.OfficeId NOT IN ({0}) AND  
                                 job.AssignedToId = {1}", officeIdList, request.PersonId.ToString());

						Repositories.Core.ExecuteNonQuery(jobUpdateSql);

						// Remove this person from any job seekers they are assigned to that are assigned to the Office they are being removed from
						var jobSeekerUpdateSql = string.Format(@"UPDATE js
                                       SET js.AssignedToId = null
                                       FROM [Data.Application.Person] js WITH (NOLOCK)
                                       INNER JOIN [Data.Application.PersonOfficeMapper] pom WITH (NOLOCK) ON js.Id = pom.PersonId
                                       INNER JOIN [Data.Application.User] u WITH (NOLOCK) ON js.Id = u.PersonId
                                       WHERE pom.OfficeId NOT IN ({0}) AND
                                       js.AssignedToId = {1} AND 
                                       u.UserType = {2}", officeIdList, request.PersonId.ToString(), Convert.ToInt16(UserTypes.Career | UserTypes.Explorer).ToString());

						Repositories.Core.ExecuteNonQuery(jobSeekerUpdateSql);
					}

					#endregion

					// If the staff member has been updated to be statewide then remove them as default administrator for any offices
					if (AppSettings.IntegrationClient == IntegrationClient.EKOS && isStatewide)
					{
						var staffIsDefaultAdmin = Repositories.Core.Offices.Where(x => x.DefaultAdministratorId.Equals(request.PersonId));

						if (staffIsDefaultAdmin.Any())
						{
							var changes = new Dictionary<string, object> {{"DefaultAdministratorId", null}};

							var updateQuery = new Query(typeof(Office), Entity.Attribute("DefaultAdministratorId") == request.PersonId);
							Repositories.Core.Update(updateQuery, changes);
						}
					}

					Repositories.Core.SaveChanges(true);

					personOfficesSaved.ForEach(ed => LogAction(request, ActionTypes.AddPersonOffice, typeof(PersonOfficeMapper).Name, ed.Id, request.UserContext.UserId));
					deletePersonOffices.ForEach(ed => LogAction(request, ActionTypes.DeletePersonOffice, typeof(PersonOfficeMapper).Name, ed.Id, request.UserContext.UserId));
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Assigns the office staff.
		/// This method is used to assign multiple staff members to one office
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public PersonOfficeMapperResponse AssignOfficeStaff(PersonOfficeMapperRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new PersonOfficeMapperResponse(request);

				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var personOfficesSaved = new List<PersonOfficeMapper>();
					var deletePersonOffices = new List<PersonOfficeMapper>();

					// Get distinct list of office Ids
					var officeIds = request.PersonOffices.Select(x => x.OfficeId).Distinct();

					// Get a distinct list of person Ids
					var personIds = request.PersonOffices.Select(x => x.PersonId).Distinct();

                    if (officeIds.Count() != 1)
                        return response;

                    if (!personIds.Any())
                        return response;

					var officeId = officeIds.FirstOrDefault();

					var existingAssignedStaff = (from o in Repositories.Core.PersonOfficeMappers
																			 where o.OfficeId.Equals(officeId) && o.Person.User.UserType.Equals(UserTypes.Assist)
																			 select o).ToList();

					var currentStaffIds = existingAssignedStaff.Select(x => x.PersonId).ToList();

					#region New people to assign to the office

					// Determine the people in the request that aren't already added to this office on the DB
					var addPersonOffices = request.PersonOffices.Where(x => !currentStaffIds.Contains(x.PersonId));

					foreach (var addPersonOffice in addPersonOffices)
					{
						var pom = addPersonOffice.CopyTo();
                        if (pom.PersonId != 0)
                        {
                            Repositories.Core.Add(pom);
                            personOfficesSaved.Add(pom);
                            currentStaffIds.Add(pom.PersonId);
                        }
					}

					#endregion
					
					#region People to be removed from the office

					// Get a list of people from the DB that are not contained in the request 
					var removeStaff = existingAssignedStaff.Where(x => x.OfficeId.Equals(officeId) && !personIds.Contains(x.PersonId)).ToList();

					if (removeStaff.Count > 0)
					{
						foreach (var staff in removeStaff)
						{
							// Delete the PersonOfficeMapper record
							Repositories.Core.Remove(staff);
						}

						var staffIds = removeStaff.Select(x => x.PersonId).ToList();

						var staffIdList = string.Join(",", staffIds);
						if (!staffIdList.Any())
							staffIdList = "0";

						// The below code is not ideal however I considered both an SP and Mindscape bulk updates.  Updating in an SP is not ideal and I didn't 
						// manage to implement bulk updates with joins without having to use a linq statement with joins anyway.

						// Remove this person from any jobs they are assigned to that are NOT assigned to the Office they are being removed from
						var jobUpdateSql = string.Format(@"UPDATE job 
                              SET job.AssignedToId = null  
                              FROM  
                              [Data.Application.Job] job WITH (NOLOCK) 
                              INNER JOIN [Data.Application.JobOfficeMapper] jom WITH (NOLOCK) ON job.Id = jom.JobId 
                              WHERE 
                              jom.OfficeId != {0} AND  
                              job.AssignedToId IN ({1})", officeId, staffIdList);

						Repositories.Core.ExecuteNonQuery(jobUpdateSql);

						// Remove this person from any job seekers they are assigned to that are NOT assigned to the Office they are being removed from
						var jobSeekerUpdateSql = string.Format(@"UPDATE js
                                    SET js.AssignedToId = null
                                    FROM [Data.Application.Person] js WITH (NOLOCK)
                                    INNER JOIN [Data.Application.PersonOfficeMapper] pom WITH (NOLOCK) ON js.Id = pom.PersonId
                                    INNER JOIN [Data.Application.User] u WITH (NOLOCK) ON js.Id = u.PersonId
                                    WHERE pom.OfficeId != {0} AND
                                    js.AssignedToId IN ({1}) AND 
                                    u.UserType = {2}", officeId, staffIdList, Convert.ToInt16(UserTypes.Career | UserTypes.Explorer).ToString());

						Repositories.Core.ExecuteNonQuery(jobSeekerUpdateSql);
					}

					#endregion

					Repositories.Core.SaveChanges(true);

					personOfficesSaved.ForEach(ed => LogAction(request, ActionTypes.AddPersonOffice, typeof(PersonOfficeMapper).Name, ed.Id, request.UserContext.UserId));
					deletePersonOffices.ForEach(ed => LogAction(request, ActionTypes.DeletePersonOffice, typeof(PersonOfficeMapper).Name, ed.Id, request.UserContext.UserId));
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the state of the manager.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public GetManagerStateResponse GetManagerState(GetManagerStateRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new GetManagerStateResponse(request);

				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var stateId = (from pom in Repositories.Core.PersonOfficeMappers 
															 where pom.PersonId == request.PersonId &&
															 pom.StateId != null
															 select pom.StateId).FirstOrDefault();

					if (stateId.IsNotNull())
						response.StateId = Convert.ToInt64(stateId);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Determines whether the specified request has employers.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public OfficeEmployersResponse GetOfficeEmployers(OfficeEmployersRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new OfficeEmployersResponse(request);

				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					response.OfficeEmployers = Helpers.Office.GetOfficeEmployers(request.OfficeId);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Determines whether the specified request has employers.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public OfficeJobSeekersResponse GetOfficeJobSeekers(OfficeJobSeekersRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new OfficeJobSeekersResponse(request);

				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					response.OfficeJobSeekers = Helpers.Office.GetOfficeJobSeekers(request.OfficeId);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Updates the employer assigned office.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public UpdateEmployerAssignedOfficeResponse UpdateEmployerAssignedOffice(UpdateEmployerAssignedOfficeRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new UpdateEmployerAssignedOfficeResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var existingEmployerOffices = (from e in Repositories.Core.EmployerOfficeMappers
																				 where e.EmployerId == request.EmployerId
																			 select e).ToList();

					#region Adding a employer's offices

					var existingOfficeIds = existingEmployerOffices.Select(x => x.OfficeId).ToList();

					// Get the employer offices from the request that don't already exist in the DB i.e. that need to be added
					var addEmployerOffices = request.EmployerOffices.Where(x => !existingOfficeIds.Contains(x.OfficeId));

					var employerOfficesSaved = new List<EmployerOfficeMapper>();

					foreach (var addEmployerOffice in addEmployerOffices)
					{
						Repositories.Core.Add(addEmployerOffice.CopyTo());

						employerOfficesSaved.Add(addEmployerOffice.CopyTo());
					}

					#endregion

					#region Deleting a employer's offices

					var officeIds = request.EmployerOffices.Select(x => x.OfficeId).ToList();

					// Get the employer's offices from the DB that don't exist in the request.  These need deleting
					var deleteEmployerOffices = existingEmployerOffices.Where(x => !officeIds.Contains(x.OfficeId)).ToList();

					deleteEmployerOffices.ForEach(x => Repositories.Core.Remove(x));

					#endregion

          var defaultOfficeId = Repositories.Core.Offices.FirstOrDefault(x => (x.DefaultType & OfficeDefaultType.Employer) == OfficeDefaultType.Employer).Id;

					var defaultOffice = request.EmployerOffices.FirstOrDefault(x => x.OfficeId == defaultOfficeId);

					if (defaultOffice.IsNotNull())
					{
						Repositories.Core.Remove(defaultOffice.CopyTo());
					}

					Repositories.Core.SaveChanges(true);

					employerOfficesSaved.ForEach(ed => LogAction(request, ActionTypes.AddEmployerOffice, typeof(EmployerOfficeMapper).Name, ed.Id, request.UserContext.UserId));
					deleteEmployerOffices.ForEach(ed => LogAction(request, ActionTypes.DeleteEmployerOffice, typeof(EmployerOfficeMapper).Name, ed.Id, request.UserContext.UserId));
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Updates the default office.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public UpdateDefaultOfficeResponse UpdateDefaultOffice(UpdateDefaultOfficeRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new UpdateDefaultOfficeResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
          if (AppSettings.EnableDefaultOfficePerRecordType && request.OfficeDefaultType == OfficeDefaultType.None)
              throw new Exception("Invalid default office type");

          var defaultType = request.OfficeDefaultType ?? OfficeDefaultType.All;

          // Find existing default and deselect it as the default
          var currentDefaultOffices = Repositories.Core.Offices.Where(x => (x.DefaultType & defaultType) != 0).ToList();
          currentDefaultOffices.ForEach(current => current.DefaultType = (current.DefaultType & ~defaultType));

          var newDefaultOffice = Repositories.Core.Offices.SingleOrDefault(x => x.Id == request.DefaultOfficeId);
          if (newDefaultOffice.IsNull())
          {
            response.SetFailure(FormatErrorMessage(request, ErrorTypes.OfficeNotFound));
            Logger.Error(request.LogData(), "Office not found");
            return response;
          }

          newDefaultOffice.DefaultType = (newDefaultOffice.DefaultType | defaultType);

				  Repositories.Core.SaveChanges();

          currentDefaultOffices.ForEach(current => LogAction(request, ActionTypes.DeselectedOfficeAsDefault, typeof(Office).Name, current.Id, (long?)request.OfficeDefaultType));
          LogAction(request, ActionTypes.SelectedOfficeAsDefault, typeof(Office).Name, newDefaultOffice.Id, (long?)request.OfficeDefaultType);
				}

				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.IntegrationCandidateUpdateSystemDefaultsFailure), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the office job postings.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public OfficeJobsResponse GetOfficeJobs(OfficeJobsRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new OfficeJobsResponse(request);

				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					response.OfficeJobs = Helpers.Office.GetOfficeJobOrders(request.OfficeId);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Updates the office assigned zips.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public UpdateOfficeAssignedZipsResponse UpdateOfficeAssignedZips(UpdateOfficeAssignedZipsRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new UpdateOfficeAssignedZipsResponse(request);

				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					// Get the office records that need updating
					var offices = Repositories.Core.Offices.Where(x => request.OfficeIds.Contains(x.Id));
					var officesSaved = new List<Office>();

					foreach (var office in offices)
					{
						foreach (var zip in request.Zips)
						{
							if (zip.IsNotNullOrEmpty())
							{
								var zipIndex = -1;
								var prefix = string.Empty;
								var suffix = string.Empty;

								if (office.AssignedPostcodeZip.IsNotNullOrEmpty())
								{
									zipIndex = office.AssignedPostcodeZip.IndexOf(zip);

									if (zipIndex > -1)
									{
										if (zipIndex + zip.Length < office.AssignedPostcodeZip.Length)
											suffix = office.AssignedPostcodeZip.Substring(zipIndex + zip.Length, 1);

										if (zipIndex - 1 > 0)
											prefix = office.AssignedPostcodeZip.Substring(zipIndex - 1, 1);
									}
								}

								// If the zip doesn't already exist in the assigned Zip string then add it to the end of the list
								if (zipIndex == -1 || (prefix != "," && prefix != string.Empty) || (suffix != "," && suffix != string.Empty))
								{
									office.AssignedPostcodeZip = office.AssignedPostcodeZip.IsNotNullOrEmpty() ? String.Concat(office.AssignedPostcodeZip, ",", zip) : zip;

									if (!officesSaved.Contains(office))
										officesSaved.Add(office);
								}
							}
						}

						if (office.AssignedPostcodeZip.IsNotNullOrEmpty())
						{
							if (office.AssignedPostcodeZip.Substring(0, 1) == ",")
								office.AssignedPostcodeZip = office.AssignedPostcodeZip.Substring(1);

							if (office.AssignedPostcodeZip.Substring(office.AssignedPostcodeZip.Length - 1, 1) == ",")
								office.AssignedPostcodeZip = office.AssignedPostcodeZip.Substring(0, office.AssignedPostcodeZip.Length - 1);
						}
					}

					Repositories.Core.SaveChanges();

					response.Offices = offices.Select(x => x.AsDto()).ToList();

					officesSaved.ForEach(ed => LogAction(request, ActionTypes.UpdatedOfficeAssignedZips, typeof(Office).Name, ed.Id, request.UserContext.UserId));
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the spidered employers.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public SpideredEmployerResponse GetSpideredEmployers(SpideredEmployerRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new SpideredEmployerResponse(request);

				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					response.SpideredEmployersPaged = Repositories.LiveJobs.GetEmployers(request.Criteria);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Gets the spidered employers with good matches.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public SpideredEmployerResponse GetSpideredEmployersWithGoodMatches(SpideredEmployerRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new SpideredEmployerResponse(request);

				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var rowCount = 0;

					var employerName = (request.Criteria.EmployerName.IsNotNullOrEmpty()) ? request.Criteria.EmployerName : "";
					var minimumDate = (request.Criteria.PostingAge > 0) ? DateTime.Today.AddDays(1 - request.Criteria.PostingAge.GetValueOrDefault()) : DateTime.Today.AddDays(-30);

					var pageNumber = request.Criteria.PageIndex + 1;

					IQueryable<GetSpideredEmployersWithGoodMatchesResult> spideredEmployers;

					switch (request.Criteria.OrderBy)
					{
						case Constants.SortOrders.EmployerNameAsc:
							spideredEmployers = Repositories.Core.GetSpideredEmployersWithGoodMatchesOrderByEmployerNameAsc(request.UserContext.UserId, employerName, request.Criteria.MinimumPostingCount, minimumDate,
							                                                                                                 pageNumber, request.Criteria.PageSize, ref rowCount);
							break;
						case Constants.SortOrders.EmployerNameDesc:
							spideredEmployers = Repositories.Core.GetSpideredEmployersWithGoodMatchesOrderByEmployerNameDesc(request.UserContext.UserId, employerName, request.Criteria.MinimumPostingCount, minimumDate,
																																																							 pageNumber, request.Criteria.PageSize, ref rowCount);
							break;
						case Constants.SortOrders.JobTitleAsc:
							spideredEmployers = Repositories.Core.GetSpideredEmployersWithGoodMatchesOrderByJobTitleAsc(request.UserContext.UserId, employerName, request.Criteria.MinimumPostingCount, minimumDate,
																																																							 pageNumber, request.Criteria.PageSize, ref rowCount);
							break;
						case Constants.SortOrders.JobTitleDesc:
							spideredEmployers = Repositories.Core.GetSpideredEmployersWithGoodMatchesOrderByJobTitleDesc(request.UserContext.UserId, employerName, request.Criteria.MinimumPostingCount, minimumDate,
																																																							 pageNumber, request.Criteria.PageSize, ref rowCount);
							break;
						case Constants.SortOrders.PostingDateAsc:
							spideredEmployers = Repositories.Core.GetSpideredEmployersWithGoodMatchesOrderByPostingDateAsc(request.UserContext.UserId, employerName, request.Criteria.MinimumPostingCount, minimumDate,
																																																							 pageNumber, request.Criteria.PageSize, ref rowCount);
							break;
						case Constants.SortOrders.PostingDateDesc:
							spideredEmployers = Repositories.Core.GetSpideredEmployersWithGoodMatchesOrderByPostingDateDesc(request.UserContext.UserId, employerName, request.Criteria.MinimumPostingCount, minimumDate,
																																																							 pageNumber, request.Criteria.PageSize, ref rowCount);
							break;
						default:
							spideredEmployers = Repositories.Core.GetSpideredEmployersWithGoodMatchesOrderByPostingCountDesc(request.UserContext.UserId, employerName, request.Criteria.MinimumPostingCount, minimumDate,
																																																							 pageNumber, request.Criteria.PageSize, ref rowCount);
							break;
					}

					var spideredEmployerModels = new PagedList<SpideredEmployerModel>();
					spideredEmployerModels.AddRange(spideredEmployers.Select(spideredEmployer => spideredEmployer.ToSpideredEmployerModel()));

					response.SpideredEmployersPaged = new PagedList<SpideredEmployerModel>(spideredEmployerModels, rowCount, request.Criteria.PageIndex, request.Criteria.PageSize, false);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}

		/// <summary>
		/// Sets the current office for person.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public SetCurrentOfficeForPersonResponse SetCurrentOfficeForPerson(SetCurrentOfficeForPersonRequest request)
		{
			var response = new SetCurrentOfficeForPersonResponse(request);

			if (!ValidateRequest(request, response, Validate.All))
				return response;

			try
			{
				var currentOfficeId = request.CurrentOfficeId;
				var personId = request.PersonId;

				var personsCurrentOffice = new PersonsCurrentOffice {OfficeId = currentOfficeId, PersonId = personId, StartTime = DateTime.Now};
				Repositories.Core.Add(personsCurrentOffice);

				Repositories.Core.SaveChanges();
				response.Suceeded = true;
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
				response.Suceeded = false;
			}

			return response;
		}

		/// <summary>
		/// Gets the current office for person.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		/// <exception cref="System.ArgumentException">Could not find current office.</exception>
		public GetCurrentOfficeForPersonResponse GetCurrentOfficeForPerson(GetCurrentOfficeForPersonRequest request)
		{
			var response = new GetCurrentOfficeForPersonResponse(request);

			if (!ValidateRequest(request, response, Validate.All))
				return response;

			try
			{
				var personId = request.PersonId;

				var currentOffice = Repositories.Core.PersonsCurrentOffices
					.Where(x => x.PersonId == personId)
					.OrderByDescending(x => x.StartTime)
					.FirstOrDefault();

				if (currentOffice.IsNotNull())
				{
					response.CurrentOffice = new CurrentOfficeModel
					{
						OfficeId = currentOffice.OfficeId,
						StartTime = currentOffice.StartTime,
						PersonId = currentOffice.PersonId
					};
				}
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}

			return response;
		}

		/// <summary>
		/// Determines whether [is office assigned] [the specified request].
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public IsOfficeAssignedResponse IsOfficeAssigned(IsOfficeAssignedRequest request)
		{
			var response = new IsOfficeAssignedResponse(request);

			if (!ValidateRequest(request, response, Validate.All))
				return response;

			try
			{
				response.Errors = new List<ErrorTypes>();

				var office = Repositories.Core.FindById<Office>(request.OfficeId);

				if (office.IsNull())
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.OfficeNotFound));
					Logger.Info(request.LogData(), string.Format("OfficeId '{0}' not found.", request.OfficeId));
				}

				#region Validation

				#region Test if office has staff members

				var hasStaffMembers =
					Helpers.Office.GetOfficeStaffMembers(new StaffMemberCriteria
						{FetchOption = CriteriaBase.FetchOptions.List, OfficeIds = new List<long?> {request.OfficeId}}).Any();

				if (hasStaffMembers)
					response.Errors.Add(ErrorTypes.OfficeHasStaffMembers);

				#endregion

				#region Test if office has job seekers

				var hasJobSeekers = Helpers.Office.GetOfficeJobSeekers(request.OfficeId).Any();

				if (hasJobSeekers)
					response.Errors.Add(ErrorTypes.OfficeHasJobSeekers);

				#endregion

				#region Test if office has job orders

				var hasJobOrders = Helpers.Office.GetOfficeJobOrders(request.OfficeId).Any();

				if (hasJobOrders)
					response.Errors.Add(ErrorTypes.OfficeHasJobOrders);

				#endregion

				#region Test if office has employers

				var hasEmployers = Helpers.Office.GetOfficeEmployers(request.OfficeId).Any();

				if (hasEmployers)
					response.Errors.Add(ErrorTypes.OfficeHasEmployers);

				#endregion

				#region Test if office has assigned zips codes

				var hasAssignedZips = Helpers.Office.GetAssignedZips(new OfficeCriteria{OfficeId = request.OfficeId}).IsNotNullOrEmpty();

				if (hasAssignedZips)
					response.Errors.Add(ErrorTypes.OfficeHasAssignedZips);

				#endregion

				#endregion

				if (response.Errors.Count == 0)
					response.IsAssigned = false;
				else
				{
					response.IsAssigned = true;
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.OfficeIsAssigned));
					Logger.Info(request.LogData(), string.Format("{0} failed validation because it is assigned", office.OfficeName));
				}
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}

			return response;
		}

		/// <summary>
		/// Activates or Deactivates the office.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public ActivateDeactivateOfficeResponse ActivateDeactivateOffice(ActivateDeactivateOfficeRequest request)
		{
			var response = new ActivateDeactivateOfficeResponse(request);

			if (!ValidateRequest(request, response, Validate.All))
				return response;

			try
			{
				var office = Repositories.Core.FindById<Office>(request.OfficeId);

				if (office.IsNull())
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.OfficeNotFound));
					Logger.Info(request.LogData(), string.Format("OfficeId '{0}' not found.", request.OfficeId));
				}

				if (request.Deactivate)
				{
					if (office.InActive)
						response.ErrorType = ErrorTypes.OfficeIsNotActive;
				}
				else
				{
					if (office.InActive.IsNotNull() && !office.InActive)
						response.ErrorType = ErrorTypes.OfficeIsNotInactive;
				}

				if (response.ErrorType.IsNotNull())
				{
					office.InActive = request.Deactivate;

					Repositories.Core.SaveChanges();
					response.Office = office.AsDto();
				}
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}

			return response;
		}

    /// <summary>
    /// Sets whether the employer is preferred or not
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    public EmployerPreferredResponse SetEmployerPreferredStatus(EmployerPreferredRequest request)
    {
      var response = new EmployerPreferredResponse(request);

      if (!ValidateRequest(request, response, Validate.All))
        return response;

      try
      {
        var businessUnit = Repositories.Core.FindById<BusinessUnit>(request.BusinessUnitId);

        if (businessUnit.IsNull())
        {
          response.SetFailure(FormatErrorMessage(request, ErrorTypes.BusinessUnitNotFound));
          Logger.Info(request.LogData(), string.Format("BusinessUnit with Id '{0}' not found.", request.BusinessUnitId));
        }

        if (businessUnit.IsPreferred != request.IsPreferred)
        {
          businessUnit.IsPreferred = request.IsPreferred;
          Repositories.Core.SaveChanges(true);

          response.PreferredStatusUpdated = true;
        }
        else
        {
          response.PreferredStatusUpdated = false;
        }
      }
      catch (Exception ex)
      {
        response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
        Logger.Error(request.LogData(), response.Message, response.Exception);
      }

      return response;
    }

		/// <summary>
		/// Gets the employer account types.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public GetEmployerAccountTypesResponse GetEmployerAccountTypes(GetEmployerAccountTypesRequest request)
		{
			var response = new GetEmployerAccountTypesResponse(request);

			if (!ValidateRequest(request, response, Validate.All))
				return response;

			try
			{
				if (request.LensPostingIds.IsNotNullOrEmpty())
				{
					var lensPostingIds = request.LensPostingIds.Where(s => !string.IsNullOrWhiteSpace(s)).Distinct().ToList();
					
					response.EmployerAccountTypes = //new Dictionary<string, long?> { { "ABC123_1847DFD03B4D4127A023676B2D671F94", 1531216 } };
						(from p in Repositories.Core.Postings
						 join j in Repositories.Core.Jobs on p.JobId equals j.Id
						 join e in Repositories.Core.Employers on j.EmployerId equals e.Id
						 where lensPostingIds.Contains(p.LensPostingId)
             select new { p.LensPostingId, e.AccountTypeId }).ToDictionary(x => x.LensPostingId, x => x.AccountTypeId);
				}
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}

			return response;
		}
		
		/// <summary>
		/// Gets the FEIN.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public GetFEINResponse GetFEIN(GetFEINRequest request)
		{
			var response = new GetFEINResponse(request);

			if (!ValidateRequest(request, response, Validate.All))
				return response;

			try
			{
				if (request.BusinessUnitId.HasValue)
				{
					var employerId = Repositories.Core.BusinessUnits.Where(x => x.Id == request.BusinessUnitId).Select(x => x.EmployerId).FirstOrDefault();
					
					if (employerId > 0)
						response.FEIN = Repositories.Core.Employers.Where(x => x.Id == employerId).Select(x => x.FederalEmployerIdentificationNumber).FirstOrDefault();
					else
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.BusinessUnitNotFound));
						Logger.Info(request.LogData(), string.Format("Business Unit with Id '{0}' not found.", request.BusinessUnitId));
					}
				}
				else
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.CriteriaRequired));
					Logger.Info(request.LogData(), "Business Unit Id required.");
				}
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}

			return response;
		}

		/// <summary>
		/// Validates the fein change.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public ValidateFEINChangeResponse ValidateFEINChange(ValidateFEINChangeRequest request)
		{
			var response = new ValidateFEINChangeResponse(request);

			if (!ValidateRequest(request, response, Validate.All))
				return response;

			try
			{
				response.ValidationResult = Helpers.Employer.ValidateFEINChange(request.BusinessUnitId, request.NewFEIN);
			}
			catch (ArgumentNullException ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.ArgumentNull, ex.Message));
				Logger.Info(request.LogData(), ex.Message);
			}
			catch (ArgumentException ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.ArgumentInvalid, ex.Message));
				Logger.Info(request.LogData(), ex.Message);
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}

			return response;
		}

		/// <summary>
		/// Saves the fein change.
		/// </summary>
		/// <param name="request">The request.</param>
		public SaveFEINChangeResponse SaveFEINChange(SaveFEINChangeRequest request)
		{
			var response = new SaveFEINChangeResponse(request);

			if (!ValidateRequest(request, response, Validate.All))
				return response;

			try
			{

				if (request.LockVersion.HasValue)
				{
					var employer = (from bu in Repositories.Core.BusinessUnits
													join emp in Repositories.Core.Employers
														on bu.EmployerId equals emp.Id
													where bu.Id == request.BusinessUnitId
													select emp).FirstOrDefault();

					if (employer.IsNotNull() && employer.LockVersion != request.LockVersion.Value)
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.LockVersionOutOfDate));
						return response;
					}
				}

				response.ActionCarriedOut = Helpers.Employer.SaveFEINChange(request.BusinessUnitId, request.NewFEIN);
			}
			catch (ArgumentNullException ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.ArgumentNull, ex.Message));
				Logger.Info(request.LogData(), ex.Message);
			}
			catch (ArgumentException ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.ArgumentInvalid, ex.Message));
				Logger.Info(request.LogData(), ex.Message);
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}

			return response;
		}

		/// <summary>
		/// Updates the primary business unit.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public UpdatePrimaryBusinessUnitResponse UpdatePrimaryBusinessUnit(UpdatePrimaryBusinessUnitRequest request)
		{
			var response = new UpdatePrimaryBusinessUnitResponse(request);

			if (!ValidateRequest(request, response, Validate.All))
				return response;

			try
			{
				Helpers.Employer.UpdatePrimaryBusinessUnit(request.CurrentPrimaryBusinessUnitId, request.NewPrimaryBusinessUnitId);
			}
			catch (ArgumentNullException ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.ArgumentNull, ex.Message));
				Logger.Info(request.LogData(), ex.Message);
			}
			catch (ArgumentException ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.InvalidBusinessUnit, ex.Message));
				Logger.Info(request.LogData(), ex.Message);
			}
			catch (Exception)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown));
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}

			return response;
		}

		/// <summary>
		/// Gets the hiring manager office email.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public GetHiringManagerOfficeEmailResponse GetHiringManagerOfficeEmail(GetHiringManagerOfficeEmailRequest request)
		{
			var response = new GetHiringManagerOfficeEmailResponse(request);

			if (!ValidateRequest(request, response, Validate.All))
				return response;

			try
			{
				if (request.PersonId.IsNotNull())
				{
					response.Email = Helpers.Office.GetHiringManagerOfficeEmail(request.PersonId);
				}
			}
			catch (Exception)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown));
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}

			return response;
		}

		/// <summary>
		/// Determines whether [is office administrator] [the specified request].
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public OfficeResponse IsOfficeAdministrator(OfficeRequest request)
		{
			var response = new OfficeResponse(request);

			if (!ValidateRequest(request, response, Validate.All))
				return response;

			try
			{
				if (request.Criteria.PersonId.IsNotNull() && request.Criteria.OfficeId.IsNotNull())
				{
					var office = Repositories.Core.FindById<Office>(request.Criteria.OfficeId);

					if (office.IsNotNull())
						response.IsDefaultAdministrator = office.DefaultAdministratorId.Equals(request.Criteria.PersonId);
					else
					{
						response.SetFailure(FormatErrorMessage(request, ErrorTypes.OfficeNotFound));
						Logger.Info(request.LogData(), string.Format("OfficeId '{0}' not found.", request.Criteria.OfficeId));
					}
				}
				else if (request.Criteria.PersonId.IsNotNull() && request.Criteria.OfficeId.IsNull())
				{
					response.Offices = Repositories.Core.Offices.Where(x => x.DefaultAdministratorId.Equals(request.Criteria.PersonId)).Select(x => x.AsDto()).ToList(); ;
					response.IsDefaultAdministrator = response.Offices.Any();
				}
			}
			catch (Exception ex)
			{
				response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
				Logger.Error(request.LogData(), response.Message, response.Exception);
			}

			return response;
		}

		/// <summary>
		/// Creates the business unit homepage alert.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public CreateBusinessUnitHomepageAlertResponse CreateBusinessUnitHomepageAlert(CreateBusinessUnitHomepageAlertRequest request)
		{
			using (Profiler.Profile(request.LogData(), request))
			{
				var response = new CreateBusinessUnitHomepageAlertResponse(request);

				// Validate request
				if (!ValidateRequest(request, response, Validate.All))
					return response;

				try
				{
					var person = Repositories.Core.FindById<Person>(request.UserContext.PersonId);

					foreach (var homepageAlert in request.HomepageAlerts)
					{
						CreateAlertMessage(Constants.AlertMessageKeys.BusinessUnitMessage, homepageAlert.IsSystemAlert,
							homepageAlert.BusinessUnitId, homepageAlert.ExpiryDate, MessageAudiences.BusinessUnit, MessageTypes.General, null,
							person.FirstName,
							person.LastName,
							LocaliseOrDefault(request, "ReminderCreatedOnDate.Format", "{0:MMM d, yyyy}", DateTime.Now),
							homepageAlert.Message);
					}

					LogAction(request, ActionTypes.CreateBusinessUnitHomepageAlerts, null);
				}
				catch (Exception ex)
				{
					response.SetFailure(FormatErrorMessage(request, ErrorTypes.CreateBusinessUnitHomepageAlertsFailure), ex);
					Logger.Error(request.LogData(), response.Message, response.Exception);
				}

				return response;
			}
		}
	}
}
