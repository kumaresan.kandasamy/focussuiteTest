﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;

using Focus.Core;
using Focus.Services.Messages;
using Focus.Services.Repositories;
using Framework.Messaging;
using Framework.Messaging.Entities;
using HALens.Extensions;

#endregion

namespace Focus.Services.Helpers
{
	#region MessagingHelper Class

	public class MessagingHelper : HelperBase, IMessagingHelper
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="MessagingHelper"/> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		public MessagingHelper(IRuntimeContext runtimeContext) : base(runtimeContext) { }

		/// <summary>
		/// Performs some initialisation
		/// </summary>
		public void Initialise()
		{
			var messageBus = new MessageBus();
			messageBus.UpdateScheduledMessages(Constants.SystemVersion);
		}

		/// <summary>
		/// Publishes the specified message to the bus.
		/// </summary>
		/// <typeparam name="TMessage">The type of the message.</typeparam>
		/// <param name="message">The message.</param>
		/// <param name="messagePriority">The message priority.</param>
		/// <param name="updateIfExists">Update any existing message with matching entity key and type if not yet processed</param>
		/// <param name="processOn">An optional future date to process the message.</param>
		/// <param name="entityName">An optional name for the entity being processed in the message.</param>
		/// <param name="entityKey">The optional key for the entity being processed in the message.</param>
		public void Publish<TMessage>(TMessage message, MessagePriority? messagePriority = null, bool updateIfExists = false, DateTime? processOn = null, string entityName = null, long? entityKey = null) where TMessage : class, IMessage
		{	
			// Check if Integration Message is implemented
			var integrationmessage = message as IntegrationRequestMessage;

			if (integrationmessage != null)
			{
				var integrationRepository = new IntegrationRepository(RuntimeContext);

				if (!integrationRepository.IsImplemented(integrationmessage.IntegrationPoint))
					return;
			}

			var messageBus = new MessageBus();

			// Add the version stamp to the message
			message.Version = Constants.SystemVersion;

			messageBus.Publish(message, messagePriority, updateIfExists, processOn: processOn, entityName: entityName, entityKey: entityKey);
		}

		/// <summary>
		/// Processes the messages.
		/// </summary>
		/// <param name="messageTypes">Optional message types to target.</param>
		/// <param name="excludeMessageTypes">Optional message types to exclude.</param>
		public void ProcessMessages(List<Guid> messageTypes = null, List<Guid> excludeMessageTypes = null)
		{
			var messageBusHandler = new MessageBusHandler();
			messageBusHandler.Process(version: Constants.SystemVersion, messageTypes: messageTypes, excludeMessageTypes: excludeMessageTypes);
		}

		/// <summary>
		/// Schedules a message to run at a set interval
		/// </summary>
		/// <param name="message">The message to schedule</param>
		/// <param name="schedulePlan">The planned schedule to run</param>
		/// <param name="messagePriority">The message priority.</param>
		public void ScheduleMessage<TMessage>(TMessage message, MessageSchedulePlan schedulePlan, MessagePriority? messagePriority = MessagePriority.Medium) where TMessage : class, IMessage
		{
			var messageBus = new MessageBus();

			// Add the version stamp to the message
			message.Version = Constants.SystemVersion;

			messageBus.ScheduleMessage(message, schedulePlan, messagePriority);
		}

		/// <summary>
		/// Gets a MessageType record for a message
		/// </summary>
		/// <param name="message">The message whose type is required</param>
		/// <returns>The MessageType record</returns>
		public MessageTypeEntity GetMessageType<TMessage>(TMessage message) where TMessage : class, IMessage
		{
			var messageBus = new MessageBus();

			return messageBus.GetMessageType(message);
		}
	}

	#endregion

	#region MessagingHelper Interface

	public interface IMessagingHelper
	{
		/// <summary>
		/// Performs some initialisation
		/// </summary>
		void Initialise();

		/// <summary>
		/// Publishes the specified message.
		/// </summary>
		/// <typeparam name="TMessage">The type of the message.</typeparam>
		/// <param name="message">The message.</param>
		/// <param name="messagePriority">The message priority.</param>
		/// <param name="updateIfExists">Update any existing message with matching entity key and type if not yet processed</param>
		/// <param name="processOn">An optional future date to process the message.</param>
		/// <param name="entityName">An optional name for the entity being processed in the message.</param>
		/// <param name="entityKey">The optional key for the entity being processed in the message.</param>
		void Publish<TMessage>(TMessage message, MessagePriority? messagePriority = null, bool updateIfExists = false, DateTime? processOn = null, string entityName = null, long? entityKey = null) where TMessage : class, IMessage;

		/// <summary>
		/// Processes the messages.
		/// </summary>
		/// <param name="messageTypes">Optional message types to target.</param>
		/// <param name="excludeMessageTypes">Optional message types to exclude.</param>
		void ProcessMessages(List<Guid> messageTypes = null, List<Guid> excludeMessageTypes = null);

		/// <summary>
		/// Schedules a message to run at a set interval
		/// </summary>
		/// <param name="message">The message to schedule</param>
		/// <param name="schedulePlan">The planned schedule to run</param>
		/// <param name="messagePriority">The message priority.</param>
		void ScheduleMessage<TMessage>(TMessage message, MessageSchedulePlan schedulePlan, MessagePriority? messagePriority = MessagePriority.Medium) where TMessage : class, IMessage;

		/// <summary>
		/// Gets a MessageType record for a message
		/// </summary>
		/// <param name="message">The message whose type is required</param>
		/// <returns>The MessageType record</returns>
		MessageTypeEntity GetMessageType<TMessage>(TMessage message) where TMessage : class, IMessage;
	}

	#endregion
}
