﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using System.Web;
using Focus.Core;
using Focus.Core.Models;
using Focus.Data.Core.Entities;
using Framework.Core;
using Framework.Encryption;

#endregion


namespace Focus.Services.Helpers
{
	public class EncryptionHelper : HelperBase, IEncryptionHelper
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="EncryptionHelper"/> class.
		/// </summary>
		public EncryptionHelper() : base(null) {}

		/// <summary>
		/// Initializes a new instance of the <see cref="EncryptionHelper"/> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		public EncryptionHelper(IRuntimeContext runtimeContext) : base(runtimeContext) { }

    /// <summary>
    /// Encrypts the specified value.
    /// </summary>
    /// <param name="value">The value.</param>
    /// <param name="vector">The vector.</param>
    /// <param name="urlEncode">if set to <c>true</c> [URL encode].</param>
    /// <returns></returns>
    /// <exception cref="System.Exception">Encryption configs are not defined</exception>
    public string Encrypt(string value, string vector, bool urlEncode = false)
    {
      if (AppSettings.SharedEncryptionKey.IsNullOrEmpty() || vector.IsNullOrEmpty())
        throw new Exception("Encryption configs are not defined");

      var encryptor = new AESEncryption(AppSettings.SharedEncryptionKey, vector);

      var encryptedValue = encryptor.EncryptToString(value);

      if (urlEncode)
        encryptedValue = HttpUtility.UrlEncode(encryptedValue);

      return encryptedValue;
    }

		/// <summary>
		/// Encrypts the specified value.
		/// </summary>
		/// <param name="value">The value.</param>
    /// <param name="targetType">Type of the target.</param>
    /// <param name="entityType">Type of the entity.</param>
    /// <param name="entityId">The entity id.</param>
    /// <param name="urlEncode">if set to <c>true</c> [URL encode].</param>
		/// <returns></returns>
		/// <exception cref="System.Exception">Encryption configs are not defined</exception>
    public string Encrypt(string value, TargetTypes targetType, EntityTypes entityType, long entityId, bool urlEncode = false)
		{
		  var vector = GenerateVector(targetType, entityType, entityId);

      return Encrypt(value, vector.Vector, urlEncode);
		}

		/// <summary>
		/// Decrypts the specified value.
		/// </summary>
		/// <param name="value">The value.</param>
    /// <param name="vector">The vector.</param>
    /// <param name="urlDecode">if set to <c>true</c> [URL decode].</param>
		/// <returns></returns>
		/// <exception cref="System.Exception">Encryption configs are not defined</exception>
		public string Decrypt(string value, string vector, bool urlDecode = false)
		{
			if (AppSettings.SharedEncryptionKey.IsNullOrEmpty() || vector.IsNullOrEmpty())
				throw new Exception("Encryption configs are not defined");

			var encryptor = new AESEncryption(AppSettings.SharedEncryptionKey, vector);

			if (urlDecode)
				value = HttpUtility.UrlDecode(value);

			var decryptedValue = encryptor.DecryptString(value);

			return decryptedValue;
		}

    /// <summary>
    /// Decrypts the specified value.
    /// </summary>
    /// <param name="value">The value.</param>
    /// <param name="targetType">Type of the target.</param>
    /// <param name="entityType">Type of the entity.</param>
    /// <param name="entityId">The entity id.</param>
    /// <param name="urlDecode">if set to <c>true</c> [URL decode].</param>
    /// <returns></returns>
    /// <exception cref="System.Exception">Encryption configs are not defined</exception>
    public string Decrypt(string value, TargetTypes targetType, EntityTypes entityType, long entityId, bool urlDecode = false)
    {
      var vector = GetVector(targetType, entityType, entityId, true);

      return Decrypt(value, vector.Vector, urlDecode);
    }

    /// <summary>
    /// Generates the vector.
    /// </summary>
    /// <param name="targetType">Type of the target.</param>
    /// <param name="entityType">Type of the entity.</param>
    /// <param name="entityId">The entity id.</param>
    /// <returns></returns>
    /// <exception cref="System.Exception">Vector does not exist</exception>
    public EncryptionModel GenerateVector(TargetTypes targetType, EntityTypes entityType, long entityId)
    {
      // Determine if a vector exists already
      var encryption = GetVector(targetType, entityType, entityId);

      // If it doesn't exist then create one
      if (encryption.IsNull())
      {
        var vector = Guid.NewGuid().ToString().Substring(0, 16);

        var newEncryption = new Encryption
        {
          TargetTypeId = targetType,
          EntityTypeId = entityType,
          EntityId = entityId,
          Vector = vector
        };

        Repositories.Core.Add(newEncryption);
        Repositories.Core.SaveChanges();

        return new EncryptionModel { EncryptionId = newEncryption.Id, Vector = vector };
      }

      return encryption;
    }

	  /// <summary>
	  /// Gets a vector.
	  /// </summary>
	  /// <param name="targetType">Type of the target.</param>
	  /// <param name="entityType">Type of the entity.</param>
	  /// <param name="entityId">The entity id.</param>
	  /// <param name="forDecryption">Whether the vector is required for description (in which case the record needs to exist)</param>
	  /// <returns></returns>
	  /// <exception cref="System.Exception">Vector does not exist</exception>
	  public EncryptionModel GetVector(TargetTypes targetType, EntityTypes entityType, long entityId, bool forDecryption = false)
	  {
	    // Determine if a vector exists already
	    var encryption = Repositories.Core.Encryptions.FirstOrDefault(x => x.TargetTypeId.Equals(targetType) && x.EntityTypeId.Equals(entityType) && x.EntityId.Equals(entityId));

	    if (encryption.IsNull())
	    {
	      if (forDecryption)
	        throw new Exception("Vector does not exist");

	      return null;
	    }

      return new EncryptionModel { EncryptionId = encryption.Id, Vector = encryption.Vector };
	  }

	  /// <summary>
		/// Gets an existing vector.
		/// </summary>
		/// <param name="encryptionId">The encryption id.</param>
		/// <returns></returns>
    public EncryptionModel GetVector(long encryptionId)
		{
			// Determine if a vector exists already
			var encryption = Repositories.Core.FindById<Encryption>(encryptionId);

      if (encryption.IsNull())
        throw new Exception("Vector does not exist");

			return new EncryptionModel { EncryptionId = encryption.Id, Vector = encryption.Vector };
		}
	}

	public interface IEncryptionHelper
	{
		/// <summary>
		/// Encrypts the specified value.
		/// </summary>
		/// <param name="value">The value.</param>
    /// <param name="vector">The vector.</param>
    /// <param name="urlEncode">if set to <c>true</c> [URL encode].</param>
		/// <returns></returns>
		string Encrypt(string value, string vector = null, bool urlEncode = false);

	  /// <summary>
	  /// Encrypts the specified value.
	  /// </summary>
	  /// <param name="value">The value.</param>
	  /// <param name="targetType">Type of the target.</param>
	  /// <param name="entityType">Type of the entity.</param>
	  /// <param name="entityId">The entity id.</param>
	  /// <param name="urlEncode">if set to <c>true</c> [URL encode].</param>
	  /// <returns></returns>
	  /// <exception cref="System.Exception">Encryption configs are not defined</exception>
	  string Encrypt(string value, TargetTypes targetType, EntityTypes entityType, long entityId, bool urlEncode = false);

		/// <summary>
		/// Decrypts the specified value.
		/// </summary>
		/// <param name="value">The value.</param>
    /// <param name="vector">The vector.</param>
    /// <param name="urlDecode">if set to <c>true</c> [URL decode].</param>
		/// <returns></returns>
		string Decrypt(string value, string vector = null, bool urlDecode = false);

	  /// <summary>
	  /// Decrypts the specified value.
	  /// </summary>
	  /// <param name="value">The value.</param>
	  /// <param name="targetType">Type of the target.</param>
	  /// <param name="entityType">Type of the entity.</param>
	  /// <param name="entityId">The entity id.</param>
	  /// <param name="urlDecode">if set to <c>true</c> [URL decode].</param>
	  /// <returns></returns>
	  /// <exception cref="System.Exception">Encryption configs are not defined</exception>
	  string Decrypt(string value, TargetTypes targetType, EntityTypes entityType, long entityId, bool urlDecode = false);

		/// <summary>
		/// Generates the vector.
		/// </summary>
		/// <param name="targetType">Type of the target.</param>
		/// <param name="entityType">Type of the entity.</param>
		/// <param name="entityId">The entity id.</param>
    /// <returns></returns>
    EncryptionModel GenerateVector(TargetTypes targetType, EntityTypes entityType, long entityId);

    /// <summary>
    /// Gets an existing vector.
    /// </summary>
    /// <param name="targetType">Type of the target.</param>
    /// <param name="entityType">Type of the entity.</param>
    /// <param name="entityId">The entity id.</param>
    /// <param name="forDecryption">Whether the vector is required for description (in which case a new one should not be created)</param>
    /// <returns></returns>
    EncryptionModel GetVector(TargetTypes targetType, EntityTypes entityType, long entityId, bool forDecryption = false);

	  /// <summary>
	  /// Gets an existing vector.
	  /// </summary>
	  /// <param name="encryptionId">The encryption id.</param>
	  /// <returns></returns>
    EncryptionModel GetVector(long encryptionId);
	}
}
