﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;

using Framework.Caching;
using Framework.Core;

using Focus.Core;
using Focus.Core.Views;
using Focus.Services.Helpers;

#endregion

namespace Focus.Services.Core
{
	#region LookupHelper Class

	public class LookupHelper : HelperBase, ILookupHelper
	{
		private readonly static List<CachedLookup> Lookups = new List<CachedLookup>();

		private class CachedLookup
		{
			internal string Culture { get; set; }
			internal LookupTypes Type { get; set; }
			internal List<LookupItemView> Items { get; set; }
		}

    /// <summary>
		/// Initializes a new instance of the <see cref="LookupHelper" /> class.
		/// </summary>
		public LookupHelper() : this(null)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="LookupHelper" /> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		public LookupHelper(IRuntimeContext runtimeContext) : base(runtimeContext)
		{ }

		/// <summary>
		/// Gets the lookup.
		/// </summary>
		/// <param name="lookupType">Type of the lookup.</param>
		/// <param name="parentId">The parent id.</param>
		/// <param name="key">The key.</param>
    /// <param name="prefix">An optional prefix</param>
    /// <returns></returns>
    public List<LookupItemView> GetLookup(LookupTypes lookupType, long parentId = 0, string key = null, string prefix = null)
		{
		  var lookup = prefix.IsNullOrEmpty()
		                 ? GetCachedLookup(lookupType)
		                 : GetCachedLookup(lookupType).Where(x => x.Text.StartsWith(prefix, StringComparison.OrdinalIgnoreCase));

			var lookupItems = (parentId == 0)
        ? lookup.OrderBy(x => x.DisplayOrder).ThenBy(x => x.Text)
        : lookup.Where(x => x.ParentId == parentId).OrderBy(x => x.DisplayOrder).ThenBy(x => x.Text);

			return (key.IsNotNullOrEmpty() ? lookupItems.Where(x => x.Key == key).ToList() : lookupItems.ToList());
		}

		/// <summary>
		/// Gets a lookup by Id
		/// </summary>
		/// <param name="lookupType">Type of the lookup.</param>
		/// <param name="id">The id of the lookup</param>
		/// <returns>The lookup record</returns>
		public LookupItemView GetLookupById(LookupTypes lookupType, long id)
		{
			return GetCachedLookup(lookupType).FirstOrDefault(x => x.Id == id);
		}

		/// <summary>
		/// Gets the lookups external id.
		/// </summary>
		/// <param name="lookupType">Type of the lookup.</param>
		/// <param name="id">The id.</param>
		/// <returns></returns>
		public string GetLookupExternalId(LookupTypes lookupType, long id)
		{
			var result = GetCachedLookup(lookupType).Where(x => x.Id == id).Select(x => x.ExternalId).FirstOrDefault();
			return result.IsNotNullOrEmpty() ? result : string.Empty;
		}

		/// <summary>
		/// Gets the lookup text.
		/// </summary>
		/// <param name="lookupType">Type of the lookup.</param>
		/// <param name="id">The id.</param>
		/// <returns></returns>
		public string GetLookupText(LookupTypes lookupType, long id)
		{
			var result = GetCachedLookup(lookupType).Where(x => x.Id == id).Select(x => x.Text).FirstOrDefault();
			return result.IsNotNullOrEmpty() ? result : string.Empty;
		}

	  /// <summary>
	  /// Gets the lookup text.
	  /// </summary>
	  /// <param name="lookupType">Type of the lookup.</param>
	  /// <param name="key">The key</param>
	  /// <param name="defaultText">The default text</param>
	  /// <returns></returns>
	  public string GetLookupText(LookupTypes lookupType, string key, string defaultText = "")
    {
      var result = GetCachedLookup(lookupType).Where(x => x.Key == key).Select(x => x.Text).FirstOrDefault();
      return result.IsNotNullOrEmpty() ? result : defaultText;
    }

    /// <summary>
    /// Gets the cached lookup.
    /// </summary>
    /// <param name="lookupType">Type of the lookup.</param>
    /// <returns></returns>
    protected IEnumerable<LookupItemView> GetCachedLookup(LookupTypes lookupType)
    {
      var culture = RuntimeContext.CurrentRequest.UserContext.Culture;

			// If we have already loaded the desired lookup types into "Lookups" then return them
	    var requestedLookup = Lookups.SingleOrDefault(l => l.Culture == culture && l.Type == lookupType);
			if (requestedLookup.IsNotNull()) return requestedLookup.Items;

	    List<LookupItemView> lookupItems;

      lock (SyncLock)
      {
				var retestRequestedLookup = Lookups.SingleOrDefault(l => l.Culture == culture && l.Type == lookupType);
				if (retestRequestedLookup != null) return retestRequestedLookup.Items;


				// Check to see if we have already cached the desired lookup types
        var lookupKey = string.Format(Constants.CacheKeys.LookupKey, culture, lookupType);
        lookupItems = Cacher.Get<List<LookupItemView>>(lookupKey);

				// If not then get them and cache them
				if (lookupItems == null)
        {
          // Users and culture
          var keySet1 = GetLookupItems(RuntimeContext.CurrentRequest.UserContext.Culture, lookupType);

          // Users config and default culture
          var rootSet = new List<LookupItemView>();
          if (RuntimeContext.CurrentRequest.UserContext.Culture != Constants.DefaultCulture)
            rootSet = GetLookupItems(Constants.DefaultCulture, lookupType);

					lookupItems = MergeLists(rootSet, keySet1);

					Cacher.Set(lookupKey, lookupItems);
        }

				Lookups.Add(new CachedLookup { Culture = culture, Type = lookupType, Items = lookupItems });
      }

      // Return the requested set of data
      return lookupItems;
    }

		/// <summary>
		/// Gets the lookup items.
		/// </summary>
		/// <param name="culture">The culture.</param>
		/// <param name="type">The lookup type</param>
		/// <returns></returns>
		private List<LookupItemView> GetLookupItems(string culture, LookupTypes type)
		{
			return (
							 from lu in Repositories.Configuration.LookupItemsViews
							 where lu.Culture == culture && lu.LookupType == type.ToString()
							 select new LookupItemView(lu.Id, lu.Key, lu.LookupType, Helpers.Localisation.LocaliseConfiguablePlaceHolders(lu.Value), lu.DisplayOrder, lu.ParentId, lu.ParentKey, lu.ExternalId, lu.CustomFilterId)
						 ).ToList();
		}

		/// <summary>
		/// Merges the lists.
		/// </summary>
		/// <param name="rootSet">The root set.</param>
		/// <param name="keySet">The key set.</param>
		/// <returns></returns>
		private static List<LookupItemView> MergeLists(ICollection<LookupItemView> rootSet, IEnumerable<LookupItemView> keySet)
		{
			var removeSet = (from rs in rootSet
											 join ks in keySet on
												new { rs.Key, rs.LookupType }
												equals
												new { ks.Key, ks.LookupType }
											 select rs).ToList();

			removeSet.ForEach(x => rootSet.Remove(x));
			return keySet.Union(rootSet).ToList();
		}
	}

	#endregion

	#region LookupHelper Interface

	public interface ILookupHelper
	{
		/// <summary>
		/// Gets the lookup.
		/// </summary>
		/// <param name="lookupType">Type of the lookup.</param>
		/// <param name="parentId">The parent id.</param>
		/// <param name="key">The key.</param>
    /// <param name="prefix">An optional prefix</param>
    /// <returns></returns>
    List<LookupItemView> GetLookup(LookupTypes lookupType, long parentId = 0, string key = null, string prefix = null);

		/// <summary>
		/// Gets a lookup by Id
		/// </summary>
		/// <param name="lookupType">Type of the lookup.</param>
		/// <param name="id">The id of the lookup</param>
		/// <returns>The lookup record</returns>
		LookupItemView GetLookupById(LookupTypes lookupType, long id);

		/// <summary>
		/// Gets the lookups external id.
		/// </summary>
		/// <param name="lookupType">Type of the lookup.</param>
		/// <param name="id">The id.</param>
		/// <returns></returns>
		string GetLookupExternalId(LookupTypes lookupType, long id);

		/// <summary>
		/// Gets the lookup text.
		/// </summary>
		/// <param name="lookupType">Type of the lookup.</param>
		/// <param name="id">The id.</param>
		/// <returns></returns>
		string GetLookupText(LookupTypes lookupType, long id);

		/// <summary>
		/// Gets the lookup text.
		/// </summary>
		/// <param name="lookupType">Type of the lookup.</param>
		/// <param name="key">The key</param>
		/// <param name="defaultText">The default text</param>
		/// <returns></returns>
		string GetLookupText(LookupTypes lookupType, string key, string defaultText = "");
	}

	#endregion
}
