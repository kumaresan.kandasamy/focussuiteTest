﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;

using Focus.Core;
using Focus.Core.IntegrationMessages;
using Focus.Core.Messages;
using Focus.Data.Core.Entities;
using Focus.Services.Messages;
using Focus.Services.Repositories;
using Framework.Core;

using Mindscape.LightSpeed;
using Mindscape.LightSpeed.Querying;

#endregion

namespace Focus.Services.Helpers
{
	#region LoggingHelper Class

	public class LoggingHelper : HelperBase, ILoggingHelper
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="LoggingHelper"/> class.
		/// </summary>
		public LoggingHelper() : base(null) { }

		/// <summary>
		/// Initializes a new instance of the <see cref="LoggingHelper"/> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		public LoggingHelper(IRuntimeContext runtimeContext) : base(runtimeContext) {}

		/// <summary>
		/// Gets the action type record
		/// </summary>
		/// <param name="actionType">The action type enum value</param>
		/// <returns>The ActionType entity</returns>
		public ActionType GetActionType(ActionTypes actionType)
		{
			var action = Repositories.Core.ActionTypes.FirstOrDefault(at => at.Name == actionType.ToString());

			if (action == null)
			{
				action = new ActionType { Name = actionType.ToString(), ReportOn = false, AssistAction = false };
				Repositories.Core.Add(action);
			}

			return action;
		}

    /// <summary>
    /// Logs an action.
    /// </summary>
    /// <param name="actionType">Type of the action.</param>
    /// <param name="entityName">Name of the entity.</param>
    /// <param name="entityId">The entity id.</param>
    /// <param name="entityIdAdditional01">The entity id additional01.</param>
    /// <param name="entityIdAdditional02">The entity id additional02.</param>
    /// <param name="additionalDetails">The additional details.</param>
    /// <param name="commit">if set to <c>true</c> [commit].</param>
    /// <param name="actionedOn">Override the ActionedOn date</param>
    /// <param name="overrideUserId">Whether to override the id of the user performing the action</param>
    public long LogAction(ActionTypes actionType, string entityName, long? entityId, long? entityIdAdditional01 = null, long? entityIdAdditional02 = null, string additionalDetails = null, bool commit = true, DateTime? actionedOn = null, long? overrideUserId = null, DateTime? overrideCreatedOn = null)
    {
	    var action = GetActionType(actionType);

			EntityType entityType = null;

			if (entityName.IsNotNullOrEmpty())
			{
				entityType = Repositories.Core.EntityTypes.FirstOrDefault(et => et.Name == entityName);

				if (entityType == null)
				{
					entityType = new EntityType { Name = entityName };
					Repositories.Core.Add(entityType);
				}
			}

            //Set activity to backdate status if actionedOn is not today.
            var activityStatusId = (actionedOn.IsNotNull() && actionedOn < DateTime.Today) ? ActionEventStatus.Backdated : ActionEventStatus.None;

			var actionEvent = new ActionEvent
			                  	{
			                  		SessionId = RuntimeContext.CurrentRequest.SessionId,
			                  		RequestId = RuntimeContext.CurrentRequest.RequestId,
                            UserId = overrideUserId ?? RuntimeContext.CurrentRequest.UserContext.ActionerId,
			                  		ActionedOn = actionedOn ?? DateTime.Now,
			                  		ActionType = action,
			                  		EntityId = entityId,
			                  		EntityType = entityType,
			                  		EntityIdAdditional01 = entityIdAdditional01,
			                  		EntityIdAdditional02 = entityIdAdditional02,
			                  		AdditionalDetails = additionalDetails,
                            CreatedOn = DateTime.Now
			                  	};

            if (activityStatusId != ActionEventStatus.None) { actionEvent.ActivityStatusId = ActionEventStatus.Backdated; }

			Repositories.Core.Add(actionEvent);

			if (commit)
				Repositories.Core.SaveChanges();

			if (overrideCreatedOn != null)
			{
				var changes = new Dictionary<string, object>();
				
				changes.Add("ActionedOn", overrideCreatedOn);

				var updateQuery = new Query(typeof(ActionEvent), Entity.Attribute("Id") == actionEvent.Id);
				Repositories.Core.Update(updateQuery, changes);
				Repositories.Core.SaveChanges();
			}

			LogActionToIntegration(RuntimeContext.CurrentRequest, actionType, actionEvent.Id, entityId);

			return actionEvent.Id;
    }

		/// <summary>
		/// Logs the actions.
		/// </summary>
		/// <param name="actionType">Type of the action.</param>
		/// <param name="entityName">Name of the entity.</param>
		/// <param name="entityId">The entity id.</param>
		/// <param name="entityIdAdditionals">The entity id additional01s.</param>
		/// <param name="commit">if set to <c>true</c> [commit].</param>
		public List<long> LogActions(ActionTypes actionType, string entityName, long entityId, long[] entityIdAdditionals, bool commit = true)
		{
			var action = Repositories.Core.ActionTypes.FirstOrDefault(at => at.Name == actionType.ToString());

			if (action == null)
			{
				action = new ActionType { Name = actionType.ToString(), ReportOn = false, AssistAction = false };
				Repositories.Core.Add(action);
			}

			EntityType entityType = null;

			if (entityName.IsNotNullOrEmpty())
			{
				entityType = Repositories.Core.EntityTypes.FirstOrDefault(et => et.Name == entityName);

				if (entityType == null)
				{
					entityType = new EntityType { Name = entityName };
					Repositories.Core.Add(entityType);
				}
			}

			var actionEventIds = new List<long>();

			foreach (var entityIdAdditional in entityIdAdditionals)
			{
				var actionEvent = new ActionEvent
				{
					SessionId = RuntimeContext.CurrentRequest.SessionId,
					RequestId = RuntimeContext.CurrentRequest.RequestId,
					UserId = RuntimeContext.CurrentRequest.UserContext.ActionerId,
					ActionedOn = DateTime.Now,
					ActionType = action,
					EntityId = entityId,
					EntityType = entityType,
					EntityIdAdditional01 = entityIdAdditional
				};

				Repositories.Core.Add(actionEvent);
				actionEventIds.Add(actionEvent.Id);
			}

			if (commit)
				Repositories.Core.SaveChanges();

			actionEventIds.ForEach(actionEventId => LogActionToIntegration(RuntimeContext.CurrentRequest, actionType, actionEventId, entityId));

			return actionEventIds;
		}

		/// <summary>
		/// Logs the action.
		/// </summary>
		/// <param name="actionType">Type of the action.</param>
		/// <param name="entityName">Name of the entity.</param>
		/// <param name="entityId">The entity id.</param>
		/// <param name="commit">if set to <c>true</c> [commit].</param>
		public long LogAction(ActionTypes actionType, string entityName, long? entityId, bool commit = true)
		{
			return LogAction(actionType, entityName, entityId, null, commit: commit);
		}

		/// <summary>
		/// Logs an action.
		/// </summary>
		/// <param name="actionType">Type of the action.</param>
		/// <param name="entity">The entity.</param>
		/// <param name="commit">if set to <c>true</c> [commit].</param>
    /// <param name="overrideUserId">Override the id of the user for which the action is logged</param>
    /// <param name="actionedOn">Override the ActionedOn date</param>
    public long LogAction(ActionTypes actionType, Entity<long> entity, bool commit = true, long? overrideUserId = null, DateTime? actionedOn = null)
		{
			string entityName = null;
			long? entityId = null;

			if (entity.IsNotNull())
			{
				entityName = entity.GetType().Name;
				entityId = entity.Id;
			}

      return LogAction(actionType, entityName, entityId, null, commit: commit, overrideUserId: overrideUserId, actionedOn: actionedOn);
		}

		/// <summary>
		/// Logs the status change.
		/// </summary>
		/// <param name="entityName">Name of the entity.</param>
		/// <param name="entityId">The entity id.</param>
		/// <param name="originalStatus">The original status.</param>
		/// <param name="newStatus">The new status.</param>
    /// <param name="overrideUserId">Override the id of the user for which the action is logged</param>
    public void LogStatusChange(string entityName, long entityId, long? originalStatus, long newStatus, long? overrideUserId = null)
		{
			EntityType entityType = null;

			if (entityName.IsNotNullOrEmpty())
			{
				entityType = Repositories.Core.EntityTypes.FirstOrDefault(et => et.Name == entityName);

				if (entityType == null)
				{
					entityType = new EntityType { Name = entityName };
					Repositories.Core.Add(entityType);
				}
			}

			var statusLog = new StatusLog
			{
				EntityId = entityId,
				EntityType = entityType,
				OriginalStatus = originalStatus,
				NewStatus = newStatus,
				UserId = overrideUserId ?? RuntimeContext.CurrentRequest.UserContext.UserId,
				ActionedOn = DateTime.Now
			};

			Repositories.Core.Add(statusLog);
			Repositories.Core.SaveChanges();
		}

		/// <summary>
		/// Logs the action to integration.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <param name="actionType">Type of the action.</param>
		/// <param name="actionId">The action identifier.</param>
		/// <param name="entityId">The id of the entity for the action</param>
		public void LogActionToIntegration(IServiceRequest request, ActionTypes actionType, long? actionId, long? entityId)
		{
			// If we are logging to standalone then don't do anything
			// TODO - Make this configurable. It was a quick fix to ignore EKOS (This doesn't handle Action records, so no need to publish so many integration requets to the message bus)
			if (AppSettings.IntegrationClient.IsIn(IntegrationClient.Standalone, IntegrationClient.EKOS))
				return;

			// If there are some Integration Loggable Actions set then ensure this is one of them.  
			// If not, then don't do anything
			if (!AppSettings.IntegrationLoggableActions.Contains(actionType)) return;

			if ((actionType == ActionTypes.LogIn || actionType == ActionTypes.RegisterAccount) && entityId != null)
				request.UserContext.UserId = (long)entityId;

            //If the staff updates jobseeker resume,then assign the jobseeker user id in place of staff 
            var user = Repositories.Core.Query<User>().Where(x => x.PersonId == request.UserContext.PersonId).FirstOrDefault();
            if ((actionType == ActionTypes.SaveResume || actionType == ActionTypes.ExternalStaffReferral) && user.UserType == UserTypes.Assist)
            {
                if (actionType == ActionTypes.SaveResume)
                {
                    var resume = Repositories.Core.FindById<Resume>(entityId);
                    request.UserContext.UserId = resume.Person.User.Id;
                }
                else
                {
                    var action = Repositories.Core.FindById<ActionEvent>(actionId);
                    var person = Repositories.Core.FindById<Person>(action.EntityIdAdditional01);
                    request.UserContext.UserId = person.User.Id;
                }
            }

			// We need a user context to continue, if we don't have one then get out of dodge!!
			if (request.UserContext.IsNull() ||
					(request.UserContext.ExternalUserId.IsNullOrEmpty() && (request.UserContext.UserId.IsNull() || request.UserContext.UserId == 0)))
				return;

			// So we have a user context but do we have an externall user id? 
			// If not then go to the DB and get it
			var externalId = !request.UserContext.ExternalUserId.IsNullOrEmpty()
				? request.UserContext.ExternalUserId
				: (Repositories.Core.FindById<User>(request.UserContext.UserId) ?? new User()).ExternalId;

			// No external id then again get out of dodge (except for Georgia)
			if (AppSettings.IntegrationClient != IntegrationClient.Georgia)
				if (externalId.IsNullOrEmpty()) return;

			var integrationRepository = new IntegrationRepository(RuntimeContext);

			var actionerId = request.UserContext.ActionerId == 0 ? request.UserContext.UserId : request.UserContext.ActionerId;

			var logActionRequest = new LogActionRequest
			{
				ActionerExternalId = externalId,
				ActionedOn = DateTime.Now,
				ActionType = actionType
			};

			if (AppSettings.IntegrationOneADayActions.Contains(actionType))
			{
				logActionRequest.CheckForOneADay = true;
				logActionRequest.UserId = request.UserContext.UserId;
				logActionRequest.ActionerId = actionerId;
			}

			var integrationRequestMessage = new IntegrationRequestMessage
			{
				ActionerId = actionerId,
				IntegrationPoint = IntegrationPoint.LogAction,
				LogActionRequest = logActionRequest
			};

			// Add Json ActionData if required 
			var actionData = integrationRepository.GetActionData(actionType, actionId, actionerId);
			if (actionData.IsNotNullOrEmpty())
			{
				integrationRequestMessage.LogActionRequest.ActionData = actionData;
				Helpers.Messaging.Publish(integrationRequestMessage);
			}
		}
	}

	#endregion

	#region LoggingHelper Interface

	public interface ILoggingHelper
	{
		/// <summary>
		/// Gets the action type record
		/// </summary>
		/// <param name="actionType">The action type enum value</param>
		/// <returns>The ActionType entity</returns>
		ActionType GetActionType(ActionTypes actionType);

		/// <summary>
		/// Logs an action.
		/// </summary>
		/// <param name="actionType">Type of the action.</param>
		/// <param name="entityName">Name of the entity.</param>
		/// <param name="entityId">The entity id.</param>
		/// <param name="entityIdAdditional01">The entity id additional01.</param>
		/// <param name="entityIdAdditional02">The entity id additional02.</param>
		/// <param name="additionalDetails">The additional details.</param>
		/// <param name="commit">if set to <c>true</c> [commit].</param>
    /// <param name="actionedOn">Override the ActionedOn date</param>
    /// <param name="overrideUserId">Whether to override the id of the user performing the action</param>
    long LogAction(ActionTypes actionType, string entityName, long? entityId, long? entityIdAdditional01 = null, long? entityIdAdditional02 = null, string additionalDetails = null, bool commit = true, DateTime? actionedOn = null, long? overrideUserId = null, DateTime? overrideCreatedOn = null);

		/// <summary>
		/// Logs the actions.
		/// </summary>
		/// <param name="actionType">Type of the action.</param>
		/// <param name="entityName">Name of the entity.</param>
		/// <param name="entityId">The entity id.</param>
		/// <param name="entityIdAdditionals">The entity id additional01s.</param>
		/// <param name="commit">if set to <c>true</c> [commit].</param>
		List<long> LogActions(ActionTypes actionType, string entityName, long entityId, long[] entityIdAdditionals, bool commit = true);

		/// <summary>
		/// Logs the action.
		/// </summary>
		/// <param name="actionType">Type of the action.</param>
		/// <param name="entityName">Name of the entity.</param>
		/// <param name="entityId">The entity id.</param>
		/// <param name="commit">if set to <c>true</c> [commit].</param>
		long LogAction(ActionTypes actionType, string entityName, long? entityId, bool commit = true);

		/// <summary>
		/// Logs an action.
		/// </summary>
		/// <param name="actionType">Type of the action.</param>
		/// <param name="entity">The entity.</param>
		/// <param name="commit">if set to <c>true</c> [commit].</param>
    /// <param name="overrideUserId">Override the id of the user for which the action is logged</param>
    /// <param name="actionedOn">Override the ActionedOn date</param>
    long LogAction(ActionTypes actionType, Entity<long> entity, bool commit = true, long? overrideUserId = null, DateTime? actionedOn = null);

		/// <summary>
		/// Logs the status change.
		/// </summary>
		/// <param name="entityName">Name of the entity.</param>
		/// <param name="entityId">The entity id.</param>
		/// <param name="originalStatus">The original status.</param>
		/// <param name="newStatus">The new status.</param>
    /// <param name="overrideUserId">Override the id of the user for which the action is logged</param>
    void LogStatusChange(string entityName, long entityId, long? originalStatus, long newStatus, long? overrideUserId = null);

		/// <summary>
		/// Logs the action to integration.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <param name="actionType">Type of the action.</param>
		/// <param name="actionId">The action identifier.</param>
		/// <param name="entityId">The id of the entity for the action</param>
		void LogActionToIntegration(IServiceRequest request, ActionTypes actionType, long? actionId, long? entityId);
	}

	#endregion
}
