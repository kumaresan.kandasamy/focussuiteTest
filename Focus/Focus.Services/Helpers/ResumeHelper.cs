﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Xml.Xsl;

using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Messages;
using Focus.Core.Models.Career;
using Focus.Data.Core.Entities;
using Focus.Data.Library.Entities;
using Focus.Services.DtoMappers;
using Focus.Services.Mappers;
using Focus.Services.Messages;
using Focus.Services.Repositories.Lens;

using Framework.Core;

#endregion

namespace Focus.Services.Helpers
{
	#region ResumeHelper Class

	public class ResumeHelper : HelperBase, IResumeHelper
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="ResumeHelper" /> class.
		/// </summary>
		public ResumeHelper() : this(null)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="ResumeHelper" /> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		public ResumeHelper(IRuntimeContext runtimeContext) : base(runtimeContext)
		{ }

		/// <summary>
		/// Updates the status.
		/// </summary>
		/// <param name="resumeId">The resume id.</param>
		/// <param name="status">The status.</param>
		public void UpdateStatus(long resumeId, ResumeStatuses status)
		{
			var resume = Repositories.Core.FindById<Resume>(resumeId);
			resume.StatusId = status;

			Repositories.Core.SaveChanges(true);
		}

    /// <summary>
    /// Generates the automated summary.
    /// </summary>
    /// <param name="taggedResume">The tagged resume.</param>
    /// <param name="filterSkills">if set to <c>true</c> [filter skills].</param>
    /// <param name="removeSpaces">if set to <c>true</c> [remove spaces].</param>
    /// <param name="escapeText">Whether to escape (XML Encode) the summary</param>
    /// <returns></returns>
    public string GenerateAutomatedSummary(string taggedResume, bool filterSkills, bool removeSpaces, bool escapeText)
		{
			var summary = "";

			// Load the resume into an xml document
			var xDoc = new XmlDocument { PreserveWhitespace = true };
			xDoc.LoadXml(taggedResume);

			// Get years of experience for the resume
			var yearsOfExperience = GetYearsOfExperience(xDoc);

			if (yearsOfExperience > 0)
				summary = "I have " + yearsOfExperience + ((yearsOfExperience > 1) ? " years " : " year ") + "of experience";
			else if (xDoc.SelectNodes("//job").Count > 0)
				summary = "I have work experience";

			// 1. Let us retrieve the list of titles and sic information for every job.
			// 2. If title is not identified in any of the job, let us consider the O*Net title

			var titles = new List<string>();
			var sics = new List<string>();

			// Get the industry lookups
			var industryLookups = Repositories.Library.StandardIndustrialClassifications.Where(x => x.SicSecondary != null && x.Name != null).OrderBy(x => x.Id).Select(x => x.AsDto()).ToList();

			foreach (XmlNode job in xDoc.SelectNodes("//job"))
			{
				#region Extract the title from resume. If not found emit SOC title

				if (job.SelectSingleNode("title") != null)
				{
					var jobTitle = job.SelectSingleNode("title").InnerText.Trim().Replace("\n", "").Replace("\r", "").Replace("\t", " ").Replace("  ", " ");

					if (!titles.Contains(jobTitle))
					{
						jobTitle = TitleCase(jobTitle);
						titles.Add(jobTitle);
					}
				}

				#endregion

				#region Extract the SIC information and get the name of that from the industry lookup

				if (job.Attributes != null && job.Attributes["sic2"] != null)
					foreach (var industryLookup in industryLookups)
						if (industryLookup.SicSecondary.ToLower() == job.Attributes["sic2"].Value.ToLower())
						{
							var sicName = industryLookup.Name.Replace("\n", "").Replace("\r", "").Replace("\t", " ").Replace("  ", " ");

							if (!sics.Contains(sicName))
							{
								sicName = TitleCase(sicName);
								sics.Add(sicName);
							}
						}

				#endregion
			}

			#region Frame the summary based on the titles

			if (titles.Count > 0)
			{
				summary += ", including ";

				// We will limit the title count display to 3.
				var titleCount = 3;

				if (titles.Count <= titleCount)
					titleCount = titles.Count;

				for (var i = 0; i < titleCount; i++)
				{
					if (i == titleCount - 1 && i != 0)
					{
						if (summary.LastIndexOf(", ") > 0)
							summary = summary.Substring(0, summary.LastIndexOf(", ")) + " ";

						summary += "and ";
					}

					#region Check for english vowels in job titles to frame the sentence.

					if (!String.IsNullOrEmpty(titles[i].ToString()))
					{
						var regExp = new Regex(@"^[aeiou].*", RegexOptions.IgnoreCase);

						if (regExp.IsMatch(titles[i].ToString()))
							summary += "as an " + titles[i].ToString().Replace(" And ", " and ").Replace(" ANd ", " and ").Replace(" AND ", " and ") + ", ";
						else
							summary += "as a " + titles[i].ToString().Replace(" And ", " and ").Replace(" ANd ", " and ").Replace(" AND ", " and ") + ", ";
					}

					#endregion
				}

				if (summary.LastIndexOf(", ") > 0)
					summary = summary.Substring(0, summary.LastIndexOf(", ")) + " ";


				if (sics.Count > 0)
				{
					var industries = "";

					// We will limit the sic title count display to 3.
					var sicCount = 3;

					if (sics.Count <= sicCount)
						sicCount = sics.Count;

					for (var i = 0; i < sicCount; i++)
					{
						if (i == sicCount - 1 && i != 0)
						{
							if (industries.LastIndexOf(", ") > 0)
								industries = industries.Substring(0, industries.LastIndexOf(", "));

							industries += " and ";
						}

						industries += sics[i].ToString().Replace(" And ", " and ").Replace(" ANd ", " and ").Replace(" AND ", " and ") + ", ";
					}

					// Remove character ',' from end of industries list.
					if (industries.LastIndexOf(", ") > 0)
						industries = industries.Substring(0, industries.LastIndexOf(", "));

					if (!String.IsNullOrEmpty(industries))
						summary += "in industries including " + industries.Trim() + ".\n";
				}
				else
					summary = summary.Trim() + ".\n\n";
			}
			else
			{
				if (!String.IsNullOrEmpty(summary))
					summary += ".\n";
			}

			#endregion

			#region Process the most recent job

			foreach (XmlNode job in xDoc.SelectNodes("//job[@pos='1']"))
			{
				if (summary.EndsWith("\n\n"))
					summary += "Most recently, I have been working ";
				else
					summary += "\nMost recently, I have been working ";


				if (job.SelectSingleNode("title") != null)
				{
					var title = job.SelectSingleNode("title").InnerText.Trim().Replace("\n", "").Replace("\r", "").Replace("\t", " ").Replace("  ", " ");
					var regExp = new Regex(@"^[aeiou].*", RegexOptions.IgnoreCase);

					if (regExp.IsMatch(title.ToString()))
						title = "as an " + title;
					else
						title = "as a " + title;
					summary += title + " ";
				}

				if (job.SelectSingleNode("employer") != null && !(job.SelectSingleNode("employer").InnerText.Trim() == "" || job.SelectSingleNode("employer").InnerText.Trim().ToLower() == "self"))
					summary += "at " + job.SelectSingleNode("employer").InnerText.Trim().Replace("\n", "").Replace("\r", "").Replace("\t", " ").Replace("  ", " ") + " ";

				summary += "from ";

				if (job.SelectSingleNode("daterange/start") != null && job.SelectSingleNode("daterange/start").Attributes != null && job.SelectSingleNode("daterange/start").Attributes["iso8601"] != null)
					summary += DateTime.Parse(job.SelectSingleNode("daterange/start").Attributes["iso8601"].Value, new System.Globalization.CultureInfo("en-US")).ToString("MMMM yyyy");

				if (job.SelectSingleNode("daterange/end") != null && job.SelectSingleNode("daterange/end").Attributes != null && job.SelectSingleNode("daterange/end").Attributes["iso8601"] != null)
					summary += " to " + DateTime.Parse(job.SelectSingleNode("daterange/end").Attributes["iso8601"].Value, new System.Globalization.CultureInfo("en-US")).ToString("MMMM yyyy");

				summary += ".\n\n";
			}

			#endregion

			#region Process the skills section

			if (xDoc.SelectSingleNode("//skills/skills") != null)
			{
				var skillsInSummary = "";

				var skills = new List<string>();

				if (xDoc.SelectSingleNode("//skills/skills") != null)
					foreach (var skill in xDoc.SelectSingleNode("//skills/skills").InnerText.Split(','))
						skills.Add(skill.Trim());

				skills = FilterSkills(skills, SkillType.Specialized, filterSkills);

				if (skills != null && skills.Count > 0)
				{
					foreach (var skill in skills)
						skillsInSummary += skill + ", ";

					// Handle "StartIndex cannot be less than zero"
					if (skills.Count > 0 && skillsInSummary.LastIndexOf(", ") > 0)
						skillsInSummary = skillsInSummary.Remove(skillsInSummary.LastIndexOf(", ")) + ".\n";

					if (!String.IsNullOrEmpty(skillsInSummary))
						summary += "My skills and experiences include: " + skillsInSummary;
				}
			}

			#endregion

			#region Process the education section

			if (xDoc.SelectNodes("//school").Count > 0)
			{
				if (summary.EndsWith("\n\n"))
					summary += "I";
				else
					summary += "\nI";

				var index = 1;

				foreach (XmlNode school in xDoc.SelectNodes("//school"))
				{
					// Where no degree name has been extracted, replace "holds a" with "has
					// studied at" where Institution Name is available or "has studied" where
					// only major is available.
					var isSchoolInfoAvailable = false;

					if (school.SelectSingleNode("degree") != null)
					{
						// If completion date is future date; sentence has to be framed as “I will complete my (degree) in (major/subject) in (year).”
						var year = 0;
						var futureDate = false;
					  var expectedDate = false;

						if (school.SelectSingleNode("completiondate") != null)
						{
							var completionDate = DateTime.MaxValue;

							if (school.SelectSingleNode("completiondate").Attributes != null && school.SelectSingleNode("completiondate").Attributes["iso8601"] != null)
								DateTime.TryParse(school.SelectSingleNode("completiondate").Attributes["iso8601"].Value, out completionDate);
							else
								DateTime.TryParse(school.SelectSingleNode("completiondate").InnerText, out completionDate);

							if (completionDate != DateTime.MaxValue)
							{
								year = completionDate.Year;
								if (completionDate > DateTime.Today)
									futureDate = true;
							}
						}
            else if (school.SelectSingleNode("expectedcompletiondate") != null)
            {
              expectedDate = true;
            }

						var degreeName = "";

						if (school.SelectSingleNode("degree").Attributes["name"] != null)
							degreeName = school.SelectSingleNode("degree").Attributes["name"].Value;
						else
							degreeName = school.SelectSingleNode("degree").InnerText.Trim();

            if (expectedDate)
					  {
              summary += " am studying towards a ";
					  }
					  else if (year == 0 || (year > 0 && !futureDate))
					  {
					    var regExp = new Regex(@"^[aeiou].*", RegexOptions.IgnoreCase);

					    if (regExp.IsMatch(degreeName))
					      summary += " hold an ";
					    else
					      summary += " hold a ";
					  }
					  else
					  {
					    summary += " will complete my ";
					  }

            summary += degreeName.Replace("\n", "").Replace("\r", "").Replace("\t", " ").Replace("  ", " ") + " degree ";

						if (school.SelectSingleNode("major") != null && !String.IsNullOrEmpty(school.SelectSingleNode("major").InnerText.Trim()))
							summary += "in " + school.SelectSingleNode("major").InnerText.Trim().Replace("\n", "").Replace("\r", "").Replace("\t", " ").Replace("  ", " ") + " ";

						if (school.SelectSingleNode("institution") != null && !String.IsNullOrEmpty(school.SelectSingleNode("institution").InnerText.Trim()))
							summary += "from " + school.SelectSingleNode("institution").InnerText.Trim().Replace("\n", "").Replace("\r", "").Replace("\t", " ").Replace("  ", " ") + " ";

						if (year > 0 && futureDate)
							summary += "in " + year + " ";

						isSchoolInfoAvailable = true;
					}
					else if (school.SelectSingleNode("major") != null && !String.IsNullOrEmpty(school.SelectSingleNode("major").InnerText.Trim()))
					{
						summary += " have studied ";

						if (school.SelectSingleNode("major") != null && !String.IsNullOrEmpty(school.SelectSingleNode("major").InnerText.Trim()))
							summary += school.SelectSingleNode("major").InnerText.Trim().Replace("\n", "").Replace("\r", "").Replace("\t", " ").Replace("  ", " ") + " ";

						if (school.SelectSingleNode("institution") != null && !String.IsNullOrEmpty(school.SelectSingleNode("institution").InnerText.Trim()))
							summary += "at " + school.SelectSingleNode("institution").InnerText.Trim().Replace("\n", "").Replace("\r", "").Replace("\t", " ").Replace("  ", " ") + " ";

						isSchoolInfoAvailable = true;
					}
					else if (school.SelectSingleNode("institution") != null && !String.IsNullOrEmpty(school.SelectSingleNode("institution").InnerText.Trim()))
					{
						summary += " have studied at ";

						if (school.SelectSingleNode("institution") != null)
							summary += school.SelectSingleNode("institution").InnerText.Trim().Replace("\n", "").Replace("\r", "").Replace("\t", " ").Replace("  ", " ") + " ";

						isSchoolInfoAvailable = true;
					}

					if (index < xDoc.SelectNodes("//school").Count && isSchoolInfoAvailable)
						summary += "and";

					index++;
				}

				summary = summary.Trim() + ".\n\n";
			}
			else if (xDoc.SelectNodes("//degree").Count > 0)
			{
				summary += "I";

				// Where no degree name has been extracted, replace "holds a" with "has
				// studied at" where Institution Name is available or "has studied" where
				// only major is available.
				if (xDoc.SelectSingleNode("degree") != null)
				{
					summary += " hold ";

					if (xDoc.SelectSingleNode("degree").Attributes["name"] != null)
						summary += xDoc.SelectSingleNode("degree").Attributes["name"].Value + " ";
					else
						summary += xDoc.SelectSingleNode("degree").InnerText.Trim().Replace("\n", "").Replace("\r", "").Replace("\t", " ").Replace("  ", " ") + " ";

					if (xDoc.SelectSingleNode("major") != null)
						summary += "in " + xDoc.SelectSingleNode("major").InnerText.Trim().Replace("\n", "").Replace("\r", "").Replace("\t", " ").Replace("  ", " ") + " ";

					if (xDoc.SelectSingleNode("institution") != null)
						summary += "from " + xDoc.SelectSingleNode("institution").InnerText.Trim().Replace("\n", "").Replace("\r", "").Replace("\t", " ").Replace("  ", " ") + " ";
				}
				else if (xDoc.SelectSingleNode("major") != null)
				{
					summary += " have studied ";

					if (xDoc.SelectSingleNode("major") != null && !String.IsNullOrEmpty(xDoc.SelectSingleNode("major").InnerText.Trim()))
						summary += xDoc.SelectSingleNode("major").InnerText.Trim().Replace("\n", "").Replace("\r", "").Replace("\t", " ").Replace("  ", " ") + " ";

					if (xDoc.SelectSingleNode("institution") != null && !String.IsNullOrEmpty(xDoc.SelectSingleNode("institution").InnerText.Trim()))
						summary += "at " + xDoc.SelectSingleNode("institution").InnerText.Trim().Replace("\n", "").Replace("\r", "").Replace("\t", " ").Replace("  ", " ") + " ";
				}
				else if (xDoc.SelectSingleNode("institution") != null)
				{
					summary += " have studied at ";

					if (xDoc.SelectSingleNode("institution") != null)
						summary += xDoc.SelectSingleNode("institution").InnerText.Trim().Replace("\n", "").Replace("\r", "").Replace("\t", " ").Replace("  ", " ") + " ";
				}

				summary = summary.Trim() + ".\n\n";
			}

			#endregion

			if (removeSpaces)
			{
				summary = Regex.Replace(summary, @"[\n]+", " ");
			}

			// Make the summary XML compliant
			return escapeText 
        ? summary.Trim().XmlEncode()
        : summary.Trim();
		}

		/// <summary>
		/// Gets the years of experience.
		/// </summary>
		/// <param name="taggedResumeXml">The tagged resume XML.</param>
		/// <returns></returns>
		public static int GetYearsOfExperience(string taggedResumeXml)
		{
			if (taggedResumeXml.IsNullOrEmpty())
				return 0;

			var xDoc = new XmlDocument { PreserveWhitespace = true };
			xDoc.LoadXml(taggedResumeXml);

			return GetYearsOfExperience(xDoc);
		}

		#region Automated Summary Helpers

		/// <summary>
		/// Gets the yearsof experience.
		/// </summary>
		/// <param name="resume">The resume.</param>
		/// <returns></returns>
		private static int GetYearsOfExperience(XmlNode resume)
		{
			var yearsOfExperience = 0;

            //try
          //  {
          //      var present = 0;
          //      var minstart = 0;
          //      var maxend = 0;
          //      var value = 0;

          //      if (resume.SelectSingleNode("//resume[@present]") != null)
          //          Int32.TryParse(resume.SelectSingleNode("//resume").Attributes["present"].Value.Trim(), out present);

          //      foreach (XmlElement exp in resume.SelectNodes("//experience"))
          //      {
          //          if (exp.Attributes.Count > 0)
          //          {
          //              if (exp.Attributes["start"] != null)
          //                  Int32.TryParse(exp.Attributes["start"].Value, out value);

          //              if ((value > 0) && ((minstart == 0) || (value < minstart)))
          //                  minstart = value;

          //              if (exp.Attributes["end"] != null)
          //              {
          //                  if (exp.Attributes["end"].Value == "present")
          //                      value = present;
          //                  else
          //                      Int32.TryParse(exp.Attributes["end"].Value, out value);
          //              }

          //              if ((value > 0) && ((minstart == 0) || (value > maxend)))
          //                  maxend = value;
          //          }
          //      }

          //      if ((minstart != 0) && (maxend != 0))
          //      {
          //          // Start and end are in Julian days, so rough rounding ..
          //// Use 365.25 (not 365.2425) here to roughly take into leap years. (2000 was a leap year, 1900 wasn't, but jobs are unlikely to start before 1900)
          //          var temp = Convert.ToInt32(Math.Abs(Math.Round((maxend - minstart) / 365.25)));

          //          if (temp > 0 && temp < 100)
          //              yearsOfExperience = temp;
          //          else
          //              yearsOfExperience = 0;
          //      }

          //  }
            try
            {
                double totalDays = 0;
                foreach (XmlElement job in resume.SelectNodes("//experience//job"))
                {
                    var startDate = job.GetElementsByTagName("start")[0].InnerText;
                    var endDate = job.GetElementsByTagName("end")[0].InnerText;

                    totalDays += (Convert.ToDateTime(endDate) - Convert.ToDateTime(startDate)).TotalDays;
                }

                yearsOfExperience = Convert.ToInt32(totalDays / 365.25);

            }
			catch
			{
				yearsOfExperience = 0;
			}

			return yearsOfExperience;
		}

		/// <summary>
		/// Titles the case text.
		/// </summary>
		/// <param name="sourceText">The source text.</param>
		/// <returns></returns>
		public string TitleCase(string sourceText)
		{
			string destText;

			try
			{
				destText = Thread.CurrentThread.CurrentCulture.TextInfo.ToTitleCase(sourceText);

				// Avoid capitalization to stop words
				// stop words - I, a, about, an, are, as, at, be, by, com, de, en, for, from, how, in, is, it, la, of, on, or, 
				//              that, the, this, to, was, what, when, where, who, will, with, und, www
				destText = destText.Replace(" A ", " a ").Replace(" An ", " an ").Replace(" At ", " at ").Replace(" Are ", " are ").Replace(" As ", " as ").Replace(" Be ", " be ");
				destText = destText.Replace(" By ", " by ").Replace(" Com ", " com ").Replace(" De ", " de ").Replace(" En ", " en ").Replace(" For ", " for ").Replace(" From ", " from ");
				destText = destText.Replace(" How ", " how ").Replace(" In ", " in ").Replace(" Is ", " is ").Replace(" It ", " it ").Replace(" La ", " la ").Replace(" Of ", " of ");
				destText = destText.Replace(" On ", " on ").Replace(" Or ", " or ").Replace(" That ", " that ").Replace(" The ", " the ").Replace(" To ", " to ").Replace(" Was ", " was ");
				destText = destText.Replace(" What ", " what ").Replace(" When ", " when ").Replace(" Where ", " where ").Replace(" Who ", " who ").Replace(" Will ", " will ").Replace(" With ", " with ");
				destText = destText.Replace(" Und ", " und ").Replace(" Www ", " www ").Replace(" And ", " and ");
			}
			catch
			{
				destText = sourceText;
			}

			return destText;
		}

		/// <summary>
		/// Filters the skills.
		/// </summary>
		/// <param name="sourceSkills">The source skills.</param>
		/// <param name="skillType">Type of the skill.</param>
		/// <param name="filterSkills">if set to <c>true</c> [filter skills].</param>
		/// <returns></returns>
		public List<string> FilterSkills(List<string> sourceSkills, SkillType skillType, bool filterSkills)
		{
			var destSkills = new List<string>();

			if (sourceSkills.Count > 0)
			{
				destSkills.AddRange(sourceSkills);

				for (var i = 0; i < sourceSkills.Count; i++)
					sourceSkills[i] = sourceSkills[i].ToLower();

				var query = Repositories.Library.ResumeSkills;

				if (filterSkills)
					query = query.Where(x => x.Type == skillType && x.NoiseSkill == 0 && sourceSkills.Contains(x.Name));
				else
					query = query.Where(x => x.NoiseSkill == 0 && sourceSkills.Contains(x.Name));

				var specializedSkills = query.Select(x => x.Name).Distinct().ToList();

				foreach (var sourceSkill in sourceSkills.Where(sourceSkill => !specializedSkills.Contains(sourceSkill, StringComparer.OrdinalIgnoreCase)))
					destSkills.RemoveAll(destSkill => sourceSkill.Equals(destSkill, StringComparison.OrdinalIgnoreCase));
			}

			return destSkills;
		}		

		#endregion

		/// <summary>
		/// Gets the number of words in the resume.
		/// </summary>
		/// <param name="taggedResume">The tagged resume.</param>
		/// <returns></returns>
    public int GetNumberOfWords(string taggedResume)
    {
			var xslTransform = new XslCompiledTransform();

			var resume = taggedResume.GetResumeFromTaggedResume();

			#region Remove the tags from the xml using xslt so that they're not considered in the final word count

			try
			{
				var assembly = Assembly.GetExecutingAssembly();
				var stream = assembly.GetManifestResourceStream("Focus.Services.Assets.Xslts.ResumeText.xslt");

				if (stream == null)
					throw new Exception("Unable to get ResumeText xslt");

				var reader = new StreamReader(stream);
				var xmlReader = XmlReader.Create(reader);

				xslTransform.Load(xmlReader);

				xmlReader.Close();
				reader.Close();
				stream.Close();
			}
			catch{ }

			#endregion

			var stringWriter = new StringWriter();
			var xmlDocument = new XmlDocument { PreserveWhitespace = true };
			xmlDocument.LoadXml(resume);

			xslTransform.Transform(new XmlNodeReader(xmlDocument), null, stringWriter);

			var resumeText = stringWriter.ToString();

			resumeText = resumeText.Replace(" . ", " ");
			resumeText = resumeText.Replace("\r\n", " ");
			resumeText = resumeText.Replace("\r", " ");
			resumeText = resumeText.Replace(Environment.NewLine, " ");

			var regex = new Regex(@"[ ]{2,}", RegexOptions.None);
			resumeText = regex.Replace(resumeText, @" ");

      // Count the words in the string by splitting them wherever a space is found
			return Regex.Matches(resumeText, @"\S+").Count;
    }

		/// <summary>
		/// Registers the resume.
		/// </summary>
		/// <param name="resumeId">The resume id.</param>
		public void RegisterResume(long resumeId)
		{
			// Resumes are registered under the person id so we need to go and get that
			if (resumeId.IsNull())
			{
				throw new Exception("Resume id was not provided.");
			}

			var resume = Repositories.Core.FindById<Resume>(resumeId);

			if (resume.IsNull())
			{
				throw new Exception(string.Format("Resume {0} was not found.", resumeId));
			}

      List<long> degreeEducationLevelIds = null;
      var degreeId = resume.Person.DegreeId;
      if (degreeId.HasValue)
      {
        if (degreeId.Value > 0)
        {
          degreeEducationLevelIds = new List<long> { degreeId.Value };
        }
        else
        {
          degreeEducationLevelIds =
            Repositories.Library.Query<ProgramAreaDegreeEducationLevelView>()
                        .Where(pad => pad.ProgramAreaId == 0 - degreeId.Value)
                        .Select(pad => pad.DegreeEducationLevelId)
                        .ToList();
        }
      }
      Repositories.Lens.RegisterResume(resume.PersonId.ToString(), resume.ResumeXml, resume.CreatedOn, resume.UpdatedOn, resume.PersonId, resume.Person.EnrollmentStatus, degreeEducationLevelIds, resume.DateOfBirth);
    }

    /// <summary>
    /// Unregisters resumes for a list of person Ids
    /// </summary>
    /// <param name="personIds">The list of person Ids</param>
    public void UnregisterResume(List<long> personIds)
    {
      var personIdsList = personIds.ConvertAll(x => x.ToString(CultureInfo.InvariantCulture));
      Repositories.Lens.UnregisterResume(personIdsList);
    }

    /// <summary>
    /// Unregisters the resume.
    /// </summary>
    /// <param name="resumeId">The resume id.</param>
    /// <param name="personId">The person id.</param>
		public void UnregisterResume(long resumeId, long personId)
		{
      if (personId.IsNull() || personId == 0)
      {
        // Resumes are registered under the person id so we need to go and get that
        if (resumeId.IsNull())
          throw new Exception("Resume id was not provided.");

        personId = Repositories.Core.Resumes.Where(x => x.Id == resumeId).Select(x => x.PersonId).FirstOrDefault();

        if (personId.IsNull() || personId == 0)
          throw new Exception(string.Format("Resume {0} was not found.", resumeId));
      }

      var personIds = new List<string> { personId.ToString(CultureInfo.InvariantCulture) };
      Repositories.Lens.UnregisterResume(personIds);
		}

    /// <summary>
    /// Resets a resume back to being searchable if it had been set to be not searchable when the user was made inactive
    /// </summary>
    /// <param name="request">The current service request.</param>
    /// <param name="personId">The id of the person whose default resume needs to be reset</param>
    public void ResetSearchableDefaultResumeForPerson(ServiceRequest request, long personId)
    {
      var defaultResume = Repositories.Core.Resumes.FirstOrDefault(r => r.PersonId == personId
                                                                        && r.IsDefault
                                                                        && r.StatusId == ResumeStatuses.Active
                                                                        && r.ResumeCompletionStatusId == ResumeCompletionStatuses.Completed
                                                                        && r.ResetSearchable == true);

      if (defaultResume.IsNotNull())
      {
        var resumeXml = XDocument.Parse(defaultResume.ResumeXml);
        var searchableElement = resumeXml.Descendants("resume_searchable").FirstOrDefault();
        if (searchableElement.IsNotNull() && searchableElement.Value != "1")
        {
          searchableElement.SetValue("1");
          defaultResume.ResumeXml = resumeXml.ToString(SaveOptions.DisableFormatting);
        }

        defaultResume.IsSearchable = true;
        defaultResume.ResetSearchable = false;

        Repositories.Core.SaveChanges(true);

        Helpers.Messaging.Publish(request.ToRegisterResumeMessage(defaultResume.Id, personId), updateIfExists: true, entityName: EntityTypes.Resume.ToString(), entityKey: personId);
				Helpers.Logging.LogAction(ActionTypes.SearchableResume, defaultResume);
      }
    }

		/// <summary>
		/// Updates the email in the default resume
		/// </summary>
		/// <param name="request">The current service request.</param>
		/// <param name="personId">The id of the person whose default resume needs to be reset</param>
		/// <param name="emailAddress">The email address</param>
		public void UpdateDefaultResumeEmailField(ServiceRequest request, long personId, string emailAddress)
		{
			var defaultResume = Repositories.Core.Resumes.FirstOrDefault(r => r.PersonId == personId
																																				&& r.IsDefault
																																				&& r.StatusId == ResumeStatuses.Active
																																				&& r.ResumeCompletionStatusId == ResumeCompletionStatuses.Completed);

			if (defaultResume.IsNotNull() && defaultResume.EmailAddress != emailAddress)
			{
				var resumeXml = XDocument.Parse(defaultResume.ResumeXml);
				var emailElement = resumeXml.XPathSelectElements("/ResDoc/resume/contact/email").FirstOrDefault();
				if (emailElement.IsNotNull() && emailElement.Value != emailAddress)
				{
					emailElement.SetValue(emailAddress);
					defaultResume.ResumeXml = resumeXml.ToString(SaveOptions.DisableFormatting);
				}

				defaultResume.EmailAddress = emailAddress;
				Repositories.Core.SaveChanges(true);

				if (defaultResume.IsSearchable)
					Helpers.Messaging.Publish(request.ToRegisterResumeMessage(defaultResume.Id, personId), updateIfExists: true, entityName: EntityTypes.Resume.ToString(), entityKey: personId);
			}
		}

		/// <summary>
		/// Fully unregisters the resume, this includes setting the resume as not searchable in the Focus DB
		/// </summary>
		/// <param name="jobseekerId">The jobseeker id.</param>
		public void FullUnregisterResume(long jobseekerId)
		{
			FullUnregisterResumes(new List<long> { jobseekerId });
		}

		/// <summary>
		/// Fully unregisters the resumes, this includes setting the resumes as not searchable in the Focus DB
		/// </summary>
		/// <param name="jobSeekerIds">The job seeker ids.</param>
		public void FullUnregisterResumes(List<long> jobSeekerIds)
		{
			const string updateSql = @"
UPDATE
	[Data.Application.Resume]
SET
  ResumeXml = REPLACE(ResumeXml, '<resume_searchable>1</resume_searchable>', '<resume_searchable>0</resume_searchable>'),
  IsSearchable = 0,
  ResetSearchable = 1
WHERE
  PersonId IN ({0})
  AND IsDefault = 1
  AND StatusId = 1
  AND IsSearchable = 1
  AND ResumeCompletionStatusId = 127";

			var index = 0;
			while (index < jobSeekerIds.Count)
			{
				// Perform bulk update in batches
				var batchIds = jobSeekerIds.Skip(index).Take(500).ToArray();

				var query = from resume in Repositories.Core.Resumes
										join person in Repositories.Core.Persons
											on resume.PersonId equals person.Id
										join user in Repositories.Core.Users
											on person.Id equals user.PersonId
										where batchIds.Contains(user.Id)
													&& resume.IsDefault
													&& resume.StatusId == ResumeStatuses.Active
													&& resume.IsSearchable
													&& resume.ResumeCompletionStatusId == ResumeCompletionStatuses.Completed
										select person.Id;

				var allPersonIds = query.Distinct().ToList();

				if (allPersonIds.Any())
				{
					var sql = string.Format(updateSql, allPersonIds.AsCommaListForSql());
					Repositories.Core.ExecuteNonQuery(sql);
					Repositories.Core.SaveChanges(true);

					// Unregister resumes in batches of 10
					var subIndex = 0;
					while (subIndex < allPersonIds.Count)
					{
						var personIds = allPersonIds.Skip(subIndex).Take(10).ToList();

						Helpers.Messaging.Publish(new ResumeProcessMessage()
						{
							ActionerId = RuntimeContext.CurrentRequest.UserContext.ActionerId,
							Culture = RuntimeContext.CurrentRequest.UserContext.Culture,
							PersonIds = personIds,
							IgnoreLens = false,
							RegisterResume = false
						});

						subIndex += personIds.Count;
					}
				}

				index += batchIds.Length;
			}
		}
	}

	#endregion

	#region ResumeHelper Interface

	public interface IResumeHelper
	{
		/// <summary>
		/// Updates the status.
		/// </summary>
		/// <param name="resumeId">The resume id.</param>
		/// <param name="status">The status.</param>
		void UpdateStatus(long resumeId, ResumeStatuses status);

		/// <summary>
		/// Generates the automated summary.
		/// </summary>
		/// <param name="taggedResume">The tagged resume.</param>
		/// <param name="filterSkills">if set to <c>true</c> [filter skills].</param>
		/// <param name="removeSpaces">if set to <c>true</c> [remove spaces].</param>
    /// <param name="escapeText">Whether to escape (HTML Encode) the summary</param>
    /// <returns></returns>
		string GenerateAutomatedSummary(string taggedResume, bool filterSkills, bool removeSpaces, bool escapeText);

		/// <summary>
		/// Titles the case text.
		/// </summary>
		/// <param name="sourceText">The source text.</param>
		/// <returns></returns>
		string TitleCase(string sourceText);

		/// <summary>
		/// Filters the skills.
		/// </summary>
		/// <param name="sourceSkills">The source skills.</param>
		/// <param name="skillType">Type of the skill.</param>
		/// <param name="filterSkills">if set to <c>true</c> [filter skills].</param>
		/// <returns></returns>
		List<string> FilterSkills(List<string> sourceSkills, SkillType skillType, bool filterSkills);

		/// <summary>
		/// Gets the number of words in the resume.
		/// </summary>
		/// <param name="taggedResume">The tagged resume.</param>
		/// <returns></returns>
		int GetNumberOfWords(string taggedResume);

		/// <summary>
		/// Registers the resume.
		/// </summary>
		/// <param name="resumeId">The resume id.</param>
		void RegisterResume(long resumeId);

	  /// <summary>
	  /// Unregisters resumes for a list of person Ids
	  /// </summary>
	  /// <param name="personIds">The list of person Ids</param>
	  void UnregisterResume(List<long> personIds);

		/// <summary>
		/// Unregisters the resume.
		/// </summary>
    /// <param name="resumeId">The resume id.</param>
    /// <param name="personId">The person id.</param>
    void UnregisterResume(long resumeId, long personId);

		/// <summary>
		/// Updates the email in the default resume
		/// </summary>
		/// <param name="request">The current service request.</param>
		/// <param name="personId">The id of the person whose default resume needs to be reset</param>
		/// <param name="emailAddress">The email address</param>
		void UpdateDefaultResumeEmailField(ServiceRequest request, long personId, string emailAddress);

    /// <summary>
    /// Resets a resume back to being searchable if it had been set to be not searchable when the user was made inactive
    /// </summary>
    /// <param name="request">The current service request.</param>
    /// <param name="personId">The id of the person whose default resume needs to be reset</param>
	  void ResetSearchableDefaultResumeForPerson(ServiceRequest request, long personId);

		/// <summary>
		/// Fully unregisters the resume, this includes setting the resume as not searchable in the Focus DB
		/// </summary>
		/// <param name="jobseekerId">The jobseeker id.</param>
		void FullUnregisterResume(long jobseekerId);

		/// <summary>
		/// Fully unregisters the resumes, this includes setting the resumes as not searchable in the Focus DB
		/// </summary>
		/// <param name="jobSeekerIds">The job seeker ids.</param>
		void FullUnregisterResumes(List<long> jobSeekerIds);
	}

	#endregion
}