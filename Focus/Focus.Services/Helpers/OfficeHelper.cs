﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using Focus.Core;
using Focus.Core.Criteria.Employer;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.EmailTemplate;
using Focus.Data.Core.Entities;
using Focus.Services.DtoMappers;
using Focus.Services.Queries;
using Framework.Core;

#endregion



namespace Focus.Services.Helpers
{

	#region OfficeHelper Class

	public class OfficeHelper : HelperBase, IOfficeHelper
	{

		/// <summary>
		/// Initializes a new instance of the <see cref="OfficeHelper"/> class.
		/// </summary>
		public OfficeHelper() : base(null) { }

		/// <summary>
		/// Initializes a new instance of the <see cref="OfficeHelper"/> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		public OfficeHelper(IRuntimeContext runtimeContext) : base(runtimeContext) { }

		/// <summary>
		/// Determines whether the specified office id is active.
		/// </summary>
		/// <param name="officeId">The office id.</param>
		/// <returns>
		///   <c>true</c> if office is active; otherwise, <c>false</c>.
		/// </returns>
		public bool IsOfficeActive(long officeId)
		{
			var inActive = Repositories.Core.FindById<Office>(officeId).InActive;

			return !inActive;
		}

		/// <summary>
		/// Gets the office staff members.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		public IQueryable<Person> GetOfficeStaffMembers(StaffMemberCriteria criteria)
		{
			var query = new OfficeStaffMemberQuery(Repositories.Core, criteria);

			return query.Query();
		}

		/// <summary>
		/// Gets the office job seekers.
		/// </summary>
		/// <param name="officeId">The office id.</param>
		/// <returns></returns>
		public List<PersonDto> GetOfficeJobSeekers(long officeId)
		{
			var careerExplorerUserType = UserTypes.Career | UserTypes.Explorer;

			var jobSeekers = (from u in Repositories.Core.Users
			                  join p in Repositories.Core.Persons on u.PersonId equals p.Id
			                  join pom in Repositories.Core.PersonOfficeMappers on p.Id equals pom.PersonId
			                  where pom.OfficeId == officeId &&
			                        (u.UserType == UserTypes.Career || u.UserType == careerExplorerUserType)
			                  select p.AsDto()).ToList();

			return jobSeekers;
		}

		/// <summary>
		/// Gets the office job orders.
		/// </summary>
		/// <param name="officeId">The office id.</param>
		/// <returns></returns>
		public List<JobOfficeMapperDto> GetOfficeJobOrders(long officeId)
		{
			var jobsOrders = (from j in Repositories.Core.JobOfficeMappers
												where j.OfficeId == officeId
														 select j.AsDto()).ToList();

			return jobsOrders;
		}

		/// <summary>
		/// Gets the office employers.
		/// </summary>
		/// <param name="officeId">The office id.</param>
		/// <returns></returns>
		public List<EmployerDto> GetOfficeEmployers(long officeId)
		{
			var employers = (from e in Repositories.Core.Employers
																	join eom in Repositories.Core.EmployerOfficeMappers on e.Id equals eom.EmployerId
																	where eom.OfficeId == officeId
																	select e.AsDto()).ToList();

			return employers;
		}

		/// <summary>
		/// Gets the assigned zips.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		public string GetAssignedZips(OfficeCriteria criteria)
		{
      var query = new OfficeQuery(Repositories.Core, criteria);

			var assignedZip = query.Query().Where(x => !string.IsNullOrEmpty(x.AssignedPostcodeZip)).Select(x => x.AssignedPostcodeZip).FirstOrDefault();
	
			return assignedZip;
		}

		/// <summary>
		/// Gets the office ids for postcode zip.
		/// </summary>
		/// <param name="postcodeZip">The postcode zip.</param>
		/// <returns></returns>
		public List<long> GetOfficeIdsForPostcodeZip(string postcodeZip)
		{
			return Repositories.Core.Offices.Where(x => x.AssignedPostcodeZip.Contains(postcodeZip)).Select(x => x.Id).ToList();
		}

    /// <summary>
    /// Gets the default office identifier.
    /// </summary>
    /// <param name="defaultType">The default office type.</param>
    /// <returns></returns>
		public long? GetDefaultOfficeId(OfficeDefaultType defaultType)
		{
      return Repositories.Core.Offices.Where(x => (x.DefaultType & defaultType) == defaultType).Select(x => x.Id).FirstOrDefault();
		}

		/// <summary>
		/// Gets the hiring manager office email.
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <returns></returns>
		public string GetHiringManagerOfficeEmail(long personId)
		{
			return (from o in Repositories.Core.Offices
			        join pco in Repositories.Core.PersonsCurrentOffices on o.Id equals pco.OfficeId
			        where pco.PersonId.Equals(personId)
			        orderby pco.StartTime descending
			        select o.OfficeManagerMailbox).FirstOrDefault();
		}
	}

	#endregion

	#region OfficeHelper Interface

	public interface IOfficeHelper
	{
		bool IsOfficeActive(long officeId);
		IQueryable<Person> GetOfficeStaffMembers(StaffMemberCriteria criteria);
		List<PersonDto> GetOfficeJobSeekers(long officeId);
		List<JobOfficeMapperDto> GetOfficeJobOrders(long officeId);
		List<EmployerDto> GetOfficeEmployers(long officeId);
		string GetAssignedZips(OfficeCriteria criteria);
		List<long> GetOfficeIdsForPostcodeZip(string postcodeZip);
    long? GetDefaultOfficeId(OfficeDefaultType defaultType);
		string GetHiringManagerOfficeEmail(long personId);
	}

	#endregion

}
