﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Linq;
using Focus.Data.Repositories.Contracts;
using Focus.Services.Core;
using Framework.Core;

#endregion

namespace Focus.Services.Helpers
{
	#region OrganizationHelper Class

	public class OrganizationHelper : HelperBase, IOrganizationHelper
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="OrganizationHelper" /> class.
		/// </summary>
		public OrganizationHelper() : this(null)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="OrganizationHelper" /> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		public OrganizationHelper(IRuntimeContext runtimeContext) : base(runtimeContext)
		{ }

		/// <summary>
		/// Gets the excluded lens postings.
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <returns></returns>
		public List<string> GetExcludedLensPostings(long personId)
		{
			// Get a list of excluded postings for the person
			var excludedLensPostings = Repositories.Core.ExcludedLensPostings.Where(x => x.PersonId == personId);

			var excludedLensPostingIds = new List<string>();

			// Get a list of the Lens Posting IDs
			if (excludedLensPostings.IsNotNullOrEmpty())
				excludedLensPostingIds = excludedLensPostings.Select(x => x.LensPostingId).ToList();

			return excludedLensPostingIds;
		}
	}

	#endregion

	#region OrganizationHelper Interface

	public interface IOrganizationHelper
	{
		/// <summary>
		/// Gets the excluded lens postings.
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <returns></returns>
		List<string> GetExcludedLensPostings(long personId);
	}

	#endregion
}