﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusExplorer;
using Focus.Data.Core.Entities;
using Focus.Data.Library.Entities;
using Focus.Services.Core.Extensions;
using Focus.Services.DtoMappers;
using Focus.Services.Repositories.Lens;

using Framework.Core;

#endregion

namespace Focus.Services.Helpers
{
	#region PostingHelper Class

	public class PostingHelper : HelperBase, IPostingHelper
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="PostingHelper"/> class.
		/// </summary>
		public PostingHelper() : base(null) {}

		/// <summary>
		/// Initializes a new instance of the <see cref="PostingHelper"/> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		public PostingHelper(IRuntimeContext runtimeContext) : base(runtimeContext) {}

		/// <summary>
		/// Registers the posting.
		/// </summary>
		/// <param name="postingId">The posting id.</param>
		public void RegisterPosting(long postingId)
		{
		  if (postingId.IsNull())
			{
				throw new Exception("Posting id was not provided.");
			}

			var posting = Repositories.Core.FindById<Posting>(postingId);

			if (posting.IsNull())
			{
				throw new Exception(string.Format("Posting {0} was not found.", postingId));
			}

			if (posting.LensPostingId.IsNullOrEmpty())
			{
				posting.LensPostingId = String.Format("{0}_{1}", AppSettings.ClientTag, Guid.NewGuid().ToString("N").ToUpper());
				Repositories.Core.SaveChanges();
			}
			
			if (posting.PostingXml.IsNotNullOrEmpty())
			{
				var taggedPosting = CanonPostingXml(posting.PostingXml);
				var dataElementsXml = taggedPosting.GetJobMineDataElementsFromTaggedPosting();

				// TODO: Martha (Low) - Vendor service Posting.UpdateTaggedPostingContent possibly needs to be plugged in here

				var mappedRequiredProgramsOfStudy = new List<int>();

				if (AppSettings.Theme == FocusThemes.Education)
				{
					// If this is education theme get any mapped required programs of study so we can add them to the mapped programs of study programs later
					var requiredProgramsOfStudy = Repositories.Core.JobProgramsOfStudy.Where(x => x.JobId == posting.JobId).Select(x => x.DegreeEducationLevelId).ToList();

					if(requiredProgramsOfStudy.IsNotNullOrEmpty())
					{
						mappedRequiredProgramsOfStudy = Repositories.Library.Query<DegreeEducationLevelLensMapping>().Where(x => requiredProgramsOfStudy.Contains((int)x.DegreeEducationLevelId)).Select(x => x.LensId).Distinct().ToList();
					}
				}

        var postingXmlDoc = posting.PostingXml.IsNotNullOrEmpty() ? XDocument.Parse(posting.PostingXml) : null;
        PostalCodeDto postalCodeDto = null;
        if (postingXmlDoc.IsNotNull())
        {
          var postalCode = postingXmlDoc.XPathAsString("//address/postalcode");
          if (postalCode.IsNotNullOrEmpty())
            postalCodeDto = Repositories.Library.Query<PostalCode>().Where(pc => pc.Code == postalCode).Select(pc => pc.AsDto()).FirstOrDefault();
        }
        Repositories.Lens.RegisterPosting(posting.LensPostingId, posting.Id, postingXmlDoc, dataElementsXml, RuntimeContext.CurrentRequest.UserContext.Culture, mappedRequiredProgramsOfStudy, postalCodeDto);
      }
		}

		/// <summary>
		/// Unregisters the posting.
		/// </summary>
		/// <param name="postingId">The posting id.</param>
		public void UnregisterPosting(long postingId)
		{
			if (postingId.IsNull())
			{
				throw new Exception("Posting id was not provided.");
			}

			var posting = Repositories.Core.FindById<Posting>(postingId);

			if (posting.IsNull())
			{
				throw new Exception(string.Format("Posting {0} was not found.", postingId));
			}

			if (posting.LensPostingId.IsNotNullOrEmpty())
				Repositories.Lens.UnregisterPosting(posting.LensPostingId);
		}

		/// <summary>
		/// Canons the posting XML.
		/// </summary>
		/// <param name="postingXml">The posting XML.</param>
		/// <returns></returns>
		private string CanonPostingXml(string postingXml)
		{
			var jobDocXml = postingXml.GetJobDocFromXml();

			var canonResult = (AppSettings.LensServices.Any(x => x.ServiceType == LensServiceTypes.JobMine)) ? Repositories.Lens.CanonPostingWithJobMine(jobDocXml) : Repositories.Lens.Canon(jobDocXml);

			var canonnedJobDocXml = canonResult.GetJobDocFromXml();

			return postingXml.UpdateJobDocInTaggedPosting(canonnedJobDocXml);
		}
	}

	#endregion

	#region PostingHelper Interface

	public interface IPostingHelper
	{
		/// <summary>
		/// Registers the posting.
		/// </summary>
		/// <param name="postingId">The posting id.</param>
		void RegisterPosting(long postingId);

		/// <summary>
		/// Unregisters the posting.
		/// </summary>
		/// <param name="postingId">The posting id.</param>
		void UnregisterPosting(long postingId);
	}

	#endregion

}
