﻿#region Copyright © 2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.EnterpriseServices;
using System.Globalization;
using System.Linq;
using System.Xml.Linq;
using System.Xml.XPath;
using Focus.Core;
using Focus.Core.Criteria.CandidateSearch;
using Focus.Core.EmailTemplate;
using Focus.Core.Models.Career;
using Focus.Core.Views;
using Focus.Data.Core.Entities;
using Focus.Data.Library.Entities;
using Focus.Services.Core.Extensions;

using Job = Focus.Data.Core.Entities.Job;

using Framework.Core;

using Mindscape.LightSpeed;
using Query = Mindscape.LightSpeed.Querying.Query;

#endregion

namespace Focus.Services.Helpers
{
	#region JobDevelopmentHelper Class

	public class JobDevelopmentHelper : HelperBase, IJobDevelopmentHelper
	{
		private class PostedJobModel
		{
			public long Id { get; set; }
			public DateTime? ClosingOn { get; set; }
			public bool? ClosingOnUpdated { get; set; }
			public ExtendVeteranPriorityOfServiceTypes? ExtendVeteranPriority { get; set; }
			public DateTime? VeteranPriorityEndDate { get; set; }
			public DateTime? PostedOn { get; set; }
			public DateTime? LastPostedOn { get; set; }
			public DateTime? LastRefreshedOn { get; set; }
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="JobDevelopmentHelper"/> class.
		/// </summary>
		public JobDevelopmentHelper()
			: base(null)
		{

		}

		/// <summary>
		/// Initializes a new instance of the <see cref="JobDevelopmentHelper"/> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		public JobDevelopmentHelper(IRuntimeContext runtimeContext)
			: base(runtimeContext)
		{
		}

	#endregion

		#region Matches to recently placed job seekers

		/// <summary>
		/// Refreshes the matches to all recently placed job seekers.
		/// </summary>
		/// <param name="daysBack">The days back.</param>
		public void RefreshMatchesToAllRecentlyPlacedJobSeekers(int? daysBack = null)
		{
			// Delete those who no longer meet the date range
			TrimRecentlyPlacedJobSeekers(daysBack);
			// Get all job seekers we need to find matches for
			var jobSeekers = Repositories.Core.RecentlyPlaced.Select(x => x.PersonId).Distinct().ToList();
			// Process if any found
			if (jobSeekers.IsNotNullOrEmpty())
				RefreshMatchesToRecentlyPlacedJobSeekers(jobSeekers);
		}

		/// <summary>
		/// Refreshes the matches to recently placed job seekers.
		/// </summary>
		/// <param name="jobSeekers">The job seekers.</param>
		public void RefreshMatchesToRecentlyPlacedJobSeekers(List<long> jobSeekers)
		{
			if (jobSeekers.IsNullOrEmpty()) return;
			// Loop through all job seekers we need to process
			foreach (var jobSeeker in jobSeekers.Distinct())
			{
				RefreshMatchesToRecentlyPlacedJobSeeker(jobSeeker);
			}
		}

		/// <summary>
		/// Refreshes the matches to recently placed job seeker.
		/// </summary>
		/// <param name="jobSeekerId">The job seeker identifier.</param>
		public void RefreshMatchesToRecentlyPlacedJobSeeker(long jobSeekerId)
		{
			// Delete existing matches
			Repositories.Core.Remove<RecentlyPlacedMatch>(x => x.RecentlyPlaced.PersonId == jobSeekerId);
			Repositories.Core.SaveChanges();

			// Get all recently placerd records, there may be more than one per job seeker
			var recentlyPlaced = Repositories.Core.RecentlyPlaced.Where(x => x.PersonId == jobSeekerId).ToList();

			if (recentlyPlaced.IsNullOrEmpty()) return;

			var culture = Constants.DefaultCulture;
			if (RuntimeContext.CurrentRequest.IsNotNull() && RuntimeContext.CurrentRequest.UserContext.IsNotNull())
				culture = RuntimeContext.CurrentRequest.UserContext.Culture;

			// Get all matches for the job seeekr
			var matches = (from c in GetCandidatesLikeThis(jobSeekerId, culture)
										 select new RecentlyPlacedMatch
										 {
											 Score = c.Score,
											 PersonId = c.Id
										 }).ToList();

			if (matches.IsNullOrEmpty()) return;

			// Loop through each
			foreach (var jobseeker in recentlyPlaced)
			{
				jobseeker.RecentlyPlacedMatches.AddRange(matches);
			}
			Repositories.Core.SaveChanges();
		}

		/// <summary>
		/// Trims the recently placed job seekers to those within the required date range
		/// </summary>
		/// <param name="daysBack">The days back to go when deleting.</param>
		private void TrimRecentlyPlacedJobSeekers(int? daysBack = null)
		{
			if (daysBack.IsNull())
				daysBack = AppSettings.RecentPlacementTimeframe;

			Repositories.Core.Remove<RecentlyPlaced>(x => x.PlacementDate < DateTime.Now.Date.AddDays(-1 * daysBack.Value));
			Repositories.Core.SaveChanges();
		}

		/// <summary>
		/// Checks whether the veteran is eligible for the veteran prority job
		/// </summary>
		/// <param name="postingPostalCode">The posting location details</param>
		/// <param name="candidateId">The candidiate id of the veteran</param>
		/// <returns></returns>
		private bool IsPostingMatchesToVeteran(PostalCode postingPostalCode, long candidateId)
		{
			var isMatch = false;
			var resume = Repositories.Core.Resumes.Where(x => x.PersonId == candidateId && x.IsDefault).Select(x => x.ResumeXml).FirstOrDefault();

			if (resume.IsNotNullOrEmpty())
			{

				var resumeXml = XDocument.Parse(resume);
				var preferences = resumeXml.XPathSelectElement("//preferences");

				if (preferences.IsNull())
				{
					var postalCode = resumeXml.XPathSelectElement("//contact/address/postalcode");
					var personPostalCode = postalCode.IsNotNull() && postalCode.Value.IsNotNullOrEmpty() ? Repositories.Library.Query<PostalCode>().FirstOrDefault(pc => pc.Code == postalCode.Value) : null;

					isMatch = (postingPostalCode.IsNotNull() && personPostalCode.IsNotNull() && GetEarthDistance(personPostalCode, postingPostalCode) <= Convert.ToDouble(100));
				}
				else
				{
					var relocate = preferences.XPathSelectElement("relocate");

					if (relocate.IsNotNull() && relocate.Value.IsNotNullOrEmpty() && relocate.Value.AsBoolean())
						isMatch = true;

					else if (postingPostalCode.IsNotNull())
					{
						var desiredZip = preferences.XPathSelectElement("//desired_zips/desired_zip");
						var desiredMSA = preferences.XPathSelectElement("//desired_msas/desired_msa");

						if (desiredZip.IsNull() && desiredMSA.IsNull())
						{
							var postalCode = resumeXml.XPathSelectElement("//contact/address/postalcode");
							var personPostalCode = postalCode.IsNotNull() && postalCode.Value.IsNotNullOrEmpty() ? Repositories.Library.Query<PostalCode>().FirstOrDefault(pc => pc.Code == postalCode.Value) : null;

							isMatch = (personPostalCode.IsNotNull() && GetEarthDistance(personPostalCode, postingPostalCode) <= Convert.ToDouble(100));
						}
						else if (desiredZip.IsNotNull())
						{
							var zip = desiredZip.XPathSelectElement("zip");
							var radius = desiredZip.XPathSelectElement("radius");

							var personPostalCode = zip.IsNotNull() && zip.Value.IsNotNullOrEmpty() ? Repositories.Library.Query<PostalCode>().FirstOrDefault(pc => pc.Code == zip.Value) : null;

							isMatch = (personPostalCode.IsNotNull() && GetEarthDistance(personPostalCode, postingPostalCode) <= Convert.ToDouble(radius.IsNotNull() && radius.Value.IsNotNullOrEmpty() ? radius.Value : "100"));

							var state = preferences.XPathSelectElement("//desired_states/desired_state/code");

							if (isMatch && state.IsNotNull() && state.Value.IsNotNullOrEmpty() && postingPostalCode.StateKey.IsNotNullOrEmpty())
							{
								isMatch = (postingPostalCode.StateKey.Substring(postingPostalCode.StateKey.IndexOf(".", StringComparison.InvariantCulture) + 1) == state.Value);
							}
						}
						else if (desiredMSA.IsNotNull())
						{
							var state = desiredMSA.XPathSelectElement("code");
							var msa = desiredMSA.XPathSelectElement("msa");

							if (msa.IsNotNull() && msa.Value.IsNotNullOrEmpty())
								isMatch = (postingPostalCode.MsaId.IsNotNullOrEmpty() && postingPostalCode.MsaId == msa.Value);
							else
								isMatch = (state.IsNotNull() && state.Value.IsNotNullOrEmpty() && postingPostalCode.StateKey.IsNotNullOrEmpty() && postingPostalCode.StateKey.Substring(postingPostalCode.StateKey.IndexOf(".", StringComparison.InvariantCulture) + 1) == state.Value);
						}
					}
				}
			}
			return isMatch;
		}

		/// <summary>
		/// Returns the earth distance between two postal codes
		/// </summary>
		/// <param name="origin">The origin postal code</param>
		/// <param name="destination">The destination postal code</param>
		/// <returns></returns>
		private Double GetEarthDistance(PostalCode origin, PostalCode destination)
		{
			var radius = 0.0;

			try
			{
				if (origin.Code == destination.Code)
					return radius;

				const double pi = Math.PI / 180.0;
				const double earthRadius = 6378.7;

				// convert all to radians
				double latOrigin = origin.Latitude * pi;
				double lonOrigin = origin.Longitude * pi;
				double latDest = destination.Latitude * pi;
				double lonDest = destination.Longitude * pi;

				double cosA = Math.Sin(latOrigin) * Math.Sin(latDest) + Math.Cos(latOrigin) * Math.Cos(latDest) * Math.Cos(lonDest - lonOrigin);

				radius = earthRadius * Math.Atan2(Math.Sqrt(1.0 - cosA * cosA), cosA);
			}
			catch (Exception)
			{ }

			return radius;
		}

		/// <summary>
		/// Generates the recently placed matches.
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <param name="applicationId">The application id.</param>
		/// <param name="culture">The culture.</param>
		//public void GenerateRecentlyPlacedMatches(long personId, long applicationId, string culture)
		//{
		//  RemoveOldRecentlyPlacedAndMatches();

		//  var application = Repositories.Core.FindById<Application>(applicationId);
		//  var resume = application.Resume;
		//  var posting = application.Posting;

		//  //  if recentlyPlaced already exists, just update the placement date

		//  var recentlyPlaced = Repositories.Core.RecentlyPlaced.SingleOrDefault(x => x.JobId == posting.JobId && x.PersonId == resume.PersonId);

		//  if (recentlyPlaced == null)
		//  {
		//    recentlyPlaced = new RecentlyPlaced
		//    {
		//      PlacementDate = application.UpdatedOn,
		//      Job = posting.Job,
		//      Person = resume.Person
		//    };

		//    Repositories.Core.Add(recentlyPlaced);
		//  }
		//  else
		//  {
		//    recentlyPlaced.PlacementDate = application.UpdatedOn;

		//    //clear any existing matches
		//    var existingMatches = Repositories.Core.RecentlyPlacedMatches.Where(x => x.RecentlyPlacedId == recentlyPlaced.Id);

		//    foreach (var recentlyPlacedMatch in existingMatches)
		//    {
		//      Repositories.Core.Remove(recentlyPlacedMatch);
		//    }
		//  }

		//  var recentlyPlacedMatches = from c in GetCandidatesLikeThis(resume.PersonId, culture)
		//                              select new RecentlyPlacedMatch
		//                              {
		//                                Score = c.Score,
		//                                PersonId = c.Id,
		//                                RecentlyPlaced = recentlyPlaced
		//                              };

		//  foreach (var recentlyPlacedMatch in recentlyPlacedMatches)
		//    Repositories.Core.Add(recentlyPlacedMatch);

		//  Repositories.Core.SaveChanges();
		//}

		/// <summary>
		/// Generates the person matches for job.
		/// </summary>
		/// <param name="postingId">The posting id.</param>
		/// <param name="isVeteranJob">Whether this is a veteran job (WOTC specifies Veterans)</param>
		/// <param name="culture">The culture.</param>
		/// <param name="ignoreClient">Whether to ignore the call to the client system</param>
		public void GeneratePersonMatchesForPosting(long postingId, bool isVeteranJob, string culture, bool ignoreClient = false)
		{
			var postingDetails = Repositories.Core.Postings.Where(x => x.Id == postingId)
																										 .Select(x => new
																										 {
																											 x.JobId,
																											 x.JobTitle,
																											 x.Html,
																											 x.LensPostingId,
																											 x.ExternalId,
																											 x.PostingXml
																										 }).FirstOrDefault();

			#region Call the Lens repository

			var jobDetailsCriteria = new CandidateSearchJobDetailsCriteria
			{
				Posting = (postingDetails.IsNotNull() && postingDetails.Html.IsNotNullOrEmpty()) ? postingDetails.Html : (postingDetails.IsNotNull() && postingDetails.JobId.HasValue) ? Repositories.Core.Jobs.Where(x => x.Id == postingDetails.JobId).Select(x => x.PostingHtml).SingleOrDefault() : "",
				JobId = (postingDetails.IsNotNull() && postingDetails.JobId.HasValue) ? postingDetails.JobId.Value : 0
			};

			var criteria = new CandidateSearchCriteria
			{
				JobDetailsCriteria = jobDetailsCriteria,
				MinimumScore = AppSettings.JobDevelopmentScoreThreshold,
				MinimumDocumentCount = 250,
				SearchType = CandidateSearchTypes.ByJobDescription
			};

			var candidates = Repositories.Lens.SearchResumes(criteria, culture);

			#endregion

			if (jobDetailsCriteria.JobId > 0)
			{
				var postedJob = Repositories.Core.Jobs.Where(job => job.Id == jobDetailsCriteria.JobId)
																												.Select(job => new PostedJobModel
																													{
																														Id = job.Id,
																														ClosingOn = job.ClosingOn,
																														ClosingOnUpdated = job.ClosingOnUpdated,
																														ExtendVeteranPriority = job.ExtendVeteranPriority,
																														VeteranPriorityEndDate = job.VeteranPriorityEndDate,
																														PostedOn = job.PostedOn,
																														LastPostedOn = job.LastPostedOn,
																														LastRefreshedOn = job.LastRefreshedOn
																													})
																																														.FirstOrDefault();

				if (postedJob.IsNotNull())
				{
					if (AppSettings.VeteranPriorityServiceEnabled && isVeteranJob)
					{
						//  Search purely for veterans
						var veteranScore = AppSettings.StarRatingMinimumScores[AppSettings.VeteranPriorityStarMatching];

						criteria.MinimumScore = veteranScore;
						criteria.AdditionalCriteria = new CandidateSearchAdditionalCriteria
						{
							StatusCriteria = new CandidateSearchStatusCriteria
							{
								Veteran = true
							},
						};

						if (postedJob.ExtendVeteranPriority == ExtendVeteranPriorityOfServiceTypes.ExtendForLifeOfPosting && postedJob.ClosingOnUpdated.HasValue && (bool)postedJob.ClosingOnUpdated)
						{
							var veteranPriorityEndDate = Convert.ToDateTime(postedJob.ClosingOn);
							var ts = new TimeSpan(23, 59, 59);
							veteranPriorityEndDate = veteranPriorityEndDate.Date + ts;

							postedJob.VeteranPriorityEndDate = veteranPriorityEndDate;
							postedJob.ClosingOnUpdated = false;
						}

						var isNewPosting = (postingDetails.IsNotNull() && postedJob.PostedOn == postedJob.LastPostedOn && postedJob.LastRefreshedOn.IsNull());
						var baseDate = postedJob.PostedOn ?? DateTime.Now;
						var autoVeteranDate = baseDate.AddHours(AppSettings.VeteranPriorityExclusiveHours);

						// Always check for veterans on new postings
						// For existing postings, only check for veterans if the job had previously been manually extended, but had then been switched off (indicated by the veteran end date being set to 01/01/1900. See JobWizardStep7.ascx.cs)
						// Note that in the latter case, the search on veterans is only needed if still within 24 hours of the first posted date
						candidates = isNewPosting || (autoVeteranDate > DateTime.Now && postedJob.VeteranPriorityEndDate.IsNotNull() && postedJob.VeteranPriorityEndDate.Value.Year == 1900)
	? Repositories.Lens.SearchResumes(criteria, culture)
	: new List<ResumeView>();

						if (candidates.Count > 0)
						{
							var jobProperties = Repositories.Core.Jobs.Where(job => job.Id == jobDetailsCriteria.JobId).Select(j => new { j.SuitableForHomeWorker, j.JobLocationType }).FirstOrDefault();

							if (jobProperties.IsNotNull() && !(jobProperties.SuitableForHomeWorker.AsBoolean() || (jobProperties.JobLocationType.IsNotNull() && jobProperties.JobLocationType == JobLocationTypes.NoFixedLocation)))
							{
								var postingXml = XDocument.Parse(postingDetails.PostingXml);
								var postingZip = new string(postingXml.XPathSelectElement("//address/postalcode").Value.Where(char.IsNumber).Take(5).ToArray());

								var postingPostalCode = postingZip.IsNotNullOrEmpty() && postingZip.Length == 5 ? Repositories.Library.Query<PostalCode>().FirstOrDefault(pc => pc.Code == postingZip) : null;
								
								foreach (var candidate in candidates.ToList())
								{
									if (!IsPostingMatchesToVeteran(postingPostalCode, candidate.Id))
									{
										candidates.Remove(candidate);
									}
								}
							}
						}

						if (candidates.Count > 0)
						{
							if ((postedJob.ExtendVeteranPriority.IsNull() || postedJob.ExtendVeteranPriority == ExtendVeteranPriorityOfServiceTypes.None)
													&& (!postedJob.VeteranPriorityEndDate.HasValue || postedJob.VeteranPriorityEndDate.Value.Year == 1900))
							{
								postedJob.VeteranPriorityEndDate = autoVeteranDate;
							}

							UpdateJobDetails(postedJob.Id, postedJob);

							if (isNewPosting)
							{
								var careerPath = AppSettings.CareerApplicationPath.EndsWith("/")
																	 ? AppSettings.CareerApplicationPath
																	 : string.Concat(AppSettings.CareerApplicationPath, "/");

								var jobLinkUrl = string.Format("{0}jobposting/{1}/invite/email", careerPath, postingDetails.LensPostingId);

								var candidateIds = candidates.Select(candidate => candidate.Id).ToList();
								var candidateEmails = Repositories.Core.CandidateViews
																											 .Where(candidate => candidateIds.Contains(candidate.Id) && candidate.VeteranPriorityServiceAlertsSubscription)
																											 .ToList();

								foreach (var candidate in candidateEmails)
								{
									var emailTemplateData = new EmailTemplateData
									{
										RecipientName = candidate.FirstName,
										JobLinkUrls = new List<string> { jobLinkUrl }
									};
									var emailTemplate = Helpers.Email.GetEmailTemplatePreview(EmailTemplateTypes.VeteranPriorityPosting, emailTemplateData);

									Helpers.Email.SendEmail(candidate.EmailAddress, "", "", emailTemplate.Subject, emailTemplate.Body, detectUrl: true);
								}
							}
						}
						else
						{
							if (postedJob.VeteranPriorityEndDate.IsNotNull() && postedJob.VeteranPriorityEndDate.Value.Year == 1900)
							{
								postedJob.VeteranPriorityEndDate = null;
							}

							postedJob.ClosingOnUpdated = false;
							UpdateJobDetails(postedJob.Id, postedJob);
						}
					}
					else
					{
						postedJob.ClosingOnUpdated = false;
						UpdateJobDetails(postedJob.Id, postedJob);
					}
				}
			}

			// Get person to posting matches
			var personPostingMatches = Repositories.Core.PersonPostingMatches.Where(x => x.PostingId == postingId);

			// Remove matches for requested posting
			foreach (var personPostingMatch in personPostingMatches)
				Repositories.Core.Remove(personPostingMatch);

			var newPersonPostingMatches = from c in candidates
																		select new PersonPostingMatch
																		{
																			Score = c.Score,
																			PostingId = postingId,
																			PersonId = c.Id
																		};

			foreach (var newPersonPostingMatch in newPersonPostingMatches)
				Repositories.Core.Add(newPersonPostingMatch);

			Repositories.Core.SaveChanges();
		}

		/// <summary>
		/// Updates the job details. 
		/// </summary>
		/// <param name="jobId">The id of the job</param>
		/// <param name="jobModel">A limited model of job fields</param>
		/// <remarks>
		/// This is done to reduce the chance of an optimistic concurrency check violation. It is done via an update query so that the LockVersion is not updated for two reasons:
		/// 1) Previously, the job entity was read from the database, then a lens search was performed before updating. This delay could result in the message bus update failing if the job was updated on the front-end in the meantime.
		/// 2) It is also to prevent users who update the job in the job wizard getting a optimistic concurrency check if the message bus updates the job whilst it is being edited.
		///    THIS IS A TEMPORARY HACK BEFORE A MORE PERMANENT SOLUTION IS PUT IN PLACE. SPEAK TO JIM!
		/// </remarks>
		private void UpdateJobDetails(long jobId, PostedJobModel jobModel)
		{
			var job = Repositories.Core.Jobs.FirstOrDefault(j => j.Id == jobId);
			if (job.IsNull())
				return;

			var changes = new Dictionary<string, object>
			{
				{"VeteranPriorityEndDate", jobModel.VeteranPriorityEndDate},
				{"ClosingOnUpdated", jobModel.ClosingOnUpdated}
			};

			var updateQuery = new Query(typeof(Job), Entity.Attribute("Id") == jobId);
			Repositories.Core.Update(updateQuery, changes);

			Repositories.Core.SaveChanges();
		}

		/// <summary>
		/// Generates the posting matches for a person.
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <param name="culture">The culture.</param>
		/// <param name="ignoreClient">Whether to ignore the call to the client system</param>
		public void GeneratePostingMatchesForPerson(long personId, string culture, bool ignoreClient = false)
		{
			var resumeId = Repositories.Core.Resumes.Where(x => x.PersonId == personId && x.IsDefault).Select(x => x.Id).FirstOrDefault();

			var matchScore = AppSettings.JobDevelopmentScoreThreshold;
			var minimumStars = matchScore.ToStarMatching(AppSettings.StarRatingMinimumScores);

			if (resumeId > 0)
			{
				var criteria = new SearchCriteria
												{
													SearchType = PostingSearchTypes.Postings,
													RequiredResultCriteria = new RequiredResultCriteria { MaximumDocumentCount = AppSettings.JobDevelopmentMaximumPostingMatchesCount, MinimumStarMatch = minimumStars, DocumentsToSearch = DocumentType.Posting },
													ReferenceDocumentCriteria = new ReferenceDocumentCriteria { DocumentType = DocumentType.Resume, DocumentId = resumeId.ToString(CultureInfo.InvariantCulture) }
												};

				var postings = Repositories.Lens.SearchPostings(criteria, culture, false);
				var lensPostingIds = postings.Select(p => p.Id).ToList();

				var newPersonPostingMatches = new List<PersonPostingMatch>();
				var jobPostings = (from p in Repositories.Core.Postings
													 where lensPostingIds.Contains(p.LensPostingId)
													 select new
														 {
															 p.LensPostingId,
															 p.Id
														 }).ToDictionary(obj => obj.LensPostingId, obj => obj.Id);

				foreach (var posting in postings)
				{
					long postingId;

					if (!jobPostings.ContainsKey(posting.Id))
					{
						var newPosting = new Posting
															{
																LensPostingId = posting.Id,
																JobTitle = (posting.JobTitle.IsNotNullOrEmpty()) ? posting.JobTitle.TruncateString(255) : "Unknown",
																StatusId = PostingStatuses.Active,
																OriginId = (posting.OriginId > 0) ? posting.OriginId : 999,
																EmployerName = (posting.Employer.IsNotNullOrEmpty()) ? posting.Employer : "Unknown",
																Html = "To be loaded"
															};

						Repositories.Core.Add(newPosting);

						postingId = newPosting.Id;
					}
					else
					{
						postingId = jobPostings[posting.Id];
					}

					newPersonPostingMatches.Add(new PersonPostingMatch
																			{
																				Score = posting.Rank,
																				PostingId = postingId,
																				PersonId = personId
																			});
				}

				// Remove person to posting matches
				var removeRecords = new Query(typeof(PersonPostingMatch), Entity.Attribute("PersonId") == personId);
				Repositories.Core.Remove(removeRecords);
				Repositories.Core.SaveChanges();

				//Repositories.Core.Remove<PersonPostingMatch>(x => x.PersonId == personId);

				foreach (var newPersonPostingMatch in newPersonPostingMatches)
					Repositories.Core.Add(newPersonPostingMatch);

				Repositories.Core.SaveChanges();
			}
		}

		/// <summary>
		/// Cleans the person posting matches.
		/// </summary>
		/// <param name="postingId">The posting id.</param>
		/// <param name="personId">The person id.</param>
		public void CleanPersonPostingMatches(long postingId, long personId)
		{
			// Get person to posting matches
			var personPostingMatches = Repositories.Core.PersonPostingMatches;

			if (postingId != 0)
				personPostingMatches = personPostingMatches.Where(x => x.PostingId == postingId);

			if (personId != 0)
				personPostingMatches = personPostingMatches.Where(x => x.PersonId == personId);

			// Remove matches for requested job/person
			foreach (var personPostingMatch in personPostingMatches)
				Repositories.Core.Remove(personPostingMatch);

			Repositories.Core.SaveChanges();
		}
		#endregion

		#region Helper Methods

		/// <summary>
		/// Gets the candidates like this.
		/// </summary>
		/// <param name="personId">The person id.</param>
		/// <param name="culture">The culture.</param>
		/// <returns></returns>
		private List<ResumeView> GetCandidatesLikeThis(long personId, string culture)
		{
			var criteria = new CandidateSearchCriteria
			{
				CandidatesLikeThisSearchCriteria = new CandidatesLikeThisCriteria { CandidateId = personId.ToString(CultureInfo.InvariantCulture) },
				MinimumScore = AppSettings.JobDevelopmentScoreThreshold,
				MinimumDocumentCount = AppSettings.JobDevelopmentMinimumRecentMatchesCount,
				SearchType = CandidateSearchTypes.CandidatesLikeThis
			};

			List<ResumeView> candidates;

			try
			{
				candidates = Repositories.Lens.SearchResumes(criteria, culture);
			}

			// Empty catch block to avoid problem of lens not finding candidate
			catch
			{
				candidates = new List<ResumeView>();
			}

			// Remove the original candidate from returned list
			return candidates.Where(x => x.Id != personId).ToList();
		}
	}

		#endregion




	#region JobDevelopmentHelper Interface

	public interface IJobDevelopmentHelper
	{
		void GeneratePersonMatchesForPosting(long postingId, bool isVeteranJob, string culture, bool ignoreClient = false);
		void GeneratePostingMatchesForPerson(long personId, string culture, bool ignoreClient = false);
		void CleanPersonPostingMatches(long postingId, long personId);

		void RefreshMatchesToAllRecentlyPlacedJobSeekers(int? daysBack = null);
		void RefreshMatchesToRecentlyPlacedJobSeekers(List<long> jobSeekers);
		void RefreshMatchesToRecentlyPlacedJobSeeker(long jobSeekerId);
	}

	#endregion
}
