﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using Focus.Core.Settings.Interfaces;
using Focus.Data.Configuration.Entities;
using Framework.Caching;

using Focus.Core;
using Focus.Core.Views;

using Framework.Core;

#endregion

namespace Focus.Services.Core
{
	#region ConfigurationHelper Class

	public class ConfigurationHelper : IConfigurationHelper
	{
		protected static readonly object SyncLock = new object();

		/// <summary>
		/// Initializes a new instance of the <see cref="ConfigurationHelper" /> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		public ConfigurationHelper(IRuntimeContext runtimeContext)
		{
			if (runtimeContext == null)
				throw new ArgumentNullException("runtimeContext");

			RuntimeContext = runtimeContext;			
		}

		private IRuntimeContext RuntimeContext { get; set; }
		private IRepositories Repositories { get { return RuntimeContext.Repositories; } }

		public IAppSettings AppSettings { get { return new AppSettings(ConfigurationItems); } }
    public IAppSettings UncachedAppSettings { get { return new AppSettings(UncachedConfigurationItems); } }

		/// <summary>
		/// Gets the Configuation Items.
		/// </summary>
		/// <returns>The Configuation Items from the ConfiguationItem table in the database</returns>
		public IEnumerable<ConfigurationItemView> ConfigurationItems
		{
			get
			{
				var configurationItems = Cacher.Get<List<ConfigurationItemView>>(Constants.CacheKeys.Configuration);

				if (configurationItems == null)
				{
					lock (SyncLock)
					{
						var unCachedConfigurationItems = UncachedConfigurationItems;
						Cacher.Set(Constants.CacheKeys.Configuration, unCachedConfigurationItems);
						configurationItems = unCachedConfigurationItems.ToList();
					}
				}

				return configurationItems;
			}
		}

	  private IEnumerable<ConfigurationItemView> UncachedConfigurationItems
	  {
	    get
	    {
        var keySet = (from ci in Repositories.Configuration.ConfigurationItems
                      orderby ci.Key
                      select ci).ToList();

        return (from ci in keySet
                              select new ConfigurationItemView { Id = ci.Id, Key = ci.Key, Value = ci.Value, InternalOnly = ci.InternalOnly }).ToList();
	    }
	  }

    /// <summary>
    /// Gets the Configuation Items.
    /// </summary>
    /// <returns>The Configuation Items from the ConfiguationItem table in the database</returns>
    public IEnumerable<ConfigurationItemView> ExternalConfigurationItems
    {
      get { return ConfigurationItems.Where(ci => !ci.InternalOnly); }
    }

    /// <summary>
		/// Gets the configuration item by key.
		/// </summary>
		/// <param name="configurationItemKey">The configuration item key.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <returns></returns>
		public ConfigurationItem GetConfigurationItem(string configurationItemKey, string defaultValue = "")
		{
			ConfigurationItem item = null;

			try
			{
				item = Repositories.Configuration.ConfigurationItems.First(x => x.Key == configurationItemKey);
			}
			catch { }

			if (item.IsNull())
			{
				item = new ConfigurationItem { Key = configurationItemKey, Value = defaultValue };
				Repositories.Configuration.Add(item);
			}

			return item;
		}

		/// <summary>
		/// Refreshes the app settings.
		/// </summary>
		public void RefreshAppSettings()
		{
			lock (SyncLock)
			{
				Cacher.Remove(Constants.CacheKeys.Configuration);
			}
		}
	}

	#endregion

	#region ConfigurationHelper Interface

	public interface IConfigurationHelper
	{
		/// <summary>
		/// Gets the application settings.
		/// </summary>
		/// <returns></returns>
		IAppSettings AppSettings { get; }

    /// <summary>
    /// Gets the uncached application settings.
    /// </summary>
	  IAppSettings UncachedAppSettings { get; }

	  /// <summary>
		/// Gets the Configuation Items.
		/// </summary>
		/// <returns>The Configuation Items from the ConfiguationItem table in the database</returns>
		IEnumerable<ConfigurationItemView> ConfigurationItems { get; }

    /// <summary>
    /// Gets the External Configuation Items.
    /// </summary>
    /// <returns>The Configuation Items from the ConfiguationItem table in the database</returns>
    IEnumerable<ConfigurationItemView> ExternalConfigurationItems { get; }

		/// <summary>
		/// Refreshes the app settings.
		/// </summary>
		void RefreshAppSettings();

		/// <summary>
		/// Gets the configuration item by key.
		/// </summary>
		/// <param name="configurationItemKey">The configuration item key.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <returns></returns>
		ConfigurationItem GetConfigurationItem(string configurationItemKey, string defaultValue = "");
	}

	#endregion
}
