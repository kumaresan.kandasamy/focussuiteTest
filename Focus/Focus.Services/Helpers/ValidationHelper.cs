﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.IO;
using System.Reflection;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

using Focus.Core;
using Focus.Core.Models.Validation;
using Focus.Services.Core.Censorship;
using Focus.Services.Core.Validation;

using Framework.Core;

#endregion


namespace Focus.Services.Helpers
{
  #region ValidationHelper class

  public class ValidationHelper : HelperBase, IValidationHelper
  {
		/// <summary>
    /// Initializes a new instance of the <see cref="ValidationHelper"/> class.
		/// </summary>
		public ValidationHelper() : base(null) {}

		/// <summary>
    /// Initializes a new instance of the <see cref="ValidationHelper"/> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
    public ValidationHelper(IRuntimeContext runtimeContext) : base(runtimeContext) { }

    /// <summary>
    /// Gets the validation schema for a specific type
    /// </summary>
    /// <param name="schemaType">The schema type</param>
    /// <returns>The XML schema</returns>
    public XDocument GetValidationSchema(ValidationSchema schemaType)
    {
      return GetSchemaXml(schemaType);
    }

    /// <summary>
    /// Gets the record type for a schema
    /// </summary>
    /// <param name="schemaType">The schema type</param>
    /// <returns>The record type</returns>
    public Type GetRecordType(ValidationSchema schemaType)
    {
      var schema = GetSchemaXml(schemaType);

      // Tell the serializer about derived types
	    var xmlSerializer = new XmlSerializer(typeof(FileValidator), new[] { typeof(TextFieldValidator), typeof(DateFieldValidator), typeof(DecimalFieldValidator), typeof(IntegerFieldValidator), typeof(BooleanFieldValidator) });

      FileValidator validator;
      using (var stringReader = new StringReader(schema.ToString()))
      {
        validator = (FileValidator)xmlSerializer.Deserialize(stringReader);
      }

      return validator.RecordValidator.RecordType;
    }

    /// <summary>
    /// Validates a file
    /// </summary>
    /// <param name="schemaType">The schema type</param>
    /// <param name="fileData">The file data</param>
    /// <param name="fileType">The type of the file</param>
    /// <param name="profanity">A list of profanity rules</param>
    /// <param name="criminal">A list of criminal background exclusion rules</param>
    /// <param name="ignoreHeader">Whether to ignore the first record</param>
    /// <returns>Results of the validation</returns>
    public FileValidationResult ValidateFile(ValidationSchema schemaType, byte[] fileData, UploadFileType fileType, CensorshipRuleList profanity, CensorshipRuleList criminal, bool ignoreHeader = true)
    {
      var schema = GetSchemaXml(schemaType);

      // Tell the serializer about derived types
	    var xmlSerializer = new XmlSerializer(typeof(FileValidator), new[] { typeof(TextFieldValidator), typeof(DateFieldValidator), typeof(DecimalFieldValidator), typeof(IntegerFieldValidator), typeof(BooleanFieldValidator) });

      FileValidator validator;
      using (var stringReader = new StringReader(schema.ToString()))
      {
        validator = (FileValidator)xmlSerializer.Deserialize(stringReader);
      }

      validator.Localiser = Helpers.Localisation;
      validator.LibraryRepository = Repositories.Library;
      validator.Settings = AppSettings;
      return validator.Validate(fileData, fileType, profanity, criminal, ignoreHeader);
    }

    /// <summary>
    /// Gets the validation schema for a specific type
    /// </summary>
    /// <param name="schemaType">The schema type</param>
    /// <returns>The XML schema</returns>
    private static XDocument GetSchemaXml(ValidationSchema schemaType)
    {
      XDocument schemaXml;
      
      var resourceName = string.Concat("Focus.Services.Assets.Xmls.", schemaType.ToString(), "Schema.xml");
			var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceName);

			if (stream == null)
				throw new Exception("Unable to get validation schema");

      StreamReader reader = null;
      XmlReader xmlReader = null;

      try
      {
				reader = new StreamReader(stream);
				xmlReader = XmlReader.Create(reader);

        schemaXml = XDocument.Load(xmlReader);
      }
      finally
      {
        if (xmlReader.IsNotNull())
				  xmlReader.Close();

        if (reader.IsNotNull())
				  reader.Close();

				stream.Close();
      }

      return schemaXml;
		}
  }

  #endregion

	#region ValidationHelper interface

  public interface IValidationHelper
  {
    /// <summary>
    /// Gets the validation schema for a specific type
    /// </summary>
    /// <param name="schemaType">The schema type</param>
    /// <returns>The XML schema</returns>
    XDocument GetValidationSchema(ValidationSchema schemaType);

    /// <summary>
    /// Gets the record type for a schema
    /// </summary>
    /// <param name="schemaType">The schema type</param>
    /// <returns>The record type</returns>
    Type GetRecordType(ValidationSchema schemaType);

    /// <summary>
    /// Validates a file
    /// </summary>
    /// <param name="schemaType">The schema type</param>
    /// <param name="fileData">The file data</param>
    /// <param name="fileType">The type of the file</param>
    /// <param name="profanity">A list of profanity rules</param>
    /// <param name="criminal">A list of criminal background exclusion rules</param>
    /// <param name="ignoreHeader">Whether to ignore the first record</param>
    /// <returns>Results of the validation</returns>
    FileValidationResult ValidateFile(ValidationSchema schemaType, byte[] fileData, UploadFileType fileType, CensorshipRuleList profanity, CensorshipRuleList criminal, bool ignoreHeader = true);
  }

  #endregion
}
