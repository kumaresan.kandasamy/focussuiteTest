﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.IntegrationMessages;
using Focus.Data.Core.Entities;
using Focus.Services.DtoMappers;
using Focus.Services.Messages;
using Framework.Core;

#endregion

namespace Focus.Services.Helpers
{
	public class SelfServiceHelper : HelperBase, ISelfServiceHelper
	{
		private bool IsStandalone { get { return AppSettings.IntegrationClient == IntegrationClient.Standalone; } }
		public SelfServiceHelper(IRuntimeContext runtimeContext)
			: base(runtimeContext)
		{
		}

		#region Implementation of ISelfServiceHelper

		public AssignJobSeekerActivityRequest PublishSelfServiceActivity(ActionTypes actionType, long actioner, long jobSeekerUserId = 0, long jobSeekerId = 0, bool publishIfNoExternalId = false, bool ignorePublish = false)
		{
			// We don't need to send this to an integration client if we are in standalone mode
			if (IsStandalone) return null;
			if (jobSeekerId == 0 && jobSeekerUserId == 0) return null;

			var user = jobSeekerId == 0 ?
				Repositories.Core.Users.First(x => x.Id == jobSeekerUserId).AsDto() :
				Repositories.Core.Users.First(x => x.PersonId == jobSeekerId).AsDto();

			if (user.ExternalId.IsNullOrEmpty() && !publishIfNoExternalId)
				return null;

			var assignJobSeekerActivityRequest = new AssignJobSeekerActivityRequest
			{
			  ActionTypeName = actionType.ToString(),
			  SelfServicedActivity = (actioner == user.Id),
			  JobSeekerId = user.ExternalId,
        ActivityUserId = user.Id.GetValueOrDefault(0),
				ActionerId = actioner
			};

			if (RuntimeContext.IsNotNull() && RuntimeContext.CurrentRequest.IsNotNull() &&
			    RuntimeContext.CurrentRequest.UserContext.IsNotNull())
			{
				assignJobSeekerActivityRequest.ExternalAdminId = user.ExternalId;

				string currentOffice = null;
				if (!assignJobSeekerActivityRequest.SelfServicedActivity)
				{
					var userDetails = Repositories.Core.Users.Where(u => u.Id == actioner).Select(u => new { u.PersonId, u.ExternalId }).First();
					assignJobSeekerActivityRequest.ExternalAdminId = userDetails.ExternalId;

					currentOffice = (from pco in Repositories.Core.PersonsCurrentOffices
 													 join o in Repositories.Core.Offices
														 on pco.OfficeId equals o.Id
													 where pco.PersonId == userDetails.PersonId
													 orderby pco.StartTime descending
													 select o.ExternalId).FirstOrDefault();

					assignJobSeekerActivityRequest.OfficeId = currentOffice;
				}

				assignJobSeekerActivityRequest.ExternalOfficeId = currentOffice.IsNotNullOrEmpty() ? currentOffice : RuntimeContext.CurrentRequest.UserContext.ExternalOfficeId;
				assignJobSeekerActivityRequest.ExternalPassword = RuntimeContext.CurrentRequest.UserContext.ExternalPassword;
				assignJobSeekerActivityRequest.ExternalUsername = RuntimeContext.CurrentRequest.UserContext.ExternalUserName;
			}

			if (!ignorePublish)
			{
				Helpers.Messaging.Publish(new IntegrationRequestMessage
				{
					IntegrationPoint = IntegrationPoint.AssignJobSeekerActivity,
					ActionerId = actioner,
					AssignJobSeekerActivityRequest = assignJobSeekerActivityRequest
				});
			}

			if (actioner != user.Id && AppSettings.ShowStaffAssistedLMI)
			{
				if (Repositories.Integration.IsActivityStaffAssisted(actionType))
				{
					Helpers.Logging.LogAction(ActionTypes.StaffAssistedLMI, typeof (Person).Name, user.PersonId, false);
				}
			}

			return assignJobSeekerActivityRequest;
		}

		#endregion
	}

	public interface ISelfServiceHelper
	{
		AssignJobSeekerActivityRequest PublishSelfServiceActivity(ActionTypes actionType, long actioner, long jobSeekerUserId = 0, long jobSeekerId = 0, bool publishIfNoExternalId = false, bool ignorePublish = false);
	}
}
