﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Linq;
using Focus.Core;
using Focus.Data.Core.Entities;
using Framework.Core;

#endregion

namespace Focus.Services.Helpers
{

  public interface IEmployeeHelper
  {
      /// <summary>
      /// Validates the referral status reasons.
      /// </summary>
      /// <param name="reasons">The reasons.</param>
      /// <param name="reasonType">Type of the reason.</param>
      /// <param name="otherItemKey">The other item key.</param>
      /// <returns></returns>
     ErrorTypes ValidateReferralStatusReasons(List<KeyValuePair<long, string>> reasons, LookupTypes reasonType, string otherItemKey);
  }

  public class EmployeeHelper : HelperBase, IEmployeeHelper
  {
    /// <summary>
		/// Initializes a new instance of the <see cref="EmployerHelper"/> class.
		/// </summary>
		public EmployeeHelper() : base(null) {}

		/// <summary>
		/// Initializes a new instance of the <see cref="EmployerHelper"/> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
    public EmployeeHelper(IRuntimeContext runtimeContext) : base(runtimeContext) { }


    public  ErrorTypes ValidateReferralStatusReasons(List<KeyValuePair<long, string>> reasons, LookupTypes reasonType, string otherItemKey)
    {
        if (AppSettings.Theme == FocusThemes.Workforce)
        {
            if (reasons.IsNullOrEmpty())
                return ErrorTypes.ReferralStatusReasonsNotSpecified;

            var otherReasonId = Helpers.Lookup.GetLookup(reasonType).Where(x => x.Key.Equals(otherItemKey)).Select(x => x.Id).FirstOrDefault();

            if (reasons.Any(x => x.Key.Equals(otherReasonId) && string.IsNullOrEmpty(x.Value)))
                return ErrorTypes.ReferralStatusReasonsOtherReasonTextNotSpecified;

            if (reasons.Any(x => !x.Key.Equals(otherReasonId) && !string.IsNullOrEmpty(x.Value)))
                return ErrorTypes.ReferralStatusReasonsOtherReasonTextNotValidWithSpecifiedReasonId;
        }

        return ErrorTypes.Ok;
    }
  
  }
}
