﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Drawing;
using System.Globalization;
using System.Linq;
using Aspose.Words.Lists;
using Focus.Core.Criteria.CandidateSearch;
using Focus.Core.EmailTemplate;
using Focus.Core.Messages.CandidateService;
using Focus.Core.Messages.PostingService;
using Focus.Core.Views;
using Focus.Data.Core.Entities;
using Focus.Core;
using Framework.Core;
using Framework.Logging;
using System.Collections.Generic;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Models;
using Mindscape.LightSpeed;

#endregion

namespace Focus.Services.Helpers
{
	#region CandidateHelper Class

	public class CandidateHelper : HelperBase, ICandidateHelper
	{
		public CandidateHelper() : base(null) { }

		public CandidateHelper(IRuntimeContext runtimeContext) : base(runtimeContext) { }

		/// <summary>
		/// Denies the referral.
		/// </summary>
		/// <param name="applicationId">The application identifier.</param>
		/// <param name="changedByUserId">The changed by user identifier.</param>
		/// <param name="autoDenied"> </param>
		/// <exception cref="System.ArgumentException"></exception>
		public void DenyReferral(long applicationId, long changedByUserId, int? lockVersion, bool autoDenied = false)
		{
			var application = Repositories.Core.FindById<Application>(applicationId);

			if (lockVersion.HasValue && application.LockVersion != lockVersion.Value)
				throw new OptimisticConcurrencyException();

			if (application == null) throw new ArgumentException(string.Format("The application {0} could not be found in the database.", applicationId));

		  application.PreviousApprovalStatus = application.ApprovalStatus;
			application.ApprovalStatus = ApprovalStatuses.Rejected;
			application.StatusLastChangedOn = DateTime.Now;
			application.StatusLastChangedBy = changedByUserId;
			application.AutomaticallyDenied = autoDenied;

			Repositories.Core.SaveChanges(true);

			var actionType = autoDenied
				                 ? ActionTypes.UpdateReferralStatusToAutoDenied
												 : ActionTypes.DenyCandidateReferral;

			RuntimeContext.Helpers.Logging.LogAction(actionType, typeof(Application).Name, application.Id, null, null, String.Format("{0} {1}", application.Resume.FirstName, application.Resume.LastName));
		}

		public void AutoDenyReferrals()
		{
			Logger.Debug("DenyReferralRequestMessageHandle: Searching for referrals where the job was closed " + AppSettings.AutoDenyJobseekerTimePeriod + " days ago, which corresponds to a date of " + DateTime.Today.AddDays(-AppSettings.AutoDenyJobseekerTimePeriod));

			var onHoldReferrals = Repositories.Core.Applications
				.Where(a => a.ApprovalStatus == ApprovalStatuses.OnHold && a.Posting.Job.JobStatus == JobStatuses.Closed
				&& a.Posting.Job.ClosedOn.HasValue && a.Posting.Job.ClosedOn < DateTime.Today.AddDays(-AppSettings.AutoDenyJobseekerTimePeriod)).ToList();

			Logger.Debug("DenyReferralRequestMessageHandle: " + onHoldReferrals.Count + " referrals were found.");

			onHoldReferrals.ForEach(r =>
			{
				var templateValues = new EmailTemplateData
				{
					RecipientName = r.Resume.Person.FirstName,
					JobTitle = r.Posting.JobTitle,
					JobId = r.Posting.JobId.ToString()
				};

				DenyReferral(r.Id, RuntimeContext.CurrentRequest.UserContext.UserId, r.LockVersion, true);

				RuntimeContext.Helpers.Email.SendEmailFromTemplate(EmailTemplateTypes.AutoDeniedExpiredPosting, templateValues, r.Resume.EmailAddress, string.Empty, string.Empty);
			});
		}

		/// <summary>
		/// Puts the referral on hold.
		/// </summary>
		/// <param name="applicationId">The application identifier.</param>
		/// <param name="reason">The reason.</param>
		/// <param name="autoOnHold">if set to <c>true</c> [automatic on hold].</param>
		/// <param name="changedByUserId">The changed by user identifier.</param>
		/// <param name="lockVersion">The lock version.</param>
		/// <exception cref="System.ArgumentException"></exception>
		/// <exception cref="Mindscape.LightSpeed.OptimisticConcurrencyException"></exception>
		public void HoldReferral(long applicationId, ApplicationOnHoldReasons reason, bool autoOnHold, long changedByUserId, int? lockVersion = null)
		{
			ValidateOnHoldReason(reason);

			var application = Repositories.Core.FindById<Application>(applicationId);

			if (application == null)
				throw new ArgumentException(string.Format("The application {0} could not be found in the database.", applicationId));

			if (lockVersion.HasValue && application.LockVersion != lockVersion.Value)
				throw new OptimisticConcurrencyException();

		  application.PreviousApprovalStatus = application.ApprovalStatus;
			application.ApprovalStatus = ApprovalStatuses.OnHold;
			application.AutomaticallyOnHold = autoOnHold;
			application.StatusLastChangedOn = DateTime.Now;
			if (!autoOnHold)
				application.StatusLastChangedBy = changedByUserId;

			var applicant = GetApplicantNameAndEmailAddress(application.ResumeId);

			#region Send Email

			switch (reason)
			{
				case ApplicationOnHoldReasons.JobClosed:
				case ApplicationOnHoldReasons.JobPutOnHold:
					var job = GetJobIdAndTitle(application.PostingId);
					SendReferralOnHoldDueToJobClosingEmail(job.Item1, job.Item2, applicant.Item1, applicant.Item2);
					break;
			}

			#endregion

			Repositories.Core.SaveChanges();

			var actionType = autoOnHold
												 ? ActionTypes.UpdateReferralStatusToAutoOnHold
												 : ActionTypes.HoldCandidateReferral;

			Helpers.Logging.LogAction(actionType, typeof(Application).Name, application.Id, null, null, applicant.Item1);
		}

		/// <summary>
		/// Validates the on hold reason.
		/// </summary>
		/// <param name="reason">The reason.</param>
		private void ValidateOnHoldReason(ApplicationOnHoldReasons reason)
		{
			if (reason.IsNotIn(ApplicationOnHoldReasons.JobClosed, ApplicationOnHoldReasons.JobPutOnHold, ApplicationOnHoldReasons.PutOnHoldByStaff))
				throw new ArgumentException(string.Format("The application on hold reason {0} is not valid.", reason));
		}

		/// <summary>
		/// Sends the expired alient registration email.
		/// </summary>
		/// <param name="applicantName">Name of the applicant.</param>
		/// <param name="applicantEmailAddress">The applicant email address.</param>
		public void SendExpiredAlienRegistrationEmail(string applicantName, string applicantEmailAddress)
		{
			var emailTemplateData = new EmailTemplateData
			{
				RecipientName = applicantName
			};

			var emailTemplate = Helpers.Email.GetEmailTemplatePreview(EmailTemplateTypes.ExpiredAlienRegistration, emailTemplateData);
			Helpers.Email.SendEmail(applicantEmailAddress, string.Empty, string.Empty, emailTemplate.Subject, emailTemplate.Body);
		}

		/// <summary>
		/// Sends the referral on hold due to job closing email.
		/// </summary>
		/// <param name="jobId">The job identifier.</param>
		/// <param name="jobTitle">The job title.</param>
		/// <param name="applicantName">Name of the applicant.</param>
		/// <param name="applicantEmailAddress">The applicant email address.</param>
		private void SendReferralOnHoldDueToJobClosingEmail(long? jobId, string jobTitle, string applicantName, string applicantEmailAddress)
		{
			var emailTemplateData = new EmailTemplateData
			{
				JobTitle = jobTitle,
				JobId = jobId.ToString(),
				RecipientName = applicantName
			};

			var emailTemplate = Helpers.Email.GetEmailTemplatePreview(EmailTemplateTypes.JobSeekerNoticeOfOnHoldJob, emailTemplateData);
			Helpers.Email.SendEmail(applicantEmailAddress, string.Empty, string.Empty, emailTemplate.Subject, emailTemplate.Body);
		}

		/// <summary>
		/// Gets the job identifier and title.
		/// </summary>
		/// <param name="postingId">The posting identifier.</param>
		/// <returns></returns>
		private Tuple<long?, string> GetJobIdAndTitle(long postingId)
		{
			var jobId = Repositories.Core.Postings.Where(x => x.Id == postingId).Select(x => x.JobId).SingleOrDefault();

			if (jobId.IsNull())
				throw new Exception(string.Format("Could not find job details for posting {0}.", postingId));

			var job = Repositories.Core.Jobs.Where(x => x.Id == jobId).Select(x => new { x.Id, x.JobTitle }).SingleOrDefault();

			if (job.IsNull())
				throw new Exception(string.Format("Could not find job details for job {0}.", jobId));

			return new Tuple<long?, string>(jobId, job.JobTitle);
		}

		/// <summary>
		/// Gets the applicant name and email address.
		/// </summary>
		/// <param name="resumeId">The resume identifier.</param>
		/// <returns></returns>
		private Tuple<string, string> GetApplicantNameAndEmailAddress(long resumeId)
		{
			var personId = Repositories.Core.Resumes.Where(x => x.Id == resumeId).Select(x => x.PersonId).SingleOrDefault();

			if (personId.IsNull())
				throw new Exception(string.Format("Could not find person details for resume {0}.", resumeId));

			var person = Repositories.Core.Persons.Where(x => x.Id == personId).Select(x => new { x.FirstName, x.LastName, x.EmailAddress }).SingleOrDefault();

			if (person.IsNull())
				throw new Exception(string.Format("Could not find person details for person {0}.", personId));

			return new Tuple<string, string>(string.Format("{0} {1}", person.FirstName, person.LastName), person.EmailAddress);
		}

		/// <summary>
		/// Get candidate invites for specified job
		/// </summary>
		/// <param name="resumeId"></param>
		/// <returns></returns>
		public bool IsCandidateInvitedToApplyForThisJob(long jobId, long personId)
		{
			return Repositories.Core.InviteToApplyBasicViews.FirstOrDefault(x => x.JobId == jobId && x.PersonId == personId).IsNotNull();
		}

		/// <summary>
		/// Gets the invitees for job.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		public PagedList<ResumeView> GetInviteesForJob(InviteesResumeViewCriteria criteria)
		{
			var query = Repositories.Core.InviteToApplyBasicViews.Where( x => x.JobId == criteria.JobId );

			var list = query.GetPagedList(invitee => new ResumeView
																								{
																									Name = string.Concat(invitee.FirstName, " ", invitee.LastName),
																									Id = invitee.PersonId,
																									DateInvitedCreatedOn = invitee.CreatedOn,
																									CandidateLocation = string.Format("{0}, {1}", invitee.Town, Helpers.Lookup.GetLookupExternalId(LookupTypes.States, invitee.StateId.GetValueOrDefault())),
																									Branding = invitee.Branding,
																									ContactInfoVisible = invitee.IsContactInfoVisible,
																									YearsExperience = invitee.YearsExperience ?? 0,
																									NcrcLevelId = invitee.NcrcLevelId,
																									IsVeteran = invitee.CandidateIsVeteran,
                                                                                                    IsInviteQueued = invitee.InvitationQueued ?? false
																								}, criteria.PageIndex, criteria.PageSize);

		  var idList = list.Select(invitee => invitee.Id).ToList();
      var employerApplicantIds = Repositories.Core.ApplicationViews
                                                  .Where(x => x.EmployerId == RuntimeContext.CurrentRequest.UserContext.EmployerId && x.CandidateApplicationApprovalStatus == ApprovalStatuses.Approved && idList.Contains(x.CandidateId))
                                                  .Select(x => x.CandidateId)
                                                  .ToList();

      //var inviteesId = Repositories.Core.InviteToApply.Where(i => i.JobId == criteria.JobId && (i.QueueInvitation ?? false)).Select(x => x.PersonId);
      //list.ForEach(invitee => {
      //    if(inviteesId.IsNotNullOrEmpty())
      //        invitee.IsInviteQueued = inviteesId.Contains(invitee.Id);
      //});

      list.ForEach(invitee => UpdateResumeViewName(invitee, employerApplicantIds, RuntimeContext));

		  return list;
		}

		

		/// <summary>
    /// Mask the candidate name is they are not an applicant and have not chosen to share their details
    /// </summary>
    /// <param name="candidate">The candidate to check</param>
    /// <param name="employerApplicantIds">A list of applicant ids for any employer job</param>
    /// <param name="runtimeContext">The runtime contect</param>
    internal static void UpdateResumeViewName(ResumeView candidate, List<long> employerApplicantIds, IRuntimeContext runtimeContext)
    {
      var isApplicant = employerApplicantIds.IsNotNullOrEmpty() && (employerApplicantIds.Exists(x => x == candidate.Id));

      if (!isApplicant && ((!candidate.ContactInfoVisible && runtimeContext.AppSettings.ShowContactDetails == ShowContactDetails.Full) || runtimeContext.AppSettings.ShowContactDetails == ShowContactDetails.None) && runtimeContext.AppSettings.Module != FocusModules.Assist)
        candidate.Name = candidate.Id.ToString(CultureInfo.InvariantCulture);
    }

		public void SendStaffReferralEmail(long candidateId, PostingDto posting, IUserContext userContext)
		{
			var person = Repositories.Core.FindById<Person>(candidateId);
			var careerPath = AppSettings.CareerApplicationPath.EndsWith("/")
                                   ? AppSettings.CareerApplicationPath
                                   : string.Concat(AppSettings.CareerApplicationPath, "/");

      var jobLinkUrl = string.Format("{0}jobposting/{1}/invite/email", careerPath, posting.LensPostingId);

			var templateValues = new EmailTemplateData
					{
						RecipientName = person.FirstName,
						EmployerName = posting.EmployerName,
						JobTitle = posting.JobTitle,
						JobLinkUrls = new List<string> { jobLinkUrl },
						SenderName = String.Format("{0} {1}", userContext.FirstName, userContext.LastName)
					};

			Helpers.Email.SendEmailFromTemplate(EmailTemplateTypes.StaffReferralNotification, templateValues, person.EmailAddress, string.Empty, string.Empty);
		}

        public void SendQueuedStaffReferralEmail(long candidateId, PostingDto posting, IUserContext userContext, DateTime DateToSend)
        {
            var person = Repositories.Core.FindById<Person>(candidateId);
            var careerPath = AppSettings.CareerApplicationPath.EndsWith("/")
                                   ? AppSettings.CareerApplicationPath
                                   : string.Concat(AppSettings.CareerApplicationPath, "/");

            var jobLinkUrl = string.Format("{0}jobposting/{1}/invite/email", careerPath, posting.LensPostingId);

            var templateValues = new EmailTemplateData
            {
                RecipientName = person.FirstName,
                EmployerName = posting.EmployerName,
                JobTitle = posting.JobTitle,
                JobLinkUrls = new List<string> { jobLinkUrl },
                SenderName = String.Format("{0} {1}", userContext.FirstName, userContext.LastName)
            };

            Helpers.Email.SendQueuedEmailFromTemplate(EmailTemplateTypes.StaffReferralNotification, templateValues, person.EmailAddress, string.Empty, string.Empty, DateToSend);
        }
	}

	#endregion

	#region ICandidateHelper Interface

	public interface ICandidateHelper
	{
		void DenyReferral(long applicationId, long changedByUserId, int? lockVersion, bool autoDenied = false);

		void AutoDenyReferrals();

		void HoldReferral(long applicationId, ApplicationOnHoldReasons reason, bool autoOnHold, long changedByUserId, int? lockVersion = null);

		PagedList<ResumeView> GetInviteesForJob(InviteesResumeViewCriteria criteria);

		bool IsCandidateInvitedToApplyForThisJob(long jobId, long personId);

		void SendStaffReferralEmail(long candidateId, PostingDto posting, IUserContext userContext);

        void SendQueuedStaffReferralEmail(long candidateId, PostingDto posting, IUserContext userContext, DateTime DateToSend);
	}

	#endregion
}
