﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using Focus.Core;
using Focus.Data.Core.Entities;
using Framework.Logging;
using Framework.ServiceLocation;
using PushSharp;
using PushSharp.Android;
using PushSharp.Apple;
using PushSharp.Core;

namespace Focus.Services.Helpers
{
	public class PushNotificationHelper : HelperBase, IPushNotificationHelper
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="PushNotificationHelper"/> class.
		/// </summary>
		public PushNotificationHelper() : base(null) { }

		/// <summary>
		/// Initializes a new instance of the <see cref="PushNotificationHelper"/> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		public PushNotificationHelper(IRuntimeContext runtimeContext) : base(runtimeContext) { }

		/// <summary>
		/// Sends the notification.
		/// </summary>
		/// <param name="deviceType">Type of the device.</param>
		/// <param name="deviceToken">The device token.</param>
		/// <param name="deviceId">The device id.</param>
		/// <param name="title">The title.</param>
		/// <param name="message">The message.</param>
		public void SendNotification(DeviceTypes deviceType, string deviceToken, long deviceId, string title, string message)
		{
			// log notification
			var pushNotification = new PushNotification();

			try
			{
				pushNotification.Title = title;
				pushNotification.Text = message;
				pushNotification.PersonMobileDeviceToken = deviceToken;

				Repositories.Core.Add(pushNotification);
				Repositories.Core.SaveChanges(true);
			}
			catch (Exception ex)
			{
				Logger.Error("Problem logging push notification..." + ex);
			}

			// Limit message to 50 chars for title and 100 chars for text
			var pushTitle = title;
			var pushMsg = message;
			if (pushTitle.Length > 50)
				pushTitle = pushTitle.Substring(0, 50);
			if (pushMsg.Length > 100)
				pushMsg = pushMsg.Substring(0, 100);

			//Create our push services broker
			var push = new PushBroker();

			//Wire up the events for all the services that the broker registers
			push.OnNotificationSent += NotificationSent;
			push.OnChannelException += ChannelException;
			push.OnServiceException += ServiceException;
			push.OnNotificationFailed += NotificationFailed;
			push.OnDeviceSubscriptionExpired += DeviceSubscriptionExpired;
			push.OnDeviceSubscriptionChanged += DeviceSubscriptionChanged;
			push.OnChannelCreated += ChannelCreated;
			push.OnChannelDestroyed += ChannelDestroyed;

			switch (deviceType)
			{
				case DeviceTypes.Android:

					push.RegisterGcmService(new GcmPushChannelSettings("AIzaSyAd4eyzR27PZVJmtx6v6d4HjYD9Zs61ntI"));
					push.QueueNotification(new GcmNotification().ForDeviceRegistrationId(deviceToken).WithJson(string.Format(@"{{""title"":""{0}"", ""text"":""{1}"", ""id"":""{2}""}}", EscapeStringValue(pushTitle), EscapeStringValue(pushMsg), pushNotification.Id)));
					push.StopAllServices();
					break;


				case DeviceTypes.Apple:
					
					//-------------------------
					// APPLE NOTIFICATIONS
					//-------------------------
					//Configure and start Apple APNS
					// IMPORTANT: Make sure you use the right Push certificate.  Apple allows you to generate one for connecting to Sandbox,
					//   and one for connecting to Production.  You must use the right one, to match the provisioning profile you build your
					//   app with!
					var assembly = Assembly.GetExecutingAssembly();
					
					var stream = assembly.GetManifestResourceStream("Focus.Services.Resources.PushSharp.Apns.Sandbox.p12");

					using (var ms = new MemoryStream())
					{
						if (stream != null)
						{
							stream.CopyTo(ms);
							var appleCert = ms.ToArray();

							//IMPORTANT: If you are using a Development provisioning Profile, you must use the Sandbox push notification server 
							//  (so you would leave the first arg in the ctor of ApplePushChannelSettings as 'false')
							//  If you are using an AdHoc or AppStore provisioning profile, you must use the Production push notification server
							//  (so you would change the first arg in the ctor of ApplePushChannelSettings to 'true')
							push.RegisterAppleService(new ApplePushChannelSettings(false, appleCert, "FocusPass")); //Extension method

							push.QueueNotification(new AppleNotification()
								.ForDeviceToken(deviceToken)
								.WithAlert(EscapeStringValue(pushTitle))
								.WithCustomItem(string.Format(@"{{""id"":""{0}""}}", pushNotification.Id)));
						}
					}

					
					break;
					/*	
					case DeviceTypes.WindowsPhone:

							push.RegisterWindowsPhoneService();
							//Fluent construction of a Windows Phone Toast notification
							//IMPORTANT: For Windows Phone you MUST use your own Endpoint Uri here that gets generated within your Windows Phone app itself!
							push.QueueNotification(new WindowsPhoneToastNotification()
									.ForEndpointUri(new Uri("DEVICE REGISTRATION CHANNEL URI HERE"))
									.ForOSVersion(WindowsPhoneDeviceOSVersion.MangoSevenPointFive)
									.WithBatchingInterval(BatchingInterval.Immediate)
									.WithNavigatePath("/MainPage.xaml")
									.WithText1("PushSharp")
									.WithText2("This is a Toast"));
						*/
					break;

			}
		}

		private static void ChannelDestroyed(object sender)
		{
			Logger.Info("Channel destroyed : Sender = " + sender);
		}

		private static void ChannelCreated(object sender, IPushChannel pushChannel)
		{
			Logger.Info("Channel created : Sender = " + sender);
		}

		private static void DeviceSubscriptionChanged(object sender, string oldSubscriptionId, string newSubscriptionId, INotification notification)
		{
			Logger.Info("Device Subscription Changed");
			DeleteDevice(oldSubscriptionId);
		}

		private static void DeviceSubscriptionExpired(object sender, string expiredSubscriptionId, DateTime expirationDateUtc, INotification notification)
		{
			Logger.Info("Device Subscription Expired");
			DeleteDevice(expiredSubscriptionId);
		}

		private static void NotificationFailed(object sender, INotification notification, Exception error)
		{
			Logger.Debug("Notification Failed : " + error.Message + "  " + error.InnerException);
		}

		private static void ServiceException(object sender, Exception error)
		{
			Logger.Debug("Notification service exception : " + error.Message + "  " + error.InnerException);
		}

		private static void ChannelException(object sender, IPushChannel pushChannel, Exception error)
		{
			Logger.Debug("Channel exception : " + error.Message + "  " + error.InnerException);
		}

		private static void NotificationSent(object sender, INotification notification)
		{
			Logger.Info("Notification Sent");

		}

		private static void DeleteDevice(string oldRegistrationId)
		{
			var servicesRuntime = new ServicesRuntime<StructureMapServiceLocator>(ServiceRuntimeEnvironment.DllOrExe, new StructureMapServiceLocator());
			servicesRuntime.Start();
			try
			{
				var runtimeContext = new RuntimeContext(ServiceRuntimeEnvironment.DllOrExe);
				var query = runtimeContext.Repositories.Core.PersonMobileDevices.Where(um => um.Token == oldRegistrationId);
				var device = query.FirstOrDefault();
				if (device == null) return;
				runtimeContext.Repositories.Core.Remove(device);
				runtimeContext.Repositories.Core.SaveChanges(true);
			}
			catch (Exception e)
			{
				Logger.Debug("Failed to delete old device : " + e.Message + "  " + e.InnerException);
			}
			finally
			{
				servicesRuntime.Shutdown();
			}
		}

		private static string EscapeStringValue(string value)
		{
			const char BACK_SLASH = '\\';
			const char SLASH = '/';
			const char DBL_QUOTE = '"';

			var output = new StringBuilder(value.Length);
			foreach (char c in value)
			{
				switch (c)
				{
					case SLASH:
						output.AppendFormat("{0}{1}", BACK_SLASH, SLASH);
						break;

					case BACK_SLASH:
						output.AppendFormat("{0}{0}", BACK_SLASH);
						break;

					case DBL_QUOTE:
						output.AppendFormat("{0}{1}", BACK_SLASH, DBL_QUOTE);
						break;

					default:
						output.Append(c);
						break;
				}
			}

			return output.ToString();
		}


	}

	public interface IPushNotificationHelper
	{
		/// <summary>
		/// Sends the notification.
		/// </summary>
		/// <param name="deviceType">Type of the device.</param>
		/// <param name="deviceToken">The device token.</param>
		/// <param name="deviceId">The device id.</param>
		/// <param name="title">The title.</param>
		/// <param name="message">The message.</param>
		void SendNotification(DeviceTypes deviceType, string deviceToken, long deviceId, string title, string message);
	}



}
