﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Data.Core.Entities;
using Framework.Core;

#endregion

namespace Focus.Services.Helpers
{
	public interface IUserHelper
	{
		void UpdateSecurityQuestions( long userId, UserSecurityQuestionDto[] changedQuestions, bool changeSingleItem );
	}

	public class UserHelper : HelperBase, IUserHelper
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="EmployerHelper"/> class.
		/// </summary>
		public UserHelper() : base(null)
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="EmployerHelper"/> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		public UserHelper(IRuntimeContext runtimeContext) : base(runtimeContext)
		{
		}

		public void UpdateSecurityQuestions(long userId, UserSecurityQuestionDto[] changedQuestions, bool changeSingleItem)
		{
			var userSecurityQuestionRecords = Repositories.Core.Find<UserSecurityQuestion>(usq => usq.UserId == userId);
			var questionIndex = 1;
			if (changeSingleItem)
			{
				// if we are changing just a single item, then start the loop with this index
				foreach (var question in changedQuestions)
				{
					questionIndex = question.QuestionIndex;
					break;
				}
			}
			for (;; questionIndex++)
			{
				var userSecurityQuestion = changedQuestions.IsNull() ? null : changedQuestions.FirstOrDefault(usq => usq.QuestionIndex == questionIndex);
				var userSecurityQuestionRecord = userSecurityQuestionRecords.FirstOrDefault(usq => usq.QuestionIndex == questionIndex);
				if (userSecurityQuestion.IsNull())
				{
					if (userSecurityQuestionRecord.IsNotNull())
					{
						var index = questionIndex;
						foreach( var usq in userSecurityQuestionRecords.Where( usq => usq.QuestionIndex >= index) )
						{
							Repositories.Core.Remove( usq );				
						}
					}
					break;
				}
				if (userSecurityQuestion.SecurityAnswerEncrypted.Length > 250)
				{
					throw new Exception( "Security Answer length must be between 0 and 250" );
				}
				if (userSecurityQuestionRecord.IsNull())
				{
					userSecurityQuestionRecord = new UserSecurityQuestion
					{
						QuestionIndex = questionIndex,
						SecurityQuestion = userSecurityQuestion.SecurityQuestion,
						SecurityQuestionId = userSecurityQuestion.SecurityQuestionId,
						SecurityAnswerHash = userSecurityQuestion.SecurityAnswerHash,
						SecurityAnswerEncrypted = Helpers.Encryption.Encrypt(userSecurityQuestion.SecurityAnswerEncrypted, TargetTypes.SecurityAnswer, EntityTypes.User, userId),
						UserId = userId
					};
					Repositories.Core.Add(userSecurityQuestionRecord);
				}
				else
				{
					userSecurityQuestionRecord.SecurityQuestion = userSecurityQuestion.SecurityQuestion;
					userSecurityQuestionRecord.SecurityQuestionId = userSecurityQuestion.SecurityQuestionId;
					userSecurityQuestionRecord.SecurityAnswerHash = userSecurityQuestion.SecurityAnswerHash;
					userSecurityQuestionRecord.SecurityAnswerEncrypted = Helpers.Encryption.Encrypt(userSecurityQuestion.SecurityAnswerEncrypted, TargetTypes.SecurityAnswer, EntityTypes.User, userId);
				}
				// if we are just changing a single item, then we are done
				if (changeSingleItem)
					break;
			}
			Repositories.Core.SaveChanges();
		}
	}
}
