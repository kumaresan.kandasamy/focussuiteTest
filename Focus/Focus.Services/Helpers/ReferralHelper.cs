﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using Focus.Core;
using Focus.Core.Models;

#endregion

namespace Focus.Services.Helpers
{
  public class ReferralHelper
  {
    public List<ApprovalDenialReasonsModel> SaveApprovalDenialReasons(List<long> denialReasons, string otherReasonText, EntityTypes entityType)
    {
      
    }
  }

  public interface IReferralHelper
  {
    /// <summary>
    /// Saves the approval denial reasons.
    /// </summary>
    /// <param name="denialReasons">The denial reasons.</param>
    /// <param name="otherReasonText">The other reason text.</param>
    /// <param name="entityType">Type of the entity.</param>
    /// <returns></returns>
    List<ApprovalDenialReasonsModel> SaveApprovalDenialReasons(List<long> denialReasons, string otherReasonText, EntityTypes entityType);
  }
}
