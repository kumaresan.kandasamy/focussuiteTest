﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;

using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.EmailTemplate;
using Focus.Data.Core.Entities;
using Focus.Services.DtoMappers;
using Framework.Core;

#endregion



namespace Focus.Services.Helpers
{

	#region AlertsHelper Class

	public class AlertsHelper : HelperBase, IAlertsHelper
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="AlertsHelper"/> class.
		/// </summary>
		public AlertsHelper() : base(null) {}

		/// <summary>
		/// Initializes a new instance of the <see cref="AlertsHelper"/> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		public AlertsHelper(IRuntimeContext runtimeContext) : base(runtimeContext) {}

    /// <summary>
	  /// Creates the alert message.
	  /// </summary>
	  /// <param name="alertMessageKey">The alert message key.</param>
	  /// <param name="audienceEntityId">The audience entity id.</param>
	  /// <param name="expiryDate">The expiry date.</param>
	  /// <param name="audience">The audience.</param>
	  /// <param name="messageType">Type of the message.</param>
	  /// <param name="entityId">The entity id.</param>
	  /// <param name="messageProperties">The message properties.</param>
    public void CreateAlertMessage(string alertMessageKey, long? audienceEntityId, DateTime expiryDate, MessageAudiences audience, MessageTypes messageType, long? entityId, params object[] messageProperties)
    {
    	CreateAlertMessage(alertMessageKey, false, audienceEntityId, expiryDate, audience, messageType, entityId, messageProperties);
    }

	  /// <summary>
	  /// Creates the alert message.
	  /// </summary>
	  /// <param name="alertMessageKey">The alert message key.</param>
	  /// <param name="isSystemAlert">Whether this is a system alert</param>
	  /// <param name="audienceEntityId">The audience entity id.</param>
	  /// <param name="expiryDate">The expiry date.</param>
	  /// <param name="audience">The audience.</param>
	  /// <param name="messageType">Type of the message.</param>
	  /// <param name="entityId">The entity id.</param>
	  /// <param name="messageProperties">The message properties.</param>
	  public void CreateAlertMessage(string alertMessageKey, bool isSystemAlert, long? audienceEntityId, DateTime expiryDate, MessageAudiences audience, MessageTypes messageType, long? entityId, params object[] messageProperties)
		{
			// Get all possible variations of message from Localisation items
			var localisedMessageTexts = Repositories.Configuration.LocalisationItems.Where(x => x.Key == alertMessageKey).ToList();

			if (!localisedMessageTexts.Any()) return;

			//Add the parent message record
			var message = new Message
			              	{
			              		IsSystemAlert = isSystemAlert,
			              		EmployerId = (audience == MessageAudiences.Employer) ? audienceEntityId : null,
			              		ExpiresOn = expiryDate,
			              		Audience = audience,
			              		MessageType = messageType,
			              		EntityId = entityId,
			              		UserId = (audience == MessageAudiences.User) ? audienceEntityId : null,
			              		BusinessUnitId = (audience == MessageAudiences.BusinessUnit) ? audienceEntityId : null,
												CreatedBy = RuntimeContext.CurrentRequest.UserContext.UserId
			              	};

			Repositories.Core.Add(message);
			Repositories.Core.SaveChanges();  

			// Add localised child messages
			foreach (var messageText in localisedMessageTexts.Select(localisedMessageText => new MessageText
								{
									MessageId = message.Id,
									LocalisationId = localisedMessageText.LocalisationId,
									Text = string.Format(localisedMessageText.Value, messageProperties)
								}))
								{
									Repositories.Core.Add(messageText);
								}

			Repositories.Core.SaveChanges();
		}

		/// <summary>
		/// Sends the reminder.
		/// </summary>
		/// <param name="id">The id.</param>
		public void SendReminder(long id)
		{
			var reminder = Repositories.Core.FindById<NoteReminder>(id);

			if (reminder.IsNull())
			{
				throw new Exception(string.Format("The reminder id '{0}' was not found.", id));
			}

			if (reminder.NoteReminderType != NoteReminderTypes.Reminder)
			{
				throw new Exception(string.Format("The record '{0}' is not a reminder.", id));
			}
			
			var recipients = Repositories.Core.Query<NoteReminderRecipient>().Where(x => x.NoteReminderId == reminder.Id).Select(x => x.AsDto()).ToList();

			var businessUnitRecipients = recipients.Where(x => x.RecipientEntityType == EntityTypes.BusinessUnit).Select(x => x.RecipientEntityId).ToList();
			if (businessUnitRecipients.Any())
			{
				foreach (var noteReminderRecipient in businessUnitRecipients)
				{
					var users = Repositories.Core.Query<BusinessUnitUser>().Where(x => x.BusinessUnitId == noteReminderRecipient && x.Enabled).Select(x => x.AsDto()).ToList();
					recipients.AddRange(users.Select(user => new NoteReminderRecipientDto
					{
						NoteReminderId = id,
						RecipientEntityType = EntityTypes.User,
						RecipientEntityId = user.UserId
					}));
				}
			}

			var jobSeekerRecipients = recipients.Where(x => x.RecipientEntityType == EntityTypes.JobSeeker).Select(x => x.RecipientEntityId).ToList();

			if (jobSeekerRecipients.Any())
			{
				foreach (var noteReminderRecipient in jobSeekerRecipients)
				{
					var user = Repositories.Core.Query<User>().Where(x => x.PersonId == noteReminderRecipient && x.Enabled).Select(x => x.AsDto()).SingleOrDefault();
					if (user.IsNotNull() && user.Id.HasValue)
					{
						recipients.Add(new NoteReminderRecipientDto { NoteReminderId = id, RecipientEntityType = EntityTypes.User, RecipientEntityId = user.Id.Value });	
					}
				}
			}


			// Now send the reminder
			switch (reminder.ReminderVia)
			{
				case ReminderMedia.DashboardMessage:

					#region Create dashboard message

					switch (reminder.EntityType)
					{
						case EntityTypes.Job:
							var job = Repositories.Core.FindById<Job>(reminder.EntityId);

							if (job.IsNull())
							{
								throw new Exception(string.Format("The job id  '{0}' was not found.", reminder.EntityId));
							}

							foreach (var noteReminderRecipient in recipients.Where(x => x.RecipientEntityType == EntityTypes.User))
							{
								CreateAlertMessage(Constants.AlertMessageKeys.JobReminder, noteReminderRecipient.RecipientEntityId, DateTime.Now.AddDays(7), MessageAudiences.User, MessageTypes.General, reminder.EntityId, job.JobTitle,
																	 job.Id, Helpers.Localisation.Localise("ReminderCreatedOnDate.Format", "{0:MMM d, yyyy}", DateTime.Now), reminder.Text);
							}

							break;
						case EntityTypes.BusinessUnit:
							var businessUnit = Repositories.Core.FindById<BusinessUnit>(reminder.EntityId);

							foreach (var noteReminderRecipient in recipients.Where(x => x.RecipientEntityType == EntityTypes.User))
							{
								CreateAlertMessage(Constants.AlertMessageKeys.BusinessUnitReminder, noteReminderRecipient.RecipientEntityId, DateTime.Now.AddDays(7), MessageAudiences.User, MessageTypes.General, reminder.EntityId, 
																		businessUnit.Name, Helpers.Localisation.Localise("ReminderCreatedOnDate.Format", "{0:MMM d, yyyy}", DateTime.Now), reminder.Text);
							}
							break;
						case EntityTypes.Employee:
							var user = Repositories.Core.FindById<User>(reminder.CreatedBy);

							foreach (var noteReminderRecipient in recipients.Where(x => x.RecipientEntityType == EntityTypes.User))
							{
								CreateAlertMessage(Constants.AlertMessageKeys.EmployeeReminder, noteReminderRecipient.RecipientEntityId, DateTime.Now.AddDays(7), MessageAudiences.User, MessageTypes.General, reminder.EntityId,
																	 user.Person.FirstName, user.Person.LastName, Helpers.Localisation.Localise("ReminderCreatedOnDate.Format", "{0:MMM d, yyyy}", DateTime.Now), reminder.Text);
							}
							break;

						case EntityTypes.JobSeeker:
							user = Repositories.Core.FindById<User>(reminder.CreatedBy);

							foreach (var noteReminderRecipient in recipients.Where(x => x.RecipientEntityType == EntityTypes.User))
							{
								CreateAlertMessage(Constants.AlertMessageKeys.JobSeekerReminder, noteReminderRecipient.RecipientEntityId, DateTime.Now.AddDays(7), MessageAudiences.User, MessageTypes.General, reminder.EntityId,
																	 user.Person.FirstName, user.Person.LastName, Helpers.Localisation.Localise("ReminderCreatedOnDate.Format", "{0:MMM d, yyyy}", DateTime.Now), reminder.Text);
							}
							break;

						default:
							throw new Exception(string.Format("The reminder service does not currently support the  '{0}' entity.", reminder.EntityType));
					}

					#endregion

					break;
				case ReminderMedia.Email:

					#region Send email

					var emailTemplateType = EmailTemplateTypes.JobReminder;
					var templateData = new EmailTemplateData();

					switch (reminder.EntityType)
					{
						case EntityTypes.Job:
							emailTemplateType = EmailTemplateTypes.JobReminder;
							templateData.JobId = reminder.EntityId.ToString();
							var job = Repositories.Core.FindById<Job>(reminder.EntityId);

							if (job.IsNull())
							{
								throw new Exception(string.Format("The job id  '{0}' was not found.", reminder.EntityId));
							}

							templateData.JobTitle = job.JobTitle;

							break;

						case EntityTypes.BusinessUnit:
							emailTemplateType = EmailTemplateTypes.BusinessUnitReminder;
							var businessUnit = Repositories.Core.FindById<BusinessUnit>(reminder.EntityId);

							if (businessUnit.IsNull())
							{
								throw new Exception(string.Format("The business unit id  '{0}' was not found.", reminder.EntityId));
							}

							templateData.EmployerName = businessUnit.Name;

							break;

						case EntityTypes.Employee:
							emailTemplateType = EmailTemplateTypes.EmployeeReminder;
							var employee = Repositories.Core.FindById<Employee>(reminder.EntityId);

							if (employee.IsNull())
							{
								throw new Exception(string.Format("The employee id  '{0}' was not found.", reminder.EntityId));
							}

							templateData.HiringManagerName = employee.Person.FirstName + " " + employee.Person.LastName;
							break;

						case EntityTypes.JobSeeker:
							emailTemplateType = EmailTemplateTypes.JobSeekerReminder;
							break;

						default:
							throw new Exception(string.Format("The reminder service does not currently support the  '{0}' entity.", reminder.EntityType));
					}

					var emailTemplate = Helpers.Email.GetEmailTemplate(emailTemplateType);

					if (emailTemplate.IsNull())
					{
						throw new Exception(string.Format("The email template '{0}' was not found.", emailTemplateType));
					}

					if (emailTemplate.Body.IsNull())
					{
						throw new Exception(string.Format("The email template '{0}' is not configured.", emailTemplateType));
					}

					var sender = Repositories.Core.FindById<User>(reminder.CreatedBy);

					if (sender.IsNull())
					{
						throw new Exception(string.Format("The user id '{0}' was not found.", reminder.CreatedBy));
					}

					var phoneNumber = String.Empty;
					if (sender.Person.PhoneNumbers.IsNotNullOrEmpty())
					{
						var phone = sender.Person.PhoneNumbers.Where(x => x.IsPrimary).Select(x => x.AsDto()).FirstOrDefault();
						if (phone.IsNotNull()) phoneNumber = phone.Number;
					}

					foreach (var emailRecipient in recipients.Where(x => x.RecipientEntityType == EntityTypes.User))
					{
						var recipient = Repositories.Core.FindById<User>(emailRecipient.RecipientEntityId);

						if (recipient.IsNull())
						{
							throw new Exception(string.Format("The user id '{0}' was not found.", emailRecipient.RecipientEntityId));
						}

						templateData.RecipientName = recipient.Person.FirstName;
						templateData.SenderPhoneNumber = phoneNumber;
						switch (emailTemplate.SenderEmailType)
						{
							case SenderEmailTypes.ClientSpecific:
								templateData.SenderEmailAddress = emailTemplate.ClientSpecificEmailAddress;
								break;
							case SenderEmailTypes.StaffUser:
								templateData.SenderEmailAddress = sender.Person.EmailAddress;
								break;
							default:
                templateData.SenderEmailAddress = RuntimeContext.AppSettings.SystemDefaultSenderEmailAddress;
								break;
						}
						templateData.SenderName = String.Format("{0} {1}", sender.Person.FirstName, sender.Person.LastName);
						templateData.Reminder = reminder.Text;

						var email = Helpers.Email.GetEmailTemplatePreview(emailTemplate, templateData);

						Helpers.Email.SendEmail(recipient.Person.EmailAddress, String.Empty, String.Empty, email.Subject, email.Body);
					}

					#endregion

					break;
			}

			// Set the sent on date
			reminder.ReminderSentOn = DateTime.Now;

			Repositories.Core.SaveChanges();

			Helpers.Logging.LogAction(ActionTypes.SendReminder, reminder);
		}
	}

	#endregion

	#region AlertsHelper Interface

	public interface IAlertsHelper
	{
	  /// <summary>
	  /// Creates the alert message.
	  /// </summary>
	  /// <param name="alertMessageKey">The alert message key.</param>
	  /// <param name="audienceEntityId">The audience entity id.</param>
	  /// <param name="expiryDate">The expiry date.</param>
	  /// <param name="audience">The audience.</param>
	  /// <param name="messageType">Type of the message.</param>
	  /// <param name="entityId">The entity id.</param>
	  /// <param name="messageProperties">The message properties.</param>
	  void CreateAlertMessage(string alertMessageKey, long? audienceEntityId, DateTime expiryDate, MessageAudiences audience, MessageTypes messageType, long? entityId, params object[] messageProperties);

    /// <summary>
    /// Creates the alert message.
    /// </summary>
    /// <param name="alertMessageKey">The alert message key.</param>
    /// <param name="isSystemAlert">Whether this is a system alert</param>
    /// <param name="audienceEntityId">The audience entity id.</param>
    /// <param name="expiryDate">The expiry date.</param>
    /// <param name="audience">The audience.</param>
    /// <param name="messageType">Type of the message.</param>
    /// <param name="entityId">The entity id.</param>
    /// <param name="messageProperties">The message properties.</param>
    void CreateAlertMessage(string alertMessageKey, bool isSystemAlert, long? audienceEntityId, DateTime expiryDate, MessageAudiences audience, MessageTypes messageType, long? entityId, params object[] messageProperties);

		void SendReminder(long id);
	}

	#endregion

}
