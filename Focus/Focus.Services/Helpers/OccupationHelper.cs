﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Linq;

using Focus.Core;
using Focus.Core.Criteria.Onet;
using Focus.Core.Criteria.ROnet;
using Focus.Core.Views;
using Focus.Data.Library.Entities;
using Focus.Services.Core.Onet;
using Focus.Services.Queries;
using OnetTaskView = Focus.Core.Views.OnetTaskView;

using Framework.Caching;
using Framework.Core;

#endregion

namespace Focus.Services.Helpers
{
	#region OccupationHelper Class

	public class OccupationHelper : HelperBase, IOccupationHelper
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="OccupationHelper" /> class.
		/// </summary>
		public OccupationHelper() : this(null)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="OccupationHelper" /> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		public OccupationHelper(IRuntimeContext runtimeContext) : base(runtimeContext)
		{ }

		/// <summary>
		/// Gets the clarified words.
		/// </summary>
		/// <param name="text">The text.</param>
		/// <param name="minScore">The min score.</param>
		/// <param name="maxSearchCount">The max search count.</param>
		/// <param name="type">The type.</param>
		/// <returns></returns>
		public List<string> GetClarifiedWords(string text, int minScore, int maxSearchCount, string type)
		{	
			var clarifiedWords = Repositories.Lens.Clarify(text, minScore, maxSearchCount, type);
			return clarifiedWords;
		}

	  /// <summary>
	  /// Gets the onets.
	  /// </summary>
	  /// <param name="criteria">The criteria.</param>
	  /// <param name="culture">The culture.</param>
	  /// <param name="convertOnet17"></param>
	  /// <returns></returns>
	  public List<OnetDetailsView> GetOnets(OnetCriteria criteria, string culture, bool convertOnet17 = false)
		{

      var onets = new List<OnetDetailsView>();

      if (criteria.OnetId.HasValue)
      {
        onets.Add(GetOnetById(criteria.OnetId.Value, culture));
      }

      if (criteria.OnetCode.IsNotNullOrEmpty())
      {
        onets.Add(GetOnetByCode(criteria.OnetCode, culture));
      }

      // matching titles
      if (criteria.JobTitle.IsNotNullOrEmpty())
      {
        // Get the O*Net Codes from Integrations Search Service if we can
        var onetCodesFromIntegrations = Repositories.Lens.GetOnetCodes(criteria.JobTitle);

				if (onetCodesFromIntegrations.IsNotNullOrEmpty())
				{
					if (convertOnet17)
					{
						onetCodesFromIntegrations = (from o in Repositories.Library.Onet17Onet12MappingViews
						                             where o.Onet17Code == onetCodesFromIntegrations.FirstOrDefault()
						                             select o.Onet12Code).ToList();
					}
					else
					{
						var jobTasks = Repositories.Library.Query<JobTaskView>().Any(x => x.OnetCode == (onetCodesFromIntegrations.FirstOrDefault()));
						if (!jobTasks) onetCodesFromIntegrations = null;
					}
				}

      	List<string> allOnetCodes;

        // If Integrations Search Service did not return the required amount, try the DB
        if (onetCodesFromIntegrations.IsNull() || onetCodesFromIntegrations.Count < criteria.ListSize)
        {
          // Get the O*Net Codes from DB using O*Net online algorithm
          // OnetSearch has been taken from FC and needs a lot of refactoring to fit the Focus structure
          var onetSearch = new OnetSearch(Repositories.Library);
          var onetCodesFromDb = onetSearch.GetOnetCodes(criteria.JobTitle, criteria.ListSize, true);

          // Merge the sets of data
          allOnetCodes = onetCodesFromIntegrations.IsNotNull() ? onetCodesFromIntegrations.Union(onetCodesFromDb).ToList() : onetCodesFromDb;
        }
        else
          allOnetCodes = onetCodesFromIntegrations;

        //allOnetCodes = new List<string>();

        if (allOnetCodes.IsNotNull())
        {
          var onetViews = (from o in Repositories.Library.Query<OnetView>()
                           where allOnetCodes.Contains(o.OnetCode)
                           select o);

          var passedCulture = culture;
          culture = (onetViews.Count(x => passedCulture.IsNotNullOrEmpty() && x.Culture == passedCulture) > 0 ? culture : Constants.DefaultCulture);

          var s = culture;
          var onetCodeAndOccupations = onetViews.Where(x => x.Culture == s && x.JobTasksAvailable).Select(onetView => new OnetDetailsView
          {
            Id = onetView.Id,
            Key = onetView.Key,
            Occupation = onetView.Occupation,
            Description = onetView.Description,
            JobFamilyId = onetView.JobFamilyId,
            OnetCode = onetView.OnetCode
          }).ToList();

          // Add to Onets but not the one that may have been added previously
          onets.AddRange(onetCodeAndOccupations.Where(x => x.Id != criteria.OnetId));
        }
      }
      else if (criteria.JobFamilyId.HasValue)
      {
        // Add to Onets but not the one that may have been added previously
        onets.AddRange(GetCachedOnets(culture, criteria.JobFamilyId.Value).Where(x => x.Id != criteria.OnetId && x.JobTasksAvailable));
      }
      else if (criteria.OnetsWithJobTasks.HasValue && criteria.OnetsWithJobTasks.Value)
      {
        onets.AddRange(GetCachedOnets(culture).Where(onet => onet.JobTasksAvailable));
      }

      onets = onets.Take(criteria.ListSize).ToList();
      return onets;
		}

    /// <summary>
    /// Gets the onets.
    /// </summary>
    /// <param name="criteria">The criteria.</param>
    /// <param name="culture">The culture.</param>
    /// <returns></returns>
    public List<OnetDetailsView> GetOnetsForROnets(OnetCriteria criteria, string culture)
    {
      var onets = new List<OnetDetailsView>();

      if (criteria.OnetId.HasValue)
      {
        onets.Add(GetOnetById(criteria.OnetId.Value, culture));
      }

      if (criteria.OnetCode.IsNotNullOrEmpty())
      {
        onets.Add(GetOnetByCode(criteria.OnetCode, culture));
      }

      // matching titles
      if (criteria.JobTitle.IsNotNullOrEmpty())
      {
        // Get the O*Net Codes from Integrations Search Service if we can
        var onetCodesFromIntegrations = Repositories.Lens.GetOnetCodes(criteria.JobTitle);

        List<string> allOnetCodes;

        // If Integrations Search Service did not return the required amount, try the DB
        if (onetCodesFromIntegrations.IsNull() || onetCodesFromIntegrations.Count < criteria.ListSize)
        {
          // Get the O*Net Codes from DB using O*Net online algorithm
          // OnetSearch has been taken from FC and needs a lot of refactoring to fit the Focus structure
          var onetSearch = new OnetSearch(Repositories.Library);
          var onetCodesFromDb = onetSearch.GetOnetCodes(criteria.JobTitle, criteria.ListSize, true);

          // Merge the sets of data
          allOnetCodes = onetCodesFromIntegrations.IsNotNull() ? onetCodesFromIntegrations.Union(onetCodesFromDb).ToList() : onetCodesFromDb;
        }
        else
          allOnetCodes = onetCodesFromIntegrations;

        //allOnetCodes = new List<string>();

        var onetViews = (from o in Repositories.Library.Query<OnetView>()
                         where allOnetCodes.Contains(o.OnetCode)
                         select o);

        var passedCulture = culture;
        culture = (onetViews.Count(x => passedCulture.IsNotNullOrEmpty() && x.Culture == passedCulture) > 0 ? culture : Constants.DefaultCulture);

        var s = culture;
        var onetCodeAndOccupations = onetViews.Where(x => x.Culture == s).Select(onetView => new OnetDetailsView
        {
          Id = onetView.Id,
          Key = onetView.Key,
          Occupation = onetView.Occupation,
          Description = onetView.Description,
          JobFamilyId = onetView.JobFamilyId,
          OnetCode = onetView.OnetCode
        }).ToList();

        // Add to Onets but not the one that may have been added previously
        onets.AddRange(onetCodeAndOccupations.Where(x => x.Id != criteria.OnetId));
      }
      else if (criteria.JobFamilyId.HasValue)
      {
        // Add to Onets but not the one that may have been added previously
        //onets.AddRange(GetCachedOnets(culture, criteria.JobFamilyId.Value).Where(x => x.Id != criteria.OnetId && x.JobTasksAvailable));
      }

      onets = onets.Take(criteria.ListSize).ToList();
      return onets;
    }

    /// <summary>
    /// Gets the onet.
    /// </summary>
    /// <param name="criteria">The criteria.</param>
    /// <returns></returns>
    public long? GetOnet(OnetCriteria criteria)
    {
      return (from r in Repositories.Library.ROnets
                    where r.Id == criteria.ROnetId
                    select r.OnetId).SingleOrDefault();
      
    }

    /// <summary>
    /// Gets the ronet.
    /// </summary>
    /// <param name="criteria">The criteria.</param>
    /// <returns></returns>
    public long? GetROnet(ROnetCodeCriteria criteria)
    {
      return (from r in Repositories.Library.ROnets
              where r.Code == criteria.ROnetCode
              select r.Id).SingleOrDefault();

    }

    /// <summary>
    /// Gets the Ronet code by id.
    /// </summary>
    /// <param name="ronetId">The ronet id.</param>
    /// <returns></returns>
    public string GetROnetCodeById(long ronetId)
    {
      var ronetCode = (from r in Repositories.Library.ROnets
                       where r.Id == ronetId
                       select r.Code).SingleOrDefault();

      return ronetCode;

    }


		/// <summary>
		/// Gets the ronet by identifier.
		/// </summary>
		/// <param name="rOnetId">The ronet identifier.</param>
		/// <param name="culture">The culture.</param>
		/// <returns></returns>
		public ROnetView GetROnetById(long rOnetId, string culture)
		{
			var rOnetView = new ROnetView();

			var rOnetViews = GetCachedROnets(culture);

			if (rOnetViews.IsNotNullOrEmpty())
				rOnetView = rOnetViews.FirstOrDefault(x => x.Id == rOnetId);

			return rOnetView;
		}

	  /// <summary>
	  /// Gets the Ronets.
	  /// </summary>
	  /// <param name="criteria">The criteria.</param>
	  /// <param name="culture">The culture.</param>
	  /// <returns></returns>
	  public List<ROnetDetailsView> GetROnets(OnetCriteria criteria, string culture)
	  {
      //To be removed when Onet tasks updated
	    var onetDetails = GetOnets(criteria, culture, true);

	    var ronetDetails = new List<ROnetDetailsView>();

	    foreach (var onetDetail in onetDetails)
	    {
		    if (ronetDetails.Count >= criteria.ListSize) continue;

		    var onetDetailId = onetDetail.Id;

				// Get best fit ROnet Id, should only be one per Onet
				var rOnetId = Repositories.Library.OnetROnet.Where(x => x.OnetId == onetDetailId && x.BestFit).Select(x => x.ROnetId).FirstOrDefault();

				// Check if ROnet already in result set
		    if (ronetDetails.Any(x => x.Id == rOnetId)) continue;
				
				// Check ronet has job tasks
		    if (!CheckROnetHasJobTasks(rOnetId)) continue;
				
				// Get Localised ROnet
		    var rOnet = GetROnetById(rOnetId, culture);

		    // Add to result
				ronetDetails.Add(new ROnetDetailsView
				{
					Id = rOnetId,
					Description = onetDetail.Description,
					JobFamilyId = onetDetail.JobFamilyId,
					JobTasksAvailable = onetDetail.JobTasksAvailable,
					OnetCode = onetDetail.OnetCode,
					Occupation = rOnet.Occupation
				});
	    }

	    return ronetDetails.Take(criteria.ListSize).DistinctBy(x=> x.Id).ToList();
	  }

	  /// <summary>
		/// Gets the onet keywords.
		/// </summary>
		/// <param name="onetCode">The onet code.</param>
		/// <returns></returns>
		public List<string> GetOnetKeywords(string onetCode)
		{
			return (
							 from oc in Repositories.Library.OnetCommodities join o in Repositories.Library.Onets on oc.OnetId equals o.Id
							 where o.OnetCode == onetCode
							 select oc.Title
						 ).ToList();
		}

		/// <summary>
		/// Gets the onet tasks.
		/// </summary>
		/// <param name="onetId">The onet id.</param>
		/// <param name="culture">The culture.</param>
		/// <param name="listSize">Size of the list.</param>
		/// <returns></returns>
		public List<OnetTaskView> GetOnetTasks(long onetId, string culture, int listSize)
		{
			var onetTaskKey = "";
			var onetTaskViews = new List<OnetTaskView>();

			if (onetId.IsNotNull())
			{
				onetTaskKey = string.Format(Constants.CacheKeys.OnetTaskKey, culture, onetId);
				onetTaskViews = Cacher.Get<List<OnetTaskView>>(onetTaskKey);
			}

			if (onetTaskViews == null)
			{
				var userCultureOnetTasks = (from ot in Repositories.Library.Query<Data.Library.Entities.OnetTaskView>()
																		where ot.Culture == culture &&
						                          ot.OnetId == onetId
						                    select new OnetTaskView
						                            {
						                              Id = ot.Id,
						                              OnetId = ot.OnetId,
						                              TaskKey = ot.TaskKey,
																					Task = ot.Task,
																					TaskPastTense = ot.TaskPastTense
						                            }).ToList();

				var defaultCultureOnetTasks = (from ot in Repositories.Library.Query<Data.Library.Entities.OnetTaskView>()
						                        where ot.Culture == Constants.DefaultCulture &&
																					ot.OnetId == onetId
						                        select new OnetTaskView
						                                {
						                                  Id = ot.Id,
						                                  OnetId = ot.OnetId,
						                                  TaskKey = ot.TaskKey,
						                                  Task = ot.Task,
																							TaskPastTense = ot.TaskPastTense
						                                }).ToList();

				onetTaskViews = MergeOnetTasks(userCultureOnetTasks, defaultCultureOnetTasks);

				lock (SyncLock)
					Cacher.Set(onetTaskKey, onetTaskViews);
			}

			return onetTaskViews.Take(listSize).ToList();
		}

		/// <summary>
		/// Merges the onet tasks.
		/// </summary>
		/// <param name="cultureSpecificOnetTasks">The culture specific onet tasks.</param>
		/// <param name="defaultCultureOnetTasks">The default culture onet tasks.</param>
		/// <returns></returns>
		private static List<OnetTaskView> MergeOnetTasks(List<OnetTaskView> cultureSpecificOnetTasks, List<OnetTaskView> defaultCultureOnetTasks)
		{
			if (cultureSpecificOnetTasks.Count > 0)
			{
				var removeSet = (from csot in cultureSpecificOnetTasks
												 join dcot in defaultCultureOnetTasks on csot.Id equals dcot.Id
												 select dcot
												).ToList();

				removeSet.ForEach(x => defaultCultureOnetTasks.Remove(x));

				return cultureSpecificOnetTasks.Union(defaultCultureOnetTasks).ToList();
			}

			return defaultCultureOnetTasks;
		}

		/// <summary>
		/// Gets the onet by code.
		/// </summary>
		/// <param name="onetCode">The onet code.</param>
		/// <param name="culture">The culture.</param>
		/// <returns></returns>
		public OnetDetailsView GetOnetByCode(string onetCode, string culture)
		{
			var onetView = new OnetDetailsView();

			var onetViews = GetCachedOnets(culture);

			if (onetViews.IsNotNullOrEmpty())
				onetView = onetViews.FirstOrDefault(x => x.OnetCode == onetCode);
      
			return onetView;
		}

		/// <summary>
		/// Gets the onet by id.
		/// </summary>
		/// <param name="onetId">The onet id.</param>
		/// <param name="culture">The culture.</param>
		/// <returns></returns>
		public OnetDetailsView GetOnetById(long onetId, string culture)
		{
			var onetView = new OnetDetailsView();

			var onetViews = GetCachedOnets(culture);

			if (onetViews.IsNotNullOrEmpty())
				onetView = onetViews.FirstOrDefault(x => x.Id == onetId);
			
			return onetView;
		}

	  /// <summary>
	  /// Gets the onet by occupation.
	  /// </summary>
	  /// <param name="occupation"></param>
	  /// <param name="culture">The culture.</param>
	  /// <returns></returns>
	  public string GetOnetCodeByOccupation(string occupation, string culture)
	  {
	    var onetMappings = (from om in Repositories.Library.Onet17Onet12MappingViews
	                        where om.Occupation == occupation
	                        select om).ToList();

	    return onetMappings.Any() ? onetMappings[0].Onet17Code : string.Empty;
	  }

	  /// <summary>
	  /// Gets the onet family details by id.
	  /// </summary>
	  /// <param name="id"></param>
	  /// <param name="culture">The culture.</param>
	  /// <returns></returns>
    public OnetDetailsView GetOnetFamilyById(long id, string culture)
    {
      var onetView = new OnetDetailsView();

      var onetViews = GetCachedOnets(culture, id);

	    if (onetViews.IsNotNullOrEmpty())
	      onetView = onetViews.First();
      
      return onetView;
    }

	  /// <summary>
		/// Gets the cached onets.
		/// </summary>
		/// <param name="culture">The culture.</param>
		/// <param name="familyId">The family id.</param>
		/// <returns></returns>
		private List<OnetDetailsView> GetCachedOnets(string culture, long? familyId = null)
		{
			var cacheKey = string.Format(Constants.CacheKeys.OnetKey, culture, (familyId.HasValue) ? familyId.Value.ToString() : "ALL");
			var onets = Cacher.Get<List<OnetDetailsView>>(cacheKey);

			if (onets.IsNullOrEmpty())
			{
				lock (SyncLock)
				{
					var cultureOnetQuery = new OnetDetailsViewQuery(Repositories.Library, new OnetViewCriteria { Culture = culture, JobFamilyId  = familyId});
					var cultureOnetViews = cultureOnetQuery.Query().ToList();

					if (culture != Constants.DefaultCulture)
					{
						var defaultCultureOnetQuery = new OnetDetailsViewQuery(Repositories.Library, new OnetViewCriteria { Culture = Constants.DefaultCulture, JobFamilyId = familyId });
						var defaultCultureOnetViews = defaultCultureOnetQuery.Query().ToList();

						onets = MergeOnets(cultureOnetViews, defaultCultureOnetViews);
					}
					else
						onets = cultureOnetViews;

					Cacher.Set(cacheKey, onets);
				}
			}

			return onets;
		}

		/// <summary>
		/// Merges the onets.
		/// </summary>
		/// <param name="cultureSpecificOnets">The culture specific onets.</param>
		/// <param name="defaultCultureOnets">The default culture onets.</param>
		/// <returns></returns>
		private List<OnetDetailsView> MergeOnets(List<OnetDetailsView> cultureSpecificOnets, List<OnetDetailsView> defaultCultureOnets)
		{
			if (cultureSpecificOnets.Count > 0)
			{
				var removeSet = (from cso in cultureSpecificOnets
												 join dco in defaultCultureOnets on cso.Id equals dco.Id
												 select dco
												).ToList();

				removeSet.ForEach(x => defaultCultureOnets.Remove(x));

				return cultureSpecificOnets.Union(defaultCultureOnets).ToList();
			}

			return defaultCultureOnets;
		}

		/// <summary>
		/// Gets the cached ronets.
		/// </summary>
		/// <param name="culture">The culture.</param>
		private List<ROnetView> GetCachedROnets(string culture)
		{
			var cacheKey = string.Format(Constants.CacheKeys.ROnetKey, culture);
			var ronets = Cacher.Get<List<ROnetView>>(cacheKey);

			if (ronets.IsNullOrEmpty())
			{
				lock (SyncLock)
				{
					var cultureROnetViews = GetROnetsFromDatabase(culture);

					if (culture != Constants.DefaultCulture)
					{
						var defaultCultureROnetViews = GetROnetsFromDatabase(Constants.DefaultCulture);

						ronets = MergeROnets(cultureROnetViews, defaultCultureROnetViews);
					}
					else
						ronets = cultureROnetViews;

					Cacher.Set(cacheKey, ronets);
				}
			}

			return ronets;
		}

		/// <summary>
		/// Gets the r onets from database.
		/// </summary>
		/// <param name="culture">The culture.</param>
		/// <returns></returns>
		private List<ROnetView> GetROnetsFromDatabase(string culture)
		{
			return (from r in Repositories.Library.ROnets
				join ol in Repositories.Library.OnetLocalisationItems on r.Key equals ol.Key
				join l in Repositories.Library.Localisations on ol.LocalisationId equals l.Id
				where l.Culture == culture
				select new ROnetView
				{
					Id = r.Id,
					Code = r.Code,
					Occupation = ol.PrimaryValue
				}).ToList();
		}

		/// <summary>
		/// Merges the ronets.
		/// </summary>
		/// <param name="cultureSpecificROnets">The culture specific ronets.</param>
		/// <param name="defaultCultureROnets">The default culture ronets.</param>
		/// <returns></returns>
		private List<ROnetView> MergeROnets(List<ROnetView> cultureSpecificROnets, List<ROnetView> defaultCultureROnets)
		{
			if (cultureSpecificROnets.Count > 0)
			{
				var removeSet = (from csr in cultureSpecificROnets
												 join dcr in defaultCultureROnets on csr.Id equals dcr.Id
												 select dcr
												).ToList();

				removeSet.ForEach(x => defaultCultureROnets.Remove(x));

				return cultureSpecificROnets.Union(defaultCultureROnets).ToList();
			}

			return defaultCultureROnets;
		}

		/// <summary>
		/// Gets the job tasks.
		/// </summary>
		/// <param name="onetId">The onet id.</param>
		/// <param name="scope">The scope.</param>
		/// <param name="culture">The culture.</param>
		/// <returns></returns>
		public List<JobTaskDetailsView> GetJobTasks(long? onetId, JobTaskScopes scope, string culture)
		{
			var jobTaskQuery = Repositories.Library.Query<JobTaskView>().Where(x => x.OnetId == onetId && x.Scope == scope).AsQueryable();
			var jobTaskCulture = (jobTaskQuery.Any(x => x.Culture == culture)) ? culture : Constants.DefaultCulture;

			var jobTaskViews = (from jt in jobTaskQuery.Where(x => x.Culture == jobTaskCulture)
													select new JobTaskDetailsView { JobTaskId = jt.Id, Prompt = jt.Prompt, Response = jt.Response, JobTaskType = jt.JobTaskType, Certificate = jt.Certificate}).ToList();

			if (jobTaskViews.Count > 0)
			{
				var jobTaskMultiOptions = Repositories.Library.Query<JobTaskMultiOptionView>().Where(x => x.OnetId == onetId && x.Culture == jobTaskCulture).ToList();

				foreach (var jobTaskView in jobTaskViews)
				{
					var multiOptionPrompts = jobTaskMultiOptions.Where(x => x.JobTaskId == jobTaskView.JobTaskId).Select(x => x.Prompt).ToList();

					if (multiOptionPrompts.Count > 0)
						jobTaskView.MultiOptionPrompts.AddRange(multiOptionPrompts);
				}
			}

			return jobTaskViews;
		}

		/// <summary>
		/// Localises the onet occupation.
		/// </summary>
		/// <param name="onetKey">The onet key.</param>
		/// <param name="culture">The culture.</param>
		/// <returns></returns>
		public string LocaliseOnetOccupation(string onetKey, string culture)
		{
			var result = "";

			var onetViews = (from o in Repositories.Library.Query<OnetView>()
											 where o.Key == onetKey
											 select o);

			if (onetViews.Any())
			{
				result = onetViews.Where(x => x.Culture == culture).Select(x => x.Occupation).SingleOrDefault();

				if (result.IsNullOrEmpty() && (culture != Constants.DefaultCulture)) result = onetViews.Where(x => x.Culture == Constants.DefaultCulture).Select(x => x.Occupation).SingleOrDefault();

				if (result.IsNullOrEmpty()) result = "";
			}

			return result;
		}

		/// <summary>
		/// Checks the ronet has job tasks.
		/// </summary>
		/// <param name="rOnetId">The ronet identifier.</param>
		/// <returns></returns>
		private bool CheckROnetHasJobTasks(long? rOnetId)
		{
			if (!rOnetId.HasValue || rOnetId.Value == 0)
				return false;

			var onetId = Repositories.Library.ROnetOnets.Where(x => x.ROnetId == rOnetId && x.BestFit).Select(x => x.OnetId).FirstOrDefault();

			return onetId.IsNotNull() && Repositories.Library.JobTasks.Any(x => x.OnetId == onetId);
		}
	}

	#endregion

	#region OccupationHelper Interface

	public interface IOccupationHelper
	{
		/// <summary>
		/// Gets the clarified words.
		/// </summary>
		/// <param name="text">The text.</param>
		/// <param name="minScore">The min score.</param>
		/// <param name="maxSearchCount">The max search count.</param>
		/// <param name="type">The type.</param>
		/// <returns></returns>
		List<string> GetClarifiedWords(string text, int minScore, int maxSearchCount, string type);

	  /// <summary>
	  /// Gets the onets.
	  /// </summary>
	  /// <param name="criteria">The criteria.</param>
	  /// <param name="culture">The culture.</param>
	  /// <param name="convertOnet17"></param>
	  /// <returns></returns>
	  List<OnetDetailsView> GetOnets(OnetCriteria criteria, string culture, bool convertOnet17 = false);

    /// <summary>
    /// Gets the onet.
    /// </summary>
    /// <param name="criteria">The criteria.</param>
    /// <returns></returns>
    long? GetOnet(OnetCriteria criteria);

	  /// <summary>
	  /// Gets the ronet.
	  /// </summary>
	  /// <param name="criteria">The criteria.</param>
	  /// <returns></returns>
	  long? GetROnet(ROnetCodeCriteria criteria);

    /// <summary>
    /// Gets the ronets.
    /// </summary>
    /// <param name="criteria">The criteria.</param>
    /// <param name="culture">The culture.</param>
    /// <returns></returns>
    List<ROnetDetailsView> GetROnets(OnetCriteria criteria, string culture);

		/// <summary>
		/// Gets the onet keywords.
		/// </summary>
		/// <param name="onetCode">The onet code.</param>
		/// <returns></returns>
		List<string> GetOnetKeywords(string onetCode);

		/// <summary>
		/// Gets the onet tasks.
		/// </summary>
		/// <param name="onetId">The onet id.</param>
		/// <param name="culture">The culture.</param>
		/// <param name="listSize">Size of the list.</param>
		/// <returns></returns>
		List<OnetTaskView> GetOnetTasks(long onetId, string culture, int listSize);

		/// <summary>
		/// Gets the onet by code.
		/// </summary>
		/// <param name="onetCode">The onet code.</param>
		/// <param name="culture">The culture.</param>
		/// <returns></returns>
		OnetDetailsView GetOnetByCode(string onetCode, string culture);

		/// <summary>
		/// Gets the onet by id.
		/// </summary>
		/// <param name="onetId">The onet id.</param>
		/// <param name="culture">The culture.</param>
		/// <returns></returns>
		OnetDetailsView GetOnetById(long onetId, string culture);

	  /// <summary>
	  /// Gets the onet by occuaption.
	  /// </summary>
	  /// <param name="occupation"></param>
	  /// <param name="culture">The culture.</param>
	  /// <returns></returns>
	  string GetOnetCodeByOccupation(string occupation, string culture);

    /// <summary>
    /// Gets the family details by id.
    /// </summary>
    /// <param name="id"></param>
    /// <param name="culture">The culture.</param>
    /// <returns></returns>
    OnetDetailsView GetOnetFamilyById(long id, string culture);

		//test only
	  string GetROnetCodeById(long ronetId);

		/// <summary>
		/// Gets the job tasks.
		/// </summary>
		/// <param name="onetId">The onet id.</param>
		/// <param name="scope">The scope.</param>
		/// <param name="culture">The culture.</param>
		/// <returns></returns>
		List<JobTaskDetailsView> GetJobTasks(long? onetId, JobTaskScopes scope, string culture);

		/// <summary>
		/// Localises the onet occupation.
		/// </summary>
		/// <param name="onetKey">The onet key.</param>
		/// <param name="culture">The culture.</param>
		/// <returns></returns>
		string LocaliseOnetOccupation(string onetKey, string culture);

		/// <summary>
		/// Gets the ronet by identifier.
		/// </summary>
		/// <param name="rOnetId">The ronet identifier.</param>
		/// <param name="culture">The culture.</param>
		/// <returns></returns>
		ROnetView GetROnetById(long rOnetId, string culture);
	}

	#endregion
}