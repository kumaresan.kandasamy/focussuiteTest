﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;

using Focus.Core;
using Focus.Core.Criteria.CandidateSearch;
using Focus.Core.Models.Career;
using Focus.Core.Views;
using Focus.Data.Core.Entities;
using Focus.Services.DtoMappers;

using Framework.Core;

#endregion


namespace Focus.Services.Helpers
{
  #region SearchHelper Class

  public class SearchHelper : HelperBase, ISearchHelper
  {
    private const string ServicePersistedSearchKeyPrefix = "ServicePersistedSearch";

    /// <summary>
    /// Initializes a new instance of the <see cref="SearchHelper"/> class.
    /// </summary>
    public SearchHelper() : base(null) { }

    /// <summary>
    /// Initializes a new instance of the <see cref="SearchHelper"/> class.
    /// </summary>
    /// <param name="runtimeContext">The runtime context.</param>
    public SearchHelper(IRuntimeContext runtimeContext) : base(runtimeContext) { }

    public ResumeSearchResult SearchResumes(CandidateSearchCriteria criteria, Guid searchId, bool forCount = false)
    {
      var result = new ResumeSearchResult();

      #region Help of what this is all doing

      // Based on the ScopeType (CandidateSearchScopeTypes) we will be asking for 1 or more things:
      //
      // If "Open" then it is just a basic open search 
      //
      // If "JobApplicants" then we need to get list of applications for a particular job or if no job in scope any applicants for the employer 
      //
      // If "Flagged" or "FlaggedAndRecommended" then we need to use the FlaggedJobSeeker for this employee.
			//
			// If "JobInvitees" then we need to get a list of invitees for the job.
			
			#endregion

      var sessionStateKey = GenerateSessionStateKey(searchId);

      var jobId = (criteria.JobDetailsCriteria.IsNotNull()) ? criteria.JobDetailsCriteria.JobId : 0;

      // Get a list of all applicants for any job the employer has had
      var employerApplicantIds = Repositories.Core.ApplicationViews
                                                  .Where(x => x.EmployerId == RuntimeContext.CurrentRequest.UserContext.EmployerId && x.CandidateApplicationApprovalStatus == ApprovalStatuses.Approved)
                                                  .Select(x => x.CandidateId)
                                                  .ToList();

      // Get a list of all applicants for the job in scope
      var jobApplicants = (jobId > 0) ? Repositories.Core.ApplicationViews.Where(x => x.JobId == jobId && x.CandidateApplicationApprovalStatus == ApprovalStatuses.Approved).ToList() : new List<ApplicationView>();
			var jobApplicantsReferred = (jobId > 0) ? Repositories.Core.ApplicationViews.Where(x => x.JobId == jobId && x.CandidateApplicationApprovalStatus != ApprovalStatuses.Approved && x.CandidateApplicationApprovalStatus != ApprovalStatuses.None).ToList() : new List<ApplicationView>();

      // Get a list of all applicants for the job in scope
	    var jobApplicantsAwaitingApproval = (jobId > 0) ? Repositories.Core.ApplicationViews.Where(x => x.JobId == jobId && x.CandidateApplicationApprovalStatus == ApprovalStatuses.WaitingApproval ).ToList() : new List<ApplicationView>();

      // Get a list of the employees flagged candidates
      var flaggedApplicants = Repositories.Core.PersonListCandidateViews.Where(x => x.PersonId == RuntimeContext.CurrentRequest.UserContext.PersonId && x.ListType == ListTypes.FlaggedJobSeeker).ToList();

			// Get a list of invitees for the job.
	    var jobInvitees = Repositories.Core.InviteToApplyBasicViews.Where(x => x.JobId == jobId);

      // Check to see if we need to do anything
      if ((criteria.ScopeType == CandidateSearchScopeTypes.JobApplicants && (jobApplicants.IsNullOrEmpty() && jobId > 0))
          || (criteria.ScopeType == CandidateSearchScopeTypes.JobApplicants && (employerApplicantIds.IsNullOrEmpty() && jobId == 0))
          || (criteria.ScopeType == CandidateSearchScopeTypes.Flagged && flaggedApplicants.IsNullOrEmpty())
					|| (criteria.ScopeType == CandidateSearchScopeTypes.JobInvitees && jobInvitees.IsNullOrEmpty() && jobId == 0)
				 )
			{
        result.Resumes = null;
        return result;
      }

      // First attempt to get search result from persisted data
      // The idea is it will be a lot faster to pull the search from the database, for paging, rather than from Integrations Search Service
      List<ResumeView> candidates = null;

      if (searchId != Guid.Empty)
        candidates = Helpers.Session.GetSessionState<List<ResumeView>>(RuntimeContext.CurrentRequest.SessionId, sessionStateKey);

      if (candidates != null)
      {
        result.SearchId = searchId;
      }
      else
      {
        if (criteria.JobDetailsCriteria.IsNotNull() && criteria.JobDetailsCriteria.JobId.IsNotNull() && criteria.JobDetailsCriteria.Posting.IsNullOrEmpty())
          criteria.JobDetailsCriteria.Posting = Repositories.Core.Jobs.Where(x => x.Id == criteria.JobDetailsCriteria.JobId).Select(x => x.PostingHtml).SingleOrDefault();

        #region Call the Lens repository

        // Get a list of candidate scope id's 
        switch (criteria.ScopeType)
        {
          case CandidateSearchScopeTypes.JobApplicants:
            criteria.ScopeSet = (jobId > 0) ? jobApplicants.Select(x => x.CandidateId.ToString()).ToList() : employerApplicantIds.Select(x => x.ToString()).ToList();
            break;

          case CandidateSearchScopeTypes.Flagged:
            criteria.ScopeSet = flaggedApplicants.Select(x => x.Id.ToString()).ToList();
            break;

          case CandidateSearchScopeTypes.JobApplicantsAwaitingApproval:
            criteria.ScopeSet = jobApplicantsAwaitingApproval.Select(x => x.Id.ToString()).ToList();
            break;

					case CandidateSearchScopeTypes.JobInvitees:
						criteria.ScopeSet = jobInvitees.Select(x => x.Id.ToString()).ToList();
		        break;

          default:
            if (criteria.CandidateId.IsNotNull() && criteria.CandidateId > 0)
            {
              var candidateId = new List<string> { criteria.CandidateId.ToString() };
              criteria.ScopeSet = candidateId;

              if (criteria.SearchType == CandidateSearchTypes.OpenSearch)
                criteria.MinimumScore = 0;
            }
            else
            {
              criteria.ScopeSet = null;
            }
            break;
        }

        candidates = Repositories.Lens.SearchResumes(criteria, RuntimeContext.CurrentRequest.UserContext.Culture);
           var activeUserIds = Repositories.Core.Users.Where(x => x.Enabled == true).Select(x => x.PersonId).ToList();
           var underAgeSeekers = Repositories.Core.Persons.Where(p => p.Age < AppSettings.UnderAgeJobSeekerRestrictionThreshold).Select(x => x.Id).ToList();
               candidates = candidates.Where(x => x.Id.IsNotNull() && activeUserIds.Contains(x.Id) && !underAgeSeekers.Contains(x.Id)).ToList();

				if (criteria.ScopeType == CandidateSearchScopeTypes.Open && !forCount && criteria.LogSearch)
					// Log the User ID of the person running the search
					Helpers.Logging.LogAction(ActionTypes.SearchCandidates, typeof(User).Name, RuntimeContext.CurrentRequest.UserContext.UserId, true);

        #endregion

        #region Recently Placed Matches

        if (criteria.GetRecentlyMatchedInfo && jobId != 0)
        {
          var businessUnitId = Repositories.Core.Jobs.Where(x => x.Id == criteria.JobDetailsCriteria.JobId).Select(x => x.BusinessUnitId).SingleOrDefault();

          if (businessUnitId.IsNotNull() && businessUnitId > 0)
          {
            var candidateIds = candidates.Select(x => x.Id).ToList();

            var candidatePossibleRecentlyPlacedMatches = Repositories.Core.RecentlyPlacedPersonMatchViews.Where(x => x.BusinessUnitId == businessUnitId && candidateIds.Contains(x.MatchedPersonId)).Select(x => x.AsDto()).ToList();

            if (candidatePossibleRecentlyPlacedMatches.IsNotNullOrEmpty())
            {
              var candidatesWithPossibleRecentlyPlacedMatches = candidatePossibleRecentlyPlacedMatches.Select(x => x.MatchedPersonId).Distinct().ToList();

              // Filter out ignored matches
              var usersPossibleRecentlyPlacedMatchesToIgnore = Repositories.Core.RecentlyPlacedMatchesToIgnore.Where(x => candidatesWithPossibleRecentlyPlacedMatches.Contains(x.PersonId)).Select(x => new { x.PersonId, x.RecentlyPlacedId }).ToList();

              if (usersPossibleRecentlyPlacedMatchesToIgnore.IsNotNullOrEmpty())
              {
                foreach (var match in candidatePossibleRecentlyPlacedMatches.Where(match => usersPossibleRecentlyPlacedMatchesToIgnore.Any(x => x.PersonId == match.MatchedPersonId && x.RecentlyPlacedId == match.RecentlyPlacedId)))
                {
                  candidatePossibleRecentlyPlacedMatches.Remove(match);
                }
              }

              foreach (var rv in candidates.Where(rv => candidatePossibleRecentlyPlacedMatches.Any(x => x.MatchedPersonId == rv.Id)))
              {
                // Get the best match
                var match = candidatePossibleRecentlyPlacedMatches.Where(x => x.MatchedPersonId == rv.Id).OrderByDescending(x => x.Score).First();

                if (match.IsNull()) continue;

                rv.HasRecentlyPlacedMatch = true;

                rv.RecentlyPlacedPersonId = match.PlacedPersonId;
                rv.RecentlyPlacedCandidateName = match.PlacedCandidateName;
                rv.RecentlyPlacedScore = match.Score;
                rv.RecentlyPlacedJobTitle = match.JobTitle;
                rv.RecentlyPlaceBusinessUnitName = match.BusinessUnitName;
                rv.RecentlyPlacedMatchId = match.Id;
              }
            }
          }
        }

        #endregion

        if (!forCount)
        {
          result.SummarisedSearchResults = candidates.Select(x => new Tuple<long, int>(x.Id, x.Score)).ToList();

          if (candidates.IsNotNullOrEmpty())
						candidates = UpdateCachedFocusData(candidates, employerApplicantIds, jobApplicants, ((!RuntimeContext.CurrentRequest.UserContext.IsShadowingUser && AppSettings.Module == FocusModules.Talent) || AppSettings.Module == FocusModules.MessageBus), jobApplicantsReferred);

          result.SearchId = Guid.NewGuid();
          sessionStateKey = GenerateSessionStateKey(result.SearchId);

          // Persist all the candidates 
          Helpers.Session.SetSessionState(RuntimeContext.CurrentRequest.SessionId, sessionStateKey, candidates);
          Repositories.Core.SaveChanges(true);
        }

        // Required for count and tests
        result.Resumes = new PagedList<ResumeView>(candidates, candidates.Count, criteria.PageIndex, criteria.PageSize);
      }

      var employerNotes = Repositories.Core.CandidateNoteViews.Where(x => x.EmployerId == RuntimeContext.CurrentRequest.UserContext.EmployerId).ToList();

      if (candidates.IsNotNullOrEmpty())
				candidates = UpdateNonCachedFocusData(candidates, jobApplicants, flaggedApplicants, employerNotes, jobApplicantsReferred, employerApplicantIds);

      // Do sorting, Veterans should always appear on top (this does not apply for candidates like this)
      if (criteria.SearchType != CandidateSearchTypes.CandidatesLikeThis)
      {
        var orderBy = (criteria.OrderBy.IsNotNullOrEmpty()) ? criteria.OrderBy : "PoolResultSortBys.Score";

        switch (orderBy)
        {
          case "PoolResultSortBys.Status":
            candidates = candidates.OrderByDescending(x => x.Status).ThenByDescending(x => x.IsVeteran).ThenByDescending(x => x.Score).ToList();
            break;
          case "PoolResultSortBys.ApplicationReceivedOn":
            candidates = candidates.OrderByDescending(x => x.ApplicationReceivedOn).ThenByDescending(x => x.IsVeteran).ThenByDescending(x => x.Score).ToList();
            break;
          default:
            candidates = candidates.OrderByDescending(x => x.StarRating).ThenByDescending(x => x.IsVeteran).ThenByDescending(x => x.Score).ToList();
            break;
        }
      }

      // Return Paged Data
      result.Resumes = new PagedList<ResumeView>(candidates, candidates.Count, criteria.PageIndex, criteria.PageSize);
			
			result.HighestStarRating = (candidates.IsNotNullOrEmpty()) ? candidates.Max(x => x.StarRating) : 0;

      if (criteria.GetNotesAndRemindersCount)
      {
        var candidateIds = result.Resumes.Select(r => r.Id).ToList();

        var notesCounts = Repositories.Core.NoteReminderViews
                                      .Where(n => candidateIds.Contains(n.EntityId) && n.EntityType == EntityTypes.JobSeeker)
                                      .GroupBy(n => n.EntityId)
                                      .Select(group => new
                                      {
                                        Id = group.Key,
                                        Count = group.Count()
                                      })
                                      .ToDictionary(n => n.Id, n => n.Count);

        result.Resumes.ForEach(r => r.NotesAndRemindersCount = notesCounts.Keys.Contains(r.Id) ? notesCounts[r.Id] : r.NotesAndRemindersCount);
      }

      return result;
    }

	  /// <summary>
	  /// Searches the postings.
	  /// </summary>
	  /// <param name="criteria">The criteria.</param>
	  /// <param name="userId">The user id performing the search (If not the one specified by user context).</param>
	  /// <param name="personId">The person id performing the search (If not the one specified by user context).</param>
	  /// <param name="isForSearchAlert">Whether this search is running as part of the overnight search alert.</param>
	  /// <param name="manualSearch"></param>
	  /// <returns></returns>
	  public List<PostingSearchResultView> SearchPostings(SearchCriteria criteria, long? userId = null, long? personId = null, bool? isForSearchAlert = null, bool manualSearch = true)
    {
      int? originalMaximumDocumentCount = null;
      int? maximumDocumentCount = null;

      var originalRequiredResultCriteria = criteria.RequiredResultCriteria;
      if (originalRequiredResultCriteria.IsNotNull())
        originalMaximumDocumentCount = originalRequiredResultCriteria.MaximumDocumentCount;

		  List<string> postingIdsToExclude = null;

      var userContext = RuntimeContext.CurrentRequest.UserContext;

      if (userId.IsNull())
        userId = userContext.UserId;

      if (personId.IsNull())
        personId = userContext.PersonId;

      var userType = Repositories.Core.Users
                                      .Where(user => user.Id == userId)
                                      .Select(user => user.UserType)
                                      .FirstOrDefault();

      var doVeteranSearch = false;

      if ((userType.GetValueOrDefault(UserTypes.Career) & UserTypes.Career) > 0)
      {
        var isVeteran = Repositories.Core.Resumes
                                         .Where(resume => resume.PersonId == personId && resume.IsDefault && resume.StatusId == ResumeStatuses.Active && resume.ResumeCompletionStatusId == ResumeCompletionStatuses.Completed)
                                         .Select(resume => resume.IsVeteran)
                                         .FirstOrDefault();

        if (AppSettings.VeteranPriorityServiceEnabled &&  !isVeteran.GetValueOrDefault(false))
        {
          // Get jobs still exclusive to veterans to ignore
          postingIdsToExclude = (from job in Repositories.Core.Jobs
                                 join posting in Repositories.Core.Postings
                                   on job.Id equals posting.JobId
                                 where job.VeteranPriorityEndDate > DateTime.Now
                                 select posting.LensPostingId).ToList();

          doVeteranSearch = AppSettings.VeteranPriorityServiceEnabled && isForSearchAlert.GetValueOrDefault(false);
          /*
          if (isForSearchAlert.GetValueOrDefault(false) && AppSettings.VeteranPriorityServiceEnabled)
          {
            // Posting age needs to look an extra day back
            criteria.PostingAgeCriteria.PostingAgeInDays += 1;
            var postingAge = criteria.PostingAgeCriteria.PostingAgeInDays;

            // When searching overnight, ignore non-veteran jobs from the oldest day as these should have been included in the previous search
            postingIdsToExclude.AddRange(from job in Repositories.Core.Jobs
                                         join posting in Repositories.Core.Postings
                                           on job.Id equals posting.JobId
                                         where job.VeteranPriorityEndDate == null
                                               && job.PostedOn >= DateTime.Now.Date.AddDays(0 - postingAge)
                                               && job.PostedOn < DateTime.Now.AddDays(0 - postingAge + 1)
                                         select posting.LensPostingId);
          }
          */
        }
        /*
        else if (isForSearchAlert.GetValueOrDefault(false) && AppSettings.VeteranPriorityServiceEnabled)
        {
          var postingAge = criteria.PostingAgeCriteria.PostingAgeInDays;

          // For veterans, ignore all veteran jobs 
          postingIdsToExclude = (from job in Repositories.Core.Jobs
                                  join posting in Repositories.Core.Postings
                                    on job.Id equals posting.JobId
                                  where job.VeteranPriorityEndDate != null
                                    && job.PostedOn >= DateTime.Now.Date.AddDays(0 - postingAge)
                                  select posting.LensPostingId).ToList();
        }
        */

        maximumDocumentCount = AdjustCriteriaForIgnoredPostings(criteria, postingIdsToExclude);
      }

      var postings = Repositories.Lens.SearchPostings(criteria, RuntimeContext.CurrentRequest.UserContext.Culture, isForSearchAlert.GetValueOrDefault(), personId);

      if (doVeteranSearch)
      {
        // Perform a search for the day before the search to pick up jobs that may have been previously excluded due to VPS
        var originalPostingAge = criteria.PostingAgeCriteria.PostingAgeInDays;
        criteria.PostingAgeCriteria.MinimumPostingAgeInDays =
        criteria.PostingAgeCriteria.PostingAgeInDays = originalPostingAge + 1;

        var originalEducationCriteria = criteria.EducationSchoolCriteria;
        if (originalEducationCriteria.IsNull())
          criteria.EducationSchoolCriteria = new EducationSchoolCriteria();

        criteria.EducationSchoolCriteria.OnlyShowExclusivePostings = true;

        // Ignore non-veteran jobs from the oldest day as these should have been included in the previous search
        var postingAge = criteria.PostingAgeCriteria.PostingAgeInDays;
        postingIdsToExclude.AddRange(from job in Repositories.Core.Jobs
                                     join posting in Repositories.Core.Postings
                                       on job.Id equals posting.JobId
                                     where job.VeteranPriorityEndDate == null
                                           && job.PostedOn >= DateTime.Now.Date.AddDays(0 - postingAge)
                                           && job.PostedOn < DateTime.Now.AddDays(0 - postingAge + 1)
                                     select posting.LensPostingId);

        AdjustCriteriaForIgnoredPostings(criteria, postingIdsToExclude);

        var extraPostings = Repositories.Lens.SearchPostings(criteria, RuntimeContext.CurrentRequest.UserContext.Culture, isForSearchAlert.GetValueOrDefault());
        var existingPostingIds = postings.Select(posting => posting.Id).ToList();

        postings.AddRange(extraPostings.Where(post => !existingPostingIds.Contains(post.Id)));

				postings = Repositories.Lens.OrderPostings(postings, criteria.IsMatchedSearch(),
					(criteria.RequiredResultCriteria.IsNotNull() && criteria.RequiredResultCriteria.MaximumDocumentCount.HasValue &&
					 criteria.RequiredResultCriteria.MaximumDocumentCount > 0)
						? criteria.RequiredResultCriteria.MaximumDocumentCount.Value
						: AppSettings.MaximumNoDocumentToReturnInSearch);

        criteria.EducationSchoolCriteria = originalEducationCriteria;
        criteria.PostingAgeCriteria.PostingAgeInDays = originalPostingAge;
        criteria.PostingAgeCriteria.MinimumPostingAgeInDays = null;
      }

      if (postings.IsNotNullOrEmpty())
      {
        #region Remove the user's excluded postings from the search results

				var excludedLensPostingIds = Helpers.Organization.GetExcludedLensPostings(Convert.ToInt64(personId));
        postings = postings.Where(x => !excludedLensPostingIds.Contains(x.Id)).ToList();

        #endregion

        // Remove any veteran jobs
			  if (postingIdsToExclude.IsNotNullOrEmpty())
        {
          var adjustedPostings = postings.Where(x => !postingIdsToExclude.Contains(x.Id));
          if (maximumDocumentCount.IsNotNull())
            adjustedPostings = adjustedPostings.Take(maximumDocumentCount.Value);

          postings = adjustedPostings.ToList();
        }
      }

      // Restore original document count in criteria
      criteria.RequiredResultCriteria = originalRequiredResultCriteria;
      if (criteria.RequiredResultCriteria.IsNotNull())
        criteria.RequiredResultCriteria.MaximumDocumentCount = originalMaximumDocumentCount;

      Helpers.Logging.LogAction(ActionTypes.JobSearch, typeof(Person).Name, RuntimeContext.CurrentRequest.UserContext.PersonId, true);
      
			if (manualSearch && AppSettings.Module.IsIn(FocusModules.CareerExplorer, FocusModules.Career, FocusModules.Explorer))
			{
				Helpers.Logging.LogAction(ActionTypes.RunManualJobSearch, typeof(Person).Name, RuntimeContext.CurrentRequest.UserContext.PersonId, true);
				if (userId.HasValue)
					Helpers.SelfService.PublishSelfServiceActivity(ActionTypes.RunManualJobSearch, RuntimeContext.CurrentRequest.UserContext.ActionerId, userId.Value);
			}

      var criterion = criteria;

      var isHighGrowthSearch = (criterion.IsNotNull() && criterion.JobSectorCriteria.IsNotNull()) && criterion.JobSectorCriteria.RequiredJobSectorIds.IsNotNullOrEmpty();

      if (isHighGrowthSearch)
        Helpers.Logging.LogAction(ActionTypes.JobSearchHighGrowth, typeof(Person).Name, RuntimeContext.CurrentRequest.UserContext.PersonId, true);

      return postings;
    }

		/// <summary>
		/// Gets the resume search session.
		/// </summary>
		/// <param name="searchId">The search identifier.</param>
		/// <param name="starRating">The star rating.</param>
		/// <returns></returns>
	  public List<ResumeView> GetResumeSearchSession(Guid searchId, int? starRating)
	  {
			var sessionStateKey = GenerateSessionStateKey(searchId);
			var resumes = Helpers.Session.GetSessionState<List<ResumeView>>(RuntimeContext.CurrentRequest.SessionId, sessionStateKey);

			if(resumes.IsNull())
				throw new Exception("Search session has expired");

		  return (starRating.HasValue) ? resumes.Where(x => x.StarRating >= starRating).ToList() : resumes;
	  }

		#region Private methods

		private int? AdjustCriteriaForIgnoredPostings(SearchCriteria criteria, List<string> postingIdsToExclude)
    {
			if (criteria.RequiredResultCriteria.IsNull())
        criteria.RequiredResultCriteria = new RequiredResultCriteria();

      int? maximumDocumentCount = criteria.RequiredResultCriteria.MaximumDocumentCount.IsNotNull()
	      ? criteria.RequiredResultCriteria.MaximumDocumentCount.Value
	      : AppSettings.MaximumNoDocumentToReturnInSearch;

      if (postingIdsToExclude.IsNotNull())
        criteria.RequiredResultCriteria.MaximumDocumentCount = maximumDocumentCount + postingIdsToExclude.Count;

      return maximumDocumentCount;
    }

    /// <summary>
    /// Generates the session key.
    /// </summary>
    /// <param name="searchId">The search id.</param>
    /// <returns></returns>
    private static string GenerateSessionStateKey(Guid searchId)
    {
      return string.Format("{0}:{1}", ServicePersistedSearchKeyPrefix, searchId);
    }

    #region Candidate Search Functions

		/// <summary>
		/// Updates the cached focus data.
		/// </summary>
		/// <param name="candidates">The candidates.</param>
		/// <param name="employerApplicantIds">The employer applicant Ids.</param>
		/// <param name="jobApplicants">The job applicants.</param>
		/// <param name="maskName">if set to <c>true</c> [mask name].</param>
		/// <param name="jobApplicantsReferred">The job applicants referred.</param>
		/// <returns></returns>
		private List<ResumeView> UpdateCachedFocusData(List<ResumeView> candidates, List<long> employerApplicantIds, List<ApplicationView> jobApplicants, bool maskName, List<ApplicationView> jobApplicantsReferred)
    {
      foreach (var candidate in candidates)
      {
				if (AppSettings.ShowContactDetails != ShowContactDetails.Partial && maskName)
	      {
		      // Mask name if has not applied for any employer jobs
          CandidateHelper.UpdateResumeViewName(candidate, employerApplicantIds, RuntimeContext);
	      }
        
        // Get date application received on
        if (jobApplicants.IsNotNullOrEmpty())
        {
          var jobApplicant = jobApplicants.FirstOrDefault(x => x.CandidateId == candidate.Id);
          if (jobApplicant.IsNotNull())
            candidate.ApplicationReceivedOn = jobApplicant.CandidateApplicationReceivedOn;
        }

				if (jobApplicantsReferred.IsNotNullOrEmpty())
				{
					var jobApplicant = jobApplicantsReferred.FirstOrDefault(x => x.CandidateId == candidate.Id);

					if (jobApplicant.IsNotNull())
					{
						// Application Status
						candidate.Status = jobApplicant.CandidateApplicationStatus;

						// Application Approval Status
						candidate.ApplicationApprovalStatus = jobApplicant.CandidateApplicationApprovalStatus;

						// Application Id
						candidate.ApplicationId = jobApplicant.Id;

						// Lock version
						candidate.ApplicationLockVersion = jobApplicant.LockVersion;
					}
				}
      }

      return candidates;
    }

		/// <summary>
		/// Updates the non cached focus data.
		/// </summary>
		/// <param name="candidates">The candidates.</param>
		/// <param name="jobApplicants">The job applicants.</param>
		/// <param name="flaggedApplicants">The flagged applicants.</param>
		/// <param name="employerNotes">The employer notes.</param>
		/// <param name="jobApplicantsReferred">The job applicants referred.</param>
		/// <returns></returns>
		private List<ResumeView> UpdateNonCachedFocusData(List<ResumeView> candidates, List<ApplicationView> jobApplicants, List<PersonListCandidateView> flaggedApplicants, List<CandidateNoteView> employerNotes, List<ApplicationView> jobApplicantsReferred, List<long> employerApplicantIds)
    {
      foreach (var candidate in candidates)
      {
        candidate.Status = ApplicationStatusTypes.NotApplicable;
        candidate.ApplicationApprovalStatus = ApprovalStatuses.None;

        if (jobApplicants.IsNotNullOrEmpty())
        {
          var jobApplicant = jobApplicants.FirstOrDefault(x => x.CandidateId == candidate.Id);

          if (jobApplicant.IsNotNull())
          {
            // Application Status
            candidate.Status = jobApplicant.CandidateApplicationStatus;

            // Application Approval Status
            candidate.ApplicationApprovalStatus = jobApplicant.CandidateApplicationApprovalStatus;
          }
        }

				if (jobApplicantsReferred.IsNotNullOrEmpty())
				{
					var jobApplicant = jobApplicantsReferred.FirstOrDefault(x => x.CandidateId == candidate.Id);

					if (jobApplicant.IsNotNull())
					{
						// Application Status
						candidate.Status = jobApplicant.CandidateApplicationStatus;

						// Application Approval Status
						candidate.ApplicationApprovalStatus = jobApplicant.CandidateApplicationApprovalStatus;

						// Application Id
						candidate.ApplicationId = jobApplicant.Id;

						// Lock version
						candidate.ApplicationLockVersion = jobApplicant.LockVersion;
					}
				}

        // Flagged Status
        candidate.Flagged = (flaggedApplicants.IsNotNullOrEmpty() && flaggedApplicants.Exists(x => x.CandidatePersonId == candidate.Id));

				// Candidate has applied for any job for an employer
				candidate.IsApplicantForEmployer = employerApplicantIds.IsNotNullOrEmpty() && employerApplicantIds.Exists(id => id == candidate.Id);

        // Applicant Note
        var note = employerNotes.FirstOrDefault(x => x.CandidateId == candidate.Id);

        if (note.IsNotNull()) candidate.EmployerNote = note.Note;
      }
      return candidates;
    }

    #endregion

		#endregion

	}

  #endregion

  #region SearchHelper Interface

  public interface ISearchHelper
  {
    ResumeSearchResult SearchResumes(CandidateSearchCriteria criteria, Guid searchId, bool forCount = false);

		List<PostingSearchResultView> SearchPostings(SearchCriteria criteria, long? userId = null, long? personId = null, bool? isForSearchAlert = null, bool manualSearch = true);

	  List<ResumeView> GetResumeSearchSession(Guid searchId, int? starRating);
  }

  #endregion

}
