﻿#region Copyright © 2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;

using Framework.Caching;
using Framework.Core;
using Framework.Core.Attributes;

using Focus.Common.Models;
using Focus.Core;
using Focus.Data.Configuration.Entities;

#endregion

namespace Focus.Services.Helpers
{
	#region LocalisationHelper Class

	public class LocalisationHelper : HelperBase, ILocalisationHelper
	{
		private LocalisationDictionary _localisationDictionary;
	  private const string CacheCheckPrefix = "-Check";

		///// <summary>
		///// Initializes a new instance of the <see cref="LocalisationHelper"/> class.
		///// </summary>
		//public LocalisationHelper() : base(null) {}

		/// <summary>
		/// Initializes a new instance of the <see cref="LocalisationHelper"/> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		public LocalisationHelper(IRuntimeContext runtimeContext) : base(runtimeContext)
		{ }

		/// <summary>
		/// Localises the specified key.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		public string Localise(string key)
		{
      CheckLocalisationDictionary();
			var s = TryGetLocalisationItem(key, "");
			return LocaliseConfiguablePlaceHolders((s.IsNotNullOrEmpty()) ? s : string.Format("[{0}]", key));
		}

		/// <summary>
		/// Localises the specified key.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <returns></returns>
		public string Localise(string key, string defaultValue)
		{
		  CheckLocalisationDictionary();
			var s = TryGetLocalisationItem(key, defaultValue);
			return LocaliseConfiguablePlaceHolders((s.IsNotNullOrEmpty()) ? s : defaultValue);
		}

		/// <summary>
		/// Localises the specified key.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <param name="args">The args.</param>
		/// <returns></returns>
		public string Localise(string key, string defaultValue, params object[] args)
		{
		  CheckLocalisationDictionary();
			var s = TryGetLocalisationItem(key, defaultValue);
			return (s.IsNotNullOrEmpty()) ? LocaliseConfiguablePlaceHolders(string.Format(s, args)) : string.Empty;
		}

		/// <summary>
		/// Localises the specified identifier.
		/// </summary>
		/// <param name="id">The identifier.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <param name="args">The arguments.</param>
		/// <returns></returns>
		public string Localise(long? id, string defaultValue, params object[] args)
		{
			var lookupItem = id.HasValue ? Repositories.Configuration.FindById<LookupItemsView>(id) : null;
			return string.Format(lookupItem.IsNotNull() ? lookupItem.Value : defaultValue, args);
		}

		/// <summary>
		/// Gets the code item localised value.
		/// </summary>
		/// <param name="codeItemId">The code item id.</param>
		/// <returns></returns>
		public string GetCodeItemLocalisedValue(long codeItemId)
		{
		  CheckLocalisationDictionary();
			var codeItemKey = Repositories.Configuration.CodeItems.Where(x => x.Id == codeItemId).Select(x => x.Key).SingleOrDefault();
			return codeItemKey.IsNullOrEmpty() ? codeItemId.ToString() : Localise(codeItemKey, codeItemKey);
		}

	  /// <summary>
	  /// Gets the code item localised value.
	  /// </summary>
	  /// <param name="key"></param>
	  /// <returns></returns>
	  public LocalisationDictionaryEntry GetLocalisationItem(string key)
    {
      var localisationItem = Repositories.Configuration.LocalisationItems.SingleOrDefault(x => x.Key == key);
      var localisationDictionaryEntry = new LocalisationDictionaryEntry();
      if (localisationItem.IsNotNull())
      {
        localisationDictionaryEntry.LocaliseKey = key;
        localisationDictionaryEntry.LocalisedValue = localisationItem.Value;
      }

      return localisationDictionaryEntry;
    }

		/// <summary>
		/// Gets the enum localised text.
		/// </summary>
		/// <param name="enumValue">The enum value.</param>
    /// <param name="useAnnotation">Whether to use the annotation for the enum as a default</param>
    /// <returns></returns>
		public string GetEnumLocalisedText(Enum enumValue, bool useAnnotation = false)
		{
		  var defaulValue = useAnnotation
		                      ? enumValue.GetAttributeValue<EnumLocalisationDefaultAttribute>()
		                      : string.Concat("** ", enumValue, " **");
			return GetEnumLocalisedText(enumValue, defaulValue);
		}

		/// <summary>
		/// Gets the enum localised text.
		/// </summary>
		/// <param name="enumValue">The enum value.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <returns></returns>
		public string GetEnumLocalisedText(Enum enumValue, string defaultValue)
		{
			return Localise(enumValue.GetType().Name + "." + enumValue, defaultValue);
		}

    /// <summary>
    /// Checks if the localisation dictionary needs to be re-read from the cache.
    /// This is achieved by checking a simple "check" flag in the cache. If the cache has been reset, the flag will not be there
    /// </summary>
    private void CheckLocalisationDictionary()
    {
      var getFromCache = false;

      var culture = RuntimeContext.CurrentRequest.UserContext.Culture;
      var localisationDictionaryCacheKey = string.Format(Constants.CacheKeys.LocalisationDictionary, culture);
      var localisationDictionaryCacheCheckKey = string.Concat(localisationDictionaryCacheKey, CacheCheckPrefix);

      if (_localisationDictionary.IsNull())
      {
        getFromCache = true;
      }
      else
      {
        var checkCache = Cacher.Get<string>(localisationDictionaryCacheCheckKey);
        if (checkCache.IsNullOrEmpty())
          getFromCache = true;
      }

      if (getFromCache)
      {
        _localisationDictionary = GetLocalisationDictionary();
        Cacher.Set(localisationDictionaryCacheCheckKey, "1");
      }
    }

		/// <summary>
		/// Gets the localisation dictionary.
		/// </summary>
		/// <returns></returns>
		public LocalisationDictionary GetLocalisationDictionary()
		{
			var culture = RuntimeContext.CurrentRequest.UserContext.Culture;

			var localisationDictionaryCacheKey = string.Format(Constants.CacheKeys.LocalisationDictionary, culture);
			var localisationDictionary = GetCachedLocalisationDictionary(localisationDictionaryCacheKey);

			if (localisationDictionary.IsNullOrEmpty())
			{
				// Users Culture
				var keySet1 = GetLocalisationItems(culture);

				// Users Config + Default Culture
				var rootSet = new LocalisationDictionary();
				if (culture != Constants.DefaultCulture)
					rootSet = GetLocalisationItems(Constants.DefaultCulture);

        localisationDictionary = MergeLists(rootSet, keySet1);
				Cacher.Set(localisationDictionaryCacheKey, localisationDictionary);
			}

			return localisationDictionary;
		}

    /// <summary>
    /// Clears the existing cache and refreshes it with the latest localisation data
    /// </summary>
    /// <returns>The latest localisation data</returns>
    public LocalisationDictionary RefreshLocalisationDictionary()
    {
      var culture = RuntimeContext.CurrentRequest.UserContext.Culture;

      var localisationDictionaryCacheKey = string.Format(Constants.CacheKeys.LocalisationDictionary, culture);
      RemoveCachedLocalisationDictionary(localisationDictionaryCacheKey);

      _localisationDictionary = GetLocalisationDictionary();
      return _localisationDictionary;
    }

		/// <summary>
		/// Localises the configuable place holders.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <returns></returns>
		public string LocaliseConfiguablePlaceHolders(string source)
		{
		  CheckLocalisationDictionary();

			if (source.IsNullOrEmpty()) return source;

			return source.Replace(Constants.PlaceHolders.FocusTalent, AppSettings.FocusTalentApplicationName).
										Replace(Constants.PlaceHolders.FocusAssist, AppSettings.FocusAssistApplicationName).
										Replace(Constants.PlaceHolders.FocusCareer, AppSettings.FocusCareerApplicationName).
										Replace(Constants.PlaceHolders.FocusReporting, AppSettings.FocusReportingApplicationName).
										Replace(Constants.PlaceHolders.FocusExplorer, AppSettings.FocusExplorerApplicationName).
										Replace(Constants.PlaceHolders.SupportEmail, AppSettings.SupportEmail).
										Replace(Constants.PlaceHolders.SupportPhone, AppSettings.SupportPhone).
										Replace(Constants.PlaceHolders.SupportHoursOfBusiness, AppSettings.SupportHoursOfBusiness).
										Replace(Constants.PlaceHolders.SupportHelpTeam, AppSettings.SupportHelpTeam).
										Replace(Constants.PlaceHolders.SchoolName, AppSettings.SchoolName).
										Replace(Constants.PlaceHolders.CandidateType, GetThemeTypes(Constants.PlaceHolders.CandidateType)).
										Replace(Constants.PlaceHolders.EmploymentType, GetThemeTypes(Constants.PlaceHolders.EmploymentType)).
										Replace(Constants.PlaceHolders.EmploymentTypes, GetThemeTypes(Constants.PlaceHolders.EmploymentTypes)).
										Replace(Constants.PlaceHolders.PostingType, GetThemeTypes(Constants.PlaceHolders.PostingType)).
										Replace(Constants.PlaceHolders.CandidateTypes, GetThemeTypes(Constants.PlaceHolders.CandidateTypes)).
										Replace(Constants.PlaceHolders.Business, GetThemeTypes(Constants.PlaceHolders.Business)).
										Replace(Constants.PlaceHolders.Businesses, GetThemeTypes(Constants.PlaceHolders.Businesses));
		}

		/// <summary>
		/// Gets the cached localisation dictionary.
		/// </summary>
		/// <param name="localisationCacheKey">The localisation cache key.</param>
		/// <returns></returns>
		private static LocalisationDictionary GetCachedLocalisationDictionary(string localisationCacheKey)
		{
			return Cacher.Get<LocalisationDictionary>(localisationCacheKey);
		}

    /// <summary>
    /// Removes the cached localisation dictionary.
    /// </summary>
    /// <param name="localisationCacheKey">The localisation cache key.</param>
    /// <returns></returns>
    private static void RemoveCachedLocalisationDictionary(string localisationCacheKey)
    {
      var localisationCacheCheckKey = string.Concat(localisationCacheKey, CacheCheckPrefix);

      Cacher.Remove(localisationCacheKey);
      Cacher.Remove(localisationCacheCheckKey);
    }

		/// <summary>
		/// Gets the localisation items.
		/// </summary>
		/// <param name="culture">The culture.</param>
		/// <returns></returns>
		private LocalisationDictionary GetLocalisationItems(string culture)
		{
		  var localisationQuery = from li in Repositories.Configuration.LocalisationItems
		                          join l in Repositories.Configuration.Localisations on li.LocalisationId equals l.Id
		                          where l.Culture == culture
		                          select new LocalisationDictionaryEntry
		                          {
		                            LocaliseKey = li.Key,
		                            LocalisedValue = li.Value,
		                            LocaliseKeyUpper = li.Key.ToUpper()
		                          };

      return new LocalisationDictionary(localisationQuery);
		}

    /// <summary>
    /// Merges the lists.
    /// </summary>
    /// <param name="rootSet">The root set.</param>
    /// <param name="keySet">The key set.</param>
    /// <returns></returns>
    private static LocalisationDictionary MergeLists(LocalisationDictionary rootSet, LocalisationDictionary keySet)
    {
      if (keySet.Any())
      {
        foreach (var ks in keySet.Values)
        {
          if (rootSet.ContainsKey(ks.LocaliseKeyUpper))
            rootSet[ks.LocaliseKeyUpper] = ks;
          else
            rootSet.Add(ks.LocaliseKeyUpper, ks);
        }
      }

      return rootSet;
    }

		/// <summary>
		/// Tries the get localisation item.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <returns></returns>
		private string TryGetLocalisationItem(string key, string defaultValue)
		{
		  var keyUpper = key.ToUpper();
		  var value = _localisationDictionary.ContainsKey(keyUpper) ? _localisationDictionary[keyUpper].LocalisedValue : defaultValue;

			try
			{
				if (AppSettings.SaveDefaultLocalisationItems)
				{
					if (!Repositories.Configuration.DefaultLocalisationItems.Any(x => x.Key == key))
					{
						Repositories.Configuration.Add(new DefaultLocalisationItem { Key = key, DefaultValue = defaultValue ?? "" });
						Repositories.Configuration.SaveChanges();
					}
				}		
			}
			catch
			{
				// Do Nothing
			}

			return value;
		}

		/// <summary>
		/// Gets the type of candidate and employment according to the Theme.
		/// </summary>
		/// <returns></returns>
		private String GetThemeTypes(string placeholderType)
		{
			var localisedItem = String.Empty;

			switch (AppSettings.Theme)
			{
				case FocusThemes.Workforce:
					switch (placeholderType)
					{
						case Constants.PlaceHolders.CandidateType:
							localisedItem = TryGetLocalisationItem("Global.CandidateType.Workforce", "Job seeker");
							break;
							
						case Constants.PlaceHolders.CandidateTypes:
							localisedItem = TryGetLocalisationItem("Global.CandidateTypes.Workforce", "Job seekers");
							break;

						case Constants.PlaceHolders.EmploymentType:
							localisedItem = TryGetLocalisationItem("Global.EmploymentType.Workforce", "Job posting");
							break;

						case Constants.PlaceHolders.EmploymentTypes:
							localisedItem = TryGetLocalisationItem("Global.EmploymentTypes.Workforce", "Job postings");
							break;

						case Constants.PlaceHolders.Business:
							localisedItem = TryGetLocalisationItem("Global.Business.Workforce", "Business");
							break;

						case Constants.PlaceHolders.Businesses:
							localisedItem = TryGetLocalisationItem("Global.Businesses.Workforce", "Businesses");
							break;
							
						default:
							localisedItem = TryGetLocalisationItem("Global.PostingType.Workforce", "Job");
							break;
					}
					break;

				case FocusThemes.Education:
					switch (placeholderType)
					{
						case Constants.PlaceHolders.CandidateType:
							localisedItem = TryGetLocalisationItem("Global.CandidateType.Education", "Student/Alumni");
							break;

						case Constants.PlaceHolders.EmploymentType:
							localisedItem = TryGetLocalisationItem("Global.EmploymentType.Education", "Job Order/Internships");
							break;

						case Constants.PlaceHolders.CandidateTypes:
							localisedItem = TryGetLocalisationItem("Global.CandidateTypes.Education", "Students");
							break;

						case Constants.PlaceHolders.Business:
							localisedItem = TryGetLocalisationItem("Global.Business.Education", "Employer");
							break;

						case Constants.PlaceHolders.Businesses:
							localisedItem = TryGetLocalisationItem("Global.Businesses.Education", "Employers");
							break;

						default:
							localisedItem = TryGetLocalisationItem("Global.PostingType.Education", "Job/Internship");
							break;
					}
					break;
			}

			return localisedItem;
    }    
  }

	#endregion

	#region LocalisationHelper Interface

	public interface ILocalisationHelper
	{
		/// <summary>
		/// Localises the specified key.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		string Localise(string key);

		/// <summary>
		/// Localises the specified key.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <returns></returns>
		string Localise(string key, string defaultValue);

		/// <summary>
		/// Localises the specified key.
		/// </summary>
		/// <param name="key">The key.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <param name="args">The args.</param>
		/// <returns></returns>
		string Localise(string key, string defaultValue, params object[] args);

		/// <summary>
		/// Localises the specified identifier.
		/// </summary>
		/// <param name="id">The identifier.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <param name="args">The arguments.</param>
		/// <returns></returns>
		string Localise(long? id, string defaultValue, params object[] args);

		/// <summary>
		/// Gets the code item localised value.
		/// </summary>
		/// <param name="codeItemId">The code item id.</param>
		/// <returns></returns>
		string GetCodeItemLocalisedValue(long codeItemId);

	  /// <summary>
	  /// Gets the code item localised value.
	  /// </summary>
	  /// <param name="key"></param>
	  /// <returns></returns>
	  LocalisationDictionaryEntry GetLocalisationItem(string key);

	  /// <summary>
	  /// Gets the enum localised text.
	  /// </summary>
	  /// <param name="enumValue">The enum value.</param>
	  /// <param name="useAnnotation">Whether to use the annotation for the enum as a default</param>
	  /// <returns></returns>
	  string GetEnumLocalisedText(Enum enumValue, bool useAnnotation = false);

		/// <summary>
		/// Gets the enum localised text.
		/// </summary>
		/// <param name="enumValue">The enum value.</param>
		/// <param name="defaultValue">The default value.</param>
		/// <returns></returns>
		string GetEnumLocalisedText(Enum enumValue, string defaultValue);

		/// <summary>
		/// Gets the localisation dictionary.
		/// </summary>
		/// <returns></returns>
		LocalisationDictionary GetLocalisationDictionary();

	  /// <summary>
	  /// Clears the existing cache and refreshes it with the latest localisation data
	  /// </summary>
	  /// <returns>The latest localisation data</returns>
	  LocalisationDictionary RefreshLocalisationDictionary();

		/// <summary>
		/// Localises the configuable place holders.
		/// </summary>
		/// <param name="source">The source.</param>
		/// <returns></returns>
		string LocaliseConfiguablePlaceHolders(string source);
	}

	#endregion
}
