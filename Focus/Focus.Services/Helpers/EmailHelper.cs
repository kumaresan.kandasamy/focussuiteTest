﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using System.Net.Mail;
using System.Text.RegularExpressions;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.EmailTemplate;
using Focus.Core.Views;
using Focus.Services.DtoMappers;
using Focus.Services.Messages;

using Framework.Core;
using Framework.Email;
using Framework.Logging;

#endregion

namespace Focus.Services.Helpers
{

	#region EmailHelper class

	public class EmailHelper : HelperBase, IEmailHelper
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="EmailHelper"/> class.
		/// </summary>
		public EmailHelper() : base(null) {}

		/// <summary>
		/// Initializes a new instance of the <see cref="EmailHelper"/> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		public EmailHelper(IRuntimeContext runtimeContext) : base(runtimeContext) {}

		/// <summary>
		/// Sends the email.
		/// </summary>
		/// <param name="mailMessage">The mail message.</param>
		/// <param name="sendAsync">if set to <c>true</c> [send async].</param>
		/// <param name="isBodyHtml">if set to <c>true</c> [is body HTML].</param>
		public void SendEmail(MailMessage mailMessage, bool sendAsync = false, bool isBodyHtml = true)
		{
			if(RuntimeContext.CurrentRequest.IsNull() || RuntimeContext.CurrentRequest.UserContext.IsNull())
				throw new Exception("Anonymous emailing not allowed.");

			if (mailMessage.To.Count == 0) throw new ArgumentException("The mail message contains no recipients");

			var doPublish = true;

			if (RuntimeContext.AppSettings.DirectEmails)
			{
				try
				{
					Emailer.SendEmail(mailMessage, sendAsync, isBodyHtml);

					doPublish = false;
				}
				catch (Exception ex)
				{
					Logger.Error("Unable to send message", ex);
				}
			}
			
			if (doPublish)
			{
				Helpers.Messaging.Publish(new SendEmailMessage
				{
					MailMessage = mailMessage,
					SendAsync = RuntimeContext.AppSettings.ASyncEmails || sendAsync,
					IsBodyHtml = isBodyHtml,
					ActionerId = RuntimeContext.CurrentRequest.UserContext.ActionerId,
					Culture = RuntimeContext.CurrentRequest.UserContext.Culture
				});
			}
		}

		/// <summary>
		/// Sends the email.
		/// </summary>
		/// <param name="to">To.</param>
		/// <param name="cc">The cc.</param>
		/// <param name="bcc">The BCC.</param>
		/// <param name="subject">The subject.</param>
		/// <param name="body">The body.</param>
		/// <param name="sendAsync">if set to <c>true</c> [send async].</param>
		/// <param name="isBodyHtml">if set to <c>true</c> [is body HTML].</param>
		/// <param name="attachment">The attachment.</param>
		/// <param name="attachmentName">Name of the attachment.</param>
		/// <param name="detectUrl">Whether to detect Url and make it an html link</param>
		/// <param name="senderAddress">Optional email address to send from</param>
		/// <param name="footerType">Type of footer.</param>
		/// <exception cref="System.Exception">Anonymous emailing not allowed.</exception>
		public void SendEmail(string to, string cc, string bcc, string subject, string body, bool sendAsync = false, bool isBodyHtml = true, byte[] attachment = null, string attachmentName = null, bool detectUrl = false, string senderAddress = null, EmailFooterTypes footerType = EmailFooterTypes.None, string footerUrl = null)
    {
			if (RuntimeContext.CurrentRequest.IsNull() || RuntimeContext.CurrentRequest.UserContext.IsNull())
				throw new Exception("Anonymous emailing not allowed.");

      if (detectUrl && isBodyHtml)
      {
        var urlRegex = AppSettings.UrlRegExPattern;
        if (urlRegex.IsNotNullOrEmpty())
        {
          if (urlRegex.StartsWith("^"))
            urlRegex = urlRegex.Substring(1);
          if (urlRegex.EndsWith("$"))
            urlRegex = urlRegex.Substring(0, urlRegex.Length - 1);

          var regex = new Regex(string.Concat("(", urlRegex, ")"));
          body = regex.Replace(body, @"<a href='$1'>$1</a>");
        }
      }

			if (footerType != EmailFooterTypes.None)
			{
				var footer = string.Empty;

				//generate footer
				switch (footerType)
				{
					case EmailFooterTypes.HiringManagerUnsubscribe:
					case EmailFooterTypes.JobSeekerUnsubscribe:
						footer = Helpers.Localisation.Localise("UnsubscribeEmailFooter.Text", AppSettings.UnsubscribeEmailFooter);
						footer = string.Format(footer, footerUrl);
						if (!footer.EndsWith("<br />", StringComparison.OrdinalIgnoreCase) && !footer.EndsWith("<br>", StringComparison.OrdinalIgnoreCase))
							footer = string.Concat(footer, "<br /><br />");

						break;
				}

				body = string.Concat(body, footer);
			}

			var doPublish = true;

			if (RuntimeContext.AppSettings.DirectEmails)
			{
				try
				{
					Emailer.SendEmail(senderAddress,
														to,
														cc,
														bcc,
														subject,
														body,
														RuntimeContext.AppSettings.ASyncEmails || sendAsync,
														isBodyHtml,
														attachment,
														attachmentName);

					doPublish = false;
				}
				catch (Exception ex)
				{
					Logger.Error("Unable to send message", ex);
				}
			}
			
			if (doPublish)
			{
				Helpers.Messaging.Publish(new SendEmailMessage
				{
					From = senderAddress,
					To = to,
					Cc = cc,
					Bcc = bcc,
					Subject = subject,
					Body = body,
					SendAsync = RuntimeContext.AppSettings.ASyncEmails || sendAsync,
					IsBodyHtml = isBodyHtml,
					ActionerId = RuntimeContext.CurrentRequest.UserContext.ActionerId,
					Culture = RuntimeContext.CurrentRequest.UserContext.Culture,
					Attachment = attachment,
					AttachmentName = attachmentName
				});
			}
		}

        public void SendQueuedEmail(string to, string cc, string bcc, string subject, string body, DateTime SendEmailOn, bool sendAsync = false, bool isBodyHtml = true, byte[] attachment = null, string attachmentName = null, bool detectUrl = false, string senderAddress = null, EmailFooterTypes footerType = EmailFooterTypes.None, string footerUrl = null)
        {
            if (RuntimeContext.CurrentRequest.IsNull() || RuntimeContext.CurrentRequest.UserContext.IsNull())
				throw new Exception("Anonymous emailing not allowed.");

      if (detectUrl && isBodyHtml)
      {
        var urlRegex = AppSettings.UrlRegExPattern;
        if (urlRegex.IsNotNullOrEmpty())
        {
          if (urlRegex.StartsWith("^"))
            urlRegex = urlRegex.Substring(1);
          if (urlRegex.EndsWith("$"))
            urlRegex = urlRegex.Substring(0, urlRegex.Length - 1);

          var regex = new Regex(string.Concat("(", urlRegex, ")"));
          body = regex.Replace(body, @"<a href='$1'>$1</a>");
        }
      }

			if (footerType != EmailFooterTypes.None)
			{
				var footer = string.Empty;

				//generate footer
				switch (footerType)
				{
					case EmailFooterTypes.HiringManagerUnsubscribe:
					case EmailFooterTypes.JobSeekerUnsubscribe:
						footer = Helpers.Localisation.Localise("UnsubscribeEmailFooter.Text", AppSettings.UnsubscribeEmailFooter);
						footer = string.Format(footer, footerUrl);
						if (!footer.EndsWith("<br />", StringComparison.OrdinalIgnoreCase) && !footer.EndsWith("<br>", StringComparison.OrdinalIgnoreCase))
							footer = string.Concat(footer, "<br /><br />");

						break;
				}

				body = string.Concat(body, footer);
			}

            Helpers.Messaging.Publish(new SendEmailMessage
            {
                From = senderAddress,
                To = to,
                Cc = cc,
                Bcc = bcc,
                Subject = subject,
                Body = body,
                SendAsync = RuntimeContext.AppSettings.ASyncEmails || sendAsync,
                IsBodyHtml = isBodyHtml,
                ActionerId = RuntimeContext.CurrentRequest.UserContext.ActionerId,
                Culture = RuntimeContext.CurrentRequest.UserContext.Culture,
                Attachment = attachment,
                AttachmentName = attachmentName
            },null,false,SendEmailOn);
        }

		/// <summary>
		/// Sends the email from template.
		/// </summary>
		/// <param name="type">The type.</param>
		/// <param name="data">The data.</param>
		/// <param name="to">To.</param>
		/// <param name="cc">The cc.</param>
		/// <param name="bcc">The BCC.</param>
		/// <param name="sendAsync">if set to <c>true</c> [send asynchronous].</param>
		/// <param name="isBodyHtml">if set to <c>true</c> [is body HTML].</param>
		/// <param name="attachment">The attachment.</param>
		/// <param name="attachmentName">Name of the attachment.</param>
		/// <param name="detectUrl">if set to <c>true</c> [detect URL].</param>
		/// <param name="senderAddress">The sender address.</param>
		/// <exception cref="System.Exception">Anonymous emailing not allowed.</exception>
		public void SendEmailFromTemplate(EmailTemplateTypes type, EmailTemplateData data, string to, string cc, string bcc, bool sendAsync = false, bool isBodyHtml = true, byte[] attachment = null, string attachmentName = null, bool detectUrl = false)
		{
			if (RuntimeContext.CurrentRequest.IsNull() || RuntimeContext.CurrentRequest.UserContext.IsNull())
				throw new Exception("Anonymous emailing not allowed.");

			// This code taken from SendEmailFromTemplateMessage
			var emailTemplate = RuntimeContext.Helpers.Email.GetEmailTemplatePreview(type, data);

			if (detectUrl && isBodyHtml)
			{
				var urlRegex = AppSettings.UrlRegExPattern;
				if (urlRegex.IsNotNullOrEmpty())
				{
					if (urlRegex.StartsWith("^"))
						urlRegex = urlRegex.Substring(1);
					if (urlRegex.EndsWith("$"))
						urlRegex = urlRegex.Substring(0, urlRegex.Length - 1);

					var regex = new Regex(string.Concat("(", urlRegex, ")"));
					emailTemplate.Body = regex.Replace(emailTemplate.Body, @"<a href='$1'>$1</a>");
				}
			}

			RuntimeContext.Helpers.Email.SendEmail(to, cc, bcc, emailTemplate.Subject, emailTemplate.Body, sendAsync, isBodyHtml, attachment, attachmentName);

			// The SendEmailFromTemplateMessage was just re-calling the EmailHelper anyway, so instead call it directly
			/* 
				Helpers.Messaging.Publish(new SendEmailFromTemplateMessage
				{
					TemplateType = type,
					TemplateData = data,
					To = to,
					Cc = cc,
					Bcc = bcc,
					SendAsync = sendAsync,
					IsBodyHtml = isBodyHtml,
					ActionerId = RuntimeContext.CurrentRequest.UserContext.ActionerId,
					Culture = RuntimeContext.CurrentRequest.UserContext.Culture,
					Attachment = attachment,
					AttachmentName = attachmentName,
					DetectUrl = detectUrl
				});				
			*/
		}

        /// <summary>
        /// Sends the email from template.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="data">The data.</param>
        /// <param name="to">To.</param>
        /// <param name="cc">The cc.</param>
        /// <param name="bcc">The BCC.</param>
        /// <param name="SendEmailOn">The DateTime email to be sent on.</param>
        /// <param name="sendAsync">if set to <c>true</c> [send asynchronous].</param>
        /// <param name="isBodyHtml">if set to <c>true</c> [is body HTML].</param>
        /// <param name="attachment">The attachment.</param>
        /// <param name="attachmentName">Name of the attachment.</param>
        /// <param name="detectUrl">if set to <c>true</c> [detect URL].</param>
        /// <param name="senderAddress">The sender address.</param>
        /// <exception cref="System.Exception">Anonymous emailing not allowed.</exception>
        public void SendQueuedEmailFromTemplate(EmailTemplateTypes type, EmailTemplateData data, string to, string cc, string bcc, DateTime SendEmailOn, bool sendAsync = false, bool isBodyHtml = true, byte[] attachment = null, string attachmentName = null, bool detectUrl = false)
        {
            if (RuntimeContext.CurrentRequest.IsNull() || RuntimeContext.CurrentRequest.UserContext.IsNull())
                throw new Exception("Anonymous emailing not allowed.");

            // This code taken from SendEmailFromTemplateMessage
            var emailTemplate = RuntimeContext.Helpers.Email.GetEmailTemplatePreview(type, data);

            if (detectUrl && isBodyHtml)
            {
                var urlRegex = AppSettings.UrlRegExPattern;
                if (urlRegex.IsNotNullOrEmpty())
                {
                    if (urlRegex.StartsWith("^"))
                        urlRegex = urlRegex.Substring(1);
                    if (urlRegex.EndsWith("$"))
                        urlRegex = urlRegex.Substring(0, urlRegex.Length - 1);

                    var regex = new Regex(string.Concat("(", urlRegex, ")"));
                    emailTemplate.Body = regex.Replace(emailTemplate.Body, @"<a href='$1'>$1</a>");
                }
            }

            RuntimeContext.Helpers.Email.SendQueuedEmail(to, cc, bcc, emailTemplate.Subject, emailTemplate.Body, SendEmailOn, sendAsync, isBodyHtml, attachment, attachmentName);
        }


		/// <summary>
		/// Gets the email template.
		/// </summary>
		/// <param name="emailTemplateType">Type of the email template.</param>
		/// <returns></returns>
		public EmailTemplateDto GetEmailTemplate(EmailTemplateTypes emailTemplateType)
		{
			return Repositories.Configuration.EmailTemplates.Where(x => x.EmailTemplateType == emailTemplateType).Select(x => x.AsDto()).FirstOrDefault();
		}

		/// <summary>
		/// Gets the email template preview.
		/// </summary>
		/// <param name="emailTemplateType">Type of the email template.</param>
		/// <param name="templateValues">The template values.</param>
		/// <returns></returns>
		public EmailTemplateView GetEmailTemplatePreview(EmailTemplateTypes emailTemplateType, EmailTemplateData templateValues)
		{
			var emailTemplate = Repositories.Configuration.EmailTemplates.Where(x => x.EmailTemplateType == emailTemplateType).Select(x => x.AsDto()).FirstOrDefault();

			if(emailTemplate.IsNull())
				throw new Exception(string.Format("Email template type {0} not found", emailTemplateType));

			return GetEmailTemplatePreview(emailTemplate, templateValues);
		}

		/// <summary>
		/// Gets the email template preview.
		/// </summary>
		/// <param name="emailTemplate">The email template.</param>
		/// <param name="templateValues">The template values.</param>
		/// <returns></returns>
		public EmailTemplateView GetEmailTemplatePreview(EmailTemplateDto emailTemplate, EmailTemplateData templateValues)
		{
			switch (emailTemplate.SenderEmailType)
			{
				case SenderEmailTypes.ClientSpecific:
					templateValues.SenderEmailAddress = emailTemplate.ClientSpecificEmailAddress;
					break;
				case SenderEmailTypes.StaffUser:
					templateValues.SenderEmailAddress = RuntimeContext.CurrentRequest.UserContext.EmailAddress;
					break;
        default:
          templateValues.SenderEmailAddress = RuntimeContext.AppSettings.SystemDefaultSenderEmailAddress ?? templateValues.SenderEmailAddress;
          break;
			}

			var preview = new EmailTemplateView
			       	{
			       		Subject = ReplaceEmailTextPlaceholders(emailTemplate.Subject, templateValues),
			       		Body = BuildEmailBody(emailTemplate, templateValues)
			       	};

			return preview;
		}

		/// <summary>
		/// Builds the email body.
		/// </summary>
		/// <param name="template">The template.</param>
		/// <param name="templateValues">The template values.</param>
		/// <returns></returns>
		private string BuildEmailBody(EmailTemplateDto template, EmailTemplateData templateValues)
		{
			var showSalutation = !(templateValues.ShowSalutation.HasValue) || templateValues.ShowSalutation.Value;
			var salutation = (showSalutation) ? template.Salutation : String.Empty;
			var templateText = (salutation.IsNotNullOrEmpty()) ? String.Format("{0}{1}{2}", salutation, Environment.NewLine + Environment.NewLine, template.Body) : template.Body;

			// Now replace place holders with values
			return ReplaceEmailTextPlaceholders(templateText, templateValues);
		}

		/// <summary>
		/// Replaces the email text placeholders.
		/// </summary>
		/// <param name="emailText">The email text.</param>
		/// <param name="templateValues">The template values.</param>
		/// <returns></returns>
		private string ReplaceEmailTextPlaceholders(string emailText, EmailTemplateData templateValues)
		{
			if (emailText.IsNullOrEmpty()) return string.Empty;

			emailText = emailText.Replace(Constants.PlaceHolders.RecipientName, (templateValues.RecipientName.IsNotNullOrEmpty()) ? templateValues.RecipientName : String.Empty);
			emailText = emailText.Replace(Constants.PlaceHolders.JobTitle, (templateValues.JobTitle.IsNotNullOrEmpty()) ? templateValues.JobTitle : String.Empty);
      emailText = emailText.Replace(Constants.PlaceHolders.JobStatus, (templateValues.JobStatus.IsNotNullOrEmpty()) ? templateValues.JobStatus : String.Empty);
      emailText = emailText.Replace(Constants.PlaceHolders.JobPosting, (templateValues.JobPosting.IsNotNullOrEmpty()) ? templateValues.JobPosting : String.Empty);
			emailText = emailText.Replace(Constants.PlaceHolders.MatchedJobs, (templateValues.MatchingJobs.IsNotNullOrEmpty()) ? templateValues.MatchingJobs : String.Empty);
      emailText = emailText.Replace(Constants.PlaceHolders.EmployerName, (templateValues.EmployerName.IsNotNullOrEmpty()) ? templateValues.EmployerName : String.Empty);
      emailText = emailText.Replace(Constants.PlaceHolders.EmployeeName, templateValues.EmployeeName ?? String.Empty);
      emailText = emailText.Replace(Constants.PlaceHolders.SenderName, (templateValues.SenderName.IsNotNullOrEmpty()) ? templateValues.SenderName : String.Empty);

      // Format the phone number - if the number is already formatted the regex isn't applied again (tested)
		  emailText = emailText.Replace(Constants.PlaceHolders.SenderPhoneNumber,
		    (templateValues.SenderPhoneNumber.IsNotNullOrEmpty())
		      ? Regex.Replace(templateValues.SenderPhoneNumber, AppSettings.PhoneNumberStrictRegExPattern,
		        AppSettings.PhoneNumberFormat)
		      : String.Empty);

			emailText = emailText.Replace(Constants.PlaceHolders.SenderEmailAddress, (templateValues.SenderEmailAddress.IsNotNullOrEmpty()) ? templateValues.SenderEmailAddress : String.Empty);
			emailText = emailText.Replace(Constants.PlaceHolders.FreeText, (templateValues.FreeText.IsNotNullOrEmpty()) ? templateValues.FreeText : String.Empty);
			emailText = emailText.Replace(Constants.PlaceHolders.JobLinkUrl, (templateValues.JobLinkUrls.IsNotNullOrEmpty()) ? string.Join(Environment.NewLine, templateValues.JobLinkUrls) : String.Empty);
			emailText = emailText.Replace(Constants.PlaceHolders.ApplicationInstructions, (templateValues.ApplicationInstructions.IsNotNullOrEmpty()) ? templateValues.ApplicationInstructions : String.Empty);
			emailText = emailText.Replace(Constants.PlaceHolders.SavedSearchName, (templateValues.SavedSearchName.IsNotNullOrEmpty()) ? templateValues.SavedSearchName : String.Empty);
			emailText = emailText.Replace(Constants.PlaceHolders.MatchedCandidates, (templateValues.MatchingCandidates.IsNotNullOrEmpty()) ? templateValues.MatchingCandidates : String.Empty);
      emailText = emailText.Replace(Constants.PlaceHolders.JobId, (templateValues.JobId.IsNotNullOrEmpty()) ? templateValues.JobId : String.Empty);
      emailText = emailText.Replace(Constants.PlaceHolders.Reminder, (templateValues.Reminder.IsNotNullOrEmpty()) ? templateValues.Reminder : String.Empty);
      emailText = emailText.Replace(Constants.PlaceHolders.EmailAddress, (templateValues.EmailAddress.IsNotNullOrEmpty()) ? templateValues.EmailAddress : String.Empty);
      emailText = emailText.Replace(Constants.PlaceHolders.Password, (templateValues.Password.IsNotNullOrEmpty()) ? templateValues.Password : String.Empty);

			emailText = emailText.Replace(Constants.PlaceHolders.HiringManagerName, (templateValues.HiringManagerName.IsNotNullOrEmpty()) ? templateValues.HiringManagerName : String.Empty);

			emailText = emailText.Replace(Constants.PlaceHolders.HiringManagerPhoneNumber, (templateValues.HiringManagerPhoneNumber.IsNotNullOrEmpty()) ? templateValues.HiringManagerPhoneNumber : String.Empty);

			emailText = emailText.Replace(Constants.PlaceHolders.HiringManagerEmail, (templateValues.HiringManagerEmail.IsNotNullOrEmpty()) ? templateValues.HiringManagerEmail : String.Empty);

			emailText = emailText.Replace(Constants.PlaceHolders.JobPostingDate, (templateValues.JobPostingDate.IsNotNullOrEmpty()) ? templateValues.JobPostingDate : String.Empty);

			emailText = emailText.Replace(Constants.PlaceHolders.WOTC, (templateValues.WOTC.IsNotNullOrEmpty()) ? templateValues.WOTC : String.Empty);

			emailText = emailText.Replace(Constants.PlaceHolders.ApprovalCount, (templateValues.ApprovalCount.IsNotNullOrEmpty()) ? templateValues.ApprovalCount : String.Empty);

			emailText = emailText.Replace(Constants.PlaceHolders.ApprovalQueueName, (templateValues.ApprovalQueueName.IsNotNullOrEmpty()) ? templateValues.ApprovalQueueName : String.Empty);

      emailText = emailText.Replace(Constants.PlaceHolders.AuthenticateUrl, templateValues.AuthenticateUrl ?? String.Empty);
      emailText = emailText.Replace(Constants.PlaceHolders.PinNumber, templateValues.PinNumber ?? String.Empty);

			emailText = emailText.Replace(Constants.PlaceHolders.Username, templateValues.Username ?? String.Empty);
			emailText = emailText.Replace(Constants.PlaceHolders.Url, templateValues.Url ?? String.Empty);

      emailText = emailText.Replace(Constants.PlaceHolders.DaysLeftToJobExpiry, templateValues.DaysLeftToJobExpiry ?? String.Empty);
      emailText = emailText.Replace(Constants.PlaceHolders.ExpiryDate, templateValues.ExpiryDate ?? String.Empty);

      emailText = emailText.Replace(Constants.PlaceHolders.OfficeName, (templateValues.OfficeName.IsNotNullOrEmpty()) ? templateValues.OfficeName : String.Empty);

			emailText = emailText.Replace(Constants.PlaceHolders.ReportFromDate, (templateValues.ReportFromDate.IsNotNullOrEmpty()) ? templateValues.ReportFromDate : String.Empty);
			emailText = emailText.Replace(Constants.PlaceHolders.ReportToDate, (templateValues.ReportToDate.IsNotNullOrEmpty()) ? templateValues.ReportToDate : String.Empty);

      // Now replace settings based placeholders
			emailText = emailText.Replace(Constants.PlaceHolders.FocusAssistUrl, AppSettings.AssistApplicationPath);
			emailText = emailText.Replace(Constants.PlaceHolders.FocusTalentUrl, AppSettings.TalentApplicationPath);
		  emailText = emailText.Replace(Constants.PlaceHolders.FocusCareerUrl, AppSettings.CareerApplicationPath);
			emailText = emailText.Replace(Constants.PlaceHolders.FocusTalent, AppSettings.FocusTalentApplicationName);
			emailText = emailText.Replace(Constants.PlaceHolders.FocusAssist, AppSettings.FocusAssistApplicationName);
			emailText = emailText.Replace(Constants.PlaceHolders.FocusCareer, AppSettings.FocusCareerApplicationName);
			emailText = emailText.Replace(Constants.PlaceHolders.FocusReporting, AppSettings.FocusReportingApplicationName);
			emailText = emailText.Replace(Constants.PlaceHolders.FocusExplorer, AppSettings.FocusExplorerApplicationName);
			emailText = emailText.Replace(Constants.PlaceHolders.SupportEmail, AppSettings.SupportEmail);
			emailText = emailText.Replace(Constants.PlaceHolders.SupportPhone, AppSettings.SupportPhone);
			emailText = emailText.Replace(Constants.PlaceHolders.SupportHoursOfBusiness, AppSettings.SupportHoursOfBusiness);
			emailText = emailText.Replace(Constants.PlaceHolders.SupportHelpTeam, AppSettings.SupportHelpTeam);

			return emailText;
		}
	}

	#endregion

	#region EmailHelper interface

	public interface IEmailHelper
	{
		/// <summary>
		/// Sends the email.
		/// </summary>
		/// <param name="mailMessage">The mail message.</param>
		/// <param name="sendAsync">if set to <c>true</c> [send async].</param>
		/// <param name="isBodyHtml">if set to <c>true</c> [is body HTML].</param>
		void SendEmail(MailMessage mailMessage, bool sendAsync = false, bool isBodyHtml = true);

		/// <summary>
		/// Sends the email.
		/// </summary>
		/// <param name="to">To.</param>
		/// <param name="cc">The cc.</param>
		/// <param name="bcc">The BCC.</param>
		/// <param name="subject">The subject.</param>
		/// <param name="body">The body.</param>
		/// <param name="sendAsync">if set to <c>true</c> [send async].</param>
		/// <param name="isBodyHtml">if set to <c>true</c> [is body HTML].</param>
		/// <param name="attachment">The attachment.</param>
		/// <param name="attachmentName">Name of the attachment.</param>
		/// <param name="detectUrl">if set to <c>true</c> [detect URL].</param>
		/// <param name="senderAddress">The sender address.</param>
		/// <param name="footerType">Footer type.</param>
		/// <param name="footerUrl">The footer URL.</param>
		void SendEmail(string to, string cc, string bcc, string subject, string body, bool sendAsync = false, bool isBodyHtml = true, byte[] attachment = null, string attachmentName = null, bool detectUrl = false, string senderAddress = null, EmailFooterTypes footerType = EmailFooterTypes.None, string footerUrl = null);

        /// <summary>
        /// Sends the email.
        /// </summary>
        /// <param name="to">To.</param>
        /// <param name="cc">The cc.</param>
        /// <param name="bcc">The BCC.</param>
        /// <param name="subject">The subject.</param>
        /// <param name="body">The body.</param>
        /// <param name="SendEmailOn">The DateTime email to be queued for.</param>
        /// <param name="sendAsync">if set to <c>true</c> [send async].</param>
        /// <param name="isBodyHtml">if set to <c>true</c> [is body HTML].</param>
        /// <param name="attachment">The attachment.</param>
        /// <param name="attachmentName">Name of the attachment.</param>
        /// <param name="detectUrl">if set to <c>true</c> [detect URL].</param>
        /// <param name="senderAddress">The sender address.</param>
        /// <param name="footerType">Footer type.</param>
        /// <param name="footerUrl">The footer URL.</param>
        void SendQueuedEmail(string to, string cc, string bcc, string subject, string body, DateTime SendEmailOn, bool sendAsync = false, bool isBodyHtml = true, byte[] attachment = null, string attachmentName = null, bool detectUrl = false, string senderAddress = null, EmailFooterTypes footerType = EmailFooterTypes.None, string footerUrl = null);

		/// <summary>
		/// Sends the email from template.
		/// </summary>
		/// <param name="type">The type.</param>
		/// <param name="data">The data.</param>
		/// <param name="to">To.</param>
		/// <param name="cc">The cc.</param>
		/// <param name="bcc">The BCC.</param>
		/// <param name="sendAsync">if set to <c>true</c> [send asynchronous].</param>
		/// <param name="isBodyHtml">if set to <c>true</c> [is body HTML].</param>
		/// <param name="attachment">The attachment.</param>
		/// <param name="attachmentName">Name of the attachment.</param>
		/// <param name="detectUrl">if set to <c>true</c> [detect URL].</param>
		void SendEmailFromTemplate(EmailTemplateTypes type, EmailTemplateData data, string to, string cc, string bcc, bool sendAsync = false, bool isBodyHtml = true, byte[] attachment = null, string attachmentName = null, bool detectUrl = false);

        /// <summary>
        /// Sends the email from template.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <param name="data">The data.</param>
        /// <param name="to">To.</param>
        /// <param name="cc">The cc.</param>
        /// <param name="bcc">The BCC.</param>
        /// <param name="SendEmailOn">The DateTime email to be sent on.</param>
        /// <param name="sendAsync">if set to <c>true</c> [send asynchronous].</param>
        /// <param name="isBodyHtml">if set to <c>true</c> [is body HTML].</param>
        /// <param name="attachment">The attachment.</param>
        /// <param name="attachmentName">Name of the attachment.</param>
        /// <param name="detectUrl">if set to <c>true</c> [detect URL].</param>
        void SendQueuedEmailFromTemplate(EmailTemplateTypes type, EmailTemplateData data, string to, string cc, string bcc, DateTime SendEmailOn, bool sendAsync = false, bool isBodyHtml = true, byte[] attachment = null, string attachmentName = null, bool detectUrl = false);

		/// <summary>
		/// Gets the email template.
		/// </summary>
		/// <param name="emailTemplateType">Type of the email template.</param>
		/// <returns></returns>
		EmailTemplateDto GetEmailTemplate(EmailTemplateTypes emailTemplateType);

		/// <summary>
		/// Gets the email template preview.
		/// </summary>
		/// <param name="emailTemplateType">Type of the email template.</param>
		/// <param name="templateValues">The template values.</param>
		/// <returns></returns>
		EmailTemplateView GetEmailTemplatePreview(EmailTemplateTypes emailTemplateType, EmailTemplateData templateValues);

		/// <summary>
		/// Gets the email template preview.
		/// </summary>
		/// <param name="emailTemplate">The email template.</param>
		/// <param name="templateValues">The template values.</param>
		/// <returns></returns>
		EmailTemplateView GetEmailTemplatePreview(EmailTemplateDto emailTemplate, EmailTemplateData templateValues);
	}

	#endregion

}
