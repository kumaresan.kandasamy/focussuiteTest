﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using Focus.Core;
using Focus.Core.IntegrationMessages;
using Focus.Core.Messages.EmployerService;
using Focus.Core.Models.Assist;
using Focus.Core.Models.Integration;
using Focus.Data.Core.Entities;
using Focus.Services.Core;
using Focus.Services.ServiceImplementations;
using Focus.Services.Utilities;
using Framework.Core;
using Framework.Logging;

#endregion



namespace Focus.Services.Helpers
{
	#region EmployerHelper Class

	public class EmployerHelper : HelperBase, IEmployerHelper
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="EmployerHelper"/> class.
		/// </summary>
		public EmployerHelper() : base(null) {}

		/// <summary>
		/// Initializes a new instance of the <see cref="EmployerHelper"/> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		public EmployerHelper(IRuntimeContext runtimeContext) : base(runtimeContext) { }

		/// <summary>
		/// Validates the fein change.
		/// </summary>
		/// <param name="businessUnitId">The business unit identifier.</param>
		/// <param name="newFEIN">The new fein.</param>
		/// <returns></returns>
		public ValidateFEINChangeModel ValidateFEINChange(long? businessUnitId, string newFEIN)
		{
			var result = new ValidateFEINChangeModel { IsValid = true };

			#region Validate inputs

			if (newFEIN.IsNullOrEmpty())
				throw new ArgumentNullException("newFEIN", "New FEIN is required.");

			if (!businessUnitId.HasValue)
				throw new ArgumentNullException("businessUnitId", "Business Unit Id is required.");

			var businessUnit = Repositories.Core.BusinessUnits.Where(x => x.Id == businessUnitId).Select(x => new { x.EmployerId, x.IsPrimary, x.Name }).FirstOrDefault();

			if (businessUnit.IsNull())
				throw new ArgumentException(string.Format("Business Unit with Id '{0}' not found.", businessUnitId));

			#endregion

			var currentFEIN = Repositories.Core.Employers.Where(x => x.Id == businessUnit.EmployerId).Select(x => x.FederalEmployerIdentificationNumber).FirstOrDefault();

			if (currentFEIN.IsNull())
				throw new Exception("Could not find the current FEIN");

			if (currentFEIN == newFEIN)
			{
				result.IsValid = false;
				result.ValidationMessage = FEINChangeValidationMessages.FEINNotChanged;
				return result;
			}

			var employer = Repositories.Core.Employers.Where(x => x.FederalEmployerIdentificationNumber == newFEIN && (!x.Archived)).Select(x => new { x.Id, x.Name }).FirstOrDefault();
			var feinExists = false;

			if (employer.IsNotNull())
			{
				result.NewEmployerName = employer.Name;
				result.NewEmployerId = employer.Id;
				feinExists = true;
			}

			result.BusinessUnitName = businessUnit.Name;

			if (businessUnit.IsPrimary)
			{
				var hasSibling = Repositories.Core.BusinessUnits.Any(x => x.EmployerId == businessUnit.EmployerId && x.IsPrimary == false);

				if (feinExists)
					result.ActionToBeCarriedOut = (hasSibling) ? FEINChangeActions.PrimaryBusinessUnitExistingFEINSiblings : FEINChangeActions.PrimaryBusinessUnitExistingFEINNoSiblings;
				else
					result.ActionToBeCarriedOut = (hasSibling) ? FEINChangeActions.PrimaryBusinessUnitNewFEINSiblings : FEINChangeActions.PrimaryBusinessUnitNewFEINNoSiblings;
			}
			else
			{
				// Check for employees who exist in a business unit not being moved to new strucuture
				var businessUnitEmployees = Repositories.Core.EmployeeBusinessUnits.Where(x => x.BusinessUnitId == businessUnitId).Select(x => x.EmployeeId).ToList();

				if (businessUnitEmployees.IsNotNull())
				{
					if (Repositories.Core.EmployeeBusinessUnits.Any(x => businessUnitEmployees.Contains(x.EmployeeId) && x.BusinessUnitId != businessUnitId))
					{
						result.IsValid = false;
						result.ValidationMessage = FEINChangeValidationMessages.EmployeeAssignedToSiblingBusinessUnit;
						return result;
					}
				}

				result.ActionToBeCarriedOut = (feinExists) ? FEINChangeActions.NonPrimaryBusinessUnitExistingFEIN : FEINChangeActions.NonPrimaryBusinessUnitNewFEIN;
			}

			return result;
		}

		/// <summary>
		/// Updates the primary business unit.
		/// </summary>
		/// <param name="currentPrimaryBusinessUnitId">The current primary business unit identifier.</param>
		/// <param name="newPrimaryBusinessUnitId">The new primary business unit identifier.</param>
		/// <exception cref="System.ArgumentNullException">
		/// currentPrimaryBusinessUnitId;@Current primary business unit ID required
		/// or
		/// newPrimaryBusinessUnitId;@New primary business unit ID is required.
		/// </exception>
		/// <exception cref="System.Exception">
		/// Could not find employer to update name.
		/// </exception>
		/// <exception cref="System.ArgumentException"></exception>
		public void UpdatePrimaryBusinessUnit(long? currentPrimaryBusinessUnitId, long? newPrimaryBusinessUnitId)
		{
			#region Validate Inputs

			if (!currentPrimaryBusinessUnitId.HasValue)
				throw new ArgumentNullException("currentPrimaryBusinessUnitId", @"Current primary business unit ID required");

			if (!newPrimaryBusinessUnitId.HasValue)
				throw new ArgumentNullException("newPrimaryBusinessUnitId", @"New primary business unit ID is required.");

			// Update the current Primary Business unit.
			var currentPrimaryBU = GetBusinessUnit(currentPrimaryBusinessUnitId);

			if (!currentPrimaryBU.IsPrimary)
				throw new ArgumentException(String.Format("Current business unit with ID: {0} is not a primary business unit.", currentPrimaryBusinessUnitId));

			currentPrimaryBU.IsPrimary = false;

			// Update new Primary business unit.
			var newPrimaryBU = GetBusinessUnit(newPrimaryBusinessUnitId);

			if (newPrimaryBU.IsPrimary)
				throw new Exception(string.Format("Business Unit with Id '{0}' is already the primary business unit.", newPrimaryBusinessUnitId));
	
			newPrimaryBU.IsPrimary = true;

			if (currentPrimaryBU.EmployerId != newPrimaryBU.EmployerId)
				throw new Exception("Business Unit employer IDs do not match.");

			#endregion
			
			// Update Employer.
			var employer = GetEmployer(newPrimaryBU.EmployerId);
			
			employer.Name = newPrimaryBU.Name;
            newPrimaryBU.ApprovalStatus = employer.ApprovalStatus;
			//employer.LegalName = newPrimaryBU.LegalName;

            var employeeid = Repositories.Core.EmployeeBusinessUnits.Where(x => x.BusinessUnitId == newPrimaryBusinessUnitId).Select(x => x.EmployeeId).First();
            var employee = Repositories.Core.Employees.First(x => x.Id == employeeid);
            employee.ApprovalStatus = employer.ApprovalStatus;

			// Save the changes.
			Repositories.Core.SaveChanges();
		}

		/// <summary>
		/// Saves the fein change.
		/// </summary>
		/// <param name="businessUnitId">The business unit identifier.</param>
		/// <param name="newFEIN">The new fein.</param>
		/// <returns></returns>
		public FEINChangeActions SaveFEINChange(long? businessUnitId, string newFEIN)
		{
			var validationResult = ValidateFEINChange(businessUnitId, newFEIN);

			if (validationResult.ActionToBeCarriedOut == FEINChangeActions.None)
				return FEINChangeActions.None;

			var currentEmployerId = GetEmployerId(businessUnitId);

			switch (validationResult.ActionToBeCarriedOut)
			{
				case FEINChangeActions.PrimaryBusinessUnitNewFEINNoSiblings:
				case FEINChangeActions.PrimaryBusinessUnitNewFEINSiblings:
					
					UpdateEmployerFEIN(currentEmployerId, newFEIN);
					break;
				
				case FEINChangeActions.PrimaryBusinessUnitExistingFEINNoSiblings:
				case FEINChangeActions.PrimaryBusinessUnitExistingFEINSiblings:	
					switch (validationResult.ActionToBeCarriedOut)
					{
						case FEINChangeActions.PrimaryBusinessUnitExistingFEINNoSiblings:	
							UpdateBusinessUnitEmployer(businessUnitId, validationResult.NewEmployerId);
							break;

						case FEINChangeActions.PrimaryBusinessUnitExistingFEINSiblings:
							UpdateEmployerBusinessUnitEmployer(currentEmployerId, validationResult.NewEmployerId);
							break;
					}

					ReallocateEmployees(currentEmployerId, validationResult.NewEmployerId);
					ReallocateJobs(currentEmployerId, validationResult.NewEmployerId);
					ReallocatePersonNotes(currentEmployerId, validationResult.NewEmployerId);
					ArchiveEmployer(currentEmployerId);
					break;

				case FEINChangeActions.NonPrimaryBusinessUnitExistingFEIN:
					UpdateBusinessUnitEmployer(businessUnitId, validationResult.NewEmployerId);
					ReallocateEmployees(currentEmployerId, validationResult.NewEmployerId, businessUnitId);
					ReallocateJobs(currentEmployerId, validationResult.NewEmployerId, businessUnitId);
					break;

				case FEINChangeActions.NonPrimaryBusinessUnitNewFEIN:
					var newEmployerId = CreateEmployerFromBusinessUnit(businessUnitId, newFEIN);
					UpdateBusinessUnitEmployer(businessUnitId, newEmployerId, true);
					ReallocateEmployees(currentEmployerId, newEmployerId, businessUnitId);
					ReallocateJobs(currentEmployerId, newEmployerId, businessUnitId);
					break;
			}

			Repositories.Core.SaveChanges();

			RuntimeContext.Helpers.Logging.LogAction(ActionTypes.SaveFederalEmployerIdentificationNumberChange, typeof(BusinessUnit).Name, businessUnitId, true);

			return validationResult.ActionToBeCarriedOut;
		}

		/// <summary>
		/// Assigns the default offices.
		/// </summary>
		/// <param name="employerId">The employer identifier.</param>
		/// <param name="postCodeZip">The post code zip.</param>
		public void AssignDefaultOffices(long? employerId, string postCodeZip)
		{
			if (!employerId.HasValue)
				throw new ArgumentNullException("employerId");

			var offices = new List<long>();
			var officeUnassigned = (bool?)null;

			// Get the office that deals with the postcode
			if (postCodeZip.IsNotNullOrEmpty())
                offices = Helpers.Office.GetOfficeIdsForPostcodeZip(MiscUtils.SafeSubstring(postCodeZip, 0, 5));

			if (offices.IsNullOrEmpty())
			{
				// Get default Office Id
				var defaultOfficeId = Helpers.Office.GetDefaultOfficeId(OfficeDefaultType.Employer);

				if (defaultOfficeId.HasValue && defaultOfficeId > 0)
				{
					offices.Add(defaultOfficeId.Value);
					officeUnassigned = true;
				}
			}

			if (offices.IsNotNullOrEmpty())
			{
				var employerOffices = from o in offices
					select new EmployerOfficeMapper {OfficeId = o, EmployerId = employerId.Value, OfficeUnassigned = officeUnassigned};

				var employerOfficesSaved = new List<EmployerOfficeMapper>();

				foreach (var employerOffice in employerOffices)
				{
					Repositories.Core.Add(employerOffice);
					employerOfficesSaved.Add(employerOffice);
				}

				// Log the action
				employerOfficesSaved.ForEach(ed => Helpers.Logging.LogAction(ActionTypes.AddEmployerOffice, typeof (EmployerOfficeMapper).Name, ed.Id, RuntimeContext.CurrentRequest.UserContext.UserId, commit: false));
			}
		}

		#region Private helper methods

		/// <summary>
		/// Updates the employer fein.
		/// </summary>
		/// <param name="employerId">The employer identifier.</param>
		/// <param name="newFEIN">The new fein.</param>
		private void UpdateEmployerFEIN(long? employerId, string newFEIN)
		{
			if (!employerId.HasValue)
				throw new ArgumentNullException("employerId");

			if(newFEIN.IsNullOrEmpty())
				throw new ArgumentNullException("newFEIN");

			if(IsExistingFEIN(newFEIN))
				throw new ArgumentException(string.Format("The new FEIN already exists on an active employer"));

			var employer = GetEmployer(employerId);

			employer.FederalEmployerIdentificationNumber = newFEIN;

			// Save employer in any integrated client
			if (employer.ApprovalStatus == ApprovalStatuses.Approved)
				employer.PublishEmployerUpdateIntegrationRequest(RuntimeContext.CurrentRequest.UserContext.ActionerId, RuntimeContext);

			// Log the action
			RuntimeContext.Helpers.Logging.LogAction(ActionTypes.UpdateFederalEmployerIdentificationNumber, employer, false);
		}

		/// <summary>
		/// Updates the business unit employer.
		/// </summary>
		/// <param name="businessUnitId">The business unit identifier.</param>
		/// <param name="employerId">The employer identifier.</param>
		/// <param name="isPrimary">if set to <c>true</c> [is primary].</param>
		/// <exception cref="System.ArgumentNullException">
		/// businessUnitId
		/// or
		/// employerId
		/// </exception>
		private void UpdateBusinessUnitEmployer(long? businessUnitId, long? employerId, bool isPrimary = false)
		{
			if (!businessUnitId.HasValue)
				throw new ArgumentNullException("businessUnitId");

			if (!employerId.HasValue)
				throw new ArgumentNullException("employerId");

			if (isPrimary)
			{
				// Validate there is not already a primary business unit
				if(Repositories.Core.BusinessUnits.Any(x => x.EmployerId == employerId && x.IsPrimary))
					throw new ArgumentException(string.Format("Cannot make Business Unit {0} primary for Employer {1} as Employer {1} already has a primary business unit", businessUnitId, employerId));
			}

			var businessUnit = GetBusinessUnit(businessUnitId);

			businessUnit.EmployerId = employerId.Value;
			businessUnit.IsPrimary = isPrimary;
			
			// Log the action
			RuntimeContext.Helpers.Logging.LogAction(ActionTypes.UpdateBusinessUnitEmployer, businessUnit, false);
		}

		/// <summary>
		/// Updates the employer business unit employer.
		/// </summary>
		/// <param name="employerId">The employer identifier.</param>
		/// <param name="newEmployerId">The new employer identifier.</param>
		private void UpdateEmployerBusinessUnitEmployer(long? employerId, long? newEmployerId)
		{
			if (!employerId.HasValue)
				throw new ArgumentNullException("employerId");

			if (!newEmployerId.HasValue)
				throw new ArgumentNullException("newEmployerId");

			var businessUnitIds = Repositories.Core.BusinessUnits.Where(x => x.EmployerId == employerId).Select(x => x.Id).ToList();

			if (businessUnitIds.IsNullOrEmpty()) 
				return;

			foreach (var businessUnitId in businessUnitIds)
			{
				UpdateBusinessUnitEmployer(businessUnitId, newEmployerId, false);
			}
		}

		/// <summary>
		/// Archives the employer.
		/// </summary>
		/// <param name="employerId">The employer identifier.</param>
		private void ArchiveEmployer(long? employerId)
		{
			if (!employerId.HasValue)
				throw new ArgumentNullException("employerId");

			var employer = GetEmployer(employerId);

			employer.Archived = true;

			// Remove office mappings when archiving this stops the archived employer causing validation errors when deactivating an office it is associated to
			Repositories.Core.Remove<EmployerOfficeMapper>(x => x.EmployerId == employerId);
			
			// Save employer in any integrated client
			if (employer.ApprovalStatus == ApprovalStatuses.Approved)
				employer.PublishEmployerUpdateIntegrationRequest(RuntimeContext.CurrentRequest.UserContext.ActionerId, RuntimeContext);

			// Log the action
			RuntimeContext.Helpers.Logging.LogAction(ActionTypes.ArchiveEmployer, employer, false);
		}

		/// <summary>
		/// Gets the employer.
		/// </summary>
		/// <param name="employerId">The employer identifier.</param>
		/// <returns></returns>
		/// <exception cref="System.ArgumentNullException">employerId</exception>
		/// <exception cref="System.ArgumentException"></exception>
		private Employer GetEmployer(long? employerId)
		{
			if(!employerId.HasValue)
				throw new ArgumentNullException("employerId");

			var employer = Repositories.Core.FindById<Employer>(employerId);

			if(employer.IsNull())
				throw new ArgumentException(string.Format("Employer with Id {0} could not be found.", employerId));

			return employer;
		}

		/// <summary>
		/// Gets the business unit.
		/// </summary>
		/// <param name="businessUnitId">The business unit identifier.</param>
		/// <returns></returns>
		/// <exception cref="System.ArgumentNullException">businessUnitId</exception>
		/// <exception cref="System.ArgumentException"></exception>
		private BusinessUnit GetBusinessUnit(long? businessUnitId)
		{
			if (!businessUnitId.HasValue)
				throw new ArgumentNullException("businessUnitId");

			var businessUnit = Repositories.Core.FindById<BusinessUnit>(businessUnitId);

			if (businessUnit.IsNull())
				throw new ArgumentException(string.Format("Business Unit with Id {0} could not be found.", businessUnitId));

			return businessUnit;
		}

		/// <summary>
		/// Gets the employer identifier for a businss unit.
		/// </summary>
		/// <param name="businessUnitId">The business unit identifier.</param>
		/// <returns></returns>
		/// <exception cref="System.ArgumentNullException">businessUnitId</exception>
		/// <exception cref="System.ArgumentException"></exception>
		private long? GetEmployerId(long? businessUnitId)
		{
			if (!businessUnitId.HasValue)
				throw new ArgumentNullException("businessUnitId");

			var employerId = Repositories.Core.BusinessUnits.Where(x => x.Id == businessUnitId).Select(x => x.EmployerId).FirstOrDefault();

			if(employerId == 0)
				throw new ArgumentException(string.Format("Business Unit with Id {0} could not be found.", businessUnitId));

			return employerId;
		}

		/// <summary>
		/// Reallocates the employees.
		/// </summary>
		/// <param name="oldEmployerId">The old employer identifier.</param>
		/// <param name="newEmployerId">The new employer identifier.</param>
		/// <param name="businessUnitId">The business unit identifier.</param>
		/// <exception cref="System.ArgumentNullException">
		/// oldEmployerId
		/// or
		/// newEmployerId
		/// </exception>
		private void ReallocateEmployees(long? oldEmployerId, long? newEmployerId, long? businessUnitId = null)
		{
			if (!oldEmployerId.HasValue)
				throw new ArgumentNullException("oldEmployerId");

			if (!newEmployerId.HasValue)
				throw new ArgumentNullException("newEmployerId");

			var employees = new List<Employee>();

			if (businessUnitId.HasValue)
			{
				var businessUnitEmployeeIds = Repositories.Core.EmployeeBusinessUnits.Where(x => x.BusinessUnitId == businessUnitId).Select(x => x.EmployeeId).ToList();

				// Validate employees do not belong to any other business unit
				if (Repositories.Core.EmployeeBusinessUnits.Any(x => businessUnitEmployeeIds.Contains(x.EmployeeId) && x.BusinessUnitId != businessUnitId))
					throw new Exception("Employee cannot be allocated to new employer as they are still associated with a business unit in the old employer");

				employees = Repositories.Core.Employees.Where(x => businessUnitEmployeeIds.Contains(x.Id)).ToList();
			}
			else
				employees = Repositories.Core.Employees.Where(x => x.EmployerId == oldEmployerId).ToList();

			foreach (var employee in employees)
			{
				employee.EmployerId = newEmployerId.Value;
				RuntimeContext.Helpers.Logging.LogAction(ActionTypes.UpdateEmployeeEmployer, employee, false);
			}
		}

		/// <summary>
		/// Reallocates the jobs.
		/// </summary>
		/// <param name="oldEmployerId">The old employer identifier.</param>
		/// <param name="newEmployerId">The new employer identifier.</param>
		/// <param name="businessUnitId">The business unit identifier.</param>
		/// <exception cref="System.ArgumentNullException">
		/// oldEmployerId
		/// or
		/// newEmployerId
		/// </exception>
		private void ReallocateJobs(long? oldEmployerId, long? newEmployerId, long? businessUnitId = null)
		{
			if (!oldEmployerId.HasValue)
				throw new ArgumentNullException("oldEmployerId");

			if (!newEmployerId.HasValue)
				throw new ArgumentNullException("newEmployerId");

			var jobs = (businessUnitId.HasValue) ? Repositories.Core.Jobs.Where(x => x.BusinessUnitId == businessUnitId).ToList() : Repositories.Core.Jobs.Where(x => x.EmployerId == oldEmployerId).ToList();

			foreach (var job in jobs)
			{
				job.EmployerId = newEmployerId.Value;
				RuntimeContext.Helpers.Logging.LogAction(ActionTypes.UpdateJobEmployer, job, false);
			}
		}

		/// <summary>
		/// Reallocates the person notes.
		/// </summary>
		/// <param name="oldEmployerId">The old employer identifier.</param>
		/// <param name="newEmployerId">The new employer identifier.</param>
		private void ReallocatePersonNotes(long? oldEmployerId, long? newEmployerId)
		{
			if (!oldEmployerId.HasValue)
				throw new ArgumentNullException("oldEmployerId");

			if (!newEmployerId.HasValue)
				throw new ArgumentNullException("newEmployerId");

			var notes = Repositories.Core.PersonNotes.Where(x => x.EmployerId == oldEmployerId).ToList();

			foreach (var note in notes)
			{
				note.EmployerId = newEmployerId.Value;
				RuntimeContext.Helpers.Logging.LogAction(ActionTypes.UpdatePersonNoteEmployer, note, false);
			}
		}

		/// <summary>
		/// Creates the employer from business unit.
		/// </summary>
		/// <param name="businessUnitId">The business unit identifier.</param>
		/// <param name="newFEIN">The new fein.</param>
		/// <returns></returns>
		private long CreateEmployerFromBusinessUnit(long? businessUnitId, string newFEIN)
		{
			if (!businessUnitId.HasValue)
				throw new ArgumentNullException("businessUnitId");

			if (newFEIN.IsNullOrEmpty())
				throw new ArgumentNullException("newFEIN");

			if (IsExistingFEIN(newFEIN))
				throw new ArgumentException(string.Format("The new FEIN already exists on an active employer"));

			var businessUnit = GetBusinessUnit(businessUnitId);

			var clientEmployer = ValidateFEINAgainstClientApplication(newFEIN);

			var employer = (clientEmployer.IsNotNull()) ? (CreateEmployerFromIntegrationEmployerModel(clientEmployer)) : new Employer();

			employer.FederalEmployerIdentificationNumber = newFEIN;
			employer.TermsAccepted = true;
			employer.IsRegistrationComplete = true;
			employer.ApprovalStatus = (employer.IsValidFederalEmployerIdentificationNumber) ? ApprovalStatuses.Approved : ApprovalStatuses.WaitingApproval;

			employer = UpdateEmployerFromBusinessUnit(employer, businessUnit);

			Repositories.Core.Add(employer);

			if (employer.EmployerAddresses.Any())
				AssignDefaultOffices(employer.Id, employer.EmployerAddresses[0].PostcodeZip);

			RuntimeContext.Helpers.Logging.LogAction(ActionTypes.CreateEmployer, employer, false);

			return employer.Id;
		}

		/// <summary>
		/// Determines whether [is existing fein] [the specified fein].
		/// </summary>
		/// <param name="FEIN">The fein.</param>
		/// <returns></returns>
		private bool IsExistingFEIN(string FEIN)
		{
			if (FEIN.IsNullOrEmpty())
				throw new ArgumentNullException("FEIN");

			return Repositories.Core.Employers.Any(x => x.FederalEmployerIdentificationNumber == FEIN && (!x.Archived));
		}

		/// <summary>
		/// Validates the FEIN against client application.
		/// </summary>
		/// <param name="FEIN">The fein.</param>
		/// <returns></returns>
		private EmployerModel ValidateFEINAgainstClientApplication(string FEIN)
		{
			if (FEIN.IsNullOrEmpty())
				throw new ArgumentNullException("FEIN");

			var integrationRequest = new GetEmployerRequest { FEIN = FEIN, UserId = RuntimeContext.CurrentRequest.UserContext.UserId };
			var integrationResponse = Repositories.Integration.GetEmployer(integrationRequest);

			if(integrationResponse.IsNull())
				throw new Exception("Problem encountered getting employer from client system");

			if(integrationResponse.Employer.IsNull())
				Logger.Warning(RuntimeContext.CurrentRequest.LogData(), string.Format("Unable to validate employer with an FEIN of '{0}'. ", FEIN));

			return (integrationResponse.IsNotNull()) ? integrationResponse.Employer : null;
		}

		/// <summary>
		/// Creates the employer from integration employer model.
		/// </summary>
		/// <param name="integrationModel">The model.</param>
		/// <returns></returns>
		private Employer CreateEmployerFromIntegrationEmployerModel(EmployerModel integrationModel)
		{
			if(integrationModel.IsNull())
				throw new ArgumentNullException("integrationModel");

			var naics = integrationModel.Naics;
			if (naics.IsNotNullOrEmpty())
			{
				var lookupNaics = Repositories.Library.NAICS.FirstOrDefault(n => n.Code == naics);
				if (lookupNaics.IsNotNull())
					naics = string.Concat(naics, " - ", lookupNaics.Name);
			}

			var employer = new Employer
			{
				Name = integrationModel.Name,
				StateEmployerIdentificationNumber = integrationModel.StateEmployerIdentificationNumber,
				Url = integrationModel.Url,
				PrimaryPhone = integrationModel.Phone,
				PrimaryPhoneExtension = integrationModel.PhoneExt,
				PrimaryPhoneType = (integrationModel.Phone.IsNotNullOrEmpty()) ? "Phone" : null,
				AlternatePhone1 = integrationModel.AltPhone,
				AlternatePhone1Type = (integrationModel.AltPhone.IsNotNullOrEmpty()) ? "Phone" : null,
				ExternalId = integrationModel.ExternalId,
				OwnershipTypeId = integrationModel.Owner,
				IndustrialClassification = naics,
				IsValidFederalEmployerIdentificationNumber = true
			};

			var employerAddress = (integrationModel.Address.IsNotNull()) ? CreateEmployerAddressFromIntegrationAddressModel(integrationModel.Address) : null;

			if(employerAddress.IsNotNull())
				employer.EmployerAddresses.Add(employerAddress);
			
			return employer;
		}

		/// <summary>
		/// Creates the employer address from integration address model.
		/// </summary>
		/// <param name="integrationModel">The integration model.</param>
		/// <returns></returns>
		private EmployerAddress CreateEmployerAddressFromIntegrationAddressModel(AddressModel integrationModel)
		{
			if (integrationModel.IsNull())
				throw new ArgumentNullException("integrationModel");

			return new EmployerAddress
			{
				Line1 = integrationModel.AddressLine1,
				Line2 = integrationModel.AddressLine2,
				TownCity = integrationModel.City,
				PostcodeZip = integrationModel.PostCode,
				
				CountyId = (integrationModel.CountyId != 0)
					? integrationModel.CountyId
					: Helpers.Lookup.GetLookup(LookupTypes.Counties).Where(s => s.ExternalId == integrationModel.ExternalCountyId).Select(s => s.Id).FirstOrDefault(),
				
				StateId = (integrationModel.StateId != 0)
					? integrationModel.StateId
					: Helpers.Lookup.GetLookup(LookupTypes.States).Where(s => s.ExternalId == integrationModel.ExternalStateId).Select(s => s.Id).FirstOrDefault(),
				
				CountryId = (integrationModel.CountryId != 0)
					? integrationModel.CountryId
					: Helpers.Lookup.GetLookup(LookupTypes.Countries).Where(s => s.ExternalId == integrationModel.ExternalCountryId).Select(s => s.Id).FirstOrDefault(),
				
				IsPrimary = true
			};
		}

		/// <summary>
		/// Updates the employer from business unit.
		/// </summary>
		/// <param name="employer">The employer.</param>
		/// <param name="businessUnit">The business unit.</param>
		private Employer UpdateEmployerFromBusinessUnit(Employer employer, BusinessUnit businessUnit)
		{
			if (employer.IsNull())
				throw new ArgumentNullException("employer");

			if (businessUnit.IsNull())
				throw new ArgumentNullException("businessUnit");

			if (employer.Name.IsNullOrEmpty())
				employer.Name = businessUnit.Name;

			if (employer.Url.IsNullOrEmpty())
				employer.Url = businessUnit.Url;

			if (employer.OwnershipTypeId.IsNull() || employer.OwnershipTypeId == 0)
				employer.OwnershipTypeId = businessUnit.OwnershipTypeId;

			if (employer.IndustrialClassification.IsNullOrEmpty())
				employer.IndustrialClassification = businessUnit.IndustrialClassification;

			if (employer.PrimaryPhone.IsNullOrEmpty())
				employer.PrimaryPhone = businessUnit.PrimaryPhone;

			if (employer.PrimaryPhoneExtension.IsNullOrEmpty())
				employer.PrimaryPhoneExtension = businessUnit.PrimaryPhoneExtension;

			if (employer.PrimaryPhoneType.IsNullOrEmpty())
				employer.PrimaryPhoneType = businessUnit.PrimaryPhoneType;

			if (employer.AlternatePhone1.IsNullOrEmpty())
				employer.AlternatePhone1 = businessUnit.AlternatePhone1;

			if (employer.AlternatePhone1Type.IsNullOrEmpty())
				employer.AlternatePhone1Type = businessUnit.AlternatePhone1Type;

			if (employer.AlternatePhone2.IsNullOrEmpty())
				employer.AlternatePhone2 = businessUnit.AlternatePhone2;

			if (employer.AlternatePhone2Type.IsNullOrEmpty())
				employer.AlternatePhone2Type = businessUnit.AlternatePhone2Type;

			if (!employer.EmployerAddresses.Any() && businessUnit.BusinessUnitAddresses.Any())
			{
				var primaryAddress = businessUnit.BusinessUnitAddresses.FirstOrDefault(x => x.IsPrimary);

				if (primaryAddress.IsNull())
					primaryAddress = businessUnit.BusinessUnitAddresses[0];

				employer.EmployerAddresses.Add(CreateEmployerAddressFromBusinessUnitAddress(primaryAddress));
			}

			return employer;
		}

		/// <summary>
		/// Creates the employer address from business unit address.
		/// </summary>
		/// <param name="address">The address.</param>
		private EmployerAddress CreateEmployerAddressFromBusinessUnitAddress(BusinessUnitAddress address)
		{
			return new EmployerAddress
			{
				Line1 = address.Line1,
				Line2 = address.Line2,
				Line3 = address.Line3,
				TownCity = address.TownCity,
				CountyId = address.CountyId,
				PostcodeZip = address.PostcodeZip,
				StateId = address.StateId,
				CountryId = address.CountryId,
				IsPrimary = true,
				PublicTransitAccessible = address.PublicTransitAccessible
			};
		}

		#endregion
	}

	#endregion

	#region EmployerHelper Interface

	public interface IEmployerHelper
	{
		ValidateFEINChangeModel ValidateFEINChange(long? businessUnitId, string newFEIN);

		FEINChangeActions SaveFEINChange(long? businessUnitId, string newFEIN);

		void AssignDefaultOffices(long? employerId, string postCodeZip);

		void UpdatePrimaryBusinessUnit(long? currentPrimaryBusinessUnitId, long? newPrimaryBusinessUnitId);
	}

	#endregion

}
