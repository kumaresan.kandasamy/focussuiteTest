﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

using Focus.Data.Core.Entities;

using Framework.Core;
using Framework.Exceptions;
using Framework.Logging;
using Mindscape.LightSpeed;
using Mindscape.LightSpeed.Querying;

#endregion

namespace Focus.Services.Helpers
{
	#region SessionHelper Class

	public class SessionHelper : HelperBase, ISessionHelper
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="SessionHelper" /> class.
		/// </summary>
		public SessionHelper() : this(null)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="SessionHelper" /> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		public SessionHelper(IRuntimeContext runtimeContext) : base(runtimeContext)
		{ }
		
		/// <summary>
		/// Creates the session.
		/// </summary>
		/// <param name="userId">The user id.</param>
		/// <returns></returns>
		public Guid CreateSession(long userId = 0)
		{
			var session = new Session { Id = Guid.NewGuid(), UserId = userId, LastRequestOn = DateTime.UtcNow };
			Repositories.Core.Add(session);
			Repositories.Core.SaveChanges(true);

			return session.Id;
		}

		/// <summary>
		/// Updates the session.
		/// </summary>
		/// <param name="sessionId">The session id.</param>
		/// <param name="userId">The user id.</param>
    /// <param name="checkUser">Whether to check the user Id of the session matches before updating.</param>
    public void UpdateSession(Guid sessionId, long userId, bool checkUser = false)
		{
		    Repositories.Core.UpdateSession(sessionId, userId, DateTime.UtcNow, checkUser);
		}

		/// <summary>
		/// Validates the session.
		/// </summary>
		/// <param name="sessionId">The session id.</param>
		/// <param name="userId">The user id.</param>
		/// <returns></returns>
		public bool ValidateSession(Guid sessionId, long userId)
		{
			const string validatedSessionContextKey = "ValidatedSessionGuid";

			if (HttpContext.Current != null)
			{
				var hasPreviouslyBeenValidated = HttpContext.Current.Items.Contains(validatedSessionContextKey);
				if (hasPreviouslyBeenValidated) return true;
			}

			var session = Repositories.Core.FindById<Session>(sessionId);
			if (session.IsNull()) throw new SessionException("Session no longer exists");

			if (session.LastRequestOn.AddMinutes(AppSettings.SessionTimeout + 5) < DateTime.UtcNow)
				throw new SessionException("Session has expired");

			if (session.UserId != 0 && session.UserId != userId)
				throw new SessionException("Session does not belong to user");
			
		  	UpdateSession(sessionId, userId);

			if (HttpContext.Current != null)
				HttpContext.Current.Items.Add(validatedSessionContextKey, sessionId);

			return true;
		}

    /// <summary>
    /// Sets a value into the session state.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="sessionId">The session id.</param>
    /// <param name="key">The key.</param>
    /// <param name="value">The value.</param>
    /// <param name="knownTypes">The known types.</param>
    public void SetSessionState<T>(Guid sessionId, string key, T value, IEnumerable<Type> knownTypes = null)
		{
			using (var stream = new MemoryStream())
			{
        var serializer = new DataContractSerializer(value.GetType(), knownTypes);

				serializer.WriteObject(stream, value);
				stream.Seek(0, SeekOrigin.Begin);

				var t = typeof(T);
        var name = t.ToString();
				name = (name.Length > 200) 
          ? string.Concat(name.Substring(0, 197), "...") 
          : name;

        var sessionStateId = Repositories.Core.Query<SessionState>().Where(x => x.SessionId == sessionId && x.Key == key).Select(x => x.Id).ToList().SingleOrDefault();
        if (sessionStateId == 0)
        {
          var sessionState = new SessionState { SessionId = sessionId, Key = key, Value = stream.ToArray(), TypeOf = name, Size = stream.Length };
          Repositories.Core.Add(sessionState);
        }
        else
        {
          var sessionToUpdate = new Query(typeof(SessionState), Entity.Attribute("Id") == sessionStateId);

          Repositories.Core.Update(sessionToUpdate, new
          {
            Value = stream.ToArray(),
            TypeOf = name,
            Size = stream.Length
          });
        }
        Repositories.Core.SaveChanges();
      }
		}

    /// <summary>
    /// Gets a value from the the session state.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="sessionId">The session id.</param>
    /// <param name="key">The key.</param>
    /// <param name="defaultValue">The default value.</param>
    /// <param name="knownTypes">The known types.</param>
    /// <returns></returns>
    public T GetSessionState<T>(Guid sessionId, string key, T defaultValue = default(T), IEnumerable<Type> knownTypes = null) 
		{
      var bytes = Repositories.Core.Query<SessionState>().Where(x => x.SessionId == sessionId && x.Key == key).Select(x => x.Value).ToList().SingleOrDefault();
			if (bytes == null)
				return defaultValue;

      using (var stream = new MemoryStream(bytes))
			{
        var serializer = new DataContractSerializer(typeof(T), knownTypes);
				stream.Seek(0, SeekOrigin.Begin);

				return (T)serializer.ReadObject(stream);
			}
		}

	  /// <summary>
	  /// Removes a value from the session state.
	  /// </summary>
	  /// <param name="sessionId">The session id.</param>
	  /// <param name="key">The key.</param>		
	  public void RemoveSessionState(Guid sessionId, string key)
	  {
	    var sessionStateToRemove = new Query(typeof (SessionState),
	                                          Entity.Attribute("SessionId") == sessionId &&
	                                          Entity.Attribute("Key") == key &&
	                                          Entity.Attribute("DeletedOn") == null);

	    Repositories.Core.Update(sessionStateToRemove, new
	    {
	      DeletedOn = DateTime.Now
	    });
	    Repositories.Core.SaveChanges();
    }

    /// <summary>
    /// Removes all value from the session state.
    /// </summary>
    /// <param name="sessionId">The session id.</param>
    public void ClearSessionState(Guid sessionId)
    {
      var sessionStateToRemove = new Query(typeof (SessionState),
                                            Entity.Attribute("SessionId") == sessionId &&
                                            Entity.Attribute("DeletedOn") == null);

      Repositories.Core.Update(sessionStateToRemove, new
      {
        DeletedOn = DateTime.Now
      });
      Repositories.Core.SaveChanges();
    }
  }

	#endregion

	#region SessionHelper Interface

	public interface ISessionHelper
	{
		/// <summary>
		/// Creates the session.
		/// </summary>
		/// <param name="userId">The user id.</param>
		/// <returns></returns>
		Guid CreateSession(long userId = 0);

    /// <summary>
    /// Updates the session.
    /// </summary>
    /// <param name="sessionId">The session id.</param>
    /// <param name="userId">The user id.</param>
    /// <param name="checkUser">Whether to check the user Id of the session matches before updating.</param>
		void UpdateSession(Guid sessionId, long userId, bool checkUser = false);

		/// <summary>
		/// Validates the session.
		/// </summary>
		/// <param name="sessionId">The session id.</param>
		/// <param name="userId">The user id.</param>
		/// <returns></returns>
		bool ValidateSession(Guid sessionId, long userId);

    /// <summary>
    /// Sets a value into the session state.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="sessionId">The session id.</param>
    /// <param name="key">The key.</param>
    /// <param name="value">The value.</param>
    /// <param name="knownTypes">The known types.</param>
    void SetSessionState<T>(Guid sessionId, string key, T value, IEnumerable<Type> knownTypes = null);

    /// <summary>
    /// Gets a value from the the session state.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="sessionId">The session id.</param>
    /// <param name="key">The key.</param>
    /// <param name="defaultValue">The default value.</param>
    /// <param name="knownTypes">The known types.</param>
    /// <returns></returns>
    T GetSessionState<T>(Guid sessionId, string key, T defaultValue = default (T), IEnumerable<Type> knownTypes = null);

	  /// <summary>
	  /// Removes a value from the session state.
	  /// </summary>
	  /// <param name="sessionId">The session id.</param>
	  /// <param name="key">The key.</param>		
	  void RemoveSessionState(Guid sessionId, string key);

	  /// <summary>
	  /// Removes all value from the session state.
	  /// </summary>
	  /// <param name="sessionId">The session id.</param>
	  void ClearSessionState(Guid sessionId);
	}

	#endregion
}
