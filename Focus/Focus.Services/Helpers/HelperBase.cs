﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using Focus.Core.Settings.Interfaces;

#endregion

namespace Focus.Services.Helpers
{
	public abstract class HelperBase
	{
		protected static readonly object SyncLock = new object();

		/// <summary>
		/// Initializes a new instance of the <see cref="HelperBase" /> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		protected HelperBase(IRuntimeContext runtimeContext)
		{
			if (runtimeContext == null)
				throw new ArgumentNullException("runtimeContext");

			RuntimeContext = runtimeContext;
		}

		internal IRuntimeContext RuntimeContext { get; private set; }

		internal IAppSettings AppSettings { get { return RuntimeContext.AppSettings; } }
		internal IRepositories Repositories { get { return RuntimeContext.Repositories; } }
		internal IHelpers Helpers { get { return RuntimeContext.Helpers; } }
	}
}
