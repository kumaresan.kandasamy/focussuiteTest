﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;

using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.IntegrationMessages;
using Focus.Core.Models.Career;
using Focus.Core.Models.Integration;
using Focus.Core.Settings.Interfaces;
using Focus.Data.Configuration.Entities;
using Focus.Data.Core.Entities;
using Focus.Data.Library.Entities;
using Focus.Services.DtoMappers;
using Focus.Services.Integration;
using Focus.Services.Integration.Implementations.Georgia;
using Focus.Services.Mappers;
using Focus.Services.Messages;
using Focus.Services.Repositories.Integration;
using Framework.Caching;
using Framework.Core;
using Framework.ServiceLocation;
using Mindscape.LightSpeed;
using Newtonsoft.Json;
using Employer = Focus.Data.Core.Entities.Employer;
using Job = Focus.Data.Core.Entities.Job;

#endregion

namespace Focus.Services.Repositories
{
    public class IntegrationRepository : IIntegrationRepository
    {
        private IIntegrationRepository Provider { get; set; }

        protected static readonly object SyncLock = new object();

        ///// <summary>
        ///// Initializes a new instance of the <see cref="IntegrationRepository"/> class.
        ///// </summary>
        public IntegrationRepository(IRuntimeContext runtimeContext)
        {
            RuntimeContext = runtimeContext ?? ServiceLocator.Current.Resolve<IRuntimeContext>();
            Provider = new IntegrationRepositoryFactory(AppSettings.IntegrationClient, AppSettings.IntegrationSettings, ExternalLookUpItems).Provider;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="IntegrationRepository"/> class for test constructs.
        /// </summary>
        /// <param name="runtimeContext">The runtime context.</param>
        /// <param name="integrationClient">The integration client.</param>
        /// <param name="settings">The settings.</param>
        public IntegrationRepository(IRuntimeContext runtimeContext, IntegrationClient integrationClient, string settings)
        {
            RuntimeContext = runtimeContext ?? ServiceLocator.Current.Resolve<IRuntimeContext>();
            Provider = new IntegrationRepositoryFactory(integrationClient, settings, ExternalLookUpItems).Provider;
        }
        internal IRuntimeContext RuntimeContext { get; private set; }

        internal IAppSettings AppSettings
        {
            get { return RuntimeContext.AppSettings; }
        }

        internal IRepositories Repositories
        {
            get { return RuntimeContext.Repositories; }
        }

        internal IHelpers Helpers
        {
            get { return RuntimeContext.Helpers; }
        }

        public bool IsImplemented(IntegrationPoint integrationPoint)
        {
            return Provider.IsImplemented(integrationPoint);
        }

        #region Implementation of IIntegrationRepository

        public LogActionResponse LogAction(LogActionRequest request)
        {
            UserActionTypeActivity activity = null;

            if (request.CheckForOneADay)
            {
                activity = Repositories.Core.Query<UserActionTypeActivity>()
                                                                        .Where(uaa => uaa.UserId == request.UserId && uaa.ExternalActivityId == request.ActionType.ToString() && uaa.ActionerId == request.ActionerId)
                                                                        .OrderBy(uaa => uaa.Id)
                                                                        .FirstOrDefault();

                if (activity.IsNotNull() && request.RequestMade.Date == activity.LastActivityDate.Date)
                {
                    return new LogActionResponse(request) { Outcome = IntegrationOutcome.Ignored };
                }
            }

            var response = Provider.LogAction(request);
            LogIntegrationRequest(response);

            if (response.Outcome == IntegrationOutcome.Success && request.CheckForOneADay)
            {
                if (activity.IsNull())
                {
                    activity = new UserActionTypeActivity
                    {
                        ExternalActivityId = request.ActionType.ToString(),
                        UserId = request.UserId,
                        ActionerId = request.ActionerId
                    };
                    Repositories.Core.Add(activity);
                }

                activity.LastActivityDate = request.RequestMade;
                Repositories.Core.SaveChanges();
            }

            return response;
        }

        public GetJobSeekerResponse GetJobSeeker(GetJobSeekerRequest request)
        {
            var response = Provider.GetJobSeeker(request);
            LogIntegrationRequest(response);

            if (AppSettings.IntegrationClient == IntegrationClient.Wisconsin && !request.ForImport && response.Outcome == IntegrationOutcome.Success)
                WisconsinJobSeekerResumeTemplate(request.PersonId, response.JobSeeker.Resume, response.JobSeeker.UiClaimant);

            return response;
        }

        public GetJobSeekerResponse GetJobSeekerBySocialSecurityNumber(GetJobSeekerRequest request)
        {
            var response = Provider.GetJobSeekerBySocialSecurityNumber(request);
            LogIntegrationRequest(response);
            return response;
        }

        public AuthenticateJobSeekerResponse AuthenticateJobSeeker(AuthenticateJobSeekerRequest request)
        {
            var response = Provider.AuthenticateJobSeeker(request);
            LogIntegrationRequest(response);
            return response;
        }

        public SaveJobSeekerResponse SaveJobSeeker(SaveJobSeekerRequest request)
        {
            request.IncludeSelectiveService = AppSettings.SelectiveServiceRegistration;

            if (request.JobSeeker.IsNull())
                request.JobSeeker = GetJobSeekerModel(request.PersonId);

            var isNewJobSeeker = request.JobSeeker.ExternalId.IsNullOrEmpty();

            var response = Provider.SaveJobSeeker(request);
            response.IsNewJobSeeker = isNewJobSeeker;

            LogIntegrationRequest(response);
            return response;
        }

        public JobSeekerLoginResponse JobSeekerLogin(JobSeekerLoginRequest request)
        {
            if (request.JobSeeker.IsNull())
                request.JobSeeker = GetJobSeekerModel(request.PersonId);

            var response = Provider.JobSeekerLogin(request);
            LogIntegrationRequest(response);

            if (AppSettings.IntegrationClient == IntegrationClient.Wisconsin && response.Outcome == IntegrationOutcome.Success)
                WisconsinJobSeekerResumeTemplate(request.PersonId, response.Resume, response.UiClaimant);

            return response;
        }

        public SaveJobMatchesResponse SaveJobMatches(SaveJobMatchesRequest request)
        {
            var response = Provider.SaveJobMatches(request);
            LogIntegrationRequest(response);
            return response;
        }

        public AssignJobSeekerActivityResponse AssignJobSeekerActivity(AssignJobSeekerActivityRequest request)
        {
            // Get Assist User External Id if not specified in request
            if (request.AdminId.IsNullOrEmpty() && request.UserId != 0 && !request.IsSelfAssigned)
            {
                var user = Repositories.Core.Users.FirstOrDefault(u => u.Id == request.UserId);

                if (user != null)
                    request.AdminId = user.ExternalId;
            }

            // Get Assist User External Id if not specified in request
            if (request.JobSeekerId.IsNullOrEmpty() && request.ActivityUserId != 0 && !request.IsSelfAssigned)
            {
                var user = Repositories.Core.Users.FirstOrDefault(u => u.Id == request.ActivityUserId);

                if (user != null)
                    request.JobSeekerId = user.ExternalId;
            }

            // Get the current office of the Assist user
            if (request.OfficeId.IsNullOrEmpty() && !request.IsSelfAssigned)
            {
                request.OfficeId = request.ExternalOfficeId;
            }

            if (request.ActivityId.IsNullOrEmpty())
                request.ActivityId = GetActivityId(request.ActivityActionType, request.SelfServicedActivity);

            var isRestricted = false;
            var lastActivityDate = new DateTime(1900, 1, 1);
            UserActionTypeActivity userActionTypeActivity = null;

            var activityRestriction = Provider.GetActivityRestriction(request);

            if (activityRestriction > 0)
            {
                userActionTypeActivity = request.ActivityId.IsNotNullOrEmpty()
                    ? Repositories.Core.Query<UserActionTypeActivity>().Where(uaa => uaa.UserId == request.ActivityUserId && uaa.ExternalActivityId == request.ActivityId && uaa.ActionerId == request.ActionerId)
                                                                                                                         .OrderBy(uaa => uaa.Id)
                                                                                                                         .FirstOrDefault()
                    : null;

                if (userActionTypeActivity.IsNotNull())
                {
                    lastActivityDate = userActionTypeActivity.LastActivityDate;
                }

                // 24 hours = 1 a day
                if (lastActivityDate.Year > 1900)
                {
                    if (activityRestriction == 24)
                    {
                        if (request.RequestMade.Date == lastActivityDate.Date)
                        {
                            isRestricted = true;
                        }
                    }
                    else if (request.RequestMade.AddHours(activityRestriction * -1) < lastActivityDate)
                    {
                        isRestricted = true;
                    }
                }

                if (!isRestricted)
                {
                    if (userActionTypeActivity.IsNull())
                    {
                        userActionTypeActivity = new UserActionTypeActivity
                        {
                            ExternalActivityId = request.ActivityId,
                            UserId = request.ActivityUserId,
                            ActionerId = request.ActionerId
                        };
                        Repositories.Core.Add(userActionTypeActivity);
                    }

                    userActionTypeActivity.LastActivityDate = request.RequestMade;
                    Repositories.Core.SaveChanges();
                }
            }

            var response = !isRestricted
                                             ? Provider.AssignJobSeekerActivity(request)
                                             : new AssignJobSeekerActivityResponse(request) { Outcome = IntegrationOutcome.Ignored };

            if (response.Outcome == IntegrationOutcome.Failure)
            {
                if (!isRestricted && userActionTypeActivity.IsNotNull())
                {
                    userActionTypeActivity.LastActivityDate = lastActivityDate;
                    Repositories.Core.SaveChanges();
                }
            }

            LogIntegrationRequest(response);
            return response;
        }

        public AssignAdminToJobSeekerResponse AssignAdminToJobSeeker(AssignAdminToJobSeekerRequest request)
        {
            var response = Provider.AssignAdminToJobSeeker(request);
            LogIntegrationRequest(response);
            return response;
        }

        public IsUIClaimantResponse IsUIClaimant(IsUIClaimantRequest request)
        {
            var response = Provider.IsUIClaimant(request);
            LogIntegrationRequest(response);
            return response;
        }

        public ChangeUserPasswordResponse ChangeUserPassword(ChangeUserPasswordRequest request)
        {
            var response = Provider.ChangeUserPassword(request);
            LogIntegrationRequest(response);
            return response;
        }

        public SaveJobResponse SaveJob(SaveJobRequest request)
        {
            var coreJob = RuntimeContext.Repositories.Core.Jobs.Single(x => x.Id == request.JobId);
            var isNewJob = coreJob.ExternalId.IsNullOrEmpty();

            if (AppSettings.AllJobPostingsRequireApproval && coreJob.ApprovalStatus != ApprovalStatuses.Approved)
                return new SaveJobResponse(request)
                {
                    Exception = new Exception("Cannot save a job that is not approved."),
                    Outcome = IntegrationOutcome.Failure
                };
            // Convert job to integrationJob
            if (request.Job.IsNull())
                request.Job = coreJob.ToIntegrationJob(RuntimeContext);

            var response = Provider.SaveJob(request);
            response.IsNewJob = isNewJob;

            if (response.Outcome == IntegrationOutcome.Success)
            {
                var job = Repositories.Core.FindById<Job>(request.JobId);
                job.ExternalId = response.ExternalJobId;
                Repositories.Core.SaveChanges();
            }

            LogIntegrationRequest(response);
            return response;
        }

        public GetJobResponse GetJob(GetJobRequest request)
        {
            var response = Provider.GetJob(request);
            LogIntegrationRequest(response);
            return response;
        }

        public ReferJobSeekerResponse ReferJobSeeker(ReferJobSeekerRequest request)
        {
            // Convert job to integrationJob
            if (request.Job.IsNull())
            {
                if (request.JobId != 0)
                {
                    request.Job = Repositories.Core.Jobs.Single(x => x.Id == request.JobId).ToIntegrationJob(RuntimeContext);
                }
                else if (request.PostingId != 0)
                {
                    var postingJobId = Repositories.Core.Postings.Where(x => x.Id == request.PostingId).Select(p => p.JobId).FirstOrDefault();

                    if (postingJobId.IsNotNullOrZero())
                    {
                        request.JobId = postingJobId.Value;
                        request.Job = Repositories.Core.Jobs.Single(x => x.Id == postingJobId).ToIntegrationJob(RuntimeContext);
                    }
                    else
                    {
                        request.Job = Repositories.Core.Postings.Single(x => x.Id == request.PostingId).AsDto().ToIntegrationJobModel();

                        if (request.Job.Addresses.IsNotNullOrEmpty())
                        {
                            var countryId = Helpers.Lookup.GetLookup(LookupTypes.Countries).FirstOrDefault(x => x.Key == "Country.US").Id;

                            // Convert zip to county Id
                            foreach (var address in request.Job.Addresses)
                            {
                                var state = Helpers.Lookup.GetLookup(LookupTypes.States).FirstOrDefault(x => x.ExternalId == address.State);
                                address.CountryId = countryId;

                                if (!(address.CountyId == 0 & address.PostCode.IsNotNullOrEmpty())) continue;

                                var postCode = Repositories.Library.Query<PostalCode>().FirstOrDefault(x => x.Code == address.PostCode);

                                if (postCode.IsNull()) continue;
                                // Get county
                                var lookup = Helpers.Lookup.GetLookup(LookupTypes.Counties).SingleOrDefault(x => x.Key == postCode.CountyKey);

                                if (lookup.IsNotNull())
                                    address.CountyId = lookup.Id;
                                // Get state
                                lookup = Helpers.Lookup.GetLookup(LookupTypes.States).SingleOrDefault(x => x.Key == postCode.StateKey);
                                if (lookup.IsNotNull())
                                    address.StateId = lookup.Id;
                            }
                        }
                    }
                }
            }

            if (request.JobSeekerId.IsNullOrEmpty() && request.PersonId.GetValueOrDefault() != 0)
            {
                var jobSeeker = Repositories.Core.Persons.Single(x => x.Id == request.PersonId).User.AsDto();
                request.JobSeekerId = jobSeeker.ExternalId;
            }

            var response = Provider.ReferJobSeeker(request);

            if (response.ExternalJobId.IsNotNullOrEmpty() && (request.JobId != 0 || request.PostingId != 0))
            {
                var posting = request.JobId == 0
                                  ? Repositories.Core.Postings.Single(p => p.Id == request.PostingId)
                                                    : Repositories.Core.Postings.Single(p => p.JobId == request.JobId);

                posting.ExternalId = response.ExternalJobId;

                var job = request.JobId == 0
                    ? Repositories.Core.Jobs.Single(x => x.Id == posting.JobId)
                    : Repositories.Core.Jobs.Single(x => x.Id == request.JobId);

                if (job.IsNotNull())
                {
                    job.ExternalId = response.ExternalJobId;
                }

                Repositories.Core.SaveChanges();
            }

            LogIntegrationRequest(response);
            return response;
        }

        public SetApplicationStatusResponse SetApplicationStatus(SetApplicationStatusRequest request)
        {
            var response = Provider.SetApplicationStatus(request);
            LogIntegrationRequest(response);
            return response;
        }

        public GetEmployerResponse GetEmployer(GetEmployerRequest request)
        {
            var response = Provider.GetEmployer(request);
            LogIntegrationRequest(response);
            return response;
        }

        public SaveEmployerResponse SaveEmployer(SaveEmployerRequest request)
        {
            if (request.Employer.IsNull())
                request.Employer = RuntimeContext.Repositories.Core.Employers.Single(x => x.Id == request.EmployerId).ToIntegrationEmployerModel(RuntimeContext);

            var response = Provider.SaveEmployer(request);

            if (response.Outcome == IntegrationOutcome.Success)
            {
                var employer = Repositories.Core.FindById<Employer>(request.EmployerId);
                employer.ExternalId = response.ExternalEmployerId;
                foreach (var employee in response.Employees)
                {
                    var employeeRecord = Repositories.Core.FindById<Employee>(employee.Item1);
                    employeeRecord.Person.User.ExternalId = employee.Item2;
                }
                Repositories.Core.SaveChanges();
            }

            LogIntegrationRequest(response);
            return response;
        }

        public AssignAdminToEmployerResponse AssignAdminToEmployer(AssignAdminToEmployerRequest request)
        {
            var response = Provider.AssignAdminToEmployer(request);
            LogIntegrationRequest(response);
            return response;
        }

        public AssignAdminToJobOrderResponse AssignAdminToJobOrder(AssignAdminToJobOrderRequest request)
        {
            var response = Provider.AssignAdminToJobOrder(request);
            LogIntegrationRequest(response);
            return response;
        }

        public AssignEmployerActivityResponse AssignEmployerActivity(AssignEmployerActivityRequest request)
        {
            var response = Provider.AssignEmployerActivity(request);
            LogIntegrationRequest(response);
            return response;
        }

        public SaveEmployeeResponse SaveEmployee(SaveEmployeeRequest request)
        {
            var response = Provider.SaveEmployee(request);

            if (request.Employee.IsNull())
            {
                var employee = RuntimeContext.Repositories.Core.Employees.SingleOrDefault(x => x.PersonId == request.PersonId);

                if (employee.IsNull())
                    response.Outcome = IntegrationOutcome.Failure;
                else
                    request.Employee = employee.ToIntegrationEmployee(RuntimeContext);
            }

            if (response.Outcome == IntegrationOutcome.Success && request.Employee.ExternalId.IsNullOrEmpty())
            {
                var person = Repositories.Core.FindById<Person>(request.PersonId);
                person.User.ExternalId = response.ExternalEmployeeId;
                Repositories.Core.SaveChanges();
            }

            LogIntegrationRequest(response);
            return response;
        }

        public UpdateEmployeeStatusResponse UpdateEmployeeStatus(UpdateEmployeeStatusRequest request)
        {
            var response = Provider.UpdateEmployeeStatus(request);
            LogIntegrationRequest(response);
            return response;
        }

        public AuthenticateStaffResponse AuthenticateStaffUser(AuthenticateStaffRequest request)
        {
            var response = Provider.AuthenticateStaffUser(request);
            LogIntegrationRequest(response);
            return response;
        }

        public DisableJobSeekersReportResponse DisableJobSeekersReport(DisableJobSeekersReportRequest request)
        {
            var response = Provider.DisableJobSeekersReport(request);
            LogIntegrationRequest(response);
            return response;
        }

        public bool OfficeRequiresUpdate(IIntegrationRequest request)
        {
            return Provider.OfficeRequiresUpdate(request);
        }

        public virtual IntegrationAuthentication AuthenticationSupported(FocusModules module)
        {
            return Provider.AuthenticationSupported(module);
        }

        #endregion

        #region private methods

        private List<ExternalLookUpItemDto> ExternalLookUpItems
        {
            get
            {
                var externalLookUpItems = Cacher.Get<List<ExternalLookUpItemDto>>(Constants.CacheKeys.ExternalLookUpItems);

                if (externalLookUpItems.IsNull())
                {
                    lock (SyncLock)
                    {
                        externalLookUpItems = Repositories.Configuration.Query<ExternalLookUpItem>().Select(x => x.AsDto()).ToList();
                        UpdateOfficeLookup(externalLookUpItems);
                        Cacher.Set(Constants.CacheKeys.ExternalLookUpItems, externalLookUpItems);
                    }
                }

                return externalLookUpItems;
            }
        }

        /// <summary>
        /// Update the IntegrationOfficeIdPerZip using information held in the office table
        /// </summary>
        /// <param name="externalLookUpItems"></param>
        /// <remarks>
        /// The logic for looking up offices is as follows:
        /// The zip code should first be looked up in the External Lookup table, and if it is not there, it should be looked up in the office table. 
        /// The reason for two tables is that Focus allows a zip to be assigned to more than one office, but EKOS only wants one office. 
        /// Because there is no concept of a "default" office for a given zip code, the external look up table is used first, as this effectively holds the default.
        /// However, because Focus.Services.Integration does not have access to the database, what this method is doing is adding the zips from the Office table into the external lookup table,
        /// but only where the zip does not exist in the look-up already. This then means the external look-up contains the other office data to fall back on.
        /// </remarks>
        private void UpdateOfficeLookup(List<ExternalLookUpItemDto> externalLookUpItems)
        {
            var splitChars = new[] { ',' };

            var offices = Repositories.Core.Offices.Where(office => office.InActive == false && !string.IsNullOrEmpty(office.ExternalId) && !string.IsNullOrEmpty(office.AssignedPostcodeZip)).ToList();
            var existingZips = externalLookUpItems.Where(ex => ex.ExternalLookUpType == ExternalLookUpType.IntegrationOfficeIdPerZip)
                                                                                        .Select(ex => ex.InternalId)
                                                                                        .ToList();

            foreach (var office in offices)
            {
                var officeExternalId = office.ExternalId;

                var assignedZips = office.AssignedPostcodeZip.Split(splitChars, StringSplitOptions.RemoveEmptyEntries);
                var itemsToAdd = assignedZips.Where(zip => !existingZips.Contains(zip))
                                                                         .Select(zip => new ExternalLookUpItemDto
                                                                         {
                                                                             InternalId = zip,
                                                                             ExternalId = officeExternalId,
                                                                             ExternalLookUpType = ExternalLookUpType.IntegrationOfficeIdPerZip
                                                                         }).ToList();

                externalLookUpItems.AddRange(itemsToAdd);
                existingZips.AddRange(itemsToAdd.Select(i => i.InternalId));
            }
        }

        /// <summary>
        /// Logs the integration request ready for message bus processing
        /// </summary>
        /// <param name="response">The response.</param>
        private void LogIntegrationRequest(IntegrationResponse response)
        {
            // Don't bother logging if in standalone mode (Shouldn't really get here but just in case)
            if (AppSettings.IntegrationClient == IntegrationClient.Standalone || response.Outcome == IntegrationOutcome.Ignored)
                return;

            // Log if the IntegrationLoggingLevel Setting allows
            if ((AppSettings.IntegrationLoggingLevel == IntegrationLoggingLevel.All) ||
                (AppSettings.IntegrationLoggingLevel == IntegrationLoggingLevel.Failed &&
                 response.Outcome == IntegrationOutcome.Failure))
            {
                var log = new IntegrationLog
                {
                    IntegrationPoint = response.Request.IntegrationPoint,
                    RequestededBy = response.Request.UserId,
                    RequestedOn = response.Request.RequestMade,
                    RequestStart = response.Begin,
                    RequestEnd = DateTime.Now,
                    Outcome = response.Outcome,
                    RequestId = response.Request.RequestId
                };

                foreach (var requestResponse in response.IntegrationBreadcrumbs)
                {
                    LogBreadcrumb(log, requestResponse);
                }

                if (response.Exception.IsNotNull())
                {
                    var requestException = response.Exception as IntegrationRequestException;

                    if (requestException.IsNotNull() && requestException.RequestResponse.IsNotNull())
                    {
                        LogBreadcrumb(log, requestException.RequestResponse);
                    }

                    var integrationException = new IntegrationException
                    {
                        Message = response.Exception.Message.TruncateString(1000),
                        StackTrace = response.Exception.StackTrace ?? string.Empty
                    };

                    if (integrationException.StackTrace.Length == 0)
                        integrationException.StackTrace = "(No Stack Trace)";

                    log.IntegrationExceptions.Add(integrationException);
                }

                Repositories.Core.Add(log);
                Repositories.Core.SaveChanges();
            }
        }

        /// <summary>
        /// Adds a RequestRepsonse to the Log record
        /// </summary>
        /// <param name="log">The log record</param>
        /// <param name="requestResponse">The rq</param>
        private void LogBreadcrumb(IntegrationLog log, RequestResponse requestResponse)
        {
            var integrationRequestResponse = new IntegrationRequestResponse
            {
                Url = requestResponse.Url.IsNotNullOrEmpty() ? requestResponse.Url : "URL not found",
                StatusCode = (int?)requestResponse.StatusCode,
                StatusDescription = requestResponse.StatusDescription.TruncateString(250),
                RequestSentOn = requestResponse.StartTime != DateTime.MinValue ? requestResponse.StartTime : new DateTime(1900, 1, 1),
                ResponseReceivedOn = requestResponse.EndTime != DateTime.MinValue ? requestResponse.EndTime : (DateTime?)null
            };
            integrationRequestResponse.IntegrationRequestResponseMessages.Add(new IntegrationRequestResponseMessages
            {
                RequestMessage = requestResponse.OutgoingRequest,
                ResponseMessage = requestResponse.IncomingResponse ?? "Not received"
            });
            log.IntegrationRequestResponses.Add(integrationRequestResponse);
        }

        public string GetActivityId(ActionTypes actionType, bool selfServicedActivity)
        {
            return Provider.GetActivityId(actionType, selfServicedActivity);
        }

        public bool IsActivityStaffAssisted(ActionTypes actionType)
        {
            return Provider.IsActivityStaffAssisted(actionType);
        }

        public virtual int GetActivityRestriction(AssignJobSeekerActivityRequest request)
        {
            return Provider.GetActivityRestriction(request);
        }

        #endregion

        #region Build models

        /// <summary>
        /// Gets the job seeker model for a person
        /// </summary>
        /// <param name="personId">The person id.</param>
        /// <returns>The job seeker model for the person</returns>
        private JobSeekerModel GetJobSeekerModel(long personId)
        {
            var person = Repositories.Core.FindById<Person>(personId);
            var user = Repositories.Core.Users.Single(x => x.PersonId == personId);

            var jobSeeker = new JobSeekerModel
            {
                ExternalId = user.ExternalId,
                LastName = person.LastName,
                FirstName = person.FirstName,
                MiddleInitial = person.MiddleInitial,
                Username = user.UserName,
                Password = user.PasswordHash,
                EmailAddress = person.EmailAddress,
                SocialSecurityNumber = person.SocialSecurityNumber,
                DateOfBirth = person.DateOfBirth,
                Id = user.Id
            };

            if (person.AssignedToId.IsNotNull())
                jobSeeker.AdminId = Repositories.Core.Users.Where(u => u.PersonId == person.AssignedToId).Select(u => u.ExternalId).FirstOrDefault();

            var resume = Repositories.Core.Resumes.FirstOrDefault(r => r.PersonId == personId && r.StatusId == ResumeStatuses.Active && r.IsDefault && r.ResumeCompletionStatusId == ResumeCompletionStatuses.Completed);

            if (resume.IsNotNull())
            {
                var resumeDto = resume.AsDto();
                jobSeeker.Resume = resumeDto.ToResumeModel(RuntimeContext, "EN-US");
                jobSeeker.ResumeXml = resumeDto.ResumeXml;
                jobSeeker.PrimaryOnet = resumeDto.PrimaryOnet;
                jobSeeker.DefaultResumeCompletionStatus = resumeDto.ResumeCompletionStatusId;
            }

            var phone = person.PhoneNumbers.FirstOrDefault(pn => pn.IsPrimary);

            if (phone.IsNotNull())
                jobSeeker.PrimaryPhone = phone.Number;

            var address = person.PersonAddresses.FirstOrDefault(add => add.IsPrimary);

            if (address.IsNotNull())
            {
                jobSeeker.AddressLine1 = address.Line1;
                jobSeeker.AddressLine2 = address.Line2;
                jobSeeker.AddressTownCity = address.TownCity;
                jobSeeker.AddressPostcodeZip = address.PostcodeZip;
                jobSeeker.AddressCountyId = address.CountyId;
                jobSeeker.AddressStateId = address.StateId;
                jobSeeker.AddressCountryId = address.CountryId;
            }

            if (AppSettings.IntegrationClient == IntegrationClient.Wisconsin)
            {
                jobSeeker.Stats = GetResumeStatistics(personId);
            }

            return jobSeeker;
        }

        private ResumeStatistics GetResumeStatistics(long personId)
        {
            var resumes = Repositories.Core.Resumes.Where(r => r.PersonId == personId && r.StatusId == ResumeStatuses.Active && r.ResumeCompletionStatusId == ResumeCompletionStatuses.Completed).Select(x => x.IsSearchable).ToList();
            return new ResumeStatistics
            {
                ActiveResumes = resumes.Count,
                SearchableResumes = resumes.Count(x => x)
            };
        }

        #endregion

        #region Wisconsin Specific Business Logic

        /// <summary>
        /// Wisconsins specific logic for a job seeker logging in.
        /// </summary>
        /// <param name="response">The response.</param>
        private void WisconsinJobSeekerResumeTemplate(long personId, ResumeModel resume, bool uiClaimant)
        {
            // Update the user record WI data
            var person = RuntimeContext.Repositories.Core.FindById<Person>(personId);

            if (person.IsNotNull())
            {
                person.EnrollmentStatus = resume.ResumeContent.EducationInfo.SchoolStatus;
                person.IsUiClaimant = uiClaimant;
                person.FirstName = resume.ResumeContent.Profile.UserGivenName.FirstName;
                person.LastName = resume.ResumeContent.Profile.UserGivenName.LastName;
                person.DateOfBirth = resume.ResumeContent.Profile.DOB;
                person.EmailAddress = resume.ResumeContent.Profile.EmailAddress;
                person.User.ScreenName = resume.ResumeContent.Profile.UserGivenName.FirstName + " " + resume.ResumeContent.Profile.UserGivenName.LastName;

                var address = person.PersonAddresses.FirstOrDefault(x => x.IsPrimary);

                if (address.IsNull())
                {
                    address = new PersonAddress { IsPrimary = true };
                    person.PersonAddresses.Add(address);
                }
                address.Line1 = resume.ResumeContent.Profile.PostalAddress.Street1;
                address.Line2 = resume.ResumeContent.Profile.PostalAddress.Street2;
                address.TownCity = resume.ResumeContent.Profile.PostalAddress.City;
                address.PostcodeZip = resume.ResumeContent.Profile.PostalAddress.Zip;
                address.CountyId = resume.ResumeContent.Profile.PostalAddress.CountyId;
                address.StateId = resume.ResumeContent.Profile.PostalAddress.StateId.GetValueOrDefault();
                address.CountryId = resume.ResumeContent.Profile.PostalAddress.CountryId.GetValueOrDefault();

                if (resume.ResumeContent.Profile.PrimaryPhone.IsNotNull())
                {
                    var phone = RuntimeContext.Repositories.Core.PhoneNumbers.FirstOrDefault(x => x.IsPrimary && x.PersonId == personId);

                    if (phone.IsNull())
                    {
                        phone = new PhoneNumber { PersonId = personId, IsPrimary = true };
                        RuntimeContext.Repositories.Core.Add(phone);
                    }
                    phone.Number = resume.ResumeContent.Profile.PrimaryPhone.PhoneNumber;
                    phone.PhoneType = PhoneTypes.Mobile;
                }

                // Add/Update resume template
                var resumeTemplate = person.ResumeTemplate;

                if (resumeTemplate.IsNull())
                {
                    resumeTemplate = new ResumeTemplate();
                    person.ResumeTemplate = resumeTemplate;
                }
                resumeTemplate.ResumeXml = resume.ToResumeDto(RuntimeContext, RuntimeContext.CurrentRequest.UserContext.Culture, AppSettings.DefaultCountryKey).ResumeXml;

                RuntimeContext.Repositories.Core.SaveChanges();
            }
        }

        #endregion

        #region Georgia Specific Business Logic

        public string GetActionData(ActionTypes actionType, long? actionId, long actionerId)
        {
            if (AppSettings.IntegrationClient != IntegrationClient.Georgia) return "";

            if (actionType.IsIn(ActionTypes.PostJobToLens, ActionTypes.UnregisterJobFromLens, ActionTypes.UpdateJobAssignedOffice))
            {
                var jobId = (actionType == ActionTypes.UpdateJobAssignedOffice)
                                ? Repositories.Core.ActionEvents.Where(ae => ae.Id == actionId).Select(ae => ae.EntityId).FirstOrDefault()
                                : actionId;

                var job = (from j in Repositories.Core.Jobs
                           where j.Id == jobId
                           select j).FirstOrDefault();

                if (job == null || job.JobStatus.IsNotIn(JobStatuses.Active, JobStatuses.OnHold, JobStatuses.Closed))
                    return "";

                var posting = (from p in Repositories.Core.Postings
                               where p.JobId == job.Id
                               select p).FirstOrDefault();

                var jobOffice = (from jo in Repositories.Core.JobOfficeMappers
                                 where jo.JobId == job.Id
                                 orderby jo.Id descending
                                 select jo.Office).FirstOrDefault();

                var jobLocation = (from jl in Repositories.Core.JobLocations
                                   where jl.JobId == job.Id
                                   select jl).FirstOrDefault();

                var employer = (from e in Repositories.Core.Employers
                                where e.Id == job.EmployerId
                                select e).FirstOrDefault();

                var businessAddress = (from ba in Repositories.Core.BusinessUnitAddresses
                                       where ba.BusinessUnitId == job.BusinessUnitId
                                                   && ba.IsPrimary
                                       select ba).FirstOrDefault();

                #region ActiveJobModel
                var activeJob = new ActiveJobModel
                {
                    JobId = job.Id,
                    JobStatus = job.JobStatus,
                    JobTitle = job.JobTitle,
                    JobDescription = job.Description,
                    JobOffice = jobOffice.IsNotNull() ? jobOffice.ExternalId : string.Empty,
                    FederalEmployerIdentificationNumber = employer.IsNotNull() ? employer.FederalEmployerIdentificationNumber : string.Empty,
                    Onet = job.OnetId,
                    JobCity = jobLocation.IsNotNull() ? jobLocation.City : string.Empty,
                    JobState = jobLocation.IsNotNull() ? jobLocation.State : string.Empty,
                    JobZip = jobLocation.IsNotNull() ? jobLocation.Zip : string.Empty,
                    DatePosted = job.CreatedOn.ToString("yyyy-MM-dd hh:mm:ss"),
                    ExpirationDate = job.ClosingOn != null ? job.ClosingOn.Value.ToString("yyyy-MM-dd hh:mm:ss") : null,
                    MinSalary = job.MinSalary,
                    MaxSalary = job.MaxSalary,
                    SalaryFrequencyId = job.SalaryFrequencyId,
                    HideSalaryOnPosting = job.HideSalaryOnPosting,
                    EmploymentStatusId = job.EmploymentStatusId,
                    Url =
                                posting.IsNotNull()
                                    ? AppSettings.CareerApplicationPath + "/jobposting/" + posting.LensPostingId +
                                        "/JobSearchResults/JobSearchResults"
                                    : string.Empty,
                    EmployerCity = businessAddress.IsNotNull() ? businessAddress.TownCity : string.Empty,
                    EmployerStateId = businessAddress.IsNotNull() ? businessAddress.StateId : 0,
                    MinimumEducationLevel = job.MinimumEducationLevel,
                    MinimumExperienceYears = job.MinimumExperience,
                    MinimumExperienceMonths = job.MinimumExperienceMonths,
                    HideEducationOnPosting = job.HideEducationOnPosting,
                    HideExperienceOnPosting = job.HideExperienceOnPosting
                };
                #endregion

                return JsonConvert.SerializeObject(activeJob, Formatting.Indented, new JsonSerializerSettings { PreserveReferencesHandling = PreserveReferencesHandling.None });
            }

            var actionEvent = (from ae in Repositories.Core.ActionEvents
                               join et in Repositories.Core.EntityTypes
                                      on ae.EntityTypeId equals et.Id
                               where ae.Id == actionId
                               select new ActionEventModel
                               {
                                   CreatedOn = ae.CreatedOn,
                                   ActionedOn = ae.ActionedOn,
                                   EntityId = ae.EntityId,
                                   EntityIdAdditional01 = ae.EntityIdAdditional01,
                                   UserId = ae.UserId,
                                   EntityTypeName = et.Name,
                                   ActionId = ae.Id,
                                   ActivityStatusId = ae.ActivityStatusId
                               }).FirstOrDefault();

            var serviceId = actionType.ToString();

            if (actionEvent == null)
                throw new Exception("Unable to retrieve ActionEvent to facilitate gathering integration data for Georgia");

            var entityType = GetEntityType(actionType, actionEvent.EntityTypeName);

            long? personId = null;
            long? resumeId = null;
            long? characteristicJobId = null;

            switch (entityType)
            {
                case EntityTypes.Resume:
                    resumeId = actionEvent.EntityId;
                    break;

                case EntityTypes.Application:

                    var applicationDetails = Repositories.Core.Applications.Where(a => a.Id == actionEvent.EntityId)
                                                                                                                                 .Select(a => new { a.ResumeId, a.PostingId })
                                                                                                                       .FirstOrDefault();

                    if (applicationDetails.IsNull())
                        return "";

                    resumeId = applicationDetails.ResumeId;
                    characteristicJobId = Repositories.Core.Postings.Where(p => p.Id == applicationDetails.PostingId).Select(a => a.JobId).FirstOrDefault();
                    break;

                case EntityTypes.User:
                    if (actionType == ActionTypes.LogIn && !AppSettings.Module.IsIn(FocusModules.Career, FocusModules.CareerExplorer))
                        return "";

                    personId = Repositories.Core.Users.Where(u => u.Id == actionEvent.EntityId && u.UserType != UserTypes.Talent)
                                                                                        .Select(u => u.PersonId)
                                                                                        .FirstOrDefault();
                    break;

                case EntityTypes.JobSeeker:
                    personId = actionEvent.EntityId;

                    if (actionType == ActionTypes.InviteJobSeekerToApply && actionEvent.EntityIdAdditional01.IsNotNull())
                        personId = actionEvent.EntityIdAdditional01;

                    break;

                case EntityTypes.Posting:
                    if (actionType.IsIn(ActionTypes.ExternalReferral, ActionTypes.ExternalStaffReferral))
                    {
                        personId = actionEvent.EntityIdAdditional01;
                    }
                    else
                    {
                        var userId = actionType == ActionTypes.ViewedPostingFromSearch
                                         ? actionEvent.EntityIdAdditional01 ?? actionEvent.UserId
                                         : actionEvent.UserId;

                        personId = Repositories.Core.Users.Where(u => u.Id == userId)
                                                                                            .Select(u => u.PersonId)
                                                                                            .FirstOrDefault();
                    }
                    break;

                case EntityTypes.Employee:
                    if (actionType == ActionTypes.AssignActivityToEmployee)
                    {
                        var employee = Repositories.Core.FindById<Employee>(actionEvent.EntityId);
                        personId = employee.PersonId;
                    }
                    break;
            }

            if (personId.IsNullOrZero() && resumeId.IsNullOrZero())
                return "";

            ResumeBasicModel resumeModel;

            if (!entityType.Equals(EntityTypes.Employee))
            {
                if (resumeId.IsNotNullOrZero())
                {
                    resumeModel = (from r in Repositories.Core.Resumes
                                   where r.Id == resumeId
                                   select new ResumeBasicModel
                                   {
                                       Id = r.Id,
                                       PersonId = r.PersonId,
                                       IsDefault = r.IsDefault,
                                       HighestEducationLevel = r.HighestEducationLevel,
                                       CountyId = r.CountyId,
                                       MigrantSeasonalFarmWorker = Repositories.Core.FindById<Person>(Repositories.Core.FindById<Resume>(resumeId).PersonId).MigrantSeasonalFarmWorkerVerified,
                                       HomelessNoShelter = r.HomelessNoShelter,
                                       HomelessWithShelter = r.HomelessWithShelter,
                                       ExOffender = r.ExOffender,
                                       RunawayYouth18OrUnder = r.RunawayYouth18OrUnder,
                                       DisplacedHomemaker = r.DisplacedHomemaker,
                                       SingleParent = r.SingleParent,
                                       LowIncomeStatus = r.LowIncomeStatus,
                                       LowLevelLiteracy = r.LowLevelLiteracy,
                                       CulturalBarriers = r.CulturalBarriers,
                                       PreferredLanguage = r.PreferredLanguage,
                                       NativeLanguage = r.NativeLanguage,
                                       CommonLanguage = r.CommonLanguage,
                                       NoOfDependents = r.NoOfDependents,
                                       EstMonthlyIncome = r.EstMonthlyIncome
                                   }).FirstOrDefault();

                    if (resumeModel.IsNotNull())
                        personId = resumeModel.PersonId;
                }
                else
                {
                    resumeModel = (from r in Repositories.Core.Resumes
                                   where r.PersonId == personId
                                      && r.IsDefault
                                   select new ResumeBasicModel
                                   {
                                       Id = r.Id,
                                       PersonId = r.PersonId,
                                       IsDefault = r.IsDefault,
                                       HighestEducationLevel = r.HighestEducationLevel,
                                       CountyId = r.CountyId,
                                       MigrantSeasonalFarmWorker = Repositories.Core.FindById<Person>(personId).MigrantSeasonalFarmWorkerVerified,
                                       HomelessNoShelter = r.HomelessNoShelter,
                                       HomelessWithShelter = r.HomelessWithShelter,
                                       ExOffender = r.ExOffender,
                                       RunawayYouth18OrUnder = r.RunawayYouth18OrUnder,
                                       DisplacedHomemaker = r.DisplacedHomemaker,
                                       SingleParent = r.SingleParent,
                                       LowIncomeStatus = r.LowIncomeStatus,
                                       LowLevelLiteracy = r.LowLevelLiteracy,
                                       CulturalBarriers = r.CulturalBarriers,
                                       PreferredLanguage = r.PreferredLanguage,
                                       NativeLanguage = r.NativeLanguage,
                                       CommonLanguage = r.CommonLanguage,
                                       NoOfDependents = r.NoOfDependents,
                                       EstMonthlyIncome = r.EstMonthlyIncome
                                   }).FirstOrDefault();
                }

                if (actionType == ActionTypes.CompleteResume && resumeModel.IsNotNull() && !resumeModel.IsDefault)
                    return "";

                var characteristics = (from person in Repositories.Core.Persons
                                       join user in Repositories.Core.Users
                                         on person.Id equals user.PersonId
                                       where person.Id == personId
                                       select new CharacteristicsModel
                                       {
                                           FirstName = person.FirstName,
                                           MiddleName = person.MiddleInitial,
                                           LastName = person.LastName,
                                           Ssn = person.SocialSecurityNumber,
                                           EnrollmentStatus = person.EnrollmentStatus,
                                           JobSeekerId = person.Id,
                                           UserId = user.Id
                                       }).FirstOrDefault();

                if (characteristics.IsNull())
                    return "";

                characteristics.ServiceId = serviceId;
                characteristics.Activity = new ActivityId();
                characteristics.ActionId = actionEvent.ActionId;
                characteristics.EnteredDate = actionEvent.CreatedOn.ToString("yyyy-MM-dd hh:mm:ss");
                characteristics.DeliveryDate = actionEvent.ActionedOn.ToString("yyyy-MM-dd hh:mm:ss");

                switch (actionEvent.ActivityStatusId)
                {
                    case ActionEventStatus.Backdated:
                        characteristics.ActivityStatusId = 1;
                        break;
                    case ActionEventStatus.Deleted:
                        characteristics.ActivityStatusId = 2;
                        break;
                    case ActionEventStatus.None:
                        characteristics.ActivityStatusId = 0;
                        break;
                }

               
                characteristics.JobId = characteristicJobId;

                if (resumeModel != null)
                {
                    characteristics.HighestGradeCompleted = resumeModel.HighestEducationLevel;
                    characteristics.CountyOfResidence = resumeModel.CountyId;
                    characteristics.MigrantSeasonalFarmWorkerVerified = resumeModel.MigrantSeasonalFarmWorker;
                    characteristics.HomelessNoShelter = resumeModel.HomelessNoShelter ? 1 : 9;
                    characteristics.HomelessWithShelter = resumeModel.HomelessWithShelter ? 1 : 9;
                    characteristics.RunawayYouth18OrUnder = resumeModel.RunawayYouth18OrUnder ? 1 : 9;
                    characteristics.ExOffender = resumeModel.ExOffender ? 1 : 9;
                    characteristics.DisplacedHomemaker = resumeModel.DisplacedHomemaker ? 1 : 9;
                    characteristics.SingleParent = resumeModel.SingleParent ? 1 : 9;
                    characteristics.LowIncomeStatus = resumeModel.LowIncomeStatus ? 1 : 9;
                    characteristics.LowLevelLiteracy = resumeModel.LowLevelLiteracy ? 1 : 9;
                    characteristics.CulturalBarriers = resumeModel.CulturalBarriers ? 1 : 9;
                    characteristics.PreferredLanguage = resumeModel.PreferredLanguage;
                    characteristics.CommonLanguage = resumeModel.CommonLanguage;
                    characteristics.NativeLanguage = resumeModel.NativeLanguage;
                    characteristics.NoOfDependents = resumeModel.NoOfDependents;
                    characteristics.EstMonthlyIncome = resumeModel.EstMonthlyIncome;
                }

                // set activity id when assigned activity
                if (actionType == ActionTypes.AssignActivityToCandidate)
                {
                    var activity = Repositories.Core.Query<Activity>().FirstOrDefault(a => a.Id == actionEvent.EntityIdAdditional01);

                    characteristics.Activity = new ActivityId
                    {
                        Key = activity != null ? activity.LocalisationKey : null,
                        Name = activity != null ? activity.Name : null
                    };
                }

                var es = (from js in Repositories.Report.JobSeekers
                          where js.FocusPersonId == personId
                          select js).FirstOrDefault();

                if (es != null)
                    characteristics.EmploymentStatus = es.EmploymentStatus;

                var so = (from po in Repositories.Core.PersonOfficeMappers
                          where po.PersonId == personId
                          select po).OrderByDescending(x => x.CreatedOn).FirstOrDefault();

                if (so != null)
                    characteristics.SeekerCareerCenter = so.OfficeId;

                if (actionerId > 0 && actionerId != characteristics.UserId)
                {
                    var staffMember = Repositories.Core.FindById<User>(actionerId);

                    var sto = (from pom in Repositories.Core.PersonsCurrentOffices
                               where pom.PersonId == staffMember.PersonId
                               select pom).OrderByDescending(x => x.StartTime).FirstOrDefault();

                    if (sto != null)
                        characteristics.StaffCareerCenter = sto.OfficeId;
                    characteristics.StaffExternalId = staffMember.ExternalId;
                }
                return JsonConvert.SerializeObject(characteristics, Formatting.Indented, new JsonSerializerSettings { PreserveReferencesHandling = PreserveReferencesHandling.None });
            }
            else
            {
                //This feature of sending activities of Hiring Manager to Georgia Endpoints will be implemented in future based on the client's requirement

                var characteristics = (from person in Repositories.Core.Persons
                                       join user in Repositories.Core.Users
                                         on person.Id equals user.PersonId
                                       where person.Id == personId
                                       select new EmployeeCharacteristicsModel
                                       {
                                           FirstName = person.FirstName,
                                           MiddleName = person.MiddleInitial,
                                           LastName = person.LastName,
                                           UserId = user.Id
                                       }).FirstOrDefault();
                var employee = Repositories.Core.FindById<Employee>(actionEvent.EntityId);

                characteristics.EmployeeId = employee.Id;
                characteristics.EmployerId = employee.EmployerId;
                characteristics.FEIN = employee.Employer.FederalEmployerIdentificationNumber;
                characteristics.BusinessUnitId = employee.EmployeeBusinessUnits.Select(x => x.BusinessUnitId).FirstOrDefault();
                characteristics.BusinessLegalName = employee.Employer.LegalName;

                characteristics.ServiceId = serviceId;
                characteristics.Activity = new ActivityId();
                characteristics.ActionId = actionEvent.ActionId;
                characteristics.EnteredDate = actionEvent.CreatedOn.ToString("yyyy-MM-dd hh:mm:ss");
                characteristics.DeliveryDate = actionEvent.ActionedOn.ToString("yyyy-MM-dd hh:mm:ss");

                // set activity id when assigned activity
                if (actionType == ActionTypes.AssignActivityToEmployee)
                {
                    var activity = Repositories.Core.Query<Activity>().FirstOrDefault(a => a.Id == actionEvent.EntityIdAdditional01);

                    characteristics.Activity = new ActivityId
                    {
                        Key = activity != null ? activity.LocalisationKey : null,
                        Name = activity != null ? activity.Name : null
                    };
                }

                var so = (from po in Repositories.Core.PersonOfficeMappers
                          where po.PersonId == personId
                          select po).OrderByDescending(x => x.CreatedOn).FirstOrDefault();

                if (so != null)
                    characteristics.SeekerCareerCenter = so.OfficeId;

                if (actionerId > 0 && actionerId != characteristics.UserId)
                {
                    var staffMember = Repositories.Core.FindById<User>(actionerId);

                    var sto = (from pom in Repositories.Core.PersonsCurrentOffices
                               where pom.PersonId == staffMember.PersonId
                               select pom).OrderByDescending(x => x.StartTime).FirstOrDefault();

                    if (sto != null)
                        characteristics.StaffCareerCenter = sto.OfficeId;
                    characteristics.StaffExternalId = staffMember.ExternalId;
                }

                return JsonConvert.SerializeObject(characteristics, Formatting.Indented, new JsonSerializerSettings { PreserveReferencesHandling = PreserveReferencesHandling.None });
            }
        }

        private EntityTypes? GetEntityType(ActionTypes actionType, string entityTypeName)
        {
            // TODO - Remove hard-coding, if possible, and use EntityTypeId from ActionEvent table instead
            switch (actionType)
            {
                // RESUME 
                case ActionTypes.SaveResume:
                case ActionTypes.CreateNewResume:
                case ActionTypes.ChangeResume:
                case ActionTypes.CompleteResume:
                case ActionTypes.CompleteNonDefaultResume:
                case ActionTypes.DeleteResume:
                    return EntityTypes.Resume;

                // APPLICATION
                /*
				case ActionTypes.SelfReferral:
				case ActionTypes.CreateCandidateApplication:
				case ActionTypes.UpdateApplicationStatusToHired:
				case ActionTypes.UpdateApplicationStatusToNotHired:
				case ActionTypes.UpdateApplicationStatusToDidNotApply:
				case ActionTypes.UpdateApplicationStatusToFailedToRespondToInvitation:
				case ActionTypes.UpdateApplicationStatusToFailedToReportToJob:
				case ActionTypes.UpdateApplicationStatusToFoundJobFromOtherSource:
				case ActionTypes.UpdateApplicationStatusToInterviewDenied:
				case ActionTypes.UpdateApplicationStatusToInterviewScheduled:
				case ActionTypes.UpdateApplicationStatusToJobAlreadyFilled:
				case ActionTypes.UpdateApplicationStatusToNotQualified:
				case ActionTypes.UpdateApplicationStatusToRecommended:
				case ActionTypes.UpdateApplicationStatusToRefusedOffer:
				case ActionTypes.UpdateApplicationStatusToRefusedReferral:
				case ActionTypes.UpdateApplicationStatusToUnderConsideration:
				case ActionTypes.ApproveCandidateReferral:
				case ActionTypes.ReferralRequest:
				case ActionTypes.AutoApprovedReferralBypass:
					return EntityTypes.Application;
				*/

                //APPLICATION
                case ActionTypes.UpdateApplicationStatusToFailedToRespondToInvitation:
                    return EntityTypes.Application;

                // USER
                case ActionTypes.LogIn:
                case ActionTypes.CreateSingleSignOn:
                case ActionTypes.RegisterAccount:
                    return EntityTypes.User;

                // PERSON
                case ActionTypes.LensJobSearch:
                case ActionTypes.FindJobsForSeeker:
                case ActionTypes.ChangeJobSeekerSsn:
                case ActionTypes.AssignActivityToCandidate:
                case ActionTypes.ActivateAccount:
                case ActionTypes.InactivateAccount:
                case ActionTypes.BlockUser:
                case ActionTypes.UnblockUser:
                case ActionTypes.ViewJobDetails:
                case ActionTypes.InviteJobSeekerToApply:
                    return EntityTypes.JobSeeker;

                //POSTING
                case ActionTypes.ViewedPostingFromSearch:
                case ActionTypes.ExternalReferral:
                case ActionTypes.ExternalStaffReferral:
                    return EntityTypes.Posting;
            }

            return entityTypeName.AsEnum<EntityTypes>();
        }

        #endregion
    }
}