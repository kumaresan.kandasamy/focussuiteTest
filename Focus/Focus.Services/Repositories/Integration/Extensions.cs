﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;

using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Models.Career;
using Focus.Core.Models.Integration;
using Focus.Data.Core.Entities;
using Focus.Data.Library.Entities;
using Focus.Services.DtoMappers;
using Focus.Services.Mappers;
using Name = Focus.Core.Models.Career.Name;

using Framework.Core;

#endregion

namespace Focus.Services.Repositories.Integration
{
  public static class Extensions
  {
    public static JobModel ToIntegrationJob(this Data.Core.Entities.Job job, IRuntimeContext runtimeContext)
    {
      if (job.JobAddresses.IsNullOrEmpty()) throw new Exception("Job Address is required.");

      var jobAddresses = new List<AddressModel>{job.JobAddresses[0].ToIntegrationAddress(runtimeContext)};
      var jobLocations = job.JobLocations;
      if (jobLocations.IsNotNullOrEmpty())
      {
        jobAddresses.AddRange(jobLocations.Select(jobLocation => jobLocation.ToIntegrationAddress(runtimeContext)));
      }

      var integrationJob = new JobModel
      {
        // Identifiers
        Id = job.Id,
        ExternalId = (job.ExternalId.IsNotNull()) ? job.ExternalId : "",
        // Basic information
        Title = (job.JobTitle.IsNotNull()) ? job.JobTitle : "",
        BusinessUnitName = job.BusinessUnit.Name,
        Description = (job.Description.IsNotNull()) ? job.Description : "",
        Addresses = jobAddresses,
        JobStatus = job.JobStatus,
        Openings = job.NumberOfOpenings.GetValueOrDefault(),
        InterviewContactPreferences = job.InterviewContactPreferences ?? ContactMethods.None,
        ContactUrl = job.InterviewApplicationUrl,
        // Dates
        PostingDate = job.PostedOn,
        ClosingDate = job.ClosingOn.GetValueOrDefault(),
        CreatedDate = job.CreatedOn,
        UpdateDate = job.UpdatedOn,
        // Salary
        MinSalary = (job.MinSalary.HasValue) ? job.MinSalary : null,
        MaxSalary = (job.MaxSalary.HasValue) ? job.MaxSalary : null,
        SalaryUnit = runtimeContext.Helpers.Lookup.GetLookupText(LookupTypes.Frequencies, job.SalaryFrequencyId.GetValueOrDefault()),
        SalaryUnitId = job.SalaryFrequencyId,
				EmploymentStatusId = job.EmploymentStatusId.GetValueOrDefault(),
				JobTypeId = job.JobTypeId.GetValueOrDefault(),
        // Shift
        WorkDaysVary = job.WorkDaysVary,
        Weekdays = job.NormalWorkDays ?? DaysOfWeek.None,
        HoursPerWeek = job.HoursPerWeek,
				PublicTransportAccessible = (job.JobLocations != null ? job.JobLocations.Any(jl => jl.IsPublicTransitAccessible) : false),
				CourtOrderedAffirmativeAction = job.CourtOrderedAffirmativeAction,
				Shift = runtimeContext.Helpers.Lookup.GetLookupExternalId(LookupTypes.WorkShifts, job.NormalWorkShiftsId.GetValueOrDefault()),
        //Experience
        MinimumExperienceYears = job.MinimumExperience,
        MinimumExperienceMonths = job.MinimumExperienceMonths,
        //Education
        EducationLevel = job.MinimumEducationLevel,
        //Driving
        DrivingLicenceClassId = job.DrivingLicenceClassId,
        DrivingLicenceEndorsements = job.JobDrivingLicenceEndorsements.Select(x => x.DrivingLicenceEndorsementId).ToList(),
        //Benefits
        InsuranceBenefits = job.InsuranceBenefits ?? InsuranceBenefits.None,
        LeaveBenefits = job.LeaveBenefits ?? LeaveBenefits.None,
        RetirementBenefits = job.RetirementBenefits ?? RetirementBenefits.None,
        MiscellaneousBenefits = job.MiscellaneousBenefits ?? MiscellaneousBenefits.None,
				OriginId = 7
      };

			// Map values to duration id if possible
			if (integrationJob.DurationId.IsNullOrZero())
			{
				var employmentStatus = runtimeContext.Helpers.Lookup.GetLookupById(LookupTypes.EmploymentStatuses, job.EmploymentStatusId.GetValueOrDefault(0));
				var employmentStatusKey = (employmentStatus.IsNull() || employmentStatus.Key == "EmploymentStatuses.Fulltime") ? "FullTime" : "PartTime";

				var jobtype = runtimeContext.Helpers.Lookup.GetLookupById(LookupTypes.JobTypes, job.JobTypeId.GetValueOrDefault(0));
				var jobTypeKey = jobtype.IsNull() ? "Regular" : jobtype.Key.SubstringAfter(".");
				switch (jobTypeKey)
				{
					case "Permanent":
						jobTypeKey = "Regular";
						break;
					case "Interim":
						jobTypeKey = "ShortTerm";
						break;
				}

				var durationKey = string.Concat("Duration.", employmentStatusKey, jobTypeKey);
				integrationJob.DurationId = runtimeContext.Helpers.Lookup.GetLookup(LookupTypes.Durations, 0, durationKey).Select(l => l.Id).FirstOrDefault();

				// Fix for mis-spelt key in lookup table
				if (integrationJob.DurationId.IsNullOrZero() && durationKey == "Duration.FullTimeShortTerm")
				{
					durationKey = "Duration.FullTimeShorTerm";
					integrationJob.DurationId = runtimeContext.Helpers.Lookup.GetLookup(LookupTypes.Durations, 0, durationKey).Select(l => l.Id).FirstOrDefault();
				}
				if (integrationJob.DurationId.IsNullOrZero())
				{
					durationKey = "Duration.FullTimeRegular";
					integrationJob.DurationId = runtimeContext.Helpers.Lookup.GetLookup(LookupTypes.Durations, 0, durationKey).Select(l => l.Id).FirstOrDefault();
				}
			}

      if (job.OnetId.IsNotNull())
      {
        var oNet = runtimeContext.Repositories.Library.FindById<Onet>(job.OnetId);
        if (oNet.IsNotNull())
        {
          integrationJob.OnetCode = oNet.OnetCode;
          var oNet12Mapping = runtimeContext.Repositories.Library.Onet17Onet12MappingViews.FirstOrDefault(x => x.Onet12Code == oNet.OnetCode);
          if (oNet12Mapping.IsNotNull())
            integrationJob.Onet17Code = oNet12Mapping.Onet17Code;
          else
            integrationJob.Onet17Code = oNet.OnetCode;
        }
      }

      if (job.ROnetId.IsNotNull())
      {
        var rOnet = runtimeContext.Repositories.Library.FindById<ROnet>(job.ROnetId);
        if (rOnet.IsNotNull())
          integrationJob.BGTOcc = rOnet.Code;
      }

      var certificates = runtimeContext.Repositories.Core.JobCertificates.Where(x => x.JobId == job.Id).ToList();
      if (certificates.IsNotNullOrEmpty())
      {
        integrationJob.Certificates = new List<string>();
        foreach (var certificate in certificates)
        {
          integrationJob.Certificates.Add(certificate.Certificate);
        }
      }

      var licences = runtimeContext.Repositories.Core.JobLicences.Where(x => x.JobId == job.Id).ToList();
      if (licences.IsNotNullOrEmpty())
      {
        integrationJob.Licences = new List<string>();
        foreach (var licence in licences)
        {
          integrationJob.Licences.Add(licence.Licence);
        }
      }
      var posting = job.Posting;
      if (posting.IsNotNull()) integrationJob.LensId = posting.LensPostingId;

      // Hiring Manager
      if (job.EmployeeId.HasValue)
      {
        var employee = runtimeContext.Repositories.Core.Employees.SingleOrDefault(x => x.Id == job.EmployeeId);
        var person = employee.Person;
        var user = person.User;
        var manager = new HiringManager { Email = person.EmailAddress, FirstName = person.FirstName, Surname = person.LastName, ExternalId = user.ExternalId, UserId = user.Id, PersonId = person.Id};
        var businessUnit = runtimeContext.Repositories.Core.BusinessUnits.SingleOrDefault(x => x.Id == job.BusinessUnitId);

        if (businessUnit.IsNotNull())
        {
          manager.PhoneNumber = businessUnit.PrimaryPhone;
          manager.Extension = businessUnit.PrimaryPhoneExtension;

          var buAddress = businessUnit.BusinessUnitAddresses.SingleOrDefault(x => x.IsPrimary);
          if (buAddress.IsNotNull())
          {
            manager.Address = new AddressModel
            {
              AddressLine1 = buAddress.Line1,
              AddressLine2 = buAddress.Line2,
              AddressLine3 = buAddress.Line3,
              City = buAddress.TownCity,
              PostCode = buAddress.PostcodeZip,
              StateId = buAddress.StateId,
              State = runtimeContext.Helpers.Lookup.GetLookupText(LookupTypes.States, buAddress.StateId),
              CountryId = buAddress.CountryId,
              Country = runtimeContext.Helpers.Lookup.GetLookupText(LookupTypes.Countries, buAddress.CountryId)
            };
          }
        }

        integrationJob.HiringManager = manager;
      }

      return integrationJob;
    }

		public static JobSeekerModel ToIntegrationJobSeeker(this Person person, IRuntimeContext context)
		{
			var user = person.User;

			var model = new JobSeekerModel
			{
				ExternalId = user.ExternalId,
				LastName = person.LastName,
				FirstName = person.FirstName,
				MiddleInitial = person.MiddleInitial,
				Username = user.UserName,
				Password = user.PasswordHash,
				EmailAddress = person.EmailAddress,
				SocialSecurityNumber = person.SocialSecurityNumber,
				DateOfBirth = person.DateOfBirth
			};

			if (person.AssignedToId.IsNotNull())
				model.AdminId = context.Repositories.Core.Users.Where(u => u.PersonId == person.AssignedToId).Select(u => u.ExternalId).FirstOrDefault();

			#region Get / create the Resume model

			var resume = context.Repositories.Core.Resumes.FirstOrDefault(r => r.PersonId == person.Id && r.IsDefault);
			if (resume.IsNotNull())
				resume = context.Repositories.Core.Resumes.Where(r => r.PersonId == person.Id).OrderByDescending(x => x.UpdatedOn).FirstOrDefault();

			if (resume.IsNotNull())
			{
				var resumeDto = resume.AsDto();
				model.Resume = resumeDto.ToResumeModel(context, context.CurrentRequest.UserContext.Culture);
				model.ResumeXml = resumeDto.ResumeXml;
			}

			if (model.Resume == null)
			{
				model.Resume = new ResumeModel
				{
					ResumeContent = new ResumeBody(),
					ResumeMetaInfo = new ResumeEnvelope { CompletionStatus = ResumeCompletionStatuses.None }
				};
			}

			// Create this here just in case we don't have a mapped Seeker Contact details
			if (model.Resume.ResumeContent.SeekerContactDetails == null)
			{
				model.Resume.ResumeContent.SeekerContactDetails = new Contact
				{
					SeekerName = new Name
					{
						FirstName = person.FirstName,
						MiddleName = person.MiddleInitial,
						LastName = person.LastName
					},
					EmailAddress = person.EmailAddress,
				};
			}

			// Create this here just in case we don't have a mapped Phone Number details
			if (model.Resume.ResumeContent.SeekerContactDetails.PhoneNumber == null)
				model.Resume.ResumeContent.SeekerContactDetails.PhoneNumber = new List<Phone>();

			#endregion

			#region Get the Address

			var address = person.PersonAddresses.FirstOrDefault(add => add.IsPrimary);
			if (address.IsNotNull())
			{
				model.AddressLine1 = address.Line1;
				model.AddressLine2 = address.Line2;
				model.AddressTownCity = address.TownCity;
				model.AddressPostcodeZip = address.PostcodeZip;
				model.AddressCountyId = address.CountyId;
				model.AddressStateId = address.StateId;
				model.AddressCountryId = address.CountryId;

				if (model.Resume.ResumeContent.SeekerContactDetails.PostalAddress == null)
				{
					model.Resume.ResumeContent.SeekerContactDetails.PostalAddress = new Address()
					{
						Street1 = address.Line1,
						Street2 = address.Line2,
						City = address.TownCity,
						CountyId = address.CountyId,
						StateId = address.StateId,
						CountryId = address.CountryId,
						Zip = address.PostcodeZip
					};
				}
			}

			// Create this here just in case we don't have a mapped Postal Address details
			if (model.Resume.ResumeContent.SeekerContactDetails.PostalAddress == null)
				model.Resume.ResumeContent.SeekerContactDetails.PostalAddress = new Address();

			#endregion

			#region Get the primary phone number

			var phone = person.PhoneNumbers.FirstOrDefault(pn => pn.IsPrimary);
			if (phone.IsNotNull())
			{
				model.PrimaryPhone = phone.Number;

				if (model.Resume.ResumeContent.SeekerContactDetails.PhoneNumber.Count == 0)
					model.Resume.ResumeContent.SeekerContactDetails.PhoneNumber.Add(new Phone {PhoneNumber = phone.Number});
			}

			#endregion

			return model;
		}

    public static EmployerModel ToIntegrationEmployerModel(this Data.Core.Entities.Employer employer, IRuntimeContext context)
    {
      var businessUnit = employer.BusinessUnits.FirstOrDefault(bu => bu.IsPreferred);
      if (businessUnit.IsNull())
        businessUnit = employer.BusinessUnits.FirstOrDefault();

      if (businessUnit.IsNull())
        throw new Exception(string.Format("Business Unit not found for employer id {0}", employer.Id));

      var model = new EmployerModel
                    {
                      Id = employer.Id,
                      ExternalId = employer.ExternalId,
                      Name = businessUnit.Name,
                      LegalName = businessUnit.LegalName.IsNullOrEmpty() ? employer.LegalName : businessUnit.LegalName,
                      FederalEmployerIdentificationNumber = employer.FederalEmployerIdentificationNumber,
                      IsValidFederalEmployerIdentificationNumber = employer.IsValidFederalEmployerIdentificationNumber,
                      StateEmployerIdentificationNumber = employer.StateEmployerIdentificationNumber,
                      IsRegistrationComplete = employer.ApprovalStatus == ApprovalStatuses.Approved
                    };

      if (businessUnit.PrimaryPhoneType != PhoneTypes.Fax.ToString())
      {
        model.Phone = businessUnit.PrimaryPhone;
        model.PhoneExt = businessUnit.PrimaryPhoneExtension;
        if (businessUnit.AlternatePhone1Type != PhoneTypes.Fax.ToString())
          model.AltPhone = businessUnit.AlternatePhone1;
        else if (businessUnit.AlternatePhone2Type != PhoneTypes.Fax.ToString())
          model.AltPhone = businessUnit.AlternatePhone2;
      }
      else if (businessUnit.AlternatePhone1Type != PhoneTypes.Fax.ToString())
      {
        model.Phone = businessUnit.AlternatePhone1;
        if (businessUnit.AlternatePhone2Type != PhoneTypes.Fax.ToString())
          model.AltPhone = businessUnit.AlternatePhone2;
      }
      else if (businessUnit.AlternatePhone2Type != PhoneTypes.Fax.ToString())
      {
        model.Phone = businessUnit.AlternatePhone2;
      }

      if (businessUnit.PrimaryPhoneType == PhoneTypes.Fax.ToString())
      {
        model.Fax = businessUnit.PrimaryPhone;
      }
      else if (businessUnit.AlternatePhone1Type == PhoneTypes.Fax.ToString())
      {
        model.Fax = businessUnit.AlternatePhone1;
      }
      else if (businessUnit.AlternatePhone2Type == PhoneTypes.Fax.ToString())
      {
        model.Fax = businessUnit.AlternatePhone2;
      }

      model.Owner = businessUnit.OwnershipTypeId;
      model.AccountType = businessUnit.AccountTypeId;
      model.Naics = businessUnit.IndustrialClassification;
      model.Url = businessUnit.Url;
      model.Address = businessUnit.BusinessUnitAddresses.SingleOrDefault(x => x.IsPrimary).AsDto().ToIntegrationAddress(context);

      var employees = employer.Employees;
      if (employees.IsNotNullOrEmpty())
      {
        foreach (var employee in employer.Employees)
        {
          model.Employees.Add(employee.ToIntegrationEmployee(context));
        }
      }
      var offices = employer.EmployerOfficeMappers.ToList();
      if (offices.IsNotNullOrEmpty())
      {
        var office = offices[0].Office;
        model.OfficeId = office.Id;
        model.ExternalOfficeId = office.ExternalId;
      }

      return model;
    }

    public static EmployeeModel ToIntegrationEmployee(this Employee employee, IRuntimeContext runtimeContext)
    {
      var person = employee.Person;
      var staffUser = runtimeContext.Repositories.Core.Users.SingleOrDefault(x => x.Id == employee.StaffUserId);
      var phoneNumbers = person.PhoneNumbers.ToList();
      var phones = phoneNumbers.Where(x => x.PhoneType == PhoneTypes.Mobile || x.PhoneType == PhoneTypes.Phone).ToList();

      var model = new EmployeeModel
                    {
                      Id = employee.Id,
                      ExternalId = person.User.ExternalId,
                      ExternalEmployerId = employee.Employer.ExternalId,
                      EmployerId = employee.EmployerId,
                      AdminId = employee.StaffUserId,
                      ExternalAdminId = staffUser.IsNotNull() ? staffUser.ExternalId : null,
                      FirstName = person.FirstName,
                      LastName = person.LastName,
                      SalutationId = person.TitleId,
                      JobTitle = person.JobTitle,
                      Email = person.EmailAddress,
                      Address = person.PersonAddresses.FirstOrDefault(x => x.IsPrimary).ToIntegrationAddress(runtimeContext)
                    };

      

      if (phoneNumbers.Any(x => x.PhoneType == PhoneTypes.Fax))
        model.Fax = phoneNumbers.Single(x => x.PhoneType == PhoneTypes.Fax).Number;

      if (phones.IsNotNullOrEmpty())
      {
        if (phones.Any(x => x.IsPrimary))
        {
          model.Phone = phones.First(x => x.IsPrimary).Number;
          model.PhoneExt = phones.First(x => x.IsPrimary).Extension;
          if (phones.Any(x => !x.IsPrimary))
          {
            model.AltPhone = phones.First(x => x.IsPrimary).Number;
            model.AltPhoneExt = phones.First(x => x.IsPrimary).Extension;
          }
        }
        else
        {
          model.Phone = phones[0].Number;
          model.PhoneExt = phones[0].Extension;
          if (phones.Count > 1)
          {
            model.AltPhone = phones[1].Number;
            model.AltPhone = phones[1].Extension;
          }
        }
      }



      return model;
    }


    /// <summary>
    /// Toes the integration address.
    /// </summary>
    /// <param name="personAddress">The job address.</param>
    /// <param name="runtimeContext">The runtime context.</param>
    /// <returns></returns>
    /// <exception cref="System.Exception">Job Address is required.</exception>
    private static AddressModel ToIntegrationAddress(this PersonAddress personAddress, IRuntimeContext runtimeContext)
    {
      if (personAddress.IsNull()) throw new Exception("Person Address is required.");
      var address = new AddressModel
      {
        AddressLine1 = personAddress.Line1,
        AddressLine2 = personAddress.Line2,
        AddressLine3 = personAddress.Line3,
        City = personAddress.TownCity,
        CountyId = (personAddress.CountyId.HasValue) ? personAddress.CountyId.Value : 0,
        CountryId = personAddress.CountryId,
        StateId = personAddress.StateId,
        State = runtimeContext.Helpers.Lookup.GetLookupText(LookupTypes.States, personAddress.StateId),
        Country = runtimeContext.Helpers.Lookup.GetLookupText(LookupTypes.Countries, personAddress.CountryId),
        PostCode = personAddress.PostcodeZip,
        FromJobAddress = true
      };

      if (personAddress.PostcodeZip.IsNotNull())
      {
        var zip = runtimeContext.Repositories.Library.Find<PostalCode>(x => x.Code == personAddress.PostcodeZip).SingleOrDefault();
        if (zip.IsNotNull())
        {
          address.Longitude = zip.Longitude;
          address.Latitude = zip.Latitude;
        }
      }

      return address;
    }


    /// <summary>
    /// Toes the integration address.
    /// </summary>
    /// <param name="jobAddress">The job address.</param>
    /// <param name="runtimeContext">The runtime context.</param>
    /// <returns></returns>
    /// <exception cref="System.Exception">Job Address is required.</exception>
    private static AddressModel ToIntegrationAddress(this JobAddress jobAddress, IRuntimeContext runtimeContext)
    {
      if (jobAddress.IsNull()) throw new Exception("Job Address is required.");
      var address = new AddressModel
      {
        AddressLine1 = jobAddress.Line1,
        AddressLine2 = jobAddress.Line2,
        AddressLine3 = jobAddress.Line3,
        City = jobAddress.TownCity,
        CountyId = (jobAddress.CountyId.HasValue) ? jobAddress.CountyId.Value : 0,
        CountryId = jobAddress.CountryId,
        StateId = jobAddress.StateId,
        State = runtimeContext.Helpers.Lookup.GetLookupText(LookupTypes.States, jobAddress.StateId),
        Country = runtimeContext.Helpers.Lookup.GetLookupText(LookupTypes.Countries, jobAddress.CountryId),
        PostCode = jobAddress.PostcodeZip,
        FromJobAddress = true
      };

      if (jobAddress.PostcodeZip.IsNotNull())
      {
        var zip = runtimeContext.Repositories.Library.Find<PostalCode>(x => x.Code == jobAddress.PostcodeZip).SingleOrDefault();
        if (zip.IsNotNull())
        {
          address.Longitude = zip.Longitude;
          address.Latitude = zip.Latitude;
        }
      }

      return address;
    }


    private static AddressModel ToIntegrationAddress(this JobLocation jobLocation, IRuntimeContext runtimeContext)
    {
      if (jobLocation.IsNull()) throw new Exception("job location is required");

      var compositeAddress = jobLocation.Location;

	    var postcode = jobLocation.Zip.TruncateString(5);
	    var state = jobLocation.State;
	    var city = jobLocation.City;

	    if (postcode.IsNullOrEmpty())
	    {
		    var split = compositeAddress.Split(Convert.ToChar("("), Convert.ToChar(")"));
		    // The below if probably overly fussy but just incase we end up with ",", "(" or ")" characters in the city string we need to do it this way
		    postcode = split[split.Length - 2];
	    }

			if (state.IsNullOrEmpty())
				state = compositeAddress.Substring(compositeAddress.LastIndexOf(",", StringComparison.Ordinal) + 1, compositeAddress.LastIndexOf("(") - compositeAddress.LastIndexOf(",", StringComparison.Ordinal) - 1).Trim();

			if (city.IsNullOrEmpty())
				city = compositeAddress.Contains(",") ? compositeAddress.Substring(0, compositeAddress.LastIndexOf(",", StringComparison.Ordinal)).Trim() : string.Empty;

      var address = new AddressModel
                      {
                        PostCode = postcode,
                        State = state,
                        Country = "US",
                        City = city,
                        FromJobAddress = false,
                        AddressLine1 = jobLocation.AddressLine1
                      };
      if (postcode.IsNotNullOrEmpty())
      {
        var zip = runtimeContext.Repositories.Library.Find<PostalCode>(x => x.Code == postcode).SingleOrDefault();
        if (zip.IsNotNull())
        {
          address.Longitude = zip.Longitude;
          address.Latitude = zip.Latitude;
          var stateItems = runtimeContext.Helpers.Lookup.GetLookup(LookupTypes.States, 0, zip.StateKey, null);
          if (stateItems.IsNotNullOrEmpty())
          {
            var stateItem = stateItems.First();
            address.State = stateItem.Text;
            address.StateId = stateItem.Id;
          }

          var countyItems = runtimeContext.Helpers.Lookup.GetLookup(LookupTypes.Counties, address.StateId, zip.CountyKey, null);
          if (countyItems.IsNotNullOrEmpty())
          {
            var countyItem = countyItems.First();
            address.CountyId = countyItem.Id;
          }

          var countryItems = runtimeContext.Helpers.Lookup.GetLookup(LookupTypes.Countries, 0, zip.CountryKey, null);
          if (countryItems.IsNotNullOrEmpty())
          {
            var countryItem = countryItems[0];
            address.Country = countryItem.Text;
            address.CountryId = countryItem.Id;
          }
        }

        var lookup = runtimeContext.Repositories.Configuration.ExternalLookUpItems
                                                              .FirstOrDefault(e => e.ExternalLookUpType == ExternalLookUpType.IntegrationOfficeIdPerZip && e.InternalId == postcode);
        if (lookup.IsNotNull())
        {
          address.OfficeExternalId = lookup.ExternalId;
        }
        else
        {
          var office = runtimeContext.Repositories.Core.Offices
                                                       .FirstOrDefault(o => o.InActive == false && o.ExternalId != null && o.AssignedPostcodeZip.Contains(postcode));
          if (office != null)
            address.OfficeExternalId = office.ExternalId;
        }
      }

      return address;
    }

    /// <summary>
    /// Toes the integration address.
    /// </summary>
    /// <param name="employerAddress">The job address.</param>
    /// <param name="runtimeContext">The runtime context.</param>
    /// <returns></returns>
    /// <exception cref="System.Exception">Job Address is required.</exception>
    private static AddressModel ToIntegrationAddress(this BusinessUnitAddressDto employerAddress, IRuntimeContext runtimeContext)
    {
      if (employerAddress.IsNull()) throw new Exception("Job Address is required.");
      var address = new AddressModel
      {
        AddressLine1 = employerAddress.Line1,
        AddressLine2 = employerAddress.Line2,
        AddressLine3 = employerAddress.Line3,
        City = employerAddress.TownCity,
        CountyId = (employerAddress.CountyId.HasValue) ? employerAddress.CountyId.Value : 0,
        CountryId = employerAddress.CountryId,
        StateId = employerAddress.StateId,
        State = runtimeContext.Helpers.Lookup.GetLookupText(LookupTypes.States, employerAddress.StateId),
        Country = runtimeContext.Helpers.Lookup.GetLookupText(LookupTypes.Countries, employerAddress.CountryId),
        PostCode = employerAddress.PostcodeZip,
        FromJobAddress = true
      };

      if (employerAddress.PostcodeZip.IsNotNull())
      {
        var zip = runtimeContext.Repositories.Library.Find<PostalCode>(x => x.Code == employerAddress.PostcodeZip).SingleOrDefault();
        if (zip.IsNotNull())
        {
          address.Longitude = zip.Longitude;
          address.Latitude = zip.Latitude;
        }
      }

      return address;
    }
  }
}
