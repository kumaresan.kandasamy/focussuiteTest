﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using Focus.Core;
using Focus.Core.Criteria.JobDistribution;
using Focus.Core.Criteria.SpideredEmployer;
using Focus.Core.Criteria.SpideredPosting;
using Focus.Core.Models.Assist;

#endregion

namespace Focus.Services.Repositories
{
	public interface ILiveJobsRepository
	{
		/// <summary>
		/// Gets the employers.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		PagedList<SpideredEmployerModel> GetEmployers(SpideredEmployerCriteria criteria);

		/// <summary>
		/// Gets the postings.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		PagedList<SpideredPostingModel> GetPostings(SpideredPostingCriteria criteria);

		/// <summary>
		/// Gets the total active postings.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		List<JobDistributionModel> GetJobDistribution(JobDistributionCriteria criteria);
	}
}
