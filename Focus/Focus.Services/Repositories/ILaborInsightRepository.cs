﻿using System;
using System.Collections.Generic;
using Focus.Core;
using Focus.Core.Views;
using Focus.Services.Repositories.LaborInsight;
using Focus.Services.Repositories.LaborInsight.Criteria;

namespace Focus.Services.Repositories
{
    public interface ILaborInsightRepository
    {
      LaborInsightJobReport GetJobReport(JobReportCriteria criteria);
      void SetCachedSupplyDemand(PagedList<SupplyDemandReportView> supplyDemand);
      PagedList<SupplyDemandReportView> GetCachedSupplyDemand(JobReportBaseCriteria criteria);
    }
}