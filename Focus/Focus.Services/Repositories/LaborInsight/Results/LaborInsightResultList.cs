﻿#region Copyright © 2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion

namespace Focus.Services.Repositories.LaborInsight
{
	public abstract class LaborInsightResultList : LaborInsightResult
	{
		public int Offset { get; set; }
		public int Limit { get; set; }
		public int TotalCount { get; set; }
		public int Count { get; set; }
		public int TotalClassified { get; set; }
		public int TotalUnclassified { get; set; }
	}
}
