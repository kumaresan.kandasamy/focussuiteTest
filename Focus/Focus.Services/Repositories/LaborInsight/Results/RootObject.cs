﻿#region Copyright © 2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

#endregion

namespace Focus.Services.Repositories.LaborInsight
{
	public class RootObject
	{
		public Result result { get; set; }
		public string requestId { get; set; }
		public int timestamp { get; set; }
		public string status { get; set; }
		public string statusCode { get; set; }
	}

	public class Datum
	{
		public string group { get; set; }
		public int count { get; set; }
	}

	public class ReportData
	{
		public List<Datum> data { get; set; }
		public int offset { get; set; }
		public int limit { get; set; }
		public int totalCount { get; set; }
		public int count { get; set; }
	}

	public class Data
	{
		public ReportData reportData { get; set; }
		public int totalClassifiedJobs { get; set; }
		public int totalUnclassifiedJobs { get; set; }
	}

	public class Result
	{
		public int count { get; set; }
		public Data data { get; set; }
	}
}
