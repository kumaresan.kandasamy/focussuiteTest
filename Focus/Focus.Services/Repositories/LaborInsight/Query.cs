﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Services.Repositories.LaborInsight.Criteria;

namespace Focus.Services.Repositories.LaborInsight
{
  public class Query
  {
    private string _queryText;
    private readonly List<LaborInsightMappingViewDto> _mappings;
    private readonly JobReportCriteria _criteria;
    private const string QuerystringFormat = "[{0}]: \\\"{1}\\\"";

    /// <summary>
    /// Gets the text.
    /// </summary>
    public string Text
    {
      get
      {
        if (String.IsNullOrEmpty(_queryText))
          _queryText = BuildQuery();

        return _queryText;
      }
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="Query"/> class.
    /// </summary>
    public Query(List<LaborInsightMappingViewDto> mappings, JobReportCriteria criteria)
    {
      _mappings = mappings;
      _criteria = criteria;
    }

    /// <summary>
    /// Builds the query.
    /// </summary>
    /// <returns></returns>
    /// <exception cref="System.Exception">cannot build query with no parameters</exception>
    private string BuildQuery()
    {
      if (!_criteria.QueryParameters.Any())
        throw new Exception("cannot build query with no parameters");

      var query = String.Format("{{{0},{1},{2} {4},{5},{6}}}",
                                BuildGroupByCriteria(),
                                BuildTimePeriodCriteria(),
                                BuildQuerystringCriteria(),
                                BuildGeographyCriteria(),
                                BuildIncludeTotalsCriteria(),
                                BuildOffSetCriteria(),
                                BuildPageSizeCriteria()
        );

      return query;
    }

    #region BuildCriteriaMethods

    /// <summary>
    /// Builds the time period criteria.
    /// </summary>
    /// <returns></returns>
		private string BuildTimePeriodCriteria()
    {
      return "\"timePeriod\":{\"from\":\"" + _criteria.Criteria.FromDate.ToString("yyyy-MM-ddThh:mm:ss") + "\",\"to\":\"" +
             _criteria.Criteria.ToDate.ToString("yyyy-MM-ddThh:mm:ss") + "\"}";
    }

    /// <summary>
    /// Builds the group by criteria.
    /// </summary>
    /// <returns></returns>
    private string BuildGroupByCriteria()
		{
			return "\"groupBy\":\"" + _criteria.Criteria.GroupBy + "\"";
		}

    /// <summary>
    /// Builds the off set criteria.
    /// </summary>
    /// <returns></returns>
		private string BuildOffSetCriteria()
		{
      return "\"offset\":" + _criteria.Criteria.OffSet;
		}

    /// <summary>
    /// Builds the page size criteria.
    /// </summary>
    /// <returns></returns>
		private string BuildPageSizeCriteria()
		{
      return "\"limit\":" + _criteria.Criteria.Limit;
		}

    /// <summary>
    /// Builds the geography criteria.
    /// </summary>
    /// <returns></returns>
		private string BuildGeographyCriteria()
		{
      return "\"geography\":\"" + _criteria.Criteria.Geography + "\"";
		}

    /// <summary>
    /// Builds the include totals criteria.
    /// </summary>
    /// <returns></returns>
		private string BuildIncludeTotalsCriteria()
    {
      return "\"includeTotalClassifiedPostings\":\"" + _criteria.Criteria.IncludeTotalClassifiedPostings +
             "\",\"includeTotalUnclassifiedPostings\":\"" + _criteria.Criteria.IncludeTotalUnclassifiedPostings + "\"";
    }

    /// <summary>
    /// Builds the querystring criteria.
    /// </summary>
    /// <returns></returns>
    private string BuildQuerystringCriteria()
    {
      var sb = new StringBuilder();
      var parameters = _criteria.QueryParameters;

      for (var i = 0; i < parameters.Count; i++)
      {
        var value = string.Empty;
        switch (parameters[i].Filter)
        {
          case FilterName.Onet:
            if (parameters[i].Value != string.Empty)
              value = _criteria.Criteria.OnetIsFamily ? GetValue(parameters[i].Value) : parameters[i].Value;
            break;
          case FilterName.Msa:
          case FilterName.State:
            value = GetValue(parameters[i].Value);
            break;
          default:
            value = parameters[i].Value;
            break;
        }

        if (value == string.Empty) continue;
        sb.AppendFormat(QuerystringFormat, parameters[i].Filter, value);
        if (i != parameters.Count - 1)
          sb.Append(" AND ");
      }

      return "\"queryString\":\"" + sb + "\",";
    }
    
    #endregion

    #region Helpers

    /// <summary>
    /// Gets the value.
    /// </summary>
    /// <param name="parameterValue">The parameter value.</param>
    /// <returns></returns>
    private string GetValue(string parameterValue)
    {
      var mapping = _mappings.FirstOrDefault(x => x.CodeGroupItemId == Convert.ToInt64(parameterValue));
      return mapping != null ? mapping.LaborInsightId : string.Empty;
    }

    #endregion
  }
}
