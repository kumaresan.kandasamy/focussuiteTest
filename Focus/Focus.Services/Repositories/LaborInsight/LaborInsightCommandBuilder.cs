﻿#region Copyright © 2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Data.Library.Entities;
using Focus.Services.Repositories.LaborInsight.Criteria;

#endregion

namespace Focus.Services.Repositories.LaborInsight
{
	internal class LaborInsightCommandBuilder
	{
		private readonly List<LaborInsightMappingViewDto> _mappings;

		public LaborInsightCommandBuilder(List<LaborInsightMappingViewDto> mappings)
		{
			_mappings = mappings;
		}

		public string BuildJobReportCommand(string oNet, string oNetFamily, DateTime fromDate, DateTime toDate, int stateId, int countyId, int mSAId, IList<QueryStringParams> parameters, int offset = 0, int pageSize = 10)
		{
			var criteria = "{" + BuildGroupByCriteria("State") + "," + BuildTimePeriodCriteria(fromDate, toDate) + "," + BuildQuerystringCriteria(parameters) + " " + BuildGeographyCriteria("US") + "," + BuildIncludeTotalsCriteria(true, true) +","+ BuildOffSetCriteria(offset) + "," + BuildPageSizeCriteria(pageSize) +  "}";
			//Console.WriteLine(criteria);
			return criteria;
		}

		private string BuildTimePeriodCriteria(DateTime fromDate, DateTime toDate)
		{
			return "\"timePeriod\":{\"from\":\"" + fromDate.ToString("yyyy-MM-ddThh:mm:ss") + "\",\"to\":\"" + toDate.ToString("yyyy-MM-ddThh:mm:ss") + "\"}";
		}

		private string BuildGroupByCriteria(string groupBy)
		{
			return "\"groupBy\":\"" + groupBy + "\"";
		}

		private string BuildOffSetCriteria(int offSet)
		{
			return "\"offset\":" + offSet;
		}

		private string BuildPageSizeCriteria(int pageSize)
		{
			return "\"limit\":" + pageSize;
		}

		private string BuildGeographyCriteria(string geography)
		{
			return "\"geography\":\"" + geography + "\"";
		}

		private string BuildIncludeTotalsCriteria(bool includeClassified, bool includeUnclassified)
		{
			return "\"includeTotalClassifiedPostings\":\""+ includeClassified + "\",\"includeTotalUnclassifiedPostings\":\"" + includeUnclassified + "\"";
		}

		private string BuildQuerystringCriteria(IList<QueryStringParams> parameters)
		{
			// TODO: Build query string parameters depending on what may be provided. (Must have traiiling comma as details may not be provided)
			return "\"queryString\":\"[nationwide]: \\\" nationwide  \\\"\",";
		}

	}
}
