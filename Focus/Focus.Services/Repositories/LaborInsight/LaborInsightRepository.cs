﻿#region Copyright © 2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Settings.Interfaces;
using Focus.Core.Settings.OAuth;
using Focus.Core.Views;
using Focus.Services.DtoMappers;
using Focus.Services.Repositories.LaborInsight;
using Focus.Services.Repositories.LaborInsight.Criteria;
using Framework.Api;
using Framework.Caching;
using Framework.Core;
using Framework.ServiceLocation;

#endregion

namespace Focus.Services.Repositories
{
  public class LaborInsightRepository : ILaborInsightRepository
  {
    internal IRuntimeContext RuntimeContext { get; private set; }

    /// <summary>
    /// Gets the settings.
    /// </summary>
    /// <value>The settings.</value>
    private IAppSettings Settings
    {
      get { return RuntimeContext.AppSettings; }
    }

    protected static readonly object SyncObject = new object();

    /// <summary>
    /// Gets the oauth setting.
    /// </summary>
    /// <value>The oauth setting.</value>
    private OAuthSettings OauthSetting
    {
      get
      {
        return Settings.OutgoingOAuthSettings.IsNull()
                 ? null
                 : Settings.OutgoingOAuthSettings.FirstOrDefault(x => x.Client == OAuthClient.LaborInsight);
      }
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="LaborInsightRepository"/> class.
    /// </summary>
    /// <param name="runtimeContext">The runtime context.</param>
    public LaborInsightRepository(IRuntimeContext runtimeContext)
    {
      RuntimeContext = runtimeContext ?? ServiceLocator.Current.Resolve<IRuntimeContext>();
    }

    /// <summary>
    /// Gets the job report.
    /// </summary>
    /// <param name="criteria"></param>
    public LaborInsightJobReport GetJobReport(JobReportCriteria criteria)
    {
      var query = new Query(GetLaborInsightMappings(), criteria);

      var results = GetApiClient("insight/jobs").Post<RootObject>(query.Text);

      if (results.Result.statusCode == HttpStatusCode.OK.ToString())
      {
        var resultSet = new LaborInsightJobReport
                          {
                            Count = results.Result.result.data.reportData.count,
                            Limit = results.Result.result.data.reportData.limit,
                            Offset = results.Result.result.data.reportData.limit,
                            StatusCode = results.Result.statusCode,
                            Status = results.Result.status,
                            TimeStamp = results.Result.timestamp,
                            TotalCount = results.Result.result.data.reportData.totalCount,
                            TotalClassified = results.Result.result.data.totalClassifiedJobs,
                            TotalUnclassified = results.Result.result.data.totalUnclassifiedJobs,
                            JobData = results.Result.result.data.reportData.data.Select(
                              x => new LaborInsightGroupedResult {Group = x.group, Count = x.count}).ToList()
                          };
        return resultSet;
      }

      return null;
    }

    /// <summary>
    /// Gets the cached job report.
    /// </summary>
    /// <param name="criteria">The criteria.</param>
    /// <returns></returns>
    public PagedList<SupplyDemandReportView> GetCachedSupplyDemand(JobReportBaseCriteria criteria)
    {
      const string criteriaKey = Constants.CacheKeys.LaborInsightCriteriaKey;
      const string resultsSetKey = Constants.CacheKeys.LaborInsightResultsSetKey;
      var criteriaCache = Cacher.Get<JobReportBaseCriteria>(criteriaKey);
      var resultsSet = Cacher.Get<PagedList<SupplyDemandReportView>>(resultsSetKey);

      if (criteriaCache.IsNotNull() && resultsSet.IsNotNull())
      {
        if (Compare(criteriaCache, criteria))
          return resultsSet;
      }

      SetCachedCriteria(criteria);
      return null;
    }

    /// <summary>
    /// Gets the cached job report.
    /// </summary>
    /// <param name="supplyDemand"></param>
    /// <returns></returns>
    public void SetCachedSupplyDemand(PagedList<SupplyDemandReportView> supplyDemand)
    {
      lock (SyncObject)
      {
        Cacher.Set(Constants.CacheKeys.LaborInsightResultsSetKey, supplyDemand);
      }
    }

    /// <summary>
    /// Gets the cached job report.
    /// </summary>
    /// <param name="criteria">The criteria.</param>
    /// <returns></returns>
    private static void SetCachedCriteria(JobReportBaseCriteria criteria)
    {
      lock (SyncObject)
      {
        Cacher.Set(Constants.CacheKeys.LaborInsightCriteriaKey, criteria);
      }
    }

    /// <summary>
    /// Gets the Labor/Insight API client.
    /// </summary>
    /// <returns></returns>
    /// <exception cref="Exception">No valid Labor/Insight OAuth settings available</exception>
    private WebApiClient GetApiClient(string method)
    {
      if (OauthSetting.IsNull())
        throw new Exception("No valid Labor/Insight OAuth settings available");

      return new WebApiClient(OauthSetting.EndPoint + method, OauthSetting.ConsumerKey, OauthSetting.ConsumerSecret,
                              OauthSetting.Token, OauthSetting.TokenSecret);
      //return new WebApiClient("http://sandbox.api.burning-glass.com/v202/insight/jobs", "BGT", "086718ED10AC48B685F93535208AE903", "Insight", "32611633362D4A76A3C6038E71BFE90C");
    }

    /// <summary>
    /// Gets the labor insight mappings.
    /// </summary>
    /// <returns></returns>
    public List<LaborInsightMappingViewDto> GetLaborInsightMappings()
    {
      const string mappingKey = Constants.CacheKeys.LaborInsightMappingsKey;
      var mappings = Cacher.Get<List<LaborInsightMappingViewDto>>(mappingKey);

      if (mappings.IsNullOrEmpty())
      {
        lock (SyncObject)
        {
          mappings = RuntimeContext.Repositories.Configuration.LaborInsightMappingsView.Select(x => x.AsDto()).ToList();

          Cacher.Set(mappingKey, mappings);
        }
      }

      return mappings;
    }

    /// <summary>
    /// Compares the specified cache criteria.
    /// </summary>
    /// <param name="cacheCriteria">The cache criteria.</param>
    /// <param name="criteria">The criteria.</param>
    /// <returns></returns>
    public bool Compare(JobReportBaseCriteria cacheCriteria, JobReportBaseCriteria criteria)
    {
      if (cacheCriteria.FromDate != criteria.FromDate)
        return false;
      if (cacheCriteria.Geography != criteria.Geography)
        return false;
      if (cacheCriteria.GroupBy != criteria.GroupBy)
        return false;
      if (cacheCriteria.IncludeTotalClassifiedPostings != criteria.IncludeTotalClassifiedPostings)
        return false;
      if (cacheCriteria.IncludeTotalUnclassifiedPostings != criteria.IncludeTotalUnclassifiedPostings)
        return false;
      if (cacheCriteria.OffSet != criteria.OffSet)
        return false;
      if (cacheCriteria.OnetFamily != criteria.OnetFamily)
        return false;
      if (cacheCriteria.OnetID != criteria.OnetID)
        return false;
      if (cacheCriteria.OnetIsFamily != criteria.OnetIsFamily)
        return false;
      if (cacheCriteria.State != criteria.State)
        return false;
      if (cacheCriteria.MSA != criteria.MSA)
        return false;
      return cacheCriteria.Limit == criteria.Limit;
    }

  }
}
