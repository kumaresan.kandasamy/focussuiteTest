﻿#region Copyright © 2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Focus.Core;

#endregion

namespace Focus.Services.Repositories.LaborInsight.Criteria
{
  public class QueryStringParams
  {
    public string Filter { get; set; }
    public string Value { get; set; }
  }
}