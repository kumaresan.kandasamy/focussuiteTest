﻿#region Copyright © 2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;

#endregion



namespace Focus.Services.Repositories.LaborInsight.Criteria
{
  public class JobReportBaseCriteria
  {
    public bool IncludeTotalClassifiedPostings { get; set; }
    public bool IncludeTotalUnclassifiedPostings { get; set; }
    public DateTime FromDate { get; set; }
    public DateTime ToDate { get; set; }
    public string OnetID { get; set; }
    public long OnetFamily { get; set; }
    public long State { get; set; }
    public long MSA { get; set; }
    public int OffSet { get; set; }
    public int Limit { get; set; }
    public string GroupBy { get; set; }
    public string Geography { get; set; }
    public bool OnetIsFamily { get; set; }
  }
}
