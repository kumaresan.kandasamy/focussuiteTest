﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

#endregion

namespace Focus.Services.Repositories.LaborInsight
{
  public class FilterName
  {
    public const string Nationwide = "nationwide";
    public const string State = "state";
    public const string County = "county";
    public const string Msa = "msa";
    public const string Onet = "onet";
    public const string BgtOcc = "bgtocc";
    public const string BgtOccFamily = "bgtoccfamily";
    public const string Certification = "certification";
    public const string Skill = "skill";
    public const string JobTitleKeyword = "jobtitleKeyword";
    public const string CertificationKeyword = "certificationKeyword";
    public const string SkillKeyword = "skillKeyword";
    public const string Keyword = "keyword";
  }
}
