﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.DataTransferObjects.FocusExplorer;
using Focus.Core.Settings.Interfaces;
using Focus.Core.Settings.Lens;
using Focus.Data.Core.Entities;
using Focus.Data.Library.Entities;
using Focus.Services.Core.Extensions;
using Focus.Services.DtoMappers;
using Focus.Services.Repositories.Lens.Clients;
using Focus.Services.ServiceImplementations;
using Framework.Core;
using Framework.Logging;
using Framework.ServiceLocation;

using Focus.Core;
using Focus.Core.Criteria.CandidateSearch;
using Focus.Core.Models.Career;
using Focus.Core.Views;

#endregion

namespace Focus.Services.Repositories.Lens
{
	public class LensRepository : ILensRepository
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="LensRepository" /> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		public LensRepository(IRuntimeContext runtimeContext)
		{
			RuntimeContext = runtimeContext ?? ServiceLocator.Current.Resolve<IRuntimeContext>();
		}

		internal IRuntimeContext RuntimeContext { get; private set; }

		internal IAppSettings AppSettings { get { return RuntimeContext.AppSettings; } }
		internal IRepositories Repositories { get { return RuntimeContext.Repositories; } }
		internal IHelpers Helpers { get { return RuntimeContext.Helpers; } }

		#region Resume Operations

		/// <summary>
		/// Searches the resumes.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <param name="culture">The culture.</param>
		/// <returns></returns>
		/// <exception cref="System.Exception">Could not tag resume</exception>
		public List<ResumeView> SearchResumes(CandidateSearchCriteria criteria, string culture)
		{
			var postingXml = string.Empty;
			var resumeXml = string.Empty;
			var candidates = new List<ResumeView>();

			var genericLensService = GetGenericLensService();

			const string defaultPosting = "e";

			if (criteria.MinimumScore < 0) criteria.MinimumScore = 0;

			switch (criteria.SearchType)
			{
				case CandidateSearchTypes.ByJobDescription:
					string posting;

					if (criteria.JobDetailsCriteria.Posting.IsNullOrEmpty())
					{
						// Try the job title or else send the default
						posting = (criteria.JobDetailsCriteria.JobTitle.IsNotNullOrEmpty()) ? criteria.JobDetailsCriteria.JobTitle : defaultPosting;
					}
					else
						posting = criteria.JobDetailsCriteria.Posting;

					// Tag the Job Description and extract the posting
					var taggedJobDescription = TagPosting(genericLensService, posting);
					postingXml = taggedJobDescription.GetPostingFromTaggedPosting();

					break;

				case CandidateSearchTypes.ByResume:
					//Tag the Resume
					if (criteria.ResumeCriteria.ResumeXml.IsNotNullOrEmpty())
					{
						var taggedResume = TagResume(genericLensService, criteria.ResumeCriteria.ResumeXml);
						resumeXml = taggedResume.GetResumeFromTaggedResume();

						if (resumeXml.IsNullOrEmpty()) throw new Exception("Could not tag resume");
					}
					else if (criteria.ResumeCriteria.Resume.IsNotNull() && criteria.ResumeCriteria.Resume.Length > 0)
					{
						var taggedResume = TagResume(genericLensService, criteria.ResumeCriteria.Resume, criteria.ResumeCriteria.ResumeFileExtension);
						resumeXml = taggedResume.GetResumeFromTaggedResume();

						if (resumeXml.IsNullOrEmpty()) throw new Exception("Could not tag resume");
					}

					break;

				case CandidateSearchTypes.ByKeywords:
					// Tag the Keywords and extract the posting (Hack to get around search needing some sort of job description)
					if (criteria.KeywordCriteria.IsNotNull() && criteria.KeywordCriteria.Keywords.IsNotNullOrEmpty())
					{
						var taggedKeywords = TagPosting(genericLensService, criteria.KeywordCriteria.Keywords);
						postingXml = taggedKeywords.GetPostingFromTaggedPosting();
					}
					break;

				case CandidateSearchTypes.OpenSearch:
					var taggedDefaultPosting = TagPosting(genericLensService, defaultPosting);
					postingXml = taggedDefaultPosting.GetPostingFromTaggedPosting();
					break;
			}

			var resumeLensServices = GetLensServices(LensServiceTypes.Resume);

			if (resumeLensServices.IsNullOrEmpty())
				throw new Exception("No resume lens services defined");

			#region Database queries, this saves doing them in each thread which was causing issues in live env

			DateTime? postingDate = null;
			var jobId = (criteria.JobDetailsCriteria.IsNotNull()) ? criteria.JobDetailsCriteria.JobId : 0;

			if (jobId > 0 && criteria.AdditionalCriteria.IsNotNull() && criteria.AdditionalCriteria.EducationCriteria.IsNotNull() && criteria.AdditionalCriteria.EducationCriteria.MonthsUntilExpectedCompletion.HasValue)
			{
				postingDate = Repositories.Core.Jobs.Where(x => x.Id == jobId).Select(x => x.PostedOn).FirstOrDefault();
			}

			#endregion

			// if we are only searching one Lens do not go into seperate threads
			if (resumeLensServices.Count == 1)
			{
				candidates = SearchResumesOnLensService(resumeLensServices[0], criteria, postingXml, resumeXml, culture, postingDate);
			}
			else
			{
				var resultSets = new ConcurrentBag<List<ResumeView>>();
				var context = RuntimeContext.GetContext();

				Parallel.ForEach(resumeLensServices, service => resultSets.Add(SearchResumesOnLensService(context, service, criteria, postingXml, resumeXml, culture, postingDate)));

				//TODO: Martha (Medium) De-dupe results
				foreach (var resultSet in resultSets)
				{
					candidates.AddRange(resultSet);
				}
			}

			var maximumDocumentCount = (criteria.MinimumDocumentCount > 0) ? criteria.MinimumDocumentCount : AppSettings.MaximumNoDocumentToReturnInSearch;

			return candidates.OrderByDescending(x => x.Score).Take(maximumDocumentCount).ToList();
		}

		/// <summary>
		/// Gets the tagged resume without skills.
		/// </summary>
		/// <param name="resume">The resume.</param>
		/// <param name="resumeFileExtension">The resume file extension.</param>
		/// <returns></returns>
		public string ParseResume(byte[] resume, string resumeFileExtension)
		{
			return ParseResume(resume, resumeFileExtension, true);
		}

		/// <summary>
		/// Gets the tagged resume with or without skills
		/// </summary>
		/// <param name="resume"></param>
		/// <param name="resumeFileExtension"></param>
		/// <param name="resumeOnly"></param>
		/// <returns></returns>
		public string ParseResume(byte[] resume, string resumeFileExtension, bool resumeOnly)
		{
			string resumeXml;

			var lensService = GetGenericLensService();
			var taggedResume = TagResume(lensService, resume, resumeFileExtension);

			if (resumeOnly)
				resumeXml = taggedResume.GetResumeFromTaggedResume();
			else
				resumeXml = taggedResume;

			if (resumeXml.IsNullOrEmpty())
				throw new Exception("Could not parse resume");

			return resumeXml;
		}

		/// <summary>
		/// Registers the resume.
		/// </summary>
		/// <param name="lensId">The Lens Id (Focus uses the PersonId as the Id for Lens)</param>
		/// <param name="resumeXml">The resume XML.</param>
		/// <param name="resumeRegisteredOn">The resume registered on.</param>
		/// <param name="resumeModifiedOn">The resume modified on.</param>
		/// <param name="personId">The person id.</param>
		/// <param name="enrollmentStatus">The enrollment status.</param>
		/// <param name="degreeEducationLevelIds">The degree education level ids.</param>
		/// <param name="dateOfBirth">The date of birth.</param>
		public void RegisterResume(string lensId, string resumeXml, DateTime resumeRegisteredOn, DateTime resumeModifiedOn, long personId, SchoolStatus? enrollmentStatus, List<long> degreeEducationLevelIds, DateTime? dateOfBirth)
		{
			var lensServices = GetLensServices(LensServiceTypes.Resume, true);

			if (lensServices.IsNotNullOrEmpty())
			{
				// If only one Lens Service do not multithread
				if (lensServices.Count == 1)
				{
					RegisterResumeOnLensService(lensServices[0], lensId, resumeXml, resumeRegisteredOn, resumeModifiedOn, personId, enrollmentStatus, degreeEducationLevelIds, dateOfBirth);
				}
				else
				{
					// Create a task to register the resume on the lens service
					var context = RuntimeContext.GetContext();
					var tasks = lensServices.Select(service => Task.Factory.StartNew(() => RegisterResumeOnLensService(context, service, lensId, resumeXml, resumeRegisteredOn, resumeModifiedOn, personId, enrollmentStatus, degreeEducationLevelIds, dateOfBirth))).ToArray();

					// Wait for all tasks to complete
					Task.WaitAll(tasks);
				}
			}
		}

		/// <summary>
		/// Unregisters the resume.
		/// </summary>
		/// <param name="lensIds">A list of ids for Lens (Focus uses the PersonId as the Id for Lens)</param>
		public void UnregisterResume(List<string> lensIds)
		{
			var lensServices = GetLensServices(LensServiceTypes.Resume, true);

			if (lensServices.IsNotNullOrEmpty())
			{
				// If only one Lens Service do not multithread
				if (lensServices.Count == 1)
				{
					UnregisterResume(lensServices[0], lensIds);
				}
				else
				{
					// Create a task to unregister the resume on the lens service
					var context = RuntimeContext.GetContext();
					var tasks = lensServices.Select(service => Task.Factory.StartNew(() => UnregisterResumeOnLensService(context, service, lensIds))).ToArray();

					// Wait for all tasks to complete
					Task.WaitAll(tasks);
				}
			}
		}

		#endregion

		#region Posting Operations

		/// <summary>
		/// Searches the postings.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <param name="culture">The culture.</param>
		/// <param name="forAlert">if set to <c>true</c> for job alerts.</param>
		/// <param name="personId">The id of the person searching the postings.</param>
		/// <returns></returns>
		/// <exception cref="System.Exception">No posting lens services defined</exception>
		public List<PostingSearchResultView> SearchPostings(SearchCriteria criteria, string culture, bool forAlert, long? personId = null)
		{
			var postings = new List<PostingSearchResultView>();

			//Martha (Assumption) - Assuming all lens are set up the same so only one command has to be built
			// For alerts a JobAlert service type overrides the posting type lens services
			var postingLensServices = GetLensServices(forAlert ? LensServiceTypes.JobAlert : LensServiceTypes.Posting, forAnonymous: criteria.ExcludePostingCriteria.IsNotNull() && criteria.ExcludePostingCriteria.AnonymousOnly);
			// If we don't have any jobalert lens services then use the standard posting lens services
			if (forAlert && postingLensServices.IsNullOrEmpty())
				postingLensServices = GetLensServices(LensServiceTypes.Posting, forAnonymous: criteria.ExcludePostingCriteria.IsNotNull() && criteria.ExcludePostingCriteria.AnonymousOnly);

			if (postingLensServices.IsNullOrEmpty())
				throw new Exception("No posting lens services defined");

			const string defaultXml = "<resume></resume>";
			var resumeXml = "";
			var postingXml = "";

			// Loop through the search criteria to see if you need to get the users resume
			if (criteria.IsNotNull() && criteria.ReferenceDocumentCriteria.IsNotNull())
			{
				var referencePostingCriteria = criteria.ReferenceDocumentCriteria;

				if (referencePostingCriteria.DocumentId.IsNotNullOrEmpty())
				{
					if (referencePostingCriteria.DocumentType == DocumentType.Resume)
					{
						resumeXml = Repositories.Core.Resumes.Where(x => x.Id == referencePostingCriteria.DocumentId.ToLong()).Select(x => x.ResumeXml).FirstOrDefault().GetResumeFromTaggedResume();

						if (resumeXml.IsNotNull() && referencePostingCriteria.ExcludedJobs.IsNotNullOrEmpty())
							resumeXml = RemoveExcludedJobs(resumeXml, referencePostingCriteria.ExcludedJobs);
					}
					else if (referencePostingCriteria.DocumentType == DocumentType.Posting)
					{
						var postingDto = PostingService.GetPosting(RuntimeContext, referencePostingCriteria.DocumentId, isForSearch: true);
						postingXml = postingDto.IsNotNull() ? postingDto.PostingXml.GetPostingFromTaggedPosting() : null;

						if (postingXml.IsNullOrEmpty() || postingXml.Replace("<posting>", "").Replace("</posting>", "").IsNullOrEmpty())
							return new List<PostingSearchResultView>();
					}
				}
			}

			if (resumeXml.IsNullOrEmpty() && postingXml.IsNullOrEmpty())
				resumeXml = defaultXml;

			#region Database queries, this saves doing them in each thread which was causing issues in live env

			var programOfStudyROnets = new List<string>();
			var programOfStudyLensIds = new List<int>();

			if (criteria.EducationProgramOfStudyCriteria.IsNotNull() && criteria.EducationProgramOfStudyCriteria.DegreeEducationLevelId.HasValue && criteria.EducationProgramOfStudyCriteria.DegreeEducationLevelId > 0)
			{
				programOfStudyLensIds = GetDegreeLensIdsForDegreeEducationLevel(criteria.EducationProgramOfStudyCriteria.DegreeEducationLevelId);

				if (programOfStudyLensIds.IsNullOrEmpty())
					programOfStudyROnets = GetROnetsForDegreeEducationLevel(criteria.EducationProgramOfStudyCriteria.DegreeEducationLevelId);
			}
			else if (criteria.EducationProgramOfStudyCriteria.IsNotNull() && criteria.EducationProgramOfStudyCriteria.ProgramAreaId.HasValue)
			{
				programOfStudyLensIds = GetDegreeLensIdsForProgramArea(criteria.EducationProgramOfStudyCriteria.ProgramAreaId);

				if (programOfStudyLensIds.IsNullOrEmpty())
					programOfStudyROnets = GetROnetsForProgramArea(criteria.EducationProgramOfStudyCriteria.ProgramAreaId);
			}

			var internshipCategoryLensFilterIds = new List<int>();

			if (criteria.InternshipCriteria.IsNotNull() && criteria.InternshipCriteria.InternshipCategories.IsNotNullOrEmpty())
			{
				internshipCategoryLensFilterIds = Repositories.Library.Query<InternshipCategory>().Where(x => criteria.InternshipCriteria.InternshipCategories.Contains(x.Id)).Select(x => x.LensFilterId).Distinct().ToList();
			}

			var onetCodes = new List<string>();

			if (criteria.OccupationCriteria.IsNotNull() && criteria.OccupationCriteria.JobFamilyId > 0 && criteria.OccupationCriteria.OccupationIds.IsNotNullOrEmpty() && criteria.OccupationCriteria.OccupationIds.Count > 0)
			{
				var onets = Repositories.Library.Onets.Where(x => criteria.OccupationCriteria.OccupationIds.Contains(x.Id) && x.JobFamilyId == criteria.OccupationCriteria.JobFamilyId).ToList();

				//get a list of just the onet codes from the list of onets
				onetCodes = onets.Select(x => x.OnetCode.Replace("-", "").Replace(".", "")).ToList();
			}

			var industryCodes = new List<string>();

			if (criteria.IndustryCriteria.IsNotNull() && (criteria.IndustryCriteria.IndustryId.IsNotNull() || (criteria.IndustryCriteria.IndustryDetailIds.IsNotNullOrEmpty())))
			{
				var searchIds = new List<long>();

				// get the industry detail ids if there are none we will use the parent industry
				if (criteria.IndustryCriteria.IndustryDetailIds.IsNotNullOrEmpty())
					searchIds.AddRange(criteria.IndustryCriteria.IndustryDetailIds);
				else if (criteria.IndustryCriteria.IndustryId.IsNotNull() && criteria.IndustryCriteria.IndustryId > 0)
					searchIds.Add(Convert.ToInt64(criteria.IndustryCriteria.IndustryId));

				// Get a list of industry codes for the industry Ids.
				industryCodes = Repositories.Library.NAICS.Where(x => searchIds.Contains(x.Id)).Select(x => x.Code).ToList();
			}

			#endregion


			Logger.TraceStartEnd("Searching Postings", () =>
			{
				var checkReferrals = (personId.HasValue && (AppSettings.ConfidentialEmployersEnabled || AppSettings.ConfidentialEmployersDefault));

				// If only one Lens Service do not multithread
				if (postingLensServices.Count == 1 && !checkReferrals)
				{
					postings = SearchPostingsOnLensService(postingLensServices[0], criteria, resumeXml, postingXml, culture, programOfStudyROnets, internshipCategoryLensFilterIds, onetCodes, industryCodes,
						programOfStudyLensIds);
				}
				else
				{
					var resultSets = new ConcurrentBag<List<PostingSearchResultView>>();
					var referrals = new Dictionary<string, ReferralViewDto>();

					var context = RuntimeContext.GetContext();

					var actionCount = postingLensServices.Count + (checkReferrals ? 1 : 0);
					var homeBasedCriteria = criteria.IsNotNull() ? criteria.CloneObject<SearchCriteria>() : null;
					var addService = false;
					LensService addedService = new LensService();
					JobLocationCriteria location = null;


					//Check for home based jobs criteria

					var homeBasedJobsCriteria = criteria.HomeBasedJobsCriteria;
					if (homeBasedJobsCriteria.IsNotNull() && homeBasedJobsCriteria.HomeBasedJobs.IsNotNull())
					{
						switch (homeBasedJobsCriteria.HomeBasedJobs)
						{
							case FilterTypes.ShowOnly:
								if (criteria.JobLocationCriteria.IsNotNull())
								{
									location = criteria.JobLocationCriteria.CloneObject<JobLocationCriteria>();
									criteria.JobLocationCriteria = null;
								}
								break;
							case FilterTypes.Exclude:
								break;
							default:
								{
									//add another lens search for home based jobs
									postingLensServices.ForEach(service =>
									{
										if (!service.ReadOnly)
										{
											addedService = service;
											actionCount += 1;
											addService = true;
											criteria.HomeBasedJobsCriteria.HomeBasedJobs = FilterTypes.Exclude;
											homeBasedCriteria.HomeBasedJobsCriteria.HomeBasedJobs = FilterTypes.ShowOnly;
											homeBasedCriteria.JobLocationCriteria = null;
										}
									});
									break;
								}
						}
					}


					var actions = new Action[actionCount];
					var index = 0;

					postingLensServices.ForEach(service =>
					{
						actions[index] = (() => resultSets.Add(SearchPostingsOnLensService(context, service, criteria, resumeXml, postingXml, culture, programOfStudyROnets, internshipCategoryLensFilterIds, onetCodes, industryCodes, programOfStudyLensIds)));
						index += 1;
					});

					if (addService)
					{
						actions[index] = (() => resultSets.Add(SearchPostingsOnLensService(context, addedService, homeBasedCriteria, resumeXml, postingXml, culture, programOfStudyROnets, internshipCategoryLensFilterIds, onetCodes, industryCodes, programOfStudyLensIds)));
						index += 1;
					}

					if (checkReferrals)
						actions[index] = (() => GetReferrals(context, personId.Value, referrals));

					Parallel.Invoke(actions);

					//TODO: Martha (Medium) De-dupe results
					foreach (var resultSet in resultSets)
						postings.AddRange(resultSet);

					//restore the critria
					if (addService)
						criteria.HomeBasedJobsCriteria.HomeBasedJobs = FilterTypes.NoFilter;
					else if (location.IsNotNull())
						criteria.JobLocationCriteria = location;

					// If there are any approved referrals, the employer name can be shown
					if (referrals.Any())
					{
						postings.ForEach(posting =>
						{
							if (referrals.ContainsKey(posting.Id) && referrals[posting.Id].EmployerName.IsNotNullOrEmpty())
							{
								posting.Employer = referrals[posting.Id].EmployerName;
								posting.IsReferred = true;
							}
						});
					}
				}
			});

			var maximumDocumentCount = (criteria.RequiredResultCriteria.IsNotNull() && criteria.RequiredResultCriteria.MaximumDocumentCount.HasValue && criteria.RequiredResultCriteria.MaximumDocumentCount > 0)
																	? criteria.RequiredResultCriteria.MaximumDocumentCount.Value : AppSettings.MaximumNoDocumentToReturnInSearch;

			var adjustForStarMatch = false;
			if (RuntimeContext.AppSettings.ScreeningPrefExclusiveToMinStarMatch) // See FVN-1091
			{
				var resultCriteria = criteria.RequiredResultCriteria;
				if (resultCriteria.IsNotNull() && resultCriteria.MinimumStarMatch.IsNotIn(StarMatching.None, StarMatching.ZeroStar))
					adjustForStarMatch = true;
			}

			var postingsResults = adjustForStarMatch
															? postings.Where(pv => pv.ScreeningPreferences.IsNull() || pv.StarRating >= ConvertScreeningPreferencesToMininumStarValue(pv.ScreeningPreferences.Value))
															: postings.AsEnumerable();


			return OrderPostings(postingsResults, criteria.IsMatchedSearch(), maximumDocumentCount);
		}

		/// <summary>
		/// Orders the posting search results.
		/// </summary>
		/// <param name="postings">The postings.</param>
		/// <param name="matchedSearch">if set to <c>true</c> [matched search].</param>
		/// <param name="documentCount">The recordset size.</param>
		/// <returns></returns>
		public List<PostingSearchResultView> OrderPostings(IEnumerable<PostingSearchResultView> postings,
			bool matchedSearch, int documentCount)
		{
			// When matching we order by star rating, then origin then rank and date should we need to. (i.e. If a spidered job has a higher rank but the same start rating as a talent job show the talent job first)
            if (matchedSearch)
            {
                //FVN - 5798 - Display Talent job matches first and then by star rating, score and job date
                if(AppSettings.ShowTalentJobsAsHighPriority)
                    return
                    postings.OrderBy(x => x.OriginId)
                        .ThenByDescending(x => x.StarRating)
                        .ThenByDescending(x => x.Rank)
                        .ThenByDescending(x => x.JobDate)
                        .Take(documentCount)
                        .ToList();
                return
                    postings.OrderByDescending(x => x.StarRating)
                        .ThenBy(x => x.OriginId)
                        .ThenByDescending(x => x.Rank)
                        .ThenByDescending(x => x.JobDate)
                        .Take(documentCount)
                        .ToList();
            }
			// When not matching show Talent jobs first then ordered by date (Freshest first)
			return postings.OrderBy(x => x.OriginId)
						.ThenByDescending(x => x.JobDate)
						.Take(documentCount)
						.ToList();
		}

		/// <summary>
		/// Converts the screening preference to a minimum star rating to apply if applicable
		/// </summary>
		/// <param name="preferenceValue">The preference value</param>
		/// <returns>The minumum star rating to apply</returns>
		private static int ConvertScreeningPreferencesToMininumStarValue(ScreeningPreferences preferenceValue)
		{
			switch (preferenceValue)
			{
				case ScreeningPreferences.JobSeekersMustHave1StarMatchToApply:
					return 1;
				case ScreeningPreferences.JobSeekersMustHave2StarMatchToApply:
					return 2;
				case ScreeningPreferences.JobSeekersMustHave3StarMatchToApply:
					return 3;
				case ScreeningPreferences.JobSeekersMustHave4StarMatchToApply:
					return 4;
				case ScreeningPreferences.JobSeekersMustHave5StarMatchToApply:
					return 5;
			}

			return 0;
		}

		/// <summary>
		/// Registers the posting.
		/// </summary>
		/// <param name="lensPostingId">The lens posting id.</param>
		/// <param name="postingId">The posting id.</param>
		/// <param name="postingXml">The posting XML.</param>
		/// <param name="dataElementsXml">The data elements XML.</param>
		/// <param name="culture">The culture.</param>
		/// <param name="mappedRequiredProgramsOfStudy">The mapped required programs of study.</param>
		/// <param name="postalCode">The postal code.</param>
		public void RegisterPosting(string lensPostingId, long? postingId, XDocument postingXml, string dataElementsXml, string culture, List<int> mappedRequiredProgramsOfStudy, PostalCodeDto postalCode = null)
		{
			var lensServices = GetLensServices(LensServiceTypes.Posting, true);

			if (lensServices.IsNotNullOrEmpty())
			{
				// If only one Lens Service do not multithread
				if (lensServices.Count == 1)
				{
					RegisterPostingOnLensService(lensServices[0], lensPostingId, postingId, postingXml, dataElementsXml, culture, mappedRequiredProgramsOfStudy, postalCode);
				}
				else
				{
					// Create a task to register the posting on the lens service
					var context = RuntimeContext.GetContext();
					var tasks = lensServices.Select(service => Task.Factory.StartNew(() => RegisterPostingOnLensService(context, service, lensPostingId, postingId, postingXml, dataElementsXml, culture, mappedRequiredProgramsOfStudy, postalCode))).ToArray();

					// Wait for all tasks to complete
					Task.WaitAll(tasks);
				}
			}
		}

		/// <summary>
		/// Unregisters the posting.
		/// </summary>
		/// <param name="lensPostingId">The lens posting id.</param>
		public void UnregisterPosting(string lensPostingId)
		{
			var lensServices = GetLensServices(LensServiceTypes.Posting, true);

			if (lensServices.IsNotNullOrEmpty())
			{
				// If only one Lens Service do not multithread
				if (lensServices.Count == 1)
				{
					UnregisterPosting(lensServices[0], lensPostingId);
				}
				else
				{
					// Create a task to unregister the posting on the lens service
					var context = RuntimeContext.GetContext();
					var tasks = lensServices.Select(service => Task.Factory.StartNew(() => UnregisterPostingOnLensService(context, service, lensPostingId))).ToArray();

					// Wait for all tasks to complete
					Task.WaitAll(tasks);
				}
			}
		}

		/// <summary>
		/// Canon the posting with job mine.
		/// </summary>
		/// <param name="postingXml">The posting XML.</param>
		/// <returns></returns>
		public string CanonPostingWithJobMine(string postingXml)
		{
			var lensService = GetJobMineLensService();
			return (lensService.IsNotNull()) ? Canon(lensService, postingXml) : String.Empty;
		}

		#endregion

		#region Library Operations

		/// <summary>
		/// Gets the onet codes.
		/// </summary>
		/// <param name="jobTitle">The job title.</param>
		/// <returns></returns>
		public List<string> GetOnetCodes(string jobTitle)
		{
			var lensService = GetGenericLensService();
			var lensClient = GetLensClient(lensService);

			var results = new List<string>();
			var commandBuilder = new LensCommandBuilder(RuntimeContext, lensService);
			var onetCommand = commandBuilder.BuildSearchForOnetCommand(jobTitle);
			var onetcodes = (onetCommand.IsNotNullOrEmpty()) ? lensClient.ExecuteCommand(onetCommand) : null;

			if (onetcodes.IsNotNullOrEmpty())
			{
				var xDoc = new XmlDocument { PreserveWhitespace = true };
				xDoc.LoadXml(onetcodes);

				if (xDoc.SelectSingleNode("//title").IsNotNull() && xDoc.SelectSingleNode("//title").Attributes.IsNotNull() &&
						xDoc.SelectSingleNode("//title").Attributes["onet"].IsNotNull())
				{
					foreach (var onetcode in xDoc.SelectSingleNode("//title").Attributes["onet"].Value.Split('|'))
					{
						if (onetcode.IsNotNullOrEmpty())
							results.Add(onetcode);
					}
				}
			}

			return results;
		}

		#endregion

		#region Utility Operations

		/// <summary>
		/// Determines whether this instance is licensed.
		/// </summary>
		/// <returns>
		/// 	<c>true</c> if this instance is licensed; otherwise, <c>false</c>.
		/// </returns>
		public bool IsLicensed()
		{
			try
			{
				var genericLensService = GetGenericLensService();
				var lensClient = GetLensClient(genericLensService);

				var commandBuilder = new LensCommandBuilder(RuntimeContext, genericLensService);
				var isLicensedCommand = commandBuilder.BuildIsLicensedCommand();

				var result = lensClient.ExecuteCommand(isLicensedCommand);

				return !(result.ToLower().Contains("license expired"));
			}
			catch
			{
				return false;
			}
		}

		/// <summary>
		/// Clarifies the specified text.
		/// </summary>
		/// <param name="text">The text.</param>
		/// <param name="minScore">The min score.</param>
		/// <param name="maxSearchCount">The max search count.</param>
		/// <param name="type">The type.</param>
		/// <returns></returns>
		public List<string> Clarify(string text, int minScore, int maxSearchCount, string type)
		{
			var genericLensService = GetGenericLensService();
			var lensClient = GetLensClient(genericLensService);

			var commandBuilder = new LensCommandBuilder(RuntimeContext, genericLensService);
			var clarifyCommand = commandBuilder.BuildClarifyCommand(text, minScore, maxSearchCount, type);
			var clarifications = (clarifyCommand.IsNotNullOrEmpty()) ? lensClient.ExecuteCommand(clarifyCommand) : null;
			var clarifiedWords = new List<string>();

			if (clarifications.IsNotNullOrEmpty())
			{
				var xDoc = new XmlDocument { PreserveWhitespace = true };
				xDoc.LoadXml(clarifications);

				var wordNodes = xDoc.SelectNodes("//word");
				clarifiedWords = (from XmlNode wordNode in wordNodes select wordNode.InnerText).ToList();
			}
			else
				clarifiedWords = new List<string>();

			return clarifiedWords;
		}

		/// <summary>
		/// Coverts the binary data to ASCII Text.
		/// </summary>
		/// <param name="binaryFileData">The binary file data.</param>
		/// <param name="fileExtension">The file extension.</param>
		/// <returns></returns>
		public string CovertBinaryData(byte[] binaryFileData, string fileExtension)
		{
			var lensService = GetGenericLensService();
			var lensClient = GetLensClient(lensService);
			return lensClient.ConvertBinaryData(binaryFileData, fileExtension);
		}

		/// <summary>
		/// Canons the XML.
		/// </summary>
		/// <param name="xml">The XML.</param>
		/// <returns></returns>
		public string Canon(string xml)
		{
			var lensService = GetGenericLensService();

			var xmlDocument = new XmlDocument { PreserveWhitespace = true };
			xmlDocument.LoadXml(xml);

			#region Remove existing canon attributes

			if (xmlDocument.SelectSingleNode("//resume").IsNotNull())
				xmlDocument.SelectSingleNode("//resume").Attributes.RemoveAll();

			else if (xmlDocument.SelectSingleNode("//posting").IsNotNull())
				xmlDocument.SelectSingleNode("//posting").Attributes.RemoveAll();

			#endregion

			return Canon(lensService, xml);
		}

		/// <summary>
		/// Calculates a match score between a resume and a posting.
		/// </summary>
		/// <param name="resumeXml">The resume XML.</param>
		/// <param name="postingXml">The posting XML.</param>
		/// <returns></returns>
		public int Match(string resumeXml, string postingXml)
		{
			var lensService = GetGenericLensService();
			return Match(lensService, resumeXml, postingXml);
		}

		/// <summary>
		/// Tags the specified plain text.
		/// </summary>
		/// <param name="plainText">The plain text.</param>
		/// <param name="type">The type.</param>
		/// <returns></returns>
		public string Tag(string plainText, DocumentType type)
		{
			var lensService = GetGenericLensService();

			switch (type)
			{
				case (DocumentType.Posting):
					return TagPosting(lensService, plainText);
				case (DocumentType.Resume):
					return TagResume(lensService, plainText);
			}

			return String.Empty;
		}

		/// <summary>
		/// Tags the specified binary file data.
		/// </summary>
		/// <param name="binaryFileData">The binary file data.</param>
		/// <param name="fileExtension">The file extension.</param>
		/// <param name="type">The type.</param>
		/// <param name="html">The HTML.</param>
		/// <returns></returns>
		public string TagWithHtml(byte[] binaryFileData, string fileExtension, DocumentType type, out string html)
		{
			var lensService = GetGenericLensService();
			var lensClient = GetLensClient(lensService);
			return lensClient.TagBinaryDataWithHtml(binaryFileData, fileExtension, type, out html);
		}

		/// <summary>
		/// Executes the job mine command.
		/// </summary>
		/// <param name="command">The command.</param>
		/// <returns></returns>
		public string ExecuteJobMineCommand(string command)
		{
			var lensService = GetJobMineLensService();

			if (lensService.IsNull())
				return string.Empty;

			var lensClient = GetLensClient(lensService);

			return lensClient.ExecuteCommand(command);
		}

		#endregion

		#region Private helper classes

		/// <summary>
		/// Gets the lens services.
		/// </summary>
		/// <param name="serviceType">Type of the service.</param>
		/// <param name="forUpdate">if set to <c>true</c> [for update].</param>
		/// <param name="forAnonymous">if set to <c>true</c> [for anonymous].</param>
		/// <returns></returns>
		private List<LensService> GetLensServices(LensServiceTypes serviceType, bool forUpdate = false, bool forAnonymous = false)
		{
			var query = AppSettings.LensServices.Where(x => x.ServiceType == serviceType);
			if (forUpdate)
				query = query.Where(x => x.ReadOnly == false);

			if (forAnonymous)
				query = query.Where(x => x.AnonymousAccess);

			return query.ToList();
		}

		/// <summary>
		/// Gets the generic lens service.
		/// </summary>
		/// <returns></returns>
		private LensService GetGenericLensService()
		{
			var lensServices = GetLensServices(LensServiceTypes.Generic);

			if (lensServices.IsNull())
				throw new Exception("No generic lens service defined");

			return lensServices[0];
		}

		/// <summary>
		/// Gets the job mine lens service.
		/// </summary>
		/// <returns></returns>
		private LensService GetJobMineLensService()
		{
			var lensServices = GetLensServices(LensServiceTypes.JobMine);

			return (lensServices.IsNotNull()) ? lensServices[0] : null;
		}

		/// <summary>
		/// Registers the posting on lens service.
		/// </summary>
		/// <param name="context">The context.</param>
		/// <param name="lensService">The lens service.</param>
		/// <param name="lensPostingId">The lens posting id.</param>
		/// <param name="postingId">The posting id.</param>
		/// <param name="postingXml">The posting XML.</param>
		/// <param name="dataElementsXml">The data elements XML.</param>
		/// <param name="culture">The culture.</param>
		/// <param name="mappedRequiredProgramsOfStudy">The mapped required programs of study.</param>
		/// <param name="postalCode">The postal code.</param>
		private void RegisterPostingOnLensService(object context, LensService lensService, string lensPostingId, long? postingId, XDocument postingXml, string dataElementsXml, string culture, List<int> mappedRequiredProgramsOfStudy, PostalCodeDto postalCode)
		{
			RuntimeContext.SetContext(context);
			RegisterPostingOnLensService(lensService, lensPostingId, postingId, postingXml, dataElementsXml, culture, mappedRequiredProgramsOfStudy, postalCode);
		}

		/// <summary>
		/// Registers the posting on lens service.
		/// </summary>
		/// <param name="lensService">The lens service.</param>
		/// <param name="lensPostingId">The lens posting identifier.</param>
		/// <param name="postingId">The posting identifier.</param>
		/// <param name="postingXml">The posting XML.</param>
		/// <param name="dataElementsXml">The data elements XML.</param>
		/// <param name="culture">The culture.</param>
		/// <param name="mappedRequiredProgramsOfStudy">The mapped required programs of study.</param>
		/// <param name="postalCode">The postal code.</param>
		private void RegisterPostingOnLensService(LensService lensService, string lensPostingId, long? postingId, XDocument postingXml, string dataElementsXml, string culture, List<int> mappedRequiredProgramsOfStudy, PostalCodeDto postalCode)
		{
			UnregisterPosting(lensService, lensPostingId);

			var jobdocXml = lensService.PopulatePostingCustomFiltersAndGetJobDoc(RuntimeContext, postingXml, dataElementsXml, postingId, culture, AppSettings.Theme, mappedRequiredProgramsOfStudy, postalCode);
			RegisterPosting(lensService, jobdocXml, lensPostingId);
		}

		/// <summary>
		/// Unregisters the posting on lens service.
		/// </summary>
		/// <param name="context">The context.</param>
		/// <param name="lensService">The lens service.</param>
		/// <param name="lensPostingId">The lens posting id.</param>
		private void UnregisterPostingOnLensService(object context, LensService lensService, string lensPostingId)
		{
			RuntimeContext.SetContext(context);
			UnregisterPosting(lensService, lensPostingId);
		}

		/// <summary>
		/// Registers the resume on lens service.
		/// </summary>
		/// <param name="context">The context.</param>
		/// <param name="lensService">The lens service.</param>
		/// <param name="lensId">The Lens Id (Focus uses the PersonId as the Id for Lens)</param>
		/// <param name="resumeXml">The resume XML.</param>
		/// <param name="resumeRegisteredOn">The resume registered on.</param>
		/// <param name="resumeModifiedOn">The resume modified on.</param>
		/// <param name="personId">The person id.</param>
		/// <param name="enrollmentStatus">The enrollment status.</param>
		/// <param name="degreeEducationLevelIds">The degree education level ids.</param>
		/// <param name="dateOfBirth">The date of birth.</param>
		private void RegisterResumeOnLensService(object context, LensService lensService, string lensId, string resumeXml, DateTime resumeRegisteredOn, DateTime resumeModifiedOn, long personId, SchoolStatus? enrollmentStatus, List<long> degreeEducationLevelIds, DateTime? dateOfBirth)
		{
			RuntimeContext.SetContext(context);
			RegisterResumeOnLensService(lensService, lensId, resumeXml, resumeRegisteredOn, resumeModifiedOn, personId, enrollmentStatus, degreeEducationLevelIds, dateOfBirth);
		}

		/// <summary>
		/// Registers the resume on lens service.
		/// </summary>
		/// <param name="lensService">The lens service.</param>
		/// <param name="lensId">The Lens Id (Focus uses the PersonId as the Id for Lens)</param>
		/// <param name="resumeXml">The resume XML.</param>
		/// <param name="resumeRegisteredOn">The resume registered on.</param>
		/// <param name="resumeModifiedOn">The resume modified on.</param>
		/// <param name="personId">The person identifier.</param>
		/// <param name="enrollmentStatus">The enrollment status.</param>
		/// <param name="degreeEducationLevelIds">The degree education level ids.</param>
		/// <param name="dateOfBirth">The date of birth.</param>
		private void RegisterResumeOnLensService(LensService lensService, string lensId, string resumeXml, DateTime resumeRegisteredOn, DateTime resumeModifiedOn, long personId, SchoolStatus? enrollmentStatus, List<long> degreeEducationLevelIds, DateTime? dateOfBirth)
		{
			UnregisterResume(lensService, new List<string> { lensId });

			resumeXml = lensService.PopulateResumeCustomFilters(RuntimeContext, resumeXml, resumeRegisteredOn, resumeModifiedOn, personId, enrollmentStatus, degreeEducationLevelIds, dateOfBirth);
			RegisterResume(lensService, resumeXml, lensId);
		}

		/// <summary>
		/// Unregisters the resume on lens service.
		/// </summary>
		/// <param name="context">The context.</param>
		/// <param name="lensService">The lens service.</param>
		/// <param name="lensIds">A list of ids for Lens (Focus uses the PersonId as the Id for Lens)</param>
		private void UnregisterResumeOnLensService(object context, LensService lensService, List<string> lensIds)
		{
			RuntimeContext.SetContext(context);
			UnregisterResume(lensService, lensIds);
		}

		/// <summary>
		/// Searches the postings on lens service.
		/// </summary>
		/// <param name="context">The context.</param>
		/// <param name="lensService">The Lens service.</param>
		/// <param name="criteria">The criteria.</param>
		/// <param name="resumeXml">The resume XML.</param>
		/// <param name="postingXml">The posting XML.</param>
		/// <param name="culture">The culture.</param>
		/// <param name="programOfStudyRonets">The program of study ronets.</param>
		/// <param name="internshipCategoryLensFilterIds">The internship category lens filter ids.</param>
		/// <param name="onetCodes">The onet codes.</param>
		/// <param name="industryCodes">The industry codes.</param>
		/// <param name="programOfStudyLensIds">The program of study lens ids.</param>
		/// <returns></returns>
		private List<PostingSearchResultView> SearchPostingsOnLensService(object context, LensService lensService, SearchCriteria criteria, string resumeXml, string postingXml, string culture,
																																			List<string> programOfStudyRonets, List<int> internshipCategoryLensFilterIds, List<string> onetCodes, List<string> industryCodes,
																																			List<int> programOfStudyLensIds)
		{
			RuntimeContext.SetContext(context);
			return SearchPostingsOnLensService(lensService, criteria, resumeXml, postingXml, culture, programOfStudyRonets, internshipCategoryLensFilterIds, onetCodes, industryCodes, programOfStudyLensIds);
		}

		/// <summary>
		/// Searches the postings on lens service.
		/// </summary>
		/// <param name="lensService">The lens service.</param>
		/// <param name="criteria">The criteria.</param>
		/// <param name="resumeXml">The resume XML.</param>
		/// <param name="postingXml">The posting XML.</param>
		/// <param name="culture">The culture.</param>
		/// <param name="programOfStudyRonets">The program of study ronets.</param>
		/// <param name="internshipCategoryLensFilterIds">The internship category lens filter ids.</param>
		/// <param name="onetCodes">The onet codes.</param>
		/// <param name="industryCodes">The industry codes.</param>
		/// <param name="programOfStudyLensIds">The program of study lens ids.</param>
		/// <returns></returns>
		private List<PostingSearchResultView> SearchPostingsOnLensService(LensService lensService, SearchCriteria criteria, string resumeXml, string postingXml, string culture,
																																			List<string> programOfStudyRonets, List<int> internshipCategoryLensFilterIds, List<string> onetCodes, List<string> industryCodes,
																																			List<int> programOfStudyLensIds)
		{
			string postsXml = null;


			var commandBuilder = new LensCommandBuilder(RuntimeContext, lensService);
			var command = commandBuilder.BuildPostingSearchCommand(resumeXml, postingXml, criteria, culture, programOfStudyRonets, internshipCategoryLensFilterIds, onetCodes, industryCodes, programOfStudyLensIds, lensService.ReadOnly);

			Logger.Info(RuntimeContext.CurrentRequest.SessionId, RuntimeContext.CurrentRequest.RequestId, RuntimeContext.CurrentRequest.UserContext.UserId, "Search Postings Lens Command", null, command);

			var lensClient = GetLensClient(lensService);

			var requestId = Guid.Empty;
			var sessionId = Guid.Empty;
			long userId = 0;

			RuntimeContext.GetRequestIdentifiers(out sessionId, out requestId, out userId);

			Logger.TraceStartEnd("Searching Postings against: " + (lensService.Url ?? lensService.Host) + " - " + lensService.PortNumber + " : Vendor '" + lensService.Vendor + "'",
				() =>
				{
					postsXml = (!String.IsNullOrEmpty(command) ? lensClient.ExecuteCommand(command) : null);
				}, sessionId, requestId, userId);

			return postsXml.ToPostingSearchResultList(lensService, RuntimeContext, culture);
		}

		/// <summary>
		/// Gets the referrals for a candidate
		/// </summary>
		/// <param name="context">The runtime context.</param>
		/// <param name="candidateId">The candidate identifier.</param>
		/// <param name="referrals">The referrals.</param>
		private void GetReferrals(object context, long candidateId, Dictionary<string, ReferralViewDto> referrals)
		{
			RuntimeContext.SetContext(context);

			var approvalStatuses = new List<ApprovalStatuses>
			{
				ApprovalStatuses.Approved,
				ApprovalStatuses.None
			};

			var referralQuery = Repositories.Core.ReferralViews
																					 .Where(referral => referral.CandidateId == candidateId && approvalStatuses.Contains(referral.ApplicationApprovalStatus))
																					 .Select(referral => referral.AsDto());

			foreach (var referral in referralQuery)
			{
				if (!referrals.ContainsKey(referral.LensPostingId))
					referrals.Add(referral.LensPostingId, referral);
			}
		}


		/// <summary>
		/// Searches the resumes on lens service.
		/// </summary>
		/// <param name="context">The context.</param>
		/// <param name="lensService">The lens service.</param>
		/// <param name="criteria">The criteria.</param>
		/// <param name="postingXml">The posting XML.</param>
		/// <param name="resumeXml">The resume XML.</param>
		/// <param name="culture">The culture.</param>
		/// <param name="postingDate">The posting date.</param>
		/// <returns></returns>
		private List<ResumeView> SearchResumesOnLensService(object context, LensService lensService, CandidateSearchCriteria criteria, string postingXml, string resumeXml, string culture, DateTime? postingDate)
		{
			RuntimeContext.SetContext(context);
			return SearchResumesOnLensService(lensService, criteria, postingXml, resumeXml, culture, postingDate);
		}

		/// <summary>
		/// Searches the resumes on lens service.
		/// </summary>
		/// <param name="lensService">The lens service.</param>
		/// <param name="criteria">The criteria.</param>
		/// <param name="postingXml">The posting XML.</param>
		/// <param name="resumeXml">The resume XML.</param>
		/// <param name="culture">The culture.</param>
		/// <param name="postingDate">The posting date.</param>
		/// <returns></returns>
		private List<ResumeView> SearchResumesOnLensService(LensService lensService, CandidateSearchCriteria criteria, string postingXml, string resumeXml, string culture, DateTime? postingDate)
		{
			var commandBuilder = new LensCommandBuilder(RuntimeContext, lensService);
			var searchCommand = (criteria.SearchType == CandidateSearchTypes.CandidatesLikeThis)
														? commandBuilder.BuildSearchResumesLikeThisCommand(criteria)
														: commandBuilder.BuildResumeSearchCommand(postingXml, resumeXml, criteria, culture, postingDate);
			Logger.Info(RuntimeContext.CurrentRequest.SessionId, RuntimeContext.CurrentRequest.RequestId, RuntimeContext.CurrentRequest.UserContext.UserId, "Search Resumes Lens Command", null, searchCommand);

			var lensClient = GetLensClient(lensService);

			var requestId = Guid.Empty;
			var sessionId = Guid.Empty;
			long userId = 0;

			RuntimeContext.GetRequestIdentifiers(out sessionId, out requestId, out userId);

			string resumes = null;

			Logger.TraceStartEnd("Searching resumes against: " + (lensService.Url ?? lensService.Host) + " - " + lensService.PortNumber + " : Vendor '" + lensService.Vendor + "'",
				() =>
				{
					resumes = (!String.IsNullOrEmpty(searchCommand) ? lensClient.ExecuteCommand(searchCommand) : null);
				}, sessionId, requestId, userId);

			return (resumes.IsNotNullOrEmpty()) ? resumes.ToCandidateList(lensService.VeteranStatusCustomFilterTag, lensService.NCRCLevelTag, RuntimeContext, culture) : new List<ResumeView>();
		}

		/// <summary>
		/// Gets the ROnets for a degree education level.
		/// </summary>
		/// <param name="degreeEducationLevelId">The degree education level id.</param>
		/// <returns></returns>
		private List<string> GetROnetsForDegreeEducationLevel(long? degreeEducationLevelId)
		{
			var result = new List<string>();

			if (!degreeEducationLevelId.HasValue)
				return result;

			var rOnets = Repositories.Library.Query<DegreeEducationLevelROnetView>().Where(x => x.DegreeEducationLevelId == degreeEducationLevelId).Select(x => x.ROnet).ToList();

			if (rOnets.IsNullOrEmpty())
				return result;

			result.AddRange(rOnets.Select(rOnet => rOnet.AsLensROnet()));

			return result;
		}

		/// <summary>
		/// Gets the ROnets for a program area.
		/// </summary>
		/// <param name="programAreaId">The program area id.</param>
		/// <returns></returns>
		private List<string> GetROnetsForProgramArea(long? programAreaId)
		{
			var result = new List<string>();

			if (!programAreaId.HasValue)
				return result;

			var rOnets = Repositories.Library.Query<DegreeEducationLevelROnetView>().Where(x => x.ProgramAreaId == programAreaId).Select(x => x.ROnet).ToList();

			if (rOnets.IsNullOrEmpty())
				return result;

			foreach (var numericRonet in rOnets.Select(rOnet => rOnet.AsLensROnet()).Where(numericRonet => !result.Any(x => x == numericRonet)))
			{
				result.Add(numericRonet);
			}

			return result;
		}

		/// <summary>
		/// Gets the degree lens ids for a degree education level.
		/// </summary>
		/// <param name="degreeEducationLevelId">The degree education level id.</param>
		/// <returns></returns>
		private List<int> GetDegreeLensIdsForDegreeEducationLevel(long? degreeEducationLevelId)
		{
			if (!degreeEducationLevelId.HasValue)
				return new List<int>();

			return Repositories.Library.Query<DegreeEducationLevelLensMapping>().Where(x => x.DegreeEducationLevelId == degreeEducationLevelId).Select(x => x.LensId).Distinct().ToList();
		}

		/// <summary>
		/// Gets the degree lens ids for a program area.
		/// </summary>
		/// <param name="programAreaId">The program area id.</param>
		/// <returns></returns>
		private List<int> GetDegreeLensIdsForProgramArea(long? programAreaId)
		{
			if (!programAreaId.HasValue)
				return new List<int>();

			return Repositories.Library.Query<DegreeEducationLevelLensMapping>().Where(x => x.ProgramAreaId == programAreaId).Select(x => x.LensId).Distinct().ToList();
		}

		/// <summary>
		/// Removes the excluded jobs.
		/// </summary>
		/// <param name="resumeXml">The resume XML.</param>
		/// <param name="excludedJobs">The excluded jobs.</param>
		/// <returns></returns>
		private string RemoveExcludedJobs(string resumeXml, List<Focus.Core.Models.Career.Job> excludedJobs)
		{
			if (resumeXml.IsNullOrEmpty() || excludedJobs.IsNullOrEmpty())
				return resumeXml;

			var xDoc = XDocument.Parse(resumeXml);

			if (xDoc.IsNotNull())
			{
				var resumeElement = xDoc.XPathSelectElement("//ResDoc|resume");

				if (resumeElement.IsNotNull())
				{
					foreach (var excludedJob in excludedJobs.Where(excludedJob => excludedJob.JobId.HasValue))
					{
						resumeElement.RemoveElementsByXpath("experience/job[jobid='" + excludedJob.JobId + "']");
					}
				}

				return xDoc.ToString();
			}

			return resumeXml;
		}

		/// <summary>
		/// Tags the job description.
		/// </summary>
		/// <param name="lensService">The lens service.</param>
		/// <param name="posting">The job description.</param>
		/// <returns></returns>
		/// <exception cref="System.Exception">Exception tagging job posting</exception>
		private string TagPosting(LensService lensService, string posting)
		{
			if (String.IsNullOrEmpty(posting))
				return string.Empty;

			string tagResult;
			var commandBuilder = new LensCommandBuilder(RuntimeContext, lensService);

			try
			{
				var tagCommand = commandBuilder.BuildTagPostingCommand(posting);
				var lensClient = GetLensClient(lensService);
				tagResult = lensClient.ExecuteCommand(tagCommand);
			}
			catch (Exception ex)
			{
				throw new Exception("Exception tagging job posting", ex);
			}

			return tagResult;
		}

		/// <summary>
		/// Tags the resume.
		/// </summary>
		/// <param name="lensService">The Lens service.</param>
		/// <param name="posting">The posting.</param>
		/// <param name="postingFileExtension">The posting file extension.</param>
		/// <returns></returns>
		private string TagPosting(LensService lensService, byte[] posting, string postingFileExtension)
		{
			if (posting.Length == 0)
				return string.Empty;

			var lensClient = GetLensClient(lensService);

			return lensClient.TagBinaryData(posting, postingFileExtension, DocumentType.Posting);
		}

		/// <summary>
		/// Tags the resume.
		/// </summary>
		/// <param name="lensService">The Lens service.</param>
		/// <param name="resume">The resume.</param>
		/// <returns></returns>
		/// <exception cref="System.Exception">Exception tagging job posting</exception>
		private string TagResume(LensService lensService, string resume)
		{
			if (String.IsNullOrEmpty(resume))
				return string.Empty;

			string tagResult;
			var commandBuilder = new LensCommandBuilder(RuntimeContext, lensService);

			try
			{
				var tagCommand = commandBuilder.BuildTagResumeCommand(resume);
				var lensClient = GetLensClient(lensService);
				tagResult = lensClient.ExecuteCommand(tagCommand);
			}
			catch (Exception ex)
			{
				throw new Exception("Exception tagging job posting", ex);
			}

			return tagResult;
		}

		/// <summary>
		/// Tags the resume.
		/// </summary>
		/// <param name="lensService">The Lens service.</param>
		/// <param name="resume">The resume.</param>
		/// <param name="resumeFileExtension">The resume file extension.</param>
		/// <returns></returns>
		private string TagResume(LensService lensService, byte[] resume, string resumeFileExtension)
		{
			if (resume.Length == 0)
				return string.Empty;

			var lensClient = GetLensClient(lensService);
			return lensClient.TagBinaryData(resume, resumeFileExtension, DocumentType.Resume);
		}

		/// <summary>
		/// Canons the specified XML.
		/// </summary>
		/// <param name="lensService">The Lens service.</param>
		/// <param name="xml">The XML.</param>
		/// <returns></returns>
		private string Canon(LensService lensService, string xml)
		{
			var commandBuilder = new LensCommandBuilder(RuntimeContext, lensService);
			var command = commandBuilder.BuildCanonCommand(xml);
			var lensClient = GetLensClient(lensService);
			return lensClient.ExecuteCommand(command);
		}

		/// <summary>
		/// Matches the specified lens service.
		/// </summary>
		/// <param name="lensService">The Lens service.</param>
		/// <param name="resumeXml">The resume XML.</param>
		/// <param name="postingXml">The posting XML.</param>
		/// <returns></returns>
		private int Match(LensService lensService, string resumeXml, string postingXml)
		{
			var commandBuilder = new LensCommandBuilder(RuntimeContext, lensService);
			var command = commandBuilder.BuildMatchCommand(resumeXml, postingXml);
			var lensClient = GetLensClient(lensService);
			var result = lensClient.ExecuteCommand(command);

			var doc = new XmlDocument { PreserveWhitespace = true };
			doc.LoadXml(result);

			return doc.AsInt32("//match");
		}

		/// <summary>
		/// Registers the resume.
		/// </summary>
		/// <param name="lensService">The lens service.</param>
		/// <param name="resumeXml">The resume XML.</param>
		/// <param name="lensId">The Lens Id (Focus uses the PersonId as the Id for Lens)</param>
		private void RegisterResume(LensService lensService, string resumeXml, string lensId)
		{
			var commandBuilder = new LensCommandBuilder(RuntimeContext, lensService);
			var command = commandBuilder.BuildRegisterCommand(lensId, resumeXml, DocumentType.Resume);
			var lensClient = GetLensClient(lensService);
			lensClient.ExecuteCommand(command);
		}

		/// <summary>
		/// Unregisters the resume.
		/// </summary>
		/// <param name="lensService">The lens service.</param>
		/// <param name="lensIds">A list of ids for Lens (Focus uses the PersonId as the Id for Lens)</param>
		private void UnregisterResume(LensService lensService, List<string> lensIds)
		{
			var commandBuilder = new LensCommandBuilder(RuntimeContext, lensService);
			var command = commandBuilder.BuildUnregisterCommand(lensIds, DocumentType.Resume);
			var lensClient = GetLensClient(lensService);

			// When unregistering resume we do not check the lens output as the resume may have not been registered on lens already
			lensClient.ExecuteCommand(command, false);
		}

		/// <summary>
		/// Registers the posting.
		/// </summary>
		/// <param name="lensService">The Lens service.</param>
		/// <param name="postingXml">The posting XML.</param>
		/// <param name="postingId">The posting id.</param>
		private void RegisterPosting(LensService lensService, string postingXml, string postingId)
		{
			var commandBuilder = new LensCommandBuilder(RuntimeContext, lensService);
			var command = commandBuilder.BuildRegisterCommand(postingId, postingXml, DocumentType.Posting);
			var lensClient = GetLensClient(lensService);
			lensClient.ExecuteCommand(command);
		}

		/// <summary>
		/// Unregisters the posting.
		/// </summary>
		/// <param name="lensService">The Lens service.</param>
		/// <param name="postingId">The posting id.</param>
		private void UnregisterPosting(LensService lensService, string postingId)
		{
			var commandBuilder = new LensCommandBuilder(RuntimeContext, lensService);
			var command = commandBuilder.BuildUnregisterCommand(new List<string> { postingId }, DocumentType.Posting);
			var lensClient = GetLensClient(lensService);

			// When unregistering posting we do not check the lens output as the posting may have not been registered on lens already
			lensClient.ExecuteCommand(command, false);
		}

		/// <summary>
		/// Gets the lens client.
		/// </summary>
		/// <param name="service">The service.</param>
		/// <returns></returns>
		private ILensClient GetLensClient(LensService service)
		{
			if (service.Url.IsNotNullOrEmpty())
				return new HttpClient(service);

			return new TcpClient(service);
		}

		#endregion
	}
}
