﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using Focus.Core.Settings.Lens;
using HALens;
using HALens.Extensions;

#endregion



namespace Focus.Services.Repositories.Lens.Clients
{
	internal class HttpClient : ILensClient
	{
		private readonly Focus.Core.Settings.Lens.LensService _service;

		/// <summary>
		/// Initializes a new instance of the <see cref="HttpClient"/> class.
		/// </summary>
		/// <param name="service">The lens service definition.</param>
		public HttpClient(Focus.Core.Settings.Lens.LensService service)
		{
			_service = service;
		}

		/// <summary>
		/// Converts the binary data to ASCII text.
		/// </summary>
		/// <param name="data">The data.</param>
		/// <param name="fileExtension">The file extension.</param>
		/// <returns></returns>
		public string ConvertBinaryData(byte[] data, string fileExtension)
		{
			string result;

			var isDocx = fileExtension == "application/vnd.openxmlformats-officedocument.wordprocessingml.document";

			// docx files use UTF-8 encoding by default so if we have one of these files then set the encoding accordingly.
			// Otherwise use the encoding from the lens service.
			var encoding = isDocx
				? "UTF-8"
				: _service.CharacterSet;

			using (var session = new Session(_service.ConsumerKey, _service.Url, _service.Timeout, encoding))
			{
				var tagResponse = session.ConvertBinaryData(data, fileExtension);
				result = tagResponse.Data.AsString(encoding);

				if (isDocx) result = UnEscapeXml(result);
			}

			return result;
		}

		/// <summary>
		/// Executes the command.
		/// </summary>
		/// <param name="command">The command.</param>
		/// <param name="validateResponse">if set to <c>true</c> [validate response].</param>
		/// <returns></returns>
		/// <exception cref="System.Exception"></exception>
		public string ExecuteCommand(string command, bool validateResponse = true)
		{
			var result = "";

			try
			{
				using (var session = new Session(_service.ConsumerKey, _service.Url, _service.Timeout, _service.CharacterSet))
				{
					var requestMessage = new HALensMessage(command, MessageType.Xml);
					var responseMessage = session.SendMessage(requestMessage);
					result = responseMessage.Data.AsString(_service.CharacterSet);
				}
			}
			catch (HALensException)
			{
				if (validateResponse)
					throw;
			}

			if (!validateResponse)
				return result;

			// Check for Failure
			if (!String.IsNullOrEmpty(result) && (result.Contains("<fail") || result.Contains("failure>") || result.Contains("<error>")))
				throw new Exception(result);
			
			return result;
		}

		/// <summary>
		/// Tags the binary data.
		/// </summary>
		/// <param name="binaryData">The binary data.</param>
		/// <param name="fileExtension">The file extension.</param>
		/// <param name="documentType">Type of the document.</param>
		/// <returns></returns>
		public string TagBinaryData(byte[] binaryData, string fileExtension, Focus.Core.DocumentType documentType)
		{
			string result;
			var dataType = (documentType == Focus.Core.DocumentType.Posting) ? DocumentType.Posting : DocumentType.Resume;

			using (var session = new Session(_service.ConsumerKey, _service.Url, _service.Timeout, _service.CharacterSet))
			{
				var tagResponse = session.TagBinaryData(binaryData, fileExtension, dataType);
				result = tagResponse.Data.AsString(_service.CharacterSet);
			}

			return result;
		}

		/// <summary>
		/// Tags the binary data with HTML.
		/// </summary>
		/// <param name="binaryData">The binary data.</param>
		/// <param name="fileExtension">The file extension.</param>
		/// <param name="documentType">Type of the document.</param>
		/// <param name="html">The HTML.</param>
		/// <returns></returns>
		public string TagBinaryDataWithHtml(byte[] binaryData, string fileExtension, Focus.Core.DocumentType documentType, out string html)
		{
			if (binaryData.Length == 0)
			{
				html = string.Empty;
				return string.Empty;
			}

			string result;
			var dataType = (documentType == Focus.Core.DocumentType.Posting) ? DocumentType.Posting : DocumentType.Resume;

			using (var session = new Session(_service.ConsumerKey, _service.Url, _service.Timeout, _service.CharacterSet))
			{
				var tagResponse = session.TagBinaryDataWithHTM(binaryData, fileExtension, dataType);
				result = tagResponse.Data.AsString(_service.CharacterSet);
			}

			html = result.GetHtmlFromTaggedDocument();
			return result.RemoveHtmlFromTaggedDocument();
		}

		/// <summary>
		/// Uns the escape XML.
		/// </summary>
		/// <param name="xmlEscapedString">The XML escaped string.</param>
		/// <returns></returns>
		private string UnEscapeXml(string xmlEscapedString)
		{
			return xmlEscapedString
				.Replace("&amp;", "&")
				.Replace("&lt;", "<")
				.Replace("&gt;", ">")
				.Replace("&quot;", "\"")
				.Replace("&apos;", "\'");
		}
	}
}
