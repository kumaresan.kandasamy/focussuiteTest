﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using Focus.Core;

#endregion

namespace Focus.Services.Repositories.Lens.Clients
{
	internal interface ILensClient
	{
		/// <summary>
		/// Converts the binary data to ASCII text.
		/// </summary>
		/// <param name="data">The data.</param>
		/// <param name="fileExtension">The file extension.</param>
		/// <returns></returns>
		string ConvertBinaryData(byte[] data, string fileExtension);

		/// <summary>
		/// Executes the command.
		/// </summary>
		/// <param name="command">The command.</param>
		/// <param name="validateResponse">if set to <c>true</c> [validate response].</param>
		/// <returns></returns>
		string ExecuteCommand(string command, bool validateResponse = true);

		/// <summary>
		/// Tags the binary data.
		/// </summary>
		/// <param name="binaryData">The binary data.</param>
		/// <param name="fileExtension">The file extension.</param>
		/// <param name="documentType">Type of the document.</param>
		/// <returns></returns>
		string TagBinaryData(byte[] binaryData, string fileExtension, DocumentType documentType);

		/// <summary>
		/// Tags the binary data with HTML.
		/// </summary>
		/// <param name="binaryData">The binary data.</param>
		/// <param name="fileExtension">The file extension.</param>
		/// <param name="documentType">Type of the document.</param>
		/// <param name="html">The HTML.</param>
		/// <returns></returns>
		string TagBinaryDataWithHtml(byte[] binaryData, string fileExtension, DocumentType documentType, out string html);
	}
}
