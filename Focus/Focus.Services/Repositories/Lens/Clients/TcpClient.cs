﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Text;
using Focus.Core;
using Focus.Core.Settings.Lens;

using com.bgt.lens;

#endregion



namespace Focus.Services.Repositories.Lens.Clients
{
	internal class TcpClient : ILensClient
	{
		private readonly LensService _service;
		
		/// <summary>
		/// Initializes a new instance of the <see cref="TcpClient" /> class.
		/// </summary>
		/// <param name="service">The lens service definition.</param>
		public TcpClient(LensService service)
		{
			_service = service;
		}

		/// <summary>
		/// Converts the binary data to ASCII text.
		/// </summary>
		/// <param name="data">The data.</param>
		/// <param name="fileExtension">The file extension.</param>
		/// <returns></returns>
		public string ConvertBinaryData(byte[] data, string fileExtension)
		{
			string result;

			var isDocx = fileExtension == "application/vnd.openxmlformats-officedocument.wordprocessingml.document";

			// docx files use UTF-8 encoding by default so if we have one of these files then set the encoding accordingly.
			// Otherwise use the encoding from the lens service.
			var encoding = isDocx
				? Encoding.GetEncoding("UTF-8")
				: Encoding.GetEncoding(_service.CharacterSet);

			var session = MSLens.CreateSession(_service.Host, (uint)_service.PortNumber, encoding);

			try
			{
				session.SetEnableTransactionTimeout(true);
				session.SetTransactionTimeout(1000 * (ulong)_service.Timeout);
				session.Open();

				result = session.ConvertBinaryData(data, fileExtension).GetMessageData();

				if (isDocx) result = UnEscapeXml(result);
			}
			finally
			{
				if (session != null && session.IsOpen())
					session.Close();
			}

			return result;
		}

		/// <summary>
		/// Executes the command.
		/// </summary>
		/// <param name="command">The command.</param>
		/// <param name="validateResponse">if set to <c>true</c> [validate response].</param>
		/// <returns></returns>
		/// <exception cref="System.Exception"></exception>
		public string ExecuteCommand(string command, bool validateResponse = true)
		{
			string result;
			var session = MSLens.CreateSession(_service.Host, (uint)_service.PortNumber, Encoding.GetEncoding(_service.CharacterSet));

			try
			{
				session.SetEnableTransactionTimeout(true);
				session.SetTransactionTimeout(1000 * (ulong)_service.Timeout);
				session.Open();

				var messageRequest = LensMessage.Create(command, LensMessage.XML_TYPE);
				var messageResult = session.SendMessage(messageRequest);
				result = messageResult.GetMessageData();

				if (validateResponse)
				{
					// Check for Failure
					if (!String.IsNullOrEmpty(result) && (result.Contains("<fail") || result.Contains("failure>") || result.Contains("<error>")))
						throw new Exception(result);
				}
			}
			finally
			{
				if (session != null && session.IsOpen())
					session.Close();
			}

			return result;
		}

		/// <summary>
		/// Tags the binary data.
		/// </summary>
		/// <param name="binaryData">The binary data.</param>
		/// <param name="fileExtension">The file extension.</param>
		/// <param name="documentType">Type of the document.</param>
		/// <returns></returns>
		public string TagBinaryData(byte[] binaryData, string fileExtension, DocumentType documentType)
		{
			string result;
			var dataType = (documentType == DocumentType.Posting) ? 'P' : 'R';

			var session = MSLens.CreateSession(_service.Host, (uint)_service.PortNumber, Encoding.GetEncoding(_service.CharacterSet));

			try
			{
				session.SetEnableTransactionTimeout(true);
				session.SetTransactionTimeout(1000 * (ulong)_service.Timeout);
				session.Open();

				result = session.TagBinaryData(binaryData, fileExtension, dataType).GetMessageData();
			}
			finally
			{
				if (session != null && session.IsOpen())
					session.Close();
			}

			return result;
		}

		/// <summary>
		/// Tags the binary data with HTML.
		/// </summary>
		/// <param name="binaryData">The binary data.</param>
		/// <param name="fileExtension">The file extension.</param>
		/// <param name="documentType">Type of the document.</param>
		/// <param name="html">The HTML.</param>
		/// <returns></returns>
		public string TagBinaryDataWithHtml(byte[] binaryData, string fileExtension, DocumentType documentType, out string html)
		{
			if (binaryData.Length == 0)
			{
				html = string.Empty;
				return string.Empty;
			}

			string result = "";
			var dataType = (documentType == DocumentType.Posting) ? 'P' : 'R';

			var session = MSLens.CreateSession(_service.Host, (uint)_service.PortNumber, Encoding.GetEncoding(_service.CharacterSet));

			try
			{
				session.SetEnableTransactionTimeout(true);
				session.SetTransactionTimeout(1000 * (ulong)_service.Timeout);
				session.Open();

				result = session.TagBinaryDataWithHTM(binaryData, fileExtension, dataType).GetMessageData();
			}
			catch (Exception ex)
			{
				var test = ex.Message;
			}
			finally
			{
				if (session != null && session.IsOpen())
					session.Close();
			}

			html = result.GetHtmlFromTaggedDocument();
			return result.RemoveHtmlFromTaggedDocument();
		}

		/// <summary>
		/// Uns the escape XML.
		/// </summary>
		/// <param name="xmlEscapedString">The XML escaped string.</param>
		/// <returns></returns>
		private string UnEscapeXml(string xmlEscapedString)
		{
			return xmlEscapedString
				.Replace("&amp;", "&")
				.Replace("&lt;", "<")
				.Replace("&gt;", ">")
				.Replace("&quot;", "\"")
				.Replace("&apos;", "\'");
		}
	}
}
