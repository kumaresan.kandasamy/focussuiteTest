﻿#region Copyright © 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using Focus.Core.Settings.Interfaces;
using Focus.Core.Settings.Lens;
using Focus.Core;
using Focus.Core.Criteria.CandidateSearch;
using Focus.Core.Models.Career;
using Focus.Services.Core.Extensions;
using DistanceUnits = Focus.Core.DistanceUnits;

using Framework.Core;

#endregion

namespace Focus.Services.Repositories.Lens
{
	internal class LensCommandBuilder
	{
		private readonly LensService _lensService;
		private readonly string _xmlHeader;

    private static IEnumerable<DaysOfWeek> DaysOfTheWeek
    {
      get
      {
        yield return DaysOfWeek.Monday;
        yield return DaysOfWeek.Tuesday;
        yield return DaysOfWeek.Wednesday;
        yield return DaysOfWeek.Thursday;
        yield return DaysOfWeek.Friday;
        yield return DaysOfWeek.Saturday;
        yield return DaysOfWeek.Sunday;
      }
    }

		/// <summary>
		/// Initializes a new instance of the <see cref="LensCommandBuilder" /> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <param name="lensService">The lens service.</param>
		public LensCommandBuilder(IRuntimeContext runtimeContext, LensService lensService)
		{
			RuntimeContext = runtimeContext;
			_lensService = lensService;

			_xmlHeader = string.Format("<?xml version='1.0' encoding='{0}'?>", Encoding.GetEncoding(lensService.CharacterSet).WebName);
		}

		private IRuntimeContext RuntimeContext { get; set; }

		private IAppSettings AppSettings { get { return RuntimeContext.AppSettings; } }
		private IRepositories Repositories { get { return RuntimeContext.Repositories; } }
		private IHelpers Helpers { get { return RuntimeContext.Helpers; } }		

		/// <summary>
		/// Builds the licensed command.
		/// </summary>
		/// <returns></returns>
		public string BuildIsLicensedCommand()
		{
			const string command = "<bgtcmd><tag>\nBurning Glass Technologies\nOne Faneuil Hall Market, 4th Floor\nBoston, MA 02109</tag></bgtcmd>";

			var xDoc = new XmlDocument { PreserveWhitespace = true };
			xDoc.LoadXml(_xmlHeader + command);

			return xDoc.OuterXml;
		}

		/// <summary>
		/// Builds the tag command.
		/// </summary>
		/// <param name="dataToTag">The data to tag.</param>
		/// <returns></returns>
		public string BuildTagPostingCommand(string dataToTag)
		{
			const string command = "<bgtcmd><tag type='posting'></tag></bgtcmd>";

			var xDoc = new XmlDocument {PreserveWhitespace = true};
			xDoc.LoadXml(_xmlHeader + command);

			var tagNode = xDoc.SelectSingleNode("//tag");
			if (tagNode != null) tagNode.AppendChild(xDoc.CreateCDataSection(dataToTag));

			return xDoc.OuterXml;
		}

		/// <summary>
		/// Builds the tag resume command.
		/// </summary>
		/// <param name="dataToTag">The data to tag.</param>
		/// <returns></returns>
		public string BuildTagResumeCommand(string dataToTag)
		{
			const string command = "<bgtcmd><tag type='resume'></tag></bgtcmd>";

			var xDoc = new XmlDocument { PreserveWhitespace = true };
			xDoc.LoadXml(_xmlHeader + command);

			var tagNode = xDoc.SelectSingleNode("//tag");
			if (tagNode != null) tagNode.AppendChild(xDoc.CreateCDataSection(dataToTag));

			return xDoc.OuterXml;
		}

		/// <summary>
		/// Builds the resume search command.
		/// </summary>
		/// <param name="postingXml">The posting XML.</param>
		/// <param name="resumeXml">The resume XML.</param>
		/// <param name="criteria">The criteria.</param>
		/// <param name="culture">The culture.</param>
		/// <param name="postingDate">The posting date.</param>
		/// <returns></returns>
		public string BuildResumeSearchCommand(string postingXml, string resumeXml, CandidateSearchCriteria criteria, string culture, DateTime? postingDate)
		{
			var command = new StringBuilder("<bgtcmd><search type='resume' vendor='{0}' count='{1}' min='{2}'>{3}{4}{5}" +
			                                "<include var='id' /><include var='name' /><include var='yrsexp' /><include var='jobs' /><include var='special_{6}' /><include var='special_{7}' />" +
			                                "<include var='xpath://ResDoc/resume/statements/personal/preferences/contact_info_visible'/>");

      command.Append("<include var='xpath://ResDoc/resume/contact/name' />");

			if (RuntimeContext.AppSettings.ShowBrandingStatement)
				command.Append("<include var='xpath://ResDoc/special/branding'/>");

			if (RuntimeContext.AppSettings.ShowHomeLocationInSearchResults)
				command.Append("<include var='xpath://ResDoc/resume/contact/address/city'/>" +
				               "<include var='xpath://ResDoc/resume/contact/address/state'/>");
			
			command.Append("</search></bgtcmd>");											 
			
			var criteriaBuilder = new StringBuilder("");
			var filters = new List<string>();

			#region Keywords

			if (criteria.KeywordCriteria.IsNotNull() && criteria.KeywordCriteria.Keywords.IsNotNull())
			{
				var keywords = MapKeywords(criteria.KeywordCriteria);
				if (keywords.Count > 0)
					filters.AddRange(keywords);
			}

			#endregion

			// Skip the additional criteria
		  var additionalCriteria = criteria.AdditionalCriteria;
      if (additionalCriteria.IsNotNull())
			{
				#region Add Distance to command

				if (criteria.AdditionalCriteria.LocationCriteria.IsNotNull())
				{
					criteriaBuilder.AppendFormat("<distance max='{0}' units='{1}' unknown='{2}' />",
														criteria.AdditionalCriteria.LocationCriteria.Distance,
														MapDistanceUnit(criteria.AdditionalCriteria.LocationCriteria.DistanceUnits),
														MapLocationUnknown(criteria.AdditionalCriteria.LocationCriteria.ExcludeUnknownLocations));

					// Update the postcode of the job or resume to trick lens to search from the correct location
					if(postingXml.IsNotNullOrEmpty())
						postingXml = UpdatePostingsPostcode(postingXml, criteria.AdditionalCriteria.LocationCriteria.PostalCode);

					if(resumeXml.IsNotNullOrEmpty())
						resumeXml = UpdateResumesPostcode(resumeXml, criteria.AdditionalCriteria.LocationCriteria.PostalCode);
				}

				#endregion

				#region Add Education Level

				if (criteria.AdditionalCriteria.EducationCriteria.IsNotNull())
				{
					if (criteria.AdditionalCriteria.EducationCriteria.EducationLevel > 0)
						criteriaBuilder.Append(MapCustomFilter(_lensService.EducationLevelCustomFilterTag, MapEducation(criteria.AdditionalCriteria.EducationCriteria.EducationLevel)));

					if (criteria.AdditionalCriteria.EducationCriteria.EducationLevels.IsNotNullOrEmpty() && _lensService.EducationLevelCustomFilterTag.IsNotNullOrEmpty())
						criteriaBuilder.Append(MapCustomFilter(_lensService.EducationLevelCustomFilterTag, criteria.AdditionalCriteria.EducationCriteria.EducationLevels.Select(educationLevel => ((int) educationLevel).ToString()).ToList()));
				}

				#endregion

				#region Languages

				var proficiencyList = criteria.AdditionalCriteria.LanguagesWithProficiencies;
        
				if (proficiencyList.IsNotNull())
				{
					var languagesWithProficiencies = new List<string>();
					proficiencyList.LanguageProficiencies.ForEach(pair =>
					{
						var language = pair.Language;
						var proficiencyId = pair.Proficiency;
						var proficiencyPosition = Helpers.Lookup.GetLookup(LookupTypes.LanguageProficiencies).Where(x => x.Id == proficiencyId).Select(x => x.DisplayOrder).FirstOrDefault();
						var allFilters = Helpers.Lookup.GetLookup(LookupTypes.LanguageProficiencies)
							                             .Where(x => x.DisplayOrder >= proficiencyPosition && x.ExternalId != null)
													 							   .Select(x => string.Concat(language, x.ExternalId))
														 							 .ToList();
						languagesWithProficiencies.Add(allFilters.Any() ? string.Join(" or ", allFilters) : language);
					});
					filters.Add(MapLanguages(languagesWithProficiencies, criteria.AdditionalCriteria.LanguagesWithProficiencies.LanguageSearchType));
				}
				else if (criteria.AdditionalCriteria.Languages.IsNotNullOrEmpty())
				{
          filters.Add(MapLanguages(criteria.AdditionalCriteria.Languages));
				}

				#endregion
				
				#region Licences

				if (criteria.AdditionalCriteria.Licences.IsNotNullOrEmpty())
					filters.Add(MapCertificateLicense(criteria.AdditionalCriteria.Licences));

				#endregion

				#region Certification

				if (criteria.AdditionalCriteria.Certifications.IsNotNullOrEmpty())
					filters.Add(MapCertificateLicense(criteria.AdditionalCriteria.Certifications));

				#endregion

				#region Education Requirements

				if (criteria.AdditionalCriteria.JobTypesConsidered.IsNotNull() && _lensService.JobTypesCustomFilterTag.IsNotNullOrEmpty())
				{
					var jobTypeFlags = GetAllPossibleIntsForFlag((int) criteria.AdditionalCriteria.JobTypesConsidered, (int) JobTypes.All);
					
					if(jobTypeFlags.IsNotNullOrEmpty())
						criteriaBuilder.Append(MapCustomFilter(_lensService.JobTypesCustomFilterTag, jobTypeFlags, true));
				}

        var educationCriteria = criteria.AdditionalCriteria.EducationCriteria;
        if (educationCriteria.IsNotNull())
        {
				  if (criteria.AdditionalCriteria.EducationCriteria.MinimumGPA.IsNotNull() && _lensService.GPACustomFilterTag.IsNotNullOrEmpty())
				  {
					  criteriaBuilder.Append(MapCustomFilter(_lensService.GPACustomFilterTag, (criteria.AdditionalCriteria.EducationCriteria.MinimumGPA).ToString(), "5.0", criteria.AdditionalCriteria.EducationCriteria.AlsoSearchGPANotSpecified));
				  }

				  if (criteria.AdditionalCriteria.EducationCriteria.MinimumExperienceMonths != 0 && _lensService.MonthsExperienceCustomFilterTag.IsNotNullOrEmpty())
				  {
					  criteriaBuilder.Append(MapCustomFilterWithNoMax(_lensService.MonthsExperienceCustomFilterTag, criteria.AdditionalCriteria.EducationCriteria.MinimumExperienceMonths.ToString()));
				  }

				  if ((criteria.AdditionalCriteria.EducationCriteria.CurrentlyEnrolledGraduate || criteria.AdditionalCriteria.EducationCriteria.CurrentlyEnrolledUndergraduate) && _lensService.EnrollmentStatusCustomFilterTag.IsNotNullOrEmpty())
				  {
					  var enrollmentList = new List<string>();

					  if (criteria.AdditionalCriteria.EducationCriteria.CurrentlyEnrolledGraduate)
						  enrollmentList.Add("14");

					  if (criteria.AdditionalCriteria.EducationCriteria.CurrentlyEnrolledUndergraduate)
					  {
						  switch (criteria.AdditionalCriteria.EducationCriteria.MinimumUndergraduateLevel)
						  {
							  case (SchoolStatus.Senior):
								  enrollmentList.Add("13");
								  break;

							  case (SchoolStatus.Junior):
								  enrollmentList.AddRange(new List<string>{"12","13"});
								  break;

							  case (SchoolStatus.Sophomore):
								  enrollmentList.AddRange(new List<string> { "8","11","12", "13" });
								  break;

							  default:
								  enrollmentList.AddRange(new List<string> {"6", "7", "8", "9", "11", "12", "13"});
								  break;
						  }
					  }

					  criteriaBuilder.Append(MapCustomFilter(_lensService.EnrollmentStatusCustomFilterTag, enrollmentList));
				  }

          if (educationCriteria.ProgramsOfStudy.IsNotNull() && _lensService.ProgramOfStudyCustomFilterTag.IsNotNullOrEmpty())
				  {
            var degreeIds = educationCriteria.ProgramsOfStudy.Where(p => p.Key.IsNotNullOrEmpty() && !p.Key.StartsWith("-")).Select(p => p.Key).ToList();

            if (educationCriteria.ExtraDegreeLevelEducationIds.IsNotNullOrEmpty())
              degreeIds.AddRange(educationCriteria.ExtraDegreeLevelEducationIds);

            if (degreeIds.Any())
					    criteriaBuilder.Append(MapCustomFilter(_lensService.ProgramOfStudyCustomFilterTag, degreeIds));
				  }

          if (criteria.AdditionalCriteria.EducationCriteria.InternshipSkills.IsNotNullOrEmpty())
						filters.Add(MapInternshipSkills(criteria.AdditionalCriteria.EducationCriteria.InternshipSkills));

					if (criteria.AdditionalCriteria.EducationCriteria.EnrollmentStatus.IsNotNull() && criteria.AdditionalCriteria.EducationCriteria.EnrollmentStatus != SchoolStatus.NA 
							&& _lensService.EnrollmentStatusCustomFilterTag.IsNotNullOrEmpty())
						criteriaBuilder.Append(MapCustomFilter(_lensService.EnrollmentStatusCustomFilterTag, ((int)criteria.AdditionalCriteria.EducationCriteria.EnrollmentStatus).ToString()));

					if(criteria.AdditionalCriteria.EducationCriteria.MonthsUntilExpectedCompletion.HasValue && _lensService.DegreeCompletionDateFilterTag.IsNotNullOrEmpty())
					{
						var includeAlumni = (criteria.AdditionalCriteria.EducationCriteria.IncludeAlumni.IsNotNull()) && criteria.AdditionalCriteria.EducationCriteria.IncludeAlumni;
						criteriaBuilder.Append(MapCompletionDateCustomFilter(criteria.AdditionalCriteria.EducationCriteria.MonthsUntilExpectedCompletion.Value, includeAlumni, postingDate));
					}
        }
				#endregion
				
				// Limits us for the moment - should pass a list in if we want more citizenships
				if (criteria.AdditionalCriteria.StatusCriteria.IsNotNull())
				{
					#region StatusSearchCriteria.USCitizen

					var usCitizen = MapCitizenship(criteria.AdditionalCriteria.StatusCriteria);
					if (usCitizen != "")
						filters.Add(usCitizen);
					
					#endregion

					#region Veteran

					// If there is no custom filter defined there is nothing we can do, got the value of one from Kishore's version
					if (criteria.AdditionalCriteria.StatusCriteria.Veteran && _lensService.VeteranStatusCustomFilterTag.IsNotNullOrEmpty())
						criteriaBuilder.Append(MapCustomFilter(_lensService.VeteranStatusCustomFilterTag, "1"));

					#endregion
				}

				#region Driving Licence

				if (criteria.AdditionalCriteria.DrivingLicenceCriteria.IsNotNull())
				{
					
					#region Driving Licence Class

					if (criteria.AdditionalCriteria.DrivingLicenceCriteria.DrivingLicenceClassId.HasValue && criteria.AdditionalCriteria.DrivingLicenceCriteria.DrivingLicenceClassId > 0 && _lensService.DrivingLicenceClassCustomFilterTag.IsNotNullOrEmpty())
					{
						var drivingLicenceClassExternalId = Helpers.Lookup.GetLookup(LookupTypes.DrivingLicenceClasses).Where(x => x.Id == criteria.AdditionalCriteria.DrivingLicenceCriteria.DrivingLicenceClassId).Select(x => x.ExternalId).SingleOrDefault();
						if (drivingLicenceClassExternalId.IsNotNullOrEmpty())
							criteriaBuilder.Append(MapCustomFilter(_lensService.DrivingLicenceClassCustomFilterTag, drivingLicenceClassExternalId));
					}

					#endregion

					#region Driving Licence Endorsements

					if (criteria.AdditionalCriteria.DrivingLicenceCriteria.DrivingLicenceEndorsementsIds.IsNotNullOrEmpty() && _lensService.DrivingLicenceEndorsementCustomFilterTag.IsNotNullOrEmpty())
					{
						var drivingLicenceEndorsements = criteria.AdditionalCriteria.DrivingLicenceCriteria.DrivingLicenceEndorsementsIds.Select(drivingLicenceEndorsementId => Helpers.Lookup.GetLookup(LookupTypes.DrivingLicenceEndorsements).Where(x => x.Id == drivingLicenceEndorsementId && x.CustomFilterId.IsNotNull()).Select(x => x.CustomFilterId.ToString()).SingleOrDefault()).Where(drivingLicenceEndorsementExternalId => drivingLicenceEndorsementExternalId.IsNotNullOrEmpty()).ToList();
						criteriaBuilder.Append(MapCustomFilter(_lensService.DrivingLicenceEndorsementCustomFilterTag, drivingLicenceEndorsements));
					}

					#endregion
				}

				#endregion

				#region Availability

				if (criteria.AdditionalCriteria.AvailabilityCriteria.IsNotNull())
				{
					
					#region Work Overtime

					if (criteria.AdditionalCriteria.AvailabilityCriteria.WorkOvertime.HasValue && _lensService.WorkOvertimeCustomFilterTag.IsNotNullOrEmpty())
					{
						var workOvertime = (criteria.AdditionalCriteria.AvailabilityCriteria.WorkOvertime.Value) ? "1" : "0";
						criteriaBuilder.Append(MapCustomFilter(_lensService.WorkOvertimeCustomFilterTag, workOvertime));
					}

					#endregion

          #region Willing to relocate

          if (criteria.AdditionalCriteria.AvailabilityCriteria.WillingToRelocate.HasValue && _lensService.RelocationCustomFilterTag.IsNotNullOrEmpty())
          {
            var willingToRelocate = (criteria.AdditionalCriteria.AvailabilityCriteria.WillingToRelocate.Value) ? "1" : "0";
            criteriaBuilder.Append(MapCustomFilter(_lensService.RelocationCustomFilterTag, willingToRelocate));
          }

          #endregion

					#region Work Shift

					if (criteria.AdditionalCriteria.AvailabilityCriteria.NormalWorkShiftId.HasValue && criteria.AdditionalCriteria.AvailabilityCriteria.NormalWorkShiftId > 0 && _lensService.ShiftTypeCustomFilterTag.IsNotNullOrEmpty())
					{
						var normalWorkShiftExternalId = Helpers.Lookup.GetLookup(LookupTypes.WorkShifts).Where(x => x.Id == criteria.AdditionalCriteria.AvailabilityCriteria.NormalWorkShiftId).Select(x => x.ExternalId).SingleOrDefault();
						if(normalWorkShiftExternalId.IsNotNullOrEmpty())
							criteriaBuilder.Append(MapCustomFilter(_lensService.ShiftTypeCustomFilterTag, normalWorkShiftExternalId));
					}

					#endregion

					#region Work Type

					if (criteria.AdditionalCriteria.AvailabilityCriteria.WorkTypeId.HasValue && criteria.AdditionalCriteria.AvailabilityCriteria.WorkTypeId > 0 && _lensService.WorkTypeCustomFilterTag.IsNotNullOrEmpty())
					{
						var workTypeExternalId = Helpers.Lookup.GetLookup(LookupTypes.Durations).Where(x => x.Id == criteria.AdditionalCriteria.AvailabilityCriteria.WorkTypeId).Select(x => x.ExternalId).SingleOrDefault();
						if (workTypeExternalId.IsNotNullOrEmpty())
							criteriaBuilder.Append(MapCustomFilter(_lensService.WorkTypeCustomFilterTag, workTypeExternalId));
					}

					#endregion

					#region Work Week

					if (criteria.AdditionalCriteria.AvailabilityCriteria.WorkWeekId.HasValue && criteria.AdditionalCriteria.AvailabilityCriteria.WorkWeekId > 0 && _lensService.WorkWeekCustomFilterTag.IsNotNullOrEmpty())
					{
						var workWeekExternalId = Helpers.Lookup.GetLookup(LookupTypes.WorkWeeks).Where(x => x.Id == criteria.AdditionalCriteria.AvailabilityCriteria.WorkWeekId).Select(x => x.ExternalId).SingleOrDefault();
						if (workWeekExternalId.IsNotNullOrEmpty())
							criteriaBuilder.Append(MapCustomFilter(_lensService.WorkWeekCustomFilterTag, workWeekExternalId));
					}

					#endregion
				}
				
				#endregion

				#region Resume industries

				if (criteria.AdditionalCriteria.IndustriesCriteria != null
					&& criteria.AdditionalCriteria.IndustriesCriteria.IndustryCodes.IsNotNullOrEmpty()
					&& _lensService.ResumeIndustriesCustomFilterTag.IsNotNullOrEmpty())
				{
					criteriaBuilder.Append(MapCustomFilter(_lensService.ResumeIndustriesCustomFilterTag, criteria.AdditionalCriteria.IndustriesCriteria.IndustryCodes));
				}

				#endregion

        #region Home Based Job Seekers

        if (criteria.AdditionalCriteria.HomeBasedJobSeekers)
        {
          if (_lensService.HomeBasedTag.IsNotNullOrEmpty())
            criteriaBuilder.Append(MapCustomFilter(_lensService.HomeBasedTag, "1"));
        }

        #endregion


        #region NCRC Level

        if (additionalCriteria.NCRCLevel.IsNotNullOrZero())
        {
          if (_lensService.NCRCLevelTag.IsNotNullOrEmpty())
          {
            var careerLevelExternalId = Helpers.Lookup.GetLookup(LookupTypes.NCRCLevel).Where(x => x.Id == additionalCriteria.NCRCLevel).Select(x => x.ExternalId).FirstOrDefault();
            if (careerLevelExternalId.IsNotNullOrEmpty())
              criteriaBuilder.Append(MapCustomFilter(_lensService.NCRCLevelTag, careerLevelExternalId));
          }
        }

        #endregion
      }

      #region Scope Filter

			if (criteria.ScopeSet.IsNotNullOrEmpty() && _lensService.JobseekerIdCustomFilterTag.IsNotNullOrEmpty())
				criteriaBuilder.Append(MapCustomFilter(_lensService.JobseekerIdCustomFilterTag, criteria.ScopeSet));

			#endregion

			#region Registered From Filter

			if (criteria.CandidateRegisteredFrom.HasValue && _lensService.RegisteredOnCustomFilterTag.IsNotNullOrEmpty())
			{
				var toJulianDate = ToJulianDate(DateTime.Now.Date.AddDays(1));
				var fromJulianDate = ToJulianDate(criteria.CandidateRegisteredFrom.Value);

				criteriaBuilder.Append(MapCustomFilter(_lensService.RegisteredOnCustomFilterTag, fromJulianDate.ToString(), toJulianDate.ToString()));
			}

			#endregion

			#region Last Modified On Filter

			if (criteria.CandidateUpdatedFrom.HasValue && _lensService.ModifiedOnCustomFilterTag.IsNotNullOrEmpty())
			{
				var toJulianDate = ToJulianDate(DateTime.Now.Date.AddDays(1));
				var fromJulianDate = ToJulianDate(criteria.CandidateUpdatedFrom.Value);

				criteriaBuilder.Append(MapCustomFilter(_lensService.ModifiedOnCustomFilterTag, fromJulianDate.ToString(), toJulianDate.ToString()));
			}

			#endregion

			if (filters.Count > 0)
			{
				var allFilters = String.Join(" and ", filters.ToArray());

				var xDoc = new XmlDocument {PreserveWhitespace = true};
				xDoc.LoadXml(_xmlHeader + "<filter var='keyword'></filter>");
				
				var filterNode = xDoc.SelectSingleNode("//filter");
				if (filterNode != null)
				{
					filterNode.AppendChild(xDoc.CreateCDataSection(allFilters));

					criteriaBuilder.AppendFormat(filterNode.OuterXml);
				}
			}

			var searchVendor = (_lensService.SearchAllVendors) ? "" : _lensService.Vendor;

			return ValidatedCommand(String.Format(command.ToString(), searchVendor, criteria.MinimumDocumentCount, criteria.MinimumScore, criteriaBuilder, postingXml, resumeXml, _lensService.VeteranStatusCustomFilterTag, _lensService.NCRCLevelTag));
		}

		/// <summary>
		/// Builds the search resumes like this command.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		public string BuildSearchResumesLikeThisCommand(CandidateSearchCriteria criteria)
		{
			var command = new StringBuilder("<bgtcmd><search type='resume' vendor='{0}' count='{1}' min='{2}'>" +
			                                "<doc type='resume' vendor='{0}' id='{3}'/>" +
																			"<include var='id' /><include var='name' /><include var='yrsexp' /><include var='jobs' /><include var='special_{4}' /><include var='special_{5}' />");

		  command.Append("<include var='xpath://ResDoc/resume/contact/name' />");

      if (RuntimeContext.AppSettings.ShowContactDetails == ShowContactDetails.Full)
        command.Append("<include var='xpath://ResDoc/resume/statements/personal/preferences/contact_info_visible'/>");

			if (RuntimeContext.AppSettings.ShowBrandingStatement)
				command.Append("<include var='xpath://ResDoc/special/branding'/>");

			if (RuntimeContext.AppSettings.ShowHomeLocationInSearchResults)
				command.Append("<include var='xpath://ResDoc/resume/contact/address/city'/>" +
				               "<include var='xpath://ResDoc/resume/contact/address/state'/>");
			
			command.Append("</search></bgtcmd>");

			var searchVendor = (_lensService.SearchAllVendors) ? "" : _lensService.Vendor;

			return ValidatedCommand(String.Format(command.ToString(), searchVendor, criteria.MinimumDocumentCount, criteria.MinimumScore, criteria.CandidatesLikeThisSearchCriteria.CandidateId, _lensService.VeteranStatusCustomFilterTag, _lensService.NCRCLevelTag));
		}

		/// <summary>
		/// Builds the posting search command.
		/// </summary>
		/// <param name="resumeXml">The resume XML.</param>
		/// <param name="postingXml">The posting XML.</param>
		/// <param name="searchCriteria">The search criteria.</param>
		/// <param name="culture">The culture.</param>
		/// <param name="programOfStudyRonets">The program of study ronets.</param>
		/// <param name="internshipCategoryLensFilterIds">The internship category lens filter ids.</param>
		/// <param name="onetCodes">The onet codes.</param>
		/// <param name="industryCodes">The industry codes.</param>
		/// <param name="programOfStudyLensIds">The program of study lens ids.</param>
		/// <param name="isSpideredSearch">Whether this is for searching for spidered jobs</param>
		/// <returns></returns>
		public string BuildPostingSearchCommand(string resumeXml, string postingXml, SearchCriteria searchCriteria, string culture, List<string> programOfStudyRonets, List<int> internshipCategoryLensFilterIds,
                                            List<string> onetCodes, List<string> industryCodes, List<int> programOfStudyLensIds, bool isSpideredSearch = false)
		{
			const string command = "<bgtcmd><search type='posting' vendor='{0}' count='{1}' min='{2}'>" +
                             "{3}{4}{5}" +
                             "<include var='id'/>" +
                             "<include var='title'/>" +
														 "<include var='xpath://special/jobemployer'/>" +
														 "<include var='special_{6}'/>" +
                             "<include var='special_{7}'/>" +
                             "<include var='special_{8}'/>" +
                             "<include var='xpath://address'/>" +
														 "<include var='xpath://posting/background/experience'/>" +
                             "<include var='xpath://special/minexperiencelevel'/>" +
                             "<include var='xpath://special/screening'/>" +
                             "{9}" +
                             "</search></bgtcmd>";

			if (searchCriteria.IsNull()) return null;

			var criteriaBuilder = new StringBuilder("");
			var filters = new List<string>();
			var minimumStarMatch = "";
			var maximumDocumentCount = "";

			#region Location

			var stateFilterApplied = false;
		  var msaFilterApplied = false;

			if (searchCriteria.JobLocationCriteria.IsNotNull())
			{
				var locationCriteria = searchCriteria.JobLocationCriteria;

				#region Distance (radius) from postcode/zipcode filter

				if (locationCriteria.Radius.IsNotNull())
				{
          var distance = "<distance max='{0}' units='{1}' unknown='exclude'/>";
				  if (AppSettings.NoFixedLocation && !isSpideredSearch)
            distance = "<distance max='{0}' units='{1}' unknown='include'/>";
					
					var distanceUnits = locationCriteria.Radius.DistanceUnits == Focus.Core.Models.Career.DistanceUnits.Kilometres ? "km" : "miles";

					criteriaBuilder.AppendLine(String.Format(distance, locationCriteria.Radius.Distance, distanceUnits));

					if (postingXml.IsNotNullOrEmpty())
						postingXml = UpdatePostingsPostcode(postingXml, locationCriteria.Radius.PostalCode);

					if (resumeXml.IsNotNullOrEmpty())
						resumeXml = UpdateResumesPostcode(resumeXml, locationCriteria.Radius.PostalCode);
				}

				#endregion

				#region Area filter

				if (locationCriteria.Area.IsNotNull())
				{
					#region State filter

					if (locationCriteria.Area.StateId.IsNotNull())
					{
						var customFilterId = (locationCriteria.SelectedStateInCriteria.IsNotNullOrEmpty())
						                     	? Helpers.Lookup.GetLookup(LookupTypes.States).Where(x => x.ExternalId == locationCriteria.SelectedStateInCriteria).Select(x => x.CustomFilterId).FirstOrDefault()
						                     	: Helpers.Lookup.GetLookup(LookupTypes.States).Where(x => x.Id == locationCriteria.Area.StateId).Select(x => x.CustomFilterId).FirstOrDefault();

						if (customFilterId.IsNotNull() && customFilterId > 0)
						{
							criteriaBuilder.Append(AppSettings.NoFixedLocation && !isSpideredSearch
                ? MapCustomFilter(_lensService.StateCodeCustomFilterTag, customFilterId.ToString(),true)
                : MapCustomFilter(_lensService.StateCodeCustomFilterTag, customFilterId.ToString()));
							stateFilterApplied = true;
						}
					}

				  if (locationCriteria.Area.MsaId.IsNotNull() && locationCriteria.Area.MsaId > 0 && _lensService.MsaCustomFilterTag.IsNotNullOrEmpty())
				  {
				    var msaCustomFilterId =
				      Helpers.Lookup.GetLookup(LookupTypes.StateMetropolitanStatisticalAreas)
				        .Where(x => x.Id == locationCriteria.Area.MsaId)
				        .Select(x => x.ExternalId)
				        .FirstOrDefault();

				    if (msaCustomFilterId.IsNotNullOrEmpty())
				    {
							criteriaBuilder.Append(AppSettings.NoFixedLocation && !isSpideredSearch
                ? MapCustomFilter(_lensService.MsaCustomFilterTag, msaCustomFilterId,true)
                : MapCustomFilter(_lensService.MsaCustomFilterTag, msaCustomFilterId));
				      msaFilterApplied = true;
				    }
				  }

				  #endregion
				}

				#endregion

				#region Multiple States

				if (locationCriteria.Radius.IsNull() && locationCriteria.Area.IsNull() && locationCriteria.SearchMultipleStates.IsNotNullOrEmpty())
				{
					var customFilterIds = Helpers.Lookup.GetLookup(LookupTypes.States)
						.Where(x => locationCriteria.SearchMultipleStates.Contains(x.ExternalId))
						.Select(x => x.CustomFilterId.ToString())
						.ToList();
					if (customFilterIds.Any())
					{
            criteriaBuilder.Append(AppSettings.NoFixedLocation 
              ? MapCustomFilter(_lensService.StateCodeCustomFilterTag, customFilterIds,true)
              : MapCustomFilter(_lensService.StateCodeCustomFilterTag, customFilterIds));
						stateFilterApplied = true;
					}
        }

        #endregion

      }

			#region Add default state filter
				
				// Add default state and nearby states to the state filter if no other state filter has been applied and the instance is not nationwide
      if ((!stateFilterApplied && !msaFilterApplied) && (!AppSettings.NationWideInstance))
			{
				var defaultStateCustomFilterIds = (AppSettings.SearchableStateKeys.IsNotNullOrEmpty())
				                                  	? Helpers.Lookup.GetLookup(LookupTypes.States).Where(x => AppSettings.SearchableStateKeys.Contains(x.Key)).Select(x => x.CustomFilterId.ToString()).ToList()
				                                  	: new List<string> {"0"};

				criteriaBuilder.Append(AppSettings.NoFixedLocation  
          ? MapCustomFilter(_lensService.StateCodeCustomFilterTag, defaultStateCustomFilterIds,true)
          : MapCustomFilter(_lensService.StateCodeCustomFilterTag, defaultStateCustomFilterIds));
			}

			#endregion
			

			#endregion

			#region Home Based Jobs

			if (searchCriteria.HomeBasedJobsCriteria.IsNotNull())
			{
				var homeBasedJobsCriteria = searchCriteria.HomeBasedJobsCriteria;

				if (homeBasedJobsCriteria.IsNotNull() && homeBasedJobsCriteria.HomeBasedJobs.IsNotNull() && _lensService.HomeBasedTag.IsNotNullOrEmpty())
				{
					criteriaBuilder.Append(MapJobTypeFilter((FilterTypes)homeBasedJobsCriteria.HomeBasedJobs, _lensService.HomeBasedTag));
				}
			}

			#endregion

			#region Commission Based Jobs

			if (searchCriteria.CommissionBasedJobsCriteria.IsNotNull())
			{
				var commissionBasedJobsCriteria = searchCriteria.CommissionBasedJobsCriteria;

				if (commissionBasedJobsCriteria.IsNotNull() && commissionBasedJobsCriteria.CommissionBasedJobs.IsNotNull() && _lensService.CommissionBasedTag.IsNotNullOrEmpty())
				{
					criteriaBuilder.Append(MapJobTypeFilter((FilterTypes)commissionBasedJobsCriteria.CommissionBasedJobs, _lensService.CommissionBasedTag));
				}
			}

			#endregion

			#region Salary And Commission Based Jobs

			if (searchCriteria.SalaryAndCommissionBasedJobsCriteria.IsNotNull())
			{
				var salaryAndCommissionBasedJobsCriteria = searchCriteria.SalaryAndCommissionBasedJobsCriteria;

				if (salaryAndCommissionBasedJobsCriteria.IsNotNull() && salaryAndCommissionBasedJobsCriteria.SalaryAndCommissionBasedJobs.IsNotNull() && _lensService.SalaryAndCommissionBasedTag.IsNotNullOrEmpty())
				{
					criteriaBuilder.Append(MapJobTypeFilter((FilterTypes)salaryAndCommissionBasedJobsCriteria.SalaryAndCommissionBasedJobs, _lensService.SalaryAndCommissionBasedTag));
				}
			}

			#endregion

			#region Keywords

			if (searchCriteria.KeywordCriteria.IsNotNull())
			{
				var keywordCriteria = searchCriteria.KeywordCriteria;

				var keywords = MapPostingKeywords(keywordCriteria);

				if (keywords.Count > 0)
					filters.AddRange(keywords);
			}

			#endregion

			#region Posting Age Criteria

		  var postingAgeInDays = 0;

			if (_lensService.PostingDateCustomFilterTag.IsNotNullOrEmpty())
			{
				var postingAgeCriteria = searchCriteria.PostingAgeCriteria;
			  if (postingAgeCriteria.IsNotNull())
			    postingAgeInDays = postingAgeCriteria.PostingAgeInDays;

			  var lensSearchLimit = _lensService.MaximumDaysSearchableOn.GetValueOrDefault(0);
        if (lensSearchLimit > 0 && (postingAgeInDays == 0 || lensSearchLimit < postingAgeInDays))
          postingAgeInDays = lensSearchLimit;

        if (postingAgeInDays > 0)
				{
					// Adding a day to the to date to capture all jobs posted today
          var toJulianDate = postingAgeCriteria.MinimumPostingAgeInDays.HasValue 
                             ? ToJulianDate(DateTime.Now.Date.AddDays(-postingAgeCriteria.MinimumPostingAgeInDays.Value))
                             : ToJulianDate(DateTime.Now.Date.AddDays(1));

          var fromJulianDate = (_lensService.NewJobsPostingDelayDays.HasValue)
             ? ToJulianDate(DateTime.Now.Date.AddDays(-postingAgeCriteria.PostingAgeInDays - _lensService.NewJobsPostingDelayDays.Value))
             : ToJulianDate(DateTime.Now.Date.AddDays(-postingAgeCriteria.PostingAgeInDays));

					criteriaBuilder.Append(MapCustomFilter(_lensService.PostingDateCustomFilterTag, fromJulianDate.ToString(), toJulianDate.ToString()));
				}
			}

			#endregion

      #region Education Criteria

			if (searchCriteria.EducationSchoolCriteria.IsNotNull() && _lensService.OriginCustomFilterTag.IsNotNullOrEmpty())
      {
        var onlyShowExclusive = searchCriteria.EducationSchoolCriteria.OnlyShowExclusivePostings;

        if (onlyShowExclusive)
        {
          criteriaBuilder.Append(MapCustomFilter(_lensService.OriginCustomFilterTag, "0", "998"));
        }
      }
			
			#endregion

			#region Program Of Study

			if (searchCriteria.EducationProgramOfStudyCriteria.IsNotNull())
			{
				if (programOfStudyLensIds.IsNotNullOrEmpty() && _lensService.ProgramOfStudyCustomFilterTag.IsNotNullOrEmpty())
					criteriaBuilder.Append(MapCustomFilter(_lensService.ProgramOfStudyCustomFilterTag, programOfStudyLensIds));
				else if (_lensService.ROnetCustomFilterTag.IsNotNullOrEmpty() && programOfStudyRonets.IsNotNullOrEmpty())
					criteriaBuilder.Append(MapCustomFilter(_lensService.ROnetCustomFilterTag, programOfStudyRonets));
			
				// Add a filter for the required Program Of Study Id
				if (_lensService.RequiredProgramOfStudyIdFilterTag.IsNotNullOrEmpty())
					criteriaBuilder.Append(MapCustomFilter(_lensService.RequiredProgramOfStudyIdFilterTag, searchCriteria.EducationProgramOfStudyCriteria.DegreeEducationLevelId.ToString(), true));
			}

      #endregion

      #region Education Level

			if (searchCriteria.EducationLevelCriteria.IsNotNull() && _lensService.EducationLevelCustomFilterTag.IsNotNullOrEmpty())
			{
				var educationCriteria = searchCriteria.EducationLevelCriteria;

				if (educationCriteria.IsNotNull() && educationCriteria.RequiredEducationIds.IsNotNullOrEmpty() && educationCriteria.RequiredEducationIds.Count > 0)
				{
					// Get external IDs from lookup 

					var externalIds = new List<string>();
					foreach (var requiredEducationId in educationCriteria.RequiredEducationIds)
					{
						var educationLevelExternalIds = Helpers.Lookup.GetLookup(LookupTypes.RequiredEducationLevels).Where(x => x.Id == requiredEducationId).Select(x => x.ExternalId).SingleOrDefault();
						externalIds.AddRange(educationLevelExternalIds.Split(','));
					}

					if (educationCriteria.IncludeJobsWithoutEducationRequirement)
						externalIds.Add("0");

					criteriaBuilder.Append(MapCustomFilter(_lensService.EducationLevelCustomFilterTag, externalIds));
				}
			}

			#endregion

			#region Languages

			var languageCriteria = searchCriteria.LanguageCriteria;
			if (languageCriteria.IsNotNull())
			{
				var proficiencyList = languageCriteria.LanguagesWithProficiencies;
				var languagesWithProficiencies = new List<string>();
				proficiencyList.ForEach(pair =>
				{
					var language = pair.Language;
					var proficiencyId = pair.Proficiency;
					var proficiencyPosition = Helpers.Lookup.GetLookup(LookupTypes.LanguageProficiencies).Where(x => x.Id == proficiencyId).Select(x => x.DisplayOrder).FirstOrDefault();
					var allFilters = Helpers.Lookup.GetLookup(LookupTypes.LanguageProficiencies)
																				 .Where(x => x.DisplayOrder <= proficiencyPosition && x.ExternalId != null)
																				 .Select(x => string.Concat(language, x.ExternalId))
																				 .ToList();
					languagesWithProficiencies.Add(allFilters.Any() ? string.Join(" or ", allFilters) : language);
				});
				filters.Add(MapLanguages(languagesWithProficiencies, languageCriteria.LanguageSearchType));
			}

			#endregion

			#region Minimum salary

			if (searchCriteria.SalaryCriteria.IsNotNull() && _lensService.MinimumSalaryCustomFilterTag.IsNotNullOrEmpty())
			{
				var salaryCriteria = searchCriteria.SalaryCriteria;
				if (salaryCriteria.IsNotNull() && salaryCriteria.MinimumSalary > 0)
				{
					criteriaBuilder.Append(MapCustomFilterWithNoMax(_lensService.MinimumSalaryCustomFilterTag, salaryCriteria.MinimumSalary.ToString(), salaryCriteria.IncludeJobsWithoutSalaryInformation));
				}
			}

			#endregion

			#region Internship

			if (searchCriteria.InternshipCriteria.IsNotNull())
			{
				var internshipCriteria = searchCriteria.InternshipCriteria;

				if (internshipCriteria.IsNotNull() && internshipCriteria.Internship.IsNotNull() && _lensService.InternshipCustomFilterTag.IsNotNullOrEmpty())
				{
					criteriaBuilder.Append(MapJobTypeFilter((FilterTypes)internshipCriteria.Internship, _lensService.InternshipCustomFilterTag));
				}

				if (internshipCategoryLensFilterIds.IsNotNullOrEmpty() && _lensService.InternshipCategoriesFilterTag.IsNotNullOrEmpty())
				{
					criteriaBuilder.Append(MapCustomFilter(_lensService.InternshipCategoriesFilterTag, internshipCategoryLensFilterIds.Select(internshipCategoryLensFilterId => internshipCategoryLensFilterId.ToString()).ToList()));
				}
			}

			#endregion

			#region Occupation

			if (onetCodes.IsNotNullOrEmpty() && _lensService.OccupationCustomFilterTag.IsNotNullOrEmpty())
				criteriaBuilder.Append(MapCustomFilter(_lensService.OccupationCustomFilterTag, onetCodes));
			
			#endregion

			#region Industry

			if (industryCodes.IsNotNullOrEmpty() && _lensService.IndustryCustomFilterTag.IsNotNullOrEmpty())
			{
				criteriaBuilder.Append(CreateNaicsCustomFilter(industryCodes));
			}

			#endregion

			#region Sector

			if (searchCriteria.JobSectorCriteria.IsNotNull() && _lensService.SectorCustomFilterTag.IsNotNullOrEmpty())
			{
				var sectorCriteria = searchCriteria.JobSectorCriteria;
				if (sectorCriteria.IsNotNull() && sectorCriteria.RequiredJobSectorIds.IsNotNullOrEmpty() &&
					  sectorCriteria.RequiredJobSectorIds.Count > 0)
				{
					var sectorClusterIds = Helpers.Lookup.GetLookup(LookupTypes.EmergingSectorClusters).Where(x => sectorCriteria.RequiredJobSectorIds.Contains(x.ParentId)).Select(x => x.CustomFilterId.ToString()).ToList(); // sectorCriteria.RequiredJobSectorIds.Select(x => x.ToString()).ToList();

					criteriaBuilder.Append(MapCustomFilter(_lensService.SectorCustomFilterTag, sectorClusterIds));
				}
			}

			#endregion

      #region Physical Ability

			if (searchCriteria.PhysicalAbilityCriteria.IsNotNull())
			{
				// TODO: Martha (Low) - code not available yet
			}

			#endregion

			#region ROnet

			if (searchCriteria.ROnetCriteria.IsNotNull() && searchCriteria.ROnetCriteria.ROnets.IsNotNullOrEmpty() && _lensService.ROnetCustomFilterTag.IsNotNullOrEmpty())
			{
				var numericROnets = searchCriteria.ROnetCriteria.ROnets.Select(rOnet => rOnet.AsLensROnet()).ToList();
				criteriaBuilder.Append(MapCustomFilter(_lensService.ROnetCustomFilterTag, numericROnets));
			}

			#endregion

      #region Work Availabilty

      var workDaysCriteria = searchCriteria.WorkDaysCriteria;
      if (workDaysCriteria.IsNotNull())
      {
        if (workDaysCriteria.WorkDays.GetValueOrDefault(DaysOfWeek.None).IsNotIn(DaysOfWeek.None, DaysOfWeek.All))
          filters.Add(MapNormalWorkDays(workDaysCriteria.WorkDays.Value));

        if (workDaysCriteria.WeekDayVaries)
          filters.Add("(workdays/varies:1)");

        if (workDaysCriteria.WorkWeek.IsNotNullOrZero())
        {
          var employmentStatus = Helpers.Lookup.GetLookup(LookupTypes.EmploymentStatuses).Where(x => x.Id == workDaysCriteria.WorkWeek).Select(x => x.Key).FirstOrDefault();
          if (employmentStatus.IsNotNullOrEmpty())
          {
            var hoursText = employmentStatus == "EmploymentStatuses.Fulltime" ? "fulltime" : "parttime";
            filters.Add(string.Format("jobtype[hours='{0}']", hoursText));
          }
          
        }
      }

      #endregion

			#region Exclude Past Jobs

			if (searchCriteria.ExcludePostingCriteria.IsNotNull())
			{
				// TODO: Martha (Low)  - How do we exclude certain jobs from a search, code not available yet in Career WS

        var excludeStatues = searchCriteria.ExcludePostingCriteria.JobStatuses;
        if (excludeStatues.IsNotNullOrEmpty())
        {
          var allStatuses = Enumerable.Range(0, 8).Select(i => i.ToString(CultureInfo.InvariantCulture)).Where(i => !excludeStatues.Contains(i)).ToList();
          criteriaBuilder.Append(MapCustomFilter(_lensService.StatusCustomFilterTag, allStatuses));
        }
			}

			#endregion

			#region Result

			if (searchCriteria.RequiredResultCriteria.IsNotNull())
			{
				var resultCriteria = searchCriteria.RequiredResultCriteria;

        maximumDocumentCount = resultCriteria.MaximumDocumentCount.ToString();
				minimumStarMatch = resultCriteria.MinimumStarMatch != StarMatching.None ? resultCriteria.MinimumStarMatch.ToMinimumScore(AppSettings.StarRatingMinimumScores).ToString() : AppSettings.MinimumScoreForSearch.ToString();
			}

      if (maximumDocumentCount.IsNullOrEmpty())
        maximumDocumentCount = AppSettings.MaximumNoDocumentToReturnInSearch.ToString(CultureInfo.InvariantCulture);

			#endregion

			if (filters.Count > 0)
			{
				var allFilters = String.Join(" and ", filters.ToArray());

				var xDoc = new XmlDocument { PreserveWhitespace = true };
				xDoc.LoadXml(_xmlHeader + "<filter var='keyword'></filter>");

				var filterNode = xDoc.SelectSingleNode("//filter");
				if (filterNode != null)
				{
					filterNode.AppendChild(xDoc.CreateCDataSection(allFilters));

					criteriaBuilder.Append(filterNode.OuterXml);
				}
			}

		  var extraInclude = searchCriteria.RequiredResultCriteria.IsNotNull() && searchCriteria.RequiredResultCriteria.IncludeDuties 
        ? "<include var='xpath://duties'/>" 
        : "";

			if (AppSettings.Module == FocusModules.Assist || RuntimeContext.CurrentRequest.UserContext.IsShadowingUser)
				extraInclude = string.Concat(extraInclude, "<include var='xpath://special/talentemployer'/>");

			var searchVendor = (_lensService.SearchAllVendors) ? "" : _lensService.Vendor;

			return ValidatedCommand(String.Format(command, searchVendor, maximumDocumentCount, minimumStarMatch, resumeXml, postingXml, criteriaBuilder, _lensService.PostingDateCustomFilterTag, _lensService.OriginCustomFilterTag, _lensService.InternshipCustomFilterTag, extraInclude));
		}

		/// <summary>
		/// Creates the naics custom filter.
		/// </summary>
		/// <param name="industryCodes">The industry codes.</param>
		/// <returns></returns>
		private string CreateNaicsCustomFilter(List<string> industryCodes)
		{
			// As custom filters do not work with or clauses I am going to assume that only one none 4 digit NAICS or one range can be passed
			// A naics range looks like 44-45
			if (industryCodes.Any(x => x.Contains("-")))
				return CreateNaicsCustomFilterFromRange(industryCodes);

			if (industryCodes.Any(x => x.Length > 4))
				throw new Exception("Industry search only supports NAICS 4 characters or shorter.");

			if (industryCodes.All(x => x.Length == 4))
				return MapCustomFilter(_lensService.IndustryCustomFilterTag, industryCodes);

			if (industryCodes.Count > 1)
				throw new Exception("Industry search only supports searching one NAICS shorter than 4 characters.");
			
			var naics = industryCodes[0];

			switch (naics.Length)
			{
				case 2:
					return MapCustomFilter(_lensService.IndustryCustomFilterTag, string.Format("{0}00", naics), string.Format("{0}99", naics));
				case 3:
					return MapCustomFilter(_lensService.IndustryCustomFilterTag, string.Format("{0}0", naics), string.Format("{0}9", naics));
				default:
					throw new Exception("Industry search does not support searching one character NAICS.");
			}
		}

		/// <summary>
		/// Creates the naics custom filter from range.
		/// </summary>
		/// <param name="industryCodes">The industry codes.</param>
		/// <returns></returns>
		/// <exception cref="System.Exception">
		/// Industry search only supports one NAICS range.
		/// or
		/// Industry search only supports NAICS 4 characters or shorter.
		/// or
		/// Industry search does not support searching one character NAICS.
		/// </exception>
		private string CreateNaicsCustomFilterFromRange(List<string> industryCodes)
		{
			// A naics range looks like 44-45
			if (industryCodes.Count != 1)
				throw new Exception("Industry search only supports one NAICS range.");

			var splitIndustryCodes = industryCodes[0].Split('-');

			if (splitIndustryCodes.Any(x => x.Length > 4))
				throw new Exception("Industry search only supports NAICS 4 characters or shorter.");

			if (splitIndustryCodes.Any(x => x.Length == 1))
				throw new Exception("Industry search does not support searching one character NAICS.");

			var startNaics = splitIndustryCodes[0];
			var endNaics = splitIndustryCodes[1];

			switch (startNaics.Length)
			{
				case 2:
					startNaics = string.Format("{0}00", startNaics);
					break;
				case 3:
					startNaics = string.Format("{0}0", startNaics);
					break;
			}

			switch (endNaics.Length)
			{
				case 2:
					endNaics = string.Format("{0}99", endNaics);
					break;
				case 3:
					endNaics = string.Format("{0}9", endNaics);
					break;
			}

			return MapCustomFilter(_lensService.IndustryCustomFilterTag, startNaics, endNaics);
		}

		/// <summary>
		/// Builds the search postings like this command.
		/// </summary>
		/// <param name="searchCriteria">The criteria.</param>
		/// <returns></returns>
		public string BuildSearchPostingLikeThisCommand(SearchCriteria searchCriteria)
		{

			const string command = "<bgtcmd><search type='posting' vendor='{0}' count='{1}' min='{2}'>" +
														 "<doc type='posting' vendor='{0}' id='{3}'/>" +
														 "<include var='id'/><include var='title'/><include var='xpath://contact/company'/><include var='special_{4}'/><include var='special_{5}'/><include var='xpath://address'/>" +
														 "</search></bgtcmd>";

			if (searchCriteria.IsNull()) return null;

			
			var documentId = "";
			var minimumStarMatch = "";
			var maximumDocumentCount = "";

			#region Reference Document

			if (searchCriteria.ReferenceDocumentCriteria.IsNotNull())
			{
				var referenceDocumentCriteria = searchCriteria.ReferenceDocumentCriteria;
				if (referenceDocumentCriteria.IsNotNull() || referenceDocumentCriteria.DocumentId.IsNotNullOrEmpty())
				{
					documentId = referenceDocumentCriteria.DocumentId;
				}	
			}

			#endregion

			#region Result

			if (searchCriteria.RequiredResultCriteria.IsNotNull())
			{
				var resultCriteria = searchCriteria.RequiredResultCriteria;

				if (resultCriteria.IsNotNull())
				{
					maximumDocumentCount = resultCriteria.MaximumDocumentCount.ToString();
					minimumStarMatch = resultCriteria.MinimumStarMatch.ToString();
				}
			}

			#endregion

			var searchVendor = (_lensService.SearchAllVendors) ? "" : _lensService.Vendor;

			return ValidatedCommand(String.Format(command, searchVendor, maximumDocumentCount, minimumStarMatch, documentId, _lensService.PostingDateCustomFilterTag, _lensService.OriginCustomFilterTag));
		}

		/// <summary>
		/// Builds the search for onet command.
		/// </summary>
		/// <param name="jobTitle">The job title.</param>
		/// <returns></returns>
		public string BuildSearchForOnetCommand(string jobTitle)
		{
			var command = "<bgtcmd><canon><resume><experience><job><title>" + System.Security.SecurityElement.Escape(jobTitle) + "</title></job></experience></resume></canon></bgtcmd>";

			return ValidatedCommand(command);
		}

		/// <summary>
		/// Builds the clarify command.
		/// </summary>
		/// <param name="dataToClarify">The data to clarify.</param>
		/// <param name="minScore">The min score.</param>
		/// <param name="maxSearchCount">The max search count.</param>
		/// <param name="type">The type.</param>
		/// <returns></returns>
		public string BuildClarifyCommand(string dataToClarify, int minScore, int maxSearchCount, string type)
		{
			type = type.IsNullOrEmpty() ? "posting" : type;
			maxSearchCount = maxSearchCount == 0 ? 100 : maxSearchCount;

			// Build the command XML in the format <bgtcmd><clarify type='posting' count='100' min='0'></clarify></bgtcmd>"
			string command = "<bgtcmd><clarify type='" + type + "' count='" + maxSearchCount.ToString() + "' min='" + minScore.ToString() + "'></clarify></bgtcmd>";

			var xDoc = new XmlDocument { PreserveWhitespace = true };
			xDoc.LoadXml(_xmlHeader + command);

			var clarifyNode = xDoc.SelectSingleNode("//clarify");
			if (clarifyNode != null) clarifyNode.AppendChild(xDoc.CreateCDataSection(dataToClarify));

			return xDoc.OuterXml;
		}

		/// <summary>
		/// Builds the canon command.
		/// </summary>
		/// <param name="xml">The XML.</param>
		/// <returns></returns>
		public string BuildCanonCommand(string xml)
		{
			var command = String.Format("<bgtcmd><canon>{0}</canon></bgtcmd>", xml);
			return ValidatedCommand(command);
		}

		/// <summary>
		/// Builds the match command.
		/// </summary>
		/// <param name="resumeXml">The resume XML.</param>
		/// <param name="postingXml">The posting XML.</param>
		/// <returns></returns>
		public string BuildMatchCommand(string resumeXml, string postingXml)
		{
			var command = String.Format("<bgtcmd><match>{0}{1}</match></bgtcmd>", postingXml, resumeXml);
			return ValidatedCommand(command);
		}

		/// <summary>
		/// Builds the register command.
		/// </summary>
		/// <param name="id">The id.</param>
		/// <param name="xmlDocument">The XML document.</param>
		/// <param name="documentType">Type of the document.</param>
		/// <returns></returns>
		public string BuildRegisterCommand(string id, string xmlDocument, DocumentType documentType)
		{
			var type = (documentType == DocumentType.Posting) ? "posting" : "resume";
			var command = String.Format("<bgtcmd><register type='{0}' vendor='{1}' id='{2}'>{3}</register></bgtcmd>", type, _lensService.Vendor, id, xmlDocument);

			return ValidatedCommand(command);
		}

		/// <summary>
		/// Builds the unregister command.
		/// </summary>
		/// <param name="ids">A list of ids.</param>
		/// <param name="documentType">Type of the document.</param>
		/// <returns></returns>
		public string BuildUnregisterCommand(List<string> ids, DocumentType documentType)
		{
			var type = (documentType == DocumentType.Posting) ? "posting" : "resume";

      const string unregisterXml = "<unregister type='{0}' vendor='{1}' id='{2}'/>";

      var command = new StringBuilder("<bgtcmd>");
      ids.ForEach(id => command.AppendFormat(unregisterXml, type, _lensService.Vendor, id));
		  command.Append("</bgtcmd>");

			//var command = String.Format("<bgtcmd><unregister type='{0}' vendor='{1}' id='{2}'/></bgtcmd>", type, _lensService.Vendor, id);

			return ValidatedCommand(command.ToString());
		}

		/// <summary>
		/// Validates the command Xml.
		/// </summary>
		/// <param name="xml">The XML.</param>
		/// <returns></returns>
		public string ValidatedCommand(string xml)
		{
			var xDoc = new XmlDocument {PreserveWhitespace = true};
			xDoc.LoadXml(_xmlHeader + xml);
			return xDoc.OuterXml;
		}
		
		#region SearchCommand Helpers

		/// <summary>
		/// Maps the citizenship.
		/// </summary>
		/// <param name="statusCriteria">The status criteria.</param>
		/// <returns></returns>
		private static string MapCitizenship(CandidateSearchStatusCriteria statusCriteria)
		{
			return statusCriteria.USCitizen ? "citizenship:(American)" : "";
		}

		/// <summary>
		/// Maps the certificate license.
		/// </summary>
		/// <param name="licencesAndCertifications">The licences and certifications.</param>
		/// <returns></returns>
		private static string MapCertificateLicense(IEnumerable<string> licencesAndCertifications)
		{
      return MapTag("certifications:({0})", licencesAndCertifications, true);
		}

		/// <summary>
		/// Maps the languages.
		/// </summary>
		/// <param name="languages">The languages.</param>
    /// <param name="useAndOperator">Whether to use 'Or' (default) or 'And' Logical Operators</param>
		/// <returns></returns>
    private static string MapLanguages(IEnumerable<string> languages, bool useAndOperator = false)
		{
      return MapTag("languages:({0})", languages, false, useAndOperator);
		}

		/// <summary>
		/// Maps the internship skills.
		/// </summary>
		/// <param name="skillIds">The skill ids.</param>
		/// <returns></returns>
		private static string MapInternshipSkills(IEnumerable<long> skillIds)
		{
			var skillIdsAsString = skillIds.Select(skillId => skillId.ToString()).ToList();
			return MapTag(@"internship_skill/id:({0})", skillIdsAsString);
		}

		/// <summary>
		/// Maps the completion date.
		/// </summary>
		/// <param name="monthsUntilExpectedCompletion">The months until expected completion.</param>
		/// <param name="includeAlumni">if set to <c>true</c> [include alumni].</param>
		/// <param name="postingDate">The posting date.</param>
		/// <returns></returns>
		private string MapCompletionDateCustomFilter(int monthsUntilExpectedCompletion, bool includeAlumni, DateTime? postingDate)
		{
			var startDate = (postingDate.HasValue) ? postingDate.Value.Date : DateTime.Now.Date;
			var endDate = startDate.AddMonths(monthsUntilExpectedCompletion);

			if (!includeAlumni && startDate < DateTime.Now.Date)
				startDate = DateTime.Now.Date;
			else if (includeAlumni)
				startDate = new DateTime(1900, 6, 1);
			
			return (MapCustomFilter(_lensService.DegreeCompletionDateFilterTag, startDate.GetJulianDays().ToString(), endDate.GetJulianDays().ToString()));
		}

	  /// <summary>
	  /// Maps the tag.
	  /// </summary>
	  /// <param name="template">The template.</param>
	  /// <param name="values">The values.</param>
	  /// <param name="addQuotationMarks">Whether to add quotation marks around words containing hyphen</param>
    /// <param name="useAndOperator">Whether to use 'Or' (default) or 'And' Logical Operators</param>
	  /// <returns></returns>
    private static string MapTag(string template, IEnumerable<string> values, bool addQuotationMarks = false, bool useAndOperator = false)
		{
			var retVal = "";
		  var wordSplitRegex = new Regex(@"[^\s""]+|""[^""]*""");

			foreach (var value in values)
			{
			  var tagValue = value;

			  if (addQuotationMarks)
			  {
			    var terms = wordSplitRegex.Matches(value);
			    var newTerms = new List<string>();

			    foreach (Match term in terms)
			    {
			      var termValue = term.Value;

			      if (termValue.Contains("-") && !termValue.StartsWith("-") && !termValue.StartsWith("\""))
			        newTerms.Add(string.Concat("\"", termValue, "\""));
			      else
			        newTerms.Add(term.Value);
			    }

          tagValue = string.Join(" ", newTerms);
			  }

			  if (retVal.Length == 0)
          retVal = "(" + tagValue + ")";
				else
          retVal += useAndOperator ? " and (" + tagValue + ")" : " or (" + tagValue + ")";
			}
			return string.Format(template, retVal);
		}

    /// <summary>
    /// Maps the posting keywords.
    /// </summary>
    /// <param name="keywordCriteria">The keyword criteria.</param>
    /// <returns></returns>
    private static List<string> MapPostingKeywords(KeywordCriteria keywordCriteria)
    {
      const string quote = "\"";
      const string regex = "(-?\".*?\")|(\\S*)";
			const string specialCharRemovalRegex = "[^0-9A-Za-z\\-\"']+";

      var keyWordFilters = new List<string>();

      if (!String.IsNullOrEmpty(keywordCriteria.KeywordText))
      {
				keywordCriteria.KeywordText = Regex.Replace(keywordCriteria.KeywordText, specialCharRemovalRegex, " ");

        var criteria = MapPostingKeywordKeywordCriteria(keywordCriteria.SearchLocation);

        var lensCriteria = new StringBuilder("(");

        if (keywordCriteria.Operator == LogicalOperators.None)
        {
          var keywordText = string.Concat(quote, keywordCriteria.KeywordText.Replace(quote, ""), quote);
          var keywordValue = string.Format(criteria, keywordText);
          lensCriteria.Append(keywordValue);
        }
        else
        {
          var keywordText = keywordCriteria.KeywordText;

          if (keywordText.Length - keywordText.Replace(quote, "").Length % 2 == 1)
            keywordText = string.Concat(keywordText, quote);

          var logicalOperator = MapLogicalOperator(keywordCriteria.Operator);

          var keywords = Regex.Split(keywordText, regex, RegexOptions.IgnoreCase).Where(x => x.Trim().Length > 0).ToList();

          var positiveWords = keywords.Where(word => !word.StartsWith("-")).ToList();
          var negativeWords = keywords.Where(word => word.StartsWith("-")).Select(word => word.Substring(1)).ToList();

          if (positiveWords.Count > 1 && negativeWords.Count > 0)
            lensCriteria.Append("(");

          var clauseAdded = false;
          positiveWords.ForEach(keyword =>
          {
            if (clauseAdded)
              lensCriteria.Append(" ").Append(logicalOperator).Append(" ");

            var formattedKeyword = keyword.StartsWith("\"") ? keyword : string.Concat(quote, keyword, quote);

            var keywordValue = string.Format(criteria, formattedKeyword);
            lensCriteria.Append(keywordValue);

            clauseAdded = true;
          });

          if (positiveWords.Count > 1 && negativeWords.Count > 0)
            lensCriteria.Append(")");

          negativeWords.ForEach(keyword =>
          {
            if (clauseAdded)
              lensCriteria.Append(" and ");

            lensCriteria.Append("NOT ");

            var formattedKeyword = keyword.StartsWith("\"") ? keyword : string.Concat(quote, keyword, quote);

            var keywordValue = string.Format(criteria, formattedKeyword);
            lensCriteria.Append(keywordValue);

            clauseAdded = true;
          });
        }

        lensCriteria.Append(")");
        keyWordFilters.Add(lensCriteria.ToString());
      }

      return keyWordFilters;
    }

		/// <summary>
		/// Maps the keyword keyword criteria.
		/// </summary>
		/// <param name="keywordLocation">The keyword criteria.</param>
		/// <returns></returns>
		private static string MapPostingKeywordKeywordCriteria(PostingKeywordScopes keywordLocation)
		{
			switch (keywordLocation)
			{
				case PostingKeywordScopes.Employer:
					return "jobemployer:{0}";
				case PostingKeywordScopes.JobDescription:
					return "duties:{0}";
				case PostingKeywordScopes.JobTitle:
					return "jobtitle:{0}";
				default:
					return "{0}";// default is anywhere
			}
		}

		/// <summary>
		/// Maps the keywords.
		/// </summary>
		/// <param name="keywordCriteria">The keyword criteria.</param>
		/// <returns></returns>
		private static List<string> MapKeywords(CandidateSearchKeywordCriteria keywordCriteria)
		{
			var keyWordFilters = new List<string>();

			if (!String.IsNullOrEmpty(keywordCriteria.Keywords))
			{
				var criteria = MapKeywordKeywordCriteria(keywordCriteria);
				var logicalOperator = MapLogicalOperator(keywordCriteria.Operator);

				var keywordValue = string.Format(criteria, keywordCriteria.Keywords);

				// only do this if we have an operator
				if (keywordCriteria.Operator != LogicalOperators.None)
				{
					var keywords = keywordCriteria.Keywords.Split(',', ';', ' ');

					foreach (var keyword in keywords)
						if (!String.IsNullOrEmpty(keyword))
							keywordValue += String.Format(" {0} {1}", logicalOperator, string.Format(criteria, keyword));
				}

				var lensCriteria = "(" + keywordValue + ")";

				keyWordFilters.Add(lensCriteria);
			}

			return keyWordFilters;
		}

		/// <summary>
		/// Maps the keyword keyword criteria.
		/// </summary>
		/// <param name="keywordCriteria">The keyword criteria.</param>
		/// <returns></returns>
		private static string MapKeywordKeywordCriteria(CandidateSearchKeywordCriteria keywordCriteria)
		{
			var isJob = false;

			switch (keywordCriteria.Context)
			{
				case KeywordContexts.Education:
					return "education:{0}";
				case KeywordContexts.FullResume:
					return "{0}";
				case KeywordContexts.JobTitle:
					isJob = true;
					break;
				case KeywordContexts.EmployerName:
					break;
				default:
					return "{0}";// default is the full resume
			}

			switch (keywordCriteria.Scope)
			{
				case KeywordScopes.EntireWorkHistory:
					return "experience:{0}";
				case KeywordScopes.LastJob:
					return isJob
									? "job[pos='1']/title:{0}"
									: "job[pos='1']/employer:{0}";
				case KeywordScopes.LastTwoJobs:
					return isJob
									? "job[pos='1']/title:{0} or job[pos='2']/title:{0}"
									: "job[pos='1']/employer:{0} or job[pos='2']/employer:{0}";
				case KeywordScopes.LastThreeJobs:
					return isJob
									? "job[pos='1']/title:{0} or job[pos='2']/title:{0} or job[pos='3']/title:{0}"
									: "job[pos='1']/employer:{0} or job[pos='2']/employer:{0} or job[pos='3']/employer:{0}";
				//case KeywordScopes.LastYear:
				//  return isJob ? "title:{0}" : "employer:{0}";
				//case KeywordScopes.LastThreeYears:
				//  return isJob ? "title:{0}" : "employer:{0}";
				//case KeywordScopes.LastFiveYears:
				//  return isJob ? "title:{0}" : "employer:{0}";
				default: // default just search the job title or employer without additional filters
					return isJob ? "title:{0}" : "employer:{0}";
			}
		}

		/// <summary>
		/// Maps the logical operator.
		/// </summary>
		/// <param name="logocalOperator">The logocal operator.</param>
		/// <returns></returns>
		private static string MapLogicalOperator(LogicalOperators logocalOperator)
		{
			return logocalOperator.ToString().ToLowerInvariant();
		}

	  /// <summary>
	  /// Maps the normal work days.
	  /// </summary>
	  /// <param name="days">The normal work days.</param>
	  /// <returns>A filter string</returns>
	  private static string MapNormalWorkDays(DaysOfWeek days)
    {
      var daysFilters = (from day in DaysOfTheWeek 
                           let dayName = day.ToString().ToLowerInvariant() 
                         where ((day & days) != day)
                         select string.Format("workdays/{0}:0", dayName)).ToList();

      return string.Concat("(", string.Join(" and ", daysFilters), ")");
    }

		/// <summary>
		/// Maps the education.
		/// </summary>
		/// <param name="educationLevel">The education level.</param>
		/// <returns></returns>
      private static List<string> MapEducation(EducationLevels educationLevel)
		{
			var educationFilters = new List<string>();
			var educationFilter = "";

            if ((educationLevel & EducationLevels.NoDiploma) == EducationLevels.NoDiploma)
                educationFilter =
                    "0 1 2 3 4 5 6 7 8 9 10 11 12";

			if ((educationLevel & EducationLevels.HighSchoolDiplomaOrEquivalent) == EducationLevels.HighSchoolDiplomaOrEquivalent)
            {
                if (educationFilter != "") educationFilter += " ";
                educationFilter += "13 14 88";
            }

            if ((educationLevel & EducationLevels.SomeCollegeNoDegree) == EducationLevels.SomeCollegeNoDegree)
            {
                if (educationFilter != "") educationFilter += " ";
                educationFilter += "15 16 17";
            }

            if ((educationLevel & EducationLevels.HighSchoolDiploma) == EducationLevels.HighSchoolDiploma)
            {
                if (educationFilter != "") educationFilter += " ";
                educationFilter += "0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 88 15 16 17";
            }

			if ((educationLevel & EducationLevels.AssociatesDegree) == EducationLevels.AssociatesDegree)
			{
				if (educationFilter != "") educationFilter += " ";
                educationFilter += "18 19 20 21 22 23";
			}

			if ((educationLevel & EducationLevels.BachelorsDegree) == EducationLevels.BachelorsDegree)
			{
				if (educationFilter != "") educationFilter += " ";
				educationFilter += "24";
			}

			if ((educationLevel & EducationLevels.GraduateDegree) == EducationLevels.GraduateDegree)
			{
				if (educationFilter != "") educationFilter += " ";
				educationFilter += "25 26";
			}
            if ((educationLevel & EducationLevels.MastersDegree) == EducationLevels.MastersDegree)
            {
                if (educationFilter != "") educationFilter += " ";
                educationFilter += "25";
            }
            if ((educationLevel & EducationLevels.DoctorateDegree) == EducationLevels.DoctorateDegree)
            {
                if (educationFilter != "") educationFilter += " ";
                educationFilter += "26";
            }
			educationFilters.Add(educationFilter);
			return educationFilters;
		}

		/// <summary>
		/// Maps the location unknown.
		/// </summary>
		/// <param name="excludeUnknownLocations">if set to <c>true</c> exclude unknown locations else include unknown locations.</param>
		/// <returns></returns>
		private static string	MapLocationUnknown(bool excludeUnknownLocations)
		{
			return excludeUnknownLocations ? "exclude" : "include";
		}

		/// <summary>
		/// Maps the distance unit.
		/// </summary>
		/// <param name="distanceUnit">The distance unit.</param>
		/// <returns></returns>
		private static string MapDistanceUnit(DistanceUnits distanceUnit)
		{
			return distanceUnit == DistanceUnits.Miles ? "miles" : "km";
		}

		/// <summary>
		/// Maps the job type filter.
		/// </summary>
		/// <param name="filterType">The job type filter.</param>
		/// <param name="customFilterTag">The job type custom filter tag.</param>
		/// <returns></returns>
		private static string MapJobTypeFilter(FilterTypes filterType, string customFilterTag)
		{
			var jobTypeFilter = "";

			if (filterType == FilterTypes.ShowOnly)
				jobTypeFilter = MapCustomFilter(customFilterTag, "1"); //internshipFilter = "(jobtitle:Intern or jobtitle:Interns or jobtitle:{^Internship.*} or jobtitle:{^Apprentice.*})";
			else if (filterType == FilterTypes.Exclude)
				jobTypeFilter = MapCustomFilter(customFilterTag, "0", true); //internshipFilter = "not(jobtitle:Intern or jobtitle:Interns or jobtitle:{^Internship.*} or jobtitle:{^Apprentice.*})";

			return jobTypeFilter;
		}

		/// <summary>
		/// Maps the custom filter.
		/// </summary>
		/// <param name="customFilterTag">The custom filter tag.</param>
		/// <param name="value">The value.</param>
		/// <param name="includeUnknown">if set to <c>true</c> [include unknown].</param>
		/// <returns></returns>
		private static string MapCustomFilter(string customFilterTag, string value, bool includeUnknown = false)
		{
			return MapCustomFilter(customFilterTag, value, value, includeUnknown);
		}

		/// <summary>
		/// Maps the custom filter.
		/// </summary>
		/// <param name="customFilterTag">The custom filter tag.</param>
		/// <param name="value">The value.</param>
		/// <param name="includeUnknown">if set to <c>true</c> [include unknown].</param>
		/// <returns></returns>
		private static string MapCustomFilterWithNoMax(string customFilterTag, string value, bool includeUnknown = false)
		{
			var unknown = (includeUnknown) ? "include" : "exclude";
			return String.Format("<{0} min='{1}' unknown='{2}'/>", customFilterTag, value, unknown);
		}

    /// <summary>
    /// Maps the custom filter where no minimum value
    /// </summary>
    /// <param name="customFilterTag">The custom filter tag.</param>
    /// <param name="value">The value.</param>
    /// <param name="includeUnknown">Whether the filter should return records where it is not set</param>
    /// <returns></returns>
    private static string MapCustomFilterWithNoMin(string customFilterTag, string value, bool includeUnknown = false)
    {
      var unknown = (includeUnknown) ? "include" : "exclude";
      return String.Format("<{0} max='{1}' unknown='{2}'/>", customFilterTag, value, unknown);
    }

		/// <summary>
		/// Maps the custom filter.
		/// </summary>
		/// <param name="customFilterTag">The custom filter tag.</param>
		/// <param name="minimumValue">The minimum value.</param>
		/// <param name="maximumValue">The maximum value.</param>
		/// <param name="includeUnknown">if set to <c>true</c> [include unknown].</param>
		/// <returns></returns>
		private static string MapCustomFilter(string customFilterTag, string minimumValue, string maximumValue, bool includeUnknown = false)
		{
			var unknown = (includeUnknown) ? "include" : "exclude";
			return String.Format("<{0} min='{1}' max='{2}' unknown='{3}'/>", customFilterTag, minimumValue, maximumValue, unknown);
		}

		/// <summary>
		/// Maps the custom filter.
		/// </summary>
		/// <param name="customFilterTag">The custom filter tag.</param>
		/// <param name="values">The values.</param>
		/// <param name="includeUnknown">if set to <c>true</c> [include unknown].</param>
		/// <returns></returns>
		public static string MapCustomFilter(string customFilterTag, List<string> values, bool includeUnknown = false)
		{
			var unknown = (includeUnknown) ? "include" : "exclude";
			return String.Format("<{0} array='{1}' unknown='{2}'/>", customFilterTag, string.Join(" ",values), unknown);
		}

		/// <summary>
		/// Maps the custom filter.
		/// </summary>
		/// <param name="customFilterTag">The custom filter tag.</param>
		/// <param name="values">The values.</param>
		/// <param name="includeUnknown">if set to <c>true</c> [include unknown].</param>
		/// <returns></returns>
		public static string MapCustomFilter(string customFilterTag, List<int> values, bool includeUnknown = false)
		{
			var unknown = (includeUnknown) ? "include" : "exclude";
			return String.Format("<{0} array='{1}' unknown='{2}'/>", customFilterTag, string.Join(" ", values), unknown);
		}

		/// <summary>
		/// Updates the postings postcode.
		/// </summary>
		/// <param name="postingXml">The posting XML.</param>
		/// <param name="postalCode">The postal code.</param>
		/// <returns></returns>
		private string UpdatePostingsPostcode(string postingXml, string postalCode)
		{
			if (String.IsNullOrEmpty(postingXml))
				postingXml = "<posting></posting>";

			if (!String.IsNullOrEmpty(postalCode))
			{
				var xDoc = new XmlDocument { PreserveWhitespace = true };
				xDoc.LoadXml(_xmlHeader + postingXml);

				var postingNode = xDoc.SelectSingleNode("//posting");
				var contactNode = postingNode.GetOrCreateElement("contact");
				var addressNode = contactNode.GetOrCreateElement("address");
				addressNode.GetOrCreateElement("postalcode").InnerText = postalCode;
				
				postingXml = postingNode.OuterXml;

				// If we have to mess with the document, we need to force 
				// canonicalization so we have to remove that attribute ...
				// It looks like <posting canonversion="2"...> or just in case <posting canonversion='2'...>			
				postingXml = postingXml.Replace("<posting canonversion=\"2\"", "<posting");
				postingXml = postingXml.Replace("<posting canonversion='2'", "<posting");
			}

			return postingXml;
		}

		/// <summary>
		/// Updates the resumes postcode.
		/// </summary>
		/// <param name="resumeXml">The resume XML.</param>
		/// <param name="postalCode">The postal code.</param>
		/// <returns></returns>
		private string UpdateResumesPostcode(string resumeXml, string postalCode)
		{
			if (String.IsNullOrEmpty(resumeXml))
				resumeXml = "<resume></resume>";

			if (!String.IsNullOrEmpty(postalCode))
			{
				var xDoc = new XmlDocument { PreserveWhitespace = true };
				xDoc.LoadXml(_xmlHeader + resumeXml);

				var resumeNode = xDoc.SelectSingleNode("//resume");
				var contactNode = resumeNode.GetOrCreateElement("contact");
				var addressNode = contactNode.GetOrCreateElement("address");
				addressNode.GetOrCreateElement("postalcode").InnerText = postalCode;


				resumeXml = resumeNode.OuterXml;

				// If we have to mess with the document, we need to force 
				// canonicalization so we have to remove that attribute ...
				// It looks like <resume canonversion="2"...> or just in case <resume canonversion='2'...>			
				resumeXml = resumeXml.Replace("<resume canonversion=\"2\"", "<resume");
				resumeXml = resumeXml.Replace("<resume canonversion='2'", "<resume");
			}

			return resumeXml;
		}

		/// <summary>
		/// To the julian date.
		/// </summary>
		/// <param name="date">The date.</param>
		/// <returns></returns>
		private double ToJulianDate(DateTime date)
		{
			try
			{
				return date.ToOADate() + 693596;
			}
			catch
			{
				return 0;
			}
		}

		/// <summary>
		/// Gets all possible ints for flag.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <param name="maxValue">The max value.</param>
		/// <returns></returns>
		private List<string> GetAllPossibleIntsForFlag(int value, int maxValue)
		{
			var returnList = new List<string>();

			for (var i = 0; i < 8; i++)
			{
				var flagValue = (int) Math.Pow(2, i);
				
				if ((flagValue & value) == flagValue)
				{
					for (var j = 0; j <= maxValue; j++)
					{
						if ((flagValue & j) == flagValue && !returnList.Contains(j.ToString()))
							returnList.Add(j.ToString());
					}
				}
			}

			return returnList;
		}

		#endregion
  }
}
