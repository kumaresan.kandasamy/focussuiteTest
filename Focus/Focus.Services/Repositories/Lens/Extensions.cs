﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;
using System.Xml.XPath;
using Focus.Core.DataTransferObjects.FocusExplorer;
using Focus.Core.Settings.Lens;
using Focus.Services.Core.Extensions;
using Focus.Core;
using Focus.Core.Models.Career;
using Focus.Core.Views;
using Focus.Services.Mappers;
using Framework.Core;

#endregion

namespace Focus.Services.Repositories.Lens
{
	internal static class Extensions
	{
		#region Lens Data Helpers

		/// <summary>
		/// Converts a Lens Xml response into an list of candidates.
		/// </summary>
		/// <param name="xml">The XML.</param>
		/// <param name="lensVeteranCustomFilterTag">The lens veteran custom filter tag.</param>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <param name="culture">The culture</param>
		/// <returns></returns>
		public static List<ResumeView> ToCandidateList(this string xml, string lensVeteranCustomFilterTag, string lensNcrcLevelTag, IRuntimeContext runtimeContext, string culture)
		{
			var candidates = new List<ResumeView>();

			var doc = new XmlDocument {PreserveWhitespace = true};
			doc.LoadXml(xml);

			var resumeNodes = doc.SelectNodes("//resume");

			if (resumeNodes != null && resumeNodes.Count > 0)
				foreach (XmlNode resumeNode in resumeNodes)
				{
					var candidate = new ResumeView
					{
						Id = resumeNode.AsLong("id"),
						Score = int.Parse(resumeNode.Attributes["score"].Value),
						Name = GetName(resumeNode),
						YearsExperience = resumeNode.AsInt32("yrsexp"),
						IsVeteran = (resumeNode.AsInt32("special_" + lensVeteranCustomFilterTag, 0) == 1),
						NcrcLevel = resumeNode.AsString("special_" + lensNcrcLevelTag).IsNotNullOrEmpty() ? (NcrcLevelTypes) resumeNode.AsInt32("special_" + lensNcrcLevelTag) : NcrcLevelTypes.None,
						StarRating = int.Parse(resumeNode.Attributes["score"].Value).ToStarRating(runtimeContext.AppSettings.StarRatingMinimumScores),
						Branding = resumeNode.AsString("xpath/branding"),
						ContactInfoVisible = resumeNode.AsBoolean("xpath/contact_info_visible"),
						CandidateLocation = resumeNode.AsString("xpath/city").IsNotNullOrEmpty() && resumeNode.AsString("xpath/state").IsNotNullOrEmpty() ? string.Format("{0}, {1}", resumeNode.AsString("xpath/city"), resumeNode.AsString("xpath/state")) : string.Empty
					};

					var jobNodes = resumeNode.SelectNodes("jobs/job");

					if (jobNodes != null && jobNodes.Count > 0)
						foreach (var employmentHistory in from XmlNode jobNode in jobNodes
							select new ResumeView.ResumeJobView
							{
								JobTitle = jobNode.AsString("title"),
								Employer = jobNode.AsString("employer"),
								YearStart = jobNode.AsString("start"),
								YearEnd = jobNode.AsString("end")
							})
						{
							candidate.EmploymentHistories.Add(employmentHistory);
						}

					candidates.Add(candidate);
				}

			return candidates;
		}

    /// <summary>
    /// Gets the name from the XML results
    /// </summary>
    /// <param name="resumeNode">The resume node in the results</param>
    /// <returns>The job seeker's name</returns>
    /// <remarks>
    /// This is used due to a change to how suffixes were stored in the XML 
    /// In 3.31 a "suffix_fullname" node was being stored under the "name" node, but this ended up being shown as part of the name in the Talent Pool Search, which was not required
    /// The "suffix_fullname" was removed in 3.32, and replace with just "suffix" under the "contact" node
    /// This method is simply to cope with any existing resumes posted to Lens in the old format
    /// </remarks>
		private static string GetName(XmlNode resumeNode)
		{
			var fullName = resumeNode.SelectSingleNode("xpath[@path='xpath://ResDoc/resume/contact/name']/name");

			if (fullName == null)
				return "Name not found";

			if (fullName.SelectSingleNode("suffix_fullname").IsNull())
			{
				var firstName = fullName.AsString("givenname[1]");
				var middleName = fullName.AsString("givenname[2]");
				var lastName = fullName.AsString("surname");
				var displayName = String.Empty;

				if (!String.IsNullOrWhiteSpace(firstName))
					displayName = firstName;

				if (!String.IsNullOrWhiteSpace(middleName))
					displayName += " " + middleName;

				if (!String.IsNullOrWhiteSpace(" " + lastName))
					displayName += " " + lastName;

				return displayName == String.Empty ? "Name not found" : displayName;
			}

			return string.Concat(fullName.AsString("givenname[1]"), " ", fullName.AsString("givenname[2]"), " ", "", fullName.AsString("surname")).Trim();
		}

		/// <summary>
		/// Toes the posting search result list.
		/// </summary>
		/// <param name="xml">The XML.</param>
		/// <param name="lensService">The lens service.</param>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <param name="culture">The culture.</param>
		/// <returns></returns>
		public static List<PostingSearchResultView> ToPostingSearchResultList(this string xml, LensService lensService, IRuntimeContext runtimeContext, string culture)
		{
			if (xml.IsNullOrEmpty())
				return new List<PostingSearchResultView>();

			var postings = new List<PostingSearchResultView>();

			var doc = new XmlDocument {PreserveWhitespace = true};
			doc.LoadXml(xml);

			var postingNodes = doc.SelectNodes("//posting");
			var internshipNodeXpath = "special_" + lensService.InternshipCustomFilterTag;

			if (postingNodes != null && postingNodes.Count > 0)
				foreach (XmlNode postingNode in postingNodes)
				{
					var state = postingNode.SelectSingleNode("xpath[@path='xpath://address']").AsString("address/state");
					var city = postingNode.SelectSingleNode("xpath[@path='xpath://address']").AsString("address/city");

					var julianpostingDate = postingNode.AsString(string.Format("special_{0}", lensService.PostingDateCustomFilterTag));
					julianpostingDate = julianpostingDate.IsNullOrEmpty() ? "0" : julianpostingDate;
					var postingDate = JulianDaysToDate(Convert.ToDouble(julianpostingDate));

					var minExperience = 0;

					var talentMinExpNode = postingNode.SelectSingleNode("xpath[@path='xpath://posting/background/experience']");
					if (talentMinExpNode.IsNotNull())
						minExperience = talentMinExpNode.AsInt32("experience");

					if (minExperience == 0)
					{
						var spideredMinExpNode = postingNode.SelectSingleNode("xpath[@path='xpath://special/minexperiencelevel']");
						if (spideredMinExpNode.IsNotNull())
							minExperience = spideredMinExpNode.AsInt32("MinExperienceLevel")*12;
					}

					var talentEmployerNode = runtimeContext.AppSettings.Module == FocusModules.Assist || runtimeContext.CurrentRequest.UserContext.IsShadowingUser
						                         ? postingNode.SelectSingleNode("xpath[@path='xpath://special/talentemployer']")
						                         : null;

					var post = new PostingSearchResultView
					{
						Id = postingNode.AsString("id"),
						Rank = postingNode.AsInt32("@score"),
						JobTitle = postingNode.AsString("title"),
						Internship = postingNode.AsBoolean(internshipNodeXpath),
						JobDate = postingDate,
						Employer = talentEmployerNode.IsNotNull() && talentEmployerNode.AsString("talentemployer").IsNotNullOrEmpty()
							? talentEmployerNode.AsString("talentemployer")
							: postingNode.SelectSingleNode("xpath[@path='xpath://special/jobemployer']").AsString("jobemployer"),
						Location = runtimeContext.AppSettings.NoFixedLocation && city.IsNullOrEmpty() && state.IsNullOrEmpty() ? "No Fixed Location" : string.Concat(city, ", ", state) ,
						StarRating = postingNode.AsInt32("@score").ToStarRating(runtimeContext.AppSettings.StarRatingMinimumScores),
						OriginId = postingNode.AsInt32(string.Format("special_{0}", lensService.OriginCustomFilterTag)),
						Duties = postingNode.SelectNodes("xpath[@path='xpath://duties']/duties").AsStringList() ?? new List<string>(),
						MinExperience = minExperience,
						ScreeningPreferences = postingNode.AsString("xpath[@path='xpath://special/screening']/screening").ToScreeningPreferences()
					};

					postings.Add(post);
				}

			return postings;
		}

		/// <summary>
		/// Gets the posting from tagged posting.
		/// </summary>
		/// <param name="taggedPosting">The tagged posting.</param>
		/// <returns></returns>
		public static string GetPostingFromTaggedPosting(this string taggedPosting)
		{
			const string defaultPosting = "<posting></posting>";

			if (string.IsNullOrEmpty(taggedPosting))
				return defaultPosting;

			try
			{
				var xDoc = LoadXmlDocument(taggedPosting);

				var postingNode = xDoc.SelectSingleNode("//posting");

				return postingNode == null ? defaultPosting : postingNode.OuterXml;
			}
			catch
			{
				return defaultPosting;
			}
		}

		/// <summary>
		/// Gets the job mine data elements from tagged posting.
		/// </summary>
		/// <param name="taggedPosting">The tagged posting.</param>
		/// <returns></returns>
		public static string GetJobMineDataElementsFromTaggedPosting(this string taggedPosting)
		{
			const string defaultDataElements = "<DataElementsRollup></DataElementsRollup>";

			if (string.IsNullOrEmpty(taggedPosting))
				return defaultDataElements;

			try
			{
				var xDoc = LoadXmlDocument(taggedPosting);

				var dataElementsNode = xDoc.SelectSingleNode("//DataElementsRollup");

				return dataElementsNode == null ? defaultDataElements : dataElementsNode.OuterXml;
			}
			catch
			{
				return defaultDataElements;
			}
		}

		/// <summary>
		/// Gets the job document from the xml string.
		/// </summary>
		/// <param name="xml">The xml string.</param>
		/// <returns></returns>
		public static string GetJobDocFromXml(this string xml)
		{
			const string defaultPosting = "<JobDoc></JobDoc>";

			if (string.IsNullOrEmpty(xml))
				return defaultPosting;

			try
			{
				var xDoc = LoadXmlDocument(xml);

				var jobDocNode = xDoc.SelectSingleNode("//JobDoc");

				return jobDocNode == null ? defaultPosting : jobDocNode.OuterXml;
			}
			catch
			{
				return defaultPosting;
			}
		}

		/// <summary>
		/// Updates the job document in tagged posting.
		/// </summary>
		/// <param name="taggedPosting">The tagged posting.</param>
		/// <param name="jobDoc">The job document.</param>
		/// <returns></returns>
		public static string UpdateJobDocInTaggedPosting(this string taggedPosting, string jobDoc)
		{
			if (taggedPosting.IsNullOrEmpty() || jobDoc.IsNullOrEmpty())
				return taggedPosting;

			const string jobDocPattern = "(<JobDoc>)(?s)(.*?)(</JobDoc>)";
			var regex = new Regex(jobDocPattern);

			return regex.Replace(taggedPosting, jobDoc);
		}

		/// <summary>
		/// Loads the XML document.
		/// </summary>
		/// <param name="xml">The XML.</param>
		/// <returns></returns>
		private static XmlDocument LoadXmlDocument(string xml)
		{
			var xDoc = new XmlDocument {PreserveWhitespace = true};
			xDoc.LoadXml(xml);
			return xDoc;
		}

		/// <summary>
		/// Populates the resume custom filters.
		/// </summary>
		/// <param name="lensService">The Lens service.</param>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <param name="resumeXml">The resume XML.</param>
		/// <param name="resumeRegisteredOn">The resume registered on.</param>
		/// <param name="resumeModifiedOn">The resume modified on.</param>
		/// <param name="personId">The person id.</param>
		/// <param name="enrollmentStatus">The enrollment status.</param>
		/// <param name="degreeEducationLevelIds">The degree education level ids.</param>
		/// <param name="dateOfBirth">The date of birth.</param>
		/// <returns></returns>
		/// <exception cref="System.Exception">
		/// No resume xml.
		/// or
		/// Resume Xml is invalid. No ResDoc node.
		/// </exception>
		public static string PopulateResumeCustomFilters(this LensService lensService, IRuntimeContext runtimeContext, string resumeXml, DateTime resumeRegisteredOn, DateTime resumeModifiedOn, long personId, SchoolStatus? enrollmentStatus, List<long> degreeEducationLevelIds, DateTime? dateOfBirth)
		{
			if (resumeXml.IsNullOrEmpty())
				throw new Exception("No resume xml.");

			var resumeDoc = (XDocument.Parse(resumeXml));

			var resdocElement = resumeDoc.XPathSelectElement("//ResDoc");
			if (resdocElement == null)
				throw new Exception("Resume Xml is invalid. No ResDoc node.");

			var specialElement = resdocElement.GetOrCreateElement("special");

			specialElement.AddCustomFilterNode(lensService.StatusCustomFilterTag, "1");
			specialElement.AddCustomFilterNode(lensService.RegisteredOnCustomFilterTag, resumeRegisteredOn.GetJulianDays().ToString());
			specialElement.AddCustomFilterNode(lensService.ModifiedOnCustomFilterTag, resumeModifiedOn.GetJulianDays().ToString());
			specialElement.AddCustomFilterNode(lensService.DateOfBirthCustomFilterTag, dateOfBirth.HasValue ? dateOfBirth.Value.GetJulianDays().ToString() : string.Empty);
			specialElement.AddCustomFilterNode(lensService.CitizenFlagCustomFilterTag, (resdocElement.XPathAsString("//citizen_flag").AsBoolean()) ? "1" : "0");
			specialElement.AddCustomFilterNode(lensService.VeteranStatusCustomFilterTag, (resdocElement.XPathAsString("//vet_flag").AsBoolean()) ? "1" : "0");

			if (resdocElement.XPathAsString("//languages_profiency").IsNotNullOrEmpty())
				specialElement.AddCustomFilterNode(lensService.LanguagesCustomFilterTag, resdocElement.XPathAsString("//languages_profiency").ToLower());
			else
				specialElement.AddCustomFilterNode(lensService.LanguagesCustomFilterTag, resdocElement.GetLanguagesCustomFilterValue());

			specialElement.AddCustomFilterNode(lensService.DrivingLicenceClassCustomFilterTag, resdocElement.XPathAsString("//driver_class"));
			specialElement.AddCustomFilterNode(lensService.DrivingLicenceEndorsementCustomFilterTag, resdocElement.GetEndorsementsCustomFilterValue(runtimeContext));
			specialElement.AddCustomFilterNode(lensService.CertificatesCustomFilterTag, resdocElement.GetCertificatesCustomFilterValue());
			specialElement.AddCustomFilterNode(lensService.WorkOvertimeCustomFilterTag, (resdocElement.XPathAsString("//work_over_time").AsBoolean()) ? "1" : "0");
			specialElement.AddCustomFilterNode(lensService.ShiftTypeCustomFilterTag, resdocElement.GetShiftTypeCustomFilterValue());
			specialElement.AddCustomFilterNode(lensService.WorkTypeCustomFilterTag, resdocElement.XPathAsString("//work_type"));
			specialElement.AddCustomFilterNode(lensService.WorkWeekCustomFilterTag, resdocElement.XPathAsString("//work_week"));
			specialElement.AddCustomFilterNode(lensService.EducationLevelCustomFilterTag, resdocElement.XPathAsString("//education/norm_edu_level_cd"));
			specialElement.AddCustomFilterNode(lensService.JobseekerIdCustomFilterTag, personId.ToString());
			specialElement.AddCustomFilterNode(lensService.RelocationCustomFilterTag, (resdocElement.XPathAsString("//relocate").AsBoolean()) ? "1" : "0");
			specialElement.AddCustomFilterNode(lensService.JobTypesCustomFilterTag, resumeDoc.XPathAsString("//postings_interested_in"));
			specialElement.AddCustomFilterNode(lensService.GPACustomFilterTag, resdocElement.GetGPACustomFilterValue());
			specialElement.AddCustomFilterNode(lensService.MonthsExperienceCustomFilterTag, resdocElement.XPathAsString("//months_experience"));
			specialElement.AddCustomFilterNode(lensService.ResumeIndustriesCustomFilterTag, resdocElement.GetIndustriesCustomFilterValue(runtimeContext));

			specialElement.AddCustomFilterNode(lensService.NCRCLevelTag, resdocElement.GetNCRCCustomFilterValue(runtimeContext));

			if (degreeEducationLevelIds.IsNotNullOrEmpty())
			{
				var degreeIds = string.Join(" ", degreeEducationLevelIds);
				specialElement.AddCustomFilterNode(lensService.ProgramOfStudyCustomFilterTag, degreeIds);
			}

			if (enrollmentStatus.IsNull())
				specialElement.AddCustomFilterNode(lensService.EnrollmentStatusCustomFilterTag, resdocElement.XPathAsString("//school_status_cd"));
			else
				specialElement.AddCustomFilterNode(lensService.EnrollmentStatusCustomFilterTag, ((int) enrollmentStatus).ToString());

			specialElement.AddCustomFilterNode(lensService.DegreeCompletionDateFilterTag, resdocElement.GetDegreeCompletionDate());

			// Home-based job seekers
			specialElement.AddCustomFilterNode(lensService.HomeBasedTag, resdocElement.XPathAsString("//home_based_jobpostings"));

			// Also add keyword filters for languages, as these need to support AND and OR
			var languagesNode = resdocElement.XPathSelectElement("//languages");
			if (languagesNode.IsNotNull() && languagesNode.HasElements)
			{
				var languageFilter = new StringBuilder();
				foreach (var languageNode in languagesNode.Elements("language"))
				{
					languageFilter.Append(languageNode.XPathAsString("language"));
					languageFilter.Append(languageNode.XPathAsString("proficiency"));
					languageFilter.Append(" ");
				}
				languagesNode.CreateElement("languagefilter", languageFilter.ToString());
			}

			return resumeDoc.ToString();
		}

		/// <summary>
		/// Populates the posting custom filters and get job doc.
		/// </summary>
		/// <param name="lensService">The Lens service.</param>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <param name="postingXml">The posting XML.</param>
		/// <param name="dataElementsXml">The data elements XML.</param>
		/// <param name="postingId">The posting id.</param>
		/// <param name="culture">The culture.</param>
		/// <param name="theme">The theme</param>
		/// <param name="mappedRequiredProgramsOfStudy">The mapped required programs of study.</param>
		/// <param name="postalCode">The postal code.</param>
		/// <returns></returns>
		/// <exception cref="System.Exception">No posting xml.</exception>
		public static string PopulatePostingCustomFiltersAndGetJobDoc(this LensService lensService, IRuntimeContext runtimeContext, XDocument postingXml, string dataElementsXml, long? postingId, string culture,
			FocusThemes theme, List<int> mappedRequiredProgramsOfStudy, PostalCodeDto postalCode)
		{
			if (postingXml.IsNull())
				throw new Exception("No posting xml.");

			if (dataElementsXml.IsNullOrEmpty())
				throw new Exception("No posting data elements xml.");

			var jobdocElement = postingXml.XPathSelectElement("//JobDoc");
			if (jobdocElement.IsNull())
				throw new Exception("Posting Xml is invalid. No JobDoc node.");

			var clientJobDataElement = postingXml.XPathSelectElement("//clientjobdata");

			var dataElementsDoc = (XDocument.Parse(dataElementsXml));
			var dataElementsRollupElement = dataElementsDoc.XPathSelectElement("//DataElementsRollup");
			if (dataElementsRollupElement.IsNull())
				throw new Exception("Posting data element Xml is invalid. No DataElementsRollup node.");

			var specialElement = jobdocElement.GetOrCreateElement("special");

			specialElement.AddCustomFilterNode(lensService.StatusCustomFilterTag, jobdocElement.XPathAsString("//job_status_cd"));
			if (clientJobDataElement.IsNotNull()) specialElement.AddCustomFilterNode(lensService.OpenDateCustomFilterTag, clientJobDataElement.XPathAsDateTime("//JOB_CREATE_DATE").GetJulianDays().ToString());
			if (clientJobDataElement.IsNotNull()) specialElement.AddCustomFilterNode(lensService.ClosingOnCustomFilterTag, clientJobDataElement.XPathAsDateTime("//JOB_LAST_OPEN_DATE").GetJulianDays().ToString());
			if (clientJobDataElement.IsNotNull()) specialElement.AddCustomFilterNode(lensService.MinimumSalaryCustomFilterTag, clientJobDataElement.XPathAsDecimal("//SAL_MIN").ToAnnualSalary(clientJobDataElement.XPathAsString("//SAL_UNIT_CD").ToFrequency()).ToString());
			if (clientJobDataElement.IsNotNull()) specialElement.AddCustomFilterNode(lensService.MaximumSalaryCustomFilterTag, clientJobDataElement.XPathAsDecimal("//SAL_MAX").ToAnnualSalary(clientJobDataElement.XPathAsString("//SAL_UNIT_CD").ToFrequency()).ToString());
			specialElement.AddCustomFilterNode(lensService.ExternalPostingIdCustomFilterTag, jobdocElement.XPathAsString("//foreignpostingid"));
			specialElement.AddCustomFilterNode(lensService.OccupationCustomFilterTag, jobdocElement.XPathAsString("//duties/title/@onet").Replace("-", "").Replace(".", ""));

			var stateCode = jobdocElement.XPathAsString("//address/state/@code");
		  if (stateCode.IsNullOrEmpty())
		    stateCode = jobdocElement.XPathAsString("//address/state");
      if (stateCode.IsNullOrEmpty())
        stateCode = runtimeContext.AppSettings.DefaultStateKey;

      specialElement.AddStateCustomFilterNode(runtimeContext, lensService.StateCodeCustomFilterTag, stateCode, culture);

			specialElement.AddCustomFilterNode(lensService.PostingIdCustomFilterTag, (postingId.HasValue) ? postingId.Value.ToString() : String.Empty);

			var originId = jobdocElement.XPathAsString("//special/originid");
			specialElement.AddCustomFilterNode(lensService.OriginCustomFilterTag, (originId.IsNotNullOrEmpty() ? originId : "999"));

			specialElement.AddCustomFilterNode(lensService.Sic2CustomFilterTag, jobdocElement.XPathAsString("//posting/@sic2"));

			var inferredNaics = dataElementsRollupElement.XPathAsString("ConsolidatedInferredNAICS");

			if (inferredNaics.IsNullOrEmpty())
				inferredNaics = jobdocElement.XPathAsString("//posting/@inferred-naics");

			specialElement.AddIndustryCustomFilterNode(runtimeContext, lensService.IndustryCustomFilterTag, inferredNaics);
			specialElement.AddCustomFilterNode(lensService.EducationLevelCustomFilterTag, jobdocElement.XPathAsString("//EDUCATION_CD"));
			specialElement.AddGreenJobCustomFilterNode(lensService.GreenJobCustomFilterTag, jobdocElement.XPathAsString("//skillrollup"));
			specialElement.AddSectorCustomFilterNode(lensService.SectorCustomFilterTag, jobdocElement.XPathAsString("//duties/title/@onet").Replace("-", "").Replace(".", ""), inferredNaics);
			specialElement.AddCustomFilterNode(lensService.CountyCodeCustomFilterTag, jobdocElement.XPathAsString("//address/county"));
			if (clientJobDataElement.IsNotNull()) specialElement.AddCustomFilterNode(lensService.MaximumSalaryCustomFilterTag, clientJobDataElement.XPathAsString("//COMPUTED_ANNUAL_PAY"));
			if (clientJobDataElement.IsNotNull()) specialElement.AddCustomFilterNode(lensService.SalaryCustomFilterTag, clientJobDataElement.XPathAsDecimal("//SAL_MIN").ToAnnualSalary(clientJobDataElement.XPathAsString("//SAL_UNIT_CD").ToFrequency()).ToString());

			var msaId = postalCode.IsNotNull() ? (postalCode.MsaId ?? "") : "";
			specialElement.AddCustomFilterNode(lensService.MsaCustomFilterTag, msaId);

			specialElement.AddCustomFilterNode(lensService.ROnetCustomFilterTag, dataElementsRollupElement.GetROnetCustomFilterValue());

			var jobType = (JobTypes) jobdocElement.XPathAsInt("//JOB_TYPE");

			//if (theme == FocusThemes.Workforce || (theme == FocusThemes.Education && jobType != JobTypes.Job))
			//  specialElement.AddCustomFilterNode(lensService.ROnetCustomFilterTag, dataElementsRollupElement.GetROnetCustomFilterValue());

			if (jobType == JobTypes.Job)
				specialElement.AddCustomFilterNode(lensService.InternshipCustomFilterTag, "0");

			if (jobType == JobTypes.InternshipPaid)
			{
				specialElement.AddCustomFilterNode(lensService.InternshipCustomFilterTag, "1");
				specialElement.AddCustomFilterNode(lensService.IncludeUnpaidCustomFilterTag, "0");
			}

			if (jobType == JobTypes.InternshipUnpaid)
			{
				specialElement.AddCustomFilterNode(lensService.InternshipCustomFilterTag, "1");
				specialElement.AddCustomFilterNode(lensService.IncludeUnpaidCustomFilterTag, "1");
			}

			if (jobType == JobTypes.InternshipPaid || jobType == JobTypes.InternshipUnpaid)
				specialElement.AddCustomFilterNode(lensService.InternshipCategoriesFilterTag, dataElementsRollupElement.GetInternshipCategoriesFilterValue());

			var requiredProgramOfStudyCustomFilterValue = clientJobDataElement.GetRequiredProgramOfStudyCustomFilterValue();

			if (lensService.ProgramOfStudyCustomFilterTag.IsNotNullOrEmpty())
			{
				// Get the programs of study mapped to this posting via the rules
				var mappedProgramsOfStudy = dataElementsRollupElement.GetMappedProgramOfStudy(runtimeContext.AppSettings.ExplorerDegreeClientDataTag);

				// Add any mapped required programs of study not already identified
				foreach (var programId in mappedRequiredProgramsOfStudy)
					mappedProgramsOfStudy.AddToUniqueList(programId);

				specialElement.AddCustomFilterNode(lensService.ProgramOfStudyCustomFilterTag, string.Join(",", mappedProgramsOfStudy));
			}

			// Required programs of study
			specialElement.AddCustomFilterNode(lensService.RequiredProgramOfStudyIdFilterTag, requiredProgramOfStudyCustomFilterValue);

			// Home-based jobs
			specialElement.AddCustomFilterNode(lensService.HomeBasedTag, jobdocElement.XPathAsString("//suitable_for_homeworker"));

			// Commission-based jobs
			specialElement.AddCustomFilterNode(lensService.CommissionBasedTag, jobdocElement.XPathAsString("//commission_based"));

			// Salary and commission-based jobs
			specialElement.AddCustomFilterNode(lensService.SalaryAndCommissionBasedTag, jobdocElement.XPathAsString("//salary_and_commission_based"));

			// Also add keyword filters for languages, as these need to support AND and OR
			var languagesNode = jobdocElement.XPathSelectElement("//languages");
			if (languagesNode.IsNotNull() && languagesNode.HasElements)
			{
				var languageFilter = new StringBuilder();
				foreach (var languageNode in languagesNode.Elements("language"))
				{
					languageFilter.Append(languageNode.XPathAsString("language"));
					languageFilter.Append(languageNode.XPathAsString("proficiency"));
					languageFilter.Append(" ");
				}
				languagesNode.CreateElement("languagefilter", languageFilter.ToString());
			}

			return jobdocElement.ToString();
		}

		/// <summary>
		/// Gets the resume from tagged resume.
		/// </summary>
		/// <param name="taggedResume">The tagged resume.</param>
		/// <returns></returns>
		public static string GetResumeFromTaggedResume(this string taggedResume)
		{
			if (string.IsNullOrEmpty(taggedResume))
				return string.Empty;

			try
			{
				var xDoc = new XmlDocument {PreserveWhitespace = true};
				xDoc.LoadXml(taggedResume);

				var resumeNode = xDoc.SelectSingleNode("//resume");

				return resumeNode == null ? string.Empty : resumeNode.OuterXml;
			}
			catch
			{
				return string.Empty;
			}
		}

		/// <summary>
		/// Gets the HTML from tagged document.
		/// </summary>
		/// <param name="taggedDocument">The tagged document.</param>
		/// <returns></returns>
		public static string GetHtmlFromTaggedDocument(this string taggedDocument)
		{
			if (string.IsNullOrEmpty(taggedDocument))
				return string.Empty;

			try
			{
				var xDoc = new XmlDocument {PreserveWhitespace = true};
				xDoc.LoadXml(taggedDocument);

				var htmlNode = xDoc.SelectSingleNode("//htmdoc");

				if (htmlNode.IsNull()) return string.Empty;

				var cdataElement = htmlNode.ChildNodes.OfType<XmlCDataSection>().FirstOrDefault();

				var html = (cdataElement.IsNull()) ? string.Empty : cdataElement.Value;

				// Remove any branding
				html = Regex.Replace(html, @"\<span.*?\>Created by the HTML-to-RTF Pro DLL \.Net [0123456789.]+\</span\>", "");

				return html;
			}
			catch
			{
				return string.Empty;
			}
		}

		/// <summary>
		/// Removes the HTML from tagged document.
		/// </summary>
		/// <param name="taggedDocument">The tagged document.</param>
		/// <returns></returns>
		public static string RemoveHtmlFromTaggedDocument(this string taggedDocument)
		{
			if (string.IsNullOrEmpty(taggedDocument))
				return taggedDocument;

			try
			{
				var xDoc = new XmlDocument {PreserveWhitespace = true};
				xDoc.LoadXml(taggedDocument);

				var htmlNode = xDoc.SelectSingleNode("//htmdoc");

				if (htmlNode.IsNotNull() && htmlNode.ParentNode.IsNotNull()) htmlNode.ParentNode.RemoveChild(htmlNode);

				return xDoc.OuterXml;
			}
			catch
			{
				return taggedDocument;
			}
		}

		/// <summary>
		/// Converts Julian days to date.
		/// </summary>
		/// <param name="julianDays">The julian days.</param>
		/// <returns></returns>
		private static DateTime JulianDaysToDate(double julianDays)
		{
			try
			{
				if (julianDays <= 0) return DateTime.MinValue;

				// From stack overflow http://stackoverflow.com/questions/5248827/convert-datetime-to-julian-date-in-c-sharp-tooadate-safe
				// To convert from Julian days you can use return DateTime.FromOADate(julianDays - 2415018.5)
				// BUT
				// Current values in lens are Julian Days from 1 Jan 01 which is 1721423.5
				// So we have to use the value (2415018.5 - 1721423.5 + 1) = 693594
				var oleAutomationDate = julianDays - 693596;
				return DateTime.FromOADate(oleAutomationDate);
			}
			catch
			{
				return DateTime.MinValue;
			}
		}

		/// <summary>
		/// Adds the custom filter node.
		/// </summary>
		/// <param name="specialElement">The special element.</param>
		/// <param name="customFilterTag">The custom filter tag.</param>
		/// <param name="value">The value.</param>
		/// <returns></returns>
		private static XElement AddCustomFilterNode(this XElement specialElement, string customFilterTag, string value)
		{
			if (customFilterTag.IsNullOrEmpty())
				return specialElement;

			specialElement.GetOrCreateElement(customFilterTag, customFilterTag, value);

			return specialElement;
		}

		/// <summary>
		/// Gets the endorsements custom filter value.
		/// </summary>
		/// <param name="resdocElement">The resdoc element.</param>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <returns></returns>
		private static string GetEndorsementsCustomFilterValue(this XElement resdocElement, IRuntimeContext runtimeContext)
		{
			var endorsementLookups = runtimeContext.Helpers.Lookup.GetLookup(LookupTypes.DrivingLicenceEndorsements);

			if (endorsementLookups.IsNullOrEmpty())
				return String.Empty;

			var endorsementsStringBuilder = new StringBuilder("");

			foreach (var lookup in endorsementLookups.Where(lookup => resdocElement.XPathAsString(string.Format("//{0}", lookup.ExternalId)).AsBoolean()))
			{
				endorsementsStringBuilder.AppendFormat("{0},", lookup.CustomFilterId);
			}

			var endorsementsString = endorsementsStringBuilder.ToString();

			return (endorsementsString.IsNotNullOrEmpty()) ? endorsementsString.Substring(0, endorsementsString.Length - 1) : String.Empty;
		}

		private static string GetIndustriesCustomFilterValue(this XElement resdocElement, IRuntimeContext runtimeContext)
		{
			var industries = resdocElement.XPathSelectElements("//education/targetindustries/industrycode | //education/mostexperienceindustries/industrycode").Select(e => e.Value).ToList();

			return (industries.IsNotNullOrEmpty()) ? String.Join(",", industries.Distinct()) : String.Empty;
		}

		/// <summary>
		/// Gets the NCRC custom filter value.
		/// </summary>
		/// <param name="resdocElement">The resdoc element.</param>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <returns></returns>
		private static string GetNCRCCustomFilterValue(this XElement resdocElement, IRuntimeContext runtimeContext)
		{
			var careerLevelLookups = runtimeContext.Helpers.Lookup.GetLookup(LookupTypes.NCRCLevel);

			if (careerLevelLookups.IsNullOrEmpty())
				return String.Empty;

			var careerReadinessLevel = resdocElement.XPathAsString("//ResDoc/resume/education/ncrc_level");
			if (careerReadinessLevel.IsNotNullOrEmpty())
				return careerLevelLookups.Where(lookup => lookup.Id == careerReadinessLevel.ToLong()).Select(lookup => lookup.ExternalId).FirstOrDefault() ?? string.Empty;

			return String.Empty;
		}

		/// <summary>
		/// Gets the shift type custom filter value.
		/// </summary>
		/// <param name="resdocElement">The resdoc element.</param>
		/// <returns></returns>
		private static string GetShiftTypeCustomFilterValue(this XElement resdocElement)
		{
			var shiftsStringBuilder = new StringBuilder("");

			if (resdocElement.XPathAsString("//shift_first_flag").AsBoolean())
				shiftsStringBuilder.Append("201,");

			if (resdocElement.XPathAsString("//shift_second_flag").AsBoolean())
				shiftsStringBuilder.Append("202,");

			if (resdocElement.XPathAsString("//shift_third_flag").AsBoolean())
				shiftsStringBuilder.Append("203,");

			if (resdocElement.XPathAsString("//shift_rotating_flag").AsBoolean())
				shiftsStringBuilder.Append("204,");

			if (resdocElement.XPathAsString("//shift_split_flag").AsBoolean())
				shiftsStringBuilder.Append("205,");

			if (resdocElement.XPathAsString("//shift_varies_flag").AsBoolean())
				shiftsStringBuilder.Append("206,");

			var shiftsString = shiftsStringBuilder.ToString();

			return (shiftsString.IsNotNullOrEmpty()) ? shiftsString.Substring(0, shiftsString.Length - 1) : String.Empty;
		}

		/// <summary>
		/// Gets the GPA custom filter value.
		/// </summary>
		/// <param name="resdocElement">The resdoc element.</param>
		/// <returns></returns>
		private static string GetGPACustomFilterValue(this XElement resdocElement)
		{
			var gpaValuesStringBuilder = new StringBuilder("");

			var gpaNodes = resdocElement.XPathSelectElements("//gpa");

			foreach (var node in gpaNodes)
			{
				var gpa = node.Value;

				if (gpa.IsNotNullOrEmpty())
					gpaValuesStringBuilder.AppendFormat("{0},", gpa);
			}

			var gpaValuesString = gpaValuesStringBuilder.ToString();

			return (gpaValuesString.IsNotNullOrEmpty()) ? gpaValuesString.Substring(0, gpaValuesString.Length - 1) : String.Empty;
		}

		/// <summary>
		/// Gets the degree completion date.
		/// </summary>
		/// <param name="resdocElement">The resdoc element.</param>
		/// <returns></returns>
		private static string GetDegreeCompletionDate(this XElement resdocElement)
		{
			var latestCompletionDate = DateTime.MinValue;

			var schoolNodes = resdocElement.XPathSelectElements("//education/school");

			foreach (var node in schoolNodes)
			{
				var expectedCompletionDate = node.GetElementValueAsDateTime("expectedcompletiondate").AsNull<DateTime>();
				var completionDate = node.GetElementValueAsDateTime("completiondate").AsNull<DateTime>();

				if (expectedCompletionDate.IsNotNull() && expectedCompletionDate > latestCompletionDate)
					latestCompletionDate = expectedCompletionDate.Value;

				if (completionDate.IsNotNull() && completionDate > latestCompletionDate)
					latestCompletionDate = completionDate.Value;
			}

			return (latestCompletionDate > DateTime.MinValue) ? latestCompletionDate.GetJulianDays().ToString() : String.Empty;
		}

		/// <summary>
		/// Gets the R onet custom filter value.
		/// </summary>
		/// <param name="dataElementsElement">The data elements element.</param>
		/// <returns></returns>
		private static string GetROnetCustomFilterValue(this XElement dataElementsElement)
		{
			var rOnet = "";

			var bgtOcc = dataElementsElement.XPathAsString("//BGTOcc");

			if (bgtOcc.IsNotNullOrEmpty())
				rOnet = bgtOcc.AsLensROnet();

			return rOnet;
		}

		/// <summary>
		/// Gets the internship categories filter value.
		/// </summary>
		/// <param name="dataElementsElement">The data elements element.</param>
		/// <returns></returns>
		private static string GetInternshipCategoriesFilterValue(this XElement dataElementsElement)
		{
			var categoryIds = string.Empty;

			var jobTitle = dataElementsElement.XPathAsString("//CleanJobTitle");
			var skillClustersString = dataElementsElement.XPathAsString("//CanonSkillClusters");

			if (jobTitle.IsNullOrEmpty() && skillClustersString.IsNullOrEmpty())
				return string.Empty;

			// Skill Clusters are seperated by pipes
			var skillClusters = (skillClustersString.IsNotNullOrEmpty()) ? skillClustersString.Split('|').ToList() : new List<string>();
			var splittedJobTitle = (jobTitle.IsNotNullOrEmpty()) ? jobTitle.Split(new char[0]) : new string[0];

			var internshipRules = GetInternshipCategoryRules();

			var ruleValueDelimeter = "ˆ".ToCharArray();

			foreach (var rule in internshipRules.Rules)
			{
				var categoryFound = false;

				foreach (var searchParameter in rule.SearchParamater.Split(','))
				{
					switch (searchParameter)
					{
						case "title":
							if (jobTitle.IsNotNullOrEmpty() && rule.Titles.IsNotNullOrEmpty())
							{
								foreach (var ruleJobTitle in rule.Titles.Split(ruleValueDelimeter))
								{
									var splittedRuleJobTitle = ruleJobTitle.Split(new char[0]);
									var wordCount = splittedRuleJobTitle.Length;

									if (wordCount > 1)
									{
										if (jobTitle.Trim().ToLower().Contains(ruleJobTitle.Trim().ToLower()))
										{
											categoryFound = true;
											break;
										}
									}
									else
									{
										foreach (var _splittedJobTitle in splittedJobTitle)
										{
											if (_splittedJobTitle.ToLower().Trim() == ruleJobTitle.ToLower().Trim())
											{
												categoryFound = true;
												break;
											}
										}
									}

									if (categoryFound)
										break;
								}
							}
							break;

						case "skillcluster":
							if (skillClusters.IsNotNullOrEmpty() && rule.SkillClusters.IsNotNullOrEmpty())
							{
								foreach (var ruleSkillCluster in rule.SkillClusters.Split(ruleValueDelimeter))
								{
									foreach (var skillCluster in skillClusters)
									{
										if (skillCluster.Trim().ToLower().Contains(ruleSkillCluster.Trim().ToLower()))
										{
											categoryFound = true;
											break;
										}
									}

									if (categoryFound)
										break;
								}

								// Revoking categoryFound if one of the Not Skill Clusters is present
								if (categoryFound && rule.NotSkillClusters.IsNotNullOrEmpty())
								{
									foreach (var ruleNotSkillCluster in rule.NotSkillClusters.Split(ruleValueDelimeter))
									{
										foreach (var skillCluster in skillClusters)
										{
											if (skillCluster.Trim().ToLower().Contains(ruleNotSkillCluster.Trim().ToLower()))
											{
												categoryFound = false;
												break;
											}
										}

										if (!categoryFound)
											break;
									}
								}
							}
							break;
					}

					if (categoryFound)
						break;
				}

				if (categoryFound)
					categoryIds += rule.Id + ",";
			}

			return (categoryIds.EndsWith(",")) ? categoryIds.Substring(0, categoryIds.Length - 1) : categoryIds;
		}

		/// <summary>
		/// Gets the internship category rules.
		/// </summary>
		/// <returns></returns>
		private static InternshipCategoryRules GetInternshipCategoryRules()
		{
			var document = GetRulesDocument("Focus.Services.Resources.InternshipCategoryRules.xml");
			return (InternshipCategoryRules) document.OuterXml.Deserialize(typeof (InternshipCategoryRules));
		}

		/// <summary>
		/// Gets the mapped programs of study for the posting.
		/// </summary>
		/// <param name="dataElementsElement">The data elements element.</param>
		/// <param name="clientDataTag">The client data tag.</param>
		/// <returns></returns>
		public static List<int> GetMappedProgramOfStudy(this XElement dataElementsElement, string clientDataTag)
		{
			var programIds = new List<int>();
			var programRules = GetProgramOfStudyRules(clientDataTag);

			if (programRules.IsNull() || programRules.Rules.IsNullOrEmpty())
				return programIds;

			var jobTitle = dataElementsElement.XPathAsString("//CleanJobTitle");
			var skillClustersString = dataElementsElement.XPathAsString("//CanonSkillClusters");
			var canonSkills = dataElementsElement.XPathAttributeAsListOfString("//canonskill/@name");
			var bgtocc = dataElementsElement.XPathAsString("//BGTOcc").Replace("S", "9"); // I have been told Job Mine returns numeric only ronets but doing the replcement just in case
			var consolidatedDegreeLevelsString = dataElementsElement.XPathAsString("//ConsolidatedDegreeLevels");
			var standardMajorsString = dataElementsElement.XPathAsString("//StdMajor");
			var jobUrl = dataElementsElement.XPathAsString("//JobUrl");
			var consolidatedDegreesString = dataElementsElement.XPathAsString("//ConsolidatedDegree");
			var consolidatedInferredNaicsString = dataElementsElement.XPathAsString("//ConsolidatedInferredNAICS");
			var consolidatedOnetsString = dataElementsElement.XPathAsString("//ConsolidatedONET");
			var canonEmployer = dataElementsElement.XPathAsString("//CanonEmployer");

			if (jobTitle.IsNullOrEmpty() && skillClustersString.IsNullOrEmpty() && canonSkills.IsNullOrEmpty() && bgtocc.IsNullOrEmpty() && consolidatedDegreeLevelsString.IsNullOrEmpty()
			    && standardMajorsString.IsNullOrEmpty() && jobUrl.IsNullOrEmpty() && consolidatedDegreesString.IsNullOrEmpty() && consolidatedInferredNaicsString.IsNullOrEmpty()
			    && consolidatedOnetsString.IsNullOrEmpty() && canonEmployer.IsNullOrEmpty())
				return programIds;

			// Skill Clusters, Consolidated Degree Levels, Standard Majors, Consolidated Degrees and Consolidated Onets are seperated by pipes in the job mine data
			var skillClusters = (skillClustersString.IsNotNullOrEmpty()) ? skillClustersString.Split('|').ToList() : new List<string>();
			var consolidatedDegreeLevels = (consolidatedDegreeLevelsString.IsNotNullOrEmpty()) ? consolidatedDegreeLevelsString.Split('|').ToList() : new List<string>();
			var standardMajors = (standardMajorsString.IsNotNullOrEmpty()) ? standardMajorsString.Split('|').ToList() : new List<string>();
			var consolidatedDegrees = (consolidatedDegreesString.IsNotNullOrEmpty()) ? consolidatedDegreesString.Split('|').ToList() : new List<string>();
			var consolidatedOnets = (consolidatedOnetsString.IsNotNullOrEmpty()) ? consolidatedOnetsString.Split('|').ToList() : new List<string>();

			// Consolidated Inferred Naics are seperated by semi-colons in the job mine data
			var consolidatedInferredNaics = (consolidatedInferredNaicsString.IsNotNullOrEmpty()) ? consolidatedInferredNaicsString.Split(';').ToList() : new List<string>();

			foreach (var rule in programRules.Rules)
			{
				var programMatched = false;

				//If we have already found a match for the program id no need to check again
				if (!programIds.Contains(rule.ProgramId) && rule.Enabled)
				{
					foreach (var searchParameter in rule.SearchParamater.Split(','))
					{
						var parameterMatched = false;

						switch (searchParameter.ToLower())
						{
							case "jobtitle":
								parameterMatched = jobTitle.ContainsRuleParameter(rule.JobTitles);
								break;
							case "notjobtitle":
								parameterMatched = !jobTitle.ContainsRuleParameter(rule.NotJobTitles);
								break;
							case "canonskills":
								parameterMatched = canonSkills.ContainsRuleParameter(rule.CanonSkills);
								break;
							case "skillcluster":
								parameterMatched = skillClusters.ContainsRuleParameter(rule.SkillClusters);
								break;
							case "bgtocc":
								parameterMatched = bgtocc.ContainsRuleParameter(rule.BgtOccs);
								break;
							case "consolidateddegreelevel":
								parameterMatched = consolidatedDegreeLevels.ContainsRuleParameter(rule.ConsolidatedDegreeLevels);
								break;
							case "standardmajor":
								parameterMatched = standardMajors.ContainsRuleParameter(rule.StandardMajors);
								break;
							case "url":
								parameterMatched = jobUrl.ContainsRuleParameter(rule.Urls);
								break;
							case "canondegree":
								parameterMatched = consolidatedDegrees.ContainsRuleParameter(rule.CanonDegrees);
								break;
							case "naics2":
								parameterMatched = consolidatedInferredNaics.StartsWithRuleParameter(rule.Naics2s);
								break;
							case "notonet2":
								parameterMatched = !consolidatedOnets.StartsWithRuleParameter(rule.NotOnet2s);
								break;
							case "onet":
								parameterMatched = consolidatedOnets.ContainsRuleParameter(rule.Onets);
								break;
							case "canonemployer":
								parameterMatched = canonEmployer.ContainsRuleParameter(rule.CanonEmployers);
								break;
						}

						var exitRule = false;

						switch (rule.SearchCondition.ToLower())
						{
							case "and":
								if (!parameterMatched)
								{
									programMatched = false;
									exitRule = true;
								}
								else
								{
									programMatched = true;
								}
								break;
							default:
								if (parameterMatched)
								{
									programMatched = true;
									exitRule = true;
								}
								break;
						}

						if (exitRule)
							break;
					}
				}

				// Do we need to add program to result
				if (programMatched)
					programIds.AddToUniqueList(rule.ProgramId);
			}

			return programIds;
		}

		/// <summary>
		/// Gets the program of study rules.
		/// </summary>
		/// <param name="clientDataTag">The client data tag.</param>
		/// <returns></returns>
		public static ProgramOfStudyRules GetProgramOfStudyRules(string clientDataTag)
		{
			var document = GetRulesDocument("Focus.Services.Resources.ProgramOfStudyRules.xml");
			var allRules = (ProgramOfStudyRules) document.OuterXml.Deserialize(typeof (ProgramOfStudyRules));

			var rules = new ProgramOfStudyRules {Rules = allRules.Rules.Where(x => x.ClientDataTag == clientDataTag).ToList()};

			return rules;
		}

		/// <summary>
		/// Gets the rules document.
		/// </summary>
		/// <param name="embeddedResourceName">Name of the embedded resource.</param>
		/// <returns></returns>
		private static XmlDocument GetRulesDocument(string embeddedResourceName)
		{
			var assembly = Assembly.GetExecutingAssembly();
			var stream = assembly.GetManifestResourceStream(embeddedResourceName);
			var document = new XmlDocument();

			if (stream.IsNull())
			{
				throw new FileNotFoundException("Could not find embedded resource file.", embeddedResourceName);
			}

			document.Load(stream);

			return document;
		}

		private static string GetLanguagesCustomFilterValue(this XElement resdocElement)
		{
			var languages = resdocElement.XPathSelectElements("//skills/languages/language").Select(l => l.Value).ToList();

			return (languages.IsNotNullOrEmpty()) ? String.Join(",", languages.Distinct()) : String.Empty;
		}

		/// <summary>
		/// Determines if the value contains one of the rule parameters.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <param name="parameterValue">The parameter value.</param>
		/// <returns></returns>
		private static bool ContainsRuleParameter(this string value, string parameterValue)
		{
			if (parameterValue.IsNullOrEmpty() || value.IsNullOrEmpty())
				return false;

			var parameterValues = parameterValue.Split(',');

			foreach (var matchValue in parameterValues)
			{
				// "*" denotes a wildcard match
				if (matchValue.Contains("*"))
				{
					var matchSplitValues = matchValue.Split('*');
					var matched = true;

					foreach (var matchSplitValue in matchSplitValues)
					{
						matched = value.Contains(matchSplitValue, StringComparison.OrdinalIgnoreCase);

						if (!matched)
							break;
					}

					if (matched)
						return true;
				}
				else
				{
					if (value.Contains(matchValue, StringComparison.OrdinalIgnoreCase))
						return true;
				}
			}

			return false;
		}

		/// <summary>
		/// Determines if one of the strings in the list contains one of the rules parameter.
		/// </summary>
		/// <param name="values">The values.</param>
		/// <param name="parameterValue">The parameter value.</param>
		/// <returns></returns>
		private static bool ContainsRuleParameter(this List<string> values, string parameterValue)
		{
			if (parameterValue.IsNullOrEmpty() || values.IsNullOrEmpty())
				return false;

			return values.Any(value => value.ContainsRuleParameter(parameterValue));
		}

		/// <summary>
		/// Determines if the value starts with any of the rule parameters.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <param name="parameterValue">The parameter value.</param>
		/// <returns></returns>
		private static bool StartsWithRuleParameter(this string value, string parameterValue)
		{
			if (parameterValue.IsNullOrEmpty() || value.IsNullOrEmpty())
				return false;

			var parameterValues = parameterValue.Split(',');

			return parameterValues.Any(matchValue => value.StartsWith(matchValue, StringComparison.OrdinalIgnoreCase));
		}

		/// <summary>
		/// Determines if any of the values in the list starts with any of the rule parameters.
		/// </summary>
		/// <param name="values">The values.</param>
		/// <param name="parameterValue">The parameter value.</param>
		/// <returns></returns>
		private static bool StartsWithRuleParameter(this List<string> values, string parameterValue)
		{
			if (parameterValue.IsNullOrEmpty() || values.IsNullOrEmpty())
				return false;

			return values.Any(value => value.StartsWithRuleParameter(parameterValue));
		}

		/// <summary>
		/// Adds to unique list.
		/// </summary>
		/// <param name="currentList">The current list.</param>
		/// <param name="newValue">The new value.</param>
		/// <returns></returns>
		private static List<int> AddToUniqueList(this List<int> currentList, int newValue)
		{
			if ((!currentList.Contains(newValue)))
				currentList.Add(newValue);

			return currentList;
		}


		/// <summary>
		/// Gets the certificates custom filter value.
		/// </summary>
		/// <param name="resdocElement">The resdoc element.</param>
		/// <returns></returns>
		private static string GetCertificatesCustomFilterValue(this XElement resdocElement)
		{
			var certificatesStringBuilder = new StringBuilder("");

			var certificateNodes = resdocElement.XPathSelectElements("//certificate");

			foreach (var certificateNode in certificateNodes)
			{
				if (certificateNode.Value.IsNotNullOrEmpty())
					certificatesStringBuilder.AppendFormat("{0},", certificateNode.Value);
			}

			var certificatesString = certificatesStringBuilder.ToString();

			return (certificatesString.IsNotNullOrEmpty()) ? certificatesString.Substring(0, certificatesString.Length - 1) : String.Empty;
		}

		/// <summary>
		/// Gets the required program of study custom filter value.
		/// </summary>
		/// <param name="clientJobDataElement">The client job data element.</param>
		/// <returns></returns>
		private static string GetRequiredProgramOfStudyCustomFilterValue(this XElement clientJobDataElement)
		{
			var requiredProgramOfStudyIdsStringBuilder = new StringBuilder("");

			var requiredProgramOfStudyNodes = clientJobDataElement.XPathSelectElements("//REQUIREMENTS/REQUIREMENT/PROGRAMOFSTUDY");

			foreach (var node in requiredProgramOfStudyNodes)
			{
				var id = node.XPathAsString("@ID");

				if (id.IsNotNullOrEmpty())
					requiredProgramOfStudyIdsStringBuilder.AppendFormat("{0},", id);
			}

			var requiredProgramOfStudyIdsString = requiredProgramOfStudyIdsStringBuilder.ToString();

			return (requiredProgramOfStudyIdsString.IsNotNullOrEmpty()) ? requiredProgramOfStudyIdsString.Substring(0, requiredProgramOfStudyIdsString.Length - 1) : String.Empty;
		}

		/// <summary>
		/// Converts the lens id to a frequency.
		/// </summary>
		/// <param name="lensFrequencyId">The lens frequency id.</param>
		/// <returns></returns>
		private static Frequencies ToFrequency(this string lensFrequencyId)
		{
			var frequency = Frequencies.Yearly;

			switch (lensFrequencyId)
			{
				case "1":
					frequency = Frequencies.Hourly;
					break;
				case "2":
					frequency = Frequencies.Daily;
					break;
				case "3":
					frequency = Frequencies.Weekly;
					break;
				case "4":
					frequency = Frequencies.Monthly;
					break;
			}

			return frequency;
		}

		/// <summary>
		/// Adds the state custom filter node.
		/// </summary>
		/// <param name="specialElement">The special element.</param>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <param name="customFilterTag">The custom filter tag.</param>
		/// <param name="stateCode">The state code.</param>
		/// <param name="culture">The culture.</param>
		/// <returns></returns>
		private static XElement AddStateCustomFilterNode(this XElement specialElement, IRuntimeContext runtimeContext, string customFilterTag, string stateCode, string culture)
		{
			if (customFilterTag.IsNullOrEmpty())
				return specialElement;

			var stateCustomFilterId = (int?) null;

			if (stateCode.IsNotNullOrEmpty())
				stateCustomFilterId = runtimeContext.Helpers.Lookup.GetLookup(LookupTypes.States).Where(x => x.ExternalId == stateCode).Select(x => x.CustomFilterId).FirstOrDefault();

			return specialElement.AddCustomFilterNode(customFilterTag, (stateCustomFilterId.HasValue) ? stateCustomFilterId.Value.ToString() : string.Empty);
		}

		/// <summary>
		/// Adds the green job custom filter node.
		/// </summary>
		/// <param name="specialElement">The special element.</param>
		/// <param name="customFilterTag">The custom filter tag.</param>
		/// <param name="skillRollup">The skill rollup.</param>
		/// <returns></returns>
		private static XElement AddGreenJobCustomFilterNode(this XElement specialElement, string customFilterTag, string skillRollup)
		{
			if (customFilterTag.IsNullOrEmpty())
				return specialElement;

			var isGreenJob = false;

			if (skillRollup.IsNotNullOrEmpty())
			{
				isGreenJob = skillRollup.ToLower().Contains("green: ");
			}

			return specialElement.AddCustomFilterNode(customFilterTag, (isGreenJob) ? "1" : "0");
		}

		/// <summary>
		/// Adds the sector custom filter node.
		/// </summary>
		/// <param name="specialElement">The special element.</param>
		/// <param name="customFilterTag">The custom filter tag.</param>
		/// <param name="onet">The onet.</param>
		/// <param name="naics">The naics.</param>
		/// <returns></returns>
		private static XElement AddSectorCustomFilterNode(this XElement specialElement, string customFilterTag, string onet, string naics)
		{
			if (customFilterTag.IsNullOrEmpty())
				return specialElement;

			var sectorBuilder = new StringBuilder("");

			if (onet.IsNotNullOrEmpty())
				sectorBuilder.AppendFormat("2{0},", onet.Replace("-", "").Replace(".", ""));

			if (naics.IsNotNullOrEmpty())
			{
				var naicsString = (naics.Length > 4) ? naics.Substring(0, 4) : naics;
				sectorBuilder.AppendFormat("30000{0},", naicsString);
			}

			var sectors = (sectorBuilder.Length > 0) ? sectorBuilder.ToString().Substring(0, sectorBuilder.Length - 1) : String.Empty;
			return specialElement.AddCustomFilterNode(customFilterTag, sectors);
		}

		/// <summary>
		/// Adds the industry custom filter node.
		/// </summary>
		/// <param name="specialElement">The special element.</param>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <param name="customFilterTag">The custom filter tag.</param>
		/// <param name="naics">The naics.</param>
		/// <returns></returns>
		private static XElement AddIndustryCustomFilterNode(this XElement specialElement, IRuntimeContext runtimeContext, string customFilterTag, string naics)
		{
			if (customFilterTag.IsNullOrEmpty())
				return specialElement;

			// In case several naics have been passed we will select the first (hopefully this is the best match). Naics spilt on ; in Job/Mine
			if (naics.Contains(";"))
				naics = naics.Split(';')[0];

			if (naics.IsNotNullOrEmpty())
				naics = naics.RemoveNonNumericCharacters();

			var naics4 = "";

			if (naics.IsNotNullOrEmpty() && naics.Length > 1)
			{
				naics4 = (naics.Length > 4) ? naics.Substring(0, 4) : Get4DigitNaics(naics, runtimeContext);
			}

			return specialElement.AddCustomFilterNode(customFilterTag, naics4);
		}

		/// <summary>
		/// Get4s the digit naics that start with the current naics.
		/// </summary>
		/// <param name="currentNaics">The naics.</param>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <returns></returns>
		private static string Get4DigitNaics(string currentNaics, IRuntimeContext runtimeContext)
		{
			var naics = runtimeContext.Repositories.Library.NAICS.Where(x => x.Code.StartsWith(currentNaics) && x.Code.Length == 4).Select(x => x.Code).ToList();
			return naics.IsNullOrEmpty() ? "" : string.Join(",", naics);
		}

		#endregion

	}

	[XmlRoot("internship"), Serializable]
	public class InternshipCategoryRules
	{
		[XmlElement("area")]
		public List<InternshipCategoryRule> Rules { get; set; }
	}


	public class InternshipCategoryRule
	{
		[XmlAttribute("id")]
		public int Id { get; set; }

		[XmlAttribute("name")]
		public string Name { get; set; }

		[XmlAttribute("searchparam")]
		public string SearchParamater { get; set; }

		[XmlElement("title")]
		public string Titles { get; set; }

		[XmlElement("skillcluster")]
		public string SkillClusters { get; set; }

		[XmlElement("notskillcluster")]
		public string NotSkillClusters { get; set; }
	}

	[XmlRoot("programs"), Serializable]
	public class ProgramOfStudyRules
	{
		[XmlElement("program")]
		public List<ProgramOfStudyRule> Rules { get; set; }
	}

	public class ProgramOfStudyRule
	{
		[XmlAttribute("id")]
		public int ProgramId { get; set; }

		[XmlAttribute("name")]
		public string ProgramName { get; set; }

		[XmlAttribute("rcip")]
		public string RCip { get; set; }

		[XmlAttribute("clientdatatag")]
		public string ClientDataTag { get; set; }

		[XmlAttribute("searchparam")]
		public string SearchParamater { get; set; }

		[XmlAttribute("searchcondition")]
		public string SearchCondition { get; set; }

		[XmlAttribute("enabled")]
		public bool Enabled { get; set; }

		[XmlElement("bgtocc")]
		public string BgtOccs { get; set; }

		[XmlElement("standardmajor")]
		public string StandardMajors { get; set; }

		[XmlElement("consolidateddegreelevel")]
		public string ConsolidatedDegreeLevels { get; set; }

		[XmlElement("jobtitle")]
		public string JobTitles { get; set; }

		[XmlElement("canonskills")]
		public string CanonSkills { get; set; }

		[XmlElement("skillcluster")]
		public string SkillClusters { get; set; }

		[XmlElement("url")]
		public string Urls { get; set; }

		[XmlElement("naics2")]
		public string Naics2s { get; set; }

		[XmlElement("notonet2")]
		public string NotOnet2s { get; set; }

		[XmlElement("canondegree")]
		public string CanonDegrees { get; set; }

		[XmlElement("notjobtitle")]
		public string NotJobTitles { get; set; }

		[XmlElement("onet")]
		public string Onets { get; set; }

		[XmlElement("canonemployer")]
		public string CanonEmployers { get; set; }
	}
}
