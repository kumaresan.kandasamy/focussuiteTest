﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Xml.Linq;
using Focus.Core;
using Focus.Core.Criteria.CandidateSearch;
using Focus.Core.DataTransferObjects.FocusExplorer;
using Focus.Core.Models.Career;
using Focus.Core.Views;

#endregion

namespace Focus.Services.Repositories
{
	public interface ILensRepository
	{
		#region Resume Operations

		/// <summary>
		/// Searches the resumes.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <param name="culture">The culture.</param>
		/// <returns></returns>
		List<ResumeView> SearchResumes(CandidateSearchCriteria criteria, string culture);

		/// <summary>
		/// Gets the tagged the resume without skills.
		/// </summary>
		/// <param name="resume">The resume.</param>
		/// <param name="resumeFileExtension">The resume file extension.</param>
		/// <returns></returns>
		string ParseResume(byte[] resume, string resumeFileExtension);

		/// <summary>
		/// Gets the tagged resume with or without skills
		/// </summary>
		/// <param name="resume">The resume.</param>
		/// <param name="resumeFileExtension">The resume file extension.</param>
		/// <param name="resumeOnly">If true, return resume only else return full resume document</param>
		/// <returns></returns>
		string ParseResume(byte[] resume, string resumeFileExtension, bool resumeOnly);

    /// <summary>
    /// Registers the resume.
    /// </summary>
		/// <param name="lensId">The Lens Id (Focus uses the PersonId as the Id for Lens)</param>
    /// <param name="resumeXml">The resume XML.</param>
    /// <param name="resumeRegisteredOn">The resume registered on.</param>
    /// <param name="resumeModifiedOn">The resume modified on.</param>
    /// <param name="personId">The person id.</param>
    /// <param name="enrollmentStatus">The enrollment status.</param>
    /// <param name="degreeEducationLevelIds">The degree education level ids.</param>
    /// <param name="dateOfBirth">The date of birth.</param>
    void RegisterResume(string lensId, string resumeXml, DateTime resumeRegisteredOn, DateTime resumeModifiedOn, long personId, SchoolStatus? enrollmentStatus, List<long> degreeEducationLevelIds, DateTime? dateOfBirth);

		/// <summary>
		/// Unregisters the resume.
		/// </summary>
		/// <param name="lensIds">A list of ids for Lens (Focus uses the PersonId as the Id for Lens).</param>
		void UnregisterResume(List<string> lensIds);

		#endregion

		#region Posting Operations

		/// <summary>
		/// Searches the postings.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <param name="culture">The culture.</param>
		/// <param name="forAlert">if set to <c>true</c> for job alerts.</param>
		/// <param name="personId">The id of the person searching the postings.</param>
		/// <returns></returns>
		List<PostingSearchResultView> SearchPostings(SearchCriteria criteria, string culture, bool forAlert, long? personId = null);

		/// <summary>
		/// Registers the posting.
		/// </summary>
		/// <param name="lensPostingId">The lens posting id.</param>
		/// <param name="postingId">The posting id.</param>
		/// <param name="postingXml">The posting XML.</param>
		/// <param name="dataElementsXml">The data elements XML.</param>
		/// <param name="culture">The culture.</param>
		/// <param name="mappedRequiredProgramsOfStudy">The mapped required programs of study.</param>
    /// <param name="postalCode">The postal code.</param>
    void RegisterPosting(string lensPostingId, long? postingId, XDocument postingXml, string dataElementsXml, string culture, List<int> mappedRequiredProgramsOfStudy, PostalCodeDto postalCode = null);

		/// <summary>
		/// Unregisters the posting.
		/// </summary>
		/// <param name="lensPostingId">The lens posting id.</param>
		void UnregisterPosting(string lensPostingId);

		/// <summary>
		/// Canon the posting with job mine.
		/// </summary>
		/// <param name="postingXml">The posting XML.</param>
		/// <returns></returns>
		string CanonPostingWithJobMine(string postingXml);

		/// <summary>
		/// Orders the posting search results.
		/// </summary>
		/// <param name="postings">The postings.</param>
		/// <param name="matchedSearch">if set to <c>true</c> [matched search].</param>
		/// <param name="documentCount">The document count.</param>
		/// <returns></returns>
		List<PostingSearchResultView> OrderPostings(IEnumerable<PostingSearchResultView> postings, bool matchedSearch, int documentCount);

		#endregion

		#region Library Operations

		/// <summary>
		/// Gets the onet codes.
		/// </summary>
		/// <param name="jobTitle">The job title.</param>
		/// <returns></returns>
		List<string> GetOnetCodes(string jobTitle);

		#endregion

		#region Utility Operations
		
		/// <summary>
		/// Determines whether this instance is licensed.
		/// </summary>
		/// <returns>
		/// 	<c>true</c> if this instance is licensed; otherwise, <c>false</c>.
		/// </returns>
		bool IsLicensed();

		/// <summary>
		/// Clarifies the specified text.
		/// </summary>
		/// <param name="text">The text.</param>
		/// <param name="minScore">The min score.</param>
		/// <param name="maxSearchCount">The max search count.</param>
		/// <param name="type">The type.</param>
		/// <returns></returns>
		List<string> Clarify(string text, int minScore, int maxSearchCount, string type);

		/// <summary>
		/// Coverts the binary data to ASCII Text.
		/// </summary>
		/// <param name="binaryFileData">The binary file data.</param>
		/// <param name="fileExtension">The file extension.</param>
		/// <returns></returns>
		string CovertBinaryData(byte[] binaryFileData, string fileExtension);

		/// <summary>
		/// Canons the XML.
		/// </summary>
		/// <param name="xml">The XML.</param>
		/// <returns></returns>
		string Canon(string xml);

		/// <summary>
		/// Calculates a match score between a resume and a posting.
		/// </summary>
		/// <param name="resumeXml">The resume XML.</param>
		/// <param name="postingXml">The posting XML.</param>
		/// <returns></returns>
		int Match(string resumeXml, string postingXml);

		/// <summary>
		/// Tags the specified plain text.
		/// </summary>
		/// <param name="plainText">The plain text.</param>
		/// <param name="type">The type.</param>
		/// <returns></returns>
		string Tag(string plainText, DocumentType type);

		/// <summary>
		/// Tags the specified binary file data.
		/// </summary>
		/// <param name="binaryFileData">The binary file data.</param>
		/// <param name="fileExtension">The file extension.</param>
		/// <param name="type">The type.</param>
		/// <param name="html">The HTML.</param>
		/// <returns></returns>
		string TagWithHtml(byte[] binaryFileData, string fileExtension, DocumentType type, out string html);

		/// <summary>
		/// Executes the job mine command.
		/// </summary>
		/// <param name="command">The command.</param>
		/// <returns></returns>
		string ExecuteJobMineCommand(string command);

		#endregion
	}
}
