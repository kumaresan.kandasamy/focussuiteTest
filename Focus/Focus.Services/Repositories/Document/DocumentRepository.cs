﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.IO;
using Focus.Services.DocStoreService;
using Framework.Core;
using Framework.Logging;

#endregion



namespace Focus.Services.Repositories.Document
{
	public class DocumentRepository : IDocumentRepository
	{
		private readonly DocStoreSvc _documentService;

		/// <summary>
		/// Initializes a new instance of the <see cref="DocumentRepository"/> class.
		/// </summary>
		/// <param name="serviceUrl">The service URL.</param>
		public DocumentRepository(string serviceUrl)
		{
			_documentService = new DocStoreSvc { Url = serviceUrl };
		}

		
		/// <summary>
		/// Gets the document text.
		/// </summary>
		/// <param name="documentId">The document id.</param>
		/// <returns>The document text</returns>
		public string GetDocumentText(string documentId)
		{
			if (_documentService.Url.IsNullOrEmpty())
				throw new Exception("Document store service url is undefined.");

			var documentText = _documentService.FetchJob(documentId);

			if(documentText.Contains("<error>"))
			{
				var message = documentText.Replace("<error>", "").Replace("</error>", "");
				throw new Exception(string.Format("Error getting document text from document store - {0}", message));
			}

			return documentText;
		}
	}
}
