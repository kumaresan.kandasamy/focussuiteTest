﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using Focus.Core;
using Focus.Core.Criteria.JobDistribution;
using Focus.Core.Criteria.SpideredEmployer;
using Focus.Core.Criteria.SpideredPosting;
using Focus.Core.Models.Assist;
using Focus.Services.Repositories.LiveJobs.Entities;
using Focus.Services.Repositories.LiveJobs.Filters;
using Framework.Core;

#endregion

namespace Focus.Services.Repositories.LiveJobs.Mappers
{
	internal static class LiveJobsRepositoryMappers
	{
		/// <summary>
		/// Converts the criteria to the service filter.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		internal static EmployerFilter ToEmployerFilter (this SpideredEmployerCriteria criteria)
		{
			var filter = new EmployerFilter
			       	{
			       		Industry = (criteria.EmployerName.IsNotNullOrEmpty()) ? new IndustryFilter {WithEmployer = new[] { criteria.EmployerName.ToFilterWilcard() }} : null,
								Advanced = new AdvancedFilter
								           	{
								           		IncludeField = new[] { "canonemployer"},
															JobsPerPage = criteria.PageSize,
															MaxNumberOfRecords = criteria.ListSize,
															MinExpectedCount = criteria.MinimumPostingCount,
															PageNumber = criteria.PageIndex + 1
								           	},
								Location = (criteria.StateCodes.IsNotNullOrEmpty())  ? new LocationFilter { States = criteria.StateCodes.ToArray() } : null,
								TimePeriod = (criteria.PostingAge.HasValue) ? new TimePeriodFilter { To = DateTime.Today.AddDays(1), From = DateTime.Today.AddDays(1 - criteria.PostingAge.GetValueOrDefault()) } : null
			       	};

			switch(criteria.OrderBy)
			{
				case Constants.SortOrders.EmployerNameAsc :
					filter.Advanced.SortBy = "EMPLOYER";
					filter.Advanced.SortOrder = "ASC";
					break;
				case Constants.SortOrders.EmployerNameDesc:
					filter.Advanced.SortBy = "EMPLOYER";
					filter.Advanced.SortOrder = "DESC";
					break;
				default:
					filter.Advanced.SortBy = "COUNT";
					filter.Advanced.SortOrder = "DESC";
					break;
			}

			return filter;
		}

		/// <summary>
		/// Converts api response to spidered employer model paged list.
		/// </summary>
		/// <param name="result">The result.</param>
		/// <param name="pageIndex">Index of the page.</param>
		/// <param name="pageSize">Size of the page.</param>
		/// <returns></returns>
		internal static PagedList<SpideredEmployerModel> ToSpideredEmployerModelPagedList(this EmployerApiResult result, int pageIndex, int pageSize)
		{
			if (result.IsNull() || result.Employers.IsNullOrEmpty())
				return new PagedList<SpideredEmployerModel>(new List<SpideredEmployerModel>(), 0, pageIndex, pageSize, false);

			var employers = result.Employers.Select(employer => new SpideredEmployerModel {EmployerName = employer.Name, NumberOfPostings = employer.Count});

			return new PagedList<SpideredEmployerModel>(employers, result.TotalRecords, pageIndex, pageSize, false);
		}

		/// <summary>
		/// Converts the criteria to the service filter.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		internal static PostingFilter ToPostingFilter(this SpideredPostingCriteria criteria)
		{
			return new PostingFilter
			       	{
			       		Industry = (criteria.EmployerName.IsNotNullOrEmpty()) ? new IndustryFilter {WithEmployer = new[] {criteria.EmployerName.ToFilterExactMatch(true)}} : null,
			       		Advanced = new AdvancedFilter
			       		           	{
															IncludeField = new[] { "lensjobid|lensjobid", "cleanjobtitle|cleanjobtitle", "jobdate|jobdate" },
															JobsPerPage = criteria.PageSize,
			       		           		MaxNumberOfRecords = criteria.ListSize,
			       		           		PageNumber = criteria.PageIndex + 1,
															SortBy = "DATE",
															SortOrder = "DESC"
			       		           	},
								Location = (criteria.StateCodes.IsNotNullOrEmpty()) ? new LocationFilter { States = criteria.StateCodes.ToArray() } : null,
								TimePeriod = (criteria.PostingAge.HasValue) ? new TimePeriodFilter { To = DateTime.Today.AddDays(1), From = DateTime.Today.AddDays(1 - criteria.PostingAge.GetValueOrDefault()) } : null
			       	};
		}

		/// <summary>
		/// To the job distribution filter.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		internal static JobDistributionFilter ToJobDistributionFilter(this JobDistributionCriteria criteria)
		{
			return new JobDistributionFilter
			{
				Category = criteria.Category,
				Date = criteria.Date
			};
		}

		/// <summary>
		/// Converts api response to spidered posting model paged list.
		/// </summary>
		/// <param name="apiPostings">The API postings.</param>
		/// <param name="pageIndex">Index of the page.</param>
		/// <param name="pageSize">Size of the page.</param>
		/// <returns></returns>
		internal static PagedList<SpideredPostingModel> ToSpideredPostingModelPagedList(this Posting[] apiPostings, int pageIndex, int pageSize)
		{
			var postings = apiPostings.Select(posting => new SpideredPostingModel { LensPostingId = posting.LensJobId, JobTitle = posting.CleanJobTitle, PostingDate = posting.JobDate.ToDateTime()});
			return new PagedList<SpideredPostingModel>(postings, apiPostings.Count(), pageIndex, pageSize, false);
		}

		/// <summary>
		/// To the job distribution model list.
		/// </summary>
		/// <param name="result">The result.</param>
		/// <returns></returns>
		internal static List<JobDistributionModel> ToJobDistributionModelList(this JobDistributionApiResult result)
		{
			if (result.IsNull() || result.Sources.IsNullOrEmpty())
				return new List<JobDistributionModel>();

			var totals = result.Sources.Select(feed => new JobDistributionModel { FeedName = feed.Name, Count = Convert.ToInt32(feed.Count) }).ToList();

			return totals;
		}

		/// <summary>
		/// Converts string to the api filter wildcard format.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <returns></returns>
		private static string ToFilterWilcard(this string value)
		{
			return String.Format(@"""*{0}*""", value);
		}

		/// <summary>
		/// Converts string to the api filter exact match format.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <param name="doubleUpApostrophes">Whether to replace single apostrophes with double apostrophes (See FVN-3077)</param>
		/// <returns></returns>
		private static string ToFilterExactMatch(this string value, bool doubleUpApostrophes = false)
		{
			if (doubleUpApostrophes)
				value = value.Replace("'", "''");

			return String.Format(@"""{0}""", value);
		}

		/// <summary>
		/// Converts the api date time string to a date time.
		/// </summary>
		/// <param name="apiDate">The API date.</param>
		/// <returns></returns>
		private static DateTime ToDateTime(this string apiDate)
		{
			var date = new DateTime();

			if (apiDate.IsNullOrEmpty())
				return date;

			// set the culture to US as that is the format the date will come back in
			var culture = new System.Globalization.CultureInfo("en-US", true);

			DateTime.TryParse(apiDate, culture, System.Globalization.DateTimeStyles.None, out date);

			return date;
		}
	}
}
