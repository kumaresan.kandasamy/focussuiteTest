﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using Focus.Core;
using Focus.Core.Criteria.JobDistribution;
using Focus.Core.Criteria.SpideredEmployer;
using Focus.Core.Criteria.SpideredPosting;
using Focus.Core.Models.Assist;
using Focus.Core.Settings.Interfaces;
using Focus.Services.Repositories.LiveJobs.Entities;
using Focus.Services.Repositories.LiveJobs.Mappers;
using Framework.Api;
using Framework.Core;
using Newtonsoft.Json;

#endregion

namespace Focus.Services.Repositories.LiveJobs
{
	public class LiveJobsRepository : ILiveJobsRepository
	{
		private IAppSettings _appSettings;

		/// <summary>
		/// Initializes a new instance of the <see cref="LiveJobsRepository"/> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		public LiveJobsRepository(IRuntimeContext runtimeContext)
		{
			_appSettings = runtimeContext.AppSettings;
		}

		/// <summary>
		/// Gets the employers.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		public PagedList<SpideredEmployerModel> GetEmployers(SpideredEmployerCriteria criteria)
		{
			var client = GetWebApiClient("/jobs/distribution");

			var filter = criteria.ToEmployerFilter();

			var response = client.Post<EmployerApiResult>(filter, DateFormatHandling.MicrosoftDateFormat, DateTimeZoneHandling.Utc);
			
			return response.Result.ToSpideredEmployerModelPagedList(criteria.PageIndex, criteria.PageSize);
		}

		/// <summary>
		/// Gets the postings.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		public PagedList<SpideredPostingModel> GetPostings(SpideredPostingCriteria criteria)
		{
			var client = GetWebApiClient("/search/joblist");

			var filter = criteria.ToPostingFilter();

			// API returns the object as a string, so we have to deserialise it differently
			var response = client.Post<string>(filter, DateFormatHandling.MicrosoftDateFormat, DateTimeZoneHandling.Utc);

			var postings = response.Result.DeserializeJson(typeof (Posting[]));

			return ((Posting[])postings).ToSpideredPostingModelPagedList(criteria.PageIndex, criteria.PageSize);
		}

		/// <summary>
		/// Gets the job distribution.
		/// </summary>
		/// <param name="criteria">The criteria.</param>
		/// <returns></returns>
		public List<JobDistributionModel> GetJobDistribution(JobDistributionCriteria criteria)
		{
			var client = GetWebApiClient("/jobs/jobdistribution");

			var filter = criteria.ToJobDistributionFilter();

			var response = client.Post<JobDistributionApiResult>(filter, DateFormatHandling.MicrosoftDateFormat, DateTimeZoneHandling.Utc);

			return response.Result.ToJobDistributionModelList();
		}

		/// <summary>
		/// Gets the web API client.
		/// </summary>
		/// <param name="apiMethodPath">The API method path.</param>
		/// <returns></returns>
		private WebApiClient GetWebApiClient(string apiMethodPath)
		{
			var fullApiMethodPath = string.Format("{0}{1}", _appSettings.LiveJobsApiRootUrl, apiMethodPath);
			return new WebApiClient(fullApiMethodPath, _appSettings.LiveJobsApiKey, _appSettings.LiveJobsApiSecret, _appSettings.LiveJobsApiToken, _appSettings.LiveJobsApiTokenSecret);
		}
	}
}
