﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

#endregion

namespace Focus.Services.Repositories.LiveJobs.Entities
{
	public class EmployerApiResult
	{
		[DataMember(Name = "TotalRecords")]
		public int TotalRecords { get; set; }

		[DataMember(Name = "Employers")]
		public Employer[] Employers { get; set; }
	}
	
	public class Employer
	{
		[DataMember(Name = "Name")]
		public string Name { get; set; }

		[DataMember(Name = "Count")]
		public int Count { get; set; }
	}
}
