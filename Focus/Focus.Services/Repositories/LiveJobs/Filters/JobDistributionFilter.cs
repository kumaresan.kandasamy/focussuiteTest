﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;
using Focus.Core;

#endregion

namespace Focus.Services.Repositories.LiveJobs.Filters
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class JobDistributionFilter
	{
		[DataMember(Name = "category")]
		public string Category { get; set; }

		[DataMember(Name = "Date")]
		public string Date { get; set; }
	}
}
