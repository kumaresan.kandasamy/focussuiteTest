﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;
using Newtonsoft.Json;

#endregion

namespace Focus.Services.Repositories.LiveJobs.Filters
{
	public class LocationFilter
	{
		[DataMember(Name = "State")]
		[JsonProperty(PropertyName = "State")]
		public string[] States { get; set; }
	}
}
