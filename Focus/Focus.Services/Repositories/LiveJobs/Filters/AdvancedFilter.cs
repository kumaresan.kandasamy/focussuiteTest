﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

#endregion

namespace Focus.Services.Repositories.LiveJobs.Filters
{
	public class AdvancedFilter
	{
		[DataMember(Name = "MinExpectedCount")]
		public int MinExpectedCount { get; set; }

		[DataMember(Name = "MaxNumberOfRecords")]
		public int MaxNumberOfRecords { get; set; }

		[DataMember(Name = "SortBy")]
		public string SortBy { get; set; }

		[DataMember(Name = "SortOrder")]
		public string SortOrder { get; set; }

		[DataMember(Name = "PageNumber")]
		public int PageNumber { get; set; }

		[DataMember(Name = "JobsPerPage")]
		public int JobsPerPage { get; set; }

		[DataMember(Name = "IncludeField")]
		public string[] IncludeField { get; set; }
	}
}
