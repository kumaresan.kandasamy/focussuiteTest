﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

#endregion

namespace Focus.Services.Repositories.LiveJobs.Filters
{
	public class PostingFilter
	{
		[DataMember(Name = "Industry")]
		public IndustryFilter Industry { get; set; }

		[DataMember(Name = "TimePeriod")]
		public TimePeriodFilter TimePeriod { get; set; }

		[DataMember(Name = "Location")]
		public LocationFilter Location { get; set; }

		[DataMember(Name = "Advanced")]
		public AdvancedFilter Advanced { get; set; }
	}
}
