﻿#region Copyright © 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using Focus.Core;
using Focus.Core.Messages.AccountService;
using Focus.Services.ServiceContracts;

#endregion

namespace Focus.Services.ServiceClients
{
  [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
  public interface IAccountServiceChannel : IAccountService, System.ServiceModel.IClientChannel
  {
  }

  [System.Diagnostics.DebuggerStepThroughAttribute]
  [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
  public class AccountServiceClient : System.ServiceModel.ClientBase<IAccountService>, IAccountService
  {

    public AccountServiceClient()
    {
    }

    public AccountServiceClient(string endpointConfigurationName) :
      base(endpointConfigurationName)
    {
    }

    public AccountServiceClient(string endpointConfigurationName, string remoteAddress) :
      base(endpointConfigurationName, remoteAddress)
    {
    }

    public AccountServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) :
      base(endpointConfigurationName, remoteAddress)
    {
    }

    public AccountServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) :
      base(binding, remoteAddress)
    {
    }

    public BlockUnblockPersonResponse BlockUnblockPerson(BlockUnblockPersonRequest request)
    {
      return Channel.BlockUnblockPerson(request);
    }

    public ChangePasswordResponse ChangePassword(ChangePasswordRequest request)
    {
      return Channel.ChangePassword(request);
    }

    public ValidateEmployerResponse ValidateEmployer(ValidateEmployerRequest request)
    {
      return Channel.ValidateEmployer(request);
    }

    public RegisterTalentUserResponse RegisterTalentUser(RegisterTalentUserRequest request)
    {
      return Channel.RegisterTalentUser(request);
    }

    public RegisterTalentUserResponse UpdateTalentUser(RegisterTalentUserRequest request)
    {
      return Channel.UpdateTalentUser(request);
    }

    public CheckUserExistsResponse CheckUserExists(CheckUserExistsRequest request)
    {
      return Channel.CheckUserExists(request);
    }

    public CheckSocialSecurityNumberInUseResponse CheckSocialSecurityNumberInUse(
      CheckSocialSecurityNumberInUseRequest request)
    {
      return Channel.CheckSocialSecurityNumberInUse(request);
    }

    public FindUserResponse FindUser(FindUserRequest request)
    {
      return Channel.FindUser(request);
    }

	  public FindUserResponse FindUserByExternalId(FindUserRequest request)
	  {
		  return Channel.FindUserByExternalId(request);
	  }

	  public CreateAssistUserResponse CreateAssistUser(CreateAssistUserRequest request)
    {
      return Channel.CreateAssistUser(request);
    }

    public DeleteUserResponse DeleteUser(DeleteUserRequest request)
    {
      return Channel.DeleteUser(request);
    }

    public ResetUsersPasswordResponse ResetUsersPassword(ResetUsersPasswordRequest request)
    {
      return Channel.ResetUsersPassword(request);
    }

    public DeactivateUserResponse DeactivateUser(DeactivateUserRequest request)
    {
      return Channel.DeactivateUser(request);
    }

		public GetUserTypeResponse GetUserType(GetUserTypeRequest request)
		{
			return Channel.GetUserType(request);
		}

    public GetUserDetailsResponse GetUserDetails(GetUserDetailsRequest request)
    {
      return Channel.GetUserDetails(request);
    }

    public ChangeUsernameResponse ChangeUsername(ChangeUsernameRequest request)
    {
      return Channel.ChangeUsername(request);
    }

		public CheckEmailAddressResponse CheckEmailAddress(CheckEmailAddressRequest request)
		{
			return Channel.CheckEmailAddress(request);
		}

    public ChangeEmailAddressResponse ChangeEmailAddress(ChangeEmailAddressRequest request)
    {
      return Channel.ChangeEmailAddress(request);
    }

    public UpdateContactDetailsResponse UpdateContactDetails(UpdateContactDetailsRequest request)
    {
      return Channel.UpdateContactDetails(request);
    }

		public GetSSNResponse GetSSN(GetSSNRequest request)
		{
			return Channel.GetSSN(request);
		}

		public UpdateSsnResponse UpdateSsn(UpdateSsnRequest request)
		{
			return Channel.UpdateSsn(request);
		}

		public UserLookupResponse GetUserLookup(UserLookupRequest request)
    {
      return Channel.GetUserLookup(request);
    }

    public RoleResponse GetRole(RoleRequest request)
    {
      return Channel.GetRole(request);
    }

    public UpdateUsersRolesResponse UpdateUsersRoles(UpdateUsersRolesRequest request)
    {
      return Channel.UpdateUsersRoles(request);
    }

    public ActivateUserResponse ActivateUser(ActivateUserRequest request)
    {
      return Channel.ActivateUser(request);
    }

    public SendAccountActivationEmailResponse SendAccountActivationEmail(SendAccountActivationEmailRequest request)
    {
      return Channel.SendAccountActivationEmail(request);
    }

    public ResetPasswordResponse ResetPassword(ResetPasswordRequest request)
    {
      return Channel.ResetPassword(request);
    }

    public RegisterExplorerUserResponse RegisterExplorerUser(RegisterExplorerUserRequest request)
    {
      return Channel.RegisterExplorerUser(request);
    }

    public UpdateScreenNameAndEmailAddressResponse UpdateScreenNameAndEmailAddress(UpdateScreenNameAndEmailAddressRequest request)
    {
      return Channel.UpdateScreenNameAndEmailAddress(request);
    }

    public ChangeSecurityQuestionResponse ChangeSecurityQuestion(ChangeSecurityQuestionRequest request)
    {
      return Channel.ChangeSecurityQuestion(request);
    }

    public RegisterCareerUserResponse RegisterCareerUser(RegisterCareerUserRequest request)
    {
      return Channel.RegisterCareerUser(request);
    }

    public GetUserAlertResponse GetUserAlert(GetUserAlertRequest request)
    {
      return Channel.GetUserAlert(request);
    }

    public UpdateUserAlertResponse UpdateUserAlert(UpdateUserAlertRequest request)
    {
      return Channel.UpdateUserAlert(request);
    }

    public ChangeConsentStatusResponse ChangeConsentStatus(ChangeConsentStatusRequest request)
    {
      return Channel.ChangeConsentStatus(request);
    }

    public ChangeMigratedStatusResponse ChangeMigratedStatus(ChangeMigratedStatusRequest request)
    {
      return Channel.ChangeMigratedStatus(request);
    }

    public ChangeProgramOfStudyResponse ChangeProgramOfStudy(ChangeProgramOfStudyRequest request)
    {
      return Channel.ChangeProgramOfStudy(request);
    }

    public ChangeEnrollmentStatusResponse ChangeEnrollmentStatus(ChangeEnrollmentStatusRequest request)
    {
      return Channel.ChangeEnrollmentStatus(request);
    }

    public ChangeCampusResponse ChangeCampus(ChangeCampusRequest request)
    {
      return Channel.ChangeCampus(request);
    }

    public ChangePhoneNumberResponse ChangePhoneNumber(ChangePhoneNumberRequest request)
    {
      return Channel.ChangePhoneNumber(request);
    }

    public RegisterCareerUserResponse UpdateUser(RegisterCareerUserRequest request)
    {
      return Channel.UpdateUser(request);
    }

    public ValidateEmailResponse ValidateEmail(ValidateEmailRequest request)
    {
      return Channel.ValidateEmail(request);
    }

    public SendRegistrationPinEmailResponse SendRegistrationPinEmail(SendRegistrationPinEmailRequest request)
    {
      return Channel.SendRegistrationPinEmail(request);
    }

    public UpdateManagerFlagResponse UpdateManagerFlag(UpdateManagerFlagRequest request)
    {
      return Channel.UpdateManagerFlag(request);
    }

    public UpdateLastSurveyedDateResponse UpdateLastSurveyedDate(UpdateLastSurveyedDateRequest request)
    {
      return Channel.UpdateLastSurveyedDate(request);
    }

    public ExportAssistUsersResponse ExportAssistUsers(ExportAssistUsersRequest request)
    {
      return Channel.ExportAssistUsers(request);
    }

    public ImportJobSeekerAndResumesResponse ImportJobSeekerAndResumes(ImportJobSeekerAndResumesRequest request)
    {
      return Channel.ImportJobSeekerAndResumes(request);
    }

    public ImportEmployerAndArtifactsResponse ImportEmployerAndArtifacts(ImportEmployerAndArtifactsRequest request)
    {
      return Channel.ImportEmployerAndArtifacts(request);
    }

    public UnsubscribeUserFromEmailsResponse UnsubscribeUserFromEmails(UnsubscribeUserFromEmailsRequest request)
    {
	    return Channel.UnsubscribeUserFromEmails(request);
    }

    public FindPersonMobileDeviceResponse FindPersonMobileDevice(FindPersonMobileDeviceRequest request)
    {
        return Channel.FindPersonMobileDevice(request);
    }

    public CreatePersonMobileDeviceResponse CreatePersonMobileDevice(CreatePersonMobileDeviceRequest request)
    {
        return Channel.CreatePersonMobileDevice(request);
    }

    public DeletePersonMobileDeviceResponse DeletePersonMobileDevice(DeletePersonMobileDeviceRequest request)
    {
        return Channel.DeletePersonMobileDevice(request);
    }
    
		/// <summary>
		/// Inactivates the job seeker.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		public InactivateJobSeekerResponse InactivateJobSeeker(InactivateJobSeekerRequest request)
		{
			return Channel.InactivateJobSeeker(request);
		}

    /// <summary>
    /// Updates the assist user.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    public UpdateAssistUserResponse UpdateAssistUser(UpdateAssistUserRequest request)
    {
      return Channel.UpdateAssistUser(request);
    }

	  public SecurityQuestionDetailsResponse GetUserSecurityQuestion(SecurityQuestionDetailsRequest request)
	  {
		  return Channel.GetUserSecurityQuestion(request);
	  }

		public SecurityQuestionDetailsResponse HasUserSecurityQuestions(SecurityQuestionDetailsRequest request)
		{
			return Channel.HasUserSecurityQuestions(request);
		}

		/// <returns></returns>
		public ChangeCareerAccountTypeResponse ChangeCareerAccountType(ChangeCareerAccountTypeRequest request)
		{
			return Channel.ChangeCareerAccountType(request);
		}
  }
}
