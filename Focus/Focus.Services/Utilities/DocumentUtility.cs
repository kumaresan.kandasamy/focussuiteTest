﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Data;
using System.IO;
using System.Drawing;
using System.Linq;
using Aspose.Cells;
using Aspose.Words;
using Aspose.Words.Fonts;
using Aspose.Words.Tables;
using Focus.Core;
using Cell = Aspose.Words.Tables.Cell;
using License = Aspose.Words.License;
using LoadFormat = Aspose.Cells.LoadFormat;
using LoadOptions = Aspose.Cells.LoadOptions;
using SaveFormat = Aspose.Words.SaveFormat;

#endregion

namespace Focus.Services.Utilities
{
  internal class DocumentUtility
  {

    private DocumentBuilder _builder;
    private SaveFormat DocumentType { get; set; }

    public string DefaultFont
    {
      get { return FontSettings.DefaultFontName; }
      set { FontSettings.DefaultFontName = value; }
    }

    /// <summary>
    /// Initializes a new instance of the <see cref="DocumentUtility"/> class and validates licence
    /// </summary>
    public DocumentUtility()
    {
      var l = new License();
      l.SetLicense("Aspose.Words.lic");
    }

    /// <summary>
    /// Creates a new document and sets the document type
    /// </summary>
    /// <param name="documentType">Type of document.</param>
    public void New(DocumentFileType documentType)
    {
      _builder = new DocumentBuilder();

      switch (documentType)
      {
        case DocumentFileType.Pdf:
          DocumentType = SaveFormat.Pdf;
          break;
        case DocumentFileType.Doc:
          DocumentType = SaveFormat.Doc;
          break;
        case DocumentFileType.Docx:
          DocumentType = SaveFormat.Docx;
          break;
      }
			_builder.PageSetup.LeftMargin = 30;
			_builder.PageSetup.RightMargin = 30;
			_builder.PageSetup.TopMargin = 30;
			_builder.PageSetup.BottomMargin = 30;
	    }

    /// <summary>
    /// Exports spreadsheet to a data table.
    /// </summary>
    /// <param name="documentType">Type of the document.</param>
    /// <param name="file">The file bytes.</param>
    /// <param name="addSpaceToBlankLine">Whether to add a space to a line of commas in CSV files to force line to be imported</param>
    /// <returns>
    /// Exported datatable
    /// </returns>
    public DataTable ExportToDataTable(DocumentFileType documentType, byte[] file, bool addSpaceToBlankLine = true)
    {
      var loadFormat = LoadFormat.Unknown;

      switch (documentType)
      {
        case DocumentFileType.Xls:
          loadFormat = LoadFormat.Excel97To2003;
          break;

        case DocumentFileType.Xlsx:
          loadFormat = LoadFormat.Xlsx;
          break;

        case DocumentFileType.Csv:
          loadFormat = LoadFormat.CSV;
          if (addSpaceToBlankLine)
          {
            var fileData = System.Text.Encoding.Default.GetString(file);
            var lines = fileData.Split(new[] { "\r\n" }, StringSplitOptions.None);

            // Check for lines consisting of only commas, and add a space before them to force Aspose.Cells to import them
            var blankLineIndexes = lines.Select((line, index) => index)
                                        .Where(index => lines[index].Length > 0 && lines[index].Replace(",", "").Length == 0)
                                        .ToList();
            blankLineIndexes.ForEach(index =>
            {
              lines[index] = string.Concat(" ", lines[index]);
            });
            fileData = string.Join("\r\n", lines);
            file = System.Text.Encoding.Default.GetBytes(fileData);
          }
          break;
      }

      var stream = new MemoryStream();
      stream.Write(file, 0, file.Length);

      var loadOptions = new LoadOptions(loadFormat);

      stream.Seek(0, SeekOrigin.Begin);
      var workbook = new Workbook(stream, loadOptions);

      //Accessing the first worksheet in the Excel file
      var worksheet = workbook.Worksheets[0];

      //Exporting the contents of all rows and columns starting from 1st cell to a DataTable
      var dataTable = worksheet.Cells.ExportDataTableAsString(0, 0, worksheet.Cells.MaxRow + 1, worksheet.Cells.MaxColumn + 1);

      //Closing the file stream to free all resources
      stream.Close();

      return dataTable;
    }

    #region Document formatting/ text insertion
    /// <summary>
    /// Inserts a carriage return into the current document
    /// </summary>
    /// <param name="count">The number of carriage returns to enter.</param>
    public void CarriageReturn(int count = 1)
    {
      var i = 0;
      while (count > i)
      {
        _builder.InsertBreak(BreakType.LineBreak);
        i++;
      }
    }

    /// <summary>
    /// Inserts a space into the document
    /// </summary>
    public void Space()
    {
      _builder.Write(" ");
    }

    /// <summary>
    /// Insert a new paragraph into the document
    /// </summary>
    public void NewParagraph()
    {
      _builder.InsertParagraph();
    }

    /// <summary>
    /// Appends text to the current document.
    /// </summary>
    /// <param name="text">The text.</param>
    public void AppendText(string text)
    {
      _builder.Write(text);
    }
    #endregion

    #region Font manipulation

    /// <summary>
    /// Sets the current font to be used
    /// </summary>
    /// <param name="fontType">Font name</param>
    public void Font(string fontType)
    {
      _builder.Font.Name = fontType;
    }

    /// <summary>
    /// Appends bold text to the document
    /// </summary>
    /// <param name="text">The text.</param>
    public void AppendBoldText(string text)
    {
      _builder.Font.Bold = true;
      _builder.Write(text);
      _builder.Font.Bold = false;
    }

    /// <summary>
    /// Appends italic text to the document.
    /// </summary>
    /// <param name="text">The text.</param>
    public void AppendItalicText(string text)
    {
      _builder.Font.Italic = true;
      _builder.Write(text);
      _builder.Font.Italic = false;
    }

    /// <summary>
    /// Starts a section of bold text
    /// </summary>
    public void BoldStart()
    {
      _builder.Font.Bold = true;
    }

    /// <summary>
    /// Ends a section of bold text
    /// </summary>
    public void BoldEnd()
    {
      _builder.Font.Bold = false;
    }

    /// <summary>
    /// Starts a section of italic text
    /// </summary>
    public void ItalicStart()
    {
      _builder.Font.Italic = true;
    }

    /// <summary>
    /// Ends a section of italic text
    /// </summary>
    public void ItalicEnd()
    {
      _builder.Font.Italic = false;
    }

    /// <summary>
    /// Starts a section of underlined text
    /// </summary>
    public void UnderlineStart()
    {
      _builder.Font.Underline = Underline.Single;
    }

    /// <summary>
    /// Ends a section of underlined text
    /// </summary>
    public void UnderlineEnd()
    {
      _builder.Font.Underline = Underline.None;
    }

    /// <summary>
    /// Sets the current font size
    /// </summary>
    public void FontSize(int size)
    {
      _builder.Font.Size = size;
    }

    #endregion

    #region Table methods
    /// <summary>
    /// Starts a new table
    /// </summary>
    public Table NewTable()
    {
      return _builder.StartTable();
		}

    /// <summary>
    /// Inserts a new cell into the current table
    /// </summary>
    public Cell NewCell()
    {
	    var cell = _builder.InsertCell();
			return cell;
    }

    /// <summary>
    /// Adds a new cell and text to the current table
    /// </summary>
    /// <param name="text">The text.</param>
    public Cell AddCellAndText(string text)
    {
      var cell = NewCell();
	    AppendText(text);
			
	    return cell;
    }
    /// <summary>
    /// Adds a new cell and bold text to the current table
    /// </summary>
    /// <param name="text">The text.</param>
    public Cell AddCellAndBoldText(string text)
    {
      var cell = NewCell();
      AppendBoldText(text);

	    return cell;
    }

		public Cell AddCellAndImage(Image image)
		{
			var cell = NewCell();
			_builder.InsertImage(image);

			return cell;
		}

    /// <summary>
    /// Starts a new row in the table
    /// </summary>
    public void NewRow()
    {
      _builder.EndRow();
    }

    /// <summary>
    /// Closes the table.
    /// </summary>
    public void EndTable()
    {
      _builder.EndTable();
    }

    /// <summary>
    /// Clears the table borders.
    /// </summary>
    public void ClearTableBorders()
    {
      var borders = _builder.CellFormat.Borders;
      borders.LineStyle = LineStyle.None;
      borders.LineWidth = 0;
    }
    #endregion
		
    #region Output methods

    /// <summary>
    /// Saves the document to a specified
    /// </summary>
    /// <param name="path">The path.</param>
    public void Save(string path)
    {
      _builder.Document.Save(path);
    }

    /// <summary>
    /// Converts the document to a byte array.
    /// </summary>
    /// <returns></returns>
    public byte[] ToByteArray()
    {
      using (var outStream = new MemoryStream())
      {
        _builder.Document.Save(outStream, DocumentType);
        return outStream.ToArray();
      }
    }

    #endregion
  }
}
