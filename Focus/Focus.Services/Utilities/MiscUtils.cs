﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Focus.Services.Utilities
{
    class MiscUtils
    {
        public static string SafeSubstring(string text, int start, int length)
        {
            if (text == null) return null;

            return text.Substring(start, Math.Min(length, text.Length));
        }
    }
}
