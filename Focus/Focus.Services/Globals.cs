﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.Threading;
using System.Web;

using Focus.Core.Messages;
using Focus.Core.Settings.Interfaces;
using Focus.Data.Repositories;
using Focus.Data.Repositories.Contracts;
using Focus.Services.Core;
using Focus.Services.Helpers;
using Focus.Services.Integration;
using Focus.Services.Messages;
using Focus.Services.Repositories;
using Framework.Core;
using Framework.ServiceLocation;

#endregion

namespace Focus.Services
{
	public class RuntimeContext : IRuntimeContext, IDisposable
	{
		private static bool _isDisposing;
		private readonly ServiceRuntimeEnvironment _environment;

		private const string CurrentRequestItemKey = "__RuntimeContext__ServiceRequest__";

		/// <summary>
		/// Initializes a new instance of the <see cref="RuntimeContext" /> class.
		/// </summary>
		/// <param name="environment">The environment.</param>
		public RuntimeContext(ServiceRuntimeEnvironment environment) : this(environment, null, null, null)
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="RuntimeContext" /> class.
		/// </summary>
		/// <param name="environment">The environment.</param>
		/// <param name="appSettings">The app settings.</param>
		/// <param name="repositories">The repositories.</param>
		/// <param name="helpers">The helpers.</param>
		public RuntimeContext(ServiceRuntimeEnvironment environment, IAppSettings appSettings, IRepositories repositories, IHelpers helpers)
		{
			_environment = environment;
			AppSettings = appSettings ?? (AppSettings = ServiceLocator.Current.Resolve<IAppSettings>());
			Repositories = repositories ?? (Repositories = ServiceLocator.Current.Resolve<IRepositories>());
			Helpers = helpers ?? (Helpers = ServiceLocator.Current.Resolve<IHelpers>());
		}

		/// <summary>
		/// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
		/// </summary>
		public void Dispose()
		{
			if (_isDisposing) return;

			_isDisposing = true;
		}

		public IAppSettings AppSettings { get; private set; }
		public IRepositories Repositories { get; private set; }
		public IHelpers Helpers { get; private set; }

		/// <summary>
		/// Gets the current request.
		/// </summary>
		public IServiceRequest CurrentRequest { get { return GetRequest(); } }

		/// <summary>
		/// Sets the request.
		/// </summary>
		/// <param name="serviceRequest">The service request.</param>
		public void SetRequest(IServiceRequest serviceRequest)
		{
			switch (_environment)
			{
				case ServiceRuntimeEnvironment.Web:
					if (HttpContext.Current != null)
						HttpContext.Current.Items[CurrentRequestItemKey] = serviceRequest;

					break;

				case ServiceRuntimeEnvironment.Wcf:
					if (OperationContext.Current != null)
					{
						var extension = OperationContext.Current.Extensions.Find<ServiceRequestExtension>();

						if (extension == null)
						{
							extension = new ServiceRequestExtension(serviceRequest);
							OperationContext.Current.Extensions.Add(extension);
							OperationContext.Current.OperationCompleted += WcfOperationCompleted;
						}
						else
							extension.ServiceRequest = serviceRequest;
					}

					break;
			}

			Thread.SetData(Thread.GetNamedDataSlot(CurrentRequestItemKey), serviceRequest);
		}

		/// <summary>
		/// Gets the request identifiers.
		/// </summary>
		/// <param name="sessionId">The session identifier.</param>
		/// <param name="requestId">The request identifier.</param>
		/// <param name="userId">The user identifier.</param>
		public void GetRequestIdentifiers(out Guid sessionId, out Guid requestId, out long userId)
		{
			requestId = sessionId = Guid.Empty;
			userId = 0;

			var currentRequest = CurrentRequest;

			if (currentRequest.IsNull())
				return;

			sessionId = currentRequest.SessionId;
			requestId = currentRequest.RequestId;

			if (currentRequest.UserContext.IsNull())
				return;

			userId = currentRequest.UserContext.UserId;
		}

		/// <summary>
		/// Gets the request.
		/// </summary>
		/// <returns></returns>
		private IServiceRequest GetRequest()
		{
			switch (_environment)
			{
				case ServiceRuntimeEnvironment.Web:
					if (HttpContext.Current != null)
						return (IServiceRequest)HttpContext.Current.Items[CurrentRequestItemKey];

					break;

				case ServiceRuntimeEnvironment.Wcf:
					if (OperationContext.Current != null)
					{
						var extension = OperationContext.Current.Extensions.Find<ServiceRequestExtension>();
						return (extension != null ? extension.ServiceRequest : null);
					}

					break;
			}

			return (IServiceRequest)Thread.GetData(Thread.GetNamedDataSlot(CurrentRequestItemKey));
		}

		/// <summary>
		/// WCFs the operation completed.
		/// </summary>
		/// <param name="sender">The sender.</param>
		/// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
		private void WcfOperationCompleted(object sender, EventArgs e)
		{
			// Might have to do something here
		}

		private class ServiceRequestExtension : IExtension<OperationContext>
		{
			/// <summary>
			/// Initializes a new instance of the <see cref="ServiceRequestExtension" /> class.
			/// </summary>
			/// <param name="serviceRequest">The service request.</param>
			/// <exception cref="System.ArgumentNullException">serviceRequest</exception>
			public ServiceRequestExtension(IServiceRequest serviceRequest)
			{
				if (serviceRequest == null)
					throw new ArgumentNullException("serviceRequest");

				ServiceRequest = serviceRequest;
			}

			/// <summary>
			/// Gets or sets the service request.
			/// </summary>
			/// <value>
			/// The service request.
			/// </value>
			public IServiceRequest ServiceRequest { get; set; }

			/// <summary>
			/// Attaches the specified owner.
			/// </summary>
			/// <param name="owner">The owner.</param>
			void IExtension<OperationContext>.Attach(OperationContext owner)
			{ }

			/// <summary>
			/// Detaches the specified owner.
			/// </summary>
			/// <param name="owner">The owner.</param>
			void IExtension<OperationContext>.Detach(OperationContext owner)
			{ }
		}
	}

	internal static class RuntimeContextExtensions
	{
		//const string ContextItemKey = "__RuntimeContext__Context__";

		/// <summary>
		/// Gets the context.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <returns></returns>
		internal static object GetContext(this IRuntimeContext runtimeContext)
		{
			if (HttpContext.Current != null)
				return HttpContext.Current;

			if (OperationContext.Current != null)
				return OperationContext.Current;

			return ThreadContext.GetContext(runtimeContext);
		}

		/// <summary>
		/// Resets the context.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <param name="context">The context.</param>
		internal static void SetContext(this IRuntimeContext runtimeContext, object context)
		{
			if (context is HttpContext)
				HttpContext.Current = (HttpContext)context;
			else if (context is OperationContext)
				OperationContext.Current = (OperationContext)context;
			else if (context is ThreadContext)
				ThreadContext.SetContext((ThreadContext)context, runtimeContext);
		}
	}

	internal class ServiceRepositories : IRepositories
	{
		private IConfigurationRepository _configurationRepository;
		private ILibraryRepository _libraryRepository;
		private ICoreRepository _coreRepository;
		private ILensRepository _lensRepository;
		private IDocumentRepository _documentRepository;
		private IReportRepository _reportRepository;
		private ILiveJobsRepository _liveJobsRepository;
		private IMigrationRepository _migrationRepository;
		private IIntegrationRepository _integrationRepository;
    private ILaborInsightRepository _laborInsightRepository;

		/// <summary>
		/// Initializes a new instance of the <see cref="ServiceRepositories" /> class.
		/// </summary>
		public ServiceRepositories()
		{ }

    /// <summary>
    /// Initializes a new instance of the <see cref="ServiceRepositories" /> class.
    /// </summary>
    /// <param name="configurationRepository">The configuration repository.</param>
    /// <param name="libraryRepository">The library repository.</param>
    /// <param name="coreRepository">The core repository.</param>
    /// <param name="lensRepository">The lens repository.</param>
    /// <param name="documentRepository">The document repository.</param>
    /// <param name="reportRepository">The report repository.</param>
    /// <param name="liveJobsRepository">The live jobs repository.</param>
    /// <param name="migrationRepository">The migration repository</param>
    /// <param name="integrationRepository">The integration repository.</param>
    /// <param name="laborInsightRepository">Labor/Insight repository</param>
    public ServiceRepositories(IConfigurationRepository configurationRepository = null, ILibraryRepository libraryRepository = null, ICoreRepository coreRepository = null,
                                ILensRepository lensRepository = null, IDocumentRepository documentRepository = null,
                               IReportRepository reportRepository = null, ILiveJobsRepository liveJobsRepository = null, IMigrationRepository migrationRepository = null, IIntegrationRepository integrationRepository = null, ILaborInsightRepository laborInsightRepository = null)
    {
      _configurationRepository = configurationRepository;
      _libraryRepository = libraryRepository;
      _coreRepository = coreRepository;
      _lensRepository = lensRepository;
      _documentRepository = documentRepository;
      _reportRepository = reportRepository;
      _liveJobsRepository = liveJobsRepository;
      _migrationRepository = migrationRepository;
      _integrationRepository = integrationRepository;
      _laborInsightRepository = laborInsightRepository;
    }

		public IConfigurationRepository Configuration { get { return _configurationRepository ?? (_configurationRepository = ServiceLocator.Current.Resolve<IConfigurationRepository>()); } }
		public ILibraryRepository Library { get { return _libraryRepository ?? (_libraryRepository = ServiceLocator.Current.Resolve<ILibraryRepository>()); } }

		public ICoreRepository Core
		{
			get
			{
				if (_coreRepository == null)
				{
					_coreRepository = ServiceLocator.Current.Resolve<ICoreRepository>();
					_coreRepository.EntityStateChanged += Repository_EntityStateChanged;
				}
				return _coreRepository;
			}
		}

		private void Repository_EntityStateChanged(object sender, RepositoryEntityStateChangedEventArgs eventArgs)
		{
			var runtimeContext = ServiceLocator.Current.Resolve<IRuntimeContext>();

			runtimeContext.Helpers.Messaging.Publish(new UpdateReportingMessage
			{
				EntityIds = new List<Tuple<long, long?>> { Tuple.Create(eventArgs.EntityId, eventArgs.AdditionalEntityId) },
				EntityType = eventArgs.EntityType,
				EntityDeletion = eventArgs.EntityDeletion,
				ActionerId = runtimeContext.CurrentRequest.UserContext.ActionerId,
				Culture = runtimeContext.CurrentRequest.UserContext.Culture
			});
		}

		public ILensRepository Lens { get { return _lensRepository ?? (_lensRepository = ServiceLocator.Current.Resolve<ILensRepository>()); } }
		public IDocumentRepository Document { get { return _documentRepository ?? (_documentRepository = ServiceLocator.Current.Resolve<IDocumentRepository>()); } }
		public IReportRepository Report { get { return _reportRepository ?? (_reportRepository = ServiceLocator.Current.Resolve<IReportRepository>()); } }
		public ILiveJobsRepository LiveJobs { get { return _liveJobsRepository ?? (_liveJobsRepository = ServiceLocator.Current.Resolve<ILiveJobsRepository>()); } }
		public IMigrationRepository Migration { get { return _migrationRepository ?? (_migrationRepository = ServiceLocator.Current.Resolve<IMigrationRepository>()); } }
		public IIntegrationRepository Integration { get { return _integrationRepository ?? (_integrationRepository = ServiceLocator.Current.Resolve<IIntegrationRepository>()); } }
    public ILaborInsightRepository LaborInsight { get { return _laborInsightRepository ?? (_laborInsightRepository = ServiceLocator.Current.Resolve<ILaborInsightRepository>()); } }
	}

	internal class ServiceHelpers : IHelpers
	{
		private IConfigurationHelper _configurationHelper;
		private ILookupHelper _lookupHelper;
		private IOccupationHelper _occupationHelper;
		private IOrganizationHelper _organizationHelper;
		private IResumeHelper _resumeHelper;
		private ISessionHelper _sessionHelper;
		private IMessagingHelper _messagingHelper;
		private IPostingHelper _postingHelper;
		private IJobDevelopmentHelper _jobDevelopmentHelper;
		private ILoggingHelper _loggingHelper;
		private ILocalisationHelper _localisationHelper;
		private IAlertsHelper _alertsHelper;
		private IEmailHelper _emailHelper;
		private ISearchHelper _searchHelper;
		private IOfficeHelper _officeHelper;
		private ICandidateHelper _candidateHelper;
		private IEmployerHelper _employerHelper;
		private IEmployeeHelper _employeeHelper;
		private ISelfServiceHelper _selfServiceHelper;
		private IValidationHelper _validationHelper;
		private IEncryptionHelper _encryptionHelper;
		private IPushNotificationHelper _pushNotificationHelper;
		private IUserHelper _userHelper;

		/// <summary>
		/// Initializes a new instance of the <see cref="ServiceHelpers" /> class.
		/// </summary>
		public ServiceHelpers()
		{ }

		/// <summary>
		/// Initializes a new instance of the <see cref="ServiceHelpers" /> class.
		/// </summary>
		/// <param name="configurationHelper">The configuration helper.</param>
		/// <param name="lookupHelper">The lookup helper.</param>
		/// <param name="occupationHelper">The occupation helper.</param>
		/// <param name="organizationHelper">The organization helper.</param>
		/// <param name="resumeHelper">The resume helper.</param>
		/// <param name="sessionHelper">The session helper.</param>
		/// <param name="messagingHelper">The messaging helper.</param>
		/// <param name="postingHelper">The posting helper.</param>
		/// <param name="jobDevelopmentHelper">The job development helper.</param>
		/// <param name="loggingHelper">The logging helper.</param>
		/// <param name="localisationHelper">The localisation helper.</param>
		/// <param name="alertsHelper">The alerts helper.</param>
		/// <param name="emailHelper">The email helper.</param>
		/// <param name="searchHelper">The search helper.</param>
		/// <param name="officeHelper">The office helper.</param>
		/// <param name="candidateHelper">The candidate helper.</param>
		/// <param name="employerHelper">The employer helper.</param>
		/// <param name="employeeHelper">The employee helper.</param>
		/// <param name="selfServiceHelper">The self service helper.</param>
		/// <param name="validationHelper">The validation helper.</param>
		/// <param name="encryptionHelper">The encryption helper.</param>
		/// <param name="pushNotificationHelper">The push notification helper.</param>
		/// <param name="userHelper">The user helper</param>
		public ServiceHelpers(IConfigurationHelper configurationHelper = null, ILookupHelper lookupHelper = null, IOccupationHelper occupationHelper = null, IOrganizationHelper organizationHelper = null,
			IResumeHelper resumeHelper = null, ISessionHelper sessionHelper = null, IMessagingHelper messagingHelper = null, IPostingHelper postingHelper = null,
			IJobDevelopmentHelper jobDevelopmentHelper = null, ILoggingHelper loggingHelper = null, ILocalisationHelper localisationHelper = null, IAlertsHelper alertsHelper = null,
			IEmailHelper emailHelper = null, ISearchHelper searchHelper = null, IOfficeHelper officeHelper = null, ICandidateHelper candidateHelper = null, IEmployerHelper employerHelper = null,
			IEmployeeHelper employeeHelper = null, ISelfServiceHelper selfServiceHelper = null, IValidationHelper validationHelper = null, IEncryptionHelper encryptionHelper = null,
			IPushNotificationHelper pushNotificationHelper = null, IUserHelper userHelper = null)
		{
			_configurationHelper = configurationHelper;
			_lookupHelper = lookupHelper;
			_occupationHelper = occupationHelper;
			_organizationHelper = organizationHelper;
			_resumeHelper = resumeHelper;
			_sessionHelper = sessionHelper;
			_messagingHelper = messagingHelper;
			_postingHelper = postingHelper;
			_jobDevelopmentHelper = jobDevelopmentHelper;
			_loggingHelper = loggingHelper;
			_localisationHelper = localisationHelper;
			_alertsHelper = alertsHelper;
			_emailHelper = emailHelper;
			_searchHelper = searchHelper;
			_officeHelper = officeHelper;
			_candidateHelper = candidateHelper;
			_employerHelper = employerHelper;
			_employeeHelper = employeeHelper;
			_selfServiceHelper = selfServiceHelper;
			_validationHelper = validationHelper;
			_encryptionHelper = encryptionHelper;
			_pushNotificationHelper = pushNotificationHelper;
			_userHelper = userHelper;
		}

		public IConfigurationHelper Configuration { get { return _configurationHelper ?? (_configurationHelper = ServiceLocator.Current.Resolve<IConfigurationHelper>()); } }
		public ILookupHelper Lookup { get { return _lookupHelper ?? (_lookupHelper = ServiceLocator.Current.Resolve<ILookupHelper>()); } }
		public IOccupationHelper Occupation { get { return _occupationHelper ?? (_occupationHelper = ServiceLocator.Current.Resolve<IOccupationHelper>()); } }
		public IOrganizationHelper Organization { get { return _organizationHelper ?? (_organizationHelper = ServiceLocator.Current.Resolve<IOrganizationHelper>()); } }
		public IResumeHelper Resume { get { return _resumeHelper ?? (_resumeHelper = ServiceLocator.Current.Resolve<IResumeHelper>()); } }
		public ISessionHelper Session { get { return _sessionHelper ?? (_sessionHelper = ServiceLocator.Current.Resolve<ISessionHelper>()); } }
		public IMessagingHelper Messaging { get { return _messagingHelper ?? (_messagingHelper = ServiceLocator.Current.Resolve<IMessagingHelper>()); } }
		public IPostingHelper Posting { get { return _postingHelper ?? (_postingHelper = ServiceLocator.Current.Resolve<IPostingHelper>()); } }
		public IJobDevelopmentHelper JobDevelopment { get { return _jobDevelopmentHelper ?? (_jobDevelopmentHelper = ServiceLocator.Current.Resolve<IJobDevelopmentHelper>()); } }
		public ILoggingHelper Logging { get { return _loggingHelper ?? (_loggingHelper = ServiceLocator.Current.Resolve<ILoggingHelper>()); } }
		public ILocalisationHelper Localisation { get { return _localisationHelper ?? (_localisationHelper = ServiceLocator.Current.Resolve<ILocalisationHelper>()); } }
		public IAlertsHelper Alerts { get { return _alertsHelper ?? (_alertsHelper = ServiceLocator.Current.Resolve<IAlertsHelper>()); } }
		public IEmailHelper Email { get { return _emailHelper ?? (_emailHelper = ServiceLocator.Current.Resolve<IEmailHelper>()); } }
		public ISearchHelper Search { get { return _searchHelper ?? (_searchHelper = ServiceLocator.Current.Resolve<ISearchHelper>()); } }
		public IOfficeHelper Office { get { return _officeHelper ?? (_officeHelper = ServiceLocator.Current.Resolve<IOfficeHelper>()); } }
		public ICandidateHelper Candidate { get { return _candidateHelper ?? (_candidateHelper = ServiceLocator.Current.Resolve<ICandidateHelper>()); } }
		public IEmployerHelper Employer { get { return _employerHelper ?? (_employerHelper = ServiceLocator.Current.Resolve<IEmployerHelper>()); } }
        public IEmployeeHelper Employee { get { return _employeeHelper ?? (_employeeHelper = ServiceLocator.Current.Resolve<IEmployeeHelper>()); } }
        public ISelfServiceHelper SelfService { get { return _selfServiceHelper ?? (_selfServiceHelper = ServiceLocator.Current.Resolve<ISelfServiceHelper>()); } }
        public IValidationHelper Validation { get { return _validationHelper ?? (_validationHelper = ServiceLocator.Current.Resolve<IValidationHelper>()); } }
		public IEncryptionHelper Encryption { get { return _encryptionHelper ?? (_encryptionHelper = ServiceLocator.Current.Resolve<IEncryptionHelper>()); } }
        public IPushNotificationHelper PushNotification { get { return _pushNotificationHelper ?? (_pushNotificationHelper = ServiceLocator.Current.Resolve<IPushNotificationHelper>()); } }
		public IUserHelper User { get { return _userHelper ?? (_userHelper = ServiceLocator.Current.Resolve<IUserHelper>()); } }
	}

	public interface IRuntimeContext
	{
		IAppSettings AppSettings { get; }
		IRepositories Repositories { get; }
		IHelpers Helpers { get; }

		IServiceRequest CurrentRequest { get; }
		void SetRequest(IServiceRequest serviceRequest);
		void GetRequestIdentifiers(out Guid sessionId, out Guid requestId, out long userId);
	}

	public interface IRepositories
	{
		IConfigurationRepository Configuration { get; }
		ILibraryRepository Library { get; }
		ICoreRepository Core { get; }
		ILensRepository Lens { get; }
		IDocumentRepository Document { get; }
		IReportRepository Report { get; }
		ILiveJobsRepository LiveJobs { get; }
		IMigrationRepository Migration { get; }
		IIntegrationRepository Integration { get; }
    ILaborInsightRepository LaborInsight { get; }
	}

	public interface IHelpers
	{
		IConfigurationHelper Configuration { get; }
		ILookupHelper Lookup { get; }
		IOccupationHelper Occupation { get; }
		IOrganizationHelper Organization { get; }
		IResumeHelper Resume { get; }
		ISessionHelper Session { get; }
		IMessagingHelper Messaging { get; }
		IPostingHelper Posting { get; }
		IJobDevelopmentHelper JobDevelopment { get; }
		ILoggingHelper Logging { get; }
		ILocalisationHelper Localisation { get; }
		IAlertsHelper Alerts { get; }
		IEmailHelper Email { get; }
		ISearchHelper Search { get; }
		IOfficeHelper Office { get; }
		ICandidateHelper Candidate { get; }
		IEmployerHelper Employer { get; }
		IEmployeeHelper Employee { get; }
		ISelfServiceHelper SelfService { get; }
		IValidationHelper Validation { get; }
		IEncryptionHelper Encryption { get; }
		IPushNotificationHelper PushNotification { get; }
		IUserHelper User { get; }
	}

	internal class ThreadContext
	{
		internal IServiceRequest CurrentRequest { get; set; }

		internal static void SetContext(ThreadContext context, IRuntimeContext runtimeContext)
		{
			runtimeContext.SetRequest(context.CurrentRequest);
		}

		internal static ThreadContext GetContext(IRuntimeContext runtimeContext)
		{
			return new ThreadContext { CurrentRequest = runtimeContext.CurrentRequest };
		}
	}
}
