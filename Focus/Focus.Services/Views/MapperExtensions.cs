﻿using Focus.Core.Views;
using Focus.Data.Configuration.Entities;

namespace Focus.Services.Views
{
	public static class MapperExtensions
	{
		#region ConfigurationItem

		public static ConfigurationItemView AsView(this ConfigurationItem entity)
		{
			var view = new ConfigurationItemView
			{ 
				Key = entity.Key,
				Value = entity.Value
			};
			return view;
		}

		/// <summary>
		/// Copies ConfigurationDto to Entity
		/// </summary>
		/// <param name="view">The view.</param>
		/// <param name="entity">The entity.</param>
		/// <returns></returns>
		public static ConfigurationItem CopyTo(this ConfigurationItemView view, ConfigurationItem entity)
		{
			entity.Key = view.Key;
			entity.Value = view.Value;

			return entity;
		}

		/// <summary>
		/// Copies ConfigurationDto to Entity
		/// </summary>
		/// <param name="view">The view.</param>
		/// <returns></returns>
		public static ConfigurationItem CopyTo(this ConfigurationItemView view)
		{
			var entity = new ConfigurationItem();
			return view.CopyTo(entity);
		}

		#endregion
	}
}
