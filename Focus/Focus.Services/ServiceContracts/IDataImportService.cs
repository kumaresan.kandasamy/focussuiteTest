﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.ServiceModel;

using Focus.Core;
using Focus.Core.Messages.DataImportService;

#endregion

namespace Focus.Services.ServiceContracts
{
  [ServiceContract(Namespace = Constants.ServiceContractNamespace)]
  public interface IDataImportService
  {
    #region Employer

    /// <summary>
    /// Sends an employer request from the migration database to the focus database.
    /// </summary>
    /// <param name="request">The import request object.</param>
    [OperationContract]
    EmployerImportResponse ProcessEmployer(EmployerImportRequest request);

    /// <summary>
    /// Sends an business unit description request from the migration database to the focus database.
    /// </summary>
    /// <param name="request">The import request object.</param>
    /// <param name="resetRepository">Whether to reset the migration repository</param>
    /// <param name="employeeId">The employee identifier.</param>
    /// <returns>The response</returns>
    [OperationContract]
    EmployerImportResponse ProcessBusinessUnitDescription(EmployerImportRequest request, bool resetRepository = true, long employeeId = 0);

    /// <summary>
    /// Sends an business unit logo request from the migration database to the focus database.
    /// </summary>
    /// <param name="request">The import request object.</param>
    /// <param name="resetRepository">Whether to reset the migration repository</param>
    /// <param name="employeeId">The employee identifier.</param>
    /// <returns>The response</returns>
    [OperationContract]
    EmployerImportResponse ProcessBusinessUnitLogo(EmployerImportRequest request, bool resetRepository = true, long employeeId = 0);

    /// <summary>
    /// Sends an resume search request from the migration database to the focus database.
    /// </summary>
    /// <param name="request">The import request object.</param>
    /// <param name="recordType">Type of the record.</param>
    /// <param name="resetRepository">Whether to reset the migration repository</param>
    /// <returns>
    /// The response
    /// </returns>
    [OperationContract]
    EmployerImportResponse ProcessResumeSearch(EmployerImportRequest request, MigrationEmployerRecordType recordType, bool resetRepository = true);

		/// <summary>
		/// Sends an employer trade name request from the migration database to the focus database.
		/// </summary>
		/// <param name="request">The import request object.</param>
		/// <param name="recordType">Type of the record.</param>
		/// <param name="resetRepository">Whether to reset the migration repository</param>
		/// <returns>
		/// The response
		/// </returns>
		[OperationContract]
		EmployerImportResponse ProcessEmployerTradeNames(EmployerImportRequest request, bool resetRepository = true, long employeeId = 0);

    #endregion

    #region Job Seeker

    /// <summary>
    /// Sends a job seeker request from the migration database to the focus database.
    /// </summary>
    /// <param name="request">The import request object.</param>
    [OperationContract]
    JobSeekerImportResponse ProcessJobSeeker(JobSeekerImportRequest request);
    
    /// <summary>
    /// Sends a job seeker resume request from the migration database to the focus database.
    /// </summary>
    /// <param name="request">The import request object.</param>
    [OperationContract]
    JobSeekerImportResponse ProcessJobSeekerResume(JobSeekerImportRequest request);

    /// <summary>
    /// Sends a job seeker resume document request from the migration database to the focus database.
    /// </summary>
    /// <param name="request">The import request object.</param>
    /// <param name="resetRepository">Whether to reset the migration repository</param>
    /// <returns>The response</returns>
    [OperationContract]
    JobSeekerImportResponse ProcessJobSeekerResumeDocument(JobSeekerImportRequest request, bool resetRepository = true);

    /// <summary>
    /// Sends a viewed posting request from the migration database to the focus database.
    /// </summary>
    /// <param name="request">The import request object.</param>
    [OperationContract]
    JobSeekerImportResponse ProcessJobSeekerViewedPosting(JobSeekerImportRequest request);

    /// <summary>
    /// Sends a saved search request from the migration database to the focus database.
    /// </summary>
    /// <param name="request">The import request object.</param>
    /// <param name="recordType">Type of the record.</param>
    /// <returns></returns>
    [OperationContract]
    JobSeekerImportResponse ProcessJobSeekerSavedSearch(JobSeekerImportRequest request, MigrationJobSeekerRecordType recordType);

    /// <summary>
    /// Sends a referral request from the migration database to the focus database.
    /// </summary>
    /// <param name="request">The import request object.</param>
    /// <param name="recordType">Type of the record.</param>
    [OperationContract]
    JobSeekerImportResponse ProcessReferral(JobSeekerImportRequest request, MigrationJobSeekerRecordType recordType);

    /// <summary>
    /// Sends an activity assignment from the migration database to the focus database.
    /// </summary>
    /// <param name="request">The import request object.</param>
    [OperationContract]
    JobSeekerImportResponse ProcessJobSeekerActivity(JobSeekerImportRequest request);

    /// <summary>
    /// Sends an toggle job seeker flag request from the migration database to the focus database.
    /// </summary>
    /// <param name="request">The import request object.</param>
    [OperationContract]
    JobSeekerImportResponse ProcessFlagJobSeeker(JobSeekerImportRequest request);

    #endregion

    #region Job Order

    /// <summary>
    /// Sends a job order request from the migration database to the focus database.
    /// </summary>
    /// <param name="request">The import request object.</param>
    [OperationContract]
    JobOrderImportResponse ProcessJobOrder(JobOrderImportRequest request);

    /// <summary>
    /// Sends a job location request from the migration database to the focus database.
    /// </summary>
    /// <param name="request">The import request object.</param>
    /// <param name="resetRepository">Whether to reset the migration repository</param>
    /// <returns>The response</returns>
    [OperationContract]
    JobOrderImportResponse ProcessJobLocation(JobOrderImportRequest request, bool resetRepository = true);

    /// <summary>
    /// Sends a job address request from the migration database to the focus database.
    /// </summary>
    /// <param name="request">The import request object.</param>
    /// <param name="resetRepository">Whether to reset the migration repository</param>
    /// <returns>The response</returns>
    [OperationContract]
    JobOrderImportResponse ProcessJobAddress(JobOrderImportRequest request, bool resetRepository = true);

    /// <summary>
    /// Sends a job certificate request from the migration database to the focus database.
    /// </summary>
    /// <param name="request">The import request object.</param>
    /// <param name="resetRepository">Whether to reset the migration repository</param>
    /// <returns>The response</returns>
    [OperationContract]
    JobOrderImportResponse ProcessJobCertificate(JobOrderImportRequest request, bool resetRepository = true);

    /// <summary>
    /// Sends a job licence request from the migration database to the focus database.
    /// </summary>
    /// <param name="request">The import request object.</param>
    /// <param name="resetRepository">Whether to reset the migration repository</param>
    /// <returns>The response</returns>
    [OperationContract]
    JobOrderImportResponse ProcessJobLicence(JobOrderImportRequest request, bool resetRepository = true);

    /// <summary>
    /// Sends a job licence request from the migration database to the focus database.
    /// </summary>
    /// <param name="request">The import request object.</param>
    /// <param name="resetRepository">Whether to reset the migration repository</param>
    /// <returns>The response</returns>
    [OperationContract]
    JobOrderImportResponse ProcessJobLanguage(JobOrderImportRequest request, bool resetRepository = true);

    /// <summary>
    /// Sends a job special requirement request from the migration database to the focus database.
    /// </summary>
    /// <param name="request">The import request object.</param>
    /// <param name="resetRepository">Whether to reset the migration repository</param>
    /// <returns>The response</returns>
    [OperationContract]
    JobOrderImportResponse ProcessJobSpecialRequirement(JobOrderImportRequest request, bool resetRepository = true);

    /// <summary>
    /// Sends a job program of study request from the migration database to the focus database.
    /// </summary>
    /// <param name="request">The import request object.</param>
    /// <param name="resetRepository">Whether to reset the migration repository</param>
    /// <returns>The response</returns>
    [OperationContract]
    JobOrderImportResponse ProcessJobProgramOfStudy(JobOrderImportRequest request, bool resetRepository = true);

    /// <summary>
    /// Updates a job so that the Posting field is generated.
    /// </summary>
    /// <param name="request">The request object for the import service.</param>
    [OperationContract]
    JobOrderImportResponse GenerateJobPosting(JobOrderImportRequest request);

		/// <summary>
		/// Processes a spidered job request from migration tables to focus tables.
		/// </summary>
		/// <param name="request">The request object for the import service.</param>
		[OperationContract]
		JobOrderImportResponse ProcessSpideredJob(JobOrderImportRequest jobOrderImportRequest);

		/// <summary>
		/// Processes a spidered job referral request from migration tables to focus tables.
		/// </summary>
		/// <param name="request">The request object for the import service.</param>
		[OperationContract]
		JobOrderImportResponse ProcessSpideredJobReferral(JobOrderImportRequest jobOrderImportRequest);

		/// <summary>
		/// Processes a saved job request from migration tables to focus tables.
		/// </summary>
		/// <param name="request">The request object for the import service.</param>
		[OperationContract]
		JobOrderImportResponse ProcessSavedJob(JobOrderImportRequest jobOrderImportRequest);

    #endregion

    #region Assist

    /// <summary>
    /// Sends a Create Assist User request from the migration database to the focus database
    /// </summary>
    /// <param name="request">The request object for the import service.</param>
    /// <returns></returns>
    [OperationContract]
    AssistImportResponse ProcessCreateAssistUser(AssistImportRequest request);

    /// <summary>
    /// Sends a note/reminder request from the migration database to the focus database
    /// </summary>
    /// <param name="request">The request object for the import service.</param>
    /// <returns></returns>
    [OperationContract]
    AssistImportResponse ProcessNoteReminder(AssistImportRequest request);

    /// <summary>
    /// Sends a Save Office request from the migration database to the focus database
    /// </summary>
    /// <param name="request">The request object for the import service.</param>
    /// <returns></returns>
    [OperationContract]
    AssistImportResponse ProcessSaveOffice(AssistImportRequest request);

    /// <summary>
    /// Sends a Person Office Mapping request from the migration database to the focus database
    /// </summary>
    /// <param name="request">The request object for the import service.</param>
    /// <returns></returns>
    [OperationContract]
    AssistImportResponse ProcessOfficeMappings(AssistImportRequest request);

    #endregion

	}
}
