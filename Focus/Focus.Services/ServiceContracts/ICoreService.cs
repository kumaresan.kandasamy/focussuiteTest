﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.ServiceModel;
using Focus.Core;
using Focus.Core.Messages.CandidateService;
using Focus.Core.Messages.CoreService;
using Focus.Core.Messages.JobService;

#endregion

namespace Focus.Services.ServiceContracts
{
    [ServiceContract(Namespace = Constants.ServiceContractNamespace)]
    public interface ICoreService
    {
        /// <summary>
        /// Gets the version info.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OperationContract]
        GetVersionInfoResponse GetVersionInfo(GetVersionInfoRequest request);

        /// <summary>
        /// Gets external only configurations.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>A response containing all configuration items</returns>
        [OperationContract]
        GetConfigurationResponse GetExternalConfigurationItems(GetConfigurationRequest request);

        /// <summary>
        /// Gets all configuration.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        GetConfigurationResponse GetConfigurationItems(GetConfigurationRequest request);

        /// <summary>
        /// Saves only external configuration items.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OperationContract]
        SaveConfigurationItemsResponse SaveExternalConfigurationItems(SaveConfigurationItemsRequest request);

        /// <summary>
        /// Saves the configuration items.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        SaveConfigurationItemsResponse SaveConfigurationItems(SaveConfigurationItemsRequest request);

        /// <summary>
        /// Gets a localisation dictionary.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OperationContract]
        GetLocalisationDictionaryResponse GetLocalisationDictionary(GetLocalisationDictionaryRequest request);

        /// <summary>
        /// Updates a localisation item.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The save response</returns>
        SaveLocalisationItemsResponse SaveLocalisationItems(SaveLocalisationItemsRequest request);

        /// <summary>
        /// Saves the default localisation items.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OperationContract]
        SaveDefaultLocalisationItemsResponse SaveDefaultLocalisationItems(SaveDefaultLocalisationItemsRequest request);

        /// <summary>
        /// Gets a lookup.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OperationContract]
        GetLookupResponse GetLookup(GetLookupRequest request);

        /// <summary>
        /// Gets the generic job titles.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OperationContract]
        GetGenericJobTitlesResponse GetGenericJobTitles(GetGenericJobTitlesRequest request);

        /// <summary>
        /// Gets the naics.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OperationContract]
        GetNaicsResponse GetNaics(GetNaicsRequest request);

        /// <summary>
        /// Gets the state and county for a postal code.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OperationContract]
        GetStateCityAndCountyForPostalCodeResponse GetStateCityAndCountyForPostalCode(GetStateCityAndCountyForPostalCodeRequest request);

        /// <summary>
        /// Establishes the location of a postal code or city and state.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OperationContract]
        EstablishLocationResponse EstablishLocation(EstablishLocationRequest request);

        /// <summary>
        /// Gets a language.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OperationContract]
        GetLanguageResponse GetLanguage(GetLanguageRequest request);

        /// <summary>
        /// Gets a certificate licenses.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OperationContract]
        GetCertificateLicenseResponse GetCertificateLicenses(GetCertificateLicenseRequest request);

        /// <summary>
        /// Gets an email template populated for preview.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OperationContract]
        EmailTemplateResponse GetEmailTemplatePreview(EmailTemplateRequest request);

        /// <summary>
        /// Gets the email template.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OperationContract]
        EmailTemplateResponse GetEmailTemplate(EmailTemplateRequest request);

        /// <summary>
        /// Gets the loc.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OperationContract]
        GetLocalisationDictionaryResponse GetLocalisationItem(GetLocalisationDictionaryRequest request);


        /// <summary>
        /// Saves the email template.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OperationContract]
        SaveEmailTemplateResponse SaveEmailTemplate(SaveEmailTemplateRequest request);

        /// <summary>
        /// Gets emails sent regarding referrals.
        /// </summary>
        /// <param name="request">The request containing the relevant ids.</param>
        /// <returns>A list of emails sent</returns>
        [OperationContract]
        GetReferralEmailResponse GetReferralEmails(GetReferralEmailRequest request);

        /// <summary>
        /// Saves an email sent on referral
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OperationContract]
        SaveReferralEmailResponse SaveReferralEmail(SaveReferralEmailRequest request);

        /// <summary>
        /// Profiles an event.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OperationContract]
        ProfilerEventResponse ProfileEvent(ProfilerEventRequest request);

        /// <summary>
        /// Gets a message.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OperationContract]
        GetMessageResponse GetMessage(GetMessageRequest request);

        /// <summary>
        /// Gets recent messages.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OperationContract]
        GetMessageResponse GetRecentMessages(GetMessageRequest request);

        /// <summary>
        /// Expires a message.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OperationContract]
        DismissMessageResponse ExpireMessage(DismissMessageRequest request);

        /// <summary>
        /// Dismisses a message.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OperationContract]
        DismissMessageResponse DismissMessage(DismissMessageRequest request);

        /// <summary>
        /// Creates the homepage alert.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OperationContract]
        CreateHomepageAlertResponse CreateHomepageAlert(CreateHomepageAlertRequest request);

        /// <summary>
        /// Gets the non default localisations.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OperationContract]
        GetLocalisationResponse GetLocalisation(GetLocalisationRequest request);

        /// <summary>
        /// Saves the message text.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OperationContract]
        SaveMessageTextsResponse SaveMessageTexts(SaveMessageTextsRequest request);

        /// <summary>
        /// Gets the saved message.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OperationContract]
        GetSavedMessageResponse GetSavedMessage(GetSavedMessageRequest request);

        /// <summary>
        /// Gets the saved message texts.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OperationContract]
        GetSavedMessageTextsResponse GetSavedMessageTexts(GetSavedMessageTextsRequest request);

        /// <summary>
        /// Deletes the saved message.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OperationContract]
        DeleteSavedMessageResponse DeleteSavedMessage(DeleteSavedMessageRequest request);

        /// <summary>
        /// Gets the application image.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OperationContract]
        ApplicationImageResponse GetApplicationImage(ApplicationImageRequest request);

        /// <summary>
        /// Saves the application image.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OperationContract]
        SaveApplicationImageResponse SaveApplicationImage(SaveApplicationImageRequest request);

        /// <summary>
        /// Sends the feedback.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OperationContract]
        FeedbackResponse SendFeedback(FeedbackRequest request);

        /// <summary>
        /// Gets the note reminder view.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OperationContract]
        NoteReminderViewResponse GetNoteReminder(NoteReminderViewRequest request);

        /// <summary>
        /// Saves the note reminder.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OperationContract]
        SaveNoteReminderResponse SaveNoteReminder(SaveNoteReminderRequest request);

        /// <summary>
        /// Sends the reminder.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OperationContract]
        SendReminderResponse SendReminder(SendReminderRequest request);

        /// <summary>
        /// Sends the hiring from tax credit program notification reminder.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OperationContract]
        EmailHiringFromTaxCreditProgramNotificationResponse SendHiringFromTaxCreditProgramNotification(EmailHiringFromTaxCreditProgramNotificationRequest request);

        /// <summary>
        /// Gets the industry classification.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OperationContract]
        IndustryClassificationResponse GetIndustryClassification(IndustryClassificationRequest request);

        /// <summary>
        /// Gets the activity categories.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OperationContract]
        GetActivityCategoriesResponse GetActivityCategories(GetActivityCategoriesRequest request);

        /// <summary>
        /// Saves an activity category.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response (containing the save category DTO)</returns>
        [OperationContract]
        SaveActivityCategoryResponse SaveActivityCategory(SaveActivityCategoryRequest request);

        /// <summary>
        /// Deletes an activity category.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        [OperationContract]
        DeleteActivityCategoryResponse DeleteActivityCategory(DeleteActivityCategoryRequest request);

        /// <summary>
        /// Gets the activities.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OperationContract]
        GetActivitiesResponse GetActivities(GetActivitiesRequest request);

        /// <summary>
        /// Gets the action event.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OperationContract]
        GetActionEventResponse GetActionEvent(GetActionEventRequest request);

        /// <summary>
        /// Checks the last action event.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OperationContract]
        CheckLastActionResponse CheckLastAction(CheckLastActionRequest request);


        /// <summary>
        /// Backdates the action event
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OperationContract]
        BackdateActionEventResponse BackdateActionEvent(BackdateActionEventRequest request);

        /// <summary>
        /// Deletes the action event
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OperationContract]
        DeleteActionEventResponse DeleteActionEvent(DeleteActionEventRequest request);

        /// <summary>
        /// Gets the education internship categories.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OperationContract]
        GetEducationInternshipCategoriesResponse GetEducationInternshipCategories(GetEducationInternshipCategoriesRequest request);

        /// <summary>
        /// Gets the education internship statements.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OperationContract]
        GetEducationInternshipStatementsResponse GetEducationInternshipStatements(GetEducationInternshipStatementsRequest request);

        /// <summary>
        /// Saves the activities.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OperationContract]
        SaveActivitiesResponse SaveActivities(SaveActivitiesRequest request);

        /// <summary>
        /// Saves an activity.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response</returns>
        [OperationContract]
        SaveActivitiesResponse SaveActivity(SaveActivitiesRequest request);

        /// <summary>
        /// Deletes an activity.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>The response.</returns>
        [OperationContract]
        DeleteActivityResponse DeleteActivity(DeleteActivityRequest request);

        /// <summary>
        /// Sends the email.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OperationContract]
        SendEmailResponse SendEmail(SendEmailRequest request);

        /// <summary>
        /// Sends the email from template.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OperationContract]
        SendEmailFromTemplateResponse SendEmailFromTemplate(SendEmailFromTemplateRequest request);

        /// <summary>
        /// Gets the job name from R onet.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OperationContract]
        JobNameResponse GetJobNameFromROnet(JobNameRequest request);

        /// <summary>
        /// Gets the name of the career area.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OperationContract]
        CareerAreaNameResponse GetCareerAreaName(CareerAreaNameRequest request);

        SessionResponse<T> GetSessionValue<T>(SessionRequest<T> request);

        SessionResponse<T> SetSessionValue<T>(SessionRequest<T> request);

        [OperationContract]
        SessionResponse RemoveSessionValue(SessionRequest request);

        [OperationContract]
        SessionResponse ClearSession(SessionRequest request);

        [OperationContract]
        ConfigurationItemsResponse GetBoolConfigItems(ConfigurationItemsRequest request);

        [OperationContract]
        SaveSelfServiceResponse SaveSelfService(SaveSelfServiceRequest request);

        [OperationContract]
        AssignActivityOrActionResponse AssignAction(AssignActivityOrActionRequest request);

        GetIntegrationRequestResponsesResponse GetIntegrationRequestResponses(GetIntegrationRequestResponsesRequest request);

        #region Upload File Methods

        /// <summary>
        /// Gets the validation schema for a specific type
        /// </summary>
        /// <param name="request">The service request contain the schema type</param>
        /// <returns>The response containing the schema Xml</returns>
        [OperationContract]
        ValidationSchemaResponse GetValidationSchema(ValidationSchemaRequest request);

        /// <summary>
        /// Gets the csv template for a specific schema type
        /// </summary>
        /// <param name="request">The service request contain the schema type</param>
        /// <returns>The response containing the csv template</returns>
        [OperationContract]
        ValidationSchemaResponse GetCSVTemplateForSchema(ValidationSchemaRequest request);

        /// <summary>
        /// Validates a file
        /// </summary>
        /// <param name="request">The request containing the file</param>
        /// <returns>The validation status of the file</returns>
        [OperationContract]
        UploadFileValidationResponse ValidateUploadFile(UploadFileValidationRequest request);

        /// <summary>
        /// Gets the processing state of the upload file
        /// </summary>
        /// <param name="request">The request containing the file Id</param>
        /// <returns></returns>
        [OperationContract]
        UploadFileDetailsResponse GetUploadFileProcessingState(UploadFileDetailsRequest request);

        /// <summary>
        /// Gets the details of a validation file
        /// </summary>
        /// <param name="request">The request containing the file id</param>
        /// <returns>The validation status of the file</returns>
        [OperationContract]
        UploadFileDetailsResponse GetUploadFileDetails(UploadFileDetailsRequest request);

        /// <summary>
        /// Commits the file after validation
        /// </summary>
        /// <param name="request">The request containing the file id</param>
        /// <returns>The outcome of the commit</returns>
        [OperationContract]
        UploadFileCommitResponse CommitUploadFile(UploadFileCommitRequest request);

        #endregion

        #region Integration

        /// <summary>
        /// Gets the case management records.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        CaseManagementResponse GetCaseManagementRecords(CaseManagementRequest request);

        /// <summary>
        /// Gets the case management record.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        CaseManagementResponse GetCaseManagementRecord(CaseManagementRequest request);

        /// <summary>
        /// Sends a command to the case management system to ignore a message
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        CaseManagementCommandResponse IgnoreCaseManagementMessage(CaseManagementCommandRequest request);

        /// <summary>
        /// Sends a command to the case management system to resend a message
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        CaseManagementCommandResponse ResendCaseManagementMessage(CaseManagementCommandRequest request);

        #endregion

        #region Documents

        /// <summary>
        /// Gets the documents.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OperationContract]
        GetDocumentsResponse GetDocuments(GetDocumentsRequest request);

        /// <summary>
        /// Uploads the document.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OperationContract]
        GetDocumentsResponse UploadDocument(GetDocumentsRequest request);

        /// <summary>
        /// Deletes the document.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OperationContract]
        GetDocumentsResponse DeleteDocument(GetDocumentsRequest request);

        #endregion
    }
}
