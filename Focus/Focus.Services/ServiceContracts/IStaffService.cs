﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.ServiceModel;
using Focus.Core;
using Focus.Core.Messages.StaffService;

#endregion

namespace Focus.Services.ServiceContracts
{
    [ServiceContract(Namespace = Constants.ServiceContractNamespace)]
    public interface IStaffService
    {
        /// <summary>
        /// Searches the staff.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OperationContract]
        StaffSearchResponse SearchStaff(StaffSearchRequest request);

        /// <summary>
        /// Gets the assist user activity view.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OperationContract]
        AssistUserActivityResponse GetAssistUserActivityView(AssistUserActivityRequest request);

        /// <summary>
        /// Gets the assist user employers assisted view.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OperationContract]
        AssistUserEmployersAssistedResponse GetAssistUserEmployersAssistedView(AssistUserEmployersAssistedRequest request);

        /// <summary>
        /// Gets the assist user job seekers assisted view.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OperationContract]
        AssistUserJobSeekersAssistedResponse GetAssistUserJobSeekersAssistedView(AssistUserJobSeekersAssistedRequest request);

        /// <summary>
        /// Gets the staff profile.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OperationContract]
        StaffInfoResponse GetStaffProfile(StaffInfoRequest request);

        /// <summary>
        /// Creates the staff homepage alert.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OperationContract]
        CreateStaffHomepageAlertResponse CreateStaffHomepageAlert(CreateStaffHomepageAlertRequest request);

        /// <summary>
        /// Gets the staff backdate settings.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OperationContract]
        StaffBackdateSettingsResponse GetStaffBackdateSettings(StaffBackdateSettingsRequest request);

        /// <summary>
        /// Sets the staff backdate settings.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns></returns>
        [OperationContract]
        SaveStaffBackdateSettingResponse UpdateStaffBackdateSettings(SaveStaffBackdateSettingRequest request);
    }
}
