﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.ServiceModel;
using Focus.Core;
using Focus.Core.Messages.EmployerService;
using Focus.Data.Migration.Entities;

#endregion

namespace Focus.Services.ServiceContracts
{
	[ServiceContract(Namespace = Constants.ServiceContractNamespace)]
	public interface IEmployerService
	{
		/// <summary>
		/// Gets business units.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		BusinessUnitResponse GetBusinessUnits(BusinessUnitRequest request);

		/// <summary>
		/// Saves business unit.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		SaveBusinessUnitResponse SaveBusinessUnit(SaveBusinessUnitRequest request);

		/// <summary>
		/// Gets business unit descriptions.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		BusinessUnitDescriptionResponse GetBusinessUnitDescriptions(BusinessUnitDescriptionRequest request);

		/// <summary>
		/// Saves business unit description.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		SaveBusinessUnitDescriptionResponse SaveBusinessUnitDescription(SaveBusinessUnitDescriptionRequest request);

		/// <summary>
		/// Saves a business unit's descriptions.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		SaveBusinessUnitDescriptionsResponse SaveBusinessUnitDescriptions(SaveBusinessUnitDescriptionsRequest request);

		/// <summary>
		/// Gets the employer approval status.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		GetEmployerDetailsResponse GetEmployerApprovalStatus(GetEmployerDetailsRequest request);

		/// <summary>
		/// Gets the employer.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		GetEmployerDetailsResponse GetEmployerDetails(GetEmployerDetailsRequest request);

		/// <summary>
		/// Gets the employer legal name.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		GetEmployerDetailsResponse GetEmployerLegalName(GetEmployerDetailsRequest request);

		/// <summary>
		/// Gets the business unit logos.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		BusinessUnitLogoResponse GetBusinessUnitLogos(BusinessUnitLogoRequest request);

		/// <summary>
		/// Saves a business unit's logo.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		SaveBusinessUnitLogoResponse SaveBusinessUnitLogo(SaveBusinessUnitLogoRequest request);

		/// <summary>
		/// Updates a business unit's logo.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		SaveBusinessUnitLogoResponse UpdateBusinessUnitLogo(SaveBusinessUnitLogoRequest request, EmployerRequest originalRequest);
		
		/// <summary>
		/// Updates a business unit's logo.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		SaveBusinessUnitDescriptionResponse UpdateBusinessUnitDescription(SaveBusinessUnitDescriptionRequest request, EmployerRequest originalRequest);

		/// <summary>
		/// Saves a business unit's logos.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		SaveBusinessUnitLogosResponse SaveBusinessUnitLogos(SaveBusinessUnitLogosRequest request);

		/// <summary>
		/// Saves business unit address.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		SaveBusinessUnitAddressResponse SaveBusinessUnitAddress(SaveBusinessUnitAddressRequest request);

		/// <summary>
		/// Gets employer jobs.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		EmployerJobResponse GetEmployerJobs(EmployerJobRequest request);

		/// <summary>
		/// Gets employer job titles.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		EmployerJobTitleResponse GetEmployerJobTitles(EmployerJobTitleRequest request);

		/// <summary>
		/// Gets the employer placements.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		EmployerPlacementsViewResponse GetEmployerPlacements(EmployerPlacementsViewRequest request);

		/// <summary>
		/// Gets an employer address.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		EmployerAddressResponse GetEmployerAddress(EmployerAddressRequest request);

		/// <summary>
		/// Gets the business unit address.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		BusinessUnitAddressResponse GetBusinessUnitAddress(BusinessUnitAddressRequest request);

		/// <summary>
		/// Assigns the employer to admin.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		AssignEmployerToAdminResponse AssignEmployerToAdmin(AssignEmployerToAdminRequest request);

		/// <summary>
		/// Checks the naics exists.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		CheckNaicsExistsResponse CheckNaicsExists(CheckNaicsExistsRequest request);

		/// <summary>
		/// Blocks the employees accounts.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		BlockEmployeesAccountsResponse BlockEmployeesAccounts(BlockEmployeesAccountsRequest request);

		/// <summary>
		/// Unblocks the employees accounts.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		UnblockEmployeesAccountsResponse UnblockEmployeesAccounts(UnblockEmployeesAccountsRequest request);

		/// <summary>
		/// Saves the business unit model.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		SaveBusinessUnitModelResponse SaveBusinessUnitModel(SaveBusinessUnitModelRequest request);

		/// <summary>
		/// Saves the employer details.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		SaveEmployerDetailsResponse SaveEmployerDetails(SaveEmployerDetailsRequest request);

		/// <summary>
		/// Gets the employer profile.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		BusinessUnitProfileResponse GetBusinessUnitProfile(BusinessUnitProfileRequest request);

		/// <summary>
		/// Gets the linked companies for a business unit.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		BusinessUnitLinkedCompaniesResponse GetBusinessUnitLinkedCompanies(BusinessUnitLinkedCompaniesRequest request);


		/// <summary>
		/// Gets the business unit activity view.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		BusinessUnitActivityResponse GetBusinessUnitActivities(BusinessUnitActivityRequest request);

		/// <summary>
		/// Gets the business unit placements view.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		BusinessUnitPlacementsResponse GetBusinessUnitPlacements(BusinessUnitPlacementsRequest request);

		/// <summary>
		/// Gets employer recently placed matches.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		EmployerRecentlyPlacedMatchResponse GetEmployerRecentlyPlacedMatches(EmployerRecentlyPlacedMatchRequest request);

		/// <summary>
		/// Ignores the recently placed matches.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		IgnoreRecentlyPlacedMatchResponse IgnoreRecentlyPlacedMatch(IgnoreRecentlyPlacedMatchRequest request);

		/// <summary>
		/// Allows the recently placed matches.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		AllowRecentlyPlacedMatchResponse AllowRecentlyPlacedMatch(AllowRecentlyPlacedMatchRequest request);

		/// <summary>
		/// Searches the employers.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		SearchBusinessUnitsResponse SearchBusinessUnits(SearchBusinessUnitsRequest request);

		/// <summary>
		/// Gets the offices.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		OfficeResponse GetOffices(OfficeRequest request);

		/// <summary>
		/// Saves the office.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		SaveOfficeResponse SaveOffice(SaveOfficeRequest request);

		/// <summary>
		/// Gets the office staff members.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		StaffMemberResponse GetOfficeStaffMembers(StaffMemberRequest request);

		/// <summary>
		/// Gets the office managers.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		StaffMemberResponse GetOfficeManagers(StaffMemberRequest request);

		/// <summary>
		/// Gets the staff member count.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		StaffMemberResponse GetStaffMemberCount(StaffMemberRequest request);

		/// <summary>
		/// Determines whether [has assigned postcodes] [the specified request].
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		OfficeResponse HasAssignedPostcodes(OfficeRequest request);

		/// <summary>
		/// Saves the person offices.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		PersonOfficeMapperResponse SavePersonOffices(PersonOfficeMapperRequest request);

		/// <summary>
		/// Assigns the office staff.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		PersonOfficeMapperResponse AssignOfficeStaff(PersonOfficeMapperRequest request);

		/// <summary>
		/// Gets the state of the manager.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		GetManagerStateResponse GetManagerState(GetManagerStateRequest request);

		/// <summary>
		/// Determines whether the specified request has employers.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		OfficeEmployersResponse GetOfficeEmployers(OfficeEmployersRequest request);

		/// <summary>
		/// Gets the office job seekers.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		OfficeJobSeekersResponse GetOfficeJobSeekers(OfficeJobSeekersRequest request);

		/// <summary>
		/// Updates the employer assigned office.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		UpdateEmployerAssignedOfficeResponse UpdateEmployerAssignedOffice(UpdateEmployerAssignedOfficeRequest request);

		/// <summary>
		/// Updates the default office.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		UpdateDefaultOfficeResponse UpdateDefaultOffice(UpdateDefaultOfficeRequest request);

		/// <summary>
		/// Gets the office job postings.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		OfficeJobsResponse GetOfficeJobs(OfficeJobsRequest request);

		/// <summary>
		/// Updates the office assigned zips.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		UpdateOfficeAssignedZipsResponse UpdateOfficeAssignedZips(UpdateOfficeAssignedZipsRequest request);

		/// <summary>
		/// Gets the spidered employers.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		SpideredEmployerResponse GetSpideredEmployers(SpideredEmployerRequest request);

		/// <summary>
		/// Gets the spidered employers with good matches.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		SpideredEmployerResponse GetSpideredEmployersWithGoodMatches(SpideredEmployerRequest request);

		/// <summary>
		/// Sets the current office for person.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		SetCurrentOfficeForPersonResponse SetCurrentOfficeForPerson(SetCurrentOfficeForPersonRequest request);

		/// <summary>
		/// Gets the current office for person.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		GetCurrentOfficeForPersonResponse GetCurrentOfficeForPerson(GetCurrentOfficeForPersonRequest request);

		/// <summary>
		/// Determines whether [is office assigned] [the specified request].
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		IsOfficeAssignedResponse IsOfficeAssigned(IsOfficeAssignedRequest request);

		/// <summary>
		/// Activates or Deactivates the office.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		ActivateDeactivateOfficeResponse ActivateDeactivateOffice(ActivateDeactivateOfficeRequest request);

    /// <summary>
    /// Sets whether the employer is preferred or not
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    [OperationContract]
    EmployerPreferredResponse SetEmployerPreferredStatus(EmployerPreferredRequest request);

		/// <summary>
		/// Gets the employer account types.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		GetEmployerAccountTypesResponse GetEmployerAccountTypes(GetEmployerAccountTypesRequest request);
		
		/// <summary>
		/// Gets the FEIN.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		GetFEINResponse GetFEIN(GetFEINRequest request);

		/// <summary>
		/// Validates the fein change.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		ValidateFEINChangeResponse ValidateFEINChange(ValidateFEINChangeRequest request);

		/// <summary>
		/// Saves the fein change.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		SaveFEINChangeResponse SaveFEINChange(SaveFEINChangeRequest request);

		/// <summary>
		/// Updates the primary business unit.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		UpdatePrimaryBusinessUnitResponse UpdatePrimaryBusinessUnit(UpdatePrimaryBusinessUnitRequest request);

		/// <summary>
		/// Gets the hiring manager office email.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		GetHiringManagerOfficeEmailResponse GetHiringManagerOfficeEmail(GetHiringManagerOfficeEmailRequest request);

		/// <summary>
		/// Determines whether [is office administrator] [the specified request].
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		OfficeResponse IsOfficeAdministrator(OfficeRequest request);

		/// <summary>
		/// Creates the business unit homepage alert.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		CreateBusinessUnitHomepageAlertResponse CreateBusinessUnitHomepageAlert(CreateBusinessUnitHomepageAlertRequest request);

	}
}
