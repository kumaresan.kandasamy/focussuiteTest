﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.ServiceModel;
using Focus.Core;
using Focus.Core.Messages.OrganizationService;

#endregion

namespace Focus.Services.ServiceContracts
{
  [ServiceContract(Namespace = Constants.ServiceContractNamespace)]
  public interface IOrganizationService
	{
		/// <summary>
		/// Donts the show job.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    DontDisplayJobResponse DontShowJob(DontDisplayJobRequest request);

		/// <summary>
		/// Applies for job.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    ApplyForJobResponse ApplyForJob(ApplyForJobRequest request);

		/// <summary>
		/// Gets the application list.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    ApplicationListResponse GetApplicationList(ApplicationListRequest request);

		/// <summary>
		/// Matches the resume to posting.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    MatchResponse MatchResumeToPosting(MatchRequest request);

		/// <summary>
		/// Jobs the information.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    JobInformationResponse JobInformation(JobInformationRequest request);
	}
}
