﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.ServiceModel;
using Focus.Core;
using Focus.Core.Messages.CandidateService;
using Focus.Core.Messages.ResumeService;

#endregion

namespace Focus.Services.ServiceContracts
{
  [ServiceContract(Namespace = Constants.ServiceContractNamespace)]
  public interface ICandidateService
	{
		/// <summary>
		/// Sends candidate emails.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    SendCandidateEmailsResponse SendCandidateEmails(SendCandidateEmailsRequest request);

		/// <summary>
		/// Gets a resume.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    GetResumeAsHtmlResponse GetResumeAsHtml(GetResumeAsHtmlRequest request);
		
    /// <summary>
    /// Gets a resume.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    [OperationContract]
    GetResumeResponse GetResume(GetResumeRequest request);

		/// <summary>
		/// Gets a resume.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    GetResumeResponse GetResumeById(GetResumeRequest request);

		/// <summary>
		/// Gets the resume.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    GetResumesResponse GetResumes(GetResumesRequest request);

		/// <summary>
		/// Toggles the candidate flag.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    ToggleCandidateFlagResponse ToggleCandidateFlag(ToggleCandidateFlagRequest request);

		/// <summary>
		/// Gets the job seeker referral view.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    JobSeekerReferralViewResponse GetJobSeekerReferralView(JobSeekerReferralViewRequest request);

		/// <summary>
		/// Searches the job seekers.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    JobSeekerSearchResponse SearchJobSeekers(JobSeekerSearchRequest request);

		/// <summary>
		/// Approves the referral.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    CandidateApplicationResponse ApproveReferral(CandidateApplicationRequest request);

		/// <summary>
		/// Denies the referral.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    CandidateApplicationResponse DenyReferral(CandidateApplicationRequest request);

	  /// <summary>
	  /// Denies the referral.
	  /// </summary>
	  /// <param name="request">The request.</param>
	  /// <returns></returns>
	  [OperationContract]
	  CandidateApplicationResponse HoldReferral(CandidateApplicationRequest request);
		
		/// <summary>
		/// Assigns the activity.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    AssignActivityOrActionResponse AssignActivity(AssignActivityOrActionRequest request);

		/// <summary>
		/// Assigns the activity to multiple users.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
	  [OperationContract]
	  AssignActivityOrActionResponse AssignActivityToMultipleUsers(AssignActivityOrActionRequest request);

		/// <summary>
		/// Gets the job seekers list.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    GetJobSeekerListsResponse GetJobSeekerLists(GetJobSeekerListsRequest request);

		/// <summary>
		/// Adds the job seeker(s) to list.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    AddJobSeekersToListResponse AddJobSeekersToList(AddJobSeekersToListRequest request);

		/// <summary>
		/// Removes the job seeker from list.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    RemoveJobSeekersFromListResponse RemoveJobSeekersFromList(RemoveJobSeekersFromListRequest request);

		/// <summary>
		/// Deletes the job seeker list.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    DeleteJobSeekerListResponse DeleteJobSeekerList(DeleteJobSeekerListRequest request);

		/// <summary>
		/// Gets the job seekers for list.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    GetJobSeekersForListResponse GetJobSeekersForList(GetJobSeekersForListRequest request);

		/// <summary>
		/// Assigns the job seeker to admin.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    AssignJobSeekerToAdminResponse AssignJobSeekerToAdmin(AssignJobSeekerToAdminRequest request);

		/// <summary>
		/// Refers the candidate.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    ReferCandidateResponse ReferCandidate(ReferCandidateRequest request);

		/// <summary>
		/// Gets the candidate system defaults.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    GetCandidateSystemDefaultsResponse GetCandidateSystemDefaults(GetCandidateSystemDefaultsRequest request);

		/// <summary>
		/// Updates the candidate system defaults.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    UpdateCandidateSystemDefaultsResponse UpdateCandidateSystemDefaults(UpdateCandidateSystemDefaultsRequest request);

		/// <summary>
		/// Creates the job seeker homepage alert.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    CreateJobSeekerHomepageAlertResponse CreateJobSeekerHomepageAlert(CreateJobSeekerHomepageAlertRequest request);

		/// <summary>
		/// Saves the person note.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    SavePersonNoteResponse SavePersonNote(SavePersonNoteRequest request);

		/// <summary>
		/// Gets the person note.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    GetPersonNoteResponse GetPersonNote(GetPersonNoteRequest request);

		/// <summary>
		/// Updates the application status.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    UpdateApplicationStatusResponse UpdateApplicationStatus(UpdateApplicationStatusRequest request);

		/// <summary>
		/// Gets the flagged candidates.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    GetFlaggedCandidatesResponse GetFlaggedCandidates(GetFlaggedCandidatesRequest request);

		/// <summary>
		/// Gets the application.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    GetApplicationResponse GetApplication(GetApplicationRequest request);

    /// <summary>
    /// Gets the candidate's referrals
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns>A list of referrals</returns>
    [OperationContract]
    GetApplicationViewResponse GetApplicationViews(GetApplicationViewRequest request);

		/// <summary>
		/// Gets the applicant resume view.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    ResumeViewResponse GetApplicantResumeView(ResumeViewRequest request);

    [OperationContract]
    ApplicationStatusLogViewResponse GetApplicationStatusLogView(ApplicationStatusLogViewRequest request);

		/// <summary>
		/// Gets the job seeker activity view.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    JobSeekerActivityViewResponse GetJobSeekerActivityView(JobSeekerActivityViewRequest request);

		/// <summary>
		/// Gets the job seeker activities.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    JobSeekerActivityActionViewResponse GetJobSeekerActivities(JobSeekerActivityActionViewRequest request);

		/// <summary>
		/// Gets the job seeker activity users.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    JobSeekerActivityActionViewResponse GetJobSeekerActivityUsers(JobSeekerActivityActionViewRequest request);

		/// <summary>
		/// Gets the job seekers and their issues.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    GetCanididatesAndIssuesResponse GetCandidatesAndIssues(GetCandidatesAndIssuesRequest request);

		/// <summary>
		/// Gets the candidate school status.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		GetStudentAlumniIssuesResponse GetStudentAlumniIssues(GetStudentAlumniIssuesRequest request);

		/// <summary>
		/// Gets the job seeker profile.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    JobSeekerProfileResponse GetJobSeekerProfile(JobSeekerProfileRequest request);

    /// <summary>
    /// Gets the candidate.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    [OperationContract]
    GetCandidateResponse GetCandidate(GetCandidateRequest request);

    /// <summary>
    /// Resolves the candidate issues.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    [OperationContract]
    ResolveCandidateIssuesResponse ResolveCandidateIssues(ResolveCandidateIssuesRequest request, bool autoresolve);

    /// <summary>
		/// Gets the open position matches for the candidate.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    CandidateOpenPositionMatchesResponse GetOpenPositionJobs(CandidateOpenPositionMatchesRequest request);
		
		/// <summary>
		/// Ignores the person to posting match.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
		IgnorePersonPostingMatchResponse IgnorePersonPostingMatch(IgnorePersonPostingMatchRequest request);
		
		/// <summary>
		/// Allows the person to posting match.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
		AllowPersonPostingMatchResponse AllowPersonPostingMatch(AllowPersonPostingMatchRequest request);

    /// <summary>
    /// Flags the candidate for follow up.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    [OperationContract]
    FlagCandidateForFollowUpResponse FlagCandidateForFollowUp(FlagCandidateForFollowUpRequest request);

    /// <summary>
    /// Registers action when an Assist user ran a search for jobs for a seeker
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    [OperationContract]
    RegisterFindJobsForSeekerResponse RegisterFindJobsForSeeker(RegisterFindJobsForSeekerRequest request);

		/// <summary>
		/// Registers action when job seeker click how to apply.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    RegisterHowToApplyResponse RegisterHowToApply(RegisterHowToApplyRequest request);

		/// <summary>
		/// Registers the self referral.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
	  [OperationContract]
		RegisterSelfReferralResponse RegisterSelfReferral(RegisterSelfReferralRequest request);
		
		/// <summary>
		/// Shows the not what your looking for.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    LogUIActionResponse ShowNotWhatYourLookingFor(LogUIActionRequest request);
		
		/// <summary>
		/// Views the job details.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    LogUIActionResponse ViewJobDetails(LogUIActionRequest request);

		/// <summary>
		/// Updates the assigned staff member.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		UpdateAssignedStaffMemberResponse UpdateAssignedStaffMember(UpdateAssignedStaffMemberRequest request);

    /// <summary>
    /// Registers the job posting of interest sent.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    [OperationContract]
    RegisterJobPostingOfInterestSentResponse RegisterJobPostingOfInterestSent(RegisterJobPostingOfInterestSentRequest request);

    /// <summary>
    /// Gets the postings of interest received.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    [OperationContract]
    GetPostingsOfInterestReceivedResponse GetPostingsOfInterestReceived(GetPostingsOfInterestReceivedRequest request);

    /// <summary>
    /// Marks the application viewed.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    [OperationContract]
    MarkApplicationViewedResponse MarkApplicationViewed(MarkApplicationViewedRequest request);

		/// <summary>
		/// Gets the new applicant referral view.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
	  [OperationContract]
		NewApplicantReferralViewResponse GetNewApplicantReferralView(NewApplicantReferralViewRequest request);

		/// <summary>
		/// Saves the job seeker survey.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
	  [OperationContract]
	  JobSeekerSurveyResponse SaveJobSeekerSurvey(JobSeekerSurveyRequest request);

    /// <summary>
    /// Gets the latest job seeker survey.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    [OperationContract]
    JobSeekerSurveyResponse GetLatestSurvey(JobSeekerSurveyRequest request);

		/// <summary>
		/// Gets the referral view.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
	  [OperationContract]
	  GetReferralViewResponse GetReferralViews(GetReferralViewRequest request);

    /// <summary>
    /// Gets the campuses.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    [OperationContract]
    GetCampusesResponse GetCampuses(GetCampusesRequest request);

		/// <summary>
		/// Gets the UI claimant status.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
	  [OperationContract]
	  UiClaimantStatusResponse GetUiClaimantStatus(UiClaimantStatusRequest request);
		
		/// <summary>
		/// Gets the invitees for job.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns>InviteesForJobResponse.</returns>
	  [OperationContract]
	  InviteesForJobResponse GetInviteesForJob(InviteesForJobRequest request);

		/// <summary>
		/// Determines whether [is candidate exisiting job invitee or applicant] [the specified request].
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		IsCandidateExistingInviteeOrApplicantResponse IsCandidateExisitingJobInviteeOrApplicant(IsCandidateExistingInviteeOrApplicantRequest request);

    /// <summary>
    /// Sets the MSFW verified status.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns>VerifyMSFWStatusResponse.</returns>
    [OperationContract]
    VerifyMSFWStatusResponse VerifyMSFWStatus(VerifyMSFWStatusRequest request);

		/// <summary>
		/// Checks if candidate is invited to apply for the specified job
		/// </summary>
		/// <param name="jobId"></param>
		/// <param name="pesonId"></param>
		/// <returns></returns>
	  [OperationContract]
	  bool IsCandidateInvitedToApplyForThisJob(long jobId, long personId);

		/// <summary>
		/// Gets the push notification list.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		GetPushNotificationListResponse GetPushNotificationList(GetPushNotificationListRequest request);

		/// <summary>
		/// Gets the push notification.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		GetPushNotificationResponse GetPushNotification(GetPushNotificationRequest request);

		/// <summary>
		/// Vieweds the push notification.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		ViewedPushNotificationResponse ViewedPushNotification(ViewedPushNotificationRequest request);
	}
}
