﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.ServiceModel;
using Focus.Core;
using Focus.Core.Messages.ExplorerService;
using SendEmailRequest = Focus.Core.Messages.ExplorerService.SendEmailRequest;
using SendEmailResponse = Focus.Core.Messages.ExplorerService.SendEmailResponse;

#endregion

namespace Focus.Services.ServiceContracts
{
  [ServiceContract(Namespace = Constants.ServiceContractNamespace)]
  public interface IExplorerService
  {
    [OperationContract]
    PingResponse Ping(PingRequest request);

    [OperationContract]
    StateAreaResponse GetStateAreas(StateAreaRequest request);

    [OperationContract]
    JobStateAreaResponse GetJobs(JobStateAreaRequest request);

    [OperationContract]
    EmployerResponse GetEmployers(EmployerRequest request);

    [OperationContract]
    DegreeEducationLevelResponse GetDegreeAliases(DegreeEducationLevelRequest request);

    [OperationContract]
    CareerAreaResponse GetCareerArea(CareerAreaRequest request);

    [OperationContract]
    CareerAreaResponse GetCareerAreas(CareerAreaRequest request);

    [OperationContract]
    JobTitleResponse GetJobTitles(JobTitleRequest request);

    [OperationContract]
    SkillCategoryResponse GetSkillCategories(SkillCategoryRequest request);

    [OperationContract]
    SkillResponse GetSkills(SkillRequest request);

    [OperationContract]
    SkillResponse GetSkillsForJobs(SkillRequest request);

    [OperationContract]
    SiteSearchResponse GetSiteSearchResults(SiteSearchRequest request);

    [OperationContract]
    RelatedJobsResponse GetRelatedJobs(RelatedJobsRequest request);

    [OperationContract]
    JobReportResponse GetJobReport(JobReportRequest request);

    [OperationContract]
    JobTitleResponse GetJobJobTitles(JobTitleRequest request);

    [OperationContract]
    JobReportResponse GetSimilarJobReport(JobReportRequest request);

    [OperationContract]
    JobEmployerResponse GetJobEmployers(JobEmployerRequest request);

    [OperationContract]
    JobSkillResponse GetJobSkills(JobSkillRequest request);

    [OperationContract]
    JobDegreeCertificationResponse GetJobDegreeCertifications(JobDegreeCertificationRequest request);

    [OperationContract]
    JobDegreeCertificationResponse GetJobDegreeReportCertifications(JobDegreeCertificationRequest request);

    [OperationContract]
    JobEducationRequirementResponse GetJobEducationRequirements(JobEducationRequirementRequest request);

    [OperationContract]
    JobExperienceLevelResponse GetJobExperienceLevels(JobExperienceLevelRequest request);

    [OperationContract]
    JobCareerPathResponse GetJobCareerPaths(JobCareerPathRequest request);


    [OperationContract]
    EmployerResponse GetEmployerReport(EmployerRequest request);

    [OperationContract]
    EmployerResponse GetEmployer(EmployerRequest request);

    [OperationContract]
    EmployerJobResponse GetEmployerJobs(EmployerJobRequest request);

    [OperationContract]
    EmployerSkillResponse GetEmployerSkills(EmployerSkillRequest request);

    [OperationContract]
    EmployerInternshipResponse GetEmployerInternships(EmployerInternshipRequest request);


    [OperationContract]
    DegreeEducationLevelResponse GetDegreeEducationLevelReport(DegreeEducationLevelRequest request);

    [OperationContract]
    DegreeEducationLevelResponse GetDegreeEducationLevel(DegreeEducationLevelRequest request);

    [OperationContract]
    JobReportResponse GetDegreeEducationLevelJobReport(JobReportRequest request);

    [OperationContract]
    DegreeEducationLevelSkillResponse GetDegreeEducationLevelSkills(DegreeEducationLevelSkillRequest request);

    [OperationContract]
    DegreeEducationLevelEmployerResponse GetDegreeEducationLevelEmployers(DegreeEducationLevelEmployerRequest request);

    [OperationContract]
    DegreeEducationLevelStudyPlaceResponse GetDegreeEducationLevelStudyPlaces(DegreeEducationLevelStudyPlaceRequest request);

    [OperationContract]
    DegreeEducationLevelResponse GetDegreeEducationLevelExts(DegreeEducationLevelRequest request);


    [OperationContract]
    SkillResponse GetSkillReport(SkillRequest request);

    [OperationContract]
    SkillResponse GetSkill(SkillRequest request);

    [OperationContract]
    JobReportResponse GetSkillJobReport(JobReportRequest request);

    [OperationContract]
    SkillEmployerResponse GetSkillEmployers(SkillEmployerRequest request);

    [OperationContract]
    SkillInternshipsResponse GetInternshipsBySkill(SkillInternshipsRequest request);


    [OperationContract]
    InternshipResponse GetInternshipReport(InternshipRequest request);

    [OperationContract]
    InternshipResponse GetInternship(InternshipRequest request);

    [OperationContract]
    InternshipSkillResponse GetInternshipSkills(InternshipSkillRequest request);

    [OperationContract]
    InternshipEmployerResponse GetInternshipEmployers(InternshipEmployerRequest request);
    
    [OperationContract]
    InternshipJobResponse GetInternshipJobs(InternshipJobRequest request);

  	[OperationContract]
  	InternshipCategoryResponse GetInternshipCategories(InternshipCategoryRequest request);


    [OperationContract]
    SendEmailResponse SendEmail(SendEmailRequest request);

    [OperationContract]
    SendEmailResponse SendHelpEmail(SendEmailRequest request);

		/// <summary>
		/// Uploads a resume.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    UploadResumeResponse UploadResume(UploadResumeRequest request);

    [OperationContract]
    MyAccountResponse GetMyAccountModel(MyAccountRequest request);

    [OperationContract]
    MyResumeResponse GetMyResumeModel(MyResumeRequest request);

    [OperationContract]
    JobFromROnetResponse GetJobFromROnet(JobFromROnetRequest request);

    [OperationContract]
    ROnetFromResumeResponse GetROnetFromResume(ROnetFromResumeRequest request);

    [OperationContract]
    JobIdToROnetResponse GetROnetsFromJob(JobIdToROnetRequest request);

    [OperationContract]
    JobIntroductionResponse GetJobIntroduction(JobIntroductionRequest request);

    [OperationContract]
    ProgramAreaResponse GetProgramAreas(ProgramAreaRequest request);

    [OperationContract]
    DegreeResponse GetDegrees(DegreeRequest request);

    [OperationContract]
    ProgramAreaResponse GetProgramAreaDegrees(ProgramAreaRequest request);

    [OperationContract]
    ProgramAreaResponse GetROnetProgramAreaDegrees(ProgramAreaRequest request);

    [OperationContract]
    ROnetToOnetResponse GetOnetFromROnet(ROnetToOnetRequest request);

  	[OperationContract]
  	CareerAreaJobResponse GetCareerAreaJobs(CareerAreaJobRequest request);

    [OperationContract]
    CareerAreaJobTotalResponse GetCareerAreaJobTotals(CareerAreaJobTotalRequest request);

    [OperationContract]
    JobCareerAreaResponse GetJobCareerAreas(JobCareerAreaRequest request);

    [OperationContract]
    OnetToSOCResponse GetSOCForOnet(OnetToSOCRequest request);
  }
}
