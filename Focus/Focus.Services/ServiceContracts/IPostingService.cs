﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.ServiceModel;
using Focus.Core;
using Focus.Core.Messages.OrganizationService;
using Focus.Core.Messages.PostingService;

#endregion

namespace Focus.Services.ServiceContracts
{
  [ServiceContract(Namespace = Constants.ServiceContractNamespace)]
  public interface IPostingService
	{
		/// <summary>
		/// Fetches the posting.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    PostingResponse FetchPosting(PostingRequest request);

		/// <summary>
		/// Gets the posting id.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
    PostingResponse GetPostingId(PostingRequest request);

    /// <summary>
    /// Gets the posting's BGTOCCs
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    [OperationContract]
    PostingResponse GetPostingBGTOccs(PostingRequest request);

		/// <summary>
		/// Closes the posting.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    ClosePostingResponse ClosePosting(ClosePostingRequest request);

		/// <summary>
		/// Gets the typical resumes.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    TypicalResumesResponse GetTypicalResumes(TypicalResumesRequest request);

		/// <summary>
		/// Gets the viewed postings.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    ViewedPostingResponse GetViewedPostings(ViewedPostingRequest request);

		/// <summary>
		/// Adds the viewed postings.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    AddViewedPostingResponse AddViewedPostings(AddViewedPostingRequest request);

		/// <summary>
    /// Gets the job id from posting.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    [OperationContract]
    GetJobIdFromPostingResponse GetJobIdFromPosting(GetJobIdFromPostingRequest request);

		/// <summary>
		/// Gets the compare posting to resume modal model.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
  	[OperationContract]
  	GetComparePostingToResumeModalModelResponse GetComparePostingToResumeModalModel(GetComparePostingToResumeModalModelRequest request);

		/// <summary>
		/// Gets the posting employer contact model.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
  	[OperationContract]
  	GetPostingEmployerContactModelResponse GetPostingEmployerContactModel(GetPostingEmployerContactModelRequest request);

		/// <summary>
		/// Gets the spidered postings.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
  	[OperationContract]
  	SpideredPostingResponse GetSpideredPostings(SpideredPostingRequest request);

		/// <summary>
		/// Gets the spidered postings with good matches.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		SpideredPostingResponse GetSpideredPostingsWithGoodMatches(SpideredPostingRequest request);
	}
}
