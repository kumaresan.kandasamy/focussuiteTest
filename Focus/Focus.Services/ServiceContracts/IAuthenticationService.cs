﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.ServiceModel;
using Focus.Core;
using Focus.Core.Messages.AuthenticationService;

#endregion

namespace Focus.Services.ServiceContracts
{
  [ServiceContract(Namespace = Constants.ServiceContractNamespace)]
  public interface IAuthenticationService
	{
    /// <summary>
    /// Validates a session.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    [OperationContract]
    SessionResponse ValidateSession(SessionRequest request);

		/// <summary>
		/// Starts a session.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    SessionResponse StartSession(SessionRequest request);

		/// <summary>
		/// Logs the user in.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    LogInResponse LogIn(LogInRequest request);

		/// <summary>
		/// Logs the user out.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    LogOutResponse LogOut(LogOutRequest request);

		/// <summary>
		/// Processes a forgotten password processing for the user.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    ProcessForgottenPasswordResponse ProcessForgottenPassword(ProcessForgottenPasswordRequest request);

		/// <summary>
		/// Creates the single sign on.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    CreateSingleSignOnResponse CreateSingleSignOn(CreateSingleSignOnRequest request);

		/// <summary>
		/// Validates the single sign on.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    ValidateSingleSignOnResponse ValidateSingleSignOn(ValidateSingleSignOnRequest request);

    /// <summary>
    /// Validates whether (encrypted) user details exist in an external system
    /// </summary>
    /// <param name="request"></param>
    /// <returns></returns>
    [OperationContract]
    ExternalAuthenticationResponse ValidateExternalAuthentication(ExternalAuthenticationRequest request);

		/// <summary>
		/// Validates the password reset validation key.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    ValidatePasswordResetValidationKeyResponse ValidatePasswordResetValidationKey(ValidatePasswordResetValidationKeyRequest request);

    /// <summary>
    /// Gets the career user data for the authenticated user
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    [OperationContract]
    CareerUserDataResponse GetCareerUserData(CareerUserDataRequest request);

		/// <summary>
		/// Validates the pin registration.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
  	[OperationContract]
  	ValidatePinResponse ValidatePinRegistration(ValidatePinRequest request);

    /// <summary>
    /// Gets the security questions.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    [OperationContract]
    SecurityQuestionResponse GetSecurityQuestions(SecurityQuestionRequest request);

    /// <summary>
    /// Processes the security questions.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    [OperationContract]
    ProcessSecurityQuestionsResponse ProcessSecurityQuestions(ProcessSecurityQuestionsRequest request);
	}
}
