﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.ServiceModel;

using Focus.Core;
using Focus.Core.Messages.ProcessorService;

#endregion

namespace Focus.Services.ServiceContracts
{
  [ServiceContract(Namespace = Constants.ServiceContractNamespace)]
  public interface IProcessorService
  {
		/// <summary>
		/// Enqueues the batch process.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
  	[OperationContract]
  	EnqueueBatchResponse EnqueueBatch(EnqueueBatchRequest request);

    /// <summary>
    /// Enqueues the import process.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    [OperationContract]
    EnqueueImportResponse EnqueueImport(EnqueueImportRequest request);

    /// <summary>
    /// Enqueues the report population request
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    [OperationContract]
    EnqueueReportPopulationResponse EnqueueReportPopulation(EnqueueReportPopulationRequest request);

    /// <summary>
    /// Resets the cache.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    [OperationContract]
    ResetCacheResponse ResetCache(ResetCacheRequest request);

    /// <summary>
    /// Encrypts fields in the database (when encryption has been enabled).
    /// </summary>
    /// <param name="request">The request containing the field to encrypt.</param>
    /// <returns>A response containing a boolean indicating if the field had already been encrypted</returns>
    [OperationContract]
    EncryptionResponse EncryptFields(EncryptionRequest request);

    /// <summary>
    /// Decrypts fields in the database (when decryption has been enabled).
    /// </summary>
    /// <param name="request">The request containing the field to decrypt.</param>
    /// <returns>A response containing a boolean indicating if the field had already been decrypted</returns>
    [OperationContract]
    EncryptionResponse DecryptFields(EncryptionRequest request);

    /// <summary>
    /// Sends Reset Password emails to imported users
    /// </summary>
    /// <param name="request">The request containing the number of emails to send.</param>
    /// <returns>A response containing a boolean indicating if any emails were sent</returns>
    [OperationContract]
    ImportedUsersResponse SendEmailsToImportedUsers(ImportedUsersRequest request);
  }
}
