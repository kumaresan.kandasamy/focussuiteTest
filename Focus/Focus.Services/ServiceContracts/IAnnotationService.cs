﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.ServiceModel;

using Focus.Core;
using Focus.Core.Messages.AnnotationService;

#endregion

namespace Focus.Services.ServiceContracts
{
  [ServiceContract(Namespace = Constants.ServiceContractNamespace)]
  public interface IAnnotationService
	{
    /// <summary>
    /// Gets a bookmark.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    [OperationContract]
    BookmarkResponse GetBookmark(BookmarkRequest request);

		/// <summary>
		/// Creates the bookmark.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    CreateBookmarkResponse CreateBookmark(CreateBookmarkRequest request);

		/// <summary>
		/// Deletes the bookmark.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    DeleteBookmarkResponse DeleteBookmark(DeleteBookmarkRequest request);

		/// <summary>
		/// Saves the search.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    SaveSearchResponse SaveSearch(SaveSearchRequest request);

		/// <summary>
		/// Lists the search.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    ListSearchResponse ListSearch(ListSearchRequest request);

		/// <summary>
		/// Deletes the save search.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    DeleteSaveSearchResponse DeleteSaveSearch(DeleteSaveSearchRequest request);
	}
}
