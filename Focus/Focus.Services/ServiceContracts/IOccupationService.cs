﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.ServiceModel;
using Focus.Core;
using Focus.Core.Messages.OccupationService;

#endregion

namespace Focus.Services.ServiceContracts
{
  [ServiceContract(Namespace = Constants.ServiceContractNamespace)]
  public interface IOccupationService
	{
		/// <summary>
		/// Gets the resume builder questions.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    ResumeBuilderQuestionsResponse GetResumeBuilderQuestions(ResumeBuilderQuestionsRequest request);

		/// <summary>
		/// Gets the statements.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    StatementsResponse GetStatements(StatementsRequest request);

		/// <summary>
		/// Gets the onet skills.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    OnetSkillsResponse GetOnetSkills(OnetSkillsRequest request);

		/// <summary>
		/// Gets the moc occupation codes.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
		OccupationCodeResponse GetMocOccupationCodes(OccupationCodeRequest request);

		/// <summary>
		/// Gets the occupation codes.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
		OccupationCodeResponse GetOccupationCodes(OccupationCodeRequest request);

		/// <summary>
		/// Gets the military occupation job titles.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
  	[OperationContract]
  	MilitaryOccupationJobTitleResponse GetMilitaryOccupationJobTitles(MilitaryOccupationJobTitleRequest request);

    /// <summary>
    /// Converts the onet to ronet.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    [OperationContract]
    OnetToROnetConversionResponse ConvertOnetToROnet(OnetToROnetConversionRequest request);
	}
}
