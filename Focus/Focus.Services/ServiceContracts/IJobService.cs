﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.ServiceModel;
using Focus.Core;
using Focus.Core.Messages.JobService;

#endregion

namespace Focus.Services.ServiceContracts
{
  [ServiceContract(Namespace = Constants.ServiceContractNamespace)]
  public interface IJobService
	{
		/// <summary>
		/// Gets a job view.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    JobViewResponse GetJobView(JobViewRequest request);

		/// <summary>
		/// Gets the job details.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    JobDetailsResponse GetJobDetails(JobDetailsRequest request);

	  /// <summary>
	  /// Gets the previous job details.
	  /// </summary>
	  /// <param name="request">The request.</param>
	  /// <returns></returns>
	  [OperationContract]
	  JobPrevDataResponse GetJobPrevData(JobPrevDataRequest request);


    /// <summary>
    /// Gets the job status.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    [OperationContract]
    JobStatusResponse GetJobStatus(JobStatusRequest request);

    /// <summary>
    /// Gets dates for the job
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    [OperationContract]
    JobDatesResponse GetJobDates(JobDatesRequest request);

		/// <summary>
		/// Gets the similar jobs.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    SimilarJobResponse GetSimilarJobs(SimilarJobRequest request);

		/// <summary>
		/// Updates a job status.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    JobResponse UpdateJobStatus(JobRequest request);

		/// <summary>
		/// Refreshes a job.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    JobResponse RefreshJob(JobRequest request);

		/// <summary>
		/// Reactivates a job.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    JobResponse ReactivateJob(JobRequest request);

		/// <summary>
		/// Closes a job.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    JobResponse CloseJob(JobRequest request);

	  /// <summary>
	  /// Flags a job issue for follow up.
	  /// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
	  [OperationContract]
	  JobResponse FlagJobForFollowUp(JobRequest request);

		/// <summary>
		/// Duplicates a job.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    JobResponse DuplicateJob(JobRequest request);

		/// <summary>
		/// Deletes a job.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    JobResponse DeleteJob(JobRequest request);

		/// <summary>
		/// Gets a job.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    JobResponse GetJob(JobRequest request);

		/// <summary>
		/// Saves a job.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    SaveJobResponse SaveJob(SaveJobRequest request);

	  /// <summary>
	  /// Stores relevant sections of job in the JobPrevData table for later proofing.
	  /// </summary>
	  /// <param name="request">The request.</param>
	  /// <returns></returns>
	  [OperationContract]
	  JobResponse SaveBeforeImageOfJob(JobRequest request);

		/// <summary>
		/// Saves a spidered job
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		SaveSpideredJobResponse SaveSpideredJob(SaveSpideredJobRequest request);

		/// <summary>
		/// Posts a job.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    PostJobResponse PostJob(PostJobRequest request);

		/// <summary>
		/// Checks a job for censoredship.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    CheckCensorshipResponse CheckCensorship(CheckCensorshipRequest request);

		/// <summary>
		/// Gets a job's locations.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    JobLocationResponse GetJobLocations(JobLocationRequest request);

		/// <summary>
		/// Saves a job's locations.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    SaveJobLocationsResponse SaveJobLocations(SaveJobLocationsRequest request);

		/// <summary>
		/// Gets a job description.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    JobDescriptionResponse GetJobDescription(JobDescriptionRequest request);

		/// <summary>
		/// Gets a job's certificates.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    JobCertificateResponse GetJobCertificates(JobCertificateRequest request);

		/// <summary>
		/// Saves a job's certificates.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    SaveJobCertificatesResponse SaveJobCertificates(SaveJobCertificatesRequest request);

    /// <summary>
    /// Saves a job's certificates.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    [OperationContract]
    SaveJobEducationInternshipSkillsResponse SaveJobEducationInternshipSkills(SaveJobEducationInternshipSkillsRequest request);

		/// <summary>
		/// Gets a job's languages.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    JobLanguageResponse GetJobLanguages(JobLanguageRequest request);

		/// <summary>
		/// Saves a job's languages.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    SaveJobLanguagesResponse SaveJobLanguages(SaveJobLanguagesRequest request);
		
		/// <summary>
		/// Gets the job programs of study.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		JobProgramOfStudyResponse GetJobProgramsOfStudy(JobProgramOfStudyRequest request);
		
		/// <summary>
		/// Saves the job programs of study.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		SaveJobProgramsOfStudyResponse SaveJobProgramsOfStudy(SaveJobProgramsOfStudyRequest request);
		
		/// <summary>
		/// Gets a job's licences.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    JobLicenceResponse GetJobLicences(JobLicenceRequest request);

		/// <summary>
		/// Saves a job's licences.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    SaveJobLicencesResponse SaveJobLicences(SaveJobLicencesRequest request);

		/// <summary>
		/// Gets a job's special requirements.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    JobSpecialRequirementResponse GetJobSpecialRequirements(JobSpecialRequirementRequest request);

		/// <summary>
		/// Saves a job's special requirements.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    SaveJobSpecialRequirementsResponse SaveJobSpecialRequirements(SaveJobSpecialRequirementsRequest request);

		/// <summary>
		/// Gets the job driving licence endorsements.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		JobDrivingLicenceEndorsementResponse GetJobDrivingLicenceEndorsements(JobDrivingLicenceEndorsementRequest request);

		/// <summary>
		/// Saves the job driving licence endorsements.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		SaveJobDrivingLicenceEndorsementsResponse SaveJobDrivingLicenceEndorsements(SaveJobDrivingLicenceEndorsementsRequest request);

		/// <summary>
		/// Gets a job's tasks.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    JobTaskResponse GetJobTasks(JobTaskRequest request);
		
		/// <summary>
		/// Gets the job posting referral view.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    JobPostingReferralViewResponse GetJobPostingReferralView(JobPostingReferralViewRequest request);

		/// <summary>
		/// Approves the referral.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    JobResponse ApproveReferral(JobRequest request);


		/// <summary>
		/// Denies the referral.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    JobResponse DenyReferral(JobRequest request);

		/// <summary>
		/// Edits the posting referral.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    JobResponse EditPostingReferral(JobRequest request);

		/// <summary>
		/// Gets the job lookup.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    JobLookupResponse GetJobLookup(JobLookupRequest request);

		/// <summary>
		/// Gets the job URL.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    JobLensPostingIdResponse GetJobLensPostingId(JobLensPostingIdRequest request);

		/// <summary>
		/// Gets the job URL.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract(Name = "GetJobLensPostingByExternalId")]
		JobIdsResponse GetJobLensPostingId(JobIdsRequest request);

		/// <summary>
		/// Gets the job address.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    JobAddressResponse GetJobAddress(JobAddressRequest request);

		/// <summary>
		/// Saves the job address.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    SaveJobAddressResponse SaveJobAddress(SaveJobAddressRequest request);

		/// <summary>
		/// Gets the application instructions.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    ApplicationInstructionsResponse GetApplicationInstructions(ApplicationInstructionsRequest request);

		/// <summary>
		/// Gets the job applicants.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    JobApplicantsResponse GetJobApplicants(JobApplicantsRequest request);

		/// <summary>
		/// Generates the posting.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    GeneratePostingResponse GeneratePosting(GeneratePostingRequest request);

    /// <summary>
    /// Gets the job activities.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    [OperationContract]
    JobActivitiesResponse GetJobActivities(JobActivitiesRequest request);

    /// <summary>
    /// Prints the job.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    [OperationContract]
    PrintJobResponse PrintJob(PrintJobRequest request);

    /// <summary>
    /// Gets the job activity users.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    [OperationContract]
    JobActivityUsersResponse GetJobActivityUsers(JobActivityUsersRequest request);

		/// <summary>
		/// Gets the job activity summary.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    JobDetailsResponse GetJobActivitySummary(JobDetailsRequest request);

    /// <summary>
    /// Saves the job invite to apply.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    [OperationContract]
    SaveJobInviteToReplyResponse SaveJobInviteToApply(SaveJobInviteToApplyRequest request);

    /// <summary>
    /// Updates the invite to apply.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    [OperationContract]
    InviteToApplyVisitedResponse UpdateInviteToApply(InviteToApplyVisitedRequest request);
		
    /// <summary>
    /// Gets a job's education internship skills.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    [OperationContract]
    JobEducationInternshipSkillResponse GetJobEducationInternshipSkills(JobEducationInternshipSkillRequest request);

		/// <summary>
		/// Updates the job assigned office.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
  	[OperationContract]
		UpdateJobAssignedOfficeResponse UpdateJobAssignedOffice(UpdateJobAssignedOfficeRequest request);

		/// <summary>
		/// Updates the job assigned staff member.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
  	[OperationContract]
  	UpdateJobAssignedStaffMemberResponse UpdateJobAssignedStaffMember(UpdateJobAssignedStaffMemberRequest request);

		/// <summary>
		/// Updates the criminal background exclusion required.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
	  [OperationContract]
	  UpdateCriminalBackgroundExclusionRequiredResponse UpdateCriminalBackgroundExclusionRequired(UpdateCriminalBackgroundExclusionRequiredRequest request);

		/// <summary>
		/// Gets the criminal background exclusion required.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		CriminalBackgroundExclusionRequiredResponse GetCriminalBackgroundExclusionRequired(CriminalBackgroundExclusionRequiredRequest request);

    /// <summary>
    /// Gets the job's issues.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    [OperationContract]
    JobIssuesResponse GetJobIssues(JobIssuesRequest request);

    /// <summary>
    /// Resolve the job's issues.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    [OperationContract]
    ResolveJobIssuesResponse ResolveJobIssues(ResolveJobIssuesRequest request);

		/// <summary>
		/// Invites the jobseekers to apply for the job.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
	  [OperationContract]
	  InviteJobseekersToApplyResponse InviteJobseekersToApply(InviteJobseekersToApplyRequest request);

    /// <summary>
    /// Imports the job.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    [OperationContract]
    ImportJobResponse ImportJob(ImportJobRequest request);

		/// <summary>
		/// Gets the job credit check required.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		GetJobCreditCheckRequiredResponse GetJobCreditCheckRequired(GetJobCreditCheckRequiredRequest request);

		/// <summary>
		/// Updates the credit check required.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
	  [OperationContract]
	  UpdateCreditCheckRequiredResponse UpdateCreditCheckRequired(UpdateCreditCheckRequiredRequest request);

		/// <summary>
		/// Gets the hiring manager person name email for job.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
	  [OperationContract]
	  HiringManagerPersonNameEmailForJobResponse GetHiringManagerPersonNameEmailForJob(HiringManagerPersonNameEmailForJobRequest request);
	}
}
