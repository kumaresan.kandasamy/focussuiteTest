﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.ServiceModel;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Messages.AccountService;

#endregion

namespace Focus.Services.ServiceContracts
{
  [ServiceContract(Namespace = Constants.ServiceContractNamespace)]
  public interface IAccountService
  {
    /// <summary>
    /// Blocks or unblocks a person
    /// </summary>
    /// <param name="request">The request indicating the person and whether to block/unblock them.</param>
    /// <returns>A response indicating whether they were successfully blocked/unblocked</returns>
    [OperationContract]
    BlockUnblockPersonResponse BlockUnblockPerson(BlockUnblockPersonRequest request);

		/// <summary>
		/// Changes the password for the user.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    ChangePasswordResponse ChangePassword(ChangePasswordRequest request);

		/// <summary>
		/// Validates an Employer.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    ValidateEmployerResponse ValidateEmployer(ValidateEmployerRequest request);

		/// <summary>
		/// Registers a new Talent user.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    RegisterTalentUserResponse RegisterTalentUser(RegisterTalentUserRequest request);

    /// <summary>
    /// Updates an existing Talent user (along with associated BU and employer). Created mainly for the import process
    /// </summary>
    /// <param name="request">The request holding details of the Talent user.</param>
    /// <returns></returns>
    [OperationContract]
    RegisterTalentUserResponse UpdateTalentUser(RegisterTalentUserRequest request);

		/// <summary>
		/// Checks the user exists.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    CheckUserExistsResponse CheckUserExists(CheckUserExistsRequest request);

    /// <summary>
    /// Checks whether the social security number is in use.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    [OperationContract]
    CheckSocialSecurityNumberInUseResponse CheckSocialSecurityNumberInUse(
      CheckSocialSecurityNumberInUseRequest request);

		/// <summary>
		/// Finds the assist account.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    FindUserResponse FindUser(FindUserRequest request);

		/// <summary>
		/// Finds the user by external identifier.
		/// </summary>
		/// <remarks>This is not marked with the OperationContract attribute because there is no
		/// validation against the user context in the standard implementation.  Exposing the method to
		/// the wcf services could pose a potential security issue.</remarks>
		/// <param name="request">The request.</param>
		/// <returns></returns>
	  FindUserResponse FindUserByExternalId(FindUserRequest request);

		/// <summary>
		/// Creates the user.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    CreateAssistUserResponse CreateAssistUser(CreateAssistUserRequest request);

		/// <summary>
		/// Deletes the user.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    DeleteUserResponse DeleteUser(DeleteUserRequest request);

		/// <summary>
		/// Requests the user account password reset.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    ResetUsersPasswordResponse ResetUsersPassword(ResetUsersPasswordRequest request);

		/// <summary>
		/// Deactivates the user.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    DeactivateUserResponse DeactivateUser(DeactivateUserRequest request);

	  /// <summary>
	  /// Gets the user type of a user
	  /// </summary>
	  /// <param name="request">The request containing the user id</param>
	  /// <returns>The response containing the user type</returns>
	  [OperationContract]
	  GetUserTypeResponse GetUserType(GetUserTypeRequest request);

		/// <summary>
		/// Gets the user details.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    GetUserDetailsResponse GetUserDetails(GetUserDetailsRequest request);

	  /// <summary>
	  /// Get Security Questions for user
	  /// </summary>
	  /// <param name="request"></param>
	  /// <returns></returns>
	  [OperationContract]
		SecurityQuestionDetailsResponse GetUserSecurityQuestion( SecurityQuestionDetailsRequest request );

		/// <summary>
		/// Checks Security Questions for user
		/// </summary>
		/// <param name="request"></param>
		/// <returns></returns>
		[OperationContract]
		SecurityQuestionDetailsResponse HasUserSecurityQuestions(SecurityQuestionDetailsRequest request);

		/// <summary>
		/// Changes the username.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    ChangeUsernameResponse ChangeUsername(ChangeUsernameRequest request);

	  /// <summary>
	  /// Checks if an email address is in use by another user
	  /// </summary>
	  /// <param name="request">The request with the email details.</param>
	  /// <returns>A response indicating if the email address is in use</returns>
		[OperationContract]
		CheckEmailAddressResponse CheckEmailAddress(CheckEmailAddressRequest request);

    /// <summary>
    /// Updates the email address of the current user
    /// </summary>
    /// <param name="request">The request with the email details.</param>
    /// <returns>A response indicating if the change was successful or not</returns>
    [OperationContract]
    ChangeEmailAddressResponse ChangeEmailAddress(ChangeEmailAddressRequest request);

		/// <summary>
		/// Updates the contact details.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    UpdateContactDetailsResponse UpdateContactDetails(UpdateContactDetailsRequest request);

	  /// <summary>
	  /// Gets a job seeker's SSN.
	  /// </summary>
	  /// <param name="request">The request containing the job seeker details.</param>
	  /// <returns>A response containing the SSN</returns>
	  [OperationContract]
	  GetSSNResponse GetSSN(GetSSNRequest request);

		/// <summary>
		/// Updates the SSN.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
	  [OperationContract]
	  UpdateSsnResponse UpdateSsn(UpdateSsnRequest request);

		/// <summary>
		/// Gets the users for look up.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    UserLookupResponse GetUserLookup(UserLookupRequest request);

		/// <summary>
		/// Gets the role.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    RoleResponse GetRole(RoleRequest request);

		/// <summary>
		/// Updates the users roles.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    UpdateUsersRolesResponse UpdateUsersRoles(UpdateUsersRolesRequest request);

    /// <summary>
    /// Activates the user.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    [OperationContract]
    ActivateUserResponse ActivateUser(ActivateUserRequest request);

    /// <summary>
    /// Sends the account activation email.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    [OperationContract]
    SendAccountActivationEmailResponse SendAccountActivationEmail(SendAccountActivationEmailRequest request);

		/// <summary>
		/// Resets the password.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    ResetPasswordResponse ResetPassword(ResetPasswordRequest request);

		/// <summary>
		/// Registers the explorer user.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    RegisterExplorerUserResponse RegisterExplorerUser(RegisterExplorerUserRequest request);

		/// <summary>
		/// Updates the screen name and email address.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    UpdateScreenNameAndEmailAddressResponse UpdateScreenNameAndEmailAddress(UpdateScreenNameAndEmailAddressRequest request);

		/// <summary>
		/// Changes the security question.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    ChangeSecurityQuestionResponse ChangeSecurityQuestion(ChangeSecurityQuestionRequest request);
		
		/// <summary>
		/// Registers the career user.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    RegisterCareerUserResponse RegisterCareerUser(RegisterCareerUserRequest request);

    /// <summary>
    /// Gets the user alert settings.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns>The user alert response</returns>
    [OperationContract]
    GetUserAlertResponse GetUserAlert(GetUserAlertRequest request);

	  /// <summary>
	  /// Updates the contact details.
	  /// </summary>
	  /// <param name="request">The request.</param>
	  /// <returns></returns>
    [OperationContract]
    UpdateUserAlertResponse UpdateUserAlert(UpdateUserAlertRequest request);

	  /// <summary>
	  /// Updates the consent status.
	  /// </summary>
	  /// <param name="request">The request.</param>
	  /// <returns></returns>
    [OperationContract]
    ChangeConsentStatusResponse ChangeConsentStatus(ChangeConsentStatusRequest request);

	  /// <summary>
	  /// Updates the migration status.
	  /// </summary>
	  /// <param name="request">The request.</param>
	  /// <returns></returns>
    [OperationContract]
    ChangeMigratedStatusResponse ChangeMigratedStatus(ChangeMigratedStatusRequest request);

    /// <summary>
    /// Changes the program of study.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    [OperationContract]
    ChangeProgramOfStudyResponse ChangeProgramOfStudy(ChangeProgramOfStudyRequest request);

		/// <summary>
		/// Changes the enrollment status.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
  	[OperationContract]
  	ChangeEnrollmentStatusResponse ChangeEnrollmentStatus(ChangeEnrollmentStatusRequest request);

    /// <summary>
    /// Changes the enrollment status.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    [OperationContract]
    ChangeCampusResponse ChangeCampus(ChangeCampusRequest request);

    /// <summary>
    /// Changes the enrollment status.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    [OperationContract]
    ChangePhoneNumberResponse ChangePhoneNumber(ChangePhoneNumberRequest request);

		/// <summary>
		/// Updates the user.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
  	[OperationContract]
    RegisterCareerUserResponse UpdateUser(RegisterCareerUserRequest request);

		/// <summary>
		/// Validates the email.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		ValidateEmailResponse ValidateEmail(ValidateEmailRequest request);

		/// <summary>
		/// Sends the registration pin email.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
  	[OperationContract]
		SendRegistrationPinEmailResponse SendRegistrationPinEmail(SendRegistrationPinEmailRequest request);

    /// <summary>
    /// Updates the manager.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    [OperationContract]
    UpdateManagerFlagResponse UpdateManagerFlag(UpdateManagerFlagRequest request);

		/// <summary>
		/// Updates the last surveyed date.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
	  [OperationContract]
	  UpdateLastSurveyedDateResponse UpdateLastSurveyedDate(UpdateLastSurveyedDateRequest request);

		/// <summary>
		/// Exports the assist users.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		ExportAssistUsersResponse ExportAssistUsers(ExportAssistUsersRequest request);

    /// <summary>
    /// Imports the job seeker and resumes.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    [OperationContract]
    ImportJobSeekerAndResumesResponse ImportJobSeekerAndResumes(ImportJobSeekerAndResumesRequest request);

    /// <summary>
    /// Imports the employer, business units, employers and if requested the jobs as well.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    [OperationContract]
    ImportEmployerAndArtifactsResponse ImportEmployerAndArtifacts(ImportEmployerAndArtifactsRequest request);

		/// <summary>
		/// Unsubscribes the user from emails.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
	  [OperationContract]
	  UnsubscribeUserFromEmailsResponse UnsubscribeUserFromEmails(UnsubscribeUserFromEmailsRequest request);

		/// <summary>
		/// Inactivates the job seeker.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
	  [OperationContract]
	  InactivateJobSeekerResponse InactivateJobSeeker(InactivateJobSeekerRequest request);

    /// <summary>
    /// Finds the person mobile device.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    [OperationContract]
    FindPersonMobileDeviceResponse FindPersonMobileDevice(FindPersonMobileDeviceRequest request);

    /// <summary>
    /// Creates the person mobile device.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    [OperationContract]
    CreatePersonMobileDeviceResponse CreatePersonMobileDevice(CreatePersonMobileDeviceRequest request);

    /// <summary>
    /// Deletes the person mobile device.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    [OperationContract]
    DeletePersonMobileDeviceResponse DeletePersonMobileDevice(DeletePersonMobileDeviceRequest request);

    /// <summary>
    /// Updates the assist user.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    [OperationContract]
    UpdateAssistUserResponse UpdateAssistUser(UpdateAssistUserRequest request);

		/// <summary>
		/// Updates the career account type
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
	  ChangeCareerAccountTypeResponse ChangeCareerAccountType(ChangeCareerAccountTypeRequest request);
  }
}
