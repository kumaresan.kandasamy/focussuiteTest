﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.ServiceModel;

using Focus.Core;
using Focus.Core.Messages.ReportService;

#endregion

namespace Focus.Services.ServiceContracts
{
  [ServiceContract(Namespace = Constants.ServiceContractNamespace)]
  public interface IReportService
  {
    #region Job Seekers

    /// <summary>
    /// Gets the job seekers.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    [OperationContract]
    JobSeekersReportResponse GetJobSeekers(JobSeekersReportRequest request);

    /// <summary>
    /// Gets a data table job seekers corresponding to the criteria
    /// </summary>
    /// <param name="request">The request containing the corresponding criteria</param>
    /// <returns>A response containing a data table job seekers</returns>
    [OperationContract]
    JobSeekersDataTableResponse GetJobSeekersDataTable(JobSeekersDataTableRequest request);

    /// <summary>
    /// Gets an Excel or PDF file of job seekers for export
    /// </summary>
    /// <param name="request">The request containing criteria or a list of job seekers</param>
    /// <returns>The bytes making up the file</returns>
    [OperationContract]
    JobSeekersExportResponse GetJobSeekersExportBytes(JobSeekersExportRequest request);

    #endregion

    #region Job Orders

    /// <summary>
    /// Gets a list of job orders matching the criteria
    /// </summary>
    /// <param name="request">The request containing the matching criteria</param>
    /// <returns>A response containing matching job orders</returns>
    [OperationContract]
    JobOrdersReportResponse GetJobOrders(JobOrdersReportRequest request);

    /// <summary>
    /// Gets a data table job orders corresponding to the criteria
    /// </summary>
    /// <param name="request">The request containing the corresponding criteria</param>
    /// <returns>A response containing a data table job orders</returns>
    [OperationContract]
    JobOrdersDataTableResponse GetJobOrdersDataTable(JobOrdersDataTableRequest request);

    /// <summary>
    /// Gets an Excel or PDF file of job orders for export
    /// </summary>
    /// <param name="request">The request containing criteria or a list of job orders</param>
    /// <returns>The response containing the bytes making up the file</returns>
    [OperationContract]
    JobOrdersExportResponse GetJobOrdersExportBytes(JobOrdersExportRequest request);

    #endregion

    #region Employers

    /// <summary>
    /// Gets a list of job orders matching the criteria
    /// </summary>
    /// <param name="request">The request containing the matching criteria</param>
    /// <returns>A response containing matching job orders</returns>
    [OperationContract]
    EmployersReportResponse GetEmployers(EmployersReportRequest request);

    /// <summary>
    /// Gets a data table job orders corresponding to the criteria
    /// </summary>
    /// <param name="request">The request containing the corresponding criteria</param>
    /// <returns>A response containing a data table job orders</returns>
    [OperationContract]
    EmployersDataTableResponse GetEmployersDataTable(EmployersDataTableRequest request);

    /// <summary>
    /// Gets an Excel or PDF file of job orders for export
    /// </summary>
    /// <param name="request">The request containing criteria or a list of job orders</param>
    /// <returns>The response containing the bytes making up the file</returns>
    [OperationContract]
    EmployersExportResponse GetEmployersExportBytes(EmployersExportRequest request);

    #endregion

    #region Supply Demand

    /// <summary>
    /// Gets a supply demand view matching the criteria
    /// </summary>
    /// <param name="request">The request containing the matching criteria</param>
    /// <returns>A response containing supply demand view</returns>
    [OperationContract]
    SupplyDemandReportResponse GetSupplyDemand(SupplyDemandReportRequest request);

    /// <summary>
    /// Gets a data table supply demand corresponding to the criteria
    /// </summary>
    /// <param name="request">The request containing the corresponding criteria</param>
    /// <returns>A response containing a data table supply demand</returns>
    [OperationContract]
    SupplyDemandDataTableResponse GetSupplyDemandDataTable(SupplyDemandDataTableRequest request);

    /// <summary>
    /// Gets an Excel or PDF file of supply demand for export
    /// </summary>
    /// <param name="request">The request containing criteria or a list of supply demand</param>
    /// <returns>The response containing the bytes making up the file</returns>
    [OperationContract]
    SupplyDemandExportResponse GetSupplyDemandExportBytes(SupplyDemandExportRequest request);

    #endregion

    /// <summary>
    /// Converts a data table to a PDF or Excel file for export
    /// </summary>
    /// <param name="request">he request containing the data table</param>
    /// <returns>The response containing the bytes making up the file</returns>
    [OperationContract]
    DataTableExportResponse ConvertDataTableToExportBytes(DataTableExportRequest request);

    /// <summary>
    /// Gets the reporting specific look up.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns>The lookup data response</returns>
    [OperationContract]
    LookupReportDataResponse LookUpReportData(LookupReportDataRequest request);

    /// <summary>
    /// Gets the offices
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns>The office data response</returns>
    [OperationContract]
    OfficeLookupResponse OfficeLookup(OfficeLookupRequest request);

    /// <summary>
    /// Saves the report
    /// </summary>
    /// <param name="request">The request holding the report criteria</param>
    /// <returns>The response</returns>
    [OperationContract]
    SaveReportResponse SaveReport(SaveReportRequest request);

    /// <summary>
    /// Gets a saved report
    /// </summary>
    /// <param name="request">The request holding the session key and report type</param>
    /// <returns>The response</returns>
    [OperationContract]
    GetSavedReportResponse GetSavedReport(GetSavedReportRequest request);

    /// <summary>
    /// Gets all saved reports for the use
    /// </summary>
    /// <param name="request">The request</param>
    /// <returns>The response containing the saved report</returns>
    [OperationContract]
    GetSavedReportListResponse GetSavedReportList(GetSavedReportListRequest request);

    /// <summary>
    /// Deletes a report for a user
    /// </summary>
    /// <param name="request">The request containing the report id</param>
    /// <returns>The response</returns>
    [OperationContract]
    DeleteReportResponse DeleteReport(DeleteReportRequest request);

    /// <summary>
    /// Approval queue details for dashboard
    /// </summary>
    /// <param name="request">The request</param>
    /// <returns>The response containing the report</returns>
    [OperationContract]
    ApprovalQueueStatusReportResponse GetApprovalQueueStatusReport(ApprovalQueueStatusReportRequest request);

		/// <summary>
		/// Generates the employer activity report.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
	  [OperationContract]
	  ActivityReportResponse GenerateActivityReport(ActivityReportRequest request);

		#region Statistics

		[OperationContract]
		GetStatisticsResponse GetStatistics(GetStatisticsRequest request);

		#endregion
	}
}
