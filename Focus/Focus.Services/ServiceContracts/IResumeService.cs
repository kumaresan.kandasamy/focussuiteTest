﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.ServiceModel;
using Focus.Core;
using Focus.Core.Messages.CoreService;
using Focus.Core.Messages.JobService;
using Focus.Core.Messages.ResumeService;

#endregion

namespace Focus.Services.ServiceContracts
{
  [ServiceContract(Namespace = Constants.ServiceContractNamespace)]
  public interface IResumeService
	{
    [OperationContract]
    GetResumeResponse GetResume(GetResumeRequest request);

    [OperationContract]
    SaveResumeResponse SaveResume(SaveResumeRequest request);

    [OperationContract]
    SaveResumeDocumentResponse SaveResumeDocument(SaveResumeDocumentRequest request);

    [OperationContract]
    ResumeMetaInfoResponse GetResumeInfo(ResumeMetaInfoRequest request);

    [OperationContract]
    SummaryResponse GetAutomatedSummary(SummaryRequest request);

    [OperationContract]
    DeleteResumeResponse DeleteResume(DeleteResumeRequest request);

    [OperationContract]
    ResumeConvertionResponse GetStructuredResume(ResumeConvertionRequest request);

    [OperationContract]
    ResumeConvertionResponse GetTaggedResume(ResumeConvertionRequest request);

    [OperationContract]
    ResumeJobResponse GetResumeJobs(ResumeJobRequest request);

    [OperationContract]
    DefaultResumeResponse GetDefaultResume(DefaultResumeRequest request);

    [OperationContract]
    DefaultResumeResponse SetDefaultResume(DefaultResumeRequest request);

    [OperationContract]
    HTMLResumeResponse GetHTMLResume(HTMLResumeRequest request);

		[OperationContract]
		ParseResumeResponse ParseResume(ParseResumeRequest request);

    [OperationContract]
    OriginalResumeResponse GetOriginalResume(OriginalResumeRequest request);

		[OperationContract]
  	ResumeViewCountResponse GetResumeViewCount(ResumeViewCountRequest request);

    [OperationContract]
    RegisterResumeResponse RegisterDefaultResume(RegisterResumeRequest request);

    [OperationContract]
    GetResumeTemplateResponse GetResumeTemplate(GetResumeTemplateRequest request);
	}
}
