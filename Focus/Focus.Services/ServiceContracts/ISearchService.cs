﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.ServiceModel;
using Focus.Core;
using Focus.Core.Messages.SearchService;

#endregion

namespace Focus.Services.ServiceContracts
{
  [ServiceContract(Namespace = Constants.ServiceContractNamespace)]
  public interface ISearchService
	{
		/// <summary>
		/// Searches the candidates.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    CandidateSearchResponse SearchCandidates(CandidateSearchRequest request);

		/// <summary>
		/// Saves the candidate search.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    SaveCandidateSavedSearchResponse SaveCandidateSearch(SaveCandidateSavedSearchRequest request);

		/// <summary>
		/// Gets the candidate saved search.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    GetCandidateSavedSearchResponse GetCandidateSavedSearch(GetCandidateSavedSearchRequest request);

		/// <summary>
		/// Gets the candidate saved search view.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    GetCandidateSavedSearchViewResponse GetCandidateSavedSearchView(GetCandidateSavedSearchViewRequest request);

    /// <summary>
    /// Deletes the saved search.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    [OperationContract]
    DeleteSavedSearchResponse DeleteSavedSearch(DeleteSavedSearchRequest request);

		/// <summary>
		/// Gets the clarified words.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <param name="minScore">The min score.</param>
		/// <param name="maxSearchCount">The max search count.</param>
		/// <param name="type">The type.</param>
		/// <returns></returns>
    [OperationContract]
    ClarifyWordResponse GetClarifiedWords(ClarifyWordRequest request, int minScore, int maxSearchCount, string type);

		/// <summary>
		/// Converts the binary data.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    ConvertBinaryDataResponse ConvertBinaryDataToText(ConvertBinaryDataRequest request);

		/// <summary>
		/// Gets the onets.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    OnetResponse GetOnets(OnetRequest request);

    /// <summary>
		/// Gets the onet.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    OnetResponse GetOnet(OnetRequest request);

    /// <summary>
    /// Gets the ronet.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    [OperationContract]
    ROnetResponse GetROnet(ROnetRequest request);

    /// <summary>
    /// Gets the R onet code by id.
    /// </summary>
    /// <param name="ronetId">The ronet id.</param>
    /// <returns></returns>
    [OperationContract]
    string GetROnetCodeById(long ronetId);

    /// <summary>
    /// Gets the ronets.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    [OperationContract]
    ROnetResponse GetROnets(ROnetRequest request);

		/// <summary>
		/// Lens search.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    LensSearchResponse LensSearch(LensSearchRequest request);

		/// <summary>
		/// Gets the onet keywords.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    OnetKeywordsResponse GetOnetKeywords(OnetKeywordsRequest request);

		/// <summary>
		/// Gets the onet tasks.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    OnetTaskResponse GetOnetTasks(OnetTaskRequest request);

	  /// <summary>
	  /// Gets the resume search session.
	  /// </summary>
	  /// <param name="request">The request.</param>
	  /// <returns></returns>
	  [OperationContract]
	  GetResumeSearchSessionResponse GetResumeSearchSession(GetResumeSearchSessionRequest request);
	}
}
