﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.ServiceModel;

using Focus.Core;
using Focus.Core.Messages.EmployeeService;
using Focus.Core.Messages.EmployerService;

#endregion

namespace Focus.Services.ServiceContracts
{
  [ServiceContract(Namespace = Constants.ServiceContractNamespace)]
  public interface IEmployeeService
	{
		/// <summary>
		/// Gets the employee.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    EmployeeResponse GetEmployee(EmployeeRequest request);

		/// <summary>
		/// Gets the employees for same employer.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    EmployeeResponse GetEmployeesForSameEmployer(EmployeeRequest request);

    /// <summary>
    /// Gets the employees for a list of employers.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <returns></returns>
    [OperationContract]
    EmployeesForEmployerResponse GetEmployeesForEmployers(EmployeesForEmployerRequest request);

		/// <summary>
		/// Searches the specified request.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    EmployeeSearchResponse Search(EmployeeSearchRequest request);

		/// <summary>
		/// email to employers.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    EmailEmployeeResponse EmailEmployee(EmailEmployeeRequest request);
      
      /// <summary>
      /// email to list of employers
      /// </summary>
      /// <param name="request">The request</param>
      /// <returns></returns>
    [OperationContract]
    EmailEmployeesResponse EmailEmployees(EmailEmployeesRequest request);

    /// <summary>
		/// Gets the employer account referral view.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    EmployerAccountReferralViewResponse GetEmployerAccountReferralView(EmployerAccountReferralViewRequest request);

		/// <summary>
		/// Approves the referral.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    EmployeeResponse ApproveReferral(EmployeeRequest request);

		/// <summary>
		/// Gets the hiring areas.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    EmployeeResponse GetHiringAreas(EmployeeRequest request);

		/// <summary>
		/// Gets the hiring manager profile.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    EmployeeBusinessUnitInfoResponse GetHiringManagerProfile(EmployeeBusinessUnitInfoRequest request);

		/// <summary>
		/// Denies the referral.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    EmployeeResponse DenyReferral(EmployeeRequest request);

		/// <summary>
		/// Holds the referral.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    EmployeeResponse HoldReferral(EmployeeRequest request);

		/// <summary>
		/// Assigns the activity.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    AssignEmployeeActivityResponse AssignActivity(AssignEmployeeActivityRequest request);

		/// <summary>
		/// Assigns the activity to multiple employees.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
	  [OperationContract]
	  AssignEmployeeActivityResponse AssignActivityToMultipleEmployees(AssignEmployeeActivityRequest request);

		/// <summary>
		/// Blocks the account.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    EmployeeResponse BlockAccount(EmployeeRequest request);

		/// <summary>
		/// Unblocks the account.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    EmployeeResponse UnblockAccount(EmployeeRequest request);

		/// <summary>
		/// Gets the employee user details.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    EmployeeUserDetailsResponse GetEmployeeUserDetails(EmployeeUserDetailsRequest request);

		/// <summary>
		/// Gets the employee business unit view.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    EmployeeBusinessUnitViewResponse GetEmployeeBusinessUnitView(EmployeeBusinessUnitViewRequest request);

		/// <summary>
		/// Assigns the employee to business unit.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    AssignEmployeeToBusinessUnitResponse AssignEmployeeToBusinessUnit(AssignEmployeeToBusinessUnitRequest request);

		/// <summary>
		/// Sets the business unit as default.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    SetBusinessUnitAsDefaultResponse SetBusinessUnitAsDefault(SetBusinessUnitAsDefaultRequest request);

		/// <summary>
		/// Gets the hiring manager activity view.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    EmployeeBusinessUnitActivityResponse GetHiringManagerActivityView(EmployeeBusinessUnitActivityRequest request);

		/// <summary>
		/// Gets the employee business unit activity users.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
    [OperationContract]
    EmployeeBusinessUnitActivityResponse GetEmployeeBusinessUnitActivityUsers(EmployeeBusinessUnitActivityRequest request);

		/// <summary>
		/// Gets the employee by person.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
		[OperationContract]
		EmployeeResponse GetEmployeeByPerson(EmployeeRequest request);

		/// <summary>
		/// Creates the hiring manager homepage alert.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <returns></returns>
	  [OperationContract]
	  CreateHiringManagerHomepageAlertResponse CreateHiringManagerHomepageAlert(CreateHiringManagerHomepageAlertRequest request);
	}
}
