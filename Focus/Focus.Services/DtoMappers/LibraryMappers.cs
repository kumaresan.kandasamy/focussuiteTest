﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Linq;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.DataTransferObjects.FocusExplorer;
using Focus.Data.Library.Entities;
using EmployerDto = Focus.Core.DataTransferObjects.FocusExplorer.EmployerDto;
using JobDto = Focus.Core.DataTransferObjects.FocusExplorer.JobDto;
using JobSkillDto = Focus.Core.DataTransferObjects.FocusExplorer.JobSkillDto;
using LocalisationDto = Focus.Core.DataTransferObjects.FocusExplorer.LocalisationDto;
using SkillDto = Focus.Core.DataTransferObjects.FocusExplorer.SkillDto;

#endregion

namespace Focus.Services.DtoMappers
{
  public static class LibraryMappers
  {

    #region LocalisationDto Mappers

    public static LocalisationDto AsDto(this Localisation entity)
    {
      var dto = new LocalisationDto
      {
        Culture = entity.Culture,
        Id = entity.Id
      };
      return dto;
    }

    public static Localisation CopyTo(this LocalisationDto dto, Localisation entity)
    {
      entity.Culture = dto.Culture;
      return entity;
    }

    public static Localisation CopyTo(this LocalisationDto dto)
    {
      var entity = new Localisation();
      return dto.CopyTo(entity);
    }

    #endregion

    #region StateDto Mappers

    public static StateDto AsDto(this State entity)
    {
      var dto = new StateDto
      {
        CountryId = entity.CountryId,
        Name = entity.Name,
        Code = entity.Code,
        SortOrder = entity.SortOrder,
        Id = entity.Id
      };
      return dto;
    }

    public static State CopyTo(this StateDto dto, State entity)
    {
      entity.CountryId = dto.CountryId;
      entity.Name = dto.Name;
      entity.Code = dto.Code;
      entity.SortOrder = dto.SortOrder;
      return entity;
    }

    public static State CopyTo(this StateDto dto)
    {
      var entity = new State();
      return dto.CopyTo(entity);
    }

    #endregion

    #region AreaDto Mappers

    public static AreaDto AsDto(this Area entity)
    {
      var dto = new AreaDto
      {
        Name = entity.Name,
        SortOrder = entity.SortOrder,
        Id = entity.Id
      };
      return dto;
    }

    public static Area CopyTo(this AreaDto dto, Area entity)
    {
      entity.Name = dto.Name;
      entity.SortOrder = dto.SortOrder;
      return entity;
    }

    public static Area CopyTo(this AreaDto dto)
    {
      var entity = new Area();
      return dto.CopyTo(entity);
    }

    #endregion

    #region JobReportDto Mappers

    public static JobReportDto AsDto(this JobReport entity)
    {
      var dto = new JobReportDto
      {
        HiringDemand = entity.HiringDemand,
        SalaryTrend = entity.SalaryTrend,
        SalaryTrendPercentile = entity.SalaryTrendPercentile,
        SalaryTrendAverage = entity.SalaryTrendAverage,
        LocalisationId = entity.LocalisationId,
        GrowthTrend = entity.GrowthTrend,
        GrowthPercentile = entity.GrowthPercentile,
        HiringTrend = entity.HiringTrend,
        HiringTrendPercentile = entity.HiringTrendPercentile,
        SalaryTrendMin = entity.SalaryTrendMin,
        SalaryTrendMax = entity.SalaryTrendMax,
        SalaryTrendRealtime = entity.SalaryTrendRealtime,
        SalaryTrendRealtimeAverage = entity.SalaryTrendRealtimeAverage,
        SalaryTrendRealtimeMin = entity.SalaryTrendRealtimeMin,
        SalaryTrendRealtimeMax = entity.SalaryTrendRealtimeMax,
        HiringTrendSubLevel = entity.HiringTrendSubLevel,
        JobId = entity.JobId,
        StateAreaId = entity.StateAreaId,
        Id = entity.Id
      };
      return dto;
    }

    public static JobReport CopyTo(this JobReportDto dto, JobReport entity)
    {
      entity.HiringDemand = dto.HiringDemand;
      entity.SalaryTrend = dto.SalaryTrend;
      entity.SalaryTrendPercentile = dto.SalaryTrendPercentile;
      entity.SalaryTrendAverage = dto.SalaryTrendAverage;
      entity.LocalisationId = dto.LocalisationId;
      entity.GrowthTrend = dto.GrowthTrend;
      entity.GrowthPercentile = dto.GrowthPercentile;
      entity.HiringTrend = dto.HiringTrend;
      entity.HiringTrendPercentile = dto.HiringTrendPercentile;
      entity.SalaryTrendMin = dto.SalaryTrendMin;
      entity.SalaryTrendMax = dto.SalaryTrendMax;
      entity.SalaryTrendRealtime = dto.SalaryTrendRealtime;
      entity.SalaryTrendRealtimeAverage = dto.SalaryTrendRealtimeAverage;
      entity.SalaryTrendRealtimeMin = dto.SalaryTrendRealtimeMin;
      entity.SalaryTrendRealtimeMax = dto.SalaryTrendRealtimeMax;
      entity.HiringTrendSubLevel = dto.HiringTrendSubLevel;
      entity.JobId = dto.JobId;
      entity.StateAreaId = dto.StateAreaId;
      return entity;
    }

    public static JobReport CopyTo(this JobReportDto dto)
    {
      var entity = new JobReport();
      return dto.CopyTo(entity);
    }

    #endregion

    #region JobDto Mappers

    public static JobDto AsDto(this Job entity)
    {
      var dto = new JobDto
      {
        Name = entity.Name,
        ROnet = entity.ROnet,
        LocalisationId = entity.LocalisationId,
        DegreeIntroStatement = entity.DegreeIntroStatement,
        Description = entity.Description,
        StarterJob = entity.StarterJob,
        Id = entity.Id
      };
      return dto;
    }

    public static Job CopyTo(this JobDto dto, Job entity)
    {
      entity.Name = dto.Name;
      entity.ROnet = dto.ROnet;
      entity.LocalisationId = dto.LocalisationId;
      entity.DegreeIntroStatement = dto.DegreeIntroStatement;
      entity.Description = dto.Description;
      entity.StarterJob = dto.StarterJob;
      return entity;
    }

    public static Job CopyTo(this JobDto dto)
    {
      var entity = new Job();
      return dto.CopyTo(entity);
    }

    #endregion

    #region StudyPlaceDto Mappers

    public static StudyPlaceDto AsDto(this StudyPlace entity)
    {
      var dto = new StudyPlaceDto
      {
        Name = entity.Name,
        StateAreaId = entity.StateAreaId,
        Id = entity.Id
      };
      return dto;
    }

    public static StudyPlace CopyTo(this StudyPlaceDto dto, StudyPlace entity)
    {
      entity.Name = dto.Name;
      entity.StateAreaId = dto.StateAreaId;
      return entity;
    }

    public static StudyPlace CopyTo(this StudyPlaceDto dto)
    {
      var entity = new StudyPlace();
      return dto.CopyTo(entity);
    }

    #endregion

    #region EmployerDto Mappers

    public static EmployerDto AsDto(this Employer entity)
    {
      var dto = new EmployerDto
      {
        Name = entity.Name,
        Id = entity.Id
      };
      return dto;
    }

    public static Employer CopyTo(this EmployerDto dto, Employer entity)
    {
      entity.Name = dto.Name;
      return entity;
    }

    public static Employer CopyTo(this EmployerDto dto)
    {
      var entity = new Employer();
      return dto.CopyTo(entity);
    }

    #endregion

    #region EmployerJobDto Mappers

    public static EmployerJobDto AsDto(this EmployerJob entity)
    {
      var dto = new EmployerJobDto
      {
        Positions = entity.Positions,
        JobId = entity.JobId,
        EmployerId = entity.EmployerId,
        StateAreaId = entity.StateAreaId,
        Id = entity.Id
      };
      return dto;
    }

    public static EmployerJob CopyTo(this EmployerJobDto dto, EmployerJob entity)
    {
      entity.Positions = dto.Positions;
      entity.JobId = dto.JobId;
      entity.EmployerId = dto.EmployerId;
      entity.StateAreaId = dto.StateAreaId;
      return entity;
    }

    public static EmployerJob CopyTo(this EmployerJobDto dto)
    {
      var entity = new EmployerJob();
      return dto.CopyTo(entity);
    }

    #endregion

    #region EmployerJobViewDto Mappers

    public static EmployerJobViewDto AsDto(this EmployerJobView entity)
    {
      var dto = new EmployerJobViewDto
      {
        EmployerName = entity.EmployerName,
        JobId = entity.JobId,
        JobName = entity.JobName,
        Positions = entity.Positions,
        EmployerId = entity.EmployerId,
        StateAreaId = entity.StateAreaId,
        ROnet = entity.ROnet,
        StarterJob = entity.StarterJob,
        Id = entity.Id
      };
      return dto;
    }

    public static EmployerJobView CopyTo(this EmployerJobViewDto dto, EmployerJobView entity)
    {
      entity.EmployerName = dto.EmployerName;
      entity.JobId = dto.JobId;
      entity.JobName = dto.JobName;
      entity.Positions = dto.Positions;
      entity.EmployerId = dto.EmployerId;
      entity.StateAreaId = dto.StateAreaId;
      entity.ROnet = dto.ROnet;
      entity.StarterJob = dto.StarterJob;
      return entity;
    }

    public static EmployerJobView CopyTo(this EmployerJobViewDto dto)
    {
      var entity = new EmployerJobView();
      return dto.CopyTo(entity);
    }

    #endregion

    #region JobSkillViewDto Mappers

    public static JobSkillViewDto AsDto(this JobSkillView entity)
    {
      var dto = new JobSkillViewDto
      {
        JobId = entity.JobId,
        JobSkillType = entity.JobSkillType,
        SkillType = entity.SkillType,
        SkillId = entity.SkillId,
        DemandPercentile = entity.DemandPercentile,
        SkillName = entity.SkillName,
        Rank = entity.Rank,
        Id = entity.Id
      };
      return dto;
    }

    public static JobSkillView CopyTo(this JobSkillViewDto dto, JobSkillView entity)
    {
      entity.JobId = dto.JobId;
      entity.JobSkillType = dto.JobSkillType;
      entity.SkillType = dto.SkillType;
      entity.SkillId = dto.SkillId;
      entity.DemandPercentile = dto.DemandPercentile;
      entity.SkillName = dto.SkillName;
      entity.Rank = dto.Rank;
      return entity;
    }

    public static JobSkillView CopyTo(this JobSkillViewDto dto)
    {
      var entity = new JobSkillView();
      return dto.CopyTo(entity);
    }

    #endregion

    #region SkillCategoryDto Mappers

    public static SkillCategoryDto AsDto(this SkillCategory entity)
    {
      var dto = new SkillCategoryDto
      {
        Name = entity.Name,
        LocalisationId = entity.LocalisationId,
        ParentId = entity.ParentId,
        Id = entity.Id
      };
      return dto;
    }

    public static SkillCategory CopyTo(this SkillCategoryDto dto, SkillCategory entity)
    {
      entity.Name = dto.Name;
      entity.LocalisationId = dto.LocalisationId;
      entity.ParentId = dto.ParentId;
      return entity;
    }

    public static SkillCategory CopyTo(this SkillCategoryDto dto)
    {
      var entity = new SkillCategory();
      return dto.CopyTo(entity);
    }

    #endregion

    #region SkillCategorySkillDto Mappers

    public static SkillCategorySkillDto AsDto(this SkillCategorySkill entity)
    {
      var dto = new SkillCategorySkillDto
      {
        SkillCategoryId = entity.SkillCategoryId,
        SkillId = entity.SkillId,
        Id = entity.Id
      };
      return dto;
    }

    public static SkillCategorySkill CopyTo(this SkillCategorySkillDto dto, SkillCategorySkill entity)
    {
      entity.SkillCategoryId = dto.SkillCategoryId;
      entity.SkillId = dto.SkillId;
      return entity;
    }

    public static SkillCategorySkill CopyTo(this SkillCategorySkillDto dto)
    {
      var entity = new SkillCategorySkill();
      return dto.CopyTo(entity);
    }

    #endregion

    #region CareerAreaDto Mappers

    public static CareerAreaDto AsDto(this CareerArea entity)
    {
      var dto = new CareerAreaDto
      {
        Name = entity.Name,
        LocalisationId = entity.LocalisationId,
        Id = entity.Id
      };
      return dto;
    }

    public static CareerArea CopyTo(this CareerAreaDto dto, CareerArea entity)
    {
      entity.Name = dto.Name;
      entity.LocalisationId = dto.LocalisationId;
      return entity;
    }

    public static CareerArea CopyTo(this CareerAreaDto dto)
    {
      var entity = new CareerArea();
      return dto.CopyTo(entity);
    }

    #endregion

    #region JobCareerAreaDto Mappers

    public static JobCareerAreaDto AsDto(this JobCareerArea entity)
    {
      var dto = new JobCareerAreaDto
      {
        JobId = entity.JobId,
        CareerAreaId = entity.CareerAreaId,
        Id = entity.Id
      };
      return dto;
    }

    public static JobCareerArea CopyTo(this JobCareerAreaDto dto, JobCareerArea entity)
    {
      entity.JobId = dto.JobId;
      entity.CareerAreaId = dto.CareerAreaId;
      return entity;
    }

    public static JobCareerArea CopyTo(this JobCareerAreaDto dto)
    {
      var entity = new JobCareerArea();
      return dto.CopyTo(entity);
    }

    #endregion

    #region JobSkillDto Mappers

    public static JobSkillDto AsDto(this JobSkill entity)
    {
      var dto = new JobSkillDto
      {
        JobSkillType = entity.JobSkillType,
        DemandPercentile = entity.DemandPercentile,
        Rank = entity.Rank,
        JobId = entity.JobId,
        SkillId = entity.SkillId,
        Id = entity.Id
      };
      return dto;
    }

    public static JobSkill CopyTo(this JobSkillDto dto, JobSkill entity)
    {
      entity.JobSkillType = dto.JobSkillType;
      entity.DemandPercentile = dto.DemandPercentile;
      entity.Rank = dto.Rank;
      entity.JobId = dto.JobId;
      entity.SkillId = dto.SkillId;
      return entity;
    }

    public static JobSkill CopyTo(this JobSkillDto dto)
    {
      var entity = new JobSkill();
      return dto.CopyTo(entity);
    }

    #endregion

    #region SkillDto Mappers

    public static SkillDto AsDto(this Skill entity)
    {
      var dto = new SkillDto
      {
        SkillType = entity.SkillType,
        Name = entity.Name,
        LocalisationId = entity.LocalisationId,
        Id = entity.Id
      };
      return dto;
    }

    public static Skill CopyTo(this SkillDto dto, Skill entity)
    {
      entity.SkillType = dto.SkillType;
      entity.Name = dto.Name;
      entity.LocalisationId = dto.LocalisationId;
      return entity;
    }

    public static Skill CopyTo(this SkillDto dto)
    {
      var entity = new Skill();
      return dto.CopyTo(entity);
    }

    #endregion

    #region JobRelatedJobDto Mappers

    public static JobRelatedJobDto AsDto(this JobRelatedJob entity)
    {
      var dto = new JobRelatedJobDto
      {
        RelatedJobId = entity.RelatedJobId,
        Priority = entity.Priority,
        JobId = entity.JobId,
        Id = entity.Id
      };
      return dto;
    }

    public static JobRelatedJob CopyTo(this JobRelatedJobDto dto, JobRelatedJob entity)
    {
      entity.RelatedJobId = dto.RelatedJobId;
      entity.Priority = dto.Priority;
      entity.JobId = dto.JobId;
      return entity;
    }

    public static JobRelatedJob CopyTo(this JobRelatedJobDto dto)
    {
      var entity = new JobRelatedJob();
      return dto.CopyTo(entity);
    }

    #endregion

    #region StateAreaDto Mappers

    public static StateAreaDto AsDto(this StateArea entity)
    {
      var dto = new StateAreaDto
      {
        AreaCode = entity.AreaCode,
        Display = entity.Display,
        IsDefault = entity.IsDefault,
        StateId = entity.StateId,
        AreaId = entity.AreaId,
        Id = entity.Id
      };
      return dto;
    }

    public static StateArea CopyTo(this StateAreaDto dto, StateArea entity)
    {
      entity.AreaCode = dto.AreaCode;
      entity.Display = dto.Display;
      entity.IsDefault = dto.IsDefault;
      entity.StateId = dto.StateId;
      entity.AreaId = dto.AreaId;
      return entity;
    }

    public static StateArea CopyTo(this StateAreaDto dto)
    {
      var entity = new StateArea();
      return dto.CopyTo(entity);
    }

    #endregion

    #region JobReportViewDto Mappers

    public static JobReportViewDto AsDto(this JobReportView entity)
    {
      var dto = new JobReportViewDto
      {
        HiringDemand = entity.HiringDemand,
        GrowthTrend = entity.GrowthTrend,
        GrowthPercentile = entity.GrowthPercentile,
        SalaryTrend = entity.SalaryTrend,
        SalaryTrendPercentile = entity.SalaryTrendPercentile,
        SalaryTrendAverage = entity.SalaryTrendAverage,
        JobId = entity.JobId,
        Name = entity.Name,
        HiringTrend = entity.HiringTrend,
        HiringTrendPercentile = entity.HiringTrendPercentile,
        SalaryTrendMin = entity.SalaryTrendMin,
        SalaryTrendMax = entity.SalaryTrendMax,
        SalaryTrendRealtime = entity.SalaryTrendRealtime,
        SalaryTrendRealtimeAverage = entity.SalaryTrendRealtimeAverage,
        SalaryTrendRealtimeMin = entity.SalaryTrendRealtimeMin,
        SalaryTrendRealtimeMax = entity.SalaryTrendRealtimeMax,
        StateAreaId = entity.StateAreaId,
        DegreeIntroStatement = entity.DegreeIntroStatement,
        ROnet = entity.ROnet,
        StarterJob = entity.StarterJob,
        HiringTrendSubLevel = entity.HiringTrendSubLevel,
        Id = entity.Id
      };
      return dto;
    }

    public static JobReportView CopyTo(this JobReportViewDto dto, JobReportView entity)
    {
      entity.HiringDemand = dto.HiringDemand;
      entity.GrowthTrend = dto.GrowthTrend;
      entity.GrowthPercentile = dto.GrowthPercentile;
      entity.SalaryTrend = dto.SalaryTrend;
      entity.SalaryTrendPercentile = dto.SalaryTrendPercentile;
      entity.SalaryTrendAverage = dto.SalaryTrendAverage;
      entity.JobId = dto.JobId;
      entity.Name = dto.Name;
      entity.HiringTrend = dto.HiringTrend;
      entity.HiringTrendPercentile = dto.HiringTrendPercentile;
      entity.SalaryTrendMin = dto.SalaryTrendMin;
      entity.SalaryTrendMax = dto.SalaryTrendMax;
      entity.SalaryTrendRealtime = dto.SalaryTrendRealtime;
      entity.SalaryTrendRealtimeAverage = dto.SalaryTrendRealtimeAverage;
      entity.SalaryTrendRealtimeMin = dto.SalaryTrendRealtimeMin;
      entity.SalaryTrendRealtimeMax = dto.SalaryTrendRealtimeMax;
      entity.StateAreaId = dto.StateAreaId;
      entity.DegreeIntroStatement = dto.DegreeIntroStatement;
      entity.ROnet = dto.ROnet;
      entity.StarterJob = dto.StarterJob;
      entity.HiringTrendSubLevel = dto.HiringTrendSubLevel;
      return entity;
    }

    public static JobReportView CopyTo(this JobReportViewDto dto)
    {
      var entity = new JobReportView();
      return dto.CopyTo(entity);
    }

    #endregion

    #region JobDegreeLevelDto Mappers

    public static JobDegreeLevelDto AsDto(this JobDegreeLevel entity)
    {
      var dto = new JobDegreeLevelDto
      {
        DegreeLevel = entity.DegreeLevel,
        JobId = entity.JobId,
        Id = entity.Id
      };
      return dto;
    }

    public static JobDegreeLevel CopyTo(this JobDegreeLevelDto dto, JobDegreeLevel entity)
    {
      entity.DegreeLevel = dto.DegreeLevel;
      entity.JobId = dto.JobId;
      return entity;
    }

    public static JobDegreeLevel CopyTo(this JobDegreeLevelDto dto)
    {
      var entity = new JobDegreeLevel();
      return dto.CopyTo(entity);
    }

    #endregion

    #region SkillCategorySkillViewDto Mappers

    public static SkillCategorySkillViewDto AsDto(this SkillCategorySkillView entity)
    {
      var dto = new SkillCategorySkillViewDto
      {
        SkillCategoryId = entity.SkillCategoryId,
        SkillCategoryName = entity.SkillCategoryName,
        SkillId = entity.SkillId,
        SkillName = entity.SkillName,
        SkillCategoryParentId = entity.SkillCategoryParentId,
        SkillType = entity.SkillType,
        Id = entity.Id
      };
      return dto;
    }

    public static SkillCategorySkillView CopyTo(this SkillCategorySkillViewDto dto, SkillCategorySkillView entity)
    {
      entity.SkillCategoryId = dto.SkillCategoryId;
      entity.SkillCategoryName = dto.SkillCategoryName;
      entity.SkillId = dto.SkillId;
      entity.SkillName = dto.SkillName;
      entity.SkillCategoryParentId = dto.SkillCategoryParentId;
      entity.SkillType = dto.SkillType;
      return entity;
    }

    public static SkillCategorySkillView CopyTo(this SkillCategorySkillViewDto dto)
    {
      var entity = new SkillCategorySkillView();
      return dto.CopyTo(entity);
    }

    #endregion

    #region JobJobTitleDto Mappers

    public static JobJobTitleDto AsDto(this JobJobTitle entity)
    {
      var dto = new JobJobTitleDto
      {
        DemandPercentile = entity.DemandPercentile,
        JobId = entity.JobId,
        JobTitleId = entity.JobTitleId,
        Id = entity.Id
      };
      return dto;
    }

    public static JobJobTitle CopyTo(this JobJobTitleDto dto, JobJobTitle entity)
    {
      entity.DemandPercentile = dto.DemandPercentile;
      entity.JobId = dto.JobId;
      entity.JobTitleId = dto.JobTitleId;
      return entity;
    }

    public static JobJobTitle CopyTo(this JobJobTitleDto dto)
    {
      var entity = new JobJobTitle();
      return dto.CopyTo(entity);
    }

    #endregion

    #region JobTitleDto Mappers

    public static JobTitleDto AsDto(this JobTitle entity)
    {
      var dto = new JobTitleDto
      {
        Name = entity.Name,
        Id = entity.Id
      };
      return dto;
    }

    public static JobTitle CopyTo(this JobTitleDto dto, JobTitle entity)
    {
      entity.Name = dto.Name;
      return entity;
    }

    public static JobTitle CopyTo(this JobTitleDto dto)
    {
      var entity = new JobTitle();
      return dto.CopyTo(entity);
    }

    #endregion

    #region JobEducationRequirementDto Mappers

    public static JobEducationRequirementDto AsDto(this JobEducationRequirement entity)
    {
      var dto = new JobEducationRequirementDto
      {
        EducationRequirementType = entity.EducationRequirementType,
        RequirementPercentile = entity.RequirementPercentile,
        JobId = entity.JobId,
        Id = entity.Id
      };
      return dto;
    }

    public static JobEducationRequirement CopyTo(this JobEducationRequirementDto dto, JobEducationRequirement entity)
    {
      entity.EducationRequirementType = dto.EducationRequirementType;
      entity.RequirementPercentile = dto.RequirementPercentile;
      entity.JobId = dto.JobId;
      return entity;
    }

    public static JobEducationRequirement CopyTo(this JobEducationRequirementDto dto)
    {
      var entity = new JobEducationRequirement();
      return dto.CopyTo(entity);
    }

    #endregion

    #region JobExperienceLevelDto Mappers

    public static JobExperienceLevelDto AsDto(this JobExperienceLevel entity)
    {
      var dto = new JobExperienceLevelDto
      {
        ExperienceLevel = entity.ExperienceLevel,
        ExperiencePercentile = entity.ExperiencePercentile,
        JobId = entity.JobId,
        Id = entity.Id
      };
      return dto;
    }

    public static JobExperienceLevel CopyTo(this JobExperienceLevelDto dto, JobExperienceLevel entity)
    {
      entity.ExperienceLevel = dto.ExperienceLevel;
      entity.ExperiencePercentile = dto.ExperiencePercentile;
      entity.JobId = dto.JobId;
      return entity;
    }

    public static JobExperienceLevel CopyTo(this JobExperienceLevelDto dto)
    {
      var entity = new JobExperienceLevel();
      return dto.CopyTo(entity);
    }

    #endregion

    #region EmployerSkillViewDto Mappers

    public static EmployerSkillViewDto AsDto(this EmployerSkillView entity)
    {
      var dto = new EmployerSkillViewDto
      {
        SkillId = entity.SkillId,
        SkillType = entity.SkillType,
        SkillName = entity.SkillName,
        EmployerId = entity.EmployerId,
        Postings = entity.Postings,
        Id = entity.Id
      };
      return dto;
    }

    public static EmployerSkillView CopyTo(this EmployerSkillViewDto dto, EmployerSkillView entity)
    {
      entity.SkillId = dto.SkillId;
      entity.SkillType = dto.SkillType;
      entity.SkillName = dto.SkillName;
      entity.EmployerId = dto.EmployerId;
      entity.Postings = dto.Postings;
      return entity;
    }

    public static EmployerSkillView CopyTo(this EmployerSkillViewDto dto)
    {
      var entity = new EmployerSkillView();
      return dto.CopyTo(entity);
    }

    #endregion

    #region SkillReportViewDto Mappers

    public static SkillReportViewDto AsDto(this SkillReportView entity)
    {
      var dto = new SkillReportViewDto
      {
        SkillType = entity.SkillType,
        SkillName = entity.SkillName,
        SkillId = entity.SkillId,
        Frequency = entity.Frequency,
        StateAreaId = entity.StateAreaId,
        Id = entity.Id
      };
      return dto;
    }

    public static SkillReportView CopyTo(this SkillReportViewDto dto, SkillReportView entity)
    {
      entity.SkillType = dto.SkillType;
      entity.SkillName = dto.SkillName;
      entity.SkillId = dto.SkillId;
      entity.Frequency = dto.Frequency;
      entity.StateAreaId = dto.StateAreaId;
      return entity;
    }

    public static SkillReportView CopyTo(this SkillReportViewDto dto)
    {
      var entity = new SkillReportView();
      return dto.CopyTo(entity);
    }

    #endregion

    #region EmployerSkillDto Mappers

    public static EmployerSkillDto AsDto(this EmployerSkill entity)
    {
      var dto = new EmployerSkillDto
      {
        Postings = entity.Postings,
        EmployerId = entity.EmployerId,
        SkillId = entity.SkillId,
        Id = entity.Id
      };
      return dto;
    }

    public static EmployerSkill CopyTo(this EmployerSkillDto dto, EmployerSkill entity)
    {
      entity.Postings = dto.Postings;
      entity.EmployerId = dto.EmployerId;
      entity.SkillId = dto.SkillId;
      return entity;
    }

    public static EmployerSkill CopyTo(this EmployerSkillDto dto)
    {
      var entity = new EmployerSkill();
      return dto.CopyTo(entity);
    }

    #endregion

    #region SkillReportDto Mappers

    public static SkillReportDto AsDto(this SkillReport entity)
    {
      var dto = new SkillReportDto
      {
        Frequency = entity.Frequency,
        SkillId = entity.SkillId,
        StateAreaId = entity.StateAreaId,
        Id = entity.Id
      };
      return dto;
    }

    public static SkillReport CopyTo(this SkillReportDto dto, SkillReport entity)
    {
      entity.Frequency = dto.Frequency;
      entity.SkillId = dto.SkillId;
      entity.StateAreaId = dto.StateAreaId;
      return entity;
    }

    public static SkillReport CopyTo(this SkillReportDto dto)
    {
      var entity = new SkillReport();
      return dto.CopyTo(entity);
    }

    #endregion

    #region JobCareerAreaSkillViewDto Mappers

    public static JobCareerAreaSkillViewDto AsDto(this JobCareerAreaSkillView entity)
    {
      var dto = new JobCareerAreaSkillViewDto
      {
        SkillId = entity.SkillId,
        Id = entity.Id
      };
      return dto;
    }

    public static JobCareerAreaSkillView CopyTo(this JobCareerAreaSkillViewDto dto, JobCareerAreaSkillView entity)
    {
      entity.SkillId = dto.SkillId;
      return entity;
    }

    public static JobCareerAreaSkillView CopyTo(this JobCareerAreaSkillViewDto dto)
    {
      var entity = new JobCareerAreaSkillView();
      return dto.CopyTo(entity);
    }

    #endregion

    #region JobDegreeLevelSkillViewDto Mappers

    public static JobDegreeLevelSkillViewDto AsDto(this JobDegreeLevelSkillView entity)
    {
      var dto = new JobDegreeLevelSkillViewDto
      {
        SkillId = entity.SkillId,
        DegreeLevel = entity.DegreeLevel,
        Id = entity.Id
      };
      return dto;
    }

    public static JobDegreeLevelSkillView CopyTo(this JobDegreeLevelSkillViewDto dto, JobDegreeLevelSkillView entity)
    {
      entity.SkillId = dto.SkillId;
      entity.DegreeLevel = dto.DegreeLevel;
      return entity;
    }

    public static JobDegreeLevelSkillView CopyTo(this JobDegreeLevelSkillViewDto dto)
    {
      var entity = new JobDegreeLevelSkillView();
      return dto.CopyTo(entity);
    }

    #endregion

    #region JobJobTitleSkillViewDto Mappers

    public static JobJobTitleSkillViewDto AsDto(this JobJobTitleSkillView entity)
    {
      var dto = new JobJobTitleSkillViewDto
      {
        SkillId = entity.SkillId,
        Id = entity.Id
      };
      return dto;
    }

    public static JobJobTitleSkillView CopyTo(this JobJobTitleSkillViewDto dto, JobJobTitleSkillView entity)
    {
      entity.SkillId = dto.SkillId;
      return entity;
    }

    public static JobJobTitleSkillView CopyTo(this JobJobTitleSkillViewDto dto)
    {
      var entity = new JobJobTitleSkillView();
      return dto.CopyTo(entity);
    }

    #endregion

		#region EducationInternshipCategoryDto Mappers

		public static EducationInternshipCategoryDto AsDto(this EducationInternshipCategory entity)
		{
			return entity.AsDto(false);
		}

		public static EducationInternshipCategoryDto AsDto(this EducationInternshipCategory entity, bool includeCategorySkills)
		{
			var dto = new EducationInternshipCategoryDto
				          {
					          Name = entity.Name,
										LocalisationId = entity.LocalisationId,
										Id = entity.Id,
										CategoryType = entity.CategoryType
				          };
			if (includeCategorySkills)
			{
				dto.CategorySkills = entity.EducationInternshipSkills.Select(skill => skill.AsDto()).ToList();
			}
			return dto;
		}

		public static EducationInternshipCategory CopyTo(this EducationInternshipCategoryDto dto, EducationInternshipCategory entity)
		{
			entity.Name = dto.Name;
			entity.LocalisationId = dto.LocalisationId;
		  entity.CategoryType = dto.CategoryType;
			return entity;
		}

		public static EducationInternshipCategory CopyTo(this EducationInternshipCategoryDto dto)
		{
			var entity = new EducationInternshipCategory();
			return dto.CopyTo(entity);
		}

		#endregion

    #region EducationInternshipStatementDto Mappers

    public static EducationInternshipStatementDto AsDto(this EducationInternshipStatement entity)
    {
      var dto = new EducationInternshipStatementDto
      {
        PastTenseStatement = entity.PastTenseStatement,
        PresentTenseStatement = entity.PresentTenseStatement,
        LocalisationId = entity.LocalisationId,
        EducationInternshipSkillId = entity.EducationInternshipSkillId,
        Id = entity.Id
      };
      return dto;
    }

    public static EducationInternshipStatement CopyTo(this EducationInternshipStatementDto dto, EducationInternshipStatement entity)
    {
      entity.PastTenseStatement = dto.PastTenseStatement;
      entity.PresentTenseStatement = dto.PresentTenseStatement;
      entity.LocalisationId = dto.LocalisationId;
      entity.EducationInternshipSkillId = dto.EducationInternshipSkillId;
      return entity;
    }

    public static EducationInternshipStatement CopyTo(this EducationInternshipStatementDto dto)
    {
      var entity = new EducationInternshipStatement();
      return dto.CopyTo(entity);
    }

    #endregion

		#region EducationInternshipSkillDto Mappers

		public static EducationInternshipSkillDto AsDto(this EducationInternshipSkill entity)
		{
			var dto = new EducationInternshipSkillDto
			{
				Name = entity.Name,
				LocalisationId = entity.LocalisationId,
				Id = entity.Id,
				EducationInternshipCategoryId = entity.EducationInternshipCategoryId
			};
			return dto;
		}

		public static EducationInternshipSkill CopyTo(this EducationInternshipSkillDto dto, EducationInternshipSkill entity)
		{
			entity.Name = dto.Name;
			entity.LocalisationId = dto.LocalisationId;
			entity.EducationInternshipCategoryId = dto.EducationInternshipCategoryId;
			return entity;
		}

		public static EducationInternshipSkill CopyTo(this EducationInternshipSkillDto dto)
		{
			var entity = new EducationInternshipSkill();
			return dto.CopyTo(entity);
		}

		#endregion

		#region InternshipCategoryDto Mappers

		public static InternshipCategoryDto AsDto(this InternshipCategory entity)
    {
      var dto = new InternshipCategoryDto
      {
        Name = entity.Name,
        LocalisationId = entity.LocalisationId,
        Id = entity.Id,
				LensFilterId = entity.LensFilterId
      };
      return dto;
    }

    public static InternshipCategory CopyTo(this InternshipCategoryDto dto, InternshipCategory entity)
    {
      entity.Name = dto.Name;
      entity.LocalisationId = dto.LocalisationId;
    	entity.LensFilterId = dto.LensFilterId;
      return entity;
    }

    public static InternshipCategory CopyTo(this InternshipCategoryDto dto)
    {
      var entity = new InternshipCategory();
      return dto.CopyTo(entity);
    }

    #endregion

    #region InternshipSkillDto Mappers

    public static InternshipSkillDto AsDto(this InternshipSkill entity)
    {
      var dto = new InternshipSkillDto
      {
        Rank = entity.Rank,
        SkillId = entity.SkillId,
        InternshipCategoryId = entity.InternshipCategoryId,
        Id = entity.Id
      };
      return dto;
    }

    public static InternshipSkill CopyTo(this InternshipSkillDto dto, InternshipSkill entity)
    {
      entity.Rank = dto.Rank;
      entity.SkillId = dto.SkillId;
      entity.InternshipCategoryId = dto.InternshipCategoryId;
      return entity;
    }

    public static InternshipSkill CopyTo(this InternshipSkillDto dto)
    {
      var entity = new InternshipSkill();
      return dto.CopyTo(entity);
    }

    #endregion

    #region InternshipEmployerDto Mappers

    public static InternshipEmployerDto AsDto(this InternshipEmployer entity)
    {
      var dto = new InternshipEmployerDto
      {
        Frequency = entity.Frequency,
        EmployerId = entity.EmployerId,
        StateAreaId = entity.StateAreaId,
        InternshipCategoryId = entity.InternshipCategoryId,
        Id = entity.Id
      };
      return dto;
    }

    public static InternshipEmployer CopyTo(this InternshipEmployerDto dto, InternshipEmployer entity)
    {
      entity.Frequency = dto.Frequency;
      entity.EmployerId = dto.EmployerId;
      entity.StateAreaId = dto.StateAreaId;
      entity.InternshipCategoryId = dto.InternshipCategoryId;
      return entity;
    }

    public static InternshipEmployer CopyTo(this InternshipEmployerDto dto)
    {
      var entity = new InternshipEmployer();
      return dto.CopyTo(entity);
    }

    #endregion

    #region InternshipJobTitleDto Mappers

    public static InternshipJobTitleDto AsDto(this InternshipJobTitle entity)
    {
      var dto = new InternshipJobTitleDto
      {
        JobTitle = entity.JobTitle,
        Frequency = entity.Frequency,
        InternshipCategoryId = entity.InternshipCategoryId,
        Id = entity.Id
      };
      return dto;
    }

    public static InternshipJobTitle CopyTo(this InternshipJobTitleDto dto, InternshipJobTitle entity)
    {
      entity.JobTitle = dto.JobTitle;
      entity.Frequency = dto.Frequency;
      entity.InternshipCategoryId = dto.InternshipCategoryId;
      return entity;
    }

    public static InternshipJobTitle CopyTo(this InternshipJobTitleDto dto)
    {
      var entity = new InternshipJobTitle();
      return dto.CopyTo(entity);
    }

    #endregion

    #region InternshipReportDto Mappers

    public static InternshipReportDto AsDto(this InternshipReport entity)
    {
      var dto = new InternshipReportDto
      {
        NumberAvailable = entity.NumberAvailable,
        StateAreaId = entity.StateAreaId,
        InternshipCategoryId = entity.InternshipCategoryId,
        Id = entity.Id
      };
      return dto;
    }

    public static InternshipReport CopyTo(this InternshipReportDto dto, InternshipReport entity)
    {
      entity.NumberAvailable = dto.NumberAvailable;
      entity.StateAreaId = dto.StateAreaId;
      entity.InternshipCategoryId = dto.InternshipCategoryId;
      return entity;
    }

    public static InternshipReport CopyTo(this InternshipReportDto dto)
    {
      var entity = new InternshipReport();
      return dto.CopyTo(entity);
    }

    #endregion

    #region InternshipEmployerViewDto Mappers

    public static InternshipEmployerViewDto AsDto(this InternshipEmployerView entity)
    {
      var dto = new InternshipEmployerViewDto
      {
        EmployerId = entity.EmployerId,
        Name = entity.Name,
        Frequency = entity.Frequency,
        InternshipCategoryId = entity.InternshipCategoryId,
        StateAreaId = entity.StateAreaId,
        InternshipCategoryName = entity.InternshipCategoryName,
        Id = entity.Id
      };
      return dto;
    }

    public static InternshipEmployerView CopyTo(this InternshipEmployerViewDto dto, InternshipEmployerView entity)
    {
      entity.EmployerId = dto.EmployerId;
      entity.Name = dto.Name;
      entity.Frequency = dto.Frequency;
      entity.InternshipCategoryId = dto.InternshipCategoryId;
      entity.StateAreaId = dto.StateAreaId;
      entity.InternshipCategoryName = dto.InternshipCategoryName;
      return entity;
    }

    public static InternshipEmployerView CopyTo(this InternshipEmployerViewDto dto)
    {
      var entity = new InternshipEmployerView();
      return dto.CopyTo(entity);
    }

    #endregion

    #region InternshipReportViewDto Mappers

    public static InternshipReportViewDto AsDto(this InternshipReportView entity)
    {
      var dto = new InternshipReportViewDto
      {
        InternshipCategoryId = entity.InternshipCategoryId,
        Name = entity.Name,
        NumberAvailable = entity.NumberAvailable,
        StateAreaId = entity.StateAreaId,
        Id = entity.Id
      };
      return dto;
    }

    public static InternshipReportView CopyTo(this InternshipReportViewDto dto, InternshipReportView entity)
    {
      entity.InternshipCategoryId = dto.InternshipCategoryId;
      entity.Name = dto.Name;
      entity.NumberAvailable = dto.NumberAvailable;
      entity.StateAreaId = dto.StateAreaId;
      return entity;
    }

    public static InternshipReportView CopyTo(this InternshipReportViewDto dto)
    {
      var entity = new InternshipReportView();
      return dto.CopyTo(entity);
    }

    #endregion

    #region InternshipSkillViewDto Mappers

    public static InternshipSkillViewDto AsDto(this InternshipSkillView entity)
    {
      var dto = new InternshipSkillViewDto
      {
        SkillId = entity.SkillId,
        SkillType = entity.SkillType,
        SkillName = entity.SkillName,
        InternshipCategoryId = entity.InternshipCategoryId,
        Rank = entity.Rank,
        InternshipCategoryName = entity.InternshipCategoryName,
        Id = entity.Id
      };
      return dto;
    }

    public static InternshipSkillView CopyTo(this InternshipSkillViewDto dto, InternshipSkillView entity)
    {
      entity.SkillId = dto.SkillId;
      entity.SkillType = dto.SkillType;
      entity.SkillName = dto.SkillName;
      entity.InternshipCategoryId = dto.InternshipCategoryId;
      entity.Rank = dto.Rank;
      entity.InternshipCategoryName = dto.InternshipCategoryName;
      return entity;
    }

    public static InternshipSkillView CopyTo(this InternshipSkillViewDto dto)
    {
      var entity = new InternshipSkillView();
      return dto.CopyTo(entity);
    }

    #endregion

    #region StateAreaViewDto Mappers

    public static StateAreaViewDto AsDto(this StateAreaView entity)
    {
      var dto = new StateAreaViewDto
      {
        StateId = entity.StateId,
        StateName = entity.StateName,
        StateCode = entity.StateCode,
        StateSortOrder = entity.StateSortOrder,
        AreaId = entity.AreaId,
        AreaName = entity.AreaName,
        AreaSortOrder = entity.AreaSortOrder,
        StateAreaName = entity.StateAreaName,
        AreaCode = entity.AreaCode,
        IsDefault = entity.IsDefault,
        Id = entity.Id
      };
      return dto;
    }

    public static StateAreaView CopyTo(this StateAreaViewDto dto, StateAreaView entity)
    {
      entity.StateId = dto.StateId;
      entity.StateName = dto.StateName;
      entity.StateCode = dto.StateCode;
      entity.StateSortOrder = dto.StateSortOrder;
      entity.AreaId = dto.AreaId;
      entity.AreaName = dto.AreaName;
      entity.AreaSortOrder = dto.AreaSortOrder;
      entity.StateAreaName = dto.StateAreaName;
      entity.AreaCode = dto.AreaCode;
      entity.IsDefault = dto.IsDefault;
      return entity;
    }

    public static StateAreaView CopyTo(this StateAreaViewDto dto)
    {
      var entity = new StateAreaView();
      return dto.CopyTo(entity);
    }

    #endregion

    #region CertificationDto Mappers

    public static CertificationDto AsDto(this Certification entity)
    {
      var dto = new CertificationDto
      {
        Name = entity.Name,
        Id = entity.Id
      };
      return dto;
    }

    public static Certification CopyTo(this CertificationDto dto, Certification entity)
    {
      entity.Name = dto.Name;
      return entity;
    }

    public static Certification CopyTo(this CertificationDto dto)
    {
      var entity = new Certification();
      return dto.CopyTo(entity);
    }

    #endregion

    #region JobCertificationDto Mappers

    public static JobCertificationDto AsDto(this JobCertification entity)
    {
      var dto = new JobCertificationDto
      {
        DemandPercentile = entity.DemandPercentile,
        JobId = entity.JobId,
        CertificationId = entity.CertificationId,
        Id = entity.Id
      };
      return dto;
    }

    public static JobCertification CopyTo(this JobCertificationDto dto, JobCertification entity)
    {
      entity.DemandPercentile = dto.DemandPercentile;
      entity.JobId = dto.JobId;
      entity.CertificationId = dto.CertificationId;
      return entity;
    }

    public static JobCertification CopyTo(this JobCertificationDto dto)
    {
      var entity = new JobCertification();
      return dto.CopyTo(entity);
    }

    #endregion

    #region DegreeDto Mappers

    public static DegreeDto AsDto(this Degree entity)
    {
      var dto = new DegreeDto
      {
        Name = entity.Name,
        RcipCode = entity.RcipCode,
        IsDegreeArea = entity.IsDegreeArea,
        IsClientData = entity.IsClientData,
        ClientDataTag = entity.ClientDataTag,
        Tier = entity.Tier,
        Id = entity.Id
      };
      return dto;
    }

    public static Degree CopyTo(this DegreeDto dto, Degree entity)
    {
      entity.Name = dto.Name;
      entity.RcipCode = dto.RcipCode;
      entity.IsDegreeArea = dto.IsDegreeArea;
      entity.IsClientData = dto.IsClientData;
      entity.ClientDataTag = dto.ClientDataTag;
      entity.Tier = dto.Tier;
      return entity;
    }

    public static Degree CopyTo(this DegreeDto dto)
    {
      var entity = new Degree();
      return dto.CopyTo(entity);
    }

    #endregion

    #region DegreeEducationLevelDto Mappers

    public static DegreeEducationLevelDto AsDto(this DegreeEducationLevel entity)
    {
      var dto = new DegreeEducationLevelDto
      {
        EducationLevel = entity.EducationLevel,
        DegreesAwarded = entity.DegreesAwarded,
        Name = entity.Name,
        ExcludeFromReport = entity.ExcludeFromReport,
        DegreeId = entity.DegreeId,
        Id = entity.Id
      };
      return dto;
    }

    public static DegreeEducationLevel CopyTo(this DegreeEducationLevelDto dto, DegreeEducationLevel entity)
    {
      entity.EducationLevel = dto.EducationLevel;
      entity.DegreesAwarded = dto.DegreesAwarded;
      entity.Name = dto.Name;
      entity.ExcludeFromReport = dto.ExcludeFromReport;
      entity.DegreeId = dto.DegreeId;
      return entity;
    }

    public static DegreeEducationLevel CopyTo(this DegreeEducationLevelDto dto)
    {
      var entity = new DegreeEducationLevel();
      return dto.CopyTo(entity);
    }

    #endregion

    #region JobDegreeEducationLevelDto Mappers

    public static JobDegreeEducationLevelDto AsDto(this JobDegreeEducationLevel entity)
    {
      var dto = new JobDegreeEducationLevelDto
      {
        ExcludeFromJob = entity.ExcludeFromJob,
        Tier = entity.Tier,
        JobId = entity.JobId,
        DegreeEducationLevelId = entity.DegreeEducationLevelId,
        Id = entity.Id
      };
      return dto;
    }

    public static JobDegreeEducationLevel CopyTo(this JobDegreeEducationLevelDto dto, JobDegreeEducationLevel entity)
    {
      entity.ExcludeFromJob = dto.ExcludeFromJob;
      entity.Tier = dto.Tier;
      entity.JobId = dto.JobId;
      entity.DegreeEducationLevelId = dto.DegreeEducationLevelId;
      return entity;
    }

    public static JobDegreeEducationLevel CopyTo(this JobDegreeEducationLevelDto dto)
    {
      var entity = new JobDegreeEducationLevel();
      return dto.CopyTo(entity);
    }

    #endregion

    #region StudyPlaceDegreeEducationLevelDto Mappers

    public static StudyPlaceDegreeEducationLevelDto AsDto(this StudyPlaceDegreeEducationLevel entity)
    {
      var dto = new StudyPlaceDegreeEducationLevelDto
      {
        StudyPlaceId = entity.StudyPlaceId,
        DegreeEducationLevelId = entity.DegreeEducationLevelId,
        Id = entity.Id
      };
      return dto;
    }

    public static StudyPlaceDegreeEducationLevel CopyTo(this StudyPlaceDegreeEducationLevelDto dto, StudyPlaceDegreeEducationLevel entity)
    {
      entity.StudyPlaceId = dto.StudyPlaceId;
      entity.DegreeEducationLevelId = dto.DegreeEducationLevelId;
      return entity;
    }

    public static StudyPlaceDegreeEducationLevel CopyTo(this StudyPlaceDegreeEducationLevelDto dto)
    {
      var entity = new StudyPlaceDegreeEducationLevel();
      return dto.CopyTo(entity);
    }

    #endregion

    #region DegreeEducationLevelSkillViewDto Mappers

    public static DegreeEducationLevelSkillViewDto AsDto(this DegreeEducationLevelSkillView entity)
    {
      var dto = new DegreeEducationLevelSkillViewDto
      {
        StateAreaId = entity.StateAreaId,
        SkillId = entity.SkillId,
        SkillType = entity.SkillType,
        SkillName = entity.SkillName,
        DemandPercentile = entity.DemandPercentile,
        Id = entity.Id
      };
      return dto;
    }

    public static DegreeEducationLevelSkillView CopyTo(this DegreeEducationLevelSkillViewDto dto, DegreeEducationLevelSkillView entity)
    {
      entity.StateAreaId = dto.StateAreaId;
      entity.SkillId = dto.SkillId;
      entity.SkillType = dto.SkillType;
      entity.SkillName = dto.SkillName;
      entity.DemandPercentile = dto.DemandPercentile;
      return entity;
    }

    public static DegreeEducationLevelSkillView CopyTo(this DegreeEducationLevelSkillViewDto dto)
    {
      var entity = new DegreeEducationLevelSkillView();
      return dto.CopyTo(entity);
    }

    #endregion

    #region DegreeEducationLevelViewDto Mappers

    public static DegreeEducationLevelViewDto AsDto(this DegreeEducationLevelView entity)
    {
      var dto = new DegreeEducationLevelViewDto
      {
        DegreeId = entity.DegreeId,
        DegreeName = entity.DegreeName,
        EducationLevel = entity.EducationLevel,
        DegreeEducationLevelName = entity.DegreeEducationLevelName,
        IsDegreeArea = entity.IsDegreeArea,
        IsClientData = entity.IsClientData,
        ClientDataTag = entity.ClientDataTag,
        Tier = entity.Tier,
        Id = entity.Id
      };
      return dto;
    }

    public static DegreeEducationLevelView CopyTo(this DegreeEducationLevelViewDto dto, DegreeEducationLevelView entity)
    {
      entity.DegreeId = dto.DegreeId;
      entity.DegreeName = dto.DegreeName;
      entity.EducationLevel = dto.EducationLevel;
      entity.DegreeEducationLevelName = dto.DegreeEducationLevelName;
      entity.IsDegreeArea = dto.IsDegreeArea;
      entity.IsClientData = dto.IsClientData;
      entity.ClientDataTag = dto.ClientDataTag;
      entity.Tier = dto.Tier;
      return entity;
    }

    public static DegreeEducationLevelView CopyTo(this DegreeEducationLevelViewDto dto)
    {
      var entity = new DegreeEducationLevelView();
      return dto.CopyTo(entity);
    }

    #endregion

    #region JobDegreeEducationLevelSkillViewDto Mappers

    public static JobDegreeEducationLevelSkillViewDto AsDto(this JobDegreeEducationLevelSkillView entity)
    {
      var dto = new JobDegreeEducationLevelSkillViewDto
      {
        SkillId = entity.SkillId,
        Id = entity.Id
      };
      return dto;
    }

    public static JobDegreeEducationLevelSkillView CopyTo(this JobDegreeEducationLevelSkillViewDto dto, JobDegreeEducationLevelSkillView entity)
    {
      entity.SkillId = dto.SkillId;
      return entity;
    }

    public static JobDegreeEducationLevelSkillView CopyTo(this JobDegreeEducationLevelSkillViewDto dto)
    {
      var entity = new JobDegreeEducationLevelSkillView();
      return dto.CopyTo(entity);
    }

    #endregion

    #region JobDegreeEducationLevelViewDto Mappers

    public static JobDegreeEducationLevelViewDto AsDto(this JobDegreeEducationLevelView entity)
    {
      var dto = new JobDegreeEducationLevelViewDto
      {
        JobId = entity.JobId,
        DegreeEducationLevelId = entity.DegreeEducationLevelId,
        DegreeId = entity.DegreeId,
        DegreeName = entity.DegreeName,
        DegreesAwarded = entity.DegreesAwarded,
        DegreeEducationLevelName = entity.DegreeEducationLevelName,
        EducationLevel = entity.EducationLevel,
        IsDegreeArea = entity.IsDegreeArea,
        IsClientData = entity.IsClientData,
        ClientDataTag = entity.ClientDataTag,
        ExcludeFromJob = entity.ExcludeFromJob,
        Tier = entity.Tier,
        ROnet = entity.ROnet,
        Id = entity.Id
      };
      return dto;
    }

    public static JobDegreeEducationLevelView CopyTo(this JobDegreeEducationLevelViewDto dto, JobDegreeEducationLevelView entity)
    {
      entity.JobId = dto.JobId;
      entity.DegreeEducationLevelId = dto.DegreeEducationLevelId;
      entity.DegreeId = dto.DegreeId;
      entity.DegreeName = dto.DegreeName;
      entity.DegreesAwarded = dto.DegreesAwarded;
      entity.DegreeEducationLevelName = dto.DegreeEducationLevelName;
      entity.EducationLevel = dto.EducationLevel;
      entity.IsDegreeArea = dto.IsDegreeArea;
      entity.IsClientData = dto.IsClientData;
      entity.ClientDataTag = dto.ClientDataTag;
      entity.ExcludeFromJob = dto.ExcludeFromJob;
      entity.Tier = dto.Tier;
      entity.ROnet = dto.ROnet;
      return entity;
    }

    public static JobDegreeEducationLevelView CopyTo(this JobDegreeEducationLevelViewDto dto)
    {
      var entity = new JobDegreeEducationLevelView();
      return dto.CopyTo(entity);
    }

    #endregion

    #region JobCertificationViewDto Mappers

    public static JobCertificationViewDto AsDto(this JobCertificationView entity)
    {
      var dto = new JobCertificationViewDto
      {
        JobId = entity.JobId,
        CertificationId = entity.CertificationId,
        CertificationName = entity.CertificationName,
        DemandPercentile = entity.DemandPercentile,
        Id = entity.Id
      };
      return dto;
    }

    public static JobCertificationView CopyTo(this JobCertificationViewDto dto, JobCertificationView entity)
    {
      entity.JobId = dto.JobId;
      entity.CertificationId = dto.CertificationId;
      entity.CertificationName = dto.CertificationName;
      entity.DemandPercentile = dto.DemandPercentile;
      return entity;
    }

    public static JobCertificationView CopyTo(this JobCertificationViewDto dto)
    {
      var entity = new JobCertificationView();
      return dto.CopyTo(entity);
    }

    #endregion

    #region JobDegreeEducationLevelReportDto Mappers

    public static JobDegreeEducationLevelReportDto AsDto(this JobDegreeEducationLevelReport entity)
    {
      var dto = new JobDegreeEducationLevelReportDto
      {
        HiringDemand = entity.HiringDemand,
        DegreesAwarded = entity.DegreesAwarded,
        StateAreaId = entity.StateAreaId,
        JobDegreeEducationLevelId = entity.JobDegreeEducationLevelId,
        Id = entity.Id
      };
      return dto;
    }

    public static JobDegreeEducationLevelReport CopyTo(this JobDegreeEducationLevelReportDto dto, JobDegreeEducationLevelReport entity)
    {
      entity.HiringDemand = dto.HiringDemand;
      entity.DegreesAwarded = dto.DegreesAwarded;
      entity.StateAreaId = dto.StateAreaId;
      entity.JobDegreeEducationLevelId = dto.JobDegreeEducationLevelId;
      return entity;
    }

    public static JobDegreeEducationLevelReport CopyTo(this JobDegreeEducationLevelReportDto dto)
    {
      var entity = new JobDegreeEducationLevelReport();
      return dto.CopyTo(entity);
    }

    #endregion

    #region JobDegreeEducationLevelReportViewDto Mappers

    public static JobDegreeEducationLevelReportViewDto AsDto(this JobDegreeEducationLevelReportView entity)
    {
      var dto = new JobDegreeEducationLevelReportViewDto
      {
        StateAreaId = entity.StateAreaId,
        JobDegreeEducationLevelId = entity.JobDegreeEducationLevelId,
        HiringDemand = entity.HiringDemand,
        DegreesAwarded = entity.DegreesAwarded,
        JobId = entity.JobId,
        DegreeEducationLevelId = entity.DegreeEducationLevelId,
        EducationLevel = entity.EducationLevel,
        DegreeId = entity.DegreeId,
        DegreeName = entity.DegreeName,
        IsDegreeArea = entity.IsDegreeArea,
        DegreeEducationLevelName = entity.DegreeEducationLevelName,
        IsClientData = entity.IsClientData,
        ClientDataTag = entity.ClientDataTag,
        ExcludeFromReport = entity.ExcludeFromReport,
        ExcludeFromJob = entity.ExcludeFromJob,
        Id = entity.Id
      };
      return dto;
    }

    public static JobDegreeEducationLevelReportView CopyTo(this JobDegreeEducationLevelReportViewDto dto, JobDegreeEducationLevelReportView entity)
    {
      entity.StateAreaId = dto.StateAreaId;
      entity.JobDegreeEducationLevelId = dto.JobDegreeEducationLevelId;
      entity.HiringDemand = dto.HiringDemand;
      entity.DegreesAwarded = dto.DegreesAwarded;
      entity.JobId = dto.JobId;
      entity.DegreeEducationLevelId = dto.DegreeEducationLevelId;
      entity.EducationLevel = dto.EducationLevel;
      entity.DegreeId = dto.DegreeId;
      entity.DegreeName = dto.DegreeName;
      entity.IsDegreeArea = dto.IsDegreeArea;
      entity.DegreeEducationLevelName = dto.DegreeEducationLevelName;
      entity.IsClientData = dto.IsClientData;
      entity.ClientDataTag = dto.ClientDataTag;
      entity.ExcludeFromReport = dto.ExcludeFromReport;
      entity.ExcludeFromJob = dto.ExcludeFromJob;
      return entity;
    }

    public static JobDegreeEducationLevelReportView CopyTo(this JobDegreeEducationLevelReportViewDto dto)
    {
      var entity = new JobDegreeEducationLevelReportView();
      return dto.CopyTo(entity);
    }	

    #endregion

    #region DegreeEducationLevelExtDto Mappers

    public static DegreeEducationLevelExtDto AsDto(this DegreeEducationLevelExt entity)
    {
      var dto = new DegreeEducationLevelExtDto
      {
        Name = entity.Name,
        Url = entity.Url,
        Description = entity.Description,
        DegreeEducationLevelId = entity.DegreeEducationLevelId,
        Id = entity.Id
      };
      return dto;
    }

    public static DegreeEducationLevelExt CopyTo(this DegreeEducationLevelExtDto dto, DegreeEducationLevelExt entity)
    {
      entity.Name = dto.Name;
      entity.Url = dto.Url;
      entity.Description = dto.Description;
      entity.DegreeEducationLevelId = dto.DegreeEducationLevelId;
      return entity;
    }

    public static DegreeEducationLevelExt CopyTo(this DegreeEducationLevelExtDto dto)
    {
      var entity = new DegreeEducationLevelExt();
      return dto.CopyTo(entity);
    }

    #endregion

    #region DegreeEducationLevelStateAreaViewDto Mappers

    public static DegreeEducationLevelStateAreaViewDto AsDto(this DegreeEducationLevelStateAreaView entity)
    {
      var dto = new DegreeEducationLevelStateAreaViewDto
      {
        DegreeId = entity.DegreeId,
        DegreeName = entity.DegreeName,
        IsDegreeArea = entity.IsDegreeArea,
        IsClientData = entity.IsClientData,
        StateAreaId = entity.StateAreaId,
        DegreeEducationLevelName = entity.DegreeEducationLevelName,
        ClientDataTag = entity.ClientDataTag,
        EducationLevel = entity.EducationLevel,
        Id = entity.Id
      };
      return dto;
    }

    public static DegreeEducationLevelStateAreaView CopyTo(this DegreeEducationLevelStateAreaViewDto dto, DegreeEducationLevelStateAreaView entity)
    {
      entity.DegreeId = dto.DegreeId;
      entity.DegreeName = dto.DegreeName;
      entity.IsDegreeArea = dto.IsDegreeArea;
      entity.IsClientData = dto.IsClientData;
      entity.StateAreaId = dto.StateAreaId;
      entity.DegreeEducationLevelName = dto.DegreeEducationLevelName;
      entity.ClientDataTag = dto.ClientDataTag;
      entity.EducationLevel = dto.EducationLevel;
      return entity;
    }

    public static DegreeEducationLevelStateAreaView CopyTo(this DegreeEducationLevelStateAreaViewDto dto)
    {
      var entity = new DegreeEducationLevelStateAreaView();
      return dto.CopyTo(entity);
    }

    #endregion

    #region JobStateAreaViewDto Mappers

    public static JobStateAreaViewDto AsDto(this JobStateAreaView entity)
    {
      var dto = new JobStateAreaViewDto
      {
        Name = entity.Name,
        StateAreaId = entity.StateAreaId,
        Id = entity.Id
      };
      return dto;
    }

    public static JobStateAreaView CopyTo(this JobStateAreaViewDto dto, JobStateAreaView entity)
    {
      entity.Name = dto.Name;
      entity.StateAreaId = dto.StateAreaId;
      return entity;
    }

    public static JobStateAreaView CopyTo(this JobStateAreaViewDto dto)
    {
      var entity = new JobStateAreaView();
      return dto.CopyTo(entity);
    }

    #endregion

    #region DegreeAliasDto Mappers

    public static DegreeAliasDto AsDto(this DegreeAlias entity)
    {
      var dto = new DegreeAliasDto
      {
        AliasExtended = entity.AliasExtended,
        EducationLevel = entity.EducationLevel,
        Alias = entity.Alias,
        DegreeId = entity.DegreeId,
        DegreeEducationLevelId = entity.DegreeEducationLevelId,
        Id = entity.Id
      };
      return dto;
    }

    public static DegreeAlias CopyTo(this DegreeAliasDto dto, DegreeAlias entity)
    {
      entity.AliasExtended = dto.AliasExtended;
      entity.EducationLevel = dto.EducationLevel;
      entity.Alias = dto.Alias;
      entity.DegreeId = dto.DegreeId;
      entity.DegreeEducationLevelId = dto.DegreeEducationLevelId;
      return entity;
    }

    public static DegreeAlias CopyTo(this DegreeAliasDto dto)
    {
      var entity = new DegreeAlias();
      return dto.CopyTo(entity);
    }

    #endregion

    #region DegreeAliasViewDto Mappers

    public static DegreeAliasViewDto AsDto(this DegreeAliasView entity)
    {
      var dto = new DegreeAliasViewDto
      {
        EducationLevel = entity.EducationLevel,
        DegreeId = entity.DegreeId,
        IsClientData = entity.IsClientData,
        ClientDataTag = entity.ClientDataTag,
        DegreeEducationLevelId = entity.DegreeEducationLevelId,
        AliasExtended = entity.AliasExtended,
        Alias = entity.Alias,
        Id = entity.Id
      };
      return dto;
    }

    public static DegreeAliasView CopyTo(this DegreeAliasViewDto dto, DegreeAliasView entity)
    {
      entity.EducationLevel = dto.EducationLevel;
      entity.DegreeId = dto.DegreeId;
      entity.IsClientData = dto.IsClientData;
      entity.ClientDataTag = dto.ClientDataTag;
      entity.DegreeEducationLevelId = dto.DegreeEducationLevelId;
      entity.AliasExtended = dto.AliasExtended;
      entity.Alias = dto.Alias;
      return entity;
    }

    public static DegreeAliasView CopyTo(this DegreeAliasViewDto dto)
    {
      var entity = new DegreeAliasView();
      return dto.CopyTo(entity);
    }

    #endregion

    #region JobCareerPathToDto Mappers

    public static JobCareerPathToDto AsDto(this JobCareerPathTo entity)
    {
      var dto = new JobCareerPathToDto
      {
        Rank = entity.Rank,
        CurrentJobId = entity.CurrentJobId,
        Next1JobId = entity.Next1JobId,
        Next2JobId = entity.Next2JobId,
        Id = entity.Id
      };
      return dto;
    }

    public static JobCareerPathTo CopyTo(this JobCareerPathToDto dto, JobCareerPathTo entity)
    {
      entity.Rank = dto.Rank;
      entity.CurrentJobId = dto.CurrentJobId;
      entity.Next1JobId = dto.Next1JobId;
      entity.Next2JobId = dto.Next2JobId;
      return entity;
    }

    public static JobCareerPathTo CopyTo(this JobCareerPathToDto dto)
    {
      var entity = new JobCareerPathTo();
      return dto.CopyTo(entity);
    }

    #endregion

    #region JobCareerPathFromDto Mappers

    public static JobCareerPathFromDto AsDto(this JobCareerPathFrom entity)
    {
      var dto = new JobCareerPathFromDto
      {
        Rank = entity.Rank,
        FromJobId = entity.FromJobId,
        ToJobId = entity.ToJobId,
        Id = entity.Id
      };
      return dto;
    }

    public static JobCareerPathFrom CopyTo(this JobCareerPathFromDto dto, JobCareerPathFrom entity)
    {
      entity.Rank = dto.Rank;
      entity.FromJobId = dto.FromJobId;
      entity.ToJobId = dto.ToJobId;
      return entity;
    }

    public static JobCareerPathFrom CopyTo(this JobCareerPathFromDto dto)
    {
      var entity = new JobCareerPathFrom();
      return dto.CopyTo(entity);
    }

    #endregion

    #region StudyPlaceDegreeEducationLevelViewDto Mappers

    public static StudyPlaceDegreeEducationLevelViewDto AsDto(this StudyPlaceDegreeEducationLevelView entity)
    {
      var dto = new StudyPlaceDegreeEducationLevelViewDto
      {
        StudyPlaceId = entity.StudyPlaceId,
        StudyPlaceName = entity.StudyPlaceName,
        StateAreaId = entity.StateAreaId,
        DegreeEducationLevelId = entity.DegreeEducationLevelId,
        DegreeId = entity.DegreeId,
        DegreeName = entity.DegreeName,
        IsClientData = entity.IsClientData,
        ClientDataTag = entity.ClientDataTag,
        DegreeEducationLevelName = entity.DegreeEducationLevelName,
        Id = entity.Id
      };
      return dto;
    }

    public static StudyPlaceDegreeEducationLevelView CopyTo(this StudyPlaceDegreeEducationLevelViewDto dto, StudyPlaceDegreeEducationLevelView entity)
    {
      entity.StudyPlaceId = dto.StudyPlaceId;
      entity.StudyPlaceName = dto.StudyPlaceName;
      entity.StateAreaId = dto.StateAreaId;
      entity.DegreeEducationLevelId = dto.DegreeEducationLevelId;
      entity.DegreeId = dto.DegreeId;
      entity.DegreeName = dto.DegreeName;
      entity.IsClientData = dto.IsClientData;
      entity.ClientDataTag = dto.ClientDataTag;
      entity.DegreeEducationLevelName = dto.DegreeEducationLevelName;
      return entity;
    }

    public static StudyPlaceDegreeEducationLevelView CopyTo(this StudyPlaceDegreeEducationLevelViewDto dto)
    {
      var entity = new StudyPlaceDegreeEducationLevelView();
      return dto.CopyTo(entity);
    }

    #endregion

    #region JobEmployerSkillDto Mappers

    public static JobEmployerSkillDto AsDto(this JobEmployerSkill entity)
    {
      var dto = new JobEmployerSkillDto
      {
        SkillCount = entity.SkillCount,
        Rank = entity.Rank,
        EmployerId = entity.EmployerId,
        SkillId = entity.SkillId,
        Id = entity.Id
      };
      return dto;
    }

    public static JobEmployerSkill CopyTo(this JobEmployerSkillDto dto, JobEmployerSkill entity)
    {
      entity.SkillCount = dto.SkillCount;
      entity.Rank = dto.Rank;
      entity.EmployerId = dto.EmployerId;
      entity.SkillId = dto.SkillId;
      return entity;
    }

    public static JobEmployerSkill CopyTo(this JobEmployerSkillDto dto)
    {
      var entity = new JobEmployerSkill();
      return dto.CopyTo(entity);
    }

    #endregion

    #region JobEmployerSkillViewDto Mappers

    public static JobEmployerSkillViewDto AsDto(this JobEmployerSkillView entity)
    {
      var dto = new JobEmployerSkillViewDto
      {
        JobId = entity.JobId,
        EmployerId = entity.EmployerId,
        SkillId = entity.SkillId,
        SkillType = entity.SkillType,
        SkillName = entity.SkillName,
        SkillCount = entity.SkillCount,
        Id = entity.Id
      };
      return dto;
    }

    public static JobEmployerSkillView CopyTo(this JobEmployerSkillViewDto dto, JobEmployerSkillView entity)
    {
      entity.JobId = dto.JobId;
      entity.EmployerId = dto.EmployerId;
      entity.SkillId = dto.SkillId;
      entity.SkillType = dto.SkillType;
      entity.SkillName = dto.SkillName;
      entity.SkillCount = dto.SkillCount;
      return entity;
    }

    public static JobEmployerSkillView CopyTo(this JobEmployerSkillViewDto dto)
    {
      var entity = new JobEmployerSkillView();
      return dto.CopyTo(entity);
    }

    #endregion

    #region ProgramAreaDto Mappers

    public static ProgramAreaDto AsDto(this ProgramArea entity)
    {
      var dto = new ProgramAreaDto
      {
        Name = entity.Name,
        Description = entity.Description,
        IsClientData = entity.IsClientData,
        ClientDataTag = entity.ClientDataTag,
        Id = entity.Id
      };
      return dto;
    }

    public static ProgramArea CopyTo(this ProgramAreaDto dto, ProgramArea entity)
    {
      entity.Name = dto.Name;
      entity.Description = dto.Description;
      entity.IsClientData = dto.IsClientData;
      entity.ClientDataTag = dto.ClientDataTag;
      return entity;
    }

    public static ProgramArea CopyTo(this ProgramAreaDto dto)
    {
      var entity = new ProgramArea();
      return dto.CopyTo(entity);
    }

    #endregion

    #region ProgramAreaDegreeDto Mappers

    public static ProgramAreaDegreeDto AsDto(this ProgramAreaDegree entity)
    {
      var dto = new ProgramAreaDegreeDto
      {
        DegreeId = entity.DegreeId,
        ProgramAreaId = entity.ProgramAreaId,
        Id = entity.Id
      };
      return dto;
    }

    public static ProgramAreaDegree CopyTo(this ProgramAreaDegreeDto dto, ProgramAreaDegree entity)
    {
      entity.DegreeId = dto.DegreeId;
      entity.ProgramAreaId = dto.ProgramAreaId;
      return entity;
    }

    public static ProgramAreaDegree CopyTo(this ProgramAreaDegreeDto dto)
    {
      var entity = new ProgramAreaDegree();
      return dto.CopyTo(entity);
    }

    #endregion

    #region ProgramAreaDegreeViewDto Mappers

    public static ProgramAreaDegreeViewDto AsDto(this ProgramAreaDegreeView entity)
    {
      var dto = new ProgramAreaDegreeViewDto
      {
        ProgramAreaId = entity.ProgramAreaId,
        ProgramAreaName = entity.ProgramAreaName,
        DegreeId = entity.DegreeId,
        DegreeName = entity.DegreeName,
        IsClientData = entity.IsClientData,
        ClientDataTag = entity.ClientDataTag,
        Id = entity.Id
      };
      return dto;
    }

    public static ProgramAreaDegreeView CopyTo(this ProgramAreaDegreeViewDto dto, ProgramAreaDegreeView entity)
    {
      entity.ProgramAreaId = dto.ProgramAreaId;
      entity.ProgramAreaName = dto.ProgramAreaName;
      entity.DegreeId = dto.DegreeId;
      entity.DegreeName = dto.DegreeName;
      entity.IsClientData = dto.IsClientData;
      entity.ClientDataTag = dto.ClientDataTag;
      return entity;
    }

    public static ProgramAreaDegreeView CopyTo(this ProgramAreaDegreeViewDto dto)
    {
      var entity = new ProgramAreaDegreeView();
      return dto.CopyTo(entity);
    }

    #endregion

    #region ProgramAreaDegreeEducationLevelViewDto Mappers

    public static ProgramAreaDegreeEducationLevelViewDto AsDto(this ProgramAreaDegreeEducationLevelView entity)
    {
      var dto = new ProgramAreaDegreeEducationLevelViewDto
      {
        ProgramAreaId = entity.ProgramAreaId,
        ProgramAreaName = entity.ProgramAreaName,
        DegreeId = entity.DegreeId,
        DegreeName = entity.DegreeName,
        DegreeEducationLevelId = entity.DegreeEducationLevelId,
        EducationLevel = entity.EducationLevel,
        DegreeEducationLevelName = entity.DegreeEducationLevelName,
        IsClientData = entity.IsClientData,
        ClientDataTag = entity.ClientDataTag,
        Id = entity.Id
      };
      return dto;
    }

    public static ProgramAreaDegreeEducationLevelView CopyTo(this ProgramAreaDegreeEducationLevelViewDto dto, ProgramAreaDegreeEducationLevelView entity)
    {
      entity.ProgramAreaId = dto.ProgramAreaId;
      entity.ProgramAreaName = dto.ProgramAreaName;
      entity.DegreeId = dto.DegreeId;
      entity.DegreeName = dto.DegreeName;
      entity.DegreeEducationLevelId = dto.DegreeEducationLevelId;
      entity.EducationLevel = dto.EducationLevel;
      entity.DegreeEducationLevelName = dto.DegreeEducationLevelName;
      entity.IsClientData = dto.IsClientData;
      entity.ClientDataTag = dto.ClientDataTag;
      return entity;
    }

    public static ProgramAreaDegreeEducationLevelView CopyTo(this ProgramAreaDegreeEducationLevelViewDto dto)
    {
      var entity = new ProgramAreaDegreeEducationLevelView();
      return dto.CopyTo(entity);
    }

    #endregion

		#region OnetDto Mappers

		public static OnetDto AsDto(this Onet entity)
		{
			var dto = new OnetDto
			{
				OnetCode = entity.OnetCode,
				Key = entity.Key,
				JobFamilyId = entity.JobFamilyId,
				JobZone = entity.JobZone,
				Id = entity.Id
			};
			return dto;
		}

		public static Onet CopyTo(this OnetDto dto, Onet entity)
		{
			entity.OnetCode = dto.OnetCode;
			entity.Key = dto.Key;
			entity.JobFamilyId = dto.JobFamilyId;
			entity.JobZone = dto.JobZone;
			return entity;
		}

		public static Onet CopyTo(this OnetDto dto)
		{
			var entity = new Onet();
			return dto.CopyTo(entity);
		}

		#endregion

		#region OnetTaskDto Mappers

		public static OnetTaskDto AsDto(this OnetTask entity)
		{
			var dto = new OnetTaskDto
			{
				Key = entity.Key,
				OnetId = entity.OnetId,
				Id = entity.Id
			};
			return dto;
		}

		public static OnetTask CopyTo(this OnetTaskDto dto, OnetTask entity)
		{
			entity.Key = dto.Key;
			entity.OnetId = dto.OnetId;
			return entity;
		}

		public static OnetTask CopyTo(this OnetTaskDto dto)
		{
			var entity = new OnetTask();
			return dto.CopyTo(entity);
		}

		#endregion

		#region JobTaskDto Mappers

		public static JobTaskDto AsDto(this JobTask entity)
		{
			var dto = new JobTaskDto
			{
				JobTaskType = entity.JobTaskType,
				Key = entity.Key,
				Scope = entity.Scope,
				Certificate = entity.Certificate,
				OnetId = entity.OnetId,
				Id = entity.Id
			};
			return dto;
		}

		public static JobTask CopyTo(this JobTaskDto dto, JobTask entity)
		{
			entity.JobTaskType = dto.JobTaskType;
			entity.Key = dto.Key;
			entity.Scope = dto.Scope;
			entity.Certificate = dto.Certificate;
			entity.OnetId = dto.OnetId;
			return entity;
		}

		public static JobTask CopyTo(this JobTaskDto dto)
		{
			var entity = new JobTask();
			return dto.CopyTo(entity);
		}

		#endregion

		#region JobTaskMultiOptionDto Mappers

		public static JobTaskMultiOptionDto AsDto(this JobTaskMultiOption entity)
		{
			var dto = new JobTaskMultiOptionDto
			{
				Key = entity.Key,
				JobTaskId = entity.JobTaskId,
				Id = entity.Id
			};
			return dto;
		}

		public static JobTaskMultiOption CopyTo(this JobTaskMultiOptionDto dto, JobTaskMultiOption entity)
		{
			entity.Key = dto.Key;
			entity.JobTaskId = dto.JobTaskId;
			return entity;
		}

		public static JobTaskMultiOption CopyTo(this JobTaskMultiOptionDto dto)
		{
			var entity = new JobTaskMultiOption();
			return dto.CopyTo(entity);
		}

		#endregion

		#region OnetLocalisationItemDto Mappers

		public static OnetLocalisationItemDto AsDto(this OnetLocalisationItem entity)
		{
			var dto = new OnetLocalisationItemDto
			{
				Key = entity.Key,
				PrimaryValue = entity.PrimaryValue,
				SecondaryValue = entity.SecondaryValue,
				LocalisationId = entity.LocalisationId,
				Id = entity.Id
			};
			return dto;
		}

		public static OnetLocalisationItem CopyTo(this OnetLocalisationItemDto dto, OnetLocalisationItem entity)
		{
			entity.Key = dto.Key;
			entity.PrimaryValue = dto.PrimaryValue;
			entity.SecondaryValue = dto.SecondaryValue;
			entity.LocalisationId = dto.LocalisationId;
			return entity;
		}

		public static OnetLocalisationItem CopyTo(this OnetLocalisationItemDto dto)
		{
			var entity = new OnetLocalisationItem();
			return dto.CopyTo(entity);
		}

		#endregion

		#region JobTaskLocalisationItemDto Mappers

		public static JobTaskLocalisationItemDto AsDto(this JobTaskLocalisationItem entity)
		{
			var dto = new JobTaskLocalisationItemDto
			{
				Key = entity.Key,
				PrimaryValue = entity.PrimaryValue,
				SecondaryValue = entity.SecondaryValue,
				LocalisationId = entity.LocalisationId,
				Id = entity.Id
			};
			return dto;
		}

		public static JobTaskLocalisationItem CopyTo(this JobTaskLocalisationItemDto dto, JobTaskLocalisationItem entity)
		{
			entity.Key = dto.Key;
			entity.PrimaryValue = dto.PrimaryValue;
			entity.SecondaryValue = dto.SecondaryValue;
			entity.LocalisationId = dto.LocalisationId;
			return entity;
		}

		public static JobTaskLocalisationItem CopyTo(this JobTaskLocalisationItemDto dto)
		{
			var entity = new JobTaskLocalisationItem();
			return dto.CopyTo(entity);
		}

		#endregion

		#region JobTaskViewDto Mappers

		public static JobTaskViewDto AsDto(this JobTaskView entity)
		{
			var dto = new JobTaskViewDto
			{
				JobTaskType = entity.JobTaskType,
				OnetId = entity.OnetId,
				Culture = entity.Culture,
				Prompt = entity.Prompt,
				Response = entity.Response,
				Scope = entity.Scope,
				Certificate = entity.Certificate,
				OnetCode = entity.OnetCode,
				Id = entity.Id
			};
			return dto;
		}

		public static JobTaskView CopyTo(this JobTaskViewDto dto, JobTaskView entity)
		{
			entity.JobTaskType = dto.JobTaskType;
			entity.OnetId = dto.OnetId;
			entity.Culture = dto.Culture;
			entity.Prompt = dto.Prompt;
			entity.Response = dto.Response;
			entity.Scope = dto.Scope;
			entity.Certificate = dto.Certificate;
			entity.OnetCode = dto.OnetCode;
			return entity;
		}

		public static JobTaskView CopyTo(this JobTaskViewDto dto)
		{
			var entity = new JobTaskView();
			return dto.CopyTo(entity);
		}

		#endregion

		#region JobTaskMultiOptionViewDto Mappers

		public static JobTaskMultiOptionViewDto AsDto(this JobTaskMultiOptionView entity)
		{
			var dto = new JobTaskMultiOptionViewDto
			{
				OnetId = entity.OnetId,
				Culture = entity.Culture,
				Prompt = entity.Prompt,
				JobTaskId = entity.JobTaskId,
				Id = entity.Id
			};
			return dto;
		}

		public static JobTaskMultiOptionView CopyTo(this JobTaskMultiOptionViewDto dto, JobTaskMultiOptionView entity)
		{
			entity.OnetId = dto.OnetId;
			entity.Culture = dto.Culture;
			entity.Prompt = dto.Prompt;
			entity.JobTaskId = dto.JobTaskId;
			return entity;
		}

		public static JobTaskMultiOptionView CopyTo(this JobTaskMultiOptionViewDto dto)
		{
			var entity = new JobTaskMultiOptionView();
			return dto.CopyTo(entity);
		}

		#endregion

    #region OnetViewDto Mappers

    public static OnetViewDto AsDto(this OnetView entity)
    {
      var dto = new OnetViewDto
      {
        Key = entity.Key,
        Occupation = entity.Occupation,
        JobFamilyId = entity.JobFamilyId,
        OnetCode = entity.OnetCode,
        Culture = entity.Culture,
        Description = entity.Description,
        JobTasksAvailable = entity.JobTasksAvailable,
        Id = entity.Id
      };
      return dto;
    }

    public static OnetView CopyTo(this OnetViewDto dto, OnetView entity)
    {
      entity.Key = dto.Key;
      entity.Occupation = dto.Occupation;
      entity.JobFamilyId = dto.JobFamilyId;
      entity.OnetCode = dto.OnetCode;
      entity.Culture = dto.Culture;
      entity.Description = dto.Description;
      entity.JobTasksAvailable = dto.JobTasksAvailable;
      return entity;
    }

    public static OnetView CopyTo(this OnetViewDto dto)
    {
      var entity = new OnetView();
      return dto.CopyTo(entity);
    }

    #endregion

		#region NAICSDto Mappers

		public static NAICSDto AsDto(this NAICS entity)
		{
			var dto = new NAICSDto
			{
				Code = entity.Code,
				Name = entity.Name,
				ParentId = entity.ParentId,
				Id = entity.Id
			};
			return dto;
		}

		public static NAICS CopyTo(this NAICSDto dto, NAICS entity)
		{
			entity.Code = dto.Code;
			entity.Name = dto.Name;
			entity.ParentId = dto.ParentId;
			return entity;
		}

		public static NAICS CopyTo(this NAICSDto dto)
		{
			var entity = new NAICS();
			return dto.CopyTo(entity);
		}

		#endregion

		#region GenericJobTitleDto Mappers

		public static GenericJobTitleDto AsDto(this GenericJobTitle entity)
		{
			var dto = new GenericJobTitleDto
			{
				Value = entity.Value,
				Id = entity.Id
			};
			return dto;
		}

		public static GenericJobTitle CopyTo(this GenericJobTitleDto dto, GenericJobTitle entity)
		{
			entity.Value = dto.Value;
			return entity;
		}

		public static GenericJobTitle CopyTo(this GenericJobTitleDto dto)
		{
			var entity = new GenericJobTitle();
			return dto.CopyTo(entity);
		}

		#endregion

		#region OnetTaskViewDto Mappers

		public static OnetTaskViewDto AsDto(this OnetTaskView entity)
		{
			var dto = new OnetTaskViewDto
			{
				OnetId = entity.OnetId,
				TaskKey = entity.TaskKey,
				Task = entity.Task,
				Culture = entity.Culture,
				Id = entity.Id
			};
			return dto;
		}

		public static OnetTaskView CopyTo(this OnetTaskViewDto dto, OnetTaskView entity)
		{
			entity.OnetId = dto.OnetId;
			entity.TaskKey = dto.TaskKey;
			entity.Task = dto.Task;
			entity.Culture = dto.Culture;
			return entity;
		}

		public static OnetTaskView CopyTo(this OnetTaskViewDto dto)
		{
			var entity = new OnetTaskView();
			return dto.CopyTo(entity);
		}

		#endregion

    #region PostalCodeDto Mappers

    public static PostalCodeDto AsDto(this PostalCode entity)
    {
      var dto = new PostalCodeDto
      {
        Code = entity.Code,
        Longitude = entity.Longitude,
        Latitude = entity.Latitude,
        CountryKey = entity.CountryKey,
        StateKey = entity.StateKey,
        CountyKey = entity.CountyKey,
        CityName = entity.CityName,
        OfficeKey = entity.OfficeKey,
        WibLocationKey = entity.WibLocationKey,
        MsaId = entity.MsaId,
        Id = entity.Id
      };
      return dto;
    }

    public static PostalCode CopyTo(this PostalCodeDto dto, PostalCode entity)
    {
      entity.Code = dto.Code;
      entity.Longitude = dto.Longitude;
      entity.Latitude = dto.Latitude;
      entity.CountryKey = dto.CountryKey;
      entity.StateKey = dto.StateKey;
      entity.CountyKey = dto.CountyKey;
      entity.CityName = dto.CityName;
      entity.OfficeKey = dto.OfficeKey;
      entity.WibLocationKey = dto.WibLocationKey;
      entity.MsaId = dto.MsaId;
      return entity;
    }

    public static PostalCode CopyTo(this PostalCodeDto dto)
    {
      var entity = new PostalCode();
      return dto.CopyTo(entity);
    }

    #endregion

		#region PostalCodeViewDto Mappers

		public static PostalCodeViewDto AsDto(this PostalCodeView entity)
		{
			var dto = new PostalCodeViewDto
			{
				Code = entity.Code,
				Longitude = entity.Longitude,
				Latitude = entity.Latitude,
				CountryKey = entity.CountryKey,
				CountryName = entity.CountryName,
				StateKey = entity.StateKey,
				StateName = entity.StateName,
				Culture = entity.Culture,
				CountyKey = entity.CountyKey,
				CountyName = entity.CountyName,
				CityName = entity.CityName,
				Id = entity.Id
			};
			return dto;
		}

		public static PostalCodeView CopyTo(this PostalCodeViewDto dto, PostalCodeView entity)
		{
			entity.Code = dto.Code;
			entity.Longitude = dto.Longitude;
			entity.Latitude = dto.Latitude;
			entity.CountryKey = dto.CountryKey;
			entity.CountryName = dto.CountryName;
			entity.StateKey = dto.StateKey;
			entity.StateName = dto.StateName;
			entity.Culture = dto.Culture;
			entity.CountyKey = dto.CountyKey;
			entity.CountyName = dto.CountyName;
			entity.CityName = dto.CityName;
			return entity;
		}

		public static PostalCodeView CopyTo(this PostalCodeViewDto dto)
		{
			var entity = new PostalCodeView();
			return dto.CopyTo(entity);
		}

		#endregion

		#region OnetWordJobTaskViewDto Mappers

		public static OnetWordJobTaskViewDto AsDto(this OnetWordJobTaskView entity)
		{
			var dto = new OnetWordJobTaskViewDto
			{
				OnetId = entity.OnetId,
				OnetRingId = entity.OnetRingId,
				OnetSoc = entity.OnetSoc,
				Stem = entity.Stem,
				Word = entity.Word,
				JobFamilyId = entity.JobFamilyId,
				JobZone = entity.JobZone,
				OnetKey = entity.OnetKey,
				OnetCode = entity.OnetCode,
				Frequency = entity.Frequency,
				Id = entity.Id
			};
			return dto;
		}

		public static OnetWordJobTaskView CopyTo(this OnetWordJobTaskViewDto dto, OnetWordJobTaskView entity)
		{
			entity.OnetId = dto.OnetId;
			entity.OnetRingId = dto.OnetRingId;
			entity.OnetSoc = dto.OnetSoc;
			entity.Stem = dto.Stem;
			entity.Word = dto.Word;
			entity.JobFamilyId = dto.JobFamilyId;
			entity.JobZone = dto.JobZone;
			entity.OnetKey = dto.OnetKey;
			entity.OnetCode = dto.OnetCode;
			entity.Frequency = dto.Frequency;
			return entity;
		}

		public static OnetWordJobTaskView CopyTo(this OnetWordJobTaskViewDto dto)
		{
			var entity = new OnetWordJobTaskView();
			return dto.CopyTo(entity);
		}

		#endregion

		#region OnetWordViewDto Mappers

		public static OnetWordViewDto AsDto(this OnetWordView entity)
		{
			var dto = new OnetWordViewDto
			{
				OnetId = entity.OnetId,
				OnetRingId = entity.OnetRingId,
				OnetSoc = entity.OnetSoc,
				Stem = entity.Stem,
				Word = entity.Word,
				JobFamilyId = entity.JobFamilyId,
				JobZone = entity.JobZone,
				OnetKey = entity.OnetKey,
				OnetCode = entity.OnetCode,
				Frequency = entity.Frequency,
				Id = entity.Id
			};
			return dto;
		}

		public static OnetWordView CopyTo(this OnetWordViewDto dto, OnetWordView entity)
		{
			entity.OnetId = dto.OnetId;
			entity.OnetRingId = dto.OnetRingId;
			entity.OnetSoc = dto.OnetSoc;
			entity.Stem = dto.Stem;
			entity.Word = dto.Word;
			entity.JobFamilyId = dto.JobFamilyId;
			entity.JobZone = dto.JobZone;
			entity.OnetKey = dto.OnetKey;
			entity.OnetCode = dto.OnetCode;
			entity.Frequency = dto.Frequency;
			return entity;
		}

		public static OnetWordView CopyTo(this OnetWordViewDto dto)
		{
			var entity = new OnetWordView();
			return dto.CopyTo(entity);
		}

		#endregion
		
		#region OnetCommodityDto Mappers

		public static OnetCommodityDto AsDto(this OnetCommodity entity)
		{
			var dto = new OnetCommodityDto
			{
				ToolTechnologyType = entity.ToolTechnologyType,
				ToolTechnologyExample = entity.ToolTechnologyExample,
				Code = entity.Code,
				Title = entity.Title,
				OnetId = entity.OnetId,
				Id = entity.Id
			};
			return dto;
		}

		public static OnetCommodity CopyTo(this OnetCommodityDto dto, OnetCommodity entity)
		{
			entity.ToolTechnologyType = dto.ToolTechnologyType;
			entity.ToolTechnologyExample = dto.ToolTechnologyExample;
			entity.Code = dto.Code;
			entity.Title = dto.Title;
			entity.OnetId = dto.OnetId;
			return entity;
		}

		public static OnetCommodity CopyTo(this OnetCommodityDto dto)
		{
			var entity = new OnetCommodity();
			return dto.CopyTo(entity);
		}

		#endregion

		#region StandardIndustrialClassificationDto Mappers

		public static StandardIndustrialClassificationDto AsDto(this StandardIndustrialClassification entity)
		{
			var dto = new StandardIndustrialClassificationDto
			{
				Name = entity.Name,
				SicPrimary = entity.SicPrimary,
				SicSecondary = entity.SicSecondary,
				NaicsPrimary = entity.NaicsPrimary,
				NaicsSecondary = entity.NaicsSecondary,
				Id = entity.Id
			};
			return dto;
		}

		public static StandardIndustrialClassification CopyTo(this StandardIndustrialClassificationDto dto, StandardIndustrialClassification entity)
		{
			entity.Name = dto.Name;
			entity.SicPrimary = dto.SicPrimary;
			entity.SicSecondary = dto.SicSecondary;
			entity.NaicsPrimary = dto.NaicsPrimary;
			entity.NaicsSecondary = dto.NaicsSecondary;
			return entity;
		}

		public static StandardIndustrialClassification CopyTo(this StandardIndustrialClassificationDto dto)
		{
			var entity = new StandardIndustrialClassification();
			return dto.CopyTo(entity);
		}

		#endregion

		#region ROnetDto Mappers

		public static ROnetDto AsDto(this ROnet entity)
		{
			var dto = new ROnetDto
			{
				Code = entity.Code,
				Key = entity.Key,
				Id = entity.Id
			};
			return dto;
		}

		public static ROnet CopyTo(this ROnetDto dto, ROnet entity)
		{
			entity.Code = dto.Code;
			entity.Key = dto.Key;
			return entity;
		}

		public static ROnet CopyTo(this ROnetDto dto)
		{
			var entity = new ROnet();
			return dto.CopyTo(entity);
		}

		#endregion

		#region OnetROnetDto Mappers

		public static OnetROnetDto AsDto(this OnetROnet entity)
		{
			var dto = new OnetROnetDto
			{
				OnetId = entity.OnetId,
				ROnetId = entity.ROnetId,
				BestFit = entity.BestFit,
				Id = entity.Id
			};
			return dto;
		}

		public static OnetROnet CopyTo(this OnetROnetDto dto, OnetROnet entity)
		{
			entity.OnetId = dto.OnetId;
			entity.ROnetId = dto.ROnetId;
			entity.BestFit = dto.BestFit;
			return entity;
		}

		public static OnetROnet CopyTo(this OnetROnetDto dto)
		{
			var entity = new OnetROnet();
			return dto.CopyTo(entity);
		}

		#endregion

		#region MilitaryOccupationDto Mappers

		public static MilitaryOccupationDto AsDto(this MilitaryOccupation entity)
		{
			var dto = new MilitaryOccupationDto
			{
				Code = entity.Code,
				Key = entity.Key,
				BranchOfServiceId = entity.BranchOfServiceId,
				IsCommissioned = entity.IsCommissioned,
				MilitaryOccupationGroupId = entity.MilitaryOccupationGroupId,
				Id = entity.Id
			};
			return dto;
		}

		public static MilitaryOccupation CopyTo(this MilitaryOccupationDto dto, MilitaryOccupation entity)
		{
			entity.Code = dto.Code;
			entity.Key = dto.Key;
			entity.BranchOfServiceId = dto.BranchOfServiceId;
			entity.IsCommissioned = dto.IsCommissioned;
			entity.MilitaryOccupationGroupId = dto.MilitaryOccupationGroupId;
			return entity;
		}

		public static MilitaryOccupation CopyTo(this MilitaryOccupationDto dto)
		{
			var entity = new MilitaryOccupation();
			return dto.CopyTo(entity);
		}

		#endregion

		#region MilitaryOccupationGroupDto Mappers

		public static MilitaryOccupationGroupDto AsDto(this MilitaryOccupationGroup entity)
		{
			var dto = new MilitaryOccupationGroupDto
			{
				Name = entity.Name,
				Id = entity.Id,
				IsCommissioned = entity.IsCommissioned
			};
			return dto;
		}

		public static MilitaryOccupationGroup CopyTo(this MilitaryOccupationGroupDto dto, MilitaryOccupationGroup entity)
		{
			entity.Name = dto.Name;
			entity.IsCommissioned = dto.IsCommissioned;
			return entity;
		}

		public static MilitaryOccupationGroup CopyTo(this MilitaryOccupationGroupDto dto)
		{
			var entity = new MilitaryOccupationGroup();
			return dto.CopyTo(entity);
		}

		#endregion

		#region MilitaryOccupationJobTitleViewDto Mappers

		public static MilitaryOccupationJobTitleViewDto AsDto(this MilitaryOccupationJobTitleView entity)
		{
			var dto = new MilitaryOccupationJobTitleViewDto
			{
				MilitaryOccupationId = entity.MilitaryOccupationId,
				JobTitle = entity.JobTitle,
				BranchOfServiceId = entity.BranchOfServiceId,
				Id = entity.Id
			};
			return dto;
		}

		public static MilitaryOccupationJobTitleView CopyTo(this MilitaryOccupationJobTitleViewDto dto, MilitaryOccupationJobTitleView entity)
		{
			entity.MilitaryOccupationId = dto.MilitaryOccupationId;
			entity.JobTitle = dto.JobTitle;
			entity.BranchOfServiceId = dto.BranchOfServiceId;
			return entity;
		}

		public static MilitaryOccupationJobTitleView CopyTo(this MilitaryOccupationJobTitleViewDto dto)
		{
			var entity = new MilitaryOccupationJobTitleView();
			return dto.CopyTo(entity);
		}

		#endregion

		#region MilitaryOccupationROnetDto Mappers

		public static MilitaryOccupationROnetDto AsDto(this MilitaryOccupationROnet entity)
		{
			var dto = new MilitaryOccupationROnetDto
			{
				ROnetId = entity.ROnetId,
				MilitaryOccupationId = entity.MilitaryOccupationId,
				Id = entity.Id
			};
			return dto;
		}

		public static MilitaryOccupationROnet CopyTo(this MilitaryOccupationROnetDto dto, MilitaryOccupationROnet entity)
		{
			entity.ROnetId = dto.ROnetId;
			entity.MilitaryOccupationId = dto.MilitaryOccupationId;
			return entity;
		}

		public static MilitaryOccupationROnet CopyTo(this MilitaryOccupationROnetDto dto)
		{
			var entity = new MilitaryOccupationROnet();
			return dto.CopyTo(entity);
		}

		#endregion

		#region MilitaryOccupationGroupROnetDto Mappers

		public static MilitaryOccupationGroupROnetDto AsDto(this MilitaryOccupationGroupROnet entity)
		{
			var dto = new MilitaryOccupationGroupROnetDto
			{
				ROnetId = entity.ROnetId,
				MilitaryOccupationGroupId = entity.MilitaryOccupationGroupId,
				Id = entity.Id
			};
			return dto;
		}

		public static MilitaryOccupationGroupROnet CopyTo(this MilitaryOccupationGroupROnetDto dto, MilitaryOccupationGroupROnet entity)
		{
			entity.ROnetId = dto.ROnetId;
			entity.MilitaryOccupationGroupId = dto.MilitaryOccupationGroupId;
			return entity;
		}

		public static MilitaryOccupationGroupROnet CopyTo(this MilitaryOccupationGroupROnetDto dto)
		{
			var entity = new MilitaryOccupationGroupROnet();
			return dto.CopyTo(entity);
		}

		#endregion

		#region OnetRingDto Mappers

		public static OnetRingDto AsDto(this OnetRing entity)
		{
			var dto = new OnetRingDto
			{
				Name = entity.Name,
				Weighting = entity.Weighting,
				RingMax = entity.RingMax,
				Id = entity.Id
			};
			return dto;
		}

		public static OnetRing CopyTo(this OnetRingDto dto, OnetRing entity)
		{
			entity.Name = dto.Name;
			entity.Weighting = dto.Weighting;
			entity.RingMax = dto.RingMax;
			return entity;
		}

		public static OnetRing CopyTo(this OnetRingDto dto)
		{
			var entity = new OnetRing();
			return dto.CopyTo(entity);
		}

		#endregion

    #region SkillsForJobsViewDto Mappers

    public static SkillsForJobsViewDto AsDto(this SkillsForJobsView entity)
    {
      var dto = new SkillsForJobsViewDto
      {
        Name = entity.Name,
        SkillType = entity.SkillType,
        Rank = entity.Rank,
        DemandPercentile = entity.DemandPercentile,
        Id = entity.Id
      };
      return dto;
    }

    public static SkillsForJobsView CopyTo(this SkillsForJobsViewDto dto, SkillsForJobsView entity)
    {
      entity.Name = dto.Name;
      entity.SkillType = dto.SkillType;
      entity.Rank = dto.Rank;
      entity.DemandPercentile = dto.DemandPercentile;
      return entity;
    }

    public static SkillsForJobsView CopyTo(this SkillsForJobsViewDto dto)
    {
      var entity = new SkillsForJobsView();
      return dto.CopyTo(entity);
    }

    #endregion

    #region CareerAreaJobDegreeViewDto Mappers

    public static CareerAreaJobDegreeViewDto AsDto(this CareerAreaJobDegreeView entity)
    {
      var dto = new CareerAreaJobDegreeViewDto
      {
        CareerAreaId = entity.CareerAreaId,
        JobId = entity.JobId,
        DegreeEducationLevelId = entity.DegreeEducationLevelId,
        EducationLevel = entity.EducationLevel,
        DegreeId = entity.DegreeId,
        IsClientData = entity.IsClientData,
        ClientDataTag = entity.ClientDataTag,
        Id = entity.Id
      };
      return dto;
    }

    public static CareerAreaJobDegreeView CopyTo(this CareerAreaJobDegreeViewDto dto, CareerAreaJobDegreeView entity)
    {
      entity.CareerAreaId = dto.CareerAreaId;
      entity.JobId = dto.JobId;
      entity.DegreeEducationLevelId = dto.DegreeEducationLevelId;
      entity.EducationLevel = dto.EducationLevel;
      entity.DegreeId = dto.DegreeId;
      entity.IsClientData = dto.IsClientData;
      entity.ClientDataTag = dto.ClientDataTag;
      return entity;
    }

    public static CareerAreaJobDegreeView CopyTo(this CareerAreaJobDegreeViewDto dto)
    {
      var entity = new CareerAreaJobDegreeView();
      return dto.CopyTo(entity);
    }

    #endregion

    #region CareerAreaJobReportViewDto Mappers

    public static CareerAreaJobReportViewDto AsDto(this CareerAreaJobReportView entity)
    {
      var dto = new CareerAreaJobReportViewDto
      {
        CareerAreaId = entity.CareerAreaId,
        StateAreaId = entity.StateAreaId,
        JobId = entity.JobId,
        HiringDemand = entity.HiringDemand,
        Id = entity.Id
      };
      return dto;
    }

    public static CareerAreaJobReportView CopyTo(this CareerAreaJobReportViewDto dto, CareerAreaJobReportView entity)
    {
      entity.CareerAreaId = dto.CareerAreaId;
      entity.StateAreaId = dto.StateAreaId;
      entity.JobId = dto.JobId;
      entity.HiringDemand = dto.HiringDemand;
      return entity;
    }

    public static CareerAreaJobReportView CopyTo(this CareerAreaJobReportViewDto dto)
    {
      var entity = new CareerAreaJobReportView();
      return dto.CopyTo(entity);
    }

    #endregion

		#region DegreeEducationLevelLensMappingDto Mappers

		public static DegreeEducationLevelLensMappingDto AsDto(this DegreeEducationLevelLensMapping entity)
		{
			var dto = new DegreeEducationLevelLensMappingDto
			{
				LensId = entity.LensId,
				DegreeEducationLevelId = entity.DegreeEducationLevelId,
				RcipCode = entity.RcipCode,
				Id = entity.Id,
				ProgramAreaId = entity.ProgramAreaId
			};
			return dto;
		}

		public static DegreeEducationLevelLensMapping CopyTo(this DegreeEducationLevelLensMappingDto dto, DegreeEducationLevelLensMapping entity)
		{
			entity.LensId = dto.LensId;
			entity.DegreeEducationLevelId = dto.DegreeEducationLevelId;
			entity.RcipCode = dto.RcipCode;
			entity.ProgramAreaId = dto.ProgramAreaId;
			return entity;
		}

		public static DegreeEducationLevelLensMapping CopyTo(this DegreeEducationLevelLensMappingDto dto)
		{
			var entity = new DegreeEducationLevelLensMapping();
			return dto.CopyTo(entity);
		}

		#endregion

		#region Onet17Onet12MappingDto Mappers

		public static Onet17Onet12MappingDto AsDto(this Onet17Onet12Mapping entity)
		{
			var dto = new Onet17Onet12MappingDto
			{
				Onet17Code = entity.Onet17Code,
				OnetId = entity.OnetId,
				Id = entity.Id
			};
			return dto;
		}

		public static Onet17Onet12Mapping CopyTo(this Onet17Onet12MappingDto dto, Onet17Onet12Mapping entity)
		{
			entity.Onet17Code = dto.Onet17Code;
			entity.OnetId = dto.OnetId;
			return entity;
		}

		public static Onet17Onet12Mapping CopyTo(this Onet17Onet12MappingDto dto)
		{
			var entity = new Onet17Onet12Mapping();
			return dto.CopyTo(entity);
		}

		#endregion

		#region Onet17Onet12MappingViewDto Mappers

		public static Onet17Onet12MappingViewDto AsDto(this Onet17Onet12MappingView entity)
		{
			var dto = new Onet17Onet12MappingViewDto
			{
				Onet17Code = entity.Onet17Code,
				Onet12Code = entity.Onet12Code,
				OnetId = entity.OnetId,
				Id = entity.Id
			};
			return dto;
		}

		public static Onet17Onet12MappingView CopyTo(this Onet17Onet12MappingViewDto dto, Onet17Onet12MappingView entity)
		{
			entity.Onet17Code = dto.Onet17Code;
			entity.Onet12Code = dto.Onet12Code;
			entity.OnetId = dto.OnetId;
			return entity;
		}

		public static Onet17Onet12MappingView CopyTo(this Onet17Onet12MappingViewDto dto)
		{
			var entity = new Onet17Onet12MappingView();
			return dto.CopyTo(entity);
		}

		#endregion

		#region ROnetOnetDto Mappers

		public static ROnetOnetDto AsDto(this ROnetOnet entity)
		{
			var dto = new ROnetOnetDto
			{
				BestFit = entity.BestFit,
				OnetId = entity.OnetId,
				ROnetId = entity.ROnetId,
				Id = entity.Id
			};
			return dto;
		}

		public static ROnetOnet CopyTo(this ROnetOnetDto dto, ROnetOnet entity)
		{
			entity.BestFit = dto.BestFit;
			entity.OnetId = dto.OnetId;
			entity.ROnetId = dto.ROnetId;
			return entity;
		}

		public static ROnetOnet CopyTo(this ROnetOnetDto dto)
		{
			var entity = new ROnetOnet();
			return dto.CopyTo(entity);
		}

		#endregion
    
    #region OnetToROnetConversionViewDto Mappers

    public static OnetToROnetConversionViewDto AsDto(this OnetToROnetConversionView entity)
    {
      var dto = new OnetToROnetConversionViewDto
      {
        OnetId = entity.OnetId,
        OnetCode = entity.OnetCode,
        OnetKey = entity.OnetKey,
        ROnetId = entity.ROnetId,
        ROnetCode = entity.ROnetCode,
        ROnetKey = entity.ROnetKey,
        BestFit = entity.BestFit,
        Id = entity.Id
      };
      return dto;
    }

    public static OnetToROnetConversionView CopyTo(this OnetToROnetConversionViewDto dto, OnetToROnetConversionView entity)
    {
      entity.OnetId = dto.OnetId;
      entity.OnetCode = dto.OnetCode;
      entity.OnetKey = dto.OnetKey;
      entity.ROnetId = dto.ROnetId;
      entity.ROnetCode = dto.ROnetCode;
      entity.ROnetKey = dto.ROnetKey;
      entity.BestFit = dto.BestFit;
      return entity;
    }

    public static OnetToROnetConversionView CopyTo(this OnetToROnetConversionViewDto dto)
    {
      var entity = new OnetToROnetConversionView();
      return dto.CopyTo(entity);
    }

    #endregion

    #region SOCDto Mappers

    public static SOCDto AsDto(this SOC entity)
    {
      var dto = new SOCDto
      {
        Soc2010Code = entity.Soc2010Code,
        Key = entity.Key,
        Title = entity.Title,
        Id = entity.Id
      };
      return dto;
    }

    public static SOC CopyTo(this SOCDto dto, SOC entity)
    {
      entity.Soc2010Code = dto.Soc2010Code;
      entity.Key = dto.Key;
      entity.Title = dto.Title;
      return entity;
    }

    public static SOC CopyTo(this SOCDto dto)
    {
      var entity = new SOC();
      return dto.CopyTo(entity);
    }

    #endregion
  }
}