﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Models;
using Focus.Data.Core.Entities;

#endregion

namespace Focus.Services.DtoMappers
{
    public static class
        CoreMappers
    {

        #region ActionEventDto Mappers

        public static ActionEventDto AsDto(this ActionEvent entity)
        {
            var dto = new ActionEventDto
            {
                SessionId = entity.SessionId,
                RequestId = entity.RequestId,
                UserId = entity.UserId,
                ActionedOn = entity.ActionedOn,
                EntityId = entity.EntityId,
                EntityIdAdditional01 = entity.EntityIdAdditional01,
                EntityIdAdditional02 = entity.EntityIdAdditional02,
                AdditionalDetails = entity.AdditionalDetails,
                EntityTypeId = entity.EntityTypeId,
                ActionTypeId = entity.ActionTypeId,
                ActivityStatusId = entity.ActivityStatusId,
                ActivityDeletedOn = entity.ActivityDeletedOn,
                ActivityUpdatedBy = entity.ActivityUpdatedBy,
                Id = entity.Id
            };
            return dto;
        }

        public static ActionEvent CopyTo(this ActionEventDto dto, ActionEvent entity)
        {
            entity.SessionId = dto.SessionId;
            entity.RequestId = dto.RequestId;
            entity.UserId = dto.UserId;
            entity.ActionedOn = dto.ActionedOn;
            entity.EntityId = dto.EntityId;
            entity.EntityIdAdditional01 = dto.EntityIdAdditional01;
            entity.EntityIdAdditional02 = dto.EntityIdAdditional02;
            entity.AdditionalDetails = dto.AdditionalDetails;
            entity.EntityTypeId = dto.EntityTypeId;
            entity.ActionTypeId = dto.ActionTypeId;
            entity.ActivityStatusId = dto.ActivityStatusId;
            entity.ActivityDeletedOn = dto.ActivityDeletedOn;
            entity.ActivityUpdatedBy = dto.ActivityUpdatedBy;
            return entity;
        }

        public static ActionEvent CopyTo(this ActionEventDto dto)
        {
            var entity = new ActionEvent();
            return dto.CopyTo(entity);
        }

        #endregion

        #region UserDto Mappers

        public static UserDto AsDto(this User entity)
        {
            var dto = new UserDto
            {
                CreatedOn = entity.CreatedOn,
                UpdatedOn = entity.UpdatedOn,
                DeletedOn = entity.DeletedOn,
                UserName = entity.UserName,
                PasswordHash = entity.PasswordHash,
                PasswordSalt = entity.PasswordSalt,
                Enabled = entity.Enabled,
                ValidationKey = entity.ValidationKey,
                UserType = entity.UserType,
                LoggedInOn = entity.LoggedInOn,
                LastLoggedInOn = entity.LastLoggedInOn,
                ExternalId = entity.ExternalId,
                IsClientAuthenticated = entity.IsClientAuthenticated,
                ScreenName = entity.ScreenName,
                IsMigrated = entity.IsMigrated,
                RegulationsConsent = entity.RegulationsConsent,
                Pin = entity.Pin,
                FirstLoggedInOn = entity.FirstLoggedInOn,
                Blocked = entity.Blocked,
                ValidationKeyExpiry = entity.ValidationKeyExpiry,
                AccountDisabledReason = entity.AccountDisabledReason,
                MigratedPasswordHash = entity.MigratedPasswordHash,
                MigrationId = entity.MigrationId,
                PasswordAttempts = entity.PasswordAttempts,
                BlockedReason = entity.BlockedReason,
                PersonId = entity.PersonId,
                Id = entity.Id
            };
            return dto;
        }

        public static User CopyTo(this UserDto dto, User entity)
        {
            entity.UserName = dto.UserName;
            entity.PasswordHash = dto.PasswordHash;
            entity.PasswordSalt = dto.PasswordSalt;
            entity.Enabled = dto.Enabled;
            entity.ValidationKey = dto.ValidationKey;
            entity.UserType = dto.UserType;
            entity.LoggedInOn = dto.LoggedInOn;
            entity.LastLoggedInOn = dto.LastLoggedInOn;
            entity.ExternalId = dto.ExternalId;
            entity.IsClientAuthenticated = dto.IsClientAuthenticated;
            entity.ScreenName = dto.ScreenName;
            entity.IsMigrated = dto.IsMigrated;
            entity.RegulationsConsent = dto.RegulationsConsent;
            entity.Pin = dto.Pin;
            entity.FirstLoggedInOn = dto.FirstLoggedInOn;
            entity.Blocked = dto.Blocked;
            entity.ValidationKeyExpiry = dto.ValidationKeyExpiry;
            entity.AccountDisabledReason = dto.AccountDisabledReason;
            entity.MigratedPasswordHash = dto.MigratedPasswordHash;
            entity.MigrationId = dto.MigrationId;
            entity.PasswordAttempts = dto.PasswordAttempts;
            entity.BlockedReason = dto.BlockedReason;
            entity.PersonId = dto.PersonId;
            return entity;
        }

        public static User CopyTo(this UserDto dto)
        {
            var entity = new User();
            return dto.CopyTo(entity);
        }

        #endregion

        #region UserSecurityQuestionDto Mappers

        public static UserSecurityQuestionDto AsDto(this UserSecurityQuestion entity)
        {
            var dto = new UserSecurityQuestionDto
            {
                UserId = entity.UserId,
                QuestionIndex = entity.QuestionIndex,
                SecurityQuestion = entity.SecurityQuestion,
                SecurityQuestionId = entity.SecurityQuestionId,
                SecurityAnswerHash = entity.SecurityAnswerHash,
                SecurityAnswerEncrypted = entity.SecurityAnswerEncrypted,
                Id = entity.Id
            };
            return dto;
        }

        public static UserSecurityQuestion CopyTo(this UserSecurityQuestionDto dto, UserSecurityQuestion entity)
        {
            entity.UserId = dto.UserId;
            entity.QuestionIndex = dto.QuestionIndex;
            entity.SecurityQuestion = dto.SecurityQuestion;
            entity.SecurityQuestionId = dto.SecurityQuestionId;
            entity.SecurityAnswerHash = dto.SecurityAnswerHash;
            entity.SecurityAnswerEncrypted = dto.SecurityAnswerEncrypted;
            return entity;
        }

        public static UserSecurityQuestion CopyTo(this UserSecurityQuestionDto dto)
        {
            var entity = new UserSecurityQuestion();
            return dto.CopyTo(entity);
        }

        #endregion

        #region BusinessUnitDto Mappers

        public static BusinessUnitDto AsDto(this BusinessUnit entity)
        {
            var dto = new BusinessUnitDto
            {
                LockVersion = entity.LockVersion,
                Name = entity.Name,
                IsPrimary = entity.IsPrimary,
                Url = entity.Url,
                OwnershipTypeId = entity.OwnershipTypeId,
                IndustrialClassification = entity.IndustrialClassification,
                PrimaryPhone = entity.PrimaryPhone,
                PrimaryPhoneExtension = entity.PrimaryPhoneExtension,
                PrimaryPhoneType = entity.PrimaryPhoneType,
                AlternatePhone1 = entity.AlternatePhone1,
                AlternatePhone1Type = entity.AlternatePhone1Type,
                AlternatePhone2 = entity.AlternatePhone2,
                AlternatePhone2Type = entity.AlternatePhone2Type,
                IsPreferred = entity.IsPreferred,
                AccountTypeId = entity.AccountTypeId,
                NoOfEmployees = entity.NoOfEmployees,
                ApprovalStatus = entity.ApprovalStatus,
                ExternalId = entity.ExternalId,
                LegalName = entity.LegalName,
                ApprovalStatusReason = entity.ApprovalStatusReason,
                RedProfanityWords = entity.RedProfanityWords,
                YellowProfanityWords = entity.YellowProfanityWords,
                AwaitingApprovalDate = entity.AwaitingApprovalDate,
                EmployerId = entity.EmployerId,
                Id = entity.Id
            };
            return dto;
        }

        public static BusinessUnit CopyTo(this BusinessUnitDto dto, BusinessUnit entity)
        {
            entity.Name = dto.Name;
            entity.IsPrimary = dto.IsPrimary;
            entity.Url = dto.Url;
            entity.OwnershipTypeId = dto.OwnershipTypeId;
            entity.IndustrialClassification = dto.IndustrialClassification;
            entity.PrimaryPhone = dto.PrimaryPhone;
            entity.PrimaryPhoneExtension = dto.PrimaryPhoneExtension;
            entity.PrimaryPhoneType = dto.PrimaryPhoneType;
            entity.AlternatePhone1 = dto.AlternatePhone1;
            entity.AlternatePhone1Type = dto.AlternatePhone1Type;
            entity.AlternatePhone2 = dto.AlternatePhone2;
            entity.AlternatePhone2Type = dto.AlternatePhone2Type;
            entity.IsPreferred = dto.IsPreferred;
            entity.AccountTypeId = dto.AccountTypeId;
            entity.NoOfEmployees = dto.NoOfEmployees;
            entity.ApprovalStatus = dto.ApprovalStatus;
            entity.ExternalId = dto.ExternalId;
            entity.LegalName = dto.LegalName;
            entity.ApprovalStatusReason = dto.ApprovalStatusReason;
            entity.RedProfanityWords = dto.RedProfanityWords;
            entity.YellowProfanityWords = dto.YellowProfanityWords;
            entity.AwaitingApprovalDate = dto.AwaitingApprovalDate;
            entity.EmployerId = dto.EmployerId;
            return entity;
        }

        public static BusinessUnit CopyTo(this BusinessUnitDto dto)
        {
            var entity = new BusinessUnit();
            return dto.CopyTo(entity);
        }

        #endregion

        #region EmployeeDto Mappers

        public static EmployeeDto AsDto(this Employee entity)
        {
            var dto = new EmployeeDto
            {
                ApprovalStatus = entity.ApprovalStatus,
                StaffUserId = entity.StaffUserId,
                RedProfanityWords = entity.RedProfanityWords,
                YellowProfanityWords = entity.YellowProfanityWords,
                ApprovalStatusChangedBy = entity.ApprovalStatusChangedBy,
                ApprovalStatusChangedOn = entity.ApprovalStatusChangedOn,
                ApprovalStatusReason = entity.ApprovalStatusReason,
                AwaitingApprovalDate = entity.AwaitingApprovalDate,
                EmployerId = entity.EmployerId,
                PersonId = entity.PersonId,
                Id = entity.Id
            };
            return dto;
        }

        public static Employee CopyTo(this EmployeeDto dto, Employee entity)
        {
            entity.ApprovalStatus = dto.ApprovalStatus;
            entity.StaffUserId = dto.StaffUserId;
            entity.RedProfanityWords = dto.RedProfanityWords;
            entity.YellowProfanityWords = dto.YellowProfanityWords;
            entity.ApprovalStatusChangedBy = dto.ApprovalStatusChangedBy;
            entity.ApprovalStatusChangedOn = dto.ApprovalStatusChangedOn;
            entity.ApprovalStatusReason = dto.ApprovalStatusReason;
            entity.AwaitingApprovalDate = dto.AwaitingApprovalDate;
            entity.EmployerId = dto.EmployerId;
            entity.PersonId = dto.PersonId;
            return entity;
        }

        public static Employee CopyTo(this EmployeeDto dto)
        {
            var entity = new Employee();
            return dto.CopyTo(entity);
        }

        #endregion

        #region ReferralEmailDto Mappers

        public static ReferralEmailDto AsDto(this ReferralEmail entity)
        {
            var dto = new ReferralEmailDto
            {
                EmailText = entity.EmailText,
                EmailSentOn = entity.EmailSentOn,
                EmailSentBy = entity.EmailSentBy,
                ApprovalStatus = entity.ApprovalStatus,
                PersonId = entity.PersonId,
                EmployeeId = entity.EmployeeId,
                ApplicationId = entity.ApplicationId,
                Id = entity.Id
            };
            return dto;
        }

        public static ReferralEmail CopyTo(this ReferralEmailDto dto, ReferralEmail entity)
        {
            entity.EmailText = dto.EmailText;
            entity.EmailSentOn = dto.EmailSentOn;
            entity.EmailSentBy = dto.EmailSentBy;
            entity.ApprovalStatus = dto.ApprovalStatus;
            entity.PersonId = dto.PersonId;
            entity.EmployeeId = dto.EmployeeId;
            entity.ApplicationId = dto.ApplicationId;
            return entity;
        }

        public static ReferralEmail CopyTo(this ReferralEmailDto dto)
        {
            var entity = new ReferralEmail();
            return dto.CopyTo(entity);
        }

        #endregion

        #region EmployerDto Mappers

        public static EmployerDto AsDto(this Employer entity)
        {
            var dto = new EmployerDto
            {
                CreatedOn = entity.CreatedOn,
                UpdatedOn = entity.UpdatedOn,
                Name = entity.Name,
                CommencedOn = entity.CommencedOn,
                FederalEmployerIdentificationNumber = entity.FederalEmployerIdentificationNumber,
                IsValidFederalEmployerIdentificationNumber = entity.IsValidFederalEmployerIdentificationNumber,
                StateEmployerIdentificationNumber = entity.StateEmployerIdentificationNumber,
                ApprovalStatus = entity.ApprovalStatus,
                Url = entity.Url,
                ExpiredOn = entity.ExpiredOn,
                OwnershipTypeId = entity.OwnershipTypeId,
                IndustrialClassification = entity.IndustrialClassification,
                TermsAccepted = entity.TermsAccepted,
                PrimaryPhone = entity.PrimaryPhone,
                PrimaryPhoneExtension = entity.PrimaryPhoneExtension,
                PrimaryPhoneType = entity.PrimaryPhoneType,
                AlternatePhone1 = entity.AlternatePhone1,
                AlternatePhone1Type = entity.AlternatePhone1Type,
                AlternatePhone2 = entity.AlternatePhone2,
                AlternatePhone2Type = entity.AlternatePhone2Type,
                IsRegistrationComplete = entity.IsRegistrationComplete,
                ExternalId = entity.ExternalId,
                Archived = entity.Archived,
                AccountTypeId = entity.AccountTypeId,
                NoOfEmployees = entity.NoOfEmployees,
                LegalName = entity.LegalName,
                AssignedToId = entity.AssignedToId,
                Id = entity.Id
            };
            return dto;
        }

        public static Employer CopyTo(this EmployerDto dto, Employer entity)
        {
            entity.Name = dto.Name;
            entity.CommencedOn = dto.CommencedOn;
            entity.FederalEmployerIdentificationNumber = dto.FederalEmployerIdentificationNumber;
            entity.IsValidFederalEmployerIdentificationNumber = dto.IsValidFederalEmployerIdentificationNumber;
            entity.StateEmployerIdentificationNumber = dto.StateEmployerIdentificationNumber;
            entity.ApprovalStatus = dto.ApprovalStatus;
            entity.Url = dto.Url;
            entity.ExpiredOn = dto.ExpiredOn;
            entity.OwnershipTypeId = dto.OwnershipTypeId;
            entity.IndustrialClassification = dto.IndustrialClassification;
            entity.TermsAccepted = dto.TermsAccepted;
            entity.PrimaryPhone = dto.PrimaryPhone;
            entity.PrimaryPhoneExtension = dto.PrimaryPhoneExtension;
            entity.PrimaryPhoneType = dto.PrimaryPhoneType;
            entity.AlternatePhone1 = dto.AlternatePhone1;
            entity.AlternatePhone1Type = dto.AlternatePhone1Type;
            entity.AlternatePhone2 = dto.AlternatePhone2;
            entity.AlternatePhone2Type = dto.AlternatePhone2Type;
            entity.IsRegistrationComplete = dto.IsRegistrationComplete;
            entity.ExternalId = dto.ExternalId;
            entity.Archived = dto.Archived;
            entity.AccountTypeId = dto.AccountTypeId;
            entity.NoOfEmployees = dto.NoOfEmployees;
            entity.LegalName = dto.LegalName;
            entity.AssignedToId = dto.AssignedToId;
            return entity;
        }

        public static Employer CopyTo(this EmployerDto dto)
        {
            var entity = new Employer();
            return dto.CopyTo(entity);
        }

        #endregion

        #region EntityTypeDto Mappers

        public static EntityTypeDto AsDto(this EntityType entity)
        {
            var dto = new EntityTypeDto
            {
                Name = entity.Name,
                Id = entity.Id
            };
            return dto;
        }

        public static EntityType CopyTo(this EntityTypeDto dto, EntityType entity)
        {
            entity.Name = dto.Name;
            return entity;
        }

        public static EntityType CopyTo(this EntityTypeDto dto)
        {
            var entity = new EntityType();
            return dto.CopyTo(entity);
        }

        #endregion

        #region JobDto Mappers

        public static JobDto AsDto(this Job entity)
        {
            var dto = new JobDto
            {
                LockVersion = entity.LockVersion,
                CreatedOn = entity.CreatedOn,
                UpdatedOn = entity.UpdatedOn,
                JobTitle = entity.JobTitle,
                CreatedBy = entity.CreatedBy,
                UpdatedBy = entity.UpdatedBy,
                ApprovalStatus = entity.ApprovalStatus,
                JobStatus = entity.JobStatus,
                MinSalary = entity.MinSalary,
                MaxSalary = entity.MaxSalary,
                SalaryFrequencyId = entity.SalaryFrequencyId,
                HoursPerWeek = entity.HoursPerWeek,
                OverTimeRequired = entity.OverTimeRequired,
                ClosingOn = entity.ClosingOn,
                NumberOfOpenings = entity.NumberOfOpenings,
                PostedOn = entity.PostedOn,
                PostedBy = entity.PostedBy,
                Description = entity.Description,
                PostingHtml = entity.PostingHtml,
                EmployerDescriptionPostingPosition = entity.EmployerDescriptionPostingPosition,
                WizardStep = entity.WizardStep,
                WizardPath = entity.WizardPath,
                HeldOn = entity.HeldOn,
                HeldBy = entity.HeldBy,
                ClosedOn = entity.ClosedOn,
                ClosedBy = entity.ClosedBy,
                JobLocationType = entity.JobLocationType,
                FederalContractor = entity.FederalContractor,
                ForeignLabourCertification = entity.ForeignLabourCertification,
                CourtOrderedAffirmativeAction = entity.CourtOrderedAffirmativeAction,
                FederalContractorExpiresOn = entity.FederalContractorExpiresOn,
                WorkOpportunitiesTaxCreditHires = entity.WorkOpportunitiesTaxCreditHires,
                PostingFlags = entity.PostingFlags,
                IsCommissionBased = entity.IsCommissionBased,
                NormalWorkDays = entity.NormalWorkDays,
                WorkDaysVary = entity.WorkDaysVary,
                NormalWorkShiftsId = entity.NormalWorkShiftsId,
                LeaveBenefits = entity.LeaveBenefits,
                RetirementBenefits = entity.RetirementBenefits,
                InsuranceBenefits = entity.InsuranceBenefits,
                MiscellaneousBenefits = entity.MiscellaneousBenefits,
                OtherBenefitsDetails = entity.OtherBenefitsDetails,
                InterviewContactPreferences = entity.InterviewContactPreferences,
                InterviewEmailAddress = entity.InterviewEmailAddress,
                InterviewApplicationUrl = entity.InterviewApplicationUrl,
                InterviewMailAddress = entity.InterviewMailAddress,
                InterviewFaxNumber = entity.InterviewFaxNumber,
                InterviewPhoneNumber = entity.InterviewPhoneNumber,
                InterviewDirectApplicationDetails = entity.InterviewDirectApplicationDetails,
                InterviewOtherInstructions = entity.InterviewOtherInstructions,
                ScreeningPreferences = entity.ScreeningPreferences,
                MinimumEducationLevel = entity.MinimumEducationLevel,
                MinimumEducationLevelRequired = entity.MinimumEducationLevelRequired,
                MinimumAge = entity.MinimumAge,
                MinimumAgeReason = entity.MinimumAgeReason,
                MinimumAgeRequired = entity.MinimumAgeRequired,
                LicencesRequired = entity.LicencesRequired,
                CertificationRequired = entity.CertificationRequired,
                LanguagesRequired = entity.LanguagesRequired,
                Tasks = entity.Tasks,
                RedProfanityWords = entity.RedProfanityWords,
                YellowProfanityWords = entity.YellowProfanityWords,
                EmploymentStatusId = entity.EmploymentStatusId,
                JobTypeId = entity.JobTypeId,
                JobStatusId = entity.JobStatusId,
                ApprovedOn = entity.ApprovedOn,
                ApprovedBy = entity.ApprovedBy,
                ExternalId = entity.ExternalId,
                AwaitingApprovalOn = entity.AwaitingApprovalOn,
                MinimumExperience = entity.MinimumExperience,
                MinimumExperienceMonths = entity.MinimumExperienceMonths,
                MinimumExperienceRequired = entity.MinimumExperienceRequired,
                DrivingLicenceClassId = entity.DrivingLicenceClassId,
                DrivingLicenceRequired = entity.DrivingLicenceRequired,
                HideSalaryOnPosting = entity.HideSalaryOnPosting,
                ForeignLabourCertificationH2A = entity.ForeignLabourCertificationH2A,
                ForeignLabourCertificationH2B = entity.ForeignLabourCertificationH2B,
                ForeignLabourCertificationOther = entity.ForeignLabourCertificationOther,
                HiringFromTaxCreditProgramNotificationSent = entity.HiringFromTaxCreditProgramNotificationSent,
                IsConfidential = entity.IsConfidential,
                HasCheckedCriminalRecordExclusion = entity.HasCheckedCriminalRecordExclusion,
                LastPostedOn = entity.LastPostedOn,
                LastRefreshedOn = entity.LastRefreshedOn,
                MinimumAgeReasonValue = entity.MinimumAgeReasonValue,
                OtherSalary = entity.OtherSalary,
                HideOpeningsOnPosting = entity.HideOpeningsOnPosting,
                StudentEnrolled = entity.StudentEnrolled,
                MinimumCollegeYears = entity.MinimumCollegeYears,
                ProgramsOfStudyRequired = entity.ProgramsOfStudyRequired,
                StartDate = entity.StartDate,
                EndDate = entity.EndDate,
                JobType = entity.JobType,
                OnetId = entity.OnetId,
                HideEducationOnPosting = entity.HideEducationOnPosting,
                HideExperienceOnPosting = entity.HideExperienceOnPosting,
                HideMinimumAgeOnPosting = entity.HideMinimumAgeOnPosting,
                HideProgramOfStudyOnPosting = entity.HideProgramOfStudyOnPosting,
                HideDriversLicenceOnPosting = entity.HideDriversLicenceOnPosting,
                HideLicencesOnPosting = entity.HideLicencesOnPosting,
                HideCertificationsOnPosting = entity.HideCertificationsOnPosting,
                HideLanguagesOnPosting = entity.HideLanguagesOnPosting,
                HideSpecialRequirementsOnPosting = entity.HideSpecialRequirementsOnPosting,
                HideWorkWeekOnPosting = entity.HideWorkWeekOnPosting,
                DescriptionPath = entity.DescriptionPath,
                WorkWeekId = entity.WorkWeekId,
                ROnetId = entity.ROnetId,
                CriminalBackgroundExclusionRequired = entity.CriminalBackgroundExclusionRequired,
                IsSalaryAndCommissionBased = entity.IsSalaryAndCommissionBased,
                VeteranPriorityEndDate = entity.VeteranPriorityEndDate,
                MeetsMinimumWageRequirement = entity.MeetsMinimumWageRequirement,
                MinimumHoursPerWeek = entity.MinimumHoursPerWeek,
                PreScreeningServiceRequest = entity.PreScreeningServiceRequest,
                SuitableForHomeWorker = entity.SuitableForHomeWorker,
                HideCareerReadinessOnPosting = entity.HideCareerReadinessOnPosting,
                CareerReadinessLevel = entity.CareerReadinessLevel,
                CareerReadinessLevelRequired = entity.CareerReadinessLevelRequired,
                ExtendVeteranPriority = entity.ExtendVeteranPriority,
                ClosingOnUpdated = entity.ClosingOnUpdated,
                UploadIndicator = entity.UploadIndicator,
                Location = entity.Location,
                CreditCheckRequired = entity.CreditCheckRequired,
                DisplayBenefits = entity.DisplayBenefits,
                AwaitingApprovalActionedBy = entity.AwaitingApprovalActionedBy,
                BusinessUnitId = entity.BusinessUnitId,
                EmployeeId = entity.EmployeeId,
                EmployerId = entity.EmployerId,
                AssignedToId = entity.AssignedToId,
                BusinessUnitDescriptionId = entity.BusinessUnitDescriptionId,
                BusinessUnitLogoId = entity.BusinessUnitLogoId,
                Id = entity.Id
            };
            return dto;
        }

        public static Job CopyTo(this JobDto dto, Job entity)
        {
            entity.JobTitle = dto.JobTitle;
            entity.CreatedBy = dto.CreatedBy;
            entity.UpdatedBy = dto.UpdatedBy;
            entity.ApprovalStatus = dto.ApprovalStatus;
            entity.JobStatus = dto.JobStatus;
            entity.MinSalary = dto.MinSalary;
            entity.MaxSalary = dto.MaxSalary;
            entity.SalaryFrequencyId = dto.SalaryFrequencyId;
            entity.HoursPerWeek = dto.HoursPerWeek;
            entity.OverTimeRequired = dto.OverTimeRequired;
            entity.ClosingOn = dto.ClosingOn;
            entity.NumberOfOpenings = dto.NumberOfOpenings;
            entity.PostedOn = dto.PostedOn;
            entity.PostedBy = dto.PostedBy;
            entity.Description = dto.Description;
            entity.PostingHtml = dto.PostingHtml;
            entity.EmployerDescriptionPostingPosition = dto.EmployerDescriptionPostingPosition;
            entity.WizardStep = dto.WizardStep;
            entity.WizardPath = dto.WizardPath;
            entity.HeldOn = dto.HeldOn;
            entity.HeldBy = dto.HeldBy;
            entity.ClosedOn = dto.ClosedOn;
            entity.ClosedBy = dto.ClosedBy;
            entity.JobLocationType = dto.JobLocationType;
            entity.FederalContractor = dto.FederalContractor;
            entity.ForeignLabourCertification = dto.ForeignLabourCertification;
            entity.CourtOrderedAffirmativeAction = dto.CourtOrderedAffirmativeAction;
            entity.FederalContractorExpiresOn = dto.FederalContractorExpiresOn;
            entity.WorkOpportunitiesTaxCreditHires = dto.WorkOpportunitiesTaxCreditHires;
            entity.PostingFlags = dto.PostingFlags;
            entity.IsCommissionBased = dto.IsCommissionBased;
            entity.NormalWorkDays = dto.NormalWorkDays;
            entity.WorkDaysVary = dto.WorkDaysVary;
            entity.NormalWorkShiftsId = dto.NormalWorkShiftsId;
            entity.LeaveBenefits = dto.LeaveBenefits;
            entity.RetirementBenefits = dto.RetirementBenefits;
            entity.InsuranceBenefits = dto.InsuranceBenefits;
            entity.MiscellaneousBenefits = dto.MiscellaneousBenefits;
            entity.OtherBenefitsDetails = dto.OtherBenefitsDetails;
            entity.InterviewContactPreferences = dto.InterviewContactPreferences;
            entity.InterviewEmailAddress = dto.InterviewEmailAddress;
            entity.InterviewApplicationUrl = dto.InterviewApplicationUrl;
            entity.InterviewMailAddress = dto.InterviewMailAddress;
            entity.InterviewFaxNumber = dto.InterviewFaxNumber;
            entity.InterviewPhoneNumber = dto.InterviewPhoneNumber;
            entity.InterviewDirectApplicationDetails = dto.InterviewDirectApplicationDetails;
            entity.InterviewOtherInstructions = dto.InterviewOtherInstructions;
            entity.ScreeningPreferences = dto.ScreeningPreferences;
            entity.MinimumEducationLevel = dto.MinimumEducationLevel;
            entity.MinimumEducationLevelRequired = dto.MinimumEducationLevelRequired;
            entity.MinimumAge = dto.MinimumAge;
            entity.MinimumAgeReason = dto.MinimumAgeReason;
            entity.MinimumAgeRequired = dto.MinimumAgeRequired;
            entity.LicencesRequired = dto.LicencesRequired;
            entity.CertificationRequired = dto.CertificationRequired;
            entity.LanguagesRequired = dto.LanguagesRequired;
            entity.Tasks = dto.Tasks;
            entity.RedProfanityWords = dto.RedProfanityWords;
            entity.YellowProfanityWords = dto.YellowProfanityWords;
            entity.EmploymentStatusId = dto.EmploymentStatusId;
            entity.JobTypeId = dto.JobTypeId;
            entity.JobStatusId = dto.JobStatusId;
            entity.ApprovedOn = dto.ApprovedOn;
            entity.ApprovedBy = dto.ApprovedBy;
            entity.ExternalId = dto.ExternalId;
            entity.AwaitingApprovalOn = dto.AwaitingApprovalOn;
            entity.MinimumExperience = dto.MinimumExperience;
            entity.MinimumExperienceMonths = dto.MinimumExperienceMonths;
            entity.MinimumExperienceRequired = dto.MinimumExperienceRequired;
            entity.DrivingLicenceClassId = dto.DrivingLicenceClassId;
            entity.DrivingLicenceRequired = dto.DrivingLicenceRequired;
            entity.HideSalaryOnPosting = dto.HideSalaryOnPosting;
            entity.ForeignLabourCertificationH2A = dto.ForeignLabourCertificationH2A;
            entity.ForeignLabourCertificationH2B = dto.ForeignLabourCertificationH2B;
            entity.ForeignLabourCertificationOther = dto.ForeignLabourCertificationOther;
            entity.HiringFromTaxCreditProgramNotificationSent = dto.HiringFromTaxCreditProgramNotificationSent;
            entity.IsConfidential = dto.IsConfidential;
            entity.HasCheckedCriminalRecordExclusion = dto.HasCheckedCriminalRecordExclusion;
            entity.LastPostedOn = dto.LastPostedOn;
            entity.LastRefreshedOn = dto.LastRefreshedOn;
            entity.MinimumAgeReasonValue = dto.MinimumAgeReasonValue;
            entity.OtherSalary = dto.OtherSalary;
            entity.HideOpeningsOnPosting = dto.HideOpeningsOnPosting;
            entity.StudentEnrolled = dto.StudentEnrolled;
            entity.MinimumCollegeYears = dto.MinimumCollegeYears;
            entity.ProgramsOfStudyRequired = dto.ProgramsOfStudyRequired;
            entity.StartDate = dto.StartDate;
            entity.EndDate = dto.EndDate;
            entity.JobType = dto.JobType;
            entity.OnetId = dto.OnetId;
            entity.HideEducationOnPosting = dto.HideEducationOnPosting;
            entity.HideExperienceOnPosting = dto.HideExperienceOnPosting;
            entity.HideMinimumAgeOnPosting = dto.HideMinimumAgeOnPosting;
            entity.HideProgramOfStudyOnPosting = dto.HideProgramOfStudyOnPosting;
            entity.HideDriversLicenceOnPosting = dto.HideDriversLicenceOnPosting;
            entity.HideLicencesOnPosting = dto.HideLicencesOnPosting;
            entity.HideCertificationsOnPosting = dto.HideCertificationsOnPosting;
            entity.HideLanguagesOnPosting = dto.HideLanguagesOnPosting;
            entity.HideSpecialRequirementsOnPosting = dto.HideSpecialRequirementsOnPosting;
            entity.HideWorkWeekOnPosting = dto.HideWorkWeekOnPosting;
            entity.DescriptionPath = dto.DescriptionPath;
            entity.WorkWeekId = dto.WorkWeekId;
            entity.ROnetId = dto.ROnetId;
            entity.CriminalBackgroundExclusionRequired = dto.CriminalBackgroundExclusionRequired;
            entity.IsSalaryAndCommissionBased = dto.IsSalaryAndCommissionBased;
            entity.VeteranPriorityEndDate = dto.VeteranPriorityEndDate;
            entity.MeetsMinimumWageRequirement = dto.MeetsMinimumWageRequirement;
            entity.MinimumHoursPerWeek = dto.MinimumHoursPerWeek;
            entity.PreScreeningServiceRequest = dto.PreScreeningServiceRequest;
            entity.SuitableForHomeWorker = dto.SuitableForHomeWorker;
            entity.HideCareerReadinessOnPosting = dto.HideCareerReadinessOnPosting;
            entity.CareerReadinessLevel = dto.CareerReadinessLevel;
            entity.CareerReadinessLevelRequired = dto.CareerReadinessLevelRequired;
            entity.ExtendVeteranPriority = dto.ExtendVeteranPriority;
            entity.ClosingOnUpdated = dto.ClosingOnUpdated;
            entity.UploadIndicator = dto.UploadIndicator;
            entity.Location = dto.Location;
            entity.CreditCheckRequired = dto.CreditCheckRequired;
            entity.DisplayBenefits = dto.DisplayBenefits;
            entity.AwaitingApprovalActionedBy = dto.AwaitingApprovalActionedBy;
            entity.BusinessUnitId = dto.BusinessUnitId;
            entity.EmployeeId = dto.EmployeeId;
            entity.EmployerId = dto.EmployerId;
            entity.AssignedToId = dto.AssignedToId;
            entity.BusinessUnitDescriptionId = dto.BusinessUnitDescriptionId;
            entity.BusinessUnitLogoId = dto.BusinessUnitLogoId;
            return entity;
        }

        public static Job CopyTo(this JobDto dto)
        {
            var entity = new Job();
            return dto.CopyTo(entity);
        }

        #endregion

        #region ApplicationDto Mappers

        public static ApplicationDto AsDto(this Application entity)
        {
            var dto = new ApplicationDto
            {
                LockVersion = entity.LockVersion,
                CreatedOn = entity.CreatedOn,
                UpdatedOn = entity.UpdatedOn,
                ApprovalStatus = entity.ApprovalStatus,
                ApprovalRequiredReason = entity.ApprovalRequiredReason,
                ApplicationStatus = entity.ApplicationStatus,
                ApplicationScore = entity.ApplicationScore,
                StatusLastChangedOn = entity.StatusLastChangedOn,
                Viewed = entity.Viewed,
                PostHireFollowUpStatus = entity.PostHireFollowUpStatus,
                AutomaticallyApproved = entity.AutomaticallyApproved,
                StatusLastChangedBy = entity.StatusLastChangedBy,
                AutomaticallyDenied = entity.AutomaticallyDenied,
                PreviousApprovalStatus = entity.PreviousApprovalStatus,
                AutomaticallyOnHold = entity.AutomaticallyOnHold,
                ResumeId = entity.ResumeId,
                PostingId = entity.PostingId,
                Id = entity.Id
            };
            return dto;
        }

        public static Application CopyTo(this ApplicationDto dto, Application entity)
        {
            entity.ApprovalStatus = dto.ApprovalStatus;
            entity.ApprovalRequiredReason = dto.ApprovalRequiredReason;
            entity.ApplicationStatus = dto.ApplicationStatus;
            entity.ApplicationScore = dto.ApplicationScore;
            entity.StatusLastChangedOn = dto.StatusLastChangedOn;
            entity.Viewed = dto.Viewed;
            entity.PostHireFollowUpStatus = dto.PostHireFollowUpStatus;
            entity.AutomaticallyApproved = dto.AutomaticallyApproved;
            entity.StatusLastChangedBy = dto.StatusLastChangedBy;
            entity.AutomaticallyDenied = dto.AutomaticallyDenied;
            entity.PreviousApprovalStatus = dto.PreviousApprovalStatus;
            entity.AutomaticallyOnHold = dto.AutomaticallyOnHold;
            entity.ResumeId = dto.ResumeId;
            entity.PostingId = dto.PostingId;
            return entity;
        }

        public static Application CopyTo(this ApplicationDto dto)
        {
            var entity = new Application();
            return dto.CopyTo(entity);
        }

        #endregion

        #region JobSkillDto Mappers

        public static JobSkillDto AsDto(this JobSkill entity)
        {
            var dto = new JobSkillDto
            {
                SkillId = entity.SkillId,
                JobId = entity.JobId,
                Id = entity.Id
            };
            return dto;
        }

        public static JobSkill CopyTo(this JobSkillDto dto, JobSkill entity)
        {
            entity.SkillId = dto.SkillId;
            entity.JobId = dto.JobId;
            return entity;
        }

        public static JobSkill CopyTo(this JobSkillDto dto)
        {
            var entity = new JobSkill();
            return dto.CopyTo(entity);
        }

        #endregion

        #region JobProgramOfStudyDto Mappers

        public static JobProgramOfStudyDto AsDto(this JobProgramOfStudy entity)
        {
            var dto = new JobProgramOfStudyDto
            {
                DegreeEducationLevelId = entity.DegreeEducationLevelId,
                ProgramOfStudy = entity.ProgramOfStudy,
                JobId = entity.JobId,
                Id = entity.Id,
                Tier = entity.Tier
            };
            return dto;
        }

        public static JobProgramOfStudy CopyTo(this JobProgramOfStudyDto dto, JobProgramOfStudy entity)
        {
            entity.DegreeEducationLevelId = dto.DegreeEducationLevelId;
            entity.ProgramOfStudy = dto.ProgramOfStudy;
            entity.JobId = dto.JobId;
            entity.Tier = dto.Tier;
            return entity;
        }

        public static JobProgramOfStudy CopyTo(this JobProgramOfStudyDto dto)
        {
            var entity = new JobProgramOfStudy();
            return dto.CopyTo(entity);
        }

        #endregion

        #region JobSpecialRequirementDto Mappers

        public static JobSpecialRequirementDto AsDto(this JobSpecialRequirement entity)
        {
            var dto = new JobSpecialRequirementDto
            {
                Requirement = entity.Requirement,
                JobId = entity.JobId,
                IsNew = entity.IsNew,
                Id = entity.Id
            };
            return dto;
        }

        public static JobSpecialRequirement CopyTo(this JobSpecialRequirementDto dto, JobSpecialRequirement entity)
        {
            entity.Requirement = dto.Requirement;
            entity.JobId = dto.JobId;
            entity.IsNew = dto.IsNew;
            return entity;
        }

        public static JobSpecialRequirement CopyTo(this JobSpecialRequirementDto dto)
        {
            var entity = new JobSpecialRequirement();
            return dto.CopyTo(entity);
        }

        #endregion

        #region NoteDto Mappers

        public static NoteDto AsDto(this Note entity)
        {
            var dto = new NoteDto
            {
                Title = entity.Title,
                Body = entity.Body,
                Id = entity.Id
            };
            return dto;
        }

        public static Note CopyTo(this NoteDto dto, Note entity)
        {
            entity.Title = dto.Title;
            entity.Body = dto.Body;
            return entity;
        }

        public static Note CopyTo(this NoteDto dto)
        {
            var entity = new Note();
            return dto.CopyTo(entity);
        }

        #endregion

        #region PersonDto Mappers

        public static PersonDto AsDto(this Person entity)
        {
            var dto = new PersonDto
            {
                TitleId = entity.TitleId,
                FirstName = entity.FirstName,
                MiddleInitial = entity.MiddleInitial,
                LastName = entity.LastName,
                Age = entity.Age,
                DateOfBirth = entity.DateOfBirth,
                SocialSecurityNumber = entity.SocialSecurityNumber,
                JobTitle = entity.JobTitle,
                EmailAddress = entity.EmailAddress,
                EnrollmentStatus = entity.EnrollmentStatus,
                ProgramAreaId = entity.ProgramAreaId,
                DegreeId = entity.DegreeId,
                Manager = entity.Manager,
                ExternalOffice = entity.ExternalOffice,
                LastSurveyedOn = entity.LastSurveyedOn,
                IsVeteran = entity.IsVeteran,
                IsUiClaimant = entity.IsUiClaimant,
                Unsubscribed = entity.Unsubscribed,
                SuffixId = entity.SuffixId,
                MigrantSeasonalFarmWorkerVerified = entity.MigrantSeasonalFarmWorkerVerified,
                AssignedToId = entity.AssignedToId,
                CampusId = entity.CampusId,
                AccountType = entity.AccountType,
                LocalVeteranEmploymentRepresentative = entity.LocalVeteranEmploymentRepresentative,
                DisabledVeteransOutreachProgramSpecialist = entity.DisabledVeteransOutreachProgramSpecialist,
                Id = entity.Id
            };
            return dto;
        }

        public static Person CopyTo(this PersonDto dto, Person entity)
        {
            entity.TitleId = dto.TitleId;
            entity.FirstName = dto.FirstName;
            entity.MiddleInitial = dto.MiddleInitial;
            entity.LastName = dto.LastName;
            entity.Age = dto.Age;
            entity.DateOfBirth = dto.DateOfBirth;
            entity.SocialSecurityNumber = dto.SocialSecurityNumber;
            entity.JobTitle = dto.JobTitle;
            entity.EmailAddress = dto.EmailAddress;
            entity.EnrollmentStatus = dto.EnrollmentStatus;
            entity.ProgramAreaId = dto.ProgramAreaId;
            entity.DegreeId = dto.DegreeId;
            entity.Manager = dto.Manager;
            entity.ExternalOffice = dto.ExternalOffice;
            entity.LastSurveyedOn = dto.LastSurveyedOn;
            entity.IsVeteran = dto.IsVeteran;
            entity.IsUiClaimant = dto.IsUiClaimant;
            entity.Unsubscribed = dto.Unsubscribed;
            entity.SuffixId = dto.SuffixId;
            entity.MigrantSeasonalFarmWorkerVerified = dto.MigrantSeasonalFarmWorkerVerified;
            entity.AssignedToId = dto.AssignedToId;
            entity.CampusId = dto.CampusId;
            entity.AccountType = dto.AccountType;
            entity.LocalVeteranEmploymentRepresentative = dto.LocalVeteranEmploymentRepresentative;
            entity.DisabledVeteransOutreachProgramSpecialist = dto.DisabledVeteransOutreachProgramSpecialist;
            return entity;
        }

        public static Person CopyTo(this PersonDto dto)
        {
            var entity = new Person();
            return dto.CopyTo(entity);
        }

        public static PersonModel AsModel(this Person entity)
        {
            var model = new PersonModel
            {
                Id = entity.Id,
                TitleId = entity.TitleId,
                FirstName = entity.FirstName,
                MiddleInitial = entity.MiddleInitial,
                LastName = entity.LastName,
                DateOfBirth = entity.DateOfBirth,
                SocialSecurityNumber = entity.SocialSecurityNumber,
                JobTitle = entity.JobTitle,
                EmailAddress = entity.EmailAddress,
                EnrollmentStatus = entity.EnrollmentStatus,
                ProgramAreaId = entity.ProgramAreaId,
                DegreeId = entity.DegreeId,
                ExternalOffice = entity.ExternalOffice,
                Manager = entity.Manager,
                AssignedToId = entity.AssignedToId,
                LastSurveyedOn = entity.LastSurveyedOn,
                CampusId = entity.CampusId
            };

            return model;
        }

        #endregion

        #region PostingSurveyDto Mappers

        public static PostingSurveyDto AsDto(this PostingSurvey entity)
        {
            var dto = new PostingSurveyDto
            {
                SurveyType = entity.SurveyType,
                SatisfactionLevel = entity.SatisfactionLevel,
                DidInterview = entity.DidInterview,
                DidHire = entity.DidHire,
                DidOffer = entity.DidOffer,
                JobId = entity.JobId,
                Id = entity.Id
            };
            return dto;
        }

        public static PostingSurvey CopyTo(this PostingSurveyDto dto, PostingSurvey entity)
        {
            entity.SurveyType = dto.SurveyType;
            entity.SatisfactionLevel = dto.SatisfactionLevel;
            entity.DidInterview = dto.DidInterview;
            entity.DidHire = dto.DidHire;
            entity.DidOffer = dto.DidOffer;
            entity.JobId = dto.JobId;
            return entity;
        }

        public static PostingSurvey CopyTo(this PostingSurveyDto dto)
        {
            var entity = new PostingSurvey();
            return dto.CopyTo(entity);
        }

        #endregion

        #region SavedSearchDto Mappers

        public static SavedSearchDto AsDto(this SavedSearch entity)
        {
            var dto = new SavedSearchDto
            {
                CreatedOn = entity.CreatedOn,
                UpdatedOn = entity.UpdatedOn,
                Name = entity.Name,
                SearchCriteria = entity.SearchCriteria,
                AlertEmailRequired = entity.AlertEmailRequired,
                AlertEmailFrequency = entity.AlertEmailFrequency,
                AlertEmailFormat = entity.AlertEmailFormat,
                AlertEmailAddress = entity.AlertEmailAddress,
                AlertEmailScheduledOn = entity.AlertEmailScheduledOn,
                Type = entity.Type,
                Id = entity.Id
            };
            return dto;
        }

        public static SavedSearch CopyTo(this SavedSearchDto dto, SavedSearch entity)
        {
            entity.Name = dto.Name;
            entity.SearchCriteria = dto.SearchCriteria;
            entity.AlertEmailRequired = dto.AlertEmailRequired;
            entity.AlertEmailFrequency = dto.AlertEmailFrequency;
            entity.AlertEmailFormat = dto.AlertEmailFormat;
            entity.AlertEmailAddress = dto.AlertEmailAddress;
            entity.AlertEmailScheduledOn = dto.AlertEmailScheduledOn;
            entity.Type = dto.Type;
            return entity;
        }

        public static SavedSearch CopyTo(this SavedSearchDto dto)
        {
            var entity = new SavedSearch();
            return dto.CopyTo(entity);
        }

        #endregion

        #region EmployerAddressDto Mappers

        public static EmployerAddressDto AsDto(this EmployerAddress entity)
        {
            var dto = new EmployerAddressDto
            {
                Line1 = entity.Line1,
                Line2 = entity.Line2,
                Line3 = entity.Line3,
                TownCity = entity.TownCity,
                CountyId = entity.CountyId,
                PostcodeZip = entity.PostcodeZip,
                StateId = entity.StateId,
                CountryId = entity.CountryId,
                IsPrimary = entity.IsPrimary,
                PublicTransitAccessible = entity.PublicTransitAccessible,
                EmployerId = entity.EmployerId,
                Id = entity.Id
            };
            return dto;
        }

        public static EmployerAddress CopyTo(this EmployerAddressDto dto, EmployerAddress entity)
        {
            entity.Line1 = dto.Line1;
            entity.Line2 = dto.Line2;
            entity.Line3 = dto.Line3;
            entity.TownCity = dto.TownCity;
            entity.CountyId = dto.CountyId;
            entity.PostcodeZip = dto.PostcodeZip;
            entity.StateId = dto.StateId;
            entity.CountryId = dto.CountryId;
            entity.IsPrimary = dto.IsPrimary;
            entity.PublicTransitAccessible = dto.PublicTransitAccessible;
            entity.EmployerId = dto.EmployerId;
            return entity;
        }

        public static EmployerAddress CopyTo(this EmployerAddressDto dto)
        {
            var entity = new EmployerAddress();
            return dto.CopyTo(entity);
        }

        #endregion

        #region JobAddressDto Mappers

        public static JobAddressDto AsDto(this JobAddress entity)
        {
            var dto = new JobAddressDto
            {
                Line1 = entity.Line1,
                Line2 = entity.Line2,
                Line3 = entity.Line3,
                TownCity = entity.TownCity,
                CountyId = entity.CountyId,
                PostcodeZip = entity.PostcodeZip,
                StateId = entity.StateId,
                CountryId = entity.CountryId,
                IsPrimary = entity.IsPrimary,
                JobId = entity.JobId,
                Id = entity.Id
            };
            return dto;
        }

        public static JobAddress CopyTo(this JobAddressDto dto, JobAddress entity)
        {
            entity.Line1 = dto.Line1;
            entity.Line2 = dto.Line2;
            entity.Line3 = dto.Line3;
            entity.TownCity = dto.TownCity;
            entity.CountyId = dto.CountyId;
            entity.PostcodeZip = dto.PostcodeZip;
            entity.StateId = dto.StateId;
            entity.CountryId = dto.CountryId;
            entity.IsPrimary = dto.IsPrimary;
            entity.JobId = dto.JobId;
            return entity;
        }

        public static JobAddress CopyTo(this JobAddressDto dto)
        {
            var entity = new JobAddress();
            return dto.CopyTo(entity);
        }

        #endregion

        #region RoleDto Mappers

        public static RoleDto AsDto(this Role entity)
        {
            var dto = new RoleDto
            {
                Key = entity.Key,
                Value = entity.Value,
                Id = entity.Id
            };
            return dto;
        }

        public static Role CopyTo(this RoleDto dto, Role entity)
        {
            entity.Key = dto.Key;
            entity.Value = dto.Value;
            return entity;
        }

        public static Role CopyTo(this RoleDto dto)
        {
            var entity = new Role();
            return dto.CopyTo(entity);
        }

        #endregion

        #region PersonAddressDto Mappers

        public static PersonAddressDto AsDto(this PersonAddress entity)
        {
            var dto = new PersonAddressDto
            {
                Line1 = entity.Line1,
                Line2 = entity.Line2,
                Line3 = entity.Line3,
                TownCity = entity.TownCity,
                CountyId = entity.CountyId,
                PostcodeZip = entity.PostcodeZip,
                StateId = entity.StateId,
                CountryId = entity.CountryId,
                IsPrimary = entity.IsPrimary,
                PersonId = entity.PersonId,
                Id = entity.Id
            };
            return dto;
        }

        public static PersonAddress CopyTo(this PersonAddressDto dto, PersonAddress entity)
        {
            entity.Line1 = dto.Line1;
            entity.Line2 = dto.Line2;
            entity.Line3 = dto.Line3;
            entity.TownCity = dto.TownCity;
            entity.CountyId = dto.CountyId;
            entity.PostcodeZip = dto.PostcodeZip;
            entity.StateId = dto.StateId;
            entity.CountryId = dto.CountryId;
            entity.IsPrimary = dto.IsPrimary;
            entity.PersonId = dto.PersonId;
            return entity;
        }

        public static PersonAddress CopyTo(this PersonAddressDto dto)
        {
            var entity = new PersonAddress();
            return dto.CopyTo(entity);
        }

        #endregion

        #region ActionTypeDto Mappers

        public static ActionTypeDto AsDto(this ActionType entity)
        {
            var dto = new ActionTypeDto
            {
                Name = entity.Name,
                ReportOn = entity.ReportOn,
                AssistAction = entity.AssistAction,
                Id = entity.Id
            };
            return dto;
        }

        public static ActionType CopyTo(this ActionTypeDto dto, ActionType entity)
        {
            entity.Name = dto.Name;
            entity.ReportOn = dto.ReportOn;
            entity.AssistAction = dto.AssistAction;
            return entity;
        }

        public static ActionType CopyTo(this ActionTypeDto dto)
        {
            var entity = new ActionType();
            return dto.CopyTo(entity);
        }

        #endregion

        #region JobLocationDto Mappers

        public static JobLocationDto AsDto(this JobLocation entity)
        {
            var dto = new JobLocationDto
            {
                CreatedOn = entity.CreatedOn,
                Location = entity.Location,
                IsPublicTransitAccessible = entity.IsPublicTransitAccessible,
                AddressLine1 = entity.AddressLine1,
                City = entity.City,
                State = entity.State,
                Zip = entity.Zip,
                County = entity.County,
                NewLocation = entity.NewLocation,
                JobId = entity.JobId,
                Id = entity.Id
            };
            return dto;
        }

        public static JobLocation CopyTo(this JobLocationDto dto, JobLocation entity)
        {
            entity.Location = dto.Location;
            entity.IsPublicTransitAccessible = dto.IsPublicTransitAccessible;
            entity.AddressLine1 = dto.AddressLine1;
            entity.City = dto.City;
            entity.State = dto.State;
            entity.Zip = dto.Zip;
            entity.County = dto.County;
            entity.NewLocation = dto.NewLocation;
            entity.JobId = dto.JobId;
            return entity;
        }

        public static JobLocation CopyTo(this JobLocationDto dto)
        {
            var entity = new JobLocation();
            return dto.CopyTo(entity);
        }

        #endregion

        #region JobLicenceDto Mappers

        public static JobLicenceDto AsDto(this JobLicence entity)
        {
            var dto = new JobLicenceDto
            {
                Licence = entity.Licence,
                JobId = entity.JobId,
                Id = entity.Id
            };
            return dto;
        }

        public static JobLicence CopyTo(this JobLicenceDto dto, JobLicence entity)
        {
            entity.Licence = dto.Licence;
            entity.JobId = dto.JobId;
            return entity;
        }

        public static JobLicence CopyTo(this JobLicenceDto dto)
        {
            var entity = new JobLicence();
            return dto.CopyTo(entity);
        }

        #endregion

        #region JobCertificateDto Mappers

        public static JobCertificateDto AsDto(this JobCertificate entity)
        {
            var dto = new JobCertificateDto
            {
                Certificate = entity.Certificate,
                JobId = entity.JobId,
                Id = entity.Id
            };
            return dto;
        }

        public static JobCertificate CopyTo(this JobCertificateDto dto, JobCertificate entity)
        {
            entity.Certificate = dto.Certificate;
            entity.JobId = dto.JobId;
            return entity;
        }

        public static JobCertificate CopyTo(this JobCertificateDto dto)
        {
            var entity = new JobCertificate();
            return dto.CopyTo(entity);
        }

        #endregion

        #region JobLanguageDto Mappers

        public static JobLanguageDto AsDto(this JobLanguage entity)
        {
            var dto = new JobLanguageDto
            {
                Language = entity.Language,
                LanguageProficiencyId = entity.LanguageProficiencyId,
                JobId = entity.JobId,
                Id = entity.Id
            };
            return dto;
        }

        public static JobLanguage CopyTo(this JobLanguageDto dto, JobLanguage entity)
        {
            entity.Language = dto.Language;
            entity.LanguageProficiencyId = dto.LanguageProficiencyId;
            entity.JobId = dto.JobId;
            return entity;
        }

        public static JobLanguage CopyTo(this JobLanguageDto dto)
        {
            var entity = new JobLanguage();
            return dto.CopyTo(entity);
        }

        #endregion

        #region PhoneNumberDto Mappers

        public static PhoneNumberDto AsDto(this PhoneNumber entity)
        {
            var dto = new PhoneNumberDto
            {
                Number = entity.Number,
                PhoneType = entity.PhoneType,
                IsPrimary = entity.IsPrimary,
                Extension = entity.Extension,
                PersonId = entity.PersonId,
                Id = entity.Id,
                ProviderId = entity.ProviderId
            };
            return dto;
        }

        public static PhoneNumber CopyTo(this PhoneNumberDto dto, PhoneNumber entity)
        {
            entity.Number = dto.Number;
            entity.PhoneType = dto.PhoneType;
            entity.IsPrimary = dto.IsPrimary;
            entity.Extension = dto.Extension;
            entity.PersonId = dto.PersonId;
            entity.ProviderId = dto.ProviderId;
            return entity;
        }

        public static PhoneNumber CopyTo(this PhoneNumberDto dto)
        {
            var entity = new PhoneNumber();
            return dto.CopyTo(entity);
        }

        #endregion

        #region JobViewDto Mappers

        public static JobViewDto AsDto(this JobView entity)
        {
            var dto = new JobViewDto
            {
                CreatedOn = entity.CreatedOn,
                UpdatedOn = entity.UpdatedOn,
                EmployerId = entity.EmployerId,
                JobTitle = entity.JobTitle,
                JobStatus = entity.JobStatus,
                PostedOn = entity.PostedOn,
                ClosingOn = entity.ClosingOn,
                HeldOn = entity.HeldOn,
                ClosedOn = entity.ClosedOn,
                BusinessUnitName = entity.BusinessUnitName,
                EmployeeId = entity.EmployeeId,
                ApprovalStatus = entity.ApprovalStatus,
                EmployerName = entity.EmployerName,
                ReferralCount = entity.ReferralCount,
                FederalContractor = entity.FederalContractor,
                ForeignLabourCertificationH2A = entity.ForeignLabourCertificationH2A,
                ForeignLabourCertificationH2B = entity.ForeignLabourCertificationH2B,
                ForeignLabourCertificationOther = entity.ForeignLabourCertificationOther,
                CourtOrderedAffirmativeAction = entity.CourtOrderedAffirmativeAction,
                UserName = entity.UserName,
                YellowProfanityWords = entity.YellowProfanityWords,
                BusinessUnitId = entity.BusinessUnitId,
                IsConfidential = entity.IsConfidential,
                HiringManagerFirstName = entity.HiringManagerFirstName,
                HiringManagerLastName = entity.HiringManagerLastName,
                CreatedBy = entity.CreatedBy,
                UpdatedBy = entity.UpdatedBy,
                JobType = entity.JobType,
                AssignedToId = entity.AssignedToId,
                NumberOfOpenings = entity.NumberOfOpenings,
                VeteranPriorityEndDate = entity.VeteranPriorityEndDate,
                ExtendVeteranPriority = entity.ExtendVeteranPriority,
                Location = entity.Location,
                HasCheckedCriminalRecordExclusion = entity.HasCheckedCriminalRecordExclusion,
                RedProfanityWords = entity.RedProfanityWords,
                ClosedBy = entity.ClosedBy,
                AwaitingApprovalOn = entity.AwaitingApprovalOn,
                PostingId = entity.PostingId,
                Id = entity.Id,
                JobLocationType = entity.JobLocationType,
                SuitableForHomeWorker = entity.SuitableForHomeWorker,
                IsCommissionBased = entity.IsCommissionBased,
                IsSalaryAndCommissionBased = entity.IsSalaryAndCommissionBased,
                MinimumAgeReasonValue = entity.MinimumAgeReasonValue,
                CreditCheckRequired = entity.CreditCheckRequired,
                JobSpecialRequirement = entity.JobSpecialRequirement
            };
            return dto;
        }

        public static JobView CopyTo(this JobViewDto dto, JobView entity)
        {
            entity.EmployerId = dto.EmployerId;
            entity.JobTitle = dto.JobTitle;
            entity.JobStatus = dto.JobStatus;
            entity.PostedOn = dto.PostedOn;
            entity.ClosingOn = dto.ClosingOn;
            entity.HeldOn = dto.HeldOn;
            entity.ClosedOn = dto.ClosedOn;
            entity.BusinessUnitName = dto.BusinessUnitName;
            entity.EmployeeId = dto.EmployeeId;
            entity.ApprovalStatus = dto.ApprovalStatus;
            entity.EmployerName = dto.EmployerName;
            entity.ReferralCount = dto.ReferralCount;
            entity.FederalContractor = dto.FederalContractor;
            entity.ForeignLabourCertificationH2A = dto.ForeignLabourCertificationH2A;
            entity.ForeignLabourCertificationH2B = dto.ForeignLabourCertificationH2B;
            entity.ForeignLabourCertificationOther = dto.ForeignLabourCertificationOther;
            entity.CourtOrderedAffirmativeAction = dto.CourtOrderedAffirmativeAction;
            entity.UserName = dto.UserName;
            entity.YellowProfanityWords = dto.YellowProfanityWords;
            entity.BusinessUnitId = dto.BusinessUnitId;
            entity.IsConfidential = dto.IsConfidential;
            entity.HiringManagerFirstName = dto.HiringManagerFirstName;
            entity.HiringManagerLastName = dto.HiringManagerLastName;
            entity.CreatedBy = dto.CreatedBy;
            entity.UpdatedBy = dto.UpdatedBy;
            entity.JobType = dto.JobType;
            entity.AssignedToId = dto.AssignedToId;
            entity.NumberOfOpenings = dto.NumberOfOpenings;
            entity.VeteranPriorityEndDate = dto.VeteranPriorityEndDate;
            entity.ExtendVeteranPriority = dto.ExtendVeteranPriority;
            entity.Location = dto.Location;
            entity.HasCheckedCriminalRecordExclusion = dto.HasCheckedCriminalRecordExclusion;
            entity.RedProfanityWords = dto.RedProfanityWords;
            entity.ClosedBy = dto.ClosedBy;
            entity.AwaitingApprovalOn = dto.AwaitingApprovalOn;
            entity.PostingId = dto.PostingId;
            entity.SuitableForHomeWorker = dto.SuitableForHomeWorker;
            entity.CreditCheckRequired = dto.CreditCheckRequired;
            entity.MinimumAgeReasonValue = dto.MinimumAgeReasonValue;
            entity.IsCommissionBased = dto.IsCommissionBased;
            entity.IsSalaryAndCommissionBased = dto.IsSalaryAndCommissionBased;
            entity.JobLocationType = dto.JobLocationType;
            entity.JobSpecialRequirement = dto.JobSpecialRequirement;
            return entity;
        }

        public static JobView CopyTo(this JobViewDto dto)
        {
            var entity = new JobView();
            return dto.CopyTo(entity);
        }

        #endregion

        #region JobViewModel Mappers

        public static JobViewModel AsModel(this JobView entity)
        {
            var model = new JobViewModel
            {
                Id = entity.Id,
                CreatedOn = entity.CreatedOn,
                UpdatedOn = entity.UpdatedOn,
                EmployerId = entity.EmployerId,
                JobTitle = entity.JobTitle,
                JobStatus = entity.JobStatus,
                ApplicationCount = -1,
                VeteranApplicationCount = -1,
                PostedOn = entity.PostedOn,
                ClosingOn = entity.ClosingOn,
                HeldOn = entity.HeldOn,
                ClosedOn = entity.ClosedOn,
                BusinessUnitName = entity.BusinessUnitName,
                EmployeeId = entity.EmployeeId,
                ApprovalStatus = entity.ApprovalStatus,
                EmployerName = entity.EmployerName,
                ReferralCount = entity.ReferralCount,
                FederalContractor = entity.FederalContractor,
                ForeignLabourCertificationH2A = entity.ForeignLabourCertificationH2A,
                ForeignLabourCertificationH2B = entity.ForeignLabourCertificationH2B,
                ForeignLabourCertificationOther = entity.ForeignLabourCertificationOther,
                CourtOrderedAffirmativeAction = entity.CourtOrderedAffirmativeAction,
                UserName = entity.UserName,
                YellowProfanityWords = entity.YellowProfanityWords,
                BusinessUnitId = entity.BusinessUnitId,
                IsConfidential = entity.IsConfidential,
                HiringManagerFirstName = entity.HiringManagerFirstName,
                HiringManagerLastName = entity.HiringManagerLastName,
                CreatedBy = entity.CreatedBy,
                UpdatedBy = entity.UpdatedBy,
                JobType = entity.JobType,
                AssignedToId = entity.AssignedToId,
                NumberOfOpenings = entity.NumberOfOpenings,
                VeteranPriortyEndDate = entity.VeteranPriorityEndDate,
                Location = entity.Location,
                HasCheckedCriminalRecordExclusion = entity.HasCheckedCriminalRecordExclusion,
                RedProfanityWords = entity.RedProfanityWords,
                ClosedBy = entity.ClosedBy,
                AwaitingApprovalOn = entity.AwaitingApprovalOn
            };
            return model;
        }

        #endregion

        #region SessionDto Mappers

        public static SessionDto AsDto(this Session entity)
        {
            var dto = new SessionDto
            {
                UserId = entity.UserId,
                LastRequestOn = entity.LastRequestOn,
                Id = entity.Id
            };
            return dto;
        }

        public static Session CopyTo(this SessionDto dto, Session entity)
        {
            entity.UserId = dto.UserId;
            entity.LastRequestOn = dto.LastRequestOn;
            return entity;
        }

        public static Session CopyTo(this SessionDto dto)
        {
            var entity = new Session();
            return dto.CopyTo(entity);
        }

        #endregion

        #region SessionStateDto Mappers

        public static SessionStateDto AsDto(this SessionState entity)
        {
            var dto = new SessionStateDto
            {
                Key = entity.Key,
                Value = entity.Value,
                TypeOf = entity.TypeOf,
                Size = entity.Size,
                SessionId = entity.SessionId,
                Id = entity.Id
            };
            return dto;
        }

        public static SessionState CopyTo(this SessionStateDto dto, SessionState entity)
        {
            entity.Key = dto.Key;
            entity.Value = dto.Value;
            entity.TypeOf = dto.TypeOf;
            entity.Size = dto.Size;
            entity.SessionId = dto.SessionId;
            return entity;
        }

        public static SessionState CopyTo(this SessionStateDto dto)
        {
            var entity = new SessionState();
            return dto.CopyTo(entity);
        }

        #endregion

        #region MessageDto Mappers

        public static MessageDto AsDto(this Message entity)
        {
            var dto = new MessageDto
            {
                CreatedOn = entity.CreatedOn,
                IsSystemAlert = entity.IsSystemAlert,
                EmployerId = entity.EmployerId,
                ExpiresOn = entity.ExpiresOn,
                Audience = entity.Audience,
                MessageType = entity.MessageType,
                EntityId = entity.EntityId,
                UserId = entity.UserId,
                BusinessUnitId = entity.BusinessUnitId,
                CreatedBy = entity.CreatedBy,
                Id = entity.Id
            };
            return dto;
        }

        public static Message CopyTo(this MessageDto dto, Message entity)
        {
            entity.IsSystemAlert = dto.IsSystemAlert;
            entity.EmployerId = dto.EmployerId;
            entity.ExpiresOn = dto.ExpiresOn;
            entity.Audience = dto.Audience;
            entity.MessageType = dto.MessageType;
            entity.EntityId = dto.EntityId;
            entity.UserId = dto.UserId;
            entity.BusinessUnitId = dto.BusinessUnitId;
            entity.CreatedBy = dto.CreatedBy;
            return entity;
        }

        public static Message CopyTo(this MessageDto dto)
        {
            var entity = new Message();
            return dto.CopyTo(entity);
        }

        #endregion

        #region DismissedMessageDto Mappers

        public static DismissedMessageDto AsDto(this DismissedMessage entity)
        {
            var dto = new DismissedMessageDto
            {
                UserId = entity.UserId,
                MessageId = entity.MessageId,
                Id = entity.Id
            };
            return dto;
        }

        public static DismissedMessage CopyTo(this DismissedMessageDto dto, DismissedMessage entity)
        {
            entity.UserId = dto.UserId;
            entity.MessageId = dto.MessageId;
            return entity;
        }

        public static DismissedMessage CopyTo(this DismissedMessageDto dto)
        {
            var entity = new DismissedMessage();
            return dto.CopyTo(entity);
        }

        #endregion

        #region MessageTextDto Mappers

        public static MessageTextDto AsDto(this MessageText entity)
        {
            var dto = new MessageTextDto
            {
                Text = entity.Text,
                LocalisationId = entity.LocalisationId,
                MessageId = entity.MessageId,
                Id = entity.Id
            };
            return dto;
        }

        public static MessageText CopyTo(this MessageTextDto dto, MessageText entity)
        {
            entity.Text = dto.Text;
            entity.LocalisationId = dto.LocalisationId;
            entity.MessageId = dto.MessageId;
            return entity;
        }

        public static MessageText CopyTo(this MessageTextDto dto)
        {
            var entity = new MessageText();
            return dto.CopyTo(entity);
        }

        #endregion

        #region EmployeeSearchViewDto Mappers

        public static EmployeeSearchViewDto AsDto(this EmployeeSearchView entity)
        {
            var dto = new EmployeeSearchViewDto
            {
                FirstName = entity.FirstName,
                LastName = entity.LastName,
                EmployerId = entity.EmployerId,
                EmployerName = entity.EmployerName,
                IndustrialClassification = entity.IndustrialClassification,
                FederalEmployerIdentificationNumber = entity.FederalEmployerIdentificationNumber,
                WorkOpportunitiesTaxCreditHires = entity.WorkOpportunitiesTaxCreditHires,
                PostingFlags = entity.PostingFlags,
                ScreeningPreferences = entity.ScreeningPreferences,
                JobTitle = entity.JobTitle,
                EmployerCreatedOn = entity.EmployerCreatedOn,
                EmailAddress = entity.EmailAddress,
                PhoneNumber = entity.PhoneNumber,
                AccountEnabled = entity.AccountEnabled,
                EmployerApprovalStatus = entity.EmployerApprovalStatus,
                EmployeeApprovalStatus = entity.EmployeeApprovalStatus,
                TownCity = entity.TownCity,
                State = entity.State,
                BusinessUnitName = entity.BusinessUnitName,
                BusinessUnitId = entity.BusinessUnitId,
                LastLogin = entity.LastLogin,
                EmployeeBusinessUnitId = entity.EmployeeBusinessUnitId,
                StateId = entity.StateId,
                AccountBlocked = entity.AccountBlocked,
                EmployeeId = entity.EmployeeId,
                Extension = entity.Extension,
                EmployerIsPreferred = entity.EmployerIsPreferred,
                BusinessUnitIsPrimary = entity.BusinessUnitIsPrimary,
                EmployeeApprovalStatusReason = entity.EmployeeApprovalStatusReason,
                BusinessUnitApprovalStatus = entity.BusinessUnitApprovalStatus,
                BusinessUnitApprovalStatusReason = entity.BusinessUnitApprovalStatusReason,
                Unsubscribed = entity.Unsubscribed,
                AccountBlockedReason = entity.AccountBlockedReason,
                Id = entity.Id
            };
            return dto;
        }

        public static EmployeeSearchView CopyTo(this EmployeeSearchViewDto dto, EmployeeSearchView entity)
        {
            entity.FirstName = dto.FirstName;
            entity.LastName = dto.LastName;
            entity.EmployerId = dto.EmployerId;
            entity.EmployerName = dto.EmployerName;
            entity.IndustrialClassification = dto.IndustrialClassification;
            entity.FederalEmployerIdentificationNumber = dto.FederalEmployerIdentificationNumber;
            entity.WorkOpportunitiesTaxCreditHires = dto.WorkOpportunitiesTaxCreditHires;
            entity.PostingFlags = dto.PostingFlags;
            entity.ScreeningPreferences = dto.ScreeningPreferences;
            entity.JobTitle = dto.JobTitle;
            entity.EmployerCreatedOn = dto.EmployerCreatedOn;
            entity.EmailAddress = dto.EmailAddress;
            entity.PhoneNumber = dto.PhoneNumber;
            entity.AccountEnabled = dto.AccountEnabled;
            entity.EmployerApprovalStatus = dto.EmployerApprovalStatus;
            entity.EmployeeApprovalStatus = dto.EmployeeApprovalStatus;
            entity.TownCity = dto.TownCity;
            entity.State = dto.State;
            entity.BusinessUnitName = dto.BusinessUnitName;
            entity.BusinessUnitId = dto.BusinessUnitId;
            entity.LastLogin = dto.LastLogin;
            entity.EmployeeBusinessUnitId = dto.EmployeeBusinessUnitId;
            entity.StateId = dto.StateId;
            entity.AccountBlocked = dto.AccountBlocked;
            entity.EmployeeId = dto.EmployeeId;
            entity.Extension = dto.Extension;
            entity.EmployerIsPreferred = dto.EmployerIsPreferred;
            entity.BusinessUnitIsPrimary = dto.BusinessUnitIsPrimary;
            entity.EmployeeApprovalStatusReason = dto.EmployeeApprovalStatusReason;
            entity.BusinessUnitApprovalStatus = dto.BusinessUnitApprovalStatus;
            entity.BusinessUnitApprovalStatusReason = dto.BusinessUnitApprovalStatusReason;
            entity.Unsubscribed = dto.Unsubscribed;
            entity.AccountBlockedReason = dto.AccountBlockedReason;
            return entity;
        }

        public static EmployeeSearchView CopyTo(this EmployeeSearchViewDto dto)
        {
            var entity = new EmployeeSearchView();
            return dto.CopyTo(entity);
        }

        #endregion

        #region SavedMessageDto Mappers

        public static SavedMessageDto AsDto(this SavedMessage entity)
        {
            var dto = new SavedMessageDto
            {
                Audience = entity.Audience,
                Name = entity.Name,
                UserId = entity.UserId,
                Id = entity.Id
            };
            return dto;
        }

        public static SavedMessage CopyTo(this SavedMessageDto dto, SavedMessage entity)
        {
            entity.Audience = dto.Audience;
            entity.Name = dto.Name;
            entity.UserId = dto.UserId;
            return entity;
        }

        public static SavedMessage CopyTo(this SavedMessageDto dto)
        {
            var entity = new SavedMessage();
            return dto.CopyTo(entity);
        }

        #endregion

        #region SavedMessageTextDto Mappers

        public static SavedMessageTextDto AsDto(this SavedMessageText entity)
        {
            var dto = new SavedMessageTextDto
            {
                LocalisationId = entity.LocalisationId,
                Text = entity.Text,
                SavedMessageId = entity.SavedMessageId,
                Id = entity.Id
            };
            return dto;
        }

        public static SavedMessageText CopyTo(this SavedMessageTextDto dto, SavedMessageText entity)
        {
            entity.LocalisationId = dto.LocalisationId;
            entity.Text = dto.Text;
            entity.SavedMessageId = dto.SavedMessageId;
            return entity;
        }

        public static SavedMessageText CopyTo(this SavedMessageTextDto dto)
        {
            var entity = new SavedMessageText();
            return dto.CopyTo(entity);
        }

        #endregion

        #region JobSeekerReferralViewDto Mappers

        public static JobSeekerReferralViewDto AsDto(this JobSeekerReferralView entity)
        {
            var dto = new JobSeekerReferralViewDto
            {
                Name = entity.Name,
                ReferralDate = entity.ReferralDate,
                JobTitle = entity.JobTitle,
                EmployerName = entity.EmployerName,
                TimeInQueue = entity.TimeInQueue,
                EmployerId = entity.EmployerId,
                Posting = entity.Posting,
                CandidateId = entity.CandidateId,
                JobId = entity.JobId,
                ApprovalRequiredReason = entity.ApprovalRequiredReason,
                Veteran = entity.Veteran,
                Town = entity.Town,
                State = entity.State,
                StateId = entity.StateId,
                PersonId = entity.PersonId,
                ApplicationScore = entity.ApplicationScore,
                AssignedToId = entity.AssignedToId,
                LockVersion = entity.LockVersion,
                ForeignLabourCertificationH2A = entity.ForeignLabourCertificationH2A,
                ForeignLabourCertificationH2B = entity.ForeignLabourCertificationH2B,
                PostingLocations = entity.PostingLocations,
                JobSeekerAddress = entity.JobSeekerAddress,
                JobseekerTown = entity.JobseekerTown,
                JobseekerStateId = entity.JobseekerStateId,
                Id = entity.Id
            };
            return dto;
        }

        public static JobSeekerReferralView CopyTo(this JobSeekerReferralViewDto dto, JobSeekerReferralView entity)
        {
            entity.Name = dto.Name;
            entity.ReferralDate = dto.ReferralDate;
            entity.JobTitle = dto.JobTitle;
            entity.EmployerName = dto.EmployerName;
            entity.TimeInQueue = dto.TimeInQueue;
            entity.EmployerId = dto.EmployerId;
            entity.Posting = dto.Posting;
            entity.CandidateId = dto.CandidateId;
            entity.JobId = dto.JobId;
            entity.ApprovalRequiredReason = dto.ApprovalRequiredReason;
            entity.Veteran = dto.Veteran;
            entity.Town = dto.Town;
            entity.State = dto.State;
            entity.StateId = dto.StateId;
            entity.PersonId = dto.PersonId;
            entity.ApplicationScore = dto.ApplicationScore;
            entity.AssignedToId = dto.AssignedToId;
            entity.LockVersion = dto.LockVersion;
            entity.ForeignLabourCertificationH2A = dto.ForeignLabourCertificationH2A;
            entity.ForeignLabourCertificationH2B = dto.ForeignLabourCertificationH2B;
            entity.PostingLocations = dto.PostingLocations;
            entity.JobSeekerAddress = dto.JobSeekerAddress;
            entity.JobseekerTown = dto.JobseekerTown;
            entity.JobseekerStateId = dto.JobseekerStateId;
            return entity;
        }

        public static JobSeekerReferralView CopyTo(this JobSeekerReferralViewDto dto)
        {
            var entity = new JobSeekerReferralView();
            return dto.CopyTo(entity);
        }

        #endregion

        #region JobSeekerDashboardModel Mappers

        public static JobSeekerDashboardModel AsModel(this CandidateIssueView entity)
        {
            var model = new JobSeekerDashboardModel
            {
                Id = entity.Id,
                FirstName = entity.FirstName,
                LastName = entity.LastName,
                MiddleInitial = entity.MiddleInitial,
                DateOfBirth = entity.DateOfBirth,
                SocialSecurityNumber = entity.SocialSecurityNumber,
                TownCity = entity.TownCity,
                StateId = entity.StateId,
                EmailAddress = entity.EmailAddress,
                LastLoggedInOn = entity.LastLoggedInOn,
                NoLoginTriggered = entity.NoLoginTriggered,
                JobOfferRejectionTriggered = entity.JobOfferRejectionTriggered,
                NotReportingToInterviewTriggered = entity.NotReportingToInterviewTriggered,
                NotClickingOnLeadsTriggered = entity.NotClickingOnLeadsTriggered,
                NotRespondingToEmployerInvitesTriggered = entity.NotRespondingToEmployerInvitesTriggered,
                ShowingLowQualityMatchesTriggered = entity.ShowingLowQualityMatchesTriggered,
                PostingLowQualityResumeTriggered = entity.PostingLowQualityResumeTriggered,
                PostHireFollowUpTriggered = entity.PostHireFollowUpTriggered,
                FollowUpRequested = entity.FollowUpRequested,
                NotSearchingJobsTriggered = entity.NotSearchingJobsTriggered,
                InappropriateEmailAddress = entity.InappropriateEmailAddress,
                GivingNegativeFeedback = entity.GivingNegativeFeedback,
                GivingPositiveFeedback = entity.GivingPositiveFeedback,
                MigrantSeasonalFarmWorkerTriggered = entity.MigrantSeasonalFarmWorkerTriggered,
                MigrantSeasonalFarmWorkerVerified = entity.MigrantSeasonalFarmWorkerVerified,
                AssignedToId = entity.AssignedToId,
                IsVeteran = entity.IsVeteran,
                ExternalId = entity.ExternalId,
                Blocked = entity.Blocked,
                Enabled = entity.Enabled,
                BlockedReason = entity.BlockedReason,
                ExpiredAlienCertification = entity.HasExpiredAlienCertificationRegistration
            };
            return model;
        }

        #endregion

        #region EmployerAccountReferralViewDto Mappers

        public static EmployerAccountReferralViewDto AsDto(this EmployerAccountReferralView entity)
        {
            var dto = new EmployerAccountReferralViewDto
            {
                EmployerId = entity.EmployerId,
                EmployeeId = entity.EmployeeId,
                EmployeeFirstName = entity.EmployeeFirstName,
                EmployeeLastName = entity.EmployeeLastName,
                AccountCreationDate = entity.AccountCreationDate,
                EmployerName = entity.EmployerName,
                EmployerIndustrialClassification = entity.EmployerIndustrialClassification,
                EmployerPhoneNumber = entity.EmployerPhoneNumber,
                EmployerFederalEmployerIdentificationNumber = entity.EmployerFederalEmployerIdentificationNumber,
                EmployerStateEmployerIdentificationNumber = entity.EmployerStateEmployerIdentificationNumber,
                EmployerFaxNumber = entity.EmployerFaxNumber,
                EmployerUrl = entity.EmployerUrl,
                EmployeeAddressLine1 = entity.EmployeeAddressLine1,
                EmployeeAddressTownCity = entity.EmployeeAddressTownCity,
                EmployeeAddressStateId = entity.EmployeeAddressStateId,
                EmployeeAddressPostcodeZip = entity.EmployeeAddressPostcodeZip,
                EmployeeEmailAddress = entity.EmployeeEmailAddress,
                EmployeePhoneNumber = entity.EmployeePhoneNumber,
                EmployeeFaxNumber = entity.EmployeeFaxNumber,
                EmployerAddressLine1 = entity.EmployerAddressLine1,
                EmployerAddressTownCity = entity.EmployerAddressTownCity,
                EmployerAddressStateId = entity.EmployerAddressStateId,
                EmployerAddressPostcodeZip = entity.EmployerAddressPostcodeZip,
                EmployeeApprovalStatus = entity.EmployeeApprovalStatus,
                EmployerApprovalStatus = entity.EmployerApprovalStatus,
                EmployerAddressLine2 = entity.EmployerAddressLine2,
                EmployerOwnershipTypeId = entity.EmployerOwnershipTypeId,
                EmployerAssignedToId = entity.EmployerAssignedToId,
                RedProfanityWords = entity.RedProfanityWords,
                YellowProfanityWords = entity.YellowProfanityWords,
                BusinessUnitDescription = entity.BusinessUnitDescription,
                BusinessUnitName = entity.BusinessUnitName,
                EmployeeAddressLine2 = entity.EmployeeAddressLine2,
                BusinessUnitId = entity.BusinessUnitId,
                EmployerPublicTransitAccessible = entity.EmployerPublicTransitAccessible,
                PrimaryPhone = entity.PrimaryPhone,
                PrimaryPhoneType = entity.PrimaryPhoneType,
                AlternatePhone1 = entity.AlternatePhone1,
                AlternatePhone1Type = entity.AlternatePhone1Type,
                AlternatePhone2 = entity.AlternatePhone2,
                AlternatePhone2Type = entity.AlternatePhone2Type,
                ApprovalStatusChangedBy = entity.ApprovalStatusChangedBy,
                ApprovalStatusChangedOn = entity.ApprovalStatusChangedOn,
                TypeOfApproval = entity.TypeOfApproval,
                BusinessUnitApprovalStatus = entity.BusinessUnitApprovalStatus,
                EmployerIsPrimary = entity.EmployerIsPrimary,
                BusinessUnitLegalName = entity.BusinessUnitLegalName,
                BusinessUnitRedProfanityWords = entity.BusinessUnitRedProfanityWords,
                BusinessUnitYellowProfanityWords = entity.BusinessUnitYellowProfanityWords,
                SuffixId = entity.SuffixId,
                Suffix = entity.Suffix,
                AccountLockVersion = entity.AccountLockVersion,
                EmployerAccountTypeId = entity.EmployerAccountTypeId,
                AwaitingApprovalDate = entity.AwaitingApprovalDate,
                Id = entity.Id
            };
            return dto;
        }

        public static EmployerAccountReferralView CopyTo(this EmployerAccountReferralViewDto dto, EmployerAccountReferralView entity)
        {
            entity.EmployerId = dto.EmployerId;
            entity.EmployeeId = dto.EmployeeId;
            entity.EmployeeFirstName = dto.EmployeeFirstName;
            entity.EmployeeLastName = dto.EmployeeLastName;
            entity.AccountCreationDate = dto.AccountCreationDate;
            entity.EmployerName = dto.EmployerName;
            entity.EmployerIndustrialClassification = dto.EmployerIndustrialClassification;
            entity.EmployerPhoneNumber = dto.EmployerPhoneNumber;
            entity.EmployerFederalEmployerIdentificationNumber = dto.EmployerFederalEmployerIdentificationNumber;
            entity.EmployerStateEmployerIdentificationNumber = dto.EmployerStateEmployerIdentificationNumber;
            entity.EmployerFaxNumber = dto.EmployerFaxNumber;
            entity.EmployerUrl = dto.EmployerUrl;
            entity.EmployeeAddressLine1 = dto.EmployeeAddressLine1;
            entity.EmployeeAddressTownCity = dto.EmployeeAddressTownCity;
            entity.EmployeeAddressStateId = dto.EmployeeAddressStateId;
            entity.EmployeeAddressPostcodeZip = dto.EmployeeAddressPostcodeZip;
            entity.EmployeeEmailAddress = dto.EmployeeEmailAddress;
            entity.EmployeePhoneNumber = dto.EmployeePhoneNumber;
            entity.EmployeeFaxNumber = dto.EmployeeFaxNumber;
            entity.EmployerAddressLine1 = dto.EmployerAddressLine1;
            entity.EmployerAddressTownCity = dto.EmployerAddressTownCity;
            entity.EmployerAddressStateId = dto.EmployerAddressStateId;
            entity.EmployerAddressPostcodeZip = dto.EmployerAddressPostcodeZip;
            entity.EmployeeApprovalStatus = dto.EmployeeApprovalStatus;
            entity.EmployerApprovalStatus = dto.EmployerApprovalStatus;
            entity.EmployerAddressLine2 = dto.EmployerAddressLine2;
            entity.EmployerOwnershipTypeId = dto.EmployerOwnershipTypeId;
            entity.EmployerAssignedToId = dto.EmployerAssignedToId;
            entity.RedProfanityWords = dto.RedProfanityWords;
            entity.YellowProfanityWords = dto.YellowProfanityWords;
            entity.BusinessUnitDescription = dto.BusinessUnitDescription;
            entity.BusinessUnitName = dto.BusinessUnitName;
            entity.EmployeeAddressLine2 = dto.EmployeeAddressLine2;
            entity.BusinessUnitId = dto.BusinessUnitId;
            entity.EmployerPublicTransitAccessible = dto.EmployerPublicTransitAccessible;
            entity.PrimaryPhone = dto.PrimaryPhone;
            entity.PrimaryPhoneType = dto.PrimaryPhoneType;
            entity.AlternatePhone1 = dto.AlternatePhone1;
            entity.AlternatePhone1Type = dto.AlternatePhone1Type;
            entity.AlternatePhone2 = dto.AlternatePhone2;
            entity.AlternatePhone2Type = dto.AlternatePhone2Type;
            entity.ApprovalStatusChangedBy = dto.ApprovalStatusChangedBy;
            entity.ApprovalStatusChangedOn = dto.ApprovalStatusChangedOn;
            entity.TypeOfApproval = dto.TypeOfApproval;
            entity.BusinessUnitApprovalStatus = dto.BusinessUnitApprovalStatus;
            entity.EmployerIsPrimary = dto.EmployerIsPrimary;
            entity.BusinessUnitLegalName = dto.BusinessUnitLegalName;
            entity.BusinessUnitRedProfanityWords = dto.BusinessUnitRedProfanityWords;
            entity.BusinessUnitYellowProfanityWords = dto.BusinessUnitYellowProfanityWords;
            entity.SuffixId = dto.SuffixId;
            entity.Suffix = dto.Suffix;
            entity.AccountLockVersion = dto.AccountLockVersion;
            entity.EmployerAccountTypeId = dto.EmployerAccountTypeId;
            entity.AwaitingApprovalDate = dto.AwaitingApprovalDate;
            return entity;
        }

        public static EmployerAccountReferralView CopyTo(this EmployerAccountReferralViewDto dto)
        {
            var entity = new EmployerAccountReferralView();
            return dto.CopyTo(entity);
        }

        #endregion

        #region JobPostingReferralViewDto Mappers

        public static JobPostingReferralViewDto AsDto(this JobPostingReferralView entity)
        {
            var dto = new JobPostingReferralViewDto
            {
                JobTitle = entity.JobTitle,
                EmployerId = entity.EmployerId,
                EmployerName = entity.EmployerName,
                TimeInQueue = entity.TimeInQueue,
                EmployeeFirstName = entity.EmployeeFirstName,
                EmployeeLastName = entity.EmployeeLastName,
                EmployeeId = entity.EmployeeId,
                AwaitingApprovalOn = entity.AwaitingApprovalOn,
                CourtOrderedAffirmativeAction = entity.CourtOrderedAffirmativeAction,
                FederalContractor = entity.FederalContractor,
                ForeignLabourCertificationH2A = entity.ForeignLabourCertificationH2A,
                ForeignLabourCertificationH2B = entity.ForeignLabourCertificationH2B,
                ForeignLabourCertificationOther = entity.ForeignLabourCertificationOther,
                IsCommissionBased = entity.IsCommissionBased,
                IsSalaryAndCommissionBased = entity.IsSalaryAndCommissionBased,
                AssignedToId = entity.AssignedToId,
                SuitableForHomeWorker = entity.SuitableForHomeWorker,
                ApprovalStatus = entity.ApprovalStatus,
                JobLocationType = entity.JobLocationType,
                CreditCheckRequired = entity.CreditCheckRequired,
                JobStatus = entity.JobStatus,
                LockVersion = entity.LockVersion,
                CriminalBackgroundExclusionRequired = entity.CriminalBackGroundExclusionRequired,
                Id = entity.Id
            };
            return dto;
        }

        public static JobPostingReferralView CopyTo(this JobPostingReferralViewDto dto, JobPostingReferralView entity)
        {
            entity.JobTitle = dto.JobTitle;
            entity.EmployerId = dto.EmployerId;
            entity.EmployerName = dto.EmployerName;
            entity.TimeInQueue = dto.TimeInQueue;
            entity.EmployeeFirstName = dto.EmployeeFirstName;
            entity.EmployeeLastName = dto.EmployeeLastName;
            entity.EmployeeId = dto.EmployeeId;
            entity.AwaitingApprovalOn = dto.AwaitingApprovalOn;
            entity.CourtOrderedAffirmativeAction = dto.CourtOrderedAffirmativeAction;
            entity.FederalContractor = dto.FederalContractor;
            entity.ForeignLabourCertificationH2A = dto.ForeignLabourCertificationH2A;
            entity.ForeignLabourCertificationH2B = dto.ForeignLabourCertificationH2B;
            entity.ForeignLabourCertificationOther = dto.ForeignLabourCertificationOther;
            entity.IsCommissionBased = dto.IsCommissionBased;
            entity.IsSalaryAndCommissionBased = dto.IsSalaryAndCommissionBased;
            entity.AssignedToId = dto.AssignedToId;
            entity.SuitableForHomeWorker = dto.SuitableForHomeWorker;
            entity.ApprovalStatus = dto.ApprovalStatus;
            entity.JobLocationType = dto.JobLocationType;
            entity.CreditCheckRequired = dto.CreditCheckRequired;
            entity.JobStatus = dto.JobStatus;
            entity.LockVersion = dto.LockVersion;
            entity.CriminalBackGroundExclusionRequired = dto.CriminalBackgroundExclusionRequired;
            return entity;
        }

        public static JobPostingReferralView CopyTo(this JobPostingReferralViewDto dto)
        {
            var entity = new JobPostingReferralView();
            return dto.CopyTo(entity);
        }

        #endregion

        #region PersonListDto Mappers

        public static PersonListDto AsDto(this PersonList entity)
        {
            var dto = new PersonListDto
            {
                Name = entity.Name,
                ListType = entity.ListType,
                PersonId = entity.PersonId,
                Id = entity.Id
            };
            return dto;
        }

        public static PersonList CopyTo(this PersonListDto dto, PersonList entity)
        {
            entity.Name = dto.Name;
            entity.ListType = dto.ListType;
            entity.PersonId = dto.PersonId;
            return entity;
        }

        public static PersonList CopyTo(this PersonListDto dto)
        {
            var entity = new PersonList();
            return dto.CopyTo(entity);
        }

        #endregion

        #region StaffSearchViewDto Mappers

        public static StaffSearchViewDto AsDto(this StaffSearchView entity)
        {
            var dto = new StaffSearchViewDto
            {
                FirstName = entity.FirstName,
                LastName = entity.LastName,
                Enabled = entity.Enabled,
                EmailAddress = entity.EmailAddress,
                PersonId = entity.PersonId,
                Manager = entity.Manager,
                ExternalOffice = entity.ExternalOffice,
                Blocked = entity.Blocked,
                Id = entity.Id
            };
            return dto;
        }

        public static StaffSearchView CopyTo(this StaffSearchViewDto dto, StaffSearchView entity)
        {
            entity.FirstName = dto.FirstName;
            entity.LastName = dto.LastName;
            entity.Enabled = dto.Enabled;
            entity.EmailAddress = dto.EmailAddress;
            entity.PersonId = dto.PersonId;
            entity.Manager = dto.Manager;
            entity.ExternalOffice = dto.ExternalOffice;
            entity.Blocked = dto.Blocked;
            return entity;
        }

        public static StaffSearchView CopyTo(this StaffSearchViewDto dto)
        {
            var entity = new StaffSearchView();
            return dto.CopyTo(entity);
        }

        #endregion

        #region SingleSignOnDto Mappers

        public static SingleSignOnDto AsDto(this SingleSignOn entity)
        {
            var dto = new SingleSignOnDto
            {
                CreatedOn = entity.CreatedOn,
                UserId = entity.UserId,
                ActionerId = entity.ActionerId,
                Id = entity.Id,
                ExternalAdminId = entity.ExternalAdminId,
                ExternalOfficeId = entity.ExternalOfficeId,
                ExternalPassword = entity.ExternalPassword,
                ExternalUsername = entity.ExternalUsername
            };
            return dto;
        }

        public static SingleSignOn CopyTo(this SingleSignOnDto dto, SingleSignOn entity)
        {
            entity.UserId = dto.UserId;
            entity.ActionerId = dto.ActionerId;
            entity.ExternalAdminId = dto.ExternalAdminId;
            entity.ExternalOfficeId = dto.ExternalOfficeId;
            entity.ExternalPassword = dto.ExternalPassword;
            entity.ExternalUsername = dto.ExternalUsername;
            return entity;
        }

        public static SingleSignOn CopyTo(this SingleSignOnDto dto)
        {
            var entity = new SingleSignOn();
            return dto.CopyTo(entity);
        }

        #endregion

        #region PersonNoteDto Mappers

        public static PersonNoteDto AsDto(this PersonNote entity)
        {
            var dto = new PersonNoteDto
            {
                Note = entity.Note,
                DateAdded = entity.DateAdded,
                EmployerId = entity.EmployerId,
                PersonId = entity.PersonId,
                Id = entity.Id
            };
            return dto;
        }

        public static PersonNote CopyTo(this PersonNoteDto dto, PersonNote entity)
        {
            entity.Note = dto.Note;
            entity.DateAdded = dto.DateAdded;
            entity.EmployerId = dto.EmployerId;
            entity.PersonId = dto.PersonId;
            return entity;
        }

        public static PersonNote CopyTo(this PersonNoteDto dto)
        {
            var entity = new PersonNote();
            return dto.CopyTo(entity);
        }

        #endregion

        #region ApplicationViewDto Mappers

        public static ApplicationViewDto AsDto(this ApplicationView entity)
        {
            var dto = new ApplicationViewDto
            {
                JobId = entity.JobId,
                EmployerId = entity.EmployerId,
                CandidateId = entity.CandidateId,
                CandidateApplicationStatus = entity.CandidateApplicationStatus,
                CandidateApplicationScore = entity.CandidateApplicationScore,
                CandidateApplicationReceivedOn = entity.CandidateApplicationReceivedOn,
                CandidateApplicationApprovalStatus = entity.CandidateApplicationApprovalStatus,
                CandidateFirstName = entity.CandidateFirstName,
                CandidateLastName = entity.CandidateLastName,
                CandidateYearsExperience = entity.CandidateYearsExperience,
                CandidateIsVeteran = entity.CandidateIsVeteran,
                ApplicationStatusLastChangedOn = entity.ApplicationStatusLastChangedOn,
                EmployeeId = entity.EmployeeId,
                BusinessUnitId = entity.BusinessUnitId,
                Viewed = entity.Viewed,
                PostHireFollowUpStatus = entity.PostHireFollowUpStatus,
                BusinuessUnitName = entity.BusinuessUnitName,
                EmployeeFirstName = entity.EmployeeFirstName,
                EmployeeLastName = entity.EmployeeLastName,
                EmployeeEmail = entity.EmployeeEmail,
                JobClosingOn = entity.JobClosingOn,
                JobTitle = entity.JobTitle,
                LensPostingId = entity.LensPostingId,
                EmployeePhoneNumber = entity.EmployeePhoneNumber,
                VeteranPriorityEndDate = entity.VeteranPriorityEndDate,
                CandidateApplicationAutomaticallyApproved = entity.CandidateApplicationAutomaticallyApproved,
                CandidateIsContactInfoVisible = entity.CandidateIsContactInfoVisible,
                Branding = entity.Branding,
                CandidateNcrcLevelId = entity.CandidateNcrcLevelId,
                LockVersion = entity.LockVersion,
                StarRating = entity.StarRating,
                Id = entity.Id
            };
            return dto;
        }

        public static ApplicationView CopyTo(this ApplicationViewDto dto, ApplicationView entity)
        {
            entity.JobId = dto.JobId;
            entity.EmployerId = dto.EmployerId;
            entity.CandidateId = dto.CandidateId;
            entity.CandidateApplicationStatus = dto.CandidateApplicationStatus;
            entity.CandidateApplicationScore = dto.CandidateApplicationScore;
            entity.CandidateApplicationReceivedOn = dto.CandidateApplicationReceivedOn;
            entity.CandidateApplicationApprovalStatus = dto.CandidateApplicationApprovalStatus;
            entity.CandidateFirstName = dto.CandidateFirstName;
            entity.CandidateLastName = dto.CandidateLastName;
            entity.CandidateYearsExperience = dto.CandidateYearsExperience;
            entity.CandidateIsVeteran = dto.CandidateIsVeteran;
            entity.ApplicationStatusLastChangedOn = dto.ApplicationStatusLastChangedOn;
            entity.EmployeeId = dto.EmployeeId;
            entity.BusinessUnitId = dto.BusinessUnitId;
            entity.Viewed = dto.Viewed;
            entity.PostHireFollowUpStatus = dto.PostHireFollowUpStatus;
            entity.BusinuessUnitName = dto.BusinuessUnitName;
            entity.EmployeeFirstName = dto.EmployeeFirstName;
            entity.EmployeeLastName = dto.EmployeeLastName;
            entity.EmployeeEmail = dto.EmployeeEmail;
            entity.JobClosingOn = dto.JobClosingOn;
            entity.JobTitle = dto.JobTitle;
            entity.LensPostingId = dto.LensPostingId;
            entity.EmployeePhoneNumber = dto.EmployeePhoneNumber;
            entity.VeteranPriorityEndDate = dto.VeteranPriorityEndDate;
            entity.CandidateApplicationAutomaticallyApproved = dto.CandidateApplicationAutomaticallyApproved;
            entity.CandidateIsContactInfoVisible = dto.CandidateIsContactInfoVisible;
            entity.Branding = dto.Branding;
            entity.CandidateNcrcLevelId = dto.CandidateNcrcLevelId;
            entity.LockVersion = dto.LockVersion;
            entity.StarRating = dto.StarRating;
            return entity;
        }

        public static ApplicationView CopyTo(this ApplicationViewDto dto)
        {
            var entity = new ApplicationView();
            return dto.CopyTo(entity);
        }

        #endregion

        #region CandidateNoteViewDto Mappers

        public static CandidateNoteViewDto AsDto(this CandidateNoteView entity)
        {
            var dto = new CandidateNoteViewDto
            {
                Note = entity.Note,
                EmployerId = entity.EmployerId,
                CandidateId = entity.CandidateId,
                NoteAddedDateTime = entity.NoteAddedDateTime,
                EmployerName = entity.EmployerName,
                Id = entity.Id
            };
            return dto;
        }

        public static CandidateNoteView CopyTo(this CandidateNoteViewDto dto, CandidateNoteView entity)
        {
            entity.Note = dto.Note;
            entity.EmployerId = dto.EmployerId;
            entity.CandidateId = dto.CandidateId;
            entity.NoteAddedDateTime = dto.NoteAddedDateTime;
            entity.EmployerName = dto.EmployerName;
            return entity;
        }

        public static CandidateNoteView CopyTo(this CandidateNoteViewDto dto)
        {
            var entity = new CandidateNoteView();
            return dto.CopyTo(entity);
        }

        #endregion

        #region PersonListCandidateViewDto Mappers

        public static PersonListCandidateViewDto AsDto(this PersonListCandidateView entity)
        {
            var dto = new PersonListCandidateViewDto
            {
                ListType = entity.ListType,
                PersonId = entity.PersonId,
                CandidateId = entity.CandidateId,
                CandidateFirstName = entity.CandidateFirstName,
                CandidateLastName = entity.CandidateLastName,
                CandidateIsVeteran = entity.CandidateIsVeteran,
                CandidatePersonId = entity.CandidatePersonId,
                CandidateTownCity = entity.CandidateTownCity,
                CandidateStateId = entity.CandidateStateId,
                CandidateIsContactInfoVisible = entity.CandidateIsContactInfoVisible,
                Id = entity.Id
            };
            return dto;
        }

        public static PersonListCandidateView CopyTo(this PersonListCandidateViewDto dto, PersonListCandidateView entity)
        {
            entity.ListType = dto.ListType;
            entity.PersonId = dto.PersonId;
            entity.CandidateId = dto.CandidateId;
            entity.CandidateFirstName = dto.CandidateFirstName;
            entity.CandidateLastName = dto.CandidateLastName;
            entity.CandidateIsVeteran = dto.CandidateIsVeteran;
            entity.CandidatePersonId = dto.CandidatePersonId;
            entity.CandidateTownCity = dto.CandidateTownCity;
            entity.CandidateStateId = dto.CandidateStateId;
            entity.CandidateIsContactInfoVisible = dto.CandidateIsContactInfoVisible;
            return entity;
        }

        public static PersonListCandidateView CopyTo(this PersonListCandidateViewDto dto)
        {
            var entity = new PersonListCandidateView();
            return dto.CopyTo(entity);
        }

        #endregion

        #region ExpiringJobViewDto Mappers

        public static ExpiringJobViewDto AsDto(this ExpiringJobView entity)
        {
            var dto = new ExpiringJobViewDto
            {
                JobTitle = entity.JobTitle,
                EmployerId = entity.EmployerId,
                EmployeeId = entity.EmployeeId,
                ClosingOn = entity.ClosingOn,
                DaysToClosing = entity.DaysToClosing,
                PostedOn = entity.PostedOn,
                UserId = entity.UserId,
                FirstName = entity.FirstName,
                BusinessUnitId = entity.BusinessUnitId,
                BusinessUnitName = entity.BusinessUnitName,
                Id = entity.Id
            };
            return dto;
        }

        public static ExpiringJobView CopyTo(this ExpiringJobViewDto dto, ExpiringJobView entity)
        {
            entity.JobTitle = dto.JobTitle;
            entity.EmployerId = dto.EmployerId;
            entity.EmployeeId = dto.EmployeeId;
            entity.ClosingOn = dto.ClosingOn;
            entity.DaysToClosing = dto.DaysToClosing;
            entity.PostedOn = dto.PostedOn;
            entity.UserId = dto.UserId;
            entity.FirstName = dto.FirstName;
            entity.BusinessUnitId = dto.BusinessUnitId;
            entity.BusinessUnitName = dto.BusinessUnitName;
            return entity;
        }

        public static ExpiringJobView CopyTo(this ExpiringJobViewDto dto)
        {
            var entity = new ExpiringJobView();
            return dto.CopyTo(entity);
        }

        #endregion

        #region BookmarkDto Mappers

        public static BookmarkDto AsDto(this Bookmark entity)
        {
            var dto = new BookmarkDto
            {
                Name = entity.Name,
                Type = entity.Type,
                Criteria = entity.Criteria,
                UserId = entity.UserId,
                UserType = entity.UserType,
                EntityId = entity.EntityId,
                Id = entity.Id
            };
            return dto;
        }

        public static Bookmark CopyTo(this BookmarkDto dto, Bookmark entity)
        {
            entity.Name = dto.Name;
            entity.Type = dto.Type;
            entity.Criteria = dto.Criteria;
            entity.UserId = dto.UserId;
            entity.UserType = dto.UserType;
            entity.EntityId = dto.EntityId;
            return entity;
        }

        public static Bookmark CopyTo(this BookmarkDto dto)
        {
            var entity = new Bookmark();
            return dto.CopyTo(entity);
        }

        #endregion

        #region ResumeDto Mappers

        public static ResumeDto AsDto(this Resume entity)
        {
            var dto = new ResumeDto
            {
                CreatedOn = entity.CreatedOn,
                UpdatedOn = entity.UpdatedOn,
                PrimaryOnet = entity.PrimaryOnet,
                PrimaryROnet = entity.PrimaryROnet,
                ResumeSkills = entity.ResumeSkills,
                FirstName = entity.FirstName,
                LastName = entity.LastName,
                MiddleName = entity.MiddleName,
                DateOfBirth = entity.DateOfBirth,
                Gender = entity.Gender,
                AddressLine1 = entity.AddressLine1,
                TownCity = entity.TownCity,
                StateId = entity.StateId,
                CountryId = entity.CountryId,
                PostcodeZip = entity.PostcodeZip,
                WorkPhone = entity.WorkPhone,
                HomePhone = entity.HomePhone,
                MobilePhone = entity.MobilePhone,
                FaxNumber = entity.FaxNumber,
                EmailAddress = entity.EmailAddress,
                HighestEducationLevel = entity.HighestEducationLevel,
                YearsExperience = entity.YearsExperience,
                IsVeteran = entity.IsVeteran,
                IsActive = entity.IsActive,
                StatusId = entity.StatusId,
                ResumeXml = entity.ResumeXml,
                ResumeCompletionStatusId = entity.ResumeCompletionStatusId,
                IsDefault = entity.IsDefault,
                ResumeName = entity.ResumeName,
                IsSearchable = entity.IsSearchable,
                SkillsAlreadyCaptured = entity.SkillsAlreadyCaptured,
                CountyId = entity.CountyId,
                HomelessNoShelter = entity.HomelessNoShelter,
                HomelessWithShelter = entity.HomelessWithShelter,
                RunawayYouth = entity.RunawayYouth18OrUnder,
                ExOffender = entity.ExOffender,
                SkillCount = entity.SkillCount,
                WordCount = entity.WordCount,
                VeteranPriorityServiceAlertsSubscription = entity.VeteranPriorityServiceAlertsSubscription,
                EducationCompletionDate = entity.EducationCompletionDate,
                ResetSearchable = entity.ResetSearchable,
                IsContactInfoVisible = entity.IsContactInfoVisible,
                ExternalId = entity.ExternalId,
                NcrcLevelId = entity.NcrcLevelId,
                SuffixId = entity.SuffixId,
                MigrantSeasonalFarmWorker = entity.MigrantSeasonalFarmWorker,
                PersonId = entity.PersonId,
                IsUSCitizen = entity.IsUSCitizen,
                IsPermanentResident = entity.IsPermanentResident,
                AlienRegistrationNumber = entity.AlienRegistrationNumber,
                AlienRegExpiryDate = entity.AlienRegExpiryDate,
                Id = entity.Id,
                DisplacedHomemaker = entity.DisplacedHomemaker,
                SingleParent = entity.SingleParent,
                LowIncomeStatus = entity.LowIncomeStatus,
                LowLeveLiteracy = entity.LowLevelLiteracy,
                CulturalBarriers = entity.CulturalBarriers,
                PreferredLanguage = entity.PreferredLanguage,
                NativeLanguage = entity.NativeLanguage,
                CommonLanguage = entity.CommonLanguage,
                EstMonthlyIncome = entity.EstMonthlyIncome,
                NoOfDependents = entity.NoOfDependents
            };
            return dto;
        }

        public static Resume CopyTo(this ResumeDto dto, Resume entity)
        {
            entity.PrimaryOnet = dto.PrimaryOnet;
            entity.PrimaryROnet = dto.PrimaryROnet;
            entity.ResumeSkills = dto.ResumeSkills;
            entity.FirstName = dto.FirstName;
            entity.LastName = dto.LastName;
            entity.MiddleName = dto.MiddleName;
            entity.DateOfBirth = dto.DateOfBirth;
            entity.Gender = dto.Gender;
            entity.AddressLine1 = dto.AddressLine1;
            entity.TownCity = dto.TownCity;
            entity.StateId = dto.StateId;
            entity.CountryId = dto.CountryId;
            entity.PostcodeZip = dto.PostcodeZip;
            entity.WorkPhone = dto.WorkPhone;
            entity.HomePhone = dto.HomePhone;
            entity.MobilePhone = dto.MobilePhone;
            entity.FaxNumber = dto.FaxNumber;
            entity.EmailAddress = dto.EmailAddress;
            entity.HighestEducationLevel = dto.HighestEducationLevel;
            entity.YearsExperience = dto.YearsExperience;
            entity.IsVeteran = dto.IsVeteran;
            entity.IsActive = dto.IsActive;
            entity.StatusId = dto.StatusId;
            entity.ResumeXml = dto.ResumeXml;
            entity.ResumeCompletionStatusId = dto.ResumeCompletionStatusId;
            entity.IsDefault = dto.IsDefault;
            entity.ResumeName = dto.ResumeName;
            entity.IsSearchable = dto.IsSearchable;
            entity.SkillsAlreadyCaptured = dto.SkillsAlreadyCaptured;
            entity.CountyId = dto.CountyId;
            entity.HomelessNoShelter = dto.HomelessNoShelter;
            entity.HomelessWithShelter = dto.HomelessWithShelter;
            entity.RunawayYouth18OrUnder = dto.RunawayYouth;
            entity.ExOffender = dto.ExOffender;
            entity.SkillCount = dto.SkillCount;
            entity.WordCount = dto.WordCount;
            entity.VeteranPriorityServiceAlertsSubscription = dto.VeteranPriorityServiceAlertsSubscription;
            entity.EducationCompletionDate = dto.EducationCompletionDate;
            entity.ResetSearchable = dto.ResetSearchable;
            entity.IsContactInfoVisible = dto.IsContactInfoVisible;
            entity.ExternalId = dto.ExternalId;
            entity.NcrcLevelId = dto.NcrcLevelId;
            entity.SuffixId = dto.SuffixId;
            entity.MigrantSeasonalFarmWorker = dto.MigrantSeasonalFarmWorker;
            entity.PersonId = dto.PersonId;
            entity.IsUSCitizen = dto.IsUSCitizen;
            entity.IsPermanentResident = dto.IsPermanentResident;
            entity.AlienRegistrationNumber = dto.AlienRegistrationNumber;
            entity.AlienRegExpiryDate = dto.AlienRegExpiryDate;
            entity.DisplacedHomemaker = dto.DisplacedHomemaker;
            entity.SingleParent = dto.SingleParent;
            entity.LowIncomeStatus = dto.LowIncomeStatus;
            entity.LowLevelLiteracy = dto.LowLeveLiteracy;
            entity.CulturalBarriers = dto.CulturalBarriers;
            entity.PreferredLanguage = dto.PreferredLanguage;
            entity.NativeLanguage = dto.NativeLanguage;
            entity.CommonLanguage = dto.CommonLanguage;
            entity.EstMonthlyIncome = dto.EstMonthlyIncome;
            entity.NoOfDependents = dto.NoOfDependents;
            return entity;
        }

        public static Resume CopyTo(this ResumeDto dto)
        {
            var entity = new Resume();
            return dto.CopyTo(entity);
        }

        #endregion

        #region ResumeJobDto Mappers

        public static ResumeJobDto AsDto(this ResumeJob entity)
        {
            var dto = new ResumeJobDto
            {
                JobTitle = entity.JobTitle,
                Employer = entity.Employer,
                StartYear = entity.StartYear,
                EndYear = entity.EndYear,
                LensId = entity.LensId,
                ResumeId = entity.ResumeId,
                Id = entity.Id,
                JobOnet = entity.JobOnet
            };
            return dto;
        }

        public static ResumeJob CopyTo(this ResumeJobDto dto, ResumeJob entity)
        {
            entity.JobTitle = dto.JobTitle;
            entity.Employer = dto.Employer;
            entity.StartYear = dto.StartYear;
            entity.EndYear = dto.EndYear;
            entity.LensId = dto.LensId;
            entity.ResumeId = dto.ResumeId;
            entity.JobOnet = dto.JobOnet;
            return entity;
        }

        public static ResumeJob CopyTo(this ResumeJobDto dto)
        {
            var entity = new ResumeJob();
            return dto.CopyTo(entity);
        }

        #endregion

        #region StaffProfileDto Mappers

        public static StaffProfileDto AsDto(this StaffProfile entity)
        {
            var dto = new StaffProfileDto
            {
                FirstName = entity.FirstName,
                LastName = entity.LastName,
                Enabled = entity.Enabled,
                PhoneNumber = entity.PhoneNumber,
                FaxNumber = entity.FaxNumber,
                TownCity = entity.TownCity,
                StateId = entity.StateId,
                EmailAddress = entity.EmailAddress,
                ExternalOffice = entity.ExternalOffice,
                Id = entity.Id
            };
            return dto;
        }

        public static StaffProfile CopyTo(this StaffProfileDto dto, StaffProfile entity)
        {
            entity.FirstName = dto.FirstName;
            entity.LastName = dto.LastName;
            entity.Enabled = dto.Enabled;
            entity.PhoneNumber = dto.PhoneNumber;
            entity.FaxNumber = dto.FaxNumber;
            entity.TownCity = dto.TownCity;
            entity.StateId = dto.StateId;
            entity.EmailAddress = dto.EmailAddress;
            entity.ExternalOffice = dto.ExternalOffice;
            return entity;
        }

        public static StaffProfile CopyTo(this StaffProfileDto dto)
        {
            var entity = new StaffProfile();
            return dto.CopyTo(entity);
        }

        #endregion

        #region JobActionEventViewDto Mappers

        public static JobActionEventViewDto AsDto(this JobActionEventView entity)
        {
            var dto = new JobActionEventViewDto
            {
                JobId = entity.JobId,
                FirstName = entity.FirstName,
                LastName = entity.LastName,
                ActionedOn = entity.ActionedOn,
                ActionName = entity.ActionName,
                UserId = entity.UserId,
                AdditionalDetails = entity.AdditionalDetails,
                AdditionalName = entity.AdditionalName,
                EntityIdAdditional01 = entity.EntityIdAdditional01,
                EntityIdAdditional02 = entity.EntityIdAdditional02,
                Id = entity.Id
            };
            return dto;
        }

        public static JobActionEventView CopyTo(this JobActionEventViewDto dto, JobActionEventView entity)
        {
            entity.JobId = dto.JobId;
            entity.FirstName = dto.FirstName;
            entity.LastName = dto.LastName;
            entity.ActionedOn = dto.ActionedOn;
            entity.ActionName = dto.ActionName;
            entity.UserId = dto.UserId;
            entity.AdditionalDetails = dto.AdditionalDetails;
            entity.AdditionalName = dto.AdditionalName;
            entity.EntityIdAdditional01 = dto.EntityIdAdditional01;
            entity.EntityIdAdditional02 = dto.EntityIdAdditional02;
            return entity;
        }

        public static JobActionEventView CopyTo(this JobActionEventViewDto dto)
        {
            var entity = new JobActionEventView();
            return dto.CopyTo(entity);
        }

        #endregion

        #region NoteReminderDto Mappers

        public static NoteReminderDto AsDto(this NoteReminder entity)
        {
            var dto = new NoteReminderDto
            {
                CreatedOn = entity.CreatedOn,
                NoteReminderType = entity.NoteReminderType,
                Text = entity.Text,
                EntityId = entity.EntityId,
                EntityType = entity.EntityType,
                ReminderVia = entity.ReminderVia,
                ReminderDue = entity.ReminderDue,
                ReminderSentOn = entity.ReminderSentOn,
                CreatedBy = entity.CreatedBy,
                NoteType = entity.NoteType,
                Id = entity.Id
            };
            return dto;
        }

        public static NoteReminder CopyTo(this NoteReminderDto dto, NoteReminder entity)
        {
            entity.NoteReminderType = dto.NoteReminderType;
            entity.Text = dto.Text;
            entity.EntityId = dto.EntityId;
            entity.EntityType = dto.EntityType;
            entity.ReminderVia = dto.ReminderVia;
            entity.ReminderDue = dto.ReminderDue;
            entity.ReminderSentOn = dto.ReminderSentOn;
            entity.CreatedBy = dto.CreatedBy;
            entity.NoteType = dto.NoteType;
            return entity;
        }

        public static NoteReminder CopyTo(this NoteReminderDto dto)
        {
            var entity = new NoteReminder();
            return dto.CopyTo(entity);
        }

        #endregion

        #region NoteReminderViewDto Mappers

        public static NoteReminderViewDto AsDto(this NoteReminderView entity)
        {
            var dto = new NoteReminderViewDto
            {
                CreatedOn = entity.CreatedOn,
                NoteReminderType = entity.NoteReminderType,
                EntityId = entity.EntityId,
                Text = entity.Text,
                CreatedByFirstname = entity.CreatedByFirstname,
                CreatedByLastname = entity.CreatedByLastname,
                EntityType = entity.EntityType,
                NoteType = entity.NoteType,
                Id = entity.Id
            };
            return dto;
        }

        public static NoteReminderView CopyTo(this NoteReminderViewDto dto, NoteReminderView entity)
        {
            entity.NoteReminderType = dto.NoteReminderType;
            entity.EntityId = dto.EntityId;
            entity.Text = dto.Text;
            entity.CreatedByFirstname = dto.CreatedByFirstname;
            entity.CreatedByLastname = dto.CreatedByLastname;
            entity.EntityType = dto.EntityType;
            entity.NoteType = dto.NoteType;
            return entity;
        }

        public static NoteReminderView CopyTo(this NoteReminderViewDto dto)
        {
            var entity = new NoteReminderView();
            return dto.CopyTo(entity);
        }

        #endregion

        #region StatusLogDto Mappers

        public static StatusLogDto AsDto(this StatusLog entity)
        {
            var dto = new StatusLogDto
            {
                EntityId = entity.EntityId,
                OriginalStatus = entity.OriginalStatus,
                NewStatus = entity.NewStatus,
                UserId = entity.UserId,
                ActionedOn = entity.ActionedOn,
                EntityTypeId = entity.EntityTypeId,
                Id = entity.Id
            };
            return dto;
        }

        public static StatusLog CopyTo(this StatusLogDto dto, StatusLog entity)
        {
            entity.EntityId = dto.EntityId;
            entity.OriginalStatus = dto.OriginalStatus;
            entity.NewStatus = dto.NewStatus;
            entity.UserId = dto.UserId;
            entity.ActionedOn = dto.ActionedOn;
            entity.EntityTypeId = dto.EntityTypeId;
            return entity;
        }

        public static StatusLog CopyTo(this StatusLogDto dto)
        {
            var entity = new StatusLog();
            return dto.CopyTo(entity);
        }

        #endregion

        #region ApplicationStatusLogViewDto Mappers

        public static ApplicationStatusLogViewDto AsDto(this ApplicationStatusLogView entity)
        {
            var dto = new ApplicationStatusLogViewDto
            {
                ActionedOn = entity.ActionedOn,
                CandidateApplicationScore = entity.CandidateApplicationScore,
                CandidateFirstName = entity.CandidateFirstName,
                CandidateLastName = entity.CandidateLastName,
                CandidateApplicationStatus = entity.CandidateApplicationStatus,
                JobId = entity.JobId,
                CandidateYearsExperience = entity.CandidateYearsExperience,
                CandidateIsVeteran = entity.CandidateIsVeteran,
                LatestJobTitle = entity.LatestJobTitle,
                LatestJobEmployer = entity.LatestJobEmployer,
                LatestJobStartYear = entity.LatestJobStartYear,
                LatestJobEndYear = entity.LatestJobEndYear,
                CandidateApplicationId = entity.CandidateApplicationId,
                JobTitle = entity.JobTitle,
                BusinessUnitId = entity.BusinessUnitId,
                PersonId = entity.PersonId,
                CandidateApprovalStatus = entity.CandidateApprovalStatus,
                VeteranPriorityEndDate = entity.VeteranPriorityEndDate,
                CandidateOriginalApplicationStatus = entity.CandidateOriginalApplicationStatus,
                ProgramAreaId = entity.ProgramAreaId,
                DegreeId = entity.DegreeId,
                EducationCompletionDate = entity.EducationCompletionDate,
                JobStatus = entity.JobStatus,
                Id = entity.Id
            };
            return dto;
        }

        public static ApplicationStatusLogView CopyTo(this ApplicationStatusLogViewDto dto, ApplicationStatusLogView entity)
        {
            entity.ActionedOn = dto.ActionedOn;
            entity.CandidateApplicationScore = dto.CandidateApplicationScore;
            entity.CandidateFirstName = dto.CandidateFirstName;
            entity.CandidateLastName = dto.CandidateLastName;
            entity.CandidateApplicationStatus = dto.CandidateApplicationStatus;
            entity.JobId = dto.JobId;
            entity.CandidateYearsExperience = dto.CandidateYearsExperience;
            entity.CandidateIsVeteran = dto.CandidateIsVeteran;
            entity.LatestJobTitle = dto.LatestJobTitle;
            entity.LatestJobEmployer = dto.LatestJobEmployer;
            entity.LatestJobStartYear = dto.LatestJobStartYear;
            entity.LatestJobEndYear = dto.LatestJobEndYear;
            entity.CandidateApplicationId = dto.CandidateApplicationId;
            entity.JobTitle = dto.JobTitle;
            entity.BusinessUnitId = dto.BusinessUnitId;
            entity.PersonId = dto.PersonId;
            entity.CandidateApprovalStatus = dto.CandidateApprovalStatus;
            entity.VeteranPriorityEndDate = dto.VeteranPriorityEndDate;
            entity.CandidateOriginalApplicationStatus = dto.CandidateOriginalApplicationStatus;
            entity.ProgramAreaId = dto.ProgramAreaId;
            entity.DegreeId = dto.DegreeId;
            entity.EducationCompletionDate = dto.EducationCompletionDate;
            entity.JobStatus = dto.JobStatus;
            return entity;
        }

        public static ApplicationStatusLogView CopyTo(this ApplicationStatusLogViewDto dto)
        {
            var entity = new ApplicationStatusLogView();
            return dto.CopyTo(entity);
        }

        #endregion

        #region ActiveSearchAlertViewDto Mappers

        public static ActiveSearchAlertViewDto AsDto(this ActiveSearchAlertView entity)
        {
            var dto = new ActiveSearchAlertViewDto
            {
                Name = entity.Name,
                SearchCriteria = entity.SearchCriteria,
                AlertEmailRequired = entity.AlertEmailRequired,
                AlertEmailFrequency = entity.AlertEmailFrequency,
                AlertEmailFormat = entity.AlertEmailFormat,
                AlertEmailAddress = entity.AlertEmailAddress,
                AlertEmailScheduledOn = entity.AlertEmailScheduledOn,
                Type = entity.Type,
                Id = entity.Id
            };
            return dto;
        }

        public static ActiveSearchAlertView CopyTo(this ActiveSearchAlertViewDto dto, ActiveSearchAlertView entity)
        {
            entity.Name = dto.Name;
            entity.SearchCriteria = dto.SearchCriteria;
            entity.AlertEmailRequired = dto.AlertEmailRequired;
            entity.AlertEmailFrequency = dto.AlertEmailFrequency;
            entity.AlertEmailFormat = dto.AlertEmailFormat;
            entity.AlertEmailAddress = dto.AlertEmailAddress;
            entity.AlertEmailScheduledOn = dto.AlertEmailScheduledOn;
            entity.Type = dto.Type;
            return entity;
        }

        public static ActiveSearchAlertView CopyTo(this ActiveSearchAlertViewDto dto)
        {
            var entity = new ActiveSearchAlertView();
            return dto.CopyTo(entity);
        }

        #endregion

        #region BusinessUnitAddressDto Mappers

        public static BusinessUnitAddressDto AsDto(this BusinessUnitAddress entity)
        {
            var dto = new BusinessUnitAddressDto
            {
                Line1 = entity.Line1,
                Line2 = entity.Line2,
                Line3 = entity.Line3,
                TownCity = entity.TownCity,
                CountyId = entity.CountyId,
                PostcodeZip = entity.PostcodeZip,
                StateId = entity.StateId,
                CountryId = entity.CountryId,
                IsPrimary = entity.IsPrimary,
                PublicTransitAccessible = entity.PublicTransitAccessible,
                BusinessUnitId = entity.BusinessUnitId,
                Id = entity.Id
            };
            return dto;
        }

        public static BusinessUnitAddress CopyTo(this BusinessUnitAddressDto dto, BusinessUnitAddress entity)
        {
            entity.Line1 = dto.Line1;
            entity.Line2 = dto.Line2;
            entity.Line3 = dto.Line3;
            entity.TownCity = dto.TownCity;
            entity.CountyId = dto.CountyId;
            entity.PostcodeZip = dto.PostcodeZip;
            entity.StateId = dto.StateId;
            entity.CountryId = dto.CountryId;
            entity.IsPrimary = dto.IsPrimary;
            entity.PublicTransitAccessible = dto.PublicTransitAccessible;
            entity.BusinessUnitId = dto.BusinessUnitId;
            return entity;
        }

        public static BusinessUnitAddress CopyTo(this BusinessUnitAddressDto dto)
        {
            var entity = new BusinessUnitAddress();
            return dto.CopyTo(entity);
        }

        #endregion

        #region BusinessUnitDescriptionDto Mappers

        public static BusinessUnitDescriptionDto AsDto(this BusinessUnitDescription entity)
        {
            var dto = new BusinessUnitDescriptionDto
            {
                Title = entity.Title,
                Description = entity.Description,
                IsPrimary = entity.IsPrimary,
                RedProfanityWords = entity.RedProfanityWords,
                YellowProfanityWords = entity.YellowProfanityWords,
                BusinessUnitId = entity.BusinessUnitId,
                Id = entity.Id
            };
            return dto;
        }

        public static BusinessUnitDescription CopyTo(this BusinessUnitDescriptionDto dto, BusinessUnitDescription entity)
        {
            entity.Title = dto.Title;
            entity.Description = dto.Description;
            entity.IsPrimary = dto.IsPrimary;
            entity.RedProfanityWords = dto.RedProfanityWords;
            entity.YellowProfanityWords = dto.YellowProfanityWords;
            entity.BusinessUnitId = dto.BusinessUnitId;
            return entity;
        }

        public static BusinessUnitDescription CopyTo(this BusinessUnitDescriptionDto dto)
        {
            var entity = new BusinessUnitDescription();
            return dto.CopyTo(entity);
        }

        #endregion

        #region BusinessUnitLogoDto Mappers

        public static BusinessUnitLogoDto AsDto(this BusinessUnitLogo entity)
        {
            var dto = new BusinessUnitLogoDto
            {
                Name = entity.Name,
                Logo = entity.Logo,
                BusinessUnitId = entity.BusinessUnitId,
                Id = entity.Id
            };
            return dto;
        }

        public static BusinessUnitLogo CopyTo(this BusinessUnitLogoDto dto, BusinessUnitLogo entity)
        {
            entity.Name = dto.Name;
            entity.Logo = dto.Logo;
            entity.BusinessUnitId = dto.BusinessUnitId;
            return entity;
        }

        public static BusinessUnitLogo CopyTo(this BusinessUnitLogoDto dto)
        {
            var entity = new BusinessUnitLogo();
            return dto.CopyTo(entity);
        }

        #endregion

        #region EmployeeBusinessUnitDto Mappers

        public static EmployeeBusinessUnitDto AsDto(this EmployeeBusinessUnit entity)
        {
            var dto = new EmployeeBusinessUnitDto
            {
                Default = entity.Default,
                BusinessUnitId = entity.BusinessUnitId,
                EmployeeId = entity.EmployeeId,
                Id = entity.Id
            };
            return dto;
        }

        public static EmployeeBusinessUnit CopyTo(this EmployeeBusinessUnitDto dto, EmployeeBusinessUnit entity)
        {
            entity.Default = dto.Default;
            entity.BusinessUnitId = dto.BusinessUnitId;
            entity.EmployeeId = dto.EmployeeId;
            return entity;
        }

        public static EmployeeBusinessUnit CopyTo(this EmployeeBusinessUnitDto dto)
        {
            var entity = new EmployeeBusinessUnit();
            return dto.CopyTo(entity);
        }

        #endregion

        #region BusinessUnitActivityViewDto Mappers

        public static BusinessUnitActivityViewDto AsDto(this BusinessUnitActivityView entity)
        {
            var dto = new BusinessUnitActivityViewDto
            {
                EmployerId = entity.EmployerId,
                BuId = entity.BuId,
                UserName = entity.UserName,
                BuName = entity.BuName,
                ActionName = entity.ActionName,
                ActionedOn = entity.ActionedOn,
                ActionId = entity.ActionId,
                JobId = entity.JobId,
                JobTitle = entity.JobTitle,
                Id = entity.Id
            };
            return dto;
        }

        public static BusinessUnitActivityView CopyTo(this BusinessUnitActivityViewDto dto, BusinessUnitActivityView entity)
        {
            entity.EmployerId = dto.EmployerId;
            entity.BuId = dto.BuId;
            entity.UserName = dto.UserName;
            entity.BuName = dto.BuName;
            entity.ActionName = dto.ActionName;
            entity.ActionedOn = dto.ActionedOn;
            entity.ActionId = dto.ActionId;
            entity.JobId = dto.JobId;
            entity.JobTitle = dto.JobTitle;
            return entity;
        }

        public static BusinessUnitActivityView CopyTo(this BusinessUnitActivityViewDto dto)
        {
            var entity = new BusinessUnitActivityView();
            return dto.CopyTo(entity);
        }

        #endregion

        #region EmployeeJobFamilyDto Mappers

        public static EmployeeJobFamilyDto AsDto(this EmployeeJobFamily entity)
        {
            var dto = new EmployeeJobFamilyDto
            {
                JobFamilyId = entity.JobFamilyId,
                EmployeeId = entity.EmployeeId,
                Id = entity.Id
            };
            return dto;
        }

        public static EmployeeJobFamily CopyTo(this EmployeeJobFamilyDto dto, EmployeeJobFamily entity)
        {
            entity.JobFamilyId = dto.JobFamilyId;
            entity.EmployeeId = dto.EmployeeId;
            return entity;
        }

        public static EmployeeJobFamily CopyTo(this EmployeeJobFamilyDto dto)
        {
            var entity = new EmployeeJobFamily();
            return dto.CopyTo(entity);
        }

        #endregion

        #region PostingDto Mappers

        public static PostingDto AsDto(this Posting entity)
        {
            var dto = new PostingDto
            {
                CreatedOn = entity.CreatedOn,
                UpdatedOn = entity.UpdatedOn,
                LensPostingId = entity.LensPostingId,
                JobTitle = entity.JobTitle,
                PostingXml = entity.PostingXml,
                EmployerName = entity.EmployerName,
                Url = entity.Url,
                ExternalId = entity.ExternalId,
                StatusId = entity.StatusId,
                OriginId = entity.OriginId,
                Html = entity.Html,
                ViewedCount = entity.ViewedCount,
                JobLocation = entity.JobLocation,
                JobId = entity.JobId,
                Id = entity.Id
            };
            return dto;
        }

        public static Posting CopyTo(this PostingDto dto, Posting entity)
        {
            entity.LensPostingId = dto.LensPostingId;
            entity.JobTitle = dto.JobTitle;
            entity.PostingXml = dto.PostingXml;
            entity.EmployerName = dto.EmployerName;
            entity.Url = dto.Url;
            entity.ExternalId = dto.ExternalId;
            entity.StatusId = dto.StatusId;
            entity.OriginId = dto.OriginId;
            entity.Html = dto.Html;
            entity.ViewedCount = dto.ViewedCount;
            entity.JobLocation = dto.JobLocation;
            entity.JobId = dto.JobId;
            return entity;
        }

        public static Posting CopyTo(this PostingDto dto)
        {
            var entity = new Posting();
            return dto.CopyTo(entity);
        }

        #endregion

        #region BusinessUnitPlacementsViewDto Mappers

        public static BusinessUnitPlacementsViewDto AsDto(this BusinessUnitPlacementsView entity)
        {
            var dto = new BusinessUnitPlacementsViewDto
            {
                BuId = entity.BuId,
                JobSeekerId = entity.JobSeekerId,
                BuName = entity.BuName,
                JobTitle = entity.JobTitle,
                ApplicationStatus = entity.ApplicationStatus,
                FirstName = entity.FirstName,
                LastName = entity.LastName,
                Id = entity.Id
            };
            return dto;
        }

        public static BusinessUnitPlacementsView CopyTo(this BusinessUnitPlacementsViewDto dto, BusinessUnitPlacementsView entity)
        {
            entity.BuId = dto.BuId;
            entity.JobSeekerId = dto.JobSeekerId;
            entity.BuName = dto.BuName;
            entity.JobTitle = dto.JobTitle;
            entity.ApplicationStatus = dto.ApplicationStatus;
            entity.FirstName = dto.FirstName;
            entity.LastName = dto.LastName;
            return entity;
        }

        public static BusinessUnitPlacementsView CopyTo(this BusinessUnitPlacementsViewDto dto)
        {
            var entity = new BusinessUnitPlacementsView();
            return dto.CopyTo(entity);
        }

        #endregion

        #region NoteReminderRecipientDto Mappers

        public static NoteReminderRecipientDto AsDto(this NoteReminderRecipient entity)
        {
            var dto = new NoteReminderRecipientDto
            {
                RecipientEntityType = entity.RecipientEntityType,
                RecipientEntityId = entity.RecipientEntityId,
                NoteReminderId = entity.NoteReminderId,
                Id = entity.Id
            };
            return dto;
        }

        public static NoteReminderRecipient CopyTo(this NoteReminderRecipientDto dto, NoteReminderRecipient entity)
        {
            entity.RecipientEntityType = dto.RecipientEntityType;
            entity.RecipientEntityId = dto.RecipientEntityId;
            entity.NoteReminderId = dto.NoteReminderId;
            return entity;
        }

        public static NoteReminderRecipient CopyTo(this NoteReminderRecipientDto dto)
        {
            var entity = new NoteReminderRecipient();
            return dto.CopyTo(entity);
        }

        #endregion

        #region ViewedPostingDto Mappers

        public static ViewedPostingDto AsDto(this ViewedPosting entity)
        {
            var dto = new ViewedPostingDto
            {
                ViewedOn = entity.ViewedOn,
                LensPostingId = entity.LensPostingId,
                UserId = entity.UserId,
                Id = entity.Id
            };
            return dto;
        }

        public static ViewedPosting CopyTo(this ViewedPostingDto dto, ViewedPosting entity)
        {
            entity.ViewedOn = dto.ViewedOn;
            entity.LensPostingId = dto.LensPostingId;
            entity.UserId = dto.UserId;
            return entity;
        }

        public static ViewedPosting CopyTo(this ViewedPostingDto dto)
        {
            var entity = new ViewedPosting();
            return dto.CopyTo(entity);
        }

        #endregion

        #region AssistUserActivityViewDto Mappers

        public static AssistUserActivityViewDto AsDto(this AssistUserActivityView entity)
        {
            var dto = new AssistUserActivityViewDto
            {
                UserId = entity.UserId,
                ActionName = entity.ActionName,
                ActionedOn = entity.ActionedOn,
                Id = entity.Id
            };
            return dto;
        }

        public static AssistUserActivityView CopyTo(this AssistUserActivityViewDto dto, AssistUserActivityView entity)
        {
            entity.UserId = dto.UserId;
            entity.ActionName = dto.ActionName;
            entity.ActionedOn = dto.ActionedOn;
            return entity;
        }

        public static AssistUserActivityView CopyTo(this AssistUserActivityViewDto dto)
        {
            var entity = new AssistUserActivityView();
            return dto.CopyTo(entity);
        }

        #endregion

        #region HiringManagerActivityViewDto Mappers

        public static HiringManagerActivityViewDto AsDto(this HiringManagerActivityView entity)
        {
            var dto = new HiringManagerActivityViewDto
            {
                UserId = entity.UserId,
                ActionName = entity.ActionName,
                ActionedOn = entity.ActionedOn,
                EmployeeFirstName = entity.EmployeeFirstName,
                EmployeeLastName = entity.EmployeeLastName,
                EmployeeBuId = entity.EmployeeBuId,
                JobId = entity.JobId,
                JobTitle = entity.JobTitle,
                EntityIdAdditional01 = entity.EntityIdAdditional01,
                AdditionalName = entity.AdditionalName,
                Id = entity.Id
            };
            return dto;
        }

        public static HiringManagerActivityView CopyTo(this HiringManagerActivityViewDto dto, HiringManagerActivityView entity)
        {
            entity.UserId = dto.UserId;
            entity.ActionName = dto.ActionName;
            entity.ActionedOn = dto.ActionedOn;
            entity.EmployeeFirstName = dto.EmployeeFirstName;
            entity.EmployeeLastName = dto.EmployeeLastName;
            entity.EmployeeBuId = dto.EmployeeBuId;
            entity.JobId = dto.JobId;
            entity.JobTitle = dto.JobTitle;
            entity.EntityIdAdditional01 = dto.EntityIdAdditional01;
            entity.AdditionalName = dto.AdditionalName;
            return entity;
        }

        public static HiringManagerActivityView CopyTo(this HiringManagerActivityViewDto dto)
        {
            var entity = new HiringManagerActivityView();
            return dto.CopyTo(entity);
        }

        #endregion

        #region AssistUserEmployersAssistedViewDto Mappers

        public static AssistUserEmployersAssistedViewDto AsDto(this AssistUserEmployersAssistedView entity)
        {
            var dto = new AssistUserEmployersAssistedViewDto
            {
                UserId = entity.UserId,
                ActionName = entity.ActionName,
                ActionedOn = entity.ActionedOn,
                Name = entity.Name,
                BusinessUnitName = entity.BusinessUnitName,
                JobTitle = entity.JobTitle,
                SavedSearchName = entity.SavedSearchName,
                AdditionalDetails = entity.AdditionalDetails,
                Id = entity.Id
            };
            return dto;
        }

        public static AssistUserEmployersAssistedView CopyTo(this AssistUserEmployersAssistedViewDto dto, AssistUserEmployersAssistedView entity)
        {
            entity.UserId = dto.UserId;
            entity.ActionName = dto.ActionName;
            entity.ActionedOn = dto.ActionedOn;
            entity.Name = dto.Name;
            entity.BusinessUnitName = dto.BusinessUnitName;
            entity.JobTitle = dto.JobTitle;
            entity.SavedSearchName = dto.SavedSearchName;
            entity.AdditionalDetails = dto.AdditionalDetails;
            return entity;
        }

        public static AssistUserEmployersAssistedView CopyTo(this AssistUserEmployersAssistedViewDto dto)
        {
            var entity = new AssistUserEmployersAssistedView();
            return dto.CopyTo(entity);
        }

        #endregion

        #region BusinessUnitUserDto Mappers

        public static BusinessUnitUserDto AsDto(this BusinessUnitUser entity)
        {
            var dto = new BusinessUnitUserDto
            {
                BusinessUnitId = entity.BusinessUnitId,
                UserId = entity.UserId,
                Enabled = entity.Enabled,
                Id = entity.Id
            };
            return dto;
        }

        public static BusinessUnitUser CopyTo(this BusinessUnitUserDto dto, BusinessUnitUser entity)
        {
            entity.BusinessUnitId = dto.BusinessUnitId;
            entity.UserId = dto.UserId;
            entity.Enabled = dto.Enabled;
            return entity;
        }

        public static BusinessUnitUser CopyTo(this BusinessUnitUserDto dto)
        {
            var entity = new BusinessUnitUser();
            return dto.CopyTo(entity);
        }

        #endregion

        #region ResumeDocumentDto Mappers

        public static ResumeDocumentDto AsDto(this ResumeDocument entity)
        {
            var dto = new ResumeDocumentDto
            {
                FileName = entity.FileName,
                ContentType = entity.ContentType,
                DocumentBytes = entity.DocumentBytes,
                Html = entity.Html,
                ResumeId = entity.ResumeId,
                Id = entity.Id
            };
            return dto;
        }

        public static ResumeDocument CopyTo(this ResumeDocumentDto dto, ResumeDocument entity)
        {
            entity.FileName = dto.FileName;
            entity.ContentType = dto.ContentType;
            entity.DocumentBytes = dto.DocumentBytes;
            entity.Html = dto.Html;
            entity.ResumeId = dto.ResumeId;
            return entity;
        }

        public static ResumeDocument CopyTo(this ResumeDocumentDto dto)
        {
            var entity = new ResumeDocument();
            return dto.CopyTo(entity);
        }

        #endregion

        #region AssistUserJobSeekersAssistedViewDto Mappers

        public static AssistUserJobSeekersAssistedViewDto AsDto(this AssistUserJobSeekersAssistedView entity)
        {
            var dto = new AssistUserJobSeekersAssistedViewDto
            {
                UserId = entity.UserId,
                ActionName = entity.ActionName,
                ActionedOn = entity.ActionedOn,
                Name = entity.Name,
                Id = entity.Id
            };
            return dto;
        }

        public static AssistUserJobSeekersAssistedView CopyTo(this AssistUserJobSeekersAssistedViewDto dto, AssistUserJobSeekersAssistedView entity)
        {
            entity.UserId = dto.UserId;
            entity.ActionName = dto.ActionName;
            entity.ActionedOn = dto.ActionedOn;
            entity.Name = dto.Name;
            return entity;
        }

        public static AssistUserJobSeekersAssistedView CopyTo(this AssistUserJobSeekersAssistedViewDto dto)
        {
            var entity = new AssistUserJobSeekersAssistedView();
            return dto.CopyTo(entity);
        }

        #endregion

        #region UserAlertDto Mappers

        public static UserAlertDto AsDto(this UserAlert entity)
        {
            var dto = new UserAlertDto
            {
                CreatedOn = entity.CreatedOn,
                UpdatedOn = entity.UpdatedOn,
                UserId = entity.UserId,
                NewReferralRequests = entity.NewReferralRequests,
                EmployerAccountRequests = entity.EmployerAccountRequests,
                NewJobPostings = entity.NewJobPostings,
                Frequency = entity.Frequency,
                Id = entity.Id
            };
            return dto;
        }

        public static UserAlert CopyTo(this UserAlertDto dto, UserAlert entity)
        {
            entity.UserId = dto.UserId;
            entity.NewReferralRequests = dto.NewReferralRequests;
            entity.EmployerAccountRequests = dto.EmployerAccountRequests;
            entity.NewJobPostings = dto.NewJobPostings;
            entity.Frequency = dto.Frequency;
            return entity;
        }

        public static UserAlert CopyTo(this UserAlertDto dto)
        {
            var entity = new UserAlert();
            return dto.CopyTo(entity);
        }

        #endregion

        #region ExcludedLensPostingDto Mappers

        public static ExcludedLensPostingDto AsDto(this ExcludedLensPosting entity)
        {
            var dto = new ExcludedLensPostingDto
            {
                CreatedOn = entity.CreatedOn,
                UpdatedOn = entity.UpdatedOn,
                LensPostingId = entity.LensPostingId,
                PersonId = entity.PersonId,
                Id = entity.Id
            };
            return dto;
        }

        public static ExcludedLensPosting CopyTo(this ExcludedLensPostingDto dto, ExcludedLensPosting entity)
        {
            entity.LensPostingId = dto.LensPostingId;
            entity.PersonId = dto.PersonId;
            return entity;
        }

        public static ExcludedLensPosting CopyTo(this ExcludedLensPostingDto dto)
        {
            var entity = new ExcludedLensPosting();
            return dto.CopyTo(entity);
        }

        #endregion

        #region ActiveUserAlertViewDto Mappers

        public static ActiveUserAlertViewDto AsDto(this ActiveUserAlertView entity)
        {
            var dto = new ActiveUserAlertViewDto
            {
                UserId = entity.UserId,
                NewReferralRequests = entity.NewReferralRequests,
                EmployerAccountRequests = entity.EmployerAccountRequests,
                NewJobPostings = entity.NewJobPostings,
                Frequency = entity.Frequency,
                FirstName = entity.FirstName,
                LastName = entity.LastName,
                EmailAddress = entity.EmailAddress,
                Id = entity.Id
            };
            return dto;
        }

        public static ActiveUserAlertView CopyTo(this ActiveUserAlertViewDto dto, ActiveUserAlertView entity)
        {
            entity.UserId = dto.UserId;
            entity.NewReferralRequests = dto.NewReferralRequests;
            entity.EmployerAccountRequests = dto.EmployerAccountRequests;
            entity.NewJobPostings = dto.NewJobPostings;
            entity.Frequency = dto.Frequency;
            entity.FirstName = dto.FirstName;
            entity.LastName = dto.LastName;
            entity.EmailAddress = dto.EmailAddress;
            return entity;
        }

        public static ActiveUserAlertView CopyTo(this ActiveUserAlertViewDto dto)
        {
            var entity = new ActiveUserAlertView();
            return dto.CopyTo(entity);
        }

        #endregion

        #region CandidateIssueViewDto Mappers

        public static CandidateIssueViewDto AsDto(this CandidateIssueView entity)
        {
            var dto = new CandidateIssueViewDto
            {
                FirstName = entity.FirstName,
                LastName = entity.LastName,
                DateOfBirth = entity.DateOfBirth,
                SocialSecurityNumber = entity.SocialSecurityNumber,
                EmailAddress = entity.EmailAddress,
                TownCity = entity.TownCity,
                StateId = entity.StateId,
                FollowUpRequested = entity.FollowUpRequested,
                JobOfferRejectionTriggered = entity.JobOfferRejectionTriggered,
                NotReportingToInterviewTriggered = entity.NotReportingToInterviewTriggered,
                NotClickingOnLeadsTriggered = entity.NotClickingOnLeadsTriggered,
                NotRespondingToEmployerInvitesTriggered = entity.NotRespondingToEmployerInvitesTriggered,
                ShowingLowQualityMatchesTriggered = entity.ShowingLowQualityMatchesTriggered,
                PostingLowQualityResumeTriggered = entity.PostingLowQualityResumeTriggered,
                PostHireFollowUpTriggered = entity.PostHireFollowUpTriggered,
                NoLoginTriggered = entity.NoLoginTriggered,
                LastLoggedInOn = entity.LastLoggedInOn,
                NotSearchingJobsTriggered = entity.NotSearchingJobsTriggered,
                AssignedToId = entity.AssignedToId,
                IsVeteran = entity.IsVeteran,
                ExternalId = entity.ExternalId,
                InappropriateEmailAddress = entity.InappropriateEmailAddress,
                Blocked = entity.Blocked,
                GivingPositiveFeedback = entity.GivingPositiveFeedback,
                GivingNegativeFeedback = entity.GivingNegativeFeedback,
                MiddleInitial = entity.MiddleInitial,
                MigrantSeasonalFarmWorkerTriggered = entity.MigrantSeasonalFarmWorkerTriggered,
                MigrantSeasonalFarmWorkerVerified = entity.MigrantSeasonalFarmWorkerVerified,
                Enabled = entity.Enabled,
                UserName = entity.UserName,
                BlockedReason = entity.BlockedReason,
                HasExpiredAlienCertificationRegistration = entity.HasExpiredAlienCertificationRegistration,
                Id = entity.Id
            };
            return dto;
        }

        public static CandidateIssueView CopyTo(this CandidateIssueViewDto dto, CandidateIssueView entity)
        {
            entity.FirstName = dto.FirstName;
            entity.LastName = dto.LastName;
            entity.DateOfBirth = dto.DateOfBirth;
            entity.SocialSecurityNumber = dto.SocialSecurityNumber;
            entity.EmailAddress = dto.EmailAddress;
            entity.TownCity = dto.TownCity;
            entity.StateId = dto.StateId;
            entity.FollowUpRequested = dto.FollowUpRequested;
            entity.JobOfferRejectionTriggered = dto.JobOfferRejectionTriggered;
            entity.NotReportingToInterviewTriggered = dto.NotReportingToInterviewTriggered;
            entity.NotClickingOnLeadsTriggered = dto.NotClickingOnLeadsTriggered;
            entity.NotRespondingToEmployerInvitesTriggered = dto.NotRespondingToEmployerInvitesTriggered;
            entity.ShowingLowQualityMatchesTriggered = dto.ShowingLowQualityMatchesTriggered;
            entity.PostingLowQualityResumeTriggered = dto.PostingLowQualityResumeTriggered;
            entity.PostHireFollowUpTriggered = dto.PostHireFollowUpTriggered;
            entity.NoLoginTriggered = dto.NoLoginTriggered;
            entity.LastLoggedInOn = dto.LastLoggedInOn;
            entity.NotSearchingJobsTriggered = dto.NotSearchingJobsTriggered;
            entity.AssignedToId = dto.AssignedToId;
            entity.IsVeteran = dto.IsVeteran;
            entity.ExternalId = dto.ExternalId;
            entity.InappropriateEmailAddress = dto.InappropriateEmailAddress;
            entity.Blocked = dto.Blocked;
            entity.GivingPositiveFeedback = dto.GivingPositiveFeedback;
            entity.GivingNegativeFeedback = dto.GivingNegativeFeedback;
            entity.MiddleInitial = dto.MiddleInitial;
            entity.MigrantSeasonalFarmWorkerTriggered = dto.MigrantSeasonalFarmWorkerTriggered;
            entity.MigrantSeasonalFarmWorkerVerified = dto.MigrantSeasonalFarmWorkerVerified;
            entity.Enabled = dto.Enabled;
            entity.UserName = dto.UserName;
            entity.BlockedReason = dto.BlockedReason;
            entity.HasExpiredAlienCertificationRegistration = dto.HasExpiredAlienCertificationRegistration;
            return entity;
        }

        public static CandidateIssueView CopyTo(this CandidateIssueViewDto dto)
        {
            var entity = new CandidateIssueView();
            return dto.CopyTo(entity);
        }

        #endregion

        #region CandidateViewDto Mappers

        public static CandidateViewDto AsDto(this CandidateView entity)
        {
            var dto = new CandidateViewDto
            {
                CreatedOn = entity.CreatedOn,
                FirstName = entity.FirstName,
                LastName = entity.LastName,
                DateOfBirth = entity.DateOfBirth,
                SocialSecurityNumber = entity.SocialSecurityNumber,
                EmailAddress = entity.EmailAddress,
                AddressLine1 = entity.AddressLine1,
                TownCity = entity.TownCity,
                StateId = entity.StateId,
                CountryId = entity.CountryId,
                PostcodeZip = entity.PostcodeZip,
                HomePhone = entity.HomePhone,
                DefaultResumeId = entity.DefaultResumeId,
                IsSearchable = entity.IsSearchable,
                ClientId = entity.ClientId,
                UserId = entity.UserId,
                Enabled = entity.Enabled,
                LastLoggedInOn = entity.LastLoggedInOn,
                ResumeWordCount = entity.ResumeWordCount,
                ResumeSkillCount = entity.ResumeSkillCount,
                VeteranPriorityServiceAlertsSubscription = entity.VeteranPriorityServiceAlertsSubscription,
                Id = entity.Id
            };
            return dto;
        }

        public static CandidateView CopyTo(this CandidateViewDto dto, CandidateView entity)
        {
            entity.FirstName = dto.FirstName;
            entity.LastName = dto.LastName;
            entity.DateOfBirth = dto.DateOfBirth;
            entity.SocialSecurityNumber = dto.SocialSecurityNumber;
            entity.EmailAddress = dto.EmailAddress;
            entity.AddressLine1 = dto.AddressLine1;
            entity.TownCity = dto.TownCity;
            entity.StateId = dto.StateId;
            entity.CountryId = dto.CountryId;
            entity.PostcodeZip = dto.PostcodeZip;
            entity.HomePhone = dto.HomePhone;
            entity.DefaultResumeId = dto.DefaultResumeId;
            entity.IsSearchable = dto.IsSearchable;
            entity.ClientId = dto.ClientId;
            entity.UserId = dto.UserId;
            entity.Enabled = dto.Enabled;
            entity.LastLoggedInOn = dto.LastLoggedInOn;
            entity.ResumeWordCount = dto.ResumeWordCount;
            entity.ResumeSkillCount = dto.ResumeSkillCount;
            entity.VeteranPriorityServiceAlertsSubscription = dto.VeteranPriorityServiceAlertsSubscription;
            return entity;
        }

        public static CandidateView CopyTo(this CandidateViewDto dto)
        {
            var entity = new CandidateView();
            return dto.CopyTo(entity);
        }

        #endregion

        #region RecentlyPlacedDto Mappers

        public static RecentlyPlacedDto AsDto(this RecentlyPlaced entity)
        {
            var dto = new RecentlyPlacedDto
            {
                PlacementDate = entity.PlacementDate,
                JobId = entity.JobId,
                PersonId = entity.PersonId,
                Id = entity.Id
            };
            return dto;
        }

        public static RecentlyPlaced CopyTo(this RecentlyPlacedDto dto, RecentlyPlaced entity)
        {
            entity.PlacementDate = dto.PlacementDate;
            entity.JobId = dto.JobId;
            entity.PersonId = dto.PersonId;
            return entity;
        }

        public static RecentlyPlaced CopyTo(this RecentlyPlacedDto dto)
        {
            var entity = new RecentlyPlaced();
            return dto.CopyTo(entity);
        }

        #endregion

        #region RecentlyPlacedMatchDto Mappers

        public static RecentlyPlacedMatchDto AsDto(this RecentlyPlacedMatch entity)
        {
            var dto = new RecentlyPlacedMatchDto
            {
                Score = entity.Score,
                PersonId = entity.PersonId,
                RecentlyPlacedId = entity.RecentlyPlacedId,
                Id = entity.Id
            };
            return dto;
        }

        public static RecentlyPlacedMatch CopyTo(this RecentlyPlacedMatchDto dto, RecentlyPlacedMatch entity)
        {
            entity.Score = dto.Score;
            entity.PersonId = dto.PersonId;
            entity.RecentlyPlacedId = dto.RecentlyPlacedId;
            return entity;
        }

        public static RecentlyPlacedMatch CopyTo(this RecentlyPlacedMatchDto dto)
        {
            var entity = new RecentlyPlacedMatch();
            return dto.CopyTo(entity);
        }

        #endregion

        #region PersonPostingMatchDto Mappers

        public static PersonPostingMatchDto AsDto(this PersonPostingMatch entity)
        {
            var dto = new PersonPostingMatchDto
            {
                CreatedOn = entity.CreatedOn,
                Score = entity.Score,
                PostingId = entity.PostingId,
                PersonId = entity.PersonId,
                Id = entity.Id
            };
            return dto;
        }

        public static PersonPostingMatch CopyTo(this PersonPostingMatchDto dto, PersonPostingMatch entity)
        {
            entity.Score = dto.Score;
            entity.PostingId = dto.PostingId;
            entity.PersonId = dto.PersonId;
            return entity;
        }

        public static PersonPostingMatch CopyTo(this PersonPostingMatchDto dto)
        {
            var entity = new PersonPostingMatch();
            return dto.CopyTo(entity);
        }

        #endregion

        #region IssuesDto Mappers

        public static IssuesDto AsDto(this Issues entity)
        {
            var dto = new IssuesDto
            {
                PersonId = entity.PersonId,
                NoLoginTriggered = entity.NoLoginTriggered,
                NoLoginResolvedDate = entity.NoLoginResolvedDate,
                JobOfferRejectionCount = entity.JobOfferRejectionCount,
                JobOfferRejectionResolvedDate = entity.JobOfferRejectionResolvedDate,
                JobOfferRejectionTriggered = entity.JobOfferRejectionTriggered,
                NotReportingToInterviewCount = entity.NotReportingToInterviewCount,
                NotReportingToInterviewResolvedDate = entity.NotReportingToInterviewResolvedDate,
                NotReportingToInterviewTriggered = entity.NotReportingToInterviewTriggered,
                NotClickingOnLeadsCount = entity.NotClickingOnLeadsCount,
                NotClickingOnLeadsResolvedDate = entity.NotClickingOnLeadsResolvedDate,
                NotClickingOnLeadsTriggered = entity.NotClickingOnLeadsTriggered,
                NotRespondingToEmployerInvitesCount = entity.NotRespondingToEmployerInvitesCount,
                NotRespondingToEmployerInvitesResolvedDate = entity.NotRespondingToEmployerInvitesResolvedDate,
                NotRespondingToEmployerInvitesTriggered = entity.NotRespondingToEmployerInvitesTriggered,
                ShowingLowQualityMatchesCount = entity.ShowingLowQualityMatchesCount,
                ShowingLowQualityMatchesResolvedDate = entity.ShowingLowQualityMatchesResolvedDate,
                ShowingLowQualityMatchesTriggered = entity.ShowingLowQualityMatchesTriggered,
                PostingLowQualityResumeCount = entity.PostingLowQualityResumeCount,
                PostingLowQualityResumeResolvedDate = entity.PostingLowQualityResumeResolvedDate,
                PostingLowQualityResumeTriggered = entity.PostingLowQualityResumeTriggered,
                FollowUpRequested = entity.FollowUpRequested,
                PostHireFollowUpTriggered = entity.PostHireFollowUpTriggered,
                PostHireFollowUpResolvedDate = entity.PostHireFollowUpResolvedDate,
                NotSearchingJobsTriggered = entity.NotSearchingJobsTriggered,
                NotSearchingJobsResolvedDate = entity.NotSearchingJobsResolvedDate,
                LastLoggedInDateForAlert = entity.LastLoggedInDateForAlert,
                InappropriateEmailAddress = entity.InappropriateEmailAddress,
                GivingPositiveFeedback = entity.GivingPositiveFeedback,
                GivingNegativeFeedback = entity.GivingNegativeFeedback,
                MigrantSeasonalFarmWorkerTriggered = entity.MigrantSeasonalFarmWorkerTriggered,
                HasExpiredAlienCertificationRegistration = entity.HasExpiredAlienCertificationRegistration,
                Id = entity.Id
            };
            return dto;
        }

        public static Issues CopyTo(this IssuesDto dto, Issues entity)
        {
            entity.PersonId = dto.PersonId;
            entity.NoLoginTriggered = dto.NoLoginTriggered;
            entity.NoLoginResolvedDate = dto.NoLoginResolvedDate;
            entity.JobOfferRejectionCount = dto.JobOfferRejectionCount;
            entity.JobOfferRejectionResolvedDate = dto.JobOfferRejectionResolvedDate;
            entity.JobOfferRejectionTriggered = dto.JobOfferRejectionTriggered;
            entity.NotReportingToInterviewCount = dto.NotReportingToInterviewCount;
            entity.NotReportingToInterviewResolvedDate = dto.NotReportingToInterviewResolvedDate;
            entity.NotReportingToInterviewTriggered = dto.NotReportingToInterviewTriggered;
            entity.NotClickingOnLeadsCount = dto.NotClickingOnLeadsCount;
            entity.NotClickingOnLeadsResolvedDate = dto.NotClickingOnLeadsResolvedDate;
            entity.NotClickingOnLeadsTriggered = dto.NotClickingOnLeadsTriggered;
            entity.NotRespondingToEmployerInvitesCount = dto.NotRespondingToEmployerInvitesCount;
            entity.NotRespondingToEmployerInvitesResolvedDate = dto.NotRespondingToEmployerInvitesResolvedDate;
            entity.NotRespondingToEmployerInvitesTriggered = dto.NotRespondingToEmployerInvitesTriggered;
            entity.ShowingLowQualityMatchesCount = dto.ShowingLowQualityMatchesCount;
            entity.ShowingLowQualityMatchesResolvedDate = dto.ShowingLowQualityMatchesResolvedDate;
            entity.ShowingLowQualityMatchesTriggered = dto.ShowingLowQualityMatchesTriggered;
            entity.PostingLowQualityResumeCount = dto.PostingLowQualityResumeCount;
            entity.PostingLowQualityResumeResolvedDate = dto.PostingLowQualityResumeResolvedDate;
            entity.PostingLowQualityResumeTriggered = dto.PostingLowQualityResumeTriggered;
            entity.FollowUpRequested = dto.FollowUpRequested;
            entity.PostHireFollowUpTriggered = dto.PostHireFollowUpTriggered;
            entity.PostHireFollowUpResolvedDate = dto.PostHireFollowUpResolvedDate;
            entity.NotSearchingJobsTriggered = dto.NotSearchingJobsTriggered;
            entity.NotSearchingJobsResolvedDate = dto.NotSearchingJobsResolvedDate;
            entity.LastLoggedInDateForAlert = dto.LastLoggedInDateForAlert;
            entity.InappropriateEmailAddress = dto.InappropriateEmailAddress;
            entity.GivingPositiveFeedback = dto.GivingPositiveFeedback;
            entity.GivingNegativeFeedback = dto.GivingNegativeFeedback;
            entity.MigrantSeasonalFarmWorkerTriggered = dto.MigrantSeasonalFarmWorkerTriggered;
            entity.HasExpiredAlienCertificationRegistration = dto.HasExpiredAlienCertificationRegistration;
            return entity;
        }

        public static Issues CopyTo(this IssuesDto dto)
        {
            var entity = new Issues();
            return dto.CopyTo(entity);
        }

        #endregion

        #region RecentlyPlacedMatchViewDto Mappers

        public static RecentlyPlacedMatchViewDto AsDto(this RecentlyPlacedMatchView entity)
        {
            var dto = new RecentlyPlacedMatchViewDto
            {
                Score = entity.Score,
                HasMatchesToOpenPositions = entity.HasMatchesToOpenPositions,
                MatchedPersonId = entity.MatchedPersonId,
                MatchedCandidateName = entity.MatchedCandidateName,
                PlacedPersonId = entity.PlacedPersonId,
                PlacedCandidateName = entity.PlacedCandidateName,
                BusinessUnitId = entity.BusinessUnitId,
                BusinessUnitName = entity.BusinessUnitName,
                EmployerId = entity.EmployerId,
                EmployerName = entity.EmployerName,
                PlacementDate = entity.PlacementDate,
                JobId = entity.JobId,
                JobTitle = entity.JobTitle,
                RecentlyPlacedId = entity.RecentlyPlacedId,
                Id = entity.Id
            };
            return dto;
        }

        public static RecentlyPlacedMatchView CopyTo(this RecentlyPlacedMatchViewDto dto, RecentlyPlacedMatchView entity)
        {
            entity.Score = dto.Score;
            entity.HasMatchesToOpenPositions = dto.HasMatchesToOpenPositions;
            entity.MatchedPersonId = dto.MatchedPersonId;
            entity.MatchedCandidateName = dto.MatchedCandidateName;
            entity.PlacedPersonId = dto.PlacedPersonId;
            entity.PlacedCandidateName = dto.PlacedCandidateName;
            entity.BusinessUnitId = dto.BusinessUnitId;
            entity.BusinessUnitName = dto.BusinessUnitName;
            entity.EmployerId = dto.EmployerId;
            entity.EmployerName = dto.EmployerName;
            entity.PlacementDate = dto.PlacementDate;
            entity.JobId = dto.JobId;
            entity.JobTitle = dto.JobTitle;
            entity.RecentlyPlacedId = dto.RecentlyPlacedId;
            return entity;
        }

        public static RecentlyPlacedMatchView CopyTo(this RecentlyPlacedMatchViewDto dto)
        {
            var entity = new RecentlyPlacedMatchView();
            return dto.CopyTo(entity);
        }

        #endregion

        #region RecentlyPlacedMatchesToIgnoreDto Mappers

        public static RecentlyPlacedMatchesToIgnoreDto AsDto(this RecentlyPlacedMatchesToIgnore entity)
        {
            var dto = new RecentlyPlacedMatchesToIgnoreDto
            {
                IgnoredOn = entity.IgnoredOn,
                UserId = entity.UserId,
                RecentlyPlacedId = entity.RecentlyPlacedId,
                PersonId = entity.PersonId,
                Id = entity.Id
            };
            return dto;
        }

        public static RecentlyPlacedMatchesToIgnore CopyTo(this RecentlyPlacedMatchesToIgnoreDto dto, RecentlyPlacedMatchesToIgnore entity)
        {
            entity.IgnoredOn = dto.IgnoredOn;
            entity.UserId = dto.UserId;
            entity.RecentlyPlacedId = dto.RecentlyPlacedId;
            entity.PersonId = dto.PersonId;
            return entity;
        }

        public static RecentlyPlacedMatchesToIgnore CopyTo(this RecentlyPlacedMatchesToIgnoreDto dto)
        {
            var entity = new RecentlyPlacedMatchesToIgnore();
            return dto.CopyTo(entity);
        }

        #endregion

        #region OpenPositionMatchesViewDto Mappers

        public static OpenPositionMatchesViewDto AsDto(this OpenPositionMatchesView entity)
        {
            var dto = new OpenPositionMatchesViewDto
            {
                JobId = entity.JobId,
                JobTitle = entity.JobTitle,
                JobScore = entity.JobScore,
                CandidateId = entity.CandidateId,
                FirstName = entity.FirstName,
                LastName = entity.LastName,
                BusinessUnitId = entity.BusinessUnitId,
                EmployerName = entity.EmployerName,
                OpenPositionMatchId = entity.OpenPositionMatchId,
                Id = entity.Id,
                PostingId = entity.PostingId,
                EmployerId = entity.EmployerId
            };
            return dto;
        }

        public static OpenPositionMatchesView CopyTo(this OpenPositionMatchesViewDto dto, OpenPositionMatchesView entity)
        {
            entity.JobId = dto.JobId;
            entity.JobTitle = dto.JobTitle;
            entity.JobScore = dto.JobScore;
            entity.CandidateId = dto.CandidateId;
            entity.FirstName = dto.FirstName;
            entity.LastName = dto.LastName;
            entity.BusinessUnitId = dto.BusinessUnitId;
            entity.EmployerName = dto.EmployerName;
            entity.OpenPositionMatchId = dto.OpenPositionMatchId;
            entity.PostingId = dto.PostingId;
            entity.EmployerId = dto.EmployerId;
            return entity;
        }

        public static OpenPositionMatchesView CopyTo(this OpenPositionMatchesViewDto dto)
        {
            var entity = new OpenPositionMatchesView();
            return dto.CopyTo(entity);
        }

        #endregion

        #region PersonPostingMatchToIgnoreDto Mappers

        public static PersonPostingMatchToIgnoreDto AsDto(this PersonPostingMatchToIgnore entity)
        {
            var dto = new PersonPostingMatchToIgnoreDto
            {
                UserId = entity.UserId,
                IgnoredOn = entity.IgnoredOn,
                PersonId = entity.PersonId,
                PostingId = entity.PostingId,
                Id = entity.Id
            };
            return dto;
        }

        public static PersonPostingMatchToIgnore CopyTo(this PersonPostingMatchToIgnoreDto dto, PersonPostingMatchToIgnore entity)
        {
            entity.UserId = dto.UserId;
            entity.IgnoredOn = dto.IgnoredOn;
            entity.PersonId = dto.PersonId;
            entity.PostingId = dto.PostingId;
            return entity;
        }

        public static PersonPostingMatchToIgnore CopyTo(this PersonPostingMatchToIgnoreDto dto)
        {
            var entity = new PersonPostingMatchToIgnore();
            return dto.CopyTo(entity);
        }

        #endregion

        #region JobSeekerActivityActionViewDto Mappers

        public static JobSeekerActivityActionViewDto AsDto(this JobSeekerActivityActionView entity)
        {
            var dto = new JobSeekerActivityActionViewDto
            {
                UserName = entity.UserName,
                ActionedOn = entity.ActionedOn,
                ActionType = entity.ActionType,
                JobTitle = entity.JobTitle,
                BusinessUnitName = entity.BusinessUnitName,
                JobSeekerId = entity.JobSeekerId,
                UserId = entity.UserId,
                EntityIdAdditional01 = entity.EntityIdAdditional01,
                AdditionalDetails = entity.AdditionalDetails,
                EntityIdAdditional02 = entity.EntityIdAdditional02,
                JobId = entity.JobId,
                Id = entity.Id
            };
            return dto;
        }

        public static JobSeekerActivityActionView CopyTo(this JobSeekerActivityActionViewDto dto, JobSeekerActivityActionView entity)
        {
            entity.UserName = dto.UserName;
            entity.ActionedOn = dto.ActionedOn;
            entity.ActionType = dto.ActionType;
            entity.JobTitle = dto.JobTitle;
            entity.BusinessUnitName = dto.BusinessUnitName;
            entity.JobSeekerId = dto.JobSeekerId;
            entity.UserId = dto.UserId;
            entity.EntityIdAdditional01 = dto.EntityIdAdditional01;
            entity.AdditionalDetails = dto.AdditionalDetails;
            entity.EntityIdAdditional02 = dto.EntityIdAdditional02;
            entity.JobId = dto.JobId;
            return entity;
        }

        public static JobSeekerActivityActionView CopyTo(this JobSeekerActivityActionViewDto dto)
        {
            var entity = new JobSeekerActivityActionView();
            return dto.CopyTo(entity);
        }

        #endregion

        #region InviteToApplyDto Mappers

        public static InviteToApplyDto AsDto(this InviteToApply entity)
        {
            var dto = new InviteToApplyDto
            {
                CreatedOn = entity.CreatedOn,
                UpdatedOn = entity.UpdatedOn,
                LensPostingId = entity.LensPostingId,
                Viewed = entity.Viewed,
                CreatedBy = entity.CreatedBy,
                JobId = entity.JobId,
                PersonId = entity.PersonId,
                Id = entity.Id
            };
            return dto;
        }

        public static InviteToApply CopyTo(this InviteToApplyDto dto, InviteToApply entity)
        {
            entity.LensPostingId = dto.LensPostingId;
            entity.Viewed = dto.Viewed;
            entity.CreatedBy = dto.CreatedBy;
            entity.JobId = dto.JobId;
            entity.PersonId = dto.PersonId;
            return entity;
        }

        public static InviteToApply CopyTo(this InviteToApplyDto dto)
        {
            var entity = new InviteToApply();
            return dto.CopyTo(entity);
        }

        #endregion

        #region InviteToApplyBasicViewDto Mappers

        public static InviteToApplyBasicViewDto AsDto(this InviteToApplyBasicView entity)
        {
            var dto = new InviteToApplyBasicViewDto
            {
                CreatedOn = entity.CreatedOn,
                JobId = entity.JobId,
                FirstName = entity.FirstName,
                LastName = entity.LastName,
                PersonId = entity.PersonId,
                Town = entity.Town,
                StateId = entity.StateId,
                Branding = entity.Branding,
                IsContactInfoVisible = entity.IsContactInfoVisible,
                YearsExperience = entity.YearsExperience,
                NcrcLevelId = entity.NcrcLevelId,
                CandidateIsVeteran = entity.CandidateIsVeteran,
                InvitationQueued = entity.InvitationQueued,
                Id = entity.Id
            };
            return dto;
        }

        public static InviteToApplyBasicView CopyTo(this InviteToApplyBasicViewDto dto, InviteToApplyBasicView entity)
        {
            entity.JobId = dto.JobId;
            entity.FirstName = dto.FirstName;
            entity.LastName = dto.LastName;
            entity.PersonId = dto.PersonId;
            entity.Town = dto.Town;
            entity.StateId = dto.StateId;
            entity.Branding = dto.Branding;
            entity.IsContactInfoVisible = dto.IsContactInfoVisible;
            entity.YearsExperience = dto.YearsExperience;
            entity.NcrcLevelId = dto.NcrcLevelId;
            entity.CandidateIsVeteran = dto.CandidateIsVeteran;
            entity.InvitationQueued = dto.InvitationQueued;
            return entity;
        }

        public static InviteToApplyBasicView CopyTo(this InviteToApplyBasicViewDto dto)
        {
            var entity = new InviteToApplyBasicView();
            return dto.CopyTo(entity);
        }

        #endregion


        #region CandidateClickHowToApplyViewDto Mappers

        public static CandidateClickHowToApplyViewDto AsDto(this CandidateClickHowToApplyView entity)
        {
            var dto = new CandidateClickHowToApplyViewDto
            {
                ActionedOn = entity.ActionedOn,
                JobTitle = entity.JobTitle,
                EmployeeId = entity.EmployeeId,
                JobId = entity.JobId,
                PersonId = entity.PersonId,
                UserId = entity.UserId,
                Id = entity.Id
            };
            return dto;
        }

        public static CandidateClickHowToApplyView CopyTo(this CandidateClickHowToApplyViewDto dto, CandidateClickHowToApplyView entity)
        {
            entity.ActionedOn = dto.ActionedOn;
            entity.JobTitle = dto.JobTitle;
            entity.EmployeeId = dto.EmployeeId;
            entity.JobId = dto.JobId;
            entity.PersonId = dto.PersonId;
            entity.UserId = dto.UserId;
            return entity;
        }

        public static CandidateClickHowToApplyView CopyTo(this CandidateClickHowToApplyViewDto dto)
        {
            var entity = new CandidateClickHowToApplyView();
            return dto.CopyTo(entity);
        }

        #endregion

        #region CandidateDidNotClickHowToApplyViewDto Mappers

        public static CandidateDidNotClickHowToApplyViewDto AsDto(this CandidateDidNotClickHowToApplyView entity)
        {
            var dto = new CandidateDidNotClickHowToApplyViewDto
            {
                JobId = entity.JobId,
                PersonId = entity.PersonId,
                ActionedOn = entity.ActionedOn,
                EmployeeId = entity.EmployeeId,
                Id = entity.Id
            };
            return dto;
        }

        public static CandidateDidNotClickHowToApplyView CopyTo(this CandidateDidNotClickHowToApplyViewDto dto, CandidateDidNotClickHowToApplyView entity)
        {
            entity.JobId = dto.JobId;
            entity.PersonId = dto.PersonId;
            entity.ActionedOn = dto.ActionedOn;
            entity.EmployeeId = dto.EmployeeId;
            return entity;
        }

        public static CandidateDidNotClickHowToApplyView CopyTo(this CandidateDidNotClickHowToApplyViewDto dto)
        {
            var entity = new CandidateDidNotClickHowToApplyView();
            return dto.CopyTo(entity);
        }

        #endregion

        #region ViewedResumeViewDto Mappers

        public static ViewedResumeViewDto AsDto(this ViewedResumeView entity)
        {
            var dto = new ViewedResumeViewDto
            {
                ActionedOn = entity.ActionedOn,
                ResumeId = entity.ResumeId,
                ResumePersonId = entity.ResumePersonId,
                ViewerUserId = entity.ViewerUserId,
                ViewerUserType = entity.ViewerUserType,
                Id = entity.Id
            };
            return dto;
        }

        public static ViewedResumeView CopyTo(this ViewedResumeViewDto dto, ViewedResumeView entity)
        {
            entity.ActionedOn = dto.ActionedOn;
            entity.ResumeId = dto.ResumeId;
            entity.ResumePersonId = dto.ResumePersonId;
            entity.ViewerUserId = dto.ViewerUserId;
            entity.ViewerUserType = dto.ViewerUserType;
            return entity;
        }

        public static ViewedResumeView CopyTo(this ViewedResumeViewDto dto)
        {
            var entity = new ViewedResumeView();
            return dto.CopyTo(entity);
        }

        #endregion

        #region UserRoleDto Mappers

        public static UserRoleDto AsDto(this UserRole entity)
        {
            var dto = new UserRoleDto
            {
                UserId = entity.UserId,
                RoleId = entity.RoleId,
                Id = entity.Id
            };
            return dto;
        }

        public static UserRole CopyTo(this UserRoleDto dto, UserRole entity)
        {
            entity.UserId = dto.UserId;
            entity.RoleId = dto.RoleId;
            return entity;
        }

        public static UserRole CopyTo(this UserRoleDto dto)
        {
            var entity = new UserRole();
            return dto.CopyTo(entity);
        }

        #endregion

        #region JobNoteDto Mappers

        public static JobNoteDto AsDto(this JobNote entity)
        {
            var dto = new JobNoteDto
            {
                JobId = entity.JobId,
                NoteId = entity.NoteId,
                Id = entity.Id
            };
            return dto;
        }

        public static JobNote CopyTo(this JobNoteDto dto, JobNote entity)
        {
            entity.JobId = dto.JobId;
            entity.NoteId = dto.NoteId;
            return entity;
        }

        public static JobNote CopyTo(this JobNoteDto dto)
        {
            var entity = new JobNote();
            return dto.CopyTo(entity);
        }

        #endregion

        #region SavedSearchUserDto Mappers

        public static SavedSearchUserDto AsDto(this SavedSearchUser entity)
        {
            var dto = new SavedSearchUserDto
            {
                UserId = entity.UserId,
                SavedSearchId = entity.SavedSearchId,
                Id = entity.Id
            };
            return dto;
        }

        public static SavedSearchUser CopyTo(this SavedSearchUserDto dto, SavedSearchUser entity)
        {
            entity.UserId = dto.UserId;
            entity.SavedSearchId = dto.SavedSearchId;
            return entity;
        }

        public static SavedSearchUser CopyTo(this SavedSearchUserDto dto)
        {
            var entity = new SavedSearchUser();
            return dto.CopyTo(entity);
        }

        #endregion

        #region PersonListPersonDto Mappers

        public static PersonListPersonDto AsDto(this PersonListPerson entity)
        {
            var dto = new PersonListPersonDto
            {
                PersonId = entity.PersonId,
                PersonListId = entity.PersonListId,
                Id = entity.Id
            };
            return dto;
        }

        public static PersonListPerson CopyTo(this PersonListPersonDto dto, PersonListPerson entity)
        {
            entity.PersonId = dto.PersonId;
            entity.PersonListId = dto.PersonListId;
            return entity;
        }

        public static PersonListPerson CopyTo(this PersonListPersonDto dto)
        {
            var entity = new PersonListPerson();
            return dto.CopyTo(entity);
        }

        #endregion

        #region StudentAlumniIssueViewDto Mappers

        public static StudentAlumniIssueViewDto AsDto(this StudentAlumniIssueView entity)
        {
            var dto = new StudentAlumniIssueViewDto
            {
                FirstName = entity.FirstName,
                LastName = entity.LastName,
                DateOfBirth = entity.DateOfBirth,
                SocialSecurityNumber = entity.SocialSecurityNumber,
                EmailAddress = entity.EmailAddress,
                LastLoggedInOn = entity.LastLoggedInOn,
                NoLoginTriggered = entity.NoLoginTriggered,
                JobOfferRejectionTriggered = entity.JobOfferRejectionTriggered,
                NotReportingToInterviewTriggered = entity.NotReportingToInterviewTriggered,
                NotClickingOnLeadsTriggered = entity.NotClickingOnLeadsTriggered,
                NotRespondingToEmployerInvitesTriggered = entity.NotRespondingToEmployerInvitesTriggered,
                ShowingLowQualityMatchesTriggered = entity.ShowingLowQualityMatchesTriggered,
                PostingLowQualityResumeTriggered = entity.PostingLowQualityResumeTriggered,
                PostHireFollowUpTriggered = entity.PostHireFollowUpTriggered,
                FollowUpRequested = entity.FollowUpRequested,
                NotSearchingJobsTriggered = entity.NotSearchingJobsTriggered,
                EnrollmentStatus = entity.EnrollmentStatus,
                ProgramAreaId = entity.ProgramAreaId,
                DegreeId = entity.DegreeId,
                UserType = entity.UserType,
                ExternalId = entity.ExternalId,
                InappropriateEmailAddress = entity.InappropriateEmailAddress,
                CampusId = entity.CampusId,
                Blocked = entity.Blocked,
                GivingPositiveFeedback = entity.GivingPositiveFeedback,
                GivingNegativeFeedback = entity.GivingNegativeFeedback,
                MiddleInitial = entity.MiddleInitial,
                UserName = entity.UserName,
                Id = entity.Id
            };
            return dto;
        }

        public static StudentAlumniIssueView CopyTo(this StudentAlumniIssueViewDto dto, StudentAlumniIssueView entity)
        {
            entity.FirstName = dto.FirstName;
            entity.LastName = dto.LastName;
            entity.DateOfBirth = dto.DateOfBirth;
            entity.SocialSecurityNumber = dto.SocialSecurityNumber;
            entity.EmailAddress = dto.EmailAddress;
            entity.LastLoggedInOn = dto.LastLoggedInOn;
            entity.NoLoginTriggered = dto.NoLoginTriggered;
            entity.JobOfferRejectionTriggered = dto.JobOfferRejectionTriggered;
            entity.NotReportingToInterviewTriggered = dto.NotReportingToInterviewTriggered;
            entity.NotClickingOnLeadsTriggered = dto.NotClickingOnLeadsTriggered;
            entity.NotRespondingToEmployerInvitesTriggered = dto.NotRespondingToEmployerInvitesTriggered;
            entity.ShowingLowQualityMatchesTriggered = dto.ShowingLowQualityMatchesTriggered;
            entity.PostingLowQualityResumeTriggered = dto.PostingLowQualityResumeTriggered;
            entity.PostHireFollowUpTriggered = dto.PostHireFollowUpTriggered;
            entity.FollowUpRequested = dto.FollowUpRequested;
            entity.NotSearchingJobsTriggered = dto.NotSearchingJobsTriggered;
            entity.EnrollmentStatus = dto.EnrollmentStatus;
            entity.ProgramAreaId = dto.ProgramAreaId;
            entity.DegreeId = dto.DegreeId;
            entity.UserType = dto.UserType;
            entity.ExternalId = dto.ExternalId;
            entity.InappropriateEmailAddress = dto.InappropriateEmailAddress;
            entity.CampusId = dto.CampusId;
            entity.Blocked = dto.Blocked;
            entity.GivingPositiveFeedback = dto.GivingPositiveFeedback;
            entity.GivingNegativeFeedback = dto.GivingNegativeFeedback;
            entity.MiddleInitial = dto.MiddleInitial;
            entity.UserName = dto.UserName;
            return entity;
        }

        public static StudentAlumniIssueView CopyTo(this StudentAlumniIssueViewDto dto)
        {
            var entity = new StudentAlumniIssueView();
            return dto.CopyTo(entity);
        }

        #endregion

        #region JobEducationInternshipSkillDto Mappers

        public static JobEducationInternshipSkillDto AsDto(this JobEducationInternshipSkill entity)
        {
            var dto = new JobEducationInternshipSkillDto
            {
                EducationInternshipSkillId = entity.EducationInternshipSkillId,
                JobId = entity.JobId,
                Id = entity.Id
            };
            return dto;
        }

        public static JobEducationInternshipSkill CopyTo(this JobEducationInternshipSkillDto dto, JobEducationInternshipSkill entity)
        {
            entity.EducationInternshipSkillId = dto.EducationInternshipSkillId;
            entity.JobId = dto.JobId;
            return entity;
        }

        public static JobEducationInternshipSkill CopyTo(this JobEducationInternshipSkillDto dto)
        {
            var entity = new JobEducationInternshipSkill();
            return dto.CopyTo(entity);
        }

        #endregion

        #region BusinessUnitSearchViewDto Mappers

        public static BusinessUnitSearchViewDto AsDto(this BusinessUnitSearchView entity)
        {
            var dto = new BusinessUnitSearchViewDto
            {
                BusinessUnitName = entity.BusinessUnitName,
                PostcodeZip = entity.PostcodeZip,
                EmployerId = entity.EmployerId,
                AddressLine1 = entity.AddressLine1,
                Id = entity.Id
            };
            return dto;
        }

        public static BusinessUnitSearchView CopyTo(this BusinessUnitSearchViewDto dto, BusinessUnitSearchView entity)
        {
            entity.BusinessUnitName = dto.BusinessUnitName;
            entity.PostcodeZip = dto.PostcodeZip;
            entity.EmployerId = dto.EmployerId;
            entity.AddressLine1 = dto.AddressLine1;
            return entity;
        }

        public static BusinessUnitSearchView CopyTo(this BusinessUnitSearchViewDto dto)
        {
            var entity = new BusinessUnitSearchView();
            return dto.CopyTo(entity);
        }

        #endregion

        #region JobSeekerProfileViewDto Mappers

        public static JobSeekerProfileViewDto AsDto(this JobSeekerProfileView entity)
        {
            var dto = new JobSeekerProfileViewDto
            {
                CreatedOn = entity.CreatedOn,
                FirstName = entity.FirstName,
                LastName = entity.LastName,
                ClientId = entity.ClientId,
                DateOfBirth = entity.DateOfBirth,
                SocialSecurityNumber = entity.SocialSecurityNumber,
                EmailAddress = entity.EmailAddress,
                AddressLine1 = entity.AddressLine1,
                TownCity = entity.TownCity,
                StateId = entity.StateId,
                CountryId = entity.CountryId,
                PostcodeZip = entity.PostcodeZip,
                HomePhone = entity.HomePhone,
                DefaultResumeId = entity.DefaultResumeId,
                IsSearchable = entity.IsSearchable,
                EnrollmentStatus = entity.EnrollmentStatus,
                UserId = entity.UserId,
                MiddleInitial = entity.MiddleInitial,
                MigrantSeasonalFarmWorkerVerified = entity.MigrantSeasonalFarmWorkerVerified,
                Enabled = entity.Enabled,
                Id = entity.Id
            };
            return dto;
        }

        public static JobSeekerProfileView CopyTo(this JobSeekerProfileViewDto dto, JobSeekerProfileView entity)
        {
            entity.FirstName = dto.FirstName;
            entity.LastName = dto.LastName;
            entity.ClientId = dto.ClientId;
            entity.DateOfBirth = dto.DateOfBirth;
            entity.SocialSecurityNumber = dto.SocialSecurityNumber;
            entity.EmailAddress = dto.EmailAddress;
            entity.AddressLine1 = dto.AddressLine1;
            entity.TownCity = dto.TownCity;
            entity.StateId = dto.StateId;
            entity.CountryId = dto.CountryId;
            entity.PostcodeZip = dto.PostcodeZip;
            entity.HomePhone = dto.HomePhone;
            entity.DefaultResumeId = dto.DefaultResumeId;
            entity.IsSearchable = dto.IsSearchable;
            entity.EnrollmentStatus = dto.EnrollmentStatus;
            entity.UserId = dto.UserId;
            entity.MiddleInitial = dto.MiddleInitial;
            entity.MigrantSeasonalFarmWorkerVerified = dto.MigrantSeasonalFarmWorkerVerified;
            entity.Enabled = dto.Enabled;
            return entity;
        }

        public static JobSeekerProfileView CopyTo(this JobSeekerProfileViewDto dto)
        {
            var entity = new JobSeekerProfileView();
            return dto.CopyTo(entity);
        }

        #endregion

        #region RegistrationPinDto Mappers

        public static RegistrationPinDto AsDto(this RegistrationPin entity)
        {
            var dto = new RegistrationPinDto
            {
                CreatedOn = entity.CreatedOn,
                Pin = entity.Pin,
                EmailAddress = entity.EmailAddress,
                CreatedBy = entity.CreatedBy,
                Id = entity.Id
            };
            return dto;
        }

        public static RegistrationPin CopyTo(this RegistrationPinDto dto, RegistrationPin entity)
        {
            entity.Pin = dto.Pin;
            entity.EmailAddress = dto.EmailAddress;
            entity.CreatedBy = dto.CreatedBy;
            return entity;
        }

        public static RegistrationPin CopyTo(this RegistrationPinDto dto)
        {
            var entity = new RegistrationPin();
            return dto.CopyTo(entity);
        }

        #endregion

        #region OfficeDto Mappers

        public static OfficeDto AsDto(this Office entity)
        {
            var dto = new OfficeDto
            {
                OfficeName = entity.OfficeName,
                Line1 = entity.Line1,
                Line2 = entity.Line2,
                TownCity = entity.TownCity,
                CountyId = entity.CountyId,
                StateId = entity.StateId,
                CountryId = entity.CountryId,
                PostcodeZip = entity.PostcodeZip,
                AssignedPostcodeZip = entity.AssignedPostcodeZip,
                OfficeManagerMailbox = entity.OfficeManagerMailbox,
                ExternalId = entity.ExternalId,
                InActive = entity.InActive,
                BusinessOutreachMailbox = entity.BusinessOutreachMailbox,
                DefaultType = entity.DefaultType,
                DefaultAdministratorId = entity.DefaultAdministratorId,
                Id = entity.Id
            };
            return dto;
        }

        public static Office CopyTo(this OfficeDto dto, Office entity)
        {
            entity.OfficeName = dto.OfficeName;
            entity.Line1 = dto.Line1;
            entity.Line2 = dto.Line2;
            entity.TownCity = dto.TownCity;
            entity.CountyId = dto.CountyId;
            entity.StateId = dto.StateId;
            entity.CountryId = dto.CountryId;
            entity.PostcodeZip = dto.PostcodeZip;
            entity.AssignedPostcodeZip = dto.AssignedPostcodeZip;
            entity.OfficeManagerMailbox = dto.OfficeManagerMailbox;
            entity.ExternalId = dto.ExternalId;
            entity.InActive = dto.InActive;
            entity.BusinessOutreachMailbox = dto.BusinessOutreachMailbox;
            entity.DefaultType = dto.DefaultType;
            entity.DefaultAdministratorId = dto.DefaultAdministratorId;
            return entity;
        }

        public static Office CopyTo(this OfficeDto dto)
        {
            var entity = new Office();
            return dto.CopyTo(entity);
        }

        #endregion

        #region PersonPostcodeMapper Mappers

        public static PersonPostcodeMapperDto AsDto(this PersonPostcodeMapper entity)
        {
            var dto = new PersonPostcodeMapperDto
            {
                PostcodeZip = entity.PostcodeZip,
                OwnedById = entity.OwnedById,
                PersonId = entity.PersonId,
                Id = entity.Id
            };
            return dto;
        }

        public static PersonPostcodeMapper CopyTo(this PersonPostcodeMapperDto dto, PersonPostcodeMapper entity)
        {
            entity.PostcodeZip = dto.PostcodeZip;
            entity.OwnedById = dto.OwnedById;
            entity.PersonId = dto.PersonId;
            return entity;
        }

        public static PersonPostcodeMapper CopyTo(this PersonPostcodeMapperDto dto)
        {
            var entity = new PersonPostcodeMapper();
            return dto.CopyTo(entity);
        }

        #endregion

        #region PersonOfficeMapperDto Mappers

        public static PersonOfficeMapperDto AsDto(this PersonOfficeMapper entity)
        {
            var dto = new PersonOfficeMapperDto
            {
                CreatedOn = entity.CreatedOn,
                StateId = entity.StateId,
                OfficeUnassigned = entity.OfficeUnassigned,
                PersonId = entity.PersonId,
                OfficeId = entity.OfficeId,
                Id = entity.Id
            };
            return dto;
        }

        public static PersonOfficeMapper CopyTo(this PersonOfficeMapperDto dto, PersonOfficeMapper entity)
        {
            entity.StateId = dto.StateId;
            entity.OfficeUnassigned = dto.OfficeUnassigned;
            entity.PersonId = dto.PersonId;
            entity.OfficeId = dto.OfficeId;
            return entity;
        }

        public static PersonOfficeMapper CopyTo(this PersonOfficeMapperDto dto)
        {
            var entity = new PersonOfficeMapper();
            return dto.CopyTo(entity);
        }

        #endregion

        #region JobPostingOfInterestSentDto Mappers

        public static JobPostingOfInterestSentDto AsDto(this JobPostingOfInterestSent entity)
        {
            var dto = new JobPostingOfInterestSentDto
            {
                CreatedOn = entity.CreatedOn,
                Score = entity.Score,
                Applied = entity.Applied,
                IsRecommendation = entity.IsRecommendation,
                JobId = entity.JobId,
                PersonId = entity.PersonId,
                PostingId = entity.PostingId,
                Id = entity.Id,
                QueueRecommendation = entity.QueueRecommendation,
                CreatedBy = entity.CreatedBy
            };
            return dto;
        }

        public static JobPostingOfInterestSent CopyTo(this JobPostingOfInterestSentDto dto, JobPostingOfInterestSent entity)
        {
            entity.Score = dto.Score;
            entity.Applied = dto.Applied;
            entity.IsRecommendation = dto.IsRecommendation;
            entity.JobId = dto.JobId;
            entity.PersonId = dto.PersonId;
            entity.PostingId = dto.PostingId;
            entity.QueueRecommendation = dto.QueueRecommendation;
            entity.CreatedBy = dto.CreatedBy;
            return entity;
        }

        public static JobPostingOfInterestSent CopyTo(this JobPostingOfInterestSentDto dto)
        {
            var entity = new JobPostingOfInterestSent();
            return dto.CopyTo(entity);
        }

        #endregion

        #region JobPostingOfInterestSentViewDto Mappers

        public static JobPostingOfInterestSentViewDto AsDto(this JobPostingOfInterestSentView entity)
        {
            var dto = new JobPostingOfInterestSentViewDto
            {
                SentOn = entity.SentOn,
                PersonId = entity.PersonId,
                LensPostingId = entity.LensPostingId,
                JobTitle = entity.JobTitle,
                EmployerName = entity.EmployerName,
                JobLocation = entity.JobLocation,
                Score = entity.Score,
                Applied = entity.Applied,
                JobStatus = entity.JobStatus,
                IsRecommendation = entity.IsRecommendation,
                Id = entity.Id
            };
            return dto;
        }

        public static JobPostingOfInterestSentView CopyTo(this JobPostingOfInterestSentViewDto dto, JobPostingOfInterestSentView entity)
        {
            entity.SentOn = dto.SentOn;
            entity.PersonId = dto.PersonId;
            entity.LensPostingId = dto.LensPostingId;
            entity.JobTitle = dto.JobTitle;
            entity.EmployerName = dto.EmployerName;
            entity.JobLocation = dto.JobLocation;
            entity.Score = dto.Score;
            entity.Applied = dto.Applied;
            entity.JobStatus = dto.JobStatus;
            entity.IsRecommendation = dto.IsRecommendation;
            return entity;
        }

        public static JobPostingOfInterestSentView CopyTo(this JobPostingOfInterestSentViewDto dto)
        {
            var entity = new JobPostingOfInterestSentView();
            return dto.CopyTo(entity);
        }

        #endregion

        #region RecentlyPlacedPersonMatchViewDto Mappers

        public static RecentlyPlacedPersonMatchViewDto AsDto(this RecentlyPlacedPersonMatchView entity)
        {
            var dto = new RecentlyPlacedPersonMatchViewDto
            {
                MatchedPersonId = entity.MatchedPersonId,
                MatchedCandidateName = entity.MatchedCandidateName,
                RecentlyPlacedId = entity.RecentlyPlacedId,
                PlacedPersonId = entity.PlacedPersonId,
                PlacedCandidateName = entity.PlacedCandidateName,
                Score = entity.Score,
                BusinessUnitId = entity.BusinessUnitId,
                BusinessUnitName = entity.BusinessUnitName,
                EmployerId = entity.EmployerId,
                EmployerName = entity.EmployerName,
                PlacementDate = entity.PlacementDate,
                JobId = entity.JobId,
                JobTitle = entity.JobTitle,
                Id = entity.Id
            };
            return dto;
        }

        public static RecentlyPlacedPersonMatchView CopyTo(this RecentlyPlacedPersonMatchViewDto dto, RecentlyPlacedPersonMatchView entity)
        {
            entity.MatchedPersonId = dto.MatchedPersonId;
            entity.MatchedCandidateName = dto.MatchedCandidateName;
            entity.RecentlyPlacedId = dto.RecentlyPlacedId;
            entity.PlacedPersonId = dto.PlacedPersonId;
            entity.PlacedCandidateName = dto.PlacedCandidateName;
            entity.Score = dto.Score;
            entity.BusinessUnitId = dto.BusinessUnitId;
            entity.BusinessUnitName = dto.BusinessUnitName;
            entity.EmployerId = dto.EmployerId;
            entity.EmployerName = dto.EmployerName;
            entity.PlacementDate = dto.PlacementDate;
            entity.JobId = dto.JobId;
            entity.JobTitle = dto.JobTitle;
            return entity;
        }

        public static RecentlyPlacedPersonMatchView CopyTo(this RecentlyPlacedPersonMatchViewDto dto)
        {
            var entity = new RecentlyPlacedPersonMatchView();
            return dto.CopyTo(entity);
        }

        #endregion

        #region JobOfficeMapperDto Mappers

        public static JobOfficeMapperDto AsDto(this JobOfficeMapper entity)
        {
            var dto = new JobOfficeMapperDto
            {
                OfficeUnassigned = entity.OfficeUnassigned,
                JobId = entity.JobId,
                OfficeId = entity.OfficeId,
                Id = entity.Id
            };
            return dto;
        }

        public static JobOfficeMapper CopyTo(this JobOfficeMapperDto dto, JobOfficeMapper entity)
        {
            entity.OfficeUnassigned = dto.OfficeUnassigned;
            entity.JobId = dto.JobId;
            entity.OfficeId = dto.OfficeId;
            return entity;
        }

        public static JobOfficeMapper CopyTo(this JobOfficeMapperDto dto)
        {
            var entity = new JobOfficeMapper();
            return dto.CopyTo(entity);
        }

        #endregion

        #region EmployerOfficeMapperDto Mappers

        public static EmployerOfficeMapperDto AsDto(this EmployerOfficeMapper entity)
        {
            var dto = new EmployerOfficeMapperDto
            {
                OfficeUnassigned = entity.OfficeUnassigned,
                EmployerId = entity.EmployerId,
                OfficeId = entity.OfficeId,
                Id = entity.Id
            };
            return dto;
        }

        public static EmployerOfficeMapper CopyTo(this EmployerOfficeMapperDto dto, EmployerOfficeMapper entity)
        {
            entity.OfficeUnassigned = dto.OfficeUnassigned;
            entity.EmployerId = dto.EmployerId;
            entity.OfficeId = dto.OfficeId;
            return entity;
        }

        public static EmployerOfficeMapper CopyTo(this EmployerOfficeMapperDto dto)
        {
            var entity = new EmployerOfficeMapper();
            return dto.CopyTo(entity);
        }

        #endregion

        #region JobIssuesDto Mappers

        public static JobIssuesDto AsDto(this JobIssues entity)
        {
            var dto = new JobIssuesDto
            {
                LowQualityMatches = entity.LowQualityMatches,
                LowQualityMatchesResolvedDate = entity.LowQualityMatchesResolvedDate,
                LowReferralActivity = entity.LowReferralActivity,
                LowReferralActivityResolvedDate = entity.LowReferralActivityResolvedDate,
                NotViewingReferrals = entity.NotViewingReferrals,
                NotViewingReferralsResolvedDate = entity.NotViewingReferralsResolvedDate,
                SearchingButNotInviting = entity.SearchingButNotInviting,
                SearchingButNotInvitingResolvedDate = entity.SearchingButNotInvitingResolvedDate,
                ClosingDateRefreshed = entity.ClosingDateRefreshed,
                EarlyJobClosing = entity.EarlyJobClosing,
                NegativeSurveyResponse = entity.NegativeSurveyResponse,
                PositiveSurveyResponse = entity.PositiveSurveyResponse,
                FollowUpRequested = entity.FollowUpRequested,
                JobId = entity.JobId,
                Id = entity.Id
            };
            return dto;
        }

        public static JobIssues CopyTo(this JobIssuesDto dto, JobIssues entity)
        {
            entity.LowQualityMatches = dto.LowQualityMatches;
            entity.LowQualityMatchesResolvedDate = dto.LowQualityMatchesResolvedDate;
            entity.LowReferralActivity = dto.LowReferralActivity;
            entity.LowReferralActivityResolvedDate = dto.LowReferralActivityResolvedDate;
            entity.NotViewingReferrals = dto.NotViewingReferrals;
            entity.NotViewingReferralsResolvedDate = dto.NotViewingReferralsResolvedDate;
            entity.SearchingButNotInviting = dto.SearchingButNotInviting;
            entity.SearchingButNotInvitingResolvedDate = dto.SearchingButNotInvitingResolvedDate;
            entity.ClosingDateRefreshed = dto.ClosingDateRefreshed;
            entity.EarlyJobClosing = dto.EarlyJobClosing;
            entity.NegativeSurveyResponse = dto.NegativeSurveyResponse;
            entity.PositiveSurveyResponse = dto.PositiveSurveyResponse;
            entity.FollowUpRequested = dto.FollowUpRequested;
            entity.JobId = dto.JobId;
            return entity;
        }

        public static JobIssues CopyTo(this JobIssuesDto dto)
        {
            var entity = new JobIssues();
            return dto.CopyTo(entity);
        }

        #endregion

        #region CandidateSearchHistoryDto Mappers

        public static CandidateSearchHistoryDto AsDto(this CandidateSearchHistory entity)
        {
            var dto = new CandidateSearchHistoryDto
            {
                CreatedOn = entity.CreatedOn,
                CreatedBy = entity.CreatedBy,
                JobId = entity.JobId,
                Id = entity.Id
            };
            return dto;
        }

        public static CandidateSearchHistory CopyTo(this CandidateSearchHistoryDto dto, CandidateSearchHistory entity)
        {
            entity.CreatedBy = dto.CreatedBy;
            entity.JobId = dto.JobId;
            return entity;
        }

        public static CandidateSearchHistory CopyTo(this CandidateSearchHistoryDto dto)
        {
            var entity = new CandidateSearchHistory();
            return dto.CopyTo(entity);
        }

        #endregion

        #region CandidateSearchResultDto Mappers

        public static CandidateSearchResultDto AsDto(this CandidateSearchResult entity)
        {
            var dto = new CandidateSearchResultDto
            {
                PersonId = entity.PersonId,
                Score = entity.Score,
                CandidateSearchHistoryId = entity.CandidateSearchHistoryId,
                Id = entity.Id
            };
            return dto;
        }

        public static CandidateSearchResult CopyTo(this CandidateSearchResultDto dto, CandidateSearchResult entity)
        {
            entity.PersonId = dto.PersonId;
            entity.Score = dto.Score;
            entity.CandidateSearchHistoryId = dto.CandidateSearchHistoryId;
            return entity;
        }

        public static CandidateSearchResult CopyTo(this CandidateSearchResultDto dto)
        {
            var entity = new CandidateSearchResult();
            return dto.CopyTo(entity);
        }

        #endregion

        #region EmployerPlacementsViewDto Mappers

        public static EmployerPlacementsViewDto AsDto(this EmployerPlacementsView entity)
        {
            var dto = new EmployerPlacementsViewDto
            {
                EmployerName = entity.EmployerName,
                JobTitle = entity.JobTitle,
                StartDate = entity.StartDate,
                Name = entity.Name,
                Id = entity.Id,
                EmployerId = entity.EmployerId,
                JobSeekerId = entity.JobSeekerId,
                BusinessUnitId = entity.BusinessUnitId
            };
            return dto;
        }

        public static EmployerPlacementsView CopyTo(this EmployerPlacementsViewDto dto, EmployerPlacementsView entity)
        {
            entity.EmployerName = dto.EmployerName;
            entity.JobTitle = dto.JobTitle;
            entity.StartDate = dto.StartDate;
            entity.Name = dto.Name;
            entity.EmployerId = dto.EmployerId;
            entity.JobSeekerId = dto.JobSeekerId;
            entity.BusinessUnitId = dto.BusinessUnitId;
            return entity;
        }

        public static EmployerPlacementsView CopyTo(this EmployerPlacementsViewDto dto)
        {
            var entity = new EmployerPlacementsView();
            return dto.CopyTo(entity);
        }

        #endregion

        #region NewApplicantReferralViewDto Mappers

        public static NewApplicantReferralViewDto AsDto(this NewApplicantReferralView entity)
        {
            var dto = new NewApplicantReferralViewDto
            {
                PersonId = entity.PersonId,
                JobTitle = entity.JobTitle,
                EmployerName = entity.EmployerName,
                ActionType = entity.ActionType,
                CandidateApplicationStatus = entity.CandidateApplicationStatus,
                ApplicationId = entity.ApplicationId,
                IsVeteran = entity.IsVeteran,
                VeteranPriorityEndDate = entity.VeteranPriorityEndDate,
                ActualEmployerName = entity.ActualEmployerName,
                ApprovalStatus = entity.ApprovalStatus,
                Id = entity.Id
            };
            return dto;
        }

        public static NewApplicantReferralView CopyTo(this NewApplicantReferralViewDto dto, NewApplicantReferralView entity)
        {
            entity.PersonId = dto.PersonId;
            entity.JobTitle = dto.JobTitle;
            entity.EmployerName = dto.EmployerName;
            entity.ActionType = dto.ActionType;
            entity.CandidateApplicationStatus = dto.CandidateApplicationStatus;
            entity.ApplicationId = dto.ApplicationId;
            entity.IsVeteran = dto.IsVeteran;
            entity.VeteranPriorityEndDate = dto.VeteranPriorityEndDate;
            entity.ActualEmployerName = dto.ActualEmployerName;
            entity.ApprovalStatus = dto.ApprovalStatus;
            return entity;
        }

        public static NewApplicantReferralView CopyTo(this NewApplicantReferralViewDto dto)
        {
            var entity = new NewApplicantReferralView();
            return dto.CopyTo(entity);
        }

        #endregion

        #region JobIssuesViewDto Mappers

        public static JobIssuesViewDto AsDto(this JobIssuesView entity)
        {
            var dto = new JobIssuesViewDto
            {
                JobId = entity.JobId,
                LowQualityMatches = entity.LowQualityMatches,
                LowReferralActivity = entity.LowReferralActivity,
                NotViewingReferrals = entity.NotViewingReferrals,
                SearchingButNotInviting = entity.SearchingButNotInviting,
                ClosingDateRefreshed = entity.ClosingDateRefreshed,
                EarlyJobClosing = entity.EarlyJobClosing,
                NegativeSurveyResponse = entity.NegativeSurveyResponse,
                PositiveSurveyResponse = entity.PositiveSurveyResponse,
                FollowUpRequested = entity.FollowUpRequested,
                PostHireFollowUpCount = entity.PostHireFollowUpCount,
                Id = entity.Id
            };
            return dto;
        }

        public static JobIssuesView CopyTo(this JobIssuesViewDto dto, JobIssuesView entity)
        {
            entity.JobId = dto.JobId;
            entity.LowQualityMatches = dto.LowQualityMatches;
            entity.LowReferralActivity = dto.LowReferralActivity;
            entity.NotViewingReferrals = dto.NotViewingReferrals;
            entity.SearchingButNotInviting = dto.SearchingButNotInviting;
            entity.ClosingDateRefreshed = dto.ClosingDateRefreshed;
            entity.EarlyJobClosing = dto.EarlyJobClosing;
            entity.NegativeSurveyResponse = dto.NegativeSurveyResponse;
            entity.PositiveSurveyResponse = dto.PositiveSurveyResponse;
            entity.FollowUpRequested = dto.FollowUpRequested;
            entity.PostHireFollowUpCount = dto.PostHireFollowUpCount;
            return entity;
        }

        public static JobIssuesView CopyTo(this JobIssuesViewDto dto)
        {
            var entity = new JobIssuesView();
            return dto.CopyTo(entity);
        }

        #endregion

        #region JobSeekerSurveyDto Mappers

        public static JobSeekerSurveyDto AsDto(this JobSeekerSurvey entity)
        {
            var dto = new JobSeekerSurveyDto
            {
                CreatedOn = entity.CreatedOn,
                SatisfactionLevel = entity.SatisfactionLevel,
                UnanticipatedMatches = entity.UnanticipatedMatches,
                WasInvitedDidApply = entity.WasInvitedDidApply,
                PersonId = entity.PersonId,
                Id = entity.Id
            };
            return dto;
        }

        public static JobSeekerSurvey CopyTo(this JobSeekerSurveyDto dto, JobSeekerSurvey entity)
        {
            entity.SatisfactionLevel = dto.SatisfactionLevel;
            entity.UnanticipatedMatches = dto.UnanticipatedMatches;
            entity.WasInvitedDidApply = dto.WasInvitedDidApply;
            entity.PersonId = dto.PersonId;
            return entity;
        }

        public static JobSeekerSurvey CopyTo(this JobSeekerSurveyDto dto)
        {
            var entity = new JobSeekerSurvey();
            return dto.CopyTo(entity);
        }

        #endregion

        #region JobSeekerSurveyTotalsViewDto Mappers

        public static JobSeekerSurveyTotalsViewDto AsDto(this JobSeekerSurveyTotalsView entity)
        {
            var dto = new JobSeekerSurveyTotalsViewDto
            {
                VerySatisfied = entity.VerySatisfied,
                SomewhatSatisfied = entity.SomewhatSatisfied,
                SomewhatDissatisfied = entity.SomewhatDissatisfied,
                Dissatisfied = entity.Dissatisfied,
                UnanticipatedMatches = entity.UnanticipatedMatches,
                NoUnanticipatedMatches = entity.NoUnanticipatedMatches,
                WasInvitedDidApply = entity.WasInvitedDidApply,
                WasNotInvitedOrDidNotApply = entity.WasNotInvitedOrDidNotApply,
                Id = entity.Id
            };
            return dto;
        }

        public static JobSeekerSurveyTotalsView CopyTo(this JobSeekerSurveyTotalsViewDto dto, JobSeekerSurveyTotalsView entity)
        {
            entity.VerySatisfied = dto.VerySatisfied;
            entity.SomewhatSatisfied = dto.SomewhatSatisfied;
            entity.SomewhatDissatisfied = dto.SomewhatDissatisfied;
            entity.Dissatisfied = dto.Dissatisfied;
            entity.UnanticipatedMatches = dto.UnanticipatedMatches;
            entity.NoUnanticipatedMatches = dto.NoUnanticipatedMatches;
            entity.WasInvitedDidApply = dto.WasInvitedDidApply;
            entity.WasNotInvitedOrDidNotApply = dto.WasNotInvitedOrDidNotApply;
            return entity;
        }

        public static JobSeekerSurveyTotalsView CopyTo(this JobSeekerSurveyTotalsViewDto dto)
        {
            var entity = new JobSeekerSurveyTotalsView();
            return dto.CopyTo(entity);
        }

        #endregion

        #region ReferralViewDto Mappers

        public static ReferralViewDto AsDto(this ReferralView entity)
        {
            var dto = new ReferralViewDto
            {
                LensPostingId = entity.LensPostingId,
                ApplicationDate = entity.ApplicationDate,
                JobTitle = entity.JobTitle,
                EmployerName = entity.EmployerName,
                CandidateApplicationAutomaticallyApproved = entity.CandidateApplicationAutomaticallyApproved,
                EmployeeName = entity.EmployeeName,
                CandidateId = entity.CandidateId,
                CandidateApplicationScore = entity.CandidateApplicationScore,
                ApplicationApprovalStatus = entity.ApplicationApprovalStatus,
                EmployeePhoneNumber = entity.EmployeePhoneNumber,
                EmployeeEmail = entity.EmployeeEmail,
                Location = entity.Location,
                PostingCreatedOn = entity.PostingCreatedOn,
                JobClosingOn = entity.JobClosingOn,
                JobId = entity.JobId,
                VeteranPriorityEndDate = entity.VeteranPriorityEndDate,
                IsVeteran = entity.IsVeteran,
                PreviousApprovalStatus = entity.PreviousApprovalStatus,
                PostingEmployerName = entity.PostingEmployerName,
                IsConfidential = entity.IsConfidential,
                InterviewContactPreferences = entity.InterviewContactPreferences,
                InterviewMailAddress = entity.InterviewMailAddress,
                InterviewEmailAddress = entity.InterviewEmailAddress,
                InterviewFaxNumber = entity.InterviewFaxNumber,
                InterviewDirectApplicationDetails = entity.InterviewDirectApplicationDetails,
                InterviewApplicationUrl = entity.InterviewApplicationUrl,
                InterviewPhoneNumber = entity.InterviewPhoneNumber,
                InterviewOtherInstructions = entity.InterviewOtherInstructions,
                Id = entity.Id
            };
            return dto;
        }

        public static ReferralView CopyTo(this ReferralViewDto dto, ReferralView entity)
        {
            entity.LensPostingId = dto.LensPostingId;
            entity.ApplicationDate = dto.ApplicationDate;
            entity.JobTitle = dto.JobTitle;
            entity.EmployerName = dto.EmployerName;
            entity.CandidateApplicationAutomaticallyApproved = dto.CandidateApplicationAutomaticallyApproved;
            entity.EmployeeName = dto.EmployeeName;
            entity.CandidateId = dto.CandidateId;
            entity.CandidateApplicationScore = dto.CandidateApplicationScore;
            entity.ApplicationApprovalStatus = dto.ApplicationApprovalStatus;
            entity.EmployeePhoneNumber = dto.EmployeePhoneNumber;
            entity.EmployeeEmail = dto.EmployeeEmail;
            entity.Location = dto.Location;
            entity.PostingCreatedOn = dto.PostingCreatedOn;
            entity.JobClosingOn = dto.JobClosingOn;
            entity.JobId = dto.JobId;
            entity.VeteranPriorityEndDate = dto.VeteranPriorityEndDate;
            entity.IsVeteran = dto.IsVeteran;
            entity.PreviousApprovalStatus = dto.PreviousApprovalStatus;
            entity.PostingEmployerName = dto.PostingEmployerName;
            entity.IsConfidential = dto.IsConfidential;
            entity.InterviewContactPreferences = dto.InterviewContactPreferences;
            entity.InterviewMailAddress = dto.InterviewMailAddress;
            entity.InterviewEmailAddress = dto.InterviewEmailAddress;
            entity.InterviewFaxNumber = dto.InterviewFaxNumber;
            entity.InterviewDirectApplicationDetails = dto.InterviewDirectApplicationDetails;
            entity.InterviewApplicationUrl = dto.InterviewApplicationUrl;
            entity.InterviewPhoneNumber = dto.InterviewPhoneNumber;
            entity.InterviewOtherInstructions = dto.InterviewOtherInstructions;
            return entity;
        }

        public static ReferralView CopyTo(this ReferralViewDto dto)
        {
            var entity = new ReferralView();
            return dto.CopyTo(entity);
        }

        #endregion

        #region CampusDto Mappers

        public static CampusDto AsDto(this Campus entity)
        {
            var dto = new CampusDto
            {
                Name = entity.Name,
                IsNonCampus = entity.IsNonCampus,
                Id = entity.Id
            };
            return dto;
        }

        public static Campus CopyTo(this CampusDto dto, Campus entity)
        {
            entity.Name = dto.Name;
            entity.IsNonCampus = dto.IsNonCampus;
            return entity;
        }

        public static Campus CopyTo(this CampusDto dto)
        {
            var entity = new Campus();
            return dto.CopyTo(entity);
        }

        #endregion

        #region IntegrationLogDto Mappers

        public static IntegrationLogDto AsDto(this IntegrationLog entity)
        {
            var dto = new IntegrationLogDto
            {
                CreatedOn = entity.CreatedOn,
                UpdatedOn = entity.UpdatedOn,
                IntegrationPoint = entity.IntegrationPoint,
                RequestededBy = entity.RequestededBy,
                RequestedOn = entity.RequestedOn,
                RequestStart = entity.RequestStart,
                RequestEnd = entity.RequestEnd,
                Outcome = entity.Outcome,
                RequestId = entity.RequestId,
                Id = entity.Id
            };
            return dto;
        }

        public static IntegrationLog CopyTo(this IntegrationLogDto dto, IntegrationLog entity)
        {
            entity.IntegrationPoint = dto.IntegrationPoint;
            entity.RequestededBy = dto.RequestededBy;
            entity.RequestedOn = dto.RequestedOn;
            entity.RequestStart = dto.RequestStart;
            entity.RequestEnd = dto.RequestEnd;
            entity.Outcome = dto.Outcome;
            entity.RequestId = dto.RequestId;
            return entity;
        }

        public static IntegrationLog CopyTo(this IntegrationLogDto dto)
        {
            var entity = new IntegrationLog();
            return dto.CopyTo(entity);
        }

        #endregion

        #region IntegrationExceptionDto Mappers

        public static IntegrationExceptionDto AsDto(this IntegrationException entity)
        {
            var dto = new IntegrationExceptionDto
            {
                Message = entity.Message,
                StackTrace = entity.StackTrace,
                IntegrationLogId = entity.IntegrationLogId,
                Id = entity.Id
            };
            return dto;
        }

        public static IntegrationException CopyTo(this IntegrationExceptionDto dto, IntegrationException entity)
        {
            entity.Message = dto.Message;
            entity.StackTrace = dto.StackTrace;
            entity.IntegrationLogId = dto.IntegrationLogId;
            return entity;
        }

        public static IntegrationException CopyTo(this IntegrationExceptionDto dto)
        {
            var entity = new IntegrationException();
            return dto.CopyTo(entity);
        }

        #endregion

        #region IntegrationRequestResponseDto Mappers

        public static IntegrationRequestResponseDto AsDto(this IntegrationRequestResponse entity)
        {
            var dto = new IntegrationRequestResponseDto
            {
                Url = entity.Url,
                RequestSentOn = entity.RequestSentOn,
                ResponseReceivedOn = entity.ResponseReceivedOn,
                StatusCode = entity.StatusCode,
                StatusDescription = entity.StatusDescription,
                IntegrationLogId = entity.IntegrationLogId,
                Id = entity.Id
            };
            return dto;
        }

        public static IntegrationRequestResponse CopyTo(this IntegrationRequestResponseDto dto, IntegrationRequestResponse entity)
        {
            entity.Url = dto.Url;
            entity.RequestSentOn = dto.RequestSentOn;
            entity.ResponseReceivedOn = dto.ResponseReceivedOn;
            entity.StatusCode = dto.StatusCode;
            entity.StatusDescription = dto.StatusDescription;
            entity.IntegrationLogId = dto.IntegrationLogId;
            return entity;
        }

        public static IntegrationRequestResponse CopyTo(this IntegrationRequestResponseDto dto)
        {
            var entity = new IntegrationRequestResponse();
            return dto.CopyTo(entity);
        }

        #endregion

        #region IntegrationRequestResponseMessagesDto Mappers

        public static IntegrationRequestResponseMessagesDto AsDto(this IntegrationRequestResponseMessages entity)
        {
            var dto = new IntegrationRequestResponseMessagesDto
            {
                RequestMessage = entity.RequestMessage,
                ResponseMessage = entity.ResponseMessage,
                IntegrationRequestResponseId = entity.IntegrationRequestResponseId,
                Id = entity.Id
            };
            return dto;
        }

        public static IntegrationRequestResponseMessages CopyTo(this IntegrationRequestResponseMessagesDto dto, IntegrationRequestResponseMessages entity)
        {
            entity.RequestMessage = dto.RequestMessage;
            entity.ResponseMessage = dto.ResponseMessage;
            entity.IntegrationRequestResponseId = dto.IntegrationRequestResponseId;
            return entity;
        }

        public static IntegrationRequestResponseMessages CopyTo(this IntegrationRequestResponseMessagesDto dto)
        {
            var entity = new IntegrationRequestResponseMessages();
            return dto.CopyTo(entity);
        }

        #endregion

        #region JobDrivingLicenceEndorsementDto Mappers

        public static JobDrivingLicenceEndorsementDto AsDto(this JobDrivingLicenceEndorsement entity)
        {
            var dto = new JobDrivingLicenceEndorsementDto
            {
                DrivingLicenceEndorsementId = entity.DrivingLicenceEndorsementId,
                JobId = entity.JobId,
                Id = entity.Id
            };
            return dto;
        }

        public static JobDrivingLicenceEndorsement CopyTo(this JobDrivingLicenceEndorsementDto dto, JobDrivingLicenceEndorsement entity)
        {
            entity.DrivingLicenceEndorsementId = dto.DrivingLicenceEndorsementId;
            entity.JobId = dto.JobId;
            return entity;
        }

        public static JobDrivingLicenceEndorsement CopyTo(this JobDrivingLicenceEndorsementDto dto)
        {
            var entity = new JobDrivingLicenceEndorsement();
            return dto.CopyTo(entity);
        }

        #endregion

        #region JobSeekerReferralAllStatusViewDto Mappers

        public static JobSeekerReferralAllStatusViewDto AsDto(this JobSeekerReferralAllStatusView entity)
        {
            var dto = new JobSeekerReferralAllStatusViewDto
            {
                Name = entity.Name,
                ReferralDate = entity.ReferralDate,
                JobTitle = entity.JobTitle,
                EmployerName = entity.EmployerName,
                EmployerId = entity.EmployerId,
                Posting = entity.Posting,
                CandidateId = entity.CandidateId,
                JobId = entity.JobId,
                ApprovalRequiredReason = entity.ApprovalRequiredReason,
                IsVeteran = entity.IsVeteran,
                Town = entity.Town,
                StateId = entity.StateId,
                PersonId = entity.PersonId,
                ApplicationScore = entity.ApplicationScore,
                AssignedToId = entity.AssignedToId,
                ApprovalStatus = entity.ApprovalStatus,
                ApplicationStatus = entity.ApplicationStatus,
                State = entity.State,
                TimeInQueue = entity.TimeInQueue,
                LensPostingId = entity.LensPostingId,
                StatusLastChangedOn = entity.StatusLastChangedOn,
                StatusLastChangedBy = entity.StatusLastChangedBy,
                AutomaticallyDenied = entity.AutomaticallyDenied,
                IsConfidential = entity.IsConfidential,
                LockVersion = entity.LockVersion,
                AutomaticallyOnHold = entity.AutomaticallyOnHold,
                ForeignLabourCertificationH2A = entity.ForeignLabourCertificationH2A,
                ForeignLabourCertificationH2B = entity.ForeignLabourCertificationH2B,
                JobseekerTown = entity.JobseekerTown,
                JobseekerStateId = entity.JobseekerStateId,
                JobLocation = entity.JobLocation,
                JobSeekerAddress = entity.JobSeekerAddress,
                PostingLocations = entity.PostingLocations,
                Id = entity.Id
            };
            return dto;
        }

        public static JobSeekerReferralAllStatusView CopyTo(this JobSeekerReferralAllStatusViewDto dto, JobSeekerReferralAllStatusView entity)
        {
            entity.Name = dto.Name;
            entity.ReferralDate = dto.ReferralDate;
            entity.JobTitle = dto.JobTitle;
            entity.EmployerName = dto.EmployerName;
            entity.EmployerId = dto.EmployerId;
            entity.Posting = dto.Posting;
            entity.CandidateId = dto.CandidateId;
            entity.JobId = dto.JobId;
            entity.ApprovalRequiredReason = dto.ApprovalRequiredReason;
            entity.IsVeteran = dto.IsVeteran;
            entity.Town = dto.Town;
            entity.StateId = dto.StateId;
            entity.PersonId = dto.PersonId;
            entity.ApplicationScore = dto.ApplicationScore;
            entity.AssignedToId = dto.AssignedToId;
            entity.ApprovalStatus = dto.ApprovalStatus;
            entity.ApplicationStatus = dto.ApplicationStatus;
            entity.State = dto.State;
            entity.TimeInQueue = dto.TimeInQueue;
            entity.LensPostingId = dto.LensPostingId;
            entity.StatusLastChangedOn = dto.StatusLastChangedOn;
            entity.StatusLastChangedBy = dto.StatusLastChangedBy;
            entity.AutomaticallyDenied = dto.AutomaticallyDenied;
            entity.IsConfidential = dto.IsConfidential;
            entity.LockVersion = dto.LockVersion;
            entity.AutomaticallyOnHold = dto.AutomaticallyOnHold;
            entity.ForeignLabourCertificationH2A = dto.ForeignLabourCertificationH2A;
            entity.ForeignLabourCertificationH2B = dto.ForeignLabourCertificationH2B;
            entity.JobseekerTown = dto.JobseekerTown;
            entity.JobseekerStateId = dto.JobseekerStateId;
            entity.JobLocation = dto.JobLocation;
            entity.JobSeekerAddress = dto.JobSeekerAddress;
            entity.PostingLocations = dto.PostingLocations;
            return entity;
        }

        public static JobSeekerReferralAllStatusView CopyTo(this JobSeekerReferralAllStatusViewDto dto)
        {
            var entity = new JobSeekerReferralAllStatusView();
            return dto.CopyTo(entity);
        }

        #endregion

        #region UploadFileDto Mappers

        public static UploadFileDto AsDto(this UploadFile entity)
        {
            var dto = new UploadFileDto
            {
                CreatedOn = entity.CreatedOn,
                UpdatedOn = entity.UpdatedOn,
                UserId = entity.UserId,
                SchemaType = entity.SchemaType,
                Status = entity.Status,
                RecordTypeName = entity.RecordTypeName,
                ProcessingState = entity.ProcessingState,
                FileName = entity.FileName,
                FileType = entity.FileType,
                CustomInformation = entity.CustomInformation,
                FileData = entity.FileData,
                Id = entity.Id
            };
            return dto;
        }

        public static UploadFile CopyTo(this UploadFileDto dto, UploadFile entity)
        {
            entity.UserId = dto.UserId;
            entity.SchemaType = dto.SchemaType;
            entity.Status = dto.Status;
            entity.RecordTypeName = dto.RecordTypeName;
            entity.ProcessingState = dto.ProcessingState;
            entity.FileName = dto.FileName;
            entity.FileType = dto.FileType;
            entity.CustomInformation = dto.CustomInformation;
            entity.FileData = dto.FileData;
            return entity;
        }

        public static UploadFile CopyTo(this UploadFileDto dto)
        {
            var entity = new UploadFile();
            return dto.CopyTo(entity);
        }

        #endregion

        #region UploadRecordDto Mappers

        public static UploadRecordDto AsDto(this UploadRecord entity)
        {
            var dto = new UploadRecordDto
            {
                RecordNumber = entity.RecordNumber,
                Status = entity.Status,
                ValidationMessage = entity.ValidationMessage,
                FieldValidation = entity.FieldValidation,
                FieldData = entity.FieldData,
                UploadFileId = entity.UploadFileId,
                Id = entity.Id
            };
            return dto;
        }

        public static UploadRecord CopyTo(this UploadRecordDto dto, UploadRecord entity)
        {
            entity.RecordNumber = dto.RecordNumber;
            entity.Status = dto.Status;
            entity.ValidationMessage = dto.ValidationMessage;
            entity.FieldValidation = dto.FieldValidation;
            entity.FieldData = dto.FieldData;
            entity.UploadFileId = dto.UploadFileId;
            return entity;
        }

        public static UploadRecord CopyTo(this UploadRecordDto dto)
        {
            var entity = new UploadRecord();
            return dto.CopyTo(entity);
        }

        #endregion

        #region ResumeAddInDto Mappers

        public static ResumeAddInDto AsDto(this ResumeAddIn entity)
        {
            var dto = new ResumeAddInDto
            {
                ResumeSectionType = entity.ResumeSectionType,
                Description = entity.Description,
                ResumeId = entity.ResumeId,
                Id = entity.Id
            };
            return dto;
        }

        public static ResumeAddIn CopyTo(this ResumeAddInDto dto, ResumeAddIn entity)
        {
            entity.ResumeSectionType = dto.ResumeSectionType;
            entity.Description = dto.Description;
            entity.ResumeId = dto.ResumeId;
            return entity;
        }

        public static ResumeAddIn CopyTo(this ResumeAddInDto dto)
        {
            var entity = new ResumeAddIn();
            return dto.CopyTo(entity);
        }

        #endregion

        #region ResumeTemplateDto Mappers

        public static ResumeTemplateDto AsDto(this ResumeTemplate entity)
        {
            var dto = new ResumeTemplateDto
            {
                CreatedOn = entity.CreatedOn,
                UpdatedOn = entity.UpdatedOn,
                ResumeXml = entity.ResumeXml,
                PersonId = entity.PersonId,
                Id = entity.Id
            };
            return dto;
        }

        public static ResumeTemplate CopyTo(this ResumeTemplateDto dto, ResumeTemplate entity)
        {
            entity.ResumeXml = dto.ResumeXml;
            entity.PersonId = dto.PersonId;
            return entity;
        }

        public static ResumeTemplate CopyTo(this ResumeTemplateDto dto)
        {
            var entity = new ResumeTemplate();
            return dto.CopyTo(entity);
        }

        #endregion

        #region UserActionTypeActivityDto Mappers

        public static UserActionTypeActivityDto AsDto(this UserActionTypeActivity entity)
        {
            var dto = new UserActionTypeActivityDto
            {
                DeletedOn = entity.DeletedOn,
                UserId = entity.UserId,
                LastActivityDate = entity.LastActivityDate,
                ExternalActivityId = entity.ExternalActivityId,
                ActionerId = entity.ActionerId,
                Id = entity.Id
            };
            return dto;
        }

        public static UserActionTypeActivity CopyTo(this UserActionTypeActivityDto dto, UserActionTypeActivity entity)
        {
            entity.UserId = dto.UserId;
            entity.LastActivityDate = dto.LastActivityDate;
            entity.ExternalActivityId = dto.ExternalActivityId;
            entity.ActionerId = dto.ActionerId;
            return entity;
        }

        public static UserActionTypeActivity CopyTo(this UserActionTypeActivityDto dto)
        {
            var entity = new UserActionTypeActivity();
            return dto.CopyTo(entity);
        }

        #endregion

        #region EncryptionDtoMappers

        public static EncryptionDto AsDto(this Encryption entity)
        {
            return new EncryptionDto
            {
                EntityTypeId = entity.EntityTypeId,
                EntityId = entity.EntityId,
                Vector = entity.Vector,
                TargetTypeId = entity.TargetTypeId
            };
        }

        public static Encryption CopyTo(this EncryptionDto dto, Encryption entity)
        {
            entity.EntityTypeId = dto.EntityTypeId;
            entity.EntityId = dto.EntityId;
            entity.Vector = dto.Vector;
            entity.TargetTypeId = dto.TargetTypeId;
            return entity;
        }

        public static Encryption CopyTo(this EncryptionDto dto)
        {
            var entity = new Encryption();
            return dto.CopyTo(entity);
        }

        #endregion

        #region FailedIntegrationMessageDto Mappers

        public static FailedIntegrationMessageDto AsDto(this FailedIntegrationMessage entity)
        {
            var dto = new FailedIntegrationMessageDto
            {
                IntegrationPoint = entity.IntegrationPoint,
                DateSubmitted = entity.DateSubmitted,
                MessageType = entity.MessageType,
                ErrorDescription = entity.ErrorDescription,
                Request = entity.Request,
                IsIgnored = entity.IsIgnored,
                DateResent = entity.DateResent,
                Id = entity.Id
            };
            return dto;
        }

        public static FailedIntegrationMessage CopyTo(this FailedIntegrationMessageDto dto, FailedIntegrationMessage entity)
        {
            entity.IntegrationPoint = dto.IntegrationPoint;
            entity.DateSubmitted = dto.DateSubmitted;
            entity.MessageType = dto.MessageType;
            entity.ErrorDescription = dto.ErrorDescription;
            entity.Request = dto.Request;
            entity.IsIgnored = dto.IsIgnored;
            entity.DateResent = dto.DateResent;
            return entity;
        }

        public static FailedIntegrationMessage CopyTo(this FailedIntegrationMessageDto dto)
        {
            var entity = new FailedIntegrationMessage();
            return dto.CopyTo(entity);
        }

        #endregion

        #region ResumeSelectedDataDto Mappers

        public static ResumeSelectedDataDto AsDto(this ResumeSelectedData entity)
        {
            var dto = new ResumeSelectedDataDto
            {
                LookupType = entity.LookupType,
                LookupId = entity.LookupId,
                ResumeId = entity.ResumeId,
                Id = entity.Id
            };
            return dto;
        }

        public static ResumeSelectedData CopyTo(this ResumeSelectedDataDto dto, ResumeSelectedData entity)
        {
            entity.LookupType = dto.LookupType;
            entity.LookupId = dto.LookupId;
            entity.ResumeId = dto.ResumeId;
            return entity;
        }

        public static ResumeSelectedData CopyTo(this ResumeSelectedDataDto dto)
        {
            var entity = new ResumeSelectedData();
            return dto.CopyTo(entity);
        }

        #endregion

        #region JobIdsViewDto Mappers

        public static JobIdsViewDto AsDto(this JobIdsView entity)
        {
            var dto = new JobIdsViewDto
            {
                LensPostingId = entity.LensPostingId,
                JobStatus = entity.JobStatus,
                ExternalId = entity.ExternalId,
                VeteranPriorityEndDate = entity.VeteranPriorityEndDate,
                Id = entity.Id
            };
            return dto;
        }

        public static JobIdsView CopyTo(this JobIdsViewDto dto, JobIdsView entity)
        {
            entity.LensPostingId = dto.LensPostingId;
            entity.JobStatus = dto.JobStatus;
            entity.ExternalId = dto.ExternalId;
            entity.VeteranPriorityEndDate = dto.VeteranPriorityEndDate;
            return entity;
        }

        public static JobIdsView CopyTo(this JobIdsViewDto dto)
        {
            var entity = new JobIdsView();
            return dto.CopyTo(entity);
        }

        #endregion

        #region ApplicationReferralStatusReason

        public static ApplicationReferralStatusReasonDto AsDto(this ApplicationReferralStatusReason entity)
        {
            var dto = new ApplicationReferralStatusReasonDto
            {
                ReasonId = entity.ReasonId,
                OtherReasonText = entity.OtherReasonText,
                ApplicationId = entity.ApplicationId,
                EntityType = entity.EntityType,
                ApprovalStatus = entity.ApprovalStatus,
                Id = entity.Id
            };
            return dto;
        }

        public static ApplicationReferralStatusReason CopyTo(this ApplicationReferralStatusReasonDto dto, ApplicationReferralStatusReason entity)
        {
            entity.ReasonId = dto.ReasonId;
            entity.OtherReasonText = dto.OtherReasonText;
            entity.ApplicationId = dto.ApplicationId;
            entity.EntityType = dto.EntityType;
            entity.ApprovalStatus = dto.ApprovalStatus;
            return entity;
        }

        public static ApplicationReferralStatusReason CopyTo(this ApplicationReferralStatusReasonDto dto)
        {
            var entity = new ApplicationReferralStatusReason();
            return dto.CopyTo(entity);
        }

        #endregion

        #region EmployerReferralStatusReason

        public static EmployerReferralStatusReasonDto AsDto(this EmployerReferralStatusReason entity)
        {
            var dto = new EmployerReferralStatusReasonDto
            {
                ReasonId = entity.ReasonId,
                OtherReasonText = entity.OtherReasonText,
                EmployerId = entity.EmployerId,
                EntityType = entity.EntityType,
                ApprovalStatus = entity.ApprovalStatus,
                Id = entity.Id
            };
            return dto;
        }

        public static EmployerReferralStatusReason CopyTo(this EmployerReferralStatusReasonDto dto, EmployerReferralStatusReason entity)
        {
            entity.ReasonId = dto.ReasonId;
            entity.OtherReasonText = dto.OtherReasonText;
            entity.EmployerId = dto.EmployerId;
            entity.EntityType = dto.EntityType;
            entity.ApprovalStatus = dto.ApprovalStatus;
            return entity;
        }

        public static EmployerReferralStatusReason CopyTo(this EmployerReferralStatusReasonDto dto)
        {
            var entity = new EmployerReferralStatusReason();
            return dto.CopyTo(entity);
        }

        #endregion

        #region BusinessUnitReferralStatusReason

        public static BusinessUnitReferralStatusReasonDto AsDto(this BusinessUnitReferralStatusReason entity)
        {
            var dto = new BusinessUnitReferralStatusReasonDto
            {
                ReasonId = entity.ReasonId,
                OtherReasonText = entity.OtherReasonText,
                BusinessUnitId = entity.BusinessUnitId,
                EntityType = entity.EntityType,
                ApprovalStatus = entity.ApprovalStatus,
                Id = entity.Id
            };
            return dto;
        }

        public static BusinessUnitReferralStatusReason CopyTo(this BusinessUnitReferralStatusReasonDto dto, BusinessUnitReferralStatusReason entity)
        {
            entity.ReasonId = dto.ReasonId;
            entity.OtherReasonText = dto.OtherReasonText;
            entity.BusinessUnitId = dto.BusinessUnitId;
            entity.EntityType = dto.EntityType;
            entity.ApprovalStatus = dto.ApprovalStatus;
            return entity;
        }

        public static BusinessUnitReferralStatusReason CopyTo(this BusinessUnitReferralStatusReasonDto dto)
        {
            var entity = new BusinessUnitReferralStatusReason();
            return dto.CopyTo(entity);
        }

        #endregion

        #region JobReferralStatusReason

        public static JobReferralStatusReasonDto AsDto(this JobReferralStatusReason entity)
        {
            var dto = new JobReferralStatusReasonDto
            {
                ReasonId = entity.ReasonId,
                OtherReasonText = entity.OtherReasonText,
                JobId = entity.JobId,
                EntityType = entity.EntityType,
                ApprovalStatus = entity.ApprovalStatus,
                Id = entity.Id
            };
            return dto;
        }

        public static JobReferralStatusReason CopyTo(this JobReferralStatusReasonDto dto, JobReferralStatusReason entity)
        {
            entity.ReasonId = dto.ReasonId;
            entity.OtherReasonText = dto.OtherReasonText;
            entity.JobId = dto.JobId;
            entity.EntityType = dto.EntityType;
            entity.ApprovalStatus = dto.ApprovalStatus;
            return entity;
        }

        public static JobReferralStatusReason CopyTo(this JobReferralStatusReasonDto dto)
        {
            var entity = new JobReferralStatusReason();
            return dto.CopyTo(entity);
        }

        #endregion

        #region EmployeeReferralStatusReason

        public static EmployeeReferralStatusReasonDto AsDto(this EmployeeReferralStatusReason entity)
        {
            var dto = new EmployeeReferralStatusReasonDto
            {
                ReasonId = entity.ReasonId,
                OtherReasonText = entity.OtherReasonText,
                EmployeeId = entity.EmployeeId,
                EntityType = entity.EntityType,
                ApprovalStatus = entity.ApprovalStatus,
                Id = entity.Id
            };
            return dto;
        }

        public static EmployeeReferralStatusReason CopyTo(this EmployeeReferralStatusReasonDto dto, EmployeeReferralStatusReason entity)
        {
            entity.ReasonId = dto.ReasonId;
            entity.OtherReasonText = dto.OtherReasonText;
            entity.EmployeeId = dto.EmployeeId;
            entity.EntityType = dto.EntityType;
            entity.ApprovalStatus = dto.ApprovalStatus;
            return entity;
        }

        public static EmployeeReferralStatusReason CopyTo(this EmployeeReferralStatusReasonDto dto)
        {
            var entity = new EmployeeReferralStatusReason();
            return dto.CopyTo(entity);
        }

        #endregion

        #region DocumentDto Mappers

        public static DocumentDto AsDto(this Document entity)
        {
            var dto = new DocumentDto
            {
                File = entity.File,
                Title = entity.Title,
                Description = entity.Description,
                Category = entity.Category,
                Group = entity.Group,
                Order = entity.Order,
                Module = entity.Module,
                FileName = entity.FileName,
                MimeType = entity.MimeType,
                Id = entity.Id
            };
            return dto;
        }

        public static Document CopyTo(this DocumentDto dto, Document entity)
        {
            entity.File = dto.File;
            entity.Title = dto.Title;
            entity.Description = dto.Description;
            entity.Category = dto.Category;
            entity.Group = dto.Group;
            entity.Order = dto.Order;
            entity.Module = dto.Module;
            entity.FileName = dto.FileName;
            entity.MimeType = dto.MimeType;
            return entity;
        }

        public static Document CopyTo(this DocumentDto dto)
        {
            var entity = new Document();
            return dto.CopyTo(entity);
        }

        #endregion

        #region JobPrevDataDto Mappers

        public static JobPrevDataDto AsDto(this JobPrevData entity)
        {
            var dto = new JobPrevDataDto
            {
                ClosingDate = entity.ClosingDate,
                NumOpenings = entity.NumOpenings,
                Locations = entity.Locations,
                Description = entity.Description,
                JobRequirements = entity.JobRequirements,
                JobDetails = entity.JobDetails,
                JobSalaryBenefits = entity.JobSalaryBenefits,
                RecruitmentInformation = entity.RecruitmentInformation,
                ForeignLabourCertificationDetails = entity.ForeignLabourCertificationDetails,
                CourtOrderedAffirmativeActionDetails = entity.CourtOrderedAffirmativeActionDetails,
                FederalContractorDetails = entity.FederalContractorDetails,
                JobId = entity.JobId,
                Id = entity.Id
            };
            return dto;
        }

        public static JobPrevData CopyTo(this JobPrevDataDto dto, JobPrevData entity)
        {
            entity.ClosingDate = dto.ClosingDate;
            entity.NumOpenings = dto.NumOpenings;
            entity.Locations = dto.Locations;
            entity.Description = dto.Description;
            entity.JobRequirements = dto.JobRequirements;
            entity.JobDetails = dto.JobDetails;
            entity.JobSalaryBenefits = dto.JobSalaryBenefits;
            entity.RecruitmentInformation = dto.RecruitmentInformation;
            entity.ForeignLabourCertificationDetails = dto.ForeignLabourCertificationDetails;
            entity.CourtOrderedAffirmativeActionDetails = dto.CourtOrderedAffirmativeActionDetails;
            entity.FederalContractorDetails = dto.FederalContractorDetails;
            entity.JobId = dto.JobId;
            return entity;
        }

        public static JobPrevData CopyTo(this JobPrevDataDto dto)
        {
            var entity = new JobPrevData();
            return dto.CopyTo(entity);
        }

        #endregion

        #region ReferralDto Mappers

        public static ReferralDto AsDto(this Referral entity)
        {
            var dto = new ReferralDto
            {
                PostingId = entity.PostingId,
                PersonId = entity.PersonId,
                UserId = entity.UserId,
                ActionerId = entity.ActionerId,
                ReferralDate = entity.ReferralDate,
                ActionTypeId = entity.ActionTypeId,
                ReferralScore = entity.ReferralScore,
                Id = entity.Id
            };
            return dto;
        }

        public static Referral CopyTo(this ReferralDto dto, Referral entity)
        {
            entity.PostingId = dto.PostingId;
            entity.PersonId = dto.PersonId;
            entity.UserId = dto.UserId;
            entity.ActionerId = dto.ActionerId;
            entity.ReferralDate = dto.ReferralDate;
            entity.ActionTypeId = dto.ActionTypeId;
            entity.ReferralScore = dto.ReferralScore;
            return entity;
        }

        public static Referral CopyTo(this ReferralDto dto)
        {
            var entity = new Referral();
            return dto.CopyTo(entity);
        }

        #endregion

        #region CandidateBasicViewDto Mappers

        public static CandidateBasicViewDto AsDto(this CandidateBasicView entity)
        {
            var dto = new CandidateBasicViewDto
            {
                FirstName = entity.FirstName,
                LastName = entity.LastName,
                UserId = entity.UserId,
                Enabled = entity.Enabled,
                LastLoggedInOn = entity.LastLoggedInOn,
                CreatedOn = entity.CreatedOn,
                ClientId = entity.ClientId,
                EmailAddress = entity.EmailAddress,
                Id = entity.Id
            };
            return dto;
        }

        public static CandidateBasicView CopyTo(this CandidateBasicViewDto dto, CandidateBasicView entity)
        {
            entity.FirstName = dto.FirstName;
            entity.LastName = dto.LastName;
            entity.UserId = dto.UserId;
            entity.Enabled = dto.Enabled;
            entity.LastLoggedInOn = dto.LastLoggedInOn;
            entity.ClientId = dto.ClientId;
            entity.EmailAddress = dto.EmailAddress;
            return entity;
        }

        public static CandidateBasicView CopyTo(this CandidateBasicViewDto dto)
        {
            var entity = new CandidateBasicView();
            return dto.CopyTo(entity);
        }

        #endregion

        #region PersonActivityUpdateSettingsDto Mappers

        public static PersonActivityUpdateSettingsDto AsDto(this PersonActivityUpdateSettings entity)
        {
            var dto = new PersonActivityUpdateSettingsDto
            {
                PersonId = entity.PersonId,

                BackdateMyEntriesJSActionMenuMaxDays = entity.BackdateMyEntriesJSActionMenuMaxDays,
                BackdateMyEntriesJSActivityLogMaxDays = entity.BackdateMyEntriesJSActivityLogMaxDays,
                BackdateAnyEntryJSActivityLogMaxDays = entity.BackdateAnyEntryJSActivityLogMaxDays,
                DeleteAnyEntryJSActivityLogMaxDays = entity.DeleteAnyEntryJSActivityLogMaxDays,
                DeleteMyEntriesJSActivityLogMaxDays = entity.DeleteMyEntriesJSActivityLogMaxDays,

                BackdateMyEntriesHMActionMenuMaxDays = entity.BackdateMyEntriesHMActionMenuMaxDays,
                BackdateAnyEntryHMActivityLogMaxDays = entity.BackdateAnyEntryHMActivityLogMaxDays,
                BackdateMyEntriesHMActivityLogMaxDays = entity.BackdateMyEntriesHMActivityLogMaxDays,
                DeleteAnyEntryHMActivityLogMaxDays = entity.DeleteAnyEntryHMActivityLogMaxDays,
                DeleteMyEntriesHMActivityLogMaxDays = entity.DeleteMyEntriesHMActivityLogMaxDays
            };

            return dto;
        }

        public static PersonActivityUpdateSettings CopyTo(this PersonActivityUpdateSettingsDto dto, PersonActivityUpdateSettings entity)
        {
            entity.PersonId = dto.PersonId;

            entity.BackdateMyEntriesJSActionMenuMaxDays = dto.BackdateMyEntriesJSActionMenuMaxDays;
            entity.BackdateMyEntriesJSActivityLogMaxDays = dto.BackdateMyEntriesJSActivityLogMaxDays;
            entity.BackdateAnyEntryJSActivityLogMaxDays = dto.BackdateAnyEntryJSActivityLogMaxDays;
            entity.DeleteAnyEntryJSActivityLogMaxDays = dto.DeleteAnyEntryJSActivityLogMaxDays;
            entity.DeleteMyEntriesJSActivityLogMaxDays = dto.DeleteMyEntriesJSActivityLogMaxDays;

            entity.BackdateMyEntriesHMActionMenuMaxDays = dto.BackdateMyEntriesHMActionMenuMaxDays;
            entity.BackdateAnyEntryHMActivityLogMaxDays = dto.BackdateAnyEntryHMActivityLogMaxDays;
            entity.BackdateMyEntriesHMActivityLogMaxDays = dto.BackdateMyEntriesHMActivityLogMaxDays;
            entity.DeleteAnyEntryHMActivityLogMaxDays = dto.DeleteAnyEntryHMActivityLogMaxDays;
            entity.DeleteMyEntriesHMActivityLogMaxDays = dto.DeleteMyEntriesHMActivityLogMaxDays;

            return entity;
        }

        public static PersonActivityUpdateSettings CopyTo(this PersonActivityUpdateSettingsDto dto)
        {
            var entity = new PersonActivityUpdateSettings();
            return dto.CopyTo(entity);
        }
        #endregion

        #region BackdatedActionEventsDto Mappers
        public static BackdatedActionEventsDto AsDto(this BackdatedActionEvents entity)
        {
            var dto = new BackdatedActionEventsDto
            {
                Id = entity.Id,
                ActionId = entity.ActionId,
                SessionId = entity.SessionId,
                RequestId = entity.RequestId,
                UserId = entity.UserId,
                ActionedOn = entity.ActionedOn,
                EntityId = entity.EntityId,
                EntityIdAdditional01 = entity.EntityIdAdditional01,
                EntityIdAdditional02 = entity.EntityIdAdditional02,
                AdditionalDetails = entity.AdditionalDetails,
                EntityTypeId = entity.EntityTypeId,
                ActionTypeId = entity.ActionTypeId,
                CreatedOn = entity.CreatedOn,
                ActionBackdatedOn = entity.ActionBackdatedOn
            };

            return dto;
        }

        public static BackdatedActionEvents CopyTo(this BackdatedActionEventsDto dto, BackdatedActionEvents entity)
        {
            entity.Id = dto.Id;
            entity.ActionId = dto.ActionId;
            entity.SessionId = dto.SessionId;
            entity.RequestId = dto.RequestId;
            entity.UserId = dto.UserId;
            entity.ActionedOn = dto.ActionedOn;
            entity.EntityId = dto.EntityId;
            entity.EntityIdAdditional01 = dto.EntityIdAdditional01;
            entity.EntityIdAdditional02 = dto.EntityIdAdditional02;
            entity.AdditionalDetails = dto.AdditionalDetails;
            entity.EntityTypeId = dto.EntityTypeId;
            entity.ActionTypeId = dto.ActionTypeId;
            entity.CreatedOn = dto.CreatedOn;
            entity.ActionBackdatedOn = dto.ActionBackdatedOn;

            return entity;
        }

        public static BackdatedActionEvents CopyTo(this BackdatedActionEventsDto dto)
        {
            var entity = new BackdatedActionEvents();
            return dto.CopyTo(entity);
        }
        #endregion
    }
}
