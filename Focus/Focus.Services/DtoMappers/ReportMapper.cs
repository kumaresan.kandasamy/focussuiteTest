﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using Focus.Core.DataTransferObjects.Report;
using Focus.Data.Report.Entities;

#endregion

namespace Focus.Services.DtoMappers
{
  public static class ReportMappers
  {
    #region JobOrderActionDto Mappers

    public static JobOrderActionDto AsDto(this JobOrderAction entity)
    {
      var dto = new JobOrderActionDto
      {
        ActionDate = entity.ActionDate,
        StaffReferrals = entity.StaffReferrals,
        SelfReferrals = entity.SelfReferrals,
        EmployerInvitationsSent = entity.EmployerInvitationsSent,
        ApplicantsInterviewed = entity.ApplicantsInterviewed,
        ApplicantsFailedToShow = entity.ApplicantsFailedToShow,
        ApplicantsDeniedInterviews = entity.ApplicantsDeniedInterviews,
        ApplicantsHired = entity.ApplicantsHired,
        JobOffersRefused = entity.JobOffersRefused,
        ApplicantsDidNotApply = entity.ApplicantsDidNotApply,
        InvitedJobSeekerViewed = entity.InvitedJobSeekerViewed,
        InvitedJobSeekerClicked = entity.InvitedJobSeekerClicked,
        ReferralsRequested = entity.ReferralsRequested,
        ApplicantsNotHired = entity.ApplicantsNotHired,
        ApplicantsNotYetPlaced = entity.ApplicantsNotYetPlaced,
        FailedToRespondToInvitation = entity.FailedToRespondToInvitation,
        FailedToReportToJob = entity.FailedToReportToJob,
        FoundJobFromOtherSource = entity.FoundJobFromOtherSource,
        JobAlreadyFilled = entity.JobAlreadyFilled,
        NewApplicant = entity.NewApplicant,
        NotQualified = entity.NotQualified,
        ApplicantRecommended = entity.ApplicantRecommended,
        RefusedReferral = entity.RefusedReferral,
        UnderConsideration = entity.UnderConsideration,
        JobOrderId = entity.JobOrderId,
        Id = entity.Id
      };
      return dto;
    }

    public static JobOrderAction CopyTo(this JobOrderActionDto dto, JobOrderAction entity)
    {
      entity.ActionDate = dto.ActionDate;
      entity.StaffReferrals = dto.StaffReferrals;
      entity.SelfReferrals = dto.SelfReferrals;
      entity.EmployerInvitationsSent = dto.EmployerInvitationsSent;
      entity.ApplicantsInterviewed = dto.ApplicantsInterviewed;
      entity.ApplicantsFailedToShow = dto.ApplicantsFailedToShow;
      entity.ApplicantsDeniedInterviews = dto.ApplicantsDeniedInterviews;
      entity.ApplicantsHired = dto.ApplicantsHired;
      entity.JobOffersRefused = dto.JobOffersRefused;
      entity.ApplicantsDidNotApply = dto.ApplicantsDidNotApply;
      entity.InvitedJobSeekerViewed = dto.InvitedJobSeekerViewed;
      entity.InvitedJobSeekerClicked = dto.InvitedJobSeekerClicked;
      entity.ReferralsRequested = dto.ReferralsRequested;
      entity.ApplicantsNotHired = dto.ApplicantsNotHired;
      entity.ApplicantsNotYetPlaced = dto.ApplicantsNotYetPlaced;
      entity.FailedToRespondToInvitation = dto.FailedToRespondToInvitation;
      entity.FailedToReportToJob = dto.FailedToReportToJob;
      entity.FoundJobFromOtherSource = dto.FoundJobFromOtherSource;
      entity.JobAlreadyFilled = dto.JobAlreadyFilled;
      entity.NewApplicant = dto.NewApplicant;
      entity.NotQualified = dto.NotQualified;
      entity.ApplicantRecommended = dto.ApplicantRecommended;
      entity.RefusedReferral = dto.RefusedReferral;
      entity.UnderConsideration = dto.UnderConsideration;
      entity.JobOrderId = dto.JobOrderId;
      return entity;
    }

    public static JobOrderAction CopyTo(this JobOrderActionDto dto)
    {
      var entity = new JobOrderAction();
      return dto.CopyTo(entity);
    }

    #endregion

    #region JobSeekerActionDto Mappers

    public static JobSeekerActionDto AsDto(this JobSeekerAction entity)
    {
      var dto = new JobSeekerActionDto
      {
        ActionDate = entity.ActionDate,
        PostingsViewed = entity.PostingsViewed,
        Logins = entity.Logins,
        ReferralRequests = entity.ReferralRequests,
        SelfReferrals = entity.SelfReferrals,
        StaffReferrals = entity.StaffReferrals,
        NotesAdded = entity.NotesAdded,
        AddedToLists = entity.AddedToLists,
        ActivitiesAssigned = entity.ActivitiesAssigned,
        StaffAssignments = entity.StaffAssignments,
        EmailsSent = entity.EmailsSent,
        FollowUpIssues = entity.FollowUpIssues,
        IssuesResolved = entity.IssuesResolved,
        Hired = entity.Hired,
        NotHired = entity.NotHired,
        FailedToApply = entity.FailedToApply,
        FailedToReportToInterview = entity.FailedToReportToInterview,
        InterviewDenied = entity.InterviewDenied,
        InterviewScheduled = entity.InterviewScheduled,
        NewApplicant = entity.NewApplicant,
        Recommended = entity.Recommended,
        RefusedOffer = entity.RefusedOffer,
        UnderConsideration = entity.UnderConsideration,
        UsedOnlineResumeHelp = entity.UsedOnlineResumeHelp,
        SavedJobAlerts = entity.SavedJobAlerts,
        TargetingHighGrowthSectors = entity.TargetingHighGrowthSectors,
        SelfReferralsExternal = entity.SelfReferralsExternal,
        StaffReferralsExternal = entity.StaffReferralsExternal,
        ReferralsApproved = entity.ReferralsApproved,
        ActivityServices = entity.ActivityServices,
        FindJobsForSeeker = entity.FindJobsForSeeker,
        FailedToReportToJob = entity.FailedToReportToJob,
        FailedToRespondToInvitation = entity.FailedToRespondToInvitation,
        FoundJobFromOtherSource = entity.FoundJobFromOtherSource,
        JobAlreadyFilled = entity.JobAlreadyFilled,
        NotQualified = entity.NotQualified,
        NotYetPlaced = entity.NotYetPlaced,
        RefusedReferral = entity.RefusedReferral,
        SurveySatisfied = entity.SurveySatisfied,
        SurveyDissatisfied = entity.SurveyDissatisfied,
        SurveyNoUnexpectedMatches = entity.SurveyNoUnexpectedMatches,
        SurveyUnexpectedMatches = entity.SurveyUnexpectedMatches,
        SurveyReceivedInvitations = entity.SurveyReceivedInvitations,
        SurveyDidNotReceiveInvitations = entity.SurveyDidNotReceiveInvitations,
        SurveyVerySatisfied = entity.SurveyVerySatisfied,
        SurveyVeryDissatisfied = entity.SurveyVeryDissatisfied,
        JobSeekerId = entity.JobSeekerId,
        Id = entity.Id
      };
      return dto;
    }

    public static JobSeekerAction CopyTo(this JobSeekerActionDto dto, JobSeekerAction entity)
    {
      entity.ActionDate = dto.ActionDate;
      entity.PostingsViewed = dto.PostingsViewed;
      entity.Logins = dto.Logins;
      entity.ReferralRequests = dto.ReferralRequests;
      entity.SelfReferrals = dto.SelfReferrals;
      entity.StaffReferrals = dto.StaffReferrals;
      entity.NotesAdded = dto.NotesAdded;
      entity.AddedToLists = dto.AddedToLists;
      entity.ActivitiesAssigned = dto.ActivitiesAssigned;
      entity.StaffAssignments = dto.StaffAssignments;
      entity.EmailsSent = dto.EmailsSent;
      entity.FollowUpIssues = dto.FollowUpIssues;
      entity.IssuesResolved = dto.IssuesResolved;
      entity.Hired = dto.Hired;
      entity.NotHired = dto.NotHired;
      entity.FailedToApply = dto.FailedToApply;
      entity.FailedToReportToInterview = dto.FailedToReportToInterview;
      entity.InterviewDenied = dto.InterviewDenied;
      entity.InterviewScheduled = dto.InterviewScheduled;
      entity.NewApplicant = dto.NewApplicant;
      entity.Recommended = dto.Recommended;
      entity.RefusedOffer = dto.RefusedOffer;
      entity.UnderConsideration = dto.UnderConsideration;
      entity.UsedOnlineResumeHelp = dto.UsedOnlineResumeHelp;
      entity.SavedJobAlerts = dto.SavedJobAlerts;
      entity.TargetingHighGrowthSectors = dto.TargetingHighGrowthSectors;
      entity.SelfReferralsExternal = dto.SelfReferralsExternal;
      entity.StaffReferralsExternal = dto.StaffReferralsExternal;
      entity.ReferralsApproved = dto.ReferralsApproved;
      entity.ActivityServices = dto.ActivityServices;
      entity.FindJobsForSeeker = dto.FindJobsForSeeker;
      entity.FailedToReportToJob = dto.FailedToReportToJob;
      entity.FailedToRespondToInvitation = dto.FailedToRespondToInvitation;
      entity.FoundJobFromOtherSource = dto.FoundJobFromOtherSource;
      entity.JobAlreadyFilled = dto.JobAlreadyFilled;
      entity.NotQualified = dto.NotQualified;
      entity.NotYetPlaced = dto.NotYetPlaced;
      entity.RefusedReferral = dto.RefusedReferral;
      entity.SurveySatisfied = dto.SurveySatisfied;
      entity.SurveyDissatisfied = dto.SurveyDissatisfied;
      entity.SurveyNoUnexpectedMatches = dto.SurveyNoUnexpectedMatches;
      entity.SurveyUnexpectedMatches = dto.SurveyUnexpectedMatches;
      entity.SurveyReceivedInvitations = dto.SurveyReceivedInvitations;
      entity.SurveyDidNotReceiveInvitations = dto.SurveyDidNotReceiveInvitations;
      entity.SurveyVerySatisfied = dto.SurveyVerySatisfied;
      entity.SurveyVeryDissatisfied = dto.SurveyVeryDissatisfied;
      entity.JobSeekerId = dto.JobSeekerId;
      return entity;
    }

    public static JobSeekerAction CopyTo(this JobSeekerActionDto dto)
    {
      var entity = new JobSeekerAction();
      return dto.CopyTo(entity);
    }

    #endregion

    #region SavedReportDto Mappers

    public static SavedReportDto AsDto(this SavedReport entity)
    {
      var dto = new SavedReportDto
      {
        ReportType = entity.ReportType,
        Criteria = entity.Criteria,
        UserId = entity.UserId,
        Name = entity.Name,
        ReportDate = entity.ReportDate,
        DisplayOnDashboard = entity.DisplayOnDashboard,
        IsSessionReport = entity.IsSessionReport,
        ReportDisplayType = entity.ReportDisplayType,
        Id = entity.Id
      };
      return dto;
    }

    public static SavedReport CopyTo(this SavedReportDto dto, SavedReport entity)
    {
      entity.ReportType = dto.ReportType;
      entity.Criteria = dto.Criteria;
      entity.UserId = dto.UserId;
      entity.Name = dto.Name;
      entity.ReportDate = dto.ReportDate;
      entity.DisplayOnDashboard = dto.DisplayOnDashboard;
      entity.IsSessionReport = dto.IsSessionReport;
      entity.ReportDisplayType = dto.ReportDisplayType;
      return entity;
    }

    public static SavedReport CopyTo(this SavedReportDto dto)
    {
      var entity = new SavedReport();
      return dto.CopyTo(entity);
    }

    #endregion

    #region EmployerActionDto Mappers

    public static EmployerActionDto AsDto(this EmployerAction entity)
    {
      var dto = new EmployerActionDto
      {
        ActionDate = entity.ActionDate,
        JobOrdersEdited = entity.JobOrdersEdited,
        JobSeekersInterviewed = entity.JobSeekersInterviewed,
        JobSeekersHired = entity.JobSeekersHired,
        JobOrdersCreated = entity.JobOrdersCreated,
        JobOrdersPosted = entity.JobOrdersPosted,
        JobOrdersPutOnHold = entity.JobOrdersPutOnHold,
        JobOrdersRefreshed = entity.JobOrdersRefreshed,
        JobOrdersClosed = entity.JobOrdersClosed,
        InvitationsSent = entity.InvitationsSent,
        JobSeekersNotHired = entity.JobSeekersNotHired,
        SelfReferrals = entity.SelfReferrals,
        StaffReferrals = entity.StaffReferrals,
        EmployerId = entity.EmployerId,
        Id = entity.Id
      };
      return dto;
    }

    public static EmployerAction CopyTo(this EmployerActionDto dto, EmployerAction entity)
    {
      entity.ActionDate = dto.ActionDate;
      entity.JobOrdersEdited = dto.JobOrdersEdited;
      entity.JobSeekersInterviewed = dto.JobSeekersInterviewed;
      entity.JobSeekersHired = dto.JobSeekersHired;
      entity.JobOrdersCreated = dto.JobOrdersCreated;
      entity.JobOrdersPosted = dto.JobOrdersPosted;
      entity.JobOrdersPutOnHold = dto.JobOrdersPutOnHold;
      entity.JobOrdersRefreshed = dto.JobOrdersRefreshed;
      entity.JobOrdersClosed = dto.JobOrdersClosed;
      entity.InvitationsSent = dto.InvitationsSent;
      entity.JobSeekersNotHired = dto.JobSeekersNotHired;
      entity.SelfReferrals = dto.SelfReferrals;
      entity.StaffReferrals = dto.StaffReferrals;
      entity.EmployerId = dto.EmployerId;
      return entity;
    }

    public static EmployerAction CopyTo(this EmployerActionDto dto)
    {
      var entity = new EmployerAction();
      return dto.CopyTo(entity);
    }

    #endregion
  }
}
