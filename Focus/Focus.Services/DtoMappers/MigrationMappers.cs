﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using Focus.Core.DataTransferObjects.FocusMigration;
using Focus.Data.Migration.Entities;

#endregion

namespace Focus.Services.DtoMappers
{
  public static class MigrationMappers
  {

    #region EmployerRequestDto Mappers

    public static EmployerRequestDto AsDto(this EmployerRequest entity)
    {
      var dto = new EmployerRequestDto
      {
        SourceId = entity.SourceId,
        FocusId = entity.FocusId,
        Request = entity.Request,
        Status = entity.Status,
        ExternalId = entity.ExternalId,
        Message = entity.Message,
        RecordType = entity.RecordType,
        Id = entity.Id
      };
      return dto;
    }

    public static EmployerRequest CopyTo(this EmployerRequestDto dto, EmployerRequest entity)
    {
      entity.SourceId = dto.SourceId;
      entity.FocusId = dto.FocusId;
      entity.Request = dto.Request;
      entity.Status = dto.Status;
      entity.ExternalId = dto.ExternalId;
      entity.Message = dto.Message;
      entity.RecordType = dto.RecordType;
      return entity;
    }

    public static EmployerRequest CopyTo(this EmployerRequestDto dto)
    {
      var entity = new EmployerRequest();
      return dto.CopyTo(entity);
    }

    #endregion

    #region JobOrderRequestDto Mappers

    public static JobOrderRequestDto AsDto(this JobOrderRequest entity)
    {
      var dto = new JobOrderRequestDto
      {
        SourceId = entity.SourceId,
        FocusId = entity.FocusId,
        Request = entity.Request,
        Status = entity.Status,
        ExternalId = entity.ExternalId,
        RecordType = entity.RecordType,
        Message = entity.Message,
        Id = entity.Id
      };
      return dto;
    }

    public static JobOrderRequest CopyTo(this JobOrderRequestDto dto, JobOrderRequest entity)
    {
      entity.SourceId = dto.SourceId;
      entity.FocusId = dto.FocusId;
      entity.Request = dto.Request;
      entity.Status = dto.Status;
      entity.ExternalId = dto.ExternalId;
      entity.RecordType = dto.RecordType;
      entity.Message = dto.Message;
      return entity;
    }

    public static JobOrderRequest CopyTo(this JobOrderRequestDto dto)
    {
      var entity = new JobOrderRequest();
      return dto.CopyTo(entity);
    }

    #endregion

    #region JobSeekerRequestDto Mappers

    public static JobSeekerRequestDto AsDto(this JobSeekerRequest entity)
    {
      var dto = new JobSeekerRequestDto
      {
        SourceId = entity.SourceId,
        FocusId = entity.FocusId,
        Request = entity.Request,
        Status = entity.Status,
        ExternalId = entity.ExternalId,
        Message = entity.Message,
        Id = entity.Id
      };
      return dto;
    }

    public static JobSeekerRequest CopyTo(this JobSeekerRequestDto dto, JobSeekerRequest entity)
    {
      entity.SourceId = dto.SourceId;
      entity.FocusId = dto.FocusId;
      entity.Request = dto.Request;
      entity.Status = dto.Status;
      entity.ExternalId = dto.ExternalId;
      entity.Message = dto.Message;
      return entity;
    }

    public static JobSeekerRequest CopyTo(this JobSeekerRequestDto dto)
    {
      var entity = new JobSeekerRequest();
      return dto.CopyTo(entity);
    }

    #endregion

    #region UserRequestDto Mappers

    public static UserRequestDto AsDto(this UserRequest entity)
    {
      var dto = new UserRequestDto
      {
        SourceId = entity.SourceId,
        FocusId = entity.FocusId,
        Request = entity.Request,
        Status = entity.Status,
        ExternalId = entity.ExternalId,
        Message = entity.Message,
        ProcessedBy = entity.ProcessedBy,
        RecordType = entity.RecordType,
        ParentExternalId = entity.ParentExternalId,
        IsUpdate = entity.IsUpdate,
        EmailRequired = entity.EmailRequired,
        SecondaryExternalId = entity.SecondaryExternalId,
        Id = entity.Id
      };
      return dto;
    }

    public static UserRequest CopyTo(this UserRequestDto dto, UserRequest entity)
    {
      entity.SourceId = dto.SourceId;
      entity.FocusId = dto.FocusId;
      entity.Request = dto.Request;
      entity.Status = dto.Status;
      entity.ExternalId = dto.ExternalId;
      entity.Message = dto.Message;
      entity.ProcessedBy = dto.ProcessedBy;
      entity.RecordType = dto.RecordType;
      entity.ParentExternalId = dto.ParentExternalId;
      entity.IsUpdate = dto.IsUpdate;
      entity.EmailRequired = dto.EmailRequired;
      entity.SecondaryExternalId = dto.SecondaryExternalId;
      return entity;
    }

    public static UserRequest CopyTo(this UserRequestDto dto)
    {
      var entity = new UserRequest();
      return dto.CopyTo(entity);
    }

    #endregion

  }
}