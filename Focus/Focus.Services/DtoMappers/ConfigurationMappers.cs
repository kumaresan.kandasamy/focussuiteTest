﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Data.Configuration.Entities;
using Focus.Data.Core.Entities;

#endregion

namespace Focus.Services.DtoMappers
{
	public static class ConfigurationMappers
	{
		#region CodeGroupDto Mappers

		public static CodeGroupDto AsDto(this CodeGroup entity)
		{
			var dto = new CodeGroupDto
			{
				Key = entity.Key,
				Id = entity.Id
			};
			return dto;
		}

		public static CodeGroup CopyTo(this CodeGroupDto dto, CodeGroup entity)
		{
			entity.Key = dto.Key;
			return entity;
		}

		public static CodeGroup CopyTo(this CodeGroupDto dto)
		{
			var entity = new CodeGroup();
			return dto.CopyTo(entity);
		}

		#endregion

		#region CodeGroupItemDto Mappers

		public static CodeGroupItemDto AsDto(this CodeGroupItem entity)
		{
			var dto = new CodeGroupItemDto
			{
				DisplayOrder = entity.DisplayOrder,
				CodeGroupId = entity.CodeGroupId,
				CodeItemId = entity.CodeItemId,
				Id = entity.Id
			};
			return dto;
		}

		public static CodeGroupItem CopyTo(this CodeGroupItemDto dto, CodeGroupItem entity)
		{
			entity.DisplayOrder = dto.DisplayOrder;
			entity.CodeGroupId = dto.CodeGroupId;
			entity.CodeItemId = dto.CodeItemId;
			return entity;
		}

		public static CodeGroupItem CopyTo(this CodeGroupItemDto dto)
		{
			var entity = new CodeGroupItem();
			return dto.CopyTo(entity);
		}

		#endregion

		#region CodeItemDto Mappers

		public static CodeItemDto AsDto(this CodeItem entity)
		{
			var dto = new CodeItemDto
			{
				Key = entity.Key,
				IsSystem = entity.IsSystem,
				ParentKey = entity.ParentKey,
				ExternalId = entity.ExternalId,
				CustomFilterId = entity.CustomFilterId,
				Id = entity.Id
			};
			return dto;
		}

		public static CodeItem CopyTo(this CodeItemDto dto, CodeItem entity)
		{
			entity.Key = dto.Key;
			entity.IsSystem = dto.IsSystem;
			entity.ParentKey = dto.ParentKey;
			entity.ExternalId = dto.ExternalId;
			entity.CustomFilterId = dto.CustomFilterId;
			return entity;
		}

		public static CodeItem CopyTo(this CodeItemDto dto)
		{
			var entity = new CodeItem();
			return dto.CopyTo(entity);
		}

		#endregion

		#region ConfigurationItemDto Mappers

		public static ConfigurationItemDto AsDto(this ConfigurationItem entity)
		{
			var dto = new ConfigurationItemDto
			{
				Key = entity.Key,
				Value = entity.Value,
				Id = entity.Id
			};
			return dto;
		}

		public static ConfigurationItem CopyTo(this ConfigurationItemDto dto, ConfigurationItem entity)
		{
			entity.Key = dto.Key;
			entity.Value = dto.Value;
			return entity;
		}

		public static ConfigurationItem CopyTo(this ConfigurationItemDto dto)
		{
			var entity = new ConfigurationItem();
			return dto.CopyTo(entity);
		}

		#endregion

		#region LocalisationDto Mappers

		public static LocalisationDto AsDto(this Localisation entity)
		{
			var dto = new LocalisationDto
			{
				Culture = entity.Culture,
				Id = entity.Id
			};
			return dto;
		}

		public static Localisation CopyTo(this LocalisationDto dto, Localisation entity)
		{
			entity.Culture = dto.Culture;
			return entity;
		}

		public static Localisation CopyTo(this LocalisationDto dto)
		{
			var entity = new Localisation();
			return dto.CopyTo(entity);
		}

		#endregion

		#region LocalisationItemDto Mappers

		public static LocalisationItemDto AsDto(this LocalisationItem entity)
		{
			var dto = new LocalisationItemDto
			{
				ContextKey = entity.ContextKey,
				Key = entity.Key,
				Value = entity.Value,
				Localised = entity.Localised,
				LocalisationId = entity.LocalisationId,
				Id = entity.Id
			};
			return dto;
		}

		public static LocalisationItem CopyTo(this LocalisationItemDto dto, LocalisationItem entity)
		{
			entity.ContextKey = dto.ContextKey;
			entity.Key = dto.Key;
			entity.Value = dto.Value;
			entity.Localised = dto.Localised;
			entity.LocalisationId = dto.LocalisationId;
			return entity;
		}

		public static LocalisationItem CopyTo(this LocalisationItemDto dto)
		{
			var entity = new LocalisationItem();
			return dto.CopyTo(entity);
		}

		#endregion

		#region EmailTemplateDto Mappers

		public static EmailTemplateDto AsDto(this EmailTemplate entity)
		{
			var dto = new EmailTemplateDto
			{
				EmailTemplateType = entity.EmailTemplateType,
				Subject = entity.Subject,
				Body = entity.Body,
				Salutation = entity.Salutation,
				Recipient = entity.Recipient,
				Id = entity.Id,
				SenderEmailType = entity.SenderEmailType,
				ClientSpecificEmailAddress = entity.ClientSpecificEmailAddress
			};
			return dto;
		}

		public static EmailTemplate CopyTo(this EmailTemplateDto dto, EmailTemplate entity)
		{
			entity.EmailTemplateType = dto.EmailTemplateType;
			entity.Subject = dto.Subject;
			entity.Body = dto.Body;
			entity.Salutation = dto.Salutation;
			entity.Recipient = dto.Recipient;
			entity.SenderEmailType = dto.SenderEmailType;
			entity.ClientSpecificEmailAddress = dto.ClientSpecificEmailAddress;
			return entity;
		}

		public static EmailTemplate CopyTo(this EmailTemplateDto dto)
		{
			var entity = new EmailTemplate();
			return dto.CopyTo(entity);
		}

		#endregion

		#region CertificateLicenceDto Mappers

		public static CertificateLicenceDto AsDto(this CertificateLicence entity)
		{
			var dto = new CertificateLicenceDto
			{
				Key = entity.Key,
				CertificateLicenseType = entity.CertificateLicenseType,
				OnetParentCode = entity.OnetParentCode,
				IsCertificate = entity.IsCertificate,
				IsLicence = entity.IsLicence,
				Id = entity.Id
			};
			return dto;
		}

		public static CertificateLicence CopyTo(this CertificateLicenceDto dto, CertificateLicence entity)
		{
			entity.Key = dto.Key;
			entity.CertificateLicenseType = dto.CertificateLicenseType;
			entity.OnetParentCode = dto.OnetParentCode;
			entity.IsCertificate = dto.IsCertificate;
			entity.IsLicence = dto.IsLicence;
			return entity;
		}

		public static CertificateLicence CopyTo(this CertificateLicenceDto dto)
		{
			var entity = new CertificateLicence();
			return dto.CopyTo(entity);
		}

		#endregion

		#region CertificateLicenceLocalisationItemDto Mappers

		public static CertificateLicenceLocalisationItemDto AsDto(this CertificateLicenceLocalisationItem entity)
		{
			var dto = new CertificateLicenceLocalisationItemDto
			{
				Key = entity.Key,
				Value = entity.Value,
				LocalisationId = entity.LocalisationId,
				Id = entity.Id
			};
			return dto;
		}

		public static CertificateLicenceLocalisationItem CopyTo(this CertificateLicenceLocalisationItemDto dto, CertificateLicenceLocalisationItem entity)
		{
			entity.Key = dto.Key;
			entity.Value = dto.Value;
			entity.LocalisationId = dto.LocalisationId;
			return entity;
		}

		public static CertificateLicenceLocalisationItem CopyTo(this CertificateLicenceLocalisationItemDto dto)
		{
			var entity = new CertificateLicenceLocalisationItem();
			return dto.CopyTo(entity);
		}

		#endregion

		#region ApplicationImageDto Mappers

		public static ApplicationImageDto AsDto(this ApplicationImage entity)
		{
			var dto = new ApplicationImageDto
			{
				Type = entity.Type,
				Image = entity.Image,
				Id = entity.Id
			};
			return dto;
		}

		public static ApplicationImage CopyTo(this ApplicationImageDto dto, ApplicationImage entity)
		{
			entity.Type = dto.Type;
			entity.Image = dto.Image;
			return entity;
		}

		public static ApplicationImage CopyTo(this ApplicationImageDto dto)
		{
			var entity = new ApplicationImage();
			return dto.CopyTo(entity);
		}

		public static Application CopyTo(this ApplicationDto dto)
		{
			var entity = new Application();
			return dto.CopyTo(entity);
		}

		#endregion

		#region LookupItemsViewDto Mappers

		public static LookupItemsViewDto AsDto(this LookupItemsView entity)
		{
			var dto = new LookupItemsViewDto
			{
				Key = entity.Key,
				LookupType = entity.LookupType,
				Value = entity.Value,
				DisplayOrder = entity.DisplayOrder,
				ParentId = entity.ParentId,
				ParentKey = entity.ParentKey,
				Culture = entity.Culture,
				ExternalId = entity.ExternalId,
				CustomFilterId = entity.CustomFilterId,
				Id = entity.Id
			};
			return dto;
		}

		public static LookupItemsView CopyTo(this LookupItemsViewDto dto, LookupItemsView entity)
		{
			entity.Key = dto.Key;
			entity.LookupType = dto.LookupType;
			entity.Value = dto.Value;
			entity.DisplayOrder = dto.DisplayOrder;
			entity.ParentId = dto.ParentId;
			entity.ParentKey = dto.ParentKey;
			entity.Culture = dto.Culture;
			entity.ExternalId = dto.ExternalId;
			entity.CustomFilterId = dto.CustomFilterId;
			return entity;
		}

		public static LookupItemsView CopyTo(this LookupItemsViewDto dto)
		{
			var entity = new LookupItemsView();
			return dto.CopyTo(entity);
		}

		#endregion

		#region CensorshipRuleDto Mappers

		public static CensorshipRuleDto AsDto(this CensorshipRule entity)
		{
			var dto = new CensorshipRuleDto
			{
				Type = entity.Type,
				Level = entity.Level,
				Phrase = entity.Phrase,
				WhiteList = entity.WhiteList,
				Id = entity.Id
			};
			return dto;
		}

		public static CensorshipRule CopyTo(this CensorshipRuleDto dto, CensorshipRule entity)
		{
			entity.Type = dto.Type;
			entity.Level = dto.Level;
			entity.Phrase = dto.Phrase;
			entity.WhiteList = dto.WhiteList;
			return entity;
		}

		public static CensorshipRule CopyTo(this CensorshipRuleDto dto)
		{
			var entity = new CensorshipRule();
			return dto.CopyTo(entity);
		}

		#endregion

		#region ActivityCategoryDto Mappers

		public static ActivityCategoryDto AsDto(this ActivityCategory entity)
		{
			var dto = new ActivityCategoryDto
			{
				LocalisationKey = entity.LocalisationKey,
				Name = entity.Name,
				EnableDisable = entity.Visible,
				Administrable = entity.Administrable,
				ActivityType = entity.ActivityType,
				Id = entity.Id
			};
			return dto;
		}

		public static ActivityCategory CopyTo(this ActivityCategoryDto dto, ActivityCategory entity)
		{
			entity.LocalisationKey = dto.LocalisationKey;
			entity.Name = dto.Name;
			entity.Visible = dto.EnableDisable;
			entity.Administrable = dto.Administrable;
			entity.ActivityType = dto.ActivityType;
			return entity;
		}

		public static ActivityCategory CopyTo(this ActivityCategoryDto dto)
		{
			var entity = new ActivityCategory();
			return dto.CopyTo(entity);
		}

		#endregion

		#region ActivityDto Mappers

		public static ActivityDto AsDto(this Activity entity)
		{
			var dto = new ActivityDto
			{
				LocalisationKey = entity.LocalisationKey,
				Name = entity.Name,
				EnableDisable = entity.EnableDisable,
				Administrable = entity.Administrable,
				SortOrder = entity.SortOrder,
				Used = entity.Used,
				ExternalId = entity.ExternalId,
				ActivityCategoryId = entity.ActivityCategoryId,
				Id = entity.Id
			};
			return dto;
		}

		public static Activity CopyTo(this ActivityDto dto, Activity entity)
		{
			entity.LocalisationKey = dto.LocalisationKey;
			entity.Name = dto.Name;
			entity.EnableDisable = dto.EnableDisable;
			entity.Administrable = dto.Administrable;
			entity.SortOrder = dto.SortOrder;
			entity.Used = dto.Used;
			entity.ExternalId = dto.ExternalId;
			entity.ActivityCategoryId = dto.ActivityCategoryId;
			return entity;
		}

		public static Activity CopyTo(this ActivityDto dto)
		{
			var entity = new Activity();
			return dto.CopyTo(entity);
		}

		#endregion

		#region ActivityViewDto Mappers

		public static ActivityViewDto AsDto(this ActivityView entity)
		{
			var dto = new ActivityViewDto
			{
				CategoryLocalisationKey = entity.CategoryLocalisationKey,
				CategoryName = entity.CategoryName,
				CategoryVisible = entity.CategoryVisible,
				CategoryAdministrable = entity.CategoryAdministrable,
				ActivityType = entity.ActivityType,
				ActivityLocalisationKey = entity.ActivityLocalisationKey,
				ActivityName = entity.ActivityName,
				ActivityVisible = entity.ActivityVisible,
				ActivityAdministrable = entity.ActivityAdministrable,
				SortOrder = entity.SortOrder,
				ActivityCategoryId = entity.ActivityCategoryId,
				ActivityExternalId = entity.ActivityExternalId,
				Id = entity.Id,
				Editable = entity.Editable
			};
			return dto;
		}

		public static ActivityView CopyTo(this ActivityViewDto dto, ActivityView entity)
		{
			entity.CategoryLocalisationKey = dto.CategoryLocalisationKey;
			entity.CategoryName = dto.CategoryName;
			entity.CategoryVisible = dto.CategoryVisible;
			entity.CategoryAdministrable = dto.CategoryAdministrable;
			entity.ActivityType = dto.ActivityType;
			entity.ActivityLocalisationKey = dto.ActivityLocalisationKey;
			entity.ActivityName = dto.ActivityName;
			entity.ActivityVisible = dto.ActivityVisible;
			entity.ActivityAdministrable = dto.ActivityAdministrable;
			entity.SortOrder = dto.SortOrder;
			entity.ActivityCategoryId = dto.ActivityCategoryId;
			entity.ActivityExternalId = dto.ActivityExternalId;
			entity.Editable = dto.Editable;
			return entity;
		}

		public static ActivityView CopyTo(this ActivityViewDto dto)
		{
			var entity = new ActivityView();
			return dto.CopyTo(entity);
		}

		#endregion

    #region ExternalLookUpItemDto Mappers

    public static ExternalLookUpItemDto AsDto(this ExternalLookUpItem entity)
    {
      var dto = new ExternalLookUpItemDto
      {
        InternalId = entity.InternalId,
        ExternalId = entity.ExternalId,
        ExternalLookUpType = entity.ExternalLookUpType,
        Id = entity.Id
      };
      return dto;
    }

    public static ExternalLookUpItem CopyTo(this ExternalLookUpItemDto dto, ExternalLookUpItem entity)
    {
      entity.InternalId = dto.InternalId;
      entity.ExternalId = dto.ExternalId;
      entity.ExternalLookUpType = dto.ExternalLookUpType;
      return entity;
    }

    public static ExternalLookUpItem CopyTo(this ExternalLookUpItemDto dto)
    {
      var entity = new ExternalLookUpItem();
      return dto.CopyTo(entity);
    }

    #endregion

		#region LaborInsightMappingDto Mappers

		public static LaborInsightMappingDto AsDto(this LaborInsightMapping entity)
		{
			var dto = new LaborInsightMappingDto
			{
				LaborInsightId = entity.LaborInsightId,
				LaborInsightValue = entity.LaborInsightValue,
				CodeGroupKey = entity.CodeGroupKey,
				CodeItemKey = entity.CodeItemKey,
				Id = entity.Id
			};
			return dto;
		}

		public static LaborInsightMapping CopyTo(this LaborInsightMappingDto dto, LaborInsightMapping entity)
		{
			entity.LaborInsightId = dto.LaborInsightId;
			entity.LaborInsightValue = dto.LaborInsightValue;
			entity.CodeGroupKey = dto.CodeGroupKey;
			entity.CodeItemKey = dto.CodeItemKey;
			return entity;
		}

		public static LaborInsightMapping CopyTo(this LaborInsightMappingDto dto)
		{
			var entity = new LaborInsightMapping();
			return dto.CopyTo(entity);
		}

		#endregion

		#region LaborInsightMappingViewDto Mappers

		public static LaborInsightMappingViewDto AsDto(this LaborInsightMappingView entity)
		{
			var dto = new LaborInsightMappingViewDto
			{
				CodeGroupItemId = entity.CodeGroupItemId,
				CodeGroupKey = entity.CodeGroupKey,
				LaborInsightId = entity.LaborInsightId,
				LaborInsightValue = entity.LaborInsightValue,
				Id = entity.Id
			};
			return dto;
		}

		public static LaborInsightMappingView CopyTo(this LaborInsightMappingViewDto dto, LaborInsightMappingView entity)
		{
			entity.CodeGroupItemId = dto.CodeGroupItemId;
			entity.CodeGroupKey = dto.CodeGroupKey;
			entity.LaborInsightId = dto.LaborInsightId;
			entity.LaborInsightValue = dto.LaborInsightValue;
			return entity;
		}

		public static LaborInsightMappingView CopyTo(this LaborInsightMappingViewDto dto)
		{
			var entity = new LaborInsightMappingView();
			return dto.CopyTo(entity);
		}

		#endregion
	}
}
