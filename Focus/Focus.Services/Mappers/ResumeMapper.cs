﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;

using Framework.Core;
using Framework.DataAccess;

using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Models.Career;
using Focus.Core.Views;
using Focus.Data.Core.Entities;
using Focus.Services.Core;
using Focus.Services.Core.Extensions;
using Job = Focus.Core.Models.Career.Job;

#endregion

namespace Focus.Services.Mappers
{
	public static class ResumeMapper
	{
		//private static XDocument _resumeDoc = new XDocument();
		//private static XElement _resumeElement;
		private const string BlankResumeXml = "<ResDoc><special/><resume><contact></contact><experience></experience><education></education></resume></ResDoc>";

		/// <summary>
		/// Returns the resume model.
		/// </summary>
		/// <param name="resumeXml">The resume XML.</param>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <param name="culture">The culture</param>
		/// <returns>
		/// The resume model
		/// </returns>
		public static ResumeModel ToResumeModel(this string resumeXml, IRuntimeContext runtimeContext, string culture)
		{
			XDocument resumeDoc;
			return resumeXml.ToResumeModel(runtimeContext, culture, out resumeDoc);
		}

		/// <summary>
		/// Returns the resume model.
		/// </summary>
		/// <param name="resumeXml">The resume XML.</param>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <param name="culture">The culture</param>
		/// <param name="resumeDoc">The resume document.</param>
		/// <returns>
		/// The resume model
		/// </returns>
		public static ResumeModel ToResumeModel(this string resumeXml, IRuntimeContext runtimeContext, string culture, out XDocument resumeDoc)
		{
			var resume = new ResumeDto { ResumeXml = resumeXml };
			var resumeModel = resume.ToResumeModel(runtimeContext, culture, out resumeDoc);
			return resumeModel;
		}

		/// <summary>
		/// Maps the resume dto to the resume model.
		/// </summary>
		/// <param name="resumeDto">The resume dto</param>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <param name="culture">The culture</param>
		/// <returns>
		/// The resume model
		/// </returns>
		/// <exception cref="System.Exception">No resume xml</exception>
		public static ResumeModel ToResumeModel(this ResumeDto resumeDto, IRuntimeContext runtimeContext, string culture)
		{
			XDocument resumeDoc;
			return resumeDto.ToResumeModel(runtimeContext, culture, out resumeDoc);
		}

		/// <summary>
		/// Maps the resume dto to the resume model.
		/// </summary>
		/// <param name="resumeDto">The resume dto</param>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <param name="culture">The culture</param>
		/// <param name="resumeDoc">The resume document.</param>
		/// <returns>
		/// The resume model
		/// </returns>
		/// <exception cref="System.Exception">No resume xml</exception>
		public static ResumeModel ToResumeModel(this ResumeDto resumeDto, IRuntimeContext runtimeContext, string culture, out XDocument resumeDoc)
		{
			if (resumeDto.ResumeXml.IsNullOrEmpty())
				throw new Exception("No resume xml");

			try
			{
				resumeDoc = XDocument.Parse(resumeDto.ResumeXml);
			}
			catch
			{
				throw new Exception("Could not parse posting xml");
			}

			var resumeElement = resumeDoc.XPathSelectElement("//resume");

			var resumeEnvelope = GetResumeEnvelope(resumeElement);
			var resumeBody = GetResumeBody(resumeElement, runtimeContext);

			if (resumeBody.Profile.DOB.IsNull())
				resumeBody.Profile.DOB = resumeDto.DateOfBirth;

			if (resumeBody.SeekerContactDetails.PostalAddress.CountyId.IsNull())
				resumeBody.SeekerContactDetails.PostalAddress.CountyId = resumeDto.CountyId;
            if (resumeBody.Profile.IsHomeless.IsNull())
                resumeBody.Profile.IsHomeless = resumeDto.HomelessNoShelter;
            if (resumeBody.Profile.IsStaying.IsNull())
                resumeBody.Profile.IsStaying = resumeDto.HomelessWithShelter;
            if (resumeBody.Profile.IsUnderYouth.IsNull())
                resumeBody.Profile.IsUnderYouth = resumeDto.RunawayYouth;
            if (resumeBody.Profile.IsExOffender.IsNull())
                resumeBody.Profile.IsExOffender = resumeDto.ExOffender;
            if (resumeBody.Profile.DisplacedHomemaker.IsNull())
                resumeBody.Profile.DisplacedHomemaker = resumeDto.DisplacedHomemaker;
            if (resumeBody.Profile.SingleParent.IsNull())
                resumeBody.Profile.SingleParent = resumeDto.SingleParent;
            if (resumeBody.Profile.LowIncomeStatus.IsNull())
                resumeBody.Profile.LowIncomeStatus = resumeDto.LowIncomeStatus;
            if (resumeBody.Profile.EstMonthlyIncome.IsNull())
                resumeBody.Profile.EstMonthlyIncome = resumeDto.EstMonthlyIncome;
            if (resumeBody.Profile.NoOfDependents.IsNull())
                resumeBody.Profile.NoOfDependents = resumeDto.NoOfDependents;
            if (resumeBody.Profile.PreferredLanguage.IsNull())
                resumeBody.Profile.PreferredLanguage = resumeDto.PreferredLanguage;
            if (resumeBody.Profile.LowLevelLiteracy.IsNull())
                resumeBody.Profile.LowLevelLiteracy = resumeDto.LowLeveLiteracy;
            if (resumeBody.Profile.LowLevelLiteracyIssues.NativeLanguage.IsNull())
                resumeBody.Profile.LowLevelLiteracyIssues.NativeLanguage = resumeDto.NativeLanguage;
            if (resumeBody.Profile.LowLevelLiteracyIssues.CommonLanguage.IsNull())
                resumeBody.Profile.LowLevelLiteracyIssues.CommonLanguage = resumeDto.CommonLanguage;
            if (resumeBody.Profile.CulturalBarriers.IsNull())
                resumeBody.Profile.CulturalBarriers = resumeDto.CulturalBarriers;

			var resumeSpecialInfo = GetResumeSpecialInfo(resumeElement, runtimeContext);

			return new ResumeModel { ResumeMetaInfo = resumeEnvelope, ResumeContent = resumeBody, Special = resumeSpecialInfo };
		}

		/// <summary>
		/// Maps the resume model to the resume dto.
		/// </summary>
		/// <param name="resumeModel">The resume model.</param>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <param name="culture">The culture</param>
		/// <param name="countryCode">The country code.</param>
		/// <returns></returns>
		/// <exception cref="System.Exception">Resume model is null</exception>
		public static ResumeDto ToResumeDto(this ResumeModel resumeModel, IRuntimeContext runtimeContext, string culture, string countryCode)
		{
			if (resumeModel.IsNull())
				throw new Exception("Resume model is null");

			var resumeDoc = XDocument.Parse(BlankResumeXml);

			var resdocElement = resumeDoc.XPathSelectElement("//ResDoc");
			if (resdocElement == null)
				throw new Exception("Resume Xml is invalid.  No ResDoc node.");

			var resumeElement = resumeDoc.XPathSelectElement("//resume");
			if (resumeElement == null)
				throw new Exception("Resume Xml is invalid.  No resume node.");

			var resumeDto = new ResumeDto();

			if (resumeModel.ResumeContent.SeekerContactDetails.IsNotNull())
			{
				#region Resume content seeker contact details

				resumeDto.CountryId = resumeModel.ResumeContent.SeekerContactDetails.PostalAddress.CountryId.IsNotNull()
					? resumeModel.ResumeContent.SeekerContactDetails.PostalAddress.CountryId
					: null;

				resumeDto.CountyId = resumeModel.ResumeContent.SeekerContactDetails.PostalAddress.CountyId;
                resumeDto.HomelessNoShelter = (bool)resumeModel.ResumeContent.Profile.IsHomeless;
                resumeDto.HomelessWithShelter = (bool)resumeModel.ResumeContent.Profile.IsStaying;
                resumeDto.RunawayYouth = (bool)resumeModel.ResumeContent.Profile.IsUnderYouth;
                resumeDto.ExOffender = (bool)resumeModel.ResumeContent.Profile.IsExOffender;


				resumeDto.AddressLine1 = resumeModel.ResumeContent.SeekerContactDetails.PostalAddress.Street1.IsNotNull()
					? resumeModel.ResumeContent.SeekerContactDetails.PostalAddress.Street1
					: null;

				if (resumeModel.ResumeContent.SeekerContactDetails.PhoneNumber.IsNotNull())
				{
					var homePhone = resumeModel.ResumeContent.SeekerContactDetails.PhoneNumber.FirstOrDefault(p => p.PhoneType == PhoneType.Home);
					if (homePhone != null) resumeDto.HomePhone = homePhone.PhoneNumber;
					var mobilePhone = resumeModel.ResumeContent.SeekerContactDetails.PhoneNumber.FirstOrDefault(p => p.PhoneType == PhoneType.Cell);
					if (mobilePhone != null) resumeDto.MobilePhone = mobilePhone.PhoneNumber;
					var workPhone = resumeModel.ResumeContent.SeekerContactDetails.PhoneNumber.FirstOrDefault(p => p.PhoneType == PhoneType.Work);
					if (workPhone != null) resumeDto.WorkPhone = workPhone.PhoneNumber;
					var faxNumber = resumeModel.ResumeContent.SeekerContactDetails.PhoneNumber.FirstOrDefault(p => p.PhoneType == PhoneType.Fax);
					if (faxNumber != null) resumeDto.FaxNumber = faxNumber.PhoneNumber;
				}

				#endregion
			}

			if (resumeModel.ResumeContent.Profile.IsNotNull())
			{
				#region Resume content profile

				resumeDto.DateOfBirth = resumeModel.ResumeContent.Profile.DOB.IsNotNull() ? resumeModel.ResumeContent.Profile.DOB : null;
				resumeDto.EmailAddress = resumeModel.ResumeContent.Profile.EmailAddress.IsNotNull() ? resumeModel.ResumeContent.Profile.EmailAddress : string.Empty;

				var userGiveName = resumeModel.ResumeContent.Profile.UserGivenName;
				if (userGiveName.IsNotNull())
				{
					resumeDto.FirstName = userGiveName.FirstName.IsNotNull() ? userGiveName.FirstName : string.Empty;
					resumeDto.LastName = userGiveName.LastName.IsNotNull() ? userGiveName.LastName : string.Empty;
					resumeDto.MiddleName = userGiveName.MiddleName.IsNotNull() ? userGiveName.MiddleName : null;
					resumeDto.SuffixId = userGiveName.SuffixId;
				}

				resumeDto.Gender = resumeModel.ResumeContent.Profile.Sex.IsNotNull() ? resumeModel.ResumeContent.Profile.Sex : null;
				resumeDto.IsVeteran = resumeModel.ResumeContent.Profile.Veteran.IsVeteran;
				resumeDto.VeteranPriorityServiceAlertsSubscription = resumeModel.ResumeContent.Profile.Veteran.VeteranPriorityServiceAlertsSubscription;
				resumeDto.MigrantSeasonalFarmWorker = resumeModel.ResumeContent.Profile.IsMSFW;
				resumeDto.IsUSCitizen = resumeModel.ResumeContent.Profile.IsUSCitizen;
				resumeDto.AlienRegistrationNumber = resumeModel.ResumeContent.Profile.AlienId;
				resumeDto.AlienRegExpiryDate = resumeModel.ResumeContent.Profile.AlienExpires;
				resumeDto.IsPermanentResident = resumeModel.ResumeContent.Profile.IsPermanentResident;

				if (resumeModel.ResumeContent.Profile.PostalAddress.IsNotNull())
				{
					resumeDto.StateId = resumeModel.ResumeContent.Profile.PostalAddress.StateId.IsNotNull() ? resumeModel.ResumeContent.Profile.PostalAddress.StateId : null;
					resumeDto.PostcodeZip = resumeModel.ResumeContent.Profile.PostalAddress.Zip.IsNotNull() ? resumeModel.ResumeContent.Profile.PostalAddress.Zip : string.Empty;
					resumeDto.TownCity = resumeModel.ResumeContent.Profile.PostalAddress.City.IsNotNull() ? resumeModel.ResumeContent.Profile.PostalAddress.City : string.Empty;
				}

				#endregion
			}

			if (resumeModel.ResumeMetaInfo.IsNotNull())
			{
				resumeDto.Id = resumeModel.ResumeMetaInfo.ResumeId.IsNotNull() ? resumeModel.ResumeMetaInfo.ResumeId : null;
				resumeDto.IsActive = resumeModel.ResumeMetaInfo.ResumeStatus == ResumeStatuses.Active;
				resumeDto.PrimaryOnet = resumeModel.ResumeMetaInfo.ResumeOnet.IsNotNull() ? resumeModel.ResumeMetaInfo.ResumeOnet : string.Empty;
				resumeDto.PrimaryROnet = resumeModel.ResumeMetaInfo.ResumeROnet.IsNotNull() ? resumeModel.ResumeMetaInfo.ResumeROnet : string.Empty;
				resumeDto.ResumeCompletionStatusId = resumeModel.ResumeMetaInfo.CompletionStatus.IsNotNull() ? resumeModel.ResumeMetaInfo.CompletionStatus : ResumeCompletionStatuses.None;
				resumeDto.ResumeName = resumeModel.ResumeMetaInfo.ResumeName.IsNotNull() ? resumeModel.ResumeMetaInfo.ResumeName : string.Empty;
				resumeDto.StatusId = resumeModel.ResumeMetaInfo.ResumeStatus.IsNotNull() ? resumeModel.ResumeMetaInfo.ResumeStatus : ResumeStatuses.Pending;
				if (resumeModel.ResumeMetaInfo.IsDefault != null)
					resumeDto.IsDefault = (bool)resumeModel.ResumeMetaInfo.IsDefault;
			}

			if (resumeModel.ResumeContent.Skills.IsNotNull())
			{
				var skills = resumeModel.ResumeContent.Skills.Skills;
				resumeDto.ResumeSkills = skills.IsNotNullOrEmpty()
					? string.Join(",", resumeModel.ResumeContent.Skills.Skills)
					: string.Empty;
				resumeDto.SkillsAlreadyCaptured = resumeModel.ResumeContent.Skills.SkillsAlreadyCaptured;

				resumeDto.SkillCount = skills.IsNotNullOrEmpty()
					? skills.Count
					: 0;
			}

			if (resumeModel.ResumeContent.EducationInfo.IsNotNull())
			{
				if (resumeModel.ResumeContent.EducationInfo.EducationLevel != null)
					resumeDto.HighestEducationLevel = (EducationLevel?)resumeModel.ResumeContent.EducationInfo.EducationLevel.Value;

				if (resumeModel.ResumeContent.EducationInfo.NCRCLevel.IsNotNullOrZero())
					resumeDto.NcrcLevelId = resumeModel.ResumeContent.EducationInfo.NCRCLevel.Value;
			}

			if (resumeModel.Special.IsNotNull() && resumeModel.Special.Preferences.IsNotNull())
			{
				if (resumeModel.Special.Preferences.IsResumeSearchable != null)
					resumeDto.IsSearchable = (bool)resumeModel.Special.Preferences.IsResumeSearchable;

				if (resumeModel.Special.Preferences.IsContactInfoVisible != null)
					resumeDto.IsContactInfoVisible = (bool)resumeModel.Special.Preferences.IsContactInfoVisible;
				else
					resumeDto.IsContactInfoVisible = resumeDto.IsSearchable;
			}
			else
			{
				resumeDto.IsContactInfoVisible = resumeDto.IsSearchable;
			}

			if (resumeModel.ResumeMetaInfo.IsNotNull())
			{
				#region Set Resume Meta Information in Resume Header

				resumeElement.RemoveElementsByXpath("header");
				var resumeHeader = resumeElement.GetOrCreateElement("header");
				if (resumeModel.ResumeMetaInfo.IsDefault.HasValue)
					resumeHeader.CreateElement("default", (bool)resumeModel.ResumeMetaInfo.IsDefault ? "1" : "0");
				if ((int)resumeModel.ResumeMetaInfo.ResumeCreationMethod > 0)
					resumeHeader.CreateElement("buildmethod", (int)resumeModel.ResumeMetaInfo.ResumeCreationMethod);
				if (resumeModel.ResumeMetaInfo.ResumeId.IsNotNull())
					resumeHeader.CreateElement("resumeid", resumeModel.ResumeMetaInfo.ResumeId);
				if (resumeModel.ResumeMetaInfo.ResumeName.IsNotNullOrEmpty())
					resumeHeader.CreateElement("resumename", resumeModel.ResumeMetaInfo.ResumeName);
				if (resumeModel.ResumeMetaInfo.ResumeOnet.IsNotNullOrEmpty())
					resumeHeader.CreateElement("onet", resumeModel.ResumeMetaInfo.ResumeOnet);
				if (resumeModel.ResumeMetaInfo.ResumeROnet.IsNotNullOrEmpty())
					resumeHeader.CreateElement("ronet", resumeModel.ResumeMetaInfo.ResumeROnet);
				resumeHeader.CreateElement("status", (int)resumeModel.ResumeMetaInfo.ResumeStatus);
				resumeHeader.CreateElement("completionstatus", (int)resumeModel.ResumeMetaInfo.CompletionStatus);
				resumeHeader.CreateElement("updatedon", DateTime.Now.ToString());

				#endregion
			}

			if (resumeModel.ResumeContent.IsNotNull())
			{
				#region Set the Contact

				var contactElement = resumeElement.GetOrCreateElement("contact", "contact");

				if (resumeModel.ResumeContent.SeekerContactDetails.IsNotNull())
				{
					contactElement.SetName(runtimeContext, ref resumeModel);
					contactElement.SetPhone(ref resumeModel);
					contactElement.SetEmail(ref resumeModel);
					contactElement.SetWebsite(ref resumeModel);
					contactElement.RemoveAttributes();
					contactElement.SetAddress(runtimeContext, ref resumeModel, countryCode);
				}

				#endregion

				#region Set the Statements

				var statementsElement = resumeElement.GetOrCreateElement("statements");

				var personalElement = statementsElement.GetOrCreateElement("personal");

				if (resumeModel.ResumeContent.IsNotNull() && resumeModel.ResumeContent.Profile.IsNotNull())
				{
					//personalElement.SetDateOfBirth(ref resumeModel);
					personalElement.SetGender(ref resumeModel);
					personalElement.SetEthnicityAndRace(runtimeContext, ref resumeModel);
					//personalElement.SetMilitaryStatus(ref resumeModel);
					personalElement.SetUSCitizen(runtimeContext, ref resumeModel);
					personalElement.SetMSFW(runtimeContext, resumeModel.ResumeContent.Profile);
					personalElement.SetDisabled(runtimeContext, ref resumeModel);
					personalElement.SetSelectiveService(ref resumeModel);
					personalElement.SetDateOfBirth(ref resumeModel);
                    personalElement.SetExOffender(ref resumeModel);
                    personalElement.SetHomeless(ref resumeModel);
                    personalElement.SetStaying(ref resumeModel);
                    personalElement.SetUnderYouth(ref resumeModel);
                    personalElement.SetLanguageCulture(runtimeContext, ref resumeModel);
                    personalElement.SetIncome(runtimeContext, ref resumeModel);
                    var profileElement = resumeModel.ResumeContent.Profile;
                    resumeDto.DisplacedHomemaker = resumeModel.ResumeContent.Profile.DisplacedHomemaker;
                    if (profileElement.SingleParent)
                        resumeDto.SingleParent = resumeModel.ResumeContent.Profile.SingleParent;
                    if (profileElement.LowLevelLiteracy)
                        resumeDto.LowLeveLiteracy = resumeModel.ResumeContent.Profile.LowLevelLiteracy;
                    if (profileElement.LowIncomeStatus)
                        resumeDto.LowIncomeStatus = resumeModel.ResumeContent.Profile.LowIncomeStatus;
                    if (profileElement.LowLevelLiteracyIssues.IsNotNull())
                    {
                        resumeDto.CommonLanguage = resumeModel.ResumeContent.Profile.LowLevelLiteracyIssues.CommonLanguage;
                        resumeDto.NativeLanguage = resumeModel.ResumeContent.Profile.LowLevelLiteracyIssues.NativeLanguage;
                    }
                    if (profileElement.PreferredLanguage.IsNotNullOrEmpty())
                        resumeDto.PreferredLanguage = !profileElement.PreferredLanguage.Equals("Not disclosed")?resumeModel.ResumeContent.Profile.PreferredLanguage:"Not disclosed";
                    if(profileElement.NoOfDependents.IsNotNullOrEmpty())
                        resumeDto.NoOfDependents = !profileElement.NoOfDependents.Equals("Not disclosed")?resumeModel.ResumeContent.Profile.NoOfDependents.ToString():"Not disclosed";
                    if(profileElement.EstMonthlyIncome.IsNotNullOrEmpty())
                        resumeDto.EstMonthlyIncome = !profileElement.EstMonthlyIncome.Equals("Not disclosed") ? resumeModel.ResumeContent.Profile.EstMonthlyIncome.ToString() : "Not disclosed";
                    if ((bool)profileElement.CulturalBarriers)
                        resumeDto.CulturalBarriers = resumeModel.ResumeContent.Profile.CulturalBarriers;

				}

				#endregion

				#region Set the Employment History

				resumeElement.SetEmploymentHistory(runtimeContext, ref resumeModel);

				#endregion

				#region  Set the Education History

				var educationElement = resumeElement.GetOrCreateElement("education");
				educationElement.SetEducationHistory(runtimeContext, ref resumeModel);

				#endregion

				#region  Set the Summary

				if (resumeModel.ResumeContent.SummaryInfo.IsNotNull())
				{
					if (string.IsNullOrEmpty(resumeModel.ResumeContent.SummaryInfo.Summary) && resumeElement.XPathSelectElement("summary") != null)
						resumeElement.RemoveElementsByXpath("summary");
					else
					{
						XElement summaryElement = resumeElement.GetOrCreateElement("summary").GetOrCreateElement("summary", "summary", resumeModel.ResumeContent.SummaryInfo.Summary);
						summaryElement.RemoveAttributes();
						summaryElement.CreateAttribute("include", (resumeModel.ResumeContent.SummaryInfo.IncludeSummary.HasValue && !(bool)resumeModel.ResumeContent.SummaryInfo.IncludeSummary) ? "0" : "1");
					}
				}

				#endregion

				var skillsElement = resumeElement.GetOrCreateElement("skills");
				var skillsRollupElement = resumeElement.GetOrCreateElement("skillrollup");

				if (resumeModel.ResumeContent.Skills.IsNotNull())
				{
					#region  Set the Certifications

					skillsElement.SetCertifications(runtimeContext, ref resumeModel);

					#endregion

					#region  Set the Skills

					//set the skill roll up skills
					skillsRollupElement.SetSkillRollUp(ref resumeModel);

					//skills such as language proficiency and internship
					skillsElement.SetSkills(ref resumeModel, runtimeContext);

					#endregion
				}

				if (resumeModel.ResumeContent.IsNotNull() && resumeModel.ResumeContent.Profile.IsNotNull())
				{
					#region  Set the Drivers License

					personalElement.SetDriversLicense(runtimeContext, ref resumeModel);

					#endregion

					#region  Set the Military Service

					personalElement.SetMilitaryService(runtimeContext, ref resumeModel);

					#endregion
				}

				#region  Set Additional Resume Sections

				if (resumeModel.ResumeContent.AdditionalResumeSections.IsNotNull() && resumeModel.ResumeContent.AdditionalResumeSections.Count > 0)
				{
					resdocElement.SetAdditionalSections(ref resumeModel);
				}

				#endregion
			}

			#region  Set the Preferences

			if (resumeModel.Special.IsNotNull())
			{
				var personalElement = resumeElement.GetOrCreateElement("statements").GetOrCreateElement("personal");
				personalElement.SetPreferences(runtimeContext, ref resumeModel);
			}

			#endregion

			#region Set Hidden Information

			if (resumeModel.Special.IsNotNull() && resumeModel.Special.HideInfo.IsNotNull())
			{
				var hidingElement = resumeElement.GetOrCreateElement("hide_options");
				hidingElement.SetHidingInformation(ref resumeModel);
			}

			#endregion

			#region Set Resume Display Format

			if (resumeModel.Special.IsNotNull())
			{
				if (resumeModel.Special.PreservedFormatId.IsNotNull())
					resdocElement.GetOrCreateElement("special").GetOrCreateElement("preserved_format", "preserved_format", resumeModel.Special.PreservedFormatId.GetPreservedFormatExternalId(runtimeContext));

				if (resumeModel.Special.PreservedOrder.IsNotNullOrEmpty())
					resdocElement.GetOrCreateElement("special").GetOrCreateElement("preserved_order", "preserved_order", resumeModel.Special.PreservedOrder);

				if (resumeModel.Special.Branding.IsNotNullOrEmpty())
					resdocElement.GetOrCreateElement("branding").GetOrCreateElement("branding", "branding", resumeModel.Special.Branding);
			}

			#endregion

			resumeDto.ResumeXml = resumeDoc.ToString();
			resumeDto.WordCount = runtimeContext.Helpers.Resume.GetNumberOfWords(resumeDto.ResumeXml);

			resumeDto.EducationCompletionDate = null;
			if (resumeModel.ResumeContent.EducationInfo.IsNotNull() && resumeModel.ResumeContent.EducationInfo.Schools.IsNotNullOrEmpty())
			{
				resumeModel.ResumeContent.EducationInfo.Schools.ForEach(school =>
				{
					var completionDate = school.CompletionDate ?? school.ExpectedCompletionDate;
					if (completionDate.HasValue && (!resumeDto.EducationCompletionDate.HasValue || completionDate.Value > resumeDto.EducationCompletionDate))
						resumeDto.EducationCompletionDate = completionDate;
				});
			}

			return resumeDto;
		}

		/// <summary>
		/// Updates the resume XML.
		/// </summary>
		/// <param name="resume">The resume.</param>
		/// <returns></returns>
		public static string UpdateResumeXml(this Resume resume)
		{
			var resumeDoc = (resume.ResumeXml.IsNotNullOrEmpty()) ? XDocument.Parse(resume.ResumeXml) : XDocument.Parse(BlankResumeXml);

			var resdocElement = resumeDoc.XPathSelectElement("//ResDoc");
			if (resdocElement == null)
				throw new Exception("Resume Xml is invalid.  No ResDoc node.");

			var resumeElement = resumeDoc.XPathSelectElement("//resume");
			if (resumeElement == null)
				throw new Exception("Resume Xml is invalid.  No resume node.");

			var resumeHeader = resumeElement.GetOrCreateElement("header");
			resumeHeader.GetOrCreateElement("resumeid", "resumeid", resume.Id);
			resumeHeader.GetOrCreateElement("default", "default", resume.IsDefault ? "1" : "0");

			return resumeDoc.ToString();
		}

		/// <summary>
		/// Gets the envelope section.
		/// </summary>
		/// <returns>Resume envelope</returns>
		private static ResumeEnvelope GetResumeEnvelope(XElement resumeElement)
		{
			#region Get resume envelope

			return new ResumeEnvelope
			{
                ResumeId = resumeElement.XPathAsNullableLong("//resume/header/resumeid"),
				CanonVersion = resumeElement.XPathAsString("//resume/header/canonversion"),
				CompletionStatus = AsEnum(resumeElement, "//resume/header/completionstatus", ResumeCompletionStatuses.Completed),
				IsDefault = resumeElement.XPathAsBoolean("//resume/header/default"),
				LastUpdated = resumeElement.XPathAsDateTime("//resume/header/updatedon"),
				ResumeName = resumeElement.XPathAsString("//resume/header/resumename"),
				ResumeOnet = resumeElement.XPathAsString("//resume/header/onet"),
				ResumeROnet = resumeElement.XPathAsString("//resume/header/ronet").Replace("S", "9"),
				ResumeStatus = AsEnum(resumeElement, "//resume/header/status", ResumeStatuses.Active),
				ResumeCreationMethod = AsEnum(resumeElement, "//resume/header/buildmethod", ResumeCreationMethod.BuildWizard),
			};

			#endregion
		}

		/// <summary>
		/// Gets the body section.
		/// </summary>
		/// <param name="resumeElement">The resume Xml element.</param>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <returns>
		/// Resume body
		/// </returns>
		private static ResumeBody GetResumeBody(XElement resumeElement, IRuntimeContext runtimeContext)
		{
			#region Get resume body

			return new ResumeBody
			{
				SeekerContactDetails = GetContactSection(resumeElement, runtimeContext),
				EducationInfo = GetEducationInfoSection(resumeElement, runtimeContext),
				Statements = GetStatementsSection(resumeElement),
				Profile = GetUserInfoSection(resumeElement, runtimeContext),
				Skills = GetSkillsSection(resumeElement, runtimeContext),
				Professional = GetProfessionalSection(resumeElement),
				SummaryInfo = GetSummarySection(resumeElement),
				References = GetReferencesSection(resumeElement),
				ExperienceInfo = GetExperienceSection(resumeElement, runtimeContext),
				AdditionalResumeSections = GetAdditionalResumeSections(resumeElement, runtimeContext),
			};

			#endregion
		}

		/// <summary>
		/// Gets the special info section.
		/// </summary>
		/// <param name="resumeElement">The resume Xml element.</param>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <returns>
		/// Resume special info
		/// </returns>
		private static ResumeSpecialInfo GetResumeSpecialInfo(XElement resumeElement, IRuntimeContext runtimeContext)
		{
			#region Get resume special info

			var hiddenSkills = resumeElement.GetElementValueAsString("//hide_options/hidden_skills", "");

			return new ResumeSpecialInfo
			{
				IsResumeExported = resumeElement.GetElementValueAsNullableBoolean("//_resumeElementstatus/@export"),
				Canonskills = resumeElement.XPathSelectElement("//skillrollup/canonskill").IsNull()
					? null
					: (from s in resumeElement.XPathSelectElements("//skillrollup/canonskill")
						select s.GetElementValueAsString("@name")).ToList(),
				Preferences = new Preferences
				{
					Salary = resumeElement.GetElementValueAsString("//preferences/sal").AsFloat().AsNull<float>(),
					SalaryFrequencyId = GetLookupId(runtimeContext, LookupTypes.Frequencies, resumeElement.GetElementValueAsString("//preferences/salary_unit_cd")),

					IsResumeSearchable = resumeElement.GetElementValueAsNullableBoolean("//preferences/resume_searchable"),
					BridgestoOppsInterest = resumeElement.GetElementValueAsNullableBoolean("//preferences/bridges_opp_interest"),
					IsContactInfoVisible = resumeElement.GetElementValueAsNullableBoolean("//preferences/contact_info_visible"),
					PostingsInterestedIn = (JobTypes)resumeElement.GetElementValueAsInt32("//preferences/postings_interested_in"),
					WillingToRelocate = resumeElement.GetElementValueAsNullableBoolean("//preferences/relocate"),

					IsContactByEmail = resumeElement.GetElementValueAsNullableBoolean("//preferences/email_flag"),
					IsContactAtPostalAddress = resumeElement.GetElementValueAsNullableBoolean("//preferences/postal_flag"),
					IsContactAtPriPhoneNumber = resumeElement.GetElementValueAsNullableBoolean("//preferences/phone_pri_flag"),
					IsContactAtSecPhoneNumber = resumeElement.GetElementValueAsNullableBoolean("//preferences/phone_sec_flag"),
					IsContactByFax = resumeElement.GetElementValueAsNullableBoolean("//preferences/fax_flag"),

					ShiftDetail = new Shift
					{
						ShiftTypeIds = GetShiftDetails(runtimeContext, resumeElement.XPathSelectElement("//resume/statements/personal/preferences/shift")),
						ShiftTypes = GetShiftTypes(resumeElement.XPathSelectElement("//resume/statements/personal/preferences/shift")),
						WorkWeekId = GetLookupId(runtimeContext, LookupTypes.WorkWeeks, resumeElement.GetElementValueAsString("//resume/statements/personal/preferences/shift/work_week")),
						WorkDurationId = GetLookupId(runtimeContext, LookupTypes.Durations, resumeElement.GetElementValueAsString("//resume/statements/personal/preferences/shift/work_type")),
						WorkOverTime = resumeElement.GetElementValueAsNullableBoolean("//resume/statements/personal/preferences/shift/work_over_time")
					},
					ZipPreference = resumeElement.XPathSelectElement("//preferences/desired_zips/desired_zip").IsNull()
						? null
						: (from z in resumeElement.XPathSelectElements("//preferences/desired_zips/desired_zip")
							where string.IsNullOrEmpty(z.GetElementValueAsString("//zip")) == false
							select new DesiredZip
							{
								Zip = z.GetElementValueAsString("zip"),
								RadiusId = GetLookupId(runtimeContext, LookupTypes.Radiuses, z.GetElementValueAsString("radius")),
								Radius = z.GetElementValueAsString("radius").IsNullOrEmpty() ? (int?)null : Convert.ToInt32(z.GetElementValueAsString("radius")),
								StateId = GetLookupId(runtimeContext, LookupTypes.States, z.GetElementValueAsString("statecode")),
								Choice = z.GetElementValueAsInt32("choice").AsNull<int>()
							}).ToList(),
					CityPreference = resumeElement.XPathSelectElement("//preferences/desired_cities/desired_city").IsNull()
						? null
						: (from c in resumeElement.XPathSelectElements("//preferences/desired_cities/desired_city")
							where string.IsNullOrEmpty(c.GetElementValueAsString("//city")) == false
							select new DesiredCity
							{
								CityName = c.GetElementValueAsString("city"),
								StateId = GetLookupId(runtimeContext, LookupTypes.States, c.GetElementValueAsString("state")),
								StateName = c.GetElementValueAsString("state_fullname"),
								Choice = c.GetElementValueAsInt32("choice").AsNull<int>()
							}).ToList(),
					StatePreference =
						resumeElement.XPathSelectElement("//preferences/desired_states/desired_state[@homestate!='1']").IsNull()
							? null
							: (from s in resumeElement.XPathSelectElements("//preferences/desired_states/desired_state[@homestate!='1']")
								where string.IsNullOrEmpty(s.GetElementValueAsString("//code")) == false
								select new DesiredState
								{
									StateCode = s.GetElementValueAsString("code"),
									StateName = s.GetElementValueAsString("value"),
									Choice = s.GetElementValueAsInt32("choice").AsNull<int>()
								}).ToList(),
					MSAPreference = resumeElement.XPathSelectElement("//preferences/desired_msas/desired_msa").IsNull()
						? null
						: (from m in resumeElement.XPathSelectElements("//preferences/desired_msas/desired_msa")
							where string.IsNullOrEmpty(m.GetElementValueAsString("//code")) == false
							select new DesiredMSA
							{
								MSAId = GetLookupId(runtimeContext, LookupTypes.StateMetropolitanStatisticalAreas, m.GetElementValueAsString("msa"), GetLookupId(runtimeContext, LookupTypes.States, m.GetElementValueAsString("code"))) ?? 0,
								StateId = GetLookupId(runtimeContext, LookupTypes.States, m.GetElementValueAsString("code")),
								StateName = m.GetElementValueAsString("value"),
								StateCode = m.GetElementValueAsString("code"),
								Choice = m.GetElementValueAsInt32("choice").AsNull<int>()
							}).ToList(),
					SearchInMyState = resumeElement.ElementExists("//preferences/desired_states/desired_state[@homestate='1']"),
					//HomeBasedJobPostings = resumeElement.GetElementValueAsNullableBoolean("//preferences/home_based_jobpostings")
					//(!string.IsNullOrEmpty(_resumeElement.GetElementValueAsString("//contact/address/state")) && _resumeElement.GetElementValueAsString("//contact/address/state") == _resumeElement.GetElementValueAsString("//preferences/desired_states/desired_state/code"))
				},
				HideInfo = new HideResumeInfo
				{
					HideDateRange = resumeElement.GetElementValueAsBoolean("//hide_options/hide_daterange"),
					HideEducationDates = resumeElement.GetElementValueAsBoolean("//hide_options/hide_eduDates"),
					HideMilitaryServiceDates = resumeElement.GetElementValueAsBoolean("//hide_options/hide_militaryserviceDates"),
					HideWorkDates = resumeElement.GetElementValueAsBoolean("//hide_options/hide_workDates"),
					HideContact = resumeElement.GetElementValueAsBoolean("//hide_options/hide_contact"),
					HideName = resumeElement.GetElementValueAsBoolean("//hide_options/hide_name"),
					HideEmail = resumeElement.GetElementValueAsBoolean("//hide_options/hide_email"),
					HideHomePhoneNumber = resumeElement.GetElementValueAsBoolean("//hide_options/hide_homePhoneNumber"),
					HideWorkPhoneNumber = resumeElement.GetElementValueAsBoolean("//hide_options/hide_workPhoneNumber"),
					HideCellPhoneNumber = resumeElement.GetElementValueAsBoolean("//hide_options/hide_cellPhoneNumber"),
					HideFaxPhoneNumber = resumeElement.GetElementValueAsBoolean("//hide_options/hide_faxPhoneNumber"),
					HideNonUSPhoneNumber = resumeElement.GetElementValueAsBoolean("//hide_options/hide_nonUSPhoneNumber"),
					HideAffiliations = resumeElement.GetElementValueAsBoolean("//hide_options/hide_affiliations"),
					HideCertificationsAndProfessionalLicenses = resumeElement.GetElementValueAsBoolean("//hide_options/hide_certifications_professionallicenses"),
					HideEmployer = resumeElement.GetElementValueAsBoolean("//hide_options/hide_employer"),
					HideDriverLicense = resumeElement.GetElementValueAsBoolean("//hide_options/hide_driver_license"),
					UnHideDriverLicense = resumeElement.GetElementValueAsBoolean("//hide_options/unhide_driver_license"),
					HideHonors = resumeElement.GetElementValueAsBoolean("//hide_options/hide_honors"),
					HideInterests = resumeElement.GetElementValueAsBoolean("//hide_options/hide_interests"),
					HideInternships = resumeElement.GetElementValueAsBoolean("//hide_options/hide_internships"),
					HideLanguages = resumeElement.GetElementValueAsBoolean("//hide_options/hide_languages"),
					HideVeteran = resumeElement.GetElementValueAsBoolean("//hide_options/hide_veteran"),
					HideObjective = resumeElement.GetElementValueAsBoolean("//hide_options/hide_objective"),
					HidePersonalInformation = resumeElement.GetElementValueAsBoolean("//hide_options/hide_personalinformation"),
					HideProfessionalDevelopment = resumeElement.GetElementValueAsBoolean("//hide_options/hide_professionaldevelopment"),
					HidePublications = resumeElement.GetElementValueAsBoolean("//hide_options/hide_publications"),
					HideReferences = resumeElement.GetElementValueAsBoolean("//hide_options/hide_references"),
					HideSkills = resumeElement.GetElementValueAsBoolean("//hide_options/hide_skills"),
					HideTechnicalSkills = resumeElement.GetElementValueAsBoolean("//hide_options/hide_technicalskills"),
					HideVolunteerActivities = resumeElement.GetElementValueAsBoolean("//hide_options/hide_volunteeractivities"),
					HideThesisMajorProjects = resumeElement.GetElementValueAsBoolean("//hide_options/hide_thesismajorprojects"),
					HiddenJobs = resumeElement.XPathSelectElement("//experience/job").IsNull()
						? null
						: (from job in resumeElement.XPathSelectElements("//experience/job")
							where job.GetElementValueAsBoolean("display_in_resume")
							select job.GetElementValueAsString("jobid")).ToList(),
					HiddenSkills = hiddenSkills.Length == 0 ? new List<string>() : hiddenSkills.Split(',').ToList()
				},
				PreservedFormatId = GetPreservedFormatId(runtimeContext, resumeElement.GetElementValueAsString("//special/preserved_format")),
				PreservedOrder = resumeElement.GetElementValueAsString("//special/preserved_order"),
				IsPastFiveYearsVeteran = resumeElement.GetElementValueAsNullableBoolean("//statements/personal/veteran/past_five_years_veteran"),
				Branding = resumeElement.GetElementValueAsString("//special/branding"),
			};

			#endregion
		}

		/// <summary>
		/// Gets the preserved format id.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <param name="preservedFormat">The preserved format.</param>
		/// <returns></returns>
		private static long? GetPreservedFormatId(IRuntimeContext runtimeContext, string preservedFormat)
		{
			if (preservedFormat.IsNullOrEmpty())
				return null;

			if (preservedFormat == "Original")
				return 0;

			return GetLookupId(runtimeContext, LookupTypes.ResumeFormats, preservedFormat);
		}

		/// <summary>
		/// Gets the preserved format external id.
		/// </summary>
		/// <param name="preservedFormatId">The preserved format id.</param>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <returns></returns>
		private static string GetPreservedFormatExternalId(this long? preservedFormatId, IRuntimeContext runtimeContext)
		{
			if (!preservedFormatId.HasValue)
				return String.Empty;

			if (preservedFormatId == 0)
				return "Original";

			return GetLookupExternalId(runtimeContext, LookupTypes.ResumeFormats, preservedFormatId.Value);
		}

		/// <summary>
		/// Gets the resume body contact section.
		/// </summary>
		/// <param name="resumeElement">The resume Xml element.</param>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <returns>
		/// contact
		/// </returns>
		private static Contact GetContactSection(XElement resumeElement, IRuntimeContext runtimeContext)
		{
			#region Get resume contact section

			var contact = new Contact
			{
				SeekerName = new Name
				{
					FirstName = resumeElement.XPathAsString("//contact/name/givenname[1]").TruncateString(50),
					MiddleName = resumeElement.XPathAsString("//contact/name/givenname[2]"),
          LastName = resumeElement.XPathAsString("//contact/name/surname").TruncateString(50),
					SuffixId = GetSuffixId(resumeElement, runtimeContext),
					SuffixName = GetSuffixName(resumeElement)
				},
				EmailAddress = resumeElement.XPathAsString("//contact/email"),
				PostalAddress = new Address
				{
          Street1 = resumeElement.XPathAsString("//contact/address/street[1]").TruncateString(200),
					Street2 = resumeElement.XPathAsString("//contact/address/street[2]").TruncateString(200),
					City = resumeElement.XPathAsString("//contact/address/city").TruncateString(100),
					StateId = GetLookupId(runtimeContext, resumeElement, "//contact/address/state", LookupTypes.States),
					StateName = resumeElement.XPathAsString("//contact/address/state_fullname"),
					CountyId = GetLookupId(runtimeContext, resumeElement, "//contact/address/state_county", LookupTypes.Counties),
					CountyName = resumeElement.XPathAsString("//contact/address/county_fullname"),
					CountryName = resumeElement.XPathAsString("//contact/address/country"),
					CountryId = GetLookupId(runtimeContext, resumeElement, "//contact/address/country", LookupTypes.Countries),
					Zip = resumeElement.XPathAsString("//contact/address/postalcode")
				},
				PhoneNumber =
          resumeElement.XPathSelectElement("//contact/phone").IsNull()
						? null
						: (from p in resumeElement.XPathSelectElements("//contact/phone")
							 where p.Value.IsNotNull() && p.Value.Length <= 50
							select new Phone
							{
								PhoneNumber = p.Value,
								PhoneType = GetPhoneType(p),
								Extention = p.GetElementValueAsString("@ext"),
							}).ToList(),
				WebSite = resumeElement.XPathAsString("//contact/website")
			};



			// Occassionally lens puts the seeker's name in the address line
			if (contact.PostalAddress.Street1.StartsWith(contact.SeekerName.FullName, StringComparison.OrdinalIgnoreCase))
			{
				contact.PostalAddress.Street1 = contact.PostalAddress.Street1.Substring(contact.SeekerName.FullName.Length);
			}

			return contact;

			#endregion
		}

		/// <summary>
		/// Gets the phone type for a phone element
		/// </summary>
		/// <param name="phoneElement">The phone element</param>
		/// <returns>The PhoneType</returns>
		private static PhoneType GetPhoneType(XElement phoneElement)
		{
			var phoneType = PhoneType.Home;

			if (phoneElement.IsNull())
				return phoneType;

			var type = phoneElement.GetElementValueAsString("@type");

			if (type.IsNotNullOrEmpty())
				return type.AsEnum(phoneType);

			// If 'type' attribute is not specified, look for text preceding the phone element
			var previousNode = phoneElement.PreviousNode;
			if (previousNode.IsNotNull() && previousNode.NodeType == XmlNodeType.Text)
			{
				switch (((XText)previousNode).Value.Trim().Replace(":", "").ToLowerInvariant())
				{
					case "home":
						phoneType = PhoneType.Home;
						break;
					case "work":
						phoneType = PhoneType.Work;
						break;
					case "cell":
						phoneType = PhoneType.Cell;
						break;
					case "fax":
						phoneType = PhoneType.Fax;
						break;
					case "nonus":
						phoneType = PhoneType.NonUS;
						break;
				}
			}

			return phoneType;
		}

		/// <summary>
		/// Gets the resume body education info section.
		/// </summary>
		/// <param name="resumeElement">The resume Xml element.</param>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <returns>
		/// education info
		/// </returns>
		private static EducationInfo GetEducationInfoSection(XElement resumeElement, IRuntimeContext runtimeContext)
		{
			var school = resumeElement.XPathSelectElements("//education/school");

			var test = (from s in resumeElement.XPathSelectElements("//education/school")
				select new School
				{
					SchoolId = s.Attribute("id").IsNotNull() ? Convert.ToInt32(s.Attribute("id").Value) : 0,
					CompletionDate = s.GetElementValueAsDateTime("completiondate").AsNull<DateTime>(),
					ExpectedCompletionDate = s.GetElementValueAsDateTime("expectedcompletiondate").AsNull<DateTime>(),
					Degree = s.GetElementValueAsString("degree"),
					Institution = s.GetElementValueAsString("institution"),
					Major = s.GetElementValueAsString("major"),
					Courses = s.GetElementValueAsString("courses"),
					Honors = s.GetElementValueAsString("honors"),
					Activities = s.GetElementValueAsString("activities"),
					GradePointAverage = s.XPathSelectElement("gpa").IsNull() ? null :
						new GradePointAverage
						{
							Max = s.GetElementValueAsString("gpa/@max").AsFloat().AsNull<float>(),
							Value = s.GetElementValueAsString("gpa").AsFloat().AsNull<float>(),
							RawGradePointAverage = s.GetElementValueAsString("gpa")
						},
					SchoolAddress = new Address
					{
						City = s.GetElementValueAsString("address/city"),
						CountyName = s.GetElementValueAsString("address/county_fullname"),
						CountyId = GetLookupId(runtimeContext, LookupTypes.Counties, s.GetElementValueAsString("address/county")),
						StateId = GetLookupId(runtimeContext, LookupTypes.States, s.GetElementValueAsString("address/state")),
						StateName = s.GetElementValueAsString("address/state_fullname"),
						CountryId = GetLookupId(runtimeContext, LookupTypes.Countries, s.GetElementValueAsString("address/country")),
						CountryName = s.GetElementValueAsString("address/country_fullname")
					}
				}).ToList();



			#region Get resume education info section

			return new EducationInfo
			{
				NCRCConfirmation = resumeElement.GetElementValueAsBoolean("//ResDoc/resume/education/ncrc_confirmation"),
				NCRCLevel = resumeElement.GetElementValueAsInt64("//ResDoc/resume/education/ncrc_level"),
				NCRCLevelName = resumeElement.GetElementValueAsString("//ResDoc/resume/education/ncrc_level_name"),
				NCRCStateId = resumeElement.GetElementValueAsInt64("//ResDoc/resume/education/ncrc_state_id"),
				NCRCStateName = resumeElement.GetElementValueAsString("//ResDoc/resume/education/ncrc_state_name"),
				NCRCIssueDate = resumeElement.GetElementValueAsDateTime("//ResDoc/resume/education/ncrc_issue_date").AsNull<DateTime>(),
				NCRCDisplayed = resumeElement.GetElementValueAsBoolean("//ResDoc/resume/education/ncrc_displayed").IsNull() ? (bool?)null : resumeElement.GetElementValueAsBoolean("//ResDoc/resume/education/ncrc_displayed"),
				TargetIndustries = GetIndustries(resumeElement.XPathSelectElement("//education/targetindustries")),
				MostExperienceIndustries = GetIndustries(resumeElement.XPathSelectElement("//education/mostexperienceindustries")),
				Courses = resumeElement.GetElementValueAsString("//education/courses"),
				EducationLevel = (resumeElement.XPathSelectElement("//education/norm_edu_level_cd").IsNotNull() ? resumeElement.GetElementValueAsInt32("//education/norm_edu_level_cd").AsEnum<EducationLevel>()
					: (resumeElement.XPathSelectElement("//education/school").IsNotNull() && resumeElement.XPathSelectElements("//education/school").Any() ? resumeElement.XPathSelectElements("//education/school").Max(x => x.GetElementValueAsInt32("degree/@level")).AsEnum<EducationLevel>() : null)),
				SchoolStatus = resumeElement.GetElementValueAsInt32("//education/school_status_cd").AsEnum<SchoolStatus>(),
				Schools = resumeElement.XPathSelectElement("//education/school").IsNull() ? null : (from s in resumeElement.XPathSelectElements("//education/school")
					select new School
					{
						SchoolId = s.Attribute("id").IsNotNull() ? Convert.ToInt32(s.Attribute("id").Value) : 0,
						CompletionDate = s.GetElementValueAsDateTime("completiondate").AsNull<DateTime>(),
						ExpectedCompletionDate = s.GetElementValueAsDateTime("expectedcompletiondate").AsNull<DateTime>(),
						Degree = s.GetElementValueAsString("degree"),
						Institution = s.GetElementValueAsString("institution"),
						Major = s.GetElementValueAsString("major"),
						Courses = s.GetElementValueAsString("courses"),
						Honors = s.GetElementValueAsString("honors"),
						Activities = s.GetElementValueAsString("activities"),
						GradePointAverage = s.XPathSelectElement("gpa").IsNull() ? null :
							new GradePointAverage
							{
								Max = s.GetElementValueAsString("gpa/@max").AsFloat().AsNull<float>(),
								Value = s.GetElementValueAsString("gpa").AsFloat().AsNull<float>(),
								RawGradePointAverage = s.GetElementValueAsString("gpa")
							},
						SchoolAddress = new Address
						{
							City = s.GetElementValueAsString("address/city"),
							CountyName = s.GetElementValueAsString("address/county_fullname"),
							CountyId = GetLookupId(runtimeContext, LookupTypes.Counties, s.GetElementValueAsString("address/county")),
							StateId = GetLookupId(runtimeContext, LookupTypes.States, s.GetElementValueAsString("address/state")),
							StateName = s.GetElementValueAsString("address/state_fullname"),
							CountryId = GetLookupId(runtimeContext, LookupTypes.Countries, s.GetElementValueAsString("address/country")),
							CountryName = s.GetElementValueAsString("address/country_fullname")
						}
					}).ToList()
			};

			#endregion
		}

		/// <summary>
		/// Gets the resume body statements section.
		/// </summary>
		/// <returns>statement info</returns>
		private static Statements GetStatementsSection(XElement resumeElement)
		{
			#region Get statements section

			return new Statements
			{
				Honors = resumeElement.GetElementValueAsString("//statements/honors")
			};

			#endregion
		}

		/// <summary>
		/// Gets the resume body user info section.
		/// </summary>
		/// <returns>user info</returns>
		private static UserProfile GetUserInfoSection(XElement resumeElement, IRuntimeContext runtimeContext)
		{
			#region Get user info section

			var msfwQuestionsNode = resumeElement.XPathSelectElement("//statements/personal/msfw_questions");
            return new UserProfile
            {

                UserGivenName = new Name
                {
                    FirstName = resumeElement.GetElementValueAsString("//contact/name/givenname[1]"),
                    MiddleName = resumeElement.GetElementValueAsString("//contact/name/givenname[2]"),
                    LastName = resumeElement.GetElementValueAsString("//contact/name/surname"),
                    SuffixId = GetSuffixId(resumeElement, runtimeContext),
                    SuffixName = GetSuffixName(resumeElement)
                },
                PostalAddress = new Address
                {
                    Street1 = resumeElement.GetElementValueAsString("//contact/address/street[1]"),
                    Street2 = resumeElement.GetElementValueAsString("//contact/address/street[2]"),
                    City = resumeElement.GetElementValueAsString("//contact/address/city|//contact/address/town"),
                    CountyId = GetLookupId(runtimeContext, resumeElement, "//contact/address/state_county", LookupTypes.Counties),
                    CountyName = resumeElement.GetElementValueAsString("//contact/address/county_fullname"),
                    StateId = GetLookupId(runtimeContext, LookupTypes.States, resumeElement.GetElementValueAsString("//contact/address/state")),
                    StateName = resumeElement.GetElementValueAsString("//contact/address/state_fullname|//contact/address/county_fullname"),
                    CountryId = GetLookupId(runtimeContext, LookupTypes.Countries, resumeElement.GetElementValueAsString("//contact/address/country")),
                    CountryName = resumeElement.GetElementValueAsString("//contact/address/country_fullname"),
                    Zip = resumeElement.GetElementValueAsString("//contact/address/postalcode")
                },
                PrimaryPhone = new Phone
                {
                    PhoneNumber = resumeElement.GetElementValueAsString("//contact/phone[1]"),
                    PhoneType = GetPhoneType(resumeElement.XPathSelectElement("//contact/phone[1]")),
                    Extention = resumeElement.GetElementValueAsString("//contact/phone[1]/@ext"),
                },
                DOB = resumeElement.GetElementValueAsDateTime("//statements/personal/dob").AsNull<DateTime>(),
                SSN = resumeElement.GetElementValueAsString("//statements/personal/userinfo/ssn"),
                Sex = resumeElement.GetElementValueAsInt32("//statements/personal/sex").AsEnum<Genders>(),
                EmailAddress = resumeElement.GetElementValueAsString("//contact/email"),
                EthnicHeritage = new EthnicHeritage
                {
                    EthnicHeritageId = resumeElement.ElementExists("//statements/personal/ethnic_heritages/ethnic_heritage[ethnic_id='3']") ? GetLookupId(runtimeContext, LookupTypes.EthnicHeritages, resumeElement.GetElementValueAsString("//statements/personal/ethnic_heritages/ethnic_heritage/selection_flag")) : null,
                    RaceIds = resumeElement.XPathSelectElement("//statements/personal/ethnic_heritages").IsNull() ? null : (from race in resumeElement.XPathSelectElements("//statements/personal/ethnic_heritages/ethnic_heritage")
                                                                                                                            where race.GetElementValueAsString("selection_flag") == "-1" && race.GetElementValueAsString("ethnic_id") != "3"
                                                                                                                            select GetLookupId(runtimeContext, LookupTypes.Races, race.GetElementValueAsString("ethnic_id"))).ToList()
                },
                IsUSCitizen = resumeElement.GetElementValueAsNullableBoolean("//statements/personal/citizen_flag"),
                IsMigrant = resumeElement.GetElementValueAsNullableBoolean("//statements/personal/migrant_flag"),
                IsMSFW = resumeElement.GetElementValueAsNullableBoolean("//statements/personal/msfw_flag"),
                MSFWQuestions = msfwQuestionsNode.IsNull() ? null : (from question in msfwQuestionsNode.XPathSelectElements("msfw_question[selection_flag='-1']")
                                                                     select GetLookupId(runtimeContext, LookupTypes.MSFWQuestions, question.GetElementValueAsString("question_id")).GetValueOrDefault(0)).ToList(),
                DisabilityCategoryId = resumeElement.ElementExists("//statements/personal/disability_category_cd")?GetLookupId(runtimeContext, resumeElement, "//statements/personal/disability_category_cd", LookupTypes.DisabilityCategories):null,

                DisabilityStatus = resumeElement.GetElementValueAsInt32("//statements/personal/disability_status").AsEnum<DisabilityStatus>(),
               DisabilityCategoryIds = resumeElement.XPathSelectElement("//statements/personal/disability_categories").IsNull() ? null : (from disabilitytype in resumeElement.XPathSelectElements("//statements/personal/disability_categories/disability_category")
                                                                                                                                              where disabilitytype.GetElementValueAsString("selection_flag") == "-1"
                                                                                                                                              select GetLookupId(runtimeContext, LookupTypes.DisabilityCategories, disabilitytype.GetElementValueAsString("disability_id"))).ToList(),
                Veteran = GetVeteranSection(resumeElement, runtimeContext),
                License = SetDriverLicense(resumeElement, runtimeContext, resumeElement.XPathSelectElement("//statements/personal/license")),
                AlienId = resumeElement.GetElementValueAsString("//statements/personal/alien_id"),
                AlienExpires = resumeElement.GetElementValueAsDateTime("//statements/personal/alien_expires").AsNull<DateTime>(),
                IsPermanentResident = resumeElement.GetElementValueAsNullableBoolean("//statements/personal/alien_perm_flag"),
                AuthorisedToWork = resumeElement.GetElementValueAsNullableBoolean("//statements/personal/alien_authorised_flag"),
                IsCareerReadinessCertified = resumeElement.GetElementValueAsNullableBoolean("//statements/personal/crc"),
                RegisteredWithSelectiveService = resumeElement.GetElementValueAsNullableBoolean("//statements/personal/selective_service_flag"),
                LowLevelLiteracy = resumeElement.ElementExists("//statements/personal/lowlevel_literacy_flag") ? resumeElement.GetElementValueAsBoolean("//statements/personal/lowlevel_literacy_flag") : false,
                PreferredLanguage = resumeElement.ElementExists("//statements/personal/preferred_language") ? resumeElement.GetElementValueAsString("//statements/personal/preferred_language") : "Not disclosed",
                LowLevelLiteracyIssues = new LowLevelLiteracyIssues
                {
                    CommonLanguage = resumeElement.ElementExists("//statements/personal/lowlevelliteracyissues/lowlevelliteracyissues_lang") && resumeElement.XPathSelectElement("//statements/personal/lowlevelliteracyissues/lowlevelliteracyissues_lang").IsNotNull() ? resumeElement.GetElementValueAsString("//statements/personal/lowlevelliteracyissues/lowlevelliteracyissues_lang/common_language") : "Not disclosed",
                    NativeLanguage = resumeElement.ElementExists("//statements/personal/lowlevelliteracyissues/lowlevelliteracyissues_lang") && resumeElement.XPathSelectElement("//statements/personal/lowlevelliteracyissues/lowlevelliteracyissues_lang").IsNotNull() ? resumeElement.GetElementValueAsString("//statements/personal/lowlevelliteracyissues/lowlevelliteracyissues_lang/native_language") : "Not disclosed",
                },
                IsUnderYouth = resumeElement.GetElementValueAsBoolean("//statements/personal/run_away_youth"),
                IsStaying = resumeElement.GetElementValueAsBoolean("//statements/personal/homeless_with_shelter"),
                IsHomeless = resumeElement.GetElementValueAsBoolean("//statements/personal/homeless_without_shelter"),
                IsExOffender = resumeElement.GetElementValueAsBoolean("//statements/personal/ex-offender"),
                CulturalBarriers = resumeElement.ElementExists("//statements/personal/cultural_barriers_flag") ? resumeElement.GetElementValueAsBoolean("//statements/personal/cultural_barriers_flag") : false,
                DisplacedHomemaker = resumeElement.ElementExists("//statements/personal/displaced_homemaker") ? resumeElement.GetElementValueAsBoolean("//statements/personal/displaced_homemaker") : false,
                SingleParent = resumeElement.ElementExists("//statements/personal/single_parent") ? resumeElement.GetElementValueAsBoolean("//statements/personal/single_parent") : false,
                LowIncomeStatus = resumeElement.ElementExists("//statements/personal/low_income_status") ? resumeElement.GetElementValueAsBoolean("//statements/personal/low_income_status") : false,
                NoOfDependents = resumeElement.ElementExists("//statements/personal/no_of_dependants")?resumeElement.GetElementValueAsString("//statements/personal/no_of_dependants"):"Not disclosed",
                EstMonthlyIncome = resumeElement.ElementExists("//statements/personal/est_monthly_income") ? resumeElement.GetElementValueAsString("//statements/personal/est_monthly_income") : "Not disclosed"
            };

			#endregion
		}

		/// <summary>
		/// Gets the veteran information from the XML
		/// </summary>
		/// <param name="resumeElement">The current resume Xml</param>
		/// <param name="runtimeContext">The runtime context</param>
		/// <returns>The VeteranInfo section</returns>
		private static VeteranInfo GetVeteranSection(XElement resumeElement, IRuntimeContext runtimeContext)
		{
			var veteranNode = resumeElement.XPathSelectElement("//statements/personal/veteran") ?? new XElement("veteran");

			var veteran = new VeteranInfo
			{
				IsVeteran = veteranNode.GetElementValueAsNullableBoolean("vet_flag"),
				VeteranPriorityServiceAlertsSubscription = veteranNode.GetElementValueAsNullableBoolean("subscribe_vps").GetValueOrDefault(true),
				IsHomeless = veteranNode.GetElementValueAsNullableBoolean("homeless_flag"),
				AttendedTapCourse = veteranNode.GetElementValueAsNullableBoolean("attended_tap_flag"),
				DateAttendedTapCourse = veteranNode.GetElementValueAsDateTime("date_attended_tap").AsNull<DateTime>(),
				MOCRonetInfo = veteranNode.XPathSelectElement("mocronet/ronet").IsNull()
					? null
					: GetMocROnetInfo(veteranNode.XPathSelectElement("mocronet")),
				History = new List<VeteranHistory>()
			};

			var historyNodes = resumeElement.XPathSelectElements("//statements/personal/veteran/history").ToList();

			if (historyNodes.IsNullOrEmpty())
				historyNodes.Add(veteranNode);

			historyNodes.ForEach(historyNode =>
			{
				long? rankId = null;
				var militaryBranchOfServiceId = GetLookupId(runtimeContext, historyNode, "branch_of_service", LookupTypes.MilitaryBranchesOfService);

				if (militaryBranchOfServiceId.IsNotNull())
					rankId = GetLookupId(runtimeContext, historyNode, "rank", LookupTypes.MilitaryRanks, militaryBranchOfServiceId);

				var history = new VeteranHistory
				{
					VeteranDisabilityStatus = historyNode.GetElementValueAsInt32("vet_disability_status").AsEnum<VeteranDisabilityStatus>(),
					TransitionType = historyNode.GetElementValueAsInt32("vet_tsm_type_cd").AsEnum<VeteranTransitionType>(),
					VeteranEra = historyNode.GetElementValueAsInt32("vet_era").AsEnum<VeteranEra>(),
					VeteranStartDate = historyNode.GetElementValueAsDateTime("vet_start_date").AsNull<DateTime>(),
					VeteranEndDate = historyNode.GetElementValueAsDateTime("vet_end_date").AsNull<DateTime>(),
					IsCampaignVeteran = historyNode.GetElementValueAsNullableBoolean("campaign_vet_flag"),
					MilitaryDischargeId = GetLookupId(runtimeContext, historyNode, "vet_discharge", LookupTypes.MilitaryDischargeTypes),
					MilitaryBranchOfServiceId = militaryBranchOfServiceId,
					RankId = rankId,
					MilitaryOccupation = historyNode.GetElementValueAsString("moc"),
					MilitaryOccupationCode = historyNode.GetElementValueAsString("moc/@code"),
					UnitOrAffiliation = historyNode.GetElementValueAsString("unit_affiliation")
				};

				var employentStatusNode = historyNode.XPathSelectElement("employment_status");
				history.EmploymentStatus = employentStatusNode.IsNull()
					? resumeElement.GetElementValueAsInt32("//experience/employment_status_cd").AsEnum<EmploymentStatus>()
					: employentStatusNode.Value.AsInt32().AsEnum<EmploymentStatus>();

				veteran.History.Add(history);
			});

			return veteran;
		}

		/// <summary>
		/// Gets the suffixid from the XML
		/// </summary>
		/// <param name="resumeElement">The resume node in the results</param>
		/// <param name="runtimeContext">The runtime context</param>
		/// <returns>The suffix id</returns>
		/// <remarks>
		/// This is used due to a change to how suffixes were stored in the XML 
		/// In 3.31 a "suffix_fullname" and "suffix" nodes waere being stored under the "name" node, but this ended up being shown as part of the name in the Talent Pool Search, which was not required
		/// The "suffix_fullname" was removed in 3.32, and replace with just "suffix" under the "contact" node (with a "code" attribute)
		/// This method is simply to cope with any existing resumes posted to Lens in the old format
		/// </remarks>
		private static long? GetSuffixId(XElement resumeElement, IRuntimeContext runtimeContext)
		{
			var suffixElement = resumeElement.XPathSelectElement("//contact//suffix");
			if (suffixElement.IsNull())
				return null;

			var suffixAttr = suffixElement.Attribute("code");
			var suffixCode = suffixAttr.IsNull()
				? suffixElement.Value
				: suffixAttr.Value;

			return GetLookupId(runtimeContext, LookupTypes.Suffixes, suffixCode);
		}

		/// <summary>
		/// Gets the suffix name from the XML
		/// </summary>
		/// <param name="resumeElement">The resume node in the results</param>
		/// <returns>The suffix name</returns>
		/// <remarks>
		/// This is used due to a change to how suffixes were stored in the XML 
		/// In 3.31 a "suffix_fullname" and "suffix" nodes waere being stored under the "name" node, but this ended up being shown as part of the name in the Talent Pool Search, which was not required
		/// The "suffix_fullname" was removed in 3.32, and replace with just "suffix" under the "contact" node (with a "code" attribute)
		/// This method is simply to cope with any existing resumes posted to Lens in the old format
		/// </remarks>
		private static string GetSuffixName(XElement resumeElement)
		{
			var suffixElement = resumeElement.XPathSelectElement("//contact//suffix");
			if (suffixElement.IsNull())
				return null;

			return suffixElement.Attribute("code").IsNull()
				? resumeElement.GetElementValueAsString("//contact//suffix_fullname")
				: suffixElement.Value;
		}

		/// <summary>
		/// Gets the resume body skills section.
		/// </summary>
		/// <param name="resumeElement">The resume Xml element.</param>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <returns>
		/// skills info
		/// </returns>
		private static SkillInfo GetSkillsSection(XElement resumeElement, IRuntimeContext runtimeContext)
		{
			#region Get skills section

			List<LanguageProficiency> languageProficiencies;

			if (resumeElement.XPathSelectElement("//skills/languages/languages_profiency").IsNotNull())
			{
				languageProficiencies =
					(from s in
						resumeElement.GetElementValueAsString("//skills/languages/languages_profiency").Split(new[] { ", ", "," }, StringSplitOptions.RemoveEmptyEntries)
						select new LanguageProficiency { Language = s.Trim() }).Distinct().ToList();
			}
			else if (resumeElement.XPathSelectElement("//skills/languages/language").IsNotNull())
			{
				languageProficiencies = (from s in resumeElement.XPathSelectElements("//skills/languages/language")
					select new LanguageProficiency
					{
						Language = s.GetElementValueAsString("language"),
						Proficiency = GetLookupId(runtimeContext, LookupTypes.LanguageProficiencies, s.GetElementValueAsString("proficiency")).GetValueOrDefault(0)
					}).ToList();
			}
			else
			{
				languageProficiencies = null;
			}

			return new SkillInfo
			{
				Skills = resumeElement.XPathSelectElements("//skillrollup/canonskill").IsNull()
					? null
					: resumeElement.XPathSelectElements("//skillrollup/canonskill").Select(element => element.Attribute("name").Value).ToList(),
				LanguageProficiencies = languageProficiencies,
				Certifications = resumeElement.XPathSelectElement("//skills/courses/certifications/certification").IsNull()
					? null
					: (from c in resumeElement.XPathSelectElements("//skills/courses/certifications/certification")
						select new Certification
						{
							Certificate = c.GetElementValueAsString("certificate"),
							OrganizationName = c.GetElementValueAsString("organization_name"),
							CompletionDate = c.GetElementValueAsDateTime("completion_date").AsNull<DateTime>(),
							ExpectedCompletionDate = c.GetElementValueAsDateTime("expected_completion_date").AsNull<DateTime>(),
							CertificationAddress = new Address
							{
								City = c.GetElementValueAsString("address/city"),
								StateId = GetLookupId(runtimeContext, LookupTypes.States, c.GetElementValueAsString("address/state")),
								StateName = c.GetElementValueAsString("address/state_fullname"),
								CountryId = GetLookupId(runtimeContext, LookupTypes.Countries, c.GetElementValueAsString("address/country")),
								CountryName = c.GetElementValueAsString("address/country_fullname"),
							}
						}).ToList(),
				InternshipSkills = resumeElement.XPathSelectElement("//skills/internship_skills/internship_skill").IsNull()
					? null
					: (from s in resumeElement.XPathSelectElements("//skills/internship_skills/internship_skill")
						select new InternshipSkill
						{
							Id = s.GetElementValueAsInt64("id"),
							Name = s.GetElementValueAsString("name")
						}).ToList(),
				SkillsAlreadyCaptured = resumeElement.GetElementValueAsNullableBoolean("//skills/internship_skills/skills_already_captured")
			};

			#endregion
		}

		/// <summary>
		/// Gets the resume body professional section.
		/// </summary>
		/// <returns>professional info</returns>
		private static ProfessionalInfo GetProfessionalSection(XElement resumeElement)
		{
			#region Get professional section

			return new ProfessionalInfo
			{
				Affiliations = resumeElement.XPathSelectElement("//professional/affiliations").IsNull() ? null : resumeElement.GetElementValueAsString("//professional/affiliations").Split(',').ToList(),
				Publications = resumeElement.XPathSelectElement("//professional/publications").IsNull() ? null : resumeElement.GetElementValueAsString("//professional/publications").Split(',').ToList()

			};

			#endregion
		}

		/// <summary>
		/// Gets the resume body summary section.
		/// </summary>
		/// <returns>summary info</returns>
		private static SummaryInfo GetSummarySection(XElement resumeElement)
		{
			#region Get statements section

			return new SummaryInfo
			{
				IncludeSummary = resumeElement.GetElementValueAsNullableBoolean("//summary/summary/@include"),
				Summary = resumeElement.GetElementValueAsString("//summary/summary"),
				AutomatedSummary = resumeElement.GetElementValueAsString("//special/snapshot"),
				UseAutomatedSummary = resumeElement.GetElementValueAsNullableBoolean("//special/automatedsummary/@custom")
			};

			#endregion
		}

		/// <summary>
		/// Gets the resume body references section.
		/// </summary>
		/// <returns>reference info</returns>
		private static string GetReferencesSection(XElement resumeElement)
		{
			return resumeElement.GetElementValueAsString("//references");
		}

		/// <summary>
		/// Gets the resume body experience section.
		/// </summary>
		/// <param name="resumeElement">The resume Xml element.</param>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <returns>
		/// statement info
		/// </returns>
		private static ExperienceInfo GetExperienceSection(XElement resumeElement, IRuntimeContext runtimeContext)
		{
			#region Get experience section

			return new ExperienceInfo
			{
				EmploymentStatus = resumeElement.GetElementValueAsInt32("//experience/employment_status_cd").AsEnum<EmploymentStatus>(),
				Jobs = resumeElement.XPathSelectElement("//experience/job").IsNull() ? null : (from j in resumeElement.XPathSelectElements("//experience/job")
					select new Job
					{
						JobId = j.XPathAsNullableGuid("jobid"),
						IsCurrentJob = j.GetElementValueAsBoolean("daterange/end/@currentjob") || (j.GetElementValueAsString("daterange/end").IsNotNull() && j.GetElementValueAsString("daterange/end").ToLower() == "present") || (j.GetElementValueAsString("daterange/end/@days").IsNotNull() && j.GetElementValueAsString("daterange/end/@days").ToLower() == "present"),
						JobStartDate = j.GetElementValueAsDateTime("daterange/start/@iso8601").AsNull<DateTime>().IsNotNull() ? j.GetElementValueAsDateTime("daterange/start/@iso8601").AsNull<DateTime>() : j.GetElementValueAsDateTime("daterange/start").AsNull<DateTime>(),
						JobEndDate = j.GetElementValueAsDateTime("daterange/end/@iso8601").AsNull<DateTime>().IsNotNull() ? j.GetElementValueAsDateTime("daterange/end/@iso8601").AsNull<DateTime>() : j.GetElementValueAsDateTime("daterange/end").AsNull<DateTime>(),
						OnetCode = j.GetElementValueAsString("@onet"),
						ROnetCode = j.GetElementValueAsString("ronet"),
						Description = j.GetElementValueAsString("description"),
						Employer = j.GetElementValueAsString("employer"),
						Title = j.GetElementValueAsString("title"),
						MilitaryOccupationCode = j.GetElementValueAsString("title/@moc"),
						BranchOfServiceId = GetLookupId(runtimeContext, LookupTypes.MilitaryBranchesOfService, j.GetElementValueAsString("branch_of_service")),
						RankId = GetRankId(runtimeContext, j.GetElementValueAsString("rank"), j.GetElementValueAsString("branch_of_service")),
						JobAddress = new Address
						{
							City = j.GetElementValueAsString("address/city"),
							StateId = GetLookupId(runtimeContext, LookupTypes.States, j.GetElementValueAsString("address/state")),
							StateName = j.GetElementValueAsString("address/state_fullname"),
							CountryId = GetLookupId(runtimeContext, LookupTypes.Countries, j.GetElementValueAsString("address/country")),
							CountryName = j.GetElementValueAsString("address/country_fullname")
						},
						SalaryFrequencyId = GetLookupId(runtimeContext, LookupTypes.Frequencies, j.GetElementValueAsString("salary_unit_cd")),
						Salary = j.GetElementValueAsString("sal").AsFloat().AsNull<float>(),
						InternshipSkills = j.XPathEvaluate("internshipskills").IsNull() ? null : GetInternshipSkills(j),
                        JobReasonForLeaving = j.GetElementValueAsInt32("reason_for_leaving")
					}).ToList()
			};

			#endregion
		}

		#region Resume helper methods

		#region To xml helper methods

		#region Set the Person Name

		/// <summary>
		/// Sets the name.
		/// </summary>
		/// <param name="contactNode">The contact node.</param>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <param name="resume">The candidate.</param>
		private static void SetName(this XElement contactNode, IRuntimeContext runtimeContext, ref ResumeModel resume)
		{
			if (resume.ResumeContent.SeekerContactDetails.SeekerName.IsNull())
				return;

			var tempName = resume.ResumeContent.SeekerContactDetails.SeekerName;
			var nameNode = contactNode.GetOrCreateElement("name");
			if (!String.IsNullOrEmpty(tempName.FirstName))
				nameNode.GetOrCreateElement("givenname[1]", "givenname", tempName.FirstName);
			if (!String.IsNullOrEmpty(tempName.MiddleName))
				nameNode.GetOrCreateElement("givenname[2]", "givenname", tempName.MiddleName);
			if (!String.IsNullOrEmpty(tempName.LastName))
				nameNode.GetOrCreateElement("surname", "surname", tempName.LastName);
			if (tempName.SuffixId.IsNotNullOrZero())
			{
				var suffixName = tempName.SuffixName.IsNotNullOrEmpty()
					? tempName.SuffixName
					: GetLookupText(runtimeContext, LookupTypes.Suffixes, tempName.SuffixId.Value);

				var suffixNode = contactNode.GetOrCreateElement("suffix", "suffix", suffixName);
				suffixNode.SetAttributeValue("code", GetLookupExternalId(runtimeContext, LookupTypes.Suffixes, tempName.SuffixId.Value));
			}
		}

		#endregion

		#region Set the Communication

		/// <summary>
		/// Sets the phone.
		/// </summary>
		/// <param name="contactElement">The contact node.</param>
		/// <param name="resume">The candidate.</param>
		private static void SetPhone(this XElement contactElement, ref ResumeModel resume)
		{
			if (resume.ResumeContent.SeekerContactDetails.PhoneNumber.IsNull())
				return;

			// Delete all current phone nodes
			contactElement.RemoveElementsByXpath("phone");
			contactElement.RemoveElementsByNodeType(XmlNodeType.Text);

			var tempPhones = resume.ResumeContent.SeekerContactDetails.PhoneNumber;
			foreach (var t in tempPhones)
			{
				if (t.IsNull() || string.IsNullOrEmpty(t.PhoneNumber))
					continue;
				var phone = contactElement.CreateElement("phone", t.PhoneNumber);
				if (!string.IsNullOrEmpty(t.Extention))
					phone.CreateAttribute("ext", t.Extention);
				phone.CreateAttribute("type", t.PhoneType.ToString().ToLower());
				phone.AddBeforeSelf(new XText(" " + t.PhoneType.ToString() + ":"));
			}
		}

		/// <summary>
		/// Sets the email.
		/// </summary>
		/// <param name="contactElement">The contact node.</param>
		/// <param name="resume">The candidate.</param>
		private static void SetEmail(this XElement contactElement, ref ResumeModel resume)
		{
			if (resume.ResumeContent.SeekerContactDetails.EmailAddress.IsNull())
			{
				// Delete all current email nodes
				contactElement.RemoveElementsByXpath("email");
				return;
			}

			contactElement.GetOrCreateElement("email", "email", resume.ResumeContent.SeekerContactDetails.EmailAddress);
		}

		/// <summary>
		/// Sets the website.
		/// </summary>
		/// <param name="contactElement">The contact node.</param>
		/// <param name="resume">The candidate.</param>
		private static void SetWebsite(this XElement contactElement, ref ResumeModel resume)
		{
			if (resume.ResumeContent.SeekerContactDetails.WebSite.IsNull())
			{
				// Delete all current website nodes
				contactElement.RemoveElementsByXpath("website");
				return;
			}
			contactElement.GetOrCreateElement("website", "website", resume.ResumeContent.SeekerContactDetails.WebSite);
		}

		/// <summary>
		/// Sets the address.
		/// </summary>
		/// <param name="contactElement">The contact node.</param>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <param name="resume">The candidate.</param>
		/// <param name="countryCode">The country code.</param>
		private static void SetAddress(this XElement contactElement, IRuntimeContext runtimeContext, ref ResumeModel resume, string countryCode)
		{
			if (resume.ResumeContent.SeekerContactDetails.PostalAddress.IsNull())
				return;

			// Delete all current address nodes
			contactElement.RemoveElementsByXpath("address");
			var tempAddress = resume.ResumeContent.SeekerContactDetails.PostalAddress;
			string stateTag = "state", cityTag = "city";

			var externalCountryCode = GetLookupExternalId(runtimeContext, LookupTypes.Countries, countryCode);

			if (!String.IsNullOrEmpty(countryCode) && externalCountryCode == "UK")
			{
				stateTag = "county";
				cityTag = "town";
			}

			var addressElement = contactElement.GetOrCreateElement("address", "address");
			addressElement.RemoveAll();

			addressElement.CreateElement("street", tempAddress.Street1);
			addressElement.CreateElement("street", tempAddress.Street2);
			addressElement.CreateElement(cityTag, tempAddress.City);

			if (externalCountryCode == "US")
			{
				if (tempAddress.CountyId != null)
					addressElement.CreateElement("state_county", GetLookupExternalId(runtimeContext, LookupTypes.Counties, (long)tempAddress.CountyId));
				addressElement.CreateElement("county_fullname", tempAddress.CountyName);
			}

			if (tempAddress.StateId != null)
				addressElement.CreateElement(stateTag, GetLookupExternalId(runtimeContext, LookupTypes.States, (long)tempAddress.StateId));

			addressElement.CreateElement(stateTag + "_fullname", tempAddress.StateName);
			addressElement.CreateElement("postalcode", tempAddress.Zip);

			if (tempAddress.CountryId != null)
				addressElement.CreateElement("country", GetLookupExternalId(runtimeContext, LookupTypes.Countries, (long)tempAddress.CountryId));

			addressElement.CreateElement("country_fullname", tempAddress.CountryName);

			// Cleanup any attributes from the address node.
			// Not sure why this is done but taken it from other code in F/C
			addressElement.RemoveAttributes();
		}

		#endregion

		//#region Set the Military Status

		///// <summary>
		///// Sets the military status.
		///// </summary>
		///// <param name="personalElement">The personal node.</param>
		///// <param name="resume">The candidate.</param>
		//private static void SetMilitaryStatus(this XElement personalElement, ref ResumeModel resume)
		//{
		//    if (resume.ResumeContent.UserInfo.Profile.Veteran.IsNull())
		//        return;

		//    // Get/Create the veteran node
		//    var veteranNode = personalElement.GetOrCreateElement("veteran");
		//    veteranNode.RemoveAll();

		//    // Get/Create the vet_flag node
		//    veteranNode.GetOrCreateElement("vet_flag", "vet_flag", resume.ResumeContent.UserInfo.Profile.Veteran.IsVeteran ? "-1" : "0");
		//}

        //#endregion
        #region Legal Issue ExOffender
        private static void SetExOffender(this XElement personalElement, ref ResumeModel resume)
        {
            if (resume.ResumeContent.Profile.IsExOffender.GetValueOrDefault())
            {    
                personalElement.GetOrCreateElement("ex-offender","ex-offender",(bool)resume.ResumeContent.Profile.IsExOffender);
                            
            }
        }

        #endregion
        #region House Issue
        private static void SetHomeless(this XElement personalElement, ref ResumeModel resume)
        {
            if (resume.ResumeContent.Profile.IsHomeless.GetValueOrDefault())
            {
                personalElement.GetOrCreateElement("homeless_without_shelter", "homeless_without_shelter", (bool)resume.ResumeContent.Profile.IsHomeless);
               
            }
        }
        private static void SetUnderYouth(this XElement personalElement, ref ResumeModel resume)
        {
            if (resume.ResumeContent.Profile.IsUnderYouth.GetValueOrDefault())
            {
                personalElement.GetOrCreateElement("run_away_youth", "run_away_youth", (bool)resume.ResumeContent.Profile.IsUnderYouth);
                
            }
        }
        private static void SetStaying(this XElement personalElement, ref ResumeModel resume)
        {
            if (resume.ResumeContent.Profile.IsStaying.GetValueOrDefault())
            {
                personalElement.GetOrCreateElement("homeless_with_shelter", "homeless_with_shelter", (bool)resume.ResumeContent.Profile.IsStaying);
                
            }
        }

        #endregion
        #region Set the Ethnicity and Race Codes

		/// <summary>
		/// Sets the ethnicity and race.
		/// </summary>
		/// <param name="personalElement">The personal node.</param>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <param name="resume">The candidate.</param>
		private static void SetEthnicityAndRace(this XElement personalElement, IRuntimeContext runtimeContext, ref ResumeModel resume)
		{
			if (resume.ResumeContent.Profile.EthnicHeritage.IsNull())
				return;

			// Delete all current ethnic heritages child nodes
			var ethnicHeritagesElement = personalElement.GetOrCreateElement("ethnic_heritages");
			ethnicHeritagesElement.RemoveElementsByXpath("ethnic_heritage");

			var tempEthHeritage = resume.ResumeContent.Profile.EthnicHeritage;

			if (!tempEthHeritage.EthnicHeritageId.HasValue && tempEthHeritage.RaceIds.IsNull())
				return;

			var ehCode = ethnicHeritagesElement.CreateElement("ethnic_heritage");

			if (tempEthHeritage.EthnicHeritageId.HasValue)
			{
				ehCode.CreateElement("ethnic_id", 3);
				ehCode.CreateElement("selection_flag", GetLookupExternalId(runtimeContext, LookupTypes.EthnicHeritages, (int)tempEthHeritage.EthnicHeritageId));
			}

			if (tempEthHeritage.RaceIds.IsNull())
				return;

			var races = runtimeContext.Helpers.Lookup.GetLookup(LookupTypes.Races);

			foreach (LookupItemView race in races)
			{
				var ehRace = ethnicHeritagesElement.CreateElement("ethnic_heritage");
				ehRace.CreateElement("ethnic_id", Convert.ToInt64(race.ExternalId));
				ehRace.CreateElement("selection_flag", tempEthHeritage.RaceIds.Contains(Convert.ToInt64(race.Id)) ? "-1" : "0");
			}
		}

		#endregion

		#region Set the Birth Date

		/// <summary>
		/// Sets the date of birth.
		/// </summary>
		/// <param name="personalElement">The personal node.</param>
		/// <param name="resume">The candidate.</param>
		private static void SetDateOfBirth(this XElement personalElement, ref ResumeModel resume)
		{
			if (!resume.ResumeContent.Profile.DOB.HasValue)
				return;
			personalElement.RemoveElementsByXpath("dob");
			var dob = (DateTime)resume.ResumeContent.Profile.DOB;
			personalElement.CreateElement("dob", dob.ToString("MM/dd/yyyy"));
		}

		#endregion

        #region set the Income Issue
        /// <summary>
        /// Sets the Income issue
        /// </summary>
        /// <param name="personalElement">The personal node.</param>
        /// <param name="resume">The candidate.</param>
        private static void SetIncome(this XElement personalElement, IRuntimeContext runtimecontext, ref ResumeModel resume)
        {
            personalElement.RemoveElementsByXpath("displaced_homemaker");
            personalElement.CreateElement("displaced_homemaker", (bool)resume.ResumeContent.Profile.DisplacedHomemaker?"1":"0");
            personalElement.RemoveElementsByXpath("single_parent");
            personalElement.CreateElement("single_parent", (bool)resume.ResumeContent.Profile.SingleParent ? "1" : "0");
            personalElement.RemoveElementsByXpath("low_income_status");
            personalElement.CreateElement("low_income_status", (bool)resume.ResumeContent.Profile.LowIncomeStatus ? "1" : "0");
            personalElement.RemoveElementsByXpath("no_of_dependants");
            personalElement.CreateElement("no_of_dependants", resume.ResumeContent.Profile.NoOfDependents.IsNotNullOrEmpty() ? resume.ResumeContent.Profile.NoOfDependents.ToString() : "Not disclosed");
            personalElement.RemoveElementsByXpath("est_monthly_income");
            personalElement.CreateElement("est_monthly_income", resume.ResumeContent.Profile.EstMonthlyIncome.IsNotNullOrEmpty() ? resume.ResumeContent.Profile.EstMonthlyIncome.ToString() : "Not disclosed");
        }

        #endregion

        #region set the Language Culture Issue

        /// <summary>
        /// Sets the Language culture issue
        /// </summary>
        /// <param name="personalElement">The personal node.</param>
        /// <param name="resume">The candidate.</param>
        private static void SetLanguageCulture(this XElement personalElement, IRuntimeContext runtimeContext, ref ResumeModel resume)
        {
            var preferred = resume.ResumeContent.Profile.PreferredLanguage;
            personalElement.RemoveElementsByXpath("preferred_language");
            personalElement.CreateElement("preferred_language", preferred.IsNotNullOrEmpty() ? preferred.ToString() : "Not disclosed");
            var isEngPreferred = resume.ResumeContent.Profile.LowLevelLiteracy.AsBoolean();
            personalElement.RemoveElementsByXpath("lowlevel_literacy_flag");
            personalElement.CreateElement("lowlevel_literacy_flag", (bool)isEngPreferred?"1":"0");
            if ((bool)resume.ResumeContent.Profile.LowLevelLiteracy)
            {
                var lowlevelliteracy = resume.ResumeContent.Profile.LowLevelLiteracyIssues;
                var PreferredElement = personalElement.GetOrCreateElement("lowlevelliteracyissues");
                PreferredElement.RemoveElementsByXpath("lowlevelliteracyissues_lang");
                if (lowlevelliteracy.IsNotNull())
                {
                    var preferredlanguage = PreferredElement.CreateElement("lowlevelliteracyissues_lang");
                    preferredlanguage.CreateElement("native_language", lowlevelliteracy.NativeLanguage.ToString());
                    preferredlanguage.CreateElement("common_language", lowlevelliteracy.CommonLanguage.ToString());
                }
            }
            personalElement.RemoveElementsByXpath("cultural_barriers_flag");
            personalElement.CreateElement("cultural_barriers_flag", (bool)resume.ResumeContent.Profile.CulturalBarriers?"1":"0");
        }
        #endregion

        #region Set the Gender Code

        /// <summary>
		/// Sets the gender.
		/// </summary>
		/// <param name="personalElement">The personal node.</param>
		/// <param name="resume">The candidate.</param>
		private static void SetGender(this XElement personalElement, ref ResumeModel resume)
		{
			if (!resume.ResumeContent.Profile.Sex.HasValue)
				return;

			personalElement.GetOrCreateElement("sex", "sex", (int)resume.ResumeContent.Profile.Sex);
		}

		#endregion

		#region Set the US Citizen status

		/// <summary>
		/// Sets the US Citizen status.
		/// </summary>
		/// <param name="personalElement">The personal node.</param>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <param name="candidate">The candidate.</param>
		private static void SetUSCitizen(this XElement personalElement, IRuntimeContext runtimeContext, ref ResumeModel candidate)
		{
			var profile = candidate.ResumeContent.Profile;
			if (!profile.IsUSCitizen.HasValue)
				return;

			if (runtimeContext.AppSettings.Theme == FocusThemes.Workforce)
			{
				if ((bool)profile.IsUSCitizen)
				{
					XElement alientElement;
					if ((alientElement = personalElement.Element("alien_id")).IsNotNull())
						if (alientElement != null) alientElement.Remove();
					if ((alientElement = personalElement.Element("alien_perm_flag")).IsNotNull())
						if (alientElement != null) alientElement.Remove();
					if ((alientElement = personalElement.Element("alien_expires")).IsNotNull())
						if (alientElement != null) alientElement.Remove();
					personalElement.GetOrCreateElement("citizen_flag", "citizen_flag", "-1");
				}
				else
				{
					personalElement.GetOrCreateElement("alien_id", "alien_id", candidate.ResumeContent.Profile.AlienId);
					personalElement.GetOrCreateElement("alien_perm_flag", "alien_perm_flag", profile.IsPermanentResident != null && (bool)profile.IsPermanentResident ? "-1" : "0");
					if (profile.IsPermanentResident != null && profile.AlienExpires.HasValue)
						personalElement.GetOrCreateElement("alien_expires", "alien_expires", ((DateTime)profile.AlienExpires).ToString("MM/dd/yyyy"));
					personalElement.GetOrCreateElement("citizen_flag", "citizen_flag", "0");
				}
			}
			else
			{
				personalElement.GetOrCreateElement("citizen_flag", "citizen_flag", profile.IsUSCitizen.GetValueOrDefault() ? "-1" : "0");
				personalElement.GetOrCreateElement("alien_perm_flag", "alien_perm_flag", profile.IsPermanentResident.GetValueOrDefault() ? "-1" : "0");
				personalElement.GetOrCreateElement("alien_authorised_flag", "alien_authorised_flag", profile.AuthorisedToWork.GetValueOrDefault() ? "-1" : "0");
			}
		}

		#endregion

		#region Set the MSFW (Migrant and Seasonal Farmworkers)

		/// <summary>
		/// Sets the Migrant and Seasonal Farmworkers status.
		/// </summary>
		/// <param name="personalElement">The personal node.</param>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <param name="profile">The user profile.</param>
		private static void SetMSFW(this XElement personalElement, IRuntimeContext runtimeContext, UserProfile profile)
		{
			if (!profile.IsMSFW.HasValue)
				return;

			if (runtimeContext.AppSettings.Theme == FocusThemes.Workforce)
			{
                if (profile.IsMSFW.Value)
                {
                    personalElement.GetOrCreateElement("msfw_flag", "msfw_flag", "-1");
                    if (runtimeContext.AppSettings.PotentialMSFWQuestionsEnabled)
                    {
                        var questionsElement = personalElement.GetOrCreateElement("msfw_questions");
                        questionsElement.RemoveElementsByXpath("msfw_question");

                        if (profile.MSFWQuestions.IsNotNullOrEmpty())
                        {
                            var questions = runtimeContext.Helpers.Lookup.GetLookup(LookupTypes.MSFWQuestions);
                            questions.ForEach(question =>
                            {
                                var questionElement = questionsElement.CreateElement("msfw_question");
                                questionElement.CreateElement("question_id", question.ExternalId);
                                questionElement.CreateElement("selection_flag", profile.MSFWQuestions.Contains(question.Id) ? "-1" : "0");
                            });
                        }
                    }
                }
                else
                {
                    personalElement.GetOrCreateElement("msfw_flag", "msfw_flag", "0");
                }
			}
		}

		#endregion

		#region Set the Disabled status

		/// <summary>
		/// Sets the Disabled status.
		/// </summary>
		/// <param name="personalElement">The personal node.</param>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <param name="resume">The candidate.</param>
		private static void SetDisabled(this XElement personalElement, IRuntimeContext runtimeContext, ref ResumeModel resume)
		{
			if (!resume.ResumeContent.Profile.DisabilityStatus.HasValue)
				return;

            personalElement.GetOrCreateElement("disability_status", "disability_status", (int)resume.ResumeContent.Profile.DisabilityStatus);
            if (resume.ResumeContent.Profile.DisabilityStatus == DisabilityStatus.Disabled)
            {
                if (resume.ResumeContent.Profile.DisabilityCategoryIds.IsNull())
                    return;
                if (resume.ResumeContent.Profile.DisabilityCategoryId.IsNotNull())
                    personalElement.RemoveElementsByXpath("disability_category_cd");

                var disabilityCategoryElement = personalElement.GetOrCreateElement("disability_categories");
                disabilityCategoryElement.RemoveElementsByXpath("disability_category");
                var disabilities = runtimeContext.Helpers.Lookup.GetLookup(LookupTypes.DisabilityCategories);

                foreach (LookupItemView disabilitytype in disabilities)
                {
                    var categoryElement = disabilityCategoryElement.CreateElement("disability_category");
                    categoryElement.CreateElement("disability_id", disabilitytype.ExternalId);
                    categoryElement.CreateElement("selection_flag", resume.ResumeContent.Profile.DisabilityCategoryIds.Contains(Convert.ToInt64(disabilitytype.Id)) ? "-1" : "0");
                }
            }

            //        personalElement.GetOrCreateElement("disability_category_cd", "disability_category_cd", GetLookupExternalId(runtimeContext, LookupTypes.DisabilityCategories, (long)resume.ResumeContent.Profile.DisabilityCategoryId));
		}

		#endregion

		#region Set the Registered with Selective Service Flag

		/// <summary>
		/// Sets the selective service.
		/// </summary>
		/// <param name="personalElement">The personal element.</param>
		/// <param name="resume">The resume.</param>
		private static void SetSelectiveService(this XElement personalElement, ref ResumeModel resume)
		{
			if (!resume.ResumeContent.Profile.RegisteredWithSelectiveService.HasValue)
				personalElement.RemoveElementsByXpath("selective_service_flag");
			else
				personalElement.GetOrCreateElement("selective_service_flag", "selective_service_flag", (bool)resume.ResumeContent.Profile.RegisteredWithSelectiveService ? "1" : "0");
		}

		#endregion

		#region Set the Employment History

		/// <summary>
		/// Sets the employment history.
		/// </summary>
		/// <param name="resumeElement">The resume node.</param>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <param name="resume">The candidate.</param>
		private static void SetEmploymentHistory(this XElement resumeElement, IRuntimeContext runtimeContext, ref ResumeModel resume)
		{
			if (resume.ResumeContent.ExperienceInfo.IsNull())
				return;

			var tempExpInfo = resume.ResumeContent.ExperienceInfo;
			var experienceElement = resumeElement.GetOrCreateElement("experience");

			// Set the employment status			
			if (tempExpInfo.EmploymentStatus.HasValue)
				experienceElement.GetOrCreateElement("employment_status_cd", "employment_status_cd", (int)tempExpInfo.EmploymentStatus);

			experienceElement.RemoveElementsByXpath("job");
			if (tempExpInfo.Jobs.IsNull())
				return;

			int monthsExperience = 0;

			foreach (var job in tempExpInfo.Jobs.OrderByDescending(j => j.JobEndDate ?? DateTime.MaxValue).ThenByDescending(j => j.JobStartDate ?? DateTime.MinValue))
			{
				// We must have an Organization
				//if (string.IsNullOrEmpty(job.Employer) || string.IsNullOrEmpty(job.Title))
				//  continue;
				var jobElement = experienceElement.CreateElement("job");
				jobElement.CreateElement("jobid", job.JobId.IsNull() ? Guid.NewGuid().ToString("N") : job.JobId.ToString());

				if (!string.IsNullOrEmpty(job.ROnetCode))
				{
					jobElement.CreateElement("ronet", job.ROnetCode);
					if (job.StarRating.HasValue)
						jobElement.Element("ronet").CreateAttribute("starrating", (int)job.StarRating);
				}

				if (!string.IsNullOrEmpty(job.OnetCode))
					jobElement.CreateAttribute("onet", job.OnetCode);

				if (resume.Special.IsNotNull() && resume.Special.HideInfo.IsNotNull() && resume.Special.HideInfo.HiddenJobs.IsNotNull())
					if (resume.Special.HideInfo.HiddenJobs.Contains(job.JobId.ToString()))
						jobElement.CreateElement("display_in_resume", "1");

				if (!string.IsNullOrEmpty(job.Employer))
					jobElement.CreateElement("employer", job.Employer);
				if (!string.IsNullOrEmpty(job.Title))
				{
					jobElement.CreateElement("title", job.Title);
					if (!string.IsNullOrEmpty(job.MilitaryOccupationCode))
						jobElement.Element("title").CreateAttribute("moc", job.MilitaryOccupationCode);
				}

				if (job.RankId.IsNotNull())
				{
					jobElement.CreateElement("rank", GetLookupExternalId(runtimeContext, LookupTypes.MilitaryRanks, (long)job.RankId));
				}

				if (job.BranchOfServiceId.HasValue)
				{
					jobElement.CreateElement("branch_of_service", GetLookupExternalId(runtimeContext, LookupTypes.MilitaryBranchesOfService, job.BranchOfServiceId.Value));
				}

				if (job.Salary.HasValue)
				{
					jobElement.CreateElement("sal", job.Salary);
					if (job.SalaryFrequencyId.HasValue)
						jobElement.CreateElement("salary_unit_cd", GetLookupExternalId(runtimeContext, LookupTypes.Frequencies, (long)job.SalaryFrequencyId));
				}

				if (!string.IsNullOrEmpty(job.Description))
					jobElement.CreateElement("description", job.Description);

				if (job.JobAddress.IsNotNull())
				{
					var jobAddress = jobElement.CreateElement("address");
					jobAddress.CreateElement("city", job.JobAddress.City);
					if (job.JobAddress.StateId.IsNotNull())
						jobAddress.CreateElement("state", GetLookupExternalId(runtimeContext, LookupTypes.States, (long)job.JobAddress.StateId));
					jobAddress.CreateElement("state_fullname", job.JobAddress.StateName);
					if (job.JobAddress.CountryId.IsNotNull())
						jobAddress.CreateElement("country", GetLookupExternalId(runtimeContext, LookupTypes.Countries, (long)job.JobAddress.CountryId));
					jobAddress.CreateElement("country_fullname", job.JobAddress.CountryName);
				}

				// We need to have a start and end date to continue
				if (!(job.JobStartDate.HasValue || job.JobEndDate.HasValue))
					continue;

				XElement dateRangeNode = jobElement.CreateElement("daterange");

				if (job.JobStartDate.HasValue)
					dateRangeNode.CreateElement("start", ((DateTime)job.JobStartDate).ToString("MM-dd-yyyy"));
				if (job.IsCurrentJob.HasValue && (bool)job.IsCurrentJob)
				{
					XElement endDateElement = dateRangeNode.CreateElement("end", job.JobEndDate.HasValue ? ((DateTime)job.JobEndDate).ToString("MM-yyyy") : DateTime.Today.ToString("MM-yyyy"));
					endDateElement.CreateAttribute("currentjob", "1");
					if (job.JobStartDate.HasValue)
						monthsExperience += DateTime.Now.MonthDifference((DateTime)job.JobStartDate);
				}
				else if (job.JobEndDate.HasValue)
				{
					dateRangeNode.CreateElement("end", ((DateTime)job.JobEndDate).ToString("MM-dd-yyyy"));
					if (job.JobStartDate.HasValue)
						monthsExperience += ((DateTime)job.JobEndDate).MonthDifference((DateTime)job.JobStartDate);
				}

				if (job.InternshipSkills.IsNotNullOrEmpty())
				{
					var internshipSkills = jobElement.GetOrCreateElement("internship_skills");

					internshipSkills.RemoveElementsByXpath("internship_skill");

					foreach (var internshipSkill in job.InternshipSkills)
					{
						var internshipSkillElement = internshipSkills.CreateElement("internship_skill");
						internshipSkillElement.GetOrCreateElement("id", "id", internshipSkill.Id);
						internshipSkillElement.GetOrCreateElement("name", "name", internshipSkill.Name);
					}
				}

                //Adding Reason For Leaving
                if (job.JobReasonForLeaving.IsNotNull())
                {
                    jobElement.CreateElement("reason_for_leaving", job.JobReasonForLeaving); 
                }
			}

			if (monthsExperience != 0)
				experienceElement.CreateElement("months_experience", monthsExperience);
		}

		#endregion

		#region  Set the Education History

		/// <summary>
		/// Sets the education history.
		/// </summary>
		/// <param name="educationElement">The education node.</param>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <param name="resume">The candidate.</param>
		private static void SetEducationHistory(this XElement educationElement, IRuntimeContext runtimeContext, ref ResumeModel resume)
		{
			if (resume.ResumeContent.EducationInfo.IsNull())
				return;

			var tempEduInfo = resume.ResumeContent.EducationInfo;

			if (tempEduInfo.NCRCConfirmation.IsNotNull())
				educationElement.GetOrCreateElement("ncrc_confirmation", "ncrc_confirmation", tempEduInfo.NCRCConfirmation);
			else
				educationElement.RemoveElementsByXpath("ncrc_confirmation");

			if (tempEduInfo.NCRCLevel.HasValue)
				educationElement.GetOrCreateElement("ncrc_level", "ncrc_level", tempEduInfo.NCRCLevel);
			else
				educationElement.RemoveElementsByXpath("ncrc_level");

			if (tempEduInfo.NCRCLevelName.IsNotNullOrEmpty())
				educationElement.GetOrCreateElement("ncrc_level_name", "ncrc_level_name", tempEduInfo.NCRCLevelName);
			else
				educationElement.RemoveElementsByXpath("ncrc_level_name");

			if (tempEduInfo.NCRCStateId.HasValue)
				educationElement.GetOrCreateElement("ncrc_state_id", "ncrc_state_id", tempEduInfo.NCRCStateId);
			else
				educationElement.RemoveElementsByXpath("ncrc_state_id");

			if (tempEduInfo.NCRCStateName.IsNotNullOrEmpty())
				educationElement.GetOrCreateElement("ncrc_state_name", "ncrc_state_name", tempEduInfo.NCRCStateName);
			else
				educationElement.RemoveElementsByXpath("ncrc_state_name");

			if (tempEduInfo.NCRCIssueDate.HasValue)
				educationElement.GetOrCreateElement("ncrc_issue_date", "ncrc_issue_date", ((DateTime)tempEduInfo.NCRCIssueDate).ToString("MM/dd/yyyy"));
			else
				educationElement.RemoveElementsByXpath("ncrc_issue_date");

			if (tempEduInfo.NCRCDisplayed.IsNotNull())
				educationElement.GetOrCreateElement("ncrc_displayed", "ncrc_displayed", tempEduInfo.NCRCDisplayed);
			else
				educationElement.RemoveElementsByXpath("ncrc_confirmation");

			if (tempEduInfo.MostExperienceIndustries != null)
			{
				var experElement = educationElement.GetOrCreateElement("mostexperienceindustries");
				var experIds = tempEduInfo.MostExperienceIndustries;
				foreach (var industryType in experIds)
				{
					experElement.CreateElement("industrycode", industryType);
				}
			}

			if (tempEduInfo.TargetIndustries != null)
			{
				var shiftElement = educationElement.GetOrCreateElement("targetindustries");
				var shiftIds = tempEduInfo.TargetIndustries;
				foreach (var shiftType in shiftIds)
				{
					shiftElement.CreateElement("industrycode", shiftType);
				}
			}

			// Set the School Status
			if (tempEduInfo.SchoolStatus.HasValue)
				educationElement.GetOrCreateElement("school_status_cd", "school_status_cd", (int)tempEduInfo.SchoolStatus);

			// Set the Education Level
			if (tempEduInfo.EducationLevel.HasValue)
				educationElement.GetOrCreateElement("norm_edu_level_cd", "norm_edu_level_cd", (int)tempEduInfo.EducationLevel);

			educationElement.RemoveElementsByXpath("school");
			if (tempEduInfo.Schools.IsNull())
				return;
			// Set the Education History
			foreach (var schoolInfo in tempEduInfo.Schools)
			{
				var schoolElement = educationElement.CreateElement("school");

				if (!string.IsNullOrEmpty(schoolInfo.Institution))
					schoolElement.CreateElement("institution", schoolInfo.Institution);

				if (schoolInfo.CompletionDate.HasValue)
					schoolElement.CreateElement("completiondate", ((DateTime)schoolInfo.CompletionDate).ToString("MM/yyyy"));

				if (schoolInfo.ExpectedCompletionDate.HasValue)
					schoolElement.CreateElement("expectedcompletiondate", ((DateTime)schoolInfo.ExpectedCompletionDate).ToString("MM/yyyy"));

				if (!string.IsNullOrEmpty(schoolInfo.Degree))
					schoolElement.CreateElement("degree", schoolInfo.Degree);

				if (!string.IsNullOrEmpty(schoolInfo.Major))
					schoolElement.CreateElement("major", schoolInfo.Major);

				if (schoolInfo.SchoolAddress != null)
				{
					var schoolAddress = schoolElement.CreateElement("address");

					if (!string.IsNullOrEmpty(schoolInfo.SchoolAddress.City))
						schoolAddress.CreateElement("city", schoolInfo.SchoolAddress.City);

					if (schoolInfo.SchoolAddress.StateId.IsNotNull())
						schoolAddress.CreateElement("state", GetLookupExternalId(runtimeContext, LookupTypes.States, (long)schoolInfo.SchoolAddress.StateId));
					if (!string.IsNullOrEmpty(schoolInfo.SchoolAddress.StateName))
						schoolAddress.CreateElement("state_fullname", schoolInfo.SchoolAddress.StateName);

					if (schoolInfo.SchoolAddress.CountryId.IsNotNull())
						schoolAddress.CreateElement("country", GetLookupExternalId(runtimeContext, LookupTypes.Countries, (long)schoolInfo.SchoolAddress.CountryId));
					if (!string.IsNullOrEmpty(schoolInfo.SchoolAddress.CountryName))
						schoolAddress.CreateElement("country_fullname", schoolInfo.SchoolAddress.CountryName);
				}

				if (!string.IsNullOrEmpty(schoolInfo.Courses))
					schoolElement.CreateElement("courses", schoolInfo.Courses);

				if (!string.IsNullOrEmpty(schoolInfo.Honors))
					schoolElement.CreateElement("honors", schoolInfo.Honors);

				if (schoolInfo.GradePointAverage.IsNotNull())
				{
					var gpa = schoolElement.CreateElement("gpa", schoolInfo.GradePointAverage.RawGradePointAverage); //schoolInfo.GPA.Value.HasValue ? schoolInfo.GPA.Value.ToString() : schoolInfo.GPA.RawGPA
					if (schoolInfo.GradePointAverage.Max.HasValue)
						gpa.CreateAttribute("max", schoolInfo.GradePointAverage.Max);
				}

				if (!string.IsNullOrEmpty(schoolInfo.Activities))
					schoolElement.CreateElement("activities", schoolInfo.Activities);

				if (schoolInfo.SchoolId.IsNotNull())
					schoolElement.SetAttributeValue("id", schoolInfo.SchoolId);
			}
		}

		#endregion

		#region  Set the Certifications

		/// <summary>
		/// Sets the certifications.
		/// </summary>
		/// <param name="certificatesElement">The skills node.</param>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <param name="resume">The candidate.</param>
		private static void SetCertifications(this XElement certificatesElement, IRuntimeContext runtimeContext, ref ResumeModel resume)
		{
			if (resume.ResumeContent.Skills.Certifications.IsNull())
				return;

			var tempCerts = resume.ResumeContent.Skills.Certifications.OrderBy(c => c.Certificate, StringComparer.InvariantCultureIgnoreCase);

			var certElements = certificatesElement.GetOrCreateElement("courses").GetOrCreateElement("certifications");
			certElements.RemoveElementsByXpath("certification");

			foreach (var cert in tempCerts)
			{
				if (string.IsNullOrEmpty(cert.Certificate))
					continue;

				var certElement = certElements.CreateElement("certification");
				certElement.CreateElement("certificate", cert.Certificate);

				if (cert.CompletionDate.HasValue)
					certElement.CreateElement("completion_date", ((DateTime)cert.CompletionDate).ToString("MM/yyyy"));
				if (cert.ExpectedCompletionDate.HasValue)
					certElement.CreateElement("expected_completion_date", ((DateTime)cert.ExpectedCompletionDate).ToString("MM/yyyy"));

				if (!string.IsNullOrEmpty(cert.OrganizationName))
					certElement.CreateElement("organization_name", cert.OrganizationName);

				var certAddress = cert.CertificationAddress;
				if (!certAddress.IsNotNull()) continue;
				var certAddressElement = certElement.CreateElement("address");

				if (!string.IsNullOrEmpty(certAddress.City))
					certAddressElement.CreateElement("city", certAddress.City);

				if (certAddress.StateId.IsNotNull())
					certAddressElement.CreateElement("state", GetLookupExternalId(runtimeContext, LookupTypes.States, (long)certAddress.StateId));
				if (!string.IsNullOrEmpty(certAddress.StateName))
					certAddressElement.CreateElement("state_fullname", certAddress.StateName);

				if (certAddress.CountryId.IsNotNull())
					certAddressElement.CreateElement("country", GetLookupExternalId(runtimeContext, LookupTypes.Countries, (long)certAddress.CountryId));
				if (!string.IsNullOrEmpty(certAddress.CountryName))
					certAddressElement.CreateElement("country_fullname", certAddress.CountryName);
			}
		}

		#endregion

		#region  Set the Drivers License

		/// <summary>
		/// Sets the drivers license.
		/// </summary>
		/// <param name="personalElement">The personal node.</param>
		/// <param name="resume">The candidate.</param>
		private static void SetDriversLicense(this XElement personalElement, IRuntimeContext runtimeContext, ref ResumeModel resume)
		{
			if (resume.ResumeContent.Profile.License.IsNull())
				return;

			var tempLicense = resume.ResumeContent.Profile.License;

			var licenseElement = personalElement.GetOrCreateElement("license");
			licenseElement.RemoveNodes();

			if (tempLicense != null && tempLicense.HasDriverLicense.HasValue)
				licenseElement.GetOrCreateElement("driver_flag", "driver_flag", ((bool)tempLicense.HasDriverLicense ? "1" : "0"));
			if (tempLicense == null || (tempLicense.HasDriverLicense.HasValue && !(bool)tempLicense.HasDriverLicense))
				return;

			if (tempLicense.DriverStateId != null)
				licenseElement.GetOrCreateElement("driver_state", "driver_state", GetLookupExternalId(runtimeContext, LookupTypes.States, (long)tempLicense.DriverStateId));
			if (!string.IsNullOrEmpty(tempLicense.DriverStateName))
				licenseElement.GetOrCreateElement("state_fullname", "state_fullname", tempLicense.DriverStateName);

			if (tempLicense.DrivingLicenceClassId.IsNotNull())
			{
				if (tempLicense.DrivingLicenceClassId != null)
					licenseElement.GetOrCreateElement("driver_class", "driver_class", GetLookupExternalId(runtimeContext, LookupTypes.DrivingLicenceClasses, (long)tempLicense.DrivingLicenceClassId));
				licenseElement.GetOrCreateElement("driver_class_text", "driver_class_text", tempLicense.DriverClassText);
			}

			if (tempLicense.DrivingLicenceEndorsementIds.IsNull())
				tempLicense.DrivingLicenceEndorsementIds = new List<long>();

			var endorsementTypes = runtimeContext.Helpers.Lookup.GetLookup(LookupTypes.DrivingLicenceEndorsements);

			foreach (LookupItemView type in endorsementTypes)
			{
				licenseElement.GetOrCreateElement(type.ExternalId, type.ExternalId, tempLicense.DrivingLicenceEndorsementIds.Contains(Convert.ToInt64(type.Id)) ? "-1" : "0");
			}
		}

		#endregion

		#region  Set the Military Service

		/// <summary>
		/// Sets the military service.
		/// </summary>
		/// <param name="personalElement">The personal node.</param>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <param name="resume">The candidate.</param>
		private static void SetMilitaryService(this XElement personalElement, IRuntimeContext runtimeContext, ref ResumeModel resume)
		{
			if (resume.ResumeContent.Profile.Veteran.IsNull())
				return;

			var tempVeteran = resume.ResumeContent.Profile.Veteran;

			if (tempVeteran == null)
				return;

			var veteranElement = personalElement.GetOrCreateElement("veteran");
			veteranElement.RemoveAll();

            if (tempVeteran.IsVeteran.IsNotNull())
			veteranElement.GetOrCreateElement("vet_flag", "vet_flag", (bool)tempVeteran.IsVeteran ? "-1" : "0");

			if (tempVeteran.VeteranPriorityServiceAlertsSubscription.IsNotNull())
				veteranElement.GetOrCreateElement("subscribe_vps", "subscribe_vps", tempVeteran.VeteranPriorityServiceAlertsSubscription ? "-1" : "0");

            if ((tempVeteran.IsVeteran==false) || tempVeteran.IsVeteran.IsNull())
				return;

			if (tempVeteran.IsHomeless.IsNotNull())
				veteranElement.GetOrCreateElement("homeless_flag", "homeless_flag", ((bool)tempVeteran.IsHomeless) ? "-1" : "0");
			if (tempVeteran.AttendedTapCourse.IsNotNull())
				veteranElement.GetOrCreateElement("attended_tap_flag", "attended_tap_flag", ((bool)tempVeteran.AttendedTapCourse) ? "-1" : "0");
			if (tempVeteran.DateAttendedTapCourse.IsNotNull())
				veteranElement.GetOrCreateElement("date_attended_tap", "date_attended_tap", (DateTime)tempVeteran.DateAttendedTapCourse);

			foreach (var history in tempVeteran.History)
			{
				var historyElement = veteranElement.CreateElement("history");

				var employmentStatus = history.EmploymentStatus.HasValue
					? ((int)history.EmploymentStatus).ToString(CultureInfo.InvariantCulture)
					: string.Empty;
				historyElement.GetOrCreateElement("employment_status", "employment_status", employmentStatus);

				if (history.VeteranEra.HasValue)
					historyElement.GetOrCreateElement("vet_era", "vet_era", (int)history.VeteranEra);

				if (history.VeteranDisabilityStatus.HasValue)
					historyElement.GetOrCreateElement("vet_disability_status", "vet_disability_status", (int)history.VeteranDisabilityStatus);

				if (history.TransitionType.HasValue)
					historyElement.GetOrCreateElement("vet_tsm_type_cd", "vet_tsm_type_cd", (int)history.TransitionType);

				if (history.IsCampaignVeteran.IsNotNull())
					historyElement.GetOrCreateElement("campaign_vet_flag", "campaign_vet_flag", ((bool)history.IsCampaignVeteran) ? "-1" : "0");

				if (history.MilitaryDischargeId.IsNotNull())
					historyElement.GetOrCreateElement("vet_discharge", "vet_discharge", GetLookupExternalId(runtimeContext, LookupTypes.MilitaryDischargeTypes, (long)history.MilitaryDischargeId));

				if (history.MilitaryBranchOfServiceId.IsNotNull())
					historyElement.GetOrCreateElement("branch_of_service", "branch_of_service", GetLookupExternalId(runtimeContext, LookupTypes.MilitaryBranchesOfService, (long)history.MilitaryBranchOfServiceId));

				if (history.RankId.IsNotNull())
					historyElement.GetOrCreateElement("rank", "rank", GetLookupExternalId(runtimeContext, LookupTypes.MilitaryRanks, (long)history.RankId));

				if (history.MilitaryOccupation.IsNotNullOrEmpty())
				{
					var mocElement = historyElement.GetOrCreateElement("moc", "moc", history.MilitaryOccupation);

					mocElement.RemoveAttributes();
					mocElement.CreateAttribute("code", history.MilitaryOccupationCode);
				}

				if (history.VeteranStartDate.HasValue)
					historyElement.GetOrCreateElement("vet_start_date", "vet_start_date", ((DateTime)history.VeteranStartDate).ToString("MM/dd/yyyy"));
				else
					historyElement.RemoveElementsByXpath("vet_start_date");

				if (history.VeteranEndDate.HasValue)
					historyElement.GetOrCreateElement("vet_end_date", "vet_end_date", ((DateTime)history.VeteranEndDate).ToString("MM/dd/yyyy"));
				else
					historyElement.RemoveElementsByXpath("vet_end_date");

				if (history.UnitOrAffiliation.IsNotNullOrEmpty())
					historyElement.GetOrCreateElement("unit_affiliation", "unit_affiliation", history.UnitOrAffiliation);
			}

			veteranElement.RemoveElementsByXpath("mocronet");
			if (tempVeteran.MOCRonetInfo.IsNotNullOrEmpty())
			{
				veteranElement.GetOrCreateElement("mocronet", "mocronet");
				foreach (var ronet in tempVeteran.MOCRonetInfo.Keys)
					veteranElement.Element("mocronet").CreateElement("ronet", ronet).CreateAttribute("starrating", tempVeteran.MOCRonetInfo[ronet]);
			}

			if (resume.Special.IsNotNull() && resume.Special.IsPastFiveYearsVeteran.HasValue)
				veteranElement.GetOrCreateElement("past_five_years_veteran", "past_five_years_veteran", ((bool)resume.Special.IsPastFiveYearsVeteran ? "1" : "0"));
		}

		#endregion

		#region  Set the Skills

		private static void SetSkillRollUp(this XElement skillRollupElement, ref ResumeModel resume)
		{
			var tempSkills = resume.ResumeContent.Skills;

			if (tempSkills.IsNull()) return;

			if (!tempSkills.Skills.IsNotNull() || tempSkills.Skills.Count <= 0) return;

			skillRollupElement.RemoveElementsByXpath("canonskill");

			foreach (var skill in tempSkills.Skills)
			{
				var canonSkillElement = skillRollupElement.CreateElement("canonskill");
				canonSkillElement.CreateAttribute("name", skill);
			}
		}

		/// <summary>
		/// Sets the skills.
		/// </summary>
		/// <param name="skillsElement">The skills node.</param>
		/// <param name="resume">The candidate.</param>
		/// <param name="runtimeContext">The runtime context.</param>
		private static void SetSkills(this XElement skillsElement, ref ResumeModel resume, IRuntimeContext runtimeContext)
		{
			var tempSkills = resume.ResumeContent.Skills;

			if (tempSkills.IsNull()) return;

			if (tempSkills.Skills.IsNotNull() && tempSkills.Skills.Count > 0)
			{
				skillsElement.GetOrCreateElement("skills", "skills", string.Join(", ", (from s in tempSkills.Skills
					let skill = s.Trim()
					where !string.IsNullOrEmpty(skill)
					orderby skill.ToLower()
					select skill)));
			}

			var languageSkills = skillsElement.GetOrCreateElement("languages");
			if (tempSkills.LanguageProficiencies.IsNotNullOrEmpty())
			{
				languageSkills.RemoveElementsByXpath("language");
				foreach (var language in tempSkills.LanguageProficiencies)
				{
					var langElement = languageSkills.CreateElement("language");
					langElement.CreateElement("language", language.Language);

					if (language.Proficiency.IsNotNull())
					{
						var languageExternalId = GetLookupExternalId(runtimeContext, LookupTypes.LanguageProficiencies, language.Proficiency);
						langElement.CreateElement("proficiency", languageExternalId);
					}
				}
			}

			var internshipSkills = skillsElement.GetOrCreateElement("internship_skills");
			if (tempSkills.SkillsAlreadyCaptured.HasValue)
				internshipSkills.GetOrCreateElement("skills_already_captured", "skills_already_captured", tempSkills.SkillsAlreadyCaptured.Value ? "1" : "0");

			if (tempSkills.InternshipSkills.IsNotNullOrEmpty())
			{
				internshipSkills.RemoveElementsByXpath("internship_skill");

				foreach (var internshipSkill in tempSkills.InternshipSkills)
				{
					var internshipSkillElement = internshipSkills.CreateElement("internship_skill");
					internshipSkillElement.GetOrCreateElement("id", "id", internshipSkill.Id);
					internshipSkillElement.GetOrCreateElement("name", "name", internshipSkill.Name);
				}
			}
		}

		#endregion

		#region  Set the Preferences

		/// <summary>
		/// Sets the preferences.
		/// </summary>
		/// <param name="personalElement">The personal node.</param>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <param name="resume">The candidate.</param>
		private static void SetPreferences(this XElement personalElement, IRuntimeContext runtimeContext, ref ResumeModel resume)
		{
			if (resume.Special.Preferences.IsNull())
				return;

			var tempPref = resume.Special.Preferences;
			var prefElement = personalElement.GetOrCreateElement("preferences");
			if (tempPref.Salary.HasValue)
			{
				prefElement.GetOrCreateElement("sal", "sal", (double)tempPref.Salary);
				prefElement.GetOrCreateElement("salary_unit_cd", "salary_unit_cd", (tempPref.SalaryFrequencyId.HasValue ? Convert.ToInt32(GetLookupExternalId(runtimeContext, LookupTypes.Frequencies, (long)tempPref.SalaryFrequencyId)) : Defaults.DomainValueDefaults.SalaryUnitCode));
			}

			if (tempPref.IsResumeSearchable.HasValue)
				prefElement.GetOrCreateElement("resume_searchable", "resume_searchable", (bool)tempPref.IsResumeSearchable ? "1" : "0");

			if (tempPref.BridgestoOppsInterest.HasValue)
				prefElement.GetOrCreateElement("bridges_opp_interest", "bridges_opp_interest", tempPref.BridgestoOppsInterest.GetValueOrDefault(false) ? "1" : "0");

			if (tempPref.IsContactInfoVisible.HasValue)
				prefElement.GetOrCreateElement("contact_info_visible", "contact_info_visible", (bool)tempPref.IsContactInfoVisible ? "1" : "0");

			if (tempPref.PostingsInterestedIn.IsNotNull())
				prefElement.GetOrCreateElement("postings_interested_in", "postings_interested_in", ((int)tempPref.PostingsInterestedIn).ToString());

			if (tempPref.WillingToRelocate.HasValue)
				prefElement.GetOrCreateElement("relocate", "relocate", tempPref.WillingToRelocate.Value ? "-1" : "0");

			// Set Shift Period
			if (tempPref.ShiftDetail != null)
			{
				var shiftElement = prefElement.GetOrCreateElement("shift");
				var shiftIds = tempPref.ShiftDetail.ShiftTypeIds;
				if (shiftIds.IsNotNullOrEmpty())
				{
					foreach (var shiftType in shiftIds.Select(id => GetLookupExternalId(runtimeContext, LookupTypes.WorkShifts, id)))
					{
						switch (shiftType)
						{
								#region map externalId to node

							case "1":
								shiftElement.GetOrCreateElement("shift_first_flag", "shift_first_flag", "-1");
								break;

							case "2":
								shiftElement.GetOrCreateElement("shift_second_flag", "shift_second_flag", "-1");
								break;

							case "3":
								shiftElement.GetOrCreateElement("shift_third_flag", "shift_third_flag", "-1");
								break;

							case "4":
								shiftElement.GetOrCreateElement("shift_rotating_flag", "shift_rotating_flag", "-1");
								break;

							case "5":
								shiftElement.GetOrCreateElement("shift_split_flag", "shift_split_flag", "-1");
								break;

							case "6":
								shiftElement.GetOrCreateElement("shift_varies_flag", "shift_varies_flag", "-1");
								break;

								#endregion
						}
					}
				}

				if (tempPref.ShiftDetail.WorkWeekId.HasValue)
					shiftElement.GetOrCreateElement("work_week", "work_week", GetLookupExternalId(runtimeContext, LookupTypes.WorkWeeks, (long)tempPref.ShiftDetail.WorkWeekId));

				if (tempPref.ShiftDetail.WorkDurationId.HasValue)
					shiftElement.GetOrCreateElement("work_type", "work_type", GetLookupExternalId(runtimeContext, LookupTypes.Durations, (long)tempPref.ShiftDetail.WorkDurationId));

				if (tempPref.ShiftDetail.WorkOverTime.HasValue)
					shiftElement.GetOrCreateElement("work_over_time", "work_over_time", (bool)tempPref.ShiftDetail.WorkOverTime ? "-1" : "0");
			}

			if (tempPref.ZipPreference.IsNotNull())
			{
				var tempDesiredZips = prefElement.GetOrCreateElement("desired_zips");
				tempDesiredZips.RemoveElementsByXpath("desired_zip");
				foreach (var zip in tempPref.ZipPreference)
				{
					if (string.IsNullOrEmpty(zip.Zip))
						continue;

					var zipElement = tempDesiredZips.CreateElement("desired_zip");
					zipElement.CreateElement("zip", zip.Zip);
					if (zip.RadiusId != null)
						zipElement.CreateElement("radius", GetLookupExternalId(runtimeContext, LookupTypes.Radiuses, (long)zip.RadiusId));
					if (zip.StateId.IsNotNull())
						zipElement.CreateElement("statecode", GetLookupExternalId(runtimeContext, LookupTypes.States, (long)zip.StateId));
					if (zip.Choice.HasValue)
						zipElement.CreateElement("choice", zip.Choice);
				}
			}
			else
				prefElement.RemoveElementsByXpath("desired_zips");

			if (tempPref.CityPreference.IsNotNull())
			{
				var tempDesiredCities = prefElement.GetOrCreateElement("desired_cities");
				tempDesiredCities.RemoveElementsByXpath("desired_city");
				foreach (var city in tempPref.CityPreference)
				{
					if (string.IsNullOrEmpty(city.CityName))
						continue;

					var cityElement = tempDesiredCities.CreateElement("desired_city");
					cityElement.CreateElement("city", city.CityName);
					if (city.StateId.IsNotNull())
						cityElement.CreateElement("state", GetLookupExternalId(runtimeContext, LookupTypes.States, (long)city.StateId));
					if (!string.IsNullOrEmpty(city.StateName))
						cityElement.CreateElement("state_fullname", city.StateName);
					if (city.Choice.HasValue)
						cityElement.CreateElement("choice", city.Choice);
				}
			}
			else
				prefElement.RemoveElementsByXpath("desired_cities");

			if (tempPref.SearchInMyState.HasValue && (bool)tempPref.SearchInMyState)
			{
				var stateCode = GetLookupExternalId(runtimeContext, LookupTypes.States, (long)resume.ResumeContent.SeekerContactDetails.PostalAddress.StateId);

				if (tempPref.StatePreference.IsNull())
					tempPref.StatePreference = new List<DesiredState>();
				if (!tempPref.StatePreference.Exists(x => x.StateCode == stateCode))
					tempPref.StatePreference.Add(new DesiredState
					{
						Choice = 1,
						StateCode = stateCode,
						StateName = resume.ResumeContent.SeekerContactDetails.PostalAddress.StateName
					});
			}

			//if (tempPref.HomeBasedJobPostings.GetValueOrDefault())
			//	prefElement.CreateElement("home_based_jobpostings", "1");

			if (tempPref.StatePreference.IsNotNull())
			{
				var tempDesiredStates = prefElement.GetOrCreateElement("desired_states");
				tempDesiredStates.RemoveElementsByXpath("desired_state");
				foreach (var state in tempPref.StatePreference)
				{
					if (string.IsNullOrEmpty(state.StateCode))
						continue;

					var stateElement = tempDesiredStates.CreateElement("desired_state");
					if (tempPref.SearchInMyState.HasValue && (bool)tempPref.SearchInMyState)
						stateElement.CreateAttribute("homestate", "1");
					stateElement.CreateElement("code", state.StateCode);
					if (!string.IsNullOrEmpty(state.StateName))
						stateElement.CreateElement("value", state.StateName);
					if (state.Choice.HasValue)
						stateElement.CreateElement("choice", state.Choice);
				}
			}
			else
				prefElement.RemoveElementsByXpath("desired_states");

			if (tempPref.MSAPreference.IsNotNull())
			{
				var tempDesiredMSAs = prefElement.GetOrCreateElement("desired_msas");
				tempDesiredMSAs.RemoveElementsByXpath("desired_msa");
				foreach (var stateArea in tempPref.MSAPreference)
				{
					if (stateArea.MSAId.IsNull())
						continue;

					var msastateElement = tempDesiredMSAs.CreateElement("desired_msa");
					msastateElement.CreateElement("msa", GetLookupExternalId(runtimeContext, LookupTypes.StateMetropolitanStatisticalAreas, (long)stateArea.MSAId));
					if (stateArea.StateId.IsNotNull())
						msastateElement.CreateElement("code", GetLookupExternalId(runtimeContext, LookupTypes.States, (long)stateArea.StateId));
					if (!string.IsNullOrEmpty(stateArea.StateName))
						msastateElement.CreateElement("value", stateArea.StateName);
					if (stateArea.Choice.HasValue)
						msastateElement.CreateElement("choice", stateArea.Choice);
				}
			}
			else
				prefElement.RemoveElementsByXpath("desired_msas");

		}

		#endregion

		#region Set the Additional Sections

		/// <summary>
		/// Sets additional resume sections.
		/// </summary>
		/// <param name="resdocElement">The resume node.</param>
		/// <param name="resume">The candidate.</param>
		private static void SetAdditionalSections(this XElement resdocElement, ref ResumeModel resume)
		{
			if (resume.ResumeContent.AdditionalResumeSections.IsNull() || resume.ResumeContent.AdditionalResumeSections.Count == 0)
				return;

			var resumeSections = resume.ResumeContent.AdditionalResumeSections;
			foreach (ResumeSectionType sectionType in Enum.GetValues(typeof(ResumeSectionType)))
			{
				var section = resumeSections.FirstOrDefault(sec => sec.SectionType == sectionType);
				//if (section.SectionContent.IsNull())
				//    continue;

				if (section == null) continue;
				var sectionText = section.IsNotNull() ? (string)section.SectionContent : null;
				XElement eleSection;
				switch (sectionType)
				{
					case ResumeSectionType.Affiliations:
						if (string.IsNullOrEmpty(sectionText))
						{
							if (resdocElement.XPathSelectElement("//professional/affiliations") != null)
								resdocElement.RemoveElementsByXpath("//professional/affiliations");
						}
						else
						{
							eleSection = resdocElement.GetOrCreateElement("resume").GetOrCreateElement("professional").GetOrCreateElement("affiliations", "affiliations", sectionText);
							eleSection.RemoveAttributes();
							eleSection.CreateAttribute("include", section.UseSectionInResume.HasValue && !(bool)section.UseSectionInResume ? "0" : "1");
						}
						break;

					case ResumeSectionType.Description:
						if (string.IsNullOrEmpty(sectionText))
						{
							if (resdocElement.XPathSelectElement("//professional/description") != null)
								resdocElement.RemoveElementsByXpath("//professional/description");
						}
						else
						{
							eleSection = resdocElement.GetOrCreateElement("resume").GetOrCreateElement("professional").GetOrCreateElement("description", "description", sectionText);
							eleSection.RemoveAttributes();
							eleSection.CreateAttribute("include", section.UseSectionInResume.HasValue && !(bool)section.UseSectionInResume ? "0" : "1");
						}
						break;

					case ResumeSectionType.Honors:
						if (string.IsNullOrEmpty(sectionText))
						{
							if (resdocElement.XPathSelectElement("//special/honors") != null)
								resdocElement.RemoveElementsByXpath("//special/honors");
						}
						else
						{
							eleSection = resdocElement.GetOrCreateElement("special").GetOrCreateElement("honors", "honors", sectionText);
							eleSection.RemoveAttributes();
							eleSection.CreateAttribute("include", section.UseSectionInResume.HasValue && !(bool)section.UseSectionInResume ? "0" : "1");
						}
						break;

					case ResumeSectionType.Interests:
						if (string.IsNullOrEmpty(sectionText))
						{
							if (resdocElement.XPathSelectElement("//special/interests") != null)
								resdocElement.RemoveElementsByXpath("//special/interests");
						}
						else
						{
							eleSection = resdocElement.GetOrCreateElement("special").GetOrCreateElement("interests", "interests", sectionText);
							eleSection.RemoveAttributes();
							eleSection.CreateAttribute("include", section.UseSectionInResume.HasValue && !(bool)section.UseSectionInResume ? "0" : "1");
						}
						break;

					case ResumeSectionType.Internships:
						if (string.IsNullOrEmpty(sectionText))
						{
							if (resdocElement.XPathSelectElement("//special/internships") != null)
								resdocElement.RemoveElementsByXpath("//special/internships");
						}
						else
						{
							eleSection = resdocElement.GetOrCreateElement("special").GetOrCreateElement("internships", "internships", sectionText);
							eleSection.RemoveAttributes();
							eleSection.CreateAttribute("include", section.UseSectionInResume.HasValue && !(bool)section.UseSectionInResume ? "0" : "1");
						}
						break;

					case ResumeSectionType.Objective:
						if (string.IsNullOrEmpty(sectionText))
						{
							if (resdocElement.XPathSelectElement("//summary/objective") != null)
								resdocElement.RemoveElementsByXpath("//summary/objective");
						}
						else
						{
							eleSection = resdocElement.GetOrCreateElement("resume").GetOrCreateElement("summary").GetOrCreateElement("objective", "objective", sectionText);
							eleSection.RemoveAttributes();
							eleSection.CreateAttribute("include", section.UseSectionInResume.HasValue && !(bool)section.UseSectionInResume ? "0" : "1");
						}
						break;

					case ResumeSectionType.Personals:
						if (string.IsNullOrEmpty(sectionText))
						{
							if (resdocElement.XPathSelectElement("//special/personal") != null)
								resdocElement.RemoveElementsByXpath("//special/personal");
						}
						else
						{
							eleSection = resdocElement.GetOrCreateElement("special").GetOrCreateElement("personal", "personal", sectionText);
							eleSection.RemoveAttributes();
							eleSection.CreateAttribute("include", section.UseSectionInResume.HasValue && !(bool)section.UseSectionInResume ? "0" : "1");
						}
						break;

					case ResumeSectionType.ProfessionalDevelopment:
						if (string.IsNullOrEmpty(sectionText))
						{
							if (resdocElement.XPathSelectElement("//professional/description") != null)
								resdocElement.RemoveElementsByXpath("//professional/description");
						}
						else
						{
							eleSection = resdocElement.GetOrCreateElement("professional").GetOrCreateElement("description", "description", sectionText);
							eleSection.RemoveAttributes();
							eleSection.CreateAttribute("include", section.UseSectionInResume.HasValue && !(bool)section.UseSectionInResume ? "0" : "1");
						}
						break;

					case ResumeSectionType.Publications:
						if (string.IsNullOrEmpty(sectionText))
						{
							if (resdocElement.XPathSelectElement("//professional/publications") != null)
								resdocElement.RemoveElementsByXpath("//professional/publications");
						}
						else
						{
							eleSection = resdocElement.GetOrCreateElement("resume").GetOrCreateElement("professional").GetOrCreateElement("publications", "publications", sectionText);
							eleSection.RemoveAttributes();
							eleSection.CreateAttribute("include", section.UseSectionInResume.HasValue && !(bool)section.UseSectionInResume ? "0" : "1");
						}
						break;

					case ResumeSectionType.References:
						if (string.IsNullOrEmpty(sectionText))
						{
							if (resdocElement.XPathSelectElement("//references") != null)
								resdocElement.RemoveElementsByXpath("//references");
						}
						else
						{
							eleSection = resdocElement.GetOrCreateElement("resume").GetOrCreateElement("references", "references", sectionText);
							eleSection.RemoveAttributes();
							eleSection.CreateAttribute("include", section.UseSectionInResume.HasValue && !(bool)section.UseSectionInResume ? "0" : "1");
						}
						break;

					case ResumeSectionType.VolunteerActivities:
						if (string.IsNullOrEmpty(sectionText))
						{
							if (resdocElement.XPathSelectElement("//special/volunteeractivities") != null)
								resdocElement.RemoveElementsByXpath("//special/volunteeractivities");
						}
						else
						{
							eleSection = resdocElement.GetOrCreateElement("special").GetOrCreateElement("volunteeractivities", "volunteeractivities", sectionText);
							eleSection.RemoveAttributes();
							eleSection.CreateAttribute("include", section.UseSectionInResume.HasValue && !(bool)section.UseSectionInResume ? "0" : "1");
						}
						break;

					case ResumeSectionType.TechnicalSkills:
						if (string.IsNullOrEmpty(sectionText))
						{
							if (resdocElement.XPathSelectElement("//special/technicalskills") != null)
								resdocElement.RemoveElementsByXpath("//special/technicalskills");
						}
						else
						{
							eleSection = resdocElement.GetOrCreateElement("special").GetOrCreateElement("technicalskills", "technicalskills", sectionText);
							eleSection.RemoveAttributes();
							eleSection.CreateAttribute("include", section.UseSectionInResume.HasValue && !(bool)section.UseSectionInResume ? "0" : "1");
						}
						break;

					case ResumeSectionType.ThesisMajorProjects:
						if (string.IsNullOrEmpty(sectionText))
						{
							if (resdocElement.XPathSelectElement("//special/thesismajorprojects") != null)
								resdocElement.RemoveElementsByXpath("//special/thesismajorprojects");
						}
						else
						{
							eleSection = resdocElement.GetOrCreateElement("special").GetOrCreateElement("thesismajorprojects", "thesismajorprojects", sectionText);
							eleSection.RemoveAttributes();
							eleSection.CreateAttribute("include", section.UseSectionInResume.HasValue && !(bool)section.UseSectionInResume ? "0" : "1");
						}
						break;

					case ResumeSectionType.Branding:
						if (string.IsNullOrEmpty(sectionText))
						{
							if (resdocElement.XPathSelectElement("//special/branding") != null)
								resdocElement.RemoveElementsByXpath("//special/branding");
						}
						else
						{
							eleSection = resdocElement.GetOrCreateElement("special").GetOrCreateElement("branding", "branding", sectionText);
							eleSection.RemoveAttributes();
							eleSection.CreateAttribute("include", section.UseSectionInResume.HasValue && !(bool)section.UseSectionInResume ? "0" : "1");
						}
						break;
				}
			}
		}

		#endregion

		#region Set Hiding Information

		private static void SetHidingInformation(this XElement hidingElement, ref ResumeModel resume)
		{
			if (resume.Special.HideInfo.IsNull())
				return;

			var tempHideInfo = resume.Special.HideInfo;
			hidingElement.GetOrCreateElement("hide_daterange", "hide_daterange", (tempHideInfo.HideDateRange ? "1" : "0"));
			hidingElement.GetOrCreateElement("hide_eduDates", "hide_eduDates", (tempHideInfo.HideEducationDates ? "1" : "0"));
			hidingElement.GetOrCreateElement("hide_militaryserviceDates", "hide_militaryserviceDates", (tempHideInfo.HideMilitaryServiceDates ? "1" : "0"));
			hidingElement.GetOrCreateElement("hide_workDates", "hide_workDates", (tempHideInfo.HideWorkDates ? "1" : "0"));
			hidingElement.GetOrCreateElement("hide_contact", "hide_contact", (tempHideInfo.HideContact ? "1" : "0"));
			hidingElement.GetOrCreateElement("hide_name", "hide_name", (tempHideInfo.HideName ? "1" : "0"));
			hidingElement.GetOrCreateElement("hide_email", "hide_email", (tempHideInfo.HideEmail ? "1" : "0"));
			hidingElement.GetOrCreateElement("hide_homePhoneNumber", "hide_homePhoneNumber", (tempHideInfo.HideHomePhoneNumber ? "1" : "0"));
			hidingElement.GetOrCreateElement("hide_workPhoneNumber", "hide_workPhoneNumber", (tempHideInfo.HideWorkPhoneNumber ? "1" : "0"));
			hidingElement.GetOrCreateElement("hide_cellPhoneNumber", "hide_cellPhoneNumber", (tempHideInfo.HideCellPhoneNumber ? "1" : "0"));
			hidingElement.GetOrCreateElement("hide_faxPhoneNumber", "hide_faxPhoneNumber", (tempHideInfo.HideFaxPhoneNumber ? "1" : "0"));
			hidingElement.GetOrCreateElement("hide_nonUSPhoneNumber", "hide_nonUSPhoneNumber", (tempHideInfo.HideNonUSPhoneNumber ? "1" : "0"));
			hidingElement.GetOrCreateElement("hide_affiliations", "hide_affiliations", (tempHideInfo.HideAffiliations ? "1" : "0"));
			hidingElement.GetOrCreateElement("hide_certifications_professionallicenses", "hide_certifications_professionallicenses", (tempHideInfo.HideCertificationsAndProfessionalLicenses ? "1" : "0"));
			hidingElement.GetOrCreateElement("hide_employer", "hide_employer", (tempHideInfo.HideEmployer ? "1" : "0"));
			hidingElement.GetOrCreateElement("hide_driver_license", "hide_driver_license", (tempHideInfo.HideDriverLicense ? "1" : "0"));
			hidingElement.GetOrCreateElement("unhide_driver_license", "unhide_driver_license", (tempHideInfo.UnHideDriverLicense ? "1" : "0"));
			hidingElement.GetOrCreateElement("hide_honors", "hide_honors", (tempHideInfo.HideHonors ? "1" : "0"));
			hidingElement.GetOrCreateElement("hide_interests", "hide_interests", (tempHideInfo.HideInterests ? "1" : "0"));
			hidingElement.GetOrCreateElement("hide_internships", "hide_internships", (tempHideInfo.HideInternships ? "1" : "0"));
			hidingElement.GetOrCreateElement("hide_languages", "hide_languages", (tempHideInfo.HideLanguages ? "1" : "0"));
			hidingElement.GetOrCreateElement("hide_veteran", "hide_veteran", (tempHideInfo.HideVeteran ? "1" : "0"));
			hidingElement.GetOrCreateElement("hide_objective", "hide_objective", (tempHideInfo.HideObjective ? "1" : "0"));
			hidingElement.GetOrCreateElement("hide_personalinformation", "hide_personalinformation", (tempHideInfo.HidePersonalInformation ? "1" : "0"));
			hidingElement.GetOrCreateElement("hide_professionaldevelopment", "hide_professionaldevelopment", (tempHideInfo.HideProfessionalDevelopment ? "1" : "0"));
			hidingElement.GetOrCreateElement("hide_publications", "hide_publications", (tempHideInfo.HidePublications ? "1" : "0"));
			hidingElement.GetOrCreateElement("hide_references", "hide_references", (tempHideInfo.HideReferences ? "1" : "0"));
			hidingElement.GetOrCreateElement("hide_skills", "hide_skills", (tempHideInfo.HideSkills ? "1" : "0"));
			hidingElement.GetOrCreateElement("hide_technicalskills", "hide_technicalskills", (tempHideInfo.HideTechnicalSkills ? "1" : "0"));
			hidingElement.GetOrCreateElement("hide_volunteeractivities", "hide_volunteeractivities", (tempHideInfo.HideVolunteerActivities ? "1" : "0"));
			hidingElement.GetOrCreateElement("hide_thesismajorprojects", "hide_thesismajorprojects", (tempHideInfo.HideThesisMajorProjects ? "1" : "0"));
			hidingElement.GetOrCreateElement("hidden_skills", "hidden_skills", tempHideInfo.HiddenSkills.IsNotNullOrEmpty() ? string.Join(",", tempHideInfo.HiddenSkills) : "");
		}

		#endregion

		#endregion

		/// <summary>
		/// Gets the additional resume sections.
		/// </summary>
		/// <returns></returns>
		private static List<AdditionalResumeSection> GetAdditionalResumeSections(XElement resumeElement, IRuntimeContext runtimeContext)
		{

			var temp = new List<AdditionalResumeSection>();
			var appSettings = runtimeContext.AppSettings;
			if (!appSettings.HideObjectives)
			{
				//ResDoc/resume/summary/objective
				if (resumeElement.XPathSelectElement("//summary/objective") != null)
					temp.Add(new AdditionalResumeSection
					{
						SectionType = ResumeSectionType.Objective,
						UseSectionInResume = resumeElement.GetElementValueAsNullableBoolean("//summary/objective/@include"),
						SectionContent = resumeElement.GetElementValueAsString("//summary/objective")
					});
			}

			if (resumeElement.XPathSelectElement("//professional/affiliations") != null)
				temp.Add(new AdditionalResumeSection
				{
					SectionType = ResumeSectionType.Affiliations,
					UseSectionInResume = resumeElement.GetElementValueAsNullableBoolean("//professional/affiliations/@include"),
					SectionContent = resumeElement.GetElementValueAsString("//professional/affiliations")
				});

			if (resumeElement.XPathSelectElement("//professional/publications") != null)
				temp.Add(new AdditionalResumeSection
				{
					SectionType = ResumeSectionType.Publications,
					UseSectionInResume = resumeElement.GetElementValueAsNullableBoolean("//professional/publications/@include"),
					SectionContent = resumeElement.GetElementValueAsString("//professional/publications")
				});

			// Description not currently used in Resume Builder
			/*
      if (resumeElement.XPathSelectElement("//professional/description") != null)
        temp.Add(new AdditionalResumeSection
        {
          SectionType = ResumeSectionType.Description,
          UseSectionInResume = resumeElement.GetElementValueAsNullableBoolean("//professional/description/@include"),
          SectionContent = resumeElement.GetElementValueAsString("//professional/description")
        });
      */

			if (resumeElement.XPathSelectElement("//references") != null)
				temp.Add(new AdditionalResumeSection
				{
					SectionType = ResumeSectionType.References,
					UseSectionInResume = resumeElement.GetElementValueAsNullableBoolean("//references/@include"),
					SectionContent = resumeElement.GetElementValueAsString("//references")
				});

			if (resumeElement.XPathSelectElement("//special/interests") != null)
				temp.Add(new AdditionalResumeSection
				{
					SectionType = ResumeSectionType.Interests,
					UseSectionInResume = resumeElement.GetElementValueAsNullableBoolean("//special/interests/@include"),
					SectionContent = resumeElement.GetElementValueAsString("//special/interests")
				});

			if (resumeElement.XPathSelectElement("//special/internships") != null)
				temp.Add(new AdditionalResumeSection
				{
					SectionType = ResumeSectionType.Internships,
					UseSectionInResume = resumeElement.GetElementValueAsNullableBoolean("//special/internships/@include"),
					SectionContent = resumeElement.GetElementValueAsString("//special/internships")
				});

			if (resumeElement.XPathSelectElement("//special/volunteeractivities") != null)
				temp.Add(new AdditionalResumeSection
				{
					SectionType = ResumeSectionType.VolunteerActivities,
					UseSectionInResume = resumeElement.GetElementValueAsNullableBoolean("//special/volunteeractivities/@include"),
					SectionContent = resumeElement.GetElementValueAsString("//special/volunteeractivities")
				});

			if (resumeElement.XPathSelectElement("//special/personal") != null)
				temp.Add(new AdditionalResumeSection
				{
					SectionType = ResumeSectionType.Personals,
					UseSectionInResume = resumeElement.GetElementValueAsNullableBoolean("//special/personal/@include"),
					SectionContent = resumeElement.GetElementValueAsString("//special/personal")
				});

			if (resumeElement.XPathSelectElement("//professional/description") != null)
				temp.Add(new AdditionalResumeSection
				{
					SectionType = ResumeSectionType.ProfessionalDevelopment,
					UseSectionInResume = resumeElement.GetElementValueAsNullableBoolean("//professional/description/@include"),
					SectionContent = resumeElement.GetElementValueAsString("//professional/description")
				});

			if (resumeElement.XPathSelectElement("//special/honors") != null)
				temp.Add(new AdditionalResumeSection
				{
					SectionType = ResumeSectionType.Honors,
					UseSectionInResume = resumeElement.GetElementValueAsNullableBoolean("//special/honors/@include"),
					SectionContent = resumeElement.GetElementValueAsString("//special/honors")
				});

			if (resumeElement.XPathSelectElement("//special/technicalskills") != null)
				temp.Add(new AdditionalResumeSection
				{
					SectionType = ResumeSectionType.TechnicalSkills,
					UseSectionInResume = resumeElement.GetElementValueAsNullableBoolean("//special/technicalskills/@include"),
					SectionContent = resumeElement.GetElementValueAsString("//special/technicalskills")
				});

			if (resumeElement.XPathSelectElement("//special/thesismajorprojects") != null)
				temp.Add(new AdditionalResumeSection
				{
					SectionType = ResumeSectionType.ThesisMajorProjects,
					UseSectionInResume = resumeElement.GetElementValueAsNullableBoolean("//special/thesismajorprojects/@include"),
					SectionContent = resumeElement.GetElementValueAsString("//special/thesismajorprojects")
				});

			if (resumeElement.XPathSelectElement("//special/branding") != null)
				temp.Add(new AdditionalResumeSection
				{
					SectionType = ResumeSectionType.Branding,
					UseSectionInResume = resumeElement.GetElementValueAsNullableBoolean("//special/branding/@include"),
					SectionContent = resumeElement.GetElementValueAsString("//special/branding")
				});
			// If we are showing the branding statement instead of objectives then add the objectives as a branding staement
			else if (appSettings.ShowBrandingStatement && appSettings.HideObjectives && resumeElement.XPathSelectElement("//summary/objective") != null)
			{
				temp.Add(new AdditionalResumeSection
				{
					SectionType = ResumeSectionType.Branding,
					UseSectionInResume = resumeElement.GetElementValueAsNullableBoolean("//summary/objective/@include"),
					SectionContent = resumeElement.GetElementValueAsString("//summary/objective")
				});
			}

			return temp.Count > 0 ? temp : null;
		}

		private static Dictionary<string, int> GetMocROnetInfo(XNode mocROnetElement)
		{
			var mocRonet = new Dictionary<string, int>();
			foreach (var ronetEle in mocROnetElement.XPathSelectElements("ronet"))
			{
				if (!mocRonet.ContainsKey(ronetEle.Value))
					mocRonet.Add(ronetEle.Value.Replace("S", "9"), ronetEle.GetElementValueAsInt32("@starrating"));
			}
			return mocRonet;
		}

		/// <summary>
		/// Sets the driver license.
		/// </summary>
		/// <param name="resumeElement">The resume Xml element.</param>
		/// <param name="runtimeContext">The runtime context</param>
		/// <param name="driverLicense">The driver license.</param>
		/// <returns></returns>
		private static LicenseInfo SetDriverLicense(XElement resumeElement, IRuntimeContext runtimeContext, XElement driverLicense)
		{
			if (driverLicense.IsNull() || driverLicense.Element("driver_flag").IsNull())
				return null;

			var licence = new LicenseInfo
			{
				HasDriverLicense = driverLicense.GetElementValueAsBoolean("driver_flag"),
				DrivingLicenceClassId = GetLookupId(runtimeContext, resumeElement, "//statements/personal/license/driver_class", LookupTypes.DrivingLicenceClasses),
				DriverClassText = driverLicense.GetElementValueAsString("driver_class_text"),
				DriverStateId = GetLookupId(runtimeContext, resumeElement, "//statements/personal/license/driver_state", LookupTypes.States),
				DriverStateName = driverLicense.GetElementValueAsString("state_fullname"),
				DrivingLicenceEndorsementIds = GetEndorsementType(runtimeContext, driverLicense.XPathSelectElements("*")), //(from et in resumeDoc.XPathSelectElements("/statements/personal/license/*") where (new string[] { "drv_pass_flag", "drv_hazard_flag", "drv_tank_flag", "drv_tankhazard_flag", "drv_double_flag", "drv_airbrake_flag", "drv_bus_flag", "drv_cycle_flag" }).Contains(et.Name.ToString()) select GetEndorsementType(ref et)).ToList()
				DrivingLicenceEndorsements = GetEndorsements(runtimeContext, driverLicense.XPathSelectElements("*"))
			};

			return licence;
		}

		/// <summary>
		/// Gets the type of the endorsement.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <param name="licenseElements">The license elements.</param>
		/// <returns></returns>
		private static List<long> GetEndorsementType(IRuntimeContext runtimeContext, IEnumerable<XElement> licenseElements)
		{
			var endorsementIds = new List<long>();
			if (licenseElements.IsNull())
				return endorsementIds;

			var endorsementTypes = runtimeContext.Helpers.Lookup.GetLookup(LookupTypes.DrivingLicenceEndorsements).Select(x => x.ExternalId).ToList();

			var nodes = licenseElements.DescendantsAndSelf();

			endorsementIds.AddRange((nodes.Where(x => endorsementTypes.Contains(x.Name.ToString()) && x.Value == "-1").Select(x => Convert.ToInt64(GetLookupId(runtimeContext, LookupTypes.DrivingLicenceEndorsements, x.Name.ToString())))));

			return endorsementIds;
		}

		/// <summary>
		/// Gets the type of the endorsement.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <param name="licenseElements">The license elements.</param>
		/// <returns></returns>
		private static List<string> GetEndorsements(IRuntimeContext runtimeContext, IEnumerable<XElement> licenseElements)
		{
			var endorsements = new List<string>();
			if (licenseElements.IsNull())
				return endorsements;

			var endorsementTypes = runtimeContext.Helpers.Lookup.GetLookup(LookupTypes.DrivingLicenceEndorsements).Select(x => x.ExternalId).ToList();

			var nodes = licenseElements.DescendantsAndSelf();

			endorsements.AddRange((nodes.Where(x => endorsementTypes.Contains(x.Name.ToString()) && x.Value == "-1").Select(x => x.Name.ToString())));

			return endorsements;
		}

		private static List<long> GetShiftDetails(IRuntimeContext runtimeContext, XContainer shiftElement)
		{
			var shiftIds = new List<long>();
			if (shiftElement.IsNull())
				return shiftIds;

			var elements = shiftElement.Descendants();

			foreach (var element in elements)
			{
				if (element.Value.Length > 0 && element.Value != "0")
				{
					switch (element.Name.AsString())
					{
							#region map node to external IDs

						case "shift_first_flag":
							shiftIds.Add(Convert.ToInt64(GetLookupId(runtimeContext, LookupTypes.WorkShifts, "1")));
							break;

						case "shift_second_flag":
							shiftIds.Add(Convert.ToInt64(GetLookupId(runtimeContext, LookupTypes.WorkShifts, "2")));
							break;

						case "shift_third_flag":
							shiftIds.Add(Convert.ToInt64(GetLookupId(runtimeContext, LookupTypes.WorkShifts, "3")));
							break;

						case "shift_rotating_flag":
							shiftIds.Add(Convert.ToInt64(GetLookupId(runtimeContext, LookupTypes.WorkShifts, "4")));
							break;

						case "shift_split_flag":
							shiftIds.Add(Convert.ToInt64(GetLookupId(runtimeContext, LookupTypes.WorkShifts, "5")));
							break;

						case "shift_varies_flag":
							shiftIds.Add(Convert.ToInt64(GetLookupId(runtimeContext, LookupTypes.WorkShifts, "6")));
							break;

							#endregion
					}
				}
			}

			return shiftIds;
		}

		private static List<long> GetIndustries(XContainer shiftElement)
		{
			var shiftTypes = new List<long>();
			if (shiftElement.IsNull())
				return shiftTypes;

			var elements = shiftElement.Descendants();

			foreach (var element in elements)
			{
				shiftTypes.Add((long)element);
			}

			return shiftTypes;
		}

		private static List<string> GetShiftTypes(XContainer shiftElement)
		{
			var shiftTypes = new List<string>();
			if (shiftElement.IsNull())
				return shiftTypes;

			var elements = shiftElement.Descendants();

			shiftTypes.AddRange(elements.Where(element => element.Value.Length > 0 && element.Value != "0").Select(element => element.Name.AsString()));

			return shiftTypes;
		}

		#endregion

		#region Helper methods

		/// <summary>
		/// Transform the value of an object to corresponding enum value
		/// </summary>
		/// <typeparam name="T">Input type parameter</typeparam>
		/// <param name="resumeElement">The resume Xml element.</param>
		/// <param name="path">The path</param>
		/// <returns>Apptopriate enum or null</returns>
		public static T? AsEnum<T>(XElement resumeElement, string path) where T : struct
		{
			var value = resumeElement.XPathAsString(path);
			if (value.IsNull())
				return null;

			T result;
			if (Enum.TryParse(value, true, out result))
				return result;

			return null;
		}

		/// <summary>
		/// Transform the value of an object to corresponding enum value
		/// </summary>
		/// <typeparam name="T">Input type parameter</typeparam>
		/// <param name="resumeElement">The resume Xml element.</param>
		/// <param name="path">The path</param>
		/// <param name="defaultEnum">default value for the enum</param>
		/// <returns>Apptopriate enum or null</returns>
		public static T AsEnum<T>(XElement resumeElement, string path, T defaultEnum) where T : struct
		{
			var value = resumeElement.XPathAsString(path);
			if (value.IsNull())
				return defaultEnum;

			T result;
			return Enum.TryParse(value, true, out result) ? result : defaultEnum;
		}

		/// <summary>
		/// Gets the lookup id.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <param name="resumeElement">The resume Xml element.</param>
		/// <param name="path">The path.</param>
		/// <param name="lookupType">Type of the lookup.</param>
		/// <param name="parentId">The parent id.</param>
		/// <returns></returns>
		public static long? GetLookupId(IRuntimeContext runtimeContext, XElement resumeElement, string path, LookupTypes lookupType, long? parentId = null)
		{
			if (path.IsNullOrEmpty())
				return null;

			var externalId = resumeElement.XPathAsString(path);
			if (externalId.IsNullOrEmpty()) return null;

			if (externalId.Length > 2 && lookupType == LookupTypes.States)
			{
				if (resumeElement.XPathSelectElement(path).IsNotNull() && resumeElement.XPathSelectElement(path).Attribute("abbrev").IsNotNull())
					externalId = resumeElement.XPathSelectElement(path).Attribute("abbrev").Value;
			}

			var lookup = !parentId.HasValue
				? runtimeContext.Helpers.Lookup.GetLookup(lookupType)
					.SingleOrDefault(x => x.ExternalId == externalId)
				: runtimeContext.Helpers.Lookup.GetLookup(lookupType)
					.SingleOrDefault(x => x.ExternalId == externalId && x.ParentId == parentId);

			return lookup.IsNull() ? (long?)null : lookup.Id;
		}

		/// <summary>
		/// Gets the lookup id.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <param name="lookupType">Type of the lookup.</param>
		/// <param name="value">The value.</param>
		/// <param name="parentId">The parent id.</param>
		/// <returns></returns>
		internal static long? GetLookupId(IRuntimeContext runtimeContext, LookupTypes lookupType, string value, long? parentId = 0)
		{
			var parent = parentId.IsNull() ? 0 : Convert.ToInt64(parentId);

			try
			{
				var lookup = runtimeContext.Helpers.Lookup.GetLookup(lookupType, parent).SingleOrDefault(x => x.ExternalId == value);
				return lookup.IsNull() ? (long?)null : lookup.Id;
			}
			catch (Exception ex)
			{
				var temp = ex.Message;
				throw;
			}
		}

		/// <summary>
		/// Gets the lookup external id.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <param name="lookupType">Type of the lookup.</param>
		/// <param name="id">The id.</param>
		/// <returns></returns>
		internal static string GetLookupExternalId(IRuntimeContext runtimeContext, LookupTypes lookupType, long id)
		{
			var lookup = runtimeContext.Helpers.Lookup.GetLookup(lookupType).SingleOrDefault(x => x.Id == id);
			return lookup.IsNull() ? string.Empty : lookup.ExternalId;
		}

		/// <summary>
		/// Gets the lookup external id.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <param name="lookupType">Type of the lookup.</param>
		/// <param name="key">The key.</param>
		/// <returns></returns>
		internal static string GetLookupExternalId(IRuntimeContext runtimeContext, LookupTypes lookupType, string key)
		{
			var lookup = runtimeContext.Helpers.Lookup.GetLookup(lookupType).SingleOrDefault(x => x.Key == key);
			return lookup.IsNull() ? string.Empty : lookup.ExternalId;
		}

		/// <summary>
		/// Gets the lookup Text.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <param name="lookupType">Type of the lookup.</param>
		/// <param name="id">The id.</param>
		/// <returns></returns>
		internal static string GetLookupText(IRuntimeContext runtimeContext, LookupTypes lookupType, long id)
		{
			var lookup = runtimeContext.Helpers.Lookup.GetLookup(lookupType).SingleOrDefault(x => x.Id == id);
			return lookup.IsNull() ? string.Empty : lookup.Text;
		}

		/// <summary>
		/// Gets the resume completion status.
		/// </summary>
		/// <param name="resumeCompletionStatus">The resume completion status.</param>
		/// <returns></returns>
		internal static ResumeCompletionStatuses GetResumeCompletionStatus(string resumeCompletionStatus)
		{
			if (string.IsNullOrEmpty(resumeCompletionStatus))
				return 0;

			int tempStatus;

			if (!Int32.TryParse(resumeCompletionStatus, out tempStatus))
			{
				var completionStatusDoc = XDocument.Parse(resumeCompletionStatus);
				foreach (var ele in completionStatusDoc.Descendants("section").Where(ele => ele.Value == "1"))
				{
					switch (ele.Attribute("name").Value.ToUpper())
					{
						case "WORK_HISTORY":
							tempStatus |= (int)ResumeCompletionStatuses.WorkHistory;
							break;

						case "CONTACT":
							tempStatus |= (int)ResumeCompletionStatuses.Contact;
							break;

						case "EDUCATION":
							tempStatus |= (int)ResumeCompletionStatuses.Education;
							break;

						case "SUMMARY":
							tempStatus |= (int)ResumeCompletionStatuses.Summary;
							break;

						case "OPTIONS":
							tempStatus |= (int)ResumeCompletionStatuses.Options;
							break;

						case "PROFILE":
							tempStatus |= (int)ResumeCompletionStatuses.Profile;
							break;

						case "PREFERENCES":
							tempStatus |= (int)ResumeCompletionStatuses.Preferences;
							break;
					}
				}
			}

			return (ResumeCompletionStatuses)tempStatus;
		}

		/// <summary>
		/// Gets the internship skills.
		/// </summary>
		/// <returns>
		/// internship skills
		/// </returns>
		private static List<InternshipSkill> GetInternshipSkills(XElement jobElement)
		{
			var internshipSkills = (from jis in jobElement.XPathSelectElements("internship_skills/internship_skill")
				select new InternshipSkill
				{
					Id = jis.GetElementValueAsInt64("id"),
					Name = jis.GetElementValueAsString("name")
				}).Distinct().ToList();

			return internshipSkills;
		}

		/// <summary>
		/// Gets the rank id.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <param name="externalRankId">The external rank id.</param>
		/// <param name="externalBranchOfServiceId">The external branch of service id.</param>
		/// <returns></returns>
		private static long? GetRankId(IRuntimeContext runtimeContext, string externalRankId, string externalBranchOfServiceId)
		{
			if (externalRankId.IsNullOrEmpty() || externalBranchOfServiceId.IsNullOrEmpty())
				return null;

			var branchOfServiceId = GetLookupId(runtimeContext, LookupTypes.MilitaryBranchesOfService, externalBranchOfServiceId);

			return branchOfServiceId.IsNull() ? null : GetLookupId(runtimeContext, LookupTypes.MilitaryRanks, externalRankId, branchOfServiceId);
		}

		/// <summary>
		/// Transform the value of an object to corresponding enum value
		/// </summary>
		/// <typeparam name="T">Input type parameter</typeparam>
		/// <param name="item">Given object</param>
		/// <returns>Apptopriate enum or null</returns>
		public static T? AsEnum<T>(this object item) where T : struct
		{
			if (item.IsNull())
				return null;

			var s = item as string;
			if (s != null)
			{
				T result;
				if (Enum.TryParse(s, true, out result))
					return result;
			}
			else if (item is int || item is char)
			{
				var val = item is char ? (char)item : (int)item;
				var temp = Enum.GetName(typeof(T), val);

				if (temp != null)
					return (T)Enum.Parse(typeof(T), temp, true);
			}
			return null;
		}

		/// <summary>
		/// Set item vale to null when its value is set to default
		/// </summary>
		/// <typeparam name="T">Type of the object</typeparam>
		/// <param name="item">item</param>
		/// <returns>null or the value itself</returns>
		public static T? AsNull<T>(this object item) where T : struct
		{
			if (item == null)
				return null;

			if (item is DateTime && (DateTime)item == default(DateTime)
			    || (item is int && (int)item == default(int))
			    || (item is long && (long)item == default(long))
			    || (item is float && Math.Abs((float)item - default(float)) < 0.000001)
			    || (item is double && Math.Abs((double)item - default(double)) < 0.0000000001))
				return null;

			return (T)item;
		}

		/// <summary>
		/// Monthes the difference.
		/// </summary>
		/// <param name="date1">The date1.</param>
		/// <param name="date2">The date2.</param>
		/// <returns></returns>
		public static int MonthDifference(this DateTime date1, DateTime date2)
		{
			return Math.Abs((date1.Month - date2.Month) + 12 * (date1.Year - date2.Year));
		}

		#endregion
	}
}
