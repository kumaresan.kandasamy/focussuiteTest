﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using Focus.Core.Models.Assist;
using Focus.Data.Core.Entities;

#endregion

namespace Focus.Services.Mappers
{
    public static class CoreMapper
    {
        /// <summary>
        /// Convert the stored procedure result to spidered employer model.
        /// </summary>
        /// <param name="result">The result.</param>
        /// <returns></returns>
        public static SpideredEmployerModel ToSpideredEmployerModel(this GetSpideredEmployersWithGoodMatchesResult result)
        {
            var model = new SpideredEmployerModel
            {
                Id = result.Id,
                EmployerName = result.EmployerName,
                JobTitle = result.JobTitle,
                LensPostingId = result.LensPostingId,
                NumberOfPostings = result.NumberOfPostings.GetValueOrDefault(),
                PostingDate = result.PostingDate.GetValueOrDefault()
            };
            return model;
        }

        /// <summary>
        /// Convert the stored procedure result to spidered posting model.
        /// </summary>
        /// <param name="result">The result.</param>
        /// <returns></returns>
        public static SpideredPostingModel ToSpideredPostingModel(this GetSpideredPostingsWithGoodMatchesResult result)
        {
            var model = new SpideredPostingModel
            {
                LensPostingId = result.LensPostingId,
                JobTitle = result.JobTitle,
                PostingDate = result.PostingDate.GetValueOrDefault()
            };
            return model;
        }
    }
}