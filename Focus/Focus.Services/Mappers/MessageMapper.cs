﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;

using Focus.Core;
using Focus.Core.Messages;
using Focus.Services.Messages;

using Framework.Core;

#endregion


namespace Focus.Services.Mappers
{
	internal static class MessageMapper
	{
    /// <summary>
    /// Converts a service request to a post job message.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <param name="postingId">The posting id.</param>
    /// <param name="jobId">The job id.</param>
    /// <param name="isVeteranJob">Whether this is a veteran job (WOTC specifies Veterans)</param>
    /// <param name="ignoreLens">Whether to ignore the posting of the job to lens (used by import process)</param>
    /// <param name="ignoreClient">Whether to ignore any integrated client system</param>
    /// <returns></returns>
		internal static ProcessPostingMessage ToPostJobMessage(this IServiceRequest request, long postingId, long jobId, bool isVeteranJob, bool ignoreLens = false, bool ignoreClient = false)
		{
			var registerPostingMessage = new ProcessPostingMessage
			{
				ActionerId = request.UserContext.ActionerId,
				Culture = request.UserContext.Culture,
				PostingId = postingId,
        JobId = jobId,
        IsVeteranJob = isVeteranJob,
        IgnoreLens = ignoreLens,
        IgnoreClient = ignoreClient,
				RegisterPosting = true,
        ExternalAdminId = request.ExternalAdminId,
        ExternalOfficeId = request.ExternalOfficeId,
        ExternalPassword = request.ExternalPassword,
        ExternalUsername = request.ExternalUsername
      };

			if (request.UserContext.IsNotNull())
			{
				if (request.ExternalAdminId.IsNullOrEmpty() && request.ExternalOfficeId.IsNullOrEmpty() && request.ExternalPassword.IsNullOrEmpty() && request.ExternalUsername.IsNullOrEmpty())
				{
					registerPostingMessage.ExternalAdminId = request.UserContext.ExternalUserId;
					registerPostingMessage.ExternalOfficeId = request.UserContext.ExternalOfficeId;
					registerPostingMessage.ExternalPassword = request.UserContext.ExternalPassword;
					registerPostingMessage.ExternalUsername = request.UserContext.ExternalUserName;
				}
			}

	    return registerPostingMessage;
		}

		/// <summary>
		/// Converts a service request to a unregister posting message.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <param name="postingId">The posting id.</param>
		/// <returns></returns>
		internal static ProcessPostingMessage ToUnregisterPostingMessage(this IServiceRequest request, long postingId)
		{
			var unregisterPostingMessage = new ProcessPostingMessage
			{
			  ActionerId = request.UserContext.ActionerId,
			  Culture = request.UserContext.Culture,
				PostingId = postingId,
				RegisterPosting = false,
				ExternalAdminId = request.ExternalAdminId,
				ExternalOfficeId = request.ExternalOfficeId,
				ExternalPassword = request.ExternalPassword,
				ExternalUsername = request.ExternalUsername
			};

			if (request.UserContext.IsNotNull())
			{
				if (request.ExternalAdminId.IsNullOrEmpty() && request.ExternalOfficeId.IsNullOrEmpty() && request.ExternalPassword.IsNullOrEmpty() && request.ExternalUsername.IsNullOrEmpty())
				{
					unregisterPostingMessage.ExternalAdminId = request.UserContext.ExternalUserId;
					unregisterPostingMessage.ExternalOfficeId = request.UserContext.ExternalOfficeId;
					unregisterPostingMessage.ExternalPassword = request.UserContext.ExternalPassword;
					unregisterPostingMessage.ExternalUsername = request.UserContext.ExternalUserName;
				}
			}

			return unregisterPostingMessage;
		}

		/// <summary>
		/// Converts a service request to a register resume request.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <param name="resumeId">The resume id.</param>
		/// <param name="personId">The person id.</param>
		/// <param name="ignoreLens">Whether to ignore the posting of the job to lens (used by import process)</param>
		/// <param name="ignoreClient">Whether to ignore any integrated client system</param>
		/// <param name="processOn">An optional (future) time on which the message should be processed.</param>
		/// <param name="actionTypesToPublish">The action types to publish to the integration layer.</param>
		/// <returns></returns>
		public static ResumeProcessMessage ToRegisterResumeMessage(this IServiceRequest request, long resumeId, long personId, bool ignoreLens = false, bool ignoreClient = false, List<ActionTypes> actionTypesToPublish = null)
		{
			var registerResumeMessage = new ResumeProcessMessage
			{
			  ActionerId = request.UserContext.ActionerId,
			  Culture = request.UserContext.Culture,
			  ResumeId = resumeId,
				PersonId = personId,
        IgnoreLens = ignoreLens,
        IgnoreClient = ignoreClient,
								RegisterResume = true,
				ActionTypesToPublish = actionTypesToPublish
			};
      if (request.UserContext.IsNotNull())
      {
        registerResumeMessage.ExternalAdminId = request.UserContext.ExternalUserId;
        registerResumeMessage.ExternalOfficeId = request.UserContext.ExternalOfficeId;
        registerResumeMessage.ExternalPassword = request.UserContext.ExternalPassword;
        registerResumeMessage.ExternalUsername = request.UserContext.ExternalUserName;
      }

      return registerResumeMessage;
		}

		/// <summary>
		/// Converts a service request to a unregister resume request.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <param name="resumeId">The resume id.</param>
		/// <param name="personId">The person id.</param>
    /// <param name="ignoreLens">Whether to ignore the posting of the job to lens (used by import process)</param>
    /// <param name="ignoreClient">Whether to ignore any integrated client system</param>
		/// <param name="actionTypesToPublish">The action types to publish to the integration layer.</param>
		/// <returns></returns>
    public static ResumeProcessMessage ToUnregisterResumeMessage(this IServiceRequest request, long resumeId, long personId, bool ignoreLens = false, bool ignoreClient = false, List<ActionTypes> actionTypesToPublish = null)
		{
			var unregisterResumeMessage = new ResumeProcessMessage
			{
			  ActionerId = request.UserContext.ActionerId,
			  Culture = request.UserContext.Culture,
			  ResumeId = resumeId,
        PersonId = personId,
        IgnoreLens = ignoreLens,
                IgnoreClient = ignoreClient,
								RegisterResume = false,
				ActionTypesToPublish = actionTypesToPublish
			};
			if (request.UserContext.IsNotNull())
			{
				unregisterResumeMessage.ExternalAdminId = request.UserContext.ExternalUserId;
				unregisterResumeMessage.ExternalOfficeId = request.UserContext.ExternalOfficeId;
				unregisterResumeMessage.ExternalPassword = request.UserContext.ExternalPassword;
				unregisterResumeMessage.ExternalUsername = request.UserContext.ExternalUserName;
			}

			return unregisterResumeMessage;
		}

		/// <summary>
		/// Converts a service request to an applicant hired request.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <param name="applicationId">The application id.</param>
		/// <param name="personId">The person id.</param>
		/// <returns></returns>
		public static ApplicantHiredMessage ToApplicantHiredMessage(this IServiceRequest request, long applicationId, long personId)
		{
			return new ApplicantHiredMessage
			       	{
			       		ActionerId = request.UserContext.ActionerId,
			       		Culture = request.UserContext.Culture,
			       		ApplicationId = applicationId,
			       		PersonId = personId
			       	};
		}

    /// <summary>
		/// Converts a service request to a process registration pin email message.
    /// </summary>
    /// <param name="request">The request.</param>
    /// <param name="emails">The emails.</param>
    /// <param name="registrationUrl">The registration URL.</param>
    /// <param name="resetPasswordUrl">The reset password URL.</param>
    /// <param name="targetModule">The target module.</param>
    /// <returns></returns>
    public static ProcessRegistrationPinEmailMessage ToProcessRegistrationPinEmailMessage(this IServiceRequest request, List<string> emails, string registrationUrl, string resetPasswordUrl, FocusModules targetModule)
		{
			return new ProcessRegistrationPinEmailMessage
			       	{
			       		ActionerId = request.UserContext.ActionerId,
			       		Culture = request.UserContext.Culture,
								Emails = emails,
								RegistrationUrl = registrationUrl,
                ResetPasswordUrl = resetPasswordUrl,
								TargetModule = targetModule
			       	};
		}

		/// <summary>
		/// Converts a service request to an invite jobseekers to apply message.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <param name="personIdsWithScores">The person ids.</param>
		/// <param name="jobId">The job identifier.</param>
		/// <param name="jobLink">The job link.</param>
		/// <param name="lensPostingId">The lens posting identifier.</param>
		/// <param name="sendersName">Name of the senders.</param>
		/// <param name="senderEmail">The sender email.</param>
		/// <returns></returns>
		public static InviteJobseekersToApplyMessage ToInviteJobseekersToApplyMessage(this IServiceRequest request, Dictionary<long, int> personIdsWithScores, long jobId, string jobLink, string lensPostingId, string sendersName,
																																									string senderEmail)
		{
			return new InviteJobseekersToApplyMessage
			{
				ActionerId = request.UserContext.ActionerId,
				Culture = request.UserContext.Culture,
				PersonIdsWithScores = personIdsWithScores,
				JobId = jobId,
				JobLink = jobLink,
				LensPostingId = lensPostingId,
				SendersName = sendersName,
				SenderEmail = senderEmail
			};
		}
	}
}
