﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using Focus.Core;
using Focus.Core.Models.Upload;

#endregion

namespace Focus.Services.Mappers.Upload
{
  public interface IUploadMapper<in TUpload> where TUpload : class, IUploadedRecord
  {
    /// <summary>
    /// Populates any null/empty fields in the upload record
    /// </summary>
    /// <param name="uploadFileRecord">The upload file record.</param>
    /// <param name="userId">The id of the user uploading the file.</param>
    /// <param name="recordStatus">The validation status of the record.</param>
    /// <param name="customInformation">Any custom information</param>
    /// <param name="runtimeContext">The runtime context.</param>
    void PopulateFields(TUpload uploadFileRecord, long userId, ValidationRecordStatus recordStatus, Dictionary<string, string> customInformation, IRuntimeContext runtimeContext);

    /// <summary>
    /// Populates any null/empty fields in the upload record
    /// </summary>
    /// <param name="uploadFileRecord">The upload file record.</param>
    /// <param name="runtimeContext">The runtime context.</param>
    void PublishMessage(TUpload uploadFileRecord, IRuntimeContext runtimeContext);
  }
}
