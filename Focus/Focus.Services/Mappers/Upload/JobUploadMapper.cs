﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Focus.Core;
using Focus.Core.Criteria;
using Focus.Core.Criteria.Onet;
using Focus.Core.Models.Upload;
using Focus.Services.Messages;

using Framework.Core;
using Framework.Messaging;

#endregion

namespace Focus.Services.Mappers.Upload
{
	public class JobUploadMapper : IUploadMapper<JobUploadRecord>
	{
		/// <summary>
		/// Populates any null/empty fields in the upload record
		/// </summary>
		/// <param name="uploadFileRecord">The upload file record.</param>
		/// <param name="userId">The id of the user uploading the file.</param>
		/// <param name="recordStatus">The validation status of the record.</param>
		/// <param name="customInformation">Any custom information</param>
		/// <param name="runtimeContext">The runtime context.</param>
		public void PopulateFields(JobUploadRecord uploadFileRecord, long userId, ValidationRecordStatus recordStatus, Dictionary<string, string> customInformation, IRuntimeContext runtimeContext)
		{
			uploadFileRecord.UserId = userId;
			uploadFileRecord.EmployeeId = customInformation["EmployeeId"].ToLong().GetValueOrDefault(0);
			uploadFileRecord.BusinessUnitId = customInformation["BusinessUnitId"].ToLong().GetValueOrDefault(0);
			uploadFileRecord.NumberOfOpenings = uploadFileRecord.NumberOfOpenings ?? 1;
			uploadFileRecord.ClosingOn = uploadFileRecord.ClosingOn ?? DateTime.Now.AddDays(runtimeContext.AppSettings.DaysForJobPostingLifetime);
			uploadFileRecord.ApplicantCreditCheck = uploadFileRecord.ApplicantCreditCheck ?? false;
			uploadFileRecord.ApplicantCriminalCheck = uploadFileRecord.ApplicantCriminalCheck ?? false;

			var onetCriteria = new OnetCriteria
			{
				JobTitle = uploadFileRecord.JobTitle,
				ListSize = 1,
				FetchOption = CriteriaBase.FetchOptions.List
			};
			var onets = runtimeContext.Helpers.Occupation.GetOnets(onetCriteria, runtimeContext.CurrentRequest.UserContext.Culture);
			uploadFileRecord.OnetId = onets.Select(onet => onet.Id).FirstOrDefault();
		}

		/// <summary>
		/// Populates any null/empty fields in the upload record
		/// </summary>
		/// <param name="uploadFileRecord">The upload file record.</param>
		/// <param name="runtimeContext">The runtime context.</param>
		public void PublishMessage(JobUploadRecord uploadFileRecord, IRuntimeContext runtimeContext)
		{
			var message = new SaveAndPostJobMessage
			{
				JobCreatedBy = uploadFileRecord.UserId.GetValueOrDefault(0),
				EmployeeId = uploadFileRecord.EmployeeId.GetValueOrDefault(0),
				BusinessUnitId = uploadFileRecord.BusinessUnitId.GetValueOrDefault(0),
				JobTitle = HttpUtility.HtmlEncode(uploadFileRecord.JobTitle),
				JobDescription = uploadFileRecord.Description ?? string.Empty,
				PostalCode = uploadFileRecord.Zip,
				NumberOfOpenings = uploadFileRecord.NumberOfOpenings,
				ClosingOn = uploadFileRecord.ClosingOn,
				OnetId = uploadFileRecord.OnetId,
				ApplicantCriminalCheck = uploadFileRecord.ApplicantCriminalCheck,
				ApplicantCreditCheck = uploadFileRecord.ApplicantCreditCheck,
				EmploymentStatusId = uploadFileRecord.EmploymentStatusId,
				JobTypeId = uploadFileRecord.JobTypeId,
				UploadIndicator = true
			};
			runtimeContext.Helpers.Messaging.Publish(message, MessagePriority.Low);
		}
	}
}
