﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Xml;
using System.Xml.Xsl;

using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Models.Assist;
using Focus.Core.Models.Career;
using Focus.Core.Models.Integration;
using Focus.Data.Core.Entities;
using Focus.Data.Library.Entities;
using Focus.Services.Core;
using Focus.Services.Core.Extensions;
using Focus.Services.DtoMappers;

using Framework.Core;
// ReSharper disable EmptyGeneralCatchClause

#endregion

namespace Focus.Services.Mappers
{
	/// <summary>
	/// 
	/// </summary>
	public static class PostingMapper
	{
		private static IEnumerable<DaysOfWeek> DaysToCheck
		{
			get
			{
				yield return DaysOfWeek.Monday;
				yield return DaysOfWeek.Tuesday;
				yield return DaysOfWeek.Wednesday;
				yield return DaysOfWeek.Thursday;
				yield return DaysOfWeek.Friday;
				yield return DaysOfWeek.Saturday;
				yield return DaysOfWeek.Sunday;
			}
		}

		/// <summary>
		/// Converts the posting dto to the posting envelope.
		/// </summary>
		/// <param name="posting">The posting.</param>
		/// <param name="context">The runtime context.</param>
		/// <returns></returns>
		public static PostingEnvelope ToPostingEnvelope(this PostingDto posting, IRuntimeContext context = null)
		{
			if (posting.PostingXml.IsNullOrEmpty())
				throw new Exception("No posting xml");

			var postingDoc = new XmlDocument { PreserveWhitespace = true };
			postingDoc.LoadXml(posting.PostingXml);

			if (postingDoc.IsNull())
				throw new Exception("Could not parse posting xml");

			var isInternship = false;
			var internshipNodes = postingDoc.SelectNodes("//JobDoc/special/cf026");
			if (internshipNodes.IsNotNull() && internshipNodes.Count > 0)
			{
				isInternship = (internshipNodes[0].InnerText == "1");
			}
			else
			{
				internshipNodes = postingDoc.SelectNodes("//JobDoc/posting/jobinfo/JOB_TYPE");
				if (internshipNodes.IsNotNull() && internshipNodes.Count > 0)
				{
					JobTypes jobType;
					if (Enum.TryParse(internshipNodes[0].InnerText, out jobType))
					{
						if (((jobType & JobTypes.InternshipPaid) == JobTypes.InternshipPaid) || ((jobType & JobTypes.InternshipUnpaid) == JobTypes.InternshipUnpaid))
							isInternship = true;
					}
				}
				else
				{
					internshipNodes = postingDoc.SelectNodes("//JobDoc/special/jobtype");
					if (internshipNodes.IsNotNull() && internshipNodes.Count > 0)
					{
						JobTypes jobType;
						if (Enum.TryParse(internshipNodes[0].InnerText, out jobType))
						{
							if (((jobType & JobTypes.InternshipPaid) == JobTypes.InternshipPaid) || ((jobType & JobTypes.InternshipUnpaid) == JobTypes.InternshipUnpaid))
								isInternship = true;
						}
					}
				}
			}
			var jobDocNodes = postingDoc.SelectNodes("//JobDoc");
			var jobDocNode = postingDoc.SelectSingleNode("//JobDoc");
			var requirementNodes = postingDoc.SelectNodes("//REQUIREMENTS");
			var requirementNode = postingDoc.SelectSingleNode("//REQUIREMENTS");
			var clientJobDataNodes = postingDoc.SelectNodes("//jobposting/clientjobdata");
			var clientJobDataNode = postingDoc.SelectSingleNode("//jobposting/clientjobdata");

			var employerName = posting.EmployerName;
			if (context.IsNotNull())
			{
				if (context.CurrentRequest.UserContext.IsShadowingUser)
				{
					var talentEmployer = postingDoc.SelectSingleNode("//special/talentemployer");
					if (talentEmployer.IsNotNull() && talentEmployer.HasChildNodes)
						employerName = talentEmployer.FirstChild.Value;
				}
			}

			return new PostingEnvelope
							{
								PostingXml = jobDocNodes != null && jobDocNodes.Count > 0 && jobDocNode != null ? jobDocNode.OuterXml : string.Empty,
								RequirementsXml = requirementNodes != null && requirementNodes.Count > 0 && requirementNode != null ? requirementNode.OuterXml : string.Empty,
								PostingHtml = posting.Html.IsNotNullOrEmpty() ? posting.Html : string.Empty,
								EosPostingXml = clientJobDataNodes != null && clientJobDataNodes.Count > 0 && clientJobDataNode != null ? clientJobDataNode.OuterXml : string.Empty,
								PostingStatus = posting.StatusId,
								PostingInfo = new PostingInfo
																{
																	Url = posting.Url,
																	Title = posting.JobTitle,
																	PostingOriginId = posting.OriginId,
																	CustomerJobId = posting.ExternalId,
																	Employer = employerName,
																	ViewedCount = posting.ViewedCount,
																	FocusJobId = posting.JobId,
																	Internship = isInternship,
																	FocusPostingId = posting.Id,
																	CreatedOn = posting.CreatedOn
																}
							};
		}

		/// <summary>
		/// Converts the posting xml to a posting dto.
		/// </summary>
		/// <param name="jobPostingXml">The job posting XML.</param>
		/// <param name="lensPostingId">The lens posting id.</param>
		/// <param name="jobPostingStylePath">The job posting style path.</param>
		/// <param name="setExternalId">Whether to populate the external id field</param>
		/// <returns></returns>
		/// <exception cref="System.Exception">No job posting xml</exception>
		public static PostingDto ToPostingDto(this string jobPostingXml, string lensPostingId, string jobPostingStylePath, bool setExternalId)
		{
			if (jobPostingXml.IsNullOrEmpty())
				throw new Exception("No job posting xml");

			var postingDoc = new XmlDocument { PreserveWhitespace = true };
			postingDoc.LoadXml(jobPostingXml);

			if (postingDoc.IsNull())
				throw new Exception("Could not parse job posting xml");

			var postinghtml = postingDoc.AsString("//postinghtml");

			if (postinghtml.IsNullOrEmpty())
			{
				#region Generate html

				postinghtml = CreatePostingHtml(jobPostingXml, jobPostingStylePath);

				#endregion
			}

			var location = "";
			var address = postingDoc.SelectSingleNode("//posting/contact/address");
			if (address.IsNotNull())
			{
				var city = address.AsString("city");
				var state = address.AsString("state");
				if (city.IsNotNullOrEmpty())
				{
					location = state.IsNotNullOrEmpty() ? string.Concat(city, ", ", state) : city;
				}
				else
				{
					location = state ?? "";
				}
			}

			return new PostingDto
							{
								PostingXml = jobPostingXml,
								JobTitle = postingDoc.AsString("//posting/duties/title").TruncateString(255),
								JobLocation = location,
								LensPostingId = lensPostingId,
								EmployerName = postingDoc.AsString("//special/jobemployer"),
								Url = postingDoc.AsString("//joburl"),
								ExternalId = setExternalId ? postingDoc.AsString("//foreignpostingid") : string.Empty,
								StatusId = GetPostingStatus(postingDoc.AsString("//jobstatus")),
								OriginId = postingDoc.AsNullableLong("//originid", null),
								JobId = null,
								Html = postinghtml
							};
		}

		/// <summary>
		/// Converts the posting to the integration job model.
		/// </summary>
		/// <param name="posting">The posting.</param>
		/// <returns></returns>
		public static JobModel ToIntegrationJobModel(this PostingDto posting)
		{
			var postingDoc = new XmlDocument { PreserveWhitespace = true };
			postingDoc.LoadXml(posting.PostingXml);

			var currentNode = postingDoc.SelectSingleNode(Constants.PostingXmlPaths.JobTitlePath);
			var jobTitle = currentNode != null ? currentNode.InnerText : string.Empty;

			currentNode = postingDoc.SelectSingleNode(Constants.PostingXmlPaths.OnetPath);
			var oNet = string.Empty;
			if (currentNode != null)
			{
				oNet = currentNode.InnerText.Split(',')[0];
			}

			currentNode = postingDoc.SelectSingleNode(Constants.PostingXmlPaths.JobDescriptionPath);
			var description = currentNode != null ? currentNode.InnerText : string.Empty;

			currentNode = postingDoc.SelectSingleNode(Constants.PostingXmlPaths.UrlPath);
			var url = currentNode != null ? currentNode.InnerText : string.Empty;

			currentNode = postingDoc.SelectSingleNode(Constants.PostingXmlPaths.CityPath);
			var city = currentNode != null ? currentNode.InnerText : string.Empty;

			currentNode = postingDoc.SelectSingleNode(Constants.PostingXmlPaths.StatePath);
			var state = currentNode != null ? currentNode.InnerText : string.Empty;

			currentNode = postingDoc.SelectSingleNode(Constants.PostingXmlPaths.ZipPath);
			var zip = currentNode != null ? currentNode.InnerText : string.Empty;

			currentNode = postingDoc.SelectSingleNode(Constants.PostingXmlPaths.EducationLevelPath);
			EducationLevels? educationLevel = null;
			if (currentNode != null && currentNode.InnerText != "")
			{
				EducationLevels edu;
				if (Enum.TryParse(currentNode.InnerText, out edu))
					educationLevel = edu;
			}

			currentNode = postingDoc.SelectSingleNode(Constants.PostingXmlPaths.NaicsPath);
			var naics = currentNode != null ? currentNode.InnerText : string.Empty;

			return new JobModel
			{
				Openings = 1,
				JobStatus = JobStatuses.Active,
				Title = jobTitle,
				OnetCode = oNet, // Strore default as config setting
				Description = description,
				Url = url,
				InterviewContactPreferences = ContactMethods.Online,
				Addresses = new List<AddressModel> { new AddressModel { City = city, State = state, PostCode = zip, Country = "US", CountyId = 0 } }, // Get County using zip (Default zip if not found)
				EducationLevel = educationLevel,
				BusinessUnitNaics = naics,
				ExternalId = posting.ExternalId,
				OriginId = posting.OriginId
			};
		}

		/// <summary>
		/// Gets the contact details from the posting xml in the dto.
		/// </summary>
		/// <param name="posting">The posting.</param>
		/// <returns></returns>
		public static PostingContactDetailsModel GetContactDetails(this PostingDto posting)
		{
			if (posting.PostingXml.IsNullOrEmpty())
				throw new Exception("No posting xml");

			var postingDoc = new XmlDocument { PreserveWhitespace = true };
			postingDoc.LoadXml(posting.PostingXml);

			if (postingDoc.IsNull())
				throw new Exception("Could not parse posting xml");

			return new PostingContactDetailsModel
							{
								EmployerName = posting.EmployerName,
								ContactName = postingDoc.AsString("//contact/person"),
								TelephoneNumber = postingDoc.AsString("//contact/phone"),
								EmailAddress = postingDoc.AsString("//contact/email"),
								Address = new PostingContactAddressModel
														{
															TownCity = postingDoc.AsString("//contact/address/city"),
															State = postingDoc.AsString("//contact/address/state"),
															PostcodeZip = postingDoc.AsString("//contact/address/postalcode")
														}
							};
		}

		/// <summary>
		/// To the posting dto.
		/// </summary>
		/// <param name="job">The job.</param>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <param name="clientOfficeId">The client office id.</param>
		/// <param name="culture">The culture.</param>
		/// <returns></returns>
		public static PostingDto ToPostingDto(this Data.Core.Entities.Job job, IRuntimeContext runtimeContext, string clientOfficeId, string culture)
		{
			var lensPostingId = String.Empty;

			if (job.Posting.IsNotNull())
				lensPostingId = job.Posting.LensPostingId;

			var postingDoc = job.ToPostingXml(runtimeContext, clientOfficeId, culture);

			var location = "";
			var address = postingDoc.SelectSingleNode("//posting/contact/address");
			if (address.IsNotNull())
			{
				var city = address.AsString("city");
				var state = address.AsString("state");
				if (city.IsNotNullOrEmpty())
				{
					location = state.IsNotNullOrEmpty() ? string.Concat(city, ", ", state) : city;
				}
				else
				{
					location = state ?? "";
				}
			}

			return new PostingDto
							{
								PostingXml = postingDoc.OuterXml,
								JobTitle = job.JobTitle,
								LensPostingId = lensPostingId,
								EmployerName = job.GetEmployerName(runtimeContext),
								JobLocation = location,
								Url = job.InterviewApplicationUrl,
								ExternalId = job.ExternalId,
								StatusId = job.JobStatus.ToPostingStatus(),
								OriginId = 7,
								JobId = job.Id,
								Html = job.PostingHtml
							};
		}

		/// <summary>
		/// Get the employer name for a job
		/// </summary>
		/// <param name="job">The job</param>
		/// <param name="runtimeContext">The runtime context</param>
		/// <returns>The employer name</returns>
		private static string GetEmployerName(this Data.Core.Entities.Job job, IRuntimeContext runtimeContext)
		{
			if (job.IsConfidential.GetValueOrDefault())
			{
				var stateKey = runtimeContext.AppSettings.DefaultStateKey;
				var stateName = runtimeContext.Helpers.Lookup.GetLookupText(LookupTypes.States, stateKey, "Confidential");
				return runtimeContext.Helpers.Localisation.Localise("Global.ConfidentialEmployerLabel", "[Confidential]", stateName);
			}

			return job.BusinessUnit.Name;
		}

		/// <summary>
		/// To the posting XML.
		/// </summary>
		/// <param name="job">The job.</param>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <param name="clientOfficeId">The client office id.</param>
		/// <param name="culture">The culture.</param>
		/// <returns></returns>
		private static XmlDocument ToPostingXml(this Data.Core.Entities.Job job, IRuntimeContext runtimeContext, string clientOfficeId, string culture)
		{
			const string jobDefinition = @"<jobposting><JobDoc></JobDoc><postinghtml></postinghtml><REQUIREMENTS></REQUIREMENTS><clientjobdata></clientjobdata></jobposting>";

			var jobDefinitionDoc = new XmlDocument { PreserveWhitespace = true };
			jobDefinitionDoc.LoadXml(jobDefinition);

			var jobDocNode = jobDefinitionDoc.SelectSingleNode("//JobDoc");
			jobDocNode.CreatePostingNode(runtimeContext, job, culture);
			var specialNode = jobDocNode.CreateSpecialNode(job, clientOfficeId, runtimeContext);

			var htmlNode = jobDefinitionDoc.SelectSingleNode("//postinghtml");
			if (htmlNode != null)
			{
				htmlNode.InnerText = job.PostingHtml;
			}
			var requirementsNode = jobDefinitionDoc.SelectSingleNode("//REQUIREMENTS");

			#region Set screening value

			var screeningValue = "0";
			if (job.ScreeningPreferences.HasValue)
			{
				specialNode.CreateElement("screening").InnerText = ((int)job.ScreeningPreferences.Value).ToString();

				switch (job.ScreeningPreferences.Value)
				{
					case ScreeningPreferences.JobSeekersMustBeScreened:
						screeningValue = "1";
						break;

					case ScreeningPreferences.AllowUnqualifiedApplications:
						screeningValue = "-1";
						break;

					case ScreeningPreferences.JobSeekersMustHave1StarMatch:
						screeningValue = "2";
						break;

					case ScreeningPreferences.JobSeekersMustHave2StarMatch:
						screeningValue = "3";
						break;

					case ScreeningPreferences.JobSeekersMustHave3StarMatch:
						screeningValue = "4";
						break;

					case ScreeningPreferences.JobSeekersMustHave4StarMatch:
						screeningValue = "5";
						break;

					case ScreeningPreferences.JobSeekersMustHave5StarMatch:
						screeningValue = "6";
						break;

					case ScreeningPreferences.JobSeekersMustHave1StarMatchToApply:
						screeningValue = "7";
						break;

					case ScreeningPreferences.JobSeekersMustHave2StarMatchToApply:
						screeningValue = "8";
						break;

					case ScreeningPreferences.JobSeekersMustHave3StarMatchToApply:
						screeningValue = "9";
						break;

					case ScreeningPreferences.JobSeekersMustHave4StarMatchToApply:
						screeningValue = "10";
						break;

					case ScreeningPreferences.JobSeekersMustHave5StarMatchToApply:
						screeningValue = "11";
						break;
				}
			}

			requirementsNode.CreateAttribute("screening").Value = screeningValue;

			#endregion

			requirementsNode.CreateRequirementsNodes(job, runtimeContext.Helpers.Lookup);

			var clientJobDataNode = jobDefinitionDoc.SelectSingleNode("//clientjobdata");
			clientJobDataNode.SetJobContactFlags(job.InterviewContactPreferences, job.InterviewOtherInstructions, job.InterviewMailAddress, job.InterviewDirectApplicationDetails);
			clientJobDataNode.SetSalaryNodes(job, runtimeContext.Helpers.Lookup);
			// See FVN-1750 for logic about refreshing
			clientJobDataNode.SetDateValue("JOB_CREATE_DATE", job.LastRefreshedOn ?? job.CreatedOn);
			clientJobDataNode.SetDateValue("JOB_LAST_OPEN_DATE", job.ClosingOn);

			return jobDefinitionDoc;
		}

		/// <summary>
		/// Gets the job doc.
		/// </summary>
		/// <param name="postingXml">The posting XML.</param>
		/// <returns></returns>
		public static string GetJobDoc(this string postingXml)
		{
			if (postingXml.IsNullOrEmpty())
				throw new Exception("No job posting xml");

			var postingDoc = new XmlDocument { PreserveWhitespace = true };
			postingDoc.LoadXml(postingXml);

			var jobDocNodes = postingDoc.SelectNodes("//JobDoc");
			var jobDocNode = postingDoc.SelectSingleNode("//JobDoc");
			return postingDoc.IsNotNull() && jobDocNodes.IsNotNull() && jobDocNodes.Count > 0 && jobDocNode.IsNotNull() ? jobDocNode.OuterXml : string.Empty;
		}

		/// <summary>
		/// Gets the posting status.
		/// </summary>
		/// <param name="status">The status.</param>
		/// <returns></returns>
		private static PostingStatuses GetPostingStatus(string status)
		{
			switch (status.ToUpper())
			{
				case "FILLED":
					return PostingStatuses.Filled;

				case "REVIEW":
					return PostingStatuses.Review;

				case "ACTIVE":
					return PostingStatuses.Active;

				case "HOLD":
					return PostingStatuses.Hold;

				case "CLOSED":
					return PostingStatuses.Closed;

				case "DELETED":
					return PostingStatuses.Deleted;

				default:
					return PostingStatuses.Active;
			}
		}

		private static PostingStatuses ToPostingStatus(this JobStatuses jobStatus)
		{
			switch (jobStatus)
			{
				case JobStatuses.Active:
					return PostingStatuses.Active;

				case JobStatuses.AwaitingEmployerApproval:
					return PostingStatuses.Review;

				case JobStatuses.Closed:
					return PostingStatuses.Closed;

				case JobStatuses.OnHold:
					return PostingStatuses.Hold;

				default:
					throw new Exception("Cannot convert current job status to a posting status");
			}
		}

		/// <summary>
		/// Creates the posting HTML.
		/// </summary>
		/// <param name="jobPostingXml">The job posting XML.</param>
		/// <param name="jobPostingStylePath">The job posting style path.</param>
		/// <returns></returns>
		private static string CreatePostingHtml(String jobPostingXml, string jobPostingStylePath)
		{
			try
			{
				jobPostingStylePath = HttpContext.Current.Server.MapPath(jobPostingStylePath);
			}
			catch { }

			//const string lineBreakPlaceHolder = "<cr />";

			//jobPostingXml = jobPostingXml.Replace("\n", lineBreakPlaceHolder);

			// The XslCompiledTransform should be enabled with script execution to work with HRXML 
			// which uses Java script to get the positiontype from a tagged resume
			var xslSecurity = new XsltSettings { EnableScript = true };

			var obj = new XslCompiledTransform();
			obj.Load(jobPostingStylePath, xslSecurity, new XmlUrlResolver());

			var txtRdrPosting = new StringReader(jobPostingXml);
			var reader = XmlReader.Create(txtRdrPosting);

			var strWriter = new StringWriter();

			obj.Transform(reader, null, strWriter);

			var postingHtml = strWriter.ToString();

			if (postingHtml.IsNotNullOrEmpty())
				postingHtml = "<postinghtml>" + postingHtml + "</postinghtml>";

			return postingHtml;
		}

		#region Private ToPostingXml methods

		/// <summary>
		/// Creates the posting node.
		/// </summary>
		/// <param name="jobDocNode">The job doc node.</param>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <param name="job">The job.</param>
		/// <param name="culture">The culture.</param>
		private static void CreatePostingNode(this XmlNode jobDocNode, IRuntimeContext runtimeContext, Data.Core.Entities.Job job, string culture)
		{
			var postingNode = jobDocNode.CreateElement("posting");

			if (job.JobTitle.IsNotNullOrEmpty() || job.Description.IsNotNullOrEmpty() || job.OnetId.IsNotNull())
				postingNode.CreateDutiesNode(runtimeContext, job, culture);

			postingNode.CreateContactNode(job, runtimeContext.Helpers.Lookup);

			postingNode.CreateBenefitsNode(job, runtimeContext.Helpers.Lookup);

			if (job.MinimumExperience.HasValue || job.MinimumExperienceMonths.HasValue)
				postingNode.CreateBackgroundNode(job);

			if (job.NumberOfOpenings.HasValue || job.MinimumEducationLevel.HasValue)
				postingNode.CreateJobInfoNode(job);

			if (job.JobCertificates.IsNotNullOrEmpty() || job.JobLicences.IsNotNullOrEmpty())
				postingNode.CreateQualificationsNode(job);

			if (job.JobLanguages.IsNotNullOrEmpty())
				postingNode.CreateLanguagesNode(job, runtimeContext.Helpers.Lookup);

		}

		/// <summary>
		/// Creates a duties node.
		/// </summary>
		/// <param name="postingNode">The posting node.</param>
		/// <param name="runtimeContext">The runtime context.</param>
		/// <param name="job">The job.</param>
		/// <param name="culture">The culture.</param>
		private static void CreateDutiesNode(this XmlNode postingNode, IRuntimeContext runtimeContext, Data.Core.Entities.Job job, string culture)
		{
			var dutiesNode = postingNode.CreateElement("duties");

			if (job.Description.IsNotNullOrEmpty()) dutiesNode.InnerText = job.Description;
			Onet onet = null;

			if (job.OnetId.IsNotNull())
				onet = runtimeContext.Repositories.Library.FindById<Onet>(job.OnetId);

			if (job.JobTitle.IsNotNullOrEmpty() || onet.IsNotNull())
			{
				var titleNode = dutiesNode.CreateElement("title");

				if (job.JobTitle.IsNotNullOrEmpty()) titleNode.InnerText = job.JobTitle;

				if (onet.IsNotNull())
				{
					var onetAttribute = titleNode.CreateAttribute("onet");
					onetAttribute.Value = onet.OnetCode;
				}
			}

			if (onet.IsNotNull())
				dutiesNode.SetStringValue("onettitle", runtimeContext.Helpers.Occupation.LocaliseOnetOccupation(onet.Key, culture));

			if (job.EmploymentStatusId.IsNotNullOrZero())
			{
				var employmentStatus = runtimeContext.Helpers.Lookup.GetLookup(LookupTypes.EmploymentStatuses).Where(x => x.Id == job.EmploymentStatusId).Select(x => x.Key).FirstOrDefault();
				if (employmentStatus.IsNotNullOrEmpty())
				{
					var jobTypeNode = dutiesNode.CreateElement("jobtype");
					var hoursAttribute = jobTypeNode.CreateAttribute("hours");
					hoursAttribute.Value = (employmentStatus == "EmploymentStatuses.Fulltime" ? "fulltime" : "parttime");
				}
			}
		}

		/// <summary>
		/// Creates the contact node.
		/// </summary>
		/// <param name="postingNode">The posting node.</param>
		/// <param name="job">The job.</param>
		/// <param name="lookupHelper">The lookup helper.</param>
		private static void CreateContactNode(this XmlNode postingNode, Data.Core.Entities.Job job, ILookupHelper lookupHelper)
		{
			var contactNode = postingNode.CreateElement("contact");

			if (job.JobLocations.IsNotNullOrEmpty()) contactNode.CreateContactAddressNode(job.JobLocations[0], lookupHelper);

			if (job.Employer.IsNotNull() && job.Employer.Name.IsNotNullOrEmpty())
				contactNode.SetStringValue("company", job.Employer.Name);

			if (job.Employee.IsNotNull())
			{
				if (job.Employee.Person.IsNotNull())
				{
					var externalId = lookupHelper.GetLookupExternalId(LookupTypes.Titles, job.Employee.Person.TitleId);

					if (externalId.IsNotNullOrEmpty())
						contactNode.SetStringValue("person_title", externalId);

					var nameBuilder = new StringBuilder("");
					if (job.Employee.Person.FirstName.IsNotNullOrEmpty())
						nameBuilder.AppendFormat("{0} ", job.Employee.Person.FirstName);
					if (job.Employee.Person.LastName.IsNotNullOrEmpty()) nameBuilder.Append(job.Employee.Person.LastName);
					if (nameBuilder.Length > 0)
						contactNode.SetStringValue("person", nameBuilder.ToString());
				}
			}

			if (job.InterviewPhoneNumber.IsNotNullOrEmpty()) contactNode.SetStringValue("phone", job.InterviewPhoneNumber);
			if (job.InterviewApplicationUrl.IsNotNullOrEmpty())
				contactNode.SetStringValue("joburl", job.InterviewApplicationUrl);
			if (job.InterviewFaxNumber.IsNotNullOrEmpty()) contactNode.SetStringValue("fax", job.InterviewFaxNumber);
			if (job.InterviewEmailAddress.IsNotNullOrEmpty()) contactNode.SetStringValue("email", job.InterviewEmailAddress);
		}

		/// <summary>
		/// Creates the contact address node.
		/// </summary>
		/// <param name="contactNode">The contact node.</param>
		/// <param name="jobLocation">The job location.</param>
		/// <param name="lookupHelper">The lookup helper.</param>
		private static void CreateContactAddressNode(this XmlNode contactNode, JobLocation jobLocation, ILookupHelper lookupHelper)
		{
			var addressNode = contactNode.CreateElement("address");

			if (jobLocation.AddressLine1.IsNotNullOrEmpty() || (jobLocation.Zip.IsNotNullOrEmpty() && jobLocation.State.IsNotNullOrEmpty()))
			{
				// this is a 'full address' job location

				if (jobLocation.City.IsNotNullOrEmpty())
				{
					addressNode.SetStringValue("city", jobLocation.City);
					addressNode.SetStringValue("majorcity", jobLocation.City);
				}

				if (jobLocation.State.IsNotNullOrEmpty())
				{
					addressNode.SetStringValue("state", jobLocation.State);
					var stateLookup = lookupHelper.GetLookup(LookupTypes.States).FirstOrDefault(x => x.Text == jobLocation.State);
					if (stateLookup.IsNull())
						stateLookup = lookupHelper.GetLookup(LookupTypes.States).FirstOrDefault(x => x.Key == string.Concat("State.", jobLocation.State));

					if (stateLookup.IsNotNull())
					{
						var stateNode = ((XmlElement)addressNode).GetElementsByTagName("state")[0];
						var stateAttr = stateNode.CreateAttribute("code");
						stateAttr.Value = stateLookup.ExternalId;
					}
				}

				if (jobLocation.Zip.IsNotNullOrEmpty())
					addressNode.SetStringValue("postalcode", jobLocation.Zip);
			}
			else
			{
				// Job Locations are strings in the format "City, State-Code (Zip-Code) or City, County, State-Code (Zip-Code)" 
				var location = jobLocation.Location;

				// Split the relevent information	
				var firstIndexOfComma = location.IndexOf(",", StringComparison.Ordinal);
				var lastIndexOfComma = location.LastIndexOf(",", StringComparison.Ordinal);
				var lastIndexOfOpenBracket = location.LastIndexOf("(", StringComparison.Ordinal);
				var lastIndexOfCloseBracket = location.LastIndexOf(")", StringComparison.Ordinal);

				var city = firstIndexOfComma >= 0
					? location.Substring(0, firstIndexOfComma)
					: string.Empty;

				var stateCode = lastIndexOfComma >= 0
					? location.Substring(lastIndexOfComma + 2, ((lastIndexOfOpenBracket - 1) - (lastIndexOfComma + 2)))
					: location.Substring(0, lastIndexOfOpenBracket - 1);

				var postalCode = location.Substring(lastIndexOfOpenBracket + 1,
					(lastIndexOfCloseBracket - (lastIndexOfOpenBracket + 1)));

				if (city.IsNotNullOrEmpty())
				{
					addressNode.SetStringValue("city", city);
					addressNode.SetStringValue("majorcity", city);
				}

				if (stateCode.IsNotNullOrEmpty())
					addressNode.SetStringValue("state", stateCode);

				if (postalCode.IsNotNullOrEmpty())
					addressNode.SetStringValue("postalcode", postalCode);
			}
		}

		/// <summary>
		/// Creates a benefits node.
		/// </summary>
		/// <param name="postingNode">The posting node.</param>
		/// <param name="job">The job.</param>
		/// <param name="lookupHelper">The lookup helper.</param>
		private static void CreateBenefitsNode(this XmlNode postingNode, Data.Core.Entities.Job job, ILookupHelper lookupHelper)
		{
			XmlNode benefitsNode = null;

			// Salary is passed as "min-max currencysymbol salaryfrequencyid", ie "20000-40000 $ 5"
			// Using Default Values min 0, max 9,999,999, currency $, frequency 5 (5 = yearly)
			if (job.MinSalary.HasValue || job.MaxSalary.HasValue)
			{
				benefitsNode = postingNode.CreateElement("benefits");

				var minimumSalary = job.MinSalary ?? 0;
				var maximumSalary = job.MaxSalary ?? 9999999;

				var frequencyExternalId = (job.SalaryFrequencyId.HasValue)
																		? lookupHelper.GetLookupExternalId(LookupTypes.Frequencies, job.SalaryFrequencyId.Value)
																		: String.Empty;

				benefitsNode.SetStringValue("salary",
																		String.Format("{0}-{1} {2} {3}", minimumSalary.ToString("F2"),
																									maximumSalary.ToString("F2"), "$", frequencyExternalId));

			}

			if (job.NormalWorkDays.IsNotNull() || job.WorkDaysVary.HasValue)
			{
				if (benefitsNode.IsNull())
					benefitsNode = postingNode.CreateElement("benefits");

				var workdaysNode = benefitsNode.CreateElement("workdays");

				if (job.NormalWorkDays.IsNotNull())
				{
					var daysOfWeek = job.NormalWorkDays.Value;

					foreach (var day in DaysToCheck)
					{
						DayOfWeekValue(workdaysNode, daysOfWeek, day);
					}
				}

				if (job.WorkDaysVary.HasValue)
					workdaysNode.SetStringValue("varies", job.WorkDaysVary.Value.AsOneOrZero());
			}
		}

		/// <summary>
		/// Adds a node for a normal working day
		/// </summary>
		/// <param name="workdaysNode">The "workdays" to which to add the working day</param>
		/// <param name="daysOfWeek">The Job's normal work days</param>
		/// <param name="dayToCheck">The day to check</param>
		private static void DayOfWeekValue(XmlNode workdaysNode, DaysOfWeek daysOfWeek, DaysOfWeek dayToCheck)
		{
			var isDay = ((daysOfWeek & dayToCheck) == dayToCheck);

			workdaysNode.SetStringValue(dayToCheck.ToString().ToLowerInvariant(), isDay.AsOneOrZero());
		}

		/// <summary>
		/// Creates a background node.
		/// </summary>
		/// <param name="postingNode">The posting node.</param>
		/// <param name="job">The job.</param>
		private static void CreateBackgroundNode(this XmlNode postingNode, Data.Core.Entities.Job job)
		{
			var backgroundNode = postingNode.CreateElement("background");

			var monthsExperience = 0;

			if (job.MinimumExperience.HasValue)
				monthsExperience = job.MinimumExperience.Value * 12;

			if (job.MinimumExperienceMonths.HasValue)
				monthsExperience = monthsExperience + job.MinimumExperienceMonths.Value;

			backgroundNode.SetIntegerValue("experience", monthsExperience);
		}

		/// <summary>
		/// Creates the job info node.
		/// </summary>
		/// <param name="postingNode">The posting node.</param>
		/// <param name="job">The job.</param>
		private static void CreateJobInfoNode(this XmlNode postingNode, Data.Core.Entities.Job job)
		{
			var jobInfoNode = postingNode.CreateElement("jobinfo");

			if (job.NumberOfOpenings.HasValue)
				jobInfoNode.SetIntegerValue("positioncount", job.NumberOfOpenings);

			if (job.SuitableForHomeWorker.GetValueOrDefault())
				jobInfoNode.SetStringValue("suitable_for_homeworker", "1");

			if (job.IsCommissionBased.GetValueOrDefault())
				jobInfoNode.SetStringValue("commission_based", "1");

			if (job.IsSalaryAndCommissionBased.GetValueOrDefault())
				jobInfoNode.SetStringValue("salary_and_commission_based", "1");

			if (job.MinimumEducationLevel.HasValue)
			{
				var educationLevelCode = "0";

				if ((job.MinimumEducationLevel & EducationLevels.NoDiploma) == EducationLevels.NoDiploma)
					educationLevelCode = "10";
				if ((job.MinimumEducationLevel & EducationLevels.HighSchoolDiplomaOrEquivalent) == EducationLevels.HighSchoolDiplomaOrEquivalent)
					educationLevelCode = "12";
				if ((job.MinimumEducationLevel & EducationLevels.HighSchoolDiploma) == EducationLevels.HighSchoolDiploma)
					educationLevelCode = "12";
				if ((job.MinimumEducationLevel & EducationLevels.SomeCollegeNoDegree) == EducationLevels.SomeCollegeNoDegree)
					educationLevelCode = "13";
				if ((job.MinimumEducationLevel & EducationLevels.AssociatesDegree) == EducationLevels.AssociatesDegree)
					educationLevelCode = "14";
				if ((job.MinimumEducationLevel & EducationLevels.BachelorsDegree) == EducationLevels.BachelorsDegree)
					educationLevelCode = "16";
				if ((job.MinimumEducationLevel & EducationLevels.GraduateDegree) == EducationLevels.GraduateDegree)
					educationLevelCode = "18";
				if ((job.MinimumEducationLevel & EducationLevels.MastersDegree) == EducationLevels.MastersDegree)
					educationLevelCode = "18";
				if ((job.MinimumEducationLevel & EducationLevels.DoctorateDegree) == EducationLevels.DoctorateDegree)
					educationLevelCode = "21";

				jobInfoNode.SetStringValue("EDUCATION_CD", educationLevelCode);
			}

			if (job.JobType != JobTypes.None)
				jobInfoNode.SetIntegerValue("JOB_TYPE", (int)job.JobType);

		}

		/// <summary>
		/// Creates a qualifications node.
		/// </summary>
		/// <param name="postingNode">The posting node.</param>
		/// <param name="job">The job.</param>
		private static void CreateQualificationsNode(this XmlNode postingNode, Data.Core.Entities.Job job)
		{
			var qualificationsNode = postingNode.CreateElement("qualifications");

			var certificatesRequired = (job.CertificationRequired.HasValue && job.CertificationRequired.Value);
			var licenceRequired = (job.LicencesRequired.HasValue && job.LicencesRequired.Value);

			var certificateBuilder = new StringBuilder("");
			var licenceBuilder = new StringBuilder("");

			if (job.JobCertificates.IsNotNull())
			{
				foreach (var certificate in job.JobCertificates)
					certificateBuilder.AppendFormat("{0}, ", certificate.Certificate);
			}

			if (job.JobLicences.IsNullOrEmpty())
			{
				foreach (var licence in job.JobLicences)
				{
					licenceBuilder.AppendFormat("{0}, ", licence.Licence);
				}
			}

			if ((certificatesRequired && certificateBuilder.Length > 0) || (licenceRequired && licenceBuilder.Length > 0))
			{
				var requiredNode = qualificationsNode.CreateElement("required");

				if (certificatesRequired && certificateBuilder.Length > 0 && licenceRequired && licenceBuilder.Length > 0)
				{
					var requiredCertification = certificateBuilder.Append(licenceBuilder).ToString();
					requiredNode.SetStringValue("certification", requiredCertification.Substring(0, requiredCertification.Length - 2));
				}
				else if (certificatesRequired && certificateBuilder.Length > 0)
				{
					var requiredCertification = certificateBuilder.ToString();
					requiredNode.SetStringValue("certification", requiredCertification.Substring(0, requiredCertification.Length - 2));
				}
				else if (licenceRequired && licenceBuilder.Length > 0)
				{
					var requiredCertification = licenceBuilder.ToString();
					requiredNode.SetStringValue("certification", requiredCertification.Substring(0, requiredCertification.Length - 2));
				}
			}

			if (((!certificatesRequired) && certificateBuilder.Length > 0) || ((!licenceRequired) && licenceBuilder.Length > 0))
			{
				var preferredNode = qualificationsNode.CreateElement("preferred");

				if ((!certificatesRequired) && certificateBuilder.Length > 0 && (!licenceRequired) && licenceBuilder.Length > 0)
				{
					var preferredCertification = certificateBuilder.Append(licenceBuilder).ToString();
					preferredNode.SetStringValue("certification",
																			 preferredCertification.Substring(0, preferredCertification.Length - 2));
				}
				else if (certificatesRequired && certificateBuilder.Length > 0)
				{
					var preferredCertification = certificateBuilder.ToString();
					preferredNode.SetStringValue("certification",
																			 preferredCertification.Substring(0, preferredCertification.Length - 2));
				}
				else if (licenceRequired && licenceBuilder.Length > 0)
				{
					var preferredCertification = licenceBuilder.ToString();
					preferredNode.SetStringValue("certification",
																			 preferredCertification.Substring(0, preferredCertification.Length - 2));
				}
			}
		}

		/// <summary>
		/// Creates a qualifications node.
		/// </summary>
		/// <param name="postingNode">The posting node.</param>
		/// <param name="lookupHelper">The lookup helper.</param>
		/// <param name="job">The job.</param>
		private static void CreateLanguagesNode(this XmlNode postingNode, Data.Core.Entities.Job job, ILookupHelper lookupHelper)
		{
			var languagesNode = postingNode.CreateElement("languages");
			if (job.JobLanguages.IsNotNullOrEmpty())
			{
				foreach (var language in job.JobLanguages)
				{
					var langElement = languagesNode.CreateElement("language");
					langElement.SetStringValue("language", language.Language);
					if (language.LanguageProficiencyId.IsNotNullOrZero())
					{
						var externalId = lookupHelper.GetLookupExternalId(LookupTypes.LanguageProficiencies, language.LanguageProficiencyId.Value);
						langElement.SetStringValue("proficiency", externalId);
					}
				}
			}
		}

		/// <summary>
		/// Creates a special node.
		/// </summary>
		/// <param name="jobDocNode">The job doc node.</param>
		/// <param name="job">The job.</param>
		/// <param name="clientOfficeId">The client office id.</param>
		/// <param name="runtimeContext">The runtime context.</param>
		private static XmlNode CreateSpecialNode(this XmlNode jobDocNode, Data.Core.Entities.Job job, string clientOfficeId, IRuntimeContext runtimeContext)
		{
			var specialNode = jobDocNode.CreateElement("special");

			var isUpdate = job.PostedOn.HasValue;

			specialNode.SetStringValue("mode", isUpdate ? "update" : "insert");

			specialNode.SetIntegerValue("originid", 7);

			if (job.JobTitle.IsNotNullOrEmpty())
				specialNode.SetStringValue("jobtitle", job.JobTitle);

			if (job.BusinessUnit.IsNotNull() && job.BusinessUnit.Name.IsNotNullOrEmpty())
			{
				specialNode.SetStringValue("jobemployer", job.GetEmployerName(runtimeContext));
				if (job.IsConfidential.GetValueOrDefault())
					specialNode.SetStringValue("talentemployer", job.BusinessUnit.Name);
			}

			if (job.JobStatus.IsNotNull())
			{
				var lensStatusCode = job.JobStatus.ToLensStatus();
				if (lensStatusCode > 0) specialNode.SetIntegerValue("job_status_cd", lensStatusCode);
			}

			if (job.JobTitle.IsNotNullOrEmpty())
				specialNode.SetStringValue("jobtitle", job.JobTitle);

			if (clientOfficeId.IsNotNullOrEmpty())
				specialNode.SetStringValue("office_id", clientOfficeId);

			if (job.ExternalId.IsNotNullOrEmpty())
				specialNode.SetStringValue("foreignpostingid", job.ExternalId);

			specialNode.SetIntegerValue("jobtype", 4);
			specialNode.SetIntegerValue("JOBTYPE_CD", 4);

			return specialNode;
		}

		/// <summary>
		/// Converts the job status to a lens status.
		/// </summary>
		/// <param name="jobStatus">The job status.</param>
		/// <returns></returns>
		private static int ToLensStatus(this JobStatuses jobStatus)
		{
			var lensStatusCode = 0;

			switch (jobStatus)
			{
				case JobStatuses.Active:
					lensStatusCode = 1;
					break;
				case JobStatuses.AwaitingEmployerApproval:
					lensStatusCode = 3;
					break;
				case JobStatuses.Closed:
					lensStatusCode = 7;
					break;
				case JobStatuses.OnHold:
					lensStatusCode = 5;
					break;
			}

			return lensStatusCode;
		}

		/// <summary>
		/// Creates the requirements nodes.
		/// </summary>
		/// <param name="requirementsNode">The requirements node.</param>
		/// <param name="job">The job.</param>
		/// <param name="lookupHelper">The lookup helper.</param>
		private static void CreateRequirementsNodes(this XmlNode requirementsNode, Data.Core.Entities.Job job, ILookupHelper lookupHelper)
		{
			#region Age

			if (job.MinimumAge.HasValue) requirementsNode.SetMinimumAgeRequirement(job.MinimumAge.Value, job.MinimumAgeReason, (job.MinimumAgeRequired.HasValue && job.MinimumAgeRequired.Value));

			#endregion

			#region Certifications

			if (job.JobCertificates.Count > 0) requirementsNode.SetCertificatesRequirements(job.JobCertificates, (job.CertificationRequired.HasValue && job.CertificationRequired.Value));

			#endregion

			#region Driver Licences

			if (job.DrivingLicenceClassId.HasValue && job.DrivingLicenceClassId > 0)
				requirementsNode.SetDrivingLicenceRequirement(job.DrivingLicenceClassId, job.JobDrivingLicenceEndorsements.Select(endorsement => endorsement.AsDto()).ToList(), (job.DrivingLicenceRequired.HasValue && job.DrivingLicenceRequired.Value), lookupHelper);

			#endregion

			#region Languages

			if (job.JobLanguages.Count > 0) requirementsNode.SetLanguagesRequirements(job.JobLanguages, (job.LanguagesRequired.HasValue && job.LanguagesRequired.Value), lookupHelper);

			#endregion

			#region Career Readiness

			if (job.CareerReadinessLevel.IsNotNullOrZero())
				requirementsNode.SetCareerReadinessRequirement(job.CareerReadinessLevel.Value, job.CareerReadinessLevelRequired.GetValueOrDefault(), lookupHelper);

			#endregion

			#region ProgramsOfStudy

			if (job.JobProgramOfStudies.Count > 0) requirementsNode.SetProgramsOfStudyRequirements(job.JobProgramOfStudies, (job.ProgramsOfStudyRequired.HasValue && job.ProgramsOfStudyRequired.Value));

			#endregion

			#region Licences

			if (job.JobLicences.Count > 0) requirementsNode.SetLicencesRequirements(job.JobLicences, (job.LicencesRequired.HasValue && job.LicencesRequired.Value));

			#endregion

			#region Minimum Education

			if (job.MinimumEducationLevel.HasValue && job.MinimumEducationLevel != EducationLevels.None)
				requirementsNode.SetMinimumEducationRequirement(job.MinimumEducationLevel.Value, (job.MinimumEducationLevelRequired.HasValue && job.MinimumEducationLevelRequired.Value), lookupHelper);

			#endregion

			#region Onet

			// Not used in this release

			#endregion

			#region Skills

			// Not used in this release

			#endregion

			#region Special

			if (job.JobSpecialRequirements.Count > 0) requirementsNode.SetSpecialRequirements(job.JobSpecialRequirements);

			#endregion
		}


		/// <summary>
		/// Sets the minimum age requirement.
		/// </summary>
		/// <param name="requirementsNode">The requirements node.</param>
		/// <param name="minimumAge">The minimum age.</param>
		/// <param name="reason">The reason.</param>
		/// <param name="isRequirement">if set to <c>true</c> [is requirement].</param>
		private static void SetMinimumAgeRequirement(this XmlNode requirementsNode, int minimumAge, string reason, bool isRequirement)
		{
			var mandateFlag = (isRequirement) ? "1" : "0";
			var minimumAgeNode = requirementsNode.CreateElement("REQUIREMENT");
			minimumAgeNode.CreateAttribute("type").Value = "AGE";
			minimumAgeNode.CreateAttribute("mandate").Value = mandateFlag;

			minimumAgeNode.CreateElement("MIN").InnerText = minimumAge.ToString();
			minimumAgeNode.CreateElement("REASON").InnerText = reason;
		}
		/// <summary>
		/// Sets the certificates requirements.
		/// </summary>
		/// <param name="requirementsNode">The requirements node.</param>
		/// <param name="jobCertificates">The job certificates.</param>
		/// <param name="isRequirement">if set to <c>true</c> [is requirement].</param>
		private static void SetCertificatesRequirements(this XmlNode requirementsNode, IEnumerable<JobCertificate> jobCertificates, bool isRequirement)
		{
			var mandateFlag = (isRequirement) ? "1" : "0";

			var certificateNode = requirementsNode.CreateElement("REQUIREMENT");
			certificateNode.CreateAttribute("type").Value = "CERTIFICATIONS";
			certificateNode.CreateAttribute("mandate").Value = mandateFlag;

			foreach (var jobCertificate in jobCertificates)
			{
				certificateNode.SetCertificateRequirement(jobCertificate);
			}
		}

		/// <summary>
		/// Sets the certificate requirement.
		/// </summary>
		/// <param name="certificateNode">The certificate node.</param>
		/// <param name="jobCertificate">The job certificate.</param>
		private static void SetCertificateRequirement(this XmlNode certificateNode, JobCertificate jobCertificate)
		{
			certificateNode.CreateElement("CERTIFICATE").InnerText = jobCertificate.Certificate;
		}

		/// <summary>
		/// Sets the driving licence requirement.
		/// </summary>
		/// <param name="requirementsNode">The requirements node.</param>
		/// <param name="classId">The class id.</param>
		/// <param name="endorsements">The endorsements.</param>
		/// <param name="isRequirement">if set to <c>true</c> [is requirement].</param>
		/// <param name="lookupHelper">The lookup helper.</param>
		private static void SetDrivingLicenceRequirement(this XmlNode requirementsNode, long? classId, List<JobDrivingLicenceEndorsementDto> endorsements, bool isRequirement, ILookupHelper lookupHelper)
		{
			if (!classId.HasValue || classId == 0) return;

			var mandateFlag = (isRequirement) ? "1" : "0";

			var drivingLicenceNode = requirementsNode.CreateElement("REQUIREMENT");
			drivingLicenceNode.CreateAttribute("type").Value = "DRIVERS_LICENSE";
			drivingLicenceNode.CreateAttribute("mandate").Value = mandateFlag;

			var drivingLicenceClass = "";

			try
			{
				drivingLicenceClass = lookupHelper.GetLookupText(LookupTypes.DrivingLicenceClasses, classId.Value);
			}
			catch { }

			if (drivingLicenceClass.IsNullOrEmpty()) return;

			drivingLicenceNode.CreateElement("CLASS").InnerText = drivingLicenceClass;

			var endorsementsStringBuilder = new StringBuilder("");

			if (endorsements.IsNotNullOrEmpty())
			{
				foreach (var lookupText in endorsements.Select(endorsement => lookupHelper.GetLookupText(LookupTypes.DrivingLicenceEndorsements, endorsement.DrivingLicenceEndorsementId)).Where(lookupText => lookupText.IsNotNullOrEmpty()))
				{
					endorsementsStringBuilder.AppendFormat("{0}, ", lookupText);
				}
			}

			var endorsementsString = (endorsementsStringBuilder.Length > 2) ? endorsementsStringBuilder.ToString().Substring(0, endorsementsStringBuilder.Length - 2) : endorsementsStringBuilder.ToString();

			drivingLicenceNode.CreateElement("ENDORSEMENTS").InnerText = endorsementsString;
		}

		/// <summary>
		/// Sets the languages requirements.
		/// </summary>
		/// <param name="requirementsNode">The requirements node.</param>
		/// <param name="jobLanguages">The job languages.</param>
		/// <param name="isRequirement">if set to <c>true</c> [is requirement].</param>
		/// <param name="lookupHelper">The lookup helper.</param>
		private static void SetLanguagesRequirements(this XmlNode requirementsNode, IEnumerable<JobLanguage> jobLanguages, bool isRequirement, ILookupHelper lookupHelper)
		{
			var mandateFlag = (isRequirement) ? "1" : "0";

			var requirementNode = requirementsNode.CreateElement("REQUIREMENT");
			requirementNode.CreateAttribute("type").Value = "LANGUAGES";
			requirementNode.CreateAttribute("mandate").Value = mandateFlag;

			foreach (var jobLanguage in jobLanguages)
			{
				requirementNode.SetLanguageRequirement(jobLanguage, lookupHelper);
			}
		}

		/// <summary>
		/// Sets the language requirement.
		/// </summary>
		/// <param name="requirementNode">The requirement node for language.</param>
		/// <param name="jobLanguage">The job language.</param>
		/// <param name="lookupHelper">The lookup helper.</param>
		private static void SetLanguageRequirement(this XmlNode requirementNode, JobLanguage jobLanguage, ILookupHelper lookupHelper)
		{
			var languageNode = (XmlElement)requirementNode.CreateElement("LANGUAGE");
			languageNode.InnerText = jobLanguage.Language;
			if (jobLanguage.LanguageProficiencyId.IsNotNullOrZero())
			{
				var externalId = lookupHelper.GetLookupExternalId(LookupTypes.LanguageProficiencies, jobLanguage.LanguageProficiencyId.GetValueOrDefault(0));
				languageNode.SetAttribute("PROFICIENCY", externalId);
			}
		}


		/// <summary>
		/// Sets the career readiness requirement
		/// </summary>
		/// <param name="requirementsNode">The requirements node</param>
		/// <param name="careerLevelId">The career level identifier</param>
		/// <param name="isRequirement">Is the requirment mandatory?</param>
		/// <param name="lookupHelper">The lookup helper</param>
		private static void SetCareerReadinessRequirement(this XmlNode requirementsNode, long careerLevelId, bool isRequirement, ILookupHelper lookupHelper)
		{
			var mandateFlag = (isRequirement) ? "1" : "0";

			var careerReadinessNode = requirementsNode.CreateElement("REQUIREMENT");
			careerReadinessNode.CreateAttribute("type").Value = "NCRCLEVEL";
			careerReadinessNode.CreateAttribute("mandate").Value = mandateFlag;

			careerReadinessNode.InnerText = lookupHelper.GetLookupText(LookupTypes.NCRCLevel, careerLevelId);
		}

		/// <summary>
		/// Sets the programs of study requirements.
		/// </summary>
		/// <param name="requirementsNode">The requirements node.</param>
		/// <param name="jobProgramsOfStudy">The job programs of study.</param>
		/// <param name="isRequirement">if set to <c>true</c> [is requirement].</param>
		private static void SetProgramsOfStudyRequirements(this XmlNode requirementsNode, IEnumerable<JobProgramOfStudy> jobProgramsOfStudy, bool isRequirement)
		{
			var mandateFlag = (isRequirement) ? "1" : "0";

			var programOfStudyNode = requirementsNode.CreateElement("REQUIREMENT");
			programOfStudyNode.CreateAttribute("type").Value = "PROGRAMSOFSTUDY";
			programOfStudyNode.CreateAttribute("mandate").Value = mandateFlag;

			foreach (var jobProgramOfStudy in jobProgramsOfStudy)
			{
				programOfStudyNode.SetProgramOfStudyRequirement(jobProgramOfStudy);
			}
		}

		/// <summary>
		/// Sets the program of study requirement.
		/// </summary>
		/// <param name="programOfStudyNode">The program of study node.</param>
		/// <param name="jobProgramOfStudy">The job program of study.</param>
		private static void SetProgramOfStudyRequirement(this XmlNode programOfStudyNode, JobProgramOfStudy jobProgramOfStudy)
		{
			var element = programOfStudyNode.CreateElement("PROGRAMOFSTUDY");
			element.InnerText = jobProgramOfStudy.ProgramOfStudy;
			element.CreateAttribute("ID").Value = jobProgramOfStudy.DegreeEducationLevelId.ToString();
		}

		/// <summary>
		/// Sets the licences requirements.
		/// </summary>
		/// <param name="requirementsNode">The requirements node.</param>
		/// <param name="jobLicences">The job licences.</param>
		/// <param name="isRequirement">if set to <c>true</c> [is requirement].</param>
		private static void SetLicencesRequirements(this XmlNode requirementsNode, IEnumerable<JobLicence> jobLicences, bool isRequirement)
		{
			var mandateFlag = (isRequirement) ? "1" : "0";

			var licenceNode = requirementsNode.CreateElement("REQUIREMENT");
			licenceNode.CreateAttribute("type").Value = "LICENSES";
			licenceNode.CreateAttribute("mandate").Value = mandateFlag;

			foreach (var jobLicence in jobLicences)
			{
				licenceNode.SetLicenceRequirement(jobLicence);
			}
		}

		/// <summary>
		/// Sets the licence requirement.
		/// </summary>
		/// <param name="licenceNode">The licence node.</param>
		/// <param name="jobLicence">The job licence.</param>
		private static void SetLicenceRequirement(this XmlNode licenceNode, JobLicence jobLicence)
		{
			licenceNode.CreateElement("LICENSE").InnerText = jobLicence.Licence;
		}

		/// <summary>
		/// Sets the minimum education requirement.
		/// </summary>
		/// <param name="requirementsNode">The requirements node.</param>
		/// <param name="minimumEducationLevel">The minimum education level.</param>
		/// <param name="isRequirement">if set to <c>true</c> [is requirement].</param>
		/// <param name="lookupHelper"></param>
		private static void SetMinimumEducationRequirement(this XmlNode requirementsNode, EducationLevels minimumEducationLevel, bool isRequirement, ILookupHelper lookupHelper)
		{
			var mandateFlag = (isRequirement) ? "1" : "0";

			var educationNode = requirementsNode.CreateElement("REQUIREMENT");
			educationNode.CreateAttribute("type").Value = "MIN_EDU";
			educationNode.CreateAttribute("mandate").Value = mandateFlag;

			//var educationLevelLocalisationKey = "EducationLevels." + minimumEducationLevel;
			var educationLevelLocalised = "";

			// Doing this as a switch so we don't have to ensure the localisation item exists in every database
			switch (minimumEducationLevel)
			{
				case EducationLevels.NoDiploma:
					educationLevelLocalised = lookupHelper.GetLookupText(LookupTypes.RequiredEducationLevels,
																													 "RequiredEducationLevel.NoDiploma",
																													 "No Diploma");
					break;
				case EducationLevels.HighSchoolDiplomaOrEquivalent:
					educationLevelLocalised = lookupHelper.GetLookupText(LookupTypes.RequiredEducationLevels,
																																		 "RequiredEducationLevel.HighSchoolDiplomaOrEquivalent",
																															 "High school diploma or equivalent");
					break;
				case EducationLevels.HighSchoolDiploma:
					educationLevelLocalised = lookupHelper.GetLookupText(LookupTypes.RequiredEducationLevels,
																															 "RequiredEducationLevel.HighSchoolDiploma",
																															 "High school diploma or equivalent");
					break;
				case EducationLevels.SomeCollegeNoDegree:
					educationLevelLocalised = lookupHelper.GetLookupText(LookupTypes.RequiredEducationLevels,
																													 "RequiredEducationLevel.NoDegree",
																													 "Some college, no degree");
					break;
				case EducationLevels.AssociatesDegree:
					educationLevelLocalised = lookupHelper.GetLookupText(LookupTypes.RequiredEducationLevels,
																															 "RequiredEducationLevel.AssociatesDegree",
																															 "Associate’s or vocational degree");
					break;
				case EducationLevels.BachelorsDegree:
					educationLevelLocalised = lookupHelper.GetLookupText(LookupTypes.RequiredEducationLevels,
																															 "RequiredEducationLevel.BachelorsDegree",
																															 "Bachelor's degree");
					break;
				case EducationLevels.GraduateDegree:
					educationLevelLocalised = lookupHelper.GetLookupText(LookupTypes.RequiredEducationLevels,
																															 "RequiredEducationLevel.GraduateDegree",
																															 "Masters degree or equivalent");
					break;
				case EducationLevels.MastersDegree:
					educationLevelLocalised = lookupHelper.GetLookupText(LookupTypes.RequiredEducationLevels,
																															 "RequiredEducationLevel.MastersDegree",
																															 "Master's degree");
					break;
				case EducationLevels.DoctorateDegree:
					educationLevelLocalised = lookupHelper.GetLookupText(LookupTypes.RequiredEducationLevels,
																															 "RequiredEducationLevel.DoctorateDegree",
																															 "Doctorate degree");
					break;
			}

			educationNode.InnerText = educationLevelLocalised;
		}

		/// <summary>
		/// Sets the special requirements.
		/// </summary>
		/// <param name="requirementsNode">The requirements node.</param>
		/// <param name="jobSpecialRequirements">The job special requirements.</param>
		private static void SetSpecialRequirements(this XmlNode requirementsNode, IEnumerable<JobSpecialRequirement> jobSpecialRequirements)
		{
			var specialRequirementNode = requirementsNode.CreateElement("REQUIREMENT");
			specialRequirementNode.CreateAttribute("type").Value = "SPECIAL";
			specialRequirementNode.CreateAttribute("mandate").Value = "1";

			foreach (var jobSpecialRequirement in jobSpecialRequirements)
			{
				specialRequirementNode.SetSpecialRequirement(jobSpecialRequirement);
			}
		}

		/// <summary>
		/// Sets the special requirement.
		/// </summary>
		/// <param name="specialRequirementNode">The special requirement node.</param>
		/// <param name="jobSpecialRequirement">The job special requirement.</param>
		private static void SetSpecialRequirement(this XmlNode specialRequirementNode, JobSpecialRequirement jobSpecialRequirement)
		{
			specialRequirementNode.CreateElement("SPECIAL").InnerText = jobSpecialRequirement.Requirement;
		}

		/// <summary>
		/// Sets the job contact flags.
		/// </summary>
		/// <param name="clientJobDataNode">The client job data node.</param>
		/// <param name="jobContactPreferences">The job contact preferences.</param>
		/// <param name="otherInstructions">Other instructions.</param>
		/// <param name="interviewMailAddress">Interview Mail Address.</param>
		/// <param name="interviewDirectApplicationDetails">Address for direct interview.</param>
		private static void SetJobContactFlags(this XmlNode clientJobDataNode, ContactMethods? jobContactPreferences, string otherInstructions, string interviewMailAddress, string interviewDirectApplicationDetails)
		{
			if (jobContactPreferences.IsNull()) jobContactPreferences = ContactMethods.None;

			clientJobDataNode.SetContactSendDirectFlag((jobContactPreferences & ContactMethods.InPerson) == ContactMethods.InPerson);
			clientJobDataNode.SetContactPostalFlag((jobContactPreferences & ContactMethods.Mail) == ContactMethods.Mail);
			clientJobDataNode.SetContactPhoneFlag((jobContactPreferences & ContactMethods.Telephone) == ContactMethods.Telephone);
			clientJobDataNode.SetContactFaxFlag((jobContactPreferences & ContactMethods.Fax) == ContactMethods.Fax);
			clientJobDataNode.SetContactEmailFlag((jobContactPreferences & ContactMethods.Email) == ContactMethods.Email);
			clientJobDataNode.SetContactUrlFlag((jobContactPreferences & ContactMethods.Online) == ContactMethods.Online);
			clientJobDataNode.SetContactTalentFlag((jobContactPreferences & ContactMethods.FocusTalent) == ContactMethods.FocusTalent);

			if (otherInstructions.IsNotNullOrEmpty())
				clientJobDataNode.SetStringValue("CONTACT_OTHER_INSTRUCTIONS", otherInstructions);

			if ((jobContactPreferences & ContactMethods.Mail) == ContactMethods.Mail && interviewMailAddress.IsNotNullOrEmpty())
				clientJobDataNode.SetStringValue("JOB_CONTACT_ADDR", interviewMailAddress);

			if ((jobContactPreferences & ContactMethods.InPerson) == ContactMethods.InPerson && interviewDirectApplicationDetails.IsNotNullOrEmpty())
				clientJobDataNode.SetStringValue("CONTACT_SEND_DIRECT_ADDRESS", interviewDirectApplicationDetails);
		}

		/// <summary>
		/// Sets the contact send direct flag.
		/// </summary>
		/// <param name="clientJobDataNode">The client job data node.</param>
		/// <param name="flagSet">if set to <c>true</c> [flag set].</param>
		private static void SetContactSendDirectFlag(this XmlNode clientJobDataNode, bool flagSet)
		{
			clientJobDataNode.SetFlagValue("CONTACT_SEND_DIRECT_FLAG", flagSet);
		}

		/// <summary>
		/// Sets the contact postal flag.
		/// </summary>
		/// <param name="clientJobDataNode">The client job data node.</param>
		/// <param name="flagSet">if set to <c>true</c> [flag set].</param>
		private static void SetContactPostalFlag(this XmlNode clientJobDataNode, bool flagSet)
		{
			clientJobDataNode.SetFlagValue("CONTACT_POSTAL_FLAG", flagSet);
		}

		/// <summary>
		/// Sets the contact phone flag.
		/// </summary>
		/// <param name="clientJobDataNode">The client job data node.</param>
		/// <param name="flagSet">if set to <c>true</c> [flag set].</param>
		private static void SetContactPhoneFlag(this XmlNode clientJobDataNode, bool flagSet)
		{
			clientJobDataNode.SetFlagValue("CONTACT_PHONE_FLAG", flagSet);
		}

		/// <summary>
		/// Sets the contact fax flag.
		/// </summary>
		/// <param name="clientJobDataNode">The client job data node.</param>
		/// <param name="flagSet">if set to <c>true</c> [flag set].</param>
		private static void SetContactFaxFlag(this XmlNode clientJobDataNode, bool flagSet)
		{
			clientJobDataNode.SetFlagValue("CONTACT_FAX_FLAG", flagSet);
		}

		/// <summary>
		/// Sets the contact email flag.
		/// </summary>
		/// <param name="clientJobDataNode">The client job data node.</param>
		/// <param name="flagSet">if set to <c>true</c> [flag set].</param>
		private static void SetContactEmailFlag(this XmlNode clientJobDataNode, bool flagSet)
		{
			clientJobDataNode.SetFlagValue("CONTACT_EMAIL_FLAG", flagSet);
		}

		/// <summary>
		/// Sets the contact URL flag.
		/// </summary>
		/// <param name="clientJobDataNode">The client job data node.</param>
		/// <param name="flagSet">if set to <c>true</c> [flag set].</param>
		private static void SetContactUrlFlag(this XmlNode clientJobDataNode, bool flagSet)
		{
			clientJobDataNode.SetFlagValue("CONTACT_URL_FLAG", flagSet);
		}

		/// <summary>
		/// Sets the contact talent flag.
		/// </summary>
		/// <param name="clientJobDataNode">The client job data node.</param>
		/// <param name="flagSet">if set to <c>true</c> [flag set].</param>
		private static void SetContactTalentFlag(this XmlNode clientJobDataNode, bool flagSet)
		{
			clientJobDataNode.SetFlagValue("CONTACT_TALENT_FLAG", flagSet);
		}

		/// <summary>
		/// Sets the salary nodes.
		/// </summary>
		/// <param name="clientJobDataNode">The client job data node.</param>
		/// <param name="job">The job.</param>
		/// <param name="lookupHelper">The lookup helper.</param>
		private static void SetSalaryNodes(this XmlNode clientJobDataNode, Data.Core.Entities.Job job, ILookupHelper lookupHelper)
		{
			clientJobDataNode.SetSalaryMinimum(job.MinSalary);
			clientJobDataNode.SetSalaryMaximum(job.MaxSalary);
			clientJobDataNode.SetSalaryUnitCode(job.SalaryFrequencyId, lookupHelper);
		}

		/// <summary>
		/// Sets the salary minimum.
		/// </summary>
		/// <param name="clientJobDataNode">The node.</param>
		/// <param name="salaryMinimum">The salary minimum.</param>
		private static void SetSalaryMinimum(this XmlNode clientJobDataNode, decimal? salaryMinimum)
		{
			// Salary details are not mandatory in job wizard, so setting to 0 as default
			if (!salaryMinimum.HasValue) salaryMinimum = 0;

			clientJobDataNode.SetDecimalValue("SAL_MIN", salaryMinimum);
		}

		/// <summary>
		/// Sets the salary maximum.
		/// </summary>
		/// <param name="clientJobDataNode">The node.</param>
		/// <param name="salaryMaximum">The salary maximum.</param>
		private static void SetSalaryMaximum(this XmlNode clientJobDataNode, decimal? salaryMaximum)
		{
			// Salary details are not mandatory in job wizard, so setting to 0 as default
			if (!salaryMaximum.HasValue) salaryMaximum = 0;

			clientJobDataNode.SetDecimalValue("SAL_MAX", salaryMaximum);
		}

		/// <summary>
		/// Sets the salary unit code.
		/// </summary>
		/// <param name="clientJobDataNode">The node.</param>
		/// <param name="salaryFrequencyLookUpId">The salary frequency look up id.</param>
		/// <param name="lookupHelper">The lookup helper.</param>
		private static void SetSalaryUnitCode(this XmlNode clientJobDataNode, long? salaryFrequencyLookUpId, ILookupHelper lookupHelper)
		{

			#region Possible values

			//1=Hourly
			//2=Daily
			//3=Weekly
			//4=Monthly
			//5=Yearly
			//6=Other

			#endregion

			var salaryFrequencyExternalId = "";

			if (salaryFrequencyLookUpId.HasValue)
			{
				try
				{
					salaryFrequencyExternalId = lookupHelper.GetLookupExternalId(LookupTypes.Frequencies, salaryFrequencyLookUpId.Value);
				}
				catch { }
			}

			clientJobDataNode.SetStringValue("SAL_UNIT_CD", salaryFrequencyExternalId);
		}

		#endregion
	}
}
