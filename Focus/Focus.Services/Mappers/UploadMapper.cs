﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using Focus.Core;
using Focus.Core.Models.Upload;
using Focus.Services.Mappers.Upload;

using Framework.ServiceLocation;

#endregion

namespace Focus.Services.Mappers
{
  public static class UploadMapper
  {
    /// <summary>
    /// Populates any null/empty fields in the upload record
    /// </summary>
    /// <param name="uploadFileRecord">The upload file record.</param>
    /// <param name="userId">The id of the user uploading the file.</param>
    /// <param name="recordStatus">The validation status of the record.</param>
    /// <param name="customInformation">Any custom information</param>
    /// <param name="runtimeContext">The runtime context.</param>
    public static void PopulateFields(IUploadedRecord uploadFileRecord, long userId, ValidationRecordStatus recordStatus, Dictionary<string, string> customInformation, IRuntimeContext runtimeContext)
    {
      RunPopulateFieldMappers(uploadFileRecord as dynamic, userId, recordStatus, customInformation, runtimeContext);
    }

    /// <summary>
    /// Runs the populate field mappers.
    /// </summary>
    /// <typeparam name="TUpload">The type of the upload.</typeparam>
    /// <param name="uploadedRecord">The uploaded record.</param>
    /// <param name="userId">The id of the user uploading the file.</param>
    /// <param name="recordStatus">The validation status of the record.</param>
    /// <param name="customInformation">The custom information.</param>
    /// <param name="runtimeContext">The runtime context.</param>
    private static void RunPopulateFieldMappers<TUpload>(TUpload uploadedRecord, long userId, ValidationRecordStatus recordStatus, Dictionary<string, string> customInformation, IRuntimeContext runtimeContext) where TUpload : class, IUploadedRecord
    {
      var mappers = ServiceLocator.Current.ResolveServices<IUploadMapper<TUpload>>();
      foreach (var mapper in mappers)
      {
        mapper.PopulateFields(uploadedRecord, userId, recordStatus, customInformation, runtimeContext);
      }
    }

    /// <summary>
    /// Populates any null/empty fields in the upload record
    /// </summary>
    /// <param name="uploadFileRecord">The upload file record.</param>
    /// <param name="runtimeContext">The runtime context.</param>
    public static void PublishMessage(IUploadedRecord uploadFileRecord, IRuntimeContext runtimeContext)
    {
      RunPublishMappers(uploadFileRecord as dynamic, runtimeContext);
    }

    /// <summary>
    /// Runs the upload mappers for the record
    /// </summary>
    /// <typeparam name="TUpload">The type of the uploaded record</typeparam>
    /// <param name="uploadedRecord">The uploaded record</param>
    /// <param name="runtimeContext">The runtime context.</param>
    private static void RunPublishMappers<TUpload>(TUpload uploadedRecord, IRuntimeContext runtimeContext) where TUpload : class, IUploadedRecord
    {
      var mappers = ServiceLocator.Current.ResolveServices<IUploadMapper<TUpload>>();
      foreach (var mapper in mappers)
      {
        mapper.PublishMessage(uploadedRecord, runtimeContext);
      }
    }
  }
}
