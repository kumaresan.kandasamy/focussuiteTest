﻿#region Copyright © 2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Framework.ServiceLocation;
using Framework.Api;
using Framework.Testing.NUnit;
using Newtonsoft.Json;
using NUnit.Framework;

#endregion

namespace Focus.UnitTests.Framework.WebApiClient
{
    [TestFixture]
    public class WebApiClientTests
    {
        private global::Framework.Api.WebApiClient client;
        [SetUp]
        public void SetUp()
        {
            client = new global::Framework.Api.WebApiClient("http://sandbox.api.burning-glass.com/v202/insight/jobs", "BGT", "086718ED10AC48B685F93535208AE903", "Insight", "32611633362D4A76A3C6038E71BFE90C");
        }

        [Test]
        [Explicit]
        public void GetLiData()
        {



					//var criteria = new LiCriteria
					//{
					//  groupBy = "Onet",
					//  geography = "US",
					//  includeTotalClassifiedPostings = true,
					//  includeTotalUnclassifiedPostings = true,
					//  limit = 10,
					//  offset = 0,
					//  queryString = "[nationwide]: \" nationwide  \"",
					//  timePeriod = new TimePeriod { from = new DateTime(2000, 1, 1), to = DateTime.Today }

					//};
					//Console.WriteLine(JsonConvert.SerializeObject(criteria));
					var criteria = "{\"groupBy\":\"Onet\",\"timePeriod\":{\"from\":\"2000-01-01T00:00:00\",\"to\":\"2015-08-04T00:00:00+01:00\"},\"queryString\":\"[nationwide]: \\\" nationwide  \\\"\",\"geography\":\"US\",\"includeTotalClassifiedPostings\":true,\"includeTotalUnclassifiedPostings\":true,\"offset\":0,\"limit\":10}";
					////  "{ \"groupBy\":\"State\", \"timePeriod\": { \"from\":\"2014-02-06T00:00:00\", \"to\":\"2014-05-06T00:00:00\" }, \  "queryString\":\"[nationwide]: \\\" nationwide  \\\"\", \"geography\":\"US\", \"includeTotalClassifiedPostings\":true, \"includeTotalUnclassifiedPostings\":true, \"offset\":0, \"limit\":10 }";
					//    //{   "groupBy":  "Onet",    "timePeriod":{    "from":  "2000-01-01T00:00:00",   "to":  "2015-08-04T00:00:00+01:00"},"queryString":"[nationwide]: \" nationwide  \"","geography":"US","includeTotalClassifiedPostings":true,"includeTotalUnclassifiedPostings":true,"offset":0,"limit":10}
					Console.WriteLine(criteria);
            var a = client.Post<RootObject>(criteria);

            var i = 0;
            i.ShouldEqual(0);
        }
    }

    public class TimePeriod
    {
        public DateTime from { get; set; }
        public DateTime to { get; set; }
    }

    public class LiCriteria
    {
        public string groupBy { get; set; }
        public TimePeriod timePeriod { get; set; }
        public string queryString { get; set; }
        public string geography { get; set; }
        public bool includeTotalClassifiedPostings { get; set; }
        public bool includeTotalUnclassifiedPostings { get; set; }
        public int offset { get; set; }
        public int limit { get; set; }
    }

    public class Datum
    {
        public string group { get; set; }
        public int count { get; set; }
    }

    public class ReportData
    {
        public List<Datum> data { get; set; }
        public int offset { get; set; }
        public int limit { get; set; }
        public int totalCount { get; set; }
        public int count { get; set; }
    }

    public class Data
    {
        public ReportData reportData { get; set; }
        public int totalClassifiedJobs { get; set; }
        public int totalUnclassifiedJobs { get; set; }
    }

    public class Result
    {
        public int count { get; set; }
        public Data data { get; set; }
    }

    public class RootObject
    {
        public Result result { get; set; }
        public string requestId { get; set; }
        public int timestamp { get; set; }
        public string status { get; set; }
        public string statusCode { get; set; }
    }

}
    