﻿#region Copyright © 2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Configuration;
using System.Threading;
using System.Threading.Tasks;

using Framework.Email;
using Framework.Email.Providers;

using NUnit.Framework;

#endregion

namespace Focus.UnitTests.Framework.Email.Providers.Smtp
{
	[TestFixture]
	[Explicit]
	public class SendEmailTests
	{
		private IEmailProvider _emailToSmtp, _emailToFile;
		private bool _emailerSetupAsAsync = false;

		[SetUp]
		public void SetUp()
		{
			SetUpEmailer(false);
		}

		[TearDown]
		public void TearDown()
		{
			// Need this so that the background thread is happy and is allows async operations to finish
			if(_emailerSetupAsAsync)
				Thread.Sleep(10000);

			Emailer.Instance.Detach();
		}

		[Test]
		public void SendEmail_WhenUsingSmtpAsyncOverride_SendsEmail()
		{
			Emailer.SendEmail("ukdev@aperture-control.com", "jmacbeth@burning-glass.com", "", "", "SendEmail_WhenUsingSmtpAsync_SendsEmail", "This is an Async send email", true);
		}

		[Test]
		public void SendEmail_WhenUsingSmtpAsyncFromConfig_SendsEmail()
		{
			SetUpEmailer(true);

			Emailer.SendEmail("ukdev@aperture-control.com", "jmacbeth@burning-glass.com", "", "", "SendEmail_WhenUsingSmtpAsync_SendsEmail", "This is an Async send email");
		}

		[Test]
		public void Send10Emails_WhenUsingSmtpAsync_SendsEmail()
		{
			for (var i=0; i<10; i++)
				Emailer.SendEmail("ukdev@aperture-control.com", "jmacbeth@burning-glass.com", "", "", String.Format("SendEmail_WhenUsingSmtpAsync_SendsEmail {0:00}", i), "This is an Async send email", true);
		}

		[Test]
		public void Send10EmailsFrom3Threads_WhenUsingSmtpAsync_SendsEmail()
		{
			Task.Factory.StartNew(Send10Emails);
			Task.Factory.StartNew(Send10Emails);
			Task.Factory.StartNew(Send10Emails);
		}

		[Test]
		public void SendEmail_WhenUsingSmtpSync_SendsEmail()
		{
			Emailer.SendEmail("ukdev@aperture-control.com", "jmacbeth@burning-glass.com", "", "", "SendEmail_WhenUsingSmtpSync_SendsEmail", "This is an Async send email");
		}

		private static void Send10Emails()
		{
			for (var i = 0; i < 10; i++)
				Emailer.SendEmail("ukdev@aperture-control.com", "jmacbeth@burning-glass.com", "", "", String.Format("SendEmail_WhenUsingSmtpAsync_SendsEmail {0:0000} - {1:00}", Thread.CurrentThread.ManagedThreadId, i), "This is an Async send email", true);
		}

		private void SetUpEmailer(bool configureAsAsync)
		{
			Emailer.Instance.Detach();
			_emailerSetupAsAsync = configureAsAsync;

			var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);		

			config.AppSettings.Settings.Remove("Focus-Emailer-SendAsync");
			config.AppSettings.Settings.Add("Focus-Emailer-SendAsync", configureAsAsync.ToString());

			config.Save(ConfigurationSaveMode.Modified);

			ConfigurationManager.RefreshSection("appSettings");
			
			_emailToSmtp = new EmailToSmtp();
			_emailToFile = new EmailToFile(@"c:\Temp\Focus\Emails");
			Emailer.Instance.Attach(_emailToSmtp);
			Emailer.Instance.Attach(_emailToFile);			
		}
	}
}
