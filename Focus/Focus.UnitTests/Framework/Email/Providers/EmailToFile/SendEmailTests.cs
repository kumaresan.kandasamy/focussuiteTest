﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Net.Mail;
using Framework.Email;
using Framework.Email.Providers;
using NUnit.Framework;
using MailMessage = System.Net.Mail.MailMessage;

#endregion

namespace Focus.UnitTests.Framework.Email.Providers.File
{
	[TestFixture]
	public class SendEmailTests 
	{
		[Test]
		[Ignore]
		public void SendEmail_WhenPassedCorrectEventArgs_DoesNotRaiseException()
		{
			// Arrange
			var e = new EmailEventArgs("bob@test.com", "ajones@burning-glass.com", "ajones2@burning-glass.com", "ajones3@burning-glass.com", "Test", "Test Email", false);
			
			var emailToFile = new EmailToFile(@"C:\Data\Logs");

			// Act
			emailToFile.SendEmail(this, e);

			// Assert

		}

		[Test]
		[Ignore]
		public void SendEmail_WhenPassedMessageInEventArgs_DoesNotRaiseException()
		{
			// Arrange
			var CC1 = new MailAddress("ajones2@burning-glass.com");
			var CC2 = new MailAddress("ajones3@burning-glass.com");
			var BCC1 = new MailAddress("ajones4@burning-glass.com");
			//var attachment1 = new Attachment("attachment1.txt");
			//var attachment2 = new Attachment("attachment2.txt");

			var message = new MailMessage("ajones5@burning-glass.com", "ajones@burning-glass.com", "Message Test", "Message Test");
			message.CC.Add(CC1);
			message.CC.Add(CC2);
			message.Bcc.Add(BCC1);
			//message.Attachments.Add(attachment1);
			//message.Attachments.Add(attachment2);

			var e = new EmailEventArgs(message, true);
			var emailToFile = new EmailToFile(@"C:\Data\Logs");

			// Act
			emailToFile.SendEmail(this, e);

			// Assert

		}
	}
}