﻿using System;
using Focus.UnitTests.Core;
using Framework.Testing.NUnit;
using Framework.Authentication;
using NUnit.Framework;

namespace Focus.UnitTests.Framework.Authentication
{
	[TestFixture]
	class PasswordTests : TestFixtureBase
	{
		[Test]
		public void Password_WhenPassedAnEmptySalt_ThrowsException()
		{
			// Arrange/Act/Assert
			typeof(ArgumentNullException).ShouldBeThrownBy(() =>
			{
				new Password(MockAppSettings, "xxx", "");
			});
		}

		[Test]
		public void Password_WhenPassedAnEmptyPassword_ThrowsException()
		{
			// Arrange/Act/Assert
			typeof(ArgumentNullException).ShouldBeThrownBy(() =>
			{
				new Password(MockAppSettings, "", "xxx");
			});
		}

		[Test]
		public void Password_WhenPassedAnInvalidPassword_ThrowsException()
		{
			// Arrange/Act/Assert
			var ex = typeof (Exception).ShouldBeThrownBy(() => { new Password(MockAppSettings, "Test"); });

			ex.Message.ShouldEqual("Invalid Password");
		}

		[Test]
		public void Password_WhenPassedAnInvalidPasswordWithSalt_ThrowsException()
		{
			// Arrange/Act/Assert
			var ex = typeof(Exception).ShouldBeThrownBy(() =>
			{
				var pw = new Password(MockAppSettings, "Test", Guid.NewGuid().ToString("N").Replace("-", "").Substring(0, 8));
			});

			ex.Message.ShouldEqual("Invalid Password");
		}
	}
}