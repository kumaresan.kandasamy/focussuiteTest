﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Configuration;
using System.Runtime.InteropServices;
using System.Threading;
using System.Transactions;
using Focus.Services.Messages;
using Framework.Core;
using Framework.DataAccess;
using Framework.Messaging;
using Framework.ServiceLocation;
using Framework.Testing.NUnit;
using NUnit.Framework;

#endregion

namespace Focus.UnitTests.Framework.Messaging
{
	[TestFixture]
	[Explicit]
	public class MessagingTests
	{
		private StructureMapServiceLocator _serviceLocator;
		private IMessageStore _messageStore;

		[SetUp]
		public void SetUp()
		{
			// Setup the Service Locator
			_serviceLocator = new StructureMapServiceLocator();
			ServiceLocator.SetServiceLocator(_serviceLocator);

			Console.WriteLine(_serviceLocator.GetType().ToString());

			// Register the Message Store for the message bus
			_messageStore = new MessageStore();
			_serviceLocator.Register(() => _messageStore);

			// Register the message handlers			
			_serviceLocator.Register<IMessageHandler<SimpleMessage>>(() => new FirstMessageHandler());
			_serviceLocator.Register<IMessageHandler<SimpleMessage>>(() => new SecondMessageHandler());

			CleanDownMessageBus();
			SetUpMessageBusConfig();
		}

		[TearDown]
		public void TearDown()
		{
			//CleanDownMessageBus();
			SetUpMessageBusConfig();			
		}

		[Test]
		public void Publish_WithValidMessage_PublishesTheMessage()
		{
			// Arrange
			var messageBus = new MessageBus();

			// Act
			messageBus.Publish(new SimpleMessage("Test Data") { Version = "03.00.0000" });

			// Assert
			MessageCount(MessageStatus.Enqueued).ShouldEqual(1);
			MessageCount(MessageStatus.Dequeued).ShouldEqual(0);
			MessageCount(MessageStatus.Handled).ShouldEqual(0);		
		}

		[Test]
		public void PublishThenProcess_WithValidMessage_PublishesAndHandlesTheMessage()
		{
			// Arrange
			var messageBus = new MessageBus();
			var messageBusHandler = new MessageBusHandler();

			// Act
			messageBus.Publish(new SimpleMessage("Test Data") { Version = "03.00.0000" });
			messageBusHandler.Process(version: "03.00.0000");

			// Assert
			MessageCount(MessageStatus.Enqueued).ShouldEqual(0);
			MessageCount(MessageStatus.Dequeued).ShouldEqual(0);
			MessageCount(MessageStatus.Handled).ShouldEqual(1);
		}

		[Test]
		public void Publish10ThenProcess_WithValidMessage_PublishesAndHandlesTheMessages()
		{
			// Arrange
			var messageBus = new MessageBus();
			var messageBusHandler = new MessageBusHandler();

			// Act
			for (var i = 1; i <= 10; i++)
				messageBus.Publish(new SimpleMessage(String.Format("Version 03.00.0000 Test Data + {0:00}", i)) { Version = "03.00.0000" });

			messageBusHandler.Process(version: "03.00.0000", iterations: 10);

			// Assert
			MessageCount(MessageStatus.Enqueued).ShouldEqual(0);
			MessageCount(MessageStatus.Dequeued).ShouldEqual(0);
			MessageCount(MessageStatus.Handled).ShouldEqual(10);
		}

		[Test]
		public void PublishThenProcessHighPriority_WithValidMessage_PublishesAndHandlesTheMessage()
		{
			// Arrange
			var messageBus = new MessageBus();
			var messageBusHandler = new MessageBusHandler();

			// Act
			messageBus.Publish(new SimpleMessage("Medium Priority Test Data") { Version = "03.00.0000" });
			messageBus.Publish(new SimpleMessage("High Priority Test Data") { Version = "03.00.0000" }, MessagePriority.High);
			messageBusHandler.Process(version: "03.00.0000", iterations: 2);

			// Assert
			MessageCount(MessageStatus.Enqueued).ShouldEqual(0);
			MessageCount(MessageStatus.Dequeued).ShouldEqual(0);
			MessageCount(MessageStatus.Handled, MessagePriority.Medium).ShouldEqual(1);
			MessageCount(MessageStatus.Handled, MessagePriority.High).ShouldEqual(1);
		}

		[Test]
		public void PublishThenProcessCorrectVersion_WithValidMessage_PublishesAndHandlesTheMessage()
		{
			// Arrange
			var messageBus = new MessageBus();
			var messageBusHandler = new MessageBusHandler();

			// Act
			messageBus.Publish(new SimpleMessage("Version 03.00.0000 Test Data") { Version = "03.00.0000" });
			messageBus.Publish(new SimpleMessage("Version 03.01.0000 Test Data") { Version = "03.01.0000" });
			messageBusHandler.Process(version: "03.01.0000", iterations: 2);

			// Assert
			MessageCount(MessageStatus.Enqueued).ShouldEqual(1);
			MessageCount(MessageStatus.Dequeued).ShouldEqual(0);
			MessageCount(MessageStatus.Handled).ShouldEqual(1);			
		}

		[Test]
		public void PublishThenProcessCorrectVersionAndPriority_WithValidMessage_PublishesAndHandlesTheMessage()
		{
			// Arrange
			var messageBus = new MessageBus();
			var messageBusHandler = new MessageBusHandler();

			// Act
			messageBus.Publish(new SimpleMessage("Medium Priority Version 03.00.0000 Test Data") { Version = "03.00.0000" });
			messageBus.Publish(new SimpleMessage("High Priority Version 03.00.0000 Test Data") { Version = "03.00.0000" }, MessagePriority.High);
			messageBus.Publish(new SimpleMessage("Medium Priority Version 03.01.0000 Test Data") { Version = "03.01.0000" });
			messageBus.Publish(new SimpleMessage("High Priority Version 03.01.0000 Test Data") { Version = "03.01.0000" }, MessagePriority.High);
			messageBusHandler.Process(version: "03.01.0000", iterations: 4);

			// Assert
			MessageCount(MessageStatus.Enqueued, MessagePriority.Medium).ShouldEqual(1);
			MessageCount(MessageStatus.Enqueued, MessagePriority.High).ShouldEqual(1);
			MessageCount(MessageStatus.Dequeued).ShouldEqual(0);
			MessageCount(MessageStatus.Handled, MessagePriority.Medium).ShouldEqual(1);
			MessageCount(MessageStatus.Handled, MessagePriority.High).ShouldEqual(1);
		}

		[Test]
		[Explicit]
		public void PublishManyDirectMessage_WithValidMessage_PublishesTheMessageDirectly()
		{
			// Arrange
			Thread.Sleep(10000);

			SetUpMessageBusConfig(directWebApiUrlBaseAddresses: "http://localhost:8080,http://localhost:8080"); //, webApiUrlBaseAddress: "http://localhost:8080");
			//SetUpMessageBusConfig(directWebApiUrlBaseAddresses: "http://bosdemsvr009.usdev.burninglass.com:10001,http://bosdemsvr009.usdev.burninglass.com:10001");

			var messageBus = new MessageBus();

			// Act
			for (var i = 0; i < 10; i++)
			{
				var message = new SendEmailMessage { To = "jim@burning-glass.com", Subject = String.Format("Test #{0:00}", i+1), Body = String.Format("Test Email #{0:00}", i), Version = Focus.Core.Constants.SystemVersion };
				messageBus.Publish(message, MessagePriority.Direct);
			}
			
			Thread.Sleep(10000);
			// Assert
			//MessageCount(MessageStatus.Enqueued).ShouldEqual(0);
			//MessageCount(MessageStatus.Dequeued).ShouldEqual(0);
			//MessageCount(MessageStatus.Handled).ShouldEqual(10);
		}

		[Test]
		[Explicit]
		public void PublishDirectMessage_WithValidMessageDisablingAsyncHandling_PublishesTheMessageDirectlyButWaitsForCompletion()
		{
			// Arrange
			//Thread.Sleep(10000);

			SetUpMessageBusConfig(directWebApiUrlBaseAddresses: "http://devappsvr001:10001,http://devappsvr001:10001");

			var messageBus = new MessageBus();

			// Act
			for (var i = 0; i < 10; i++)
			{
				var message = new SendEmailMessage { To = "jim@burning-glass.com", Subject = String.Format("Test #{0:00}", i+1), Body = String.Format("Test Email #{0:00}", i), Version = Focus.Core.Constants.SystemVersion };
				messageBus.Publish(message, MessagePriority.Direct);
			}

			//Thread.Sleep(10000);
			// Assert
			//MessageCount(MessageStatus.Enqueued).ShouldEqual(0);
			//MessageCount(MessageStatus.Dequeued).ShouldEqual(0);
			//MessageCount(MessageStatus.Handled).ShouldEqual(10);
		}

		[Test]
		[Explicit]
		public void PublishManyMediumMessage_WithValidMessageThatCanBeUpgradedToDirect_PublishesTheMessageDirectly()
		{
			// Arrange
			Thread.Sleep(10000);

			SetUpMessageBusConfig("SendEmailMessage", "http://localhost:8080,http://localhost:8080");

			var messageBus = new MessageBus();

			// Act
			for (var i = 0; i < 10; i++)
			{
				var message = new SendEmailMessage { To = "jim@burning-glass.com", Subject = String.Format("Test #{0:00}", i + 1), Body = String.Format("Test Email #{0:00}", i), Version = Focus.Core.Constants.SystemVersion };
				messageBus.Publish(message, MessagePriority.Medium);
			}
			
			Thread.Sleep(10000);
			// Assert
			//MessageCount(MessageStatus.Enqueued).ShouldEqual(0);
			//MessageCount(MessageStatus.Dequeued).ShouldEqual(0);
			//MessageCount(MessageStatus.Handled).ShouldEqual(10);
		}

		[Test]
		[Explicit]
		public void Publish50MediumMessage_WithValidMessageThatCantBeUpgradedToDirect_PublishesTheMessageDirectly()
		{
			// Arrange
			Thread.Sleep(10000);

			SetUpMessageBusConfig(directWebApiUrlBaseAddresses: "http://localhost:8080,http://localhost:8080");

			var messageBus = new MessageBus();

			// Act
			for (var i = 0; i < 50; i++)
			{
				var message = new SendEmailMessage { To = "jim@burning-glass.com", Subject = String.Format("Test #{0:00}", i), Body = String.Format("Test Email #{0:00}", i), Version = Focus.Core.Constants.SystemVersion };
				messageBus.Publish(message, MessagePriority.Medium);
			}

			Thread.Sleep(10000);
			// Assert
			//MessageCount(MessageStatus.Enqueued).ShouldEqual(0);
			//MessageCount(MessageStatus.Dequeued).ShouldEqual(0);
			//MessageCount(MessageStatus.Handled).ShouldEqual(10);
		}

		//[Test]
		//public void PublishInProcessMessage_WithValidMessageToAFailingHandler_PublishesAndHandlesTheMessageDirectlyAnd()
		//{
		//  // Arrange
		//  _serviceLocator.Register<IMessageHandler<SimpleMessage>>(() => new FailingMessageHandler());
		//  var messageTypeId = GetMessageTypeId(typeof(SimpleMessage));
		//  SetUpMessageBusConfig(new[] { messageTypeId });

		//  var messageBus = new MessageBus();

		//  // Act
		//  messageBus.Publish(new SimpleMessage("Version 03.00.0000 Test Data") { Version = "03.00.0000" });

		//  // Need to add this so the message bus has time to handle the message in its worker thread
		//  Thread.Sleep(60000);

		//  // Assert
		//  MessageCount(MessageStatus.Enqueued).ShouldEqual(0);
		//  MessageCount(MessageStatus.Dequeued).ShouldEqual(0);
		//  MessageCount(MessageStatus.Handled, failed: true).ShouldEqual(1);
		//}

		private static void SetUpMessageBusConfig(string directMessageTypes = null, string directWebApiUrlBaseAddresses = null, int directWebApiTimeout = 120000, bool isMultiThreaded = false, int? maxThreadCount = null, string webApiUrlBaseAddress = null)
		{
			var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

			config.AppSettings.Settings.Remove("Focus-MessageBus-DirectMessageTypes");
			config.AppSettings.Settings.Remove("Focus-MessageBus-DirectWebApiUrlBaseAddresses");
			config.AppSettings.Settings.Remove("Focus-MessageBus-MultiThreaded");
			config.AppSettings.Settings.Remove("Focus-MessageBus-MaxWorkerThreadCount");
			config.AppSettings.Settings.Remove("Focus-MessageBus-ApiTimeOut");
			config.AppSettings.Settings.Remove("Focus-MessageBus-WebApiUrlBaseAddress");
			
			if (directMessageTypes.IsNotNull())
				config.AppSettings.Settings.Add("Focus-MessageBus-DirectMessageTypes", directMessageTypes);

			if (directWebApiUrlBaseAddresses.IsNotNull())
				config.AppSettings.Settings.Add("Focus-MessageBus-DirectWebApiUrlBaseAddresses", directWebApiUrlBaseAddresses);

			config.AppSettings.Settings.Add("Focus-MessageBus-ApiTimeOut", directWebApiTimeout.ToString());

			config.AppSettings.Settings.Add("Focus-MessageBus-MultiThreaded", isMultiThreaded.ToString());

			if (maxThreadCount.HasValue)
				config.AppSettings.Settings.Add("Focus-MessageBus-MaxWorkerThreadCount", maxThreadCount.Value.ToString());

			config.AppSettings.Settings.Add("Focus-MessageBus-WebApiUrlBaseAddress", webApiUrlBaseAddress);

			config.Save(ConfigurationSaveMode.Modified);

			ConfigurationManager.RefreshSection("appSettings");
		}

		private static void CleanDownMessageBus()
		{
			const string sql = @"DELETE FROM [dbo].[Messaging.MessageLog]
DELETE FROM [dbo].[Messaging.Message]
DELETE FROM [dbo].[Messaging.MessagePayload]";

			var connStr = ConfigurationManager.ConnectionStrings["SQLServerMessageBus"].ConnectionString;

			using (var db = new Database(connStr, DatabaseType.MsSql))
				db.Delete(sql);
		}

		private static int MessageCount(MessageStatus messageStatus, MessagePriority? messagePriority = null, bool failed = false)
		{
			var sql = @"SELECT COUNT(1) FROM [dbo].[Messaging.Message] WHERE [Status]=" + (int) messageStatus + " AND [Failed]=" + (failed ? 1 : 0);
			if (messagePriority.HasValue)
				sql += " AND [Priority]=" + (int)messagePriority.Value;

			var connStr = ConfigurationManager.ConnectionStrings["SQLServerMessageBus"].ConnectionString;

			using (var db = new Database(connStr, DatabaseType.MsSql))
				return db.ExecuteCount(sql);
		}

		private static Guid GetMessageTypeId(Type messageType)
		{
			var messageTypeTypeName = messageType.AssemblyQualifiedName;
			var sql = @"SELECT Id FROM [dbo].[Messaging.MessageType] WHERE [TypeName]='" + messageType.AssemblyQualifiedName + "'";

			var connStr = ConfigurationManager.ConnectionStrings["SQLServerMessageBus"].ConnectionString;

			using (var db = new Database(connStr, DatabaseType.MsSql))
				return (Guid) db.ExecuteScalar(sql);
		}
	}

	[Serializable]
	public class SimpleMessage : Message
	{
		public SimpleMessage(string data)
		{
			Data = data;
		}

		public string Data { get; private set;  }
	}

	public class FirstMessageHandler : IMessageHandler<SimpleMessage>
	{	
		public void Handle(SimpleMessage message)
		{
			Console.WriteLine(@"FirstMessageHandler Id:{0}, Data: {1}", message.Id, message.Data);
		}
	}

	public class SecondMessageHandler : IMessageHandler<SimpleMessage>
	{
		public void Handle(SimpleMessage message)
		{
			Console.WriteLine(@"SecondMessageHandler Id:{0}, Data: {1}", message.Id, message.Data);
		}
	}

	public class FailingMessageHandler : IMessageHandler<SimpleMessage>
	{
		public void Handle(SimpleMessage message)
		{
			Console.WriteLine(@"FailingMessageHandler Id:{0}, Data: {1}", message.Id, message.Data);

			throw new Exception("Message Handler has failed!");
		}
	}
}
