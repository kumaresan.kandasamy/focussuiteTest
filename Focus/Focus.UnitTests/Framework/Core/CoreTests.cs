﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using Framework.Testing.NUnit;
using NUnit.Framework;
using Framework.Core;

#endregion



namespace Focus.UnitTests.Framework.Core
{
	[TestFixture]
	public class CoreTests
	{
		[Test]
		public void RemoveNonNumericCharacters_WhenPassedValidString_RemovesNonNumericCharacters()
		{
			// Arrange
			var text = "1234Y5678@9-0:";

			// Act
			var result = text.RemoveNonNumericCharacters();

			// Assert
			result.ShouldEqual("1234567890");
		}
	}
}
