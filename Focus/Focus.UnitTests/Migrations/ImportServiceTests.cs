﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Focus.Common.Models;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Messages.AccountService;
using Focus.Core.Messages.DataImportService;
using Focus.Core.Messages.EmployerService;
using Focus.Core.Messages.JobService;
using Focus.Data.Core.Entities;
using Focus.Data.Migration.Entities;
using Focus.Data.Repositories.Contracts;
using Focus.MigrationServices;
using Focus.MigrationServices.Messages;
using Focus.MigrationServices.Providers.Common;
using Focus.MigrationServices.Providers.Implementations;
using Focus.MigrationServices.ServiceImplementations;
using Focus.Services.ServiceImplementations;
using Focus.UnitTests.Core;
using Mindscape.LightSpeed;
using Mindscape.LightSpeed.Querying;
using Moq;
using NUnit.Framework;
using AcknowledgementType = Focus.Core.Messages.AcknowledgementType;
// ReSharper disable AssignNullToNotNullAttribute

#endregion

namespace Focus.UnitTests.Migrations
{
	[TestFixture]
	[Explicit]
	class ImportServiceTests : TestFixtureBase
	{
		[SetUp]
    public override void SetUp()
    { 
      DoSetup(false);
    }

		#region Employer Tradename Tests

		[Test]
		[ExpectedException(typeof(ArgumentException))]
		public void ImportEmployerTradeNames_InvalidArgumentTests_EmployeeMigrationEmployerRecordTypeThrowsNullArgumentException()
		{
			// arrange
			var service = new ImportService(RuntimeContext);

			// act
			service.ImportEmployerTradeNameRequests(MigrationEmployerRecordType.Employee, new ImportRequest(), new ImportResponse());

			// assert
			Assert.Fail("Employee MigrationEmployerRecordtype throws an argument exception");
		}

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void ImportEmployerTradeNames_NullArgumentTests_NullImportRequestThrowsNullArgumentException()
		{
			// arrange
			var service = new ImportService(RuntimeContext);

			// act
			service.ImportEmployerTradeNameRequests(MigrationEmployerRecordType.TradeName, null, new ImportResponse());

			// assert
			Assert.Fail("Null ImportRequest throws a null argument exception");
		}

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void ImportEmployerTradeNames_NullArgumentTests_NullImportResponseThrowsNullArgumentException()
		{
			// arrange
			var service = new ImportService( RuntimeContext );

			// act
			service.ImportEmployerTradeNameRequests(MigrationEmployerRecordType.TradeName, new ImportRequest(), null);

			// assert
			Assert.Fail("Null ImportResponse throws a null argument exception");
		}

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void ProcessEmployerTradenames_NullArgumentTests_NullEmployerImportRequestThrowsNullArgumentException()
		{
			// arrange
			var dataImportService = new DataImportService( RuntimeContext );

			// act
			dataImportService.ProcessEmployerTradeNames(null);

			// assert
			Assert.Fail("Null EmployerImportRequest throws a null argument exception");
		}

		[Test]
		public void ProcessEmployerTradenames_ImportEmployerTradenameTests_BusinessUnitModelSavedWhenBusinessUnitFoundByEmployeeMigrationId()
		{
			// arrange
			var coreRepositoryMock = new Mock<ICoreRepository>();
			var migrationRepositoryMock = new Mock<IMigrationRepository>();

			var employer = new Employer { Id = 2};
			var employee = new Employee { Id = 1, EmployerId = 2};
			var employerRequest = new EmployerRequest { Id = 2 };

			coreRepositoryMock.Setup(x => x.Employees).Returns(new List<Employee> {employee}.AsQueryable);
			coreRepositoryMock.Setup( x => x.Employers ).Returns( new List<Employer> { employer }.AsQueryable );

			migrationRepositoryMock.Setup(x => x.FindById<EmployerRequest>(It.IsAny<long>())).Returns(employerRequest);
			migrationRepositoryMock.Setup(x => x.SaveChanges(It.IsAny<bool>())).Verifiable();

			((TestRepositories) RuntimeContext.Repositories).Core = coreRepositoryMock.Object;
			((TestRepositories) RuntimeContext.Repositories).Migration = migrationRepositoryMock.Object;

			var dataImportService = new DataImportService(RuntimeContext);
			
			dataImportService.LookupEmployerRequestFunc = x => new EmployerRequest { FocusId = 1, };
			dataImportService.DeserializeRequestFunc = x => new EmployerTradeNamesRequest { EmployeeMigrationId = "3" };
			dataImportService.SaveBusinessUnitModelFunc = x => new SaveBusinessUnitModelResponse { Acknowledgement = AcknowledgementType.Success };

			// act
			var response = dataImportService.ProcessEmployerTradeNames(new EmployerImportRequest());

			// assert
			Assert.AreEqual(AcknowledgementType.Success, response.Acknowledgement);
			migrationRepositoryMock.VerifyAll();
			coreRepositoryMock.VerifyAll();
		}

		[Test]
		public void ProcessEmployerTradenames_ImportEmployerTradenameTests_BusinessUnitModelSavedWhenBusinessUnitFoundByFEIN()
		{
			// arrange
			var coreRepositoryMock = new Mock<ICoreRepository>();
			var migrationRepositoryMock = new Mock<IMigrationRepository>();

			var employer = new Employer { Id = 2, FederalEmployerIdentificationNumber = "11-111111" };
			//var employee = new Employee { Id = 1, EmployerId = 2 };
			var employerRequest = new EmployerRequest { Id = 2 };

			coreRepositoryMock.Setup( x => x.Employers ).Returns( new List<Employer> { employer }.AsQueryable );

			migrationRepositoryMock.Setup(x => x.FindById<EmployerRequest>(It.IsAny<long>())).Returns(employerRequest);
			migrationRepositoryMock.Setup(x => x.SaveChanges(It.IsAny<bool>())).Verifiable();

			((TestRepositories)RuntimeContext.Repositories).Core = coreRepositoryMock.Object;
			((TestRepositories)RuntimeContext.Repositories).Migration = migrationRepositoryMock.Object;

			var dataImportService = new DataImportService( RuntimeContext );
			
			dataImportService.LookupEmployerRequestFunc = x => null;
			dataImportService.DeserializeRequestFunc = x => new EmployerTradeNamesRequest { EmployeeMigrationId = "3", Fein = "11111111" };
			dataImportService.SaveBusinessUnitModelFunc = x => new SaveBusinessUnitModelResponse { Acknowledgement = AcknowledgementType.Success };

			// act
			var response = dataImportService.ProcessEmployerTradeNames(new EmployerImportRequest());

			// assert
			Assert.AreEqual(AcknowledgementType.Success, response.Acknowledgement);
			migrationRepositoryMock.VerifyAll();
			coreRepositoryMock.VerifyAll();
		}

		[Test]
		public void ProcessEmployerTradenames_ImportEmployerTradenameTests_BusinessUnitModelNotSavedWhenBusinessUnitModelResponseUnsuccessful()
		{
			// arrange
			var coreRepositoryMock = new Mock<ICoreRepository>();
			var migrationRepositoryMock = new Mock<IMigrationRepository>();

			var employer = new Employer { Id = 2, FederalEmployerIdentificationNumber = "11-111111" };
			//var employee = new Employee { Id = 1, EmployerId = 2 };
			var employerRequest = new EmployerRequest { Id = 2 };

			coreRepositoryMock.Setup(x => x.Employers).Returns(new List<Employer> { employer }.AsQueryable);

			migrationRepositoryMock.Setup(x => x.FindById<EmployerRequest>(It.IsAny<long>())).Returns(employerRequest);
			migrationRepositoryMock.Setup(x => x.SaveChanges(It.IsAny<bool>())).Verifiable();

			((TestRepositories)RuntimeContext.Repositories).Core = coreRepositoryMock.Object;
			((TestRepositories)RuntimeContext.Repositories).Migration = migrationRepositoryMock.Object;

			var dataImportService = new DataImportService( RuntimeContext );

			dataImportService.LookupEmployerRequestFunc = x => null;
			dataImportService.DeserializeRequestFunc = x => new EmployerTradeNamesRequest { EmployeeMigrationId = "3", Fein = "11111111" };
			dataImportService.SaveBusinessUnitModelFunc = x => new SaveBusinessUnitModelResponse { Acknowledgement = AcknowledgementType.Failure };

			// act
			var response = dataImportService.ProcessEmployerTradeNames(new EmployerImportRequest());

			// assert
			Assert.AreEqual(AcknowledgementType.Failure, response.Acknowledgement);
			migrationRepositoryMock.VerifyAll();
			coreRepositoryMock.VerifyAll();
		}

		[Test]
		public void ProcessEmployerTradenames_ImportEmployerTradenameTests_BusinessUnitModelNotSavedWhenBusinessUnitNotFoundByEmployeeMigrationId()
		{
			// arrange
			var coreRepositoryMock = new Mock<ICoreRepository>();
			var migrationRepositoryMock = new Mock<IMigrationRepository>();

			var employer = new Employer { Id = 3 }; /* Employer not found in employer repository for employee */
			var employee = new Employee { Id = 1, EmployerId = 2 };
			var employerRequest = new EmployerRequest { Id = 2 };

			coreRepositoryMock.Setup(x => x.Employees).Returns(new List<Employee> { employee }.AsQueryable);
			coreRepositoryMock.Setup(x => x.Employers).Returns(new List<Employer> { employer }.AsQueryable);

			migrationRepositoryMock.Setup(x => x.FindById<EmployerRequest>(It.IsAny<long>())).Returns(employerRequest);

			((TestRepositories)RuntimeContext.Repositories).Core = coreRepositoryMock.Object;
			((TestRepositories)RuntimeContext.Repositories).Migration = migrationRepositoryMock.Object;

			var dataImportService = new DataImportService( RuntimeContext );

			dataImportService.LookupEmployerRequestFunc = x => new EmployerRequest { FocusId = 1, };
			dataImportService.DeserializeRequestFunc = x => new EmployerTradeNamesRequest { EmployeeMigrationId = "3" };

			// act
			var response = dataImportService.ProcessEmployerTradeNames(new EmployerImportRequest());

			// assert
			Assert.AreEqual(AcknowledgementType.Failure, response.Acknowledgement);
			migrationRepositoryMock.Verify(x => x.SaveChanges(It.IsAny<bool>()), Times.Never());
			migrationRepositoryMock.VerifyAll();
			coreRepositoryMock.VerifyAll();
		}

		[Test]
		public void ProcessEmployerTradenames_ImportEmployerTradenameTests_BusinessUnitModelNotSavedAndInvalidDataExceptionThrownWhenEmployeeDataIsInvalid()
		{
			// arrange
			var coreRepositoryMock = new Mock<ICoreRepository>();
			var migrationRepositoryMock = new Mock<IMigrationRepository>();

			//var employer = new Employer { Id = 3 }; /* Employer not found in employer repository for employee */
			var employee = new Employee { Id = 1, EmployerId = 0 };
			var employerRequest = new EmployerRequest { Id = 2 };

			coreRepositoryMock.Setup(x => x.Employees).Returns(new List<Employee> { employee }.AsQueryable);

			migrationRepositoryMock.Setup(x => x.FindById<EmployerRequest>(It.IsAny<long>())).Returns(employerRequest);

			((TestRepositories)RuntimeContext.Repositories).Core = coreRepositoryMock.Object;
			((TestRepositories)RuntimeContext.Repositories).Migration = migrationRepositoryMock.Object;

			var dataImportService = new DataImportService( RuntimeContext );

			dataImportService.LookupEmployerRequestFunc = x => new EmployerRequest { FocusId = 1, };
			dataImportService.DeserializeRequestFunc = x => new EmployerTradeNamesRequest { EmployeeMigrationId = "3" };

			// act
			var response = dataImportService.ProcessEmployerTradeNames(new EmployerImportRequest());

			// assert
			Assert.AreEqual(AcknowledgementType.Failure, response.Acknowledgement);
			migrationRepositoryMock.Verify(x => x.SaveChanges(It.IsAny<bool>()), Times.Never());
			migrationRepositoryMock.VerifyAll();
			coreRepositoryMock.VerifyAll();
		}

		[Test]
		public void ProcessEmployerTradenames_ImportEmployerTradenameTests_BusinessUnitModelNotSavedWhenBusinessUnitNotFoundByEmployeeMigrationIdNorFEIN()
		{
			// arrange
			var coreRepositoryMock = new Mock<ICoreRepository>();
			var migrationRepositoryMock = new Mock<IMigrationRepository>();

			var employer = new Employer { Id = 3, FederalEmployerIdentificationNumber = "99-999999" };
			//var employee = new Employee { Id = 1, EmployerId = 2 };
			var employerRequest = new EmployerRequest { Id = 2 };

			coreRepositoryMock.Setup( x => x.Employers ).Returns( new List<Employer> { employer }.AsQueryable );

			migrationRepositoryMock.Setup(x => x.FindById<EmployerRequest>(It.IsAny<long>())).Returns(employerRequest);

			((TestRepositories)RuntimeContext.Repositories).Core = coreRepositoryMock.Object;
			((TestRepositories)RuntimeContext.Repositories).Migration = migrationRepositoryMock.Object;

			var dataImportService = new DataImportService( RuntimeContext );

			dataImportService.LookupEmployerRequestFunc = x => null;
			dataImportService.DeserializeRequestFunc = x => new EmployerTradeNamesRequest { EmployeeMigrationId = "3", Fein = "11111111" };

			// act
			var response = dataImportService.ProcessEmployerTradeNames(new EmployerImportRequest());

			// assert
			Assert.AreEqual(AcknowledgementType.Failure, response.Acknowledgement);
			migrationRepositoryMock.Verify(x => x.SaveChanges(It.IsAny<bool>()), Times.Never());
			migrationRepositoryMock.VerifyAll();
			coreRepositoryMock.VerifyAll();

		}

		[Test]
		public void ProcessEmployerTradenames_ImportEmployerTradenameDeltaTests_BusinessUnitUpdatedWhenIsUpdateSetToTrue()
		{
			// arrange
			var coreRepositoryMock = new Mock<ICoreRepository>();
			var migrationRepositoryMock = new Mock<IMigrationRepository>();

			var employer = new Employer { Id = 2, FederalEmployerIdentificationNumber = "11-111111" };
			//var employee = new Employee { Id = 1, EmployerId = 2 };
			var employerRequest = new EmployerRequest { Id = 2, IsUpdate = true};

			coreRepositoryMock.Setup( x => x.Employers ).Returns( new List<Employer> { employer }.AsQueryable );

			migrationRepositoryMock.Setup(x => x.FindById<EmployerRequest>(It.IsAny<long>())).Returns(employerRequest);

			((TestRepositories)RuntimeContext.Repositories).Core = coreRepositoryMock.Object;
			((TestRepositories)RuntimeContext.Repositories).Migration = migrationRepositoryMock.Object;

			var dataImportService = new DataImportService( RuntimeContext );
			dataImportService.UpdateBusinessUnitFunc = (x, y) => true;
			dataImportService.DeserializeRequestFunc = x => new EmployerTradeNamesRequest { EmployeeMigrationId = "3", Fein = "11111111" };

			// act
			var response = dataImportService.ProcessEmployerTradeNames(new EmployerImportRequest());

			// assert
			Assert.AreEqual(AcknowledgementType.Success, response.Acknowledgement);
			migrationRepositoryMock.Verify(x => x.SaveChanges(It.IsAny<bool>()), Times.Once());
			migrationRepositoryMock.VerifyAll();
			coreRepositoryMock.VerifyAll();

		}

		[Test]
		public void ProcessEmployerTradenames_ImportEmployerTradenameDeltaTests_BusinessUnitUpdatedWhenIsUpdateSetToFalse()
		{
			// arrange
			var coreRepositoryMock = new Mock<ICoreRepository>();
			var migrationRepositoryMock = new Mock<IMigrationRepository>();

			var employer = new Employer { Id = 2, FederalEmployerIdentificationNumber = "11-111111" };
			//var employee = new Employee {Id = 1, EmployerId = 2};
			var employerRequest = new EmployerRequest {Id = 2, IsUpdate = true};

			coreRepositoryMock.Setup( x => x.Employers ).Returns( new List<Employer> { employer }.AsQueryable );

			migrationRepositoryMock.Setup(x => x.FindById<EmployerRequest>(It.IsAny<long>()))
				.Returns(employerRequest);

			((TestRepositories) RuntimeContext.Repositories).Core = coreRepositoryMock.Object;
			((TestRepositories) RuntimeContext.Repositories).Migration = migrationRepositoryMock.Object;

			var dataImportService = new DataImportService( RuntimeContext );
			dataImportService.UpdateBusinessUnitFunc = (x, y) => false;
			dataImportService.DeserializeRequestFunc =
				x => new EmployerTradeNamesRequest {EmployeeMigrationId = "3", Fein = "11111111"};

			// act
			var response = dataImportService.ProcessEmployerTradeNames(new EmployerImportRequest());

			// assert
			Assert.AreEqual(AcknowledgementType.Failure, response.Acknowledgement);
			migrationRepositoryMock.Verify(x => x.SaveChanges(It.IsAny<bool>()), Times.Once());
			migrationRepositoryMock.VerifyAll();
			coreRepositoryMock.VerifyAll();
		}
		
		[Test]
		public void ProcessEmployerTradenames_ImportEmployerTradenameDeltaTests_BusinessUnitNotUpdatedWhenIsUpdateSetToTrueButExceptionThrown()
		{
			// arrange
			var coreRepositoryMock = new Mock<ICoreRepository>();
			var migrationRepositoryMock = new Mock<IMigrationRepository>();

			var employer = new Employer { Id = 2, FederalEmployerIdentificationNumber = "11-111111" };
			//var employee = new Employee { Id = 1, EmployerId = 2 };
			var employerRequest = new EmployerRequest { Id = 2, IsUpdate = true };

			coreRepositoryMock.Setup( x => x.Employers ).Returns( new List<Employer> { employer }.AsQueryable );

			migrationRepositoryMock.Setup(x => x.FindById<EmployerRequest>(It.IsAny<long>())).Returns(employerRequest);

			((TestRepositories)RuntimeContext.Repositories).Core = coreRepositoryMock.Object;
			((TestRepositories)RuntimeContext.Repositories).Migration = migrationRepositoryMock.Object;

			var dataImportService = new DataImportService( RuntimeContext );
			dataImportService.UpdateBusinessUnitFunc = (x, y) =>
			{
				throw new Exception("Simulated exception updating business unit");
			};
			dataImportService.DeserializeRequestFunc = x => new EmployerTradeNamesRequest { EmployeeMigrationId = "3", Fein = "11111111" };

			// act
			var response = dataImportService.ProcessEmployerTradeNames(new EmployerImportRequest());

			// assert
			Assert.AreEqual(AcknowledgementType.Failure, response.Acknowledgement);
			migrationRepositoryMock.Verify(x => x.SaveChanges(It.IsAny<bool>()), Times.Never());
			migrationRepositoryMock.VerifyAll();
			coreRepositoryMock.VerifyAll();

		}

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void UpdateBusinessUnit_ImportEmployerTradenameDeltaTests_NullDescriptionRequestThrowsNullArgumentException()
		{
			// arrange
			var dataImportService = new DataImportService( RuntimeContext );
			EmployerTradeNamesRequest descriptionRequest = null;
			var employer = new Employer();

			// act
			dataImportService.UpdateBusinessUnit(employer, descriptionRequest);

			// assert
			Assert.Fail("Null Employer throws a ArgumentNullException");

		}

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void UpdateBusinessUnit_ImportEmployerTradenameDeltaTests_NullEmployerThrowsNullArgumentException()
		{
			// arrange
			var dataImportService = new DataImportService( RuntimeContext );
			var descriptionRequest = new EmployerTradeNamesRequest();
			Employer employer = null;

			// act
			dataImportService.UpdateBusinessUnit(employer, descriptionRequest);

			// assert
			Assert.Fail("Null EmployerTradeNameRequest throws a ArgumentNullException");

		}

		[Test]
		public void UpdateBusinessUnit_ImportEmployerTradenameDeltaTests_UpdateBusinessUnitSuccessfullySavesUpdatedChanges()
		{
			// arrange
			var dataImportService = new DataImportService( RuntimeContext );
			var descriptionRequest = new EmployerTradeNamesRequest
			{
				TradeName = "Test Tradename",
				Line1 = "Address Line 1",
				Line2 = "Address Line2",
				PostalCode = "Zip code",
				City = "TownCity",
			};
			var employer = new Employer
			{
				Id = 1,
				AccountTypeId = null,
			  AlternatePhone1 = "555123456",
			  AlternatePhone1Type = "0",
			  AlternatePhone2 = "555123456",
			  AlternatePhone2Type = "1",
			  IndustrialClassification = null,
			  PrimaryPhoneType = "0",
			  PrimaryPhone = "555123456",
			  PrimaryPhoneExtension = "",
			};

			var coreRepositoryMock = new Mock<ICoreRepository>();
			var migrationRepositoryMock = new Mock<IMigrationRepository>();

			coreRepositoryMock.Setup(x => x.BusinessUnits).Returns(new List<BusinessUnit> { new BusinessUnit { Id = 1, EmployerId = 1, Name = "Test Tradename" } }.AsQueryable);
			coreRepositoryMock.Setup(x => x.SaveChanges(It.IsAny<bool>())).Verifiable();
			coreRepositoryMock.Setup(x => x.BusinessUnitAddresses).Returns(new List<BusinessUnitAddress> { new BusinessUnitAddress { Id = 2, BusinessUnitId = 1 }}.AsQueryable);

			((TestRepositories)RuntimeContext.Repositories).Core = coreRepositoryMock.Object;
			((TestRepositories)RuntimeContext.Repositories).Migration = migrationRepositoryMock.Object;
			
			// act
			var result = dataImportService.UpdateBusinessUnit(employer, descriptionRequest);

			// assert

			coreRepositoryMock.VerifyAll();
			Assert.IsTrue(result);
		}

		#endregion

		#region Job Order Tests

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void ProcessJobOrder_ImportJobOrderTests_NullJobOrderImportRequestThrowsArgumentNullException()
		{
			// arrange
			var dataImportService = new DataImportService( RuntimeContext );
			JobOrderImportRequest request = null;

			// act
			dataImportService.ProcessJobOrder(request);

			// assert
			Assert.Fail("Null JobOrderImportRequest throws Argument Null Exception");
		}

		[Test]
		public void ProcessJobOrder_ImportJobOrderTests_MigrationRequestAcknowledgementFailureReturnsResponseSuccessToAllowIterationToNextRecordWhenEmployeeRecordNotFound()
		{
			// arrange
			var dataImportService = new DataImportService( RuntimeContext );
			var jobOrderImportRequest = new JobOrderImportRequest { JobOrderRequestId = 1 };
			var jobOrderRequest = new JobOrderRequest { Id = 1, IsUpdate = false };
			var employee = new Employee { Id = 3 };

			var coreRepositoryMock = new Mock<ICoreRepository>();
			var migrationRepositoryMock = new Mock<IMigrationRepository>();

			coreRepositoryMock.Setup(x => x.Employees).Returns(new List<Employee> { employee }.AsQueryable);
			//coreRepositoryMock.Setup(x => x.Employers).Returns(new List<Employer> { employer }.AsQueryable);

			migrationRepositoryMock.Setup(x => x.FindById<JobOrderRequest>(It.IsAny<long>())).Returns(jobOrderRequest);

			((TestRepositories)RuntimeContext.Repositories).Core = coreRepositoryMock.Object;
			((TestRepositories)RuntimeContext.Repositories).Migration = migrationRepositoryMock.Object;

			//dataImportService.GetJobOrderRequestByExternalIdFunc = x => new Focus.Data.Migration.Entities.EmployerRequest() { FocusId = 1, };
			dataImportService.DeserializeSaveJobRequestFunc = x => new SaveJobRequest { JobMigrationId = "4", Module = FocusModules.General };
			dataImportService.GetEmployerRequestByEmployeeMigrationIdFunc = x => new EmployerRequest { Id = 1, FocusId = 2 };

			// act
			var response = dataImportService.ProcessJobOrder(jobOrderImportRequest);

			// assert
			Assert.AreEqual(AcknowledgementType.Success, response.Acknowledgement);
			migrationRepositoryMock.VerifyAll();
			coreRepositoryMock.VerifyAll();
		}

		[Test]
		public void ProcessJobOrder_ImportJobOrderTests_MigrationRequestAcknowledgementFailureWhenSavingJobReturnsResponseSuccessToAllowIterationToNextRecord()
		{
			// arrange
			var dataImportService = new DataImportService(RuntimeContext);
			var jobOrderImportRequest = new JobOrderImportRequest { JobOrderRequestId = 1 };
			var jobOrderRequest = new JobOrderRequest { Id = 1, IsUpdate = false };
			var employee = new Employee { Id = 2, EmployerId = 3 };

			var coreRepositoryMock = new Mock<ICoreRepository>();
			var migrationRepositoryMock = new Mock<IMigrationRepository>();

			coreRepositoryMock.Setup(x => x.Employees).Returns(new List<Employee> { employee }.AsQueryable);

			migrationRepositoryMock.Setup(x => x.FindById<JobOrderRequest>(It.IsAny<long>())).Returns(jobOrderRequest);
			
			((TestRepositories) RuntimeContext.Repositories).Core = coreRepositoryMock.Object;
			((TestRepositories) RuntimeContext.Repositories).Migration = migrationRepositoryMock.Object;

			dataImportService.DeserializeSaveJobRequestFunc = x => new SaveJobRequest { JobMigrationId = "4", Module = FocusModules.General, Job = new JobDto { BusinessUnitId = 6, BusinessUnitDescriptionId = 7 } };
			dataImportService.GetEmployerRequestByEmployeeMigrationIdFunc = x => new EmployerRequest { Id = 1, FocusId = 2 };
			dataImportService.SaveJobFunc = x => new SaveJobResponse { Acknowledgement = AcknowledgementType.Failure };
			dataImportService.PopulateSaveJobRequestFunc = (x, y) => { };
			
			// act
			var response = dataImportService.ProcessJobOrder(jobOrderImportRequest);

			// assert
			Assert.AreEqual(AcknowledgementType.Success, response.Acknowledgement);
			migrationRepositoryMock.VerifyAll();
			coreRepositoryMock.VerifyAll();
		}

		[Test]
		public void ProcessJobOrder_ImportJobOrderTests_JobOrderIsProcessedAndPostingGeneratedForSuccessfulSaveJobResponse()
		{
			// arrange
			var dataImportService = new DataImportService( RuntimeContext );
			var jobOrderImportRequest = new JobOrderImportRequest { JobOrderRequestId = 1 };
			var jobOrderRequest = new JobOrderRequest { Id = 1, IsUpdate = false };
			var employee = new Employee { Id = 2, EmployerId = 3 };

			var coreRepositoryMock = new Mock<ICoreRepository>();
			var migrationRepositoryMock = new Mock<IMigrationRepository>();

			coreRepositoryMock.Setup(x => x.Employees).Returns(new List<Employee> { employee }.AsQueryable);

			migrationRepositoryMock.Setup(x => x.FindById<JobOrderRequest>(It.IsAny<long>())).Returns(jobOrderRequest);

			((TestRepositories)RuntimeContext.Repositories).Core = coreRepositoryMock.Object;
			((TestRepositories)RuntimeContext.Repositories).Migration = migrationRepositoryMock.Object;

			dataImportService.DeserializeSaveJobRequestFunc = x => new SaveJobRequest { JobMigrationId = "4", Module = FocusModules.General, Job = new JobDto { BusinessUnitId = 6, BusinessUnitDescriptionId = 7 } };
			dataImportService.GetEmployerRequestByEmployeeMigrationIdFunc = x => new EmployerRequest { Id = 1, FocusId = 2 };
			dataImportService.SaveJobFunc = x => new SaveJobResponse { Acknowledgement = AcknowledgementType.Success };
			dataImportService.PopulateSaveJobRequestFunc = (x, y) => { };

			dataImportService.ProcessJobAddressAction = x => { };
			dataImportService.ProcessJobCertificateAction = x => { };
			dataImportService.ProcessJobLanguageAction = x => { };
			dataImportService.ProcessJobLicenceAction = x => { };
			dataImportService.ProcessJobLocationAction = x => { };
			dataImportService.ProcessJobProgramOfStudyAction = x => { };
			dataImportService.ProcessJobSpecialRequirementAction = x => { };
			bool generateJobPostingWasCalled = false;
			var result = dataImportService.GenerateJobPostingFunc = x =>
			{
				generateJobPostingWasCalled = true;
				return null;
			};

			dataImportService.LookupJobOrderRequestIdsByParentFunc = (x, y) => new List<long>();

			// act
			var response = dataImportService.ProcessJobOrder(jobOrderImportRequest);

			// assert
			Assert.AreEqual(AcknowledgementType.Success, response.Acknowledgement);
			Assert.IsTrue(generateJobPostingWasCalled);
			migrationRepositoryMock.VerifyAll();
			coreRepositoryMock.VerifyAll();
			Assert.IsNotNull( result );
		}

		[Test]
		public void ProcessJobOrder_ImportJobOrderTests_JobOrderIsProcessedAndPostingGeneratedForSuccessfulSaveJobResponseWithMinimumDataPopulation()
		{
			// arrange
			var dataImportService = new DataImportService( RuntimeContext );
			var jobOrderImportRequest = new JobOrderImportRequest { JobOrderRequestId = 1 };
			var jobOrderRequest = new JobOrderRequest { Id = 1, IsUpdate = false };
			var employee = new Employee { Id = 2, EmployerId = 3 };

			var coreRepositoryMock = new Mock<ICoreRepository>();
			var migrationRepositoryMock = new Mock<IMigrationRepository>();

			coreRepositoryMock.Setup(x => x.Employees).Returns(new List<Employee> { employee }.AsQueryable);

			migrationRepositoryMock.Setup(x => x.FindById<JobOrderRequest>(It.IsAny<long>())).Returns(jobOrderRequest);

			((TestRepositories)RuntimeContext.Repositories).Core = coreRepositoryMock.Object;
			((TestRepositories)RuntimeContext.Repositories).Migration = migrationRepositoryMock.Object;

			dataImportService.DeserializeSaveJobRequestFunc = x => new SaveJobRequest { JobMigrationId = "4", Module = FocusModules.General, Job = new JobDto { BusinessUnitId = 6, BusinessUnitDescriptionId = 7 } };
			dataImportService.GetEmployerRequestByEmployeeMigrationIdFunc = x => new EmployerRequest { Id = 1, FocusId = 2 };
			dataImportService.SaveJobFunc = x => new SaveJobResponse { Acknowledgement = AcknowledgementType.Success };
			dataImportService.PopulateSaveJobRequestFunc = (x, y) => { };

			dataImportService.ProcessJobAddressAction = x => { };
			dataImportService.ProcessJobCertificateAction = x => { };
			dataImportService.ProcessJobLanguageAction = x => { };
			dataImportService.ProcessJobLicenceAction = x => { };
			dataImportService.ProcessJobLocationAction = x => { };
			dataImportService.ProcessJobProgramOfStudyAction = x => { };
			dataImportService.ProcessJobSpecialRequirementAction = x => { };
			bool generateJobPostingWasCalled = false;
			var result = dataImportService.GenerateJobPostingFunc = x =>
			{
				generateJobPostingWasCalled = true;
				return null;
			};

			dataImportService.LookupJobOrderRequestIdsByParentFunc = (x, y) => new List<long>();

			// act
			var response = dataImportService.ProcessJobOrder(jobOrderImportRequest);

			// assert
			Assert.AreEqual(AcknowledgementType.Success, response.Acknowledgement);
			Assert.IsTrue(generateJobPostingWasCalled);
			migrationRepositoryMock.VerifyAll();
			coreRepositoryMock.VerifyAll();
			Assert.IsNotNull( result );
		}

		[Test]
		public void ProcessJobOrder_ImportJobOrderTests_JobOrderIsProcessedAndPostingGeneratedForSuccessfulSaveJobResponseWithMaximumDataPopulation()
		{
			// arrange
			var dataImportService = new DataImportService( RuntimeContext );
			var jobOrderImportRequest = new JobOrderImportRequest { JobOrderRequestId = 1 };
			var jobOrderRequest = new JobOrderRequest { Id = 1, IsUpdate = false };
			var employee = new Employee { Id = 2, EmployerId = 3 };

			var coreRepositoryMock = new Mock<ICoreRepository>();
			var migrationRepositoryMock = new Mock<IMigrationRepository>();

			coreRepositoryMock.Setup(x => x.Employees).Returns(new List<Employee> { employee }.AsQueryable);

			migrationRepositoryMock.Setup(x => x.FindById<JobOrderRequest>(It.IsAny<long>())).Returns(jobOrderRequest);

			((TestRepositories)RuntimeContext.Repositories).Core = coreRepositoryMock.Object;
			((TestRepositories)RuntimeContext.Repositories).Migration = migrationRepositoryMock.Object;

			dataImportService.DeserializeSaveJobRequestFunc = x => new SaveJobRequest { JobMigrationId = "4", Module = FocusModules.General, Job = new JobDto { BusinessUnitId = 6, BusinessUnitDescriptionId = 7 } };
			dataImportService.GetEmployerRequestByEmployeeMigrationIdFunc = x => new EmployerRequest { Id = 1, FocusId = 2 };
			dataImportService.SaveJobFunc = x => new SaveJobResponse { Acknowledgement = AcknowledgementType.Success };
			dataImportService.PopulateSaveJobRequestFunc = (x, y) => { };
			
			bool generateJobPostingWasCalled = false;
			bool processJobAddressActionWasCalled = false;
			bool processJobCertificationWasCalled = false;
			bool processJobLanguageActionWasCalled = false;
			bool processJobLicenceActionWasCalled = false;
			bool processJobLocationActionWasCalled = false;
			bool processJobProgramOfStudyActionWasCalled = false;
			bool processJobSpecialRequirementActionWasCalled = false;

			dataImportService.ProcessJobAddressAction = x => { processJobAddressActionWasCalled = true; };
			dataImportService.ProcessJobCertificateAction = x => { processJobCertificationWasCalled = true; };
			dataImportService.ProcessJobLanguageAction = x => { processJobLanguageActionWasCalled = true; };
			dataImportService.ProcessJobLicenceAction = x => { processJobLicenceActionWasCalled = true; };
			dataImportService.ProcessJobLocationAction = x => { processJobLocationActionWasCalled = true; };
			dataImportService.ProcessJobProgramOfStudyAction = x => { processJobProgramOfStudyActionWasCalled = true; };
			dataImportService.ProcessJobSpecialRequirementAction = x => { processJobSpecialRequirementActionWasCalled = true; };

			dataImportService.LookupJobOrderRequestIdsByParentFunc = (x, y) => new List<long>();

			var result = dataImportService.GenerateJobPostingFunc = x =>
			{
				generateJobPostingWasCalled = true;
				return null;
			};

			dataImportService.LookupJobOrderRequestIdsByParentFunc = (x, y) => new List<long> {1,2,3};

			// act
			var response = dataImportService.ProcessJobOrder(jobOrderImportRequest);

			// assert
			Assert.AreEqual(AcknowledgementType.Success, response.Acknowledgement);
			Assert.IsTrue(generateJobPostingWasCalled);
			Assert.IsTrue(processJobAddressActionWasCalled);
			Assert.IsTrue(processJobCertificationWasCalled);
			Assert.IsTrue(processJobLanguageActionWasCalled);
			Assert.IsTrue(processJobLicenceActionWasCalled);
			Assert.IsTrue(processJobLocationActionWasCalled);
			Assert.IsTrue(processJobProgramOfStudyActionWasCalled);
			Assert.IsTrue(processJobSpecialRequirementActionWasCalled);

			migrationRepositoryMock.VerifyAll();
			coreRepositoryMock.VerifyAll();
			Assert.IsNotNull( result );
		}

		[Test]
		public void ProcessJobOrder_ImportAsUpdateJobOrderTests_JobOrderIsProcessedAsUpdateAndPostingGeneratedForSuccessfulSaveJobResponseWithMaximumDataPopulation()
		{
			// arrange
			var dataImportService = new DataImportService( RuntimeContext );
			var jobOrderImportRequest = new JobOrderImportRequest { JobOrderRequestId = 1 };
			var jobOrderRequest = new JobOrderRequest { Id = 1, IsUpdate = true };
			var employee = new Employee { Id = 2, EmployerId = 3 };

			var coreRepositoryMock = new Mock<ICoreRepository>();
			var migrationRepositoryMock = new Mock<IMigrationRepository>();

			coreRepositoryMock.Setup(x => x.Employees).Returns(new List<Employee> { employee }.AsQueryable);

			migrationRepositoryMock.Setup(x => x.FindById<JobOrderRequest>(It.IsAny<long>())).Returns(jobOrderRequest);

			((TestRepositories)RuntimeContext.Repositories).Core = coreRepositoryMock.Object;
			((TestRepositories)RuntimeContext.Repositories).Migration = migrationRepositoryMock.Object;

			dataImportService.DeserializeSaveJobRequestFunc = x => new SaveJobRequest { JobMigrationId = "4", Module = FocusModules.General, Job = new JobDto { BusinessUnitId = 6, BusinessUnitDescriptionId = 7 } };
			dataImportService.GetEmployerRequestByEmployeeMigrationIdFunc = x => new EmployerRequest { Id = 1, FocusId = 2 };
			dataImportService.SaveJobFunc = x => new SaveJobResponse { Acknowledgement = AcknowledgementType.Success };
			dataImportService.PopulateSaveJobRequestFunc = (x, y) => { };

			bool generateJobPostingWasCalled = false;
			bool processJobAddressActionWasCalled = false;
			bool processJobCertificationWasCalled = false;
			bool processJobLanguageActionWasCalled = false;
			bool processJobLicenceActionWasCalled = false;
			bool processJobLocationActionWasCalled = false;
			bool processJobProgramOfStudyActionWasCalled = false;
			bool processJobSpecialRequirementActionWasCalled = false;
			bool getJobOrderRequestByExternalIdWasCalled = false;

			dataImportService.ProcessJobAddressAction = x => { processJobAddressActionWasCalled = true; };
			dataImportService.ProcessJobCertificateAction = x => { processJobCertificationWasCalled = true; };
			dataImportService.ProcessJobLanguageAction = x => { processJobLanguageActionWasCalled = true; };
			dataImportService.ProcessJobLicenceAction = x => { processJobLicenceActionWasCalled = true; };
			dataImportService.ProcessJobLocationAction = x => { processJobLocationActionWasCalled = true; };
			dataImportService.ProcessJobProgramOfStudyAction = x => { processJobProgramOfStudyActionWasCalled = true; };
			dataImportService.ProcessJobSpecialRequirementAction = x => { processJobSpecialRequirementActionWasCalled = true; };

			dataImportService.LookupJobOrderRequestIdsByParentFunc = (x, y) => new List<long>();
			dataImportService.GetJobOrderRequestByExternalIdFunc = x =>
			{
				getJobOrderRequestByExternalIdWasCalled = true;
				return new JobOrderRequest {FocusId = 1};
			};

			var result = dataImportService.GenerateJobPostingFunc = x =>
			{
				generateJobPostingWasCalled = true;
				return null;
			};

			dataImportService.LookupJobOrderRequestIdsByParentFunc = (x, y) => new List<long> { 1, 2, 3 };

			// act
			var response = dataImportService.ProcessJobOrder(jobOrderImportRequest);

			// assert
			Assert.AreEqual(AcknowledgementType.Success, response.Acknowledgement);
			Assert.IsTrue(generateJobPostingWasCalled);
			Assert.IsTrue(processJobAddressActionWasCalled);
			Assert.IsTrue(processJobCertificationWasCalled);
			Assert.IsTrue(processJobLanguageActionWasCalled);
			Assert.IsTrue(processJobLicenceActionWasCalled);
			Assert.IsTrue(processJobLocationActionWasCalled);
			Assert.IsTrue(processJobProgramOfStudyActionWasCalled);
			Assert.IsTrue(processJobSpecialRequirementActionWasCalled);
			Assert.IsTrue(getJobOrderRequestByExternalIdWasCalled);

			migrationRepositoryMock.VerifyAll();
			coreRepositoryMock.VerifyAll();
			Assert.IsNotNull( result );
		}

		[Test]
		[Ignore]
		public void ProcessJobOrder_ImportAsUpdateJobOrderTests_JobOrderIsProcessedAsUpdateForOnHoldJobs()
		{
			// arrange
			var dataImportService = new DataImportService( RuntimeContext );
			var jobOrderImportRequest = new JobOrderImportRequest { JobOrderRequestId = 1 };
			var jobOrderRequest = new JobOrderRequest { Id = 1, IsUpdate = true };
			var employee = new Employee { Id = 2, EmployerId = 3 };

			var coreRepositoryMock = new Mock<ICoreRepository>();
			var migrationRepositoryMock = new Mock<IMigrationRepository>();

			coreRepositoryMock.Setup(x => x.Employees).Returns(new List<Employee> { employee }.AsQueryable);

			migrationRepositoryMock.Setup(x => x.FindById<JobOrderRequest>(It.IsAny<long>())).Returns(jobOrderRequest);

			((TestRepositories)RuntimeContext.Repositories).Core = coreRepositoryMock.Object;
			((TestRepositories)RuntimeContext.Repositories).Migration = migrationRepositoryMock.Object;

			dataImportService.DeserializeSaveJobRequestFunc = x => new SaveJobRequest { JobMigrationId = "4", Module = FocusModules.General, Job = new JobDto { BusinessUnitId = 6, BusinessUnitDescriptionId = 7 } };
			dataImportService.GetEmployerRequestByEmployeeMigrationIdFunc = x => new EmployerRequest { Id = 1, FocusId = 2 };
			dataImportService.SaveJobFunc = x => new SaveJobResponse { Acknowledgement = AcknowledgementType.Success };
			dataImportService.PopulateSaveJobRequestFunc = (x, y) => { };

			bool generateJobPostingWasCalled = false;
			bool processJobAddressActionWasCalled = false;
			bool processJobCertificationWasCalled = false;
			bool processJobLanguageActionWasCalled = false;
			bool processJobLicenceActionWasCalled = false;
			bool processJobLocationActionWasCalled = false;
			bool processJobProgramOfStudyActionWasCalled = false;
			bool processJobSpecialRequirementActionWasCalled = false;
			bool getJobOrderRequestByExternalIdWasCalled = false;

			dataImportService.ProcessJobAddressAction = x => { processJobAddressActionWasCalled = true; };
			dataImportService.ProcessJobCertificateAction = x => { processJobCertificationWasCalled = true; };
			dataImportService.ProcessJobLanguageAction = x => { processJobLanguageActionWasCalled = true; };
			dataImportService.ProcessJobLicenceAction = x => { processJobLicenceActionWasCalled = true; };
			dataImportService.ProcessJobLocationAction = x => { processJobLocationActionWasCalled = true; };
			dataImportService.ProcessJobProgramOfStudyAction = x => { processJobProgramOfStudyActionWasCalled = true; };
			dataImportService.ProcessJobSpecialRequirementAction = x => { processJobSpecialRequirementActionWasCalled = true; };

			dataImportService.LookupJobOrderRequestIdsByParentFunc = (x, y) => new List<long>();
			dataImportService.GetJobOrderRequestByExternalIdFunc = x =>
			{
				getJobOrderRequestByExternalIdWasCalled = true;
				return new JobOrderRequest { FocusId = 1 };
			};

			var result = dataImportService.GenerateJobPostingFunc = x =>
			{
				generateJobPostingWasCalled = true;
				return null;
			};

			dataImportService.LookupJobOrderRequestIdsByParentFunc = (x, y) => new List<long> { 1, 2, 3 };

			// act
			var response = dataImportService.ProcessJobOrder(jobOrderImportRequest);

			// assert
			Assert.AreEqual(AcknowledgementType.Success, response.Acknowledgement);
			Assert.IsTrue(generateJobPostingWasCalled);
			Assert.IsTrue(processJobAddressActionWasCalled);
			Assert.IsTrue(processJobCertificationWasCalled);
			Assert.IsTrue(processJobLanguageActionWasCalled);
			Assert.IsTrue(processJobLicenceActionWasCalled);
			Assert.IsTrue(processJobLocationActionWasCalled);
			Assert.IsTrue(processJobProgramOfStudyActionWasCalled);
			Assert.IsTrue(processJobSpecialRequirementActionWasCalled);
			Assert.IsTrue(getJobOrderRequestByExternalIdWasCalled);

			migrationRepositoryMock.VerifyAll();
			coreRepositoryMock.VerifyAll();
			Assert.IsNotNull( result );
		}

		[Test]
		public void ProcessJobOrder_ImportAsUpdateJobOrderTests_JobOrderIsProcessedAsUpdateAndPostingNOTGeneratedAndServiceNOTcalled()
		{
			// arrange
			var dataImportService = new DataImportService(RuntimeContext);
			var jobOrderImportRequest = new JobOrderImportRequest { JobOrderRequestId = 1 };
			var jobOrderRequest = new JobOrderRequest { Id = 1, IsUpdate = true };
			//Employee employee = new Employee { Id = 2, EmployerId = 3 };

			var coreRepositoryMock = new Mock<ICoreRepository>();
			var migrationRepositoryMock = new Mock<IMigrationRepository>();
			
			migrationRepositoryMock.Setup(x => x.FindById<JobOrderRequest>(It.IsAny<long>())).Returns(jobOrderRequest);

			((TestRepositories)RuntimeContext.Repositories).Core = coreRepositoryMock.Object;
			((TestRepositories)RuntimeContext.Repositories).Migration = migrationRepositoryMock.Object;

			dataImportService.DeserializeSaveJobRequestFunc = x => new SaveJobRequest { JobMigrationId = "4", Module = FocusModules.General, Job = new JobDto { BusinessUnitId = 6, BusinessUnitDescriptionId = 7 } };
			dataImportService.GetEmployerRequestByEmployeeMigrationIdFunc = x => new EmployerRequest { Id = 1, FocusId = 2 };
			dataImportService.SaveJobFunc = x => new SaveJobResponse { Acknowledgement = AcknowledgementType.Success };
			dataImportService.PopulateSaveJobRequestFunc = (x, y) => { };

			bool generateJobPostingWasCalled = false;
			bool processJobAddressActionWasCalled = false;
			bool processJobCertificationWasCalled = false;
			bool processJobLanguageActionWasCalled = false;
			bool processJobLicenceActionWasCalled = false;
			bool processJobLocationActionWasCalled = false;
			bool processJobProgramOfStudyActionWasCalled = false;
			bool processJobSpecialRequirementActionWasCalled = false;
			bool getJobOrderRequestByExternalIdWasCalled = false;

			dataImportService.ProcessJobAddressAction = x => { processJobAddressActionWasCalled = true; };
			dataImportService.ProcessJobCertificateAction = x => { processJobCertificationWasCalled = true; };
			dataImportService.ProcessJobLanguageAction = x => { processJobLanguageActionWasCalled = true; };
			dataImportService.ProcessJobLicenceAction = x => { processJobLicenceActionWasCalled = true; };
			dataImportService.ProcessJobLocationAction = x => { processJobLocationActionWasCalled = true; };
			dataImportService.ProcessJobProgramOfStudyAction = x => { processJobProgramOfStudyActionWasCalled = true; };
			dataImportService.ProcessJobSpecialRequirementAction = x => { processJobSpecialRequirementActionWasCalled = true; };

			dataImportService.LookupJobOrderRequestIdsByParentFunc = (x, y) => new List<long>();
			dataImportService.GetJobOrderRequestByExternalIdFunc = x =>
			{
				getJobOrderRequestByExternalIdWasCalled = true;
				return null; /* returning an empty list prevents callservice boolean being set to true and GetJobOrderRequestByExternalId will not be called */
			};

			var result = dataImportService.GenerateJobPostingFunc = x =>
			{
				generateJobPostingWasCalled = true;
				return null;
			};

			dataImportService.LookupJobOrderRequestIdsByParentFunc = (x, y) => new List<long> { 1, 2, 3 };

			// act
			var response = dataImportService.ProcessJobOrder(jobOrderImportRequest);

			// assert
			Assert.AreEqual(AcknowledgementType.Success, response.Acknowledgement);
			Assert.IsFalse(generateJobPostingWasCalled);
			Assert.IsFalse(processJobAddressActionWasCalled);
			Assert.IsFalse(processJobCertificationWasCalled);
			Assert.IsFalse(processJobLanguageActionWasCalled);
			Assert.IsFalse(processJobLicenceActionWasCalled);
			Assert.IsFalse(processJobLocationActionWasCalled);
			Assert.IsFalse(processJobProgramOfStudyActionWasCalled);
			Assert.IsFalse(processJobSpecialRequirementActionWasCalled);
			Assert.IsTrue(getJobOrderRequestByExternalIdWasCalled);

			migrationRepositoryMock.VerifyAll();
			coreRepositoryMock.VerifyAll();
			Assert.IsNotNull( result );
		}

		[Test]
		public void ProcessJobOrder_ImportAsUpdateJobOrderTests_ExceptionThrownInJobOrderProcessingIsHandledandPostingNOTGeneratedAndServiceNOTcalled()
		{
			// arrange
			var dataImportService = new DataImportService(RuntimeContext);
			var jobOrderImportRequest = new JobOrderImportRequest { JobOrderRequestId = 1 };
			//var jobOrderRequest = new JobOrderRequest() { Id = 1, IsUpdate = true };
			//var employee = new Employee { Id = 2, EmployerId = 3 };

			var coreRepositoryMock = new Mock<ICoreRepository>();
			var migrationRepositoryMock = new Mock<IMigrationRepository>();

			migrationRepositoryMock.Setup(x => x.FindById<JobOrderRequest>(It.IsAny<long>()))				.Throws(new Exception("Expected exception thrown for testing"));

			((TestRepositories)RuntimeContext.Repositories).Core = coreRepositoryMock.Object;
			((TestRepositories)RuntimeContext.Repositories).Migration = migrationRepositoryMock.Object;

			dataImportService.DeserializeSaveJobRequestFunc = x => new SaveJobRequest { JobMigrationId = "4", Module = FocusModules.General, Job = new JobDto { BusinessUnitId = 6, BusinessUnitDescriptionId = 7 } };
			dataImportService.GetEmployerRequestByEmployeeMigrationIdFunc = x => new EmployerRequest { Id = 1, FocusId = 2 };
			dataImportService.SaveJobFunc = x => new SaveJobResponse { Acknowledgement = AcknowledgementType.Success };
			dataImportService.PopulateSaveJobRequestFunc = (x, y) => { };

			bool generateJobPostingWasCalled = false;
			bool processJobAddressActionWasCalled = false;
			bool processJobCertificationWasCalled = false;
			bool processJobLanguageActionWasCalled = false;
			bool processJobLicenceActionWasCalled = false;
			bool processJobLocationActionWasCalled = false;
			bool processJobProgramOfStudyActionWasCalled = false;
			bool processJobSpecialRequirementActionWasCalled = false;
			bool getJobOrderRequestByExternalIdWasCalled = false;

			dataImportService.ProcessJobAddressAction = x => { processJobAddressActionWasCalled = true; };
			dataImportService.ProcessJobCertificateAction = x => { processJobCertificationWasCalled = true; };
			dataImportService.ProcessJobLanguageAction = x => { processJobLanguageActionWasCalled = true; };
			dataImportService.ProcessJobLicenceAction = x => { processJobLicenceActionWasCalled = true; };
			dataImportService.ProcessJobLocationAction = x => { processJobLocationActionWasCalled = true; };
			dataImportService.ProcessJobProgramOfStudyAction = x => { processJobProgramOfStudyActionWasCalled = true; };
			dataImportService.ProcessJobSpecialRequirementAction = x => { processJobSpecialRequirementActionWasCalled = true; };

			dataImportService.LookupJobOrderRequestIdsByParentFunc = (x, y) => new List<long>();
			dataImportService.GetJobOrderRequestByExternalIdFunc = x =>
			{
				getJobOrderRequestByExternalIdWasCalled = true;
				return null; /* returning an empty list prevents callservice boolean being set to true and GetJobOrderRequestByExternalId will not be called */
			};

			var result = dataImportService.GenerateJobPostingFunc = x =>
			{
				generateJobPostingWasCalled = true;
				return null;
			};

			dataImportService.LookupJobOrderRequestIdsByParentFunc = (x, y) => new List<long> { 1, 2, 3 };

			// act
			var response = dataImportService.ProcessJobOrder(jobOrderImportRequest);

			// assert
			Assert.AreEqual(AcknowledgementType.Failure, response.Acknowledgement);
			Assert.IsFalse(generateJobPostingWasCalled);
			Assert.IsFalse(processJobAddressActionWasCalled);
			Assert.IsFalse(processJobCertificationWasCalled);
			Assert.IsFalse(processJobLanguageActionWasCalled);
			Assert.IsFalse(processJobLicenceActionWasCalled);
			Assert.IsFalse(processJobLocationActionWasCalled);
			Assert.IsFalse(processJobProgramOfStudyActionWasCalled);
			Assert.IsFalse(processJobSpecialRequirementActionWasCalled);
			Assert.IsFalse(getJobOrderRequestByExternalIdWasCalled);

			migrationRepositoryMock.VerifyAll();
			coreRepositoryMock.VerifyAll();
			Assert.IsNotNull( result );
		}
		
		[Test]
		public void CheckJobOrderServiceResponse_ImportJobOrderTests_MigrationRequestLabelledWithFocusIdAndSuccessWhenAcknowledgementTypeIsSuccess()
		{
			// arrange
			var dataImportService = new DataImportService( RuntimeContext );
			var serviceResponse = new SaveJobResponse { Acknowledgement = AcknowledgementType.Success };
			var jobOrderRequest = new JobOrderRequest();

			// act
			dataImportService.CheckJobOrderServiceResponseAction(serviceResponse, jobOrderRequest, 99);

			// assert
			Assert.AreEqual(MigrationStatus.Processed, jobOrderRequest.Status);
			Assert.AreEqual(99, jobOrderRequest.FocusId);
		}

		#endregion

		#region Employer Tests

		[Test]
		[ExpectedException(typeof (ArgumentNullException))]
		public void ProcessEmployer_ImportEmployerTests_NullEmployerImportRequestThrowsArgumentNullException()
		{
			// arrange
			var dataImportService = new DataImportService( RuntimeContext );
			EmployerImportRequest request = null;

			// act
			dataImportService.ProcessEmployer(request);

			// assert
			Assert.Fail("Null EmployerImportRequest throws Argument Null Exception");
		}

		[Test]
		public void ProcessEmployer_ImportEmployerTests_EmployerIsProcessedWithMaximumDataPopulationCallingProcessOnBusinessUnitBusinessUnitLogoAndEmployerTradenames()
		{
			// arrange
			var dataImportService = new DataImportService(RuntimeContext);
			var employerImportRequest = new EmployerImportRequest { EmployerRequestId = 1 };
			var employerRequest = new EmployerRequest { Id = 1, IsUpdate = false };
			//Employee employee = new Employee { Id = 3 };
			

			var coreRepositoryMock = new Mock<ICoreRepository>();
			var migrationRepositoryMock = new Mock<IMigrationRepository>();
			
			migrationRepositoryMock.Setup(x => x.FindById<EmployerRequest>(It.IsAny<long>())).Returns(employerRequest);
			coreRepositoryMock.Setup(x => x.SaveChanges(It.IsAny<bool>())).Verifiable();

			((TestRepositories)RuntimeContext.Repositories).Core = coreRepositoryMock.Object;
			((TestRepositories)RuntimeContext.Repositories).Migration = migrationRepositoryMock.Object;

			bool processBusinessUnitDescriptionWasCalled = false;
			bool processBusinessUnitLogoWasCalled = false;
			bool processEmployerTradeNamesWasCalled = false;

			dataImportService.DeserializeRegisterTalentUserRequestFunc = x => new RegisterTalentUserRequest { BusinessUnit = new BusinessUnitDto() };
			dataImportService.PrepareServiceRequestPropertiesFunc = (x, y) => { };
			dataImportService.RegisterTalentUserFunc = x => new RegisterTalentUserResponse();
			dataImportService.CheckEmployerServiceResponseWrapperFunc = (x, y) => { };
			dataImportService.LookupEmployerRequestIdsByParentFunc = (x, y) => new List<long> { 1, 2, 3 };
			dataImportService.ProcessBusinessUnitDescriptionFunc = (x, y, z) =>
			{
				processBusinessUnitDescriptionWasCalled = true;
				return null;
			};
			dataImportService.ProcessBusinessUnitLogoFunc = (x, y, z) =>
			{
				processBusinessUnitLogoWasCalled = true;
				return null;
			};
			dataImportService.ProcessEmployerTradeNamesFunc = (x, y, z) =>
			{
				processEmployerTradeNamesWasCalled = true;
				return null;
			};

			// act
			var response = dataImportService.ProcessEmployer(employerImportRequest);

			// assert
			Assert.AreEqual(AcknowledgementType.Success, response.Acknowledgement);
			Assert.IsTrue(processBusinessUnitDescriptionWasCalled);
			Assert.IsTrue(processBusinessUnitLogoWasCalled);
			Assert.IsTrue(processEmployerTradeNamesWasCalled);
			migrationRepositoryMock.VerifyAll();
			coreRepositoryMock.VerifyAll();
		}

		[Test]
		public void ProcessEmployer_ImportEmployerTests_EmployerIsProcessedWithMinimumDataPopulationAndNOTCallingProcessOnBusinessUnitBusinessUnitLogoNOREmployerTradenames()
		{
			// arrange
			var dataImportService = new DataImportService(RuntimeContext);
			var employerImportRequest = new EmployerImportRequest { EmployerRequestId = 1 };
			var employerRequest = new EmployerRequest { Id = 1, IsUpdate = false };
			//Employee employee = new Employee { Id = 3 };


			var coreRepositoryMock = new Mock<ICoreRepository>();
			var migrationRepositoryMock = new Mock<IMigrationRepository>();

			migrationRepositoryMock.Setup(x => x.FindById<EmployerRequest>(It.IsAny<long>())).Returns(employerRequest);
			coreRepositoryMock.Setup(x => x.SaveChanges(It.IsAny<bool>())).Verifiable();

			((TestRepositories)RuntimeContext.Repositories).Core = coreRepositoryMock.Object;
			((TestRepositories)RuntimeContext.Repositories).Migration = migrationRepositoryMock.Object;

			bool processBusinessUnitDescriptionWasCalled = false;
			bool processBusinessUnitLogoWasCalled = false;
			bool processEmployerTradeNamesWasCalled = false;

			dataImportService.DeserializeRegisterTalentUserRequestFunc = x => new RegisterTalentUserRequest { BusinessUnit = new BusinessUnitDto() };
			dataImportService.PrepareServiceRequestPropertiesFunc = (x, y) => { };
			dataImportService.RegisterTalentUserFunc = x => new RegisterTalentUserResponse();
			dataImportService.CheckEmployerServiceResponseWrapperFunc = (x, y) => { };
			dataImportService.LookupEmployerRequestIdsByParentFunc = (x, y) => new List<long>() ;
			dataImportService.ProcessBusinessUnitDescriptionFunc = (x, y, z) =>
			{
				processBusinessUnitDescriptionWasCalled = true;
				return null;
			};
			dataImportService.ProcessBusinessUnitLogoFunc = (x, y, z) =>
			{
				processBusinessUnitLogoWasCalled = true;
				return null;
			};
			dataImportService.ProcessEmployerTradeNamesFunc = (x, y, z) =>
			{
				processEmployerTradeNamesWasCalled = true;
				return null;
			};

			// act
			var response = dataImportService.ProcessEmployer(employerImportRequest);

			// assert
			Assert.AreEqual(AcknowledgementType.Success, response.Acknowledgement);
			Assert.IsFalse(processBusinessUnitDescriptionWasCalled);
			Assert.IsFalse(processBusinessUnitLogoWasCalled);
			Assert.IsFalse(processEmployerTradeNamesWasCalled);
			migrationRepositoryMock.VerifyAll();
			coreRepositoryMock.VerifyAll();
		}

		[Test]
		public void ProcessEmployer_ImportEmployerTests_UpdateRequestButCannotFindRecordToUpdateDoesNotCallService()
		{
			// arrange
			var dataImportService = new DataImportService( RuntimeContext );
			var employerImportRequest = new EmployerImportRequest { EmployerRequestId = 1 };
			var employerRequest = new EmployerRequest { Id = 1, IsUpdate = true };
			//Employee employee = new Employee { Id = 3 };


			var coreRepositoryMock = new Mock<ICoreRepository>();
			var migrationRepositoryMock = new Mock<IMigrationRepository>();

			migrationRepositoryMock.Setup(x => x.FindById<EmployerRequest>(It.IsAny<long>())).Returns(employerRequest);

			((TestRepositories)RuntimeContext.Repositories).Core = coreRepositoryMock.Object;
			((TestRepositories)RuntimeContext.Repositories).Migration = migrationRepositoryMock.Object;

			bool processBusinessUnitDescriptionWasCalled = false;
			bool processBusinessUnitLogoWasCalled = false;
			bool processEmployerTradeNamesWasCalled = false;

			dataImportService.DeserializeRegisterTalentUserRequestFunc = x => new RegisterTalentUserRequest { BusinessUnit = new BusinessUnitDto() };
			dataImportService.LookupEmployerRequestFunc = x => null;
			dataImportService.PrepareServiceRequestPropertiesFunc = (x, y) => { };
			dataImportService.RegisterTalentUserFunc = x => new RegisterTalentUserResponse();
			dataImportService.CheckEmployerServiceResponseWrapperFunc = (x, y) => { };
			dataImportService.LookupEmployerRequestIdsByParentFunc = (x, y) => new List<long> { 1, 2, 3 };
			dataImportService.ProcessBusinessUnitDescriptionFunc = (x, y, z) =>
			{
				processBusinessUnitDescriptionWasCalled = true;
				return null;
			};
			dataImportService.ProcessBusinessUnitLogoFunc = (x, y, z) =>
			{
				processBusinessUnitLogoWasCalled = true;
				return null;
			};
			dataImportService.ProcessEmployerTradeNamesFunc = (x, y, z) =>
			{
				processEmployerTradeNamesWasCalled = true;
				return null;
			};

			// act
			var response = dataImportService.ProcessEmployer(employerImportRequest);

			// assert
			Assert.AreEqual(AcknowledgementType.Success, response.Acknowledgement); /* success is set as if it is not the record will be retried */
			Assert.IsFalse(processBusinessUnitDescriptionWasCalled);
			Assert.IsFalse(processBusinessUnitLogoWasCalled);
			Assert.IsFalse(processEmployerTradeNamesWasCalled);
			migrationRepositoryMock.VerifyAll();
			coreRepositoryMock.VerifyAll();
		}

		[Test]
		public void SaveSourceEmployerCreatedOnAndUpdatedOnDates_ImportEmployerTests_ValidCreatedOnDateUpdatesEmployerRecord()
		{
			// arrange
			var employer = new EmployerDto();
			var dataImportService = new DataImportService( RuntimeContext );
			var registerTalentUserRequest = new RegisterTalentUserRequest();
			employer.FederalEmployerIdentificationNumber = "11-1111111";
			registerTalentUserRequest.Employer = employer;
			registerTalentUserRequest.OverrideEmployerCreatedOn = new DateTime(2015, 05, 06);

			var queryExpression = Entity.Attribute("FederalEmployerIdentificationNumber") == registerTalentUserRequest.Employer.FederalEmployerIdentificationNumber;

			var coreRepositoryMock = new Mock<ICoreRepository>();
			coreRepositoryMock.Setup(x => x.Update(It.Is<Query>(y => y.EntityType == typeof(Employer) && y.QueryExpression.ToString() == queryExpression.ToString()), It.IsAny<object>()));
			coreRepositoryMock.Setup(x => x.SaveChanges(false));

			((TestRepositories)RuntimeContext.Repositories).Core = coreRepositoryMock.Object;

			// act
			dataImportService.SaveSourceEmployerCreatedOnAndUpdatedOnDates(registerTalentUserRequest);

			// assert
			coreRepositoryMock.VerifyAll();
			// update query is being verified on the coreRepositoryMock

		}

		[Test]
		public void SaveSourceEmployerCreatedOnAndUpdatedOnDates_ImportEmployerTests_ValidUpdatedOnDateUpdatesEmployerRecord()
		{
			// arrange
			var employer = new EmployerDto();
			var dataImportService = new DataImportService( RuntimeContext );
			var registerTalentUserRequest = new RegisterTalentUserRequest();
			employer.FederalEmployerIdentificationNumber = "11-1111111";
			registerTalentUserRequest.Employer = employer;
			registerTalentUserRequest.OverrideEmployerUpdatedOn = new DateTime(2015, 05, 06);

			var queryExpression = Entity.Attribute("FederalEmployerIdentificationNumber") == registerTalentUserRequest.Employer.FederalEmployerIdentificationNumber;

			var coreRepositoryMock = new Mock<ICoreRepository>();
			coreRepositoryMock.Setup(x => x.Update(It.Is<Query>(y => y.EntityType == typeof(Employer) && y.QueryExpression.ToString() == queryExpression.ToString()), It.IsAny<object>()));
			coreRepositoryMock.Setup(x => x.SaveChanges(false));

			((TestRepositories)RuntimeContext.Repositories).Core = coreRepositoryMock.Object;

			// act
			dataImportService.SaveSourceEmployerCreatedOnAndUpdatedOnDates(registerTalentUserRequest);

			// assert
			coreRepositoryMock.VerifyAll();
			// update query is being verified on the coreRepositoryMock

		}

		[Test]
		public void SaveSourceEmployeeCreatedOnAndUpdatedOnDates_ImportEmployerTests_ValidCreatedOnDateUpdatesEmployeeRecord()
		{
			// arrange
			var user = new UserDto();
			var dataImportService = new DataImportService( RuntimeContext );
			var registerTalentUserRequest = new RegisterTalentUserRequest();
			user.UserName = "test@user.com";
			registerTalentUserRequest.User = user;
			registerTalentUserRequest.OverrideUserCreatedOn = new DateTime(2015, 05, 06);

			var queryExpression = Entity.Attribute("UserName") == registerTalentUserRequest.User.UserName;

			var coreRepositoryMock = new Mock<ICoreRepository>();
			coreRepositoryMock.Setup(x => x.Update(It.Is<Query>(y => y.EntityType == typeof(User) && y.QueryExpression.ToString() == queryExpression.ToString()), It.IsAny<object>()));
			coreRepositoryMock.Setup(x => x.SaveChanges(false));

			((TestRepositories)RuntimeContext.Repositories).Core = coreRepositoryMock.Object;

			// act
			dataImportService.SaveSourceEmployeeCreatedOnAndUpdatedOnDates(registerTalentUserRequest);

			// assert
			coreRepositoryMock.VerifyAll();
			// update query is being verified on the coreRepositoryMock

		}

		[Test]
		public void SaveSourceEmployeeCreatedOnAndUpdatedOnDates_ImportEmployerTests_ValidUpdatedOnDateUpdatesEmployeeRecord()
		{
			// arrange
			var user = new UserDto();
			var dataImportService = new DataImportService( RuntimeContext );
			var registerTalentUserRequest = new RegisterTalentUserRequest();
			user.UserName = "test@user.com";
			registerTalentUserRequest.User = user;
			registerTalentUserRequest.OverrideUserUpdatedOn = new DateTime(2015, 05, 06);

			var queryExpression = Entity.Attribute("UserName") == registerTalentUserRequest.User.UserName;

			var coreRepositoryMock = new Mock<ICoreRepository>();
			coreRepositoryMock.Setup(x => x.Update(It.Is<Query>(y => y.EntityType == typeof(User) && y.QueryExpression.ToString() == queryExpression.ToString()), It.IsAny<object>()));
			coreRepositoryMock.Setup(x => x.SaveChanges(false));

			((TestRepositories)RuntimeContext.Repositories).Core = coreRepositoryMock.Object;

			// act
			dataImportService.SaveSourceEmployeeCreatedOnAndUpdatedOnDates(registerTalentUserRequest);

			// assert
			coreRepositoryMock.VerifyAll();
			// update query is being verified on the coreRepositoryMock

		}


		#endregion

		#region Migration Tests

		[Test]
		public void UseAccountUserNameAsEmailAddressIfEmailAddressIsInvalid_WhenPassedInvalidEmailAddress_OverwritesEmailAddressWithAccountUserName()
	  {
		  // Arrange
			var service = new MigrationService( RuntimeContext );
			var request = new RegisterCareerUserRequest
			{
				EmailAddress = "IWillNotMatchEmailRegex",
				AccountUserName = "Match@EmailRegex.com"
			};

			// Act
			service.UseAccountUserNameAsEmailAddressIfEmailAddressIsInvalid(request);

			// Assert
			Assert.AreEqual(request.EmailAddress, "Match@EmailRegex.com");

		}

		[Test]
		public void UseAccountUserNameAsEmailAddressIfEmailAddressIsInvalid_WhenPassedInvalidEmailAddressAndInvalidAccountUserName_DoesNotOverwriteEmailAddressWithAccountUserName()
		{
			// Arrange
			var service = new MigrationService( RuntimeContext );
			var request = new RegisterCareerUserRequest
			{
				EmailAddress = "IWillNotMatchEmailRegex",
				AccountUserName = "NoMatchEmailRegex.com"
			};

			// Act
			service.UseAccountUserNameAsEmailAddressIfEmailAddressIsInvalid(request);

			// Assert
			Assert.AreEqual(request.EmailAddress, "IWillNotMatchEmailRegex");

		}

		[Test]
		public void UseAccountUserNameAsEmailAddressIfEmailAddressIsInvalid_WhenPassedValidEmailAddressAndInvalidAccountUserName_DoesNotOverwriteEmailAddressWithAccountUserName()
		{
			// Arrange
			var service = new MigrationService( RuntimeContext );
			var request = new RegisterCareerUserRequest
			{
				EmailAddress = "IWillMatch@EmailRegex.com",
				AccountUserName = "NoMatchEmailRegex.com"
			};

			// Act
			service.UseAccountUserNameAsEmailAddressIfEmailAddressIsInvalid(request);

			// Assert
			Assert.AreEqual(request.EmailAddress, "IWillMatch@EmailRegex.com");

		}

		[Test]
		public void AssignCorrectlyFormattedEmailAddressToAccountUserNameField_WhenPassedAccountUserNameNotInEmailAddressFormat_GeneratesADummyEmailFormattedUserName()
		{
			// Arrange
			var provider = new FocusCareer01Provider();
			var model = new CreateAssistUserModel
			{
				AccountUserName = "NotEmailAddressFormat",
				UserEmailAddress = "NotEmailAddressFormat",
				UserPerson = new PersonDto { FirstName = "Chris", LastName = "Evans" }
			};

			// Act
			provider.AssignCorrectlyFormattedEmailAddressToAccountUserNameField(model);

			// Assert
			Assert.IsTrue(Regex.IsMatch(model.AccountUserName, @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"));
		}

		[Test]
		public void AssignCorrectlyFormattedEmailAddressToAccountUserNameField_WhenPassedAccountUserNameNotInEmailAddressFormatButWithAnATSymbol_GeneratesADummyEmailFormattedUserName()
		{
			// Arrange
			var provider = new FocusCareer01Provider();
			var model = new CreateAssistUserModel
			{
				AccountUserName = "@NotAnEmailAddress",
				UserEmailAddress = "NotEmailAddressFormat",
				UserPerson = new PersonDto { FirstName = "Chris", LastName = "Evans" }
			};

			// Act
			provider.AssignCorrectlyFormattedEmailAddressToAccountUserNameField(model);

			// Assert
			Assert.IsTrue(Regex.IsMatch(model.AccountUserName, @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"));
			Assert.AreEqual(model.AccountUserName, "Chris.Evans@kygovmigrated.com");
		}

		[Test]
		public void AssignCorrectlyFormattedEmailAddressToAccountUserNameField_WhenPassedAccountUserNameNotInEmailAddressFormatButWithAValidEmailAddressField_EmailAddressUsedAsUserName()
		{
			// Arrange
			var provider = new FocusCareer01Provider();
			var model = new CreateAssistUserModel
			{
				AccountUserName = "@NotAnEmailAddress",
				UserEmailAddress = "a@b.com",
				UserPerson = new PersonDto { FirstName = "Chris", LastName = "Evans" }
			};

			// Act
			provider.AssignCorrectlyFormattedEmailAddressToAccountUserNameField(model);

			// Assert
			Assert.IsTrue(Regex.IsMatch(model.AccountUserName, @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"));
			Assert.AreEqual(model.AccountUserName, "a@b.com");
		}

		#endregion

		#region Program Tests

		[Test]
		[ExpectedException(typeof(Exception))]
		public void ParseArguments_WhenPassedTooManyBatches_SelectsTheCorrectNumberOfBatches()
		{
			// Arrange
			var arguments = new[] { "migrate", "jobseeker", @"/N:21" };

			// Act
			Program.ParseArguments(arguments);

			// Assert
		}

		[Test]
		public void ParseArguments_WhenPassedTwoArguments_RecordTypeNoneIsReturned()
		{
			// Arrange
			var arguments = new[] { "migrate" };

			// Act
			var returnedArguments = Program.ParseArguments(arguments);

			// Assert
			Assert.AreEqual(RecordType.None, returnedArguments.RecordType);
		}

		[Test]
		public void ParseArguments_WhenPassedBatchingArgumentsForTenThreads_NumberOfBatchesShowsTenBatches()
		{
			// Arrange
			var arguments = new[] {"migrate", "jobseeker", "/N:10"};

			// Act
			var returnedArguments = Program.ParseArguments(arguments);

			// Assert
			Assert.AreEqual(10, returnedArguments.NumberOfBatches);
		}
		
		[Test]
		public void ParseArguments_WhenPassedProviderNameFocusCareer01_SelectsTheFocusCareer01Provider()
		{
			// Arrange
			var arguments = new[] { "migrate", "jobseeker", "/P:FocusCareer01" };

			// Act
			var returnedArguments = Program.ParseArguments(arguments);

			// Assert
			Assert.AreEqual("focuscareer01", returnedArguments.ProviderName);
		}

		[Test]
		public void ParseArguments_WhenPassedFileOrFolderName_SelectsTheCorrectFileOrFolderName()
		{
			// Arrange
			var arguments = new[] { "migrate", "jobseeker", @"/F:C:\AnyFolder\AnyFile.xml" };

			// Act
			var returnedArguments = Program.ParseArguments(arguments);

			// Assert
			Assert.AreEqual(@"c:\anyfolder\anyfile.xml", returnedArguments.FolderOrFileName);
		}

		[Test]
		public void ParseArguments_WhenPassedDatabaseConnection_SelectsTheCorrectDatabaseConnection()
		{
			// Arrange
			var arguments = new[] { "migrate", "jobseeker", @"/D:Data Source=xxx;Initial Catalog=xxx;User ID=xxx;Password=xxx" };

			// Act
			var returnedArguments = Program.ParseArguments(arguments);

			// Assert
			Assert.AreEqual(@"data source=xxx;initial catalog=xxx;user id=xxx;password=xxx", returnedArguments.DatabaseConnection);
		}

		[Test]
		public void ParseArguments_WhenPassedBatchSize_SelectsTheCorrectBatchSize()
		{
			// Arrange
			var arguments = new[] { "migrate", "jobseeker", @"/B:200" };

			// Act
			var returnedArguments = Program.ParseArguments(arguments);

			// Assert
			Assert.AreEqual(200, returnedArguments.BatchSize);
		}

		[Test]
		public void ParseArguments_WhenPassedBatchNumber_SelectsTheCorrectBatchNumber()
		{
			// Arrange
			var arguments = new[] { "migrate", "jobseeker", @"/BN:10" };

			// Act
			var returnedArguments = Program.ParseArguments(arguments);

			// Assert
			Assert.AreEqual(10, returnedArguments.BatchNumber);
		}
		
		[Test]
		public void ParseArguments_WhenPassedNumberOfBatches_SelectsTheCorrectNumberOfBatches()
		{
			// Arrange
			var arguments = new[] { "migrate", "jobseeker", @"/N:10" };

			// Act
			var returnedArguments = Program.ParseArguments(arguments);

			// Assert
			Assert.AreEqual(10, returnedArguments.NumberOfBatches);
		}
		
		[Test]
		public void ParseArguments_WhenPassedSpoof_SwitchesOnSpoofMode()
		{
			// Arrange
			var arguments = new[] { "migrate", "jobseeker", @"/spoof" };

			// Act
			var returnedArguments = Program.ParseArguments(arguments);

			// Assert
			Assert.IsTrue(returnedArguments.SpoofData);
		}
		
		[Test]
		public void ParseArguments_WhenPassedCustomParameter_SelectsTheCustomParameter()
		{
			// Arrange
			var arguments = new[] { "migrate", "jobseeker", @"/C:10" };

			// Act
			var returnedArguments = Program.ParseArguments(arguments);

			// Assert
			Assert.AreEqual("10", returnedArguments.CustomParm);
		}
		
		[Test]
		public void ParseArguments_WhenPassedUpdateExistingRecords_SelectsTheCorrectCutOffDate()
		{
			// Arrange
			var arguments = new[]{ "migrate", "jobseeker", @"/E:10/10/2015" };

			// Act
			var returnedArguments = Program.ParseArguments(arguments);

			// Assert
			Assert.AreEqual(new DateTime(2015, 10, 10), returnedArguments.CutOffDate);
		}
		
		[Test]
		public void ParseArguments_WhenPassedLastId_SelectsTheCorrectLastId()
		{
			// Arrange
			var arguments = new[] { "migrate", "jobseeker", @"/L:10" };

			// Act
			var returnedArguments = Program.ParseArguments(arguments);

			// Assert
			Assert.AreEqual(10, returnedArguments.LastId);
		}

		#endregion
	}
}
