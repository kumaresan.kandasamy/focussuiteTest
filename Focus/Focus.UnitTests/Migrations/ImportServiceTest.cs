﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using NUnit.Framework;

using Focus.MigrationServices.Messages;
using Focus.MigrationServices.ServiceImplementations;
using Framework.Testing.NUnit;

#endregion

namespace Focus.UnitTests.Migrations
{
  [TestFixture]
  [Explicit]
  public class ImportServiceTests : TestFixtureBase
  {
    [SetUp]
    public override void SetUp()
    {
      DoSetup(false);
    }

    [Ignore]
    [Test]
    public void Import_WhenPassedValidRequest_ReturnsSuccessWithCounts()
    {
      // Arrange
      var service = new ImportService(MigrationRepository, Repository);

      var request = new ImportRequest()
      {
        UserContext = MockUserData,
        ClientTag = MockClientTag,
        SessionId = MockSessionId,
        Debug = true
      };
      request.UserContext.Culture = MockCulture;

      // Act
      var response = service.Import(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      //response.NumberOfEmployersCreated.ShouldBeGreater(0);
      //response.NumberOfJobOrdersCreated.ShouldBeGreater(0);
      //response.NumberOfJobSeekersCreated.ShouldEqual(0);
      //response.NumberOfUsersCreated.ShouldEqual(0);
    }

    [Ignore]
    [Test]
    public void Import_WhenPassedInValidRequest_ReturnsFailure()
    {
      // Arrange
      var service = new ImportService(MigrationRepository, Repository);
      var request = new ImportRequest
      {
        ClientTag = "Bananas",
        Debug = true
      };

      // Act
      var response = service.Import(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
    }
  }
}
