﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Focus.Core.Criteria.JobSeeker;
using Focus.Core.Models;
using Focus.Core.Views;
using Focus.Services.Core;

using Focus.Services.Views;
using Framework.Testing.NUnit;
using Framework.Email;
using NUnit.Framework;

using Focus.Services.Repositories.FocusCareer;

namespace Focus.UnitTests.Repositories
{
	[TestFixture]
	[Explicit]
	public class FocusCareerRepositoryTests
	{
		private const string TestCandidateId = "2"; // test@demo.com
		private const string TestExternalUserId = "KY0001";
		private const string TestExternalUserName = "jmacbeth";
		private const string TestExternalPassword = "abcd1234";
		private const string TestExternalOfficeId = "KY100092967";
		
		private FocusCareerRepository _repository;
		private static IAppSettings MockAppSettings = new FakeAppSettings();

		[SetUp]
		public void Setup()
		{
			var settings = MockAppSettings.FocusCareerSettings;
			_repository = new FocusCareerRepository(settings);			
		}

		[Test]
		public void GetResume_WhenGivenValidDetails_ReturnsResumeView()
		{
			// Arrange
			const string candidateId = TestCandidateId;

			// Act 
			var resumeView = _repository.GetResume(candidateId);

			// Assert
			resumeView.ShouldNotBeNull();
			resumeView.ResumeXml.ShouldNotBeNullOrEmpty();
		}

		[Test]
		public void PostJob_WhenGivenValidDetails_PostsJob()
		{
			// Arrange
			const string jobId = "KY0431458";
			var jobDefinition = GetJobPostingXml(jobId, "Unit Test Job : " + jobId, DateTime.Now.AddDays(30));
			var jobPosting = GetJobPostingHtml("Unit Test Job : " + jobId, DateTime.Now.AddDays(30));

			// Act 
			try
			{
				_repository.PostJob(jobId, jobDefinition, jobPosting);
			}
			catch (Exception ex)
			{
				// Assert
				Assert.Fail("Focus Career Post Job failed!" + Environment.NewLine + Environment.NewLine + ex.Message);
			}		
		}

		[Test]
		public void SearchJobSeekers_WhenGivenValidCriteria_ReturnsJobSeekersView()
		{
			// Arrange
			var criteria = new JobSeekerCriteria { Firstname = "Gn1", Lastname = "Sn1" };

			// Act 
			var jobSeekers = _repository.SearchJobSeekers(criteria);

			// Assert
			jobSeekers.ShouldNotBeNull();
			jobSeekers.Count.ShouldBeGreater(0);
		}

		[Test]
		public void AccessCandidate_WhenGivenValidDetails_ReturnsUrl()
		{
			// Arrange
			var userContext = new UserContext { ExternalUserId = TestExternalUserId, ExternalUserName = TestExternalUserName, ExternalPassword = TestExternalPassword, ExternalOfficeId = TestExternalOfficeId };
			var candidateUserName = "Gn97632Sn2822@demo.com";

			// Act 
			var url = _repository.AccessCandidate(userContext, candidateUserName, null);

			// Assert
			url.ShouldNotBeNull();
		}

		[Test]
		public void CreateCandidate_WhenGivenValidDetails_ReturnsUrl()
		{
			// Arrange
			var userContext = new UserContext { ExternalUserId = TestExternalUserId, ExternalUserName = TestExternalUserName, ExternalPassword = TestExternalPassword, ExternalOfficeId = TestExternalOfficeId };

			// Act 
			var url = _repository.CreateCandidate(userContext);

			// Assert
			url.ShouldNotBeNull();
		}

		[Test]
		public void GetJobUrl_WhenGivenValidJobId_ReturnsUrl()
		{
			// Arrange
			const string jobId = "KY0408695";

			// Act 
			var url = _repository.GetJobUrl(jobId);

			// Assert
			url.ShouldNotBeNull();
		}

		[Test]
		public void SendCandidateEmails_WhenGivenValidJobId_SendsEmails()
		{			
			//TODO: Martha need to get a valid Career user person id
			// Arrange
			var candidateEmailViews = new[] { new CandidateEmailView { PersonId = "1", To = "jmacbeth@burning-glass.com", Subject = "Test Candidate Email", Body = "Test Candidate Email - Body", BccRequestor = true } };

			// Act 
			try
			{
				_repository.SendCandidateEmails(candidateEmailViews, "jmacbeth@burning-glass.com");
			}
			catch (Exception)
			{
				// Assert
				Assert.Fail("Focus Career Post Job failed!");
			}
		}

		#region Helper Methods

		/// <summary>
		/// Gets the job posting HTML.
		/// </summary>
		/// <param name="jobTitle">The job title.</param>
		/// <param name="endDate">The end date.</param>
		/// <returns></returns>
		private static string GetJobPostingHtml(string jobTitle, DateTime endDate)
		{
			var html = File.ReadAllText("App_Data\\SampleFiles\\JobPosting.html");
			return html.Replace("#JOB TITLE#", jobTitle).Replace("#END_DATE#", endDate.ToString("dd-MMM-yyyy"));
		}

		/// <summary>
		/// Gets the job posting XML.
		/// </summary>
		/// <param name="jobId">The job id.</param>
		/// <param name="jobTitle">The job title.</param>
		/// <param name="endDate">The end date.</param>
		/// <returns></returns>
		private static string GetJobPostingXml(string jobId, string jobTitle, DateTime endDate)
		{
			var xml = File.ReadAllText("App_Data\\SampleFiles\\JobPosting.xml");
			return xml.Replace("#JOB_ID#", jobId).
								 Replace("#CREATE_DATE#", endDate.ToString("yyyy-MM-dd")).
								 Replace("#JOB TITLE#", jobTitle).
								 Replace("#END_DATE#", endDate.ToString("yyyy-MM-dd"));
		}

		#endregion
	}
}
