﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.IO;
using System.Text;
using Focus.Core;
using Focus.Core.Settings.Lens;
using Focus.Services.Repositories.Lens.Clients;
using Framework.Testing.NUnit;
using NUnit.Framework;

#endregion

namespace Focus.UnitTests.Repositories.LensClients
{
	[TestFixture]
	public class HttpClientTests
	{
		[Test]
		public void ConvertBinaryData_WhenPassedValidRequest_ReturnsData()
		{
			// Arrange
			const string fileName = @"App_Data\\SampleFiles\\resume.doc";

			byte[] fileBytes;

			using (var binaryReader = new BinaryReader(new FileStream(fileName, FileMode.Open, FileAccess.Read)))
			{
				var byteLength = new FileInfo(fileName).Length;
				fileBytes = binaryReader.ReadBytes((Int32)byteLength);
			}

			var lensClient = GetLensClient();

			// Act
			var result = lensClient.ConvertBinaryData(fileBytes, "doc");

			// Assert
			result.ShouldNotBeNullOrEmpty();
		}

		[Test]
		public void ExecuteCommand_WhenPassedValidRequest_ReturnsResult()
		{
			// Arrange
			const string command = "<bgtcmd><tag>\nBurning Glass Technologies\nOne Faneuil Hall Market, 4th Floor\nBoston, MA 02109</tag></bgtcmd>";
			var lensClient = GetLensClient();

			// Act
			var result = lensClient.ExecuteCommand(command);

			// Assert
			result.ShouldNotBeNullOrEmpty();
		}

		[Test]
		public void TagBinaryData_WhenPassedValidRequest_ReturnsTaggedDocument()
		{
			// Arrange
			const string resume = "Resume for example candidate.";
			var encoding = new ASCIIEncoding();
			var bytes = encoding.GetBytes(resume);

			var lensClient = GetLensClient();

			// Act
			var result = lensClient.TagBinaryData(bytes, "txt", DocumentType.Resume);

			// Assert
			result.ShouldNotBeNullOrEmpty();
		}

		[Test]
		public void TagBinaryDataWithHtml_WhenPassedValidRequest_ReturnsHtmlAndTaggedDocument()
		{
			// Arrange
			const string resume = "Resume for example candidate.";
			var encoding = new ASCIIEncoding();
			var bytes = encoding.GetBytes(resume);

			var lensClient = GetLensClient();
			var html = String.Empty;

			// Act
			var result = lensClient.TagBinaryDataWithHtml(bytes, "txt", DocumentType.Resume, out html);

			// Assert
			result.ShouldNotBeNullOrEmpty();
			html.ShouldNotBeNullOrEmpty();
		}

		private HttpClient GetLensClient()
		{
			var lensService = new LensService
			{
				Url = "http://devappsvr001:9000",
				Timeout = 60000,
				Vendor = "careerjobs",
				CharacterSet = "iso-8859-1",
				ConsumerKey = "c8028fae-4887-4e91-8fa5-9655adae6ec1"
			};

			return new HttpClient(lensService);
		}
	}
}
