﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Xml.Linq;
using Focus.Core;
using Focus.Core.Criteria.CandidateSearch;
using Focus.Core.Models.Career;
using Focus.Core.Settings.Lens;
using Focus.Services.Repositories.Lens;
using Focus.UnitTests.Core;
using Framework.Testing.NUnit;
using Framework.Core;
using NUnit.Framework;

#endregion


namespace Focus.UnitTests.Repositories
{
	[TestFixture]
	public class LensRepositoryTests : TestFixtureBase
	{
		
		#region Len Settings Serialization Tests

		[Test]
		public void LensSettings_Serialization_ReturnsString()
		{
			// Arrange
			
			// Act
			var result = MockAppSettings.LensServices.SerializeJson();

			Trace.TraceInformation("Json:" + Environment.NewLine + Environment.NewLine + result + Environment.NewLine + Environment.NewLine);

			// Assert
			Assert.IsNotNullOrEmpty(result);
		}


		[Test]
		public void DeserializeFromJson_WhenPassedValidJsonWithLensHost_ReturnsLensService()
		{
			// Arrange
			const string json = @"{""host"":""DEVAPPSVR002"",""portNumber"":2003,""timeout"":600,""vendor"":""careerbdd"",""characterSet"":""iso-8859-1"",""serviceType"":""Posting"",""description"":""Career posting repository"",""readonly"":false,""customFilters"":[{""key"":""Status"",""tag"":""cf001""},{""key"":""OpenDate"",""tag"":""cf002""},{""key"":""ClosingOn"",""tag"":""cf003""},{""key"":""MinimumSalary"",""tag"":""cf004""},{""key"":""MaximumSalary"",""tag"":""cf005""},{""key"":""ExternalPostingId"",""tag"":""cf007""},{""key"":""Occupation"",""tag"":""cf008""},{""key"":""StateCode"",""tag"":""cf009""},{""key"":""PostingId"",""tag"":""cf010""},{""key"":""Origin"",""tag"":""cf012""},{""key"":""Sic2"",""tag"":""cf013""},{""key"":""Industry"",""tag"":""cf014""},{""key"":""EducationLevel"",""tag"":""cf016""},{""key"":""GreenJob"",""tag"":""cf022""},{""key"":""Sector"",""tag"":""cf024""},{""key"":""CountyCode"",""tag"":""cf025""},{""key"":""Internship"",""tag"":""cf026""},{""key"":""ProgramsOfStudy"",""tag"":""cf027""},{""key"":""AnnualPay"",""tag"":""cf028""},{""key"":""Salary"",""tag"":""cf029""},{""key"":""Msa"",""tag"":""cf030""},{""key"":""ROnet"",""tag"":""cf031""},{""key"":""UnpaidPosition"",""tag"":""cf033""},{""key"":""InternshipCategories"",""tag"":""cf034""},{""key"":""RequiredProgramOfStudyId"",""tag"":""cf036""},{""key"":""HomeBased"",""tag"":""cf037""},{""key"":""ResumeIndustries"",""tag"":""cf026""}]}";

			// Act
			var service = json.DeserializeJson<LensService>();

			// Assert
			service.ShouldNotBeNull();
		}

		[Test]
		public void DeserializeFromJson_WhenPassedValidJsonWithNoCustomFilters_ReturnsLensServiceWithNoCustomFilters()
		{
			// Arrange
			const string json = @"{""host"":""DEVAPPSVR002"",""portNumber"":2003,""timeout"":600,""vendor"":""careerbdd"",""characterSet"":""iso-8859-1"",""serviceType"":""Posting"",""description"":""Career posting repository"",""readonly"":false}";

			// Act
			var service = json.DeserializeJson<LensService>();

			// Assert
			service.ShouldNotBeNull();
			service.CustomFilters.ShouldBeNull();
		}

		[Test]
		public void DeserializeFromJson_WhenPassedEmptyJson_ReturnsLensServiceWithNoCustomFilters()
		{
			// Arrange
			const string json = @"{}";

			// Act
			var service = json.DeserializeJson<LensService>();

			// Assert
			service.ShouldNotBeNull();
		}

		[Test]
		public void DeserializeFromJson_WhenPassedValidJsonWithHALens_ReturnsLensService()
		{
			// Arrange
			const string json = @"{""url"":""somewhere"", ""consumerKey"":""some key"", ""timeout"":600,""vendor"":""careerbdd"",""characterSet"":""iso-8859-1"",""serviceType"":""Posting"",""description"":""Career posting repository"",""readonly"":false}";

			// Act
			var service = json.DeserializeJson<LensService>();

			// Assert
			service.ShouldNotBeNull();
			service.Url.ShouldEqual("somewhere");
			service.ConsumerKey.ShouldEqual("some key");
		}

		#endregion

		[TestCase(LensTransportMethod.Tcp, TestName = "Match_WhenPassedValidXml_ReturnsScore - MS Lens")]
		[TestCase(LensTransportMethod.Http, TestName = "Match_WhenPassedValidXml_ReturnsScore - HA Lens")]
		public void Match_WhenPassedValidXml_ReturnsScore(LensTransportMethod transport)
		{
			// Arrange
			SetUpLensServices(transport);
			
			#region Arrange xml
 
			const string resumeXml = @"<ResDoc><special><automatedsummary custom=""-1"" /><snapshot>I have 3 years of experience, including as a Design Engineer.  Most recently, I have been working as a Design Engineer at Test Employer from January 2010 to March 2012.  My skills and experiences include: Materials Management.  I hold a Bachelor of Engineering (BEng or BE) degree in Mechanical Engineer from University.</snapshot></special><resume canonversion=""2"" dateversion=""2"" iso8601=""2012-03-22"" present=""734586""><contact><name><givenname>Test</givenname><givenname>I</givenname><surname>N</surname></name>  <address lat=""42.0707"" lon=""72.625"" majorcity=""AGAWAM"" state=""MA""><state abbrev=""MA"">MA</state>    <street>Address 1</street>      <city>Test City</city>      <state_county>25013</state_county>      <state_fullname>Massachusetts</state_fullname>      <country iso3=""USA"">US</country>      <country_fullname>United States</country_fullname>      <postalcode>01001</postalcode>  </address>  home:<phone type=""home"">7777777777</phone>   home:<phone type=""home"" />   home:<phone type=""home"" />   <email>test@demo.com</email> </contact><experience><employment_status_cd>1</employment_status_cd></experience><education><school id=""2""><completiondate days=""733530"" iso8601=""2009-05-01"">05/2009</completiondate><completion_juliandate>733530</completion_juliandate><degree level=""16"">Bachelor of Engineering (BEng or BE)</degree><institution>University</institution><major code=""1502"">Mechanical Engineer</major><address><state abbrev=""MA"">MA</state><state_fullname>Massachusetts</state_fullname><country iso3=""USA"">US</country><country_fullname>United States</country_fullname></address></school><norm_edu_level_cd>24</norm_edu_level_cd><school_status_cd>5</school_status_cd></education><statements><personal><dob days=""723798"" iso8601=""1982-09-08"">09/08/1982</dob><preferences>  <sal>5.00</sal>   <salary_unit_cd>1</salary_unit_cd> <resume_searchable>-1</resume_searchable><email_flag>-1</email_flag><postal_flag>0</postal_flag><phone_pri_flag>0</phone_pri_flag><phone_sec_flag>0</phone_sec_flag><fax_flag>0</fax_flag><shift><work_week>1</work_week><work_type>1</work_type><work_over_time>0</work_over_time><shift_first_flag>-1</shift_first_flag><shift_rotating_flag>0</shift_rotating_flag><shift_second_flag>0</shift_second_flag><shift_split_flag>0</shift_split_flag><shift_third_flag>0</shift_third_flag><shift_varies_flag>0</shift_varies_flag></shift><desired_states><desired_state><code>IA</code><value>Iowa</value></desired_state><desired_state><code>KY</code><value>Kentucky</value></desired_state><desired_state><code>MA</code><value>Massachusetts</value></desired_state></desired_states></preferences><userinfo><username>test@demo.com</username><ssn_not_provided_flag>-1</ssn_not_provided_flag><customerregistrationid /><seeker_status_cd>1</seeker_status_cd><job_seeker_flag>1</job_seeker_flag></userinfo><license><driver_flag>1</driver_flag><driver_state>MA</driver_state><state_fullname>Massachusetts</state_fullname><driver_class>5</driver_class><driver_class_text>Motorcycle</driver_class_text><drv_pass_flag>-1</drv_pass_flag><drv_hazard_flag>0</drv_hazard_flag><drv_tank_flag>0</drv_tank_flag><drv_tankhazard_flag>0</drv_tankhazard_flag><drv_double_flag>-1</drv_double_flag><drv_airbrake_flag>0</drv_airbrake_flag><drv_bus_flag>0</drv_bus_flag><drv_cycle_flag>-1</drv_cycle_flag></license><crc />  <sex>2</sex> <ethnic_heritages><ethnic_heritage><ethnic_id>3</ethnic_id><selection_flag>1</selection_flag></ethnic_heritage><ethnic_heritage><ethnic_id>5</ethnic_id><selection_flag>-1</selection_flag></ethnic_heritage></ethnic_heritages>  <citizen_flag>-1</citizen_flag> <migrant_flag>0</migrant_flag>  <disability_status>1</disability_status> <veteran><vet_flag>-1</vet_flag><vet_start_date>05/05/2001</vet_start_date><vet_end_date>12/12/2012</vet_end_date>					<campaign_vet_flag>-1</campaign_vet_flag>					<vet_disability_status>1</vet_disability_status>					<vet_tsm_type_cd>2</vet_tsm_type_cd><vet_era>4</vet_era>					<vet_discharge>Honorable</vet_discharge>					<branch_of_service>USARMY</branch_of_service><rank>Sergeant Major of the Army</rank><moc>1410</moc><unit_affiliation>Regiment 143 Southern Command.</unit_affiliation><past_five_years_veteran>0</past_five_years_veteran><mostext>AN/SQS-53D Sensor Subsystem Level II Technician/Operator</mostext><mosonetcrosswalk /></veteran></personal></statements><summary><summary>I have 3 years of experience, including as a Design Engineer.  Most recently, I have been working as a Design Engineer at Test Employer from January 2010 to March 2012.  My skills and experiences include: Materials Management.  I hold a Bachelor of Engineering (BEng or BE) degree in Mechanical Engineer from University.</summary></summary><skills><skills>Materials Management, Mechanical,english</skills><languages><languages_profiency>English</languages_profiency></languages><courses><certifications><certification><certificate>REGISTERED RECORDS ADMINISTRATOR</certificate><organization_name>BGt</organization_name><completion_date>05/2010</completion_date><completion_juliandate>733895</completion_juliandate><address><state abbrev=""MA"">MA</state><state_fullname>Massachusetts</state_fullname><country iso3=""USA"">US</country><country_fullname>United States</country_fullname></address></certification></certifications></courses></skills><experience /><experience /><experience end=""734565"" start=""733775""><job end=""734565"" id=""1"" onet=""17-2141.00"" onetrank=""1"" pos=""1"" start=""733775""><display_in_resume>true</display_in_resume><daterange><start days=""733775"" iso8601=""2010-01-01"">01-2010</start><end currentjob=""1"" days=""734565"" iso8601=""2012-03-01"">03-2012</end></daterange><employer>Test Employer</employer><title onet=""17-2141.00"" onetrank=""1"">Design Engineer</title><onet>17-2141.00</onet><description>* Planned and designed mechanical systems and components. * Implemented conceptual designs. * Collaborated to implement operating procedures. * Assisted drafters in design tasks. * Evaluated and tested equipment. * Documented evaluations. * Generated bill of materials. * Resolved malfunctions. * Recommended design modifications. * Created engineering change orders. * Implemented operating procedures. * Organized tasks.</description><address><city>Test City</city><state abbrev=""MA"">MA</state><state_fullname>Massachusetts</state_fullname><country iso3=""USA"">US</country><country_fullname>United States</country_fullname></address><salary_unit_cd>6</salary_unit_cd><sal>0.00</sal></job></experience></resume> </ResDoc>";
			const string postingXml =
				@"<JobDoc>
  <special>
    <cf001>1</cf001>
    <cf002>734894</cf002>
    <cf003>735076</cf003>
    <cf004>0</cf004>
    <cf005>0</cf005>
    <cf006>1</cf006>
    <cf007>FT0004784121</cf007>
    <cf008>11101100</cf008>
    <cf009>0</cf009>
    <cf010>0</cf010>
    <cf011>1</cf011>
    <cf012>7</cf012>
    <cf013></cf013>
    <cf014>Test</cf014>
    <cf015>0</cf015>
    <cf016>0</cf016>
    <cf017>0</cf017>
    <cf018>0</cf018>
    <cf019>0</cf019>
    <cf020>0</cf020>
    <cf021>0</cf021>
    <cf022>0</cf022>
    <cf023>0</cf023>
    <cf024>211101100,30000Test</cf024>
    <cf025></cf025>
    <mode>update</mode>
    <originid>7</originid>
    <job_status_cd>1</job_status_cd>
    <office_id></office_id>
    <jobtitle>Senior Adults Director</jobtitle>
    <jobemployer>Test Company 01 Ltd A</jobemployer>
    <foreignpostingid>FT0004784121</foreignpostingid>
    <jobtype>4</jobtype>
    <JOBTYPE_CD>4</JOBTYPE_CD>
  <green>0</green><cf026>0</cf026></special>
  <posting>
    <contact>
      <company>Test Company 01 Ltd A</company>
      <person_title>Mr</person_title>
      <person>DevClientEmp ClientEmployee</person>
      <address>
        <street></street>
        <street></street>
        <city>Testerville</city>
        <state>ID</state>
        <county>Franklin</county>
        <postalcode>00000</postalcode>
        <country>US</country>
      </address>
      <email>client@Employee.com</email>
      <joburl></joburl>
      <phone></phone>
      <phone></phone>
      <phone_ext></phone_ext>
      <phone_ext></phone_ext>
      <fax></fax>
    </contact>
    <duties>
      <title>Senior Adults Director</title>
      <onettitle>Chief Executives</onettitle>* Provide overall direction and leadership in  private sector organization.
* Determine company policy
* Report to the board of directors
* Plan operational activities
* Direct operational activities at the highest level of management
</duties>
    <background>
      <experience>14</experience>
    </background>
    <benefits>
      <salary>123.00-3434.00 $ Weekly</salary>
    </benefits>
    <qualifications>
      <required>
        <certification>No License, No certificate</certification>
        <training></training>
      </required>
    </qualifications>
    <skills>
      <languages></languages>
    </skills>
    <jobinfo>
      <educationlevel>2</educationlevel>
      <positioncount>21</positioncount>
    </jobinfo>
  <qualifications><required><education><degree>High School Diploma</degree></education></required></qualifications></posting>
  <clientjobdata />
</JobDoc>";

			#endregion

			var repository = new LensRepository(RuntimeContext);

			// Act
			var result = repository.Match(resumeXml, postingXml);

			// Assert
			Assert.IsNotNull(result);
		}

		[TestCase(LensTransportMethod.Tcp, TestName = "TagWithHtml_WhenPassedValidRequest_ReturnsHtml - MS Lens")]
		[TestCase(LensTransportMethod.Http, TestName = "TagWithHtml_WhenPassedValidRequest_ReturnsHtml - HA Lens")]
		public void TagWithHtml_WhenPassedValidRequest_ReturnsHtml(LensTransportMethod transport)
		{
			// Arrange
			SetUpLensServices(transport);

			const string resume = "Resume for example candidate.";
			var encoding = new ASCIIEncoding();
			var bytes = encoding.GetBytes(resume);

			var repository = new LensRepository(RuntimeContext);
			var html = String.Empty;

			// Act
			var result = repository.TagWithHtml(bytes, "txt", DocumentType.Resume, out html);

			// Assert
			Assert.IsNotNull(result);
			Assert.IsNotNull(html);
		}

		[TestCase(LensTransportMethod.Tcp, TestName = "CanonPostingWithJobMine_WhenPassedValidRequest_ReturnsString - MS Lens")]
		[TestCase(LensTransportMethod.Http, TestName = "CanonPostingWithJobMine_WhenPassedValidRequest_ReturnsString - HA Lens")]
		public void CanonPostingWithJobMine_WhenPassedValidRequest_ReturnsString(LensTransportMethod transport)
		{
			// Arrange
			SetUpLensServices(transport);
			var jobDoc = MockPostingXml.GetJobDocFromXml();

			var repository = new LensRepository(RuntimeContext);

			// Act
			var result = repository.CanonPostingWithJobMine(jobDoc);

			// Assert
			Assert.IsNotNull(result);	
		}

		[TestCase(LensTransportMethod.Tcp, TestName = "UnregisterResume_WhenPassedValidRequest - MS Lens")]
		[TestCase(LensTransportMethod.Http, TestName = "UnregisterResume_WhenPassedValidRequest - HA Lens")]
		public void UnregisterResume_WhenPassedValidRequest(LensTransportMethod transport)
		{
			// Arrange
			SetUpLensServices(transport);

			var repository = new LensRepository(RuntimeContext);

			// Create a random Id that should not be registered in the Lens Instance
			var resumeId = Guid.NewGuid().ToString("N");

			// Act
			repository.UnregisterResume(new List<string> { resumeId });

			// Assert
		}

		[TestCase(LensTransportMethod.Tcp, TestName = "RegisterResume_WhenPassedValidRequest - MS Lens")]
		[TestCase(LensTransportMethod.Http, TestName = "RegisterResume_WhenPassedValidRequest - HA Lens")]
		public void RegisterResume_WhenPassedValidRequest(LensTransportMethod transport)
		{
			// Arrange
			SetUpLensServices(transport);

			var repository = new LensRepository(RuntimeContext);

			// Create a random Id that should not be registered in the Lens Instance
			var resumeId = Guid.NewGuid().ToString("N");

			// Act
			repository.RegisterResume(resumeId, MockResumeXml, DateTime.Now, DateTime.Now, 1, SchoolStatus.Prospective, null, null);


			// clear up after yourself
			repository.UnregisterResume(new List<string> { resumeId });

			// Assert
		}

		[TestCase(LensTransportMethod.Tcp, TestName = "RegisterResume_With_Language_Proficiencies_WhenPassedValidRequest - MS Lens")]
		[TestCase(LensTransportMethod.Http, TestName = "RegisterResume_With_Language_Proficiencies_WhenPassedValidRequest - HA Lens")]
		public void RegisterResume_With_Language_Proficiencies_WhenPassedValidRequest(LensTransportMethod transport)
		{
			// Arrange
			SetUpLensServices(transport);

			var repository = new LensRepository(RuntimeContext);

			// Create a random Id that should not be registered in the Lens Instance
			var resumeId = Guid.NewGuid().ToString("N");

			// Act
			repository.RegisterResume(resumeId, MockResumeWithLanguageProficienciesXml, DateTime.Now, DateTime.Now, 1, SchoolStatus.Prospective, null, null);


			// clear up after yourself
			repository.UnregisterResume(new List<string> { resumeId });

			// Assert
		}

		[TestCase(LensTransportMethod.Tcp, TestName = "RegisterPosting_WhenPassedValidRequest - MS Lens")]
		[TestCase(LensTransportMethod.Http, TestName = "RegisterPosting_WhenPassedValidRequest - HA Lens")]
		public void RegisterPosting_WhenPassedValidRequest(LensTransportMethod transport)
		{
			// Arrange
			SetUpLensServices(transport);

			var repository = new LensRepository(RuntimeContext);

			// Create a random Id that should not be registered in the Lens Instance
			var postingId = Guid.NewGuid().ToString("N");

			// Act
      var postingXml = XDocument.Parse(MockPostingXml);
			repository.RegisterPosting(postingId, 10, postingXml, MockPostingDataElementsXml, MockCulture, new List<int> { 299 });

			// clear up after yourself
			repository.UnregisterPosting(postingId);

			// Assert
		}

		[TestCase(LensTransportMethod.Tcp, TestName = "RegisterPosting_WhenPassedValidRequestButNoDataElements - MS Lens")]
		[TestCase(LensTransportMethod.Http, TestName = "RegisterPosting_WhenPassedValidRequestButNoDataElements - HA Lens")]
		public void RegisterPosting_WhenPassedValidRequestButNoDataElements(LensTransportMethod transport)
		{
			// Arrange
			SetUpLensServices(transport);

			var repository = new LensRepository(RuntimeContext);

			// Create a random Id that should not be registered in the Lens Instance
			var postingId = Guid.NewGuid().ToString("N");

			// Act
      var postingXml = XDocument.Parse(MockPostingXml);
			repository.RegisterPosting(postingId, 10, postingXml, "<DataElementsRollup></DataElementsRollup>", MockCulture, new List<int>());

			// clear up after yourself
			repository.UnregisterPosting(postingId);

			// Assert
		}

		[TestCase(LensTransportMethod.Tcp, TestName = "UnregisterPosting_WhenPassedValidRequest - MS Lens")]
		[TestCase(LensTransportMethod.Http, TestName = "UnregisterPosting_WhenPassedValidRequest - HA Lens")]
		public void UnregisterPosting_WhenPassedValidRequest(LensTransportMethod transport)
		{
			// Arrange
			SetUpLensServices(transport);

			var repository = new LensRepository(RuntimeContext);

			// Create a random Id that should not be registered in the Lens Instance
			var postingId = Guid.NewGuid().ToString("N");

			// Act
			repository.UnregisterPosting(postingId);

			// Assert
		}

		[TestCase(LensTransportMethod.Tcp, TestName = "ConvertBinaryData_WhenPassedValidRequest_ReturnsData - MS Lens")]
		[TestCase(LensTransportMethod.Http, TestName = "ConvertBinaryData_WhenPassedValidRequest_ReturnsData - HA Lens")]
		public void ConvertBinaryData_WhenPassedValidRequest_ReturnsData(LensTransportMethod transport)
		{
			// Arrange
			SetUpLensServices(transport);

			const string fileName = @"App_Data\\SampleFiles\\resume.doc";

			byte[] fileBytes;

			using (var binaryReader = new BinaryReader(new FileStream(fileName, FileMode.Open, FileAccess.Read)))
			{
				var byteLength = new FileInfo(fileName).Length;
				fileBytes = binaryReader.ReadBytes((Int32)byteLength);
			}

			var repository = new LensRepository(RuntimeContext);

			// Act
			var result = repository.CovertBinaryData(fileBytes, "doc");

			// Assert
			result.ShouldNotBeNullOrEmpty();
		}

		[TestCase(LensTransportMethod.Tcp, TestName = "GetOnetCodes_WithJobDescription_ReturnsOnetCodes - MS Lens")]
		[TestCase(LensTransportMethod.Http, TestName = "GetOnetCodes_WithJobDescription_ReturnsOnetCodes - HA Lens")]
		public void GetOnetCodes_WithJobDescription_ReturnsOnetCodes(LensTransportMethod transport)
		{
			// Arrange
			SetUpLensServices(transport);

			var repository = new LensRepository(RuntimeContext);

			// Act
			var results = repository.GetOnetCodes("Public Relations and Fundraising Managers");

			// Assert
			results.ShouldNotBeNull();
			results.Count.ShouldBeGreater(0);
		}

		[TestCase(LensTransportMethod.Tcp, TestName = "IsLicensed_ReturnsTrue - MS Lens")]
		[TestCase(LensTransportMethod.Http, TestName = "IsLicensed_ReturnsTrue - HA Lens")]
		public void IsLicensed_ReturnsTrue(LensTransportMethod transport)
		{
			// Arrange
			SetUpLensServices(transport);

			var repository = new LensRepository(RuntimeContext);

			// Act
			var result = repository.IsLicensed();

			// Assert
			result.ShouldNotBeNull();
			result.ShouldBeTrue();
		}

		[TestCase(LensTransportMethod.Tcp, TestName = "Clarify_WithValidRequest_ReturnsKeywords - MS Lens")]
		[TestCase(LensTransportMethod.Http, TestName = "Clarify_WithValidRequest_ReturnsKeywords - HA Lens")]
		public void Clarify_WithValidRequest_ReturnsKeywords(LensTransportMethod transport)
		{
			// Arrange
			SetUpLensServices(transport);

			var repository = new LensRepository(RuntimeContext);

			// Act
			var results = repository.Clarify("Public Relations and Fundraising Managers", 0, 10, "keywords");

			// Assert
			results.ShouldNotBeNull();
			results.Count.ShouldBeGreater(0);
		}

		[TestCase(LensTransportMethod.Tcp, TestName = "Tag_WithValidRequest_ReturnsTaggedDocument - MS Lens")]
		[TestCase(LensTransportMethod.Http, TestName = "Tag_WithValidRequest_ReturnsTaggedDocument- HA Lens")]
		public void Tag_WithValidRequest_ReturnsTaggedDocument(LensTransportMethod transport)
		{
			// Arrange
			SetUpLensServices(transport);

			var repository = new LensRepository(RuntimeContext);

			// Act
			var result = repository.Tag("Public Relations and Fundraising Managers", DocumentType.Posting);

			// Assert
			result.ShouldNotBeNullOrEmpty();
		}

		[TestCase(LensTransportMethod.Tcp, TestName = "Canon_WithValidRequest_ReturnsCanonedDocument - MS Lens")]
		[TestCase(LensTransportMethod.Http, TestName = "Canon_WithValidRequest_ReturnsCanonedDocument - HA Lens")]
		public void Canon_WithValidRequest_ReturnsCanonedDocument(LensTransportMethod transport)
		{
			// Arrange
			SetUpLensServices(transport);

			var repository = new LensRepository(RuntimeContext);

			// Act
			var result = repository.Canon(MockResumeXml);

			// Assert
			result.ShouldNotBeNullOrEmpty();
		}

		[TestCase(LensTransportMethod.Tcp, TestName = "ParseResume_WithValidRequest_ReturnsParsedResume - MS Lens")]
		[TestCase(LensTransportMethod.Http, TestName = "ParseResume_WithValidRequest_ReturnsParsedResume - HA Lens")]
		public void ParseResume_WithValidRequest_ReturnsParsedResume(LensTransportMethod transport)
		{
			// Arrange
			SetUpLensServices(transport);

			const string fileName = @"App_Data\\SampleFiles\\resume.doc";

			byte[] fileBytes;

			using (var binaryReader = new BinaryReader(new FileStream(fileName, FileMode.Open, FileAccess.Read)))
			{
				var byteLength = new FileInfo(fileName).Length;
				fileBytes = binaryReader.ReadBytes((Int32)byteLength);
			}

			var repository = new LensRepository(RuntimeContext);

			// Act
			var result = repository.ParseResume(fileBytes, "doc");

			// Assert
			result.ShouldNotBeNullOrEmpty();
		}

		[TestCase(LensTransportMethod.Tcp, TestName = "SearchResumes_WithValidCriteria_ReturnsResumes - MS Lens")]
		[TestCase(LensTransportMethod.Http, TestName = "SearchResumes_WithValidCriteria_ReturnsResumes - HA Lens")]
		public void SearchResumes_WithValidCriteria_ReturnsResumes(LensTransportMethod transport)
		{
			// Arrange
			SetUpLensServices(transport);

			var criteria = new CandidateSearchCriteria
			{
				SearchType = CandidateSearchTypes.ByKeywords,
				MinimumDocumentCount = 5,
				KeywordCriteria = new CandidateSearchKeywordCriteria {Keywords = "Test"}
			};

			var repository = new LensRepository(RuntimeContext);

			// Act
			var results = repository.SearchResumes(criteria, "en-GB");

			// Assert
			results.ShouldNotBeNull();
			results.Count.ShouldBeGreater(0);
		}

		[TestCase(LensTransportMethod.Tcp, TestName = "SearchPostings_WithValidCriteria_ReturnsResumes - MS Lens")]
		[TestCase(LensTransportMethod.Http, TestName = "SearchPostings_WithValidCriteria_ReturnsResumes - HA Lens")]
		public void SearchPostings_WithValidCriteria_ReturnsResumes(LensTransportMethod transport)
		{
			// Arrange
			SetUpLensServices(transport);

			var criteria = new SearchCriteria
			{
				SearchType = PostingSearchTypes.Postings,
				RequiredResultCriteria = new RequiredResultCriteria {MaximumDocumentCount = 5},
				KeywordCriteria = new KeywordCriteria { KeywordText = "Test" }
			};

			var repository = new LensRepository(RuntimeContext);

			// Act
			var results = repository.SearchPostings(criteria, "en-GB", false);

			// Assert
			results.ShouldNotBeNull();
			results.Count.ShouldBeGreater(0);
		}

		[TestCase(LensTransportMethod.Tcp, TestName = "ExecuteJobMineCommand_WhenPassedValidCommand_ReturnsResult - MS Lens")]
		[TestCase(LensTransportMethod.Http, TestName = "ExecuteJobMineCommand_WhenPassedValidCommand_ReturnsResult - HA Lens")]
		public void ExecuteJobMineCommand_WhenPassedValidCommand_ReturnsResult(LensTransportMethod transport)
		{
			// Arrange
			SetUpLensServices(transport);

			const string command = "<?xml version='1.0' encoding='iso-8859-1'?><bgtcmd><canon><JobDoc><posting><employer>CONTROLLER</employer><title>DIRECTOR OF ACCOUNTING</title><duties><description><skill>CONTROLLER</skill>, DPR Construction, a commercial building contractor specializing in technically demanding projects in the microelectronics, biopharmaceutical, health care. Responsibilities included overseeing all facets of <skill>accounting</skill> through <skill>Financial Statements</skill>.  Member of the Management Team, directly supervised staff and oversaw the monthly <skill>AIA</skill> billings, collections, and subsequent payments to subcontractors for as many as 40 jobs at one time.  Oversaw <skill>human resources</skill> and semi-monthly payroll. Responsible for banking relations, <skill>cash management</skill>, budgeting, leases, property taxes, and orchestrating the annual audit/review.  Established, implemented and documented all <skill>accounting</skill> procedures. Directly communicated with President, VPs, Project Managers, and Superintendents on a regular basis. Became well versed in American Contractor and <skill>Prolog</skill>.</description></duties></posting><special><CityName></CityName><StateCode></StateCode><ZipCode></ZipCode><CountryName></CountryName><META_LOCATION></META_LOCATION><META_Position_Type></META_Position_Type><JobTitle><title>DIRECTOR OF ACCOUNTING</title></JobTitle><companyname><employer>CONTROLLER</employer></companyname></special></JobDoc></canon></bgtcmd>";

			var repository = new LensRepository(RuntimeContext);
			
			// Act
			var result = repository.ExecuteJobMineCommand(command);

			// Assert
			result.ShouldNotBeNullOrEmpty();
		}

		[TearDown]
		public override void TearDown()
		{
			base.TearDown();

			var appSettings = MockAppSettings as FakeAppSettings;
			if (appSettings == null) return;
			
			appSettings.SetDefaultLensService();
		}

		public enum LensTransportMethod
		{
			Tcp,
			Http
		}

		private void SetUpLensServices(LensTransportMethod transport)
		{
			switch (transport)
			{
				case LensTransportMethod.Tcp:
					SetUpTcpLensServices();
					break;
				case LensTransportMethod.Http:
					SetUpHttpLensServices();
					break;
			}
		}

		private void SetUpTcpLensServices()
		{
			((FakeAppSettings)RuntimeContext.AppSettings).LensServices = new List<LensService>
			{
				new LensService
		    {
			    Host = "DEVAPPSVR002",
			    PortNumber = 2003,
			    Timeout = 600,
			    Vendor = "careerjobs",
			    CharacterSet = "iso-8859-1",
			    ServiceType = LensServiceTypes.Posting,
			    Description = "Career posting repository",
			    ReadOnly = false,
			    CustomFilters = new CustomFilterList
			    {
				    new CustomFilter {Key = Constants.CustomFilterKeys.Status, Tag = "cf001"},
				    new CustomFilter {Key = Constants.CustomFilterKeys.OpenDate, Tag = "cf002"},
				    new CustomFilter {Key = Constants.CustomFilterKeys.ClosingOn, Tag = "cf003"},
				    new CustomFilter {Key = Constants.CustomFilterKeys.MinimumSalary, Tag = "cf004"},
				    new CustomFilter {Key = Constants.CustomFilterKeys.MaximumSalary, Tag = "cf005"},
				    new CustomFilter {Key = Constants.CustomFilterKeys.ExternalPostingId, Tag = "cf007"},
				    new CustomFilter {Key = Constants.CustomFilterKeys.Occupation, Tag = "cf008"},
				    new CustomFilter {Key = Constants.CustomFilterKeys.StateCode, Tag = "cf009"},
				    new CustomFilter {Key = Constants.CustomFilterKeys.PostingId, Tag = "cf010"},
				    new CustomFilter {Key = Constants.CustomFilterKeys.Origin, Tag = "cf012"},
				    new CustomFilter {Key = Constants.CustomFilterKeys.Sic2, Tag = "cf013"},
				    new CustomFilter {Key = Constants.CustomFilterKeys.Industry, Tag = "cf014"},
				    new CustomFilter {Key = Constants.CustomFilterKeys.EducationLevel, Tag = "cf016"},
				    new CustomFilter {Key = Constants.CustomFilterKeys.GreenJob, Tag = "cf022"},
				    new CustomFilter {Key = Constants.CustomFilterKeys.Sector, Tag = "cf024"},
				    new CustomFilter {Key = Constants.CustomFilterKeys.CountyCode, Tag = "cf025"},
				    new CustomFilter {Key = Constants.CustomFilterKeys.Internship, Tag = "cf026"},
				    new CustomFilter {Key = Constants.CustomFilterKeys.ProgramsOfStudy, Tag = "cf027"},
				    new CustomFilter {Key = Constants.CustomFilterKeys.AnnualPay, Tag = "cf028"},
				    new CustomFilter {Key = Constants.CustomFilterKeys.Salary, Tag = "cf029"},
				    new CustomFilter {Key = Constants.CustomFilterKeys.Msa, Tag = "cf030"},
				    new CustomFilter {Key = Constants.CustomFilterKeys.ROnet, Tag = "cf031"},
				    new CustomFilter {Key = Constants.CustomFilterKeys.UnpaidPosition, Tag = "cf033"},
				    new CustomFilter {Key = Constants.CustomFilterKeys.InternshipCategories, Tag = "cf034"},
				    new CustomFilter {Key = Constants.CustomFilterKeys.RequiredProgramOfStudyId, Tag = "cf036"}
			    }
		    },
		    new LensService
		    {
			    Host = "DEVAPPSVR002",
			    PortNumber = 2002,
			    Timeout = 600,
			    Vendor = "careermartha",
			    CharacterSet = "iso-8859-1",
			    ServiceType = LensServiceTypes.Resume,
			    Description = "Talent/Assist resume repository",
			    ReadOnly = false,
			    CustomFilters = new CustomFilterList
			    {
				    new CustomFilter {Key = Constants.CustomFilterKeys.Status, Tag = "cf001"},
				    new CustomFilter {Key = Constants.CustomFilterKeys.RegisteredOn, Tag = "cf002"},
				    new CustomFilter {Key = Constants.CustomFilterKeys.ModifiedOn, Tag = "cf003"},
				    new CustomFilter {Key = Constants.CustomFilterKeys.DateOfBirth, Tag = "cf004"},
				    new CustomFilter {Key = Constants.CustomFilterKeys.CitizenFlag, Tag = "cf005"},
				    new CustomFilter {Key = Constants.CustomFilterKeys.VeteranStatus, Tag = "cf007"},
				    new CustomFilter {Key = Constants.CustomFilterKeys.Languages, Tag = "cf008"},
				    new CustomFilter {Key = Constants.CustomFilterKeys.DrivingLicenceClass, Tag = "cf009"},
				    new CustomFilter {Key = Constants.CustomFilterKeys.DrivingLicenceEndorsement, Tag = "cf010"},
				    new CustomFilter {Key = Constants.CustomFilterKeys.Certificates, Tag = "cf011"},
				    new CustomFilter {Key = Constants.CustomFilterKeys.WorkOvertime, Tag = "cf012"},
				    new CustomFilter {Key = Constants.CustomFilterKeys.ShiftType, Tag = "cf013"},
				    new CustomFilter {Key = Constants.CustomFilterKeys.WorkType, Tag = "cf014"},
				    new CustomFilter {Key = Constants.CustomFilterKeys.WorkWeek, Tag = "cf015"},
				    new CustomFilter {Key = Constants.CustomFilterKeys.EducationLevel, Tag = "cf016"},
				    new CustomFilter {Key = Constants.CustomFilterKeys.JobseekerId, Tag = "cf017"},
				    new CustomFilter {Key = Constants.CustomFilterKeys.Relocation, Tag = "cf019"},
				    new CustomFilter {Key = Constants.CustomFilterKeys.JobTypes, Tag = "cf020"},
				    new CustomFilter {Key = Constants.CustomFilterKeys.GPA, Tag = "cf021"},
				    new CustomFilter {Key = Constants.CustomFilterKeys.MonthsExperience, Tag = "cf022"},
				    new CustomFilter {Key = Constants.CustomFilterKeys.EnrollmentStatus, Tag = "cf023"},
				    new CustomFilter {Key = Constants.CustomFilterKeys.ProgramsOfStudy, Tag = "cf024"},
				    new CustomFilter {Key = Constants.CustomFilterKeys.DegreeCompletionDate, Tag = "cf025"},
				    new CustomFilter {Key = Constants.CustomFilterKeys.ResumeIndustries, Tag = "cf026"}
			    }
		    },
		    new LensService
		    {
			    Host = "DEVAPPSVR002",
			    PortNumber = 2002,
			    Timeout = 600,
			    Vendor = "career",
			    CharacterSet = "iso-8859-1",
			    ServiceType = LensServiceTypes.Generic,
			    Description = "Generic Lens",
			    ReadOnly = true
		    },
		    new LensService
		    {
			    Host = "DEVAPPSVR002",
			    PortNumber = 2003,
			    Timeout = 600,
			    Vendor = "career",
			    CharacterSet = "iso-8859-1",
			    ServiceType = LensServiceTypes.JobMine,
			    Description = "Job Mine",
			    ReadOnly = true
		    }
            // new LensService
            //{
            //    Host = "BOSAPPSVR006.usdev.burninglass.com",
            //    PortNumber = 3102,
            //    Timeout = 600,
            //    Vendor = "career",
            //    CharacterSet = "iso-8859-1",
            //    ServiceType = LensServiceTypes.JobMine,
            //    Description = "Job Mine",
            //    ReadOnly = true
            //}
	    };
		}

		private void SetUpHttpLensServices()
		{
			((FakeAppSettings) RuntimeContext.AppSettings).LensServices = new List<LensService>
			{
				new LensService
				{
					Url = "http://devappsvr001:9000",
					ConsumerKey = "c8028fae-4887-4e91-8fa5-9655adae6ec1",
					Timeout = 60000,
					Vendor = "careerjobs",
					CharacterSet = "iso-8859-1",
					ServiceType = LensServiceTypes.Posting,
					Description = "Career posting repository",
					ReadOnly = false,
					CustomFilters = new CustomFilterList
					{
						new CustomFilter {Key = Constants.CustomFilterKeys.Status, Tag = "cf001"},
						new CustomFilter {Key = Constants.CustomFilterKeys.OpenDate, Tag = "cf002"},
						new CustomFilter {Key = Constants.CustomFilterKeys.ClosingOn, Tag = "cf003"},
						new CustomFilter {Key = Constants.CustomFilterKeys.MinimumSalary, Tag = "cf004"},
						new CustomFilter {Key = Constants.CustomFilterKeys.MaximumSalary, Tag = "cf005"},
						new CustomFilter {Key = Constants.CustomFilterKeys.ExternalPostingId, Tag = "cf007"},
						new CustomFilter {Key = Constants.CustomFilterKeys.Occupation, Tag = "cf008"},
						new CustomFilter {Key = Constants.CustomFilterKeys.StateCode, Tag = "cf009"},
						new CustomFilter {Key = Constants.CustomFilterKeys.PostingId, Tag = "cf010"},
						new CustomFilter {Key = Constants.CustomFilterKeys.Origin, Tag = "cf012"},
						new CustomFilter {Key = Constants.CustomFilterKeys.Sic2, Tag = "cf013"},
						new CustomFilter {Key = Constants.CustomFilterKeys.Industry, Tag = "cf014"},
						new CustomFilter {Key = Constants.CustomFilterKeys.EducationLevel, Tag = "cf016"},
						new CustomFilter {Key = Constants.CustomFilterKeys.GreenJob, Tag = "cf022"},
						new CustomFilter {Key = Constants.CustomFilterKeys.Sector, Tag = "cf024"},
						new CustomFilter {Key = Constants.CustomFilterKeys.CountyCode, Tag = "cf025"},
						new CustomFilter {Key = Constants.CustomFilterKeys.Internship, Tag = "cf026"},
						new CustomFilter {Key = Constants.CustomFilterKeys.ProgramsOfStudy, Tag = "cf027"},
						new CustomFilter {Key = Constants.CustomFilterKeys.AnnualPay, Tag = "cf028"},
						new CustomFilter {Key = Constants.CustomFilterKeys.Salary, Tag = "cf029"},
						new CustomFilter {Key = Constants.CustomFilterKeys.Msa, Tag = "cf030"},
						new CustomFilter {Key = Constants.CustomFilterKeys.ROnet, Tag = "cf031"},
						new CustomFilter {Key = Constants.CustomFilterKeys.UnpaidPosition, Tag = "cf033"},
						new CustomFilter {Key = Constants.CustomFilterKeys.InternshipCategories, Tag = "cf034"},
						new CustomFilter {Key = Constants.CustomFilterKeys.RequiredProgramOfStudyId, Tag = "cf036"}
					}
				},
				new LensService
				{
					Url = "http://devappsvr001:9000",
					ConsumerKey = "c8028fae-4887-4e91-8fa5-9655adae6ec1",
					Timeout = 60000,
					Vendor = "careermartha",
					CharacterSet = "iso-8859-1",
					ServiceType = LensServiceTypes.Resume,
					Description = "Talent/Assist resume repository",
					ReadOnly = false,
					CustomFilters = new CustomFilterList
					{
						new CustomFilter {Key = Constants.CustomFilterKeys.Status, Tag = "cf001"},
						new CustomFilter {Key = Constants.CustomFilterKeys.RegisteredOn, Tag = "cf002"},
						new CustomFilter {Key = Constants.CustomFilterKeys.ModifiedOn, Tag = "cf003"},
						new CustomFilter {Key = Constants.CustomFilterKeys.DateOfBirth, Tag = "cf004"},
						new CustomFilter {Key = Constants.CustomFilterKeys.CitizenFlag, Tag = "cf005"},
						new CustomFilter {Key = Constants.CustomFilterKeys.VeteranStatus, Tag = "cf007"},
						new CustomFilter {Key = Constants.CustomFilterKeys.Languages, Tag = "cf008"},
						new CustomFilter {Key = Constants.CustomFilterKeys.DrivingLicenceClass, Tag = "cf009"},
						new CustomFilter {Key = Constants.CustomFilterKeys.DrivingLicenceEndorsement, Tag = "cf010"},
						new CustomFilter {Key = Constants.CustomFilterKeys.Certificates, Tag = "cf011"},
						new CustomFilter {Key = Constants.CustomFilterKeys.WorkOvertime, Tag = "cf012"},
						new CustomFilter {Key = Constants.CustomFilterKeys.ShiftType, Tag = "cf013"},
						new CustomFilter {Key = Constants.CustomFilterKeys.WorkType, Tag = "cf014"},
						new CustomFilter {Key = Constants.CustomFilterKeys.WorkWeek, Tag = "cf015"},
						new CustomFilter {Key = Constants.CustomFilterKeys.EducationLevel, Tag = "cf016"},
						new CustomFilter {Key = Constants.CustomFilterKeys.JobseekerId, Tag = "cf017"},
						new CustomFilter {Key = Constants.CustomFilterKeys.Relocation, Tag = "cf019"},
						new CustomFilter {Key = Constants.CustomFilterKeys.JobTypes, Tag = "cf020"},
						new CustomFilter {Key = Constants.CustomFilterKeys.GPA, Tag = "cf021"},
						new CustomFilter {Key = Constants.CustomFilterKeys.MonthsExperience, Tag = "cf022"},
						new CustomFilter {Key = Constants.CustomFilterKeys.EnrollmentStatus, Tag = "cf023"},
						new CustomFilter {Key = Constants.CustomFilterKeys.ProgramsOfStudy, Tag = "cf024"},
						new CustomFilter {Key = Constants.CustomFilterKeys.DegreeCompletionDate, Tag = "cf025"},
						new CustomFilter {Key = Constants.CustomFilterKeys.ResumeIndustries, Tag = "cf026"}
					}
				},
				new LensService
				{
					Url = "http://devappsvr001:9000",
					ConsumerKey = "c8028fae-4887-4e91-8fa5-9655adae6ec1",
					Timeout = 60000,
					Vendor = "career",
					CharacterSet = "iso-8859-1",
					ServiceType = LensServiceTypes.Generic,
					Description = "Generic Lens",
					ReadOnly = true
				},
				new LensService
				{
					Url = "http://devappsvr001:9000",
					ConsumerKey = "c8028fae-4887-4e91-8fa5-9655adae6ec1",
					Timeout = 60000,
					Vendor = "career",
					CharacterSet = "iso-8859-1",
					ServiceType = LensServiceTypes.JobMine,
					Description = "Job Mine",
					ReadOnly = true
				}
			};
		}
	}
}
