﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using Focus.Core.Models.Career;
using Focus.Services;
using Focus.Services.Repositories.Lens;
using Focus.UnitTests.Core;
using Framework.Testing.NUnit;
using NUnit.Framework;

#endregion



namespace Focus.UnitTests.Repositories
{
	[TestFixture]
	public class LensCommandBuilderTests : TestFixtureBase 
	{
		[Test]
		public void BuildPostingSearchCommand_WhereSearchAllVendorsIsFalse_ReturnsCommandWithVendorSpecified()
		{
			// Arrange
			var lensService = RuntimeContext.AppSettings.LensServices[0];
			var commandBuilder = new LensCommandBuilder(RuntimeContext, lensService);
			var criteria = new SearchCriteria {KeywordCriteria = new KeywordCriteria("Test")};
			
			// Act
			var command = commandBuilder.BuildPostingSearchCommand(null, null, criteria, RuntimeContext.CurrentRequest.UserContext.Culture, null, null, null, null, null);

			// Assert
			command.ShouldNotBeNullOrEmpty();
			var result = command.Contains("vendor=\"careerjobs\"");

			result.ShouldBeTrue();
		}

		[Test]
		public void BuildPostingSearchCommand_WhereSearchAllVendorsIsTrue_ReturnsCommandWithNoVendorSpecified()
		{
			// Arrange
			var lensService = RuntimeContext.AppSettings.LensServices[0];
			lensService.SearchAllVendors = true;
			var commandBuilder = new LensCommandBuilder(RuntimeContext, lensService);
			var criteria = new SearchCriteria { KeywordCriteria = new KeywordCriteria("Test") };

			// Act
      var command = commandBuilder.BuildPostingSearchCommand(null, null, criteria, RuntimeContext.CurrentRequest.UserContext.Culture, null, null, null, null, null);

			// Assert
			command.ShouldNotBeNullOrEmpty();
			var result = command.Contains("vendor=\"\"");

			result.ShouldBeTrue();
		}

		[Test]
		public void BuildPostingSearchCommand_WithOneNaics4_ReturnsCommandWithCorrectCutomFilter()
		{
			// Arrange
			var lensService = RuntimeContext.AppSettings.LensServices[0];
			var commandBuilder = new LensCommandBuilder(RuntimeContext, lensService);
			var criteria = new SearchCriteria { KeywordCriteria = new KeywordCriteria("Test") };
			var industryCodes = new List<string> {"1111"};
			
			// Act
			var command = commandBuilder.BuildPostingSearchCommand(null, null, criteria, RuntimeContext.CurrentRequest.UserContext.Culture, null, null, null, industryCodes, null);

			// Assert
			command.ShouldNotBeNullOrEmpty();
			var result = command.Contains("cf014 array=\"1111\" unknown=\"exclude\"");

			result.ShouldBeTrue();
		}

		[Test]
		public void BuildPostingSearchCommand_WithNaics4List_ReturnsCommandWithCorrectCutomFilter()
		{
			// Arrange
			var lensService = RuntimeContext.AppSettings.LensServices[0];
			var commandBuilder = new LensCommandBuilder(RuntimeContext, lensService);
			var criteria = new SearchCriteria { KeywordCriteria = new KeywordCriteria("Test") };
			var industryCodes = new List<string> { "1111","1124","1136","1178" };

			// Act
			var command = commandBuilder.BuildPostingSearchCommand(null, null, criteria, RuntimeContext.CurrentRequest.UserContext.Culture, null, null, null, industryCodes, null);

			// Assert
			command.ShouldNotBeNullOrEmpty();
			var result = command.Contains("cf014 array=\"1111 1124 1136 1178\" unknown=\"exclude\"");

			result.ShouldBeTrue();
		}

		[Test]
		public void BuildPostingSearchCommand_WithOneNaics2_ReturnsCommandWithCorrectCutomFilter()
		{
			// Arrange
			var lensService = RuntimeContext.AppSettings.LensServices[0];
			var commandBuilder = new LensCommandBuilder(RuntimeContext, lensService);
			var criteria = new SearchCriteria { KeywordCriteria = new KeywordCriteria("Test") };
			var industryCodes = new List<string> { "11" };

			// Act
			var command = commandBuilder.BuildPostingSearchCommand(null, null, criteria, RuntimeContext.CurrentRequest.UserContext.Culture, null, null, null, industryCodes, null);

			// Assert
			command.ShouldNotBeNullOrEmpty();
			var result = command.Contains("cf014 min=\"1100\" max=\"1199\" unknown=\"exclude\"");

			result.ShouldBeTrue();
		}

		[Test]
		public void BuildPostingSearchCommand_WithOneNaics3_ReturnsCommandWithCorrectCutomFilter()
		{
			// Arrange
			var lensService = RuntimeContext.AppSettings.LensServices[0];
			var commandBuilder = new LensCommandBuilder(RuntimeContext, lensService);
			var criteria = new SearchCriteria { KeywordCriteria = new KeywordCriteria("Test") };
			var industryCodes = new List<string> { "118" };

			// Act
			var command = commandBuilder.BuildPostingSearchCommand(null, null, criteria, RuntimeContext.CurrentRequest.UserContext.Culture, null, null, null, industryCodes, null);

			// Assert
			command.ShouldNotBeNullOrEmpty();
			var result = command.Contains("cf014 min=\"1180\" max=\"1189\" unknown=\"exclude\"");

			result.ShouldBeTrue();
		}

		[Test]
		public void BuildPostingSearchCommand_WithNaics2Range_ReturnsCommandWithCorrectCutomFilter()
		{
			// Arrange
			var lensService = RuntimeContext.AppSettings.LensServices[0];
			var commandBuilder = new LensCommandBuilder(RuntimeContext, lensService);
			var criteria = new SearchCriteria { KeywordCriteria = new KeywordCriteria("Test") };
			var industryCodes = new List<string> { "48-49" };

			// Act
			var command = commandBuilder.BuildPostingSearchCommand(null, null, criteria, RuntimeContext.CurrentRequest.UserContext.Culture, null, null, null, industryCodes, null);

			// Assert
			command.ShouldNotBeNullOrEmpty();
			var result = command.Contains("cf014 min=\"4800\" max=\"4999\" unknown=\"exclude\"");

			result.ShouldBeTrue();
		}

		[Test]
		public void BuildPostingSearchCommand_WithNaics3Range_ReturnsCommandWithCorrectCutomFilter()
		{
			// Arrange
			var lensService = RuntimeContext.AppSettings.LensServices[0];
			var commandBuilder = new LensCommandBuilder(RuntimeContext, lensService);
			var criteria = new SearchCriteria { KeywordCriteria = new KeywordCriteria("Test") };
			var industryCodes = new List<string> { "481-488" };

			// Act
			var command = commandBuilder.BuildPostingSearchCommand(null, null, criteria, RuntimeContext.CurrentRequest.UserContext.Culture, null, null, null, industryCodes, null);

			// Assert
			command.ShouldNotBeNullOrEmpty();
			var result = command.Contains("cf014 min=\"4810\" max=\"4889\" unknown=\"exclude\"");

			result.ShouldBeTrue();
		}

		[Test]
		public void BuildPostingSearchCommand_WithNaics4Range_ReturnsCommandWithCorrectCutomFilter()
		{
			// Arrange
			var lensService = RuntimeContext.AppSettings.LensServices[0];
			var commandBuilder = new LensCommandBuilder(RuntimeContext, lensService);
			var criteria = new SearchCriteria { KeywordCriteria = new KeywordCriteria("Test") };
			var industryCodes = new List<string> { "4815-4883" };

			// Act
			var command = commandBuilder.BuildPostingSearchCommand(null, null, criteria, RuntimeContext.CurrentRequest.UserContext.Culture, null, null, null, industryCodes, null);

			// Assert
			command.ShouldNotBeNullOrEmpty();
			var result = command.Contains("cf014 min=\"4815\" max=\"4883\" unknown=\"exclude\"");

			result.ShouldBeTrue();
		}

		[Test]
		public void BuildPostingSearchCommand_WithNaicsRange_ReturnsCommandWithCorrectCutomFilter()
		{
			// Arrange
			var lensService = RuntimeContext.AppSettings.LensServices[0];
			var commandBuilder = new LensCommandBuilder(RuntimeContext, lensService);
			var criteria = new SearchCriteria { KeywordCriteria = new KeywordCriteria("Test") };
			var industryCodes = new List<string> { "48-488" };

			// Act
			var command = commandBuilder.BuildPostingSearchCommand(null, null, criteria, RuntimeContext.CurrentRequest.UserContext.Culture, null, null, null, industryCodes, null);

			// Assert
			command.ShouldNotBeNullOrEmpty();
			var result = command.Contains("cf014 min=\"4800\" max=\"4889\" unknown=\"exclude\"");

			result.ShouldBeTrue();
		}

		[Test]
		[ExpectedException]
		public void BuildPostingSearchCommand_WithNaicsListAndNaics6_ThrowsException()
		{
			// Arrange
			var lensService = RuntimeContext.AppSettings.LensServices[0];
			var commandBuilder = new LensCommandBuilder(RuntimeContext, lensService);
			var criteria = new SearchCriteria { KeywordCriteria = new KeywordCriteria("Test") };
			var industryCodes = new List<string> { "1111", "1124", "113656", "1178" };

			// Act
			var command = commandBuilder.BuildPostingSearchCommand(null, null, criteria, RuntimeContext.CurrentRequest.UserContext.Culture, null, null, null, industryCodes, null);

			// Assert
			command.ShouldBeNullOrEmpty();
		}

		[Test]
		[ExpectedException]
		public void BuildPostingSearchCommand_WithNaics2List_ThrowsException()
		{
			// Arrange
			var lensService = RuntimeContext.AppSettings.LensServices[0];
			var commandBuilder = new LensCommandBuilder(RuntimeContext, lensService);
			var criteria = new SearchCriteria { KeywordCriteria = new KeywordCriteria("Test") };
			var industryCodes = new List<string> { "11", "14" };

			// Act
			var command = commandBuilder.BuildPostingSearchCommand(null, null, criteria, RuntimeContext.CurrentRequest.UserContext.Culture, null, null, null, industryCodes, null);

			// Assert
			command.ShouldBeNullOrEmpty();
		}

		[Test]
		[ExpectedException]
		public void BuildPostingSearchCommand_WithNaics1_ThrowsException()
		{
			// Arrange
			var lensService = RuntimeContext.AppSettings.LensServices[0];
			var commandBuilder = new LensCommandBuilder(RuntimeContext, lensService);
			var criteria = new SearchCriteria { KeywordCriteria = new KeywordCriteria("Test") };
			var industryCodes = new List<string> { "1" };

			// Act
			var command = commandBuilder.BuildPostingSearchCommand(null, null, criteria, RuntimeContext.CurrentRequest.UserContext.Culture, null, null, null, industryCodes, null);

			// Assert
			command.ShouldBeNullOrEmpty();
		}

		[Test]
		[ExpectedException]
		public void BuildPostingSearchCommand_WithTwoNaicsRanges_ThrowsException()
		{
			// Arrange
			var lensService = RuntimeContext.AppSettings.LensServices[0];
			var commandBuilder = new LensCommandBuilder(RuntimeContext, lensService);
			var criteria = new SearchCriteria { KeywordCriteria = new KeywordCriteria("Test") };
			var industryCodes = new List<string> { "44-45", "48-49" };

			// Act
			var command = commandBuilder.BuildPostingSearchCommand(null, null, criteria, RuntimeContext.CurrentRequest.UserContext.Culture, null, null, null, industryCodes, null);

			// Assert
			command.ShouldBeNullOrEmpty();
		}

		[Test]
		[ExpectedException]
		public void BuildPostingSearchCommand_WithNaicsRangesAndNaics6_ThrowsException()
		{
			// Arrange
			var lensService = RuntimeContext.AppSettings.LensServices[0];
			var commandBuilder = new LensCommandBuilder(RuntimeContext, lensService);
			var criteria = new SearchCriteria {KeywordCriteria = new KeywordCriteria("Test")};
			var industryCodes = new List<string> {"44-451111"};

			// Act
			var command = commandBuilder.BuildPostingSearchCommand(null, null, criteria, RuntimeContext.CurrentRequest.UserContext.Culture, null, null, null, industryCodes, null);

			// Assert
			command.ShouldBeNullOrEmpty();
		}

		[Test]
		[ExpectedException]
		public void BuildPostingSearchCommand_WithNaicsRangesAndNaics1_ThrowsException()
		{
			// Arrange
			var lensService = RuntimeContext.AppSettings.LensServices[0];
			var commandBuilder = new LensCommandBuilder(RuntimeContext, lensService);
			var criteria = new SearchCriteria { KeywordCriteria = new KeywordCriteria("Test") };
			var industryCodes = new List<string> { "4-5" };

			// Act
			var command = commandBuilder.BuildPostingSearchCommand(null, null, criteria, RuntimeContext.CurrentRequest.UserContext.Culture, null, null, null, industryCodes, null);

			// Assert
			command.ShouldBeNullOrEmpty();
		}

		[Test]
		public void BuildPostingSearchCommand_WhereKeywordsContainPermittedSpecialChars_ReturnsCommandWithSpecialChars()
		{
			// Arrange
			var lensService = RuntimeContext.AppSettings.LensServices[0];
			var commandBuilder = new LensCommandBuilder(RuntimeContext, lensService);
			var criteria = new SearchCriteria { KeywordCriteria = new KeywordCriteria(@"Test -Test ""-Test""") };

			// Act
			var command = commandBuilder.BuildPostingSearchCommand(null, null, criteria, RuntimeContext.CurrentRequest.UserContext.Culture, null, null, null, null, null);

			// Assert
			command.ShouldNotBeNullOrEmpty();
			var result = command.Contains(@"""-Test""");

			result.ShouldBeTrue();
		}

		[Test]
		public void BuildPostingSearchCommand_WhereKeywordsContainAmbersandAndPermittedSpecialChars_ReturnsCommandWithoutSpecialChars()
		{
			// Searching on "AT&T"

			// Arrange
			var lensService = RuntimeContext.AppSettings.LensServices[0];
			var commandBuilder = new LensCommandBuilder(RuntimeContext, lensService);
			var criteria = new SearchCriteria { KeywordCriteria = new KeywordCriteria(@"""AT&T""") };

			// Act
			var command = commandBuilder.BuildPostingSearchCommand(null, null, criteria, RuntimeContext.CurrentRequest.UserContext.Culture, null, null, null, null, null);

			// Assert
			command.ShouldNotBeNullOrEmpty();
			var result = command.Contains(@"""AT T""");

			result.ShouldBeTrue();
		}

		[Test]
		public void BuildPostingSearchCommand_WhereKeywordsContainDisallowedSpecialCharsAndPermittedSpecialChars_ReturnsCommandWithoutSpecialChars()
		{
			// Searching on "AT&T"

			// Arrange
			var lensService = RuntimeContext.AppSettings.LensServices[0];
			var commandBuilder = new LensCommandBuilder(RuntimeContext, lensService);
			var criteria = new SearchCriteria { KeywordCriteria = new KeywordCriteria(@"""AT&T"" dollar$") };

			// Act
			var command = commandBuilder.BuildPostingSearchCommand(null, null, criteria, RuntimeContext.CurrentRequest.UserContext.Culture, null, null, null, null, null);

			// Assert
			command.ShouldNotBeNullOrEmpty();
			command.Contains(@"""AT T""").ShouldBeTrue();
			command.Contains(@"dollar$").ShouldBeFalse();
			command.Contains(@"dollar").ShouldBeTrue();
		}
	}
}
