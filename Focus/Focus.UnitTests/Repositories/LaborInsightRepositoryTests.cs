﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Focus.Core;
using Focus.Core.Settings.OAuth;
using Focus.Services.Repositories;
using Focus.Services.Repositories.LaborInsight;
using Focus.Services.Repositories.LaborInsight.Criteria;
using Focus.UnitTests.Core;
using NUnit.Framework;

namespace Focus.UnitTests.Repositories
{
	[TestFixture]
	public class LaborInsightRepositoryTests: TestFixtureBase
	{
		private LaborInsightRepository repo { get; set; }

		[SetUp]
		public override void SetUp()
		{
			base.SetUp();

			((FakeAppSettings)RuntimeContext.AppSettings).OutgoingOAuthSettings = new List<OAuthSettings>
			{
				new OAuthSettings
				{
					Client = OAuthClient.LaborInsight,
					ConsumerKey = "BGT",
					ConsumerSecret = "086718ED10AC48B685F93535208AE903",
					Token = "Insight",
					TokenSecret = "32611633362D4A76A3C6038E71BFE90C",
					EndPoint = "http://sandbox.api.burning-glass.com/v202/"
				}
			};

			repo = new LaborInsightRepository(RuntimeContext);
		}

	  [Test]
	  [Explicit]
	  public void Test_GetJobReport()
	  {
	    var jobReportCriteria = new JobReportCriteria
	                              {
	                                Criteria = new JobReportBaseCriteria
	                                             {
	                                               FromDate = new DateTime(2010, 12, 1),
	                                               Geography = "US",
	                                               GroupBy = "State",
	                                               IncludeTotalClassifiedPostings = true,
	                                               IncludeTotalUnclassifiedPostings = true,
	                                               Limit = 10,
	                                               OffSet = 0,
	                                               ToDate = DateTime.Today,
	                                             },
	                                QueryParameters = new List<QueryStringParams>
	                                                    {
	                                                      new QueryStringParams
	                                                        {
	                                                          Filter = FilterName.Onet,
	                                                          Value =
	                                                            "113133"
	                                                        },
	                                                      new QueryStringParams
	                                                        {
	                                                          Filter = FilterName.BgtOccFamily,
	                                                          Value = "107180"
	                                                        }
	                                                    }
	                              };

	    repo.GetJobReport(jobReportCriteria);
	  }
	}
}
