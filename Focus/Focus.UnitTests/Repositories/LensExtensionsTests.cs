﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Xml.Linq;
using System.Xml.XPath;
using Focus.Services.Repositories.Lens;
using Focus.UnitTests.Core;
using Framework.Testing.NUnit;
using NUnit.Framework;

#endregion

namespace Focus.UnitTests.Repositories
{
	[TestFixture]
	public class LensExtensionsTests
	{
		[Test]
		public void GetMatchingProgramsOfStudy_WhenPassedValidDataElements_ReturnsValue()
		{

			#region Set up test data

			var dataElementsXml = @"<DataElementsRollup version='5.1.21 JobMine v3.2.0.0'>
<JobID/>
<SourceID/>
<CanonJobTitle/>
<CleanJobTitle>Public Health and Research Management Assistant</CleanJobTitle>
<StandardTitle>Senior Director</StandardTitle>
<CanonEmployer/>
<EmployerParent/>
<EmployerTicker/>
<LocationSpecificInformation>
  <city/>
  <state/>
  <county/>
  <country/>
  <zipcode/>
  <geocode/>
  <msa/>
  <lma/>
  <phoneareacode/>
  <divisioncode/>
</LocationSpecificInformation>
<Telephone/>
<ConsolidatedInferredNAICS>33</ConsolidatedInferredNAICS>
<GreenOnet>0</GreenOnet>
<GreenOnetType/>
<InternshipFlag>false</InternshipFlag>
<Salary/>
<CanonSalary/>
<HealthCareBenefits>true</HealthCareBenefits>
<LivingWageBenefit/>
<ONETSalaryNationalAnnual/>
<ONETSalaryNationalHourly/>
<ONETSalaryLocalAnnual/>
<ONETSalaryLocalHourly/>
<CanonOtherDegrees/>
<OtherDegreeLevels/>
<Major/>
<MajorCode/>
<StdMajor/>
<CIPCode/>
<YearsOfExperience>1 years 2 months</YearsOfExperience>
<CanonYearsOfExperience>
<min>12</min>
<max/>
<level>low</level>
<canonlevel>0-1</canonlevel>
</CanonYearsOfExperience>
<Skills>leadership|organization</Skills>
<CanonSkills>
<canonskill name='Leadership' skill-cluster='Common Skills: Business Environment Skills'/>
<canonskill name='Organizational Skills' skill-cluster='Common Skills: Business Environment Skills'/>
</CanonSkills>
<CanonSkillClusters>Common Skills: Business Environment Skills</CanonSkillClusters>
<SkillClusterIndex>
<skillcluster name='Common Skills: Business Environment Skills'>Leadership|Organizational Skills</skillcluster>
</SkillClusterIndex>
<GreenIndustry>0</GreenIndustry>
<CanonGreenIndustry/>
<GreenCompany>0</GreenCompany>
<GreenBLSNAICS>0</GreenBLSNAICS>
<NumberOfOpenings>21</NumberOfOpenings>
<CanonNumberOfOpenings>21</CanonNumberOfOpenings>
<JobType>4</JobType>
<CanonJobType/>
<JobDate/>
<Intermediary/>
<CanonIntermediary/>
<RootTitle>director</RootTitle>
<EMail>client@Employee.com</EMail>
<JobUrl/>
<JobDomain/>
<JobTitleRule/>
<EmployerRule/>
<JobReferenceID/>
<OpeningDate/>
<CanonOpeningDate/>
<ClosingDate>7/25/2013</ClosingDate>
<CanonClosingDate>2013-07-25</CanonClosingDate>
<Blacklist>0</Blacklist>
<ConsolidatedONET>21-2021.00</ConsolidatedONET>
<ConsolidatedONETRank>4</ConsolidatedONETRank>
<BGTOcc priority='724'>21-2021.00</BGTOcc>
<ConsolidatedDegree>Associate's</ConsolidatedDegree>
<ConsolidatedDegreeLevels>14</ConsolidatedDegreeLevels>
<CanonMinimumDegree/>
<CanonMaximumDegree>Associate's</CanonMaximumDegree>
<MinDegreeLevel/>
<MaxDegreeLevel>14</MaxDegreeLevel>
<CanonPreferredDegrees/>
<PreferredDegreeLevels/>
<CanonRequiredDegrees/>
<RequiredDegreeLevels/>
<Certification/>
<CanonCertification/></DataElementsRollup>";

			var dataElementsDoc = (XDocument.Parse(dataElementsXml));

			var  dataElementsElement = dataElementsDoc.XPathSelectElement("//DataElementsRollup");

			#endregion

			var programs = dataElementsElement.GetMappedProgramOfStudy("BEN");

			programs.ShouldNotBeNull();
			programs.Count.ShouldBeGreater(0);
		}

		[Test]
		public void GetMatchingProgramsOfStudy_WhenPassedValidJobTilte_ReturnsValue()
		{

			#region Set up test data

			var dataElementsXml = @"<DataElementsRollup version='5.1.21 JobMine v3.2.0.0'>
<CleanJobTitle>Public Health and Research Management Assistant</CleanJobTitle>
</DataElementsRollup>";

			var dataElementsDoc = (XDocument.Parse(dataElementsXml));

			var dataElementsElement = dataElementsDoc.XPathSelectElement("//DataElementsRollup");

			#endregion

			var programs = dataElementsElement.GetMappedProgramOfStudy("BEN");

			programs.ShouldNotBeNull();
			programs.Count.ShouldBeGreater(0);
		}

		[Test]
		public void GetMatchingProgramsOfStudy_WhenPassedValidCanonSkills_ReturnsValue()
		{

			#region Set up test data

			var dataElementsXml = @"<DataElementsRollup version='5.1.21 JobMine v3.2.0.0'>
<CanonSkills>
<canonskill name='Leadership' skill-cluster='Common Skills: Business Environment Skills'/>
<canonskill name='Community Health Improvement' skill-cluster='Common Skills: Business Environment Skills'/>
</CanonSkills>
</DataElementsRollup>";

			var dataElementsDoc = (XDocument.Parse(dataElementsXml));

			var dataElementsElement = dataElementsDoc.XPathSelectElement("//DataElementsRollup");

			#endregion

			var programs = dataElementsElement.GetMappedProgramOfStudy("BEN");

			programs.ShouldNotBeNull();
			programs.Count.ShouldBeGreater(0);
		}

		[Test]
		public void GetMatchingProgramsOfStudy_WhenPassedValidSkillClusters_ReturnsValue()
		{

			#region Set up test data

			var dataElementsXml = @"<DataElementsRollup version='5.1.21 JobMine v3.2.0.0'>
<CleanJobTitle>Public Health and Research Policy Management Assistant</CleanJobTitle>
<CanonSkillClusters>Common Skills: Business Environment Skills|Health: Health Skills</CanonSkillClusters>
</DataElementsRollup>";

			var dataElementsDoc = (XDocument.Parse(dataElementsXml));

			var dataElementsElement = dataElementsDoc.XPathSelectElement("//DataElementsRollup");

			#endregion

			var programs = dataElementsElement.GetMappedProgramOfStudy("BEN");

			programs.ShouldNotBeNull();
			programs.Count.ShouldBeGreater(0);
		}

		[Test]
		public void GetMatchingProgramsOfStudy_WhenPassedValidBgtOcc_ReturnsValue()
		{

			#region Set up test data

			var dataElementsXml = @"<DataElementsRollup version='5.1.21 JobMine v3.2.0.0'>
<BGTOcc priority='724'>11-9161.00</BGTOcc>
</DataElementsRollup>";

			var dataElementsDoc = (XDocument.Parse(dataElementsXml));

			var dataElementsElement = dataElementsDoc.XPathSelectElement("//DataElementsRollup");

			#endregion

			var programs = dataElementsElement.GetMappedProgramOfStudy("BEN");

			programs.ShouldNotBeNull();
			programs.Count.ShouldBeGreater(0);
		}

		[Test]
		public void GetMatchingProgramsOfStudy_WhenPassedValidConsolidatedDegreeLevel_ReturnsValue()
		{

			#region Set up test data

			var dataElementsXml = @"<DataElementsRollup version='5.1.21 JobMine v3.2.0.0'>
<BGTOcc priority='724'>29-1141.00</BGTOcc>
<ConsolidatedDegreeLevels>14|18</ConsolidatedDegreeLevels>
</DataElementsRollup>";

			var dataElementsDoc = (XDocument.Parse(dataElementsXml));

			var dataElementsElement = dataElementsDoc.XPathSelectElement("//DataElementsRollup");

			#endregion

			var programs = dataElementsElement.GetMappedProgramOfStudy("BEN");

			programs.ShouldNotBeNull();
			programs.Count.ShouldBeGreater(0);
		}

		[Test]
		public void GetMatchingProgramsOfStudy_WhenPassedValidStandardMajor_ReturnsValue()
		{

			#region Set up test data

			var dataElementsXml = @"<DataElementsRollup version='5.1.21 JobMine v3.2.0.0'>
<StdMajor>Nursing Administration</StdMajor>
</DataElementsRollup>";

			var dataElementsDoc = (XDocument.Parse(dataElementsXml));

			var dataElementsElement = dataElementsDoc.XPathSelectElement("//DataElementsRollup");

			#endregion

			var programs = dataElementsElement.GetMappedProgramOfStudy("BEN");

			programs.ShouldNotBeNull();
			programs.Count.ShouldBeGreater(0);
		}

		[Test]
		public void GetMatchingProgramsOfStudy_WhenPassedValidJobUrl_ReturnsValue()
		{

			#region Set up test data

			var dataElementsXml = @"<DataElementsRollup version='5.1.21 JobMine v3.2.0.0'>
<JobUrl>http://www.apha.org/job/12345</JobUrl>
</DataElementsRollup>";

			var dataElementsDoc = (XDocument.Parse(dataElementsXml));

			var dataElementsElement = dataElementsDoc.XPathSelectElement("//DataElementsRollup");

			#endregion

			var programs = dataElementsElement.GetMappedProgramOfStudy("BEN");

			programs.ShouldNotBeNull();
			programs.Count.ShouldBeGreater(0);
		}

		[Test]
		public void GetMatchingProgramsOfStudy_WhenPassedValidConsolidatedDegreeAndJobTitle_ReturnsValue()
		{

			#region Set up test data

			var dataElementsXml = @"<DataElementsRollup version='5.1.21 JobMine v3.2.0.0'>
<ConsolidatedDegree>Associate's|Master of Public Health</ConsolidatedDegree>
<CleanJobTitle>Public Health and Research Management Assistant</CleanJobTitle>
</DataElementsRollup>";

			var dataElementsDoc = (XDocument.Parse(dataElementsXml));

			var dataElementsElement = dataElementsDoc.XPathSelectElement("//DataElementsRollup");

			#endregion

			var programs = dataElementsElement.GetMappedProgramOfStudy("BEN");

			programs.ShouldNotBeNull();
			programs.Count.ShouldBeGreater(0);
		}

		[Test]
		public void GetMatchingProgramsOfStudy_WhenPassedValidConsolidatedDegreeAndOnet_ReturnsValue()
		{

			#region Set up test data

			var dataElementsXml = @"<DataElementsRollup version='5.1.21 JobMine v3.2.0.0'>
<ConsolidatedDegree>Associate's|Master of Public Health</ConsolidatedDegree>
<ConsolidatedONET>21-2021.00</ConsolidatedONET>
</DataElementsRollup>";

			var dataElementsDoc = (XDocument.Parse(dataElementsXml));

			var dataElementsElement = dataElementsDoc.XPathSelectElement("//DataElementsRollup");

			#endregion

			var programs = dataElementsElement.GetMappedProgramOfStudy("BEN");

			programs.ShouldNotBeNull();
			programs.Count.ShouldBeGreater(0);
		}

		[Test]
		public void GetMatchingProgramsOfStudy_WhenPassedValidNaicsJobTitleAndOnet_ReturnsValue()
		{

			#region Set up test data

			var dataElementsXml = @"<DataElementsRollup version='5.1.21 JobMine v3.2.0.0'>
<CleanJobTitle>Program Manager</CleanJobTitle>
<ConsolidatedInferredNAICS>33;62133</ConsolidatedInferredNAICS>
<ConsolidatedONET>21-2021.00</ConsolidatedONET>
</DataElementsRollup>";

			var dataElementsDoc = (XDocument.Parse(dataElementsXml));

			var dataElementsElement = dataElementsDoc.XPathSelectElement("//DataElementsRollup");

			#endregion

			var programs = dataElementsElement.GetMappedProgramOfStudy("BEN");

			programs.ShouldNotBeNull();
			programs.Count.ShouldBeGreater(0);
		}

		[Test]
		public void GetMatchingProgramsOfStudy_WhenPassedValidOnet_ReturnsValue()
		{

			#region Set up test data

			var dataElementsXml = @"<DataElementsRollup version='5.1.21 JobMine v3.2.0.0'>
<ConsolidatedONET>43-6014.00</ConsolidatedONET>
</DataElementsRollup>";

			var dataElementsDoc = (XDocument.Parse(dataElementsXml));

			var dataElementsElement = dataElementsDoc.XPathSelectElement("//DataElementsRollup");

			#endregion

			var programs = dataElementsElement.GetMappedProgramOfStudy("VC");

			programs.ShouldNotBeNull();
			programs.Count.ShouldBeGreater(0);
		}

		[Test]
		public void GetMatchingProgramsOfStudy_WhenPassedValidEmployerAndJobTitle_ReturnsValue()
		{

			#region Set up test data

			var dataElementsXml = @"<DataElementsRollup version='5.1.21 JobMine v3.2.0.0'>
<CanonEmployer>Buckley Country Club</CanonEmployer>
<CleanJobTitle>Golf Course Green Keeper</CleanJobTitle>
</DataElementsRollup>";

			var dataElementsDoc = (XDocument.Parse(dataElementsXml));

			var dataElementsElement = dataElementsDoc.XPathSelectElement("//DataElementsRollup");

			#endregion

			var programs = dataElementsElement.GetMappedProgramOfStudy("GOLF");


			programs.ShouldNotBeNull();
			programs.Count.ShouldBeGreater(0);
		}

		[Test]
		public void GetMatchingProgramsOfStudy_WhenPassedValidCanonSkillsAndJobTitle_ReturnsValue()
		{

			#region Set up test data

			var dataElementsXml = @"<DataElementsRollup version='5.1.21 JobMine v3.2.0.0'>
<CanonSkills>
<canonskill name='Interior Design' skill-cluster='Interior Design'/>
</CanonSkills>
<CleanJobTitle>Household Interior Consultant</CleanJobTitle>
</DataElementsRollup>";

			var dataElementsDoc = (XDocument.Parse(dataElementsXml));

			var dataElementsElement = dataElementsDoc.XPathSelectElement("//DataElementsRollup");

			#endregion

			var programs = dataElementsElement.GetMappedProgramOfStudy("RING");

			programs.ShouldNotBeNull();
			programs.Count.ShouldBeGreater(0);
		}

		[Test]
		public void GetJobDocFromXml_WhenPassedValidXml_ReturnsJobDoc()
		{
			// Arrange
			const string postingXml = MockData.MockPostingXml;

			// Act
			var jobDocXml = postingXml.GetJobDocFromXml();

			// Assert
			jobDocXml.ShouldNotBeNull();
			jobDocXml.Substring(0, 8).ShouldEqual("<JobDoc>");
			jobDocXml.Length.ShouldBeGreater(17);
		}

		[Test]
		public void GetJobDocFromXml_WhenPassedEmptyXml_ReturnsJobDoc()
		{
			// Arrange
			const string postingXml = "";

			// Act
			var jobDocXml = postingXml.GetJobDocFromXml();

			// Assert
			jobDocXml.ShouldNotBeNull();
			jobDocXml.ShouldEqual("<JobDoc></JobDoc>");
		}

		[Test]
		public void GetJobDocFromXml_WhenPassedInvalidXml_ReturnsJobDoc()
		{
			// Arrange
			const string postingXml = "Invalid Xml";

			// Act
			var jobDocXml = postingXml.GetJobDocFromXml();

			// Assert
			jobDocXml.ShouldNotBeNull();
			jobDocXml.ShouldEqual("<JobDoc></JobDoc>");
		}

		[Test]
		public void UpdateJobDocInTaggedPosting_WhenPassedValidXml_ReturnsUpdatedPostingXml()
		{
			// Arrange
			const string postingXml = MockData.MockPostingXml;
			const string jobDocXml = "<JobDoc></JobDoc>";

			// Act
			var result = postingXml.UpdateJobDocInTaggedPosting(jobDocXml);

			// Assert
			result.ShouldNotBeNullOrEmpty();
			result.ShouldNotEqual(postingXml);
			result.Contains(jobDocXml).ShouldBeTrue();
		}

		[Test]
		public void UpdateJobDocInTaggedPosting_WhenPassedEmptyTaggedPosting_ReturnsTaggedPosting()
		{
			// Arrange
			const string postingXml = "";
			const string jobDocXml = "<JobDoc></JobDoc>";

			// Act
			var result = postingXml.UpdateJobDocInTaggedPosting(jobDocXml);

			// Assert
			result.ShouldEqual(postingXml);
		}

		[Test]
		public void UpdateJobDocInTaggedPosting_WhenPassedEmptyJobDoc_ReturnsTaggedPosting()
		{
			// Arrange
			const string postingXml = MockData.MockPostingXml;
			const string jobDocXml = "";

			// Act
			var result = postingXml.UpdateJobDocInTaggedPosting(jobDocXml);

			// Assert
			result.ShouldEqual(postingXml);
		}
	}
}
