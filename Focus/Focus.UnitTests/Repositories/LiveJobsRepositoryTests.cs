﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using Focus.Core.Criteria.JobDistribution;
using Focus.Core.Criteria.SpideredEmployer;
using Focus.Core.Criteria.SpideredPosting;
using Focus.Services;
using Focus.Services.Repositories.LiveJobs;
using Focus.UnitTests.Core;
using Framework.Testing.NUnit;
using NUnit.Framework;

#endregion

namespace Focus.UnitTests.Repositories
{
	public class LiveJobsRepositoryTests
	{
		protected IRuntimeContext RuntimeContext;

		[SetUp]
		public void SetUp()
		{
			RuntimeContext = new TestRuntimeContext { AppSettings = new FakeAppSettings(), Repositories = new TestRepositories(), Helpers = new TestHelpers() };
		}

		[Test]
		[Explicit]
		public void GetEmployers_WhenPassedValidStateCriteria_ReturnsSuccess()
		{
			// Arrange
			var repository = new LiveJobsRepository(RuntimeContext);
			var states = new List<string>{"CA", "IO"};
			var criteria = new SpideredEmployerCriteria { StateCodes = states, PageIndex = 0, PageSize = 10, ListSize = 20 };

			//// Act 
			var response = repository.GetEmployers(criteria);

			// Assert
			Assert.IsNotNull(response);
			Assert.GreaterOrEqual(response.Count, 0);
		}

		[Test]
		[Explicit]
		public void GetEmployers_WhenPassedValidEmployerCriteria_ReturnsSuccess()
		{
			// Arrange
			var repository = new LiveJobsRepository(RuntimeContext);
			var criteria = new SpideredEmployerCriteria { EmployerName = "Nat", PageIndex = 0, PageSize = 10, ListSize = 20 };

			//// Act 
			var response = repository.GetEmployers(criteria);

			// Assert
			Assert.IsNotNull(response);
			Assert.GreaterOrEqual(response.Count, 0);
		}

		[Test]
		[Explicit]
		public void GetEmployers_WhenPassedAdvancedCriteria_ReturnsSuccess()
		{
			// Arrange
			var repository = new LiveJobsRepository(RuntimeContext);
			var criteria = new SpideredEmployerCriteria { PageIndex = 0, PageSize = 10, ListSize = 20 };

			//// Act 
			var response = repository.GetEmployers(criteria);

			// Assert
			Assert.IsNotNull(response);
			Assert.GreaterOrEqual(response.Count, 0);
		}


		[Test]
		[Explicit]
		public void GetEmployers_WhenPassePostingAgeCriteria_ReturnsSuccess()
		{
			// Arrange
			var repository = new LiveJobsRepository(RuntimeContext);
			var criteria = new SpideredEmployerCriteria { PostingAge = 7, PageIndex = 0, PageSize = 10, ListSize = 20 };

			//// Act 
			var response = repository.GetEmployers(criteria);

			// Assert
			Assert.IsNotNull(response);
			Assert.GreaterOrEqual(response.Count, 0);
		}

		[Test]
		[Explicit]
		public void GetPostings_WhenPassedValidNameCriteria_ReturnsSuccess()
		{
			// Arrange
			var repository = new LiveJobsRepository(RuntimeContext);
			var criteria = new SpideredPostingCriteria { EmployerName = "Pizza Hut", PageIndex = 0, PageSize = 25 };

			//// Act 
			var response = repository.GetPostings(criteria);

			// Assert
			Assert.IsNotNull(response); 
			Assert.GreaterOrEqual(response.Count, 0);
		}

		[Test]
		[Explicit]
		public void GetPostings_WhenPassedValidNameAndStateCriteria_ReturnsSuccess()
		{
			// Arrange
			var repository = new LiveJobsRepository(RuntimeContext);
			var states = new List<string> {"CA"};
			var criteria = new SpideredPostingCriteria { EmployerName = "Pizza Hut", StateCodes = states, PageIndex = 0, PageSize = 25 };

			//// Act 
			var response = repository.GetPostings(criteria);

			// Assert
			Assert.IsNotNull(response);
			Assert.GreaterOrEqual(response.Count, 0);
		}

		[Test]
		[Explicit]
		public void GetPostings_WhenPassedValidNameAndPostingAge_ReturnsSuccess()
		{
			// Arrange
			var repository = new LiveJobsRepository(RuntimeContext);
			var criteria = new SpideredPostingCriteria { EmployerName = "Pizza Hut", PostingAge = 30, PageIndex = 0, PageSize = 25 };

			//// Act 
			var response = repository.GetPostings(criteria);

			// Assert
			Assert.IsNotNull(response);
			Assert.GreaterOrEqual(response.Count, 0);
		}

		[Test]
		[Explicit]
		public void GetJobDistribution_WhenPassedValidActiveCategory_ReturnsSuccess()
		{
			// Arrange
			var repository = new LiveJobsRepository(RuntimeContext);
			var criteria = new JobDistributionCriteria { Category = "Active" };

			//// Act 
			var feeds = repository.GetJobDistribution(criteria);

			// Assert
			Assert.NotNull(feeds);
			Assert.GreaterOrEqual(feeds.Count, 0);
		}

		[Test]
		[Explicit]
		public void GetJobDistribution_WhenPassedValidTotalCategoryAndDate_ReturnsSuccess()
		{
			// Arrange
			var repository = new LiveJobsRepository(RuntimeContext);

			var date = "26/02/2015";
			var yesterday = Convert.ToDateTime(date);
			var startTs = new TimeSpan(00, 00, 00);
			var startYesterday = yesterday.Date + startTs;

			var epoch = (int)(startYesterday - new DateTime(1970, 1, 1)).TotalSeconds;

			var criteria = new JobDistributionCriteria { Category = "total", Date = epoch.ToString() };

			//// Act 
			var feeds = repository.GetJobDistribution(criteria);

			// Assert
			Assert.NotNull(feeds);
			Assert.GreaterOrEqual(feeds.Count, 0);
		}
	}
}
