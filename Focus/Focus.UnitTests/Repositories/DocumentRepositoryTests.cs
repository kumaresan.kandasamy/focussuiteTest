﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using Focus.Services.Repositories.Document;
using Focus.UnitTests.Core;
using Framework.Testing.NUnit;
using NUnit.Framework;

#endregion

namespace Focus.UnitTests.Repositories
{
	public class DocumentRepositoryTests : TestFixtureBase
	{
		[SetUp]
		public override void SetUp()
		{
			DoSetup(false);
		}

		[Test]
		[Ignore]
		public void GetPosting_WhenGivenValidDetails_ReturnsXml()
		{
			// Arrange
			var repository = new DocumentRepository(MockAppSettings.DocumentStoreServiceUrl);

			//// Act 
			var response = repository.GetDocumentText("careerjobs_71675BEACE274DF0ABE3B4768A3B9153");

			// Assert
			response.ShouldNotBeNullOrEmpty();
		}

		[Test]
		[Ignore]
		[ExpectedException]
		public void GetPosting_WhenGivenInvalidDetailsNew_ThrowsException()
		{
			// Arrange
			var repository = new DocumentRepository(MockAppSettings.DocumentStoreServiceUrl);

			//// Act 
			var response = repository.GetDocumentText("xcareerjobs_71675BEACE274DF0ABE3B4768A3B9153");

			// Assert
		}
	}
}
