﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using Focus.Core;
using Focus.Core.Messages;
using Focus.Data.Core.Entities;
using Mindscape.LightSpeed;

using Focus.Services.Helpers;

#endregion

namespace Focus.UnitTests.FakeHelpers
{
	public class TestLoggingHelper : ILoggingHelper
	{
		public ActionType GetActionType(ActionTypes actionType)
		{
			return new ActionType { Id = 1 };
		}

    public long LogAction(ActionTypes actionType, string entityName, long? entityId, long? entityIdAdditional01 = null, long? entityIdAdditional02 = null, string additionalDetails = null, bool commit = true, DateTime? actionedOn = null, long? overrideUserId = null, DateTime? overrideCreatedOn = null)
    {
	    return 0;
    }

		public List<long> LogActions(ActionTypes actionType, string entityName, long entityId, long[] entityIdAdditionals, bool commit = true)
		{
			return new List<long>{0};
		}

		public long LogAction(ActionTypes actionType, string entityName, long? entityId, bool commit = true)
		{
			return 0;
		}

		public long LogAction(ActionTypes actionType, Entity<long> entity, bool commit = true, long? overrideUserId = null, DateTime? actionedOn = null)
		{
			return 0;
		}

		public void LogStatusChange(string entityName, long entityId, long? originalStatus, long newStatus, long? overriderUserId = null)
		{
		}

		public void LogActionToIntegration(IServiceRequest request, ActionTypes actionType, long? actionId, long? entityId)
		{
		}
	}
}
