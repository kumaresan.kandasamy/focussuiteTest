﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;

using Focus.Core;
using Focus.Core.Views;
using Focus.Services.Core;

#endregion

namespace Focus.UnitTests.FakeHelpers
{
	public class TestLookupHelper : ILookupHelper
	{
		public List<LookupItemView> GetLookup(LookupTypes lookupType, long parentId = 0, string key = null, string prefix = null)
		{
			return new List<LookupItemView>();
		}

		public LookupItemView GetLookupById(LookupTypes lookupType, long id = 0)
		{
			return new LookupItemView();
		}

		public string GetLookupExternalId(LookupTypes lookupType, long id)
		{
			return String.Empty;
		}

		public string GetLookupText(LookupTypes lookupType, long id)
		{
			return String.Empty;
		}

		public string GetLookupText(LookupTypes lookupType, string key, string defaultText = "")
		{
			return String.Empty;
		}
	}
}
