﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using Focus.Services.Core;
using Focus.Services.Helpers;

#endregion

namespace Focus.UnitTests.FakeHelpers
{
	public class TestSessionHelper : ISessionHelper
	{
		public Guid CreateSession(long userId = 0)
		{
			return new Guid("00000000-2222-0000-0000-000000000000");
		}

    public void UpdateSession(Guid sessionId, long userId, bool checkUser = false)
		{
		}

		public bool ValidateSession(Guid sessionId, long userId)
		{
			return true;
		}

    public void SetSessionState<T>(Guid sessionId, string key, T value, IEnumerable<Type> knownTypes = null) 
		{
		}

    public T GetSessionState<T>(Guid sessionId, string key, T defaultValue = default(T), IEnumerable<Type> knownTypes = null) 
		{
			throw new NotImplementedException();
		}

    public void RemoveSessionState(Guid sessionId, string key)
    {
    }

    public void ClearSessionState(Guid sessionId)
    {
    }
	}
}
