﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.EmailTemplate;
using Focus.Core.Views;
using Focus.Services.Helpers;
using Focus.Core;
using Focus.Services;
using Framework.Core;

#endregion

namespace Focus.UnitTests.FakeHelpers
{
	public class TestEmailHelper :IEmailHelper
	{
		private EmailHelper RealEmailHelper { get; set; }
		private static readonly List<object> StoredFields = new List<object>();
		public List<MailMessage> SentEmails { get; set; }

		/// <summary>
		/// Initializes a new instance of the <see cref="EmailHelper"/> class.
		/// </summary>
		public TestEmailHelper()
		{
			SentEmails = new List<MailMessage>();
			RealEmailHelper = new EmailHelper();
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="EmailHelper"/> class.
		/// </summary>
		/// <param name="runtimeContext">The runtime context.</param>
		public TestEmailHelper(IRuntimeContext runtimeContext)
		{
			SentEmails = new List<MailMessage>();
			RealEmailHelper = new EmailHelper( runtimeContext );
		}

		public EmailTemplateView GetEmailTemplatePreview(EmailTemplateTypes emailTemplateType,EmailTemplateData templateValues)
		{
			return RealEmailHelper.GetEmailTemplatePreview(emailTemplateType, templateValues);
			;
		}

		public EmailTemplateView GetEmailTemplatePreview(EmailTemplateDto emailTemplate, EmailTemplateData templateValues)
		{
			return RealEmailHelper.GetEmailTemplatePreview(emailTemplate, templateValues);
		}

		public void SendEmailFromTemplate(EmailTemplateTypes type, EmailTemplateData data, string to, string cc, string bcc, bool sendAsync = false, bool isBodyHtml = true, byte[] attachment = null, string attachmentName = null, bool detectUrl = false)
		{
			RealEmailHelper.SendEmailFromTemplate(type, data, to, cc, bcc, sendAsync, isBodyHtml, attachment, attachmentName, detectUrl);
		}

        public void SendQueuedEmailFromTemplate(EmailTemplateTypes type, EmailTemplateData data, string to, string cc, string bcc, DateTime SendEmailOn, bool sendAsync = false, bool isBodyHtml = true, byte[] attachment = null, string attachmentName = null, bool detectUrl = false)
        {
            RealEmailHelper.SendQueuedEmailFromTemplate(type, data, to, cc, bcc, SendEmailOn, sendAsync, isBodyHtml, attachment, attachmentName, detectUrl);
        }

		public EmailTemplateDto GetEmailTemplate(EmailTemplateTypes emailTemplateType)
		{
			return RealEmailHelper.GetEmailTemplate( emailTemplateType );
		}

		public void SendEmail(MailMessage mailMessage, bool sendAsync = false, bool isBodyHtml = true)
		{
			RealEmailHelper.SendEmail(mailMessage, sendAsync, isBodyHtml);
			mailMessage.IsBodyHtml = isBodyHtml;
			SentEmails.Add( mailMessage );
		}

		public void SendEmail(string to, string cc, string bcc, string subject, string body, bool sendAsync = false,
			bool isBodyHtml = true, byte[] attachment = null, string attachmentName = null, bool detectUrl = false,
			string senderAddress = null, EmailFooterTypes footerType = EmailFooterTypes.None, string footerUrl = null)
		{
			RealEmailHelper.SendEmail(to, cc, bcc, subject, body, sendAsync, isBodyHtml, attachment, attachmentName, detectUrl, senderAddress, footerType, footerUrl);

			if (senderAddress.IsNotNull())
			{
				var msg = new MailMessage(senderAddress, to, subject, body);
				msg.IsBodyHtml = isBodyHtml;
				if (!cc.IsNullOrWhitespace())
				{
					msg.CC.Add(cc);
				}
				if (!bcc.IsNullOrWhitespace())
				{
					msg.CC.Add(bcc);
				}
				SentEmails.Add( msg );
			}
		}

        public void SendQueuedEmail(string to, string cc, string bcc, string subject, string body, DateTime SendEmailOn, bool sendAsync = false,
            bool isBodyHtml = true, byte[] attachment = null, string attachmentName = null, bool detectUrl = false,
            string senderAddress = null, EmailFooterTypes footerType = EmailFooterTypes.None, string footerUrl = null)
        {
            RealEmailHelper.SendQueuedEmail(to, cc, bcc, subject, body, SendEmailOn, sendAsync, isBodyHtml, attachment, attachmentName, detectUrl, senderAddress, footerType, footerUrl);

            if (senderAddress.IsNotNull())
            {
                var msg = new MailMessage(senderAddress, to, subject, body);
                msg.IsBodyHtml = isBodyHtml;
                if (!cc.IsNullOrWhitespace())
                {
                    msg.CC.Add(cc);
                }
                if (!bcc.IsNullOrWhitespace())
                {
                    msg.CC.Add(bcc);
                }
                SentEmails.Add(msg);
            }
        }
	}
}
