﻿using System.Collections.Generic;
using Focus.Core.Settings.Interfaces;
using Focus.Core.Views;
using Focus.Data.Configuration.Entities;
using Focus.Services.Core;

namespace Focus.UnitTests.FakeHelpers
{
	class TestConfigurationHelper : IConfigurationHelper
	{
		public IAppSettings AppSettings { get; private set; }
		public IAppSettings UncachedAppSettings { get; private set; }
		public IEnumerable<ConfigurationItemView> ConfigurationItems { get; private set; }
		public IEnumerable<ConfigurationItemView> ExternalConfigurationItems { get; private set; }
		public void RefreshAppSettings()
		{
		}

		public ConfigurationItem GetConfigurationItem(string configurationItemKey, string defaultValue = "")
		{
			return null;
		}
	}
}
