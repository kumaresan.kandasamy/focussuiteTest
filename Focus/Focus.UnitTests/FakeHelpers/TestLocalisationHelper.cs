﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using Focus.Common.Models;
using Focus.Services.Helpers;

#endregion

namespace Focus.UnitTests.FakeHelpers
{
	public class TestLocalisationHelper : ILocalisationHelper
	{
		public string Localise(string key)
		{
			return String.Empty;
		}

		public string Localise(string key, string defaultValue)
		{
			return defaultValue;
		}

		public string Localise(string key, string defaultValue, params object[] args)
		{
			return defaultValue;
		}

		public string Localise(long? id, string defaultValue, params object[] args)
		{
			return defaultValue;
		}

		public string GetCodeItemLocalisedValue(long codeItemId)
		{
			return String.Empty;
		}

		public string GetEnumLocalisedText(Enum enumValue, bool useAnnotation = false)
		{
			return String.Empty;
		}

		public string GetEnumLocalisedText(Enum enumValue, string defaultValue)
		{
			return defaultValue;
		}

		public LocalisationDictionary GetLocalisationDictionary()
		{
      return new LocalisationDictionary();
		}

    public LocalisationDictionary RefreshLocalisationDictionary()
		{
      return new LocalisationDictionary();
		}

		public string LocaliseConfiguablePlaceHolders(string source)
		{
			return source;
		}

    public LocalisationDictionaryEntry GetLocalisationItem(string key)
    {
      return new LocalisationDictionaryEntry();
    }
	}
}
