﻿using System.Collections.Generic;
using System.Linq;

using NUnit.Framework;

using Focus.Core;
using Focus.Services.Core.Censorship;

namespace Focus.UnitTests.Core
{
	[TestFixture]
	public class CensorshipCheckTests
	{
		[Test]
		public void CensorshipCheck_WhenPassedExcplicitWord_ReturnsWordFound()
		{
			// Arrange
			var badwords = new List<CensorshipRule>{ new CensorshipRule{Phrase= "Red", Level =CensorshipLevel.Red}};

			// Act
			var profCheck = new CensorshipCheck(badwords, "Red things are not Melyn, but they are red and also roch");

			// Assert
			profCheck.FoundCensorshipRules.Count.ShouldEqual(1);
			profCheck.FoundCensorshipRules[0].Frequency.ShouldEqual(2);
		}

		[Test]
		public void CensorshipCheck_WhenPassedWildcardWord_ReturnsWordFound()
		{
			// Arrange
			var badwords = new List<CensorshipRule> { new CensorshipRule { Phrase = "Red*", Level = CensorshipLevel.Red } };

			// Act
			var profCheck = new CensorshipCheck(badwords, "Reddish things are not Melyn, but they are red and also roch");

			// Assert
			profCheck.FoundCensorshipRules.Count.ShouldEqual(1);
			profCheck.FoundCensorshipRules[0].Frequency.ShouldEqual(2);
		}

		[Test]
		public void CensorshipCheck_WhenPassedBadWords_ReturnsListsOfGradedWords()
		{
			// Arrange
			var badwords = GetBadWords();

			// Act
			var profCheck = new CensorshipCheck(badwords, "Red things are not Melyn, but they are red and also roch");

			// Assert
			profCheck.FoundCensorshipRules.Count.ShouldEqual(3);
			profCheck.FoundCensorshipRules.Where(x => x.Level == CensorshipLevel.Red).ToList().Count.ShouldEqual(2);
			profCheck.FoundCensorshipRules.Where(x => x.Level == CensorshipLevel.Yellow).ToList().Count.ShouldEqual(1);
			profCheck.FoundCensorshipRules.Where(x => x.Phrase == "Red").FirstOrDefault().Frequency.ShouldEqual(2);
			profCheck.FoundCensorshipRules.Where(x => x.Phrase == "Roch").FirstOrDefault().Frequency.ShouldEqual(1);
			profCheck.FoundCensorshipRules.Where(x => x.Phrase == "Melyn").FirstOrDefault().Frequency.ShouldEqual(1);
		}

		[Test]
		public void CensorshipCheck_WhenPassedNoBadWords_ReturnsNoGradedWords()
		{
			// Arrange
			var badwords = GetBadWords();
			
			// Act
			var profCheck = new CensorshipCheck(badwords, "Blue things are not green, but they are blue");

			// Assert
			profCheck.FoundCensorshipRules.Count.ShouldEqual(0);
		}

		[Test]
		public void CensorshipCheck_WhenPassedText_ReturnsNothing()
		{
			// Arrange
			var badwords = GetBadWords();

			// Act
			var profCheck = new CensorshipCheck(badwords, "");

			// Assert
			profCheck.FoundCensorshipRules.Count.ShouldEqual(0);
		}

		[Test]
		public void CensorText_WhenPassedBadWordsWithHyphens_ReturnsCensoredTextWithHyphens()
		{
			// Arrange
			var badwords = new List<CensorshipRule> { new CensorshipRule { Phrase = "Red-blue", Level = CensorshipLevel.Red } };

			// Act
			var profCheck = new CensorshipCheck(badwords, "Red things are not Melyn, but they are red-blue and also roch", true);

			// Assert
			// not sure which is better returning r**-***e or r*d-b**e. The first works, the second needs a little more work
			profCheck.CensoredText.ShouldEqual("Red things are not Melyn, but they are r******e and also roch");
		}

		[Test]
		public void CensorText_WhenPassedBadWords_ReturnsCensoredText()
		{
			// Arrange
			var badwords = GetBadWords();

			// Act
			var profCheck = new CensorshipCheck(badwords, "Red things are not Melyn, but they are red and also roch", true);

			// Assert
			profCheck.CensoredText.ShouldEqual("R*d things are not M***n, but they are r*d and also r**h");
		}

		[Test]
		public void CensorText_WhenPassedBadWordsWithWildcards_ReturnsCensoredText()
		{
			// Arrange
			var badwords = new List<CensorshipRule> { new CensorshipRule { Phrase = "Red*", Level = CensorshipLevel.Red } };

			// Act
			var profCheck = new CensorshipCheck(badwords, "Red things are not Melyn, but they are reddish and also roch", true);

			// Assert
			profCheck.CensoredText.ShouldEqual("R*d things are not Melyn, but they are r*****h and also roch");
		}

		/// <summary>
		/// Gegts the bad words.
		/// </summary>
		/// <returns></returns>
		private static IEnumerable<CensorshipRule> GetBadWords()
		{
			return new List<CensorshipRule>
			       	{
			       		new CensorshipRule {Phrase = "Red", Level = CensorshipLevel.Red},
			       		new CensorshipRule {Phrase = "Roch", Level = CensorshipLevel.Red},
			       		new CensorshipRule {Phrase = "Rouge", Level = CensorshipLevel.Red},
			       		new CensorshipRule {Phrase = "Yellow", Level = CensorshipLevel.Yellow},
			       		new CensorshipRule {Phrase = "Jaune", Level = CensorshipLevel.Yellow},
			       		new CensorshipRule {Phrase = "Melyn", Level = CensorshipLevel.Yellow},
			       	};
		}
	}
}
