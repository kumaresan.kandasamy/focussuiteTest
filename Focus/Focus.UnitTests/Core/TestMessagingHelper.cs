﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using Focus.Services;
using Focus.Services.Helpers;
using Focus.UnitTests.Core.Repositories;
using Framework.Messaging;
using Framework.Messaging.Entities;

#endregion



namespace Focus.UnitTests.Core
{
	public class TestMessagingHelper : HelperBase, IMessagingHelper
	{
		public TestMessagingHelper(IRuntimeContext runtimeContext) : base(runtimeContext)
		{
			Messages = new List<FakeMessage>();
		}

		public void Initialise()
		{
			throw new NotImplementedException();
		}

		public void Publish<TMessage>(TMessage message, MessagePriority? messagePriority = null, bool updateIfExists = false, DateTime? processOn = null, string entityName = null, long? entityKey = null) where TMessage : class, IMessage
		{
			Messages.Add(new FakeMessage
			{
				Message = message,
				Priority = messagePriority,
				UpdateIfExists = updateIfExists,
				ProcessOn = processOn,
				EntityName = entityName,
				EntityKey = entityKey
			});
		}

		public void ProcessMessages(List<System.Guid> messageTypes = null, List<System.Guid> excludeMessageTypes = null)
		{
			throw new NotImplementedException();
		}

		public void ScheduleMessage<TMessage>(TMessage message, MessageSchedulePlan schedulePlan, MessagePriority? messagePriority = MessagePriority.Medium) where TMessage : class, IMessage
		{
			throw new NotImplementedException();
		}

		public MessageTypeEntity GetMessageType<TMessage>(TMessage message) where TMessage : class, IMessage
		{
			return new MessageTypeEntity
			{
				Id = new Guid(),
				TypeName = message.GetType().ToString()
			};
		}

		public List<FakeMessage> Messages { get; set; } 
	}

	public class FakeMessage
	{
		public IMessage Message { get; set; }

		public MessagePriority? Priority { get; set; }

		public bool UpdateIfExists { get; set; }

		public DateTime? ProcessOn { get; set; }

		public string EntityName { get; set; }

		public long? EntityKey { get; set; }
	}
}
