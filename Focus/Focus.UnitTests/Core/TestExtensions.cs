﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Xml.Linq;
using Focus.Core;
using NUnit.Framework;

using Focus.Core.Models;
using Focus.Core.Messages;

#endregion

namespace Focus.UnitTests.Core
{
	/// <summary>
	/// Test Extensions 
	/// </summary>
	internal static class TestExtensions
	{
		public static void ShouldBeTheSameDateTimeAs(this DateTime? actual, DateTime? expected)
		{
			Assert.That(actual, Is.EqualTo(expected).Within(TimeSpan.FromSeconds(1)));
		}

		public static void ShouldNotBeTheSameDateTimeAs(this DateTime? actual, DateTime? expected)
		{
			Assert.That(actual, Is.Not.EqualTo(expected));
		}

		/// <summary>
		/// Helper method that adds ClientTag, SessionId and RequestId to all request types.
		/// </summary>
		/// <typeparam name="T">The request type.</typeparam>
		/// <param name="request">The request</param>
		/// <param name="clientTag">The client tag.</param>
		/// <param name="sessionId">The session id.</param>
		/// <param name="userContext">The user context.</param>
		/// <param name="culture">The culture.</param>
		/// <returns>Fully prepared request, ready to use.</returns>
		public static T Prepare<T>(this T request, string clientTag, Guid sessionId, UserContext userContext, string culture) where T : ServiceRequest
		{
			request.ClientTag = clientTag;
			request.SessionId = sessionId;
			request.RequestId = Guid.NewGuid();

			request.UserContext = userContext;
			request.UserContext.Culture = culture;

		  request.VersionNumber = Constants.SystemVersion;
			
			return request;
		}
	}
}
