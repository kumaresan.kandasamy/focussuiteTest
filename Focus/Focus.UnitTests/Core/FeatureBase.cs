﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;

using Focus.Common.Code.ControllerResults;
using Focus.Core.Models;
using Focus.Services.Core;
using Focus.Services.Helpers;
using Mindscape.LightSpeed;
using Mindscape.LightSpeed.Testing;
using Moq;
using TechTalk.SpecFlow;

using Focus.Data.Core.Entities;
using Focus.UnitTests.Core.Repositories;
using Focus.Services;
using Focus.UnitTests.FakeHelpers;
using Framework.Messaging;
using Framework.ServiceLocation;

#endregion

namespace Focus.UnitTests.Core
{
	[Binding]
	public class FeatureBase
	{
		#region Test repositories

		protected static TestCoreRepository CoreRepository;

		#endregion

		#region Test helpers

		protected static ILookupHelper LookupHelper = new TestLookupHelper();
		protected static ISessionHelper SessionHelper = new TestSessionHelper();
		protected static ILoggingHelper LoggingHelper;
		protected static ILocalisationHelper LocalisationHelper = new TestLocalisationHelper();
		protected static IOfficeHelper OfficeHelper;
	  protected static IValidationHelper ValidationHelper;

		#endregion

		#region Test IApp

		protected static IApp App = new TestApp();

		#endregion

		#region Properties

		protected static User CurrentUser { get; private set; }

		protected static IControllerResult ControllerResult;

		#endregion

		#region Entity generators

		protected static TEntity GenerateSingleEntity<TEntity>(EntityState entityState = EntityState.New) where TEntity : Entity, new()
		{
			return EntityFactory.Create<TEntity>(entityState, CoreRepository.NextId);
		}

		protected static List<TEntity> GenerateMultipleEntities<TEntity>(int numberOfEntities, EntityState entityState = EntityState.New) where TEntity : Entity, new()
		{
			var entityList = new List<TEntity>();
			for (var i = 0; i < numberOfEntities; i++)
			{
				entityList.Add(EntityFactory.Create<TEntity>(entityState, CoreRepository.NextId));
			}
			return entityList;
		}

		#endregion

		#region SpecFlow hooks

		[BeforeStep]
		public void BeforeStep()
		{
			// If we are about to run an action then register the runtime context.
			var stepType = ScenarioContext.Current.CurrentScenarioBlock;
			if (stepType != ScenarioBlock.When) return;

			// If we have a logged in user, set the user context in App before registering the runtime context.
			if (CurrentUser != null) SetUserContext(CurrentUser);
			RegisterRuntimeContext();
		}

		[BeforeScenario]
		public static void BeforeScenario()
		{
			CoreRepository = new TestCoreRepository();
			App = new TestApp();
			AddBaseUser();
		}

		#endregion

		#region Private methods

		/// <summary>
		/// Registers the runtime context.
		/// </summary>
		private static void RegisterRuntimeContext()
		{
			IRuntimeContext runtimeContext = new TestRuntimeContext { AppSettings = App.Settings, Repositories = new TestRepositories(), Helpers = new TestHelpers() };

			#region Add repositores to runtime context

			((TestRepositories)runtimeContext.Repositories).Core = CoreRepository;

			#endregion

			#region Add helpers to runtime context

			((TestHelpers)runtimeContext.Helpers).Lookup = LookupHelper;
			((TestHelpers)runtimeContext.Helpers).Session = SessionHelper;
			((TestHelpers)runtimeContext.Helpers).Logging = LoggingHelper = new LoggingHelper(runtimeContext);
			((TestHelpers)runtimeContext.Helpers).Localisation = LocalisationHelper;
			((TestHelpers)runtimeContext.Helpers).Office = OfficeHelper = new OfficeHelper(runtimeContext);
		  ((TestHelpers) runtimeContext.Helpers).Validation = ValidationHelper = new ValidationHelper(runtimeContext);

			#endregion

			// Setup the Service Locator
			var serviceLocator = new StructureMapServiceLocator();
			ServiceLocator.SetServiceLocator(serviceLocator);

			// Register the Mock Message Store for the message bus
			var messageStoreMock = new Mock<IMessageStore>();
      messageStoreMock.Setup(x => x.Enqueue(It.IsAny<IMessage>(), It.IsAny<MessagePriority?>(), It.IsAny<bool>(), It.IsAny<DateTime?>(), It.IsAny<string>(), It.IsAny<long?>(), false));

			serviceLocator.Register(() => messageStoreMock.Object);
			serviceLocator.Register(() => runtimeContext);
		}

		private static void AddBaseUser()
		{
			var mockUser = GenerateSingleEntity<User>();
			mockUser.Blocked = false;
			mockUser.Enabled = true;
			mockUser.UserName = MockData.MockUsername;
			var person = GenerateSingleEntity<Person>();
			person.FirstName = "firstName";
			person.LastName = "lastName";
			person.EmailAddress = MockData.MockEmailAddress;
			mockUser.Person = person;

			CurrentUser = mockUser;

			CoreRepository.Add(mockUser);
			//CoreRepository.SaveChanges();
		}

		private static void SetUserContext(User user)
		{
			var userContext = new UserContext
			{
				ActionerId = App.User.UserId = user.Id,
				Blocked = user.Blocked,
				IsEnabled = user.Enabled,
				EmailAddress = user.Person.EmailAddress,
				FirstName = user.Person.FirstName,
				IsMigrated = user.IsMigrated,
				LastName = user.Person.LastName,
				PersonId = user.PersonId,
				ProgramAreaId = user.Person.ProgramAreaId,
				ScreenName = user.ScreenName,
				UserId = user.Id,
				UserName = user.UserName
			};

			if (user.Person.Employee != null)
			{
				userContext.EmployeeId = user.Person.Employee.Id;
				userContext.EmployerId = user.Person.Employee.EmployerId;
			}

			App.User = userContext;
		}

		#endregion
	}
}
