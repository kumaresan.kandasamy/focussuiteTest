﻿#region Copyright © 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using Focus.Common.Models;
using Focus.Core;
using Focus.Core.Models;
using Focus.Core.Models.Career;
using Focus.Core.Settings.Interfaces;
using Focus.Services;
using Focus.Web.Core.Models;
using Framework.ServiceLocation;
using Focus.Core.DataTransferObjects.FocusCore;

#endregion

namespace Focus.UnitTests.Core
{
	public class TestApp : IApp
	{
#pragma warning disable 649
		private readonly ServicesRuntime<StructureMapServiceLocator> _servicesRuntime;
#pragma warning restore 649

		private IUserContext _user;

		internal TestApp()
		{
			Settings = new FakeAppSettings();
		}

		public void Start()
		{
			// Initialize the services
			_servicesRuntime.Start();
		}

		public void Shutdown()
		{
		}

		public void BeginRequest()
		{
		}

    public void PostAuthenticateRequest()
    {
    }

		public void EndRequest()
		{
		}

		public string ClientTag { get { return MockData.MockClientTag; } }
		public Guid SessionId { get { return MockData.MockSessionId; } }
		public Guid RequestId { get { return MockData.MockRequestId; } }
		public IUserContext User
		{
			get { return _user ?? new UserContext(); }
			set { _user = value; }
		}

		public IAppSettings Settings { get; set; }

    public Guid ResetSessionId(Guid? newSessionId = null)
    {
      return newSessionId ?? MockData.MockSessionId;
    }

		public void RefreshSettings()
		{
		}

		public string VersionInfo { get; private set; }
		public string CurrentModuleNamePlaceHolder { get; private set; }
    public ExceptionModel Exception { get; set; }
		public IUserContext DeserializeUserContext(string serializedUserData)
		{
			return default(IUserContext);
		}

		public void UpdateRoles(long? userId)
		{
		}

		public List<OfficeDto> GetOfficesForUser(long? personId, bool getFromDB = false)
		{
			return new List<OfficeDto>();
		}

		public string BuildKey(string key)
		{
			return null;
		}

		public T GetCacheValue<T>(string key, T defaultValue = default(T))
		{
			return default(T);
		}

		public void SetCacheValue<T>(string key, T value)
		{
		}

    public T GetSessionValue<T>(string key, T defaultValue = default(T), IEnumerable<Type> knownTypes = null)
		{
			return default(T);
		}

    public void SetSessionValue<T>(string key, T value, IEnumerable<Type> knownTypes = null)
		{
		}

		public void RemoveSessionValue(string key)
		{
		}

    public void ClearSession()
    {
    }

		public void SetCookieValue(string key, string value, bool versionCookie = false)
		{
		}

    public string GetCookieValue(string key, string defaultValue = null, bool versionCookie = false)
		{
			return null;
		}

    public void RemoveCookieValue(string key, bool versionCookie = false)
		{
		}

		public T GetContextValue<T>(string key)
		{
			return default(T);
		}

		public void SetContextValue<T>(string key, T value)
		{
		}

    public void RemoveContextValue(string key)
    {
    }

		public string Content(string virtualPath)
		{
			return null;
		}

		public bool IsAssistUserAndCanViewEmployers(IUserContext userContext)
		{
			return false;
		}

		public bool IsAssistUserAndCanViewJobSeekers(IUserContext userContext)
		{
			return false;
		}

		public bool IsAssistUserAndCanViewQueues(IUserContext userContext)
		{
			return false;
		}

		public bool IsInRole(IUserContext userContext, string role)
		{
			return false;
		}

		public void Save(IUserContext userContext)
		{
		}

    public void ResetUserContext(IUserContext userContext)
    {
    }

		public string SerializeUserContext(IUserContext userContext)
		{
			return null;
		}

		public IUserData UserData { get { return new FakeUserData();}}

		public void ClearPageError()
		{
		}

		public void SetPageError(string error)
		{
		}

    public bool IsAnonymousAccess()
    {
      return false;
    }

    public bool IsAnonymousDomain()
    {
      return false;
    }

    public bool GuideToResume(long? personId)
    {
      return false;
    }
	}

	public class FakeUserData : IUserData
	{
		public bool HasDefaultResume { get { return false; } }
		public long? DefaultResumeId { get; set; }
		public ResumeCompletionStatuses DefaultResumeCompletionStatus { get; set; }
		public bool HasProfile { get { return false; } }
		public UserProfile Profile { get; set; }
		public bool? CareerUserDataCheck { get; set; }

		public void Load(UserDataModel model)
		{
			CareerUserDataCheck = model.CareerUserDataCheck;
			DefaultResumeId = model.DefaultResumeId;
			DefaultResumeCompletionStatus = model.DefaultResumeCompletionStatus;
			Profile = model.Profile;
		}
	}
}
