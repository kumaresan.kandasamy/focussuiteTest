﻿using System.Linq;
using System.Collections.Generic;
using Framework.Testing.NUnit;
using NUnit.Framework;
using Focus.Services.Core.Onet;



namespace Focus.UnitTests.Core
{
	[TestFixture]
	public class ONetTests : TestFixtureBase
	{
		private const string Title = "Computer Programmer";
		private OnetSearchAlt _onetOriginalReWrite;
		private OnetSearch _onet;
		

		[SetUp]
		public override void SetUp()
		{
			base.SetUp();
			_onetOriginalReWrite = new OnetSearchAlt(LibraryRepository);
			_onet = new OnetSearch(LibraryRepository);
			
		}
	
		[Test]
		public void CleanUpWords_DuplicateWordsReceived_DistinctWordsReturned()
		{
			// Act
			var returnAltValue = _onetOriginalReWrite.CleanUpWords(Title + " " + Title);
			var returnValue = _onet.ExtractWordsFromPhrase(Title + " " + Title);

			// Assert
			returnValue.Count.ShouldEqual(2);
			returnAltValue.Count.ShouldEqual(2);
		}
		
		[Test]
		public void CleanUpWords_NoWordsReceived_DistinctWordsReturned()
		{
			// Act
			var returnAltValue = _onetOriginalReWrite.CleanUpWords("");
			var returnValue = _onet.ExtractWordsFromPhrase(" ");

			// Assert
			returnValue.Count.ShouldEqual(0);
			returnAltValue.Count.ShouldEqual(0);
		}
		
		[Test]
		public void CleanUpWords_WhenNonAlphaNumericCharsReceived_UnwantedCharsRemoved()
		{
			// Act
			var returnAltValue = _onetOriginalReWrite.CleanUpWords("&amp;(" + Title + ") & " + Title);
			var returnValue = _onet.ExtractWordsFromPhrase("&amp;(" + Title + ") & " + Title);

			// Assert
			returnValue.Count.ShouldEqual(2);
			returnValue.Count(x => x.Word.Original == "&amp;").ShouldEqual(0);
			returnValue.Count(x => x.Word.Original == "&").ShouldEqual(0);
			returnValue.Count(x => x.Word.Original == "(").ShouldEqual(0);
			returnValue.Count(x => x.Word.Original == ")").ShouldEqual(0);
			returnAltValue.Count.ShouldEqual(2);
			returnAltValue.Count(x => x.Word.Original == "&amp;").ShouldEqual(0);
			returnAltValue.Count(x => x.Word.Original == "&").ShouldEqual(0);
			returnAltValue.Count(x => x.Word.Original == "(").ShouldEqual(0);
			returnAltValue.Count(x => x.Word.Original == ")").ShouldEqual(0);
		}
		
		[Test]
		public void CleanUpWords_WhenStopWordsReceived_StopWordsMarked()
		{
			// Act
			var returnAltValue = _onetOriginalReWrite.CleanUpWords(Title + " a it also");
			var returnValue = _onet.ExtractWordsFromPhrase(Title + " a it also");

			// Assert
			returnValue.Count.ShouldEqual(5);
			returnValue.Count(x => x.Word.Original == "&amp;").ShouldEqual(0);
			returnValue.Count(x => x.Word.Original == "&").ShouldEqual(0);
			returnValue.Count(x => x.Word.Original == "(").ShouldEqual(0);
			returnValue.Count(x => x.Word.Original == ")").ShouldEqual(0);
			returnValue.Count(x => x.IsStopWord).ShouldEqual(3);
			returnValue.Count(x => !x.IsStopWord).ShouldEqual(2);
			returnAltValue.Count.ShouldEqual(5);
			returnAltValue.Count(x => x.Word.Original == "&amp;").ShouldEqual(0);
			returnAltValue.Count(x => x.Word.Original == "&").ShouldEqual(0);
			returnAltValue.Count(x => x.Word.Original == "(").ShouldEqual(0);
			returnAltValue.Count(x => x.Word.Original == ")").ShouldEqual(0);
			returnAltValue.Count(x => x.IsStopWord).ShouldEqual(3);
			returnAltValue.Count(x => !x.IsStopWord).ShouldEqual(2);

		}

		[Ignore]
		[Test]
		public void ExtractInputWords_WhenPassedCleanedWords_ShouldReturnTheExpectedNumberOfScores()
		{
			// Arrange
			var words = new List<WordFrequency>
		                {
		                  new WordFrequency {IsStopWord = false, Word = new Word("Computer")},
		                  new WordFrequency {IsStopWord = false, Word = new Word("Programmer")}
		                };

			// Act
			var socScores = _onetOriginalReWrite.ExtractInputWords(words);

			// Assert
			socScores.Count.ShouldEqual(830);
		}

		[Ignore]
		[Test]
		public void GetOnetCodes_DuplicateWordsReceived_ReturnsTheExpectedCodes()
		{
			// Act
			var oNetCodesAlt = _onetOriginalReWrite.GetOnetCodes(Title, 5);
			var oNetCodes = _onet.GetOnetCodes(Title, 5);

			// Assert
			oNetCodes.ShouldNotBeNull();
			oNetCodes[0].ShouldEqual("15-1021.00");
			oNetCodes.Count.ShouldEqual(5);


			oNetCodesAlt.ShouldNotBeNull();
			oNetCodesAlt[0].ShouldEqual("15-1021.00");//153291);
			oNetCodesAlt.Count.ShouldEqual(5);
		}
		
		[Test]
		public void GetNormalizedText_WhenDeNormalizedWordsReceived_WordsAreNormalized()
		{
			// Arrange
			const string words = "Foo (Bar&)&amp;";

			// Act
			var returnValue = _onetOriginalReWrite.GetNormalizedText(words);

			// Assert
			returnValue.Contains("&amp;");
			returnValue.Contains("&");
			returnValue.Contains("(");
			returnValue.Contains(")");
		}

		[Ignore]
		[Test]
		public void CalculateFrequency_WhenDeNormalizedWordsReceived_WordsAreNormalized()
		{
			// Arrange
			var words = new List<WordFrequency>
		                {
		                  new WordFrequency {IsStopWord = false, Word = new Word("Computer")},
		                  new WordFrequency {IsStopWord = false, Word = new Word("Programmer")}
		                };

			var socScores = _onetOriginalReWrite.ExtractInputWords(words);
			var distinctSocs = _onetOriginalReWrite.GetDistinctOnetSocs(words);


			// Act
			var returnValue = _onetOriginalReWrite.CalculateFrequency(words, distinctSocs, socScores);

			// Assert
			returnValue.Count().ShouldEqual(485);

		}

		
	}
}
