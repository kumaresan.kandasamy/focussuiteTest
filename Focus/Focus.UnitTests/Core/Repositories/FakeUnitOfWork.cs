﻿#region Copyright © 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Reflection;
using Mindscape.LightSpeed;
using Mindscape.LightSpeed.Querying;
using Mindscape.LightSpeed.Search;
using Mindscape.LightSpeed.Testing;
using NUnit.Framework;

#endregion

namespace Focus.UnitTests.Core.Repositories
{
	public class FakeUnitOfWork : TestUnitOfWork
	{
		private class EntityInfo
		{
			internal Type type { get; set; }
			internal object id { get; set; }
		}

		private readonly List<EntityInfo> _watchedEntities = new List<EntityInfo>(); 

		private static long _nextId = 1;

		public static long NextId
		{
			get
			{
				_nextId++;
				return _nextId;
			}
		}

		public FakeUnitOfWork()
		{
			SetExpectedCollectionResult(new List<Entity>());
			SetExpectedSingleResult(null);
		}

		public override long Count(Query query)
		{
			return Find(query).Count;
		}

		public override Entity FindOne(Query query)
		{
			base.FindOne(query);
			if (query.Identifier != null && query.EntityType != null)
			{
				return this.FirstOrDefault(e => query.EntityType == e.GetType() && query.Identifier.Equals(e.GetId()));
			}
			return null;
		}

		public override void Find(Query query, IList results)
		{
			base.Find(query, results);
			if (query.EntityType == null) return;
			var entities = this.Where(e => query.EntityType == e.GetType());
			foreach (var entity in entities)
			{
				results.Add(entity);
			}
		}

		public override void Attach(Entity entity)
		{
			CheckAndAssignEntityId(entity);
			base.Attach(entity);
		}

		public override void Add(Entity entity)
		{
			CheckAndAssignEntityId(entity);
			base.Add(entity);
			foreach (var entityInUow in this.Where(entityInUow => !_watchedEntities.Exists(e => e.id.Equals(entityInUow.GetId()) && e.type == entityInUow.GetType())))
			{
				entityInUow.PropertyChanged += PropertyChangedEventHandler;
				_watchedEntities.Add(new EntityInfo{id = entityInUow.GetId(), type = entityInUow.GetType()});
			}
		}

		public override void Remove(Query query)
		{
			base.Remove(query);
			if (query.Identifier != null && query.EntityType != null)
			{
				Remove(FindOne(query));
			}
		}

		private void PropertyChangedEventHandler(object sender, PropertyChangedEventArgs args)
		{
			var property = sender.GetType().GetProperty(args.PropertyName);
			var entityProperty = property.GetValue(sender, null);
			var entity = entityProperty as Entity;
			if (entity != null && (this.Count(e => e.GetId().Equals(entity.GetId()) && e.GetType() == entity.GetType())) == 0)
				Add(entity);
		}

		private void CheckAndAssignEntityId(Entity entity)
		{
			try
			{
				if ((long)entity.GetId() == 0) AssignIdToEntity(entity);
			}
			catch (InvalidOperationException)
			{
				AssignIdToEntity(entity);
			}
		}

		private void AssignIdToEntity(Entity entity)
		{
			// Set the value of th id here so that lightspeed doesn't try and go to the database to get one!
			if (entity as Entity<long> != null)
			{
				var type = typeof(Entity<long>);
				var entityId = type.GetField("_id", BindingFlags.NonPublic | BindingFlags.Instance);
				if (entityId != null) entityId.SetValue(entity, NextId);
			}

			if (entity as Entity<string> != null)
			{
				// Set the value of th id here so that lightspeed doesn't try and go to the database to get one!
				var type = typeof(Entity<string>);
				var entityId = type.GetField("_id", BindingFlags.NonPublic | BindingFlags.Instance);
				if (entityId != null) entityId.SetValue(entity, NextId.ToString(CultureInfo.InvariantCulture));
			}

			if (entity as Entity<Guid> != null)
			{
				// Set the value of th id here so that lightspeed doesn't try and go to the database to get one!
				var type = typeof(Entity<Guid>);
				var entityId = type.GetField("_id", BindingFlags.NonPublic | BindingFlags.Instance);
				if (entityId != null) entityId.SetValue(entity, Guid.NewGuid());
			}
		}
	}
}
