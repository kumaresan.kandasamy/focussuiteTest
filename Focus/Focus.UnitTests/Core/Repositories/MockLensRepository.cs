﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Focus.Core;
using Focus.Core.Criteria.CandidateSearch;
using Focus.Core.DataTransferObjects.FocusExplorer;
using Focus.Core.Models.Career;
using Focus.Core.Views;
using Focus.Services.Repositories;

namespace Focus.UnitTests.Core.Repositories
{
	class MockLensRepository : ILensRepository
	{
    public List<ResumeView> SearchResumes(CandidateSearchCriteria criteria, string culture)
		{
			return null;
		}

		public string ParseResume(byte[] resume, string resumeFileExtension)
		{
			return null;
		}

		public string ParseResume(byte[] resume, string resumeFileExtension, bool resumeOnly)
		{
			return null;
		}

		public void RegisterResume(string lensId, string resumeXml, DateTime resumeRegisteredOn, DateTime resumeModifiedOn, long personId, SchoolStatus? enrollmentStatus, List<long> degreeEducationLevelIds, DateTime? dateOfBirth)
		{
		}

		public void UnregisterResume(List<string> lensIds)
		{
		}

    public List<PostingSearchResultView> SearchPostings(SearchCriteria criteria, string culture, bool forAlert, long? personId = null)
		{
			return null;
		}

		public void RegisterPosting(string lensPostingId, long? postingId, XDocument postingXml, string dataElementsXml, string culture, List<int> mappedRequiredProgramsOfStudy, PostalCodeDto postalCode = null)
		{
		}

		public void UnregisterPosting(string lensPostingId)
		{
		}

		public string CanonPostingWithJobMine(string postingXml)
		{
			return null;
		}

		public List<PostingSearchResultView> OrderPostings(IEnumerable<PostingSearchResultView> postings, bool matchedSearch, int documentCount)
		{
			return null;
		}

		public List<string> GetOnetCodes(string jobTitle)
		{
			return null;
		}

		public bool IsLicensed()
		{
			return false;
		}

		public List<string> Clarify(string text, int minScore, int maxSearchCount, string type)
		{
			return null;
		}

		public string CovertBinaryData(byte[] binaryFileData, string fileExtension)
		{
			return null;
		}

		public string Canon(string xml)
		{
			return xml;
		}

		public int Match(string resumeXml, string postingXml)
		{
			return 0;
		}

		public string Tag(string plainText, DocumentType type)
		{
			return null;
		}

		public string TagWithHtml(byte[] binaryFileData, string fileExtension, DocumentType type, out string html)
		{
			html = null;
			return null;
		}

		public string ExecuteJobMineCommand(string command)
		{
			return null;
		}
	}
}
