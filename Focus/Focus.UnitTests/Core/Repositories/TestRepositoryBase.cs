﻿#region Copyright © 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Linq.Expressions;

using Mindscape.LightSpeed;
using Mindscape.LightSpeed.Querying;

using Focus.Core;
using Focus.Data.Repositories;
using IRepository = Focus.Data.Repositories.Contracts.IRepository;

#endregion

namespace Focus.UnitTests.Core.Repositories
{
	public abstract class TestRepositoryBase : IRepository
	{
		protected FakeUnitOfWork uow = new FakeUnitOfWork();

		public long NextId
		{
			get { return FakeUnitOfWork.NextId; }
		}

		public void Add<T>(T entity) where T : Entity
		{
			uow.Add(entity);
		}

		public void Remove<T>(T entity) where T : Entity
		{
			uow.Remove(entity);
		}

		public void Remove<T>(Expression<Func<T, bool>> selector) where T : Entity
		{
			var entitiesToRemove = uow.OfType<T>().AsQueryable().Where(selector);
			foreach (var entity in entitiesToRemove)
			{
				uow.Remove(entity);
			}
		}

		public void Remove(Query query)
		{
			uow.Remove(query);
		}

		public void Update(Query query, object attributes)
		{
			uow.Update(query, attributes);
		}

		public long Count<T>() where T : Entity
		{
			return uow.OfType<T>().Count();
		}

		public long Count<T>(Expression<Func<T, bool>> selector) where T : Entity
		{
			var list = uow.OfType<T>().AsQueryable();
			return list.Where(selector).Count();
		}

		public bool Exists<T>(Expression<Func<T, bool>> selector) where T : Entity
		{
			var list = uow.OfType<T>().AsQueryable();
			return list.Where(selector).Count() != 0;
		}

		public T FindById<T>(object id) where T : Entity
		{
			return uow.OfType<T>().Single(e => id.Equals(e.GetId()));
		}

		public IList<T> Find<T>() where T : Entity
		{
			return uow.OfType<T>().ToList();
		}

		public IList<T> Find<T>(Expression<Func<T, bool>> selector) where T : Entity
		{
			return uow.OfType<T>().AsQueryable().Where(selector).ToList();
		}

		public IQueryable<T> Query<T>() where T : Entity
		{
			var list = uow.OfType<T>().ToList();
			return list.AsQueryable();
		}

		public PagedList<T> FindPaged<T>(IQueryable<T> query, int pageIndex, int pageSize) where T : Entity
		{
			return new PagedList<T>(query, pageIndex, pageSize);
		}

		public void SaveChanges(bool reset = false)
		{
			// never reset the uow for a test
			uow.SaveChanges();
		}

		public T ExecuteScalar<T>(string sql)
		{
			throw new NotImplementedException();
		}

		public int ExecuteNonQuery(string sql)
		{
			throw new NotImplementedException();
		}

		public IDataReader ExecuteDataReader(string sql)
		{
			throw new NotImplementedException();
		}

		public event EventHandler<RepositoryEntityStateChangedEventArgs> EntityStateChanged;

		private void SetFieldValue(Entity entity, long id, string fieldName, DateTime createdOn)
		{
			var changes = new Dictionary<string, object>
			{
				{ fieldName, createdOn }
			};
			var updateQuery = new Query(entity.GetType(), Entity.Attribute("Id") == id);
			Update(updateQuery, changes);
		}

		public void SetCreatedOn(Entity entity, long id, DateTime createdOn)
		{
			SetFieldValue(entity, id, "CreatedOn", createdOn);
		}

		public void SetUpdatedOn(Entity entity, long id, DateTime updatedOn)
		{
			SetFieldValue(entity, id, "UpdatedOn", updatedOn);
		}

		public void SetDeletedOn(Entity entity, long id, DateTime deletedOn)
		{
			SetFieldValue(entity, id, "DeletedOn", deletedOn);
		}

		public void SetActionedOn(Entity entity, long id, DateTime actionedOn)
		{
			SetFieldValue(entity, id, "ActionedOn", actionedOn);
		}
	}
}
