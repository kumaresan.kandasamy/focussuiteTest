﻿#region Copyright © 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;

using Focus.Data.Core.Entities;
using Focus.Data.Repositories.Contracts;

#endregion

namespace Focus.UnitTests.Core.Repositories
{
	public class TestCoreRepository : TestRepositoryBase, ICoreRepository
	{
		public TestCoreRepository()
		{
		}

		#region Entity Queryable Helpers

		/// <summary>
		/// Gets the action events.
		/// </summary>
		/// <value>The action events.</value>
		public IQueryable<ActionEvent> ActionEvents
		{
			get { return Query<ActionEvent>(); }
		}

		/// <summary>
		/// Gets the action types.
		/// </summary>
		/// <value>The action types.</value>
		public IQueryable<ActionType> ActionTypes
		{
			get { return Query<ActionType>(); }
		}

		/// <summary>
		/// Gets the applications.
		/// </summary>
		/// <value>The applications.</value>
		public IQueryable<Application> Applications
		{
			get { return Query<Application>(); }
		}

		/// <summary>
		/// Gets the users.
		/// </summary>
		/// <value>The users.</value>
		public IQueryable<User> Users
		{
			get { return Query<User>(); }
		}

		/// <summary>
		/// Gets the user security questions.
		/// </summary>
		/// <value>The user security questions.</value>
		public IQueryable<UserSecurityQuestion> UserSecurityQuestions
		{
			get { return Query<UserSecurityQuestion>(); }
		}

		/// <summary>
		/// Gets the bookmarks.
		/// </summary>
		/// <value>The bookmarks.</value>
		public IQueryable<Bookmark> Bookmarks
		{
			get { return Query<Bookmark>(); }
		}

		/// <summary>
		/// Gets the business units.
		/// </summary>
		/// <value>The business units.</value>
		public IQueryable<BusinessUnit> BusinessUnits
		{
			get { return Query<BusinessUnit>(); }
		}

		/// <summary>
		/// Gets the business unit addresses.
		/// </summary>
		/// <value>The business unit addresses.</value>
		public IQueryable<BusinessUnitAddress> BusinessUnitAddresses
		{
			get { return Query<BusinessUnitAddress>(); }
		}

		/// <summary>
		/// Gets the business unit descriptions.
		/// </summary>
		/// <value>The business unit descriptions.</value>
		public IQueryable<BusinessUnitDescription> BusinessUnitDescriptions
		{
			get { return Query<BusinessUnitDescription>(); }
		}

		/// <summary>
		/// Gets the business unit logos.
		/// </summary>
		/// <value>The business unit logos.</value>
		public IQueryable<BusinessUnitLogo> BusinessUnitLogos
		{
			get { return Query<BusinessUnitLogo>(); }
		}

		/// <summary>
		/// Gets the dismissed messages.
		/// </summary>
		/// <value>The dismissed messages.</value>
		public IQueryable<DismissedMessage> DismissedMessages
		{
			get { return Query<DismissedMessage>(); }
		}

		/// <summary>
		/// Gets the employees.
		/// </summary>
		/// <value>The employees.</value>
		public IQueryable<Employee> Employees
		{
			get { return Query<Employee>(); }
		}

		/// <summary>
		/// Gets the employee business units.
		/// </summary>
		/// <value>The employee business units.</value>
		public IQueryable<EmployeeBusinessUnit> EmployeeBusinessUnits
		{
			get { return Query<EmployeeBusinessUnit>(); }
		}

		/// <summary>
		/// Gets the business unit activity.
		/// </summary>
		/// <value>The business unit activity.</value>
		public IQueryable<BusinessUnitActivityView> BusinessUnitActivity
		{
			get { return Query<BusinessUnitActivityView>(); }
		}

		/// <summary>
		/// Gets the business unit placements.
		/// </summary>
		/// <value>The business unit placements.</value>
		public IQueryable<BusinessUnitPlacementsView> BusinessUnitPlacements
		{
			get { return Query<BusinessUnitPlacementsView>(); }
		}

		/// <summary>
		/// Gets the open positions for the matched candidate.
		/// </summary>
		/// <value>The open position matched.</value>
		public IQueryable<OpenPositionMatchesView> OpenPositionMatchViews
		{
			get { return Query<OpenPositionMatchesView>(); }
		}

		/// <summary>
		/// Gets the employers.
		/// </summary>
		/// <value>The employers.</value>
		public IQueryable<Employer> Employers
		{
			get { return Query<Employer>(); }
		}

		/// <summary>
		/// Gets the entity types.
		/// </summary>
		/// <value>The entity types.</value>
		public IQueryable<EntityType> EntityTypes
		{
			get { return Query<EntityType>(); }
		}

		/// <summary>
		/// Gets the excluded lens postings.
		/// </summary>
		/// <value>
		/// The excluded lens postings.
		/// </value>
		public IQueryable<ExcludedLensPosting> ExcludedLensPostings
		{
			get { return Query<ExcludedLensPosting>(); }
		}

		/// <summary>
		/// Gets the issues.
		/// </summary>
		/// <value>
		/// The excuded lens issues.
		/// </value>
		public IQueryable<Issues> Issues
		{
			get { return Query<Issues>(); }
		}

		/// <summary>
		/// Gets the jobs.
		/// </summary>
		/// <value>The jobs.</value>
		public IQueryable<Job> Jobs
		{
			get { return Query<Job>(); }
			set { }
		}

		/// <summary>
		/// Gets the jobs.
		/// </summary>
		/// <value>The jobs.</value>
		public IQueryable<JobPrevData> JobPrevDatas
		{
			get { return Query<JobPrevData>(); }
			set { }
		}

		/// <summary>
		/// Gets or sets the job certificates.
		/// </summary>
		/// <value>The job certificates.</value>
		public IQueryable<JobCertificate> JobCertificates
		{
			get { return Query<JobCertificate>(); }
		}

		/// <summary>
		/// Gets the job driving licence endorsements.
		/// </summary>
		/// <value>
		/// The job driving licence endorsements.
		/// </value>
		public IQueryable<JobDrivingLicenceEndorsement> JobDrivingLicenceEndorsements
		{
			get { return Query<JobDrivingLicenceEndorsement>(); }
		}

		/// <summary>
		/// Gets or sets the job education internship skills.
		/// </summary>
		/// <value>The job certificates.</value>
		public IQueryable<JobEducationInternshipSkill> JobEducationInternshipSkills
		{
			get { return Query<JobEducationInternshipSkill>(); }
		}

		/// <summary>
		/// Gets the job languages.
		/// </summary>
		/// <value>The job languages.</value>
		public IQueryable<JobLanguage> JobLanguages
		{
			get { return Query<JobLanguage>(); }
		}

		/// <summary>
		/// Gets the job licences.
		/// </summary>
		/// <value>The job licences.</value>
		public IQueryable<JobLicence> JobLicences
		{
			get { return Query<JobLicence>(); }
		}

		/// <summary>
		/// Gets the job locations.
		/// </summary>
		/// <value>The job locations.</value>
		public IQueryable<JobLocation> JobLocations
		{
			get { return Query<JobLocation>(); }
		}

		/// <summary>
		/// Gets the job programs of study.
		/// </summary>
		/// <value>The job programs of study.</value>
		public IQueryable<JobProgramOfStudy> JobProgramsOfStudy
		{
			get { return Query<JobProgramOfStudy>(); }
		}

		/// <summary>
		/// Gets the job skills.
		/// </summary>
		/// <value>The job skills.</value>
		public IQueryable<JobSkill> JobSkills
		{
			get { return Query<JobSkill>(); }
		}

		/// <summary>
		/// Gets the job special requirements.
		/// </summary>
		/// <value>The job special requirements.</value>
		public IQueryable<JobSpecialRequirement> JobSpecialRequirements
		{
			get { return Query<JobSpecialRequirement>(); }
		}

		/// <summary>
		/// Gets the note reminders.
		/// </summary>
		/// <value>The note reminders.</value>
		public IQueryable<NoteReminder> NoteReminders
		{
			get { return Query<NoteReminder>(); }
		}

		/// <summary>
		/// Gets the messages.
		/// </summary>
		/// <value>The messages.</value>
		public IQueryable<Message> Messages
		{
			get { return Query<Message>(); }
		}

		/// <summary>
		/// Gets the message texts.
		/// </summary>
		/// <value>The message texts.</value>
		public IQueryable<MessageText> MessageTexts
		{
			get { return Query<MessageText>(); }
		}

		/// <summary>
		/// Gets the notes.
		/// </summary>
		/// <value>The notes.</value>
		public IQueryable<Note> Notes
		{
			get { return Query<Note>(); }
		}

		/// <summary>
		/// Gets the persons.
		/// </summary>
		/// <value>The persons.</value>
		public IQueryable<Person> Persons
		{
			get { return Query<Person>(); }
		}

		/// <summary>
		/// Gets the person lists.
		/// </summary>
		/// <value>The person lists.</value>
		public IQueryable<PersonList> PersonLists
		{
			get { return Query<PersonList>(); }
		}

		/// <summary>
		/// Gets the person list people.
		/// </summary>
		/// <value>The person list people.</value>
		public IQueryable<PersonListPerson> PersonListPeople
		{
			get { return Query<PersonListPerson>(); }
		}

		/// <summary>
		/// Gets the person notes.
		/// </summary>
		/// <value>The person notes.</value>
		public IQueryable<PersonNote> PersonNotes
		{
			get { return Query<PersonNote>(); }
		}

		/// <summary>
		/// Gets the person posting matches.
		/// </summary>
		/// <value>The person posting matches.</value>
		public IQueryable<PersonPostingMatch> PersonPostingMatches
		{
			get { return Query<PersonPostingMatch>(); }
		}

		/// <summary>
		/// Gets the person posting matches to ignore.
		/// </summary>
		/// <value>The person posting matches to ignore.</value>
		public IQueryable<PersonPostingMatchToIgnore> PersonPostingMatchesToIgnore
		{
			get { return Query<PersonPostingMatchToIgnore>(); }
		}

		/// <summary>
		/// Gets the open position match to ignore.
		/// </summary>
		/// <value>The open position match to ignore.</value>
		public IQueryable<RecentlyPlaced> RecentlyPlaced
		{
			get { return Query<RecentlyPlaced>(); }
		}

		/// <summary>
		/// Gets the open position match to ignore.
		/// </summary>
		/// <value>The open position match to ignore.</value>
		public IQueryable<RecentlyPlacedMatch> RecentlyPlacedMatches
		{
			get { return Query<RecentlyPlacedMatch>(); }
		}

		/// <summary>
		/// Gets the resumes.
		/// </summary>
		/// <value>The resumes.</value>
		public IQueryable<Resume> Resumes
		{
			get { return Query<Resume>(); }
		}

		/// <summary>
		/// Gets the resume documents.
		/// </summary>
		/// <value>
		/// The resume documents.
		/// </value>
		public IQueryable<ResumeDocument> ResumeDocuments
		{
			get { return Query<ResumeDocument>(); }
		}

		/// <summary>
		/// Gets the resume jobs.
		/// </summary>
		/// <value>The resume jobs.</value>
		public IQueryable<ResumeJob> ResumeJobs
		{
			get { return Query<ResumeJob>(); }
		}

		/// <summary>
		/// Gets the phone numbers.
		/// </summary>
		/// <value>The phone numbers.</value>
		public IQueryable<PhoneNumber> PhoneNumbers
		{
			get { return Query<PhoneNumber>(); }
		}

		/// <summary>
		/// Gets the postings.
		/// </summary>
		/// <value>The postings.</value>
		public IQueryable<Posting> Postings
		{
			get { return Query<Posting>(); }
		}

		/// <summary>
		/// Gets the posting surveys.
		/// </summary>
		/// <value>The posting surveys.</value>
		public IQueryable<PostingSurvey> PostingSurveys
		{
			get { return Query<PostingSurvey>(); }
		}

		/// <summary>
		/// Gets the saved messages.
		/// </summary>
		/// <value>The saved messages.</value>
		public IQueryable<SavedMessage> SavedMessages
		{
			get { return Query<SavedMessage>(); }
		}

		/// <summary>
		/// Gets the saved message texts.
		/// </summary>
		/// <value>The saved message texts.</value>
		public IQueryable<SavedMessageText> SavedMessageTexts
		{
			get { return Query<SavedMessageText>(); }
		}

		/// <summary>
		/// Gets the saved searches.
		/// </summary>
		/// <value>The saved searches.</value>
		public IQueryable<SavedSearch> SavedSearches
		{
			get { return Query<SavedSearch>(); }
		}

		/// <summary>
		/// Gets the status logs.
		/// </summary>
		/// <value>The status logs.</value>
		public IQueryable<StatusLog> StatusLogs
		{
			get { return Query<StatusLog>(); }
		}

		/// <summary>
		/// Gets the employer addresses.
		/// </summary>
		/// <value>The employer addresses.</value>
		public IQueryable<EmployerAddress> EmployerAddresses
		{
			get { return Query<EmployerAddress>(); }
		}

		/// <summary>
		/// Gets the job addresses.
		/// </summary>
		/// <value>The job addresses.</value>
		public IQueryable<JobAddress> JobAddresses
		{
			get { return Query<JobAddress>(); }
		}

		/// <summary>
		/// Gets the roles.
		/// </summary>
		/// <value>The roles.</value>
		public IQueryable<Role> Roles
		{
			get { return Query<Role>(); }
		}

		/// <summary>
		/// Gets the person addresses.
		/// </summary>
		/// <value>The person addresses.</value>
		public IQueryable<PersonAddress> PersonAddresses
		{
			get { return Query<PersonAddress>(); }
		}


		/// <summary>
		/// Gets the user roles.
		/// </summary>
		/// <value>The user roles.</value>
		public IQueryable<UserRole> UserRoles
		{
			get { return Query<UserRole>(); }
		}

		/// <summary>
		/// Gets the job notes.
		/// </summary>
		/// <value>The job notes.</value>
		public IQueryable<JobNote> JobNotes
		{
			get { return Query<JobNote>(); }
		}

		/// <summary>
		/// Gets the saved search employees.
		/// </summary>
		/// <value>The saved search employees.</value>
		public IQueryable<SavedSearchUser> SavedSearchUsers
		{
			get { return Query<SavedSearchUser>(); }
		}

		/// <summary>
		/// Gets the viewed postings.
		/// </summary>
		/// <value>
		/// The viewed postings.
		/// </value>
		public IQueryable<ViewedPosting> ViewedPostings
		{
			get { return Query<ViewedPosting>(); }
		}

		/// <summary>
		/// Gets the registration pins.
		/// </summary>
		/// <value>
		/// The registration pins.
		/// </value>
		public IQueryable<RegistrationPin> RegistrationPins
		{
			get { return Query<RegistrationPin>(); }
		}

		/// <summary>
		/// Gets the offices.
		/// </summary>
		/// <value>
		/// The offices.
		/// </value>
		public IQueryable<Office> Offices
		{
			get { return Query<Office>(); }
		}

		/// <summary>
		/// Gets the user postcode mappers.
		/// </summary>
		/// <value>
		/// The user postcode mappers.
		/// </value>
		public IQueryable<PersonPostcodeMapper> PersonPostcodeMappers
		{
			get { return Query<PersonPostcodeMapper>(); }
		}

		/// <summary>
		/// Gets the person office mappers.
		/// </summary>
		/// <value>
		/// The person office mappers.
		/// </value>
		public IQueryable<PersonOfficeMapper> PersonOfficeMappers
		{
			get { return Query<PersonOfficeMapper>(); }
		}

		/// <summary>
		/// Gets the employer office mappers.
		/// </summary>
		/// <value>
		/// The employer office mappers.
		/// </value>
		public IQueryable<EmployerOfficeMapper> EmployerOfficeMappers
		{
			get { return Query<EmployerOfficeMapper>(); }
		}

		/// <summary>
		/// Gets the job office mappers.
		/// </summary>
		/// <value>
		/// The job office mappers.
		/// </value>
		public IQueryable<JobOfficeMapper> JobOfficeMappers
		{
			get { return Query<JobOfficeMapper>(); }
		}

		/// <summary>
		/// Gets the job issues.
		/// </summary>
		/// <value>
		/// The job issues.
		/// </value>
		public IQueryable<JobIssues> JobIssues
		{
			get { return Query<JobIssues>(); }
		}

		/// <summary>
		/// Gets the candidate search histories.
		/// </summary>
		/// <value>
		/// The candidate search histories.
		/// </value>
		public IQueryable<CandidateSearchHistory> CandidateSearchHistories
		{
			get { return Query<CandidateSearchHistory>(); }
		}

		/// <summary>
		/// Gets the candidate search results.
		/// </summary>
		/// <value>
		/// The candidate search results.
		/// </value>
		public IQueryable<CandidateSearchResult> CandidateSearchResults
		{
			get { return Query<CandidateSearchResult>(); }
		}

		/// <summary>
		/// Gets the job seeker surveys.
		/// </summary>
		/// <value>
		/// The job seeker surveys.
		/// </value>
		public IQueryable<JobSeekerSurvey> JobSeekerSurveys
		{
			get { return Query<JobSeekerSurvey>(); }
		}

		/// <summary>
		/// Gets the persons current offices.
		/// </summary>
		/// <value>
		/// The persons current offices.
		/// </value>
		public IQueryable<PersonsCurrentOffice> PersonsCurrentOffices
		{
			get { return Query<PersonsCurrentOffice>(); }
		}

		/// <summary>
		/// Gets the resume templates.
		/// </summary>
		/// <value>
		/// The resume templates.
		/// </value>
		public IQueryable<ResumeTemplate> ResumeTemplates
		{
			get { return Query<ResumeTemplate>(); }
		}

		/// <summary>
		/// Gets the campuses.
		/// </summary>
		/// <value>
		/// The campuses.
		/// </value>
		public IQueryable<Campus> Campuses
		{
			get { return Query<Campus>(); }
		}

		/// <summary>
		/// Gets the upload files.
		/// </summary>
		/// <value>
		/// The upload files.
		/// </value>
		public IQueryable<UploadFile> UploadFiles
		{
			get { return Query<UploadFile>(); }
		}

		/// <summary>
		/// Gets the integration import records.
		/// </summary>
		/// <value>
		/// The integration import records.
		/// </value>
		public IQueryable<IntegrationImportRecord> IntegrationImportRecords
		{
			get { return Query<IntegrationImportRecord>(); }
		}

		/// <summary>
		/// Gets the user action type activities.
		/// </summary>
		/// <value>
		/// The user action type activities.
		/// </value>
		public IQueryable<UserActionTypeActivity> UserActionTypeActivities
		{
			get { return Query<UserActionTypeActivity>(); }
		}

		/// <summary>
		/// Gets the encryptions.
		/// </summary>
		/// <value>
		/// The encryptions.
		/// </value>
		public IQueryable<Encryption> Encryptions
		{
			get { return Query<Encryption>(); }
		}

		/// <summary>
		/// Gets the person mobile device.
		/// </summary>
		/// <value>
		/// The person mobile device.
		/// </value>
		public IQueryable<PersonMobileDevice> PersonMobileDevices
		{
			get { return Query<PersonMobileDevice>(); }
		}

		/// <summary>
		/// Gets the push notification.
		/// </summary>
		/// <value>
		/// The push notifications.
		/// </value>
		public IQueryable<PushNotification> PushNotifications
		{
			get { return Query<PushNotification>(); }
		}

		/// <summary>
		/// Gets the employer referral status reasons.
		/// </summary>
		/// <value>
		/// The employer referral status reasons.
		/// </value>
		public IQueryable<EmployerReferralStatusReason> EmployerReferralStatusReasons
		{
			get { return Query<EmployerReferralStatusReason>(); }
		}

		/// <summary>
		/// Gets the business unit approval denial reasons.
		/// </summary>
		/// <value>
		/// The business unit approval denial reasons.
		/// </value>
		public IQueryable<BusinessUnitReferralStatusReason> BusinessUnitReferralStatusReasons
		{
			get { return Query<BusinessUnitReferralStatusReason>(); }
		}

		/// <summary>
		/// Gets the application referral status reasons.
		/// </summary>
		/// <value>
		/// The application referral status reasons.
		/// </value>
		public IQueryable<ApplicationReferralStatusReason> ApplicationReferralStatusReasons
		{
			get { return Query<ApplicationReferralStatusReason>(); }
		}

		/// <summary>
		/// Gets the employee referral status reasons.
		/// </summary>
		/// <value>
		/// The employee referral status reasons.
		/// </value>
		public IQueryable<EmployeeReferralStatusReason> EmployeeReferralStatusReasons
		{
			get { return Query<EmployeeReferralStatusReason>(); }
		}

		/// <summary>
		/// Gets the documents.
		/// </summary>
		/// <value>
		/// The documents.
		/// </value>
		public IQueryable<Document> Documents
		{
			get { return Query<Document>(); }
		}

				/// <summary>
				/// Gets the referrals.
				/// </summary>
				/// <value>The referrals.</value>
				public IQueryable<Referral> Referrals
				{
					get { return Query<Referral>(); }
				}

                /// <summary>
                /// Gets the activity update setting.
                /// </summary>
                /// <value>The setting.</value>
                public IQueryable<PersonActivityUpdateSettings> PersonActivityUpdateSetting
                {
                    get { return Query<PersonActivityUpdateSettings>(); }
                }

        /// <summary>
        /// Gets the backdated activity setting.
        /// </summary>
                public IQueryable<BackdatedActionEvents> BackdatedActionEvent
                {
                    get { return Query<BackdatedActionEvents>(); }
                }

		#endregion

		#region View Queryable Helpers

		/// <summary>
		/// Gets the application views.
		/// </summary>
		/// <value>The application views.</value>
		public IQueryable<ApplicationView> ApplicationViews
		{
			get { return Query<ApplicationView>(); }
		}

		/// <summary>
		/// Gets the application basic views.
		/// </summary>
		/// <value>
		/// The application basic views.
		/// </value>
		public IQueryable<ApplicationBasicView> ApplicationBasicViews
		{
			get { return Query<ApplicationBasicView>(); }
		}

		/// <summary>
		/// Gets the job views.
		/// </summary>
		/// <value>The job view</value>
		public IQueryable<JobView> JobViews
		{
			get { return Query<JobView>(); }
		}

		/// <summary>
		/// Gets the job views.
		/// </summary>
		/// <value>The job view</value>
		public IQueryable<JobIdsView> JobIdsViews
		{
			get { return Query<JobIdsView>(); }
		}

		/// <summary>
		/// Gets the employee views.
		/// </summary>
		/// <value>The employee views.</value>
		public IQueryable<EmployeeSearchView> EmployeeSearchViews
		{
			get { return Query<EmployeeSearchView>(); }
		}

		/// <summary>
		/// Gets the job seeker referral views.
		/// </summary>
		/// <value>The job seeker referral views.</value>
		public IQueryable<JobSeekerReferralView> JobSeekerReferralViews
		{
			get { return Query<JobSeekerReferralView>(); }
		}

		/// <summary>
		/// Gets the employer account referral views.
		/// </summary>
		/// <value>The employer account referral views.</value>
		public IQueryable<EmployerAccountReferralView> EmployerAccountReferralViews
		{
			get { return Query<EmployerAccountReferralView>(); }
		}

		/// <summary>
		/// Gets the job posting referral views.
		/// </summary>
		/// <value>The job posting referral views.</value>
		public IQueryable<JobPostingReferralView> JobPostingReferralViews
		{
			get { return Query<JobPostingReferralView>(); }
		}

		/// <summary>
		/// Gets the staff search views.
		/// </summary>
		/// <value>The staff search views.</value>
		public IQueryable<StaffSearchView> StaffSearchViews
		{
			get { return Query<StaffSearchView>(); }
		}

		/// <summary>
		/// Gets the single sign ons.
		/// </summary>
		/// <value>The single sign ons.</value>
		public IQueryable<SingleSignOn> SingleSignOns
		{
			get { return Query<SingleSignOn>(); }
		}

		/// <summary>
		/// Gets the candidate note views.
		/// </summary>
		/// <value>The candidate note views.</value>
		public IQueryable<CandidateNoteView> CandidateNoteViews
		{
			get { return Query<CandidateNoteView>(); }
		}

		/// <summary>
		/// Gets the person list candidate views.
		/// </summary>
		/// <value>The person list candidate views.</value>
		public IQueryable<PersonListCandidateView> PersonListCandidateViews
		{
			get { return Query<PersonListCandidateView>(); }
		}

		/// <summary>
		/// Gets the expiring job views.
		/// </summary>
		/// <value>The expiring job views.</value>
		public IQueryable<ExpiringJobView> ExpiringJobViews
		{
			get { return Query<ExpiringJobView>(); }
		}

		/// <summary>
		/// Gets the note reminder views.
		/// </summary>
		/// <value>The note reminder views.</value>
		public IQueryable<NoteReminderView> NoteReminderViews
		{
			get { return Query<NoteReminderView>(); }
		}

		/// <summary>
		/// Gets the application status log views.
		/// </summary>
		/// <value>The application status log views.</value>
		public IQueryable<ApplicationStatusLogView> ApplicationStatusLogViews
		{
			get { return Query<ApplicationStatusLogView>(); }
		}

		/// <summary>
		/// Gets the active search alert views.
		/// </summary>
		/// <value>The active search alert views.</value>
		public IQueryable<ActiveSearchAlertView> ActiveSearchAlertViews
		{
			get { return Query<ActiveSearchAlertView>(); }
		}

		/// <summary>
		/// Gets the hiring manager activity view.
		/// </summary>
		/// <value>The hiring manager activity view.</value>
		public IQueryable<HiringManagerActivityView> HiringManagerActivityView
		{
			get { return Query<HiringManagerActivityView>(); }
		}

		/// <summary>
		/// Gets the assist user activity views.
		/// </summary>
		/// <value>The assist user activity views.</value>
		public IQueryable<AssistUserActivityView> AssistUserActivityViews
		{
			get { return Query<AssistUserActivityView>(); }
		}

		/// <summary>
		/// Gets the assist user employers assisted views.
		/// </summary>
		/// <value>The assist user employers assisted views.</value>
		public IQueryable<AssistUserEmployersAssistedView> AssistUserEmployersAssistedViews
		{
			get { return Query<AssistUserEmployersAssistedView>(); }
		}

		/// <summary>
		/// Gets the assist user employers assisted views.
		/// </summary>
		/// <value>The assist user employers assisted views.</value>
		public IQueryable<AssistUserJobSeekersAssistedView> AssistUserJobSeekersAssistedViews
		{
			get { return Query<AssistUserJobSeekersAssistedView>(); }
		}

		/// <summary>
		/// Gets the user alert settings.
		/// </summary>
		/// <value>The user alerts.</value>
		public IQueryable<UserAlert> UserAlerts
		{
			get { return Query<UserAlert>(); }
		}

		/// <summary>
		/// Gets the active user alert settings, including information on Users.
		/// </summary>
		/// <value>The active user alert views.</value>
		public IQueryable<ActiveUserAlertView> ActiveUserAlertViews
		{
			get { return Query<ActiveUserAlertView>(); }
		}

		/// <summary>
		/// Gets the candidate view.
		/// </summary>
		/// <value>The candidate view.</value>
		public IQueryable<CandidateView> CandidateViews
		{
			get { return Query<CandidateView>(); }
		}

		/// <summary>
		/// Gets the recently placed views.
		/// </summary>
		/// <value>The recently placed views.</value>
		public IQueryable<RecentlyPlacedMatchesToIgnore> RecentlyPlacedMatchesToIgnore
		{
			get { return Query<RecentlyPlacedMatchesToIgnore>(); }
		}

		/// <summary>
		/// Gets the job seeker activity action views.
		/// </summary>
		/// <value>The job seeker activity action views.</value>
		public IQueryable<JobSeekerActivityActionView> JobSeekerActivityActionViews
		{
			get { return Query<JobSeekerActivityActionView>(); }
		}

		/// <summary>
		/// Gets the invitation to a job seeeker to apply.
		/// </summary>
		/// <value>The invite to apply.</value>
		public IQueryable<InviteToApply> InviteToApply
		{
			get { return Query<InviteToApply>(); }
		}

		/// <summary>
		/// Gets the candidate click how to apply view.
		/// </summary>
		/// <value>The candidate click how to apply view.</value>
		public IQueryable<CandidateClickHowToApplyView> CandidateClickHowToApplyView
		{
			get { return Query<CandidateClickHowToApplyView>(); }
		}

		/// <summary>
		/// Gets the candidate did not click how to apply view.
		/// </summary>
		/// <value>The candidate did not click how to apply view.</value>
		public IQueryable<CandidateDidNotClickHowToApplyView> CandidateDidNotClickHowToApplyView
		{
			get { return Query<CandidateDidNotClickHowToApplyView>(); }
		}

		/// <summary>
		/// Gets the recently viewed resume views.
		/// </summary>
		/// <value>The recently viewed resume views.</value>
		public IQueryable<ViewedResumeView> ViewedResumeViews
		{
			get { return Query<ViewedResumeView>(); }
		}

		/// <summary>
		/// Gets the employer search view.
		/// </summary>
		/// <value>
		/// The employer search view.
		/// </value>
		public IQueryable<BusinessUnitSearchView> BusinessUnitSearchViews
		{
			get { return Query<BusinessUnitSearchView>(); }
		}

		/// <summary>
		/// Gets the job seeker profile view.
		/// </summary>
		/// <value>The job seeker profile view.</value>
		public IQueryable<JobSeekerProfileView> JobSeekerProfileViews
		{
			get { return Query<JobSeekerProfileView>(); }
		}

		/// <summary>
		/// Gets the recently placed person match views.
		/// </summary>
		/// <value>The recently placed person match views.</value>
		public IQueryable<RecentlyPlacedPersonMatchView> RecentlyPlacedPersonMatchViews
		{
			get { return Query<RecentlyPlacedPersonMatchView>(); }
		}

		/// <summary>
		/// Gets the employer placements view.
		/// </summary>
		/// <value> The employer placements view. </value>
		public IQueryable<EmployerPlacementsView> EmployerPlacementsViews
		{
			get { return Query<EmployerPlacementsView>(); }
		}

		/// <summary>
		/// Gets the new applicant referral view.
		/// </summary>
		/// <value> The new applicant referral view. </value>
		public IQueryable<NewApplicantReferralView> NewApplicantReferralViews
		{
			get { return Query<NewApplicantReferralView>(); }
		}

		/// <summary>
		/// Gets the job issues view.
		/// </summary>
		/// <value>
		/// The job issues view.
		/// </value>
		public IQueryable<JobIssuesView> JobIssuesViews
		{
			get { return Query<JobIssuesView>(); }
		}

		/// <summary>
		/// Gets the referral views.
		/// </summary>
		/// <value>
		/// The referral views.
		/// </value>
		public IQueryable<ReferralView> ReferralViews
		{
			get { return Query<ReferralView>(); }
		}

		/// <summary>
		/// Gets the job seeker referral all status views.
		/// </summary>
		/// <value>
		/// The job seeker referral all status views.
		/// </value>
		public IQueryable<JobSeekerReferralAllStatusView> JobSeekerReferralAllStatusViews
		{
			get { return Query<JobSeekerReferralAllStatusView>(); }
		}

		/// <summary>
		/// Gets the invite to apply basic views.
		/// </summary>
		/// <value>The invite to apply basic views.</value>
		public IQueryable<InviteToApplyBasicView> InviteToApplyBasicViews
		{
			get { return Query<InviteToApplyBasicView>(); }
		}

		/// <summary>
		/// Gets the employer account activity views.
		/// </summary>
		/// <value> The employer account activity views. </value>
		public IQueryable<EmployerAccountActivityView> EmployerAccountActivityViews
		{
			get { return Query<EmployerAccountActivityView>(); }
		}

        /// <summary>
        /// The jobs sent in alerts.
        /// </summary>
        /// <value>The jobs sent in alerts for job seeker's saved searches.</value>
        public IQueryable<JobsSentInAlerts> JobsSentInAlerts
        { 
            get { return Query<JobsSentInAlerts>(); } 
        }

		#endregion

		#region Stored Procedure Queryable Helpers

		public IQueryable<RecentlyPlacedMatchView> GetRecentlyPlacedPersonMatchesOrderByBusinessUnitNameAsc(long userId, long? businessUnitId, string businessUnitName, bool filterByOffices, long? officeId, int matchCount, int pageNumber, int pageSize, ref int rowCount)
		{
			return null;
		}

		public IQueryable<RecentlyPlacedMatchView> GetRecentlyPlacedPersonMatchesOrderByBusinessUnitNameDesc(long userId, long? businessUnitId, string businessUnitName, bool filterByOffices, long? officeId, int matchCount, int pageNumber, int pageSize, ref int rowCount)
		{
			return null;
		}

		public IQueryable<RecentlyPlacedMatchView> GetRecentlyPlacedPersonMatchesOrderByPlacedPersonNameAsc(long userId, long? businessUnitId, string businessUnitName, bool filterByOffices, long? officeId, int matchCount, int pageNumber, int pageSize, ref int rowCount)
		{
			return null;
		}

		public IQueryable<RecentlyPlacedMatchView> GetRecentlyPlacedPersonMatchesOrderByPlacedPersonNameDesc(long userId, long? businessUnitId, string businessUnitName, bool filterByOffices, long? officeId, int matchCount, int pageNumber, int pageSize, ref int rowCount)
		{
			return null;
		}

		public IQueryable<GetPostingPersonMatchesResult> GetPostingPersonMatches(long postingId, long userId, int pageNumber, int pageSize, ref int rowCount)
		{
			return null;
		}

		public IQueryable<JobSeekerSurveyTotalsView> JobSeekerSurveyTotals(long jobSeekerId)
		{
			return null;
		}

		public IQueryable<GetSpideredEmployersWithGoodMatchesResult> GetSpideredEmployersWithGoodMatchesOrderByPostingCountDesc(long userId, string employerName, int minimumNumberOfPostings, DateTime minimumPostingDate, int pageNumber, int pageSize, ref int rowCount)
		{
			return null;
		}

		public IQueryable<GetSpideredEmployersWithGoodMatchesResult> GetSpideredEmployersWithGoodMatchesOrderByEmployerNameAsc(long userId, string employerName, int minimumNumberOfPostings, DateTime minimumPostingDate, int pageNumber, int pageSize, ref int rowCount)
		{
			return null;
		}

		public IQueryable<GetSpideredEmployersWithGoodMatchesResult> GetSpideredEmployersWithGoodMatchesOrderByEmployerNameDesc(long userId, string employerName, int minimumNumberOfPostings, DateTime minimumPostingDate, int pageNumber, int pageSize, ref int rowCount)
		{
			return null;
		}

		public IQueryable<GetSpideredEmployersWithGoodMatchesResult> GetSpideredEmployersWithGoodMatchesOrderByJobTitleAsc(long userId, string employerName, int minimumNumberOfPostings, DateTime minimumPostingDate, int pageNumber, int pageSize, ref int rowCount)
		{
			return null;
		}

		public IQueryable<GetSpideredEmployersWithGoodMatchesResult> GetSpideredEmployersWithGoodMatchesOrderByJobTitleDesc(long userId, string employerName, int minimumNumberOfPostings, DateTime minimumPostingDate, int pageNumber, int pageSize, ref int rowCount)
		{
			return null;
		}

		public IQueryable<GetSpideredEmployersWithGoodMatchesResult> GetSpideredEmployersWithGoodMatchesOrderByPostingDateAsc(long userId, string employerName, int minimumNumberOfPostings, DateTime minimumPostingDate, int pageNumber, int pageSize, ref int rowCount)
		{
			return null;
		}

		public IQueryable<GetSpideredEmployersWithGoodMatchesResult> GetSpideredEmployersWithGoodMatchesOrderByPostingDateDesc(long userId, string employerName, int minimumNumberOfPostings, DateTime minimumPostingDate, int pageNumber, int pageSize, ref int rowCount)
		{
			return null;
		}

		public IQueryable<GetSpideredPostingsWithGoodMatchesResult> GetSpideredPostingsWithGoodMatchesOrderByPostingDateAsc(long userId, string employerName, DateTime minimumPostingDate, int pageNumber, int pageSize, ref int rowCount)
		{
			return null;
		}

		public IQueryable<GetSpideredPostingsWithGoodMatchesResult> GetSpideredPostingsWithGoodMatchesOrderByPostingDateDesc(long userId, string employerName, DateTime minimumPostingDate, int pageNumber, int pageSize, ref int rowCount)
		{
			return null;
		}

		public IQueryable<GetSpideredPostingsWithGoodMatchesResult> GetSpideredPostingsWithGoodMatchesOrderByJobTitleAsc(long userId, string employerName, DateTime minimumPostingDate, int pageNumber, int pageSize, ref int rowCount)
		{
			return null;
		}

		public IQueryable<GetSpideredPostingsWithGoodMatchesResult> GetSpideredPostingsWithGoodMatchesOrderByJobTitleDesc(long userId, string employerName, DateTime minimumPostingDate, int pageNumber, int pageSize, ref int rowCount)
		{
			return null;
		}

		public IQueryable<StudentActivityReportView> StudentActivityReport(DateTime startDate, DateTime endDate)
		{
			return null;
		}

		public void UpdateSession(Guid sessionId, long userId, DateTime lastRequestOn, bool checkUser)
		{
		}

		#endregion

	}
}
