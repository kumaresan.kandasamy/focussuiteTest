﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Transactions;
using System.Linq;

using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Messages;
using Focus.Core.Messages.AuthenticationService;
using Focus.Core.Models;
using Focus.Core.Models.Career;
using Focus.Core.Settings.Interfaces;
using Focus.Data.Configuration.Entities;
using Focus.Data.Core.Entities;
using Focus.Data.Library.Entities;
using Focus.Data.Migration.Entities;
using Focus.Data.Report.Entities;
using Focus.Data.Repositories;
using Focus.Data.Repositories.Contracts;
using Focus.Services;
using Focus.Services.Core;
using Focus.Services.Helpers;
using Focus.Services.Repositories;
using Focus.Services.Integration;
using Focus.Services.ServiceImplementations;
using Focus.UnitTests.FakeHelpers;
using Framework.Caching;
using Framework.Caching.Providers;
using Framework.Core;
using Framework.Email;
using Framework.Email.Providers;
using Framework.Logging;
using Framework.ServiceLocation;

using Mindscape.LightSpeed;

using Moq;

using NUnit.Framework;

#endregion

namespace Focus.UnitTests.Core
{
	[TestFixture]
	public abstract class TestFixtureBase
	{
		private static readonly LightSpeedContext<ConfigurationUnitOfWork> ConfigurationContext = new LightSpeedContext<ConfigurationUnitOfWork>("Configuration");
		private static readonly LightSpeedContext<LibraryUnitOfWork> LibraryContext = new LightSpeedContext<LibraryUnitOfWork>("Library");
		private static readonly LightSpeedContext<DataCoreUnitOfWork> DataCoreContext = new LightSpeedContext<DataCoreUnitOfWork>("DataCore");
		private static readonly LightSpeedContext<DataMigrationUnitOfWork> DataMigrationContext = new LightSpeedContext<DataMigrationUnitOfWork>("DataMigration");
		private static readonly LightSpeedContext<ReportUnitOfWork> ReportContext = new LightSpeedContext<ReportUnitOfWork>("Report");

		private TransactionScope _transactionScope;

		protected SimpleUnitOfWorkScope<ConfigurationUnitOfWork> ConfigurationUnitOfWorkScope;
		protected SimpleUnitOfWorkScope<LibraryUnitOfWork> LibraryUnitOfWorkScope;
		protected SimpleUnitOfWorkScope<DataCoreUnitOfWork> DataCoreUnitOfWorkScope;
		protected SimpleUnitOfWorkScope<DataMigrationUnitOfWork> MigrationUnitOfWorkScope;
		protected SimpleUnitOfWorkScope<ReportUnitOfWork> ReportUnitOfWorkScope;

		protected IConfigurationRepository ConfigurationRepository;
		protected ILibraryRepository LibraryRepository;
		protected ICoreRepository DataCoreRepository;
		protected ILensRepository LensRepository;
		protected IDocumentRepository DocumentRepository;
		protected IReportRepository ReportRepository;
		protected IMigrationRepository MigrationRepository;
		protected IIntegrationRepository IntegrationRepository;

		protected IConfigurationHelper ConfigurationHelper;
		protected ILookupHelper LookupHelper;
		protected IOccupationHelper OccupationHelper;
		protected IOrganizationHelper OrganizationHelper;
		protected IResumeHelper ResumeHelper;
		protected ISessionHelper SessionHelper;
		protected IMessagingHelper MessagingHelper;
		protected IPostingHelper PostingHelper;
		protected IJobDevelopmentHelper JobDevelopmentHelper;
		protected ILoggingHelper LoggingHelper;
		protected ILocalisationHelper LocalisationHelper;
		protected IAlertsHelper AlertsHelper;
		protected IEmailHelper EmailHelper;
		protected ISearchHelper SearchHelper;
		protected IOfficeHelper OfficeHelper;
		protected ICandidateHelper CandidateHelper;
		protected IEmployerHelper EmployerHelper;
		protected IEmployeeHelper EmployeeHelper;
		protected ISelfServiceHelper SelfServiceHelper;
		protected IValidationHelper ValidationHelper;
		protected IEncryptionHelper EncryptionHelper;
		protected IPushNotificationHelper PushNotificationHelper;
		protected IUserHelper UserHelper;

		protected IRuntimeContext RuntimeContext;

		private Mock<ILogProvider> _mockLogProvider;

		protected const string CachePrefix = "Focus.Unit.Tests";
		protected const string ApplicationName = "Focus Unit Tests";
		protected const string ErrorEmailTo = "unittest.errors@burning-glass.com";

		#region Mocked data

		public static UserContext MockUserData;
		public static UserContext MockCareerUserData;
		public static UserContext MockImpersonatedCareerUserData;
		public static Guid MockSessionId;
		public static readonly Guid MockRequestId = new Guid("00000000-2222-0000-0000-000000000000");
		public static readonly long MockUserId = 2;
		public static readonly long MockCareerUserId = 1194832;
		public static readonly long MockTalentUserId = 17370;
		public static readonly long MockPersonId = 1;
		public static readonly long MockCareerPersonId = 1194834;
		public static readonly string MockUsername = "dev@client.com";
		public static readonly string MockCareerUsername = "wf@bgt.com";
		public static readonly string MockTalentUsername = "dev@employer.com";
		public static readonly string MockNewUserName = "tester@tester.com";
		public static readonly string MockPassword = "password";
		public static readonly string MockValidPassword = "Passw0rd";
		public static readonly string MockFirstName = "Dev";
		public static readonly string MockCareerFirstName = "Work";
		public static readonly string MockTalentFirstName = "DevClientEmp";
		public static readonly string MockLastName = "Client";
		public static readonly string MockCareerLastName = "Force";
		public static readonly string MockTalentLastName = "ClientEmployee";
		public static readonly string MockEmailAddress = "dev@client.com";
		public static readonly string MockCareerEmailAddress = "wf@bgt.com";
		public static readonly string MockTalentEmailAddress = "client@Employee.com";
		public static readonly string MockApplication = "Dev - Local";
		public static readonly string MockHost = "localhost";
		public static readonly string MockCulture = "**-**";
		public static readonly string MockNewUserName2 = "tester2@tester2.com";
		public static readonly string MockPassword2 = "password";
		public static readonly string MockValidPassword2 = "Passw0rd";

		public static readonly string MockClientTag = "ABC123";
		public static readonly long MockCandidateId = 1;

		public static IAppSettings MockAppSettings = new FakeAppSettings();

		public static readonly string[] MockRoles = new string[0];
		public static readonly long MockEmployerId = 29441;
		public static readonly long MockEmployeeId = 29452;
		public static readonly long MockOwnershipTypeId = 1714865;

		public static readonly EmployerDto MockNewEmployer = new EmployerDto
		{
			AlternatePhone2 = "123456789",
			PrimaryPhone = "123456789",
			AlternatePhone1 = "123456789",
			Name = "DevEmployer",
			FederalEmployerIdentificationNumber = "01-2345678",
			ExpiredOn = DateTime.Now.AddYears(1),
			CommencedOn = DateTime.Now,
			IsRegistrationComplete = false,
			ApprovalStatus = ApprovalStatuses.WaitingApproval,
			OwnershipTypeId = MockOwnershipTypeId,
			IndustrialClassification = "TestIndustry",
			StateEmployerIdentificationNumber = "12-3456789",
			TermsAccepted = false,
			Url = "http://www.test-employer.com"
		};

		public static readonly BusinessUnitDto MockNewBusinessUnit = new BusinessUnitDto
		{
			AlternatePhone2 = "123456789",
			PrimaryPhone = "123456789",
			AlternatePhone1 = "123456789",
			Name = "DevEmployerBusinessUnit",
			OwnershipTypeId = MockOwnershipTypeId,
			IndustrialClassification = "TestIndustry",
			Url = "http://www.test-employer.com",
			IsPrimary = true
		};

		public static readonly PersonDto MockNewEmployeePerson = new PersonDto
		{
			FirstName = "Test",
			MiddleInitial = "A",
			LastName = "Person",
			DateOfBirth = DateTime.Now.AddYears(-21),
			JobTitle = "TestJob",
			SocialSecurityNumber = "078-05-1120",
			TitleId = 12780
		};

		public static readonly PersonDto MockNewEmployeePerson2 = new PersonDto
		{
			FirstName = "Test2",
			MiddleInitial = "A2",
			LastName = "Person2",
			DateOfBirth = DateTime.Now.AddYears(-31),
			JobTitle = "TestJob2",
			SocialSecurityNumber = "078-05-1120",
			TitleId = 12780
		};

		public static readonly Address MockAddressForJobseeker = new Address
		{
			Street1 = "JobseekerLine1",
			Street2 = "JobseekerLine2",
			City = "JobseekerTownCity",
			CountyId = 0,
			Zip = "JobseekerPostcodeZip",
			CountryId = 0,
			StateId = 0
		};

		public static readonly Address MockJobSeekerAddress = MockAddressForJobseeker;

		public static readonly Address MockAddressForJobseekerWithAssignedOffice = new Address
		{
			Street1 = "JobseekerLine1",
			Street2 = "JobseekerLine2",
			City = "JobseekerTownCity",
			CountyId = 0,
			Zip = "11111",
			CountryId = 0,
			StateId = 0
		};

		public static readonly Address MockJobSeekerAddressWithAssignedOffice = MockAddressForJobseekerWithAssignedOffice;

		public static readonly Address MockAddressForJobseekerWithUnAssignedOffice = new Address
		{
			Street1 = "JobseekerLine1",
			Street2 = "JobseekerLine2",
			City = "JobseekerTownCity",
			CountyId = 0,
			Zip = "98765",
			CountryId = 0,
			StateId = 0
		};

		public static readonly Address MockJobSeekerAddressWithUnAssignedOffice = MockAddressForJobseekerWithUnAssignedOffice;

		public static readonly EmployerAddress MockAddressForEmployer = new EmployerAddress
		{
			Line1 = "EmployeeLine1",
			Line2 = "EmployeeLine2",
			Line3 = "EmployeeLine3",
			TownCity = "EmployeeTownCity",
			CountyId = 0,
			PostcodeZip = "EmployeePostcodeZip",
			CountryId = 0,
			StateId = 0,
			IsPrimary = true
		};

		public static readonly EmployerAddress MockEmployerAddress = MockAddressForEmployer;

		public static readonly EmployerAddress MockAddressForEmployerWithAssignedOffice = new EmployerAddress
		{
			Line1 = "EmployeeLine1",
			Line2 = "EmployeeLine2",
			Line3 = "EmployeeLine3",
			TownCity = "EmployeeTownCity",
			CountyId = 0,
			PostcodeZip = "11111",
			CountryId = 0,
			StateId = 0,
			IsPrimary = true
		};

		public static readonly EmployerAddress MockEmployerAddressWithAssignedOffice =
			MockAddressForEmployerWithAssignedOffice;

		public static readonly EmployerAddress MockAddressForEmployerWithMultiAssignedOffice = new EmployerAddress
		{
			Line1 = "EmployeeLine1",
			Line2 = "EmployeeLine2",
			Line3 = "EmployeeLine3",
			TownCity = "EmployeeTownCity",
			CountyId = 0,
			PostcodeZip = "74653",
			CountryId = 0,
			StateId = 0,
			IsPrimary = true
		};

		public static readonly EmployerAddress MockEmployerAddressWithMultiAssignedOffice =
			MockAddressForEmployerWithMultiAssignedOffice;

		public static readonly PersonAddress MockAddressForEmployee = new PersonAddress
		{
			Line1 = "EmployeeLine1",
			Line2 = "EmployeeLine2",
			Line3 = "EmployeeLine3",
			TownCity = "EmployeeTownCity",
			CountyId = 0,
			PostcodeZip = "EmployeePostcodeZip",
			CountryId = 0,
			StateId = 0,
			IsPrimary = true
		};

		public static readonly PersonAddress MockEmployeeAddress = MockAddressForEmployee;

		public static readonly BusinessUnitAddress MockAddressForBusinessUnit = new BusinessUnitAddress
		{
			Line1 = "BusinessUnitLine1",
			Line2 = "BusinessUnitLine2",
			Line3 = "BusinessUnitLine3",
			TownCity = "EBusinessUnitTownCity",
			CountyId = 0,
			PostcodeZip = "BUnitPostcodeZip",
			CountryId = 0,
			StateId = 0,
			IsPrimary = true
		};

		public static readonly EmployerAddress MockAddressForEmployerWithAssignedZip = new EmployerAddress
		{
			Line1 = "EmployerLine1",
			Line2 = "EmployerLine2",
			Line3 = "EmployerLine3",
			TownCity = "EmployerTownCity",
			CountyId = 0,
			PostcodeZip = "11111",
			CountryId = 0,
			StateId = 0,
			IsPrimary = true
		};

		public static readonly PushNotification MockPushNotification = new PushNotification
		{
			Id = 99999991,
			Title = "Test Notification",
			Text = "This is a test notification message",
			Sent = true,
			PersonMobileDeviceToken = "TEST123"
		};


		public static readonly BusinessUnitAddress MockBusinessUnitAddress = MockAddressForBusinessUnit;

		#endregion

		#region MockResumeXml

		internal const string MockResumeXml = @"<ResDoc>
	<resume canonversion='2' dateversion='2' iso8601='2012-07-10' present='734696' xml:space='preserve'>
		<header>
			<resumeid>1192112</resumeid>
			<completionstatus>16</completionstatus>
			<canonversion>3</canonversion>
			<default>1</default>
			<updatedon>2012-07-10</updatedon>
			<resumename>test resume name</resumename>
			<onet>11-2022.91</onet>
			<ronet>11-2022.92</ronet>
			<status>2</status>
		</header>
		<contact>
			<name>
				<givenname>SANDRA</givenname>
				<givenname>S</givenname>. 
				<surname>BUONARIGO</surname>
			</name>
			, CPA
			<address lat='33.7873' lon='117.844' majorcity='ORANGE' state='CA'>
				<street>1025 E. Chalynn Ave.</street>
				<street>Test street 2</street>
				<town>Test town</town>
				<city>Orange</city>, 
				<state abbrev='CA'>CA</state> 
				<state_fullname>California</state_fullname>
				<postalcode>92866</postalcode>
				<country>US</country>
				<country_fullname>United States Of America</country_fullname>
			</address>
			<email>test@email.com</email>
			<phone type='home'>(714) HOME</phone> home
			<phone type='cell'>(714) CELL</phone> cell
			<phone type='work' ext='1234'>(714) WORK</phone> work
		</contact>

		<summary include='1'>
			<summary include='1'>Test summary</summary>
			OBJECTIVE
			<objective>Responsible career position in Accounting or Finance of a stable company in Orange County.</objective>
		</summary>
		<statements>QUALIFICATIONS
			<activities>Certified Public Accountant-California State Board of Accountancy.Certified Public Accountant-Arizona State Board of Accountancy (inactive).</activities>
			<honors>Test statements honors</honors>
			<personal>
				<dob>12.06.1973</dob>
				<sex>2</sex>
				<userinfo>
					<ssn>Test SSN</ssn>
				</userinfo>
				<ethnic_heritages>
					<ethnic_heritage>1</ethnic_heritage>
				</ethnic_heritages>
				<citizen_flag>1</citizen_flag>
				<migrant_flag>0</migrant_flag>
				<disability_status>1</disability_status>
				<disability_category_cd>4</disability_category_cd>
				<alien_expires>01.01.2090</alien_expires>
				<alien_id>Test alien id</alien_id>
				<alien_perm_flag>1</alien_perm_flag>
				<crc>1</crc>
				<license>
					<driver_flag>1</driver_flag>
					<driver_class>1</driver_class>
					<state_fullname>Test state full name</state_fullname>
					<drv_airbrake_flag>1</drv_airbrake_flag>
				</license>
				<veteran>
					<vet_flag>1</vet_flag>
					<vet_start_date>01.01.1990</vet_start_date>
					<vet_end_date>01.01.1998</vet_end_date>
					<campaign_vet_flag>1</campaign_vet_flag>
					<vet_disability_status>2</vet_disability_status>
					<vet_tsm_type_cd>2</vet_tsm_type_cd>
					<vet_era>1</vet_era>
					<vet_discharge>Honorable</vet_discharge>
					<branch_of_service>USARMY</branch_of_service>
					<rank>018</rank>
					<moc>Test MOC</moc>
					<unit_affiliation>Test unit affiliation</unit_affiliation>
					<mostext>Sniper</mostext>
					<mosonetcrosswalk>Test mos onet crosswalk</mosonetcrosswalk>
					<past_five_years_veteran>1</past_five_years_veteran>
				</veteran>
				<preferences>
					<resume_searchable>1</resume_searchable>
					 <shift>
							<shift_first_flag>1</shift_first_flag>
							<shift_rotating_flag>1</shift_rotating_flag>
							<work_week>3</work_week>
							<work_type>6</work_type>
							<work_over_time>1</work_over_time>
					 </shift>
				</preferences>
			</personal>
		</statements>
		<special>
			<snapshot>Test snapshot</snapshot>
			<automatedsummary custom='1'>Test automated summary</automatedsummary>
			<preserved_order>Test preserved order</preserved_order>
			<preserved_format>Standard</preserved_format>
		</special>
		<preferences>
			<sal>30000</sal>
			<salary_unit_cd>5</salary_unit_cd>
			<resume_searchable>1</resume_searchable>
			<relocate>1</relocate>
			<email_flag>1</email_flag>
			<postal_flag>1</postal_flag>
			<phone_pri_flag>1</phone_pri_flag>
			<phone_sec_flag>1</phone_sec_flag>
			<fax_flag>1</fax_flag>
			<desired_zips>
				<desired_zip>
					<zip>Test zip 1</zip>
					<radius>100</radius>
					<statecode>CA</statecode>
					<choice>1</choice>
				</desired_zip>
				<desired_zip>
					<zip>Test zip 2</zip>
					<radius>25</radius>
					<statecode>OK</statecode>
					<choice>2</choice>
				</desired_zip>
			 </desired_zips>
		</preferences>
		<experience end='731126' start='724033'>EXPERIENCE
			<employment_status_cd>1</employment_status_cd>
			<job end='731126' id='1' pos='1' start='729908'><daterange><start days='729908' iso8601='1999-06-01'>June, 1999</start> to <end days='731126' iso8601='2002-10-01'>October, 2002</end></daterange>
			<description><skill>CONTROLLER</skill>, DPR Construction, a commercial building contractor specializing in technically demanding projects in the microelectronics, biopharmaceutical, health care. Responsibilities included overseeing all facets of <skill>accounting</skill> through <skill>Financial Statements</skill>.  Member of the Management Team, directly supervised staff and oversaw the monthly <skill>AIA</skill> billings, collections, and subsequent payments to subcontractors for as many as 40 jobs at one time.  Oversaw <skill>human resources</skill> and semi-monthly payroll. Responsible for banking relations, <skill>cash management</skill>, budgeting, leases, property taxes, and orchestrating the annual audit/review.  Established, implemented and documented all <skill>accounting</skill> procedures. Directly communicated with President, VPs, Project Managers, and Superintendents on a regular basis. Became well versed in American Contractor and <skill>Prolog</skill>.</description></job>

			<job end='729908' id='2' pos='2' sic='931102' sic2='93' start='729482'><daterange><start days='729482' iso8601='1998-04-01'>April, 1998</start> to <end days='729908' iso8601='1999-06-01'>June, 1999</end></daterange>
			<employer>CONTROLLER</employer>, <description>Koll Construction, L.P., an International General Contractor based in Newport Beach, <skill>CA</skill>.  Responsibilities included overseeing all facets of <skill>accounting</skill> through <skill>Financial Statements</skill> including <skill>accounting</skill> for five joint ventures, an affiliated company, and Koll Construction Asia, Ltd., a wholly owned subsidiary in Asia requiring a knowledge of multi-currency regulations.  Directly supervised a full staff and oversaw the monthly <skill>AIA</skill> billings, collections, and subsequent payments to subcontractors for as many as 40 jobs at one time. These jobs ranged from $1 million to $80 million in contract value. Responsible for licensing, banking relations, <skill>cash management</skill>, budgeting, leases, property taxes, use taxes, and orchestrating the annual audit.  Directly communicated with Owners, Divisional VPs and Project Managers on a daily basis and traveled to job sites as required.  Participated in a <skill>Y2K</skill> compliant software selection process and <skill>conversion</skill> to <skill>J.D. Edwards</skill>. Became proficient at <skill>Microsoft Outlook</skill>.</description></job>

			<job end='729482' id='3' pos='3' sic='154200' sic2='15' start='728327'><daterange><start days='728327' iso8601='1995-02-01'>February, 1995</start> to <end days='729482' iso8601='1998-04-01'>April, 1998</end></daterange>
			<title>DIRECTOR OF ACCOUNTING</title>, <employer>MBK Construction Ltd</employer>. (formerly <employer>Birtcher Construction Limited</employer>), <description>the Nation's largest builder of theatres (<skill>Sales</skill> volume of $305 million).  Responsibilities included overseeing all facets of <skill>accounting</skill> through <skill>Financial Statements</skill>. Was responsible for overseeing the monthly <skill>AIA</skill> billings, collections, and subsequent payments to subcontractors for as many as 50 jobs at any one time.  These jobs ranged from $2 million to $100 million in contract value. I directly supervised a staff of 10 people.  Participated in <skill>Y2K</skill> compliant software selection and <skill>conversion</skill>. Originally hired to oversee all facets of <skill>accounting</skill> in Billion dollar AMC National <skill>Theatre</skill> Expansion Program and to get MBK licensed from every standpoint in each state (17 in total) in which a project was to be built.  Also responsible for knowing the lien laws and sales tax laws in each state as well as filing all necessary forms, reports, and returns.  Became well versed in <skill>Ami Pro</skill>, <skill>Word Perfect</skill>, <skill>Word</skill>, <skill>Lotus</skill>, <skill>Excel</skill>, and <skill>J.D. Edwards</skill>.</description></job>

			<job end='728265' id='4' pos='4' sic='931102' sic2='93' start='727931'><daterange><start days='727931' iso8601='1994-01-01'>January, 1994</start> to <end days='728265' iso8601='1994-12-01'>December, 1994</end></daterange>
			<employer>CONTROLLER</employer>, <description>Besteel Industries, a national leading manufacturer of modular buildings and canopies primarily for the petroleum and fast food industries.  Hired in post Chapter 11 period to reorganize and reduce expenses without sacrificing quality or efficiency.  Responsibilities included overseeing all facets of <skill>accounting</skill> and <skill>management information system</skill> departments (up to eight people), preparing and maintaining short and long term <skill>cash flow projections</skill>, monthly budget to actual analysis, job cost profit analysis, and generating/maintaining various <skill>spreadsheet</skill> files.  Also responsible for banking relations, <skill>negotiating</skill> taxes, leases, payroll and employee benefits, product pricing, <skill>risk management</skill>, and required monthly reporting for U.S. Bankruptcy Courts.  Saw Corporation through asset sale to new Limited Liability Company, which involved writing value of assets down to Fair <skill>Market</skill> Value using APB 16.</description></job>

			<job end='727931' id='5' pos='5' sic='931102' sic2='93' start='725586'><daterange><start days='725586' iso8601='1987-08-01'>August, 1987</start> to <end days='727931' iso8601='1994-01-01'>January, 1994</end></daterange>
			<employer>CONTROLLER</employer>, <description>The Fitch Corporation, an Orange County based lath &amp; plaster subcontractor licensed in Arizona, California, Nevada, and Washington. Responsibilities included overseeing all facets of <skill>accounting</skill> through <skill>financial statements</skill> for four divisions, preparing monthly jobcost reports for an average of 40 jobs in process, <skill>supervising</skill> weekly payrolls of up to 300 employees and <skill>accounts payable</skill> of over 500 vendors.  Successfully oversaw <skill>automation</skill> of <skill>accounting</skill> system and <skill>networking</skill> of computer system. Saw Corporation through no-change IRS and State payroll audits.  Wrote an automated program for all billing and lien releases.</description></job>

			<job end='725555' id='6' pos='6' sic='738909' sic2='73' start='725221'><daterange><start days='725221' iso8601='1986-08-01'>August, 1986</start> to <end days='725555' iso8601='1987-07-01'>July, 1987</end></daterange>
			<employer>CHIEF FINANCIAL OFFICER, Ven-Tek Management Company</employer>, <address><city>Orange</city>, <state abbrev='CA'>CA.</state></address>  <description>Responsibilities included starting up company from initialization, automating <skill>accounting</skill> system for six subsidiaries, <skill>supervising</skill> all aspects of <skill>accounting</skill> through <skill>financial statements</skill>, preparing investment proposals, <skill>business plans</skill> and projections.</description></job>

			<job end='725190' id='7' pos='7' sic='651300' sic2='65' start='724795'><daterange><start days='724795' iso8601='1985-06-01'>June, 1985</start> to <end days='725190' iso8601='1986-07-01'>July, 1986</end></daterange>
			<employer>CONTROLLER, Crain &amp; Associates Management Company</employer>, <address><city>Tucson</city>, <state abbrev='AZ'>AZ.</state></address>  <description>Responsibilities included automating <skill>accounting</skill> system for eight different partnerships, initializing several partnerships, including a construction company, which involved designing a draw request with Wells Fargo, <skill>supervising</skill> all aspects of <skill>accounting</skill> through presentation of <skill>financial statements</skill> to investors, and <skill>programming</skill> in <skill>Dbase</skill>.</description></job>

			<job end='724795' id='8' pos='8' sic='872102' sic2='87' start='724033'><daterange><start days='724033' iso8601='1983-05-01'>May, 1983</start> to <end days='724795' iso8601='1985-06-01'>June, 1985</end></daterange>
			<employer>ACCOUNTANT, Ernst &amp; Whinney</employer>, <address><city>Tucson</city>, <state abbrev='AZ'>AZ.</state></address>  <description>Responsibilities included tax-work, audit procedures, <skill>management consultant</skill> services, preparation of <skill>financial statements</skill> for clients in the manufacturing and healthcare industries.</description></job>
		</experience>
		<education>EDUCATION
			<school id='9'>
				<completiondate>06/1993</completiondate>
				<degree level='25' name='Bachelor of Science'>BS</degree> in 
				<major code='4201'>Business Administration</major>, 
				<institution>University of Arizona</institution>.  Majors in 
				<major code='4001 4303'>Accounting and Management Information Systems</major>. GPA of 
				<gpa max='4' value='3.2'>3.2</gpa>=A). 
				<description>Completely supported self through school.</description>
			</school>
			<school id='10'>
				<expectedcompletiondate>06/2013</expectedcompletiondate>				
				<address>
					<city>Test school city</city>
					<county>21001</county>
					<county_fullname>Test school city</county_fullname>
					<state>OK</state>
					<state_fullname>Oklahoma</state_fullname>
					<country>US</country>
					<country_fullname>United States Of America</country_fullname>
				</address>
				<activities>Test activities</activities>
				<institution>SCHOLARSHIPS</institution>
				Received academic scholarship from 
				<institution>University of Arizona</institution> (for maintaining GPA of 
				<gpa max='4' value='3.5'>3.5</gpa>), 
				<daterange>
					<start days='722452' iso8601='1979-01-01'>1979</start>-
					<end days='723548' iso8601='1982-01-01'>1982</end>
				</daterange>
			</school>.
			<courses>Test education courses</courses>
			<school_status_cd>0</school_status_cd>
			<norm_edu_level_cd>14</norm_edu_level_cd>
			<targetindustries>
				<industrycode>72110</industrycode>
				<industrycode>72111</industrycode>
				<industrycode>72112</industrycode>
			</targetindustries>
			<mostexperienceindustries>
				<industrycode>72112</industrycode>
				<industrycode>72113</industrycode>
			</mostexperienceindustries>
		</education>
		<professional>ORGANIZATION INVOLVEMENT
		<affiliations>Current Treasurer for the OC Chapter of CFMA (Construction Financial Mgmt. Assoc.)
		Current Member of AICPA (American Institute of Certified Public Accountants)
		Member of NAFE (National Assoc. of Female Executives)</affiliations></professional>
		<references>REFERENCES Available upon request.</references>
	</resume>
	<skills>
		<skills>
			Test skill 1
		</skills>
		<skills>
			Test skill 2
		</skills>
		<languages>
			<languages_profiency>Test language proficiency 1</languages_profiency>
			<languages_profiency>Test language proficiency 2</languages_profiency>
		</languages>
		<courses>
			<certifications >
				<certification>Test certification 1</certification>
				<certification>Test certification 2</certification>
			</certifications>
		</courses>
	</skills>
	<skillrollup version='1'>
		<canonskill end='731126' experience='1' expidrefs='1,2,3,4,5,6,7' idrefs='1,2,3,4,5,6,7' lastused='2002' name='accountancy' start='724795'>
			<variant>accounting</variant>
		</canonskill>
		<canonskill end='727931' experience='1' expidrefs='5' idrefs='5' lastused='1994' name='accounts payable' start='725586'>
			<variant>accounts payable</variant>
		</canonskill>
		<canonskill end='731126' experience='1' expidrefs='1,2,3' idrefs='1,2,3' lastused='2002' name='aia' start='728327'>
			<variant>AIA</variant>
		</canonskill>
		<canonskill end='729482' experience='1' expidrefs='3' idrefs='3' lastused='1998' name='amipro' start='728327'>
			<variant>Ami Pro</variant>
		</canonskill>
		<canonskill end='727931' experience='1' expidrefs='5' idrefs='5' lastused='1994' name='automation' start='725586'>
			<variant>automation</variant>
		</canonskill>
		<canonskill end='731126' experience='1' expidrefs='1,2,4' idrefs='1,2,4' lastused='2002' name='banking' start='727931'>
			<variant>banking</variant>
		</canonskill>
		<canonskill end='728265' experience='1' expidrefs='4' idrefs='4' lastused='1994' name='benefits administration' start='727931'>
			<variant>benefits</variant>
		</canonskill>
		<canonskill end='727931' experience='1' expidrefs='5' idrefs='5' lastused='1994' name='billing' start='725586'>
			<variant>billing</variant>
		</canonskill>
		<canonskill end='731126' experience='1' expidrefs='1,2,3' idrefs='1,2,3' lastused='2002' name='billing systems' start='728327'>
			<variant>billings</variant>
		</canonskill>
		<canonskill end='731126' experience='1' expidrefs='1,2,4' idrefs='1,2,4' lastused='2002' name='budgeting' start='727931'>
			<variant>budgeting</variant>
			<variant>budget</variant>
		</canonskill>
		<canonskill end='725555' experience='1' expidrefs='6' idrefs='6' lastused='1987' name='business plans' start='725221'>
			<variant>business plans</variant>
		</canonskill>
		<canonskill end='728265' experience='1' expidrefs='4' idrefs='4' lastused='1994' name='cash flow' start='727931'>
			<variant>cash flow projections</variant>
		</canonskill>
		<canonskill end='731126' experience='1' expidrefs='1,2' idrefs='1,2' lastused='2002' name='cash management' start='729482'>
			<variant>cash management</variant>
		</canonskill>
		<canonskill end='729908' experience='1' expidrefs='2' idrefs='2' lastused='1999' name='computer associates packages' start='729482'>
			<variant>CA</variant>
		</canonskill>
		<canonskill end='731126' experience='1' expidrefs='1' idrefs='1' lastused='2002' name='control' start='729908'>
			<variant>CONTROLLER</variant>
		</canonskill>
		<canonskill end='729908' experience='1' expidrefs='2,3' idrefs='2,3' lastused='1999' name='conversion' start='728327'>
			<variant>conversion</variant>
		</canonskill>
		<canonskill end='725190' experience='1' expidrefs='7' idrefs='7' lastused='1986' name='corel draw' start='724795'>
			<variant>draw</variant>
		</canonskill>
		<canonskill end='724795' experience='1' expidrefs='8' idrefs='8' lastused='1985' name='customer relations' start='724033'>
			<variant>clients</variant>
		</canonskill>
		<canonskill end='725190' experience='1' expidrefs='7' idrefs='7' lastused='1986' name='dbase' start='724795'>
			<variant>Dbase</variant>
		</canonskill>
		<canonskill end='725190' experience='1' expidrefs='7' idrefs='7' lastused='1986' name='design' start='724795'>
			<variant>designing</variant>
		</canonskill>
		<canonskill end='728265' experience='1' expidrefs='4' idrefs='4' lastused='1994' name='fast' start='727931'>
			<variant>fast</variant>
		</canonskill>
		<canonskill end='729482' experience='1' expidrefs='3' idrefs='3' lastused='1998' name='file management' start='728327'>
			<variant>filing</variant>
		</canonskill>
		<canonskill end='731126' experience='1' expidrefs='1,2,3,5,6,7,8' idrefs='1,2,3,5,6,7,8' lastused='2002' name='financial statements' start='724033'>
			<variant>Financial Statements</variant>
			<variant>financial statements</variant>
		</canonskill>
		<canonskill end='729482' experience='1' expidrefs='3' idrefs='3' lastused='1998' name='forms' start='728327'>
			<variant>forms</variant>
		</canonskill>
		<canonskill end='731126' experience='1' expidrefs='1' idrefs='1' lastused='2002' name='human resources' start='729908'>
			<variant>human resources</variant>
		</canonskill>
		<canonskill end='729908' experience='1' expidrefs='2,3' idrefs='2,3' lastused='1999' name='jd edwards' start='728327'>
			<variant>J.D. Edwards</variant>
		</canonskill>
		<canonskill end='729482' experience='1' expidrefs='3' idrefs='3' lastused='1998' name='lotus' start='728327'>
			<variant>Lotus</variant>
		</canonskill>
		<canonskill end='724795' experience='1' expidrefs='8' idrefs='8' lastused='1985' name='management consulting' start='724033'>
			<variant>management consultant</variant>
		</canonskill>
		<canonskill end='728265' experience='1' expidrefs='4' idrefs='4' lastused='1994' name='marketing' start='727931'>
			<variant>Market</variant>
		</canonskill>
		<canonskill end='729482' experience='1' expidrefs='3' idrefs='3' lastused='1998' name='microsoft excel' start='728327'>
			<variant>Excel</variant>
		</canonskill>
		<canonskill end='729908' experience='1' expidrefs='2' idrefs='2' lastused='1999' name='microsoft outlook' start='729482'>
			<variant>Microsoft Outlook</variant>
		</canonskill>
		<canonskill end='729482' experience='1' expidrefs='3' idrefs='3' lastused='1998' name='microsoft word' start='728327'>
			<variant>Word</variant>
		</canonskill>
		<canonskill end='728265' experience='1' expidrefs='4' idrefs='4' lastused='1994' name='mis' start='727931'>
			<variant>management information system</variant>
		</canonskill>
		<canonskill end='728265' experience='1' expidrefs='4' idrefs='4' lastused='1994' name='negotiation' start='727931'>
			<variant>negotiating</variant>
		</canonskill>
		<canonskill end='727931' experience='1' expidrefs='5' idrefs='5' lastused='1994' name='networking' start='725586'>
			<variant>networking</variant>
		</canonskill>
		<canonskill end='731126' experience='1' expidrefs='1,4,5' idrefs='1,4,5' lastused='2002' name='payroll processing' start='725586'>
			<variant>payroll</variant>
		</canonskill>
		<canonskill end='728265' experience='1' expidrefs='4' idrefs='4' lastused='1994' name='pricing' start='727931'>
			<variant>pricing</variant>
		</canonskill>
		<canonskill end='728265' experience='1' expidrefs='4' idrefs='4' lastused='1994' name='profit' start='727931'>
			<variant>profit</variant>
		</canonskill>
		<canonskill end='725190' experience='1' expidrefs='7' idrefs='7' lastused='1986' name='programming' start='724795'>
			<variant>programming</variant>
		</canonskill>
		<canonskill end='731126' experience='1' expidrefs='1' idrefs='1' lastused='2002' name='prolog' start='729908'>
			<variant>Prolog</variant>
		</canonskill>
		<canonskill end='725555' experience='1' expidrefs='6' idrefs='6' lastused='1987' name='proposal writing' start='725221'>
			<variant>proposals</variant>
		</canonskill>
		<canonskill end='728265' experience='1' expidrefs='4' idrefs='4' lastused='1994' name='quality' start='727931'>
			<variant>quality</variant>
		</canonskill>
		<canonskill end='728265' experience='1' expidrefs='4' idrefs='4' lastused='1994' name='reports' start='727931'>
			<variant>reporting</variant>
		</canonskill>
		<canonskill end='728265' experience='1' expidrefs='4' idrefs='4' lastused='1994' name='risk management' start='727931'>
			<variant>risk management</variant>
		</canonskill>
		<canonskill end='729482' experience='1' expidrefs='3' idrefs='3' lastused='1998' name='sales' start='728327'>
			<variant>Sales</variant>
			<variant>sales</variant>
		</canonskill>
		<canonskill end='728265' experience='1' expidrefs='4' idrefs='4' lastused='1994' name='spreadsheets' start='727931'>
			<variant>spreadsheet</variant>
		</canonskill>
		<canonskill end='727931' experience='1' expidrefs='5,6,7' idrefs='5,6,7' lastused='1994' name='supervisory skills' start='724795'>
			<variant>supervising</variant>
		</canonskill>
		<canonskill end='731126' experience='1' expidrefs='1,2,3,4,8' idrefs='1,2,3,4,8' lastused='2002' name='tax planning' start='724033'>
			<variant>tax</variant>
			<variant>taxes</variant>
		</canonskill>
		<canonskill end='729482' experience='1' expidrefs='3' idrefs='3' lastused='1998' name='theatre' start='728327'>
			<variant>Theatre</variant>
		</canonskill>
		<canonskill end='729482' experience='1' expidrefs='3' idrefs='3' lastused='1998' name='wordperfect' start='728327'>
			<variant>Word Perfect</variant>
		</canonskill>
		<canonskill end='729908' experience='1' expidrefs='2,3' idrefs='2,3' lastused='1999' name='year 2000' start='728327'>
			<variant>Y2K</variant>
		</canonskill>
	</skillrollup>
</ResDoc>
";

		internal const string MockResumeWithLanguageProficienciesXml = @"<ResDoc>
	<resume canonversion='2' dateversion='2' iso8601='2012-07-10' present='734696' xml:space='preserve'>
		<header>
			<resumeid>1192112</resumeid>
			<completionstatus>16</completionstatus>
			<canonversion>3</canonversion>
			<default>1</default>
			<updatedon>2012-07-10</updatedon>
			<resumename>test resume name</resumename>
			<onet>11-2022.91</onet>
			<ronet>11-2022.92</ronet>
			<status>2</status>
		</header>
		<contact>
			<name>
				<givenname>SANDRA</givenname>
				<givenname>S</givenname>. 
				<surname>BUONARIGO</surname>
			</name>
			, CPA
			<address lat='33.7873' lon='117.844' majorcity='ORANGE' state='CA'>
				<street>1025 E. Chalynn Ave.</street>
				<street>Test street 2</street>
				<town>Test town</town>
				<city>Orange</city>, 
				<state abbrev='CA'>CA</state> 
				<state_fullname>California</state_fullname>
				<postalcode>92866</postalcode>
				<country>US</country>
				<country_fullname>United States Of America</country_fullname>
			</address>
			<email>test@email.com</email>
			<phone type='home'>(714) HOME</phone> home
			<phone type='cell'>(714) CELL</phone> cell
			<phone type='work' ext='1234'>(714) WORK</phone> work
		</contact>

		<summary include='1'>
			<summary include='1'>Test summary</summary>
			OBJECTIVE
			<objective>Responsible career position in Accounting or Finance of a stable company in Orange County.</objective>
		</summary>
		<statements>QUALIFICATIONS
			<activities>Certified Public Accountant-California State Board of Accountancy.Certified Public Accountant-Arizona State Board of Accountancy (inactive).</activities>
			<honors>Test statements honors</honors>
			<personal>
				<dob>12.06.1973</dob>
				<sex>2</sex>
				<userinfo>
					<ssn>Test SSN</ssn>
				</userinfo>
				<ethnic_heritages>
					<ethnic_heritage>1</ethnic_heritage>
				</ethnic_heritages>
				<citizen_flag>1</citizen_flag>
				<migrant_flag>0</migrant_flag>
				<disability_status>1</disability_status>
				<disability_category_cd>4</disability_category_cd>
				<alien_expires>01.01.2090</alien_expires>
				<alien_id>Test alien id</alien_id>
				<alien_perm_flag>1</alien_perm_flag>
				<crc>1</crc>
				<license>
					<driver_flag>1</driver_flag>
					<driver_class>1</driver_class>
					<state_fullname>Test state full name</state_fullname>
					<drv_airbrake_flag>1</drv_airbrake_flag>
				</license>
				<veteran>
					<vet_flag>1</vet_flag>
					<vet_start_date>01.01.1990</vet_start_date>
					<vet_end_date>01.01.1998</vet_end_date>
					<campaign_vet_flag>1</campaign_vet_flag>
					<vet_disability_status>2</vet_disability_status>
					<vet_tsm_type_cd>2</vet_tsm_type_cd>
					<vet_era>1</vet_era>
					<vet_discharge>Honorable</vet_discharge>
					<branch_of_service>USARMY</branch_of_service>
					<rank>018</rank>
					<moc>Test MOC</moc>
					<unit_affiliation>Test unit affiliation</unit_affiliation>
					<mostext>Sniper</mostext>
					<mosonetcrosswalk>Test mos onet crosswalk</mosonetcrosswalk>
					<past_five_years_veteran>1</past_five_years_veteran>
				</veteran>
				<preferences>
					<resume_searchable>1</resume_searchable>
					 <shift>
							<shift_first_flag>1</shift_first_flag>
							<shift_rotating_flag>1</shift_rotating_flag>
							<work_week>3</work_week>
							<work_type>6</work_type>
							<work_over_time>1</work_over_time>
					 </shift>
				</preferences>
			</personal>
		</statements>
		<special>
			<snapshot>Test snapshot</snapshot>
			<automatedsummary custom='1'>Test automated summary</automatedsummary>
			<preserved_order>Test preserved order</preserved_order>
			<preserved_format>Standard</preserved_format>
		</special>
		<preferences>
			<sal>30000</sal>
			<salary_unit_cd>5</salary_unit_cd>
			<resume_searchable>1</resume_searchable>
			<relocate>1</relocate>
			<email_flag>1</email_flag>
			<postal_flag>1</postal_flag>
			<phone_pri_flag>1</phone_pri_flag>
			<phone_sec_flag>1</phone_sec_flag>
			<fax_flag>1</fax_flag>
			<desired_zips>
				<desired_zip>
					<zip>Test zip 1</zip>
					<radius>100</radius>
					<statecode>CA</statecode>
					<choice>1</choice>
				</desired_zip>
				<desired_zip>
					<zip>Test zip 2</zip>
					<radius>25</radius>
					<statecode>OK</statecode>
					<choice>2</choice>
				</desired_zip>
			 </desired_zips>
		</preferences>
		<experience end='731126' start='724033'>EXPERIENCE
			<employment_status_cd>1</employment_status_cd>
			<job end='731126' id='1' pos='1' start='729908'><daterange><start days='729908' iso8601='1999-06-01'>June, 1999</start> to <end days='731126' iso8601='2002-10-01'>October, 2002</end></daterange>
			<description><skill>CONTROLLER</skill>, DPR Construction, a commercial building contractor specializing in technically demanding projects in the microelectronics, biopharmaceutical, health care. Responsibilities included overseeing all facets of <skill>accounting</skill> through <skill>Financial Statements</skill>.  Member of the Management Team, directly supervised staff and oversaw the monthly <skill>AIA</skill> billings, collections, and subsequent payments to subcontractors for as many as 40 jobs at one time.  Oversaw <skill>human resources</skill> and semi-monthly payroll. Responsible for banking relations, <skill>cash management</skill>, budgeting, leases, property taxes, and orchestrating the annual audit/review.  Established, implemented and documented all <skill>accounting</skill> procedures. Directly communicated with President, VPs, Project Managers, and Superintendents on a regular basis. Became well versed in American Contractor and <skill>Prolog</skill>.</description></job>

			<job end='729908' id='2' pos='2' sic='931102' sic2='93' start='729482'><daterange><start days='729482' iso8601='1998-04-01'>April, 1998</start> to <end days='729908' iso8601='1999-06-01'>June, 1999</end></daterange>
			<employer>CONTROLLER</employer>, <description>Koll Construction, L.P., an International General Contractor based in Newport Beach, <skill>CA</skill>.  Responsibilities included overseeing all facets of <skill>accounting</skill> through <skill>Financial Statements</skill> including <skill>accounting</skill> for five joint ventures, an affiliated company, and Koll Construction Asia, Ltd., a wholly owned subsidiary in Asia requiring a knowledge of multi-currency regulations.  Directly supervised a full staff and oversaw the monthly <skill>AIA</skill> billings, collections, and subsequent payments to subcontractors for as many as 40 jobs at one time. These jobs ranged from $1 million to $80 million in contract value. Responsible for licensing, banking relations, <skill>cash management</skill>, budgeting, leases, property taxes, use taxes, and orchestrating the annual audit.  Directly communicated with Owners, Divisional VPs and Project Managers on a daily basis and traveled to job sites as required.  Participated in a <skill>Y2K</skill> compliant software selection process and <skill>conversion</skill> to <skill>J.D. Edwards</skill>. Became proficient at <skill>Microsoft Outlook</skill>.</description></job>

			<job end='729482' id='3' pos='3' sic='154200' sic2='15' start='728327'><daterange><start days='728327' iso8601='1995-02-01'>February, 1995</start> to <end days='729482' iso8601='1998-04-01'>April, 1998</end></daterange>
			<title>DIRECTOR OF ACCOUNTING</title>, <employer>MBK Construction Ltd</employer>. (formerly <employer>Birtcher Construction Limited</employer>), <description>the Nation's largest builder of theatres (<skill>Sales</skill> volume of $305 million).  Responsibilities included overseeing all facets of <skill>accounting</skill> through <skill>Financial Statements</skill>. Was responsible for overseeing the monthly <skill>AIA</skill> billings, collections, and subsequent payments to subcontractors for as many as 50 jobs at any one time.  These jobs ranged from $2 million to $100 million in contract value. I directly supervised a staff of 10 people.  Participated in <skill>Y2K</skill> compliant software selection and <skill>conversion</skill>. Originally hired to oversee all facets of <skill>accounting</skill> in Billion dollar AMC National <skill>Theatre</skill> Expansion Program and to get MBK licensed from every standpoint in each state (17 in total) in which a project was to be built.  Also responsible for knowing the lien laws and sales tax laws in each state as well as filing all necessary forms, reports, and returns.  Became well versed in <skill>Ami Pro</skill>, <skill>Word Perfect</skill>, <skill>Word</skill>, <skill>Lotus</skill>, <skill>Excel</skill>, and <skill>J.D. Edwards</skill>.</description></job>

			<job end='728265' id='4' pos='4' sic='931102' sic2='93' start='727931'><daterange><start days='727931' iso8601='1994-01-01'>January, 1994</start> to <end days='728265' iso8601='1994-12-01'>December, 1994</end></daterange>
			<employer>CONTROLLER</employer>, <description>Besteel Industries, a national leading manufacturer of modular buildings and canopies primarily for the petroleum and fast food industries.  Hired in post Chapter 11 period to reorganize and reduce expenses without sacrificing quality or efficiency.  Responsibilities included overseeing all facets of <skill>accounting</skill> and <skill>management information system</skill> departments (up to eight people), preparing and maintaining short and long term <skill>cash flow projections</skill>, monthly budget to actual analysis, job cost profit analysis, and generating/maintaining various <skill>spreadsheet</skill> files.  Also responsible for banking relations, <skill>negotiating</skill> taxes, leases, payroll and employee benefits, product pricing, <skill>risk management</skill>, and required monthly reporting for U.S. Bankruptcy Courts.  Saw Corporation through asset sale to new Limited Liability Company, which involved writing value of assets down to Fair <skill>Market</skill> Value using APB 16.</description></job>

			<job end='727931' id='5' pos='5' sic='931102' sic2='93' start='725586'><daterange><start days='725586' iso8601='1987-08-01'>August, 1987</start> to <end days='727931' iso8601='1994-01-01'>January, 1994</end></daterange>
			<employer>CONTROLLER</employer>, <description>The Fitch Corporation, an Orange County based lath &amp; plaster subcontractor licensed in Arizona, California, Nevada, and Washington. Responsibilities included overseeing all facets of <skill>accounting</skill> through <skill>financial statements</skill> for four divisions, preparing monthly jobcost reports for an average of 40 jobs in process, <skill>supervising</skill> weekly payrolls of up to 300 employees and <skill>accounts payable</skill> of over 500 vendors.  Successfully oversaw <skill>automation</skill> of <skill>accounting</skill> system and <skill>networking</skill> of computer system. Saw Corporation through no-change IRS and State payroll audits.  Wrote an automated program for all billing and lien releases.</description></job>

			<job end='725555' id='6' pos='6' sic='738909' sic2='73' start='725221'><daterange><start days='725221' iso8601='1986-08-01'>August, 1986</start> to <end days='725555' iso8601='1987-07-01'>July, 1987</end></daterange>
			<employer>CHIEF FINANCIAL OFFICER, Ven-Tek Management Company</employer>, <address><city>Orange</city>, <state abbrev='CA'>CA.</state></address>  <description>Responsibilities included starting up company from initialization, automating <skill>accounting</skill> system for six subsidiaries, <skill>supervising</skill> all aspects of <skill>accounting</skill> through <skill>financial statements</skill>, preparing investment proposals, <skill>business plans</skill> and projections.</description></job>

			<job end='725190' id='7' pos='7' sic='651300' sic2='65' start='724795'><daterange><start days='724795' iso8601='1985-06-01'>June, 1985</start> to <end days='725190' iso8601='1986-07-01'>July, 1986</end></daterange>
			<employer>CONTROLLER, Crain &amp; Associates Management Company</employer>, <address><city>Tucson</city>, <state abbrev='AZ'>AZ.</state></address>  <description>Responsibilities included automating <skill>accounting</skill> system for eight different partnerships, initializing several partnerships, including a construction company, which involved designing a draw request with Wells Fargo, <skill>supervising</skill> all aspects of <skill>accounting</skill> through presentation of <skill>financial statements</skill> to investors, and <skill>programming</skill> in <skill>Dbase</skill>.</description></job>

			<job end='724795' id='8' pos='8' sic='872102' sic2='87' start='724033'><daterange><start days='724033' iso8601='1983-05-01'>May, 1983</start> to <end days='724795' iso8601='1985-06-01'>June, 1985</end></daterange>
			<employer>ACCOUNTANT, Ernst &amp; Whinney</employer>, <address><city>Tucson</city>, <state abbrev='AZ'>AZ.</state></address>  <description>Responsibilities included tax-work, audit procedures, <skill>management consultant</skill> services, preparation of <skill>financial statements</skill> for clients in the manufacturing and healthcare industries.</description></job>
		</experience>
		<education>EDUCATION
			<school id='9'>
				<completiondate>06/1993</completiondate>
				<degree level='25' name='Bachelor of Science'>BS</degree> in 
				<major code='4201'>Business Administration</major>, 
				<institution>University of Arizona</institution>.  Majors in 
				<major code='4001 4303'>Accounting and Management Information Systems</major>. GPA of 
				<gpa max='4' value='3.2'>3.2</gpa>=A). 
				<description>Completely supported self through school.</description>
			</school>
			<school id='10'>
				<expectedcompletiondate>06/2013</expectedcompletiondate>				
				<address>
					<city>Test school city</city>
					<county>21001</county>
					<county_fullname>Test school city</county_fullname>
					<state>OK</state>
					<state_fullname>Oklahoma</state_fullname>
					<country>US</country>
					<country_fullname>United States Of America</country_fullname>
				</address>
				<activities>Test activities</activities>
				<institution>SCHOLARSHIPS</institution>
				Received academic scholarship from 
				<institution>University of Arizona</institution> (for maintaining GPA of 
				<gpa max='4' value='3.5'>3.5</gpa>), 
				<daterange>
					<start days='722452' iso8601='1979-01-01'>1979</start>-
					<end days='723548' iso8601='1982-01-01'>1982</end>
				</daterange>
			</school>.
			<courses>Test education courses</courses>
			<school_status_cd>0</school_status_cd>
			<norm_edu_level_cd>14</norm_edu_level_cd>
			<targetindustries>
				<industrycode>72110</industrycode>
				<industrycode>72111</industrycode>
				<industrycode>72112</industrycode>
			</targetindustries>
			<mostexperienceindustries>
				<industrycode>72112</industrycode>
				<industrycode>72113</industrycode>
			</mostexperienceindustries>
		</education>
		<professional>ORGANIZATION INVOLVEMENT
		<affiliations>Current Treasurer for the OC Chapter of CFMA (Construction Financial Mgmt. Assoc.)
		Current Member of AICPA (American Institute of Certified Public Accountants)
		Member of NAFE (National Assoc. of Female Executives)</affiliations></professional>
		<references>REFERENCES Available upon request.</references>
	</resume>
	<skills>
		<skills>
			Test skill 1
		</skills>
		<skills>
			Test skill 2
		</skills>
		<languages>
			<language>
				<language>Test language proficiency 1</language>
				<proficiency>1</proficiency>
			</language>
			<language>
				<language>Test language proficiency 2</language>
				<proficiency>2</proficiency>
		</language>
		</languages>
		<courses>
			<certifications >
				<certification>Test certification 1</certification>
				<certification>Test certification 2</certification>
			</certifications>
		</courses>
	</skills>
	<skillrollup version='1'>
		<canonskill end='731126' experience='1' expidrefs='1,2,3,4,5,6,7' idrefs='1,2,3,4,5,6,7' lastused='2002' name='accountancy' start='724795'>
			<variant>accounting</variant>
		</canonskill>
		<canonskill end='727931' experience='1' expidrefs='5' idrefs='5' lastused='1994' name='accounts payable' start='725586'>
			<variant>accounts payable</variant>
		</canonskill>
		<canonskill end='731126' experience='1' expidrefs='1,2,3' idrefs='1,2,3' lastused='2002' name='aia' start='728327'>
			<variant>AIA</variant>
		</canonskill>
		<canonskill end='729482' experience='1' expidrefs='3' idrefs='3' lastused='1998' name='amipro' start='728327'>
			<variant>Ami Pro</variant>
		</canonskill>
		<canonskill end='727931' experience='1' expidrefs='5' idrefs='5' lastused='1994' name='automation' start='725586'>
			<variant>automation</variant>
		</canonskill>
		<canonskill end='731126' experience='1' expidrefs='1,2,4' idrefs='1,2,4' lastused='2002' name='banking' start='727931'>
			<variant>banking</variant>
		</canonskill>
		<canonskill end='728265' experience='1' expidrefs='4' idrefs='4' lastused='1994' name='benefits administration' start='727931'>
			<variant>benefits</variant>
		</canonskill>
		<canonskill end='727931' experience='1' expidrefs='5' idrefs='5' lastused='1994' name='billing' start='725586'>
			<variant>billing</variant>
		</canonskill>
		<canonskill end='731126' experience='1' expidrefs='1,2,3' idrefs='1,2,3' lastused='2002' name='billing systems' start='728327'>
			<variant>billings</variant>
		</canonskill>
		<canonskill end='731126' experience='1' expidrefs='1,2,4' idrefs='1,2,4' lastused='2002' name='budgeting' start='727931'>
			<variant>budgeting</variant>
			<variant>budget</variant>
		</canonskill>
		<canonskill end='725555' experience='1' expidrefs='6' idrefs='6' lastused='1987' name='business plans' start='725221'>
			<variant>business plans</variant>
		</canonskill>
		<canonskill end='728265' experience='1' expidrefs='4' idrefs='4' lastused='1994' name='cash flow' start='727931'>
			<variant>cash flow projections</variant>
		</canonskill>
		<canonskill end='731126' experience='1' expidrefs='1,2' idrefs='1,2' lastused='2002' name='cash management' start='729482'>
			<variant>cash management</variant>
		</canonskill>
		<canonskill end='729908' experience='1' expidrefs='2' idrefs='2' lastused='1999' name='computer associates packages' start='729482'>
			<variant>CA</variant>
		</canonskill>
		<canonskill end='731126' experience='1' expidrefs='1' idrefs='1' lastused='2002' name='control' start='729908'>
			<variant>CONTROLLER</variant>
		</canonskill>
		<canonskill end='729908' experience='1' expidrefs='2,3' idrefs='2,3' lastused='1999' name='conversion' start='728327'>
			<variant>conversion</variant>
		</canonskill>
		<canonskill end='725190' experience='1' expidrefs='7' idrefs='7' lastused='1986' name='corel draw' start='724795'>
			<variant>draw</variant>
		</canonskill>
		<canonskill end='724795' experience='1' expidrefs='8' idrefs='8' lastused='1985' name='customer relations' start='724033'>
			<variant>clients</variant>
		</canonskill>
		<canonskill end='725190' experience='1' expidrefs='7' idrefs='7' lastused='1986' name='dbase' start='724795'>
			<variant>Dbase</variant>
		</canonskill>
		<canonskill end='725190' experience='1' expidrefs='7' idrefs='7' lastused='1986' name='design' start='724795'>
			<variant>designing</variant>
		</canonskill>
		<canonskill end='728265' experience='1' expidrefs='4' idrefs='4' lastused='1994' name='fast' start='727931'>
			<variant>fast</variant>
		</canonskill>
		<canonskill end='729482' experience='1' expidrefs='3' idrefs='3' lastused='1998' name='file management' start='728327'>
			<variant>filing</variant>
		</canonskill>
		<canonskill end='731126' experience='1' expidrefs='1,2,3,5,6,7,8' idrefs='1,2,3,5,6,7,8' lastused='2002' name='financial statements' start='724033'>
			<variant>Financial Statements</variant>
			<variant>financial statements</variant>
		</canonskill>
		<canonskill end='729482' experience='1' expidrefs='3' idrefs='3' lastused='1998' name='forms' start='728327'>
			<variant>forms</variant>
		</canonskill>
		<canonskill end='731126' experience='1' expidrefs='1' idrefs='1' lastused='2002' name='human resources' start='729908'>
			<variant>human resources</variant>
		</canonskill>
		<canonskill end='729908' experience='1' expidrefs='2,3' idrefs='2,3' lastused='1999' name='jd edwards' start='728327'>
			<variant>J.D. Edwards</variant>
		</canonskill>
		<canonskill end='729482' experience='1' expidrefs='3' idrefs='3' lastused='1998' name='lotus' start='728327'>
			<variant>Lotus</variant>
		</canonskill>
		<canonskill end='724795' experience='1' expidrefs='8' idrefs='8' lastused='1985' name='management consulting' start='724033'>
			<variant>management consultant</variant>
		</canonskill>
		<canonskill end='728265' experience='1' expidrefs='4' idrefs='4' lastused='1994' name='marketing' start='727931'>
			<variant>Market</variant>
		</canonskill>
		<canonskill end='729482' experience='1' expidrefs='3' idrefs='3' lastused='1998' name='microsoft excel' start='728327'>
			<variant>Excel</variant>
		</canonskill>
		<canonskill end='729908' experience='1' expidrefs='2' idrefs='2' lastused='1999' name='microsoft outlook' start='729482'>
			<variant>Microsoft Outlook</variant>
		</canonskill>
		<canonskill end='729482' experience='1' expidrefs='3' idrefs='3' lastused='1998' name='microsoft word' start='728327'>
			<variant>Word</variant>
		</canonskill>
		<canonskill end='728265' experience='1' expidrefs='4' idrefs='4' lastused='1994' name='mis' start='727931'>
			<variant>management information system</variant>
		</canonskill>
		<canonskill end='728265' experience='1' expidrefs='4' idrefs='4' lastused='1994' name='negotiation' start='727931'>
			<variant>negotiating</variant>
		</canonskill>
		<canonskill end='727931' experience='1' expidrefs='5' idrefs='5' lastused='1994' name='networking' start='725586'>
			<variant>networking</variant>
		</canonskill>
		<canonskill end='731126' experience='1' expidrefs='1,4,5' idrefs='1,4,5' lastused='2002' name='payroll processing' start='725586'>
			<variant>payroll</variant>
		</canonskill>
		<canonskill end='728265' experience='1' expidrefs='4' idrefs='4' lastused='1994' name='pricing' start='727931'>
			<variant>pricing</variant>
		</canonskill>
		<canonskill end='728265' experience='1' expidrefs='4' idrefs='4' lastused='1994' name='profit' start='727931'>
			<variant>profit</variant>
		</canonskill>
		<canonskill end='725190' experience='1' expidrefs='7' idrefs='7' lastused='1986' name='programming' start='724795'>
			<variant>programming</variant>
		</canonskill>
		<canonskill end='731126' experience='1' expidrefs='1' idrefs='1' lastused='2002' name='prolog' start='729908'>
			<variant>Prolog</variant>
		</canonskill>
		<canonskill end='725555' experience='1' expidrefs='6' idrefs='6' lastused='1987' name='proposal writing' start='725221'>
			<variant>proposals</variant>
		</canonskill>
		<canonskill end='728265' experience='1' expidrefs='4' idrefs='4' lastused='1994' name='quality' start='727931'>
			<variant>quality</variant>
		</canonskill>
		<canonskill end='728265' experience='1' expidrefs='4' idrefs='4' lastused='1994' name='reports' start='727931'>
			<variant>reporting</variant>
		</canonskill>
		<canonskill end='728265' experience='1' expidrefs='4' idrefs='4' lastused='1994' name='risk management' start='727931'>
			<variant>risk management</variant>
		</canonskill>
		<canonskill end='729482' experience='1' expidrefs='3' idrefs='3' lastused='1998' name='sales' start='728327'>
			<variant>Sales</variant>
			<variant>sales</variant>
		</canonskill>
		<canonskill end='728265' experience='1' expidrefs='4' idrefs='4' lastused='1994' name='spreadsheets' start='727931'>
			<variant>spreadsheet</variant>
		</canonskill>
		<canonskill end='727931' experience='1' expidrefs='5,6,7' idrefs='5,6,7' lastused='1994' name='supervisory skills' start='724795'>
			<variant>supervising</variant>
		</canonskill>
		<canonskill end='731126' experience='1' expidrefs='1,2,3,4,8' idrefs='1,2,3,4,8' lastused='2002' name='tax planning' start='724033'>
			<variant>tax</variant>
			<variant>taxes</variant>
		</canonskill>
		<canonskill end='729482' experience='1' expidrefs='3' idrefs='3' lastused='1998' name='theatre' start='728327'>
			<variant>Theatre</variant>
		</canonskill>
		<canonskill end='729482' experience='1' expidrefs='3' idrefs='3' lastused='1998' name='wordperfect' start='728327'>
			<variant>Word Perfect</variant>
		</canonskill>
		<canonskill end='729908' experience='1' expidrefs='2,3' idrefs='2,3' lastused='1999' name='year 2000' start='728327'>
			<variant>Y2K</variant>
		</canonskill>
	</skillrollup>
</ResDoc>
";

		internal const string MockCompletedResumeXml = @"<ResDoc>
	<resume canonversion='2' dateversion='2' iso8601='2012-07-10' present='734696' xml:space='preserve'>
		<header>
			<resumeid>1192112</resumeid>
			<completionstatus>127</completionstatus>
			<canonversion>3</canonversion>
			<default>1</default>
			<updatedon>2012-07-10</updatedon>
			<resumename>test resume name</resumename>
			<onet>11-2022.91</onet>
			<ronet>11-2022.92</ronet>
			<status>2</status>
		</header>
		<contact>
			<name>
				<givenname>SANDRA</givenname>
				<givenname>S</givenname>. 
				<surname>BUONARIGO</surname>
			</name>
			, CPA
			<address lat='33.7873' lon='117.844' majorcity='ORANGE' state='CA'>
				<street>1025 E. Chalynn Ave.</street>
				<street>Test street 2</street>
				<town>Test town</town>
				<city>Orange</city>, 
				<state abbrev='CA'>CA</state> 
				<state_fullname>California</state_fullname>
				<postalcode>92866</postalcode>
				<country>US</country>
				<country_fullname>United States Of America</country_fullname>
			</address>
			<email>test@email.com</email>
			<phone type='home'>(714) HOME</phone> home
			<phone type='cell'>(714) CELL</phone> cell
			<phone type='work' ext='1234'>(714) WORK</phone> work
		</contact>

		<summary include='1'>
			<summary include='1'>Test summary</summary>
			OBJECTIVE
			<objective>Responsible career position in Accounting or Finance of a stable company in Orange County.</objective>
		</summary>
		<statements>QUALIFICATIONS
			<activities>Certified Public Accountant-California State Board of Accountancy.Certified Public Accountant-Arizona State Board of Accountancy (inactive).</activities>
			<honors>Test statements honors</honors>
			<personal>
				<dob>12.06.1973</dob>
				<sex>2</sex>
				<userinfo>
					<ssn>Test SSN</ssn>
				</userinfo>
				<ethnic_heritages>
					<ethnic_heritage>1</ethnic_heritage>
				</ethnic_heritages>
				<citizen_flag>1</citizen_flag>
				<migrant_flag>0</migrant_flag>
				<disability_status>1</disability_status>
				<disability_category_cd>4</disability_category_cd>
				<alien_expires>01.01.2090</alien_expires>
				<alien_id>Test alien id</alien_id>
				<alien_perm_flag>1</alien_perm_flag>
				<crc>1</crc>
				<license>
					<driver_flag>1</driver_flag>
					<driver_class>1</driver_class>
					<state_fullname>Test state full name</state_fullname>
					<drv_airbrake_flag>1</drv_airbrake_flag>
				</license>
				<veteran>
					<vet_flag>1</vet_flag>
					<vet_start_date>01.01.1990</vet_start_date>
					<vet_end_date>01.01.1998</vet_end_date>
					<campaign_vet_flag>1</campaign_vet_flag>
					<vet_disability_status>2</vet_disability_status>
					<vet_tsm_type_cd>2</vet_tsm_type_cd>
					<vet_era>1</vet_era>
					<vet_discharge>Honorable</vet_discharge>
					<branch_of_service>USARMY</branch_of_service>
					<rank>018</rank>
					<moc>Test MOC</moc>
					<unit_affiliation>Test unit affiliation</unit_affiliation>
					<mostext>Sniper</mostext>
					<mosonetcrosswalk>Test mos onet crosswalk</mosonetcrosswalk>
					<past_five_years_veteran>1</past_five_years_veteran>
				</veteran>
				<preferences>
					<resume_searchable>1</resume_searchable>
					 <shift>
							<shift_first_flag>1</shift_first_flag>
							<shift_rotating_flag>1</shift_rotating_flag>
							<work_week>3</work_week>
							<work_type>6</work_type>
							<work_over_time>1</work_over_time>
					 </shift>
				</preferences>
			</personal>
		</statements>
		<special>
			<snapshot>Test snapshot</snapshot>
			<automatedsummary custom='1'>Test automated summary</automatedsummary>
			<preserved_order>Test preserved order</preserved_order>
			<preserved_format>Standard</preserved_format>
		</special>
		<preferences>
			<sal>30000</sal>
			<salary_unit_cd>5</salary_unit_cd>
			<resume_searchable>1</resume_searchable>
			<relocate>1</relocate>
			<email_flag>1</email_flag>
			<postal_flag>1</postal_flag>
			<phone_pri_flag>1</phone_pri_flag>
			<phone_sec_flag>1</phone_sec_flag>
			<fax_flag>1</fax_flag>
			<desired_zips>
				<desired_zip>
					<zip>Test zip 1</zip>
					<radius>100</radius>
					<statecode>CA</statecode>
					<choice>1</choice>
				</desired_zip>
				<desired_zip>
					<zip>Test zip 2</zip>
					<radius>25</radius>
					<statecode>OK</statecode>
					<choice>2</choice>
				</desired_zip>
			 </desired_zips>
		</preferences>
		<experience end='731126' start='724033'>EXPERIENCE
			<employment_status_cd>1</employment_status_cd>
			<job end='731126' id='1' pos='1' start='729908'><daterange><start days='729908' iso8601='1999-06-01'>June, 1999</start> to <end days='731126' iso8601='2002-10-01'>October, 2002</end></daterange>
			<description><skill>CONTROLLER</skill>, DPR Construction, a commercial building contractor specializing in technically demanding projects in the microelectronics, biopharmaceutical, health care. Responsibilities included overseeing all facets of <skill>accounting</skill> through <skill>Financial Statements</skill>.  Member of the Management Team, directly supervised staff and oversaw the monthly <skill>AIA</skill> billings, collections, and subsequent payments to subcontractors for as many as 40 jobs at one time.  Oversaw <skill>human resources</skill> and semi-monthly payroll. Responsible for banking relations, <skill>cash management</skill>, budgeting, leases, property taxes, and orchestrating the annual audit/review.  Established, implemented and documented all <skill>accounting</skill> procedures. Directly communicated with President, VPs, Project Managers, and Superintendents on a regular basis. Became well versed in American Contractor and <skill>Prolog</skill>.</description></job>

			<job end='729908' id='2' pos='2' sic='931102' sic2='93' start='729482'><daterange><start days='729482' iso8601='1998-04-01'>April, 1998</start> to <end days='729908' iso8601='1999-06-01'>June, 1999</end></daterange>
			<employer>CONTROLLER</employer>, <description>Koll Construction, L.P., an International General Contractor based in Newport Beach, <skill>CA</skill>.  Responsibilities included overseeing all facets of <skill>accounting</skill> through <skill>Financial Statements</skill> including <skill>accounting</skill> for five joint ventures, an affiliated company, and Koll Construction Asia, Ltd., a wholly owned subsidiary in Asia requiring a knowledge of multi-currency regulations.  Directly supervised a full staff and oversaw the monthly <skill>AIA</skill> billings, collections, and subsequent payments to subcontractors for as many as 40 jobs at one time. These jobs ranged from $1 million to $80 million in contract value. Responsible for licensing, banking relations, <skill>cash management</skill>, budgeting, leases, property taxes, use taxes, and orchestrating the annual audit.  Directly communicated with Owners, Divisional VPs and Project Managers on a daily basis and traveled to job sites as required.  Participated in a <skill>Y2K</skill> compliant software selection process and <skill>conversion</skill> to <skill>J.D. Edwards</skill>. Became proficient at <skill>Microsoft Outlook</skill>.</description></job>

			<job end='729482' id='3' pos='3' sic='154200' sic2='15' start='728327'><daterange><start days='728327' iso8601='1995-02-01'>February, 1995</start> to <end days='729482' iso8601='1998-04-01'>April, 1998</end></daterange>
			<title>DIRECTOR OF ACCOUNTING</title>, <employer>MBK Construction Ltd</employer>. (formerly <employer>Birtcher Construction Limited</employer>), <description>the Nation's largest builder of theatres (<skill>Sales</skill> volume of $305 million).  Responsibilities included overseeing all facets of <skill>accounting</skill> through <skill>Financial Statements</skill>. Was responsible for overseeing the monthly <skill>AIA</skill> billings, collections, and subsequent payments to subcontractors for as many as 50 jobs at any one time.  These jobs ranged from $2 million to $100 million in contract value. I directly supervised a staff of 10 people.  Participated in <skill>Y2K</skill> compliant software selection and <skill>conversion</skill>. Originally hired to oversee all facets of <skill>accounting</skill> in Billion dollar AMC National <skill>Theatre</skill> Expansion Program and to get MBK licensed from every standpoint in each state (17 in total) in which a project was to be built.  Also responsible for knowing the lien laws and sales tax laws in each state as well as filing all necessary forms, reports, and returns.  Became well versed in <skill>Ami Pro</skill>, <skill>Word Perfect</skill>, <skill>Word</skill>, <skill>Lotus</skill>, <skill>Excel</skill>, and <skill>J.D. Edwards</skill>.</description></job>

			<job end='728265' id='4' pos='4' sic='931102' sic2='93' start='727931'><daterange><start days='727931' iso8601='1994-01-01'>January, 1994</start> to <end days='728265' iso8601='1994-12-01'>December, 1994</end></daterange>
			<employer>CONTROLLER</employer>, <description>Besteel Industries, a national leading manufacturer of modular buildings and canopies primarily for the petroleum and fast food industries.  Hired in post Chapter 11 period to reorganize and reduce expenses without sacrificing quality or efficiency.  Responsibilities included overseeing all facets of <skill>accounting</skill> and <skill>management information system</skill> departments (up to eight people), preparing and maintaining short and long term <skill>cash flow projections</skill>, monthly budget to actual analysis, job cost profit analysis, and generating/maintaining various <skill>spreadsheet</skill> files.  Also responsible for banking relations, <skill>negotiating</skill> taxes, leases, payroll and employee benefits, product pricing, <skill>risk management</skill>, and required monthly reporting for U.S. Bankruptcy Courts.  Saw Corporation through asset sale to new Limited Liability Company, which involved writing value of assets down to Fair <skill>Market</skill> Value using APB 16.</description></job>

			<job end='727931' id='5' pos='5' sic='931102' sic2='93' start='725586'><daterange><start days='725586' iso8601='1987-08-01'>August, 1987</start> to <end days='727931' iso8601='1994-01-01'>January, 1994</end></daterange>
			<employer>CONTROLLER</employer>, <description>The Fitch Corporation, an Orange County based lath &amp; plaster subcontractor licensed in Arizona, California, Nevada, and Washington. Responsibilities included overseeing all facets of <skill>accounting</skill> through <skill>financial statements</skill> for four divisions, preparing monthly jobcost reports for an average of 40 jobs in process, <skill>supervising</skill> weekly payrolls of up to 300 employees and <skill>accounts payable</skill> of over 500 vendors.  Successfully oversaw <skill>automation</skill> of <skill>accounting</skill> system and <skill>networking</skill> of computer system. Saw Corporation through no-change IRS and State payroll audits.  Wrote an automated program for all billing and lien releases.</description></job>

			<job end='725555' id='6' pos='6' sic='738909' sic2='73' start='725221'><daterange><start days='725221' iso8601='1986-08-01'>August, 1986</start> to <end days='725555' iso8601='1987-07-01'>July, 1987</end></daterange>
			<employer>CHIEF FINANCIAL OFFICER, Ven-Tek Management Company</employer>, <address><city>Orange</city>, <state abbrev='CA'>CA.</state></address>  <description>Responsibilities included starting up company from initialization, automating <skill>accounting</skill> system for six subsidiaries, <skill>supervising</skill> all aspects of <skill>accounting</skill> through <skill>financial statements</skill>, preparing investment proposals, <skill>business plans</skill> and projections.</description></job>

			<job end='725190' id='7' pos='7' sic='651300' sic2='65' start='724795'><daterange><start days='724795' iso8601='1985-06-01'>June, 1985</start> to <end days='725190' iso8601='1986-07-01'>July, 1986</end></daterange>
			<employer>CONTROLLER, Crain &amp; Associates Management Company</employer>, <address><city>Tucson</city>, <state abbrev='AZ'>AZ.</state></address>  <description>Responsibilities included automating <skill>accounting</skill> system for eight different partnerships, initializing several partnerships, including a construction company, which involved designing a draw request with Wells Fargo, <skill>supervising</skill> all aspects of <skill>accounting</skill> through presentation of <skill>financial statements</skill> to investors, and <skill>programming</skill> in <skill>Dbase</skill>.</description></job>

			<job end='724795' id='8' pos='8' sic='872102' sic2='87' start='724033'><daterange><start days='724033' iso8601='1983-05-01'>May, 1983</start> to <end days='724795' iso8601='1985-06-01'>June, 1985</end></daterange>
			<employer>ACCOUNTANT, Ernst &amp; Whinney</employer>, <address><city>Tucson</city>, <state abbrev='AZ'>AZ.</state></address>  <description>Responsibilities included tax-work, audit procedures, <skill>management consultant</skill> services, preparation of <skill>financial statements</skill> for clients in the manufacturing and healthcare industries.</description></job>
		</experience>
		<education>EDUCATION
			<school id='9'>
				<completiondate>06/1993</completiondate>
				<degree level='25' name='Bachelor of Science'>BS</degree> in 
				<major code='4201'>Business Administration</major>, 
				<institution>University of Arizona</institution>.  Majors in 
				<major code='4001 4303'>Accounting and Management Information Systems</major>. GPA of 
				<gpa max='4' value='3.2'>3.2</gpa>=A). 
				<description>Completely supported self through school.</description>
			</school>
			<school id='10'>
				<expectedcompletiondate>06/2013</expectedcompletiondate>				
				<address>
					<city>Test school city</city>
					<county>21001</county>
					<county_fullname>Test school city</county_fullname>
					<state>OK</state>
					<state_fullname>Oklahoma</state_fullname>
					<country>US</country>
					<country_fullname>United States Of America</country_fullname>
				</address>
				<activities>Test activities</activities>
				<institution>SCHOLARSHIPS</institution>
				Received academic scholarship from 
				<institution>University of Arizona</institution> (for maintaining GPA of 
				<gpa max='4' value='3.5'>3.5</gpa>), 
				<daterange>
					<start days='722452' iso8601='1979-01-01'>1979</start>-
					<end days='723548' iso8601='1982-01-01'>1982</end>
				</daterange>
			</school>.
			<courses>Test education courses</courses>
			<school_status_cd>0</school_status_cd>
			<norm_edu_level_cd>14</norm_edu_level_cd>
			<targetindustries>
				<industrycode>72110</industrycode>
				<industrycode>72111</industrycode>
				<industrycode>72112</industrycode>
			</targetindustries>
			<mostexperienceindustries>
				<industrycode>72112</industrycode>
				<industrycode>72113</industrycode>
			</mostexperienceindustries>
		</education>
		<professional>ORGANIZATION INVOLVEMENT
		<affiliations>Current Treasurer for the OC Chapter of CFMA (Construction Financial Mgmt. Assoc.)
		Current Member of AICPA (American Institute of Certified Public Accountants)
		Member of NAFE (National Assoc. of Female Executives)</affiliations></professional>
		<references>REFERENCES Available upon request.</references>
	</resume>
	<skills>
		<skills>
			Test skill 1
		</skills>
		<skills>
			Test skill 2
		</skills>
		<languages>
			<languages_profiency>Test language proficiency 1</languages_profiency>
			<languages_profiency>Test language proficiency 2</languages_profiency>
		</languages>
		<courses>
			<certifications >
				<certification>Test certification 1</certification>
				<certification>Test certification 2</certification>
			</certifications>
		</courses>
	</skills>
	<skillrollup version='1'>
		<canonskill end='731126' experience='1' expidrefs='1,2,3,4,5,6,7' idrefs='1,2,3,4,5,6,7' lastused='2002' name='accountancy' start='724795'>
			<variant>accounting</variant>
		</canonskill>
		<canonskill end='727931' experience='1' expidrefs='5' idrefs='5' lastused='1994' name='accounts payable' start='725586'>
			<variant>accounts payable</variant>
		</canonskill>
		<canonskill end='731126' experience='1' expidrefs='1,2,3' idrefs='1,2,3' lastused='2002' name='aia' start='728327'>
			<variant>AIA</variant>
		</canonskill>
		<canonskill end='729482' experience='1' expidrefs='3' idrefs='3' lastused='1998' name='amipro' start='728327'>
			<variant>Ami Pro</variant>
		</canonskill>
		<canonskill end='727931' experience='1' expidrefs='5' idrefs='5' lastused='1994' name='automation' start='725586'>
			<variant>automation</variant>
		</canonskill>
		<canonskill end='731126' experience='1' expidrefs='1,2,4' idrefs='1,2,4' lastused='2002' name='banking' start='727931'>
			<variant>banking</variant>
		</canonskill>
		<canonskill end='728265' experience='1' expidrefs='4' idrefs='4' lastused='1994' name='benefits administration' start='727931'>
			<variant>benefits</variant>
		</canonskill>
		<canonskill end='727931' experience='1' expidrefs='5' idrefs='5' lastused='1994' name='billing' start='725586'>
			<variant>billing</variant>
		</canonskill>
		<canonskill end='731126' experience='1' expidrefs='1,2,3' idrefs='1,2,3' lastused='2002' name='billing systems' start='728327'>
			<variant>billings</variant>
		</canonskill>
		<canonskill end='731126' experience='1' expidrefs='1,2,4' idrefs='1,2,4' lastused='2002' name='budgeting' start='727931'>
			<variant>budgeting</variant>
			<variant>budget</variant>
		</canonskill>
		<canonskill end='725555' experience='1' expidrefs='6' idrefs='6' lastused='1987' name='business plans' start='725221'>
			<variant>business plans</variant>
		</canonskill>
		<canonskill end='728265' experience='1' expidrefs='4' idrefs='4' lastused='1994' name='cash flow' start='727931'>
			<variant>cash flow projections</variant>
		</canonskill>
		<canonskill end='731126' experience='1' expidrefs='1,2' idrefs='1,2' lastused='2002' name='cash management' start='729482'>
			<variant>cash management</variant>
		</canonskill>
		<canonskill end='729908' experience='1' expidrefs='2' idrefs='2' lastused='1999' name='computer associates packages' start='729482'>
			<variant>CA</variant>
		</canonskill>
		<canonskill end='731126' experience='1' expidrefs='1' idrefs='1' lastused='2002' name='control' start='729908'>
			<variant>CONTROLLER</variant>
		</canonskill>
		<canonskill end='729908' experience='1' expidrefs='2,3' idrefs='2,3' lastused='1999' name='conversion' start='728327'>
			<variant>conversion</variant>
		</canonskill>
		<canonskill end='725190' experience='1' expidrefs='7' idrefs='7' lastused='1986' name='corel draw' start='724795'>
			<variant>draw</variant>
		</canonskill>
		<canonskill end='724795' experience='1' expidrefs='8' idrefs='8' lastused='1985' name='customer relations' start='724033'>
			<variant>clients</variant>
		</canonskill>
		<canonskill end='725190' experience='1' expidrefs='7' idrefs='7' lastused='1986' name='dbase' start='724795'>
			<variant>Dbase</variant>
		</canonskill>
		<canonskill end='725190' experience='1' expidrefs='7' idrefs='7' lastused='1986' name='design' start='724795'>
			<variant>designing</variant>
		</canonskill>
		<canonskill end='728265' experience='1' expidrefs='4' idrefs='4' lastused='1994' name='fast' start='727931'>
			<variant>fast</variant>
		</canonskill>
		<canonskill end='729482' experience='1' expidrefs='3' idrefs='3' lastused='1998' name='file management' start='728327'>
			<variant>filing</variant>
		</canonskill>
		<canonskill end='731126' experience='1' expidrefs='1,2,3,5,6,7,8' idrefs='1,2,3,5,6,7,8' lastused='2002' name='financial statements' start='724033'>
			<variant>Financial Statements</variant>
			<variant>financial statements</variant>
		</canonskill>
		<canonskill end='729482' experience='1' expidrefs='3' idrefs='3' lastused='1998' name='forms' start='728327'>
			<variant>forms</variant>
		</canonskill>
		<canonskill end='731126' experience='1' expidrefs='1' idrefs='1' lastused='2002' name='human resources' start='729908'>
			<variant>human resources</variant>
		</canonskill>
		<canonskill end='729908' experience='1' expidrefs='2,3' idrefs='2,3' lastused='1999' name='jd edwards' start='728327'>
			<variant>J.D. Edwards</variant>
		</canonskill>
		<canonskill end='729482' experience='1' expidrefs='3' idrefs='3' lastused='1998' name='lotus' start='728327'>
			<variant>Lotus</variant>
		</canonskill>
		<canonskill end='724795' experience='1' expidrefs='8' idrefs='8' lastused='1985' name='management consulting' start='724033'>
			<variant>management consultant</variant>
		</canonskill>
		<canonskill end='728265' experience='1' expidrefs='4' idrefs='4' lastused='1994' name='marketing' start='727931'>
			<variant>Market</variant>
		</canonskill>
		<canonskill end='729482' experience='1' expidrefs='3' idrefs='3' lastused='1998' name='microsoft excel' start='728327'>
			<variant>Excel</variant>
		</canonskill>
		<canonskill end='729908' experience='1' expidrefs='2' idrefs='2' lastused='1999' name='microsoft outlook' start='729482'>
			<variant>Microsoft Outlook</variant>
		</canonskill>
		<canonskill end='729482' experience='1' expidrefs='3' idrefs='3' lastused='1998' name='microsoft word' start='728327'>
			<variant>Word</variant>
		</canonskill>
		<canonskill end='728265' experience='1' expidrefs='4' idrefs='4' lastused='1994' name='mis' start='727931'>
			<variant>management information system</variant>
		</canonskill>
		<canonskill end='728265' experience='1' expidrefs='4' idrefs='4' lastused='1994' name='negotiation' start='727931'>
			<variant>negotiating</variant>
		</canonskill>
		<canonskill end='727931' experience='1' expidrefs='5' idrefs='5' lastused='1994' name='networking' start='725586'>
			<variant>networking</variant>
		</canonskill>
		<canonskill end='731126' experience='1' expidrefs='1,4,5' idrefs='1,4,5' lastused='2002' name='payroll processing' start='725586'>
			<variant>payroll</variant>
		</canonskill>
		<canonskill end='728265' experience='1' expidrefs='4' idrefs='4' lastused='1994' name='pricing' start='727931'>
			<variant>pricing</variant>
		</canonskill>
		<canonskill end='728265' experience='1' expidrefs='4' idrefs='4' lastused='1994' name='profit' start='727931'>
			<variant>profit</variant>
		</canonskill>
		<canonskill end='725190' experience='1' expidrefs='7' idrefs='7' lastused='1986' name='programming' start='724795'>
			<variant>programming</variant>
		</canonskill>
		<canonskill end='731126' experience='1' expidrefs='1' idrefs='1' lastused='2002' name='prolog' start='729908'>
			<variant>Prolog</variant>
		</canonskill>
		<canonskill end='725555' experience='1' expidrefs='6' idrefs='6' lastused='1987' name='proposal writing' start='725221'>
			<variant>proposals</variant>
		</canonskill>
		<canonskill end='728265' experience='1' expidrefs='4' idrefs='4' lastused='1994' name='quality' start='727931'>
			<variant>quality</variant>
		</canonskill>
		<canonskill end='728265' experience='1' expidrefs='4' idrefs='4' lastused='1994' name='reports' start='727931'>
			<variant>reporting</variant>
		</canonskill>
		<canonskill end='728265' experience='1' expidrefs='4' idrefs='4' lastused='1994' name='risk management' start='727931'>
			<variant>risk management</variant>
		</canonskill>
		<canonskill end='729482' experience='1' expidrefs='3' idrefs='3' lastused='1998' name='sales' start='728327'>
			<variant>Sales</variant>
			<variant>sales</variant>
		</canonskill>
		<canonskill end='728265' experience='1' expidrefs='4' idrefs='4' lastused='1994' name='spreadsheets' start='727931'>
			<variant>spreadsheet</variant>
		</canonskill>
		<canonskill end='727931' experience='1' expidrefs='5,6,7' idrefs='5,6,7' lastused='1994' name='supervisory skills' start='724795'>
			<variant>supervising</variant>
		</canonskill>
		<canonskill end='731126' experience='1' expidrefs='1,2,3,4,8' idrefs='1,2,3,4,8' lastused='2002' name='tax planning' start='724033'>
			<variant>tax</variant>
			<variant>taxes</variant>
		</canonskill>
		<canonskill end='729482' experience='1' expidrefs='3' idrefs='3' lastused='1998' name='theatre' start='728327'>
			<variant>Theatre</variant>
		</canonskill>
		<canonskill end='729482' experience='1' expidrefs='3' idrefs='3' lastused='1998' name='wordperfect' start='728327'>
			<variant>Word Perfect</variant>
		</canonskill>
		<canonskill end='729908' experience='1' expidrefs='2,3' idrefs='2,3' lastused='1999' name='year 2000' start='728327'>
			<variant>Y2K</variant>
		</canonskill>
	</skillrollup>
</ResDoc>
";

		#endregion

		#region MockPostingXml

		public const string MockPostingXml =
			@"<?xml version='1.0' encoding='iso-8859-1'?><jobposting><postinghtml>&lt;html&gt;
  &lt;body&gt;
    &lt;p&gt;&lt;img src=""http://talent-testnextgen.burning-glass.com/logo/4779742""&gt;&lt;/p&gt;
    &lt;p&gt;&lt;b&gt;Senior Adults Director&lt;/b&gt;&lt;br&gt;Test Company 01 Ltd A&lt;br&gt;Testerville, ID (00000)&lt;br&gt;Number of openings: 21&lt;br&gt;Application closing date: 7/25/2013&lt;br&gt;&lt;br&gt;Salary range $123.00 - $3434.00 Weekly&lt;br&gt;Normal working days are Monday and Saturday&lt;br&gt;Healthcare employer and Advanced manufacturing employer&lt;/p&gt;
    &lt;p&gt;* Provide overall direction and leadership in  private sector organization.
&lt;br&gt;* Determine company policy
&lt;br&gt;* Report to the board of directors
&lt;br&gt;* Plan operational activities
&lt;br&gt;* Direct operational activities at the highest level of management
&lt;br&gt;&lt;/p&gt;
    &lt;p&gt;&lt;b&gt;Requirements&lt;/b&gt;&lt;br&gt;Applicants must have at least a High school diploma/GED or equivalent&lt;br&gt;Applicants must have at least 1 years 2 months experience&lt;br&gt;Applicants must be at least 34 (No reason)&lt;br&gt;Applicants must hold a Motorcycle driving license with Pass transport, Hazardous materials, Tank vehicle and Tank hazard endorsements&lt;br&gt;Applicants must hold the occupational license of No License&lt;br&gt;Applicants must hold No certificate&lt;br&gt;Applicant must Know driving&lt;br&gt;Applicant must Know how to play games&lt;br&gt;Applicant must Know how to fly kite&lt;/p&gt;
    &lt;p&gt;&lt;b&gt;Benefits&lt;/b&gt;&lt;br&gt;Leave benefits include Paid holidays and Sick&lt;br&gt;Retirement benefits include Pension plan&lt;br&gt;Insurance benefits include Dental and Disability&lt;/p&gt;
    &lt;p&gt;&lt;b&gt;About Test Company 01 Ltd A&lt;/b&gt;&lt;br&gt;Testing Company.....&lt;/p&gt;
    &lt;p&gt;&lt;b&gt;How to apply&lt;/b&gt;&lt;br&gt;&lt;a href=""http://careerexplorerdev.usdev.burninglass.com"" target=""_blank""&gt;Log in to OK Job Match and submit your resume&lt;/a&gt;&lt;/p&gt;
    &lt;p&gt;&lt;small&gt;REF/29441/4784121&lt;/small&gt;&lt;/p&gt;
  &lt;/body&gt;
&lt;/html&gt;</postinghtml>
<joburl></joburl><JobDoc>
  <special>
    <cf001>1</cf001>
    <cf002>734894</cf002>
    <cf003>735076</cf003>
    <cf004>0</cf004>
    <cf005>0</cf005>
    <cf006>1</cf006>
    <cf007>FT0004784121</cf007>
    <cf008>11101100</cf008>
    <cf009>0</cf009>
    <cf010>0</cf010>
    <cf011>1</cf011>
    <cf012>7</cf012>
    <cf013></cf013>
    <cf014>Test</cf014>
    <cf015>0</cf015>
    <cf016>0</cf016>
    <cf017>0</cf017>
    <cf018>0</cf018>
    <cf019>0</cf019>
    <cf020>0</cf020>
    <cf021>0</cf021>
    <cf022>0</cf022>
    <cf023>0</cf023>
    <cf024>211101100,30000Test</cf024>
    <cf025></cf025>
		<cf026></cf026>
		<cf027></cf027>
		<cf028></cf028>
		<cf029></cf029>
		<cf030></cf030>
		<cf031></cf031>
		<cf032></cf032>
		<cf033></cf033>
		<cf034></cf034>
    <mode>update</mode>
    <originid>7</originid>
    <job_status_cd>1</job_status_cd>
    <office_id></office_id>
    <jobtitle>Senior Adults Director</jobtitle>
    <jobemployer>Test Company 01 Ltd A</jobemployer>
    <foreignpostingid>FT0004784121</foreignpostingid>
    <jobtype>4</jobtype>
    <JOBTYPE_CD>4</JOBTYPE_CD>
  <green>0</green><cf026>0</cf026></special>
  <posting>
    <contact>
      <company>Test Company 01 Ltd A</company>
      <person_title>Mr</person_title>
      <person>DevClientEmp ClientEmployee</person>
      <address>
        <street></street>
        <street></street>
        <city>Testerville</city>
        <state>ID</state>
        <county>Franklin</county>
        <postalcode>00000</postalcode>
        <country>US</country>
      </address>
      <email>client@Employee.com</email>
      <joburl></joburl>
      <phone></phone>
      <phone></phone>
      <phone_ext></phone_ext>
      <phone_ext></phone_ext>
      <fax></fax>
    </contact>
    <duties>
      <title>Senior Adults Director</title>
      <onettitle>Chief Executives</onettitle>* Provide overall direction and leadership in  private sector organization.
* Determine company policy
* Report to the board of directors
* Plan operational activities
* Direct operational activities at the highest level of management
</duties>
    <background>
      <experience>14</experience>
    </background>
    <benefits>
      <salary>123.00-3434.00 $ Weekly</salary>
    </benefits>
    <qualifications>
      <required>
        <certification>No License, No certificate</certification>
        <training></training>
      </required>
    </qualifications>
    <skills>
      <languages></languages>
    </skills>
    <jobinfo>
      <educationlevel>2</educationlevel>
      <positioncount>21</positioncount>
			<JOB_TYPE>2</JOB_TYPE>
    </jobinfo>
  <qualifications><required><education><degree>High School Diploma</degree></education></required></qualifications></posting>
  <clientjobdata />
</JobDoc><clientjobdata><JOBDOC ORIGINATION_METHOD_CD=""7"" MODE=""update"" ID=""FT0004784121""><JOB_CREATE_DATE>2013-01-24</JOB_CREATE_DATE><JOB_TITLE>Senior Adults Director</JOB_TITLE><JOB_STATUS_CD>1</JOB_STATUS_CD><JOB_CATEGORY_CD>A</JOB_CATEGORY_CD><OFFICE_ID /><COMPANY>Test Company 01 Ltd A</COMPANY><COMPANY_NAICS>Testing</COMPANY_NAICS><EMPLOYER_CONTACT_TITLE>Mr</EMPLOYER_CONTACT_TITLE><EMPLOYER_CONTACT_FIRST_NAME>DevClientEmp</EMPLOYER_CONTACT_FIRST_NAME><EMPLOYER_CONTACT_LAST_NAME>ClientEmployee</EMPLOYER_CONTACT_LAST_NAME><JOB_CONTACT_ADDR_1 /><JOB_CONTACT_ADDR_2 /><JOB_CONTACT_CITY> </JOB_CONTACT_CITY><JOB_CONTACT_STATE>KY</JOB_CONTACT_STATE><JOB_CONTACT_ZIP>40601</JOB_CONTACT_ZIP><JOB_CONTACT_COUNTRY>US</JOB_CONTACT_COUNTRY><JOB_CONTACT_COUNTY>21073</JOB_CONTACT_COUNTY><JOB_CONTACT_PHONE_PRI /><JOB_CONTACT_PHONE_PRI_EXT /><JOB_CONTACT_PHONE_SEC /><JOB_CONTACT_PHONE_SEC_EXT /><JOB_CONTACT_URL_PRI /><JOB_CONTACT_FAX_PRI /><JOB_CONTACT_EMAIL>client@Employee.com</JOB_CONTACT_EMAIL><CONTACT_SEND_DIRECT_FLAG>0</CONTACT_SEND_DIRECT_FLAG><CONTACT_POSTAL_FLAG>0</CONTACT_POSTAL_FLAG><CONTACT_PHONE_FLAG>0</CONTACT_PHONE_FLAG><CONTACT_FAX_FLAG>0</CONTACT_FAX_FLAG><CONTACT_EMAIL_FLAG>0</CONTACT_EMAIL_FLAG><CONTACT_URL_FLAG>0</CONTACT_URL_FLAG><CONTACT_TALENT_FLAG>1</CONTACT_TALENT_FLAG><SAL_MIN>123.00</SAL_MIN><SAL_MAX>3434.00</SAL_MAX><SAL_UNIT_CD>3</SAL_UNIT_CD><EDUCATION_CD>2</EDUCATION_CD><JOB_REQUIRED_LIC_CERT>No License, No certificate</JOB_REQUIRED_LIC_CERT><JOB_REQUIRED_TRAINING /><EXPERIENCE_IN_MONTHS>14</EXPERIENCE_IN_MONTHS><DURATION>1</DURATION><JOB_DESCRIPTION>* Provide overall direction and leadership in  private sector organization.
* Determine company policy
* Report to the board of directors
* Plan operational activities
* Direct operational activities at the highest level of management
</JOB_DESCRIPTION><JOB_LAST_OPEN_DATE>2013-07-25</JOB_LAST_OPEN_DATE><WORK_VARY_FLAG>0</WORK_VARY_FLAG><WORK_MON_FLAG>1</WORK_MON_FLAG><WORK_TUE_FLAG>0</WORK_TUE_FLAG><WORK_WED_FLAG>0</WORK_WED_FLAG><WORK_THU_FLAG>0</WORK_THU_FLAG><WORK_FRI_FLAG>0</WORK_FRI_FLAG><WORK_SAT_FLAG>1</WORK_SAT_FLAG><WORK_SUN_FLAG>0</WORK_SUN_FLAG><DRIVER_CLASS_CD>5</DRIVER_CLASS_CD><SHORTHAND_SPEED>0</SHORTHAND_SPEED><TYPING_SPEED>0</TYPING_SPEED><AGE_MIN>34</AGE_MIN><ONET_CODE>11101100</ONET_CODE><ONET_TITLE>Chief Executives</ONET_TITLE><REFERRAL_DESIRED_CT>0</REFERRAL_DESIRED_CT><REFERRAL_PROVIDED_CT>0</REFERRAL_PROVIDED_CT><POSITION_CT>21</POSITION_CT><JOB_LOCATION_CITY>Testerville</JOB_LOCATION_CITY><JOB_LOCATION_STATE>ID</JOB_LOCATION_STATE><JOB_LOCATION_ZIP>00000</JOB_LOCATION_ZIP><BENEFITS><BENEFIT401K>0</BENEFIT401K><MEDICALLEAVE>0</MEDICALLEAVE><LEAVESHARING>0</LEAVESHARING><PROFITSHARING>0</PROFITSHARING><DISABILITYINSURANCE>1</DISABILITYINSURANCE><HEALTHSAVINGS>0</HEALTHSAVINGS><LIFEINSURANCE>0</LIFEINSURANCE><VISIONINSURANCE>0</VISIONINSURANCE><RELOCATION>0</RELOCATION><TUITIONASSISTANCE>0</TUITIONASSISTANCE><BENEFITSNEGOTIABLE>0</BENEFITSNEGOTIABLE><OTHER>1</OTHER></BENEFITS><LANGUAGES><ARABIC>0</ARABIC><CHINESE>0</CHINESE><ENGLISH>0</ENGLISH><FRENCH>0</FRENCH><GERMAN>0</GERMAN><ITALIAN>0</ITALIAN><JAPANESE>0</JAPANESE><RUSSIAN>0</RUSSIAN><SPANISH>0</SPANISH><OTHER>0</OTHER></LANGUAGES><JOB_REQUIREMENTS><BONDING>0</BONDING><CREDITCHECK>0</CREDITCHECK><CRIMINALCHECK>0</CRIMINALCHECK><DRUGSCREEN>0</DRUGSCREEN><EMPLOYERTESTING>0</EMPLOYERTESTING><HEAVYLIFTING>0</HEAVYLIFTING><JOINUNION>0</JOINUNION><OWNTOOLS>0</OWNTOOLS><PHYSICALEXAM>0</PHYSICALEXAM><OTHER>0</OTHER><OVERTIMEREQUIRED>0</OVERTIMEREQUIRED><HL_MINLBS>0</HL_MINLBS></JOB_REQUIREMENTS><REQUIREMENTS screening=""-1""><REQUIREMENT type=""AGE"" mandate=""1""><MIN>34</MIN><REASON>No reason</REASON></REQUIREMENT><REQUIREMENT type=""CERTIFICATIONS"" mandate=""1""><CERTIFICATE>No certificate</CERTIFICATE></REQUIREMENT><REQUIREMENT type=""DRIVERS_LICENSE"" mandate=""1""><CLASS>Motorcycle</CLASS><ENDORSEMENTS>Pass transport, Hazardous materials, Tank vehicle, Tank hazard</ENDORSEMENTS></REQUIREMENT><REQUIREMENT type=""LICENSES"" mandate=""1""><LICENSE>No License</LICENSE></REQUIREMENT><REQUIREMENT type=""MIN_EDU"" mandate=""1"">High school diploma or equivalent</REQUIREMENT><REQUIREMENT type=""PROGRAMSOFSTUDY"" mandate=""1""><PROGRAMOFSTUDY ID=""16152"">Associate's Degree - Nursing</PROGRAMOFSTUDY><PROGRAMOFSTUDY ID=""16154"">Associate's Degree - Paralegal Studies</PROGRAMOFSTUDY></REQUIREMENT><REQUIREMENT type=""SPECIAL"" mandate=""1""><SPECIAL>Know driving</SPECIAL><SPECIAL>Know how to play games</SPECIAL><SPECIAL>Know how to fly kite</SPECIAL></REQUIREMENT></REQUIREMENTS></JOBDOC></clientjobdata>
<jobstatus>ACTIVE</jobstatus><special><jobviewed count=""2"" /><match jobseekermatchcount=""0"" veteranmatchcount=""0"" /></special></jobposting>";

		#endregion

		#region MockPostingDataElementsXml

		internal const string MockPostingDataElementsXml = @"<DataElementsRollup version=""5.1.21 JobMine v3.2.0.0"">
<JobID />
<SourceID />
<CanonJobTitle />
<CleanJobTitle>Senior Adults Director</CleanJobTitle>
<StandardTitle>Senior Director</StandardTitle>
<CanonEmployer />
<EmployerParent />
<EmployerTicker />
<LocationSpecificInformation>
  <city />
  <state />
  <county />
  <country />
  <zipcode />
  <geocode />
  <msa />
  <lma />
  <phoneareacode />
  <divisioncode />
</LocationSpecificInformation>
<Telephone />
<ConsolidatedInferredNAICS>33</ConsolidatedInferredNAICS>
<GreenOnet>0</GreenOnet>
<GreenOnetType />
<InternshipFlag>false</InternshipFlag>
<Salary />
<CanonSalary />
<HealthCareBenefits>true</HealthCareBenefits>
<LivingWageBenefit />
<ONETSalaryNationalAnnual />
<ONETSalaryNationalHourly />
<ONETSalaryLocalAnnual />
<ONETSalaryLocalHourly />
<CanonOtherDegrees />
<OtherDegreeLevels />
<Major />
<MajorCode />
<StdMajor />
<CIPCode />
<YearsOfExperience>1 years 2 months</YearsOfExperience>
<CanonYearsOfExperience>
<min>12</min>
<max />
<level>low</level>
<canonlevel>0-1</canonlevel>
</CanonYearsOfExperience>
<Skills>leadership|organization</Skills>
<CanonSkills>
<canonskill name=""Leadership"" skill-cluster=""Common Skills: Business Environment Skills"" />
<canonskill name=""Organizational Skills"" skill-cluster=""Common Skills: Business Environment Skills"" />
</CanonSkills>
<CanonSkillClusters>Common Skills: Business Environment Skills|Business: Process And Planning</CanonSkillClusters>
<SkillClusterIndex>
<skillcluster name=""Common Skills: Business Environment Skills"">Leadership|Organizational Skills</skillcluster>
</SkillClusterIndex>
<GreenIndustry>0</GreenIndustry>
<CanonGreenIndustry />
<GreenCompany>0</GreenCompany>
<GreenBLSNAICS>0</GreenBLSNAICS>
<NumberOfOpenings>21</NumberOfOpenings>
<CanonNumberOfOpenings>21</CanonNumberOfOpenings>
<JobType>4</JobType>
<CanonJobType />
<JobDate />
<Intermediary />
<CanonIntermediary />
<RootTitle>director</RootTitle>
<EMail>client@Employee.com</EMail>
<JobUrl />
<JobDomain />
<JobTitleRule />
<EmployerRule />
<JobReferenceID />
<OpeningDate />
<CanonOpeningDate />
<ClosingDate>7/25/2013</ClosingDate>
<CanonClosingDate />
<Blacklist>0</Blacklist>
<ConsolidatedONET>21-2021.00</ConsolidatedONET>
<ConsolidatedONETRank>4</ConsolidatedONETRank>
<BGTOcc priority=""724"">21-1091.00</BGTOcc>
<ConsolidatedDegree />
<ConsolidatedDegreeLevels />
<CanonMinimumDegree />
<CanonMaximumDegree />
<MinDegreeLevel />
<MaxDegreeLevel />
<CanonPreferredDegrees />
<PreferredDegreeLevels />
<CanonRequiredDegrees />
<RequiredDegreeLevels />
<Certification />
<CanonCertification /></DataElementsRollup>";

		#endregion

		[SetUp]
		public virtual void SetUp()
		{
			DoSetup(true);
		}

		/// <summary>
		/// Does the setup.
		/// </summary>
		/// <param name="useTransactionScope">if set to <c>true</c> [use transaction scope].</param>
		/// <param name="isWcfService">Whether a WCF service is being set up</param>
		protected void DoSetup(bool useTransactionScope, bool isWcfService = false)
		{
			if (useTransactionScope)
				_transactionScope = new TransactionScope(TransactionScopeOption.Required, new TimeSpan(0, 10, 0));

			RuntimeContext = new TestRuntimeContext { AppSettings = MockAppSettings, Repositories = new TestRepositories(), Helpers = new TestHelpers() };

			#region Setup Repositores

			ConfigurationUnitOfWorkScope = new SimpleUnitOfWorkScope<ConfigurationUnitOfWork>(ConfigurationContext);
			DataCoreUnitOfWorkScope = new SimpleUnitOfWorkScope<DataCoreUnitOfWork>(DataCoreContext);
			LibraryUnitOfWorkScope = new SimpleUnitOfWorkScope<LibraryUnitOfWork>(LibraryContext);
			ReportUnitOfWorkScope = new SimpleUnitOfWorkScope<ReportUnitOfWork>(ReportContext);

			var lensRepositoryMock = new Mock<ILensRepository>();
			lensRepositoryMock.Setup(x => x.IsLicensed()).Returns(true);
			LensRepository = lensRepositoryMock.Object;

			var documentRepositoryMock = new Mock<IDocumentRepository>();
			documentRepositoryMock.Setup(x => x.GetDocumentText(It.IsAny<string>())).Returns(MockPostingXml);
			DocumentRepository = documentRepositoryMock.Object;

			((TestRepositories)RuntimeContext.Repositories).Configuration = ConfigurationRepository = new ConfigurationRepository(ConfigurationUnitOfWorkScope);
			((TestRepositories)RuntimeContext.Repositories).Library = LibraryRepository = new LibraryRepository(LibraryUnitOfWorkScope);
			((TestRepositories)RuntimeContext.Repositories).Core = DataCoreRepository = new CoreRepository(DataCoreUnitOfWorkScope);
			((TestRepositories)RuntimeContext.Repositories).Document = DocumentRepository;
			((TestRepositories)RuntimeContext.Repositories).Report = ReportRepository = new ReportRepository(ReportUnitOfWorkScope);
			((TestRepositories)RuntimeContext.Repositories).Integration = new IntegrationRepository(RuntimeContext, MockAppSettings.IntegrationClient, "");
      ((TestRepositories)RuntimeContext.Repositories).LaborInsight = new LaborInsightRepository(RuntimeContext);

			MigrationUnitOfWorkScope = new SimpleUnitOfWorkScope<DataMigrationUnitOfWork>(DataMigrationContext);
			MigrationRepository = new MigrationRepository(MigrationUnitOfWorkScope);

			#endregion

			#region Setup Helpers

			((TestHelpers)RuntimeContext.Helpers).Configuration = ConfigurationHelper = new ConfigurationHelper(RuntimeContext);
			((TestHelpers)RuntimeContext.Helpers).Lookup = LookupHelper = new LookupHelper(RuntimeContext);
			((TestHelpers)RuntimeContext.Helpers).Occupation = OccupationHelper = new OccupationHelper(RuntimeContext);
			((TestHelpers)RuntimeContext.Helpers).Organization = OrganizationHelper = new OrganizationHelper(RuntimeContext);
			((TestHelpers)RuntimeContext.Helpers).Resume = ResumeHelper = new ResumeHelper(RuntimeContext);
			((TestHelpers)RuntimeContext.Helpers).Session = SessionHelper = new TestSessionHelper();
			((TestHelpers)RuntimeContext.Helpers).Messaging = MessagingHelper = new TestMessagingHelper(RuntimeContext);
			((TestHelpers)RuntimeContext.Helpers).Posting = PostingHelper = new PostingHelper(RuntimeContext);
			((TestHelpers)RuntimeContext.Helpers).JobDevelopment = JobDevelopmentHelper = new JobDevelopmentHelper(RuntimeContext);
			((TestHelpers)RuntimeContext.Helpers).Logging = LoggingHelper = new LoggingHelper(RuntimeContext);
			((TestHelpers)RuntimeContext.Helpers).Localisation = LocalisationHelper = new LocalisationHelper(RuntimeContext);
			((TestHelpers)RuntimeContext.Helpers).Alerts = AlertsHelper = new AlertsHelper(RuntimeContext);
			((TestHelpers)RuntimeContext.Helpers).Email = EmailHelper = new TestEmailHelper(RuntimeContext);
			((TestHelpers)RuntimeContext.Helpers).Search = SearchHelper = new SearchHelper(RuntimeContext);
			((TestHelpers)RuntimeContext.Helpers).Office = OfficeHelper = new OfficeHelper(RuntimeContext);
			((TestHelpers)RuntimeContext.Helpers).Candidate = CandidateHelper = new CandidateHelper(RuntimeContext);
			((TestHelpers)RuntimeContext.Helpers).SelfService = SelfServiceHelper = new SelfServiceHelper(RuntimeContext);
			((TestHelpers)RuntimeContext.Helpers).Employer = EmployerHelper = new EmployerHelper(RuntimeContext);
			((TestHelpers)RuntimeContext.Helpers).Employee = EmployeeHelper = new EmployeeHelper(RuntimeContext);
			((TestHelpers)RuntimeContext.Helpers).Validation = ValidationHelper = new ValidationHelper(RuntimeContext);
			((TestHelpers)RuntimeContext.Helpers).Encryption = EncryptionHelper = new EncryptionHelper(RuntimeContext);
			((TestHelpers)RuntimeContext.Helpers).PushNotification = PushNotificationHelper = new PushNotificationHelper( RuntimeContext );
			((TestHelpers)RuntimeContext.Helpers).User = UserHelper = new UserHelper( RuntimeContext );

			#endregion

			// Setup the UserData
			MockUserData = new UserContext
			{
				UserId = MockUserId, ActionerId = MockUserId, FirstName = MockFirstName, LastName = MockLastName, EmailAddress = MockEmailAddress,
				Culture = MockCulture, EmployerId = MockEmployerId, EmployeeId = MockEmployeeId, PersonId = MockPersonId, EnrollmentStatus = SchoolStatus.NA
			};

			MockCareerUserData = new UserContext
			{
				UserId = MockCareerUserId,
				ActionerId = MockCareerUserId,
				FirstName = MockCareerFirstName,
				LastName = MockCareerLastName,
				EmailAddress = MockCareerEmailAddress,
				Culture = MockCulture,
				PersonId = MockCareerPersonId,
				EnrollmentStatus = SchoolStatus.NA
			};

			MockImpersonatedCareerUserData = new UserContext
			{
				UserId = MockCareerUserId,
				ActionerId = MockUserId,
				FirstName = MockCareerFirstName,
				LastName = MockCareerLastName,
				EmailAddress = MockCareerEmailAddress,
				Culture = MockCulture,
				PersonId = MockCareerPersonId,
				EnrollmentStatus = SchoolStatus.NA
			};

			if (!isWcfService)
			{
				var request = new SessionRequest();
				request.Prepare(MockClientTag, Guid.Empty, MockUserData, MockCulture);

				var result = (new AuthenticationService(RuntimeContext)).StartSession(request);

				MockSessionId = result.SessionId;
			}

			// Setup the Service Locator
			var serviceLocator = new StructureMapServiceLocator();
			ServiceLocator.SetServiceLocator(serviceLocator);
			serviceLocator.Register(ConfigurationHelper);
			if (isWcfService)
				serviceLocator.Register(() => RuntimeContext);
		}

		[TearDown]
		public virtual void TearDown()
		{
			if (ConfigurationUnitOfWorkScope.HasCurrent)
				ConfigurationUnitOfWorkScope.Current.Dispose();

			if (DataCoreUnitOfWorkScope.HasCurrent)
				DataCoreUnitOfWorkScope.Current.Dispose();

			if (LibraryUnitOfWorkScope.HasCurrent)
				LibraryUnitOfWorkScope.Current.Dispose();

			if (MigrationUnitOfWorkScope.HasCurrent)
				MigrationUnitOfWorkScope.Current.Dispose();

			if (_transactionScope != null)
				_transactionScope.Dispose();
		}

		/// <summary>
		/// Sets up cache repository.
		/// </summary>
		/// <returns></returns>
		public void SetUpCacheProvider()
		{
			ICacheProvider cacheProvider = new CacheToHttpRuntime(CachePrefix);
			cacheProvider.Clear();

			Cacher.Initialize(cacheProvider);
		}

		/// <summary>
		/// Sets up a mock log provider.
		/// </summary>
		protected void SetUpMockLogProvider()
		{
			_mockLogProvider = new Mock<ILogProvider>();
			Logger.Instance.Attach(_mockLogProvider.Object);
			Profiler.Instance.Attach(_mockLogProvider.Object);
		}

		public void SetUpEmailProvider()
		{
			IEmailProvider emailProvider = new EmailToFile("");
			Emailer.Instance.Attach(emailProvider);
		}

		protected int MessageCount(Type messageType)
		{
			if (RuntimeContext.Helpers.Messaging.GetType() != typeof(TestMessagingHelper))
				throw new NotImplementedException();

			var testMessagingHelper = ((TestMessagingHelper)RuntimeContext.Helpers.Messaging);

			return (testMessagingHelper.Messages.IsNotNullOrEmpty()) ? testMessagingHelper.Messages.Count(x => x.Message.GetType() == messageType) : 0;
		}
	}

	public class TestRuntimeContext : IRuntimeContext
	{
		public IAppSettings AppSettings { get; set; }
		public IRepositories Repositories { get; set; }
		public IHelpers Helpers { get; set; }

		/// <summary>
		/// Gets the current request.
		/// </summary>
		public IServiceRequest CurrentRequest { get; set; }

		/// <summary>
		/// Sets the request.
		/// </summary>
		/// <param name="serviceRequest">The service request.</param>
		public void SetRequest(IServiceRequest serviceRequest)
		{
			CurrentRequest = serviceRequest;
		}

		/// <summary>
		/// Gets the request identifiers.
		/// </summary>
		/// <param name="sessionId">The session identifier.</param>
		/// <param name="requestId">The request identifier.</param>
		/// <param name="userId">The user identifier.</param>
		public void GetRequestIdentifiers(out Guid sessionId, out Guid requestId, out long userId)
		{
			requestId = sessionId = Guid.Empty;
			userId = 0;
		}
	}

	public class TestRepositories : IRepositories
	{
		public IConfigurationRepository Configuration { get; set; }
		public ILibraryRepository Library { get; set; }
		public ICoreRepository Core { get; set; }
		public ILensRepository Lens { get; set; }
		public IDocumentRepository Document { get; set; }
		public IReportRepository Report { get; set; }
		public ILiveJobsRepository LiveJobs { get; set; }
		public IMigrationRepository Migration { get; set; }
		public IIntegrationRepository Integration { get; set; }
		public ILaborInsightRepository LaborInsight { get; set; }
	}

	public class TestHelpers : IHelpers
	{
		public IConfigurationHelper Configuration { get; set; }
		public ILookupHelper Lookup { get; set; }
		public IOccupationHelper Occupation { get; set; }
		public IOrganizationHelper Organization { get; set; }
		public IResumeHelper Resume { get; set; }
		public ISessionHelper Session { get; set; }
		public IMessagingHelper Messaging { get; set; }
		public IPostingHelper Posting { get; set; }
		public ILoggingHelper Logging { get; set; }
		public ILocalisationHelper Localisation { get; set; }
		public IAlertsHelper Alerts { get; set; }
		public IEmailHelper Email { get; set; }
		public ISearchHelper Search { get; set; }
		public IJobDevelopmentHelper JobDevelopment { get; set; }
		public IOfficeHelper Office { get; set; }
		public ICandidateHelper Candidate { get; set; }
		public IEmployerHelper Employer { get; set; }
		public IEmployeeHelper Employee { get; set; }
		public ISelfServiceHelper SelfService { get; set; }
		public IValidationHelper Validation { get; set; }
		public IEncryptionHelper Encryption { get; set; }
		public IPushNotificationHelper PushNotification { get; set; }
		public IUserHelper User { get; set; }

	}
}
