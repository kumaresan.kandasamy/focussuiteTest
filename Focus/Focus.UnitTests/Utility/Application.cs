﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;

using Focus.Core;
using Focus.Core.Criteria;
using Focus.Data.Core.Entities;
using Focus.Core.Criteria.CandidateSearch;

using Framework.Core;

using NUnit.Framework;

#endregion



namespace Focus.UnitTests.Utility
{
	[TestFixture]
	public class Application : UtilityBase
	{
		[Test]
		[Explicit]
		public void CreateEmployer()
		{
			const string employerName = "DevEmployer";
			const string federalEmployerIdentificationNumber = "00-000000";
			const string stateEmployerIdentificationNumber = "000-0000-0";
			const string url = "http://" + "www.dev-employer.com";
			const string phone = "000 000 0000";

			const string addressLine1 = "1 Dev Street";
			const string townCity = "Dev City";
			const string postcodeZip = "00000";

			const string industrialClassification = "Development";

			using (var txn = DataCoreUnitOfWorkScope.Current.BeginTransaction())
			{
				var employer = new Employer
				               	{
				               		Name = employerName,
				               		FederalEmployerIdentificationNumber = federalEmployerIdentificationNumber,
				               		StateEmployerIdentificationNumber = stateEmployerIdentificationNumber,
				               		ApprovalStatus = ApprovalStatuses.Approved,
				               		Url = url,
				               		OwnershipTypeId = 0,
				               		TermsAccepted = true,
				               		PrimaryPhone = phone,
				               		IsRegistrationComplete = true,
				               		IndustrialClassification = industrialClassification
				               	};

				var employerAddress = new EmployerAddress
				                      	{
				                      		Line1 = addressLine1,
				                      		Line2 = "",
				                      		Line3 = "",
				                      		TownCity = townCity,
				                      		CountyId = 0,
				                      		PostcodeZip = postcodeZip,
				                      		CountryId = 0,
				                      		StateId = 0,
				                      		IsPrimary = true
				                      	};

				employer.EmployerAddresses.Add(employerAddress);

				DataCoreUnitOfWorkScope.Current.Add(employer);

				DataCoreUnitOfWorkScope.Current.SaveChanges();
				txn.Commit();
			}
		}

		[Test]
		[Explicit]
		public void CreateEmployee()
		{
			const string employerName = "DevEmployer";
			const string firstName = "Dev";
			const string lastName = "Employee";

			using (var txn = DataCoreUnitOfWorkScope.Current.BeginTransaction())
			{
				var employer = DataCoreUnitOfWorkScope.Current.Employers.FirstOrDefault(x => x.Name == employerName);
				if (employer.IsNull()) throw new Exception("Employer not found");

				var person =
					DataCoreUnitOfWorkScope.Current.Persons.FirstOrDefault(x => x.FirstName == firstName && x.LastName == lastName);
				if (person.IsNull()) throw new Exception("Person not found");

				DataCoreUnitOfWorkScope.Current.Add(new Employee
				                                 	{ApprovalStatus = ApprovalStatuses.Approved, Employer = employer, Person = person});

				DataCoreUnitOfWorkScope.Current.SaveChanges();
				txn.Commit();
			}
		}

		[Test]
		[Explicit]
		public void CreatePerson()
		{
			const string firstName = "Integration";
			const string lastName = "User";

			using (var txn = DataCoreUnitOfWorkScope.Current.BeginTransaction())
			{
				var person =
					DataCoreUnitOfWorkScope.Current.Persons.FirstOrDefault(x => x.FirstName == firstName && x.LastName == lastName);
				if (person.IsNotNull()) throw new Exception("Person already exists");

				DataCoreUnitOfWorkScope.Current.Add(new Person
				                                 	{
				                                 		FirstName = firstName,
				                                 		LastName = lastName,
				                                 		MiddleInitial = "",
				                                 		DateOfBirth = DateTime.Now,
				                                 		TitleId = 0,
				                                 		JobTitle = "Focus Talent / Assist Integration User",
				                                 		SocialSecurityNumber = "000-00-0000"
				                                 	});

				DataCoreUnitOfWorkScope.Current.SaveChanges();
				txn.Commit();
			}
		}

		[Test]
		[Explicit]
		public void CreateJob()
		{
			const string employerName = "DevEmployer";
			const string firstName = "Dev";
			const string lastName = "Client";
			const string jobTitle = "New Job Again";

			using (var txn = DataCoreUnitOfWorkScope.Current.BeginTransaction())
			{
				var employer = DataCoreUnitOfWorkScope.Current.Employers.FirstOrDefault(x => x.Name == employerName);
				if (employer.IsNull()) throw new Exception("Employer not found");

				var person =
					DataCoreUnitOfWorkScope.Current.Persons.FirstOrDefault(x => x.FirstName == firstName && x.LastName == lastName);
				if (person.IsNull()) throw new Exception("Person not found");

				var employee = DataCoreUnitOfWorkScope.Current.Employees.FirstOrDefault(x => x.PersonId == person.Id);
				if (employee.IsNull()) throw new Exception("Employee not found");

				var businessUnit = employer.BusinessUnits.FirstOrDefault(x => x.IsPrimary);
				if (businessUnit.IsNull()) throw new Exception("Employee not found");

				var logo = businessUnit.BusinessUnitLogos.FirstOrDefault();
				if (logo.IsNull()) throw new Exception("Business Unit Logo not found");

				var postingSurvey = new PostingSurvey
				                    	{
				                    		DidInterview = true,
				                    		DidHire = true,
				                    		SatisfactionLevel = 100,
				                    		SurveyType = SurveyTypes.JobRefreshSurvey
				                    	};


				var jobAddress = new JobAddress
				                 	{
				                 		CountryId = 0,
				                 		CountyId = 0,
				                 		Line1 = "Line1" + jobTitle,
				                 		Line2 = "Line2",
				                 		Line3 = "Line3",
				                 		IsPrimary = true,
				                 		PostcodeZip = "PostzipCode",
				                 		TownCity = "TownCity"
				                 	};

				var job = new Job
				          	{
				          		ApprovalStatus = ApprovalStatuses.Approved,
				          		JobTitle = jobTitle,
				          		PostedOn = DateTime.Now.AddMonths(-1),
				          		ClosedOn = DateTime.Now.AddMonths(2),
				          		ClosingOn = DateTime.Now.AddMonths(2),
											JobStatus = JobStatuses.Active
				          	};

				employer.Jobs.Add(job);

				job.BusinessUnitLogo = logo;
				job.PostingSurveys.Add(postingSurvey);
				job.CreatedBy = person.User.Id;
				job.UpdatedBy = person.User.Id;
				job.JobAddresses.Add(jobAddress);
				DataCoreUnitOfWorkScope.Current.Add(job);

				DataCoreUnitOfWorkScope.Current.SaveChanges();

				txn.Commit();
			}
		}

		[Test]
		[Explicit]
		public void CreateJobLocation()
		{
			const string employerName = "DevEmployer";
			const long jobOneId = 78150;
			const long jobTwoId = 78160;

			using (var txn = DataCoreUnitOfWorkScope.Current.BeginTransaction())
			{
				var employer = DataCoreUnitOfWorkScope.Current.Employers.FirstOrDefault(x => x.Name == employerName);
				if (employer.IsNull()) throw new Exception("Employer not found");

				var job1 = DataCoreUnitOfWorkScope.Current.Jobs.FirstOrDefault(x => x.Id == jobOneId);
				if (job1.IsNull()) throw new Exception("Job1 not found");

				var job2 = DataCoreUnitOfWorkScope.Current.Jobs.FirstOrDefault(x => x.Id == jobTwoId);
				if (job2.IsNull()) throw new Exception("Job2 not found");

				job1.JobLocations.Add(new JobLocation {Location = "Some place", IsPublicTransitAccessible = true});
				job1.JobLocations.Add(new JobLocation { Location = "Some other place", IsPublicTransitAccessible = true });

				job2.JobLocations.Add(new JobLocation { Location = "Some place else", IsPublicTransitAccessible = true });
				job2.JobLocations.Add(new JobLocation { Location = "Some other place else", IsPublicTransitAccessible = true });

				DataCoreUnitOfWorkScope.Current.SaveChanges();

				txn.Commit();
			}
		}


		[Test]
		[Explicit]
		public void CreateSavedSearch()
		{
			//const string clientName = "DevClient";
			const string firstName = "Dev";
			const string lastName = "Client";

			using (var txn = DataCoreUnitOfWorkScope.Current.BeginTransaction())
			{
				var user = DataCoreUnitOfWorkScope.Current.Users.FirstOrDefault(x => x.UserName == firstName + lastName);
        if (user.IsNull()) throw new Exception("User not found");

				var criteria = new CandidateSearchCriteria
				               	{
				               		PageIndex = 0,
				               		PageSize = 10,
                          FetchOption = CriteriaBase.FetchOptions.PagedList,
				               		SearchType = CandidateSearchTypes.ByJobDescription,
				               		MinimumScore = 100,
				               		ResumeCriteria =
				               			new CandidateSearchResumeCriteria {Resume = new byte[1], ResumeFileExtension = "test"},
				               		CandidatesLikeThisSearchCriteria =
				               			new CandidatesLikeThisCriteria {CandidateId = "999"},
				               		JobDetailsCriteria =
														new CandidateSearchJobDetailsCriteria { JobTitle = "Description", JobId = 888 },
													KeywordCriteria = new CandidateSearchKeywordCriteria{Context = KeywordContexts.FullResume, Keywords = "Keywords", Scope = KeywordScopes.LastTwoJobs},
				               		AdditionalCriteria = new CandidateSearchAdditionalCriteria
				               		                           	{
				               		                           		AvailabilityCriteria = new CandidateSearchAvailabilityCriteria
				               		                           		                             	{
				               		                           		                             		NormalWorkShiftId = 777
				               		                           		                             	},
				               		                           		EducationCriteria =
				               		                           			new CandidateSearchEducationCriteria {EducationLevel = EducationLevels.All},
				               		                           		Licences = new List<string> {"Lang&Lic"},
				               		                           		Languages = new List<string> {"Languages"},
				               		                           		LocationCriteria = new CandidateSearchLocationCriteria
				               		                           		                         	{
				               		                           		                         		Distance = 12,
				               		                           		                         		DistanceUnits = DistanceUnits.Miles,
				               		                           		                         		PostalCode = "postcode"
				               		                           		                         	},
				               		                           		StatusCriteria = new CandidateSearchStatusCriteria
				               		                           		                       	{USCitizen = false, Veteran = false}
				               		                           	}
				               	};


				var savedSearch = new SavedSearch
				                  	{
				                  		Name = "TestSearch (1)",
				                  		AlertEmailAddress = "test@tes.com",
				                  		AlertEmailFormat = EmailFormats.TextOnly,
				                  		AlertEmailFrequency = EmailAlertFrequencies.Weekly,
				                  		AlertEmailRequired = true,
										SearchCriteria = criteria.Serialize()
				                  	};

				savedSearch.Users.Add(user);

				DataCoreUnitOfWorkScope.Current.Add(savedSearch);
				DataCoreUnitOfWorkScope.Current.SaveChanges();
				txn.Commit();
			}
		}
	}
}
	