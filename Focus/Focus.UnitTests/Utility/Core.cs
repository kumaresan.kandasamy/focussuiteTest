﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using Focus.Common.Code;
using Focus.Core;
using Focus.Data.Configuration.Entities;
using Focus.Data.Core.Entities;
using Focus.Data.Library.Entities;
using Framework.Testing.NUnit;

using Framework.Authentication;
using Framework.Core;

using NUnit.Framework;

using Constants = Focus.Core.Constants;
using Localisation = Focus.Data.Configuration.Entities.Localisation;
using LocalisationItem = Focus.Data.Configuration.Entities.LocalisationItem;

#endregion



namespace Focus.UnitTests.Utility
{
	[TestFixture]
	public class Core : UtilityBase
	{
		[Test]
		[Explicit]
		public void JimTest()
		{
			Console.WriteLine("Started");
			
			var qry1 = from li in ConfigurationUnitOfWorkScope.Current.LocalisationItems
								 where li.LocalisationId == 1545139 && 
											 li.Key.StartsWith("Title.")
			           select li;

			var qry2 = from li in ConfigurationUnitOfWorkScope.Current.LocalisationItems
								 where li.LocalisationId == 12090 && 
											 li.Key.StartsWith("Title.") &&
											 !(from liInner in ConfigurationUnitOfWorkScope.Current.LocalisationItems
												 where liInner.LocalisationId == 1545139 &&
												 liInner.Key.StartsWith("Title.")
												 select liInner.Key).Contains(li.Key)
								 select li;

			var results = qry1.Union(qry2).ToList();

			foreach (var result in results)
				Console.WriteLine("Key = " + result.Key + " - " + result.Value + " [" + result.LocalisationId + "]");
		}

		[Test]
		[Explicit]
		public void CreateConfigurationItem()
		{
			using (var txn = DataCoreUnitOfWorkScope.Current.BeginTransaction())
      {
        AddConfigurationItem(Constants.ConfigurationItemKeys.ExplorerSectionOrder, "School|Research|Explore|Experience");
				//AddConfigurationItem(Constants.ConfigurationItemKeys.LensService, "{\"host\":\"BOSAPPSVR005\",\"portNumber\":600,\"timeout\":2003,\"vendor\":\"*\",\"characterSet\":\"iso-8859-1\",\"serviceType\":\"Job\",\"description\":\"Job repository\"}");
				//AddConfigurationItem(Constants.ConfigurationItemKeys.LensService, "{\"host\":\"BOSAPPSVR005\",\"portNumber\":600,\"timeout\":2004,\"vendor\":\"generic\",\"characterSet\":\"iso-8859-1\",\"serviceType\":\"Generic\",\"description\":\"Generic server for parsing and matching\"}");
				DataCoreUnitOfWorkScope.Current.SaveChanges();
				txn.Commit();
			}
		}

		[Test]
		[Explicit]
		public void CreateRole()
		{
			const string roleKey = Constants.RoleKeys.AssistStudentAccountAdministrator;
      const string roleValue = "Assist Student Account Administrator";

			using (var txn = DataCoreUnitOfWorkScope.Current.BeginTransaction())
			{
				var role = DataCoreUnitOfWorkScope.Current.Roles.FirstOrDefault(x => x.Key == roleKey && x.Value == roleValue);
				if (role.IsNotNull()) throw new Exception("Role already exists");

				DataCoreUnitOfWorkScope.Current.Add(new Role { Key = roleKey, Value = roleValue});

				DataCoreUnitOfWorkScope.Current.SaveChanges();
				txn.Commit();
			}
		}

    [Test]
    [Explicit]
    public void AddIsses()
    {
      using (var txn = DataCoreUnitOfWorkScope.Current.BeginTransaction())
      {
        DataCoreUnitOfWorkScope.Current.Add(new Issues { PersonId = 1000049 });
        DataCoreUnitOfWorkScope.Current.Add(new Issues { PersonId = 1087251 });
        DataCoreUnitOfWorkScope.Current.Add(new Issues { PersonId = 1087331 });
        DataCoreUnitOfWorkScope.Current.Add(new Issues { PersonId = 1087041 });
        DataCoreUnitOfWorkScope.Current.SaveChanges();
        txn.Commit();
      }
    }

		[Test]
		[Explicit]
		public void CreateUser()
		{
			const string firstName = "Integration";
			const string lastName = "User";

			using (var txn = DataCoreUnitOfWorkScope.Current.BeginTransaction())
			{
				var person = DataCoreUnitOfWorkScope.Current.Persons.FirstOrDefault(x => x.FirstName == firstName && x.LastName == lastName);
				if (person.IsNull()) throw new Exception("Person not found");

				DataCoreUnitOfWorkScope.Current.Add(new User { Person = person, PasswordHash = "BGT1ntegrat01n", PasswordSalt = "cded5112", UserName = "ukdev@aperture-control.com" });
				
				DataCoreUnitOfWorkScope.Current.SaveChanges();
				txn.Commit();
			}
		}

		[Test]
		[Explicit]
		public void CreatePassword()
		{
			const string password = "BGT1ntegrat01n";
			const string passwordSalt = "cded5112";

			var appSettings = new FakeAppSettings();

			var passwordHash = new Password(appSettings, password, passwordSalt, false).Hash;

			Console.WriteLine("Password Hash for {0} is {1}", password, passwordHash);
		}

		[Test]
		[Explicit]
		public void CreateUserRole()
		{
			const string userName = "dev@client.com";
			const string userRole = Constants.RoleKeys.AssistAccountAdministrator;

			using (var txn = DataCoreUnitOfWorkScope.Current.BeginTransaction())
			{
				var user = DataCoreUnitOfWorkScope.Current.Users.FirstOrDefault(x => x.UserName == userName);
				if (user.IsNull()) throw new Exception("User not found");

				var role = DataCoreUnitOfWorkScope.Current.Roles.FirstOrDefault(x => x.Key == userRole);
				if (role.IsNull()) throw new Exception("Role not found");

				DataCoreUnitOfWorkScope.Current.Add(new UserRole { User = user, Role = role});

				DataCoreUnitOfWorkScope.Current.SaveChanges();
				txn.Commit();
			}
		}
	
		[Test]
		[Explicit]
		public void CreateLocalisation()
		{
			using (var txn = DataCoreUnitOfWorkScope.Current.BeginTransaction())
			{
				var localisation = ConfigurationUnitOfWorkScope.Current.Localisations.FirstOrDefault(x => x.Culture == Culture);
				if (localisation.IsNotNull()) throw new Exception("Localisation already exists.");

				DataCoreUnitOfWorkScope.Current.Add(new Localisation { Culture = Culture });

				DataCoreUnitOfWorkScope.Current.SaveChanges();
				txn.Commit();
			}
		}

		[Test]
		[Explicit]
		public void CreateLocalisationItem()
		{
			using (var txn = ConfigurationUnitOfWorkScope.Current.BeginTransaction())
			{
				var localisation = ConfigurationUnitOfWorkScope.Current.Localisations.FirstOrDefault(x => x.Culture == Culture);
				if (localisation.IsNull()) throw new Exception("Localisation not found");
			
				// General Errors
				AddLocalisationItem(localisation, "Global.Help.ToolTip", "Help");
			
				DataCoreUnitOfWorkScope.Current.SaveChanges();
				txn.Commit();
			}
		}

    [Test]
    [Explicit]
    public void CreateActivities()
    {
      using (var txn = DataCoreUnitOfWorkScope.Current.BeginTransaction())
      {
        DataCoreUnitOfWorkScope.Current.SaveChanges();
        int i = 1;
        if (BuildActivity(i, "Assessment", ActivityType.JobSeeker, "Career Assessment", 5555157)) i++;
        if (BuildActivity(i, "Assessment", ActivityType.JobSeeker, "Eligibility Determination-HCTC", 0)) i++;
        if (BuildActivity(i, "Assessment", ActivityType.JobSeeker, "Eligibility Determination-Trade", 5555152)) i++;
        if (BuildActivity(i, "Assessment", ActivityType.JobSeeker, "Eligibility Determination-Trade A/RTAA", 0)) i++;
        if (BuildActivity(i, "Assessment", ActivityType.JobSeeker, "Eligibility Determination-Regular", 5555153)) i++;
        if (BuildActivity(i, "Assessment", ActivityType.JobSeeker, "Initial Interview/Assessment", 5555142)) i++;
        //if (BuildActivity(i, "Assessment", ActivityType.JobSeeker, "KY custom field", 0)) i++;
        if (BuildActivity(i, "Case Management", ActivityType.JobSeeker, "Assigned-Non-Vets", 5555160)) i++;
        if (BuildActivity(i, "Case Management", ActivityType.JobSeeker, "Client-Centered", 0)) i++;
        if (BuildActivity(i, "Case Management", ActivityType.JobSeeker, "Closed", 5555155)) i++;
        if (BuildActivity(i, "Case Management", ActivityType.JobSeeker, "Follow-up-Trade", 5555188)) i++;
        //if (BuildActivity(i, "Case Management", ActivityType.JobSeeker, "KY custom field", 0)) i++;
        if (BuildActivity(i, "Case Management", ActivityType.JobSeeker, "Other", 5555130)) i++;
        if (BuildActivity(i, "Case Management", ActivityType.JobSeeker, "Received Services-Non-Vets", 5555117)) i++;
        if (BuildActivity(i, "Counseling ", ActivityType.JobSeeker, "Career Guidence", 5555156)) i++;
        if (BuildActivity(i, "Counseling ", ActivityType.JobSeeker, "Employability Skills", 5555151)) i++;
        if (BuildActivity(i, "Counseling ", ActivityType.JobSeeker, "Group Sessions", 5555145)) i++;
        if (BuildActivity(i, "Counseling ", ActivityType.JobSeeker, "IEP - Individual Employment Plan ", 5555185)) i++;
        if (BuildActivity(i, "Counseling ", ActivityType.JobSeeker, "Individual & Career Planning", 5555144)) i++;
        if (BuildActivity(i, "Counseling ", ActivityType.JobSeeker, "Interest Inventory", 5555141)) i++;
        if (BuildActivity(i, "Counseling ", ActivityType.JobSeeker, "Job Coaching", 5555140)) i++;
        if (BuildActivity(i, "Counseling ", ActivityType.JobSeeker, "Job Prep Interviewing Skills", 5555138)) i++;
        if (BuildActivity(i, "Counseling ", ActivityType.JobSeeker, "Job Retention Services", 5555137)) i++;
        if (BuildActivity(i, "Counseling ", ActivityType.JobSeeker, "Job Search Planning", 5555135)) i++;
        if (BuildActivity(i, "Counseling ", ActivityType.JobSeeker, "Career/Labor Market Info", 5555119)) i++;
        if (BuildActivity(i, "Counseling ", ActivityType.JobSeeker, "Vocational Guidance-Other", 5555125)) i++;
        if (BuildActivity(i, "Counseling ", ActivityType.JobSeeker, "Vocational Rehab-Other", 5555163)) i++;
        if (BuildActivity(i, "Customer Utilization", ActivityType.JobSeeker, "Mobile Job Center", 5555166)) i++;
        if (BuildActivity(i, "Customer Utilization", ActivityType.JobSeeker, "Resource Rooms", 5555165)) i++;
        if (BuildActivity(i, "Customer Utilization", ActivityType.JobSeeker, "Translation Services", 5555170)) i++;
        if (BuildActivity(i, "Events ", ActivityType.JobSeeker, "Job Fair Attended", 5555182)) i++;
        if (BuildActivity(i, "Events ", ActivityType.JobSeeker, "Job Fair Information Provided", 5555181)) i++;
        if (BuildActivity(i, "Events ", ActivityType.JobSeeker, "Job Finding Club", 5555180)) i++;
        if (BuildActivity(i, "Labor Exchange", ActivityType.JobSeeker, "External Job Match", 5555190)) i++;
        if (BuildActivity(i, "Labor Exchange", ActivityType.JobSeeker, "External Job Referral", 5559266)) i++;
        if (BuildActivity(i, "Labor Exchange", ActivityType.JobSeeker, "Follow-up-General", 5555189)) i++;
        if (BuildActivity(i, "Labor Exchange", ActivityType.JobSeeker, "Job Development Contact", 5555183)) i++;
        if (BuildActivity(i, "Labor Exchange", ActivityType.JobSeeker, "Other Reportable Services-ES, DVOP, LVER", 5555149)) i++;
        if (BuildActivity(i, "Labor Exchange", ActivityType.JobSeeker, "Placed in HCTC", 0)) i++;
        if (BuildActivity(i, "Labor Exchange", ActivityType.JobSeeker, "Refused Referral", 5555175)) i++;
        if (BuildActivity(i, "Labor Exchange", ActivityType.JobSeeker, "Resume Preparation Assistance", 5555174)) i++;
        if (BuildActivity(i, "Labor Exchange", ActivityType.JobSeeker, "Workforce Info Services-Self-Service LMI", 5555112)) i++;
        if (BuildActivity(i, "Labor Exchange", ActivityType.JobSeeker, "Workforce Info Services-Staff-Assisted LMI", 5555111)) i++;
        if (BuildActivity(i, "Orientation", ActivityType.JobSeeker, "Informational Services", 5555143)) i++;
        if (BuildActivity(i, "Orientation", ActivityType.JobSeeker, "Other", 5555126)) i++;
        if (BuildActivity(i, "Orientation", ActivityType.JobSeeker, "Rapid Response", 5555118)) i++;
        if (BuildActivity(i, "Referrals", ActivityType.JobSeeker, "Adult Daycare", 0)) i++;
        if (BuildActivity(i, "Referrals", ActivityType.JobSeeker, "Adult Education", 0)) i++;
        //if (BuildActivity(i, "Referrals", ActivityType.JobSeeker, "KY custom field", 0)) i++;
        //if (BuildActivity(i, "Referrals", ActivityType.JobSeeker, "KY custom field", 0)) i++;
        if (BuildActivity(i, "Referrals", ActivityType.JobSeeker, "Child Care: General", 0)) i++;
        //if (BuildActivity(i, "Referrals", ActivityType.JobSeeker, "KY custom field", 0)) i++;
        if (BuildActivity(i, "Referrals", ActivityType.JobSeeker, "Community/Technical College", 0)) i++;
        if (BuildActivity(i, "Referrals", ActivityType.JobSeeker, "CSBG: Elderly/Low Income", 0)) i++;
        if (BuildActivity(i, "Referrals", ActivityType.JobSeeker, "ES/UI", 5559267)) i++;
        if (BuildActivity(i, "Referrals", ActivityType.JobSeeker, "Emergency: ESG", 0)) i++;
        if (BuildActivity(i, "Referrals", ActivityType.JobSeeker, "Emergency: FEMA", 5559265)) i++;
        if (BuildActivity(i, "Referrals", ActivityType.JobSeeker, "Energy: LIHEAP", 0)) i++;
        //if (BuildActivity(i, "Referrals", ActivityType.JobSeeker, "KY custom field", 0)) i++;
        //if (BuildActivity(i, "Referrals", ActivityType.JobSeeker, "KY custom field", 0)) i++;
        if (BuildActivity(i, "Referrals", ActivityType.JobSeeker, "Energy: Wintercare", 0)) i++;
        if (BuildActivity(i, "Referrals", ActivityType.JobSeeker, "Faith-Based CBO", 0)) i++;
        if (BuildActivity(i, "Referrals", ActivityType.JobSeeker, "Goodwill", 0)) i++;
        if (BuildActivity(i, "Referrals", ActivityType.JobSeeker, "HCTC - Health Coverage Tax Credit", 0)) i++;
        if (BuildActivity(i, "Referrals", ActivityType.JobSeeker, "Headstart: Early", 0)) i++;
        if (BuildActivity(i, "Referrals", ActivityType.JobSeeker, "Headstart: Regular", 0)) i++;
        if (BuildActivity(i, "Referrals", ActivityType.JobSeeker, "Homeless Program", 0)) i++;
        if (BuildActivity(i, "Referrals", ActivityType.JobSeeker, "Housing: Regular", 0)) i++;
        if (BuildActivity(i, "Referrals", ActivityType.JobSeeker, "Housing: Tenant-Based Rental Assistance", 0)) i++;
        if (BuildActivity(i, "Referrals", ActivityType.JobSeeker, "Job Corps", 5559264)) i++;
        if (BuildActivity(i, "Referrals", ActivityType.JobSeeker, "MSFW: Federal", 5559263)) i++;
        if (BuildActivity(i, "Referrals", ActivityType.JobSeeker, "MSFW: State", 5559262)) i++;
        if (BuildActivity(i, "Referrals", ActivityType.JobSeeker, "Native American Program", 5559261)) i++;
        if (BuildActivity(i, "Referrals", ActivityType.JobSeeker, "Older Worker Program", 5559260)) i++;
        if (BuildActivity(i, "Referrals", ActivityType.JobSeeker, "Post-Secondary Education", 0)) i++;
        if (BuildActivity(i, "Referrals", ActivityType.JobSeeker, "Services for Hearing Impaired", 0)) i++;
        if (BuildActivity(i, "Referrals", ActivityType.JobSeeker, "Services for Victims-Spouse Abuse Shelter", 0)) i++;
        if (BuildActivity(i, "Referrals", ActivityType.JobSeeker, "Services for Victims-VOCA", 0)) i++;
        if (BuildActivity(i, "Referrals", ActivityType.JobSeeker, "Services for Visual Assistance/Exams", 0)) i++;
        if (BuildActivity(i, "Referrals", ActivityType.JobSeeker, "Services for Visually Impaired", 0)) i++;
        if (BuildActivity(i, "Referrals", ActivityType.JobSeeker, "Services for Vocational Rehabilitation", 5555124)) i++;
        if (BuildActivity(i, "Referrals", ActivityType.JobSeeker, "Supportive Service-Non-Partner", 5559259)) i++;
        if (BuildActivity(i, "Referrals", ActivityType.JobSeeker, "Supportive Service-Partner", 5559258)) i++;
        if (BuildActivity(i, "Referrals", ActivityType.JobSeeker, "TANF-Needy Families", 0)) i++;
        //if (BuildActivity(i, "Referrals", ActivityType.JobSeeker, "KY custom field", 0)) i++;
        if (BuildActivity(i, "Referrals", ActivityType.JobSeeker, "Trade", 5559257)) i++;
        if (BuildActivity(i, "Referrals", ActivityType.JobSeeker, "Trade-A/RTAA", 0)) i++;
        if (BuildActivity(i, "Referrals", ActivityType.JobSeeker, "Transportation Assistance", 0)) i++;
        if (BuildActivity(i, "Referrals", ActivityType.JobSeeker, "WIA", 5559253)) i++;
        if (BuildActivity(i, "Referrals", ActivityType.JobSeeker, "Veterans Program", 5559254)) i++;
        if (BuildActivity(i, "Testing", ActivityType.JobSeeker, "ASSET Entrance Exam", 0)) i++;
        if (BuildActivity(i, "Testing", ActivityType.JobSeeker, "BEAG", 5555158)) i++;
        if (BuildActivity(i, "Testing", ActivityType.JobSeeker, "GATB", 5555147)) i++;
        if (BuildActivity(i, "Testing", ActivityType.JobSeeker, "GABT/VG", 5555146)) i++;
        if (BuildActivity(i, "Testing", ActivityType.JobSeeker, "Literacy", 5555134)) i++;
        if (BuildActivity(i, "Testing", ActivityType.JobSeeker, "NATB", 5555133)) i++;
        if (BuildActivity(i, "Testing", ActivityType.JobSeeker, "Other/Merit", 5555127)) i++;
        if (BuildActivity(i, "Testing", ActivityType.JobSeeker, "Pre-GED ", 0)) i++;
        if (BuildActivity(i, "Testing", ActivityType.JobSeeker, "Proficiency", 5555122)) i++;
        if (BuildActivity(i, "Testing", ActivityType.JobSeeker, "SATB", 5555114)) i++;
        if (BuildActivity(i, "Testing", ActivityType.JobSeeker, "TABE Assessment ", 5555107)) i++;
        if (BuildActivity(i, "Testing", ActivityType.JobSeeker, "TABE-WF Assessment ", 0)) i++;
        if (BuildActivity(i, "Testing", ActivityType.JobSeeker, "Work Keys/NCRC Assessment", 5555100)) i++;
        if (BuildActivity(i, "Testing", ActivityType.JobSeeker, "Work Keys/NCRC referral", 5559252)) i++;
        if (BuildActivity(i, "Training", ActivityType.JobSeeker, "Basic Skills Referral", 5559268)) i++;
        if (BuildActivity(i, "Training", ActivityType.JobSeeker, "Computer Skills Referral", 5555154)) i++;
        if (BuildActivity(i, "Training", ActivityType.JobSeeker, "Entrepreneurial Referral", 5555150)) i++;
        if (BuildActivity(i, "Training", ActivityType.JobSeeker, "OJT Contract ", 5555132)) i++;
        if (BuildActivity(i, "Training", ActivityType.JobSeeker, "Training Referral-External Provider", 5559255)) i++;
        if (BuildActivity(i, "Training", ActivityType.JobSeeker, "Training Referral-Internal", 5559256)) i++;
        if (BuildActivity(i, "Training", ActivityType.JobSeeker, "In Training-Other", 5555128)) i++;
        if (BuildActivity(i, "Training", ActivityType.JobSeeker, "In Training-Post-secondary", 5555123)) i++;
        if (BuildActivity(i, "Training", ActivityType.JobSeeker, "In Training-Secondary", 5555113)) i++;
        if (BuildActivity(i, "Training", ActivityType.JobSeeker, "Placed in Training-Job Corps", 5555139)) i++;
        if (BuildActivity(i, "Training", ActivityType.JobSeeker, "Placed in Training-Other Federal", 5555129)) i++;
        if (BuildActivity(i, "Training", ActivityType.JobSeeker, "Placed in Training-WIA", 5555101)) i++;
        if (BuildActivity(i, "Training", ActivityType.JobSeeker, "Training Termination-Successful Other", 5555110)) i++;
        if (BuildActivity(i, "Training", ActivityType.JobSeeker, "Training Termination-Successful Post-secondary", 5555109)) i++;
        if (BuildActivity(i, "Training", ActivityType.JobSeeker, "Training Termination-Successful Secondary", 5555108)) i++;
        if (BuildActivity(i, "UI/Reemployment", ActivityType.JobSeeker, "Benefit Rights Interview", 5555169)) i++;
        if (BuildActivity(i, "UI/Reemployment", ActivityType.JobSeeker, "Case Management-Intensive ", 5555105)) i++;
        //if (BuildActivity(i, "UI/Reemployment", ActivityType.JobSeeker, "KY custom field", 0)) i++;
        if (BuildActivity(i, "UI/Reemployment", ActivityType.JobSeeker, "Case Management-Profiled", 5555121)) i++;
        if (BuildActivity(i, "UI/Reemployment", ActivityType.JobSeeker, "ERP - Eligiblilty Review Program", 5555168)) i++;
        if (BuildActivity(i, "UI/Reemployment", ActivityType.JobSeeker, "Exempted Mandatory Profiling", 0)) i++;
        if (BuildActivity(i, "UI/Reemployment", ActivityType.JobSeeker, "Failed to Report-Individual Re-employment Plan", 5555187)) i++;
        if (BuildActivity(i, "UI/Reemployment", ActivityType.JobSeeker, "Failed to Report-Profiled", 5555186)) i++;
        if (BuildActivity(i, "UI/Reemployment", ActivityType.JobSeeker, "Failed to Report-REA", 0)) i++;
        if (BuildActivity(i, "UI/Reemployment", ActivityType.JobSeeker, "Information Provided - UI", 5555167)) i++;
        if (BuildActivity(i, "UI/Reemployment", ActivityType.JobSeeker, "Orientation-Profiled ", 5555120)) i++;
        if (BuildActivity(i, "UI/Reemployment", ActivityType.JobSeeker, "Orientation-REA", 0)) i++;
        if (BuildActivity(i, "UI/Reemployment", ActivityType.JobSeeker, "Orientation-Re-employment", 5555104)) i++;
        if (BuildActivity(i, "UI/Reemployment", ActivityType.JobSeeker, "REA-EUC Job Search Eligible", 0)) i++;
        if (BuildActivity(i, "UI/Reemployment", ActivityType.JobSeeker, "REA-EUC Regular", 0)) i++;
        if (BuildActivity(i, "UI/Reemployment", ActivityType.JobSeeker, "REA-Labor Market & Career Information", 0)) i++;
        if (BuildActivity(i, "UI/Reemployment", ActivityType.JobSeeker, "REA-Requirements Fulfilled", 0)) i++;
        if (BuildActivity(i, "Veterans", ActivityType.JobSeeker, "Assigned Case Management", 5555159)) i++;
        if (BuildActivity(i, "Veterans", ActivityType.JobSeeker, "Gold Card Outreach", 0)) i++;
        if (BuildActivity(i, "Veterans", ActivityType.JobSeeker, "Gold Card Services Eligible - Accepted", 0)) i++;
        if (BuildActivity(i, "Veterans", ActivityType.JobSeeker, "Gold Card Services Eligible - Declined", 0)) i++;
        if (BuildActivity(i, "Veterans", ActivityType.JobSeeker, "Incarcerated Veteran Outreach", 0)) i++;
        if (BuildActivity(i, "Veterans", ActivityType.JobSeeker, "Other Reportable Services Follow-up", 5555148)) i++;
        if (BuildActivity(i, "Veterans", ActivityType.JobSeeker, "Received Case Management", 5555116)) i++;
        if (BuildActivity(i, "Veterans", ActivityType.JobSeeker, "TAP Workshop", 5555106)) i++;
        if (BuildActivity(i, "Veterans", ActivityType.JobSeeker, "Vocational Guidance", 5555102)) i++;
        if (BuildActivity(i, "Veterans", ActivityType.JobSeeker, "Vocational Rehab-VA", 5555103)) i++;
        if (BuildActivity(i, "Workshops", ActivityType.JobSeeker, "Job Search Workshop", 5555136)) i++;
        if (BuildActivity(i, "Workshops", ActivityType.JobSeeker, "Resume Writing Workshop", 5555115)) i++;
        if (BuildActivity(i, "Workshops", ActivityType.JobSeeker, "Workshop Attended-Other", 5555131)) i++;
        if (BuildActivity(i, "Workshops", ActivityType.JobSeeker, "Workshop Referral", 5559251)) i++;

        if (BuildActivity(i, "Assistance", ActivityType.Employer, "Human Resource Issues", 5553528)) i++;
        if (BuildActivity(i, "Assistance", ActivityType.Employer, "Business Info & Support Services", 5553543)) i++;
        if (BuildActivity(i, "Assistance", ActivityType.Employer, "Business Partnership Agreement ", 5553542)) i++;
        if (BuildActivity(i, "Assistance", ActivityType.Employer, "Economic/Labor Market Issues", 5553539)) i++;
        if (BuildActivity(i, "Assistance", ActivityType.Employer, "Labor Market Preview", 5553522)) i++;
        if (BuildActivity(i, "Assistance", ActivityType.Employer, "Layoff-Response Planning", 5553521)) i++;
        if (BuildActivity(i, "Assistance", ActivityType.Employer, "Link to Company Home Page", 5553520)) i++;
        if (BuildActivity(i, "Assistance", ActivityType.Employer, "Marketing Service Program", 5553518)) i++;
        if (BuildActivity(i, "Assistance", ActivityType.Employer, "Rapid Response/Business Downsizing", 5553511)) i++;
        if (BuildActivity(i, "Assistance", ActivityType.Employer, "Spider Company Home Page ", 5553507)) i++;
        if (BuildActivity(i, "Assistance", ActivityType.Employer, "Statutory (legal) Issues", 5553505)) i++;
        if (BuildActivity(i, "Assistance", ActivityType.Employer, "Strategic Planning/Economic Development", 5553504)) i++;
        if (BuildActivity(i, "Assistance", ActivityType.Employer, "UI Information Issues", 5553500)) i++;
        if (BuildActivity(i, "Assistance", ActivityType.Employer, "Untapped Labor-Pool Activities", 5553499)) i++;
        if (BuildActivity(i, "Alien Labor", ActivityType.Employer, "Alien Labor Certification", 5553537)) i++;
        if (BuildActivity(i, "Alien Labor", ActivityType.Employer, "H-2A Field Checks ", 0)) i++;
        if (BuildActivity(i, "Contact", ActivityType.Employer, "E-Mail", 5553536)) i++;
        if (BuildActivity(i, "Contact", ActivityType.Employer, "Follow-Up", 5553529)) i++;
        if (BuildActivity(i, "Contact", ActivityType.Employer, "Mailing", 5553519)) i++;
        if (BuildActivity(i, "Contact", ActivityType.Employer, "On-Site Visit", 5553514)) i++;
        if (BuildActivity(i, "Contact", ActivityType.Employer, "Phone Call", 5553512)) i++;
        if (BuildActivity(i, "Contact", ActivityType.Employer, "Tax Audit", 5553502)) i++;
        if (BuildActivity(i, "Events", ActivityType.Employer, "Employer Committee Meeting", 5553535)) i++;
        if (BuildActivity(i, "Events", ActivityType.Employer, "Employer Committee Member ", 5553534)) i++;
        if (BuildActivity(i, "Events", ActivityType.Employer, "Job Fair", 5553525)) i++;
        if (BuildActivity(i, "Events", ActivityType.Employer, "On-Site Review/Customized LMI", 5553515)) i++;
        if (BuildActivity(i, "Events", ActivityType.Employer, "On-Site Recruitment", 5553516)) i++;
        if (BuildActivity(i, "Events", ActivityType.Employer, "Rapid Response", 1710438)) i++;
        if (BuildActivity(i, "Labor Exchange", ActivityType.Employer, "Account Management", 5553545)) i++;
        if (BuildActivity(i, "Labor Exchange", ActivityType.Employer, "Job Analysis", 5553526)) i++;
        //if (BuildActivity(i, "Labor Exchange", ActivityType.Employer, "KY Custom Field", 0)) i++;
        //if (BuildActivity(i, "Labor Exchange", ActivityType.Employer, "KY Custom Field", 0)) i++;
        //if (BuildActivity(i, "Labor Exchange", ActivityType.Employer, "KY Custom Field", 0)) i++;
        if (BuildActivity(i, "Labor Exchange", ActivityType.Employer, "Job-Order Taking", 5553524)) i++;
        if (BuildActivity(i, "Labor Exchange", ActivityType.Employer, "Job-Order Writing Assistance", 5553523)) i++;
        if (BuildActivity(i, "Labor Exchange", ActivityType.Employer, "Job-Seeker Screening", 5553544)) i++;
        if (BuildActivity(i, "Labor Exchange", ActivityType.Employer, "Review Employment Applications", 5553510)) i++;
        if (BuildActivity(i, "Labor Exchange", ActivityType.Employer, "Self-Directed Search", 5553509)) i++;
        if (BuildActivity(i, "Labor Exchange", ActivityType.Employer, "Veterans Outreach", 5553497)) i++;
        if (BuildActivity(i, "Labor Exchange", ActivityType.Employer, "Workforce Recruitment", 5553495)) i++;
        if (BuildActivity(i, "Tax Credit", ActivityType.Employer, "Certification", 5553540)) i++;
        if (BuildActivity(i, "Tax Credit", ActivityType.Employer, "Enterprise Zone ", 5553531)) i++;
        if (BuildActivity(i, "Tax Credit", ActivityType.Employer, "UTC", 5553498)) i++;
        if (BuildActivity(i, "Tax Credit", ActivityType.Employer, "Welfare-to-Work", 5553496)) i++;
        if (BuildActivity(i, "Tax Credit", ActivityType.Employer, "Work Opportunities", 5553493)) i++;
        if (BuildActivity(i, "Testing", ActivityType.Employer, "CAB (Comprehensive Ability Battery)", 5553541)) i++;
        if (BuildActivity(i, "Testing", ActivityType.Employer, "Educational/Basic Skill Level", 5553538)) i++;
        if (BuildActivity(i, "Testing", ActivityType.Employer, "Employer Provided (Valid)", 5553532)) i++;
        if (BuildActivity(i, "Testing", ActivityType.Employer, "State-Specific Testing", 5553506)) i++;
        if (BuildActivity(i, "Testing", ActivityType.Employer, "TABE-WF", 5553503)) i++;
        if (BuildActivity(i, "Testing", ActivityType.Employer, "Testing/General", 0)) i++;
        if (BuildActivity(i, "Testing", ActivityType.Employer, "Typing/Clerical ", 5553501)) i++;
        if (BuildActivity(i, "Testing", ActivityType.Employer, "Work Keys/NCRC", 5553494)) i++;
        if (BuildActivity(i, "Training", ActivityType.Employer, "Incumbent Worker Services", 5553527)) i++;
        //if (BuildActivity(i, "Training", ActivityType.Employer, "KY Custom Entry", 0)) i++;
        if (BuildActivity(i, "Training", ActivityType.Employer, "OJT Contract", 5553517)) i++;
        if (BuildActivity(i, "Training", ActivityType.Employer, "Other Seminar", 5553513)) i++;
        if (BuildActivity(i, "Training", ActivityType.Employer, "Self-Service Training", 1710441)) i++;
        if (BuildActivity(i, "Training", ActivityType.Employer, "Services", 5553508)) i++;


        DataCoreUnitOfWorkScope.Current.SaveChanges();
				var activities = ConfigurationUnitOfWorkScope.Current.Activities.Where(x => x.ExternalId == 0);
        foreach (var activity in activities)
        {
          activity.ExternalId = activity.Id;
          //FocusUnitOfWorkScope.Current.Update(activity);

        }
        DataCoreUnitOfWorkScope.Current.SaveChanges();
        txn.Commit();
      }
    }

    public bool BuildActivity(int count, string categoryName, ActivityType categoryType, string activityName, int externalId)
    {
      var categoryId =
				ConfigurationUnitOfWorkScope.Current.ActivityCategories.First(x => x.Name == categoryName && x.ActivityType == categoryType).Id;
			if (ConfigurationUnitOfWorkScope.Current.Activities.Any(x => x.ActivityCategoryId == categoryId && x.Name == activityName))
        return false;

      string localisationKey = "Activity.";
      switch (count.ToString().Length)
      {
        case 1:
          localisationKey += "00" + count.ToString();
          break;
        case 2:
          localisationKey += "0" + count.ToString();
          break;
        default:
          localisationKey += count.ToString();
          break;
      }

      DataCoreUnitOfWorkScope.Current.Add(new Activity{ActivityCategoryId = categoryId, LocalisationKey = localisationKey, ExternalId = externalId, Name = activityName, EnableDisable = true, Administrable = true, SortOrder = 0, Used = false, });

      return true;
    }

    [Test]
    [Explicit]
    public void CreateActivityCategories()
    {
      using (var txn = DataCoreUnitOfWorkScope.Current.BeginTransaction())
      {
        int i = 1;
        DataCoreUnitOfWorkScope.Current.Add(BuildCategory(i, "Assessment", ActivityType.JobSeeker));
        i++;
        DataCoreUnitOfWorkScope.Current.Add(BuildCategory(i, "Case Management", ActivityType.JobSeeker));
        i++;
        DataCoreUnitOfWorkScope.Current.Add(BuildCategory(i, "Counseling", ActivityType.JobSeeker));
        i++;
        DataCoreUnitOfWorkScope.Current.Add(BuildCategory(i, "Customer Utilization", ActivityType.JobSeeker));
        i++;
        DataCoreUnitOfWorkScope.Current.Add(BuildCategory(i, "Events", ActivityType.JobSeeker));
        i++;
        DataCoreUnitOfWorkScope.Current.Add(BuildCategory(i, "Labor Exchange", ActivityType.JobSeeker));
        i++;
        DataCoreUnitOfWorkScope.Current.Add(BuildCategory(i, "Orientation", ActivityType.JobSeeker));
        i++;
        DataCoreUnitOfWorkScope.Current.Add(BuildCategory(i, "Referrals", ActivityType.JobSeeker));
        i++;
        DataCoreUnitOfWorkScope.Current.Add(BuildCategory(i, "Testing", ActivityType.JobSeeker));
        i++;
        DataCoreUnitOfWorkScope.Current.Add(BuildCategory(i, "Training", ActivityType.JobSeeker));
        i++;
        DataCoreUnitOfWorkScope.Current.Add(BuildCategory(i, "UI/Reemployment", ActivityType.JobSeeker));
        i++;
        DataCoreUnitOfWorkScope.Current.Add(BuildCategory(i, "Veterans", ActivityType.JobSeeker));
        i++;
        DataCoreUnitOfWorkScope.Current.Add(BuildCategory(i, "Workshops", ActivityType.JobSeeker));
        i++;

        DataCoreUnitOfWorkScope.Current.Add(BuildCategory(i, "Assistance", ActivityType.Employer));
        i++;
        DataCoreUnitOfWorkScope.Current.Add(BuildCategory(i, "Alien Labor", ActivityType.Employer));
        i++;
        DataCoreUnitOfWorkScope.Current.Add(BuildCategory(i, "Contact", ActivityType.Employer));
        i++;
        DataCoreUnitOfWorkScope.Current.Add(BuildCategory(i, "Events", ActivityType.Employer));
        i++;
        DataCoreUnitOfWorkScope.Current.Add(BuildCategory(i, "Labor Exchange", ActivityType.Employer));
        i++;
        DataCoreUnitOfWorkScope.Current.Add(BuildCategory(i, "Tax Credit", ActivityType.Employer));
        i++;
        DataCoreUnitOfWorkScope.Current.Add(BuildCategory(i, "Testing", ActivityType.Employer));
        i++;
        DataCoreUnitOfWorkScope.Current.Add(BuildCategory(i, "Training", ActivityType.Employer));

        DataCoreUnitOfWorkScope.Current.SaveChanges();
        
        txn.Commit();
      }
    }

    private ActivityCategory BuildCategory(int count, string name, ActivityType type)
    {
      string localisationKey = "ActivityCategory.";
      switch (count.ToString().Length)
      {
        case 1:
          localisationKey += "00" + count.ToString();
          break;
        case 2:
          localisationKey += "0" + count.ToString();
          break;
        default:
          localisationKey += count.ToString();
          break;
      }
      return new ActivityCategory{LocalisationKey = localisationKey, Name = name, Visible = true, Administrable = true, ActivityType = type};
    }

		[Test]
		[Explicit]
		public void CreateCodeGroupAndCodeItemsAndCodeGroupItems()
		{
			CreateCodeGroup();
			CreateCodeItem();
			CreateCodeGroupItem();
		}

		[Test]
		[Explicit]
		public void CreateCodeGroup()
		{
			using (var txn = DataCoreUnitOfWorkScope.Current.BeginTransaction())
			{
				// Always use the Enum - that way you wont break the build :)
				AddCodeGroup(LookupTypes.SecurityQuestions.ToString());
				
				DataCoreUnitOfWorkScope.Current.SaveChanges();
				txn.Commit();
			}
		}

		[Test]
		[Explicit]
		public void CreateCodeItem()
		{
			using (var txn = DataCoreUnitOfWorkScope.Current.BeginTransaction())
			{
				var keyPrefix = LookupTypes.SecurityQuestions + ".";

				var localisation = ConfigurationUnitOfWorkScope.Current.Localisations.FirstOrDefault(x => x.Culture == Culture);
				if (localisation.IsNull()) throw new Exception("Localisation Not Found");

				var listOfItems = GetCodeItemList();

				if (listOfItems.Count == 0)
				{
					AddCodeItemAndLocalisation(localisation, keyPrefix + "OT01", "Dummy01");
					AddCodeItemAndLocalisation(localisation, keyPrefix + "OT02", "Dummy02");
					AddCodeItemAndLocalisation(localisation, keyPrefix + "OT03", "Dummy03");
				}
				else
				{
					foreach (var item in listOfItems)
						AddCodeItemAndLocalisation(localisation, keyPrefix + ConvertToKey(item), item);
				}

				DataCoreUnitOfWorkScope.Current.SaveChanges();
				txn.Commit();
			}
		}

		/// <summary>
		/// Gets the code item list. Kept here cos it made sense to me :)
		/// </summary>
		/// <returns></returns>
		private List<string> GetCodeItemList()
		{
			return new List<string> { "What is your favorite color?", "What is your mother's maiden name?", "What is your pet's name?", "What state were you born in?", "What was the first car you owned?", "What city or town were you born in?" };
		}

		[Test]
		[Explicit]
		public void CreateCodeGroupItem()
		{
			using (var txn = DataCoreUnitOfWorkScope.Current.BeginTransaction())
			{
				var codeGroup = LookupTypes.SecurityQuestions.ToString();
				var listOfItems = GetCodeItemList();

				if (listOfItems.Count == 0)
				{
					AddCodeGroupItem(codeGroup, codeGroup + ".OT01", 1);
					AddCodeGroupItem(codeGroup, codeGroup + ".OT01", 2);
					AddCodeGroupItem(codeGroup, codeGroup + ".OT01", 3);
				}
				else
				{
					var i = 1;
					foreach (var item in listOfItems)
					{
						AddCodeGroupItem(codeGroup, codeGroup + "." + ConvertToKey(item), i);
						i++;
					}
				}

				DataCoreUnitOfWorkScope.Current.SaveChanges();
				txn.Commit();
			}
		}

		[Test]
		[Explicit]
		public void CreateEmailTemplete()
		{
			using (var txn = DataCoreUnitOfWorkScope.Current.BeginTransaction())
			{
        AddEmailTemplate(EmailTemplateTypes.CareerPasswordReset, "New Password for #FOCUSCAREER#", @"Dear #RECIPIENTNAME#", @"This email is being sent to you because you forgot your password. The following new password has been generated for you:

Email Address: #EMAILADDRESS#
Password: #PASSWORD#

With this information, you should have no problem accessing your account. We do encourage you to come back often to #FOCUSCAREER# to manage your resume and job search.
We wish you luck and look forward to helping you find your next job!

#FOCUSCAREERURL#
");
			
				DataCoreUnitOfWorkScope.Current.SaveChanges();
				txn.Commit();
			}
		}

		[Test]
		[Explicit]
		public void CreateMessage()
		{
			using(var txn = DataCoreUnitOfWorkScope.Current.BeginTransaction())
			{
				var messageId = AddMessage(true, null, DateTime.Now.AddDays(3));

				var localisation = ConfigurationUnitOfWorkScope.Current.Localisations.FirstOrDefault(x => x.Culture == Culture);
				if (localisation.IsNull()) throw new Exception("Localisation Not Found");

				AddMessageText(messageId, localisation.Id, "Hello from default culture");
				
				DataCoreUnitOfWorkScope.Current.SaveChanges();
				txn.Commit();
			}
		}

    [Test]
		[Explicit]
		public void CreateOnet()
		{
			using (var txn = DataCoreUnitOfWorkScope.Current.BeginTransaction())
			{
				const string codeGroupKey = "JobFamily";
				const string keyPrfix = "Onet.";

				var localisation = ConfigurationUnitOfWorkScope.Current.Localisations.FirstOrDefault(x => x.Culture == Culture);  
				if (localisation.IsNull()) throw new Exception("Localisation Not Found");

				var onetData = new List<OnetDataCreate>();

				// check for onet
				var codeGroup = ConfigurationUnitOfWorkScope.Current.CodeGroups.FirstOrDefault(x => x.Key == codeGroupKey);
				if (codeGroup.IsNull()) throw new Exception("CodeGroup not found for Key: " + codeGroupKey);

				var codeItems = (from ci in ConfigurationUnitOfWorkScope.Current.CodeItems
												 join cgi in ConfigurationUnitOfWorkScope.Current.CodeGroupItems on ci.Id equals cgi.CodeItemId
												 join cg in ConfigurationUnitOfWorkScope.Current.CodeGroups on cgi.CodeGroupId equals cg.Id
								where cg.Id == codeGroup.Id
								select ci).ToList();
				if (codeItems.Count == 0)
					throw new Exception("CodeItems not found for CodeGroup Key: " + codeGroupKey);


				foreach (var onet in onetData)
				{
					var occupationKey = keyPrfix + "Occupation." + onet.OnetCode + "." + ConvertToKey(onet.Occupation);
					var occuptionDescriptionKey = keyPrfix + "Description." + onet.OnetCode + "." + ConvertToKey(onet.Occupation);
					// Occupation Localisation OnetCode.Occupation.OccupationKeyForm, Occupation
					AddLocalisationItem(localisation, occupationKey, onet.Occupation);
					
					// Description Localisation OnetCode.Description.OccupationKeyForm, Description
					AddLocalisationItem(localisation, occuptionDescriptionKey, onet.Description);

					var jobFamilyId = codeItems.SingleOrDefault(x => x.Key == "JobFamily." + ConvertToKey(onet.JobFamily)).Id;

					// onet
					var newOnet = new Onet { OnetCode = onet.OnetCode, Key = occupationKey, JobZone = onet.JobZone, JobFamilyId = jobFamilyId };

					DataCoreUnitOfWorkScope.Current.Add(newOnet);
				}

				DataCoreUnitOfWorkScope.Current.SaveChanges();
				txn.Commit();
			}
		}

   
		[Test]
		[Explicit]
		public void ConvertToKey_WhenPassedAString_UnwantedCharactersRemovedAndInitialLettersCapitals()
		{
			const string text = "Education Administrators, Preschool and Child Care Center/Program";
			var key = ConvertToKey(text);
			key.ShouldEqual("EducationAdministratorsPreschoolAndChildCareCenterProgram");
		}

		[Test]
		[Explicit]
		public void GenerateDemoshieldUrlParameters()
		{
			var urlParams = new string[4];
			urlParams[0] = "FED";
			urlParams[1] = DateTime.Today.GetJulianDays().ToString();
			urlParams[2] = "BGT";
			urlParams[3] = DateTime.Today.ToString("yyyyMMdd");

			var urlParamSequenceNumbers = new List<int>();
			var random = new Random();

			while (urlParamSequenceNumbers.Count < 4)
			{
				var number = random.Next(5);
				if (!urlParamSequenceNumbers.Contains(number) && number > 0)
				{
					urlParamSequenceNumbers.Add(number);
				}
			}

			var urlParamSequence = String.Empty;
			var urlParamData = String.Empty;
			foreach (var urlParamSequenceNumber in urlParamSequenceNumbers)
			{
				urlParamSequence += urlParamSequenceNumber.ToString();
				urlParamData += urlParams[urlParamSequenceNumber - 1];
			}
			urlParamData = urlParamSequence + urlParamData;

			urlParamData = Convert.ToBase64String(Encoding.ASCII.GetBytes(urlParamData));

			var queryStringParameters = new string[3];

			var urlDataSubStringLength = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(urlParamData.Length) / 3));
			if (urlDataSubStringLength > 0)
			{
				queryStringParameters[0] = urlParamData.Substring(0, urlDataSubStringLength);

				queryStringParameters[1] = urlParamData.Substring(queryStringParameters[0].Length, urlDataSubStringLength);
				if ((queryStringParameters[0].Length + queryStringParameters[1].Length + urlDataSubStringLength) > urlParamData.Length)
				{
					urlDataSubStringLength = urlParamData.Length - (queryStringParameters[0].Length + queryStringParameters[1].Length);
				}
				queryStringParameters[2] = urlParamData.Substring((queryStringParameters[0].Length + queryStringParameters[1].Length), urlDataSubStringLength);
			}

			var demoshieldUrlParameters = String.Format("?bginstcode={0}&bgauthkey={1}&bgappid={2}&cl={3}",
			                                            queryStringParameters[0], queryStringParameters[1],
			                                            queryStringParameters[2], urlParams[0].Length);

			Console.WriteLine("Demoshield Url Parameters: {0}", demoshieldUrlParameters);
		}
		
		#region Helpers

		private class OnetDataCreate
		{
			public string OnetCode { get; set; }
			public string Occupation { get; set; }
			public string Description { get; set; }
			public string JobFamily { get; set; }
			public int JobZone { get; set; }
		}

    private class OnetSkillCreate
    {
      public string Key { get; set; }
      public string PrimaryValue { get; set; }
      public string SecondaryValue { get; set; }
      public int LocalisationId { get; set; }
    }

		private static string ConvertToKey(string input)
		{
			const string uCasePattern = @"\b(\w|['-])+\b";
			const string cleanPattern = @"[ /,-.]"; // This may need extending

			// Capitalise text
			var key = Regex.Replace(input, uCasePattern, m => m.Value[0].ToString().ToUpper() + m.Value.Substring(1));

			// remove non chars m
			key = Regex.Replace(key, cleanPattern, m => "");
			return key;
		}

		private void AddLocalisationItem(Localisation localisation, string key, string value)
		{
			var configurationItem = ConfigurationUnitOfWorkScope.Current.LocalisationItems.FirstOrDefault(x => x.Key == key && x.LocalisationId == localisation.Id);
			if (configurationItem.IsNotNull()) throw new Exception("LocalisationItem already exists Key: " + key);
			DataCoreUnitOfWorkScope.Current.Add(new LocalisationItem { LocalisationId = localisation.Id, Key = key, Value = value, ContextKey = "", Localised = false});
		}

    [Test]
    [Explicit]
    public void CreateOnetSkillLocalisationItem()
    {
      using (var txn = DataCoreUnitOfWorkScope.Current.BeginTransaction())
      {
        var ident = 1;
        Onet oldOnetCode = null;

				var localisation = ConfigurationUnitOfWorkScope.Current.Localisations.FirstOrDefault(x => x.Culture == Culture);
        if (localisation.IsNull()) throw new Exception("Localisation Not Found");

         var onetSkills = (from ons in LibraryUnitOfWorkScope.Current.OnetSkills.Where(x=> !x.Skill.Contains("OnetTask"))
                         orderby ons.OnetId select ons ).ToList();

        foreach (var skill in onetSkills)
        {
          //get onet code
					var newOnetCode = LibraryUnitOfWorkScope.Current.Onets.FirstOrDefault(x => x.Id == skill.OnetId);

          if (oldOnetCode != newOnetCode)
            ident = 1;

          if (newOnetCode != null)
          {
            var key = String.Format("{0}.{1}.{2}", "OnetSkill", newOnetCode.OnetCode, ident);

            AddOnetLocalisationItem(localisation,key,skill.Skill);
            skill.Skill = key;
          }
          
          oldOnetCode = newOnetCode;
          ident++;

        }

        //FocusUnitOfWorkScope.Current.SaveChanges();
        txn.Commit();

      }
    }

    private void AddOnetLocalisationItem(Localisation localisation, string key, string value)
    {
			var configurationItem = LibraryUnitOfWorkScope.Current.OnetLocalisationItems.FirstOrDefault(x => x.Key == key && x.LocalisationId == localisation.Id);
      if (configurationItem.IsNotNull()) throw new Exception("OnetLocalisationItem already exists Key: " + key);
      //FocusUnitOfWorkScope.Current.Add(new OnetLocalisationItem { LocalisationId = localisation.Id, Key = key, PrimaryValue = value  });
    }

		private void AddConfigurationItem(string configurationItemKey, string configurationValue)
		{
			DataCoreUnitOfWorkScope.Current.Add(new ConfigurationItem { Key = configurationItemKey, Value = configurationValue });
		}

		private void AddCodeItemAndLocalisation(Localisation localisation, string key, string localisationText)
		{
			var codeItem = ConfigurationUnitOfWorkScope.Current.CodeItems.FirstOrDefault(x => x.Key == key);
			if (codeItem.IsNotNull()) throw new Exception("CodeItem already exists Key: " + key);
			DataCoreUnitOfWorkScope.Current.Add(new CodeItem { Key = key });
			AddLocalisationItem(localisation, key, localisationText);
		}

		private void AddCodeGroup(string codeGroupKey)
		{
			var codeGroup = ConfigurationUnitOfWorkScope.Current.CodeGroups.FirstOrDefault(x => x.Key == codeGroupKey);
			if (codeGroup.IsNotNull()) throw new Exception("CodeGroup already exists Key: " + codeGroupKey);

			DataCoreUnitOfWorkScope.Current.Add(new CodeGroup { Key = codeGroupKey });
		}

		private void AddCodeGroupItem(string codeGroupKey, string codeItemKey, int displayOrder)
		{
			var codeGroup = ConfigurationUnitOfWorkScope.Current.CodeGroups.FirstOrDefault(x => x.Key == codeGroupKey);
			if (codeGroup.IsNull()) throw new Exception("CodeGroup Not Found");

			var codeItem = ConfigurationUnitOfWorkScope.Current.CodeItems.FirstOrDefault(x => x.Key == codeItemKey);
			if (codeItem.IsNull()) throw new Exception("CodeItem Not Found");

			var codeGroupItem = ConfigurationUnitOfWorkScope.Current.CodeGroupItems.FirstOrDefault(x => x.CodeGroupId == codeGroup.Id && x.CodeItemId == codeItem.Id);
			if (codeGroupItem.IsNotNull()) throw new Exception("CodeGroupItem already exists for CG: " + codeGroupKey + " CI: " + codeItemKey);

			DataCoreUnitOfWorkScope.Current.Add(new CodeGroupItem { CodeGroupId = codeGroup.Id, CodeItemId = codeItem.Id, DisplayOrder = displayOrder });
		}

		private void AddEmailTemplate(EmailTemplateTypes templateType, string subject, string salutation, string body)
		{
			var emailTemplate = ConfigurationUnitOfWorkScope.Current.EmailTemplates.FirstOrDefault(x => x.EmailTemplateType == templateType);
			if (emailTemplate.IsNotNull()) throw new Exception("EmailTemplate already exists Template Type: " + templateType);
			var emailTemplateInstance = new EmailTemplate { EmailTemplateType = templateType, Salutation = salutation, Subject = subject, Body = body };
			DataCoreUnitOfWorkScope.Current.Add(emailTemplateInstance);
		}

		private long AddMessage(bool isSystemAlert, long? employerId, DateTime expiryDate)
		{
			var message = new Message {IsSystemAlert = isSystemAlert, EmployerId = employerId, ExpiresOn = expiryDate};
			DataCoreUnitOfWorkScope.Current.Add(message);
			DataCoreUnitOfWorkScope.Current.SaveChanges();
			return message.Id;
		}

		private void AddMessageText(long messageId, long localisationId, string text)
		{
			// Check text does not already exist for Message/Localisation
			var messageTextInstance = DataCoreUnitOfWorkScope.Current.MessageTexts.FirstOrDefault(x => x.MessageId == messageId && x.LocalisationId == localisationId);
			if(messageTextInstance.IsNotNull()) throw new Exception("A message text for this localisation has already been defined for mesasage " + messageId);
			var messageText = new MessageText {MessageId = messageId, LocalisationId = localisationId, Text = text};
			DataCoreUnitOfWorkScope.Current.Add(messageText);
		}

		#endregion

		#region Create Assist User and Roles (for testing purposes)

		[Test]
		[Explicit]
		public void CreateAssistUser()
		{
			const string firstName = "Assist";
			const string lastName = "Client";

			using (var txn = DataCoreUnitOfWorkScope.Current.BeginTransaction())
			{
				DataCoreUnitOfWorkScope.Current.Add(new Person { FirstName = "Assist", LastName = "Client", EmailAddress = "assist@Employee.com", DateOfBirth = new DateTime(1970, 01, 01), JobTitle = "Tester" });

				DataCoreUnitOfWorkScope.Current.SaveChanges();

				var person = DataCoreUnitOfWorkScope.Current.Persons.FirstOrDefault(x => x.FirstName == firstName && x.LastName == lastName);
				if (person.IsNull()) throw new Exception("Person not found");


				DataCoreUnitOfWorkScope.Current.Add(new User { Person = person, PasswordHash = "Ass1stCl13nt", PasswordSalt = "abcd6666", UserName = "assist@Employee.com" });

				DataCoreUnitOfWorkScope.Current.SaveChanges();
				txn.Commit();
			}
		}

		[Test]
		[Explicit]
		public void CreateAssistUserRole()
		{
			const string userName = "assist@Employee.com";
			const string userRole = Constants.RoleKeys.AssistEmployersAccountBlocker;

			using (var txn = DataCoreUnitOfWorkScope.Current.BeginTransaction())
			{
				var user = DataCoreUnitOfWorkScope.Current.Users.FirstOrDefault(x => x.UserName == userName);
				if (user.IsNull()) throw new Exception("User not found");

				var role = DataCoreUnitOfWorkScope.Current.Roles.FirstOrDefault(x => x.Key == userRole);
				if (role.IsNull()) throw new Exception("Role not found");

				DataCoreUnitOfWorkScope.Current.Add(new UserRole { User = user, Role = role });

				DataCoreUnitOfWorkScope.Current.SaveChanges();
				txn.Commit();
			}
		}

		#endregion

		#region miscellaneous tests

		[Test]
		public void TestWordReplace()
		{
			// This regex handles any symbols in the profanity word as they need to be matched literally.
			var symbolRegex = new Regex( @"(\W)" );
			const string colour = "red";

			const string origText = "The quick brown fox jumps over the lazy dog";
			const string expectedText = "The <b>quick</b> brown fox <b>jumps</b> over the <b>lazy</b> dog";
			var replaceWords = new[] { "quick", "row", "*ve", "la*", "*um*" };
			var result = CommonUtilities.WordReplace( origText, replaceWords, "<b>", "</b>" );
			result.ShouldEqual( expectedText );
		}

		#endregion

	}
}
