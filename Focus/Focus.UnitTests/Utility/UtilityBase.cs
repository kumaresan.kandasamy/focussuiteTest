﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using Focus.Core.Settings.Interfaces;
using Focus.Data.Configuration.Entities;
using Focus.Data.Core.Entities;
using Focus.Data.Library.Entities;
using Focus.Data.Repositories;
using Focus.Data.Repositories.Contracts;
using Focus.Services;
using Focus.UnitTests.Core;
using Framework.Testing.NUnit;

using Mindscape.LightSpeed;
using NUnit.Framework;

#endregion


namespace Focus.UnitTests.Utility
{
	[TestFixture]
	public class UtilityBase
	{
		private static readonly LightSpeedContext<DataCoreUnitOfWork> DataCoreContext = new LightSpeedContext<DataCoreUnitOfWork>("DataCore");
		private static readonly LightSpeedContext<ConfigurationUnitOfWork> ConfigurationContext = new LightSpeedContext<ConfigurationUnitOfWork>("Configuration");
		private static readonly LightSpeedContext<LibraryUnitOfWork> LibraryContext = new LightSpeedContext<LibraryUnitOfWork>("Library");

		protected SimpleUnitOfWorkScope<ConfigurationUnitOfWork> ConfigurationUnitOfWorkScope;
		protected SimpleUnitOfWorkScope<DataCoreUnitOfWork> DataCoreUnitOfWorkScope;
		protected SimpleUnitOfWorkScope<LibraryUnitOfWork> LibraryUnitOfWorkScope;

		protected IConfigurationRepository ConfigurationRepository;
		protected ICoreRepository DataCoreRepository;
		protected ILibraryRepository LibraryRepository;

		protected IRuntimeContext RuntimeContext;

		protected static IAppSettings MockAppSettings = new FakeAppSettings();

		//private const string Culture = "EN-GB";

		// Invariant culture and config settings
		protected const string Culture = "**-**";

		[SetUp]
		public void SetUp()
		{
			DataCoreUnitOfWorkScope = new SimpleUnitOfWorkScope<DataCoreUnitOfWork>(DataCoreContext);

			RuntimeContext = new TestRuntimeContext { AppSettings = MockAppSettings, Repositories = new TestRepositories(), Helpers = new TestHelpers() };

			#region Setup Repositores

			ConfigurationUnitOfWorkScope = new SimpleUnitOfWorkScope<ConfigurationUnitOfWork>(ConfigurationContext);
			DataCoreUnitOfWorkScope = new SimpleUnitOfWorkScope<DataCoreUnitOfWork>(DataCoreContext);
			LibraryUnitOfWorkScope = new SimpleUnitOfWorkScope<LibraryUnitOfWork>(LibraryContext);
			
			((TestRepositories)RuntimeContext.Repositories).Configuration = ConfigurationRepository = new ConfigurationRepository(ConfigurationUnitOfWorkScope);
			((TestRepositories)RuntimeContext.Repositories).Core = DataCoreRepository = new CoreRepository(DataCoreUnitOfWorkScope);
			((TestRepositories)RuntimeContext.Repositories).Library = LibraryRepository = new LibraryRepository(LibraryUnitOfWorkScope);
		
			#endregion
		}

		[TearDown]
		public virtual void TearDown()
		{
			DataCoreUnitOfWorkScope.Dispose();
			ConfigurationUnitOfWorkScope.Dispose();
			LibraryUnitOfWorkScope.Dispose();
		}
	}
}
