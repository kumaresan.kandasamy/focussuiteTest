﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Focus.Core;
using Focus.Core.IntegrationMessages;
using Focus.Core.Models.Integration;
using Focus.Services.Repositories;
using Focus.UnitTests.Core;
using Framework.Core;
using NUnit.Framework;

using Framework.Testing.NUnit;

#endregion

namespace Focus.UnitTests.ClientIntegration
{
  [TestFixture]
  public class ProviderTests : TestFixtureBase
  {
    private IntegrationRepository _testIntegrationRepository;

    /*
    private const IntegrationClient Client = IntegrationClient.Standalone;
    private const string Settings = "";
    */
    /*
    private const IntegrationClient Client = IntegrationClient.AOSOS;
    private const string Settings = "{\"Url\": \"https://www.ekostraining.ky.gov/zdac/xml/\", \"DefaultSecurityName\": \"nkumar\", \"DefaultPassword\": \"abcd1234\", \"DefaultOfficeId\": \"KY0001\", \"DefaultCountyId\": \"0\", \"DefaultAdminId\": \"KY100089632\", \"CtAggrVersion\": \"184\", \"OriginationMethod\": \"8\"}";
    */
    /*
    private const IntegrationClient Client = IntegrationClient.EKOS;
    private const string Settings = "";
    */

    //private const IntegrationClient Client = IntegrationClient.Aptimus;
    //private const string Settings = "{\"LiveInstance\":\"false\", \"BaseUrl\": \"https://developer.aptimus.com/api/\", \"JobUrlSuffix\": \"job-service/1/jobs\", \"Username\": \"jmacbeth@burning-glass.com\", \"Password\":\"01252Apt\"}";

    private const IntegrationClient Client = IntegrationClient.AtWorks;
    private const string Settings = "{\"Url\": \"????\", \"Customer\": \"????\", \"AuthCode\": \"????\"}";

    public override void SetUp()
    {
      base.SetUp();
      _testIntegrationRepository = new IntegrationRepository(RuntimeContext, Client, Settings);
    }

    [Test]
    [Explicit]
    public void Send_SaveJobRequest()
    {
      var request = new SaveJobRequest { JobId = 4760483 };
      var response = _testIntegrationRepository.SaveJob(request);
      OutputWebRequests(response);
    }

    [Test]
    [Explicit]
    public void Send_ReferJobSeekerRequest()
    {
      var request = new ReferJobSeekerRequest { JobId = 4760483, PersonId = 4767497 };
      var response = _testIntegrationRepository.ReferJobSeeker(request);
      OutputWebRequests(response);
    }

    [Test]
    [Explicit]
    public void Send_AssignJobSeekerActivity()
    {
      var request = new AssignJobSeekerActivityRequest { PersonId = 4760483, ActivityId = "1025"};
      var response = _testIntegrationRepository.AssignJobSeekerActivity(request);
      OutputWebRequests(response);
    }

    [Test]
    [Explicit]
    public void Send_GetEmployerRequest()
    {
      var request = new GetEmployerRequest { FEIN = "3" };
      var response = _testIntegrationRepository.GetEmployer(request);
      OutputWebRequests(response);
    }

    [Test]
    [Explicit]
    public void Send_SaveEmployerRequest()
    {
      var request = new SaveEmployerRequest { EmployerId = 6206098 };
      var response = _testIntegrationRepository.SaveEmployer(request);
      OutputWebRequests(response);
    }

    [Test]
    [Explicit]
    public void Send_SaveEmployeeRequest()
    {
      var request = new SaveEmployeeRequest { PersonId = 6206091 };
      var response = _testIntegrationRepository.SaveEmployee(request);
      OutputWebRequests(response);
    }


    [Test]
    [Explicit]
    public void Send_GetJobSeeker_ByUsername()
    {
      var request = new GetJobSeekerRequest { JobSeeker = new JobSeekerModel { Username = "kyaccount03@demo.com" } };
      var response = _testIntegrationRepository.GetJobSeeker(request);
      OutputWebRequests(response);
    }

    [Test]
    [Explicit]
    public void Send_SaveJobSeekerRequest()
    {
      var request = new SaveJobSeekerRequest
                      {
                        PersonId = 4770557,
                        JobSeeker = new JobSeekerModel{ ResumeXml = "<ResDoc>    <special />    <resume>      <contact>        <name>          <givenname>Unknown</givenname>          <surname>Unknown</surname>        </name>        <email>Test140821094639671141@burning-glass.com</email>        <website></website>        <address>          <street></street>          <street></street>          <city></city>          <county_fullname></county_fullname>          <state_fullname></state_fullname>          <postalcode></postalcode>          <country_fullname></country_fullname>        </address>      </contact>      <experience>        <employment_status_cd>3</employment_status_cd>        <job>          <jobid>0fad113c-ccad-40f4-825c-6b7177092bdc</jobid>          <employer>Media Guaranty</employer>          <title>Software QA Consultant</title>          <description>Created test plan and matrix for Media Asset Management client-  server system.  Used JavaStar, Silk and WinRunner to create  automated Java application client tests on Windows NT.  Designed  CORBA and Oracle Windows NT server stress tests.  Reported  problems and verified fixes.</description>          <address>            <city></city>            <state_fullname></state_fullname>            <country_fullname></country_fullname>          </address>          <daterange>            <start>10-1998</start>            <end>12-1998</end>          </daterange>        </job>        <job>          <jobid>b6757acc-9b0e-4329-a75e-1ad0d84217d1</jobid>          <employer>Hitachi</employer>          <title>Software QA Consultant</title>          <description>Created test plans for DOM (Data Object Manager), a CORBA  application on Solaris wrapping CICS/ECI, CICS/EPI and Oracle  legacy applications.  Added DOMScript methods testing statements,  functions and data type conversions to the automated test suite.  Tested C++ DOM Configuration Tool on Windows NT.  Used JavaStar to  create automated GUI tests of Java DOM Configuration Tool on Windows  NT and Solaris.  Reported problems and reviewed documentation.</description>          <address>            <city></city>            <state_fullname></state_fullname>            <country_fullname></country_fullname>          </address>          <daterange>            <start>07-1998</start>            <end>10-1998</end>          </daterange>        </job>        <job>          <jobid>04c0180e-5afa-4d3d-9895-b597aff8f4b3</jobid>          <employer>Vitria Technology</employer>          <title>Software QA Consultant</title>          <description>Wrote KPMG DataBus Publication and Subscription API Test Plan.  Implemented API test cases with Visual C++ and Java on Windows NT.  Used ClearDDTS to report software or documentation problems and  verify fixes.  Met with KPMG staff regarding API specification  changes and project status.</description>          <address>            <city></city>            <state_fullname></state_fullname>            <country_fullname></country_fullname>          </address>          <daterange>            <start>04-1998</start>            <end>05-1998</end>          </daterange>        </job>        <job>          <jobid>d0e07577-3c4d-431a-85ec-0ff2a641231d</jobid>          <employer>TimesTen Performance Software</employer>          <title>Software QA Consultant</title>          <description>Wrote Stress Test Plan for TimesTen Main Memory Database Server  software.  Developed SQL file generator and test case scripts in  perl running on Solaris, HP-UX, AIX and Windows NT.  Created C  programs to test ODBC (Open DataBase Connectivity) API.  Added new  API call support to multi-threaded test software written in C  emulating users simultaneously accessing the database.</description>          <address>            <city></city>            <state_fullname></state_fullname>            <country_fullname></country_fullname>          </address>          <daterange>            <start>01-1998</start>            <end>03-1998</end>          </daterange>        </job>        <job>          <jobid>d2d0bc41-cf78-4965-996a-8081ea5fcd5a</jobid>          <employer>Silicon Graphics</employer>          <title>Software QA Consultant</title>          <description>Installed IRIX 6.5 and IRIS FailSafe 1.2 High Availability Software  on Challenge machines.  Ran IP alias, Web server, NFS, Oracle,  Informix and Sybase failover tests.</description>          <address>            <city></city>            <state_fullname></state_fullname>            <country_fullname></country_fullname>          </address>          <daterange>            <start>10-1997</start>            <end>11-1997</end>          </daterange>        </job>        <job>          <jobid>fb339b10-72f4-4297-a146-69a62f857144</jobid>          <employer>Oracle Corporation</employer>          <title>Software QA Consultant</title>          <description>Installed Oracle 7 and Oracle 8 Database Servers on Solaris 2.5.  Tested interoperability with Kerberos, CyberSAFE, Identix and SecurID  Authentication Adapters.  Created  How to Use  documents for Kerberos,  CyberSAFE, Identix and SecurID administrators who maintain and add  client machines and users.</description>          <address>            <city></city>            <state_fullname></state_fullname>            <country_fullname></country_fullname>          </address>          <daterange>            <start>08-1997</start>            <end>10-1997</end>          </daterange>        </job>        <job>          <jobid>3aa24a81-e697-4b4b-bc58-1ba8df212576</jobid>          <employer>Sun Microsystems</employer>          <title>Software Test Development Consultant</title>          <description>Ran System V Verification Suite, SVVS, on 32-bit and 64-bit versions  of Solaris 2.6.  Created shell scripts to automate running SVVS and  emailing test results to the project team.  Created CGI scripts in  perl and HTML pages to remotely run SVVS from a web browser.  Ran  SoTest, a socket regression test tool, on UltraSPARC, SPARC and  Intel Pentium platforms running Solaris 2.6.</description>          <address>            <city></city>            <state_fullname></state_fullname>            <country_fullname></country_fullname>          </address>          <daterange>            <start>03-1997</start>            <end>08-1997</end>          </daterange>        </job>        <job>          <jobid>d51ed1e8-46b7-46c5-88db-03cd43fdbcca</jobid>          <employer>AOL Productions</employer>          <title>Software QA Consultant</title>          <description>Added load test cases to AOL s Web site QA Plan and procedures.  Created Web site load test tool with C program and shell scripts.  Evaluated Web site load test tools.  Developed Web site load test  scripts with Mercury Interactive s WebTest and LoadRunner.</description>          <address>            <city></city>            <state_fullname></state_fullname>            <country_fullname></country_fullname>          </address>          <daterange>            <start>11-1996</start>            <end>02-1997</end>          </daterange>        </job>        <job>          <jobid>329debe7-84dd-4267-a398-ac01d34870c3</jobid>          <employer>Netscape Communications</employer>          <title>Software QA Consultant</title>          <description>Tested Netscape Publishing and Community Systems on Solaris 2.5.  Installed and configured Oracle Database and Netscape Secure News  Server used by Publishing and Community Systems.  Tested Netscape  Chat Client for Windows 95.  Reported software and documentation  problems with Scopus.</description>          <address>            <city></city>            <state_fullname></state_fullname>            <country_fullname></country_fullname>          </address>          <daterange>            <start>07-1996</start>            <end>10-1996</end>          </daterange>        </job>        <job>          <jobid>242f6663-7f36-4cee-8cf9-26286f1648cc</jobid>          <employer>Amdahl</employer>          <title>Software QA Consultant</title>          <description>Created test plans for A+EDM, Amdahl Enterprise Desktop Manager,  client-server software providing desktop systems management functions:  configuration, file distribution, version control, auditing, security,  and scheduling.  Tested TCP/IP and LU6.2 clients on Windows 3.11, 95  and NT, Solaris 2.5, HP-UX 10.0 and AT&amp;T GIS.  Tested object-oriented  database servers on Solaris and MVS.  Reported problems and verified  fixes with Scopus.  Tested A+Unitree, Hierarchical Storage Management  software, against STK and ACL robotic tape libraries.</description>          <address>            <city></city>            <state_fullname></state_fullname>            <country_fullname></country_fullname>          </address>          <daterange>            <start>07-1995</start>            <end>07-1996</end>          </daterange>        </job>        <job>          <jobid>c0e59ff2-4daf-4dd8-a71d-dd7e9234b020</jobid>          <employer>Legent</employer>          <title>Software QA Consultant</title>          <description>Created test plans for Legent s Unix software configuration management  product, ENDEVOR/WSX 3.2, for SunOS 4.1.3, Solaris 2.4 and HP-UX 9.0.  Added and modified automated test cases and test suite driversusing  shell, sed, awk and perl scripts</description>          <address>            <city></city>            <state_fullname></state_fullname>            <country_fullname></country_fullname>          </address>          <daterange>            <start>07-1994</start>            <end>06-1995</end>          </daterange>        </job>        <job>          <jobid>eb367c62-cf96-4089-8dfb-ac98020d48e4</jobid>          <employer>Informix</employer>          <title>Software Development Consultant</title>          <description>Fixed C source code problems in database tools dbexport, dbimport,  dbload and dbschema.  Ran code coverage tests for OnLine 7.0 engine.  Created test cases to verify bug fixes and new product functionality  using SQL and database commands in Bourne shell scripts.</description>          <address>            <city></city>            <state_fullname></state_fullname>            <country_fullname></country_fullname>          </address>          <daterange>            <start>04-1994</start>            <end>06-1994</end>          </daterange>        </job>        <job>          <jobid>d563d81c-cd57-495a-b4e1-295e3f191552</jobid>          <employer>DHL</employer>          <title>Lead Software Test Development Consultant</title>          <description>Developed automated functional and performance tests of DHL's shipment  tracking system with Performix tools and Informix SQL on HP-UX 9.0  client-server systems.  Developed C shell test script drivers and C  functions used by the test scripts on SunOS.  Trained test development  group from Arthur Anderson that created additional Performix load  test scripts.</description>          <address>            <city></city>            <state_fullname></state_fullname>            <country_fullname></country_fullname>          </address>          <daterange>            <start>10-1993</start>            <end>04-1994</end>          </daterange>        </job>        <job>          <jobid>7e9f95da-6356-4c1b-8d89-452d8400cf61</jobid>          <employer>Frame Technology</employer>          <title>Software QA Consultant</title>          <description>Tested FrameMaker 4.0 software functionality and problem fixes for  Macintosh System 7.  Regressed problems against SunOS 4.1.3 to verify  core functionality.  Added procedures and documents to the test suite.</description>          <address>            <city></city>            <state_fullname></state_fullname>            <country_fullname></country_fullname>          </address>          <daterange>            <start>07-1993</start>            <end>09-1993</end>          </daterange>        </job>        <job>          <jobid>5947b66d-5bf2-44d0-8121-317b333a6b57</jobid>          <employer>Apple Computer</employer>          <title>Software QA Consultant</title>          <description>Tested Apple Toolbox QuickDraw and Notification Manager software  ported to the PowerPC platform.  Reported software problems and  verified fixes.  Evaluated QuickDraw test tools.</description>          <address>            <city></city>            <state_fullname></state_fullname>            <country_fullname></country_fullname>          </address>          <daterange>            <start>03-1993</start>            <end>06-1993</end>          </daterange>        </job>        <job>          <jobid>fd00fd78-c2fe-4b7f-bf5a-0e5a597fa5e9</jobid>          <employer>Quorum Software Systems</employer>          <title>Software QA Consultant</title>          <description>Created test plans for adapted Microsoft Word and Excel running on  Solaris and IRIX.  Interviewed and trained testers who implemented and  ran functional and regression tests.</description>          <address>            <city></city>            <state_fullname></state_fullname>            <country_fullname></country_fullname>          </address>          <daterange>            <start>01-1993</start>            <end>03-1993</end>          </daterange>        </job>        <job>          <jobid>4e68a172-2c59-4c78-b3d8-2a441d7ca363</jobid>          <employer>Sun Microsystems</employer>          <title>Software QA Consultant</title>          <description>Performed system administration duties: installed hardware and  operating systems, configured servers and client and installed  applications and test accounts.  Ran NFS LADDIS benchmark against  Solaris 2.1 and 2.2.  Ran test suites and reviewed manual and online  documentation for SunPro's C and Pascal compilers,</description>          <address>            <city></city>            <state_fullname></state_fullname>            <country_fullname></country_fullname>          </address>          <daterange>            <start>06-1992</start>            <end>12-1992</end>          </daterange>        </job>        <months_experience>66</months_experience>      </experience>      <education>        <school_status_cd>5</school_status_cd>        <norm_edu_level_cd>15</norm_edu_level_cd>        <school id='18'>          <institution>UC-Berkeley</institution>          <completiondate>06/2000</completiondate>          <degree>Bachelor s degree</degree>          <major>Computer Science</major>          <address />        </school>      </education>      <header>        <default>1</default>        <buildmethod>2</buildmethod>        <status>1</status>        <completionstatus>127</completionstatus>        <updatedon>21/08/2014 10:51:08</updatedon>        <resumeid>4785794</resumeid>      </header>      <statements>        <personal>          <sex>3</sex>          <ethnic_heritages>            <ethnic_heritage>              <ethnic_id>3</ethnic_id>              <selection_flag>-1</selection_flag>            </ethnic_heritage>            <ethnic_heritage>              <ethnic_id>4</ethnic_id>              <selection_flag>0</selection_flag>            </ethnic_heritage>            <ethnic_heritage>              <ethnic_id>5</ethnic_id>              <selection_flag>0</selection_flag>            </ethnic_heritage>            <ethnic_heritage>              <ethnic_id>2</ethnic_id>              <selection_flag>0</selection_flag>            </ethnic_heritage>            <ethnic_heritage>              <ethnic_id>6</ethnic_id>              <selection_flag>0</selection_flag>            </ethnic_heritage>            <ethnic_heritage>              <ethnic_id>1</ethnic_id>              <selection_flag>0</selection_flag>            </ethnic_heritage>            <ethnic_heritage>              <ethnic_id>-1</ethnic_id>              <selection_flag>-1</selection_flag>            </ethnic_heritage>          </ethnic_heritages>          <citizen_flag>-1</citizen_flag>          <disability_status>3</disability_status>          <license>            <driver_flag>1</driver_flag>            <driver_class>4</driver_class>            <driver_class_text></driver_class_text>            <drv_pass_flag>0</drv_pass_flag>            <drv_double_flag>0</drv_double_flag>            <drv_hazard_flag>0</drv_hazard_flag>            <drv_airbrake_flag>0</drv_airbrake_flag>            <drv_tank_flag>0</drv_tank_flag>            <drv_bus_flag>0</drv_bus_flag>            <drv_tankhazard_flag>0</drv_tankhazard_flag>            <drv_cycle_flag>0</drv_cycle_flag>          </license>          <veteran>            <vet_flag>-1</vet_flag>            <subscribe_vps>-1</subscribe_vps>            <vet_era>2</vet_era>            <campaign_vet_flag>-1</campaign_vet_flag>            <vet_disability_status>1</vet_disability_status>            <vet_discharge>General</vet_discharge>            <branch_of_service>USCG</branch_of_service>            <rank>020</rank>            <vet_start_date>01/01/2000</vet_start_date>            <vet_end_date>01/01/2002</vet_end_date>          </veteran>          <preferences>            <sal>100000</sal>            <salary_unit_cd>5</salary_unit_cd>            <resume_searchable>1</resume_searchable>            <postings_interested_in>0</postings_interested_in>            <relocate>-1</relocate>            <shift>              <shift_first_flag>-1</shift_first_flag>              <shift_second_flag>-1</shift_second_flag>              <shift_third_flag>-1</shift_third_flag>              <shift_rotating_flag>-1</shift_rotating_flag>              <shift_split_flag>-1</shift_split_flag>              <shift_varies_flag>-1</shift_varies_flag>              <work_week>3</work_week>              <work_type>3</work_type>              <work_over_time>-1</work_over_time>            </shift>          </preferences>        </personal>      </statements>      <summary>        <summary include='1'></summary>      </summary>      <skills>        <skills>a + certified, account adjustment, aix, amdahl, america online, api, apple, application programming interface, arbitration, asset management, auditing, automate, awk, bourne shell, c, c + +, c shell(csh), c++, cgi, cics, client/server, common gateway interface, computer hardware, computer hardware/software knowledge, configuration management, corba, customer information control system(cics), customer relations, customer service, cvs, database administration, description and demonstration of products, documentation, dom, drivers, endevor, framemaker, functional, gis, graphical user interface, graphical user interface(gui), hardware and software configuration, hardware/software error explanation, hp - ux, hp-ux, html, ibm mvs, informix, informix sql, intel, ip, irix, java, load runner, macintosh, memory, mercury interactive, microsoft excel, microsoft windows, microsoft windows 3.11, microsoft windows 95, microsoft windows nt, microsoft word, mvs, netscape, netscape enterprise server, network file system, nfs, object oriented design, object-oriented analysis and design(ooad), odbc, open database connectivity(odbc), operating systems, oracle, oracle crm, oracle dba, oracle version 7, oracle version 8, pascal, pentium, perl, publicity, quality assurance and control, quality control, robotic, robotics, scheduling, sed, servers, shell scripting, shell scripts, silk performer, silkperformer, software testing, solaris, sparc, specification, sql, sun sparcstation, sunos, sybase, system administration, system v, systems management, tcp/ip, test development, test tools, transmission control protocol/internet protocol(tcp/ip), typing, unix, version control, visual c + +, visual c++, web servers, web site production, website production, windows nt, winrunner, writing</skills>        <internship_skills />      </skills>      <skillrollup>        <canonskill name='account adjustment' />        <canonskill name='application programming interface' />        <canonskill name='arbitration' />        <canonskill name='asset management' />        <canonskill name='awk' />        <canonskill name='bourne shell' />        <canonskill name='c++' />        <canonskill name='c shell(csh)' />        <canonskill name='client/server' />        <canonskill name='common gateway interface' />        <canonskill name='computer hardware/software knowledge' />        <canonskill name='configuration management' />        <canonskill name='corba' />        <canonskill name='customer information control system(cics)' />        <canonskill name='customer service' />        <canonskill name='database administration' />        <canonskill name='description and demonstration of products' />        <canonskill name='dom' />        <canonskill name='framemaker' />        <canonskill name='gis' />        <canonskill name='graphical user interface(gui)' />        <canonskill name='hardware/software error explanation' />        <canonskill name='hardware and software configuration' />        <canonskill name='hp-ux' />        <canonskill name='informix' />        <canonskill name='irix' />        <canonskill name='java' />        <canonskill name='load runner' />        <canonskill name='microsoft excel' />        <canonskill name='microsoft windows' />        <canonskill name='microsoft windows nt' />        <canonskill name='microsoft word' />        <canonskill name='mvs' />        <canonskill name='netscape enterprise server' />        <canonskill name='network file system' />        <canonskill name='object-oriented analysis and design(ooad)' />        <canonskill name='open database connectivity(odbc)' />        <canonskill name='operating systems' />        <canonskill name='oracle' />        <canonskill name='oracle crm' />        <canonskill name='pascal' />        <canonskill name='perl' />        <canonskill name='publicity' />        <canonskill name='quality assurance and control' />        <canonskill name='robotics' />        <canonskill name='scheduling' />        <canonskill name='shell scripts' />        <canonskill name='silkperformer' />        <canonskill name='software testing' />        <canonskill name='solaris' />        <canonskill name='sparc' />        <canonskill name='sql' />        <canonskill name='sun sparcstation' />        <canonskill name='sunos' />        <canonskill name='sybase' />        <canonskill name='system administration' />        <canonskill name='system v' />        <canonskill name='systems management' />        <canonskill name='test development' />        <canonskill name='test tools' />        <canonskill name='transmission control protocol/internet protocol(tcp/ip)' />        <canonskill name='typing' />        <canonskill name='unix' />        <canonskill name='version control' />        <canonskill name='visual c++' />        <canonskill name='web servers' />        <canonskill name='website production' />        <canonskill name='windows nt' />        <canonskill name='winrunner' />        <canonskill name='a + certified' />        <canonskill name='aix' />        <canonskill name='amdahl' />        <canonskill name='america online' />        <canonskill name='api' />        <canonskill name='apple' />        <canonskill name='auditing' />        <canonskill name='automate' />        <canonskill name='c' />        <canonskill name='c + +' />        <canonskill name='cgi' />        <canonskill name='cics' />        <canonskill name='computer hardware' />        <canonskill name='customer relations' />        <canonskill name='cvs' />        <canonskill name='documentation' />        <canonskill name='drivers' />        <canonskill name='endevor' />        <canonskill name='functional' />        <canonskill name='graphical user interface' />        <canonskill name='hp - ux' />        <canonskill name='html' />        <canonskill name='ibm mvs' />        <canonskill name='informix sql' />        <canonskill name='intel' />        <canonskill name='ip' />        <canonskill name='macintosh' />        <canonskill name='memory' />        <canonskill name='mercury interactive' />        <canonskill name='microsoft windows 3.11' />        <canonskill name='microsoft windows 95' />        <canonskill name='netscape' />        <canonskill name='nfs' />        <canonskill name='object oriented design' />        <canonskill name='odbc' />        <canonskill name='oracle dba' />        <canonskill name='oracle version 7' />        <canonskill name='oracle version 8' />        <canonskill name='pentium' />        <canonskill name='quality control' />        <canonskill name='robotic' />        <canonskill name='sed' />        <canonskill name='servers' />        <canonskill name='shell scripting' />        <canonskill name='silk performer' />        <canonskill name='specification' />        <canonskill name='tcp/ip' />        <canonskill name='visual c + +' />        <canonskill name='web site production' />        <canonskill name='writing' />      </skillrollup>      <hide_options>        <hide_daterange>0</hide_daterange>        <hide_eduDates>0</hide_eduDates>        <hide_militaryserviceDates>0</hide_militaryserviceDates>        <hide_workDates>0</hide_workDates>        <hide_contact>0</hide_contact>        <hide_name>0</hide_name>        <hide_email>0</hide_email>        <hide_homePhoneNumber>0</hide_homePhoneNumber>        <hide_workPhoneNumber>0</hide_workPhoneNumber>        <hide_cellPhoneNumber>0</hide_cellPhoneNumber>        <hide_faxPhoneNumber>0</hide_faxPhoneNumber>        <hide_nonUSPhoneNumber>0</hide_nonUSPhoneNumber>        <hide_affiliations>0</hide_affiliations>        <hide_certifications_professionallicenses>0</hide_certifications_professionallicenses>        <hide_employer>0</hide_employer>        <hide_driver_license>0</hide_driver_license>        <unhide_driver_license>0</unhide_driver_license>        <hide_honors>0</hide_honors>        <hide_interests>0</hide_interests>        <hide_internships>0</hide_internships>        <hide_languages>0</hide_languages>        <hide_veteran>0</hide_veteran>        <hide_objective>0</hide_objective>        <hide_personalinformation>0</hide_personalinformation>        <hide_professionaldevelopment>0</hide_professionaldevelopment>        <hide_publications>0</hide_publications>        <hide_references>0</hide_references>        <hide_skills>0</hide_skills>        <hide_technicalskills>0</hide_technicalskills>        <hide_volunteeractivities>0</hide_volunteeractivities>        <hide_thesismajorprojects>0</hide_thesismajorprojects>        <hidden_skills></hidden_skills>      </hide_options>    </resume>  </ResDoc>"}
                      };
      var response = _testIntegrationRepository.SaveJobSeeker(request);
      OutputWebRequests(response);
    }

    [Test]
    [Explicit]
    public void Send_AuthenticateJobSeeker()
    {
      var request = new AuthenticateJobSeekerRequest { UserName = "kyaccount03@demo.com", Password = "1timep" };
      var response = _testIntegrationRepository.AuthenticateJobSeeker(request);
      OutputWebRequests(response);
    }

    
    private void OutputWebRequests(IIntegrationResponse response)
    {
      if (response.IntegrationBreadcrumbs.IsNotNullOrEmpty())
      {
        foreach (var breadcrumb in response.IntegrationBreadcrumbs)
        {
          Trace.WriteLine(breadcrumb.OutgoingRequest);
          Trace.WriteLine(breadcrumb.IncomingResponse);
        }
        Trace.Flush();
      }
    }
  }
}
