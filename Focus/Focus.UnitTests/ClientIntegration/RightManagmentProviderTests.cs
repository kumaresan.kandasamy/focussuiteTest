﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Diagnostics;
using Focus.Core.Models.Career;
using Focus.UnitTests.Core;
using NUnit.Framework;

using Framework.Core;

using Focus.Core;
using Focus.Core.IntegrationMessages;
using Focus.Core.Models.Integration;
using Focus.Services.Repositories;
using Framework.Testing.NUnit;

#endregion

namespace Focus.UnitTests.ClientIntegration
{
  [TestFixture]
  public class RightManagementProviderTests : TestFixtureBase
  {
    private IntegrationRepository _testIntegrationRepository;

		private const IntegrationClient Client = IntegrationClient.RightManagement;
		private const string Settings = "{\"Url\": \"http://ukfocusstage.burning-glass.com/integrationservices/api/rightmanagement/\", \"SourceSystem\": \"BurningGlass\", \"PhoneNumberLength\": 10}";
	  private const string UserExternalId = "54F55921-C707-42CB-A73E-89F6DD5067A4";

    public override void SetUp()
    {
      base.SetUp();

      _testIntegrationRepository = new IntegrationRepository(RuntimeContext, Client, Settings);
    }

		[Test]
		[Explicit]
		public void Send_GetJobSeeker_ByExternalId()
		{
			var request = new GetJobSeekerRequest { JobSeeker = new JobSeekerModel { Resume = new ResumeModel{ ResumeContent = new ResumeBody{SeekerContactDetails = new Contact()}}, ExternalId = UserExternalId }, IsNew = true};
			var response = _testIntegrationRepository.GetJobSeeker(request);
			OutputWebRequests(response);
		}

    [Test]
    [Explicit]
		public void Send_LogAction()
    {
			var request = new LogActionRequest { ActionerExternalId = UserExternalId, ActionType = ActionTypes.CreateNewResume, ActionedOn = DateTime.Now };
			var response = _testIntegrationRepository.LogAction(request);
      OutputWebRequests(response);
    }

		/// <summary>
		/// Outputs the web requests.
		/// </summary>
		/// <param name="response">The response.</param>
    private void OutputWebRequests(IIntegrationResponse response)
    {
      if (response.IntegrationBreadcrumbs.IsNotNullOrEmpty())
      {
        foreach (var breadcrumb in response.IntegrationBreadcrumbs)
        {
          Trace.WriteLine(breadcrumb.OutgoingRequest);
          Trace.WriteLine(breadcrumb.IncomingResponse);
        }

        Trace.Flush();
      }
    }
  }
}
