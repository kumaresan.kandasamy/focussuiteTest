﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Linq;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Models.Career;
using Focus.Services.Integration.Implementations.AOSOS.CommandBuilders;
using Framework.Testing.NUnit;
using NUnit.Framework;

using Focus.Core;
using Focus.Core.Models.Integration;
using Focus.Services.Integration.Implementations.AOSOS;

#endregion

namespace Focus.UnitTests.ClientIntegration.AOSOS
{
  [TestFixture]
  public class AOSOSEmployerCommandBuilderTests
  {
    private ClientSettings _clientSettings;
    private EmployerCommandBuilder _employerCommandBuilder;
    private const string xMLBreaker = "& < > \" '";


    [SetUp]
    public void SetUp()
    {
      _clientSettings = new ClientSettings
      {
        DefaultSecurityName = "TestSecurityName",
        DefaultPassword = "TestPassword",
        OriginationMethod = "8",
        DefaultOfficeId = "1000",
        DefaultAdminId = "TestDefaultAdminId",
        CtAggrVersion = "123"
      };


      var externalIds = new List<ExternalLookUpItemDto>();
      externalIds.Add(new ExternalLookUpItemDto { ExternalId = "externalGenderId", ExternalLookUpType = ExternalLookUpType.Gender, InternalId = Genders.Male.ToString() });
      externalIds.Add(new ExternalLookUpItemDto { ExternalId = "externalDisibilityId", ExternalLookUpType = ExternalLookUpType.DisabilityStatus, InternalId = DisabilityStatus.NotDisclosed.ToString() });
      externalIds.Add(new ExternalLookUpItemDto { ExternalId = "externalCountyId", ExternalLookUpType = ExternalLookUpType.County, InternalId = "123" });
      externalIds.Add(new ExternalLookUpItemDto { ExternalId = "externalStateId", ExternalLookUpType = ExternalLookUpType.State, InternalId = "456" });
      externalIds.Add(new ExternalLookUpItemDto { ExternalId = "US", ExternalLookUpType = ExternalLookUpType.Country, InternalId = "789" });
      externalIds.Add(new ExternalLookUpItemDto { ExternalId = "UK", ExternalLookUpType = ExternalLookUpType.Country, InternalId = "1" });
      externalIds.Add(new ExternalLookUpItemDto { ExternalId = "external TransitioningServiceMember", ExternalLookUpType = ExternalLookUpType.VeteranEra, InternalId = VeteranEra.TransitioningServiceMember.ToString() });
      externalIds.Add(new ExternalLookUpItemDto { ExternalId = "external SpecialDisabled", ExternalLookUpType = ExternalLookUpType.VeteranDisabilityStatus, InternalId = VeteranDisabilityStatus.SpecialDisabled.ToString() });
      externalIds.Add(new ExternalLookUpItemDto { ExternalId = "external Grade_12_No_Diploma_12", ExternalLookUpType = ExternalLookUpType.EducationLevel, InternalId = EducationLevel.Grade_12_No_Diploma_12.ToString() });
      externalIds.Add(new ExternalLookUpItemDto { ExternalId = "external slautation 1", ExternalLookUpType = ExternalLookUpType.Salutation, InternalId = "1" });
      externalIds.Add(new ExternalLookUpItemDto { ExternalId = "external owner 1", ExternalLookUpType = ExternalLookUpType.EmployerOwner, InternalId = "1" });

      _employerCommandBuilder = new EmployerCommandBuilder(_clientSettings, externalIds, null);
    }


    #region Get Employer

    [Test]
    public void When_GetEmployerById_PassedValidEmployerId_AValidXmlString_IsReturned()
    {
      var response = _employerCommandBuilder.GetEmployerById("& < > \" ' 123");

      response.ShouldBeValidXml();
    }

    [Test]
    public void When_GetEmployerById_PassedValidEmployerId_ACorrectFormatRequest_IsReturned()
    {
      var guidId = Guid.NewGuid();
			var expectedResponse = string.Format("<osos><request security_username='{0}' security_password='{1}' current_office_id='{2}' ct_aggr_version='{3}' admin_id='{5}'><employers><employer id='{4}' method='select' expand='*'/></employers></request></osos>",
        _clientSettings.DefaultSecurityName, _clientSettings.DefaultPassword, _clientSettings.DefaultOfficeId, _clientSettings.CtAggrVersion, guidId, _clientSettings.DefaultAdminId);

      var response = _employerCommandBuilder.GetEmployerById(guidId.ToString());

      response.ShouldEqual(expectedResponse);
    }

    [Test]
    public void When_GetEmployerByFEIN_PassedValidFEIN_AValidXmlString_IsReturned()
    {
      var response = _employerCommandBuilder.GetEmployerByFein(Guid.NewGuid().ToString());

      response.ShouldBeValidXml();
    }

    [Test]
    public void When_GetEmployerByFEIN_PassedValidFEIN_ACorrectFormatRequest_IsReturned()
    {
      const string fein = "12-3456789";
			var expectedResponse = string.Format("<osos><request security_username='{0}' security_password='{1}' current_office_id='{2}' ct_aggr_version='{3}' admin_id='{5}'><employer_search method='query' expand='legal_name,company,admin_id,office_id,sein,address.*,phone_pri,phone_sec,fax_pri,email,url_pri,url_sec,naics,sein,fein,business_desc,owner'><fein>{4}</fein></employer_search></request></osos>",
        _clientSettings.DefaultSecurityName, _clientSettings.DefaultPassword, _clientSettings.DefaultOfficeId, _clientSettings.CtAggrVersion, fein.Replace("-", ""), _clientSettings.DefaultAdminId);

      var response = _employerCommandBuilder.GetEmployerByFein(fein);

      response.ShouldEqual(expectedResponse);
    }

    [Test]
    public void When_GetEmployerByCompanyName_PassedValidName_AValidXmlString_IsReturned()
    {
      var response = _employerCommandBuilder.GetEmployerByCompanyName("'<");

      response.ShouldBeValidXml();
    }

    [Test]
    public void When_GetEmployerByCompanyName_PassedValidName_ACorrectFormatRequest_IsReturned()
    {

      const string companyName = "Company Name for testing";
			var expectedResponse = string.Format("<osos><request security_username='{0}' security_password='{1}' current_office_id='{2}' ct_aggr_version='{3}' admin_id='{5}'><employer_search method='query' expand='legal_name,company,admin_id,office_id,sein,address.*,phone_pri,phone_sec,fax_pri,email,url_pri,url_sec,naics,sein,fein,business_desc,owner'><company>{4}</company></employer_search></request></osos>",
        _clientSettings.DefaultSecurityName, _clientSettings.DefaultPassword, _clientSettings.DefaultOfficeId, _clientSettings.CtAggrVersion, companyName, _clientSettings.DefaultAdminId);

      var response = _employerCommandBuilder.GetEmployerByCompanyName(companyName);

      response.ShouldEqual(expectedResponse);
    }

    [Test]
    public void When_GetEmployerByLegalName_PassedValidName_AValidXmlString_IsReturned()
    {
      var response = _employerCommandBuilder.GetEmployerByLegalName("'<");

      response.ShouldBeValidXml();
    }

    [Test]
    public void When_GetEmployerByLegalName_PassedValidLegalName_ACorrectFormatRequest_IsReturned()
    {

      const string legalName = "Legal Name for testing";
			var expectedResponse = string.Format("<osos><request security_username='{0}' security_password='{1}' current_office_id='{2}' ct_aggr_version='{3}' admin_id='{5}'><employer_search method='query' expand='legal_name,company,admin_id,office_id,sein,address.*,phone_pri,phone_sec,fax_pri,email,url_pri,url_sec,naics,sein,fein,business_desc,owner'><legal_name>{4}</legal_name></employer_search></request></osos>",
        _clientSettings.DefaultSecurityName, _clientSettings.DefaultPassword, _clientSettings.DefaultOfficeId, _clientSettings.CtAggrVersion, legalName, _clientSettings.DefaultAdminId);

      var response = _employerCommandBuilder.GetEmployerByLegalName(legalName);

      response.ShouldEqual(expectedResponse);
    }


    #endregion

    #region Assign Admin

    [Test]
    public void When_AssignAdminToEmployer_PassedValidAdminAndEmployerIds_AValidXmlString_IsReturned()
    {
			var response = _employerCommandBuilder.AssignAdminToEmployer("& < > \" ' 123", "& < > \" ' 123", "1", null, null, null, null);

      response.ShouldBeValidXml();
    }

    [Test]
    public void When_AssignAdminToEmployer_PassedValidAdminAndEmployerIds_ACorrectFormatRequest_IsReturned()
    {
      var uuid = Guid.NewGuid();
      const string employerId = "Test Employer ID";
      const string adminId = "Test Admin ID";
      const string version = "1";
			var expectedResponse = string.Format("<osos><request security_username='{0}' security_password='{1}' current_office_id='{2}' ct_aggr_version='{3}' admin_id='{8}'><employers><employer method='update' id='{4}' version='{5}' uuid='{6}'><admin_id>{7}</admin_id></employer></employers></request></osos>",
        _clientSettings.DefaultSecurityName, _clientSettings.DefaultPassword, _clientSettings.DefaultOfficeId, _clientSettings.CtAggrVersion, employerId, version, uuid, adminId, _clientSettings.DefaultAdminId);

			var response = _employerCommandBuilder.AssignAdminToEmployer(employerId, adminId, version, null, null, null, null, uuid);

      response.ShouldEqual(expectedResponse);
    }

    #endregion

    #region Save Activity
    
    [Test]
    public void When_AssignEmployerActivity_IsPassedValidIds_AValidXmlString_IsReturned()
    {
			var response = _employerCommandBuilder.SaveActivity(xMLBreaker, xMLBreaker, xMLBreaker, xMLBreaker, xMLBreaker, "1", null, null, null, null);

      response.ShouldBeValidXml();
    }

    [Test]
    public void When_AssignEmployerActivity_IsPassedNoAdminId_ACorrectFormatRequest_IsReturned()
    {
      var uuid = Guid.NewGuid();
      const string employerId = "Test Employer ID";
      const string employeeId = "Test Employee ID";
      const string activityId = "Test Activity ID";
      const string officeId = "Test Office ID";
      const string version = "1";
      var now = DateTime.Now;

			var expectedResponse = string.Format("<osos><request security_username='{0}' security_password='{1}' current_office_id='{2}' ct_aggr_version='{3}' admin_id='{10}'><employers><employer method='update' id='{4}' version='{5}' uuid='{6}'><activities><activity uuid='{6}' method='insert'><contact_id>{7}</contact_id><employer_service_type_cd>{8}</employer_service_type_cd><ctime>{9}</ctime></activity></activities></employer></employers></request></osos>",
        _clientSettings.DefaultSecurityName, _clientSettings.DefaultPassword, _clientSettings.DefaultOfficeId, _clientSettings.CtAggrVersion, employerId, version, uuid, employeeId, activityId, now.ToString("MMddyyyy"), _clientSettings.DefaultAdminId);

			var response = _employerCommandBuilder.SaveActivity(employerId, employeeId, activityId, null, officeId, version, null, null, null, null, uuid, now);

      response.ShouldEqual(expectedResponse);
    }

    [Test]
    public void When_AssignEmployerActivity_IsPassedAdminIdAndNoOfficeId_ACorrectFormatRequest_IsReturned()
    {
      var uuid = Guid.NewGuid();
      const string employerId = "Test Employer ID";
      const string employeeId = "Test Employee ID";
      const string activityId = "Test Activity ID";
      //const string officeId = "Test Office ID";
      const string adminId = "Test Admin ID";
      const string version = "1";
      var now = DateTime.Now;

			var expectedResponse = string.Format("<osos><request security_username='{0}' security_password='{1}' current_office_id='{2}' ct_aggr_version='{3}' admin_id='{11}'><employers><employer method='update' id='{4}' version='{5}' uuid='{6}'><activities><activity uuid='{6}' method='insert'><contact_id>{7}</contact_id><employer_service_type_cd>{8}</employer_service_type_cd><admin_id>{9}</admin_id><ctime>{10}</ctime></activity></activities></employer></employers></request></osos>",
        _clientSettings.DefaultSecurityName, _clientSettings.DefaultPassword, _clientSettings.DefaultOfficeId, _clientSettings.CtAggrVersion, employerId, version, uuid, employeeId, activityId, adminId, now.ToString("MMddyyyy"), _clientSettings.DefaultAdminId);

			var response = _employerCommandBuilder.SaveActivity(employerId, employeeId, activityId, adminId, null, version, null, null, null, null, uuid, now);

      response.ShouldEqual(expectedResponse);
    }

    [Test]
    public void When_AssignEmployerActivity_IsPassedAdminIdAndOfficeId_ACorrectFormatRequest_IsReturned()
    {
      var uuid = Guid.NewGuid();
      const string employerId = "Test Employer ID";
      const string employeeId = "Test Employee ID";
      const string activityId = "Test Activity ID";
      const string officeId = "Test Office ID";
      const string adminId = "Test Admin ID";
      const string version = "1";
      var now = DateTime.Now;

			var expectedResponse = string.Format("<osos><request security_username='{0}' security_password='{1}' current_office_id='{2}' ct_aggr_version='{3}' admin_id='{12}'><employers><employer method='update' id='{4}' version='{5}' uuid='{6}'><activities><activity uuid='{6}' method='insert'><contact_id>{7}</contact_id><employer_service_type_cd>{8}</employer_service_type_cd><admin_id>{9}</admin_id><office_id>{10}</office_id><ctime>{11}</ctime></activity></activities></employer></employers></request></osos>",
        _clientSettings.DefaultSecurityName, _clientSettings.DefaultPassword, _clientSettings.DefaultOfficeId, _clientSettings.CtAggrVersion, employerId, version, uuid, employeeId, activityId, adminId, officeId, now.ToString("MMddyyyy"), _clientSettings.DefaultAdminId);

			var response = _employerCommandBuilder.SaveActivity(employerId, employeeId, activityId, adminId, officeId, version, null, null, null, null, uuid, now);

      response.ShouldEqual(expectedResponse);
    }

    #endregion

    #region Save Employer

    [Test]
    public void When_SaveEmployer_IsPassedEmployer_AValidXmlString_IsReturned()
    {
			var response = _employerCommandBuilder.SaveEmployer(BuildSimpleEmployer(1), "1", null, null, null, null);

      response.ShouldBeValidXml();
    }

    [Test]
    public void When_SaveEmployer_IsPassedValidEmployer_ASingleEmployersElementIsReturned()
    {
      var employer = BuildSimpleEmployer(1);
      employer.ExternalId = null;
			var response = _employerCommandBuilder.SaveEmployer(employer, "1", null, null, null, null);

      var responseXml = XDocument.Parse(response);

      responseXml.Descendants("employers").Count().ShouldEqual(1);
    }

    [Test]
    public void When_SaveEmployer_IsPassedValidEmployer_ASingleEmployerElementIsReturned()
    {
      var employer = BuildSimpleEmployer(1);
      employer.ExternalId = null;
			var response = _employerCommandBuilder.SaveEmployer(employer, "1", null, null, null, null);

      var responseXml = XDocument.Parse(response);

      responseXml.Descendants("employer").Count().ShouldEqual(1);
    }

    [Test]
    public void When_SaveEmployer_IsPassedNoExternalId_AnInsertCommand_IsReturned()
    {
      var employer = BuildSimpleEmployer(1);
      employer.ExternalId = null;
			var response = _employerCommandBuilder.SaveEmployer(employer, "1", null, null, null, null);

      var responseXml = XDocument.Parse(response);
      var employerElement = responseXml.Descendants("employer").SingleOrDefault();
      employerElement.Attribute("method").Value.ShouldEqual("insert");
    }

    [Test]
    public void When_SaveEmployer_IsPassedAnExternalId_AnUpdateCommand_IsReturned()
    {
			var response = _employerCommandBuilder.SaveEmployer(BuildSimpleEmployer(1), "1", null, null, null, null);

      var responseXml = XDocument.Parse(response);
      var employerElement = responseXml.Descendants("employer").SingleOrDefault();
      employerElement.Attribute("method").Value.ShouldEqual("update");
    }

    [Test]
    public void When_SaveEmployer_IsPassedCompanyName_ItIsIncludedInXmlRequest()
    {
      const string employerName = "This is the employer name";
      var employer = BuildSimpleEmployer(1);
      employer.Name = employerName;

			var response = _employerCommandBuilder.SaveEmployer(employer, "1", null, null, null, null);

      var responseXml = XDocument.Parse(response);
      var employerElement = responseXml.Descendants("employer").SingleOrDefault();

      employerElement.Element("company").Value.ShouldEqual(employerName);
    }

    [Test]
    public void When_SaveEmployer_IsPassedExternalOfficeId_ItIsIncludedInXmlRequest()
    {
      const string officeId = "Office Id";
      var employer = BuildSimpleEmployer(1);
      employer.ExternalOfficeId = officeId;

			var response = _employerCommandBuilder.SaveEmployer(employer, "1", "selfregky0055", null, "KY0055", null);

      var responseXml = XDocument.Parse(response);
      var employerElement = responseXml.Descendants("employer").SingleOrDefault();

      employerElement.Element("office_id").Value.ShouldEqual(officeId);
    }

    [Test]
    public void When_SaveEmployer_IsPassedLegalName_ItIsIncludedInXmlRequest()
    {
      const string legalName = "This is the legal name";
      var employer = BuildSimpleEmployer(1);
      employer.LegalName = legalName;

			var response = _employerCommandBuilder.SaveEmployer(employer, "1", null, null, null, null);

      var responseXml = XDocument.Parse(response);
      var employerElement = responseXml.Descendants("employer").SingleOrDefault();

      employerElement.Element("legal_name").Value.ShouldEqual(legalName);
    }

    [Test]
    public void When_SaveEmployer_IsPassedExternalAdminId_ItIsIncludedInXmlRequest()
    {
      const string externalAdminId = "This is the admin Id";
      var employer = BuildSimpleEmployer(1);
      employer.ExternalAdminId = externalAdminId;

			var response = _employerCommandBuilder.SaveEmployer(employer, "1", "selfregky0055", null, null, null);

      var responseXml = XDocument.Parse(response);
      var employerElement = responseXml.Descendants("employer").SingleOrDefault();

      employerElement.Element("admin_id").Value.ShouldEqual(externalAdminId);
    }

    [Test]
    public void When_SaveEmployer_IsPassedSEIN_ItIsIncludedInXmlRequest()
    {
      const string sein = "11-222222";
      var employer = BuildSimpleEmployer(1);
      employer.StateEmployerIdentificationNumber = sein;

			var response = _employerCommandBuilder.SaveEmployer(employer, "1", null, null, null, null);

      var responseXml = XDocument.Parse(response);
      var employerElement = responseXml.Descendants("employer").SingleOrDefault();

      employerElement.Element("sein").Value.ShouldEqual("11222222");
    }

    [Test]
    public void When_SaveEmployer_IsPassedFEIN_ItIsIncludedInXmlRequest()
    {
      const string fein = "99-997777";
      var employer = BuildSimpleEmployer(1);
      employer.FederalEmployerIdentificationNumber = fein;

			var response = _employerCommandBuilder.SaveEmployer(employer, "1", null, null, null, null);

      var responseXml = XDocument.Parse(response);
      var employerElement = responseXml.Descendants("employer").SingleOrDefault();

      employerElement.Element("fein").Value.ShouldEqual("99997777");
    }

    [Test]
    public void When_SaveEmployer_IsPassedPrimaryPhoneNumber_ItIsIncludedInXmlRequest()
    {
      const string primaryPhone = "555-555999";
      var employer = BuildSimpleEmployer(1);
      employer.Phone = primaryPhone;

			var response = _employerCommandBuilder.SaveEmployer(employer, "1", null, null, null, null);

      var responseXml = XDocument.Parse(response);
      var employerElement = responseXml.Descendants("employer").SingleOrDefault();

      employerElement.Element("phone_pri").Value.ShouldEqual("555555999");
    }

    [Test]
    public void When_SaveEmployer_IsPassedPrimaryPhoneExtension_ItIsIncludedInXmlRequest()
    {
      const string primaryPhoneExtension = "1234";
      var employer = BuildSimpleEmployer(1);
      employer.PhoneExt = primaryPhoneExtension;

			var response = _employerCommandBuilder.SaveEmployer(employer, "1", null, null, null, null);

      var responseXml = XDocument.Parse(response);
      var employerElement = responseXml.Descendants("employer").SingleOrDefault();

      employerElement.Element("phone_pri_ext").Value.ShouldEqual(primaryPhoneExtension);
    }

    [Test]
    public void When_SaveEmployer_IsPassedSecondaryPhoneNumber_ItIsIncludedInXmlRequest()
    {
      const string secondaryPhone = "555-1234567";
      var employer = BuildSimpleEmployer(1);
      employer.AltPhone = secondaryPhone;

			var response = _employerCommandBuilder.SaveEmployer(employer, "1", null, null, null, null);

      var responseXml = XDocument.Parse(response);
      var employerElement = responseXml.Descendants("employer").SingleOrDefault();

      employerElement.Element("phone_sec").Value.ShouldEqual("5551234567");
    }

    [Test]
    public void When_SaveEmployer_IsPassedSecondaryPhoneExtension_ItIsIncludedInXmlRequest()
    {
      const string secondaryPhoneExtension = "4321";
      var employer = BuildSimpleEmployer(1);
      employer.AltPhoneExt = secondaryPhoneExtension;

			var response = _employerCommandBuilder.SaveEmployer(employer, "1", null, null, null, null);

      var responseXml = XDocument.Parse(response);
      var employerElement = responseXml.Descendants("employer").SingleOrDefault();

      employerElement.Element("phone_sec_ext").Value.ShouldEqual(secondaryPhoneExtension);
    }


    [Test]
    public void When_SaveEmployer_IsPassedFaxNumber_ItIsIncludedInXmlRequest()
    {
      const string fax = "555-7654321";
      var employer = BuildSimpleEmployer(1);
      employer.Fax = fax;

			var response = _employerCommandBuilder.SaveEmployer(employer, "1", null, null, null, null);

      var responseXml = XDocument.Parse(response);
      var employerElement = responseXml.Descendants("employer").SingleOrDefault();

      employerElement.Element("fax_pri").Value.ShouldEqual("5557654321");
    }


    [Test]
    public void When_SaveEmployer_IsPassedEmail_ItIsIncludedInXmlRequest()
    {
      const string email = "test@emails.comx";
      var employer = BuildSimpleEmployer(1);
      employer.Email = email;

			var response = _employerCommandBuilder.SaveEmployer(employer, "1", null, null, null, null);

      var responseXml = XDocument.Parse(response);
      var employerElement = responseXml.Descendants("employer").SingleOrDefault();

      employerElement.Element("email").Value.ShouldEqual(email);
    }

    [Test]
    public void When_SaveEmployer_IsPassedURL_ItIsIncludedInXmlRequest()
    {
      const string url = "http://www.thisisatesturlforthebenefitsofthistestmethod.net";
      var employer = BuildSimpleEmployer(1);
      employer.Url = url;

			var response = _employerCommandBuilder.SaveEmployer(employer, "1", null, null, null, null);

      var responseXml = XDocument.Parse(response);
      var employerElement = responseXml.Descendants("employer").SingleOrDefault();

      employerElement.Element("url_pri").Value.ShouldEqual(url);
    }

    [Test]
    public void When_SaveEmployer_IsPassedNAICS_ItIsIncludedInXmlRequest()
    {
      const string naics = "213456";
      var employer = BuildSimpleEmployer(1);
      employer.Naics = naics;

			var response = _employerCommandBuilder.SaveEmployer(employer, "1", null, null, null, null);

      var responseXml = XDocument.Parse(response);
      var employerElement = responseXml.Descendants("employer").SingleOrDefault();

      employerElement.Element("naics").Value.ShouldEqual(naics);
    }

    [Test]
    public void When_SaveEmployer_IsPassedOwner_ItIsIncludedInXmlRequest()
    {
      const int owner = 1;
      var employer = BuildSimpleEmployer(1);
      employer.Owner = owner;

			var response = _employerCommandBuilder.SaveEmployer(employer, "1", null, null, null, null);

      var responseXml = XDocument.Parse(response);
      var employerElement = responseXml.Descendants("employer").SingleOrDefault();

      employerElement.Element("owner").Value.ShouldEqual("external owner 1");
    }

    [Test]
    public void When_SaveEmployer_IsPassedPublicTransportAccessibleFlag_ItIsIncludedInXmlRequest()
    {
      const bool publicTransportAccessible = true;
      var employer = BuildSimpleEmployer(1);
      employer.PublicTransportAccessible = publicTransportAccessible;

			var response = _employerCommandBuilder.SaveEmployer(employer, "1", null, null, null, null);

      var responseXml = XDocument.Parse(response);
      var employerElement = responseXml.Descendants("employer").SingleOrDefault();

      employerElement.Element("public_transport_flag").Value.ShouldEqual("-1");
    }

    [Test]
    public void When_SaveEmployer_IsPassedAddress_ItIsIncludedInXmlRequest()
    {
      const bool publicTransportAccessible = true;
      var employer = BuildSimpleEmployer(1);
      employer.PublicTransportAccessible = publicTransportAccessible;

			var response = _employerCommandBuilder.SaveEmployer(employer, "1", null, null, null, null);

      var doc = new XmlDocument();
      doc.LoadXml(response);

      // Use the XPath as there could be an address element in the employee node
      doc["osos"]["request"]["employers"]["employer"]["address"].ShouldNotBeNull();
    }

    [Test]
    public void When_SaveJob_IsPassedAnAddressWithFirstLine_ItIsIncludedInXmlRequest()
    {
      var employer = BuildSimpleEmployer(1);
      const string addressLine1 = "123 Address line 1 Road";
      employer.Address.AddressLine1 = addressLine1;

			var response = _employerCommandBuilder.SaveEmployer(employer, "1", null, null, null, null);

      var doc = new XmlDocument();
      doc.LoadXml(response);

      doc["osos"]["request"]["employers"]["employer"]["address"]["addr_1"].InnerText.ShouldEqual(addressLine1);
    }

    [Test]
    public void When_SaveJob_IsPassedAnAddressWithSecondLine_ItIsIncludedInXmlRequest()
    {
      var employer = BuildSimpleEmployer(1);
      const string addressLine2 = "123 Address line 2 Road";
      employer.Address.AddressLine2 = addressLine2;

			var response = _employerCommandBuilder.SaveEmployer(employer, "1", null, null, null, null);

      var doc = new XmlDocument();
      doc.LoadXml(response);

      doc["osos"]["request"]["employers"]["employer"]["address"]["addr_2"].InnerText.ShouldEqual(addressLine2);
    }

    [Test]
    public void When_SaveJob_IsPassedAnAddressWithCity_ItIsIncludedInXmlRequest()
    {
      var employer = BuildSimpleEmployer(1);
      const string city = "test function city";
      employer.Address.City = city;

			var response = _employerCommandBuilder.SaveEmployer(employer, "1", null, null, null, null);

      var doc = new XmlDocument();
      doc.LoadXml(response);

      doc["osos"]["request"]["employers"]["employer"]["address"]["city"].InnerText.ShouldEqual(city);
    }

    [Test]
    public void When_SaveJob_IsPassedAnAddressWithPostCode_ItIsIncludedInXmlRequest()
    {
      var employer = BuildSimpleEmployer(1);
      const string zip = "111471";
      employer.Address.PostCode = zip;

			var response = _employerCommandBuilder.SaveEmployer(employer, "1", null, null, null, null);

      var doc = new XmlDocument();
      doc.LoadXml(response);

      doc["osos"]["request"]["employers"]["employer"]["address"]["zip"].InnerText.ShouldEqual("11147");
    }

    [Test]
    public void When_SaveJob_IsPassedAnAddressWithCounty_ItIsIncludedInXmlRequest()
    {
      var employer = BuildSimpleEmployer(1);

			var response = _employerCommandBuilder.SaveEmployer(employer, "1", null, null, null, null);

      var doc = new XmlDocument();
      doc.LoadXml(response);

      doc["osos"]["request"]["employers"]["employer"]["address"]["county"].InnerText.ShouldEqual("externalCountyId");
    }

    [Test]
    public void When_SaveJob_IsPassedAnAddressWithState_ItIsIncludedInXmlRequest()
    {
      var employer = BuildSimpleEmployer(1);

			var response = _employerCommandBuilder.SaveEmployer(employer, "1", null, null, null, null);

      var doc = new XmlDocument();
      doc.LoadXml(response);

      doc["osos"]["request"]["employers"]["employer"]["address"]["state"].InnerText.ShouldEqual("externalStateId");
    }

    [Test]
    public void When_SaveJob_IsPassedAnAddressWithCountryOutsideUSSateIsDefaulted_ItIsIncludedInXmlRequest()
    {
      var employer = BuildSimpleEmployer(1);
      const string state = "WI";
      employer.Address.ExternalStateId = state;
      employer.Address.CountryId = 1;

			var response = _employerCommandBuilder.SaveEmployer(employer, "1", null, null, null, null);

      var doc = new XmlDocument();
      doc.LoadXml(response);

      doc["osos"]["request"]["employers"]["employer"]["address"]["state"].InnerText.ShouldEqual("ZZ");
    }



    [Test]
    public void When_SaveJob_IsPassedAnAddressWithCountry_ItIsIncludedInXmlRequest()
    {
      var employer = BuildSimpleEmployer(1);
      const string country = "UK";
      employer.Address.CountryId = 1;

			var response = _employerCommandBuilder.SaveEmployer(employer, "1", null, null, null, null);

      var doc = new XmlDocument();
      doc.LoadXml(response);

      doc["osos"]["request"]["employers"]["employer"]["address"]["country"].InnerText.ShouldEqual(country);
    }


    #endregion

    #region Save employee

    [Test]
    public void When_SaveEmployee_IsPassedEmployer_AValidXmlString_IsReturned()
    {
			var response = _employerCommandBuilder.SaveEmployee(BuildEmployee(1, 2), "1", null, null, null, null);

      response.ShouldBeValidXml();
    }

    [Test]
    public void When_SaveEmployer_IsPassedValidEmployee_ASingleEmployersElementIsReturned()
    {
			var response = _employerCommandBuilder.SaveEmployee(BuildEmployee(1, 2), "1", null, null, null, null);

      var responseXml = XDocument.Parse(response);

      responseXml.Descendants("employers").Count().ShouldEqual(1);

      var doc = new XmlDocument();
      doc.LoadXml(response);

      doc["osos"]["request"]["employers"].ShouldNotBeNull();
    }

    [Test]
    public void When_SaveEmployee_IsPassedValidEmployee_ASingleEmployerElementIsReturned()
    {
			var response = _employerCommandBuilder.SaveEmployee(BuildEmployee(1, 2), "1", null, null, null, null);

      var responseXml = XDocument.Parse(response);

      responseXml.Descendants("employer").Count().ShouldEqual(1);

      var doc = new XmlDocument();
      doc.LoadXml(response);

      doc["osos"]["request"]["employers"]["employer"].ShouldNotBeNull();
    }

    [Test]
    public void When_SaveEmployee_IsPassedValidEmployee_ASingleContactsElementIsReturned()
    {
			var response = _employerCommandBuilder.SaveEmployee(BuildEmployee(1, 2), "1", null, null, null, null);

      var responseXml = XDocument.Parse(response);

      responseXml.Descendants("contacts").Count().ShouldEqual(1);

      var doc = new XmlDocument();
      doc.LoadXml(response);

      doc["osos"]["request"]["employers"]["employer"]["contacts"].ShouldNotBeNull();
    }

    [Test]
    public void When_SaveEmployee_IsPassedValidEmployee_ASingleContactElementIsReturned()
    {
			var response = _employerCommandBuilder.SaveEmployee(BuildEmployee(1, 2), "1", null, null, null, null);

      var responseXml = XDocument.Parse(response);

      responseXml.Descendants("contact").Count().ShouldEqual(1);

      var doc = new XmlDocument();
      doc.LoadXml(response);

      doc["osos"]["request"]["employers"]["employer"]["contacts"]["contact"].ShouldNotBeNull();
    }

    [Test]
    public void When_SaveEmployee_IsPassedNoExternalEmployeeId_AnInsertCommand_IsReturned()
    {
      var employee = BuildEmployee(1, 2);
      employee.ExternalId = null;

			var response = _employerCommandBuilder.SaveEmployee(employee, "1", null, null, null, null);

      var responseXml = XDocument.Parse(response);
      var employerElement = responseXml.Descendants("contact").SingleOrDefault();
      employerElement.Attribute("method").Value.ShouldEqual("insert");
    }

    [Test]
    public void When_SaveEmployee_IsPassedAnExternalEmployeeId_AnUpdateCommand_IsReturned()
    {
			var response = _employerCommandBuilder.SaveEmployee(BuildEmployee(1, 2), "1", null, null, null, null);

      var responseXml = XDocument.Parse(response);
      var employerElement = responseXml.Descendants("contact").SingleOrDefault();
      employerElement.Attribute("method").Value.ShouldEqual("update");
    }

    [Test]
    public void When_SaveEmployee_IsPassedFirstName_ItIsIncludedInXmlRequest()
    {
      const string employeeName = "This is the employee name";
      var employee = BuildEmployee(1, 2);
      employee.FirstName = employeeName;

			var response = _employerCommandBuilder.SaveEmployee(employee, "1", null, null, null, null);

      var responseXml = XDocument.Parse(response);
      var employerElement = responseXml.Descendants("contact").SingleOrDefault();

      employerElement.Element("first_name").Value.ShouldEqual(employeeName.Substring(0,20));
    }

    [Test]
    public void When_SaveEmployee_IsPassedLastName_ItIsIncludedInXmlRequest()
    {
      const string employeeName = "This is the employee last name";
      var employee = BuildEmployee(1, 2);
      employee.LastName = employeeName;

			var response = _employerCommandBuilder.SaveEmployee(employee, "1", null, null, null, null);

      var responseXml = XDocument.Parse(response);
      var employerElement = responseXml.Descendants("contact").SingleOrDefault();

      employerElement.Element("last_name").Value.ShouldEqual(employeeName.Substring(0, 20));
    }

    [Test]
    public void When_SaveEmployee_IsPassedSalutation_ItIsIncludedInXmlRequest()
    {
      const string externalSalutationId = "external slautation 1";
      var employee = BuildEmployee(1, 2);
      employee.SalutationId = 1;

			var response = _employerCommandBuilder.SaveEmployee(employee, "1", null, null, null, null);

      var responseXml = XDocument.Parse(response);
      var employerElement = responseXml.Descendants("contact").SingleOrDefault();

      employerElement.Element("salutation_cd").Value.ShouldEqual(externalSalutationId);
    }

    [Test]
    public void When_SaveEmployee_IsPassedPrimaryPhone_ItIsIncludedInXmlRequest()
    {
      const string phone = "555-555556";
      var employee = BuildEmployee(1, 2);
      employee.Phone = phone;

			var response = _employerCommandBuilder.SaveEmployee(employee, "1", null, null, null, null);

      var responseXml = XDocument.Parse(response);
      var employerElement = responseXml.Descendants("contact").SingleOrDefault();

      employerElement.Element("phone_pri").Value.ShouldEqual("555555556");
    }

    [Test]
    public void When_SaveEmployee_IsPassedPhoneExtension_ItIsIncludedInXmlRequest()
    {
      const string extension = "5557";
      var employee = BuildEmployee(1, 2);
      employee.PhoneExt = extension;

			var response = _employerCommandBuilder.SaveEmployee(employee, "1", null, null, null, null);

      var responseXml = XDocument.Parse(response);
      var employerElement = responseXml.Descendants("contact").SingleOrDefault();

      employerElement.Element("phone_pri_ext").Value.ShouldEqual(extension);
    }

    [Test]
    public void When_SaveEmployee_IsPassedSecondaryPhone_ItIsIncludedInXmlRequest()
    {
      const string phone = "555-1234567";
      var employee = BuildEmployee(1, 2);
      employee.AltPhone = phone;

			var response = _employerCommandBuilder.SaveEmployee(employee, "1", null, null, null, null);

      var responseXml = XDocument.Parse(response);
      var employerElement = responseXml.Descendants("contact").SingleOrDefault();

      employerElement.Element("phone_sec").Value.ShouldEqual("5551234567");
    }

    [Test]
    public void When_SaveEmployee_IsPassedSecondaryPhoneExtension_ItIsIncludedInXmlRequest()
    {
      const string extension = "5896";
      var employee = BuildEmployee(1, 2);
      employee.AltPhoneExt = extension;

			var response = _employerCommandBuilder.SaveEmployee(employee, "1", null, null, null, null);

      var responseXml = XDocument.Parse(response);
      var employerElement = responseXml.Descendants("contact").SingleOrDefault();

      employerElement.Element("phone_sec_ext").Value.ShouldEqual(extension);
    }

    [Test]
    public void When_SaveEmployee_IsPassedFax_ItIsIncludedInXmlRequest()
    {
      const string fax = "555-8899999";
      var employee = BuildEmployee(1, 2);
      employee.Fax = fax;

			var response = _employerCommandBuilder.SaveEmployee(employee, "1", null, null, null, null);

      var responseXml = XDocument.Parse(response);
      var employerElement = responseXml.Descendants("contact").SingleOrDefault();

      employerElement.Element("fax_pri").Value.ShouldEqual("5558899999");
    }

    [Test]
    public void When_SaveEmployee_IsPassedEmail_ItIsIncludedInXmlRequest()
    {
      const string email = "testfunction@email.comx";
      var employee = BuildEmployee(1, 2);
      employee.Email = email;

			var response = _employerCommandBuilder.SaveEmployee(employee, "1", null, null, null, null);

      var responseXml = XDocument.Parse(response);
      var employerElement = responseXml.Descendants("contact").SingleOrDefault();

      employerElement.Element("email").Value.ShouldEqual(email);
    }

    [Test]
    public void When_SaveEmployee_IsPassedAdmin_ItIsIncludedInXmlRequest()
    {
      const string adminId = "admin's external Id";
      var employee = BuildEmployee(1, 2);
      employee.ExternalAdminId = adminId;

			var response = _employerCommandBuilder.SaveEmployee(employee, "1", null, null, null, null);

      var responseXml = XDocument.Parse(response);
      var employerElement = responseXml.Descendants("contact").SingleOrDefault();

      employerElement.Element("admin_id").Value.ShouldEqual(adminId);
    }

    [Test]
    public void When_SaveEmployee_IsPassedAdress_ItIsIncludedInXmlRequest()
    {
      var employee = BuildEmployee(1, 2);

			var response = _employerCommandBuilder.SaveEmployee(employee, "1", null, null, null, null);

      var responseXml = XDocument.Parse(response);
      responseXml.Descendants("contact").Descendants("address").Count().ShouldEqual(1);
    }

    [Test]
    public void When_SaveEmployee_IsPassedAddressLine1_ItIsIncludedInXmlRequest()
    {
      const string addressLine1 = "test address line 1";
      var employee = BuildEmployee(1, 2);
      employee.Address.AddressLine1 = addressLine1;

			var response = _employerCommandBuilder.SaveEmployee(employee, "1", null, null, null, null);

      var responseXml = XDocument.Parse(response);
      var addressElement = responseXml.Descendants("contact").Descendants("address").SingleOrDefault();

      addressElement.Element("addr_1").Value.ShouldEqual(addressLine1);
    }

    [Test]
    public void When_SaveEmployee_IsPassedAddressLine2_ItIsIncludedInXmlRequest()
    {
      const string addressLine2 = "test address line 2";
      var employee = BuildEmployee(1, 2);
      employee.Address.AddressLine2 = addressLine2;

			var response = _employerCommandBuilder.SaveEmployee(employee, "1", null, null, null, null);

      var responseXml = XDocument.Parse(response);
      var addressElement = responseXml.Descendants("contact").Descendants("address").SingleOrDefault();

      addressElement.Element("addr_2").Value.ShouldEqual(addressLine2);
    }

    [Test]
    public void When_SaveEmployee_IsPassedCity_ItIsIncludedInXmlRequest()
    {
      const string city = "test address city";
      var employee = BuildEmployee(1, 2);
      employee.Address.City = city;

			var response = _employerCommandBuilder.SaveEmployee(employee, "1", null, null, null, null);

      var responseXml = XDocument.Parse(response);
      var addressElement = responseXml.Descendants("contact").Descendants("address").SingleOrDefault();

      addressElement.Element("city").Value.ShouldEqual(city);
    }

    [Test]
    public void When_SaveEmployee_IsPassedZip_ItIsIncludedInXmlRequest()
    {
      const string zip = "test address zip";
      var employee = BuildEmployee(1, 2);
      employee.Address.PostCode = zip;

			var response = _employerCommandBuilder.SaveEmployee(employee, "1", null, null, null, null);

      var responseXml = XDocument.Parse(response);
      var addressElement = responseXml.Descendants("contact").Descendants("address").SingleOrDefault();

			addressElement.Element("zip").Value.ShouldEqual(Regex.Replace(zip, "[^0-9]", string.Empty).Substring(0, Math.Min(5, Regex.Replace(zip, "[^0-9]", string.Empty).Length)));
    }

		[Test]
		public void When_SaveEmployee_IsPassed5DigitZip_ItIsIncludedInXmlRequest()
		{
			const string zip = "90210";
			var employee = BuildEmployee(1, 2);
			employee.Address.PostCode = zip;
			var response = _employerCommandBuilder.SaveEmployee(employee, "1", null, null, null, null);
			var responseXml = XDocument.Parse(response);
			var addressElement = responseXml.Descendants("contact").Descendants("address").SingleOrDefault();
			addressElement.Element("zip").Value.ShouldEqual(zip);
		}

		[Test]
		public void When_SaveEmployee_IsPassed9DigitZip_ItIsIncludedInXmlRequest()
		{
			const string zip = "902101234";
			var employee = BuildEmployee(1, 2);
			employee.Address.PostCode = zip;
			var response = _employerCommandBuilder.SaveEmployee(employee, "1", null, null, null, null);
			var responseXml = XDocument.Parse(response);
			var addressElement = responseXml.Descendants("contact").Descendants("address").SingleOrDefault();
			addressElement.Element("zip").Value.ShouldEqual("90210");
		}

    [Test]
    public void When_SaveEmployee_IsPassedCounty_ItIsNotIncludedInXmlRequest()
    {
      var employee = BuildEmployee(1, 2);

			var response = _employerCommandBuilder.SaveEmployee(employee, "1", null, null, null, null);

      var responseXml = XDocument.Parse(response);
      var addressElement = responseXml.Descendants("contact").Descendants("address").SingleOrDefault();

      addressElement.Element("county").ShouldBeNull();
    }

    [Test]
    public void When_SaveEmployee_IsPassedState_ItIsIncludedInXmlRequest()
    {
      var employee = BuildEmployee(1, 2);

			var response = _employerCommandBuilder.SaveEmployee(employee, "1", null, null, null, null);

      var responseXml = XDocument.Parse(response);
      var addressElement = responseXml.Descendants("contact").Descendants("address").SingleOrDefault();

      addressElement.Element("state").Value.ShouldEqual("externalStateId");
    }

    [Test]
    public void When_SaveEmployee_IsPassedCountryOutsideOfUsStateIsDefaulted_ItIsIncludedInXmlRequest()
    {
      const string state = "WI";
      //const string country = "UK";
      var employee = BuildEmployee(1, 2);
      employee.Address.ExternalStateId = state;
      employee.Address.CountryId = 1;

			var response = _employerCommandBuilder.SaveEmployee(employee, "1", null, null, null, null);

      var responseXml = XDocument.Parse(response);
      var addressElement = responseXml.Descendants("contact").Descendants("address").SingleOrDefault();

      addressElement.Element("state").Value.ShouldEqual("ZZ");
    }

    [Test]
    public void When_SaveEmployee_IsPassedStateCountry_ItIsIncludedInXmlRequest()
    {
      const string country = "UK";
      var employee = BuildEmployee(1, 2);
      employee.Address.CountryId = 1;

			var response = _employerCommandBuilder.SaveEmployee(employee, "1", null, null, null, null);

      var responseXml = XDocument.Parse(response);
      var addressElement = responseXml.Descendants("contact").Descendants("address").SingleOrDefault();

      addressElement.Element("country").Value.ShouldEqual(country);
    }

    #endregion


    #region Pre-prepared model

    private EmployerModel BuildSimpleEmployer(int employeeCount)
    {
      return new EmployerModel
        {
          Id = 1,
          ExternalId = "Employer External Id",
          AdminId = 2,
          ExternalAdminId = "External Admin Id",
          Status = EmployerStatusTypes.Active,
          Name = "Employer name",
          LegalName = "Employer Legal Name",
          FederalEmployerIdentificationNumber = "12-3456789",
          IsValidFederalEmployerIdentificationNumber = true,
          StateEmployerIdentificationNumber = "123",
          IsRegistrationComplete = true,
          OfficeId = 3,
          ExternalOfficeId = "Employer External Office Id",
          Phone = "555-5555",
          PhoneExt = "1234",
          AltPhone = "555-5556",
          AltPhoneExt = "4321",
          Fax = "555-5557",
          Email = "email@email.comx",
          Url = "http://www.myurl.comx",
          Naics = "423120",
          Owner = 1,
          BusinessDescription = "This is the description for theis test business, nice isn't it?",
          BusinessInterests = "Some testing business interests",
          Employees = BuildEmployeeList(employeeCount, 1),
          Address = new AddressModel
                      {
                        AddressLine1 = "main street",
                        AddressLine2 = "apt. 106",
                        AddressLine3 = "",
                        City = "SYR",
                        CountryId = 789,
                        CountyId = 123,
                        ExternalCountryId = "US",
                        ExternalCountyId = "36067",
                        ExternalStateId = "NY",
                        PostCode = "13201",
                        StateId = 456,
                        TrackingId = ""
                      },
          Version = "1",
          PublicTransportAccessible = true,
          Activities = null
        };
    }

   
    private EmployeeModel BuildEmployee(int employeeIndex, long employerId)
    {
      return new EmployeeModel
               {
                 Id = employeeIndex,
                 ExternalId = "External EmployeeId " + employeeIndex,
                 EmployerId = employerId,
                 AdminId = employeeIndex + 2,
                 ExternalAdminId = "External AdminId " + employeeIndex,
                 FirstName = "Firstname " + employeeIndex,
                 LastName = "Lastname " + employeeIndex,
                 SalutationId = employeeIndex + 3,
                 JobTitle = "Job Title " + employeeIndex,
                 Phone = "555-5555" + employeeIndex,
                 PhoneExt = "1234" + employeeIndex,
                 AltPhone = "555-5556" + employeeIndex,
                 AltPhoneExt = "4321" + employeeIndex,
                 Fax = "987654" + employeeIndex,
                 Email = "employee@employee.com" + employeeIndex,
                 Responsibilities = "Employee #" + employeeIndex + " has many responsibilities",
                 Comments = "There are many comments about employee #" + employeeIndex,
                 Version = "1",
                 Address = new AddressModel
                 {
                   AddressLine1 = "main street" + employeeIndex,
                   AddressLine2 = "apt. 106" + employeeIndex,
                   AddressLine3 = "",
                   City = "SYR",
                   CountryId = 789,
                   CountyId = 123,
                   ExternalCountryId = "US",
                   ExternalCountyId = "36067" + employeeIndex,
                   ExternalStateId = "NY",
                   PostCode = "13201",
                   StateId = 456,
                   TrackingId = ""
                 },

               };
    }

    private List<EmployeeModel> BuildEmployeeList(int employeeCount, long employerId)
    {
      if (employeeCount < 1) return null;
      var employees = new List<EmployeeModel>();

      for (var i = 1; i <= employeeCount; i++)
      {
        employees.Add(BuildEmployee(i, employerId));
      }

      return employees;
    }

    #endregion

  }
}
