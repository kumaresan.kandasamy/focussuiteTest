﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Linq;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Models.Career;
using Focus.Services.Integration.Implementations.AOSOS.CommandBuilders;
using Focus.Services.Integration.Implementations.AOSOS.ResponseTranslators;
using NUnit.Framework;

using Focus.Core;
using Focus.Core.Models.Integration;
using Focus.Services.Integration.Implementations.AOSOS;
using Framework.Testing.NUnit;

#endregion

namespace Focus.UnitTests.Integration.Providers.AOSOS
{
  [TestFixture]
  public class AOSOSJobSeekerCommandBuilderTests
  {
    private ClientSettings _clientSettings;
    private JobSeekerCommandBuilder _jobSeekerCommandBuilder;
    private Translator _translator;
    private const string xMLBreaker = "& < > \" '";


    [SetUp]
    public void SetUp()
    {
      _clientSettings = new ClientSettings
                          {
                            DefaultSecurityName = "TestSecurityName",
                            DefaultPassword = "TestPassword",
                            OriginationMethod = "8",
                            DefaultOfficeId = "1000",
                            DefaultAdminId = "TestDefaultAdminId"
                          };

      var externalIds = new List<ExternalLookUpItemDto>();
      externalIds.Add(new ExternalLookUpItemDto { ExternalId = "externalGenderId", ExternalLookUpType = ExternalLookUpType.Gender, InternalId = Genders.Male.ToString() });
      externalIds.Add(new ExternalLookUpItemDto { ExternalId = "externalDisibilityId", ExternalLookUpType = ExternalLookUpType.DisabilityStatus, InternalId = DisabilityStatus.NotDisclosed.ToString() });
      externalIds.Add(new ExternalLookUpItemDto { ExternalId = "externalCountyId", ExternalLookUpType = ExternalLookUpType.County, InternalId = "123" });
      externalIds.Add(new ExternalLookUpItemDto { ExternalId = "externalStateId", ExternalLookUpType = ExternalLookUpType.State, InternalId = "456" });
      externalIds.Add(new ExternalLookUpItemDto { ExternalId = "US", ExternalLookUpType = ExternalLookUpType.Country, InternalId = "789" });
      externalIds.Add(new ExternalLookUpItemDto { ExternalId = "external TransitioningServiceMember", ExternalLookUpType = ExternalLookUpType.VeteranEra, InternalId = VeteranEra.TransitioningServiceMember.ToString() });
      externalIds.Add(new ExternalLookUpItemDto { ExternalId = "external SpecialDisabled", ExternalLookUpType = ExternalLookUpType.VeteranDisabilityStatus, InternalId = VeteranDisabilityStatus.SpecialDisabled.ToString() });
      externalIds.Add(new ExternalLookUpItemDto { ExternalId = "external Grade_12_No_Diploma_12", ExternalLookUpType = ExternalLookUpType.EducationLevel, InternalId = EducationLevel.Grade_12_No_Diploma_12.ToString() });

      _jobSeekerCommandBuilder = new JobSeekerCommandBuilder(_clientSettings, externalIds);
      _translator = new Translator(externalIds);
    }

    #region Get Job Seeker By SSN

    [Test]
    public void When_GetJobSeekerbBySSN_PassedValidSSN_AValidXmlString_IsReturned()
    {
      var response = _jobSeekerCommandBuilder.GetSeekerBySocialSecurityNumber(xMLBreaker);

      response.ShouldBeValidXml();
    }

    [Test]
    public void When_GetJobSeekerbBySSN_PassedValidSSN_ACorrectFormatRequest_IsReturned()
    {
      var ssn = Guid.NewGuid();
      var expectedResponse = string.Format("<osos><request security_username='{0}' security_password='{1}' current_office_id='{2}' ct_aggr_version='{3}' admin_id='{5}'><seeker_quicksearch method='query' returns='username,seeker_status_cd'><ssn_1>{4}</ssn_1></seeker_quicksearch></request></osos>",
                                           _clientSettings.DefaultSecurityName, _clientSettings.DefaultPassword, _clientSettings.DefaultOfficeId, _clientSettings.CtAggrVersion, ssn, _clientSettings.DefaultAdminId);

      var response = _jobSeekerCommandBuilder.GetSeekerBySocialSecurityNumber(ssn.ToString());

      response.ShouldEqual(expectedResponse);
    }

    #endregion

    #region Get Job Seeker By username

    [Test]
    public void When_GetJobSeekerbByUsername_PassedValidUsername_AValidXmlString_IsReturned()
    {
      var response = _jobSeekerCommandBuilder.GetSeekerByUsername(xMLBreaker);

      response.ShouldBeValidXml();
    }

    [Test]
    public void When_GetJobSeekerbByUsername_PassedValidUsername_ACorrectFormatRequest_IsReturned()
    {
      var username = Guid.NewGuid();
      var expectedResponse = string.Format("<osos><request security_username='{0}' security_password='{1}' current_office_id='{2}' ct_aggr_version='{3}' admin_id='{5}'><seeker_quicksearch method='query' returns='username,seeker_status_cd,first_name,last_name,ssn,full_ssn'><username_1>{4}</username_1></seeker_quicksearch></request></osos>",
                                           _clientSettings.DefaultSecurityName, _clientSettings.DefaultPassword, _clientSettings.DefaultOfficeId, _clientSettings.CtAggrVersion, username, _clientSettings.DefaultAdminId);

      var response = _jobSeekerCommandBuilder.GetSeekerByUsername(username.ToString());

      response.ShouldEqual(expectedResponse);
    }

    #endregion

    #region Get Job Seeker By Id

    [Test]
    public void When_GetJobSeekerbById_PassedValidId_AValidXmlString_IsReturned()
    {
      var response = _jobSeekerCommandBuilder.GetSeekerById(xMLBreaker);

      response.ShouldBeValidXml();
    }

    [Test]
    public void When_GetJobSeekerbById_PassedValidId_ACorrectFormatRequest_IsReturned()
    {
      var externalSeekerId = Guid.NewGuid();
			var expectedResponse = string.Format("<osos><request security_username='{0}' security_password='{1}' current_office_id='{2}' ct_aggr_version='{3}' admin_id='{5}'><seekers><seeker method='select' expand='ssn,full_ssn,desired_countries,desired_zips,ethnic_heritages,schools,desired_titles,title_skills,vet_pref,certifications,desired_states,previous_jobs' id='{4}'></seeker></seekers></request></osos>",
                                           _clientSettings.DefaultSecurityName, _clientSettings.DefaultPassword, _clientSettings.DefaultOfficeId, _clientSettings.CtAggrVersion, externalSeekerId, _clientSettings.DefaultAdminId);

      var response = _jobSeekerCommandBuilder.GetSeekerById(externalSeekerId.ToString());

      response.ShouldEqual(expectedResponse);
    }

    #endregion

    #region Save job seeker matches

    [Test]
    public void When_SaveJobMatches_PassedValidIds_AValidXmlString_IsReturned()
    {
      var jobIds = new List<string>
                     {
                       "123",
                       "456",
                       "78910"
                     };
      var response = _jobSeekerCommandBuilder.SaveJobMatches(xMLBreaker, jobIds, string.Empty, string.Empty, string.Empty, string.Empty);

      response.ShouldBeValidXml();
    }

    [Test]
    public void When_SaveJobMatches_PassedValidIds_ACorrectFormatRequest_IsReturned()
    {
      var externalSeekerId = Guid.NewGuid();
      var jobIds = new List<string>
                     {
                       "123",
                       "456",
                       "78910"
                     };
      var expectedResponse = string.Format("<osos><request security_username='{0}' security_password='{1}' current_office_id='{2}' ct_aggr_version='{3}' admin_id='{8}'><seeker_match id='{4}' method='match'><jobs><job id='{5}'/><job id='{6}'/><job id='{7}'/></jobs></seeker_match></request></osos>",
                                           _clientSettings.DefaultSecurityName, _clientSettings.DefaultPassword, _clientSettings.DefaultOfficeId, _clientSettings.CtAggrVersion, externalSeekerId, jobIds[0], jobIds[1], jobIds[2], _clientSettings.DefaultAdminId);


			var response = _jobSeekerCommandBuilder.SaveJobMatches(externalSeekerId.ToString(), jobIds, string.Empty, string.Empty, string.Empty, string.Empty);

      response.ShouldEqual(expectedResponse);
    }

    #endregion

    #region Save Activity

    [Test]
    public void When_AssignJobSeekerActivity_PassedValidIds_AValidXmlString_IsReturned()
    {
			var response = _jobSeekerCommandBuilder.SaveActivity(xMLBreaker, xMLBreaker, xMLBreaker, xMLBreaker, xMLBreaker, string.Empty, string.Empty, string.Empty, string.Empty);
      response.ShouldBeValidXml();
    }

    [Test]
    public void When_AssignJobSeekerActivity_IsPassedNoAdminId_ACorrectFormatRequest_IsReturned()
    {
      var uuid = Guid.NewGuid();
      const string jobSeekerId = "Job Seeker Id";
      const string activityId = "Test Activity ID";
      const string officeId = "Test Office ID";
      const string version = "1";
      var now = DateTime.Now;

      var expectedResponse = string.Format("<osos><request security_username='{0}' security_password='{1}' current_office_id='{2}' ct_aggr_version='{3}' admin_id='{9}'><seekers><seeker method='update' id='{4}' version='{5}' uuid='{6}'><activities><activity uuid='{6}' method='insert'><ctime>{7}</ctime><seeker_service_type_cd>{8}</seeker_service_type_cd></activity></activities></seeker></seekers></request></osos>",
        _clientSettings.DefaultSecurityName, _clientSettings.DefaultPassword, _clientSettings.DefaultOfficeId, _clientSettings.CtAggrVersion, jobSeekerId, version, uuid, now.ToString("MMddyyyy"), activityId, _clientSettings.DefaultAdminId);

			var response = _jobSeekerCommandBuilder.SaveActivity(jobSeekerId, version, activityId, null, officeId, string.Empty, string.Empty, string.Empty, string.Empty, uuid, now);

      response.ShouldEqual(expectedResponse);
    }

    [Test]
    public void When_AssignJobSeekerActivity_IsPassedAdminIdAndNoOfficeId_ACorrectFormatRequest_IsReturned()
    {
      var uuid = Guid.NewGuid();
      const string jobSeekerId = "Job Seeker Id";
      const string activityId = "Test Activity ID";
      //const string officeId = "Test Office ID";
      const string adminId = "Test Admin ID";
      const string version = "1";
      var now = DateTime.Now;

      var expectedResponse = string.Format("<osos><request security_username='{0}' security_password='{1}' current_office_id='{2}' ct_aggr_version='{3}' admin_id='{10}'><seekers><seeker method='update' id='{4}' version='{5}' uuid='{6}'><activities><activity uuid='{6}' method='insert'><ctime>{7}</ctime><seeker_service_type_cd>{8}</seeker_service_type_cd><admin_id>{9}</admin_id><office_id>{2}</office_id></activity></activities></seeker></seekers></request></osos>",
        _clientSettings.DefaultSecurityName, _clientSettings.DefaultPassword, _clientSettings.DefaultOfficeId, _clientSettings.CtAggrVersion, jobSeekerId, version, uuid, now.ToString("MMddyyyy"), activityId, adminId, _clientSettings.DefaultAdminId);

			var response = _jobSeekerCommandBuilder.SaveActivity(jobSeekerId, version, activityId, adminId, null, string.Empty, string.Empty, string.Empty, string.Empty, uuid, now);

      response.ShouldEqual(expectedResponse);
    }

    [Test]
    public void When_AssignJobSeekerActivity_IsPassedAdminIdAndOfficeId_ACorrectFormatRequest_IsReturned()
    {
      var uuid = Guid.NewGuid();
      const string jobSeekerId = "Job Seeker Id";
      const string activityId = "Test Activity ID";
      const string officeId = "Test Office ID";
      const string adminId = "Test Admin ID";
      const string version = "1";
      var now = DateTime.Now;

			var expectedResponse = string.Format("<osos><request security_username='{0}' security_password='{1}' current_office_id='{2}' ct_aggr_version='{3}' admin_id='{11}'><seekers><seeker method='update' id='{4}' version='{5}' uuid='{6}'><activities><activity uuid='{6}' method='insert'><ctime>{7}</ctime><seeker_service_type_cd>{8}</seeker_service_type_cd><admin_id>{9}</admin_id><office_id>{10}</office_id></activity></activities></seeker></seekers></request></osos>",
				 _clientSettings.DefaultSecurityName, _clientSettings.DefaultPassword, _clientSettings.DefaultOfficeId, _clientSettings.CtAggrVersion, jobSeekerId, version, uuid, now.ToString("MMddyyyy"), activityId, adminId, officeId, _clientSettings.DefaultAdminId);

			var response = _jobSeekerCommandBuilder.SaveActivity(jobSeekerId, version, activityId, adminId, officeId, string.Empty, string.Empty, string.Empty, string.Empty, uuid, now);

      response.ShouldEqual(expectedResponse);
    }

    
    #endregion

    #region Save job seeker

    [Test]
    public void When_SaveJobSeeker_IsPassedValidJobSeeker_WellFormattedXml_IsReturned()
    {
      var seekerModel = BuildModel();
			var response = _jobSeekerCommandBuilder.SaveJobSeeker(seekerModel, "1", false, null, string.Empty, string.Empty, string.Empty, string.Empty);

      response.ShouldBeValidXml();
    }

    [Test]
    public void When_SaveJobSeeker_IsPassedValidJobSeeker_ASingleRequestElementIsReturned()
    {
      var seekerModel = BuildModel();
			var response = _jobSeekerCommandBuilder.SaveJobSeeker(seekerModel, "1", false, null, string.Empty, string.Empty, string.Empty, string.Empty);

      var responseXml = XDocument.Parse(response);

      responseXml.Descendants("request").Count().ShouldEqual(1);
    }

    [Test]
    public void When_SaveJobSeeker_IsPassedValidJobSeeker_ASingleSeekersElementIsReturned()
    {
      var seekerModel = BuildModel();
			var response = _jobSeekerCommandBuilder.SaveJobSeeker(seekerModel, "1", false, null, string.Empty, string.Empty, string.Empty, string.Empty);

      var responseXml = XDocument.Parse(response);

      responseXml.Descendants("seekers").Count().ShouldEqual(1);
    }

    [Test]
    public void When_SaveJobSeeker_IsPassedValidJobSeeker_ASingleSeekerElementIsReturned()
    {
      var seekerModel = BuildModel();
			var response = _jobSeekerCommandBuilder.SaveJobSeeker(seekerModel, "1", false, null, string.Empty, string.Empty, string.Empty, string.Empty);

      var responseXml = XDocument.Parse(response);

      responseXml.Descendants("seekers").Descendants("seeker").Count().ShouldEqual(1);
      responseXml.Descendants("seeker").Count().ShouldEqual(1);
    }

    //[Test]
    //public void When_SaveJobSeeker_IsPassedJobSeekerWithoutExternalId_AnInsertCommandIsGenerated()
    //{
    //  var seekerModel = BuildModel();
    //  seekerModel.ExternalId = null;
    //  var response = _jobSeekerCommandBuilder.SaveJobSeeker(seekerModel, "1");

    //  var responseXml = XDocument.Parse(response);
    //  var jobSeekerElement = responseXml.Descendants("seeker").SingleOrDefault();
    //  jobSeekerElement.Attribute("method").Value.ShouldEqual("insert");
    //}

    #endregion

    #region Add job seeker

    [Test]
    public void When_AddJobSeeker_IsPassedValidJobSeeker_WellFormattedXml_IsReturned()
    {
      var seekerModel = BuildModel();
			var response = _jobSeekerCommandBuilder.AddJobSeeker(seekerModel, true, string.Empty, string.Empty, string.Empty, string.Empty);

      response.ShouldBeValidXml();
    }

    [Test]
    public void When_AddJobSeeker_IsPassedValidJobSeeker_ASingleRequestElementIsReturned()
    {
      var seekerModel = BuildModel();
			var response = _jobSeekerCommandBuilder.AddJobSeeker(seekerModel, true, string.Empty, string.Empty, string.Empty, string.Empty);

      var responseXml = XDocument.Parse(response);

      responseXml.Descendants("request").Count().ShouldEqual(1);
    }

    [Test]
    public void When_AddJobSeeker_IsPassedValidJobSeeker_ASingleSeekersElementIsReturned()
    {
      var seekerModel = BuildModel();
			var response = _jobSeekerCommandBuilder.AddJobSeeker(seekerModel, true, string.Empty, string.Empty, string.Empty, string.Empty);

      var responseXml = XDocument.Parse(response);

      responseXml.Descendants("seekers").Count().ShouldEqual(1);
    }

    [Test]
    public void When_AddJobSeeker_IsPassedValidJobSeeker_ASingleSeekerElementIsReturned()
    {
      var seekerModel = BuildModel();
			var response = _jobSeekerCommandBuilder.AddJobSeeker(seekerModel, true, string.Empty, string.Empty, string.Empty, string.Empty);

      var responseXml = XDocument.Parse(response);
      responseXml.Descendants("seekers").Descendants("seeker").Count().ShouldEqual(1);
      responseXml.Descendants("seeker").Count().ShouldEqual(1);
    }

    #endregion

    #region Seeker to Xml

    [Test]
    public void When_SeekerToXmlIsCalled_WithAValidFirstName_ItIsIncludedInXmlRequestAndTrimmedTo20Chars()
    {
      const string seekerFirstName = "This is the seeker first name";
      var seekerModel = BuildModel();
      seekerModel.Resume.ResumeContent.Profile.UserGivenName.FirstName = seekerFirstName;

			var response = _jobSeekerCommandBuilder.AddJobSeeker(seekerModel, true, string.Empty, string.Empty, string.Empty, string.Empty);
      var responseXml = XDocument.Parse(response);
      var seekerElement = responseXml.Descendants("seekers").Descendants("seeker").SingleOrDefault();

      seekerElement.Element("first_name").Value.ShouldEqual(seekerFirstName.Substring(0,20));
    }

    [Test]
    public void When_SeekerToXmlIsCalled_WithAValidLastName_ItIsIncludedInXmlRequestAndTrimmedTo20Chars()
    {
      const string seekerLastName = "This is the seeker last name";
      var seekerModel = BuildModel();
      seekerModel.Resume.ResumeContent.Profile.UserGivenName.LastName = seekerLastName;

			var response = _jobSeekerCommandBuilder.AddJobSeeker(seekerModel, true, string.Empty, string.Empty, string.Empty, string.Empty);
      var responseXml = XDocument.Parse(response);
      var seekerElement = responseXml.Descendants("seekers").Descendants("seeker").SingleOrDefault();

      seekerElement.Element("last_name").Value.ShouldEqual(seekerLastName.Substring(0, 20));
    }

    [Test]
    public void When_SeekerToXmlIsCalled_WithAValidMiddleName_ItIsIncludedInXmlRequest()
    {
      const string seekerMiddleName = "This is the seeker middle name";
      var seekerModel = BuildModel();
      seekerModel.Resume.ResumeContent.Profile.UserGivenName.MiddleName = seekerMiddleName;

			var response = _jobSeekerCommandBuilder.AddJobSeeker(seekerModel, true, string.Empty, string.Empty, string.Empty, string.Empty);
      var responseXml = XDocument.Parse(response);
      var seekerElement = responseXml.Descendants("seekers").Descendants("seeker").SingleOrDefault();

      seekerElement.Element("mi").Value.ShouldEqual("T");
    }

    [Test]
    public void When_SeekerToXmlIsCalled_WithAValidDOB_ItIsIncludedInXmlRequest()
    {
      var dob = new DateTime(1970, 5, 20);
      var seekerModel = BuildModel();
      seekerModel.Resume.ResumeContent.Profile.DOB = dob;

			var response = _jobSeekerCommandBuilder.AddJobSeeker(seekerModel, true, string.Empty, string.Empty, string.Empty, string.Empty);
      var responseXml = XDocument.Parse(response);
      var seekerElement = responseXml.Descendants("seekers").Descendants("seeker").SingleOrDefault();

      seekerElement.Element("dob").Value.ShouldEqual(dob.ToString("MMddyyyy"));
    }

    [Test]
    public void When_SeekerToXmlIsCalled_WithAValidGender_ItIsIncludedInXmlRequest()
    {
      var seekerModel = BuildModel();
      seekerModel.Resume.ResumeContent.Profile.Sex = Genders.Male;

			var response = _jobSeekerCommandBuilder.AddJobSeeker(seekerModel, true, string.Empty, string.Empty, string.Empty, string.Empty);
      var responseXml = XDocument.Parse(response);
      var seekerElement = responseXml.Descendants("seekers").Descendants("seeker").SingleOrDefault();

      seekerElement.Element("sex").Value.ShouldEqual("externalGenderId");
    }

    [Test]
    public void When_SeekerToXmlIsCalled_WithAValidSSN_ItIsIncludedInXmlRequest_AndSSNNotProvidedFlagSet()
    {
      const string ssn = "999888777";
      var seekerModel = BuildModel();
      seekerModel.SocialSecurityNumber = ssn;

			var response = _jobSeekerCommandBuilder.AddJobSeeker(seekerModel, true, string.Empty, string.Empty, string.Empty, string.Empty);
      var responseXml = XDocument.Parse(response);
      var seekerElement = responseXml.Descendants("seekers").Descendants("seeker").SingleOrDefault();

      seekerElement.Element("ssn").Value.ShouldEqual(ssn);
    }

    [Test]
    public void When_SeekerToXmlIsCalled_WithoutAValidSSN_ItIsIncludedInXmlRequest_AndSSNNotProvidedFlagSet()
    {
      const string ssn = "";
      var seekerModel = BuildModel();
      seekerModel.SocialSecurityNumber = ssn;

			var response = _jobSeekerCommandBuilder.AddJobSeeker(seekerModel, true, string.Empty, string.Empty, string.Empty, string.Empty);
      var responseXml = XDocument.Parse(response);
      var seekerElement = responseXml.Descendants("seekers").Descendants("seeker").SingleOrDefault();

      seekerElement.Element("ssn_not_provided_flag").Value.ShouldEqual("-1");
    }

    [Test]
    public void When_SeekerToXmlIsCalled_WithAValidDisibilityStatus_ItIsIncludedInXmlRequest()
    {
      var seekerModel = BuildModel();
      seekerModel.Resume.ResumeContent.Profile.DisabilityStatus = DisabilityStatus.NotDisclosed;

			var response = _jobSeekerCommandBuilder.AddJobSeeker(seekerModel, true, string.Empty, string.Empty, string.Empty, string.Empty);
      var responseXml = XDocument.Parse(response);
      var seekerElement = responseXml.Descendants("seekers").Descendants("seeker").SingleOrDefault();

      seekerElement.Element("disability_status").Value.ShouldEqual("externalDisibilityId");
    }

    [Test]
    public void When_SeekerToXmlIsCalled_WithAValidPrimaryPhone_ItIsIncludedInXmlRequest()
    {
      const string phoneNumber = "123454321";
      var seekerModel = BuildModel();
      seekerModel.Resume.ResumeContent.Profile.PrimaryPhone.PhoneType = PhoneType.Home;
      seekerModel.Resume.ResumeContent.Profile.PrimaryPhone.PhoneNumber = phoneNumber;

			var response = _jobSeekerCommandBuilder.AddJobSeeker(seekerModel, true, string.Empty, string.Empty, string.Empty, string.Empty);
      var responseXml = XDocument.Parse(response);
      var seekerElement = responseXml.Descendants("seekers").Descendants("seeker").SingleOrDefault();

      seekerElement.Element("phone_pri").Value.ShouldEqual(phoneNumber);
      seekerElement.Element("fax_pri").Value.ShouldBeNullOrEmpty();
    }

    [Test]
    public void When_SeekerToXmlIsCalled_WithAValidPrimaryFax_ItIsIncludedInXmlRequest()
    {
      const string fax = "987656789";
      var seekerModel = BuildModel();
      seekerModel.Resume.ResumeContent.Profile.PrimaryPhone.PhoneType = PhoneType.Fax;
      seekerModel.Resume.ResumeContent.Profile.PrimaryPhone.PhoneNumber = fax;

			var response = _jobSeekerCommandBuilder.AddJobSeeker(seekerModel, true, string.Empty, string.Empty, string.Empty, string.Empty);
      var responseXml = XDocument.Parse(response);
      var seekerElement = responseXml.Descendants("seekers").Descendants("seeker").SingleOrDefault();

      seekerElement.Element("fax_pri").Value.ShouldEqual(fax);
      seekerElement.Element("phone_pri").Value.ShouldBeNullOrEmpty();
    }

    [Test]
    public void When_SeekerToXmlIsCalled_WithoutAValidPrimaryFaxOrPhone_ItIsIncludedInXmlRequest()
    {
      var seekerModel = BuildModel();
      seekerModel.Resume.ResumeContent.Profile.PrimaryPhone = null;

			var response = _jobSeekerCommandBuilder.AddJobSeeker(seekerModel, true, string.Empty, string.Empty, string.Empty, string.Empty);
      var responseXml = XDocument.Parse(response);
      var seekerElement = responseXml.Descendants("seekers").Descendants("seeker").SingleOrDefault();

      seekerElement.Element("fax_pri").Value.ShouldBeNullOrEmpty();
      seekerElement.Element("phone_pri").Value.ShouldBeNullOrEmpty();
    }

    [Test]
    public void When_SeekerToXmlIsCalled_WithAValidEmail_ItIsIncludedInXmlRequest()
    {
      const string email = "123@testingtimes.comx";
      var seekerModel = BuildModel();
      seekerModel.Resume.ResumeContent.Profile.EmailAddress = email;

			var response = _jobSeekerCommandBuilder.AddJobSeeker(seekerModel, true, string.Empty, string.Empty, string.Empty, string.Empty);
      var responseXml = XDocument.Parse(response);
      var seekerElement = responseXml.Descendants("seekers").Descendants("seeker").SingleOrDefault();

      seekerElement.Element("email").Value.ShouldEqual(email);
    }

    [Test]
    public void When_SeekerToXmlIsCalled_WithAnAddressLine1_ItIsIncludedInXmlRequest()
    {
      const string addressLine1 = "12345 Address Line 1 For Testing";
      var seekerModel = BuildModel();
      seekerModel.Resume.ResumeContent.Profile.PostalAddress.Street1 = addressLine1;

			var response = _jobSeekerCommandBuilder.AddJobSeeker(seekerModel, true, string.Empty, string.Empty, string.Empty, string.Empty);
      var responseXml = XDocument.Parse(response);
      var addressElement = responseXml.Descendants("seekers").Descendants("seeker").Descendants("address").SingleOrDefault();

      addressElement.Element("addr_1").Value.ShouldEqual(addressLine1);
    }

    [Test]
    public void When_SeekerToXmlIsCalled_WithAnAddressLine2_ItIsIncludedInXmlRequest()
    {
      const string addressLine2 = "54321 Address Line 2 For Testing";
      var seekerModel = BuildModel();
      seekerModel.Resume.ResumeContent.Profile.PostalAddress.Street2 = addressLine2;

			var response = _jobSeekerCommandBuilder.AddJobSeeker(seekerModel, true, string.Empty, string.Empty, string.Empty, string.Empty);
      var responseXml = XDocument.Parse(response);
      var addressElement = responseXml.Descendants("seekers").Descendants("seeker").Descendants("address").SingleOrDefault();

      addressElement.Element("addr_2").Value.ShouldEqual(addressLine2);
    }

    [Test]
    public void When_SeekerToXmlIsCalled_WithACity_ItIsIncludedInXmlRequest()
    {
      const string city = "City For Testing";
      var seekerModel = BuildModel();
      seekerModel.Resume.ResumeContent.Profile.PostalAddress.City = city;

			var response = _jobSeekerCommandBuilder.AddJobSeeker(seekerModel, true, string.Empty, string.Empty, string.Empty, string.Empty);
      var responseXml = XDocument.Parse(response);
      var addressElement = responseXml.Descendants("seekers").Descendants("seeker").Descendants("address").SingleOrDefault();

      addressElement.Element("city").Value.ShouldEqual(city);
    }

    [Test]
    public void When_SeekerToXmlIsCalled_WithAZip_ItIsIncludedInXmlRequest()
    {
      const string zip = "Zip For Testing";
      var seekerModel = BuildModel();
      seekerModel.Resume.ResumeContent.Profile.PostalAddress.Zip = zip;

			var response = _jobSeekerCommandBuilder.AddJobSeeker(seekerModel, true, string.Empty, string.Empty, string.Empty, string.Empty);
      var responseXml = XDocument.Parse(response);
      var addressElement = responseXml.Descendants("seekers").Descendants("seeker").Descendants("address").SingleOrDefault();

			addressElement.Element("zip").Value.ShouldEqual(Regex.Replace(zip, "[^0-9]", string.Empty).Substring(0, Math.Min(5, Regex.Replace(zip, "[^0-9]", string.Empty).Length)));
    }

		[Test]
		public void When_SeekerToXmlIsCalled_WithA5DigitZip_ItIsIncludedInXmlRequest()
		{
			const string zip = "10118";
			var seekerModel = BuildModel();
			seekerModel.Resume.ResumeContent.Profile.PostalAddress.Zip = zip;
			var response = _jobSeekerCommandBuilder.AddJobSeeker(seekerModel, true, string.Empty, string.Empty, string.Empty, string.Empty);
			var responseXml = XDocument.Parse(response);
			var addressElement = responseXml.Descendants("seekers").Descendants("seeker").Descendants("address").SingleOrDefault();
			addressElement.Element("zip").Value.ShouldEqual("10118");
		}

		[Test]
		public void When_SeekerToXmlIsCalled_WithA9DigitZip_ItIsIncludedInXmlRequest()
		{
			const string zip = "101181234";
			var seekerModel = BuildModel();
			seekerModel.Resume.ResumeContent.Profile.PostalAddress.Zip = zip;
			var response = _jobSeekerCommandBuilder.AddJobSeeker(seekerModel, true, string.Empty, string.Empty, string.Empty, string.Empty);
			var responseXml = XDocument.Parse(response);
			var addressElement = responseXml.Descendants("seekers").Descendants("seeker").Descendants("address").SingleOrDefault();
			addressElement.Element("zip").Value.ShouldEqual("10118");
		}

		[Test]
		public void When_SeekerToXmlIsCalled_WithA9DigitZipWithAHyphen_ItIsIncludedInXmlRequest()
		{
			const string zip = "10118-1234";
			var seekerModel = BuildModel();
			seekerModel.Resume.ResumeContent.Profile.PostalAddress.Zip = zip;
			var response = _jobSeekerCommandBuilder.AddJobSeeker(seekerModel, true, string.Empty, string.Empty, string.Empty, string.Empty);
			var responseXml = XDocument.Parse(response);
			var addressElement = responseXml.Descendants("seekers").Descendants("seeker").Descendants("address").SingleOrDefault();
			addressElement.Element("zip").Value.ShouldEqual("10118");
		}

    
    [Test]
    public void When_SeekerToXmlIsCalled_WithAState_ItIsIncludedInXmlRequest()
    {
      long? stateId = 456;
      var seekerModel = BuildModel();
      seekerModel.Resume.ResumeContent.Profile.PostalAddress.StateId = stateId;
      seekerModel.Resume.ResumeContent.Profile.PostalAddress.CountryId = 789;

			var response = _jobSeekerCommandBuilder.AddJobSeeker(seekerModel, true, string.Empty, string.Empty, string.Empty, string.Empty);
      var responseXml = XDocument.Parse(response);
      var addressElement = responseXml.Descendants("seekers").Descendants("seeker").Descendants("address").SingleOrDefault();

      addressElement.Element("state").Value.ShouldEqual("externalStateId");
    }

    [Test]
    public void When_SeekerToXmlIsCalled_WithAAddressOutsideUSA_StateIsDefaultedInXmlRequest()
    {
      long? stateId = 456;
      var seekerModel = BuildModel();
      seekerModel.Resume.ResumeContent.Profile.PostalAddress.StateId = stateId;
      seekerModel.Resume.ResumeContent.Profile.PostalAddress.CountryId = 1;

			var response = _jobSeekerCommandBuilder.AddJobSeeker(seekerModel, true, string.Empty, string.Empty, string.Empty, string.Empty);
      var responseXml = XDocument.Parse(response);
      var addressElement = responseXml.Descendants("seekers").Descendants("seeker").Descendants("address").SingleOrDefault();

      addressElement.Element("state").Value.ShouldEqual("ZZ");
    }

    [Test]
    public void When_SeekerToXmlIsCalled_WithACountry_ItIsIncludedInXmlRequest()
    {
      long? countryId = 789;
      var seekerModel = BuildModel();
      seekerModel.Resume.ResumeContent.Profile.PostalAddress.CountryId = countryId;

			var response = _jobSeekerCommandBuilder.AddJobSeeker(seekerModel, true, string.Empty, string.Empty, string.Empty, string.Empty);
      var responseXml = XDocument.Parse(response);
      var addressElement = responseXml.Descendants("seekers").Descendants("seeker").Descendants("address").SingleOrDefault();

      addressElement.Element("country").Value.ShouldEqual("US");
    }

    [Test]
    public void When_SeekerToXmlIsCalled_WithNoCitizenInfo_ItIsNotIncludedInXmlRequest()
    {
      var seekerModel = BuildModel();
      seekerModel.Resume.ResumeContent.Profile.IsUSCitizen = null;

			var response = _jobSeekerCommandBuilder.AddJobSeeker(seekerModel, true, string.Empty, string.Empty, string.Empty, string.Empty);
      var responseXml = XDocument.Parse(response);
      var seekerElement = responseXml.Descendants("seekers").Descendants("seeker").SingleOrDefault();

      seekerElement.Element("citizen_flag").ShouldBeNull();
      seekerElement.Element("alien_id").ShouldBeNull();
      seekerElement.Element("alien_expires").ShouldBeNull();
      seekerElement.Element("alien_perm_flag").ShouldBeNull();
    }

    [Test]
    public void When_SeekerToXmlIsCalled_WithUSCitizenInfo_ItIsIncludedInXmlRequest_WithNoAlienInfo()
    {
      var seekerModel = BuildModel();
      seekerModel.Resume.ResumeContent.Profile.IsUSCitizen = true;

			var response = _jobSeekerCommandBuilder.AddJobSeeker(seekerModel, true, string.Empty, string.Empty, string.Empty, string.Empty);
      var responseXml = XDocument.Parse(response);
      var seekerElement = responseXml.Descendants("seekers").Descendants("seeker").SingleOrDefault();

      seekerElement.Element("citizen_flag").Value.ShouldEqual("-1");
      seekerElement.Element("alien_id").ShouldBeNull();
      seekerElement.Element("alien_expires").ShouldBeNull();
      seekerElement.Element("alien_perm_flag").ShouldBeNull();
    }

    [Test]
    public void When_SeekerToXmlIsCalled_WithNonUSCitizenInfo_ItIsIncludedInXmlRequest_WithAlienIdInfo()
    {
      const string alienId = "Aliend Id for seeker";
      var seekerModel = BuildModel();
      seekerModel.Resume.ResumeContent.Profile.IsUSCitizen = false;
      seekerModel.Resume.ResumeContent.Profile.AlienId = alienId;

			var response = _jobSeekerCommandBuilder.AddJobSeeker(seekerModel, true, string.Empty, string.Empty, string.Empty, string.Empty);
      var responseXml = XDocument.Parse(response);
      var seekerElement = responseXml.Descendants("seekers").Descendants("seeker").SingleOrDefault();

      seekerElement.Element("citizen_flag").Value.ShouldEqual("0");
      seekerElement.Element("alien_id").Value.ShouldEqual(alienId);
    }

    [Test]
    public void When_SeekerToXmlIsCalled_WithNonUSCitizenInfo_ItIsIncludedInXmlRequest_WithAlienExpiresInfo()
    {
      var alienExpiryDate = new DateTime(2015, 12, 31);
      var seekerModel = BuildModel();
      seekerModel.Resume.ResumeContent.Profile.IsUSCitizen = false;
      seekerModel.Resume.ResumeContent.Profile.AlienExpires = alienExpiryDate;

			var response = _jobSeekerCommandBuilder.AddJobSeeker(seekerModel, true, string.Empty, string.Empty, string.Empty, string.Empty);
      var responseXml = XDocument.Parse(response);
      var seekerElement = responseXml.Descendants("seekers").Descendants("seeker").SingleOrDefault();

      seekerElement.Element("citizen_flag").Value.ShouldEqual("0");
      seekerElement.Element("alien_expires").Value.ShouldEqual(alienExpiryDate.ToString("MMddyyyy"));
    }

    [Test]
    public void When_SeekerToXmlIsCalled_WithNonUSCitizenInfo_ItIsIncludedInXmlRequest_WithPermResidentInfo()
    {
      var seekerModel = BuildModel();
      seekerModel.Resume.ResumeContent.Profile.IsUSCitizen = false;
      seekerModel.Resume.ResumeContent.Profile.IsPermanentResident = true;

			var response = _jobSeekerCommandBuilder.AddJobSeeker(seekerModel, true, string.Empty, string.Empty, string.Empty, string.Empty);
      var responseXml = XDocument.Parse(response);
      var seekerElement = responseXml.Descendants("seekers").Descendants("seeker").SingleOrDefault();

      seekerElement.Element("citizen_flag").Value.ShouldEqual("0");
      seekerElement.Element("alien_perm_flag").Value.ShouldEqual("-1");
    }

    [Test]
    public void When_SeekerToXmlIsCalled_WithNoVeteranData_ItIsNotIncludedInXmlRequest()
    {
      var seekerModel = BuildModel();
      seekerModel.Resume.ResumeContent.Profile.Veteran = null;

			var response = _jobSeekerCommandBuilder.AddJobSeeker(seekerModel, true, string.Empty, string.Empty, string.Empty, string.Empty);
      var responseXml = XDocument.Parse(response);
      var seekerElement = responseXml.Descendants("seekers").Descendants("seeker").SingleOrDefault();

      seekerElement.Element("vet_flag").Value.ShouldEqual("0");
      seekerElement.Element("vet_start_date").ShouldBeNull();
      seekerElement.Element("vet_end_date").ShouldBeNull();
      seekerElement.Element("vet_era").ShouldBeNull();
      seekerElement.Element("vet_disability_status").ShouldBeNull();
      seekerElement.Element("campaign_vet_flag").ShouldBeNull();
    }

    [Test]
    public void When_SeekerToXmlIsCalled_WithVeteranFlag_ItIsIncludedInXmlRequest()
    {
      var veteran = new VeteranInfo
      {
        IsVeteran = true,
        History = new List<VeteranHistory> { new VeteranHistory () }
      };
      var seekerModel = BuildModel();
      seekerModel.Resume.ResumeContent.Profile.Veteran = veteran;

			var response = _jobSeekerCommandBuilder.AddJobSeeker(seekerModel, true, string.Empty, string.Empty, string.Empty, string.Empty);
      var responseXml = XDocument.Parse(response);
      var seekerElement = responseXml.Descendants("seekers").Descendants("seeker").SingleOrDefault();

      seekerElement.Element("vet_flag").Value.ShouldEqual("1");
    }

    [Test]
    public void When_SeekerToXmlIsCalled_WithVeteranStartDateAndCorrectEra_ItIsIncludedInXmlRequest()
    {
      var veteran = new VeteranInfo
      {
        IsVeteran = true,
        History = new List<VeteranHistory>
        {
          new VeteranHistory
          {
            VeteranEra = VeteranEra.OtherVet,
            VeteranStartDate = new DateTime(1970, 11, 21)
          }
        }
      };
      var seekerModel = BuildModel();
      seekerModel.Resume.ResumeContent.Profile.Veteran = veteran;

			var response = _jobSeekerCommandBuilder.AddJobSeeker(seekerModel, true, string.Empty, string.Empty, string.Empty, string.Empty);
      var responseXml = XDocument.Parse(response);
      var seekerElement = responseXml.Descendants("seekers").Descendants("seeker").SingleOrDefault();

      seekerElement.Element("vet_start_date").Value.ShouldEqual(veteran.History[0].VeteranStartDate.Value.ToString("MMddyyyy"));
    }

    [Test]
    public void When_SeekerToXmlIsCalled_WithVeteranEndDate_AndIncorrectEra_ItIsNotIncludedInXmlRequest()
    {
      var veteran = new VeteranInfo
      {
        IsVeteran = true,
        History = new List<VeteranHistory>
        {
          new VeteranHistory
          {
            VeteranStartDate = new DateTime(1970, 11, 21),
            VeteranEndDate = new DateTime(1971, 1, 31)
          }
        }
      };
      var seekerModel = BuildModel();
      seekerModel.Resume.ResumeContent.Profile.Veteran = veteran;

			var response = _jobSeekerCommandBuilder.AddJobSeeker(seekerModel, true, string.Empty, string.Empty, string.Empty, string.Empty);
      var responseXml = XDocument.Parse(response);
      var seekerElement = responseXml.Descendants("seekers").Descendants("seeker").SingleOrDefault();

      seekerElement.Element("vet_end_date").ShouldBeNull();
    }

    [Test]
    public void When_SeekerToXmlIsCalled_WithVeteranEndDate_AndCorrectEra_ItIsIncludedInXmlRequest()
    {
      var veteran = new VeteranInfo
      {
        IsVeteran = true,
        History = new List<VeteranHistory>
        {
          new VeteranHistory
          {
            VeteranEra = VeteranEra.OtherVet,
            VeteranStartDate = new DateTime(1970, 11, 21),
            VeteranEndDate = new DateTime(1971, 1, 31)
          }
        }
      };
      var seekerModel = BuildModel();
      seekerModel.Resume.ResumeContent.Profile.Veteran = veteran;

			var response = _jobSeekerCommandBuilder.AddJobSeeker(seekerModel, true, string.Empty, string.Empty, string.Empty, string.Empty);
      var responseXml = XDocument.Parse(response);
      var seekerElement = responseXml.Descendants("seekers").Descendants("seeker").SingleOrDefault();

      seekerElement.Element("vet_end_date").Value.ShouldEqual(veteran.History[0].VeteranEndDate.Value.ToString("MMddyyyy"));
    }

    [Test]
    public void When_SeekerToXmlIsCalled_WithVeteranEra_ItIsIncludedInXmlRequest()
    {
      var veteran = new VeteranInfo
      {
        IsVeteran = true,
        History = new List<VeteranHistory>
        {
          new VeteranHistory
          {
            VeteranStartDate = new DateTime(1970, 11, 21),
            VeteranEndDate = DateTime.Now.AddDays(7),
            VeteranEra = VeteranEra.TransitioningServiceMember
          }
        },
      };
      var seekerModel = BuildModel();
      seekerModel.Resume.ResumeContent.Profile.Veteran = veteran;

			var response = _jobSeekerCommandBuilder.AddJobSeeker(seekerModel, true, string.Empty, string.Empty, string.Empty, string.Empty);
      var responseXml = XDocument.Parse(response);
      var seekerElement = responseXml.Descendants("seekers").Descendants("seeker").SingleOrDefault();

      seekerElement.Element("vet_era").Value.ShouldEqual("external TransitioningServiceMember");
    }

    [Test]
    public void When_SeekerToXmlIsCalled_WithVeteranDisability_ItIsIncludedInXmlRequest()
    {
      var veteran = new VeteranInfo
      {
        IsVeteran = true,
        History = new List<VeteranHistory>
        {
          new VeteranHistory
          {
            VeteranStartDate = new DateTime(1970, 11, 21),
            VeteranEndDate = DateTime.Now.AddDays(7),
            VeteranEra = VeteranEra.TransitioningServiceMember,
            VeteranDisabilityStatus = VeteranDisabilityStatus.SpecialDisabled
          }
        }
      };
      var seekerModel = BuildModel();
      seekerModel.Resume.ResumeContent.Profile.Veteran = veteran;

			var response = _jobSeekerCommandBuilder.AddJobSeeker(seekerModel, true, string.Empty, string.Empty, string.Empty, string.Empty);
      var responseXml = XDocument.Parse(response);
      var seekerElement = responseXml.Descendants("seekers").Descendants("seeker").SingleOrDefault();

      seekerElement.Element("vet_disability_status").Value.ShouldEqual("external SpecialDisabled");
    }

		[Test]
		public void When_SeekerToXmlIsCalled_WithTransitioningVeteranWithElapsedDate_ItIsNotIncludedInXmlRequest()
		{
			var veteran = new VeteranInfo
			{
				IsVeteran = true,
				History = new List<VeteranHistory>
        {
          new VeteranHistory
          {
            VeteranStartDate = new DateTime(1970, 11, 21),
            VeteranEndDate = DateTime.Now.AddDays(-7),
            VeteranEra = VeteranEra.TransitioningServiceMember,
            VeteranDisabilityStatus = VeteranDisabilityStatus.SpecialDisabled
          }
        }
			};
			var seekerModel = BuildModel();
			seekerModel.Resume.ResumeContent.Profile.Veteran = veteran;

			var response = _jobSeekerCommandBuilder.AddJobSeeker(seekerModel, true, string.Empty, string.Empty, string.Empty, string.Empty);
			var responseXml = XDocument.Parse(response);
			var seekerElement = responseXml.Descendants("seekers").Descendants("seeker").SingleOrDefault();

			seekerElement.ShouldNotBeNull();
			if (seekerElement != null)
				seekerElement.Element("vet_flag").ShouldBeNull();
		}

    [Test]
    public void When_SeekerToXmlIsCalled_WithNoVeteran_CampaignVeteran_IsNotIncludedInXmlRequest()
    {
      var veteran = new VeteranInfo
      {
        IsVeteran = false,
        History = new List<VeteranHistory>
        {
          new VeteranHistory
          {
            VeteranStartDate = new DateTime(1970, 11, 21),
            VeteranEndDate = DateTime.Now.AddDays(7),
            IsCampaignVeteran = true,
            VeteranEra = VeteranEra.TransitioningServiceMember,
            VeteranDisabilityStatus = VeteranDisabilityStatus.SpecialDisabled,
          }
        }
      };
      var seekerModel = BuildModel();
      seekerModel.Resume.ResumeContent.Profile.Veteran = veteran;

			var response = _jobSeekerCommandBuilder.AddJobSeeker(seekerModel, true, string.Empty, string.Empty, string.Empty, string.Empty);
      var responseXml = XDocument.Parse(response);
      var seekerElement = responseXml.Descendants("seekers").Descendants("seeker").SingleOrDefault();

      seekerElement.Element("campaign_vet_flag").ShouldBeNull();
    }

    [Test]
    public void When_SeekerToXmlIsCalled_WithVeteran_CampaignVeteran_IsIncludedInXmlRequest()
    {
      var veteran = new VeteranInfo
      {
        IsVeteran = true,
        History = new List<VeteranHistory>
        {
          new VeteranHistory
          {
            VeteranStartDate = new DateTime(1970, 11, 21),
            VeteranEndDate = DateTime.Now.AddDays(7),
            IsCampaignVeteran = false,
            VeteranEra = VeteranEra.TransitioningServiceMember,
            VeteranDisabilityStatus = VeteranDisabilityStatus.SpecialDisabled
          }
        },
      };
      var seekerModel = BuildModel();
      seekerModel.Resume.ResumeContent.Profile.Veteran = veteran;

			var response = _jobSeekerCommandBuilder.AddJobSeeker(seekerModel, true, string.Empty, string.Empty, string.Empty, string.Empty);
      var responseXml = XDocument.Parse(response);
      var seekerElement = responseXml.Descendants("seekers").Descendants("seeker").SingleOrDefault();

      seekerElement.Element("campaign_vet_flag").Value.ShouldEqual("0");
    }

    [Test]
    public void When_SeekerToXmlIsCalled_WithNoLicenceData_ItIsNotIncludedInXmlRequest()
    {
      var seekerModel = BuildModel();
      seekerModel.Resume.ResumeContent.Profile.License = null;

			var response = _jobSeekerCommandBuilder.AddJobSeeker(seekerModel, true, string.Empty, string.Empty, string.Empty, string.Empty);
      var responseXml = XDocument.Parse(response);
      var seekerElement = responseXml.Descendants("seekers").Descendants("seeker").SingleOrDefault();

      seekerElement.Element("driver_flag").ShouldBeNull();
      seekerElement.Element("driver_state").ShouldBeNull();
    }
    
    [Test]
    public void When_SeekerToXmlIsCalled_WithLicenceData_ItIsIncludedInXmlRequest()
    {
      var licence = new LicenseInfo
      {
        HasDriverLicense = true
      };
      var seekerModel = BuildModel();
      seekerModel.Resume.ResumeContent.Profile.License = licence;

			var response = _jobSeekerCommandBuilder.AddJobSeeker(seekerModel, true, string.Empty, string.Empty, string.Empty, string.Empty);
      var responseXml = XDocument.Parse(response);
      var seekerElement = responseXml.Descendants("seekers").Descendants("seeker").SingleOrDefault();

      seekerElement.Element("driver_flag").Value.ShouldEqual("-1");
    }

    [Test]
    public void When_SeekerToXmlIsCalled_WithLicenceStateData_ItIsIncludedInXmlRequest()
    {
      var licence = new LicenseInfo
      {
        HasDriverLicense = true,
        DriverStateId = 456
      };
      var seekerModel = BuildModel();
      seekerModel.Resume.ResumeContent.Profile.License = licence;

			var response = _jobSeekerCommandBuilder.AddJobSeeker(seekerModel, true, string.Empty, string.Empty, string.Empty, string.Empty);
      var responseXml = XDocument.Parse(response);
      var seekerElement = responseXml.Descendants("seekers").Descendants("seeker").SingleOrDefault();

      seekerElement.Element("driver_state").Value.ShouldEqual("externalStateId");
    }

    [Test]
    public void When_SeekerToXmlIsCalled_WithDriverWithoutLicence_DriverState_ShouldBeEmptyInXmlRequest()
    {
      var licence = new LicenseInfo
      {
        HasDriverLicense = false,
        DriverStateId = 456
      };
      var seekerModel = BuildModel();
      seekerModel.Resume.ResumeContent.Profile.License = licence;

			var response = _jobSeekerCommandBuilder.AddJobSeeker(seekerModel, true, string.Empty, string.Empty, string.Empty, string.Empty);
      var responseXml = XDocument.Parse(response);
      var seekerElement = responseXml.Descendants("seekers").Descendants("seeker").SingleOrDefault();

      seekerElement.ShouldNotBeNull();
      if (seekerElement != null)
      {
        var driverElement = seekerElement.Element("driver_flag");
        driverElement.ShouldNotBeNull();
        if (driverElement != null)
          driverElement.Value.ShouldEqual("0");

        driverElement = seekerElement.Element("driver_class");
        driverElement.ShouldNotBeNull();
        if (driverElement != null)
          driverElement.Value.ShouldBeNullOrEmpty();

        driverElement = seekerElement.Element("driver_state");
        driverElement.ShouldNotBeNull();
        if (driverElement != null)
          driverElement.Value.ShouldBeNullOrEmpty();
      }
    }

    [Test]
    public void When_SeekerToXmlIsCalled_WithNoEducationData_ItIsNotIncludedInXmlRequest()
    {
      var seekerModel = BuildModel();
      seekerModel.Resume.ResumeContent.EducationInfo = null;

			var response = _jobSeekerCommandBuilder.AddJobSeeker(seekerModel, true, string.Empty, string.Empty, string.Empty, string.Empty);
      var responseXml = XDocument.Parse(response);
      var seekerElement = responseXml.Descendants("seekers").Descendants("seeker").SingleOrDefault();

      seekerElement.Element("edu_max").ShouldBeNull();
      seekerElement.Element("in_school_flag").ShouldBeNull();
    }

    [Test]
    public void When_SeekerToXmlIsCalled_WithEducationLevelData_ItIsIncludedInXmlRequest()
    {
      var education = new EducationInfo
                        {
                          EducationLevel = EducationLevel.Grade_12_No_Diploma_12
                        };
      var seekerModel = BuildModel();
      seekerModel.Resume.ResumeContent.EducationInfo = education;

			var response = _jobSeekerCommandBuilder.AddJobSeeker(seekerModel, true, string.Empty, string.Empty, string.Empty, string.Empty);
      var responseXml = XDocument.Parse(response);
      var seekerElement = responseXml.Descendants("seekers").Descendants("seeker").SingleOrDefault();

      seekerElement.Element("norm_edu_level_cd").Value.ShouldEqual("external Grade_12_No_Diploma_12");
    }
    
    #endregion


    #region Object creation

    private JobSeekerModel BuildModel()
    {
      return new JobSeekerModel
               {
                 Id = 99999,
                 ExternalId = "100",
                 Version = "12",
                 OfficeId = "1234",
                 AdminId = "5678",
                 Username = "jobseekerusername",
                 Password = "jobseekerpassword",
                 ExternalUsername = "externaljobseekerusername",
                 ExternalPassword = "externaljobseekerpassword",
                 EmailAddress = "123@thisisatestmethod.comx",
                 FirstName = "firstname",
                 LastName = "lastname",
                 SocialSecurityNumber = "123456789",
                 Resume = BuildResume()
               };
    }

    private ResumeModel BuildResume()
    {
      var resume = new ResumeModel();

      var resumeBody = new ResumeBody
                         {
                           Skills = new SkillInfo
                                      {
                                        Skills = "billing,c++,computer hardware/software knowledge,computer skills,customer service,data warehousing,database administration,email technical support,frontpage 97,hardware/software error explanation,hp-ux,internet development,ipx/spx,lotus cc : mail,lotus notes,mainframe,microsoft exchange,microsoft mail,microsoft office,microsoft windows,microsoft windows nt,modems,mvs,netscape enterprise server,network administration,novell netware,operating systems,printers,routers,routing optimization,rs6000,spreadsheets,system and network configuration,technical support,telephone skills,telephone technical support,transmission control protocol/internet protocol(tcp/ip),troubleshooting,unix,video cards,visual basic,website production,windows nt,wordperfect,administration,aix,backup,basic,c,c + +,compaq,computer hardware,customer relations,datawarehousing,dell computers,dos,email,epson,gateway,hewlett packard,hp - ux,ibm,ibm es/9000,ibm mvs,lan,microsoft exchange server,microsoft office 97,microsoft office 98,microsoft windows 2000,microsoft windows 95,microsoft windows 98,migration,networking,pc,programming,protocols,quality,servers,tcp/ip,toshiba,upgrades,utilities,visual basic 5.0,web site production".Split(',').ToList()
                                      }
                         };

      var userProfile = new UserProfile();
      userProfile.UserGivenName = new Name
                                    {
                                      FirstName = "seeker first name",
                                      LastName = "seeker last name",
                                      MiddleName = "x"
                                    };
      userProfile.DOB = new DateTime(1970, 2, 16);
      userProfile.Sex = Genders.Male;
      userProfile.PrimaryPhone = new Phone
                                   {
                                     PhoneNumber = "123555789",
                                     Extention = "667",
                                     PhoneType = PhoneType.Work
                                   };
      userProfile.PostalAddress = new Address
                                    {
                                      Street1 = "123 Street One",
                                      Street2 = "Street Two",
                                      MajorCity = "City (Major)",
                                      City = "City (Normal!)",
                                      CountyName = "Middlesex",
                                      CountyId = 888,
                                      StateId = 777,
                                      StateName = "TexasState",
                                      Zip = "654444",
                                      CountryId = 666,
                                      CountryName = "USA"
                                    };
      userProfile.EmailAddress = "asd@emailaddresses.comx";
      userProfile.SSN = "999999999";
      userProfile.IsUSCitizen = true;
      userProfile.IsMigrant = false;




      resumeBody.Profile = userProfile;
      resume.ResumeContent = resumeBody;
      return resume;
    }

    #endregion

  }
}
