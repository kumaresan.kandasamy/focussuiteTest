﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using System.Xml.Linq;
using Focus.Services.Integration.Implementations.AOSOS.ResponseTranslators;
using NUnit.Framework;

using Focus.Core;
using Focus.Core.Models.Integration;
using Focus.Services.Integration.Implementations.AOSOS;
using Framework.Testing.NUnit;

#endregion

namespace Focus.UnitTests.ClientIntegration.AOSOS
{
  [TestFixture]
  public class AOSOSGeneralCommandTests
  {
    private ClientSettings _clientSettings;
    private Translator _translator;

    [SetUp]
    public void SetUp()
    {
      _clientSettings = new ClientSettings
      {
        DefaultSecurityName = "TestSecurityName",
        DefaultPassword = "TestPassword",
        OriginationMethod = "8",
        DefaultOfficeId = "1000",
        DefaultAdminId = "TestDefaultAdminId"
      };
      _translator = new Translator(null);
    }

    #region Error handling

    [Test]
    public void When_AResponseContainsErrors_TheyAreCollatedAndFormatted()
    {
      var errors = _translator.FindErrors(_errorResponse);

      errors.ShouldEqual("AOSOS Error(s): 36067 is not a valid Counties\nFCJL Expire Date must be greater than today.\n");
    }

    [Test]
    public void When_AResponseContainsNoErrors_NoErrorsAreProcessed()
    {
      var errors = _translator.FindErrors(_noErrorResponse);

      errors.ShouldBeNullOrEmpty();
    }

    // Examples below taken from AOSOS documentation v5.2
    private const string _noErrorResponse = @"<osos docid='1120d206a0c43318c5334fe2f48fe75f'>
<response>
<mappings>
<mapping id='CO0259119' version='1' uuid='43'/>
<mapping benefit_id='1' job_id='CO0259119'/>
<mapping benefit_id='2' job_id='CO0259119'/>
<mapping benefit_id='3' job_id='CO0259119'/>
<mapping benefit_id='4' job_id='CO0259119'/>
<mapping benefit_id='5' job_id='CO0259119'/>
<mapping benefit_id='6' job_id='CO0259119'/>
<mapping benefit_id='7' job_id='CO0259119'/>
<mapping benefit_id='8' job_id='CO0259119'/>
</mappings>
</response>
</osos>";

    private const string _errorResponse = @"<osos docid='54d515e69f9fe1915babe757c5d90c14' >
<errors>
<error class='AFValidationError'
xpath='/osos/request/employers/employer/address/county'>
<message>36067 is not a valid Counties</message>
</error>
<error class='AFValidationError'
xpath='/osos/request/employers/employer/fcjl_expire_date' >
<message>FCJL Expire Date must be greater than today.</message>
</error>
</errors>
</osos>";

    #endregion
  }
}
