﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Models.Career;
using Focus.Services.Integration.Implementations.AOSOS.CommandBuilders;
using Focus.Services.Integration.Implementations.AOSOS.ResponseTranslators;
using NUnit.Framework;

using Focus.Core;
using Focus.Core.Models.Integration;
using Focus.Services.Integration.Implementations.AOSOS;
using Framework.Testing.NUnit;

#endregion

namespace Focus.UnitTests.ClientIntegration.AOSOS
{
  [TestFixture]
  public class AOSOSJobCommandBuilderTests
  {
    private ClientSettings _clientSettings;
    private JobCommandBuilder _jobCommandBuilder;

    private JobModel Job
    {
      get { return baseJob; }
    }

    private Translator _translator;

    [SetUp]
    public void SetUp()
    {
      _clientSettings = new ClientSettings
                          {
                            DefaultSecurityName = "TestSecurityName",
                            DefaultPassword = "TestPassword",
                            OriginationMethod = "8",
                            DefaultOfficeId = "1000",
                            DefaultAdminId = "TestDefaultAdminId",
                            CtAggrVersion = "123"
                          };

      var externalIds = new List<ExternalLookUpItemDto>();
      externalIds.Add(new ExternalLookUpItemDto
                        {
                          ExternalLookUpType = ExternalLookUpType.JobStatus,
                          InternalId = JobStatuses.Active.ToString(),
                          ExternalId = "3"
                        });
      externalIds.Add(new ExternalLookUpItemDto
                        {
                          ExternalLookUpType = ExternalLookUpType.EducationLevel,
                          InternalId = EducationLevels.BachelorsDegree.ToString(),
                          ExternalId = "6"
                        });
      externalIds.Add(new ExternalLookUpItemDto
                        {
                          ExternalId = "externalGenderId",
                          ExternalLookUpType = ExternalLookUpType.Gender,
                          InternalId = Genders.Male.ToString()
                        });
      externalIds.Add(new ExternalLookUpItemDto
                        {
                          ExternalId = "externalDisibilityId",
                          ExternalLookUpType = ExternalLookUpType.DisabilityStatus,
                          InternalId = DisabilityStatus.NotDisclosed.ToString()
                        });
      externalIds.Add(new ExternalLookUpItemDto
                        {
                          ExternalId = "externalCountyId",
                          ExternalLookUpType = ExternalLookUpType.County,
                          InternalId = "123"
                        });
      externalIds.Add(new ExternalLookUpItemDto
                        {
                          ExternalId = "externalStateId",
                          ExternalLookUpType = ExternalLookUpType.State,
                          InternalId = "456"
                        });
      externalIds.Add(new ExternalLookUpItemDto
                        {
                          ExternalId = "US",
                          ExternalLookUpType = ExternalLookUpType.Country,
                          InternalId = "789"
                        });
      externalIds.Add(new ExternalLookUpItemDto
      {
        ExternalId = "UK",
        ExternalLookUpType = ExternalLookUpType.Country,
        InternalId = "1"
      });
      externalIds.Add(new ExternalLookUpItemDto
      {
        ExternalId = "5",
        ExternalLookUpType = ExternalLookUpType.SalaryUnit,
        InternalId = "1001"
      });
      externalIds.Add(new ExternalLookUpItemDto
      {
        ExternalId = "1002",
        ExternalLookUpType = ExternalLookUpType.Shift,
        InternalId = "1"
      });

      _jobCommandBuilder = new JobCommandBuilder(_clientSettings, externalIds, null);
      _translator = new Translator(externalIds);
    }

    #region To XML requests

    #region Get Job

    [Test]
    public void When_GetJobById_PassedValidJobId_AValidXmlString_IsReturned()
    {
      var response = _jobCommandBuilder.GetJobById(Guid.NewGuid().ToString());

      response.ShouldBeValidXml();
    }

    [Test]
    public void When_GetJobById_PassedValidJobId_ACorrectFormatRequest_IsReturned()
    {
      var guidId = Guid.NewGuid();
			var expectedResponse = string.Format("<osos><request security_username='{0}' security_password='{1}' current_office_id='{2}' ct_aggr_version='{3}' admin_id='{5}'><jobs><job id='{4}' method='select' expand='*'/></jobs></request></osos>",
                                           _clientSettings.DefaultSecurityName, _clientSettings.DefaultPassword, _clientSettings.DefaultOfficeId, _clientSettings.CtAggrVersion, guidId, _clientSettings.DefaultAdminId);

      var response = _jobCommandBuilder.GetJobById(guidId.ToString());

      response.ShouldEqual(expectedResponse);
    }

    #endregion

    #region Save Job

    [Test]
    public void When_SaveJobIsCalled_ACorrectFormatRequest_IsReturned()
    {
      var guidId = Guid.NewGuid();
			var response = _jobCommandBuilder.SaveJob(Job, null, "1", null, null, null, null, guidId);
      response.ShouldBeValidXml();
    }

    [Test]
    public void When_SaveJob_IsPassedValidJob_ASingleJobsElementIsReturned()
    {
      var guidId = Guid.NewGuid();
      var response = _jobCommandBuilder.SaveJob(Job, null, "1", null, null, null, null, guidId);
      var responseXml = XDocument.Parse(response);
      responseXml.Descendants("jobs").Count().ShouldEqual(1);
    }

    [Test]
    public void When_SaveJob_IsPassedJobWithoutExternalId_AnInsertCommandIsGenerated()
    {
      var guidId = Guid.NewGuid();
      var updatedJob = Job.Clone();
      updatedJob.ExternalId = null;
      var response = _jobCommandBuilder.SaveJob(updatedJob, null, "1", null, null, null, null, guidId);
      var responseXml = XDocument.Parse(response);
      var jobElement = responseXml.Descendants("job").SingleOrDefault();
      jobElement.Attribute("method").Value.ShouldEqual("insert");
    }

    [Test]
    public void When_SaveJob_IsPassedJobWithExternalId_AnUpdateCommandIsGenerated()
    {
      var guidId = Guid.NewGuid();
      var updatedJob = Job.Clone();
      updatedJob.ExternalId = "123";
			var response = _jobCommandBuilder.SaveJob(updatedJob, null,"1", null, null, null, null, guidId);
      var responseXml = XDocument.Parse(response);
      var jobElement = responseXml.Descendants("job").SingleOrDefault();
      jobElement.Attribute("method").Value.ShouldEqual("update");
    }

    [Test]
    public void When_SaveJob_IsPassedValidJob_ASingleJobElementIsReturned()
    {
      var guidId = Guid.NewGuid();
			var response = _jobCommandBuilder.SaveJob(Job, null,"1", null, null, null, null, guidId);

      var responseXml = XDocument.Parse(response);

      responseXml.Descendants("job").Count().ShouldEqual(1);
    }

    //[Test]
    //public void When_SaveJob_IsPassedExternalOfficeId_ItIsIncludedInXmlRequest()
    //{
    //  var guidId = Guid.NewGuid();
    //  var response = _jobCommandBuilder.SaveJob(Job, "1", null, null, null, null, guidId);

    //  var responseXml = XDocument.Parse(response);
    //  var jobElement = responseXml.Descendants("job").SingleOrDefault();
    //  jobElement.Element("office_id").Value.ShouldEqual("6006");
    //}

    //[Test]
    //public void When_SaveJob_IsNotPassedExternalOfficeId_DefaultOfficeIdIsIncludedInXmlRequest()
    //{
    //  var guidId = Guid.NewGuid();
    //  var updatedJob = Job.Clone();
    //  updatedJob.ExternalOfficeId = null;
    //  var response = _jobCommandBuilder.SaveJob(updatedJob, null, "1", null, null, null, null, guidId);

    //  var responseXml = XDocument.Parse(response);
    //  var jobElement = responseXml.Descendants("job").SingleOrDefault();
    //  jobElement.Element("office_id").Value.ShouldEqual(_clientSettings.DefaultOfficeId);
    //}

    [Test]
    public void When_SaveJob_IsPassedOnet_ItIsIncludedInXmlRequest()
    {
      var guidId = Guid.NewGuid();
			var response = _jobCommandBuilder.SaveJob(Job, null,"1", null, null, null, null, guidId);

      var responseXml = XDocument.Parse(response);
      var jobElement = responseXml.Descendants("job").SingleOrDefault();
      jobElement.Element("onet3").Value.ShouldEqual("11101100");
    }

    [Test]
    public void When_SaveJob_IsPassedJobTitle_ItIsIncludedInXmlRequest()
    {
      var guidId = Guid.NewGuid();
			var response = _jobCommandBuilder.SaveJob(Job, null,"1", null, null, null, null, guidId);

      var responseXml = XDocument.Parse(response);
      var jobElement = responseXml.Descendants("job").SingleOrDefault();
      jobElement.Element("title").Value.ShouldEqual(Job.Title);
    }

    [Test]
    public void When_SaveJob_IsPassedJobTitleInExcessOf120Chars_ItIsTrimmedAndIncludedInXmlRequest()
    {
      var guidId = Guid.NewGuid();

      var updatedJob = Job.Clone();
      updatedJob.Title = BuildDummyString(130); // 130 characters

			var response = _jobCommandBuilder.SaveJob(updatedJob, null, "1", null, null, null, null, guidId);

      var responseXml = XDocument.Parse(response);
      var jobElement = responseXml.Descendants("job").SingleOrDefault();
      jobElement.Element("title").Value.Length.ShouldEqual(120); // 120 characters
    }

    [Test]
    public void When_SaveJob_IsPassedMinSalary_ItIsIncludedInXmlRequest()
    {
      var guidId = Guid.NewGuid();
			var response = _jobCommandBuilder.SaveJob(Job, null,"1", null, null, null, null, guidId);

      var responseXml = XDocument.Parse(response);
      var jobElement = responseXml.Descendants("job").SingleOrDefault();
      jobElement.Element("sal_min").Value.ShouldEqual("45000");
    }

    [Test]
    public void When_SaveJob_IsNotPassedMinSalary_ItIsNotIncludedInXmlRequest()
    {
      var guidId = Guid.NewGuid();
      var updatedJob = Job.Clone();
      updatedJob.MinSalary = null;
			var response = _jobCommandBuilder.SaveJob(updatedJob, null, "1", null, null, null, null, guidId);

      var responseXml = XDocument.Parse(response);
      var jobElement = responseXml.Descendants("job").SingleOrDefault();
      jobElement.Element("sal_min").ShouldBeNull();
    }

    [Test]
    public void When_SaveJob_IsPassedMaxSalary_ItIsIncludedInXmlRequest()
    {
      var guidId = Guid.NewGuid();
			var response = _jobCommandBuilder.SaveJob(Job, null,"1", null, null, null, null, guidId);

      var responseXml = XDocument.Parse(response);
      var jobElement = responseXml.Descendants("job").SingleOrDefault();
      jobElement.Element("sal_max").Value.ShouldEqual("65000");
    }

    [Test]
    public void When_SaveJob_IsNotPassedMaxSalary_ItIsNotIncludedInXmlRequest()
    {
      var guidId = Guid.NewGuid();
      var updatedJob = Job.Clone();
      updatedJob.MaxSalary = null;
			var response = _jobCommandBuilder.SaveJob(updatedJob, null, "1", null, null, null, null, guidId);

      var responseXml = XDocument.Parse(response);
      var jobElement = responseXml.Descendants("job").SingleOrDefault();
      jobElement.Element("sal_max").ShouldBeNull();
    }

    [Test]
    public void When_SaveJob_IsPassedMaxSalaryOrMinSalary_SalaryUnitIsIncludedInXmlRequest()
    {
      var guidId = Guid.NewGuid();
      var updatedJob = Job.Clone();
      updatedJob.SalaryUnitId = 1001;
			var response = _jobCommandBuilder.SaveJob(updatedJob, null, "1", null, null, null, null, guidId);

      var responseXml = XDocument.Parse(response);
      var jobElement = responseXml.Descendants("job").SingleOrDefault();
      jobElement.Element("sal_unit").Value.ShouldEqual("5");


      // Just max salary is null
      updatedJob.MaxSalary = null;
			response = _jobCommandBuilder.SaveJob(updatedJob, null, "1", null, null, null, null, guidId);
      responseXml = XDocument.Parse(response);
      jobElement = responseXml.Descendants("job").SingleOrDefault();
      jobElement.Element("sal_unit").Value.ShouldEqual("5");


      // Just min salary is null
      updatedJob.MaxSalary = null;
      updatedJob.MaxSalary = 65000;
			response = _jobCommandBuilder.SaveJob(updatedJob, null, "1", null, null, null, null, guidId);
      responseXml = XDocument.Parse(response);
      jobElement = responseXml.Descendants("job").SingleOrDefault();
      jobElement.Element("sal_unit").Value.ShouldEqual("5");

    }

    [Test]
    public void When_SaveJob_IsNotPassedMaxSalaryOrMinSalary_SalaryUnitIsNotIncludedInXmlRequest()
    {
      var guidId = Guid.NewGuid();
      var updatedJob = Job.Clone();
      updatedJob.MaxSalary = null;
      updatedJob.MinSalary = null;
			var response = _jobCommandBuilder.SaveJob(updatedJob, null, "1", null, null, null, null, guidId);

      var responseXml = XDocument.Parse(response);
      var jobElement = responseXml.Descendants("job").SingleOrDefault();
      jobElement.Element("sal_unit").ShouldBeNull();
    }

    [Test]
    public void When_SaveJob_IsPassedMinimumEducationValue_ItIsIncludedInXmlRequest()
    {
      var guidId = Guid.NewGuid();
			var response = _jobCommandBuilder.SaveJob(Job, null,"1", null, null, null, null, guidId);

      var responseXml = XDocument.Parse(response);
      var jobElement = responseXml.Descendants("job").SingleOrDefault();
      jobElement.Element("edu").Value.ShouldEqual("6");
    }

    [Test]
    public void When_SaveJob_IsNotPassedMinimumEducationValue_ADefaultValueIsIncludedInXmlRequest()
    {
      var guidId = Guid.NewGuid();
      var updatedJob = Job.Clone();
      updatedJob.EducationLevel = null;
			var response = _jobCommandBuilder.SaveJob(updatedJob, null, "1", null, null, null, null, guidId);

      var responseXml = XDocument.Parse(response);
      var jobElement = responseXml.Descendants("job").SingleOrDefault();
      jobElement.Element("edu").ShouldNotBeNull();
      jobElement.Element("edu").Value.ShouldEqual("1");
    }



    [Test]
    public void When_EducationIsPassed_ItIsTranslatedCorrectly()
    {
      //AOSOSTransformer.MapEducationLevel(EducationLevels.None).ShouldEqual("1");
      //AOSOSTransformer.MapEducationLevel(EducationLevels.HighSchoolDiploma).ShouldEqual("2");
      //AOSOSTransformer.MapEducationLevel(EducationLevels.AssociatesDegree).ShouldEqual("5");
      //AOSOSTransformer.MapEducationLevel(EducationLevels.BachelorsDegree).ShouldEqual("6");
      //AOSOSTransformer.MapEducationLevel(EducationLevels.GraduateDegree).ShouldEqual("7");
      //AOSOSTransformer.MapEducationLevel(EducationLevels.DoctoralDegree).ShouldEqual("8");
    }

    [Test]
    public void When_SaveJob_IsPassedHoursPerWeekValue_ItIsIncludedInXmlRequest()
    {
      var guidId = Guid.NewGuid();
			var response = _jobCommandBuilder.SaveJob(Job, null,"1", null, null, null, null, guidId);

      var responseXml = XDocument.Parse(response);
      var jobElement = responseXml.Descendants("job").SingleOrDefault();
      jobElement.Element("hours_per_wk").Value.ShouldEqual("40");
    }

    [Test]
    public void When_SaveJob_IsNotPassedHoursPerWeekValue_ItIsNotIncludedInXmlRequest()
    {
      var guidId = Guid.NewGuid();
      var updatedJob = Job.Clone();
      updatedJob.HoursPerWeek = null;
			var response = _jobCommandBuilder.SaveJob(updatedJob, null, "1", null, null, null, null, guidId);

      var responseXml = XDocument.Parse(response);
      var jobElement = responseXml.Descendants("job").SingleOrDefault();
      jobElement.Element("hours_per_wk").Value.ShouldEqual(string.Empty);
    }

    [Test]
    public void When_SaveJob_IsPassedShift_ItIsIncludedInXmlRequest()
    {
      var guidId = Guid.NewGuid();
      var updatedJob = Job.Clone();
      updatedJob.Shift = "1";
      var response = _jobCommandBuilder.SaveJob(updatedJob, null, "1", null, null, null, null, guidId);

      var responseXml = XDocument.Parse(response);
      var jobElement = responseXml.Descendants("job").SingleOrDefault();
      jobElement.Element("shift").Value.ShouldEqual("1");
    }

    [Test]
    public void When_SaveJob_IsPassedOpenings_ItIsIncludedInXmlRequest()
    {
      var guidId = Guid.NewGuid();
      var updatedJob = Job.Clone();
      updatedJob.Openings = 100;
      var response = _jobCommandBuilder.SaveJob(updatedJob, null, "1", null, null, null, null, guidId);

      var responseXml = XDocument.Parse(response);
      var jobElement = responseXml.Descendants("job").SingleOrDefault();
      jobElement.Element("position_ct").Value.ShouldEqual("100");
    }


    [Test]
    public void When_SaveJob_IsPassedDescriptionGreaterThan4000Chars_ItIsIncludedTruncatedAndIncludedInXmlRequest()
    {
      var guidId = Guid.NewGuid();
      var updatedJob = Job.Clone();
      updatedJob.Description = BuildDummyString(4010);
      var response = _jobCommandBuilder.SaveJob(updatedJob, null, "1", null, null, null, null, guidId);

      var responseXml = XDocument.Parse(response);
      var jobElement = responseXml.Descendants("job").SingleOrDefault();
      jobElement.Element("description").Value.Length.ShouldEqual(4000);
    }

    [Test]
    public void When_SaveJob_IsPassedDescriptionEqualTo4000Chars_ItIsIncludedInXmlRequest()
    {
      var guidId = Guid.NewGuid();
      var updatedJob = Job.Clone();
      updatedJob.Description = BuildDummyString(4000);
      var response = _jobCommandBuilder.SaveJob(updatedJob, null, "1", null, null, null, null, guidId);

      var responseXml = XDocument.Parse(response);
      var jobElement = responseXml.Descendants("job").SingleOrDefault();
      jobElement.Element("description").Value.ShouldEqual(updatedJob.Description);
      jobElement.Element("description").Value.Length.ShouldEqual(4000);
    }

    [Test]
    public void When_SaveJob_IsPassedDescriptionLessThan4000Chars_ItIsIncludedInXmlRequest()
    {
      var guidId = Guid.NewGuid();
      var updatedJob = Job.Clone();
      updatedJob.Description = BuildDummyString(100);
      var response = _jobCommandBuilder.SaveJob(updatedJob, null, "1", null, null, null, null, guidId);

      var responseXml = XDocument.Parse(response);
      var jobElement = responseXml.Descendants("job").SingleOrDefault();
      jobElement.Element("description").Value.ShouldEqual(updatedJob.Description);
    }

    [Test]
    public void When_SaveJob_IsPassedTheJobStatus_ItIsIncludedInXmlRequest()
    {
      var guidId = Guid.NewGuid();
      var updatedJob = Job.Clone();
      updatedJob.JobStatus = JobStatuses.Active;
      var response = _jobCommandBuilder.SaveJob(updatedJob, null, "1", null, null, null, null, guidId);

      var responseXml = XDocument.Parse(response);
      var jobElement = responseXml.Descendants("job").SingleOrDefault();
      jobElement.Element("job_status_cd").Value.ShouldEqual("3");
    }


    //[Test]
    //public void When_SaveJob_IsPassedExternalAdminId_ItIsIncludedInXmlRequest()
    //{
    //  var guidId = Guid.NewGuid();
    //  const string adminId = "6000";
    //  var updatedJob = Job.Clone();
    //  updatedJob.ExternalAdminId = adminId;
    //  var response = _jobCommandBuilder.SaveJob(updatedJob, null, "1", null, null, null, null, guidId);

    //  var responseXml = XDocument.Parse(response);
    //  var jobElement = responseXml.Descendants("job").SingleOrDefault();
    //  jobElement.Element("admin_id").Value.ShouldEqual(adminId);
    //}

    //[Test]
    //public void When_SaveJob_IsNotPassedExternalAdminId_TheDefaultIsIncludedInXmlRequest()
    //{
    //  var guidId = Guid.NewGuid();
    //  var updatedJob = Job.Clone();
    //  updatedJob.ExternalAdminId = null;
    //  var response = _jobCommandBuilder.SaveJob(updatedJob, null, "1", null, null, null, null, guidId);

    //  var responseXml = XDocument.Parse(response);
    //  var jobElement = responseXml.Descendants("job").SingleOrDefault();
    //  jobElement.Element("admin_id").Value.ShouldEqual(_clientSettings.DefaultAdminId);
    //}

    [Test]
    public void When_SaveJob_IsPassedAClosingDate_ItIsIncludedAndFormattedCorrectlyInXmlRequest()
    {
      var guidId = Guid.NewGuid();
      var closingDate = new DateTime(2014, 2, 12);
      var updatedJob = Job.Clone();
      updatedJob.ClosingDate = closingDate;
      var response = _jobCommandBuilder.SaveJob(updatedJob, null, "1", null, null, null, null, guidId);

      var responseXml = XDocument.Parse(response);
      var jobElement = responseXml.Descendants("job").SingleOrDefault();
      jobElement.Element("last_open_date").Value.ShouldEqual(closingDate.ToString("MMddyyyy"));
    }

    [Test]
    public void When_SaveJob_IsNotPassedAffirmativeActionFlag_ItIsIncludedInXmlRequest()
    {
      var guidId = Guid.NewGuid();
      var updatedJob = Job.Clone();
      updatedJob.CourtOrderedAffirmativeAction = null;
			var response = _jobCommandBuilder.SaveJob(updatedJob, null, "1", null, null, null, null, guidId);

      var responseXml = XDocument.Parse(response);
      var jobElement = responseXml.Descendants("job").SingleOrDefault();
      jobElement.Element("affirmative_action_flag").ShouldBeNull();
    }

    [Test]
    public void When_SaveJob_IsPassedWorkdaysFlags_TheyAreIncludedInXmlRequest()
    {
      var guidId = Guid.NewGuid();
      var updatedJob = Job.Clone();
      updatedJob.Weekdays = DaysOfWeek.Monday | DaysOfWeek.Wednesday | DaysOfWeek.Friday;
			var response = _jobCommandBuilder.SaveJob(updatedJob, null, "1", null, null, null, null, guidId);

      var responseXml = XDocument.Parse(response);
      var jobElement = responseXml.Descendants("job").SingleOrDefault();
      jobElement.Element("work_mon_flag").Value.ShouldEqual("-1");
      jobElement.Element("work_tue_flag").Value.ShouldEqual("0");
      jobElement.Element("work_wed_flag").Value.ShouldEqual("-1");
      jobElement.Element("work_thu_flag").Value.ShouldEqual("0");
      jobElement.Element("work_fri_flag").Value.ShouldEqual("-1");
      jobElement.Element("work_sat_flag").Value.ShouldEqual("0");
      jobElement.Element("work_sun_flag").Value.ShouldEqual("0");
    }

    // TODO: driving licence test
    //[Test]
    //public void When_SaveJob_IsDrivingLicenseValueFlags_ItIsIncludedInXmlRequest()
    //{
    //  var guidId = Guid.NewGuid();
    //  var updatedJob = Job.Clone();
    //  const string drivingLicenceClass = "5";
    //  updatedJob.DrivingLicenceClass = drivingLicenceClass;
    //  var response = _jobCommandBuilder.SaveJob(updatedJob, "1", guidId);

    //  var responseXml = XDocument.Parse(response);
    //  var jobElement = responseXml.Descendants("job").SingleOrDefault();
    //  jobElement.Element("driver_class").Value.ShouldEqual(drivingLicenceClass);
    //}

    [Test]
    public void When_SaveJob_IsPassedContactPreferences_TheyAreIncludedInXmlRequest()
    {
      var guidId = Guid.NewGuid();
      var updatedJob = Job.Clone();
      updatedJob.InterviewContactPreferences = ContactMethods.Telephone | ContactMethods.Fax;
			var response = _jobCommandBuilder.SaveJob(updatedJob, null, "1", null, null, null, null, guidId);

      var responseXml = XDocument.Parse(response);
      var jobElement = responseXml.Descendants("job").SingleOrDefault();
      jobElement.Element("contact_email_flag").Value.ShouldEqual("-1");
      jobElement.Element("contact_fax_flag").Value.ShouldEqual("0");
      jobElement.Element("contact_phone_flag").Value.ShouldEqual("0");
      jobElement.Element("contact_postal_flag").Value.ShouldEqual("0");
      jobElement.Element("contact_send_direct_flag").Value.ShouldEqual("0");
    }

    [Test]
    public void When_SaveJob_IsPassedEmployerRepId_ItIsIncludedInXmlRequest()
    {
      var guidId = Guid.NewGuid();
      var updatedJob = Job.Clone();
      const string externalEmployeeId = "123";
      updatedJob.HiringManager.ExternalId = externalEmployeeId;
			var response = _jobCommandBuilder.SaveJob(updatedJob, null, "1", null, null, null, null, guidId);

      var responseXml = XDocument.Parse(response);
      var jobElement = responseXml.Descendants("job").SingleOrDefault();
      jobElement.Element("employer_contact_id").Value.ShouldEqual(externalEmployeeId);
    }

    [Test]
    public void When_SaveJob_IsPassedAnAddress_ASingleAddressElementIncludedInXmlRequest()
    {
      var guidId = Guid.NewGuid();
      var updatedJob = Job.Clone();
			var response = _jobCommandBuilder.SaveJob(updatedJob, null, "1", null, null, null, null, guidId);

      var responseXml = XDocument.Parse(response);
      responseXml.Descendants("address").Count().ShouldEqual(1);

    }

    [Test]
    public void When_SaveJob_IsPassedAnAddressWithFirstLine_ItIsIncludedInXmlRequest()
    {
      var guidId = Guid.NewGuid();
      var updatedJob = Job.Clone();
      const string addressLine1 = "123 Address line 1 Road";
      updatedJob.Addresses[0].AddressLine1 = addressLine1;
			var response = _jobCommandBuilder.SaveJob(updatedJob, null, "1", null, null, null, null, guidId);

      var responseXml = XDocument.Parse(response);
      var addressElement = responseXml.Descendants("address").SingleOrDefault();

      addressElement.Element("addr_1").Value.ShouldEqual(addressLine1);
    }

    [Test]
    public void When_SaveJob_IsPassedAnAddressWithSecondLine_ItIsIncludedInXmlRequest()
    {
      var guidId = Guid.NewGuid();
      var updatedJob = Job.Clone();
      const string addressLine2 = "123 Address line 2 Road";
      updatedJob.Addresses[0].AddressLine2 = addressLine2;
			var response = _jobCommandBuilder.SaveJob(updatedJob, null, "1", null, null, null, null, guidId);

      var responseXml = XDocument.Parse(response);
      var addressElement = responseXml.Descendants("address").SingleOrDefault();

      addressElement.Element("addr_2").Value.ShouldEqual(addressLine2);
    }

    [Test]
    public void When_SaveJob_IsPassedAnAddressWithCity_ItIsIncludedInXmlRequest()
    {
      var guidId = Guid.NewGuid();
      var updatedJob = Job.Clone();
      const string city = "City for address";
      updatedJob.Addresses[0].City = city;
			var response = _jobCommandBuilder.SaveJob(updatedJob, null, "1", null, null, null, null, guidId);

      var responseXml = XDocument.Parse(response);
      var addressElement = responseXml.Descendants("address").SingleOrDefault();

      addressElement.Element("city").Value.ShouldEqual(city);
    }

    [Test]
    public void When_SaveJob_IsPassedAnAddressWithPostCode_ItIsIncludedInXmlRequest()
    {
      var guidId = Guid.NewGuid();
      var updatedJob = Job.Clone();
      const string postCode = "11147";
      updatedJob.Addresses[0].PostCode = postCode;
			var response = _jobCommandBuilder.SaveJob(updatedJob, null, "1", null, null, null, null, guidId);

      var responseXml = XDocument.Parse(response);
      var addressElement = responseXml.Descendants("address").SingleOrDefault();

      addressElement.Element("zip").Value.ShouldEqual(postCode);
    }

    [Test]
    public void When_SaveJob_IsPassedAnAddressWithCounty_ItIsIncludedInXmlRequest()
    {
      var guidId = Guid.NewGuid();
      var updatedJob = Job.Clone();

      updatedJob.Addresses[0].CountryId = 789;
      updatedJob.Addresses[0].CountyId = 123;
			var response = _jobCommandBuilder.SaveJob(updatedJob, null, "1", null, null, null, null, guidId);

      var responseXml = XDocument.Parse(response);
      var addressElement = responseXml.Descendants("address").SingleOrDefault();

      addressElement.Element("county").Value.ShouldEqual("externalCountyId");
    }

    [Test]
    public void When_SaveJob_IsPassedAnAddressWithState_ItIsIncludedInXmlRequest()
    {
      var guidId = Guid.NewGuid();
      var updatedJob = Job.Clone();
      updatedJob.Addresses[0].CountryId = 789;
      updatedJob.Addresses[0].StateId = 456;
			var response = _jobCommandBuilder.SaveJob(updatedJob, null, "1", null, null, null, null, guidId);

      var responseXml = XDocument.Parse(response);
      var addressElement = responseXml.Descendants("address").SingleOrDefault();

      addressElement.Element("state").Value.ShouldEqual("externalStateId");
    }

    [Test]
    public void When_SaveJob_IsPassedAnAddressWithCountryOutsideUSSateIsDefaulted_ItIsIncludedInXmlRequest()
    {
      var guidId = Guid.NewGuid();
      var updatedJob = Job.Clone();
      const string state = "WI";
      const string country = "GB";
      updatedJob.Addresses[0].ExternalStateId = state;
      updatedJob.Addresses[0].ExternalCountryId = country;
			var response = _jobCommandBuilder.SaveJob(updatedJob, null, "1", null, null, null, null, guidId);

      var responseXml = XDocument.Parse(response);
      var addressElement = responseXml.Descendants("address").SingleOrDefault();

      addressElement.Element("state").Value.ShouldEqual("ZZ");
    }



    #endregion

    #region Referrals

    // Not all requests return a response according to the examples in the documentation. (Or if they do the information returned doesn't need to be acted upon unless there are errors

    [Test]
    public void When_GetJobReferralInfo_IsPassedJobId_ReturnsValidXML()
    {
      var response = _jobCommandBuilder.GetJobReferralInfo(Guid.NewGuid().ToString());

      response.ShouldBeValidXml();
    }


    [Test]
    public void When_GetJobReferralInfo_IsPassedJobId_ReturnsExpectedResponse()
    {
      var guidId = Guid.NewGuid();
			var expectedResponse = string.Format("<osos><request security_username='{0}' security_password='{1}' current_office_id='{2}' ct_aggr_version='{3}' admin_id='{5}'><jobs><job method='select' return='job_status_cd, referral_desired_ct, referrals' id='{4}'></job></jobs></request></osos>",
                                           _clientSettings.DefaultSecurityName, _clientSettings.DefaultPassword, _clientSettings.DefaultOfficeId, _clientSettings.CtAggrVersion, guidId, _clientSettings.DefaultAdminId);

      var response = _jobCommandBuilder.GetJobReferralInfo(guidId.ToString());

      response.ShouldEqual(expectedResponse);
    }

    [Test]
    public void When_ReferJobSeeker_IsPassedJobId_ReturnsValidXML()
    {
			var response = _jobCommandBuilder.ReferJobSeeker(Guid.NewGuid().ToString(), Guid.NewGuid().ToString(), string.Empty, string.Empty, string.Empty, string.Empty);

      response.ShouldBeValidXml();
    }

    [Test]
    public void When_ReferJobSeeker_IsPassedJobId_ReturnsExpectedResponse()
    {
      var jobId = Guid.NewGuid();
      var jobSeekerId = Guid.NewGuid();
			var expectedResponse = string.Format("<osos><request security_username='{0}' security_password='{1}' current_office_id='{2}' ct_aggr_version='{3}' admin_id='{6}'><seeker_match id='{4}' method='refer'><jobs><job id='{5}'/></jobs></seeker_match></request></osos>",
				_clientSettings.DefaultSecurityName, _clientSettings.DefaultPassword, _clientSettings.DefaultOfficeId, _clientSettings.CtAggrVersion, jobSeekerId, jobId, _clientSettings.DefaultAdminId);
			var response = _jobCommandBuilder.ReferJobSeeker(jobId.ToString(), jobSeekerId.ToString(), string.Empty, string.Empty, string.Empty, string.Empty);

      response.ShouldEqual(expectedResponse);
    }


    #endregion


    #endregion

    #region From Xml Responses

    [Test]
    public void When_ReceivingSaveJobResponse_TheExternalIdCanBeExtracted()
    {
      var translatedResponse = _translator.ProcessMappings(_insertJobResponse);

      translatedResponse[0].ExternalId.ShouldEqual("CO0259119");
    }


    [Test]
    public void When_ReceivingGetJobResponse_TheVersionCanBeExtracted()
    {
      var translatedJob = _translator.XmlToJob(_getJobResponse);

      translatedJob.Version.ShouldEqual("101");
    }

    [Test]
    public void When_ReceivingGetReferralInfoResponse_TheReferralInfoCanBeExtracted()
    {
      var referralInfo = _translator.XmlToReferralCounts(_getReferralInfoResponse);

      referralInfo.Item1.ShouldEqual(10);
      referralInfo.Item2.ShouldEqual(12);
    }


    #endregion

    #region Pre-prepared models

    private readonly JobModel baseJob = new JobModel
                                          {
                                            Id = 1,
                                            OfficeId = 2,
                                            ExternalOfficeId = "6006",
                                            AdminId = 3,
                                            ExternalAdminId = "6000",
                                            OnetCode = "11-1011.00",
                                            Title = "Software Engineer",
                                            Description = "Test Data..",
                                            MinSalary = 45000,
                                            MaxSalary = 65000,
                                            SalaryUnit = "5",
                                            EducationLevel = EducationLevels.BachelorsDegree,
                                            HoursPerWeek = 40,
                                            Shift = "3",
                                            JobStatus = JobStatuses.Active,
                                            ClosingDate = new DateTime(2001, 12, 12), // Not an idea date but this is the date in the documentation
                                            Category = "",
                                            JobSource = 1,
                                            InterviewContactPreferences = ContactMethods.Telephone,
                                            Url = "",
                                            Addresses = new List<AddressModel>
                                                          {
                                                            new AddressModel
                                                              {
                                                                AddressLine1 = "main street",
                                                                AddressLine2 = "apt. 106",
                                                                AddressLine3 = "",
                                                                City = "SYR",
                                                                CountryId = 0,
                                                                CountyId = 0,
                                                                ExternalCountryId = "US",
                                                                ExternalCountyId = "36067",
                                                                ExternalStateId = "NY",
                                                                PostCode = "13201",
                                                                StateId = 0,
                                                                TrackingId = ""
                                                              }
                                                          },
                                            PublicTransportAccessible = true,
                                            Version = "1",
                                            EmployeeId = 0,
                                            ExternalId = null,
                                            Openings = 3,
                                            TrackingId = "",
                                            ForeignLabourCertificationH2A = null,
                                            ForeignLabourCertificationH2B = null,
                                            ForeignLabourCertificationOther = null,
                                            CourtOrderedAffirmativeAction = true,
                                            WorkOpportunitiesTaxCreditHires = WorkOpportunitiesTaxCreditCategories.None,
                                            Weekdays = DaysOfWeek.Monday | DaysOfWeek.Tuesday | DaysOfWeek.Wednesday | DaysOfWeek.Thursday | DaysOfWeek.Friday,
                                            //DrivingLicenceClass = "2",
                                            DrivingLicenceRequired = null,
                                            LeaveBenefits = null,
                                            RetirementBenefits = null,
                                            InsuranceBenefits = null,
                                            MiscellaneousBenefits = null,
                                            OtherBenefitsDetails = null,
                                            HiringManager = new HiringManager
                                                              {
                                                                EmployeeId = 1,
                                                                UserId = 9,
                                                                PersonId = 10,
                                                                FirstName = "Employee first name",
                                                                Surname = "Employee last name",
                                                                Email = "employees@emailaddress.comx",
                                                                PhoneNumber = "5555555555",
                                                                ExternalId = "external employee id",
                                                                Address = new AddressModel
                                                                            {
                                                                              AddressLine1 = "main street",
                                                                              AddressLine2 = "apt. 106",
                                                                              AddressLine3 = "",
                                                                              City = "SYR",
                                                                              CountryId = 0,
                                                                              CountyId = 0,
                                                                              ExternalCountryId = "US",
                                                                              ExternalCountyId = "36067",
                                                                              ExternalStateId = "NY",
                                                                              PostCode = "13201",
                                                                              StateId = 0,
                                                                              TrackingId = ""
                                                                            }
                                                              }
                                          };

    #endregion

    #region Documented responses

    // Xml examples below taken from the AOSOS API document (5.2)

    private const string _insertJobResponse = @"<osos docid='1120d206a0c43318c5334fe2f48fe75f'>
<response>
<mappings>
<mapping id='CO0259119' version='1' uuid='43'/>
<mapping benefit_id='1' job_id='CO0259119'/>
<mapping benefit_id='2' job_id='CO0259119'/>
<mapping benefit_id='3' job_id='CO0259119'/>
<mapping benefit_id='4' job_id='CO0259119'/>
<mapping benefit_id='5' job_id='CO0259119'/>
<mapping benefit_id='6' job_id='CO0259119'/>
<mapping benefit_id='7' job_id='CO0259119'/>
<mapping benefit_id='8' job_id='CO0259119'/>
</mappings>
</response>
</osos>";

    private const string _getJobResponse = @"<osos docid='5d81de6fda7ec2fd4d33e6c34787905f'>
<response>
<jobs start='0' count='1' total_count='1'>
<job security_password='q' id='NY0264175' method='select'
version='101' security_username='q' >
<job_status_cd>1</job_status_cd>
<onet3 >11101100</onet3>
<duration>3</duration>
<office_id>6003</office_id>
<title>nmbnmb</title>
<edu>9</edu>
<last_open_date>05072001</last_open_date>
<position_ct>54</position_ct>
<description>11111</description>
<employer_contact_id>576513968</employer_contact_id>
<origination_method>7</origination_method>
<dot>079374014</dot>
<title_upr>NMBNMB</title_upr>
<edu_normalized>12</edu_normalized>
<admin_id>6002</admin_id>
<affirmative_action_flag>0</affirmative_action_flag><category>A</category>
<public_transport_flag>0</public_transport_flag>
<drv_double_flag>0</drv_double_flag>
<contact_email_flag>0</contact_email_flag>
<contact_phone_flag>1</contact_phone_flag>
<status_date>03082001</status_date>
<company>QuickWits</company>
<timestamp>
<atime>03082001</atime>
<mtime>03192001</mtime>
<ctime>03082001</ctime>
</timestamp>
<address>
<country>US</country>
<zip>767676767</zip>
<state>NY</state>
<city>gfhgfhyhvggf</city>
<addr_1>123 main street</addr_1>
<county>21001</county>
</address>
</job>
</jobs>
</response>
</osos>";

    private const string _getReferralInfoResponse = @"<osos docid='0aaea18770f2a7f151acfc7e84c29a51'>
<response>
<jobs start='0' count='1' total_count='1'>
<job security_password='tester1123' id='NY0239557' order='company+'
security_username='tester1' method='select' limit='500'
version='87' returns='job_status_cd, referral_desired_ct, referrals'>
<job_status_cd>1</job_status_cd>
<referral_provided_ct>10</referral_provided_ct>
<referral_desired_ct>12</referral_desired_ct>
<timestamp>
<ctime>06282000</ctime>
</timestamp>
</job>
</jobs>
</response>
</osos>";

    #endregion



    #region Helpers

    /// <summary>
    /// Builds the dummy string for testing string concatenation in command builder
    /// </summary>
    /// <param name="length">The length.</param>
    /// <returns></returns>
    private string BuildDummyString(int length)
    {
      var returnValue = string.Empty;
      if (length < 1) return string.Empty;

      for (var i = 0; i < length; i++)
      {
        returnValue += "1";
      }

      return returnValue;
    }

    #endregion
  }
}
