﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Diagnostics;
using System.ServiceModel;
using Framework.Testing.NUnit;
using NUnit.Framework;

using Framework.Core;

using Focus.Core;
using Focus.Core.IntegrationMessages;
using Focus.Core.Models.Integration;
using Focus.Services.Repositories;
using Focus.UnitTests.Core;
using Newtonsoft.Json;

#endregion

namespace Focus.UnitTests.ClientIntegration
{
  [TestFixture]
  public class WisconsinProviderTests : TestFixtureBase
  {
    private IntegrationRepository _testIntegrationRepository;

		private const IntegrationClient Client = IntegrationClient.Wisconsin;

    public override void SetUp()
    {
      base.SetUp();

	    var settings = new Focus.Services.Integration.Implementations.Wisconsin.ClientSettings
		    {
					FocusServiceEndpoint = "https://acc.dwd.wisconsin.gov/NJCW/services/focus.svc",
					ConversionServiceEndpoint = "https://acc.dwd.wisconsin.gov/NJCW/services/conversion.svc",
					ServiceSecurityMode = WSFederationHttpSecurityMode.TransportWithMessageCredential,
					TokenServiceEndpoint = "https://fsextacc.wisconsin.gov/adfs/services/trust/13/usernamemixed",
					Username = @"WIEXTACC\FocusServiceAccount",
					Password = "password1",
					RelyingParty = "https://acc.dwd.wisconsin.gov/NJCW/"
		    };

	    var settingsString = JsonConvert.SerializeObject(settings);

			_testIntegrationRepository = new IntegrationRepository(RuntimeContext, Client, settingsString);
    }

		[Test]
		[Explicit]
		public void Send_GetJobSeeker_ByExternalId()
		{
			var request = new GetJobSeekerRequest { JobSeeker = new JobSeekerModel { ExternalId = "5050" }, IsNew = true};
			var response = _testIntegrationRepository.GetJobSeeker(request);
		}

		[Test]
		[Explicit]
		public void Send_GetEmployer_ByExternalId()
		{
			var request = new GetEmployerRequest { EmployerId = "126034" };
			var response = _testIntegrationRepository.GetEmployer(request);
		}

		[Test]
		[Explicit]
		[TestCase(ActionTypes.ViewCareerExplorationTools, IntegrationOutcome.Success, "5050")]
		[TestCase(ActionTypes.RunManualJobSearch, IntegrationOutcome.Success, "5050")]
		[TestCase(ActionTypes.CompleteResume, IntegrationOutcome.Success, "5050")]
		[TestCase(ActionTypes.EditDefaultCompletedResume, IntegrationOutcome.Success, "5050")]
		[TestCase(ActionTypes.LogIn, IntegrationOutcome.NotImplemented, "5050")]
		[TestCase(ActionTypes.ViewCareerExplorationTools, IntegrationOutcome.Failure, "oddsocks")]  // Non number id is rejected by WI's service.
		[TestCase(ActionTypes.ViewCareerExplorationTools, IntegrationOutcome.Success, "-234")]  // Seems odd but negative numbers seem to be accepted for the id.
		public void Send_AddSelfService_ByExternalId(ActionTypes actionType, IntegrationOutcome outcome, string userId)
		{
			var request = new AssignJobSeekerActivityRequest { JobSeekerId = userId, ActionType = actionType};
			var response = _testIntegrationRepository.AssignJobSeekerActivity(request);
			response.Outcome.ShouldEqual(outcome);
		}

    [Test]
    [Explicit]
    public void TestUpdateContact()
    {
      var response = _testIntegrationRepository.SaveJobSeeker(new SaveJobSeekerRequest());
      response.Outcome.ShouldEqual(IntegrationOutcome.Success);

    }
  }
}
