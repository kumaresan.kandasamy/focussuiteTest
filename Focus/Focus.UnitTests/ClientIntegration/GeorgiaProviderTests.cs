﻿#region Copyright © 2000 - 2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Diagnostics;
using Focus.Core.Models.Career;
using Focus.UnitTests.Core;
using NUnit.Framework;

using Framework.Core;

using Focus.Core;
using Focus.Core.IntegrationMessages;
using Focus.Core.Messages;
using Focus.Core.Models;
using Focus.Core.Models.Integration;
using Focus.Services.Repositories;
using Framework.Testing.NUnit;
using Newtonsoft.Json;

#endregion

namespace Focus.UnitTests.ClientIntegration
{
	public class GeorgiaProviderTests : TestFixtureBase
	{
		private IntegrationRepository _testIntegrationRepository;

		private const IntegrationClient Client = IntegrationClient.Georgia;
		private const string Settings = "{\"Url\": \"http://ukfocusstage.burning-glass.com/integrationservices/api/georgia/\", \"SourceSystem\": \"BurningGlass\", \"PhoneNumberLength\": 10}";

		public override void SetUp()
		{
			DoSetup(false);
			_testIntegrationRepository = new IntegrationRepository( RuntimeContext, Client, Settings );
		}

		[Test]
		public void Talent_RegisterAccount_LogAction()
		{
			RuntimeContext.AppSettings.IntegrationClient = Client;
			var response = _testIntegrationRepository.GetActionData( ActionTypes.RegisterAccount, 7212504, 0 );
			RuntimeContext.AppSettings.IntegrationClient = IntegrationClient.Standalone;

			response.ShouldEqual(string.Empty);
		}

		[Test]
		public void Assist_RegisterAccount_LogAction()
		{
			RuntimeContext.AppSettings.IntegrationClient = Client;
			var response = _testIntegrationRepository.GetActionData(ActionTypes.RegisterAccount, 7212504, 2);
			RuntimeContext.AppSettings.IntegrationClient = IntegrationClient.Standalone;

			response.ShouldEqual(string.Empty);
		}

		[Test]
		public void CreateSingleSignOn_LogAction()
		{
			RuntimeContext.AppSettings.IntegrationClient = Client;
			var response = _testIntegrationRepository.GetActionData(ActionTypes.CreateSingleSignOn, 7212505, 2);
			RuntimeContext.AppSettings.IntegrationClient = IntegrationClient.Standalone;

			response.ShouldEqual(string.Empty);
		}

		[Test]
		public void Assist_InviteJobSeekerToApply_LogAction_Success()
		{
			// Arrange
			RuntimeContext.AppSettings.IntegrationClient = Client;
			
			// Act
			var response = _testIntegrationRepository.GetActionData(ActionTypes.InviteJobSeekerToApply, 1097621, 2);
			RuntimeContext.AppSettings.IntegrationClient = IntegrationClient.Standalone;

			// Assert
			var model = JsonConvert.DeserializeObject<CharacteristicsModel>(response);
			model.ServiceId.ShouldEqual(ActionTypes.InviteJobSeekerToApply.ToString());
		}
	}
}
