﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using Focus.Core;
using Focus.Core.Messages.AuthenticationService;
using Focus.Services.ServiceImplementations;
using Focus.UnitTests.Core;
using Framework.Testing.NUnit;
using AcknowledgementType = Focus.Core.Messages.AcknowledgementType;

using NUnit.Framework;

#endregion

namespace Focus.UnitTests.ServicesHost
{
  [TestFixture]
  public class AccessTokenTests : TestFixtureBase
  {
    private AuthenticationService _service;

    [SetUp]
    public override void SetUp()
    {
      base.SetUp();

      SetUpCacheProvider();
      SetUpMockLogProvider();

      _service = new AuthenticationService(RuntimeContext);
    }

    [Test]
    public void GetCareerUserData_WhenUsingValidAccessToken_ReturnsSuccess()
    {
      var request = new CareerUserDataRequest();
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
      request.AccessToken = AccessTokenUtils.GenerateToken("Dummy Key", "Dummy Secret"); 

      var appSettings = MockAppSettings as FakeAppSettings;
      if (appSettings != null)
        appSettings.EnableAccessTokens("Dummy Key", "Dummy Secret");

      // Act
      var response = _service.GetCareerUserData(request);

      if (appSettings != null)
        appSettings.DisableAccessTokens();

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
    }

    [Test]
    public void GetCareerUserData_WhenUsingAccessTokenWithInvalidSecret_ReturnsFailure()
    {
      var request = new CareerUserDataRequest();
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
      request.AccessToken = AccessTokenUtils.GenerateToken("Dummy Key", "Invalid Secret");

      var appSettings = MockAppSettings as FakeAppSettings;
      if (appSettings != null)
        appSettings.EnableAccessTokens("Dummy Key", "Dummy Secret");

      // Act
      var response = _service.GetCareerUserData(request);

      if (appSettings != null)
        appSettings.DisableAccessTokens();

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
    }

    [Test]
    public void GetCareerUserData_WhenAccessTokensNotUsed_ReturnsSuccess()
    {
      var request = new CareerUserDataRequest();
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.GetCareerUserData(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
    }

    [TearDown]
    public override void TearDown()
    {
      base.TearDown();

      var appSettings = MockAppSettings as FakeAppSettings;
      if (appSettings == null) return;

      appSettings.DisableAccessTokens();
    }
  }
}
