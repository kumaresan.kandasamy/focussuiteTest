﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using Focus.Core;
using Focus.Core.Messages.AuthenticationService;
using Focus.Services.ServiceImplementations;
using Focus.UnitTests.Core;
using Framework.Testing.NUnit;
using AcknowledgementType = Focus.Core.Messages.AcknowledgementType;

using NUnit.Framework;

#endregion

namespace Focus.UnitTests.ServicesHost
{
  [TestFixture]
  public class VersionNumberTests : TestFixtureBase
  {
    private AuthenticationService _service;

    [SetUp]
    public override void SetUp()
    {
      base.SetUp();

      SetUpCacheProvider();
      SetUpMockLogProvider();

      _service = new AuthenticationService(RuntimeContext);
    }

    [Test]
    public void GetCareerUserData_WhenUsingValidVersionNumber_ReturnsSuccess()
    {
      var request = new CareerUserDataRequest();
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
      request.VersionNumber = Constants.SystemVersion;

      var appSettings = MockAppSettings as FakeAppSettings;
      if (appSettings != null)
        appSettings.ValidateServiceVersion = true;

      // Act
      var response = _service.GetCareerUserData(request);

      if (appSettings != null)
        appSettings.ValidateServiceVersion = false;

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
    }

    [Test]
    public void GetCareerUserData_WhenUsingInvalidVersionNumbert_ReturnsFailure()
    {
      var request = new CareerUserDataRequest();
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
      request.VersionNumber = "1.00.0000";

      var appSettings = MockAppSettings as FakeAppSettings;
      if (appSettings != null)
        appSettings.ValidateServiceVersion = true;

      // Act
      var response = _service.GetCareerUserData(request);

      if (appSettings != null)
        appSettings.ValidateServiceVersion = false;

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
      response.Message.ShouldEqual("[ErrorType.IncorrectVersionNumber]");
    }


    [TearDown]
    public override void TearDown()
    {
      base.TearDown();

      var appSettings = MockAppSettings as FakeAppSettings;
      if (appSettings == null) return;

      appSettings.ValidateServiceVersion = false;
    }
  }
}

