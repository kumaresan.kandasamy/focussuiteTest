﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.ObjectModel;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Configuration;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;

using Focus.Services;
using Focus.Services.Core;
using Focus.UnitTests.Core;
using Framework.Testing.NUnit;
using Framework.ServiceLocation;

#endregion

namespace Focus.UnitTests.ServicesHost
{
  public class ServiceMessageInspector : TestFixtureBase, IDispatchMessageInspector
  {
    public bool EnableProfiling;

    public ServiceMessageInspector(bool enableProfiling)
    {
      EnableProfiling = enableProfiling;
    }

    public object AfterReceiveRequest(ref Message request, IClientChannel channel, InstanceContext instanceContext)
    {
      DoSetup(true, true);

      var appSettings = MockAppSettings as FakeAppSettings;
      if (appSettings != null)
        appSettings.ProfilingEnabled = EnableProfiling;

      return null;
    }

    public void BeforeSendReply(ref Message reply, object correlationState)
    {
      var appSettings = MockAppSettings as FakeAppSettings;
      if (appSettings != null)
        appSettings.ProfilingEnabled = false;

      TearDown();
    }
  }

  [AttributeUsage(AttributeTargets.Class)]
  public class ServiceOutputBehavior : Attribute, IServiceBehavior
  {
    public bool EnableProfiling;

    public ServiceOutputBehavior(bool enableProfiling)
    {
      EnableProfiling = enableProfiling;
    }

    public void AddBindingParameters(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase, Collection<ServiceEndpoint> endpoints, BindingParameterCollection bindingParameters)
    {
    }

    public void ApplyDispatchBehavior(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
    {
      foreach (ChannelDispatcherBase t in serviceHostBase.ChannelDispatchers)
      {
        var channelDispatcher = t as ChannelDispatcher;
        if (channelDispatcher != null)
        {
          foreach (var endpointDispatcher in channelDispatcher.Endpoints)
          {
            var inspector = new ServiceMessageInspector(EnableProfiling);
            endpointDispatcher.DispatchRuntime.MessageInspectors.Add(inspector);
          }
        }
      }
    }

    public void Validate(ServiceDescription serviceDescription, ServiceHostBase serviceHostBase)
    {
    }
  }
}
