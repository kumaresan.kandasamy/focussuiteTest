﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.ServiceModel;
using System.ServiceModel.Channels;

using Focus.Core;
using Focus.Core.Messages.AuthenticationService;
using Focus.Core.Models;
using Focus.Services.ServiceContracts;
using Focus.Services.ServiceImplementations;
using Focus.UnitTests.Core;
using Framework.Testing.NUnit;
using AcknowledgementType = Focus.Core.Messages.AcknowledgementType;

using NUnit.Framework;

#endregion

namespace Focus.UnitTests.ServicesHost
{
  [TestFixture]
  public class ProfilingTest : TestFixtureBase
  {
    private ServiceHost _serviceHost;
    private ChannelFactory<IAuthenticationService> _channelFactory;
    private IAuthenticationService _channel;

    [SetUp]
    public override void SetUp()
    {
      MockUserData = new UserContext
      {
        UserId = MockUserId,
        ActionerId = MockUserId,
        FirstName = MockFirstName,
        LastName = MockLastName,
        EmailAddress = MockEmailAddress,
        Culture = MockCulture,
        EmployerId = MockEmployerId,
        EmployeeId = MockEmployeeId,
        PersonId = MockPersonId,
        EnrollmentStatus = SchoolStatus.NA
      };
    }

    public void WcfSetUp(bool enableProfiling)
    {
      var uri = new Uri("http://localhost:8732/Design_Time_Addresses/AuthenticationService.svc");
      _serviceHost = new ServiceHost(typeof(AuthenticationService), uri);
      _serviceHost.AddServiceEndpoint(typeof(IAuthenticationService), new BasicHttpBinding(), uri);

      var smb = new ServiceOutputBehavior(enableProfiling);
      _serviceHost.Description.Behaviors.Add(smb);

      _serviceHost.Open();

      _channelFactory = new ChannelFactory<IAuthenticationService>(new BasicHttpContextBinding());
      _channel = _channelFactory.CreateChannel(new EndpointAddress(uri));
    }

    [Ignore]
    [Test]
    public void StartSession_WhenProfilingDisbled_ReturnsSuccess()
    {
      WcfSetUp(false);

      var request = new SessionRequest();
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
      var response = _channel.StartSession(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
    }

    [Ignore]
    [Test]
    public void StartSession_WhenProfilingEnabled_ReturnsSuccess()
    {
      WcfSetUp(true);

      var request = new SessionRequest();
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
      var response = _channel.StartSession(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
    }

    [TearDown]
    public override void TearDown()
    {
      _channelFactory.Close();
      ((IChannel)_channel).Close();

      _serviceHost.Close();
    }
  }
}
