﻿using System;
using System.Collections.Generic;
using Focus.Core;
using Focus.Core.Messages.CoreService;
using Focus.Core.Models;
using Focus.Core.Settings;
using Focus.Core.Settings.Interfaces;
using Focus.Core.Views;
using Focus.UnitTests.Core;
using Framework.Core;
using Framework.Logging;
using Moq;
using NUnit.Framework;

using Focus.Services.Core;
using Focus.Services.ServiceContracts;
using Focus.Services.Views;
using Framework.Testing.NUnit;
using Framework.ServiceLocation;
using Mindscape.LightSpeed;
using Focus.Data.Configuration.Entities;
using Focus.Data.Repositories;

namespace Focus.UnitTests.Services
{
	[TestFixture]
	public class CoreTests
	{
		private Mock<ICoreService> _mockCoreService;
		private string _mockClientTag;
		private UserContext _mockUserData;
		private string _mockCulture;
		private readonly Guid _mockSessionId = Guid.Empty;
		protected IConfigurationHelper ConfigurationHelper;
		public static IAppSettings MockAppSettings = new FakeAppSettings();
		protected SimpleUnitOfWorkScope<ConfigurationUnitOfWork> ConfigurationUnitOfWorkScope;
		private static readonly LightSpeedContext<ConfigurationUnitOfWork> ConfigurationContext = new LightSpeedContext<ConfigurationUnitOfWork>("Configuration");

		[SetUp]
    public void SetUp()
		{
			_mockClientTag = "ABC123";

			_mockUserData = new UserContext();
			_mockUserData.Culture = _mockCulture = "EN-GB";
			_mockUserData.EmailAddress = "test@test.com";
			_mockUserData.FirstName = "FirstName";
			_mockUserData.LastName = "LastName";
			ConfigurationUnitOfWorkScope = new SimpleUnitOfWorkScope<ConfigurationUnitOfWork>(ConfigurationContext);
			ConfigurationHelper =
				new ConfigurationHelper(new TestRuntimeContext
				{
					AppSettings = MockAppSettings,
					Repositories = new TestRepositories()
					{
						Configuration = new ConfigurationRepository(ConfigurationUnitOfWorkScope)
					},
					Helpers = new TestHelpers()
				});

			var serviceLocator = new StructureMapServiceLocator();
			ServiceLocator.SetServiceLocator(serviceLocator);
			serviceLocator.Register<IConfigurationHelper>(ConfigurationHelper);

			var request = new GetConfigurationRequest().Prepare(_mockClientTag, _mockSessionId, _mockUserData, _mockCulture);
			var response = new GetConfigurationResponse(request)
			{
				ConfigurationItems = new List<ConfigurationItemView> {new ConfigurationItemView { Key = "SmtpServer", Value = "TestSmtp" }}
			};

			_mockCoreService = new Mock<ICoreService>();
      _mockCoreService.Setup(x => x.GetConfigurationItems(It.IsAny<GetConfigurationRequest>())).Returns(response);
		}

		[Test]
		public void ConfigurationItem_WhenRequestingValidConfigItems_KeyReturnsExpectedValue()
		{
			// Arrange
			// Handled By Setup

			//Act
      var appSettings = new AppSettings(_mockCoreService.Object.GetConfigurationItems(new GetConfigurationRequest()).ConfigurationItems);

			//Assert
			appSettings.SmtpServer.ShouldEqual("TestSmtp");
		}

		[Test]
    public void ConfigurationItem_WhenRequestingConfigItemNotInDatabase_KeyReturnsDefaultValue()
		{
			// Arrange
			var request = new GetConfigurationRequest().Prepare(_mockClientTag, _mockSessionId, _mockUserData, _mockCulture);

			var response = new GetConfigurationResponse(request)
			{
				ConfigurationItems = new List<ConfigurationItemView> { new ConfigurationItemView { Key = "SomeKey", Value = "SomeValue" } }
			};

			_mockCoreService = new Mock<ICoreService>();
      _mockCoreService.Setup(x => x.GetConfigurationItems(It.IsAny<GetConfigurationRequest>())).Returns(response);

			//Act
      var appSettings = new AppSettings(_mockCoreService.Object.GetConfigurationItems(new GetConfigurationRequest()).ConfigurationItems);

			//Assert
			appSettings.SmtpServer.ShouldEqual("NOTSET");
		}

		[Test]
    public void ConfigurationItem_WhenRequestingValidWebConfigItems_KeyReturnsExpectedValue()
		{
			// Arrange
			// Handled By Setup

			// NOTE: This will work against the app.config in this project
			
			//Act
      var appSettings = new AppSettings(_mockCoreService.Object.GetConfigurationItems(new GetConfigurationRequest()).ConfigurationItems);

			//Assert
			appSettings.LogSeverity.ShouldEqual(LogSeverity.Error);
		}

		[Test]
    public void ConfigurationItem_WhenRequestingUndefinedWebConfigItems_KeyReturnsExpectedValue()
		{
			// Arrange
			//Handled by SetUp

			// NOTE: This will work against the app.config in this project

			//Act
      var appSettings = new AppSettings(_mockCoreService.Object.GetConfigurationItems(new GetConfigurationRequest()).ConfigurationItems);

			//Assert
			appSettings.ProfilingEnabled.ShouldEqual(false);
		}

		[Test]
		public void SerailizeDrivingLicenceEndorsementRules_WhenPassedDefaultRules_ReturnsJson()
		{
			// Arrange

			// Act
			var json = Defaults.ConfigurationItemDefaults.DefaultDrivingLicenceEndorsementRules.SerializeJson();

			// Assert
			json.ShouldNotBeNullOrEmpty();
		}

		[Test]
		public void DeserailizeDrivingLicenceEndorsementRules_WhenPassedValidJson_ReturnsRules()
		{
			// Arrange
			var json = "[{\"LicenceKey\":\"ClassD\",\"EndorsementKeys\":[\"Motorcycle\",\"SchoolBus\"]},{\"LicenceKey\":\".Motorcycle\",\"EndorsementKeys\":[\"Motorcycle\"]},{\"LicenceKey\":\"ClassA\",\"EndorsementKeys\":[\"PassTransport\",\"DoublesTriples\",\"HazerdousMaterials\",\"Airbrakes\",\"TankVehicle\",\"Motorcycle\",\"SchoolBus\",\"TankHazard\"]},{\"LicenceKey\":\"ClassB\",\"EndorsementKeys\":[\"PassTransport\",\"DoublesTriples\",\"HazerdousMaterials\",\"Airbrakes\",\"TankVehicle\",\"Motorcycle\",\"SchoolBus\",\"TankHazard\"]},{\"LicenceKey\":\"ClassC\",\"EndorsementKeys\":[\"PassTransport\",\"DoublesTriples\",\"HazerdousMaterials\",\"Airbrakes\",\"TankVehicle\",\"Motorcycle\",\"SchoolBus\",\"TankHazard\"]}]";

			// Act
			var rules = json.DeserializeJson<DrivingLicenceEndorsementRuleList>();

			// Assert
			rules.ShouldNotBeNull(); 
		}
	}	
}
