﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using Focus.Services.Messages;
using Focus.Services.Messages.Handlers;
using Focus.UnitTests.Core;
using Framework.Testing.NUnit;
using NUnit.Framework;

#endregion

namespace Focus.UnitTests.Services.MessageHandlers
{
	class ProcessStatisticsMessageHandlerTests : TestFixtureBase
	{
		[Ignore]
		[Test]
		[Explicit]
		public void HandleWithValidMessage()
		{
			// Arrange
			var message = new ProcessStatisticsMessage
			{
				Id = Guid.NewGuid()
			};

			var messageHandler = new ProcessStatisticsMessageHandler(RuntimeContext);

			// Act	
			messageHandler.Handle(message);

			// Assert 
			var statisticsCount = ReportRepository.Statistics.Count();

			statisticsCount.ShouldEqual(4);
		}
	}
}
