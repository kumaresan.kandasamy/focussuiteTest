﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using Focus.Core;
using Focus.Services.Messages;
using Focus.Services.Messages.Handlers;
using Focus.UnitTests.Core;
using Framework.Testing.NUnit;
using NUnit.Framework;

#endregion


namespace Focus.UnitTests.Services.MessageHandlers
{
	class UpdateReportingMessageHandlerTests : TestFixtureBase
	{
		[SetUp]
		public override void SetUp()
		{
			DoSetup(false);

			SetUpCacheProvider();
			SetUpMockLogProvider();
		}

		[Test]
		public void HandleWithValidMessage()
		{
			// Arrange
			var entityIds = new List<Tuple<long, long?>> { Tuple.Create((long)1183339, (long?)null) };

			var message = new UpdateReportingMessage
			{
				Id = Guid.NewGuid(),
				ActionerId = 1,
				Culture = "**_**",
				EntityType = ReportEntityType.JobSeeker,
				EntityIds = entityIds,
				EntityDeletion = false,
				ResetAll = false
			};

			var messageHandler = new UpdateReportingMessageHandler(RuntimeContext);

			// Act	
			messageHandler.Handle(message);

			var jobseeker = ReportRepository.JobSeekers.FirstOrDefault(x => x.FocusPersonId == (long)1183339);
			var jobSeekerData = jobseeker.JobSeekerData;
			var jobSeekerJobHistory = jobseeker.JobSeekerJobHistories;

			// Assert 
		}
	}
}
