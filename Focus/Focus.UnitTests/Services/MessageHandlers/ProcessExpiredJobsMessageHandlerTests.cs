﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using Focus.Core;
using Focus.Services.Messages;
using Focus.Services.Messages.Handlers;
using Focus.UnitTests.Core;
using Framework.Testing.NUnit;

using NUnit.Framework;

#endregion

namespace Focus.UnitTests.Services.MessageHandlers
{
	public class ProcessExpiredJobsMessageHandlerTests : TestFixtureBase
	{
		[Test]
		[Explicit]
		public void HandleWithValidMessageAllExpiredJobsAreClosed()
		{
			// Arrange
			var expiryDate = DateTime.Now.Date.AddDays(-1);
			var expiredJobsCount = RuntimeContext.Repositories.Core.Jobs.Count(x => x.ClosingOn != null && x.ClosingOn <= expiryDate && x.JobStatus == JobStatuses.Active);

			var message = new ProcessExpiredJobsMessage
			              	{
			              		Id = Guid.NewGuid(),
			              		ExpiryDays = 1
			              	};

			var messageHandler = new ProcessExpiredJobsMessageHandler(RuntimeContext);

			// Act	
			messageHandler.Handle(message);

			// Assert 
			var messageCount = MessageCount(typeof(CloseExpiredJobMessage));
			messageCount.ShouldEqual(expiredJobsCount);
		}
	}
}
