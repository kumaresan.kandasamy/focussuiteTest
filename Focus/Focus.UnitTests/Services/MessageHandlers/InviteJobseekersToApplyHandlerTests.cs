﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using Focus.Data.Core.Entities;
using Focus.Data.Repositories.Contracts;
using Focus.Services.Messages;
using Focus.Services.Messages.Handlers;
using Focus.UnitTests.Core;
using Framework.Testing.NUnit;
using Moq;
using NUnit.Framework;

#endregion



namespace Focus.UnitTests.Services.MessageHandlers
{
	public class InviteJobseekersToApplyHandlerTests : TestFixtureBase
	{
		[Test]
		public void HandleWithValidMessageWhereNoPreviousInvites()
		{
			// Arrange
			var message = new InviteJobseekersToApplyMessage
			{
				ActionerId = MockUserId,
				Culture = "en-GB",
				Id = Guid.NewGuid(),
				PersonIds = new List<long> { 1, 2, 3 },
				JobId = 4,
				JobLink = "www.google.com",
				LensPostingId = "ABCD_1234",
				SenderEmail = "a@b.com",
				SendersName = "A Person"
			};

			var messageHandler = new InviteJobseekersToApplyHandler(RuntimeContext);

			// Act	
			messageHandler.Handle(message);

			// Assert 
		}

		[Test]
		public void HandleWithValidMessageWhereTwoPreviousInvites()
		{
			// Arrange
			var message = new InviteJobseekersToApplyMessage
			{
				ActionerId = MockUserId,
				Culture = "en-GB",
				Id = Guid.NewGuid(),
				PersonIds = new List<long> { 1, 2, 3 },
				JobId = 4,
				JobLink = "www.google.com",
				LensPostingId = "ABCD_1234",
				SenderEmail = "a@b.com",
				SendersName = "A Person"
			};

			var mockUsers = new List<User>
			{
				new User() {Id = 1, UserName = "IntegrationUser"}
			};

			var previousInvites = new List<InviteToApply>
			{
				new InviteToApply {PersonId = 2, LensPostingId = message.LensPostingId},
				new InviteToApply {PersonId = 1, LensPostingId = message.LensPostingId}
			};

			var coreRepositoryMock = new Mock<ICoreRepository>();

			coreRepositoryMock.Setup(y => y.Users).Returns(mockUsers.AsQueryable());
			coreRepositoryMock.Setup(y => y.InviteToApply).Returns(previousInvites.AsQueryable());
			((TestRepositories)RuntimeContext.Repositories).Core = coreRepositoryMock.Object;

			var messageHandler = new InviteJobseekersToApplyHandler(RuntimeContext);

			// Act	
			messageHandler.Handle(message);

			// Assert 
		}

		[Test]
		public void HandleWithValidMessageWhereAllPreviousInvites()
		{
			// Arrange
			var message = new InviteJobseekersToApplyMessage
			{
				ActionerId = MockUserId,
				Culture = "en-GB",
				Id = Guid.NewGuid(),
				PersonIds = new List<long> { 1, 2, 3 },
				JobId = 4,
				JobLink = "www.google.com",
				LensPostingId = "ABCD_1234",
				SenderEmail = "a@b.com",
				SendersName = "A Person"
			};

			var mockUsers = new List<User>
			{
				new User() {Id = 1, UserName = "IntegrationUser"}
			};

			var previousInvites = new List<InviteToApply>
			{
				new InviteToApply {PersonId = 2, LensPostingId = message.LensPostingId},
				new InviteToApply {PersonId = 1, LensPostingId = message.LensPostingId},
				new InviteToApply {PersonId = 3, LensPostingId = message.LensPostingId},
			};

			var coreRepositoryMock = new Mock<ICoreRepository>();

			coreRepositoryMock.Setup(y => y.Users).Returns(mockUsers.AsQueryable());
			coreRepositoryMock.Setup(y => y.InviteToApply).Returns(previousInvites.AsQueryable());
			((TestRepositories)RuntimeContext.Repositories).Core = coreRepositoryMock.Object;

			var messageHandler = new InviteJobseekersToApplyHandler(RuntimeContext);

			// Act	
			messageHandler.Handle(message);

			// Assert 
		}
	}
}
