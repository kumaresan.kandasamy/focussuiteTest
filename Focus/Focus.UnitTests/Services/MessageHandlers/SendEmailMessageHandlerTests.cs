﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Net.Mail;
using Focus.Services.Core;
using Focus.Services.Messages;
using Focus.Services.Messages.Handlers;
using Focus.UnitTests.Core;
using Framework.Testing.NUnit;
using NUnit.Framework;

#endregion

namespace Focus.UnitTests.Services.MessageHandlers
{
	public class SendEmailMessageHandlerTests : TestFixtureBase
	{
		[Test]
		public void HandleWithValidMailMessage()
		{
			// Arrange
			var mailMessage = new MailMessage();
			mailMessage.From = new MailAddress("bob@test.com");
			mailMessage.To.Add(new MailAddress("ajones@burning-glass.com"));
			mailMessage.Subject = "Test Email";
			mailMessage.Body = "Test email body";

			var message = new SendEmailMessage
			              	{
												From = "person@site.com",
			              		Id = Guid.NewGuid(),
			              		ActionerId = 1,
			              		Culture = "**_**",
			              		MailMessage = mailMessage,
			              		SendAsync = false,
			              		IsBodyHtml = false
			              	};

			var messageHandler = new SendEmailMessageHandler(RuntimeContext);

			// Act	
			messageHandler.Handle(message);

			// Assert 
		}

		[Test]
		public void HandleWithValidMessage()
		{
			// Arrange
			var message = new SendEmailMessage
			{
				From = "test@email.com",
				Id = Guid.NewGuid(),
				ActionerId = 1,
				Culture = "**_**",
				To = "ajones@burning-glass.com",
				Subject = "Test Email",
				Body = "Test email body",
				SendAsync = false,
				IsBodyHtml = false
			};

			var messageHandler = new SendEmailMessageHandler(RuntimeContext);

			// Act	
			messageHandler.Handle(message);

			// Assert 
		}

		[Test]
		public void HandleWithValidMailMessageAndOverriddenFrom()
		{
			// Arrange
			var mailMessage = new MailMessage();
			mailMessage.From = new MailAddress("bob@test.com");
			mailMessage.To.Add(new MailAddress("ajones@burning-glass.com"));
			mailMessage.Subject = "Test Email";
			mailMessage.Body = "Test email body";

			var message = new SendEmailMessage
			{
				Id = Guid.NewGuid(),
				ActionerId = 2,
				Culture = "**_**",
				MailMessage = mailMessage,
				SendAsync = false,
				IsBodyHtml = false
			};

			var messageHandler = new SendEmailMessageHandler(RuntimeContext);

			// Act	
			messageHandler.Handle(message);

			// Assert 
		}

		[Test]
		public void HandleWithValidMessageAndOverriddenFrom()
		{
			// Arrange
			var message = new SendEmailMessage
			{
				Id = Guid.NewGuid(),
				ActionerId = 2,
				Culture = "**_**",
				To = "ajones@burning-glass.com",
				Subject = "Test Email",
				Body = "Test email body",
				SendAsync = false,
				IsBodyHtml = false
			};

			var messageHandler = new SendEmailMessageHandler(RuntimeContext);

			// Act	
			messageHandler.Handle(message);

			// Assert 
		}
	}
}
