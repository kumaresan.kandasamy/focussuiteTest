﻿#region Copyright © 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;

using Focus.Core;
using Focus.Services.Messages;
using Focus.Services.Messages.Handlers;
using Focus.UnitTests.Core;
using Focus.Web.Code;
using Framework.Core;
using Framework.Testing.NUnit;

using NUnit.Framework;

#endregion


namespace Focus.UnitTests.Services.MessageHandlers
{
	class ProcessRegistrationPinEmailChildMessageHandlerTest : TestFixtureBase
	{
		[Test]
		public void HandleWithValidMessage()
		{
			const string email = "unittest@bgt.com";

			var message = new ProcessRegistrationPinEmailChildMessage
			{
				Id = Guid.NewGuid(),
				ActionerId = 1,
				Culture = "**_**",
				Email = email,
				RegistrationUrl = MockAppSettings.CareerApplicationPath + UrlBuilder.PinRegistration(false),
				TargetModule = FocusModules.CareerExplorer,

			};

			var messageHandler = new ProcessRegistrationPinEmailChildMessageHandler(RuntimeContext);

			// Act	
			messageHandler.Handle(message);

			// Assert 
			var pinRecord = DataCoreRepository.RegistrationPins.FirstOrDefault(p => p.EmailAddress == email);
			pinRecord.ShouldNotBeNull();
			if (pinRecord.IsNotNull())
			{
				pinRecord.Pin.ShouldNotBeNull();
				pinRecord.CreatedOn.Date.ShouldEqual(DateTime.Now.Date);
			}
		}
	}
}
