﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Linq;
using Focus.Core;
using Focus.Services.Messages;
using Focus.Services.Messages.Handlers;
using Focus.UnitTests.Core;
using Framework.Testing.NUnit;
using NUnit.Framework;

#endregion

namespace Focus.UnitTests.Services.MessageHandlers
{
	public class CloseExpiredJobMessageHandlerTests : TestFixtureBase
	{
		[Test]
		public void HandleWithValidMessageJobClosed()
		{
			var jobId = 1092076;

			// Arrange
			var message = new CloseExpiredJobMessage
			{
				Culture = "**_**",
				JobId = jobId
			};

			var messageHandler = new CloseExpiredJobMessageHandler(RuntimeContext);

			// Act	
			messageHandler.Handle(message);

			// Assert 
			var jobStatus = RuntimeContext.Repositories.Core.Jobs.Where(x => x.Id == jobId).Select(x => x.JobStatus).FirstOrDefault();
			jobStatus.ShouldNotBeNull();
			jobStatus.ShouldEqual(JobStatuses.Closed);
		}

		[Test]
		public void HandleWithValidMessagePostingUnregistered()
		{
			// Arrange
			var message = new CloseExpiredJobMessage
			{
				Culture = "**_**",
				JobId = 1092076
			};

			var messageHandler = new CloseExpiredJobMessageHandler(RuntimeContext);

			// Act	
			messageHandler.Handle(message);

			// Assert 
			var messageCount = MessageCount(typeof (ProcessPostingMessage));
			messageCount.ShouldEqual(1);
		}

		[Test]
		public void HandleWithValidMessageEmailSent()
		{
			// Arrange
			var message = new CloseExpiredJobMessage
			{
				Culture = "**_**",
				JobId = 1092076
			};

			var messageHandler = new CloseExpiredJobMessageHandler(RuntimeContext);

			// Act	
			messageHandler.Handle(message);

			// Assert
			var messageCount = MessageCount(typeof(SendEmailMessage));
			messageCount.ShouldEqual(1);
		}

		[Test]
		public void HandleWithValidMessageWithNoPendingReferralsNoReferralsPutOnHold()
		{
			// Arrange
			var message = new CloseExpiredJobMessage
			{
				Culture = "**_**",
				JobId = 1092076
			};

			var messageHandler = new CloseExpiredJobMessageHandler(RuntimeContext);

			// Act	
			messageHandler.Handle(message);

			// Assert
			var messageCount = MessageCount(typeof(HoldReferralMessage));
			messageCount.ShouldEqual(0);
		}

		[Test]
		public void HandleWithValidMessageWithTwoPendingReferralsTwoReferralsPutOnHold()
		{
			// Arrange
			var message = new CloseExpiredJobMessage
			{
				Culture = "**_**",
				JobId = 1087360
			};

			var messageHandler = new CloseExpiredJobMessageHandler(RuntimeContext);

			// Act	
			messageHandler.Handle(message);

			// Assert
			var messageCount = MessageCount(typeof(HoldReferralMessage));
			messageCount.ShouldEqual(3);
		}

		[Test]
		[ExpectedException]
		public void HandleWithNoJobIdThrowsException()
		{
			// Arrange
			var message = new CloseExpiredJobMessage
			{
				Culture = "**_**",
			};

			var messageHandler = new CloseExpiredJobMessageHandler(RuntimeContext);

			// Act	
			messageHandler.Handle(message);

			// Assert 
		}

		[Test]
		[ExpectedException]
		public void HandleWithInvalidJobIdThrowsException()
		{
			// Arrange
			var message = new CloseExpiredJobMessage
			{
				Culture = "**_**",
				JobId = 0
			};

			var messageHandler = new CloseExpiredJobMessageHandler(RuntimeContext);

			// Act	
			messageHandler.Handle(message);

			// Assert 
		}
	}
}
