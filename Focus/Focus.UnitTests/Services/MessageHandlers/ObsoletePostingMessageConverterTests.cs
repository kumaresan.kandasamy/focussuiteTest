﻿#region Copyright © 2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives


using Focus.Services.Messages;
using Focus.Services.Messages.Handlers;
using Focus.UnitTests.Core;
using Framework.Testing.NUnit;
using NUnit.Framework;

#endregion

namespace Focus.UnitTests.Services.MessageHandlers
{
	public class ObsoletePostingMessageConverterTests : TestFixtureBase
	{
		[Test]
		public void ConvertUnregisterPostingMessageConvertsToProcessPostingMessage()
		{
			var jobId = 1092076;

			// Arrange
			var message = new UnregisterPostingMessage
			{
				Culture = "**_**",
				PostingId = jobId
			};

			var messageHandler = new UnregisterPostingMessageHandler(RuntimeContext);

			// Act	
			messageHandler.Handle(message);

			// Assert 
			var messageCount = MessageCount(typeof(ProcessPostingMessage));
			messageCount.ShouldEqual(1);
		}
		[Test]
		public void ConvertRegisterPostingMessageConvertsToProcessPostingMessage()
		{
			var jobId = 1092076;

			// Arrange
			var message = new RegisterPostingMessage
			{
				Culture = "**_**",
				PostingId = jobId
			};

			var messageHandler = new RegisterPostingMessageHandler(RuntimeContext);

			// Act	
			messageHandler.Handle(message);

			// Assert 
			var messageCount = MessageCount(typeof(ProcessPostingMessage));
			messageCount.ShouldEqual(1);
		}
	}
}
