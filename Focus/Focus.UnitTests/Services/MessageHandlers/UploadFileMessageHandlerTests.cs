﻿#region Copyright © 2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using Focus.Core;
using Focus.Core.Models.Upload;
using Focus.Core.Models.Validation;
using Focus.Services.Messages;
using Focus.Services.Messages.Handlers;
using Focus.UnitTests.Core;
using Framework.Testing.NUnit;
using NUnit.Framework;

#endregion

namespace Focus.UnitTests.Services.MessageHandlers
{
  public class UploadFileMessageHandlerTests : TestFixtureBase
  {
	  private struct UploadFileData
	  {
		  public string JobTitle;
			public string Description;
			public string Zip;
			public int NumberOfOpenings;
			public DateTime ClosingDate;
			public bool ApplicantCreditCheck;
			public bool ApplicantCriminalCheck;
		  public List<FieldValidationResult> ValidationResult;
	  }

    [Test]
    public void HandleWithValidMessageForValidation()
    {
      var message = new UploadFileMessage
      {
        ActionerId = MockUserId,
        Culture = "en-GB",
        Id = Guid.NewGuid(),
        UploadFileId = 7057745,
        ProcessingState = UploadFileProcessingState.RequiresValidation,
        IgnoreHeader = true
      };

      var messageHandler = new UploadFileMessageHandler(RuntimeContext);

      messageHandler.Handle(message);

      var uploadFile = DataCoreRepository.UploadFiles.First(file => file.Id == message.UploadFileId);

      uploadFile.ShouldNotBeNull();
      uploadFile.ProcessingState.ShouldEqual(UploadFileProcessingState.Validated);
      uploadFile.UploadRecords.Count.ShouldEqual(6);
    }

    [Test]
    public void HandleWithValidMessageForCommit()
    {
      var message = new UploadFileMessage
      {
        ActionerId = MockUserId,
        Culture = "en-GB",
        Id = Guid.NewGuid(),
        UploadFileId = 7057747,
        ProcessingState = UploadFileProcessingState.RequiresCommitting
      };

      var messageHandler = new UploadFileMessageHandler(RuntimeContext);

      messageHandler.Handle(message);

      var uploadFile = DataCoreRepository.UploadFiles.First(file => file.Id == message.UploadFileId);

      uploadFile.ShouldNotBeNull();
      uploadFile.ProcessingState.ShouldEqual(UploadFileProcessingState.Committed);
    }

	  private static void CheckFieldValue(object uploadedValue, FieldValidationResult uploadedValidationResult, object expectedValue, FieldValidationResult expectedValidationResult, string errorMessage, string validationErrorMessage)
	  {
		  uploadedValidationResult.Status.ShouldEqual(expectedValidationResult.Status, string.Format(validationErrorMessage, "Status", uploadedValidationResult.Status, expectedValidationResult.Status));
		  if (uploadedValidationResult.Status == ValidationRecordStatus.Success)
		  {
			  uploadedValue.ShouldEqual(expectedValue, string.Format(errorMessage, uploadedValue, expectedValue));
		  }
		  else
		  {
			  uploadedValidationResult.Message.ShouldEqual(expectedValidationResult.Message, string.Format(validationErrorMessage, "Validation Message", uploadedValidationResult.Message, expectedValidationResult.Message));
			  uploadedValidationResult.OriginalValue.ShouldEqual(expectedValidationResult.OriginalValue, string.Format(validationErrorMessage, "Original Value", uploadedValidationResult.OriginalValue, expectedValidationResult.OriginalValue));
		  }
	  }

	  private static void CheckUploadedRow(JobUploadRecord uploadedRow, IList<FieldValidationResult> validationResults, UploadFileData expectedRow, int index)
	  {
			var validationErrorMessage = "Validation Details at row " + index + " differ: {0} was '{1}', should be '{2}'.";
		  var fieldNames = new[] { "Job Title", "Description", "Zip", "Number of Openings", "Closing Date", "Applicant Credit Check", "Applicant Criminal Check" };
		  object[] uploadedValues = { uploadedRow.JobTitle, uploadedRow.Description, uploadedRow.Zip, uploadedRow.NumberOfOpenings, uploadedRow.ClosingOn, uploadedRow.ApplicantCreditCheck, uploadedRow.ApplicantCriminalCheck };
		  object[] expectedValues = { expectedRow.JobTitle, expectedRow.Description, expectedRow.Zip, expectedRow.NumberOfOpenings, expectedRow.ClosingDate, expectedRow.ApplicantCreditCheck, expectedRow.ApplicantCriminalCheck };
		  for (var i = 0; i < 7; i++)
		  {
			  var errorMessage = fieldNames[i] + " at row " + index + " was '{0}', should be '{1}'.";
				CheckFieldValue( uploadedValues[i], validationResults[i], expectedValues[i], expectedRow.ValidationResult[i], errorMessage, validationErrorMessage );
		  }
	  }

	  [Test]
		public void HandleWithValidMessageAndNewColumnsForValidation()
	  {
		  var minDate = DateTime.Now.AddDays(Convert.ToInt32(1)).ToString(Thread.CurrentThread.CurrentCulture.DateTimeFormat.ShortDatePattern);
	
			var message = new UploadFileMessage
			{
				ActionerId = MockUserId,
				Culture = CultureInfo.CurrentCulture.Name,
				Id = Guid.NewGuid(),
				UploadFileId = 7057748,
				ProcessingState = UploadFileProcessingState.RequiresValidation,
				IgnoreHeader = true
			};

			UploadFileData[] expectedData =
			{
				new UploadFileData { JobTitle = "Technical Product Manager", Description = "Manages and coordinates all the processes involved in creating a product.  Oversees work involved with product design, production, distribution, marketing and sales.", Zip = "76352", NumberOfOpenings = 2, ClosingDate = Convert.ToDateTime("01/01/2002"), ApplicantCreditCheck = false, ApplicantCriminalCheck = true, ValidationResult = new List<FieldValidationResult>
				{
					new FieldValidationResult {Status = ValidationRecordStatus.Success, Message = null}, 
					new FieldValidationResult {Status = ValidationRecordStatus.Success, Message = null}, 
					new FieldValidationResult {Status = ValidationRecordStatus.Success, Message = null}, 
					new FieldValidationResult {Status = ValidationRecordStatus.Success, Message = null}, 
					new FieldValidationResult {Status = ValidationRecordStatus.SchemaFailure, Message = string.Format("Field is less than the minimum value of {0}", minDate), OriginalValue = "01/01/2002"},
					new FieldValidationResult {Status = ValidationRecordStatus.Success, Message = null}, 
					new FieldValidationResult {Status = ValidationRecordStatus.Success, Message = null}
				} },
				new UploadFileData { JobTitle = "Sexy Girls and Boys Models", Description = "Manages and coordinates all the processes involved in creating a product.  Oversees work involved with product design, production, distribution, marketing and sales.", Zip = "76352", NumberOfOpenings = 2, ClosingDate = Convert.ToDateTime("01/01/2002"), ApplicantCreditCheck = true, ApplicantCriminalCheck = false, ValidationResult = new List<FieldValidationResult>
				{
					new FieldValidationResult {Status = ValidationRecordStatus.RedWords, Message = "Field contains red words; sexy", OriginalValue = "Sexy Girls and Boys Models"}, 
					new FieldValidationResult {Status = ValidationRecordStatus.Success, Message = null}, 
					new FieldValidationResult {Status = ValidationRecordStatus.Success, Message = null}, 
					new FieldValidationResult {Status = ValidationRecordStatus.Success, Message = null}, 
					new FieldValidationResult {Status = ValidationRecordStatus.SchemaFailure, Message = string.Format("Field is less than the minimum value of {0}", minDate), OriginalValue = "01/01/2002"},
					new FieldValidationResult {Status = ValidationRecordStatus.SchemaFailure, Message = "Invalid format", OriginalValue = "Ok"}, 
					new FieldValidationResult {Status = ValidationRecordStatus.Success, Message = null}
				} },
				new UploadFileData { JobTitle = "Escort", Description = "Manages and coordinates all the processes involved in creating a product.  Oversees work involved with product design, production, distribution, marketing and sales.", Zip = "76352", NumberOfOpenings = 2, ClosingDate = Convert.ToDateTime("01/01/2002"), ApplicantCreditCheck = true, ApplicantCriminalCheck = false, ValidationResult = new List<FieldValidationResult>
				{
					new FieldValidationResult {Status = ValidationRecordStatus.YellowWords, Message = "Field contains yellow words; escort*", OriginalValue = "Escort"}, 
					new FieldValidationResult {Status = ValidationRecordStatus.Success, Message = null}, 
					new FieldValidationResult {Status = ValidationRecordStatus.Success, Message = null}, 
					new FieldValidationResult {Status = ValidationRecordStatus.Success, Message = null}, 
					new FieldValidationResult {Status = ValidationRecordStatus.SchemaFailure, Message = string.Format("Field is less than the minimum value of {0}", minDate), OriginalValue = "01/01/2002"},
					new FieldValidationResult {Status = ValidationRecordStatus.Success, Message = null}, 
					new FieldValidationResult {Status = ValidationRecordStatus.Success, Message = null}
				} },
				new UploadFileData { JobTitle = "Technical Product Manager", Description = "Manages and coordinates all the processes involved in creating a product.  Oversees work involved with product design, production, distribution, marketing and sales.", Zip = "76352333333333", NumberOfOpenings = 0, ClosingDate = Convert.ToDateTime("01/01/2002"), ApplicantCreditCheck = false, ApplicantCriminalCheck = true, ValidationResult = new List<FieldValidationResult>
				{
					new FieldValidationResult {Status = ValidationRecordStatus.Success, Message = null}, 
					new FieldValidationResult {Status = ValidationRecordStatus.Success, Message = null}, 
					new FieldValidationResult {Status = ValidationRecordStatus.SchemaFailure, Message = "Invalid format", OriginalValue = "7.64E+13"}, 
					new FieldValidationResult {Status = ValidationRecordStatus.SchemaFailure, Message = "Invalid format", OriginalValue = "x"}, 
					new FieldValidationResult {Status = ValidationRecordStatus.SchemaFailure, Message = string.Format("Field is less than the minimum value of {0}", minDate), OriginalValue = "01/01/2002"},
					new FieldValidationResult {Status = ValidationRecordStatus.SchemaFailure, Message = "Invalid format", OriginalValue = "sure"}, 
					new FieldValidationResult {Status = ValidationRecordStatus.SchemaFailure, Message = "Invalid format", OriginalValue = "nope"}
				} },
				new UploadFileData { JobTitle = "Criminal Crime Prison Felon Ex-Felon", Description = "Manages and coordinates all the processes involved in creating a product.  Oversees work involved with product design, production, distribution, marketing and sales.", Zip = "76352", NumberOfOpenings = 2, ClosingDate = Convert.ToDateTime("01/01/2002"), ApplicantCreditCheck = true, ApplicantCriminalCheck = true, ValidationResult = new List<FieldValidationResult>
				{
					new FieldValidationResult {Status = ValidationRecordStatus.YellowWords, Message = "Field contains yellow words; criminal", OriginalValue = "Criminal Crime Prison Felon Ex-Felon"}, 
					new FieldValidationResult {Status = ValidationRecordStatus.Success, Message = null}, 
					new FieldValidationResult {Status = ValidationRecordStatus.Success, Message = null}, 
					new FieldValidationResult {Status = ValidationRecordStatus.Success, Message = null}, 
					new FieldValidationResult {Status = ValidationRecordStatus.SchemaFailure, Message = string.Format("Field is less than the minimum value of {0}", minDate), OriginalValue = "01/01/2002"},
					new FieldValidationResult {Status = ValidationRecordStatus.Success, Message = null}, 
					new FieldValidationResult {Status = ValidationRecordStatus.Success, Message = null}
				} },
				new UploadFileData { JobTitle = "Technical Product Manager", Description = "Manages and coordinates all the processes involved in creating a product.  Oversees work involved with product design, production, distribution, marketing and sales.", Zip = "0", NumberOfOpenings = 2, ClosingDate = Convert.ToDateTime("01/01/2002"), ApplicantCreditCheck = false, ApplicantCriminalCheck = false, ValidationResult = new List<FieldValidationResult>
				{
					new FieldValidationResult {Status = ValidationRecordStatus.Success, Message = null}, 
					new FieldValidationResult {Status = ValidationRecordStatus.Success, Message = null}, 
					new FieldValidationResult {Status = ValidationRecordStatus.SchemaFailure, Message = "Value is not a valid zip", OriginalValue = "00000"}, 
					new FieldValidationResult {Status = ValidationRecordStatus.Success, Message = null}, 
					new FieldValidationResult {Status = ValidationRecordStatus.SchemaFailure, Message = string.Format("Field is less than the minimum value of {0}", minDate), OriginalValue = "01/01/2002"},
					new FieldValidationResult {Status = ValidationRecordStatus.Success, Message = null}, 
					new FieldValidationResult {Status = ValidationRecordStatus.Success, Message = null}
				} },
			};

			var messageHandler = new UploadFileMessageHandler( RuntimeContext );

			messageHandler.Handle( message );

			var uploadFile = DataCoreRepository.UploadFiles.First( file => file.Id == message.UploadFileId );

			uploadFile.ShouldNotBeNull();
			uploadFile.ProcessingState.ShouldEqual( UploadFileProcessingState.Validated );
			var index = 0;
			for (; index < uploadFile.UploadRecords.Count; index++)
			{
				var record = uploadFile.UploadRecords[index];
				var uploadJobRecord  = (JobUploadRecord)record.FieldData.Deserialize(typeof(JobUploadRecord));
				var validationResults = (List<FieldValidationResult>)record.FieldValidation.Deserialize(typeof(List<FieldValidationResult>));
				CheckUploadedRow( uploadJobRecord, validationResults, expectedData[index], index );
			}
			index.ShouldEqual( 6 );
		}

		[Test]
		public void HandleWithValidMessageAndNewColumnsForCommit()
		{
			var message = new UploadFileMessage
			{
				ActionerId = MockUserId,
				Culture = "en-GB",
				Id = Guid.NewGuid(),
				UploadFileId = 7057749,
				ProcessingState = UploadFileProcessingState.RequiresCommitting
			};

			var messageHandler = new UploadFileMessageHandler( RuntimeContext );

			messageHandler.Handle( message );

			var uploadFile = DataCoreRepository.UploadFiles.First( file => file.Id == message.UploadFileId );

			uploadFile.ShouldNotBeNull();
			uploadFile.ProcessingState.ShouldEqual( UploadFileProcessingState.Committed );
		}
	}
}
