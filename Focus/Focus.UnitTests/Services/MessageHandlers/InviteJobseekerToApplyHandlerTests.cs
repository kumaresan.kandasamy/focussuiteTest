﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using Focus.Services.Messages;
using Focus.Services.Messages.Handlers;
using Focus.UnitTests.Core;
using Framework.Testing.NUnit;
using NUnit.Framework;

#endregion

namespace Focus.UnitTests.Services.MessageHandlers
{
	public class InviteJobseekerToApplyHandlerTests : TestFixtureBase
	{
		[Test]
		public void HandleWithValidMessage()
		{
			// Arrange
			var message = new InviteJobseekerToApplyMessage
			{
				ActionerId = MockUserId,
				Culture = "en-GB",
				Id = Guid.NewGuid(),
				PersonId = 1000049,
				JobId = 1109568,
				JobLink = "www.google.com",
				LensPostingId = "ABC123_5BC5BCC988B347AF96F338651FD632A1",
				SendersName = "Fred Bloggs",
				SenderEmail = "fbloggs@burning-glass.com"
			};

			var messageHandler = new InviteJobseekerToApplyHandler(RuntimeContext);

			// Act	
			messageHandler.Handle(message);

			// Assert 
		}
	}
}
