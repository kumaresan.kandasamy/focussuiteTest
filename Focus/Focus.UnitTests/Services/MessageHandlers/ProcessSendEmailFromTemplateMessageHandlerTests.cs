﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Globalization;
using Focus.Core;
using Focus.Core.EmailTemplate;
using Focus.Services.Messages;
using Focus.Services.Messages.Handlers;
using Focus.UnitTests.Core;
using Framework.Testing.NUnit;

using NUnit.Framework;

#endregion

namespace Focus.UnitTests.Services.MessageHandlers
{
	public class ProcessSendEmailFromTemplateMessageHandlerTests : TestFixtureBase
	{
		[Test]
		[Explicit]
		public void HandleWithValidMessage()
		{
			// Arrange
			var templateData = new EmailTemplateData
			{
				HiringManagerName = "Name",
				HiringManagerPhoneNumber = "11111111111",
				HiringManagerEmail = "name@site.com",
				JobPostingDate = DateTime.Today.ToString(CultureInfo.InvariantCulture),
				WOTC = "WorkOpportunitiesTaxCreditCategories.ExFelons"
			};

			var message = new SendEmailFromTemplateMessage
			{
				Id = Guid.NewGuid(),
				TemplateData = templateData,
				TemplateType = EmailTemplateTypes.HiringFromTaxCreditProgramNotification
			};

			var messageHandler = new SendEmailFromTemplateMessageHandler(RuntimeContext);

			// Act	
			messageHandler.Handle(message);

			// Assert 
		}
	}
}
