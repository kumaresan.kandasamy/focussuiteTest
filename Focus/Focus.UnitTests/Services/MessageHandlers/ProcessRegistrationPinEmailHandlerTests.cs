﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Net.Mail;
using Focus.Core;
using Focus.Services.Messages;
using Focus.Services.Messages.Handlers;
using Focus.UnitTests.Core;
using Framework.Testing.NUnit;
using Focus.Web.Code;
using NUnit.Framework;

#endregion


namespace Focus.UnitTests.Services.MessageHandlers
{
	class ProcessRegistrationPinEmailHandlerTests : TestFixtureBase
	{
		[Test]
		public void HandleWithValidMessage()
		{
			// Arrange
			var emails = new List<string> {"jane@bgt.com", "jharrison@burning-glass.com", "jim@bgt.com", "andy@bgt.com", "client@Employee.com"};


			var message = new ProcessRegistrationPinEmailMessage
			{
				Id = Guid.NewGuid(),
				ActionerId = 1,
				Culture = "**_**",
				Emails = emails,
				RegistrationUrl = MockAppSettings.CareerApplicationPath + UrlBuilder.PinRegistration(false),
				TargetModule = FocusModules.CareerExplorer
			};

			var messageHandler = new ProcessRegistrationPinEmailMessageHandler(RuntimeContext);

			// Act	
			messageHandler.Handle(message);

			// Assert 
		}
	}
}
