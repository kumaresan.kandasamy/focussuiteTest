﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using Focus.Core;
using Focus.Core.IntegrationMessages;
using Focus.Services.Integration;
using Focus.Services.Messages;
using Focus.Services.Messages.Handlers;
using Focus.UnitTests.Core;
using Moq;
using NUnit.Framework;

#endregion

namespace Focus.UnitTests.Services.MessageHandlers
{
	public class IntegrationRequestMessageHandlerTests : TestFixtureBase
	{
		[Test]
		[Explicit]
		public void LogActionRequestIsSuccessfullyPassedToIntegrationRepository()
		{
			// Arrange
			var logActionRequest = new LogActionRequest
				{
					ActionerExternalId = MockUserData.ExternalUserId,
					ActionedOn = DateTime.Now,
					ActionType = ActionTypes.CreateNewResume
				};

			var mockIntegrationRepository = new Mock<IIntegrationRepository>();
			var mockReturn = new LogActionResponse(logActionRequest) { Outcome = IntegrationOutcome.Success};
			mockIntegrationRepository.Setup(m => m.LogAction(logActionRequest)).Returns(mockReturn);

			(RuntimeContext.Repositories as TestRepositories).Integration = mockIntegrationRepository.Object;

			var message = new IntegrationRequestMessage
				{
					LogActionRequest = logActionRequest,
					IntegrationPoint = IntegrationPoint.LogAction
				};

			var messageHandler = new IntegrationRequestMessageHandler(RuntimeContext);

			// Act	
			messageHandler.Handle(message);

			// Assert
			mockIntegrationRepository.Verify(r => r.LogAction(logActionRequest));
		}
	}
}
