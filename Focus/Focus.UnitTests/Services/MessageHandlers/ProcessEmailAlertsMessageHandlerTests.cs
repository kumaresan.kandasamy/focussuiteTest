﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;

using Focus.Services.Messages;
using Focus.Services.Messages.Handlers;
using Focus.UnitTests.Core;
using Framework.Testing.NUnit;

using NUnit.Framework;

#endregion

namespace Focus.UnitTests.Services.MessageHandlers
{
	public class ProcessEmailAlertsMessageHandlerTests : TestFixtureBase
	{
		[Test]
		[Explicit]
		public void HandleWithValidMessage()
		{
			// Arrange
			var message = new ProcessEmailAlertsMessage
			{
				Id = Guid.NewGuid()
			};

			var messageHandler = new ProcessEmailAlertsMessageHandler(RuntimeContext);

			// Act	
			messageHandler.Handle(message);

			// Assert 
		}
	}
}
