﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using Focus.Data.Core.Entities;
using Focus.Services.Messages;
using Focus.Services.Messages.Handlers;
using Focus.UnitTests.Core;
using Framework.Testing.NUnit;

using NUnit.Framework;

#endregion

namespace Focus.UnitTests.Services.MessageHandlers
{
	public class ApplicantHiredMessageHandlerTests : TestFixtureBase
	{
		[Test]
		public void HandleWithValidMessage()
		{
			// Arrange
			var message = new ApplicantHiredMessage
			              	{
			              		Id = Guid.NewGuid(),
			              		ActionerId = 1,
												ApplicationId = 1099214,
			              		Culture = "**_**",
												PersonId = 1097011
			              	};

			var recentlyPlaced = RuntimeContext.Repositories.Core.FindById<RecentlyPlaced>(1113427);
			recentlyPlaced.PlacementDate = DateTime.Now;
			RuntimeContext.Repositories.Core.SaveChanges();

			var messageHandler = new ApplicantHiredMessageHandler(RuntimeContext);

			// Act	
			messageHandler.Handle(message);

			// Assert 
		}
	}
}
