﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Linq;
using Focus.Core;
using Focus.Services.Messages;
using Focus.Services.Messages.Handlers;
using Focus.UnitTests.Core;
using Framework.Testing.NUnit;
using NUnit.Framework;

#endregion

namespace Focus.UnitTests.Services.MessageHandlers
{
	public class HoldReferralMessageHandlerTests : TestFixtureBase
	{
		[Test]
		[ExpectedException]
		public void HandleWithInvalidApplicationIdRaisesException()
		{
			// Arrange
			var message = new HoldReferralMessage
			{
				Culture = "**_**",
				ApplicationId = 0,
				Reason = ApplicationOnHoldReasons.JobClosed
			};

			var messageHandler = new HoldReferralMessageHandler(RuntimeContext);

			// Act	
			messageHandler.Handle(message);

			// Assert  
		}

		[Test]
		[ExpectedException]
		public void HandleWithInvalidReasonRaisesException()
		{
			// Arrange
			var message = new HoldReferralMessage
			{
				Culture = "**_**",
				ApplicationId = 1109308,
				Reason = ApplicationOnHoldReasons.Unknown
			};

			var messageHandler = new HoldReferralMessageHandler(RuntimeContext);

			// Act	
			messageHandler.Handle(message);

			// Assert  
		}

		[Test]
		public void HandleWithValidMessageApplicationPutOnHold()
		{
			const long applicationId = 1109308;

			// Arrange
			var message = new HoldReferralMessage
			{
				Culture = "**_**",
				ApplicationId = applicationId,
				Reason = ApplicationOnHoldReasons.JobClosed
			};

			var messageHandler = new HoldReferralMessageHandler(RuntimeContext);

			// Act	
			messageHandler.Handle(message);

			// Assert
			var applicationApprovalStatus = RuntimeContext.Repositories.Core.Applications.Where(x => x.Id == applicationId).Select(x => x.ApprovalStatus).FirstOrDefault();

			applicationApprovalStatus.ShouldNotBeNull();
			applicationApprovalStatus.ShouldEqual(ApprovalStatuses.OnHold);
		}

		[Test]
		public void HandleWithValidMessageEmailSent()
		{
			// Arrange
			var message = new HoldReferralMessage
			{
				Culture = "**_**",
				ApplicationId = 1109308,
				Reason = ApplicationOnHoldReasons.JobClosed
			};

			var messageHandler = new HoldReferralMessageHandler(RuntimeContext);

			// Act	
			messageHandler.Handle(message);

			// Assert
			var messageCount = MessageCount(typeof(SendEmailMessage));
			messageCount.ShouldEqual(1);
		}

		[Test]
		public void HandleWithValidMessageActionLogged()
		{
			const long applicationId = 1109308;

			// Arrange
			var message = new HoldReferralMessage
			{
				Culture = "**_**",
				ApplicationId = applicationId,
				Reason = ApplicationOnHoldReasons.JobClosed
			};

			var messageHandler = new HoldReferralMessageHandler(RuntimeContext);

			// Act	
			messageHandler.Handle(message);

			// Assert
			var logEvent = RuntimeContext.Repositories.Core.ActionEvents.FirstOrDefault(x => x.EntityId == applicationId && x.ActionType.Name == ActionTypes.UpdateReferralStatusToAutoOnHold.ToString());
			logEvent.ShouldNotBeNull();
		}
	}
}
