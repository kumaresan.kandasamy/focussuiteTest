﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Services.Messages;
using Focus.Services.Messages.Handlers;
using Focus.UnitTests.Core;
using Framework.Testing.NUnit;
using NUnit.Framework;
using Focus.Data.Core.Entities;
using Framework.Core;

#endregion

namespace Focus.UnitTests.Services.MessageHandlers
{
	internal class UserShortView
	{
		public long UserId { get; set; }
		public DateTime CreatedOn { get; set; }
		public DateTime? LastLoggedInOn { get; set; }
		public DateTime? LastLoggedInDateForAlert { get; set; }
	}

	public class ProcessCandidateIssuesMessageHandlerTests : TestFixtureBase
	{
		[Test]
		[Explicit]
		public void HandleWithValidMessage()
		{
			// Arrange
			var message = new ProcessCandidateIssuesMessage
			{
				Id = Guid.NewGuid()
			};

			var messageHandler = new ProcessCandidateIssuesMessageHandler(RuntimeContext);

			// Act	
			messageHandler.Handle(message);

			// Assert 
		}

		private void ProcessAlienRegistrations(ProcessCandidateIssuesMessageHandler messageHandler, IQueryable<ProcessCandidateIssuesRecord> jobSeekerQuery, List<ProcessCandidateIssuesUpdate> expiredAlienRegistrationIssues, List<ProcessCandidateIssuesRecord> jobSeekersToBlock)
		{
			var seekersRead = 0;
			const int batchSizeForRead = 50; // use low number to test while loop
			var processIssues = true;
			while (processIssues)
			{
				var jobSeekersList = jobSeekerQuery.Skip(seekersRead).Take(batchSizeForRead).ToList();

				if (jobSeekersList.Count > 0)
				{
					messageHandler.RecalculateAlienRegistrationIssue(jobSeekersList, expiredAlienRegistrationIssues, jobSeekersToBlock);

					// This should not do any db updates, but is done to reset the repository (to free up memory)
					RuntimeContext.Repositories.Core.SaveChanges(true);

					seekersRead += jobSeekersList.Count;
				}
				else
				{
					processIssues = false;
				}
			}
			messageHandler.BatchUpdateAlienRegistrationIssues(expiredAlienRegistrationIssues);
			messageHandler.BatchBlockJobSeekers(jobSeekersToBlock);
			messageHandler.EmailJobSeekersWithExpiredAlienRegistration(jobSeekersToBlock);
		}

		[Test]
		public void AlienRegistrationsMessageBusTest()
		{
			// Arrange
			var messageHandler = new ProcessCandidateIssuesMessageHandler(RuntimeContext);
			var jobSeekerQuery = messageHandler.GetJobSeekerQuery();

			// Act
			var expiredAlienRegistrationIssues = new List<ProcessCandidateIssuesUpdate>();
			var jobSeekersToBlock = new List<ProcessCandidateIssuesRecord>();
			ProcessAlienRegistrations(messageHandler, jobSeekerQuery, expiredAlienRegistrationIssues, jobSeekersToBlock);

			// Assert 
			expiredAlienRegistrationIssues.Count.ShouldEqual(3);
			RuntimeContext.Repositories.Core.Issues.Count(i => i.HasExpiredAlienCertificationRegistration == true).ShouldEqual(3);
			var jobSeekerIdsToBlock = jobSeekersToBlock.Select(js => js.Candidate.UserId).ToList();
			RuntimeContext.Repositories.Core.Users.Count(u => jobSeekerIdsToBlock.Contains(u.Id) && u.Blocked).ShouldEqual(3);

			//Repeat
			expiredAlienRegistrationIssues.Clear();
			jobSeekersToBlock.Clear();
			ProcessAlienRegistrations(messageHandler, jobSeekerQuery, expiredAlienRegistrationIssues, jobSeekersToBlock);

			// Assert 
			expiredAlienRegistrationIssues.Count.ShouldEqual(0);
			jobSeekersToBlock.Count.ShouldEqual(0);
		}

		private void ProcessJobSeekerInactivityEmails( ProcessCandidateIssuesMessageHandler messageHandler, IQueryable<ProcessCandidateIssuesRecord> jobSeekerQuery, List<CandidateBasicViewDto> jobSeekerEmailsToSend, List<ProcessCandidateIssuesMessageHandler.JobSeekerIds> jobSeekerDetailsToDisable )
		{
			var seekersRead = 0;
			const int batchSizeForRead = 50; // use low number to test while loop
			var processIssues = true;
			while (processIssues)
			{
				var jobSeekersList = jobSeekerQuery.Skip(seekersRead).Take(batchSizeForRead).ToList();

				if (jobSeekersList.Count > 0)
				{
					messageHandler.BuildJobSeekerInactivityEmails(jobSeekersList, jobSeekerEmailsToSend, jobSeekerDetailsToDisable);

					// This should not do any db updates, but is done to reset the repository (to free up memory)
					RuntimeContext.Repositories.Core.SaveChanges(true);

					seekersRead += jobSeekersList.Count;
				}
				else
				{
					processIssues = false;
				}
			}
			// Disable job seekers, if any
			messageHandler.DisableJobSeekers(jobSeekerDetailsToDisable);

			// Send the emails to any inactive job seekers that have been identified
			messageHandler.EmailInactiveJobSeekers(jobSeekerEmailsToSend);
		}

		[Test]
		public void JobSeekerInactivityEmailsMessageBusTest()
		{
			// Arrange
			((FakeAppSettings)RuntimeContext.AppSettings).JobSeekerSendInactivityEmail = true;
			var messageHandler = new ProcessCandidateIssuesMessageHandler(RuntimeContext);
			var jobSeekerQuery = messageHandler.GetJobSeekerQuery();

			// Create a list of user IDs that we will not expect to be picked up by ProcessJobSeekerInactivityEmails
			// First is a list of user IDs that have logged in 1 day short of the JobSeekerInactivityEmailLimit config setting
			// These User IDs exist on the FocusUnit database
			var recentlyLoggedinUsers = new long[] { 1122312, 1122519, 1123318, 1123629, 1124042, 1124400, 1125332, 1125673, 1125812, 1126539, 1126547, 1128215, 1129390, 1131567, 1156341, 1156586 };

			// Now create a list of user IDs that were created 1 day short of the JobSeekerInactivityEmailLimit config setting
			// These User IDs exist on the FocusUnit database
			var recentlyCreatedUsers = new long[] { 1156907, 1156989, 1177363, 1177651, 1178118, 1178368, 1178470, 1178728, 1179208, 1180411, 1180772, 1180958, 1181008, 1183337 };

			// Now using the user ID lists above, amend the user records so that:

			// LastLoggedInOn date is set to null - this is to ensure that ProcessJobSeekerInactivityEmails does not process on LastLoggedInOn by mistake
			// LoggedInOn will be set to 1 day short of the JobSeekerInactivityEmailLimit config setting
			var usersToUpdate = (from users in RuntimeContext.Repositories.Core.Users
				select users).Where(u => recentlyLoggedinUsers.Contains(u.Id));
			foreach (var user in usersToUpdate)
			{
				user.LastLoggedInOn = null;
				user.LoggedInOn = DateTime.Now.AddDays(-RuntimeContext.AppSettings.JobSeekerInactivityEmailLimit + 1);
			}

			// Second set - here were set up these users so that they have never logged in
			// Also set the CreatedOn date to be 1 day short of the JobSeekerInactivityEmailLimit config setting
			// So these should not be picked up either.
			usersToUpdate = (from users in RuntimeContext.Repositories.Core.Users
				select users).Where(u => recentlyCreatedUsers.Contains(u.Id));
			foreach (var user in usersToUpdate)
			{
				user.LoggedInOn = null;
				user.LastLoggedInOn = null;
				RuntimeContext.Repositories.Core.SetCreatedOn( user, user.Id, DateTime.Now.AddDays( -RuntimeContext.AppSettings.JobSeekerInactivityEmailLimit + 1 ) );
			}

			RuntimeContext.Repositories.Core.SaveChanges();

			// So now let's do our own calculation on how many records we will expect to be picked up.
			// First get the list of records that will be considered
			var minAgeForProcessing = RuntimeContext.AppSettings.UnderAgeJobSeekerRestrictionThreshold;
			var userList = (from persons in RuntimeContext.Repositories.Core.Query<Person>()
				join users in RuntimeContext.Repositories.Core.Users on persons.Id equals users.PersonId
				join issues in RuntimeContext.Repositories.Core.Issues on users.PersonId equals issues.PersonId
				join resume in RuntimeContext.Repositories.Core.Resumes on persons.Id equals resume.PersonId
				where (persons.Age == null || persons.Age >= minAgeForProcessing)
				      && resume.IsDefault && resume.StatusId == ResumeStatuses.Active
				select new UserShortView
				{
					UserId = users.Id,
					LastLoggedInOn = users.LoggedInOn,
					CreatedOn = users.CreatedOn,
					LastLoggedInDateForAlert = issues.LastLoggedInDateForAlert
				}
				);

			// Now loop through all records to check to see if they should be picked up for peocessing
			var expectedCount = 0;
			foreach (var user in userList)
			{
				if (user.LastLoggedInOn.IsNull())
				{
					if (DateTime.Now.Subtract(user.CreatedOn).TotalDays >= RuntimeContext.AppSettings.JobSeekerInactivityEmailLimit && user.CreatedOn != user.LastLoggedInDateForAlert)
						expectedCount++;
				}
				else
				{
					if (DateTime.Now.Subtract(user.LastLoggedInOn.Value).TotalDays >= RuntimeContext.AppSettings.JobSeekerInactivityEmailLimit && user.LastLoggedInOn != user.LastLoggedInDateForAlert)
						expectedCount++;
				}
			}

			// Act
			var jobSeekerEmailsToSend = new List<CandidateBasicViewDto>();
			var jobSeekerDetailsToDisable = new List<ProcessCandidateIssuesMessageHandler.JobSeekerIds>();
			ProcessJobSeekerInactivityEmails(messageHandler, jobSeekerQuery, jobSeekerEmailsToSend, jobSeekerDetailsToDisable);

			// Assert
			// So the first thing we want to check for is that none of the users that were picked up by ProcessJobSeekerInactivityEmails are in the set of users that we have prepared above
			foreach (var jobSeeker in jobSeekerEmailsToSend)
			{
				// First check that jobSeekerEmailsToSend does not contain any that have logged in recently
				recentlyLoggedinUsers.Any(userId => jobSeeker.UserId == userId).ShouldBeFalse();
				// Now check that jobSeekerEmailsToSend does not contain any that were created recently
				recentlyCreatedUsers.Any(userId => jobSeeker.UserId == userId).ShouldBeFalse();
			}

			foreach (var jobSeeker in jobSeekerEmailsToSend)
			{
				// Now check that jobSeekerDetailsToDisable does not contain any that have logged in recently
				recentlyLoggedinUsers.Any(userId => jobSeeker.UserId == userId).ShouldBeFalse();
				// Now check that jobSeekerDetailsToDisable does not contain any that were created recently
				recentlyCreatedUsers.Any(userId => jobSeeker.UserId == userId).ShouldBeFalse();
			}

			// Now check that the number of records picked up tallies up with our calculation above.
			jobSeekerEmailsToSend.Count.ShouldEqual(expectedCount);
			jobSeekerDetailsToDisable.Count.ShouldEqual(expectedCount);

			// Now check that the selected users have in fact been disabled
			var jobSeekerIdsToBlock = jobSeekerDetailsToDisable.Select(js => js.UserId).ToList();
			RuntimeContext.Repositories.Core.Users.Count(u => jobSeekerIdsToBlock.Contains(u.Id) && !u.Enabled && u.AccountDisabledReason == AccountDisabledReason.Inactivity).ShouldEqual(expectedCount);
			((FakeAppSettings)RuntimeContext.AppSettings).JobSeekerSendInactivityEmail = false;

			//Repeat
			// We want to repeat this to ensure that on the second run, no records are picked up
			jobSeekerEmailsToSend = new List<CandidateBasicViewDto>();
			jobSeekerDetailsToDisable = new List<ProcessCandidateIssuesMessageHandler.JobSeekerIds>();
			ProcessJobSeekerInactivityEmails(messageHandler, jobSeekerQuery, jobSeekerEmailsToSend, jobSeekerDetailsToDisable);

			// Assert
			jobSeekerEmailsToSend.Count.ShouldEqual(0);
			jobSeekerDetailsToDisable.Count.ShouldEqual(0);
		}
	}
}
