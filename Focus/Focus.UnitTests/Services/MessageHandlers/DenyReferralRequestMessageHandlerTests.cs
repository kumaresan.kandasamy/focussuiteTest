﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using Focus.Core;
using Focus.Data.Core.Entities;
using Focus.Services.Messages;
using Focus.Services.Messages.Handlers;
using Focus.UnitTests.Core;
using Framework.Testing.NUnit;
using Framework.Messaging;
using NUnit.Framework;

#endregion

namespace Focus.UnitTests.Services.MessageHandlers
{
	public class DenyReferralRequestMessageHandlerTests : TestFixtureBase
	{
		[Test]
		public void DenyApplicationForJobThatExpiredOverThirtyDaysAgo()
		{
			// Arrange
			(RuntimeContext.AppSettings as FakeAppSettings).AutoDenyJobseekerTimePeriod = 30;
			var application = SetUpApplicationForTest(-31);
			
			var message = new DenyReferralRequestMessage
			{
				Id = Guid.NewGuid()
			};

			var messageHandler = new DenyReferralRequestMessageHandler(RuntimeContext);

			// Act	
			messageHandler.Handle(message);

			// Assert 

			// TODO: I have had to comment out the following message check as it is throwing a distributed transaction error (ADC)
			//var messageBus = new MessageBus(new MessageStore());
			//var messages = messageBus.PeekMessages<SendEmailFromTemplateMessage>(Constants.SystemVersion);
			//var sendEmailFromTemplateMessage = messages.FirstOrDefault(m => m.TemplateType == EmailTemplateTypes.DenyCandidateReferral);

			//sendEmailFromTemplateMessage.ShouldNotBeNull();
			//var templateData = sendEmailFromTemplateMessage.TemplateData;
			//templateData.RecipientName.ShouldEqual(application.Resume.Person.FirstName);
			//templateData.JobTitle.ShouldEqual(application.Posting.JobTitle);
			//templateData.EmployerName.ShouldEqual(application.Posting.EmployerName);

			var deniedApplication = RuntimeContext.Repositories.Core.Applications.FirstOrDefault(a => a.Id == application.Id);
			deniedApplication.ApprovalStatus.ShouldEqual(ApprovalStatuses.Rejected);
			deniedApplication.AutomaticallyDenied.ShouldBeTrue();

			// Clean up
			RuntimeContext.Repositories.Core.Remove(application);
		}

		[Test]
		public void DoNotDenyApplicationForJobThatHasOnlyClosedThiryDaysAgo()
		{
			// Arrange
			(RuntimeContext.AppSettings as FakeAppSettings).AutoDenyJobseekerTimePeriod = 30;
			var application = SetUpApplicationForTest(-30);

			var message = new DenyReferralRequestMessage
			{
				Id = Guid.NewGuid()
			};

			var messageHandler = new DenyReferralRequestMessageHandler(RuntimeContext);

			// Act	
			messageHandler.Handle(message);

			// Assert 
			RuntimeContext.Repositories.Core.Applications.FirstOrDefault(a => a.Id == application.Id).ApprovalStatus.ShouldEqual(ApprovalStatuses.OnHold);

			// Clean up
			RuntimeContext.Repositories.Core.Remove(application);
		}

		/// <summary>
		/// Sets up application for test.
		/// </summary>
		/// <param name="offsetOfJobClosingDateInDays">The offset of job closing date in days.  E.g. if the job closed 30 days in the past then the value should be -30</param>
		/// <returns></returns>
		private Application SetUpApplicationForTest(int offsetOfJobClosingDateInDays)
		{
			var posting = RuntimeContext.Repositories.Core.Postings.First();
			posting.Job = new Job
			{
				JobTitle = "New job",
				Employer = RuntimeContext.Repositories.Core.Employers.First(),
				JobStatus = JobStatuses.Closed,
				ClosedOn = DateTime.Today.AddDays(offsetOfJobClosingDateInDays)
			};

			var application = new Application
			{
				ApplicationStatus = ApplicationStatusTypes.NewApplicant,
				ApprovalStatus = ApprovalStatuses.OnHold,
				ApplicationScore = 5,
				Posting = posting,
				Resume = RuntimeContext.Repositories.Core.Resumes.First()
			};

			RuntimeContext.Repositories.Core.Add(application);

			RuntimeContext.Repositories.Core.SaveChanges(true);

			return application;
		}
	}
}
