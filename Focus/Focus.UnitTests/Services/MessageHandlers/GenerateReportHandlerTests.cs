﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using Focus.Core;
using Focus.Services.Messages;
using Focus.Services.Messages.Handlers;
using Focus.UnitTests.Core;
using NUnit.Framework;

#endregion

namespace Focus.UnitTests.Services.MessageHandlers
{
	class GenerateReportHandlerTests : TestFixtureBase
	{
		[Ignore]
		[Test]
		[Explicit]
		public void HandleEmployerActivityWithValidMessage()
		{
			// Arrange
			var message = new GenerateReportMessage
			{
				Id = Guid.NewGuid(),
				ReportFromDate = new DateTime(2013,4,1),
				ReportToDate = new DateTime(2013, 4, 30),
				ReportType = AsyncReportType.EmployerActivity
			};

			var messageHandler = new GenerateReportHandler(RuntimeContext);

			// Act	
			messageHandler.Handle(message);
		}

    [Ignore]
    [Test]
    [Explicit]
    public void HandleStudentActivityWithValidMessage()
    {
      // Arrange
      var message = new GenerateReportMessage
      {
        Id = Guid.NewGuid(),
        ReportFromDate = new DateTime(2013, 4, 1),
        ReportToDate = new DateTime(2013, 4, 30),
        ReportType = AsyncReportType.StudentActivity
      };

      var messageHandler = new GenerateReportHandler(RuntimeContext);

      // Act	
      messageHandler.Handle(message);
    }
	}
}
