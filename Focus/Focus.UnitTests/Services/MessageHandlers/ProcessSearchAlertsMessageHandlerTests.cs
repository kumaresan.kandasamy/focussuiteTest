﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using Focus.Core;
using Focus.Core.Criteria.CandidateSearch;
using Focus.Core.Views;
using Focus.Services.Messages;
using Focus.Services.Messages.Handlers;
using Focus.Services.Repositories;
using Focus.Services.Repositories.Lens;
using Focus.UnitTests.Core;
using Framework.Testing.NUnit;

using Moq;
using NUnit.Framework;

#endregion

namespace Focus.UnitTests.Services.MessageHandlers
{
	public class ProcessSearchAlertsMessageHandlerTests : TestFixtureBase
	{
		[Test]
		[Explicit]
		public void HandleWithValidMessage()
		{
			// Arrange
			//var lensRepository = new Mock<ILensRepository>();
			//lensRepository.Setup(x => x.SearchResumes(It.IsAny<CandidateSearchCriteria>(), It.IsAny<string>())).Returns(new List<ResumeView>());
			//((TestRepositories)RuntimeContext.Repositories).Lens = lensRepository.Object;

			((FakeAppSettings)RuntimeContext.AppSettings).Module = FocusModules.MessageBus;

			var lensRepository = new LensRepository(RuntimeContext);
			((TestRepositories)RuntimeContext.Repositories).Lens = lensRepository;

			var message = new ProcessSearchAlertsMessage
			{
				Id = Guid.NewGuid()
			};

			var messageHandler = new ProcessSearchAlertsMessageHandler(RuntimeContext);

			// Act	
			messageHandler.Handle(message);

			// Assert 
		}
	}
}
