﻿#region Copyright © 2000-2015 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;

using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Models;
using Focus.Services.Helpers;
using Focus.UnitTests.Core;

using Framework.Testing.NUnit;
using Mindscape.LightSpeed;
using NUnit.Framework;

#endregion

namespace Focus.UnitTests.Services.Helpers
{
	public class CandidateHelperTests : TestFixtureBase
	{
		[Test]
		public void HoldReferral_WhenPassedAValidIdForManualChange()
		{
			const int applicationId = 1091911;

			// Arrange
			var candidateHelper = new CandidateHelper(RuntimeContext);

			// Act
			candidateHelper.HoldReferral(applicationId, ApplicationOnHoldReasons.PutOnHoldByStaff, false, MockUserId);

			var application = RuntimeContext.Repositories.Core.Applications.First(a => a.Id == applicationId);
			application.ApprovalStatus.ShouldEqual(ApprovalStatuses.OnHold);
			application.AutomaticallyOnHold.ShouldEqual(false);
			application.StatusLastChangedBy.ShouldEqual(MockUserId);
		}

		[Test]
		public void HoldReferral_WhenPassedAValidIdWhenLockChanged()
		{
			const int applicationId = 1091911;

			// Arrange
			var candidateHelper = new CandidateHelper(RuntimeContext);

			// Act
			try
			{
				candidateHelper.HoldReferral(applicationId, ApplicationOnHoldReasons.PutOnHoldByStaff, false, MockUserId, -1);
			}
			catch (Exception ex)
			{
				(ex is OptimisticConcurrencyException).ShouldBeTrue();
			}
		}

		[Test]
		public void HoldReferral_WhenPassedAValidIdForAutoChange()
		{
			const int applicationId = 1091911;

			// Arrange
			var candidateHelper = new CandidateHelper(RuntimeContext);

			// Act
			candidateHelper.HoldReferral(applicationId, ApplicationOnHoldReasons.JobClosed, true, MockUserId);

			var application = RuntimeContext.Repositories.Core.Applications.First(a => a.Id == applicationId);
			application.ApprovalStatus.ShouldEqual(ApprovalStatuses.OnHold);
			application.AutomaticallyOnHold.ShouldEqual(true);
			application.StatusLastChangedBy.ShouldBeNull();
		}

		[Test]
		public void HoldReferral_WhenPassedAValidReconsiderIdForAutoChange()
		{
			const int applicationId = 11725558;

			// Arrange
			var candidateHelper = new CandidateHelper(RuntimeContext);

			// Act
			candidateHelper.HoldReferral(applicationId, ApplicationOnHoldReasons.JobClosed, true, MockUserId);

			var application = RuntimeContext.Repositories.Core.Applications.First(a => a.Id == applicationId);
			application.ApprovalStatus.ShouldEqual(ApprovalStatuses.OnHold);
			application.AutomaticallyOnHold.ShouldEqual(true);
			application.StatusLastChangedBy.ShouldBeNull();
		}

		[Test]
		public void SendStaffReferralEmail_WhenPassedValidRequest_DoesNotFail()
		{
			// Arrange
			var posting = new PostingDto
			{
				Id = 1109639,
				EmployerName = "MC Enterprises",
				JobTitle = "MSMQ Manager",
				LensPostingId = "ABC123_5BC5BCC988B347AF96F338651FD632A1"
			};
			var candidateHelper = new CandidateHelper(RuntimeContext);

			// Act
			candidateHelper.SendStaffReferralEmail(1000049, posting, new UserContext{FirstName = "John",LastName = "Smith"});

		  // Assert	
		}
	}
}
