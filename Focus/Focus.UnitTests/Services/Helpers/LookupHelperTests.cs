﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using Focus.Core;
using Focus.UnitTests.Core;
using Framework.Testing.NUnit;
using NUnit.Framework;

#endregion

namespace Focus.UnitTests.Services.Helpers
{
	public class LookupHelperTests : TestFixtureBase 
	{
		[Test]
		public void GetLookup_WhenPassedValidLookup_ReturnsLookup()
		{
			// Arrange

			// Act
			var lookups = LookupHelper.GetLookup(LookupTypes.FeedbackIssues);

			// Assert
			Assert.IsNotNull(lookups);
		}
	}
}
