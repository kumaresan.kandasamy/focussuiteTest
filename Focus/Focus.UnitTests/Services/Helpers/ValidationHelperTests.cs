﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Globalization;
using System.Linq;

using Focus.Core;
using Focus.Core.Models.Upload;
using Focus.Services.Core.Censorship;
using Focus.UnitTests.Core;
using Framework.Testing.NUnit;
using NUnit.Framework;

#endregion

namespace Focus.UnitTests.Services.Helpers
{
  public class ValidationHelperTests : TestFixtureBase
  {
		[Test]
    public void GetValidationSchema_WithValidRequestForJobUpload_ReturnsSuccess()
		{
      var schema = RuntimeContext.Helpers.Validation.GetValidationSchema(ValidationSchema.JobUpload);
      schema.ShouldNotBeNull();
		}

    [Test]
    public void GetRecordType_WithValidRequestForJobUpload_ReturnsSuccess()
    {
      var type = RuntimeContext.Helpers.Validation.GetRecordType(ValidationSchema.JobUpload);
      type.ShouldNotBeNull();
      type.ShouldEqual(typeof(JobUploadRecord));
    }

    [Test]
    public void ValidateFile_WithRequestForJobUpload_ReturnsSuccess()
    {
      const string fileText = @"Job Title,Description,Zip,Number of Openings,Closing Date
Technical Product Manager,""Manages and coordinates all the processes involved in creating a product.  Oversees work involved with product design, production, distribution, marketing and sales."",76352,2,{0}
";
      var fileBytes = System.Text.Encoding.UTF8.GetBytes(string.Format(fileText, DateTime.Now.AddDays(2).ToString(CultureInfo.CurrentCulture.DateTimeFormat.ShortDatePattern)));

      var censorshipRules = new CensorshipRuleList();
      censorshipRules.AddRange(ConfigurationRepository.Query<Data.Configuration.Entities.CensorshipRule>().Select(censorshipRule => new CensorshipRule(censorshipRule)).ToList());

      var profanityRules = censorshipRules.Filter(CensorshipType.Profanity);
	    var criminalRules = new CensorshipRuleList();

      var result = RuntimeContext.Helpers.Validation.ValidateFile(ValidationSchema.JobUpload, fileBytes, UploadFileType.CSV, profanityRules, criminalRules);

      result.ShouldNotBeNull();
      result.Status.ShouldEqual(ValidationRecordStatus.Success);
    }
  }
}
