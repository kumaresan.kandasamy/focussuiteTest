﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Linq;
using Focus.Core;
using Focus.Services.Helpers;
using Focus.UnitTests.Core;
using Framework.Testing.NUnit;
using NUnit.Framework;

#endregion

namespace Focus.UnitTests.Services.Helpers
{
	public class EncryptionHelperTests : TestFixtureBase 
	{
		[Test]
		public void Encrypt_WithValidRequest_ReturnsSuccess()
		{
			// Arrange
			MockAppSettings.SharedEncryptionKey = "88D8ACA4-1A3B-4C95-98C4-9801090D";

			var encryptionHelper = new EncryptionHelper(RuntimeContext);

			// Act
			var encryptedPersonId = encryptionHelper.Encrypt("1", "493E5872-BF17-48");

			// Assert
			encryptedPersonId.ShouldNotBeNull();

			MockAppSettings.SharedEncryptionKey = string.Empty;
		}

		[Test]
		public void Encrypt_UrlEncodeWithValidRequest_ReturnsSuccess()
		{
			// Arrange
			MockAppSettings.SharedEncryptionKey = "88D8ACA4-1A3B-4C95-98C4-9801090D";

			var encryptionHelper = new EncryptionHelper(RuntimeContext);

			// Act
			var encryptedPersonId = encryptionHelper.Encrypt("1", "493E5872-BF17-48", true);

			// Assert
			encryptedPersonId.ShouldNotBeNull();

			MockAppSettings.SharedEncryptionKey = string.Empty;
		}

		[Test]
		public void Encrypt_WithSevenDigitIdRequest_ReturnsSuccess()
		{
			// Arrange
			MockAppSettings.SharedEncryptionKey = "88D8ACA4-1A3B-4C95-98C4-9801090D";

			var encryptionHelper = new EncryptionHelper(RuntimeContext);

			// Act
			var encryptedPersonId = encryptionHelper.Encrypt("9999999", "493E5872-BF17-48");

			// Assert
			encryptedPersonId.ShouldNotBeNull();

			MockAppSettings.SharedEncryptionKey = string.Empty;
		}

		[Test]
		public void Encrypt_UrlEncodeWithSevenDigitIdRequest_ReturnsSuccess()
		{
			// Arrange
			MockAppSettings.SharedEncryptionKey = "88D8ACA4-1A3B-4C95-98C4-9801090D";

			var encryptionHelper = new EncryptionHelper(RuntimeContext);

			// Act
			var encryptedPersonId = encryptionHelper.Encrypt("9999999", "493E5872-BF17-48", true);

			// Assert
			encryptedPersonId.ShouldNotBeNull();

			MockAppSettings.SharedEncryptionKey = string.Empty;
		}

		[Test]
		public void Decrypt_WithValidRequest_ReturnsSuccess()
		{
			// Arrange
			MockAppSettings.SharedEncryptionKey = "88D8ACA4-1A3B-4C95-98C4-9801090D";

			var encryptionHelper = new EncryptionHelper(RuntimeContext);

			// Act
			var decrptyedPersonId = encryptionHelper.Decrypt("tcV6KC+JGGGWW27a0TMKEA==", "493E5872-BF17-48");

			// Assert
			decrptyedPersonId.ShouldNotBeNull();
			decrptyedPersonId.ShouldEqual("1");

			MockAppSettings.SharedEncryptionKey = string.Empty;
		}

		[Test]
		public void Decrypt_UrlDecodeWithValidRequest_ReturnsSuccess()
		{
			// Arrange
			MockAppSettings.SharedEncryptionKey = "88D8ACA4-1A3B-4C95-98C4-9801090D";

			var encryptionHelper = new EncryptionHelper(RuntimeContext);

			// Act
			var decrptyedPersonId = encryptionHelper.Decrypt("tcV6KC%2bJGGGWW27a0TMKEA%3d%3d", "493E5872-BF17-48", true);

			// Assert
			decrptyedPersonId.ShouldNotBeNull();
			decrptyedPersonId.ShouldEqual("1");

			MockAppSettings.SharedEncryptionKey = string.Empty;
		}

		[Test]
		public void GenerateVector_JobSeekerTypeWhenNotExistsInDb_CreatesNewRecordReturnsSuccess()
		{
			// Arrange

			var encryptionHelper = new EncryptionHelper(RuntimeContext);

			// Act
			var model = encryptionHelper.GenerateVector(TargetTypes.JobSeekerUnsubscribe, EntityTypes.JobSeeker, 1003968);

			// Assert
			model.ShouldNotBeNull();
			model.Vector.ShouldNotBeNullOrEmpty();
			model.Vector.Length.ShouldEqual(16);

			model.EncryptionId.ShouldNotBeNull();
			model.EncryptionId.ShouldBeGreater(0);

			var encryption = DataCoreRepository.Encryptions.Count(x => x.EntityTypeId.Equals(EntityTypes.JobSeeker) && x.TargetTypeId.Equals(TargetTypes.JobSeekerUnsubscribe) &&
																																 x.EntityId.Equals(1003968) && x.Vector.Equals(model.Vector) && x.Id.Equals(model.EncryptionId));

			encryption.ShouldEqual(1);
		}

		[Test]
		public void GenerateVector_JobSeekerTypeWhenExistsInDbByEntityId_ReturnsSuccess()
		{
			// Arrange
			var encryptionHelper = new EncryptionHelper(RuntimeContext);

			// Act
			var model = encryptionHelper.GenerateVector(TargetTypes.JobSeekerUnsubscribe, EntityTypes.JobSeeker, 1004133);

			// Assert
			model.ShouldNotBeNull();
			model.Vector.ShouldNotBeNullOrEmpty();
			model.Vector.Length.ShouldEqual(16);
			model.Vector.ShouldEqual("493E5872-BF17-50");

			model.EncryptionId.ShouldNotBeNull();
			model.EncryptionId.ShouldEqual(1);
		}

		[Test]
		public void GenerateVector_JobSeekerTypeWhenExistsInDbByEncryptionId_ReturnsSuccess()
		{
			// Arrange
			var encryptionHelper = new EncryptionHelper(RuntimeContext);

			// Act
      var model = encryptionHelper.GetVector(1);

			// Assert
			model.ShouldNotBeNull();
			model.Vector.ShouldNotBeNullOrEmpty();
			model.Vector.Length.ShouldEqual(16);
			model.Vector.ShouldEqual("493E5872-BF17-50");

			model.EncryptionId.ShouldNotBeNull();
			model.EncryptionId.ShouldEqual(1);
		}

		[Test]
		public void GenerateVector_EmployeeTypeWhenNotExistsInDb_CreatesNewRecordReturnsSuccess()
		{
			// Arrange

			var encryptionHelper = new EncryptionHelper(RuntimeContext);

			// Act
			var model = encryptionHelper.GenerateVector(TargetTypes.HiringManagerUnsubscribe, EntityTypes.Employee, 7212495);

			// Assert
			model.ShouldNotBeNull();
			model.Vector.ShouldNotBeNullOrEmpty();
			model.Vector.Length.ShouldEqual(16);

			model.EncryptionId.ShouldNotBeNull();
			model.EncryptionId.ShouldBeGreater(0);

			var encryption = DataCoreRepository.Encryptions.Count(x => x.EntityTypeId.Equals(EntityTypes.Employee) && x.TargetTypeId.Equals(TargetTypes.HiringManagerUnsubscribe) &&
																																 x.EntityId.Equals(7212495) && x.Vector.Equals(model.Vector) && x.Id.Equals(model.EncryptionId));

			encryption.ShouldEqual(1);
		}

		[Test]
		public void GenerateVector_EmployeeTypeWhenExistsInDbByEntityId_ReturnsSuccess()
		{
			// Arrange
			var encryptionHelper = new EncryptionHelper(RuntimeContext);

			// Act
			var model = encryptionHelper.GenerateVector(TargetTypes.HiringManagerUnsubscribe, EntityTypes.Employee, 1192904);

			// Assert
			model.ShouldNotBeNull();
			model.Vector.ShouldNotBeNullOrEmpty();
			model.Vector.Length.ShouldEqual(16);
			model.Vector.ShouldEqual("493E5872-BF17-13");

			model.EncryptionId.ShouldNotBeNull();
			model.EncryptionId.ShouldEqual(2);
		}

		[Test]
		public void GenerateVector_EmployeeTypeWhenExistsInDbByEncryptionId_ReturnsSuccess()
		{
			// Arrange
			var encryptionHelper = new EncryptionHelper(RuntimeContext);

			// Act
			var model = encryptionHelper.GetVector(2);

			// Assert
			model.ShouldNotBeNull();
			model.Vector.ShouldNotBeNullOrEmpty();
			model.Vector.Length.ShouldEqual(16);
			model.Vector.ShouldEqual("493E5872-BF17-13");

			model.EncryptionId.ShouldNotBeNull();
			model.EncryptionId.ShouldEqual(2);
		}
	}
}
