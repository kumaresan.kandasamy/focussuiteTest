﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Net.Mail;
using Focus.Core.Criteria.CandidateSearch;
using Focus.Core.Models.Career;
using Focus.Core.Views;
using Focus.Services.Helpers;
using Focus.Services.Repositories;
using Focus.UnitTests.Core;
using Framework.Testing.NUnit;
using Moq;
using NUnit.Framework;

#endregion


namespace Focus.UnitTests.Services.Helpers
{
	public class EmailHelperTests : TestFixtureBase 
	{
		/// <summary>
		/// Provides a number of different mail messgaes to test the behaviour of SendEmail
		/// </summary>
		/// <value>
		/// The mail messages with different values.
		/// </value>
		public IEnumerable<TestCaseData> MailMessagesWithDifferentValues
		{
			get
			{
				yield return new TestCaseData(new MailMessage()).Throws(typeof(ArgumentException));
				yield return new TestCaseData(new MailMessage("from@site.com", "to@site.com"));
				yield return new TestCaseData(new MailMessage(new MailAddress("from@site.com"), new MailAddress("to@site.com")));
				yield return new TestCaseData(new MailMessage("from@site.com", "to@site.com", "Subject", "Body"));
			}
		}

		/// <summary>
		/// Tests SendEmail with a MailMessage input.
		/// </summary>
		/// <param name="mailMessage">The mail message.</param>
		[TestCaseSource("MailMessagesWithDifferentValues")]
		public void TestSendEmail_MailMessageInput(MailMessage mailMessage)
		{
			EmailHelper.SendEmail(mailMessage);
		}

		/// <summary>
		/// Tests SendEmail with individual argument inputs.
		/// </summary>
		/// <param name="sendAsync">if set to <c>true</c> [send asynchronous].</param>
		/// <param name="isBodyHtml">if set to <c>true</c> [is body HTML].</param>
		/// <param name="attachment">The attachment.</param>
		/// <param name="attachmentName">Name of the attachment.</param>
		/// <param name="detectUrl">if set to <c>true</c> [detect URL].</param>
		/// <param name="senderAddress">The sender address.</param>
		[TestCase(false, true, null, null, false, null)]
		[TestCase(true, false, new byte[] { 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20 }, "Attachment name", true, "sender@site.com")]
		public void SendEmailTest_IndividualArguments(bool sendAsync, bool isBodyHtml, byte[] attachment, string attachmentName, bool detectUrl, string senderAddress)
		{
			EmailHelper.SendEmail("to@site.com", "cc@site.com", "bcc@site.com", "Some subject", "Some body",
				sendAsync, isBodyHtml, attachment, attachmentName, detectUrl, senderAddress);
		}
	}
}
