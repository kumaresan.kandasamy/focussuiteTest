﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using Focus.Services.Repositories;
using Focus.UnitTests.Core;
using Framework.Testing.NUnit;
using Moq;
using NUnit.Framework;
using Focus.Core;

#endregion

namespace Focus.UnitTests.Services.Helpers
{
	public class ResumeHelperTests : TestFixtureBase
	{
		[Test]
		public void RegisterResume_WithValidRequest_ReturnsSuccess()
		{
			// Arrange
			var lensRepository = new Mock<ILensRepository>();
			lensRepository.Setup(x => x.RegisterResume(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<long>(), It.IsAny<SchoolStatus?>(), It.IsAny<List<long>>(), It.IsAny<DateTime?>()));
			((TestRepositories)RuntimeContext.Repositories).Lens = lensRepository.Object;
			
			// Act
			RuntimeContext.Helpers.Resume.RegisterResume(1086379);

			// Assert
		}

		[Test]
		public void UnregisterResume_WithValidRequest_ReturnsSuccess()
		{
			// Arrange
			var lensRepository = new Mock<ILensRepository>();
			lensRepository.Setup(x => x.UnregisterResume(It.IsAny<List<string>>()));
			((TestRepositories) RuntimeContext.Repositories).Lens = lensRepository.Object;

			// Act
			RuntimeContext.Helpers.Resume.UnregisterResume(1086379, 0);
			
			// Assert
		}
	}
}
