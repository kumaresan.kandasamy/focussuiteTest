﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using Focus.Core.Models.Assist;
using Focus.Services.Helpers;
using Focus.UnitTests.Core;
using Framework.Testing.NUnit;
using NUnit.Framework;

#endregion

namespace Focus.UnitTests.Services.Helpers
{
	[TestFixture]
	public class EmployerHelperTests : TestFixtureBase
	{
		private EmployerHelper _employerHelper;

		[SetUp]
		public override void SetUp()
		{
			base.SetUp();

			_employerHelper = new EmployerHelper(RuntimeContext);
		}

		#region Validate FEIN Change tests
		
		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void ValidateFEINChange_WithNoBusinessUnitId_ThrowsArgumentNullException()
		{
			// Arrange
			
			// Act
			_employerHelper.ValidateFEINChange(null, "11-7777777");
			
			// Assert
		}

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void ValidateFEINChange_WithNoNewFEIN_ThrowsArgumentNullException()
		{
			// Arrange
			
			// Act
			_employerHelper.ValidateFEINChange(5533272, null);
			
			// Assert
		}

		[Test]
		[ExpectedException(typeof(ArgumentException))]
		public void ValidateFEINChange_WithInvalidBusinessUnitId_ThrowsArgumentException()
		{
			// Arrange
			
			// Act
			_employerHelper.ValidateFEINChange(1, "11-7777777");
			
			// Assert
		}

		[Test]
		public void ValidateFEINChange_WithCurrentFEIN_ReturnsFEINNotChangedValidationMessage()
		{
			// Arrange
			
			// Act
			var result = _employerHelper.ValidateFEINChange(5533272, "01-0000001");

			// Assert
			result.ShouldNotBeNull();
			result.IsValid.ShouldBeFalse();
			result.ValidationMessage.ShouldEqual(FEINChangeValidationMessages.FEINNotChanged);
			result.ActionToBeCarriedOut.ShouldEqual(FEINChangeActions.None);
		}

		[Test]
		public void ValidateFEINChange_WithPrimaryBusinessUnitExistingFEINAndNoSiblings_ReturnsActionToBeCarriedOutAndExistingEmployerName()
		{
			// Arrange
			
			// Act
			var result = _employerHelper.ValidateFEINChange(5536589, "11-7777777");
			
			// Assert
			result.ShouldNotBeNull();
			result.IsValid.ShouldBeTrue();
			result.ActionToBeCarriedOut.ShouldEqual(FEINChangeActions.PrimaryBusinessUnitExistingFEINNoSiblings);
			result.NewEmployerName.ShouldNotBeNullOrEmpty();
		}

		[Test]
		public void ValidateFEINChange_WithPrimaryBusinessUnitExistingFEINAndSiblings_ReturnsActionToBeCarriedOutAndExistingEmployerName()
		{
			// Arrange
			
			// Act
			var result = _employerHelper.ValidateFEINChange(5531278, "11-7777777");
			
			// Assert
			result.ShouldNotBeNull();
			result.IsValid.ShouldBeTrue();
			result.ActionToBeCarriedOut.ShouldEqual(FEINChangeActions.PrimaryBusinessUnitExistingFEINSiblings);
			result.NewEmployerName.ShouldNotBeNullOrEmpty();
		}

		[Test]
		public void ValidateFEINChange_WithPrimaryBusinessUnitNewFEINAndNoSiblings_ReturnsActionToBeCarriedOut()
		{
			// Arrange
			
			// Act
			var result = _employerHelper.ValidateFEINChange(5536589, "11-8777777");
			
			// Assert
			result.ShouldNotBeNull();
			result.IsValid.ShouldBeTrue();
			result.ActionToBeCarriedOut.ShouldEqual(FEINChangeActions.PrimaryBusinessUnitNewFEINNoSiblings);
		}

		[Test]
		public void ValidateFEINChange_WithPrimaryBusinessUnitNewFEINAndSiblings_ReturnsActionToBeCarriedOut()
		{
			// Arrange
			
			// Act
			var result = _employerHelper.ValidateFEINChange(5531278, "11-8777777");
			
			// Assert
			result.ShouldNotBeNull();
			result.IsValid.ShouldBeTrue();
			result.ActionToBeCarriedOut.ShouldEqual(FEINChangeActions.PrimaryBusinessUnitNewFEINSiblings);
		}

		[Test]
		public void ValidateFEINChange_WithNonPrimaryBusinessWithSharedEmployee_ReturnsEmployeeAssignedToSiblingBusinessUnitValidationMessageAndBusinessUnitName()
		{
			// Arrange
			
			// Act
			var result = _employerHelper.ValidateFEINChange(1104440, "11-7777777");
			
			// Assert
			result.IsValid.ShouldBeFalse();
			result.ValidationMessage.ShouldEqual(FEINChangeValidationMessages.EmployeeAssignedToSiblingBusinessUnit);
			result.ActionToBeCarriedOut.ShouldEqual(FEINChangeActions.None);
			result.BusinessUnitName.ShouldNotBeNullOrEmpty();
		}

		[Test]
		public void ValidateFEINChange_WithNonPrimaryBusinessUnitExistingFEIN_ReturnsActionToBeCarriedOutAndExistingEmployerName()
		{
			// Arrange
			
			// Act
			var result = _employerHelper.ValidateFEINChange(1192846, "11-7777777");
			
			// Assert
			result.ShouldNotBeNull();
			result.IsValid.ShouldBeTrue();
			result.ActionToBeCarriedOut.ShouldEqual(FEINChangeActions.NonPrimaryBusinessUnitExistingFEIN);
			result.NewEmployerName.ShouldNotBeNullOrEmpty();
		}

		[Test]
		public void ValidateFEINChange_WithNonPrimaryBusinessUnitNewFEIN_ReturnsActionToBeCarriedOut()
		{
			// Arrange
			
			// Act
			var result = _employerHelper.ValidateFEINChange(1192846, "11-8777777");
			
			// Assert
			result.ShouldNotBeNull();
			result.IsValid.ShouldBeTrue();
			result.ActionToBeCarriedOut.ShouldEqual(FEINChangeActions.NonPrimaryBusinessUnitNewFEIN);
		}

		#endregion

		#region Save FEIN Change tests

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void SaveFEINChange_WithNoBusinessUnitId_ThrowsArgumentNullException()
		{
			// Arrange

			// Act
			_employerHelper.SaveFEINChange(null, "11-7777777");

			// Assert
		}

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void SaveFEINChange_WithNoNewFEIN_ThrowsArgumentNullException()
		{
			// Arrange

			// Act
			_employerHelper.SaveFEINChange(5533272, null);

			// Assert
		}

		[Test]
		[ExpectedException(typeof(ArgumentException))]
		public void SaveFEINChange_WithInvalidBusinessUnitId_ThrowsArgumentException()
		{
			// Arrange

			// Act
			_employerHelper.SaveFEINChange(1, "11-7777777");

			// Assert
		}

		[Test]
		public void SaveFEINChange_WithCurrentFEIN_ReturnsReturnsActionCarriedOut()
		{
			// Arrange

			// Act
			var result = _employerHelper.SaveFEINChange(5533272, "01-0000001");

			// Assert
			result.ShouldNotBeNull();
			result.ShouldEqual(FEINChangeActions.None);
		}
		
		[Test]
		public void SaveFEINChange_WithPrimaryBusinessUnitExistingFEINAndNoSiblings_ClearsEmployerOfficeMappings()
		{
			// Arrange
			const long businessUnitId = 5536589;
			const long oldEmployerId = 5536602;
			const string newFein = "11-7777777";

			// Act
			_employerHelper.SaveFEINChange(businessUnitId, newFein);

			// Assert
			var oldEmployerOfficesCount = RuntimeContext.Repositories.Core.EmployerOfficeMappers.Count(x => x.EmployerId == oldEmployerId);
			oldEmployerOfficesCount.ShouldNotBeNull();
			oldEmployerOfficesCount.ShouldEqual(0);
		}

		[Test]
		public void SaveFEINChange_WithPrimaryBusinessUnitExistingFEINAndSiblings_UpdatesSiblingBusinessUnitsEmployer()
		{
			// Arrange
			const long businessUnitId = 5531278;
			const long oldEmployerId = 5531281;
			const long newEmployerId = 1180229;
			const string newFein = "11-7777777";

			var combinedEmployerBusinessUnitCountPre = RuntimeContext.Repositories.Core.BusinessUnits.Count(x => x.EmployerId == oldEmployerId || x.EmployerId == newEmployerId);

			// Act
			_employerHelper.SaveFEINChange(businessUnitId, newFein);

			// Assert
			var oldEmployerBusinessUnitCount = RuntimeContext.Repositories.Core.BusinessUnits.Count(x => x.EmployerId == oldEmployerId);
			oldEmployerBusinessUnitCount.ShouldNotBeNull();
			oldEmployerBusinessUnitCount.ShouldEqual(0);

			var newEmployerBusinessUnitCount = RuntimeContext.Repositories.Core.BusinessUnits.Count(x => x.EmployerId == newEmployerId);
			newEmployerBusinessUnitCount.ShouldNotBeNull();
			newEmployerBusinessUnitCount.ShouldEqual(combinedEmployerBusinessUnitCountPre);
		}
		
		[Test]
		public void SaveFEINChange_WithPrimaryBusinessUnitExistingFEINAndSiblings_ClearsEmployerOfficeMappings()
		{
			// Arrange
			const long businessUnitId = 5531278;
			const long oldEmployerId = 5531281;
			const string newFein = "11-7777777";

			// Act
			_employerHelper.SaveFEINChange(businessUnitId, newFein);

			// Assert
			var oldEmployerOfficesCount = RuntimeContext.Repositories.Core.EmployerOfficeMappers.Count(x => x.EmployerId == oldEmployerId);
			oldEmployerOfficesCount.ShouldNotBeNull();
			oldEmployerOfficesCount.ShouldEqual(0);
		}

		[TestCase(5536589, "11-8777777", FEINChangeActions.PrimaryBusinessUnitNewFEINNoSiblings, TestName = "Primary business unit with no siblings to new FEIN")]
		[TestCase(5531278, "11-8777777", FEINChangeActions.PrimaryBusinessUnitNewFEINSiblings, TestName = "Primary business unit with siblings to new FEIN")]
		[TestCase(5536589, "11-7777777", FEINChangeActions.PrimaryBusinessUnitExistingFEINNoSiblings, TestName = "Primary business unit with no siblings to existing FEIN")]
		[TestCase(5531278, "11-7777777", FEINChangeActions.PrimaryBusinessUnitExistingFEINSiblings, TestName = "Primary business unit with siblings to existing FEIN")]
		[TestCase(1192846, "11-8777777", FEINChangeActions.NonPrimaryBusinessUnitNewFEIN, TestName = "Non-primary business unit to new FEIN")]
		[TestCase(1192846, "11-7777777", FEINChangeActions.NonPrimaryBusinessUnitExistingFEIN, TestName = "Non-primary business unit to existing FEIN")]
		public void SaveFEINChange_WithValidRequest_ReturnsActionCarriedOut(long businessUnitId, string newFein, FEINChangeActions expectedResult)
		{
			// Arrange

			// Act
			var result = _employerHelper.SaveFEINChange(businessUnitId, newFein);

			// Assert
			result.ShouldNotBeNull();
			result.ShouldEqual(expectedResult);
		}

		[TestCase(5536589, 5536602, "11-8777777", TestName = "With no siblings")]
		[TestCase(5531278, 5531281, "11-8777777", TestName = "With siblings")]
		public void SaveFEINChange_WithPrimaryBusinessUnitAndNewFEIN_ShouldUpdateFEINOnEmployer(long businessUnitId, long employerId, string newFein)
		{
			// Arrange
			
			// Act
			_employerHelper.SaveFEINChange(businessUnitId, newFein);

			// Assert
			var employerFEIN = RuntimeContext.Repositories.Core.Employers.Where(x => x.Id == employerId).Select(x => x.FederalEmployerIdentificationNumber).FirstOrDefault();

			employerFEIN.ShouldNotBeNullOrEmpty();
			employerFEIN.ShouldEqual(newFein);
		}

		[TestCase(5536589, 1180229, "11-7777777", TestName = "Primary business unit with no siblings")]
		[TestCase(5531278, 1180229, "11-7777777", TestName = "Primary business unit with siblings")]
		[TestCase(1192846, 1180229, "11-7777777", TestName = "Non-primary business unit")]
		public void SaveFEINChange_WithExistingFEIN_UpdatesBusinessUnitEmployer(long businessUnitId, long newEmployerId, string newFein)
		{
			// Arrange
			
			// Act
			_employerHelper.SaveFEINChange(businessUnitId, newFein);

			// Assert
			var businessUnitEmployerId = RuntimeContext.Repositories.Core.BusinessUnits.Where(x => x.Id == businessUnitId).Select(x => x.EmployerId).FirstOrDefault();
			businessUnitEmployerId.ShouldNotBeNull();
			businessUnitEmployerId.ShouldEqual(newEmployerId);
		}

		[TestCase(5536589, "11-7777777", TestName = "With no siblings")]
		[TestCase(5531278, "11-7777777", TestName = "With siblings")]
		public void SaveFEINChange_WithPrimaryBusinessUnitAndExistingFEIN_UpdatesBusinessUnitIsPrimaryToFalse(long businessUnitId, string newFein)
		{
			// Arrange
			
			// Act
			_employerHelper.SaveFEINChange(businessUnitId, newFein);

			// Assert
			var businessUnitIsPrimary = RuntimeContext.Repositories.Core.BusinessUnits.Where(x => x.Id == businessUnitId).Select(x => x.IsPrimary).FirstOrDefault();
			businessUnitIsPrimary.ShouldNotBeNull();
			businessUnitIsPrimary.ShouldBeFalse();
		}

		[TestCase(5536589, "11-7777777", 5536602, TestName = "With no siblings")]
		[TestCase(5531278, "11-7777777", 5531281, TestName = "With siblings")]
		public void SaveFEINChange_WithPrimaryBusinessUnitAndExistingFEIN_ArchivesOldEmployer(long businessUnitId, string newFein, long oldEmployerId)
		{
			// Arrange
			
			// Act
			_employerHelper.SaveFEINChange(businessUnitId, newFein);

			// Assert
			var oldEmployerArchived = RuntimeContext.Repositories.Core.Employers.Where(x => x.Id == oldEmployerId).Select(x => x.Archived).FirstOrDefault();
			oldEmployerArchived.ShouldNotBeNull();
			oldEmployerArchived.ShouldBeTrue();
		}

		[TestCase(1113441, "11-7777777", 1180229, 1113444, TestName = "With no siblings")]
		[TestCase(5539240, "11-7777777", 1180229, 5539243, TestName = "With siblings")]
		public void SaveFEINChange_WithPrimaryBusinessUnitAndExistingFEIN_ReallocatesAllEmployeesToNewEmployer(long businessUnitId, string newFein, long newEmployerId, long oldEmployerId)
		{
			// Arrange
			var combinedEmployerEmployeeCountPre = RuntimeContext.Repositories.Core.Employees.Count(x => x.EmployerId == oldEmployerId || x.EmployerId == newEmployerId);

			// Act
			_employerHelper.SaveFEINChange(businessUnitId, newFein);

			// Assert
			var oldEmployerEmployeeCount = RuntimeContext.Repositories.Core.Employees.Count(x => x.EmployerId == oldEmployerId);
			oldEmployerEmployeeCount.ShouldNotBeNull();
			oldEmployerEmployeeCount.ShouldEqual(0);

			var newEmployerEmployeeCount = RuntimeContext.Repositories.Core.Employees.Count(x => x.EmployerId == newEmployerId);
			newEmployerEmployeeCount.ShouldNotBeNull();
			newEmployerEmployeeCount.ShouldEqual(combinedEmployerEmployeeCountPre);
		}

		[TestCase(1113441, "11-7777777", 1180229, 1113444, TestName = "With no siblings")]
		[TestCase(1185025, "11-7777777", 1180229, 1185028, TestName = "With siblings")]
		public void SaveFEINChange_WithPrimaryBusinessUnitAndExistingFEIN_ReallocatesAllJobsToNewEmployer(long businessUnitId, string newFein, long newEmployerId, long oldEmployerId)
		{
			// Arrange
			var combinedEmployerJobCountPre = RuntimeContext.Repositories.Core.Jobs.Count(x => x.EmployerId == oldEmployerId || x.EmployerId == newEmployerId);

			// Act
			_employerHelper.SaveFEINChange(businessUnitId, newFein);

			// Assert
			var oldEmployerJobCount = RuntimeContext.Repositories.Core.Jobs.Count(x => x.EmployerId == oldEmployerId);
			oldEmployerJobCount.ShouldNotBeNull();
			oldEmployerJobCount.ShouldEqual(0);

			var newEmployerJobCount = RuntimeContext.Repositories.Core.Jobs.Count(x => x.EmployerId == newEmployerId);
			newEmployerJobCount.ShouldNotBeNull();
			newEmployerJobCount.ShouldEqual(combinedEmployerJobCountPre);
		}

		[TestCase(1088857, "11-7777777", 1180229, 1088910, TestName = "With no siblings")]
		[TestCase(1103829, "11-7777777", 1180229, 1103832, TestName = "With siblings")]
		public void SaveFEINChange_WithPrimaryBusinessUnitAndExistingFEIN_ReallocatesAllPersonNotesToNewEmployer(long businessUnitId, string newFein, long newEmployerId, long oldEmployerId)
		{
			// Arrange
			var combinedEmployerNoteCountPre = RuntimeContext.Repositories.Core.PersonNotes.Count(x => x.EmployerId == oldEmployerId || x.EmployerId == newEmployerId);

			// Act
			_employerHelper.SaveFEINChange(businessUnitId, newFein);

			// Assert
			var oldEmployerNoteCount = RuntimeContext.Repositories.Core.PersonNotes.Count(x => x.EmployerId == oldEmployerId);
			oldEmployerNoteCount.ShouldNotBeNull();
			oldEmployerNoteCount.ShouldEqual(0);

			var newEmployerNoteCount = RuntimeContext.Repositories.Core.PersonNotes.Count(x => x.EmployerId == newEmployerId);
			newEmployerNoteCount.ShouldNotBeNull();
			newEmployerNoteCount.ShouldEqual(combinedEmployerNoteCountPre);
		}

		[Test]
		public void SaveFEINChange_WithNonPrimaryBusinessUnitWithSharedEmployee_ReturnsActionCarriedOut()
		{
			// Arrange

			// Act
			var result = _employerHelper.SaveFEINChange(1104440, "11-7777777");

			// Assert
			result.ShouldNotBeNull();
			result.ShouldEqual(FEINChangeActions.None);
		}

		[Test]
		public void SaveFEINChange_WithNonPrimaryBusinessUnitAndNewFEIN_ReallocatesAllEmployeesToNewEmployer()
		{
			// Arrange
			const long businessUnitId = 1192846;
			const string newFein = "11-8777777";

			var businessUnitEmployeeCountPre = RuntimeContext.Repositories.Core.EmployeeBusinessUnits.Count(x => x.BusinessUnitId == businessUnitId);

			// Act
			_employerHelper.SaveFEINChange(businessUnitId, newFein);

			// Assert
			var newEmployerId = RuntimeContext.Repositories.Core.Employers.Where(x => x.FederalEmployerIdentificationNumber == newFein && (!x.Archived)).Select(x => x.Id).FirstOrDefault();

			var businessUnitEmployeeCount = RuntimeContext.Repositories.Core.EmployeeBusinessUnits.Count(x => x.BusinessUnitId == businessUnitId);
			businessUnitEmployeeCount.ShouldNotBeNull();
			businessUnitEmployeeCount.ShouldEqual(businessUnitEmployeeCountPre);

			var newEmployerEmployeeCount = RuntimeContext.Repositories.Core.Employees.Count(x => x.EmployerId == newEmployerId);
			newEmployerEmployeeCount.ShouldNotBeNull();
			newEmployerEmployeeCount.ShouldEqual(businessUnitEmployeeCountPre);
		}


		[Test]
		public void SaveFEINChange_WithNonPrimaryBusinessUnitAndExistingFEIN_ReallocatesAllEmployeesToNewEmployer()
		{
			// Arrange
			const long businessUnitId = 1192846;
			const long newEmployerId = 1180229;
			const string newFein = "11-7777777";

			var businessUnitEmployeeCountPre = RuntimeContext.Repositories.Core.EmployeeBusinessUnits.Count(x => x.BusinessUnitId == businessUnitId);
			var newEmployerEmployeeCountPre = RuntimeContext.Repositories.Core.Employees.Count(x => x.EmployerId == newEmployerId);
			
			// Act
			_employerHelper.SaveFEINChange(businessUnitId, newFein);

			// Assert
			var businessUnitEmployeeCount = RuntimeContext.Repositories.Core.EmployeeBusinessUnits.Count(x => x.BusinessUnitId == businessUnitId);
			businessUnitEmployeeCount.ShouldNotBeNull();
			businessUnitEmployeeCount.ShouldEqual(businessUnitEmployeeCountPre);

			var newEmployerEmployeeCount = RuntimeContext.Repositories.Core.Employees.Count(x => x.EmployerId == newEmployerId);
			newEmployerEmployeeCount.ShouldNotBeNull();
			newEmployerEmployeeCount.ShouldEqual(businessUnitEmployeeCountPre + newEmployerEmployeeCountPre);
		}

		[Test]
		public void SaveFEINChange_WithNonPrimaryBusinessUnitAndExistingFEIN_ReallocatesAllJobsToNewEmployer()
		{
			// Arrange
			const long businessUnitId = 1192846;
			const long newEmployerId = 1180229;
			const string newFein = "11-7777777";

			var combinedEmployerJobCountPre = RuntimeContext.Repositories.Core.Jobs.Count(x => x.BusinessUnitId == businessUnitId || x.EmployerId == newEmployerId);

			// Act
			_employerHelper.SaveFEINChange(businessUnitId, newFein);

			// Assert
			var newEmployerJobCount = RuntimeContext.Repositories.Core.Jobs.Count(x => x.EmployerId == newEmployerId);
			newEmployerJobCount.ShouldNotBeNull();
			newEmployerJobCount.ShouldEqual(combinedEmployerJobCountPre);
		}

		[Test]
		public void SaveFEINChange_WithNonPrimaryBusinessUnitAndNewFEIN_CreatesNewEmployer()
		{
			// Arrange
			const long businessUnitId = 1192846;
			const string newFEIN = "11-8777777";

			// Act
			_employerHelper.SaveFEINChange(businessUnitId, newFEIN);

			// Assert
			var newEmployerId = RuntimeContext.Repositories.Core.Employers.Where(x => x.FederalEmployerIdentificationNumber == newFEIN && (!x.Archived)).Select(x => x.Id).FirstOrDefault();
			newEmployerId.ShouldNotBeNull();
			newEmployerId.ShouldBeGreater(0);
		}

		[Test]
		public void SaveFEINChange_WithNonPrimaryBusinessUnitAndNewFEIN_CreatesNewEmployerAddress()
		{
			// Arrange
			const long businessUnitId = 1192846;
			const string newFEIN = "11-8777777";

			// Act
			_employerHelper.SaveFEINChange(businessUnitId, newFEIN);

			// Assert
			var newEmployerId = RuntimeContext.Repositories.Core.Employers.Where(x => x.FederalEmployerIdentificationNumber == newFEIN && (!x.Archived)).Select(x => x.Id).FirstOrDefault();
			var hasAddresses = RuntimeContext.Repositories.Core.EmployerAddresses.Any(x => x.EmployerId == newEmployerId);
			hasAddresses.ShouldBeTrue();
		}

		[Test]
		public void SaveFEINChange_WithNonPrimaryBusinessUnitAndNewFEIN_AssignsOfficesToNewEmployer()
		{
			// Arrange
			const long businessUnitId = 1192846;
			const string newFEIN = "11-8777777";

			// Act
			_employerHelper.SaveFEINChange(businessUnitId, newFEIN);

			// Assert
			var newEmployerId = RuntimeContext.Repositories.Core.Employers.Where(x => x.FederalEmployerIdentificationNumber == newFEIN && (!x.Archived)).Select(x => x.Id).FirstOrDefault();
			var hasOffices = RuntimeContext.Repositories.Core.EmployerOfficeMappers.Any(x => x.EmployerId == newEmployerId);
			hasOffices.ShouldBeTrue();
		}

		[Test]
		public void SaveFEINChange_WithNonPrimaryBusinessUnitAndNewFEIN_UpdatesBusinessUnitEmployer()
		{
			// Arrange
			const long businessUnitId = 1192846;
			const string newFEIN = "11-8777777";

			// Act
			_employerHelper.SaveFEINChange(businessUnitId, newFEIN);

			// Assert
			var newEmployerId = RuntimeContext.Repositories.Core.Employers.Where(x => x.FederalEmployerIdentificationNumber == newFEIN && (!x.Archived)).Select(x => x.Id).FirstOrDefault();
			var businessUnitEmployerId = RuntimeContext.Repositories.Core.BusinessUnits.Where(x => x.Id == businessUnitId).Select(x => x.EmployerId).FirstOrDefault();
			businessUnitEmployerId.ShouldNotBeNull();
			businessUnitEmployerId.ShouldEqual(newEmployerId);
		}

		[Test]
		public void SaveFEINChange_WithNonPrimaryBusinessUnitAndNewFEIN_UpdatesBusinessUnitIsPrimary()
		{
			// Arrange
			const long businessUnitId = 1192846;
			const string newFein = "11-8777777";

			// Act
			_employerHelper.SaveFEINChange(businessUnitId, newFein);

			// Assert
			var businessUnitIsPrimary = RuntimeContext.Repositories.Core.BusinessUnits.Where(x => x.Id == businessUnitId).Select(x => x.IsPrimary).FirstOrDefault();
			businessUnitIsPrimary.ShouldNotBeNull();
			businessUnitIsPrimary.ShouldBeTrue();
		}

		[Test]
		public void SaveFEINChange_WithNonPrimaryBusinessUnitAndNewFEIN_ReallocatesAllJobsToNewEmployer()
		{
			// Arrange
			const long businessUnitId = 1192846;
			const string newFein = "11-8777777";

			var combinedEmployerJobCountPre = RuntimeContext.Repositories.Core.Jobs.Count(x => x.BusinessUnitId == businessUnitId);

			// Act
			_employerHelper.SaveFEINChange(businessUnitId, newFein);

			// Assert
			var newEmployerId = RuntimeContext.Repositories.Core.Employers.Where(x => x.FederalEmployerIdentificationNumber == newFein && (!x.Archived)).Select(x => x.Id).FirstOrDefault();

			var newEmployerJobCount = RuntimeContext.Repositories.Core.Jobs.Count(x => x.EmployerId == newEmployerId);
			newEmployerJobCount.ShouldNotBeNull();
			newEmployerJobCount.ShouldEqual(combinedEmployerJobCountPre);
		}

		#endregion

		#region Update primary business unit tests

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void UpdatePrimaryBusinessUnit_WithNoCurrentBusinessUnitId_ThrowsArgumentNullException()
		{
			// Arrange

			// Act
			_employerHelper.UpdatePrimaryBusinessUnit(null, 5533272);

			// Assert
		}

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void UpdatePrimaryBusinessUnit_WithNoNewBusinessUnitId_ThrowsArgumentException()
		{
			// Arrange

			// Act
			_employerHelper.UpdatePrimaryBusinessUnit(1546755, null);

			// Assert
		}

		[Test]
        [ExpectedException(typeof(ArgumentException))]
		public void UpdatePrimaryBusinessUnit_CurrentBinessUnitNotPrimary_ThrowsArgumentException()
		{
			// Arrange

			// Act
			_employerHelper.UpdatePrimaryBusinessUnit(5533272, 5533402);

			// Assert
		}

		[Test]
		[ExpectedException(typeof(Exception))]
		public void UpdatePrimaryBusinessUnits_ValidBusinessUnitsProvided_CheckEmployerIDsDoNotMatch()
		{
			// Arrange

			// Act
			_employerHelper.UpdatePrimaryBusinessUnit(1546755, 43630);
			
			// Assert
		}

		[Test]
		public void UpdatePrimaryBusinessUnit_ValidBusinessUnitsProvided_UpdatesBusinessUnitToBePrimary()
		{
			// Arrange
			const long currentBUID = 1546755;
			const long newBUID = 5533272;
			//const string employerNewName = "Andy Test 1";

			// Act
			_employerHelper.UpdatePrimaryBusinessUnit(currentBUID, newBUID);
			var newPrimaryBU = RuntimeContext.Repositories.Core.BusinessUnits.FirstOrDefault(x => x.Id == newBUID);

			// Assert
			newPrimaryBU.ShouldNotBeNull();
			newPrimaryBU.IsPrimary.ShouldBeTrue();
		}

		[Test]
		public void UpdatePrimaryBusinessUnit_ValidBusinessUnitsProvided_UpdatesBusinessUnitToBeNonPrimary()
		{
			// Arrange
			const long currentBUID = 1546755;
			const long newBUID = 5533272;
			//const string employerNewName = "Andy Test 1";

			// Act
			_employerHelper.UpdatePrimaryBusinessUnit(currentBUID, newBUID);
			var oldPrimaryBU = RuntimeContext.Repositories.Core.BusinessUnits.FirstOrDefault(x => x.Id == currentBUID);

			// Assert
			oldPrimaryBU.ShouldNotBeNull();
			oldPrimaryBU.IsPrimary.ShouldBeFalse();
		}

		[Test]
		public void UpdatePrimaryBusinessUnit_ValidBusinessUnitsProvided_UpdatesEmployerNameToBeNewPrimaryBusinessUnitsName()
		{
			// Arrange
			const long currentBUID = 1546755;
			const long newBUID = 5533272;
			
			// Act
			_employerHelper.UpdatePrimaryBusinessUnit(currentBUID, newBUID);
			var newPrimaryBU = RuntimeContext.Repositories.Core.BusinessUnits.FirstOrDefault(x => x.Id == newBUID);
			var employer = RuntimeContext.Repositories.Core.Employers.FirstOrDefault(x => x.Id == newPrimaryBU.EmployerId);
			
			// Assert
			employer.ShouldNotBeNull();
			newPrimaryBU.ShouldNotBeNull();
			employer.Name.ShouldEqual(newPrimaryBU.Name);
		}
			
			

		#endregion
	}
}
