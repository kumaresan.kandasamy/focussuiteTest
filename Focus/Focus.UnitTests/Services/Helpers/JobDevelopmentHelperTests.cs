﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using Focus.Core.Criteria.CandidateSearch;
using Focus.Core.Models.Career;
using Focus.Core.Views;
using Focus.Services.Repositories;
using Focus.UnitTests.Core;
using Framework.Testing.NUnit;
using Moq;
using NUnit.Framework;

#endregion


namespace Focus.UnitTests.Services.Helpers
{
	public class JobDevelopmentHelperTests : TestFixtureBase 
	{
		[Test]
		public void GeneratePersonMatchesForJob_WhenPassedValidJob_DoesNotFail()
		{
			// Arrange
			var lensRepositoryMock = new Mock<ILensRepository>();
			var candidates = new List<ResumeView>
			                 	{
			                 		new ResumeView{ Score = 200, Id = 1094542}
			                 	};

			lensRepositoryMock.Setup(x => x.SearchResumes(It.IsAny<CandidateSearchCriteria>(), It.IsAny<string>())).Returns(candidates);
			lensRepositoryMock.Setup(x => x.IsLicensed()).Returns(true);

			((TestRepositories)RuntimeContext.Repositories).Lens = lensRepositoryMock.Object;
			
			// Act
			RuntimeContext.Helpers.JobDevelopment.GeneratePersonMatchesForPosting(1197833, false, "**_**");

			// Assert
		}

		[Test]
		public void GeneratePostingMatchesForPerson_WhenPassedValidPerson_DoesNotFail()
		{
			// Arrange
			var lensRepositoryMock = new Mock<ILensRepository>();
			var postings = new List<PostingSearchResultView>
			                 	{
			                 		new PostingSearchResultView{ Rank = 600, Id = "careerjobs_934CBC5B65FE459E8BAAB8EDEB966F13", JobTitle = "Test Job Match", Employer = "Test Employer", OriginId = 999 }
			                 	};

			lensRepositoryMock.Setup(x => x.SearchPostings(It.IsAny<SearchCriteria>(), It.IsAny<string>(), false, null)).Returns(postings);
			lensRepositoryMock.Setup(x => x.IsLicensed()).Returns(true);

			((TestRepositories)RuntimeContext.Repositories).Lens = lensRepositoryMock.Object;

			// Act
			RuntimeContext.Helpers.JobDevelopment.GeneratePostingMatchesForPerson(1094542, "**_**");

			// Assert
		}

		[Test]
		public void GeneratePostingMatchesForPerson_WhenPassedInvalidPerson_DoesNotFail()
		{
			// Arrange
			var lensRepositoryMock = new Mock<ILensRepository>();
			var postings = new List<PostingSearchResultView>
			                 	{
			                 		new PostingSearchResultView{ Rank = 600, Id = "careerjobs_934CBC5B65FE459E8BAAB8EDEB966F13", JobTitle = "Test Job Match", Employer = "Test Employer", OriginId = 999 }
			                 	};

			lensRepositoryMock.Setup(x => x.SearchPostings(It.IsAny<SearchCriteria>(), It.IsAny<string>(), false, null)).Returns(postings);
			lensRepositoryMock.Setup(x => x.IsLicensed()).Returns(true);

			((TestRepositories)RuntimeContext.Repositories).Lens = lensRepositoryMock.Object;

			// Act
			RuntimeContext.Helpers.JobDevelopment.GeneratePostingMatchesForPerson(0, "**_**");

			// Assert
		}

		[Test]
		public void GeneratePersonMatchesForJob_WhenPassedValidJobAndClosingDateChangedAndExtendVeteranPriorityForLifeOfPosting_DoesNotFailVeteranPriorityEndDateIsSameAsClosedDate()
		{
			MockAppSettings.ExtendedVeteranPriorityofService = true;
			MockAppSettings.VeteranPriorityServiceEnabled = true;

			// Arrange
			var lensRepositoryMock = new Mock<ILensRepository>();
			var candidates = new List<ResumeView>
			                 	{
			                 		new ResumeView{ Score = 200, Id = 1094542}
			                 	};

			lensRepositoryMock.Setup(x => x.SearchResumes(It.IsAny<CandidateSearchCriteria>(), It.IsAny<string>())).Returns(candidates);
			lensRepositoryMock.Setup(x => x.IsLicensed()).Returns(true);

			((TestRepositories)RuntimeContext.Repositories).Lens = lensRepositoryMock.Object;

			// Act
			RuntimeContext.Helpers.JobDevelopment.GeneratePersonMatchesForPosting(1197833, true, "**_**");
			
			var posting = DataCoreRepository.Postings.FirstOrDefault(x => x.Id == 1197833);

			// Assert
			posting.ShouldNotBeNull();

			var jobDetails = DataCoreRepository.Jobs.Where(j => j.Id == posting.JobId)
 	 																					  .Select(j => new
																						  {
																								j.VeteranPriorityEndDate,
																								j.ClosingOn,
																								j.ClosingOnUpdated
																							})
																							.FirstOrDefault();

			jobDetails.ShouldNotBeNull();

			if (jobDetails != null)
			{
				var veteranPriorityEndDate = Convert.ToDateTime(jobDetails.ClosingOn);
				var ts = new TimeSpan(23, 59, 59);
				veteranPriorityEndDate = veteranPriorityEndDate.Date + ts;

				jobDetails.VeteranPriorityEndDate.ShouldBeTheSameDateTimeAs(veteranPriorityEndDate);
				jobDetails.ClosingOnUpdated.ShouldEqual(false);
			}

			MockAppSettings.ExtendedVeteranPriorityofService = false;
			MockAppSettings.VeteranPriorityServiceEnabled = false;
		}

		[Test]
		public void GeneratePersonMatchesForJob_WhenPassedValidJobAndClosingDateChangedAndExtendVeteranPriorityUntil_DoesNotFailVeteranPriorityEndDateIsSameAsClosedDate()
		{
			MockAppSettings.ExtendedVeteranPriorityofService = true;
			MockAppSettings.VeteranPriorityServiceEnabled = true;

			// Arrange
			var lensRepositoryMock = new Mock<ILensRepository>();
			var candidates = new List<ResumeView>
			                 	{
			                 		new ResumeView{ Score = 200, Id = 1094542}
			                 	};

			lensRepositoryMock.Setup(x => x.SearchResumes(It.IsAny<CandidateSearchCriteria>(), It.IsAny<string>())).Returns(candidates);
			lensRepositoryMock.Setup(x => x.IsLicensed()).Returns(true);

			((TestRepositories)RuntimeContext.Repositories).Lens = lensRepositoryMock.Object;

			// Act
			RuntimeContext.Helpers.JobDevelopment.GeneratePersonMatchesForPosting(1197390, true, "**_**");

			var posting = DataCoreRepository.Postings.FirstOrDefault(x => x.Id == 1197390);

			// Assert
			posting.ShouldNotBeNull();

			var beforeMidnightClosingOn = Convert.ToDateTime(posting.Job.ClosingOn);
			var ts = new TimeSpan(23, 59, 59);
			beforeMidnightClosingOn = beforeMidnightClosingOn.Date + ts;

			posting.Job.VeteranPriorityEndDate.ShouldNotBeTheSameDateTimeAs(beforeMidnightClosingOn);

			MockAppSettings.ExtendedVeteranPriorityofService = false;
			MockAppSettings.VeteranPriorityServiceEnabled = false;
		}
	}
}
