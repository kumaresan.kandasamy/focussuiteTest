﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Transactions;
using Focus.Core;
using Focus.Core.Criteria;
using Focus.Core.Criteria.Onet;
using Focus.Core.Messages;
using Focus.Core.Messages.SearchService;
using Focus.Services.Helpers;
using Focus.Services.Messages;
using Focus.Services.Messages.Handlers;
using Focus.Services.Repositories;
using Focus.Services.Repositories.Lens;
using Focus.UnitTests.Core;
using Framework.Testing.NUnit;

using Framework.Messaging;
using Framework.ServiceLocation;
using Moq;
using NUnit.Framework;

#endregion

namespace Focus.UnitTests.Services.Helpers
{
  public class OccupationelperTests : TestFixtureBase
  {
	  [Test]
	  public void GetROnetById_WhenPassedAValidId_ReturnsROnetView()
	  {
		  // Arrange
		  var occupationHelper = new OccupationHelper(RuntimeContext);

		  // Act
		  var result = occupationHelper.GetROnetById(29, "en-GB");

			// Assert
			result.ShouldNotBeNull();
	  }

	  [Test]
	  public void GetROnets_WhenPassedValidCriteria_ReturnsROnetDetails()
	  {
		  // Arrange
			var lensRepository = new LensRepository(RuntimeContext);

			((TestRepositories)RuntimeContext.Repositories).Lens = lensRepository;
			var occupationHelper = new OccupationHelper(RuntimeContext);
		  const int listSize = 5;
			var criteria = new OnetCriteria { JobTitle = "Software developer", ListSize = listSize, FetchOption = CriteriaBase.FetchOptions.List };

		  // Act
		  var result = occupationHelper.GetROnets(criteria, Constants.DefaultCulture);

		  // Assert
			result.ShouldNotBeNull();
			result.Count.ShouldBeLessOrEqual(listSize);
	  }
  }
}
