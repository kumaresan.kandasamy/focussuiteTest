﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Xml.Linq;
using Focus.Core.DataTransferObjects.FocusExplorer;
using Focus.Services.Repositories;
using Focus.Services.Repositories.Lens;
using Focus.UnitTests.Core;
using Framework.Testing.NUnit;

using Moq;
using NUnit.Framework;

#endregion

namespace Focus.UnitTests.Services.Helpers
{
	public class PostingHelperTests : TestFixtureBase
	{
		[Test]
		public void RegisterPosting_WithValidId()
		{
			// Arrange
			var lensRepository = new Mock<ILensRepository>();
			var connectedLensRepository = new LensRepository(RuntimeContext);

			lensRepository.Setup(x => x.RegisterPosting(It.IsAny<string>(), It.IsAny<long>(), It.IsAny<XDocument>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<System.Collections.Generic.List<int>>(), It.IsAny<PostalCodeDto>()));
			lensRepository.Setup(x => x.CanonPostingWithJobMine(It.IsAny<string>())).Returns((string s) => connectedLensRepository.CanonPostingWithJobMine(s));
			((TestRepositories)RuntimeContext.Repositories).Lens = lensRepository.Object;

			// Act
			RuntimeContext.Helpers.Posting.RegisterPosting(1120577);

			// Assert
		}

		[Test]
		public void UnregisterPosting_WithValidId()
		{
			// Arrange
			var lensRepository = new Mock<ILensRepository>();
			lensRepository.Setup(x => x.UnregisterPosting(It.IsAny<string>()));
			((TestRepositories)RuntimeContext.Repositories).Lens = lensRepository.Object;

			// Act
			RuntimeContext.Helpers.Posting.UnregisterPosting(1120577);

			// Assert
		}
	}
}
