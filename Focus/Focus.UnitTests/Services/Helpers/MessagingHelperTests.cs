﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Transactions;
using System.Xml.Linq;
using Focus.Core.DataTransferObjects.FocusExplorer;
using Focus.Services.Helpers;
using Focus.Services.Messages;
using Focus.Services.Messages.Handlers;
using Focus.Services.Repositories;
using Focus.UnitTests.Core;
using Framework.Testing.NUnit;

using Framework.Messaging;
using Framework.ServiceLocation;
using Moq;
using NUnit.Framework;

#endregion

namespace Focus.UnitTests.Services.Helpers
{
	public class MessagingHelperTests : TestFixtureBase 
	{
		[SetUp]
		public override void SetUp()
		{
			base.SetUp();

			// Setup the Service Locator
			var serviceLocator = new StructureMapServiceLocator();
			ServiceLocator.SetServiceLocator(serviceLocator);

			// Register the Message Store for the message bus
			serviceLocator.Register<IMessageStore>(() => new MessageStore());

			// Register the message handlers			
			serviceLocator.Register<IMessageHandler<RegisterPostingMessage>>(() => new RegisterPostingMessageHandler());

			serviceLocator.Register(() => RuntimeContext.AppSettings);
			serviceLocator.Register(() => RuntimeContext.Repositories);
			serviceLocator.Register(() => RuntimeContext.Helpers);
		}

		[Test]
		public void PublishTest()
		{
			var messageHelper = new MessagingHelper(RuntimeContext);
			
			using (var helperTransactionScope = new TransactionScope(TransactionScopeOption.RequiresNew, new TimeSpan(0, 10, 0)))
			{
				messageHelper.Publish(new RegisterPostingMessage {ActionerId = 1, Culture = "**_**", PostingId = 3});
			}
		}

		[Test]
		[Explicit]
		public void PublishThenProcess()
		{
			// Made Explicit as I cannot get it to work within a transaction scope
			
			var lensRepository = new Mock<ILensRepository>();
			lensRepository.Setup(x => x.RegisterPosting(It.IsAny<string>(), It.IsAny<long>(), It.IsAny<XDocument>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<System.Collections.Generic.List<int>>(), It.IsAny<PostalCodeDto>()));
			((TestRepositories)RuntimeContext.Repositories).Lens = lensRepository.Object;

			var messageHelper = new MessagingHelper(RuntimeContext);

			using (var helperTransactionScope = new TransactionScope(TransactionScopeOption.Suppress))
			{
				messageHelper.Publish(new RegisterPostingMessage { ActionerId = 1, Culture = "**_**", PostingId = 1120577 });
				messageHelper.ProcessMessages();
			}
		}
	}
}
