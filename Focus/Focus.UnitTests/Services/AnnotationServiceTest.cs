﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using Focus.Core;
using Focus.Core.Criteria;
using Focus.Core.Criteria.Bookmark;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Messages.AnnotationService;
using Focus.Core.Models.Career;
using Focus.UnitTests.Core;
using Moq;
using NUnit.Framework;

using Focus.Services.Repositories;
using Focus.Services.ServiceImplementations;
using Framework.Testing.NUnit;
using AcknowledgementType = Focus.Core.Messages.AcknowledgementType;

#endregion

namespace Focus.UnitTests.Services
{
  [TestFixture]
  public class AnnotationServiceTests : TestFixtureBase
  {
    private AnnotationService _service;

    [SetUp]
    public override void SetUp()
    {
      base.SetUp();

      SetUpCacheProvider();
      SetUpMockLogProvider();
			_service = new AnnotationService(RuntimeContext);
    }

    [Test]
    public void GetBookmark_WhenPassedValidRequest_ReturnsSuccess()
    {
      // Arrange
      var request = new BookmarkRequest { Criteria = new BookmarkCriteria { BookmarkId = 1129221 } };
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.GetBookmark(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.Bookmark.ShouldNotBeNull();
    }

    [Test]
    public void GetBookmark_WhenPassedValidRequestForList_ReturnsSuccess()
    {
      // Arrange
      var request = new BookmarkRequest { Criteria = new BookmarkCriteria { FetchOption = CriteriaBase.FetchOptions.List, BookmarkType = BookmarkTypes.Report } };
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.GetBookmark(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.Bookmarks.ShouldNotBeNull();
    }

    [Test]
    public void CreateBookmark_WithValidRequestForCareer_ReturnsSuccess()
    {
      // Arrange
      var request = new CreateBookmarkRequest();
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
      request.Bookmark = new BookmarkDto
                           {
                             EntityId = 1003791,
                             UserId = 1000048,
                             UserType = UserTypes.Career,
                             Type = BookmarkTypes.Posting,
                             Name = "Ryan Test"
                           };

      // Act
      var response = _service.CreateBookmark(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
    }

    [Test]
    public void CreateBookmark_WithValidRequestForExplorer_ReturnsSuccess()
    {
      // Arrange
      var request = new CreateBookmarkRequest 
      { 
        Bookmark = new BookmarkDto 
        { 
          Name = "UnitTest", 
          Criteria = "Criteria", 
          Type = BookmarkTypes.Report, 
          UserType = UserTypes.Explorer 
        } 
      };
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.CreateBookmark(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
    }

    [Test]
    public void DeleteBookmark_WhenPassedValidRequest_ReturnsSuccess()
    {
      // Arrange
      var request = new DeleteBookmarkRequest { BookmarkId = 1129753 };
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.DeleteBookmark(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
    }
		
    [Test]
    public void SaveSearch_WithValidRequest_ReturnsSuccess()
    {
      // Arrange
      var request = new SaveSearchRequest
	                    {
		                    Name = "Test search",
												SearchCriteria = new SearchCriteria(),
												AlertEmailAddress = "test@test.com"
	                    };

      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.SaveSearch(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
    }

    [Test]
    public void ListSearch_WithValidRequest_ReturnsSuccess()
    {
      // Arrange
      var request = new ListSearchRequest();
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.ListSearch(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
    }

    [Test]
    public void DeleteSearch_WithValidRequest_ReturnsSuccess()
    {
      // Arrange
      var request = new DeleteSaveSearchRequest();
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
      request.Id = 1004572;

      // Act
      var response = _service.DeleteSaveSearch(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
    }
   
  }
}
