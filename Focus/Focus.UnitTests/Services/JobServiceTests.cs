﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Focus.Core.Criteria.JobDrivingLicenceEndorsement;
using Focus.Core.Criteria.JobProgramsOfStudy;
using Focus.Core.Criteria.OnetTask;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Messages.JobService;
using Focus.Services.DtoMappers;
using Focus.Services.Repositories.Lens;
using Focus.UnitTests.Core;
using NUnit.Framework;

using Focus.Core;
using Focus.Core.Criteria;
using Focus.Data.Core.Entities;
using Focus.Core.Criteria.Job;
using Focus.Core.Criteria.JobCertificate;
using Focus.Core.Criteria.JobLanguage;
using Focus.Core.Criteria.JobLicence;
using Focus.Core.Criteria.JobSpecialRequirement;
using Focus.Core.Criteria.JobTask;
using Focus.Services.ServiceImplementations;
using Framework.Testing.NUnit;

using AcknowledgementType = Focus.Core.Messages.AcknowledgementType;
using JobLocationCriteria = Focus.Core.Criteria.JobLocation.JobLocationCriteria;
using SaveJobRequest = Focus.Core.Messages.JobService.SaveJobRequest;

#endregion

namespace Focus.UnitTests.Services
{
	[TestFixture]
	public class JobServiceTests : TestFixtureBase
	{
		private JobService _jobService;

		[SetUp]
		public override void SetUp()
		{
			base.SetUp();

			SetUpCacheProvider();
			SetUpMockLogProvider();

			_jobService = new JobService(RuntimeContext);
		}

		[Test]
		public void GetJobOnetTasks_WhenPassedValidRequest_ReturnsSuccess()
		{
		 
			// Arrange
			var request = new JobOnetTaskRequest {Criteria = new OnetTaskCriteria {OnetTaskId = 1620257}};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _jobService.GetJobOnetTasks(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void GetJobPostingReferralView_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new JobPostingReferralViewRequest {Criteria = new JobPostingReferralCriteria {Id = 1132078}};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _jobService.GetJobPostingReferralView(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Referral.ShouldNotBeNull();
			response.Referral.Id.ShouldEqual(1132078);
		}

		[Test]
		public void GetJobPostingReferralView_WhenPassedValidRequestWithOfficeId_ReturnsSuccess()
		{
			// Arrange
			var request = new JobPostingReferralViewRequest
			{
				Criteria =
					new JobPostingReferralCriteria
					{
						OfficeIds = new List<long?> {4, 2},
						FetchOption = CriteriaBase.FetchOptions.PagedList
					}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _jobService.GetJobPostingReferralView(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.ReferralViewsPaged.ShouldNotBeNull();
			response.ReferralViewsPaged.Count.ShouldEqual(1);
			response.ReferralViewsPaged[0].Id.ShouldEqual(1101140);
		}

		[Test]
		public void GetJobPostingReferralView_WhenPassedValidRequestWithAssignedToId_ReturnsSuccess()
		{
			// Arrange
			var request = new JobPostingReferralViewRequest
			{
				Criteria =
					new JobPostingReferralCriteria {StaffMemberId = 1093026, FetchOption = CriteriaBase.FetchOptions.PagedList}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _jobService.GetJobPostingReferralView(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.ReferralViewsPaged.ShouldNotBeNull();
			response.ReferralViewsPaged.Count.ShouldEqual(1);
			response.ReferralViewsPaged[0].Id.ShouldEqual(1180944);
		}

		[Test]
		public void GetJobPostingReferralView_WhenPassedValidRequestWithNoFixedLocation_ReturnsSuccess()
		{
			// Arrange
			var request = new JobPostingReferralViewRequest
			{
				Criteria =
					new JobPostingReferralCriteria
					{
						JobType = JobListFilter.NoFixedLocation,
						FetchOption = CriteriaBase.FetchOptions.PagedList
					}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _jobService.GetJobPostingReferralView(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.ReferralViewsPaged.ShouldNotBeNull();
			response.ReferralViewsPaged.Count.ShouldEqual(1);
			response.ReferralViewsPaged[0].Id.ShouldEqual(1101140);
		}

		[Test]
		public void ApproveReferral_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new JobRequest {Criteria = new JobCriteria {JobId = 1101140}, Module = MockAppSettings.Module};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _jobService.ApproveReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}



		[Test]
		public void EditPostingReferral_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new JobRequest {Criteria = new JobCriteria {JobId = 1101140}};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _jobService.EditPostingReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void GetJobLookup_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new JobLookupRequest {Criteria = new JobLookupCriteria {JobId = 1099005}};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _jobService.GetJobLookup(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void GetJobStatus_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new JobStatusRequest {JobId = 1099005};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _jobService.GetJobStatus(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void GetApplicationInstructions_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new ApplicationInstructionsRequest {JobId = 1099005};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _jobService.GetApplicationInstructions(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void GeneratePosting_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var job = DataCoreUnitOfWorkScope.Current.Jobs.Single(x => x.Id == 1099005).AsDto();

			var request = new GeneratePostingRequest {Job = job};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _jobService.GeneratePosting(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void GetJobActivityUsers_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new JobActivityUsersRequest {JobId = 1099005};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _jobService.GetJobActivityUsers(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Ignore("to fix")]
		[Test]
		public void PrintJob_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new PrintJobRequest {JobId = 1099005};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _jobService.PrintJob(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void GetJobActivitySummary_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new JobDetailsRequest {JobId = 1099005};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _jobService.GetJobActivitySummary(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void SaveJobInviteToApply_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new SaveJobInviteToApplyRequest
			{
				PersonId = 1087251,
				JobId = 1099005,
				LensPostingId = "ABC123_2FAAC441A9EC4B67BF5CDFD4B66384C4"
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _jobService.SaveJobInviteToApply(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void UpdateInviteToApply_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new InviteToApplyVisitedRequest
			{
				PersonId = 1087251,
				LensPostingId = "ABC123_2FAAC441A9EC4B67BF5CDFD4B66384C4"
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _jobService.UpdateInviteToApply(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.IsRecommended.ShouldBeFalse();
		}

		[Test]
		public void UpdateInviteToApply_WhenPassedValidRequestWhereRecommendation_ReturnsSuccess()
		{
			// Arrange
			var request = new InviteToApplyVisitedRequest
			{
				PersonId = 1197307,
				LensPostingId = "ABC123_2FAAC441A9EC4B67BF5CDFD4B66384C4"
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _jobService.UpdateInviteToApply(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.IsRecommended.ShouldBeTrue();
		}

		[Test]
		public void GetJobApplicants_WhenPassedValidRequest_ReturnsSuccessAndData()
		{
			// Act
			var request = new JobApplicantsRequest {JobId = 1093800};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Arrange
			var response = _jobService.GetJobApplicants(request);

			// Assert
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Applicants.Count.ShouldEqual(6);
		}

		[Test]
		public void GetJobView_WhenActiveJobsRequestedAndOrderByJobTitleDesc_ReturnsPagedDataInCorrectOrder()
		{
			//Arrange
			var request = new JobViewRequest
			{
				Criteria =
					new JobCriteria
					{
						EmployerId = 29441,
						JobStatus = JobStatuses.Active,
						PageIndex = 0,
						PageSize = 10,
						OrderBy = "JobTitle Desc",
						FetchOption = CriteriaBase.FetchOptions.PagedList
					}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			request.Module = MockAppSettings.Module;

			// Act	
			var jobResponse = _jobService.GetJobView(request);

			// Assert 
			jobResponse.ShouldNotBeNull();
			jobResponse.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			jobResponse.JobViewsPaged.Count.ShouldEqual(10);
			jobResponse.JobViewsPaged.PageIndex.ShouldEqual(0);
			jobResponse.JobViewsPaged.PageSize.ShouldEqual(10);
			jobResponse.JobViewsPaged[0].JobTitle.ShouldEqual("Web Developer 2");
			jobResponse.JobViewsPaged[9].JobTitle.ShouldEqual("Software developer 4");
			jobResponse.JobView.ShouldBeNull();
			jobResponse.JobViews.ShouldBeNull();
		}

		[Test]
		public void GetJobView_WhenActiveJobsRequestedAndOrderByJobTitleAsc_ReturnsPagedDataInCorrectOrder()
		{
			// Arrange
			var request = new JobViewRequest
			{
				Criteria =
					new JobCriteria
					{
						EmployerId = 29441,
						JobStatus = JobStatuses.Active,
						PageIndex = 0,
						PageSize = 10,
						OrderBy = "JobTitle Asc",
						FetchOption = CriteriaBase.FetchOptions.PagedList
					}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			request.Module = MockAppSettings.Module;

			// Act	
			var jobResponse = _jobService.GetJobView(request);

			// Assert 
			jobResponse.ShouldNotBeNull();
			jobResponse.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			jobResponse.JobViewsPaged.ShouldNotBeNull();
			jobResponse.JobViewsPaged.Count.ShouldEqual(10);
			jobResponse.JobViewsPaged.PageIndex.ShouldEqual(0);
			jobResponse.JobViewsPaged.PageSize.ShouldEqual(10);
			jobResponse.JobViewsPaged[0].JobTitle.ShouldEqual("Electrician");
			jobResponse.JobViewsPaged[9].JobTitle.ShouldEqual("programmer intern");
			jobResponse.JobView.ShouldBeNull();
			jobResponse.JobViews.ShouldBeNull();
		}

		[Test]
		public void GetJobView_WhenActiveJobsRequestedWithJobId_ReturnsTheJobRequested()
		{
			// Arrange
			var request = new JobViewRequest
			{
				Criteria = new JobCriteria {EmployerId = 29441, JobId = 1121522, FetchOption = CriteriaBase.FetchOptions.Single}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			request.Module = MockAppSettings.Module;

			// Act	
			var jobResponse = _jobService.GetJobView(request);

			// Assert 
			jobResponse.ShouldNotBeNull();
			jobResponse.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			jobResponse.JobView.ShouldNotBeNull();
			jobResponse.JobView.JobTitle.ShouldEqual("Electrician");
			jobResponse.JobViewsPaged.ShouldBeNull();
			jobResponse.JobViews.ShouldBeNull();
		}

		[Test]
		public void GetJobView_WhenActiveJobsRequestedAndOrderByJobTitleDesc_ReturnsListInCorrectOrder()
		{
			//Arrange
			var request = new JobViewRequest
			{
				Criteria =
					new JobCriteria
					{
						EmployerId = 29441,
						JobStatus = JobStatuses.Active,
						PageIndex = 0,
						PageSize = 10,
						OrderBy = "JobTitle Desc",
						FetchOption = CriteriaBase.FetchOptions.PagedList
					}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			request.Module = MockAppSettings.Module;

			// Act	
			var jobResponse = _jobService.GetJobView(request);

			// Assert 
			jobResponse.ShouldNotBeNull();
			jobResponse.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			jobResponse.JobViewsPaged.Count.ShouldEqual(10);
			jobResponse.JobViewsPaged.PageIndex.ShouldEqual(0);
			jobResponse.JobViewsPaged.PageSize.ShouldEqual(10);
			jobResponse.JobViewsPaged[0].JobTitle.ShouldEqual("Web Developer 2");
			jobResponse.JobViewsPaged[9].JobTitle.ShouldEqual("Software developer 4");
			jobResponse.JobView.ShouldBeNull();
			jobResponse.JobViews.ShouldBeNull();
		}

		[Test]
		public void GetJobView_WhenActiveJobsRequestedAndOrderByJobTitleAsc_ReturnsListInCorrectOrder()
		{
			// Arrange
			var request = new JobViewRequest
			{
				Criteria =
					new JobCriteria
					{
						JobStatus = JobStatuses.Active,
						OrderBy = "JobTitle Asc",
						FetchOption = CriteriaBase.FetchOptions.List
					}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			request.Module = MockAppSettings.Module;

			// Act	
			var jobResponse = _jobService.GetJobView(request);

			// Assert 
			jobResponse.ShouldNotBeNull();
			jobResponse.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			jobResponse.JobViews.ShouldNotBeNull();
			jobResponse.JobViews.Count().ShouldBeGreater(10);
			jobResponse.JobView.ShouldBeNull();
			jobResponse.JobViewsPaged.ShouldBeNull();
		}

		[Test]
		public void GetJobView_WhenActiveJobsRequestedByUserId_ReturnsListInCorrectOrder()
		{
			// Arrange
			var request = new JobViewRequest
			{
				Criteria =
					new JobCriteria {EmployerId = 29441, FetchOption = CriteriaBase.FetchOptions.List, JobStatus = JobStatuses.Active}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			request.Module = MockAppSettings.Module;

			// Act	
			var jobResponse = _jobService.GetJobView(request);

			// Assert 
			jobResponse.ShouldNotBeNull();
			jobResponse.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			jobResponse.JobViews.ShouldNotBeNull();
			jobResponse.JobViews.Count().ShouldEqual(27);
			jobResponse.JobView.ShouldBeNull();
			jobResponse.JobViewsPaged.ShouldBeNull();
		}

		[Test]
		public void GetJobView_WhenRequestedByOfficeId_ReturnsList()
		{
			// Arrange
			var request = new JobViewRequest
			{
				Criteria = new JobCriteria {OfficeIds = new List<long?> {4}, FetchOption = CriteriaBase.FetchOptions.List}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			request.Module = MockAppSettings.Module;

			// Act	
			var jobResponse = _jobService.GetJobView(request);

			// Assert 
			jobResponse.ShouldNotBeNull();
			jobResponse.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			jobResponse.JobViews.ShouldNotBeNull();
			jobResponse.JobViews.Count().ShouldEqual(4);
		}

		[Test]
		public void GetJobView_WhenRequestedByOfficeId_ReturnsPagedList()
		{
			// Arrange
			var request = new JobViewRequest
			{
				Criteria = new JobCriteria {OfficeIds = new List<long?> {4}, FetchOption = CriteriaBase.FetchOptions.PagedList}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			request.Module = MockAppSettings.Module;

			// Act	
			var jobResponse = _jobService.GetJobView(request);

			// Assert 
			jobResponse.ShouldNotBeNull();
			jobResponse.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			jobResponse.JobViewsPaged.ShouldNotBeNull();
			jobResponse.JobViewsPaged.Count().ShouldEqual(4);
		}

		[Test]
		public void GetJobView_WhenRequestedByStaffMember_ReturnsList()
		{
			// Arrange
			var request = new JobViewRequest
			{
				Criteria = new JobCriteria {StaffMemberId = 1093026, FetchOption = CriteriaBase.FetchOptions.List}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			request.Module = MockAppSettings.Module;

			// Act	
			var jobResponse = _jobService.GetJobView(request);

			// Assert 
			jobResponse.ShouldNotBeNull();
			jobResponse.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			jobResponse.JobViews.ShouldNotBeNull();
			jobResponse.JobViews.Count().ShouldEqual(1);
			jobResponse.JobViews[0].AssignedToId.ShouldEqual(1093026);
		}

		[Test]
		public void GetJobView_WhenRequestedByStaffMember_ReturnsPagedList()
		{
			// Arrange
			var request = new JobViewRequest
			{
				Criteria = new JobCriteria {StaffMemberId = 1093026, FetchOption = CriteriaBase.FetchOptions.PagedList}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			request.Module = MockAppSettings.Module;

			// Act	
			var jobResponse = _jobService.GetJobView(request);

			// Assert 
			jobResponse.ShouldNotBeNull();
			jobResponse.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			jobResponse.JobViewsPaged.ShouldNotBeNull();
			jobResponse.JobViewsPaged.Count().ShouldEqual(1);
			jobResponse.JobViewsPaged[0].AssignedToId.ShouldEqual(1093026);
		}

		[Test]
		public void GetJobView_WhenRequestedByBusinessUnit_ReturnsList()
		{
			// Arrange
			var request = new JobViewRequest
			{
				Criteria = new JobCriteria
				{
					BusinessUnitId = 5533272,
					FetchOption = CriteriaBase.FetchOptions.List
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			request.Module = MockAppSettings.Module;

			// Act	
			var jobResponse = _jobService.GetJobView(request);

			// Assert 
			jobResponse.ShouldNotBeNull();
			jobResponse.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			jobResponse.JobViews.ShouldNotBeNull();
			jobResponse.JobViews.Count().ShouldBeGreater(100);
			jobResponse.JobViews.All(job => job.BusinessUnitId == request.Criteria.BusinessUnitId).ShouldBeTrue();
		}

		[Test]
		public void GetJobView_WhenRequestedByBusinessUnitButInvalidEmployee_ReturnsFailure()
		{
			// Arrange
			var request = new JobViewRequest
			{
				Criteria = new JobCriteria
				{
					BusinessUnitId = 5533272,
					ValidateEmployeeId = 1000054,
					FetchOption = CriteriaBase.FetchOptions.List
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			request.Module = MockAppSettings.Module;

			// Act	
			var jobResponse = _jobService.GetJobView(request);

			// Assert 
			jobResponse.ShouldNotBeNull();
			jobResponse.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			jobResponse.JobViews.ShouldBeNull();
		}

		[Test]
		public void GetJobView_WhenRequestedByEmployer_ReturnsList()
		{
			// Arrange
			var request = new JobViewRequest
			{
				Criteria = new JobCriteria
				{
					EmployerId = 29441,
					FetchOption = CriteriaBase.FetchOptions.List
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			request.Module = MockAppSettings.Module;

			// Act	
			var jobResponse = _jobService.GetJobView(request);

			// Assert 
			jobResponse.ShouldNotBeNull();
			jobResponse.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			jobResponse.JobViews.ShouldNotBeNull();
			jobResponse.JobViews.Count().ShouldBeGreater(100);
			jobResponse.JobViews.All(job => job.EmployerId == request.Criteria.EmployerId).ShouldBeTrue();
		}

		[Test]
		public void GetJobView_WhenRequestedByEmployerButInvalidEmployee_ReturnsFailure()
		{
			// Arrange
			var request = new JobViewRequest
			{
				Criteria = new JobCriteria
				{
					EmployerId = 29441,
					ValidateEmployeeId = 1000054,
					FetchOption = CriteriaBase.FetchOptions.List
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			request.Module = MockAppSettings.Module;

			// Act	
			var jobResponse = _jobService.GetJobView(request);

			// Assert 
			jobResponse.ShouldNotBeNull();
			jobResponse.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			jobResponse.JobViews.ShouldBeNull();
		}

		[Test]
		public void GetJobDetails_WhenActiveJobsRequestedWithJobId_ReturnsTheJobRequested()
		{
			// Arrange
			var request = new JobDetailsRequest {JobId = 1121522, Module = MockAppSettings.Module};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act	
			var jobResponse = _jobService.GetJobDetails(request);

			// Assert 
			jobResponse.ShouldNotBeNull();
			jobResponse.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			jobResponse.JobDetails.ShouldNotBeNull();
			jobResponse.JobDetails.JobTitle.ShouldEqual("Electrician");
		}

		[Test]
		public void GetJobDetails_WhenActiveJobsRequestedWithJobIdBelongingToAnotherEmployer_ReturnsFailure()
		{
			// Arrange
			var request = new JobDetailsRequest {JobId = 1779866, Module = MockAppSettings.Module};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act	
			var jobResponse = _jobService.GetJobDetails(request);

			// Assert 
			jobResponse.ShouldNotBeNull();
			jobResponse.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			jobResponse.Message.ShouldEqual(_jobService.FormatErrorMessage(request, ErrorTypes.JobNotFound));
			jobResponse.JobDetails.ShouldBeNull();
		}

		[Test]
		public void GetSimilarJobs_WhenJobsRequestedWithJobTitle_ReturnsTheJobRequested()
		{
			// Arrange
			var request = new SimilarJobRequest {JobTitle = "Software"};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			var lensRepository = new LensRepository(RuntimeContext);
			((TestRepositories) RuntimeContext.Repositories).Lens = lensRepository;

			// Act	
			var service = new JobService(RuntimeContext);
			var jobResponse = service.GetSimilarJobs(request);

			// Assert 
			jobResponse.ShouldNotBeNull();
			jobResponse.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			jobResponse.Jobs.ShouldNotBeNull();
			jobResponse.Jobs.Count.ShouldBeGreater(0);
		}

		[Test]
		public void UpdateJobStatus_WhenActiveJobMarkedClosed_UpdateFails()
		{
			// Arrange
			var request = new JobRequest
			{
				JobStatus = JobStatuses.Closed,
				Criteria =
					new JobCriteria {JobId = 90750, FetchOption = CriteriaBase.FetchOptions.Single, JobStatus = JobStatuses.Active}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act	
			var jobResponse = _jobService.UpdateJobStatus(request);

			// Assert 
			jobResponse.ShouldNotBeNull();
			jobResponse.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}

		[Test]
		public void UpdateJobStatus_WhenActiveJobMarkedOnHold_JobIsUpdated()
		{
			// Arrange
			var request = new JobRequest
			{
				JobStatus = JobStatuses.OnHold,
				Criteria =
					new JobCriteria {JobId = 1121522, FetchOption = CriteriaBase.FetchOptions.Single, JobStatus = JobStatuses.Active}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var jobResponse = _jobService.UpdateJobStatus(request);

			// Assert 
			jobResponse.ShouldNotBeNull();
			jobResponse.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			jobResponse.Job.ShouldNotBeNull();
			jobResponse.Job.JobStatus.ShouldEqual(JobStatuses.OnHold);
			jobResponse.JobsPaged.ShouldBeNull();
			jobResponse.Jobs.ShouldBeNull();
		}

		[Test]
		public void UpdateJobStatus_WhenClosedJobMarkedActive_JobIsUpdated()
		{
			// Arrange
			var job = DataCoreRepository.Jobs.First(x => x.Id == 1003735);
			job.JobStatus = JobStatuses.Closed;
			DataCoreRepository.SaveChanges();

			var request = new JobRequest
			{
				JobStatus = JobStatuses.Active,
				Criteria = new JobCriteria {JobId = 1003735, FetchOption = CriteriaBase.FetchOptions.Single}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var jobResponse = _jobService.UpdateJobStatus(request);

			// Assert 
			jobResponse.ShouldNotBeNull();
			jobResponse.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			jobResponse.Job.ShouldNotBeNull();
			jobResponse.Job.JobStatus.ShouldEqual(JobStatuses.Active);
			jobResponse.JobsPaged.ShouldBeNull();
			jobResponse.Jobs.ShouldBeNull();
		}

		[Test]
		public void DeleteJob_WhenActiveJobDeleted_JobIsRemoved()
		{
			// Arrange
			var request = new JobRequest
			{
				Criteria =
					new JobCriteria {JobId = 1003735, FetchOption = CriteriaBase.FetchOptions.Single, JobStatus = JobStatuses.Active}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var deleteResponse = _jobService.DeleteJob(request);
			var queryResponse = _jobService.GetJob(request);

			// Assert 
			deleteResponse.ShouldNotBeNull();
			deleteResponse.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			queryResponse.Job.ShouldBeNull();
		}

		[Test]
		public void GetJob_ByJobId()
		{
			// Arrange
			var request = new JobRequest
			{
				Criteria = new JobCriteria { JobId = 1003735, FetchOption = CriteriaBase.FetchOptions.Single, JobStatus = JobStatuses.Active }
			};
			request.Prepare( MockClientTag, MockSessionId, MockUserData, MockCulture );

			// Act
			var queryResponse = _jobService.GetJob( request );

			// Assert 
			queryResponse.Job.ShouldNotBeNull();
		}

		[Test]
		public void GetJob_ByJobExternalId()
		{
			// Arrange
			var request = new JobRequest
			{
				Criteria = new JobCriteria { ExternalId = "1", FetchOption = CriteriaBase.FetchOptions.Single, JobStatus = JobStatuses.Active }
			};
			request.Prepare( MockClientTag, MockSessionId, MockUserData, MockCulture );

			// Act
			var queryResponse = _jobService.GetJob( request );

			// Assert 
			queryResponse.Job.ShouldNotBeNull();
			queryResponse.Job.Id.ShouldEqual( 1087610 );
		}

		[Test]
		public void RefreshJob_WhenActiveJobIsRefreshed_JobIsUpdated()
		{
			// Arrange
			var request = new JobRequest
			{
				PostingSurvey =
					new PostingSurveyDto
					{
						DidHire = true,
						DidInterview = true,
						SatisfactionLevel = 10,
						SurveyType = SurveyTypes.JobRefreshSurvey
					},
				Criteria = new JobCriteria { JobId = 1099005, FetchOption = CriteriaBase.FetchOptions.Single, JobStatus = JobStatuses.Active },
				Module = MockAppSettings.Module
			};

			var job = DataCoreRepository.FindById<Job>(1099005);
			var closingDate = DateTime.Now;
			job.ClosedOn = closingDate.AddDays(-1);
			job.JobStatus = JobStatuses.Closed;

			var application = DataCoreRepository.FindById<Application>(1109308);
			// Store values for later
			var approvalStatusStore = application.ApprovalStatus;
			var previousApprovalStatusStore = application.PreviousApprovalStatus;
			var automaticallyOnHoldStore = application.AutomaticallyOnHold;

			// Set up values for test
			application.ApprovalStatus = ApprovalStatuses.OnHold;
			application.PreviousApprovalStatus = ApprovalStatuses.WaitingApproval;
			application.AutomaticallyOnHold = false;
			DataCoreRepository.SaveChanges();

			try
			{
				request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

				// Act
				var jobResponse = _jobService.RefreshJob(request);

				// Assert 
				jobResponse.ShouldNotBeNull();
				jobResponse.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
				jobResponse.Job.ShouldNotBeNull();
				jobResponse.Job.JobStatus.ShouldEqual(JobStatuses.Active);
				jobResponse.JobsPaged.ShouldBeNull();
				jobResponse.Jobs.ShouldBeNull();

				application = DataCoreRepository.FindById<Application>(1109308);
				application.ApprovalStatus.ShouldEqual(ApprovalStatuses.OnHold);
				application.PreviousApprovalStatus.ShouldEqual(ApprovalStatuses.WaitingApproval);
			}
			finally
			{
				// Finally put the data back how it was found
				application = DataCoreRepository.FindById<Application>(1109308);
				application.ApprovalStatus = approvalStatusStore;
				application.PreviousApprovalStatus = previousApprovalStatusStore;
				application.AutomaticallyOnHold = automaticallyOnHoldStore;
				DataCoreRepository.SaveChanges();
			}
		}

		[Test]
		public void RefreshJob_AutomaticallyOnHold_WhenActiveJobIsRefreshed_JobIsUpdated()
		{
			// Arrange
			var request = new JobRequest
			{
				PostingSurvey =
					new PostingSurveyDto
					{
						DidHire = true,
						DidInterview = true,
						SatisfactionLevel = 10,
						SurveyType = SurveyTypes.JobRefreshSurvey
					},
				Criteria = new JobCriteria { JobId = 1099005, FetchOption = CriteriaBase.FetchOptions.Single, JobStatus = JobStatuses.Active },
				Module = MockAppSettings.Module
			};

			var job = DataCoreRepository.FindById<Job>(1099005);
			var closingDate = DateTime.Now;
			job.ClosedOn = closingDate.AddDays(-1);
			job.JobStatus = JobStatuses.Closed;

			var application = DataCoreRepository.FindById<Application>(1109308);
			// Store values for later
			var approvalStatusStore = application.ApprovalStatus;
			var previousApprovalStatusStore = application.PreviousApprovalStatus;
			var automaticallyOnHoldStore = application.AutomaticallyOnHold;

			// Set up values for test
			application.ApprovalStatus = ApprovalStatuses.OnHold;
			application.PreviousApprovalStatus = ApprovalStatuses.WaitingApproval;
			application.AutomaticallyOnHold = true;
			DataCoreRepository.SaveChanges();

			try
			{
				request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

				// Act
				var jobResponse = _jobService.RefreshJob(request);

				// Assert 
				jobResponse.ShouldNotBeNull();
				jobResponse.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
				jobResponse.Job.ShouldNotBeNull();
				jobResponse.Job.JobStatus.ShouldEqual(JobStatuses.Active);
				jobResponse.JobsPaged.ShouldBeNull();
				jobResponse.Jobs.ShouldBeNull();

				application = DataCoreRepository.FindById<Application>(1109308);
				application.ApprovalStatus.ShouldEqual(ApprovalStatuses.WaitingApproval);
				application.PreviousApprovalStatus.ShouldEqual(ApprovalStatuses.WaitingApproval);
			}
			finally
			{
				// Finally put the data back how it was found
				application = DataCoreRepository.FindById<Application>(1109308);
				application.ApprovalStatus = approvalStatusStore;
				application.PreviousApprovalStatus = previousApprovalStatusStore;
				application.AutomaticallyOnHold = automaticallyOnHoldStore;
				DataCoreRepository.SaveChanges();
			}
		}

		[Test]
		public void RefreshJob_WhenClosedJobIsRefreshed_JobIsUpdated()
		{
			// Arrange
			var request = new JobRequest
			{
				PostingSurvey =
					new PostingSurveyDto
					{
						DidHire = true,
						DidInterview = true,
						SatisfactionLevel = 10,
						SurveyType = SurveyTypes.JobRefreshSurvey
					},
				Criteria =
					new JobCriteria {JobId = 1000431, FetchOption = CriteriaBase.FetchOptions.Single, JobStatus = JobStatuses.Active},
				NewExpirationDate = DateTime.Now.Date.AddDays(30),
				NewNumberOfOpenings = 5,
				Module = MockAppSettings.Module
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var jobResponse = _jobService.RefreshJob(request);

			// Assert 
			jobResponse.ShouldNotBeNull();
			jobResponse.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			jobResponse.Job.ShouldNotBeNull();
			jobResponse.Job.JobStatus.ShouldEqual(JobStatuses.Active);
			jobResponse.JobsPaged.ShouldBeNull();
			jobResponse.Jobs.ShouldBeNull();
			jobResponse.Job.ClosingOn.ShouldEqual(DateTime.Now.Date.AddDays(30));
			jobResponse.Job.NumberOfOpenings.ShouldEqual(5);
		}

		[Test]
		public void
			RefreshJob_WhenActiveExtendPriorityForLifeOfPostingJobIsRefreshed_ExtendedVeteranPriorityTrue_JobIsUpdatedVeteranPriorityEndDateIsSameAsClosingDate
			()
		{
			MockAppSettings.ExtendedVeteranPriorityofService = true;
			MockAppSettings.VeteranPriorityServiceEnabled = true;

			var newDate = DateTime.Now.AddDays(20);
			var ts = new TimeSpan(23, 59, 59);
			newDate = newDate.Date + ts;

			// Arrange
			var request = new JobRequest
			{
				PostingSurvey =
					new PostingSurveyDto
					{
						DidHire = true,
						DidInterview = true,
						SatisfactionLevel = 10,
						SurveyType = SurveyTypes.JobRefreshSurvey
					},
				Criteria =
					new JobCriteria {JobId = 1003735, FetchOption = CriteriaBase.FetchOptions.Single, JobStatus = JobStatuses.Active},
				Module = MockAppSettings.Module,
				NewExpirationDate = newDate
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var jobResponse = _jobService.RefreshJob(request);

			// Assert 
			jobResponse.ShouldNotBeNull();
			jobResponse.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			jobResponse.Job.ShouldNotBeNull();
			jobResponse.Job.JobStatus.ShouldEqual(JobStatuses.Active);
			jobResponse.Job.VeteranPriorityEndDate.ShouldEqual(newDate);
			jobResponse.JobsPaged.ShouldBeNull();
			jobResponse.Jobs.ShouldBeNull();

			// Set the appsetting back to the default value
			MockAppSettings.ExtendedVeteranPriorityofService = false;
			MockAppSettings.VeteranPriorityServiceEnabled = false;
		}

		[Test]
		public void
			RefreshJob_WhenActiveExtendPriorityUntilJobIsRefreshed_ExtendedVeteranPriorityTrue_JobIsUpdatedVeteranPriorityEndDateIsNull
			()
		{
			MockAppSettings.ExtendedVeteranPriorityofService = true;
			MockAppSettings.VeteranPriorityServiceEnabled = true;

			var newDate = DateTime.Now.AddDays(20);
			var ts = new TimeSpan(23, 59, 59);
			newDate = newDate.Date + ts;

			// Arrange
			var request = new JobRequest
			{
				PostingSurvey =
					new PostingSurveyDto
					{
						DidHire = true,
						DidInterview = true,
						SatisfactionLevel = 10,
						SurveyType = SurveyTypes.JobRefreshSurvey
					},
				Criteria =
					new JobCriteria {JobId = 1197370, FetchOption = CriteriaBase.FetchOptions.Single, JobStatus = JobStatuses.Active},
				Module = MockAppSettings.Module,
				NewExpirationDate = newDate
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var jobResponse = _jobService.RefreshJob(request);

			// Assert 
			jobResponse.ShouldNotBeNull();
			jobResponse.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			jobResponse.Job.ShouldNotBeNull();
			jobResponse.Job.JobStatus.ShouldEqual(JobStatuses.Active);
			jobResponse.Job.VeteranPriorityEndDate.ShouldBeNull();
			jobResponse.JobsPaged.ShouldBeNull();
			jobResponse.Jobs.ShouldBeNull();

			// Set the appsetting back to the default value
			MockAppSettings.ExtendedVeteranPriorityofService = false;
			MockAppSettings.VeteranPriorityServiceEnabled = false;
		}

		[Test]
		public void ReactivateJob_WhenOnHoldJobIsReactivated_JobIsUpdated()
		{
			// Arrange
			var newExpirationDate = DateTime.Now.AddDays(30);

			var request = new JobRequest
			{
				Criteria =
					new JobCriteria {JobId = 1003689, FetchOption = CriteriaBase.FetchOptions.Single, JobStatus = JobStatuses.OnHold},
				NewExpirationDate = newExpirationDate
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var jobResponse = _jobService.ReactivateJob(request);

			// Assert 
			jobResponse.ShouldNotBeNull();
			jobResponse.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			jobResponse.Job.ShouldNotBeNull();
			jobResponse.Job.JobStatus.ShouldEqual(JobStatuses.Active);
			jobResponse.Job.ClosingOn.ShouldEqual(newExpirationDate);
			jobResponse.JobsPaged.ShouldBeNull();
			jobResponse.Jobs.ShouldBeNull();
		}

		[Test]
		public void ReactivateJob_NotAutomaticallyOnHold_WhenOnHoldJobIsReactivated_JobIsUpdated()
		{
			// Arrange
			var newExpirationDate = DateTime.Now.AddDays( 30 );

			var request = new JobRequest
			{
				Criteria = new JobCriteria { JobId = 1099005, FetchOption = CriteriaBase.FetchOptions.Single, JobStatus = JobStatuses.OnHold },
				NewExpirationDate = newExpirationDate
			};

			var job = DataCoreRepository.FindById<Job>( 1099005 );
			var closingDate = DateTime.Now;
			job.ClosedOn = closingDate.AddDays( -1 );
			job.JobStatus = JobStatuses.Closed;

			var application = DataCoreRepository.FindById<Application>( 1109308 );
			// Store values for later
			var approvalStatusStore = application.ApprovalStatus;
			var previousApprovalStatusStore = application.PreviousApprovalStatus;
			var automaticallyOnHoldStore = application.AutomaticallyOnHold;

			// Set up values for test
			application.ApprovalStatus = ApprovalStatuses.OnHold;
			application.PreviousApprovalStatus = ApprovalStatuses.Reconsider;
			application.AutomaticallyOnHold = false;
			DataCoreRepository.SaveChanges();

			try
			{
				request.Prepare( MockClientTag, MockSessionId, MockUserData, MockCulture );

				// Act
				var jobResponse = _jobService.ReactivateJob( request );

				// Assert 
				jobResponse.ShouldNotBeNull();
				jobResponse.Acknowledgement.ShouldEqual( AcknowledgementType.Success );
				jobResponse.Job.ShouldNotBeNull();
				jobResponse.Job.JobStatus.ShouldEqual( JobStatuses.Active );
				jobResponse.Job.ClosingOn.ShouldEqual( newExpirationDate );
				jobResponse.JobsPaged.ShouldBeNull();
				jobResponse.Jobs.ShouldBeNull();

				application = DataCoreRepository.FindById<Application>( 1109308 );
				application.ApprovalStatus.ShouldEqual( ApprovalStatuses.OnHold );
				application.PreviousApprovalStatus.ShouldEqual( ApprovalStatuses.Reconsider );
			}
			finally
			{
				// Finally put the data back how it was found
				application = DataCoreRepository.FindById<Application>( 1109308 );
				application.ApprovalStatus = approvalStatusStore;
				application.PreviousApprovalStatus = previousApprovalStatusStore;
				application.AutomaticallyOnHold = automaticallyOnHoldStore;
				DataCoreRepository.SaveChanges();
			}
		}

		[Test]
		public void ReactivateJob_AutomaticallyOnHold_WhenOnHoldJobIsReactivated_JobIsUpdated()
		{
			// Arrange
			var newExpirationDate = DateTime.Now.AddDays( 30 );

			var request = new JobRequest
			{
				Criteria = new JobCriteria { JobId = 1099005, FetchOption = CriteriaBase.FetchOptions.Single, JobStatus = JobStatuses.OnHold },
				NewExpirationDate = newExpirationDate
			};

			var job = DataCoreRepository.FindById<Job>( 1099005 );
			var closingDate = DateTime.Now;
			job.ClosedOn = closingDate.AddDays( -1 );
			job.JobStatus = JobStatuses.Closed;

			var application = DataCoreRepository.FindById<Application>( 1109308 );
			// Store values for later
			var approvalStatusStore = application.ApprovalStatus;
			var previousApprovalStatusStore = application.PreviousApprovalStatus;
			var automaticallyOnHoldStore = application.AutomaticallyOnHold;

			// Set up values for test
			application.ApprovalStatus = ApprovalStatuses.OnHold;
			application.PreviousApprovalStatus = ApprovalStatuses.Reconsider;
			application.AutomaticallyOnHold = true;
			DataCoreRepository.SaveChanges();

			try
			{
				request.Prepare( MockClientTag, MockSessionId, MockUserData, MockCulture );

				// Act
				var jobResponse = _jobService.ReactivateJob( request );

				// Assert 
				jobResponse.ShouldNotBeNull();
				jobResponse.Acknowledgement.ShouldEqual( AcknowledgementType.Success );
				jobResponse.Job.ShouldNotBeNull();
				jobResponse.Job.JobStatus.ShouldEqual( JobStatuses.Active );
				jobResponse.Job.ClosingOn.ShouldEqual( newExpirationDate );
				jobResponse.JobsPaged.ShouldBeNull();
				jobResponse.Jobs.ShouldBeNull();

				application = DataCoreRepository.FindById<Application>( 1109308 );
				application.ApprovalStatus.ShouldEqual( ApprovalStatuses.Reconsider );
				application.PreviousApprovalStatus.ShouldEqual( ApprovalStatuses.Reconsider );
			}
			finally
			{
				// Finally put the data back how it was found
				application = DataCoreRepository.FindById<Application>( 1109308 );
				application.ApprovalStatus = approvalStatusStore;
				application.PreviousApprovalStatus = previousApprovalStatusStore;
				application.AutomaticallyOnHold = automaticallyOnHoldStore;
				DataCoreRepository.SaveChanges();
			}
		}

		[Test]
		public void
			ReactivateJob_WhenOnHoldExtendPriorityForLifeOfPostingJobIsReactivated_JobIsUpdatedVeteranPriorityEndDateIsSameAsClosingDate
			()
		{
			MockAppSettings.ExtendedVeteranPriorityofService = true;
			MockAppSettings.VeteranPriorityServiceEnabled = true;

			// Arrange
			var newExpirationDate = DateTime.Now.AddDays(30);
			var ts = new TimeSpan(23, 59, 59);
			newExpirationDate = newExpirationDate.Date + ts;

			var request = new JobRequest
			{
				Criteria =
					new JobCriteria {JobId = 1003689, FetchOption = CriteriaBase.FetchOptions.Single, JobStatus = JobStatuses.OnHold},
				NewExpirationDate = newExpirationDate
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var jobResponse = _jobService.ReactivateJob(request);

			// Assert 
			jobResponse.ShouldNotBeNull();
			jobResponse.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			jobResponse.Job.ShouldNotBeNull();
			jobResponse.Job.JobStatus.ShouldEqual(JobStatuses.Active);
			jobResponse.Job.ClosingOn.ShouldEqual(newExpirationDate);
			jobResponse.Job.VeteranPriorityEndDate.ShouldBeTheSameDateTimeAs(jobResponse.Job.ClosingOn);
			jobResponse.JobsPaged.ShouldBeNull();
			jobResponse.Jobs.ShouldBeNull();

			// Set the appsetting back to the default value
			MockAppSettings.ExtendedVeteranPriorityofService = false;
			MockAppSettings.VeteranPriorityServiceEnabled = false;
		}

		[Test]
		public void ReactivateJob_WhenOnHoldExtendPriorityUntilJobIsReactivated_JobIsUpdatedVeteranPriorityEndDateIsNull()
		{
			MockAppSettings.ExtendedVeteranPriorityofService = true;
			MockAppSettings.VeteranPriorityServiceEnabled = true;

			// Arrange
			var newExpirationDate = DateTime.Now.AddDays(30);
			var ts = new TimeSpan(23, 59, 59);
			newExpirationDate = newExpirationDate.Date + ts;

			var request = new JobRequest
			{
				Criteria =
					new JobCriteria {JobId = 1094161, FetchOption = CriteriaBase.FetchOptions.Single, JobStatus = JobStatuses.OnHold},
				NewExpirationDate = newExpirationDate
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var jobResponse = _jobService.ReactivateJob(request);

			// Assert 
			jobResponse.ShouldNotBeNull();
			jobResponse.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			jobResponse.Job.ShouldNotBeNull();
			jobResponse.Job.JobStatus.ShouldEqual(JobStatuses.Active);
			jobResponse.Job.ClosingOn.ShouldEqual(newExpirationDate);
			jobResponse.Job.VeteranPriorityEndDate.ShouldBeNull();
			jobResponse.JobsPaged.ShouldBeNull();
			jobResponse.Jobs.ShouldBeNull();

			// Set the appsetting back to the default value
			MockAppSettings.ExtendedVeteranPriorityofService = false;
			MockAppSettings.VeteranPriorityServiceEnabled = false;
		}

		[Test]
		public void CloseJob_WhenActiveJobIsClosed_JobIsUpdated()
		{
			// Arrange
			var request = new JobRequest
			{
				PostingSurvey =
					new PostingSurveyDto
					{
						DidHire = true,
						DidInterview = true,
						SatisfactionLevel = 10,
						SurveyType = SurveyTypes.JobCloseSurvey
					},
				Criteria =
					new JobCriteria {JobId = 1003735, FetchOption = CriteriaBase.FetchOptions.Single, JobStatus = JobStatuses.Active},
				Module = MockAppSettings.Module
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var jobResponse = _jobService.CloseJob(request);

			// Assert 
			jobResponse.ShouldNotBeNull();
			jobResponse.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			jobResponse.Job.ShouldNotBeNull();
			jobResponse.Job.JobStatus.ShouldEqual(JobStatuses.Closed);
			jobResponse.JobsPaged.ShouldBeNull();
			jobResponse.Jobs.ShouldBeNull();
		}

		[Test]
		public void CloseJob_WhenActiveJobWithReferralsIsClosed_JobIsUpdated()
		{
			// Arrange
			var request = new JobRequest
			{
				PostingSurvey =
					new PostingSurveyDto
					{
						DidHire = true,
						DidInterview = true,
						SatisfactionLevel = 10,
						SurveyType = SurveyTypes.JobCloseSurvey
					},
				Criteria = new JobCriteria { JobId = 1087360, FetchOption = CriteriaBase.FetchOptions.Single, JobStatus = JobStatuses.Active },
				Module = MockAppSettings.Module
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var jobResponse = _jobService.CloseJob(request);

			// Assert 
			jobResponse.ShouldNotBeNull();
			jobResponse.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			jobResponse.Job.ShouldNotBeNull();
			jobResponse.Job.JobStatus.ShouldEqual(JobStatuses.Closed);
			jobResponse.JobsPaged.ShouldBeNull();
			jobResponse.Jobs.ShouldBeNull();

			var job = DataCoreRepository.FindById<Job>(1087360);
			var applications = job.Posting.Applications;
			applications.Count.ShouldEqual(5); // Should have 1 approved, 2 waiting approval, 1 on hold, 1 reconsider
			applications.Count(x => x.ApprovalStatus.Equals(ApprovalStatuses.Rejected)).ShouldEqual(4); // All except approved should change to rejected
		}

		[Test]
		public void CloseJob_WhenActiveExtendVeteranPriorityForLifeOfPostingJobIsClosed_JobIsUpdatedVeteranPriorityEndDateIsSameAsClosedDate()
		{
			MockAppSettings.ExtendedVeteranPriorityofService = true;
			MockAppSettings.VeteranPriorityServiceEnabled = true;

			// Arrange
			var request = new JobRequest
			{
				PostingSurvey =
					new PostingSurveyDto
					{
						DidHire = true,
						DidInterview = true,
						SatisfactionLevel = 10,
						SurveyType = SurveyTypes.JobCloseSurvey
					},
				Criteria =
					new JobCriteria {JobId = 1003735, FetchOption = CriteriaBase.FetchOptions.Single, JobStatus = JobStatuses.Active},
				Module = MockAppSettings.Module
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var jobResponse = _jobService.CloseJob(request);

			// Assert 
			jobResponse.ShouldNotBeNull();
			jobResponse.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			jobResponse.Job.ShouldNotBeNull();
			jobResponse.Job.JobStatus.ShouldEqual(JobStatuses.Closed);
			jobResponse.Job.VeteranPriorityEndDate.ShouldBeTheSameDateTimeAs(jobResponse.Job.ClosedOn);
			jobResponse.JobsPaged.ShouldBeNull();
			jobResponse.Jobs.ShouldBeNull();

			MockAppSettings.ExtendedVeteranPriorityofService = false;
			MockAppSettings.VeteranPriorityServiceEnabled = false;
		}

		[Test]
		public void CloseJob_WhenActiveExtendVeteranPriorityUntilJobIsClosed_JobIsUpdatedVeteranPriorityEndDateIsNull()
		{
			MockAppSettings.ExtendedVeteranPriorityofService = true;
			MockAppSettings.VeteranPriorityServiceEnabled = true;

			// Arrange
			var request = new JobRequest
			{
				PostingSurvey =
					new PostingSurveyDto
					{
						DidHire = true,
						DidInterview = true,
						SatisfactionLevel = 10,
						SurveyType = SurveyTypes.JobCloseSurvey
					},
				Criteria =
					new JobCriteria {JobId = 1197370, FetchOption = CriteriaBase.FetchOptions.Single, JobStatus = JobStatuses.Active},
				Module = MockAppSettings.Module
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var jobResponse = _jobService.CloseJob(request);

			// Assert 
			jobResponse.ShouldNotBeNull();
			jobResponse.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			jobResponse.Job.ShouldNotBeNull();
			jobResponse.Job.JobStatus.ShouldEqual(JobStatuses.Closed);
			jobResponse.Job.VeteranPriorityEndDate.ShouldBeNull();
			jobResponse.JobsPaged.ShouldBeNull();
			jobResponse.Jobs.ShouldBeNull();

			MockAppSettings.ExtendedVeteranPriorityofService = false;
			MockAppSettings.VeteranPriorityServiceEnabled = false;
		}

		[Test]
		public void DuplicateJob_WhenActiveJobIsRefreshe_JobIsUpdated()
		{
			// Arrange
			var request = new JobRequest
			{
				PostingSurvey =
					new PostingSurveyDto
					{
						DidHire = true,
						DidInterview = true,
						SatisfactionLevel = 10,
						SurveyType = SurveyTypes.JobCloseSurvey
					},
				Criteria =
					new JobCriteria {JobId = 1121522, FetchOption = CriteriaBase.FetchOptions.Single, JobStatus = JobStatuses.Active}
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var jobResponse = _jobService.DuplicateJob(request);

			// Assert 
			jobResponse.ShouldNotBeNull();
			jobResponse.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			jobResponse.Job.ShouldNotBeNull();
			jobResponse.Job.JobStatus.ShouldEqual(JobStatuses.Draft);
			jobResponse.Job.ClosedOn.ShouldBeNull();
			jobResponse.Job.PostedOn.ShouldBeNull();
		}

		[Test]
		public void SaveJob_WhenPassedSaveJobRequestNew_ReturnSuccessJobSaved()
		{
			// Arrange
			var employer = DataCoreRepository.Employers.First(x => x.Name == "DevEmployer");
			var businessUnit = employer.BusinessUnits.First(x => x.IsPrimary);
			var onet = LibraryRepository.Onets.First(x => x.OnetCode == "11-1011.00");

			var request = new SaveJobRequest
			{
				Module = FocusModules.General,
				Job = new JobDto
				{
					JobTitle = "Chief Executives",
					BusinessUnitId = businessUnit.Id,
					EmployerDescriptionPostingPosition = EmployerDescriptionPostingPositions.AboveJobPosting,
					EmployerId = employer.Id,
					OnetId = onet.Id,
					JobStatus = JobStatuses.Draft,
					CreatedBy = MockUserId,
					UpdatedBy = MockUserId,
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var jobResponse = _jobService.SaveJob(request);

			// Assert 
			jobResponse.ShouldNotBeNull();
			jobResponse.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			jobResponse.Job.Id.HasValue.ShouldBeTrue();
			if( jobResponse.Job.Id.HasValue )
				jobResponse.Job.Id.Value.ShouldBeGreater(0);
			jobResponse.Job.CreatedBy.ShouldEqual(MockUserId);
		}

		[Test]
		public void SaveJob_WhenPassedSaveJobRequestExisting_ReturnSuccessJobSaved()
		{
			// Arrange
			var job = DataCoreRepository.Jobs.First(x => x.Id == 1000059);

			var oldTitle = job.JobTitle;
			job.JobTitle = job.JobTitle + "xxx";

			var request = new SaveJobRequest
			{
				Module = FocusModules.General,
				Job = job.AsDto()
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var jobResponse = _jobService.SaveJob(request);

			// Assert 
			jobResponse.ShouldNotBeNull();
			jobResponse.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			jobResponse.Job.JobTitle.ShouldEqual(oldTitle + "xxx");
			jobResponse.Job.CreatedBy.ShouldEqual(1000048);
		}

		[Test]
		public void SaveJob_WhenPassedSaveJobRequestWithoutMandatoryData_ReturnFailure()
		{
			// Arrange
			var employer = DataCoreRepository.Employers.First(x => x.Name == "DevEmployer");
			var businessUnit = employer.BusinessUnits.First(x => x.IsPrimary);
			var onet = LibraryRepository.Onets.First(x => x.OnetCode == "11-1011.00");

			var request = new SaveJobRequest
			{
				Module = FocusModules.General,
				Job = new JobDto
				{
					//JobTitle = "Chief Executives",
					BusinessUnitId = businessUnit.Id,
					EmployerDescriptionPostingPosition = EmployerDescriptionPostingPositions.AboveJobPosting,
					EmployerId = employer.Id,
					OnetId = onet.Id,
					JobStatus = JobStatuses.Draft
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act	
			var jobResponse = _jobService.SaveJob(request);

			// Assert 
			jobResponse.ShouldNotBeNull();
			jobResponse.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}

		[Test]
		public void GetJobAddress_WhenPassedValidRequestWithJobId_ReturnsSuccessWithData()
		{
			// Arrange
			var request = new JobAddressRequest {JobId = 1000059};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _jobService.GetJobAddress(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.JobAddress.Id.ShouldEqual(1000062);
		}

		[Test]
		public void GetJobLocations_WhenPassedValidRequestWithEmployerId_ReturnsSuccessWithData()
		{
			// Arrange
			var request = new JobLocationRequest
			{
				Criteria = new JobLocationCriteria {EmployerId = 13930, ListSize = 3, FetchOption = CriteriaBase.FetchOptions.List}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _jobService.GetJobLocations(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.JobLocations.Count().ShouldBeLessOrEqual(3);
		}

		[Test]
		public void GetJobLocations_WhenPassedValidRequestWithJobId_ReturnsSuccessWithData()
		{
			// Arrange
			var request = new JobLocationRequest
			{
				Criteria = new JobLocationCriteria {JobId = 1000059, FetchOption = CriteriaBase.FetchOptions.List}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _jobService.GetJobLocations(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.JobLocations.Count().ShouldBeLessOrEqual(2);
		}

		[Test]
		public void GetJobLicences_WhenPassedValidRequestWithJobId_ReturnsSuccessWithData()
		{
			// Arrange
			var request = new JobLicenceRequest
			{
				Criteria = new JobLicenceCriteria {JobId = 1000059, FetchOption = CriteriaBase.FetchOptions.List}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _jobService.GetJobLicences(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.JobLicences.Count().ShouldBeGreater(0);
		}

		[Test]
		public void GetJobLanguages_WhenPassedValidRequestWithJobId_ReturnsSuccessWithData()
		{
			// Arrange
			var request = new JobLanguageRequest
			{
				Criteria = new JobLanguageCriteria {JobId = 1000059, FetchOption = CriteriaBase.FetchOptions.List}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _jobService.GetJobLanguages(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.JobLanguages.Count().ShouldBeGreater(0);
		}

		[Test]
		public void GetJobProgramsOfStudy_WhenPassedValidRequestWithJodId_ReturnsSuccessWithData()
		{
			// Arrange
			var request = new JobProgramOfStudyRequest
			{
				Criteria = new JobProgramsOfStudyCriteria {JobId = 1000059, FetchOption = CriteriaBase.FetchOptions.List}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _jobService.GetJobProgramsOfStudy(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.JobProgramsOfStudy.Count().ShouldBeGreater(0);
		}

		[Test]
		public void GetJobCertificates_WhenPassedValidRequestWithJobId_ReturnsSuccessWithData()
		{
			// Arrange
			var request = new JobCertificateRequest
			{
				Criteria = new JobCertificateCriteria {JobId = 1000059, FetchOption = CriteriaBase.FetchOptions.List}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _jobService.GetJobCertificates(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.JobCertificates.Count().ShouldBeGreater(0);
		}

		[Test]
		public void GetJobSpecialRequirements_WhenPassedValidRequestWithJobId_ReturnsSuccessWithData()
		{
			// Arrange
			var request = new JobSpecialRequirementRequest
			{
				Criteria = new JobSpecialRequirementCriteria {JobId = 1000059, FetchOption = CriteriaBase.FetchOptions.List}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _jobService.GetJobSpecialRequirements(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.JobSpecialRequirements.Count().ShouldBeGreater(0);
		}

		[Test]
		public void GetJobSpecialRequirementsCount_WhenPassedValidRequestWithJobId_ReturnsSuccessWithCount()
		{
			// Arrange
			var request = new JobSpecialRequirementRequest
			{
				Criteria = new JobSpecialRequirementCriteria { JobId = 1000059, FetchOption = CriteriaBase.FetchOptions.Count }
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _jobService.GetJobSpecialRequirements(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.JobRequirementsCount.ShouldEqual(1);
		}

		[Test]
		public void SaveJobAddress_WhenPassedValidData_ReturnsSuccessAndSavesData()
		{
			// Arrange
			var request = new SaveJobAddressRequest
			{
				JobAddress = new JobAddressDto
				{
					JobId = 1000059,
					Line1 = "Address1",
					Line2 = "Address2",
					Line3 = "Address3",
					TownCity = "Town",
					PostcodeZip = "90210"
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _jobService.SaveJobAddress(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.JobAddress.JobId.ShouldEqual(1000059);
			response.JobAddress.TownCity.ShouldEqual("Town");
		}

		[Test]
		public void SaveJobLocations_WhenPassedValidRequest_ReturnsSuccessAndSavesData()
		{
			// Arrange
			var request = new SaveJobLocationsRequest
			{
				Criteria = new JobLocationCriteria {JobId = 1000059},
				JobLocations = new List<JobLocationDto>
				{
					new JobLocationDto
					{Location = "XXX", Id = 1000078, IsPublicTransitAccessible = false, JobId = 1000059},
					new JobLocationDto
					{Location = "XXX", IsPublicTransitAccessible = false, JobId = 1000059},
					new JobLocationDto
					{Location = "XXX", IsPublicTransitAccessible = false, JobId = 1000059}
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _jobService.SaveJobLocations(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.JobLocations.Count().ShouldEqual(3);
			response.JobLocations.Count(x => x.Location == "XXX").ShouldEqual(3);
			response.JobLocations.First(x => x.Id == 1000078).Location.ShouldEqual("XXX");
			DataCoreUnitOfWorkScope.Current.JobLocations.Count(x => x.JobId == 1000059).ShouldEqual(3);
		}

		[Test]
		public void SaveJobCertificates_WhenPassedValidRequest_ReturnsSuccessAndData()
		{
			// Arrange
			var request = new SaveJobCertificatesRequest
			{
				Criteria = new JobCertificateCriteria {JobId = 1000059},
				JobCertificates = new List<JobCertificateDto>
				{
					new JobCertificateDto {JobId = 1000059, Certificate = "Certificate1"},
					new JobCertificateDto {JobId = 1000059, Certificate = "Certificate2"},
					new JobCertificateDto {JobId = 1000059, Certificate = "Certificate3"},
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _jobService.SaveJobCertificates(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.JobCertificates.Count().ShouldEqual(3);
			response.JobCertificates.Count(x => x.JobId == 1000059).ShouldEqual(3);
		}

		[Test]
		public void SaveJobProgramsOfStudy_WhenPassedValidRequest_ReturnsSuccessAndSavesData()
		{
			// Arrange
			var request = new SaveJobProgramsOfStudyRequest
			{
				Criteria = new JobProgramsOfStudyCriteria {JobId = 1000059},
				JobProgramsOfStudy = new List<JobProgramOfStudyDto>
				{
					new JobProgramOfStudyDto
					{JobId = 1000059, ProgramOfStudy = "Degree1", DegreeEducationLevelId = 1001},
					new JobProgramOfStudyDto
					{JobId = 1000059, ProgramOfStudy = "Degree2", DegreeEducationLevelId = 1002},
					new JobProgramOfStudyDto
					{JobId = 1000059, ProgramOfStudy = "Degree3", DegreeEducationLevelId = 1003},
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _jobService.SaveJobProgramsOfStudy(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.JobProgramsOfStudy.Count().ShouldEqual(3);
			response.JobProgramsOfStudy.Count(x => x.JobId == 1000059).ShouldEqual(3);
		}


		[Test]
		public void SaveJobLanguages_WhenPassedValidRequest_ReturnsSuccessAndSavesData()
		{
			// Arrange
			var request = new SaveJobLanguagesRequest
			{
				Criteria = new JobLanguageCriteria {JobId = 1000059},
				JobLanguages = new List<JobLanguageDto>
				{
					new JobLanguageDto
					{JobId = 1000059, Language = "French"},
					new JobLanguageDto
					{JobId = 1000059, Language = "English"},
					new JobLanguageDto
					{JobId = 1000059, Language = "German"},
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _jobService.SaveJobLanguages(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.JobLanguages.Count().ShouldEqual(3);
			response.JobLanguages.Count(x => x.JobId == 1000059).ShouldEqual(3);
		}

		[Test]
		public void SaveJobSpecialRequirements_WhenPassedValidRequest_ReturnsSuccessAndSavesData()
		{
			// Arrange
			var request = new SaveJobSpecialRequirementsRequest
			{
				Criteria = new JobSpecialRequirementCriteria {JobId = 1000059},
				JobSpecialRequirements = new List<JobSpecialRequirementDto>
				{
					new JobSpecialRequirementDto
					{Requirement = "XXX", Id = 286045, JobId = 1000059},
					new JobSpecialRequirementDto
					{Requirement = "XXX", Id = 286046, JobId = 1000059},
					new JobSpecialRequirementDto
					{Requirement = "XXX", Id = 286047, JobId = 1000059}
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _jobService.SaveJobSpecialRequirements(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.JobSpecialRequirements.Count().ShouldEqual(3);
			response.JobSpecialRequirements.Count(x => x.Requirement == "XXX").ShouldEqual(3);
			response.JobSpecialRequirements.First(x => x.Id == 286045).Requirement.ShouldEqual("XXX");
		}

		[Test]
		public void SaveJobLicences_WhenPassedValidRequest_ReturnsSuccessAndSavesData()
		{
			var request = new SaveJobLicencesRequest
			{
				Criteria = new JobLicenceCriteria {JobId = 1000059},
				JobLicences = new List<JobLicenceDto>
				{
					new JobLicenceDto
					{JobId = 1000059, Id = 286045, Licence = "XXX"},
					new JobLicenceDto
					{JobId = 1000059, Id = 286046, Licence = "XXX"},
					new JobLicenceDto
					{JobId = 1000059, Id = 286047, Licence = "XXX"},
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _jobService.SaveJobLicences(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.JobLicences.Count().ShouldEqual(3);
			response.JobLicences.Count(x => x.JobId == 1000059).ShouldEqual(3);
		}

		[Test]
		public void GetEmployerJobDescription_WhenPassedValidEmployerId_ReturnsSuccessWithList()
		{
			// Arrange
			var request = new JobDescriptionRequest {Criteria = new JobCriteria {JobId = 1000059}};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _jobService.GetJobDescription(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.JobDescription.ShouldEqual("Description for: Test Job 1");
		}

		[Test]
		public void PostJob_WhenPassedJobWithRedWords_ReturnsFailureAndRedWords()
		{
			// Arrange
			var job = DataCoreRepository.Jobs.First(x => x.Id == 1000059);

			job.PostingHtml = job.PostingHtml + " crap";
			var request = new PostJobRequest {Job = job.AsDto(), Module = MockAppSettings.Module};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _jobService.PostJob(request);

			// Assert
			response.ShouldNotBeNull();
			response.Job.RedProfanityWords.Length.ShouldBeGreater(0);
			response.Job.ApprovalStatus.ShouldEqual(ApprovalStatuses.OnHold);
		}

		[Test]
		public void PostJob_WhenPassedJobWithYellowWords_ReturnsSuccessAndYellowWords()
		{
			// Arrange
			var job = DataCoreRepository.Jobs.First(x => x.Id == 1000059);

			job.PostingHtml = job.PostingHtml + " escort";

			var request = new PostJobRequest {Job = job.AsDto(), Module = MockAppSettings.Module};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _jobService.PostJob(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Job.YellowProfanityWords.Length.ShouldBeGreater(0);
			response.Job.ApprovalStatus.ShouldEqual(ApprovalStatuses.WaitingApproval);
			response.Job.JobStatus.ShouldEqual(JobStatuses.Draft);
		}

		[Test]
		public void PostJob_WhenPassedJobWithGreenWords_ReturnsSuccessAndGreenWords()
		{
			// Arrange
			var job = DataCoreRepository.Jobs.FirstOrDefault(x => x.Id == 1003791);

			var request = new PostJobRequest {Job = job.AsDto(), Module = MockAppSettings.Module};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _jobService.PostJob(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Job.ApprovalStatus.ShouldEqual(ApprovalStatuses.Approved);
		}

		[Test]
		public void PostJob_WhenPassedJobWithZipAssignedToOffice_ReturnsSuccess()
		{
			// Arrange
			var job = DataCoreRepository.Jobs.FirstOrDefault(x => x.Id == 1094072);

			var request = new PostJobRequest {Job = job.AsDto(), Module = MockAppSettings.Module};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _jobService.PostJob(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			var jobOfficeMappers = DataCoreRepository.JobOfficeMappers.Where(x => x.JobId == response.Job.Id).ToList();
			jobOfficeMappers.ShouldNotBeNull();
			jobOfficeMappers.Count.ShouldEqual(1);
			jobOfficeMappers[0].OfficeId.ShouldEqual(5);
			jobOfficeMappers[0].OfficeUnassigned.ShouldBeNull();
		}

		[Test]
		public void PostJob_WhenPassedJobWithZipAssignedToMultiOffice_ReturnsSuccess()
		{
			// Arrange
			var job = DataCoreRepository.Jobs.FirstOrDefault(x => x.Id == 1191122);

			var request = new PostJobRequest {Job = job.AsDto(), Module = MockAppSettings.Module};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _jobService.PostJob(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			var jobOfficeMappers = DataCoreRepository.JobOfficeMappers.Where(x => x.JobId == response.Job.Id).ToList();
			jobOfficeMappers.ShouldNotBeNull();
			jobOfficeMappers.Count.ShouldEqual(2);
		}

		[Test]
		public void PostJob_WhenPassedJobWithZipNotAssignedToOffice_ReturnsSuccess()
		{
			// Arrange
			var job = DataCoreRepository.Jobs.FirstOrDefault(x => x.Id == 1093092);

			var request = new PostJobRequest {Job = job.AsDto(), Module = MockAppSettings.Module};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _jobService.PostJob(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			var jobOfficeMappers = DataCoreRepository.JobOfficeMappers.Where(x => x.JobId == response.Job.Id).ToList();
			jobOfficeMappers.ShouldNotBeNull();
			jobOfficeMappers.Count.ShouldEqual(1);
			jobOfficeMappers[0].OfficeId.ShouldEqual(4);
			jobOfficeMappers[0].OfficeUnassigned.ShouldEqual(true);
		}

		[Test]
		public void PostJob_WhenPassedJobWithAssignedToId_ReturnsSuccessAndAssignedToId()
		{
			// Arrange
			var job = DataCoreRepository.Jobs.First(x => x.Id == 1191122);
			job.AssignedToId = 1;

			var request = new PostJobRequest {Job = job.AsDto(), Module = MockAppSettings.Module};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _jobService.PostJob(request);

			// Assert
			response.ShouldNotBeNull();
			response.Job.AssignedToId.ShouldNotBeNull();
			response.Job.AssignedToId.ShouldEqual(1);
		}

		[Test]
		public void PostJob_WhenPassedJobWithNoFixedLocationLocationType_ReturnsSuccessAndWaitingApproval()
		{
			// Arrange
			var job = DataCoreRepository.Jobs.FirstOrDefault(x => x.Id == 1179835);

			var request = new PostJobRequest {Job = job.AsDto(), Module = MockAppSettings.Module};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _jobService.PostJob(request);

			// Assert
			response.ShouldNotBeNull();
			response.Job.ApprovalStatus.ShouldNotBeNull();
			response.Job.ApprovalStatus.ShouldEqual(ApprovalStatuses.WaitingApproval);
		}

		[Test]
		public void PostJob_WhenPassedJobWithCreditCheckRequired_ReturnsSuccessAndJobIsActive()
		{
			MockAppSettings.ShowCreditCheck = true;

			// Arrange
			var job = DataCoreRepository.Jobs.First(x => x.Id == 1003791);
			job.CreditCheckRequired = true;

			var request = new PostJobRequest {Job = job.AsDto(), Module = MockAppSettings.Module};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _jobService.PostJob(request);

			// Assert
			response.ShouldNotBeNull();
			response.Job.ApprovalStatus.ShouldNotBeNull();
			response.Job.ApprovalStatus.ShouldEqual(ApprovalStatuses.Approved);

			MockAppSettings.ShowCreditCheck = false;
		}

		[Test]
		public void GetJobTasks_WhenPassedValidJobTaskRequest_ReturnsSuccessWithData()
		{
			// Arrange
			var job = DataCoreRepository.Jobs.First(x => x.Id == 1000059);

			var request = new JobTaskRequest {Criteria = new JobTaskCriteria {OnetId = job.OnetId}};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act 
			var response = _jobService.GetJobTasks(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.JobTasks.Count.ShouldBeGreater(0);
			response.JobTasks[0].MultiOptionPrompts.Count.ShouldBeGreater(0);
		}

		[Test]
		public void GetJobActivity_WhenPassedValidRequest_ReturnCorrectNumberOfRecordsAccordingToPageSize()
		{
			DeleteJobApplicationActionEvents();

			AddJobApplicationActionEvents();

			var criteria = new JobActivityCriteria
			{
				DaysBack = 30,
				JobId = 1000059,
				PageSize = 2,
				OrderBy = string.Empty,
				FetchOption = CriteriaBase.FetchOptions.PagedList
			};
			// Act
			var request = new JobActivitiesRequest {Criteria = criteria}.Prepare(MockClientTag, MockSessionId, MockUserData,
				MockCulture);
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Arrange
			var response = _jobService.GetJobActivities(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.JobActionEventViewPaged.Count.ShouldEqual(2);
		}

		[Test]
		public void GetJobActivity_WhenPassedValidRequest_ReturnCorrectNumberOfRecordsAccordingToDateRange()
		{
			DeleteJobApplicationActionEvents();

			AddJobApplicationActionEvents();

			var criteria = new JobActivityCriteria
			{
				DaysBack = 30,
				JobId = 1000059,
				PageSize = 30,
				OrderBy = string.Empty,
				FetchOption = CriteriaBase.FetchOptions.PagedList
			};

			var request = new JobActivitiesRequest {Criteria = criteria}.Prepare(MockClientTag, MockSessionId, MockUserData,
				MockCulture);
			var response = _jobService.GetJobActivities(request);

			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.JobActionEventViewPaged.Count.ShouldEqual(3);
			response.JobActionEventViewPaged.TotalPages.ShouldEqual(1);
		}

		[Test]
		public void GetJobActivity_WhenPassedValidRequest_ReturnsEventsOrderedByDateAsc()
		{
			DeleteJobApplicationActionEvents();

			var activityItems = AddJobApplicationActionEvents();

			var criteria = new JobActivityCriteria
			{
				DaysBack = 30,
				JobId = 1000059,
				PageSize = 30,
				OrderBy = Constants.SortOrders.ActivityDateAsc,
				FetchOption = CriteriaBase.FetchOptions.PagedList
			};
			var request = new JobActivitiesRequest {Criteria = criteria}.Prepare(MockClientTag, MockSessionId, MockUserData,
				MockCulture);
			var response = _jobService.GetJobActivities(request);

			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.JobActionEventViewPaged.Count.ShouldEqual(3);
			response.JobActionEventViewPaged[0].Id.ShouldEqual(activityItems[0].Id);
			response.JobActionEventViewPaged[2].Id.ShouldEqual(activityItems[2].Id);
		}

		[Test]
		public void GetJobActivity_WhenPassedValidRequest_ReturnsEventsOrderedByDateDesc()
		{
			DeleteJobApplicationActionEvents();

			var activityItems = AddJobApplicationActionEvents();

			var criteria = new JobActivityCriteria
			{
				DaysBack = 30,
				JobId = 1000059,
				PageSize = 30,
				OrderBy = Constants.SortOrders.ActivityDateDesc,
				FetchOption = CriteriaBase.FetchOptions.PagedList
			};
			var request = new JobActivitiesRequest {Criteria = criteria}.Prepare(MockClientTag, MockSessionId, MockUserData,
				MockCulture);
			var response = _jobService.GetJobActivities(request);

			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.JobActionEventViewPaged.Count.ShouldEqual(3);
			response.JobActionEventViewPaged[0].Id.ShouldEqual(activityItems[2].Id);
			response.JobActionEventViewPaged[2].Id.ShouldEqual(activityItems[0].Id);
		}

		[Test]
		public void GetJobLensPostingId_WhenPassedValidJob_ReturnsUrl()
		{
			// Arrange
			var request = new JobLensPostingIdRequest {JobId = 1092282};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act 
			var response = _jobService.GetJobLensPostingId(request);

			// Assert
			response.ShouldNotBeNull();
			response.LensPostingId.ShouldEqual( "ABC123_B464BE92A2114926A2F26BE93E935ADD" );
		}

		[Test]
		public void GetJobLensPostingIdByExternalId_WhenPassedValidJob_ReturnsUrl()
		{
			// Arrange
			var request = new JobIdsRequest { CheckId = "1", JobStatuses = new List<JobStatuses> { JobStatuses.Active }};
			request.Prepare( MockClientTag, MockSessionId, MockUserData, MockCulture );

			// Act 
			var response = _jobService.GetJobLensPostingId( request );

			// Assert
			response.ShouldNotBeNull();
			response.LensPostingId.ShouldEqual( "ABC123_01AF0331849E46BD9934CB2A05F1F6B5" );


			// Arrange
			request = new JobIdsRequest { CheckId = "1092282", JobStatuses = new List<JobStatuses> { JobStatuses.Active }};
			request.Prepare( MockClientTag, MockSessionId, MockUserData, MockCulture );

			// Act 
			response = _jobService.GetJobLensPostingId( request );

			// Assert
			response.ShouldNotBeNull();
			response.LensPostingId.ShouldEqual( "ABC123_B464BE92A2114926A2F26BE93E935ADD" );
		}

		[Test]
		public void UpdateJobAssignedOffice_WhenPassedValidRequest_ReturnsSuccessAndNewOfficeId()
		{
			// Arrange
			var jomToSave = new List<JobOfficeMapperDto>
			{
				new JobOfficeMapperDto
				{
					OfficeId = 4,
					JobId = 1003735
				}
			};

			var request = new UpdateJobAssignedOfficeRequest {JobOffices = jomToSave, JobId = 1003735};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act	
			var jobResponse = _jobService.UpdateJobAssignedOffice(request);

			// Assert 
			jobResponse.ShouldNotBeNull();
			jobResponse.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void UpdateJobAssignedOffice_WhenPassedUnassignedJob_ReturnsSuccess()
		{
			// Arrange
			var jomToSave = new List<JobOfficeMapperDto>
			{
				new JobOfficeMapperDto
				{
					OfficeId = 4,
					JobId = 1197289
				}
			};

			var request = new UpdateJobAssignedOfficeRequest {JobOffices = jomToSave, JobId = 1197289};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act	
			var jobResponse = _jobService.UpdateJobAssignedOffice(request);

			// Assert 
			jobResponse.ShouldNotBeNull();
			jobResponse.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void UpdateJobAssignedStaffMember_WhenPassedSingleJob_ReturnsSuccess()
		{
			// Arrange
			var request = new UpdateJobAssignedStaffMemberRequest {JobIds = new List<long> {1180944}, PersonId = 1};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act	
			var jobResponse = _jobService.UpdateJobAssignedStaffMember(request);

			// Assert 
			jobResponse.ShouldNotBeNull();
			jobResponse.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			var job = DataCoreRepository.Jobs.First(x => x.Id == 1180944);
			job.ShouldNotBeNull();
			job.AssignedToId.ShouldNotBeNull();
			job.AssignedToId.ShouldEqual(1);
		}

		[Test]
		public void UpdateJobAssignedStaffMember_WhenPassedMultipleJobs_ReturnsSuccess()
		{
			// Arrange
			var request = new UpdateJobAssignedStaffMemberRequest {JobIds = new List<long> {1180944, 1093800}, PersonId = 1};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act	
			var jobResponse = _jobService.UpdateJobAssignedStaffMember(request);

			// Assert 
			jobResponse.ShouldNotBeNull();
			jobResponse.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			var job = DataCoreRepository.Jobs.First(x => x.Id == 1180944);
			job.ShouldNotBeNull();
			job.AssignedToId.ShouldNotBeNull();
			job.AssignedToId.ShouldEqual(1);

			var job2 = DataCoreRepository.Jobs.First(x => x.Id == 1093800);
			job2.ShouldNotBeNull();
			job2.AssignedToId.ShouldNotBeNull();
			job2.AssignedToId.ShouldEqual(1);
		}

		[Test]
		public void UpdateJobAssignedStaffMember_WhenPassedInvalidSingleJob_ReturnsFailure()
		{
			// Arrange
			var request = new UpdateJobAssignedStaffMemberRequest {JobIds = new List<long> {-1}, PersonId = 1};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act	
			var jobResponse = _jobService.UpdateJobAssignedStaffMember(request);

			// Assert 
			jobResponse.ShouldNotBeNull();
			jobResponse.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);

			var job = DataCoreRepository.Jobs.FirstOrDefault(x => x.Id == -1);
			job.ShouldBeNull();
		}

		[Test]
		public void UpdateJobAssignedStaffMember_WhenPassedInvalidJobs_ReturnsFailure()
		{
			// Arrange
			var request = new UpdateJobAssignedStaffMemberRequest {JobIds = new List<long> {-1, 1180944, -3}, PersonId = 1};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act	
			var jobResponse = _jobService.UpdateJobAssignedStaffMember(request);

			// Assert 
			jobResponse.ShouldNotBeNull();
			jobResponse.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);

			var job = DataCoreRepository.Jobs.FirstOrDefault(x => x.Id == -1);
			job.ShouldBeNull();

			var job2 = DataCoreRepository.Jobs.First(x => x.Id == 1180944);
			job2.ShouldNotBeNull();
			job2.AssignedToId.ShouldEqual(1093026);

			var job3 = DataCoreRepository.Jobs.FirstOrDefault(x => x.Id == -3);
			job3.ShouldBeNull();
		}

		[Test]
		public void UpdateJobAssignedStaffMember_WhenPassedInvalidStaffMember_ReturnsSuccess()
		{
			// Arrange
			var request = new UpdateJobAssignedStaffMemberRequest {JobIds = new List<long> {1180944}, PersonId = -1};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act	
			var jobResponse = _jobService.UpdateJobAssignedStaffMember(request);

			// Assert 
			jobResponse.ShouldNotBeNull();
			jobResponse.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);

			var job = DataCoreRepository.Jobs.First(x => x.Id == 1180944);
			job.ShouldNotBeNull();
			job.AssignedToId.ShouldEqual(1093026);
		}

		[Test]
		public void UpdateCriminalBackgroundExclusionRequired_WhenPassedValidJobId_ReturnsSuccess()
		{
			// Arrange
			var request = new UpdateCriminalBackgroundExclusionRequiredRequest
			{
				JobId = 1180944,
				CriminalBackgroundExclusionRequired = true
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act	
			var jobResponse = _jobService.UpdateCriminalBackgroundExclusionRequired(request);

			// Assert 
			jobResponse.ShouldNotBeNull();
			jobResponse.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			jobResponse.Job.ShouldNotBeNull();

			var job = DataCoreRepository.Jobs.First(x => x.Id == 1180944);
			job.ShouldNotBeNull();
			job.CriminalBackgroundExclusionRequired.ShouldEqual(true);
		}

		[Test]
		public void UpdateCriminalBackgroundExclusionRequired_WhenPassedInValidJobId_ReturnsFailure()
		{
			// Arrange
			var request = new UpdateCriminalBackgroundExclusionRequiredRequest
			{
				JobId = 0,
				CriminalBackgroundExclusionRequired = true
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act	
			var jobResponse = _jobService.UpdateCriminalBackgroundExclusionRequired(request);

			// Assert 
			jobResponse.ShouldNotBeNull();
			jobResponse.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);

			jobResponse.Job.ShouldBeNull();
		}

		[Test]
		public void UpdateCriminalBackgroundExclusionRequired_WhenNotPassedJobId_ReturnsFailure()
		{
			// Arrange
			var request = new UpdateCriminalBackgroundExclusionRequiredRequest
			{
				CriminalBackgroundExclusionRequired = true
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act	
			var jobResponse = _jobService.UpdateCriminalBackgroundExclusionRequired(request);

			// Assert 
			jobResponse.ShouldNotBeNull();
			jobResponse.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);

			jobResponse.Job.ShouldBeNull();
		}

		[Test]
		public void FlagJobForFollowUp_WhenPassedValidJobId_ReturnsSuccess()
		{
			// Arrange
			var request = new JobRequest {Criteria = new JobCriteria {JobId = 1092043}};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var jobResponse = _jobService.FlagJobForFollowUp(request);

			// Assert
			jobResponse.ShouldNotBeNull();
			jobResponse.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			var job = DataCoreRepository.Jobs.First(x => x.Id == 1092043);
			job.JobIssue.FollowUpRequested.ShouldEqual(true);
		}

		[Test]
		public void FlagJobForFollowUp_WhenPassedInValidJobId_ReturnsSuccess()
		{
			// Arrange
			var request = new JobRequest {Criteria = new JobCriteria {JobId = -1}};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var jobResponse = _jobService.FlagJobForFollowUp(request);

			// Assert
			jobResponse.ShouldNotBeNull();
			jobResponse.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);

		}

		[Test]
		public void GetJobIssues_WhenPassedValidJobId_ReturnsSuccess()
		{
			// Arrange
			var request = new JobIssuesRequest {JobId = 1133104};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act	
			var response = _jobService.GetJobIssues(request);

			// Assert 
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			response.JobIssues.JobIssues.ShouldNotBeNull();
			response.JobIssues.PendingPostHireFollowUps.Count.ShouldBeGreaterOrEqual(0);
		}

		[Test]
		public void ResolveJobIssues_WhenPassedValidJobId_ReturnsSuccess()
		{
			// Arrange
			var request = new ResolveJobIssuesRequest
			{
				JobId = 1133104,
				IssuesResolved =
					new List<JobIssuesFilter>
					{JobIssuesFilter.NegativeSurveyResponse, JobIssuesFilter.SearchNotInviting},
				PostHireFollowUpsResolved = new List<long> {1, 2}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act	
			var response = _jobService.ResolveJobIssues(request);

			// Assert 
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

		}

		[Test]
		public void GetJobDrivingLicenceEndorsements_WhenPassedValidRequestWithJobId_ReturnsSuccessWithData()
		{
			// Arrange
			var request = new JobDrivingLicenceEndorsementRequest
			{
				Criteria = new JobDrivingLicenceEndorsementCriteria {JobId = 1003735, FetchOption = CriteriaBase.FetchOptions.List}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _jobService.GetJobDrivingLicenceEndorsements(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.JobDrivingLicenceEndorsements.Count().ShouldBeGreater(0);
		}

		[Test]
		public void SaveJobDrivingLicenceEndorsements_WhenPassedValidRequest_ReturnsSuccessAndSavesData()
		{
			// Arrange
			var request = new SaveJobDrivingLicenceEndorsementsRequest
			{
				Criteria = new JobDrivingLicenceEndorsementCriteria {JobId = 1000059},
				JobDrivingLicenceEndorsements = new List<JobDrivingLicenceEndorsementDto>
				{
					new JobDrivingLicenceEndorsementDto {DrivingLicenceEndorsementId = 1, JobId = 1000059},
					new JobDrivingLicenceEndorsementDto {DrivingLicenceEndorsementId = 2, JobId = 1000059},
					new JobDrivingLicenceEndorsementDto {DrivingLicenceEndorsementId = 3, JobId = 1000059}
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _jobService.SaveJobDrivingLicenceEndorsements(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.JobDrivingLicenceEndorsements.Count().ShouldEqual(3);
		}

		[Test]
		public void InviteJobseekersToApply_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new InviteJobseekersToApplyRequest
			{
				PersonIdsWithScores = new Dictionary<long, int>() { {1, 350}, {2, 562}, {3, 746} },
				JobId = 4,
				JobLink = "www.google.com",
				LensPostingId = "ABCD_1234",
				SenderEmail = ""
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _jobService.InviteJobseekersToApply(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void UpdateCreditCheckRequired_WhenPassedValidJobId_ReturnsSuccess()
		{
			// Arrange
			var request = new UpdateCreditCheckRequiredRequest {JobId = 1180944, CreditCheckRequired = true};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act	
			var jobResponse = _jobService.UpdateCreditCheckRequired(request);

			// Assert 
			jobResponse.ShouldNotBeNull();
			jobResponse.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			jobResponse.Job.ShouldNotBeNull();

			var job = DataCoreRepository.Jobs.First(x => x.Id == 1180944);
			job.ShouldNotBeNull();
			job.CreditCheckRequired.ShouldEqual(true);
		}

		[Test]
		public void UpdateCreditCheckRequired_WhenPassedInValidJobId_ReturnsSuccess()
		{
			// Arrange
			var request = new UpdateCreditCheckRequiredRequest {JobId = 0, CreditCheckRequired = true};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act	
			var jobResponse = _jobService.UpdateCreditCheckRequired(request);

			// Assert 
			jobResponse.ShouldNotBeNull();
			jobResponse.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);

			jobResponse.Job.ShouldBeNull();
		}

		[Test]
		public void GetCriminalBackgroundExclusionRequired_WhenPassedValidJobId_ReturnsSuccess()
		{
			// Arrange
			var request = new CriminalBackgroundExclusionRequiredRequest { JobId = 1197370 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act	
			var jobResponse = _jobService.GetCriminalBackgroundExclusionRequired(request);

			// Assert 
			jobResponse.ShouldNotBeNull();
			jobResponse.Acknowledgement.ShouldEqual(AcknowledgementType.Success);


			var job = DataCoreRepository.Jobs.FirstOrDefault(x => x.Id == 1197370);
			job.ShouldNotBeNull();
			job.CriminalBackgroundExclusionRequired.ShouldEqual(true);
		}

		[Test]
		public void GetCriminalBackgroundExclusionRequired_WhenPassedInValidJobId_ReturnsSuccess()
		{
			// Arrange
			var request = new CriminalBackgroundExclusionRequiredRequest { JobId = 10321642 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act	
			var jobResponse = _jobService.GetCriminalBackgroundExclusionRequired(request);

			// Assert 
			jobResponse.ShouldNotBeNull();
			jobResponse.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}

		[Test]
		public void GetCriminalBackgroundExclusionRequired_WhenNotPassedJobId_ReturnsSuccess()
		{
			// Arrange
			var request = new CriminalBackgroundExclusionRequiredRequest();
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act	
			var jobResponse = _jobService.GetCriminalBackgroundExclusionRequired(request);

			// Assert 
			jobResponse.ShouldNotBeNull();
			jobResponse.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}

		[Test]
		public void GetHiringManagerPersonNameEmailForJob_WhenPassedJobId_ReturnsSuccess()
		{
			// Arrange
			var request = new HiringManagerPersonNameEmailForJobRequest()
			{
				JobId = 1003735
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var queryResponse = _jobService.GetHiringManagerPersonNameEmailForJob(request);

			// Assert 
			queryResponse.Person.ShouldNotBeNull();
		}

		[Test]
		public void GetHiringManagerPersonNameEmailForJob_WhenPassedInvalidJobId_ReturnsSuccess()
		{
			// Arrange
			var request = new HiringManagerPersonNameEmailForJobRequest()
			{
				JobId = 0
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var queryResponse = _jobService.GetHiringManagerPersonNameEmailForJob(request);

			// Assert 
			queryResponse.Person.ShouldBeNull();
		}

		[Test]
		public void GetHiringManagerPersonNameEmailForJob_WhenPassedNoJobId_ReturnsSuccess()
		{
			// Arrange
			var request = new HiringManagerPersonNameEmailForJobRequest();
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var queryResponse = _jobService.GetHiringManagerPersonNameEmailForJob(request);

			// Assert 
			queryResponse.Person.ShouldBeNull();
		}

		#region DenyReferral Tests

		[Test]
		public void DenyReferral_WhenPassedInValidRequest_ReturnsFailure()
		{
			// Arrange
			var request = new JobRequest { Criteria = new JobCriteria { JobId = 1101140 } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _jobService.DenyReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}

		[Test]
		public void DenyReferral_JobWhenPassedMultipleDenialReasonsWithOtherReason_ReturnsSuccess()
		{
			// Arrange
			var request = new JobRequest
			{
				Criteria = new JobCriteria { JobId = 1101140 },
				ReferralStatusReasons = new List<KeyValuePair<long, string>>
				{
					new KeyValuePair<long, string>(5531250, null),
					new KeyValuePair<long, string>(5531251, null),
					new KeyValuePair<long, string>(5531252, null),
					new KeyValuePair<long, string>(5531254, "other text")
				}
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _jobService.DenyReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			var job = DataCoreRepository.FindById<Job>(1101140);

			job.ShouldNotBeNull();

			if (job != null)
			{
				var savedReason = job.JobReferralStatusReason.ToList();

				savedReason.Count.ShouldEqual(4);
				savedReason.Count(x => x.JobId.Equals(job.Id)).ShouldEqual(4);

				savedReason.Count(x => x.ReasonId.Equals(5531250)).ShouldEqual(1);
				savedReason.Count(x => x.ReasonId.Equals(5531251)).ShouldEqual(1);
				savedReason.Count(x => x.ReasonId.Equals(5531252)).ShouldEqual(1);
				savedReason.Count(x => x.OtherReasonText == null).ShouldEqual(3);
				savedReason.Count(x => x.OtherReasonText == "other text" && x.ReasonId.Equals(5531254)).ShouldEqual(1);
				savedReason.Count(x => x.EntityType.Equals(EntityTypes.Job)).ShouldEqual(4);
				savedReason.Count(x => x.ApprovalStatus.Equals(ApprovalStatuses.Rejected)).ShouldEqual(4);
			}
		}

		[Test]
		public void DenyReferral_JobWhenPassedOtherReason_ReturnsSuccess()
		{
			// Arrange
			var request = new JobRequest
			{
				Criteria = new JobCriteria { JobId = 1101140 },
				ReferralStatusReasons = new List<KeyValuePair<long, string>>
				{
					new KeyValuePair<long, string>(5531254, "other text")
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _jobService.DenyReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			var job = DataCoreRepository.FindById<Job>(1101140);

			job.ShouldNotBeNull();

			if (job != null)
			{
				var savedReason = job.JobReferralStatusReason.ToList();

				savedReason.Count.ShouldEqual(1);
				savedReason[0].JobId.ShouldEqual(job.Id);
				savedReason[0].ReasonId.ShouldEqual(5531254);
				savedReason[0].OtherReasonText.ShouldEqual("other text");
				savedReason[0].EntityType.ShouldEqual(EntityTypes.Job);
				savedReason[0].ApprovalStatus.ShouldEqual(ApprovalStatuses.Rejected);
			}
		}


		[Test]
		public void DenyReferral_JobWhenPassedMultipleDenialReasonsWithInvalidReasonIdAndTextCombo_ReturnsFailure()
		{
			// Arrange
			var request = new JobRequest
			{
				Criteria = new JobCriteria { JobId = 1101140 },
				ReferralStatusReasons = new List<KeyValuePair<long, string>>
				{
					new KeyValuePair<long, string>(5531250, null),
					new KeyValuePair<long, string>(5531251, null),
					new KeyValuePair<long, string>(5531252, "Text is not valid with this reason ID")
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _jobService.DenyReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);

			var job = DataCoreRepository.FindById<Job>(1101140);

			job.ShouldNotBeNull();

			if (job != null)
			{
				var savedReason = job.JobReferralStatusReason.ToList();

				savedReason.Count().ShouldEqual(0);
			}
		}

		[Test]
		public void DenyReferral_JobWhenPassedMultipleDenialReasonsWithNoOtherTextForOtherReasonId_ReturnsFailure()
		{
			// Arrange
			var request = new JobRequest
			{
				Criteria = new JobCriteria { JobId = 1101140 },
				ReferralStatusReasons = new List<KeyValuePair<long, string>>
				{
					new KeyValuePair<long, string>(5531250, null),
					new KeyValuePair<long, string>(5531251, null),
					new KeyValuePair<long, string>(5531254, "")
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _jobService.DenyReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);

			var job = DataCoreRepository.FindById<Job>(1101140);

			job.ShouldNotBeNull();

			if (job != null)
			{
				var savedReason = job.JobReferralStatusReason.ToList();

				savedReason.Count().ShouldEqual(0);
			}
		}

		[Test]
		public void DenyReferral_JobtWhenPassedMultipleDenialReasons_ReturnsSuccess()
		{
			// Arrange
			var request = new JobRequest
			{
				Criteria = new JobCriteria { JobId = 1101140 },
				ReferralStatusReasons = new List<KeyValuePair<long, string>>
				{
					new KeyValuePair<long, string>(5531250, null),
					new KeyValuePair<long, string>(5531251, null),
					new KeyValuePair<long, string>(5531252, null)
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _jobService.DenyReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			var job = DataCoreRepository.FindById<Job>(1101140);

			job.ShouldNotBeNull();

			if (job != null)
			{
				var savedReason = job.JobReferralStatusReason.ToList();

				savedReason.Count.ShouldEqual(3);
				savedReason.Count(x => x.JobId.Equals(job.Id)).ShouldEqual(3);

				savedReason.Count(x => x.ReasonId.Equals(5531250)).ShouldEqual(1);
				savedReason.Count(x => x.ReasonId.Equals(5531251)).ShouldEqual(1);
				savedReason.Count(x => x.ReasonId.Equals(5531252)).ShouldEqual(1);
				savedReason.Count(x => x.OtherReasonText == null).ShouldEqual(3);
				savedReason.Count(x => x.EntityType.Equals(EntityTypes.Job)).ShouldEqual(3);
				savedReason.Count(x => x.ApprovalStatus.Equals(ApprovalStatuses.Rejected)).ShouldEqual(3);
			}
		}

		[Test]
		public void DenyReferral_JobWhenPassedNoOtherTextForOtherReasonId_ReturnsFailure()
		{
			var reason = new KeyValuePair<long, string>(5531254, "");
			// Arrange
			var request = new JobRequest
			{
				Criteria = new JobCriteria { JobId = 1101140 },
				ReferralStatusReasons = new List<KeyValuePair<long, string>> { reason }
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _jobService.DenyReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);

			var job = DataCoreRepository.FindById<Job>(1101140);

			job.ShouldNotBeNull();

			if (job != null)
			{
				var savedReason = job.JobReferralStatusReason.ToList();

				savedReason.Count().ShouldEqual(0);
			}
		}

		[Test]
		public void DenyReferral_JobWhenPassedInvalidReasonIdAndTextCombo_ReturnsFailure()
		{
			var reason = new KeyValuePair<long, string>(5531250, "Text is not valid with this reason ID");
			// Arrange
			var request = new JobRequest
			{
				Criteria = new JobCriteria { JobId = 1101140 },
				ReferralStatusReasons = new List<KeyValuePair<long, string>> { reason }
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _jobService.DenyReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);

			var job = DataCoreRepository.FindById<Job>(1101140);

			job.ShouldNotBeNull();

			job.ShouldNotBeNull();

			if (job != null)
			{
				var savedReason = job.JobReferralStatusReason.ToList();
				savedReason.Count().ShouldEqual(0);
			}
		}

		[Test]
		public void DenyReferral_JobWhenPassedDenialReason_ReturnsSuccess()
		{
			var reason = new KeyValuePair<long, string>(5531250, null);
			// Arrange
			var request = new JobRequest
			{
				Criteria = new JobCriteria { JobId = 1101140 },
				ReferralStatusReasons = new List<KeyValuePair<long, string>> { reason }
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _jobService.DenyReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			var job = DataCoreRepository.FindById<Job>(1101140);

			job.ShouldNotBeNull();

			job.ShouldNotBeNull();

			if (job != null)
			{
				var savedReason = job.JobReferralStatusReason.ToList();

				savedReason.Count.ShouldEqual(1);
				savedReason[0].JobId.ShouldEqual(job.Id);
				savedReason[0].ReasonId.ShouldEqual(5531250);
				savedReason[0].OtherReasonText.ShouldBeNull();
				savedReason[0].EntityType.ShouldEqual(EntityTypes.Job);
				savedReason[0].ApprovalStatus.ShouldEqual(ApprovalStatuses.Rejected);
			}
		}


		#endregion

		[Test]
		public void GetHiringManagerPersonNameEmailForJob_WhenPassedValidJobId_ReturnsSuccessWithHM()
		{
			// Arrange
			var request = new HiringManagerPersonNameEmailForJobRequest()
			{
				JobId = 1003735
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var queryResponse = _jobService.GetHiringManagerPersonNameEmailForJob(request);

			// Assert 
			queryResponse.Person.ShouldNotBeNull();
		}

		[Test]
		public void GetHiringManagerPersonNameEmailForJob_WhenPassedInValidJobId_ReturnsSuccessWithoutHM()
		{
			// Arrange
			var request = new HiringManagerPersonNameEmailForJobRequest()
			{
				JobId = 0
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var queryResponse = _jobService.GetHiringManagerPersonNameEmailForJob(request);

			// Assert 
			queryResponse.Person.ShouldBeNull();
		}

		#region Helpers

		public void DeleteJobApplicationActionEvents()
		{
			var events = DataCoreRepository.ActionEvents.Where(
				x =>
					x.EntityId == 1 && (x.ActionTypeId == 1003687 || x.ActionTypeId == 1003703) &&
					x.ActionedOn >= DateTime.Now.AddDays(-30).Date).ToList();

			foreach (var actionEvent in events)
			{
				DataCoreRepository.Remove(actionEvent);
			}
			DataCoreRepository.SaveChanges();

		}

		public List<ActionEvent> AddJobApplicationActionEvents()
		{
			var actionEvents = new List<ActionEvent>
			{
				new ActionEvent
				{
					SessionId = Guid.NewGuid(),
					RequestId = Guid.NewGuid(),
					UserId = 2,
					ActionedOn = DateTime.Now.AddDays(-30),
					EntityTypeId = 99963,
					EntityId = 1000059,
					ActionTypeId = 1003687
				},
				new ActionEvent
				{
					SessionId = Guid.NewGuid(),
					RequestId = Guid.NewGuid(),
					UserId = 17370,
					ActionedOn = DateTime.Now.AddDays(-15),
					EntityTypeId = 99963,
					EntityId = 1000059,
					ActionTypeId = 1003703
				},
				new ActionEvent
				{
					SessionId = Guid.NewGuid(),
					RequestId = Guid.NewGuid(),
					UserId = 17370,
					ActionedOn = DateTime.Now.AddDays(-5),
					EntityTypeId = 99963,
					EntityId = 1000059,
					ActionTypeId = 1003687
				}
			};

			DataCoreRepository.Add(actionEvents[0]);
			DataCoreRepository.Add(actionEvents[1]);
			DataCoreRepository.Add(actionEvents[2]);
			DataCoreRepository.SaveChanges();

			return actionEvents;
		}

		#endregion
	}
}
