﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using Focus.Core.Messages;
using Focus.Core.Messages.ProcessorService;
using Focus.Services.ServiceImplementations;
using Focus.UnitTests.Core;
using Framework.Testing.NUnit;

using NUnit.Framework;

#endregion

namespace Focus.UnitTests.Services
{
	[TestFixture]
	public class ProcessorServiceTests : TestFixtureBase
	{
		private ProcessorService _processorService;

		[SetUp]
		public override void SetUp()
		{
			base.SetUp();

			_processorService = new ProcessorService(RuntimeContext);
		}

		[Test]
		public void EnqueueBatch_WithValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new EnqueueBatchRequest { Identifier = "97d8c47be2cb45deb7d93e08c2864702" };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _processorService.EnqueueBatch(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void EnqueueBatch_WithInvalidRequest_ReturnsFailure()
		{
			// Arrange
			var request = new EnqueueBatchRequest { Identifier = "7b97e3f62f32426c8286d9ba1678a446" };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _processorService.EnqueueBatch(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}
		
	}
}
