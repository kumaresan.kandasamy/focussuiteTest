﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;

using Focus.Core;
using Focus.Core.Criteria.SpideredPosting;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Messages;
using Focus.Core.Messages.OrganizationService;
using Focus.Core.Messages.PostingService;
using Focus.Services.Repositories;
using Focus.Services.ServiceImplementations;
using Focus.UnitTests.Core;

using Framework.Core;
using Framework.Testing.NUnit;

using Moq;

using NUnit.Framework;

#endregion

namespace Focus.UnitTests.Services
{
	[TestFixture]
	public class PostingServiceTests : TestFixtureBase
	{
		private PostingService _postingService;
		
		[SetUp]
		public override void SetUp()
		{
			base.SetUp();

			SetUpCacheProvider();
			SetUpMockLogProvider();
			
			_postingService = new PostingService(RuntimeContext);
		}
		
		[Test]
		public void GetTypicalResumes_WhenPassedValidRequest_ReturnsSuccessAndData()
		{
			// Arrange
			var request = new TypicalResumesRequest { LensPostingId = "TestJob" };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _postingService.GetTypicalResumes(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.TypicalResumes.Count.ShouldBeGreater(0);
		}

		[Test]
		public void FetchPosting_WithValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new PostingRequest
			{
				LensPostingId = "Test Job Id",
				LogViewAction = true
			};
			request.Prepare(MockClientTag, MockSessionId, MockCareerUserData, MockCulture);

			// Act
			var response = _postingService.FetchPosting(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.PostingDetails.ShouldNotBeNull();

			var viewedEvent = (from actionEvent in DataCoreRepository.ActionEvents
												 join actionType in DataCoreRepository.ActionTypes
													 on actionEvent.ActionTypeId equals actionType.Id
												 where actionType.Name == "ViewedPostingFromSearch"
													 && actionEvent.EntityId == 1084090
												 orderby actionEvent.ActionedOn descending
												 select actionEvent).FirstOrDefault();

			// Check event has been logged
			viewedEvent.ShouldNotBeNull();
			if (viewedEvent.IsNotNull())
			{
				viewedEvent.UserId.ShouldEqual(MockCareerUserId);
				viewedEvent.EntityIdAdditional01.ShouldEqual(MockCareerUserId);
			}
		}

		[Test]
		public void FetchPosting_WithValidRequestWhenImpersonating_ReturnsSuccess()
		{
			// Arrange
			var request = new PostingRequest
			{
				LensPostingId = "Test Job Id",
				LogViewAction = true
			};
			request.Prepare(MockClientTag, MockSessionId, MockImpersonatedCareerUserData, MockCulture);

			// Act
			var response = _postingService.FetchPosting(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.PostingDetails.ShouldNotBeNull();

			var viewedEvent = (from actionEvent in DataCoreRepository.ActionEvents
												 join actionType in DataCoreRepository.ActionTypes
													 on actionEvent.ActionTypeId equals actionType.Id
												 where actionType.Name == "ViewedPostingFromSearch"
													 && actionEvent.EntityId == 1084090
												 orderby actionEvent.ActionedOn descending
												 select actionEvent).FirstOrDefault();

			// Check event has been logged
			viewedEvent.ShouldNotBeNull();
			if (viewedEvent.IsNotNull())
			{
				viewedEvent.UserId.ShouldEqual(MockUserId);
				viewedEvent.EntityIdAdditional01.ShouldEqual(MockCareerUserId);
			}
		}

		[Test]
		public void GetPostingId_WithValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new PostingRequest { LensPostingId = "TestJob" };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _postingService.GetPostingId(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.PostingId.ShouldNotBeNull();
		}

    [Test]
    public void GetPostingBGTOccs_WithValidRequest_ReturnsSuccess()
    {
      // Arrange
      var request = new PostingRequest { LensPostingId = "TestJob" };
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _postingService.GetPostingBGTOccs(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
    }

		[Test]
		public void ClosePosting_WithValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new ClosePostingRequest { LensPostingId = "TestJob" };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			
			// Act
			var response = _postingService.ClosePosting(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}
		
		[Test]
		public void GetViewedPosting_WithValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new ViewedPostingRequest { UserId = MockCareerUserId };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _postingService.GetViewedPostings(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void AddViewedPosting_WithValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new AddViewedPostingRequest { ViewedPosting = new ViewedPostingDto { LensPostingId = "ABC123_A66A29459D1E4BC3A6B53058E1628FC6", UserId = MockUserId, ViewedOn = DateTime.Now } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _postingService.AddViewedPostings(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void GetSpideredPostingsWithGoodMatches_WithValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new SpideredPostingRequest
											{
												Criteria = new SpideredPostingCriteria
																		{
																			OrderBy = Constants.SortOrders.EmployerNameAsc,
																			PageIndex = 0,
																			PageSize = 10,
																			PostingAge = 1000,
																			EmployerName = "Test Company"
																		}
											};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _postingService.GetSpideredPostingsWithGoodMatches(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
    public void GetJobIdFromLensId_WithValidReguest_ReturnsSuccess()
    {
      // Arrange
      var request = new GetJobIdFromPostingRequest { LensPostingId = "ABC123_A66A29459D1E4BC3A6B53058E1628FC6" };
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      //Act
      var response = _postingService.GetJobIdFromPosting(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
    }

		[Test]
		public void GetComparePostingToResumeModalModel_WithValidLensPostingId_ReturnsSuccess()
		{
			// Arrange
			var request = new GetComparePostingToResumeModalModelRequest { LensPostingId = "ABC123_2D2E5EEF256148E6A4F4ECFACD0E4743" };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _postingService.GetComparePostingToResumeModalModel(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		#region GetPostingEmployerContactModel Tests

		[Test]
		public void GetPostingEmployerContactModel_WithNoPostingId_ReturnsFailure()
		{
			// Arrange
			var request = new GetPostingEmployerContactModelRequest();
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _postingService.GetPostingEmployerContactModel(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}

		[Test]
		public void GetPostingEmployerContactModel_WithInvalidPostingId_ReturnsFailure()
		{
			// Arrange
			var request = new GetPostingEmployerContactModelRequest { PostingId = 1 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _postingService.GetPostingEmployerContactModel(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}

		[Test]
		public void GetPostingEmployerContactModel_WithPostingWithNoXmlAndInvalidLensPostingId_ReturnsFailure()
		{
			// Arrange
			var request = new GetPostingEmployerContactModelRequest { PostingId = 1118331 };

			var documentRepository = new Mock<IDocumentRepository>();
			documentRepository.Setup(x => x.GetDocumentText(It.IsAny<string>())).Returns("");
			((TestRepositories)RuntimeContext.Repositories).Document = documentRepository.Object;
			
			var postingService = new PostingService(RuntimeContext);

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = postingService.GetPostingEmployerContactModel(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}

		[Test]
		public void GetPostingEmployerContactModel_WithPostingWithNoXmlAndValidLensPostingId_ReturnsSuccess()
		{
			// Arrange
			var request = new GetPostingEmployerContactModelRequest { PostingId = 1118331 };
			var postingService = new PostingService(RuntimeContext);

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = postingService.GetPostingEmployerContactModel(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void GetPostingEmployerContactModel_WithNoPersonId_ReturnsSuccess()
		{
			// Arrange
			var request = new GetPostingEmployerContactModelRequest { PostingId = 1177700 };
			var postingService = new PostingService(RuntimeContext);

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = postingService.GetPostingEmployerContactModel(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void GetPostingEmployerContactModel_WithValidPersonId_ReturnsSuccess()
		{
			// Arrange
			var request = new GetPostingEmployerContactModelRequest { PostingId = 1177700, PersonId = 1003968 };
			var postingService = new PostingService(RuntimeContext);

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = postingService.GetPostingEmployerContactModel(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void GetPostingEmployerContactModel_WithInvalidPersonId_ReturnsFailure()
		{
			// Arrange
			var request = new GetPostingEmployerContactModelRequest { PostingId = 1177700, PersonId = 4 };
			var postingService = new PostingService(RuntimeContext);

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = postingService.GetPostingEmployerContactModel(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}
		
		#endregion

	}
}
