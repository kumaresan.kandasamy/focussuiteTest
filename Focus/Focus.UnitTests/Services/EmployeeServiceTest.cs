﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;

using Focus.Core;
using Focus.Core.Criteria;
using Focus.Core.Criteria.Employee;
using Focus.Core.Criteria.Employer;
using Focus.Core.Messages.EmployeeService;
using Focus.Core.Messages.EmployerService;
using Focus.Core.Views;
using Focus.Data.Core.Entities;
using Focus.Services.Repositories;
using Focus.Services.ServiceImplementations;
using Focus.UnitTests.Core;
using Framework.Testing.NUnit;

using AcknowledgementType = Focus.Core.Messages.AcknowledgementType;

using Moq;
using NUnit.Framework;

#endregion

namespace Focus.UnitTests.Services
{
	[TestFixture]
	class EmployeeServiceTest : TestFixtureBase
	{
		private EmployeeService _service;
		// private Mock<IClientRepository> _employeeRepository;

		[SetUp]
		public override void SetUp()
		{
			base.SetUp();

			SetUpCacheProvider();
			SetUpMockLogProvider();

			_service = new EmployeeService(RuntimeContext);
		}

		[Test]
		public void GetEmployee_WhenPassedValidRequest_ReturnsSuccessAndData()
		{
			// Arrange
			var request = new EmployeeRequest { Id = 1000054 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetEmployee(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Employee.PersonId.ShouldEqual(1000049);
		}

		[Test]
		public void GetEmployeesForSameEmployer_WhenPassedValidRequest_ReturnsSuccessAndData()
		{
			// Arrange
			var request = new EmployeeRequest { Id = 1104332 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetEmployeesForSameEmployer(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.SameCompanyEmployeeIds.Count.ShouldEqual(4);
		}

		[Test]
		public void GetHiringAreas_WhenPassedValidRequest_ReturnsSuccessAndData()
		{
			// Arrange
			var request = new EmployeeRequest { Id = 1091969 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetHiringAreas(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.HiringAreas.Count.ShouldEqual(8);
		}

		[Test]
		public void GetEmployeesForEmployers_WhenPassedValidRequest_ReturnsSuccessAndData()
		{
			// Arrange
			var request = new EmployeesForEmployerRequest
			{
				Criteria = new EmployeeCriteria
				{
					FetchOption = CriteriaBase.FetchOptions.List,
					EmployerIds = new List<long> { 29441, 1594638 }
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetEmployeesForEmployers(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Employees.Count.ShouldEqual(3);

			var ids = response.Employees.Select(e => e.Id).ToList();
			ids.Contains(29452).ShouldBeTrue();
		}

		[Test]
		public void GetEmployeesForEmployers_WhenPassedValidRequestWithBusinessUnits_ReturnsSuccessAndData()
		{
			// Arrange
			var request = new EmployeesForEmployerRequest
			{
				Criteria = new EmployeeCriteria
				{
					FetchOption = CriteriaBase.FetchOptions.List,
					BusinessUnitIds = new List<long> { 29444, 1546755, 1594639 }
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetEmployeesForEmployers(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Employees.Count.ShouldEqual(2);

			var ids = response.Employees.Select(e => e.Id).ToList();
			ids.Contains(29452).ShouldBeTrue();
			ids.Contains(1000054).ShouldBeTrue();
		}

		[Test]
		public void GetEmployeesForEmployers_WhenPassedValidRequestForCount_ReturnsSuccessAndCount()
		{
			// Arrange
			var request = new EmployeesForEmployerRequest
			{
				Criteria = new EmployeeCriteria
				{
					FetchOption = CriteriaBase.FetchOptions.Count,
					BusinessUnitIds = new List<long> { 29444, 1546755, 1594639 }
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetEmployeesForEmployers(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.EmployeesCount.ShouldEqual(2);
		}

		[Test]
		public void GetEmployeesForEmployers_WhenPassedValidRequestWithBusinessUnitsSubscribedOnly_ReturnsSuccessAndData()
		{
			// Arrange
			var request = new EmployeesForEmployerRequest
			{
				Criteria = new EmployeeCriteria
				{
					FetchOption = CriteriaBase.FetchOptions.List,
					BusinessUnitIds = new List<long> { 29444, 1546755, 1594639 },
					SubscribedOnly = true
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetEmployeesForEmployers(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Employees.Count.ShouldEqual(1);
		}

		[Test]
		public void Search_WhenPassedValidRequest_ReturnsSuccessAndData()
		{
			// Arrange
			var request = new EmployeeSearchRequest { Criteria = new EmployeeCriteria { EmailAddress = "employer@talent.com" } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.Search(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.EmployeeSearchViewsPaged.Count.ShouldEqual(1);
			response.EmployeeSearchViewsPaged[0].Id.ShouldEqual(1132767);
		}

		[Test]
		public void Search_WhenPassedValidRequestWithId_ReturnsSuccessAndData()
		{
			// Arrange
			var request = new EmployeeSearchRequest { Criteria = new EmployeeCriteria { Id = 1132767 } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.Search(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.EmployeeSearchViewsPaged.Count.ShouldEqual(1);
			response.EmployeeSearchViewsPaged[0].EmailAddress.ShouldEqual("employer@talent.com");
		}

		[Test]
		public void Search_WhenPassedValidRequestWithBusinessUnitId_ReturnsSuccessAndData()
		{
			// Arrange
			var request = new EmployeeSearchRequest { Criteria = new EmployeeCriteria { BusinessUnitId = 1132768 } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.Search(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.EmployeeSearchViewsPaged.Count.ShouldBeGreaterOrEqual(1);
		}

		[Test]
		public void Search_WhenPassedValidRequestWithMultipleFeins_ReturnsSuccessAndData()
		{
			// Arrange
			var request = new EmployeeSearchRequest
			{
				Criteria = new EmployeeCriteria
				{
					EmployerCharacteristics = new EmployerCharacteristicsCriteria
					{
						FederalEmployerIdentificationNumbers = new List<string> { "88-8181818", "99-9191919", "99-2434343" }
					}
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.Search(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.EmployeeSearchViewsPaged.Count.ShouldEqual(2);
		}

		[Test]
		public void EmailEmployee_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new EmailEmployeeRequest { EmployeeId = 1000054, EmailSubject = "UnitTest", ClientTag = "TestBody", BccSelf = true };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.EmailEmployee(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void EmailEmployee_WhenPassedValidRequestWithAttachment_ReturnsSuccess()
		{
			var emailBody = System.Text.Encoding.UTF8.GetBytes("This is a test");
			// Arrange
			var request = new EmailEmployeeRequest { EmployeeId = 1000054, EmailSubject = "UnitTest", ClientTag = "TestBody", BccSelf = true, Attachment = emailBody, AttachmentName = "Test.html" };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.EmailEmployee(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void EmailEmployee_WithValidRequestOneEmailSendCopyAndUseOfficeAddressForSendMessagesTrue_ReturnsSuccess()
		{
			MockAppSettings.UseOfficeAddressToSendMessages = true;

			// Arrange
			var request = new EmailEmployeeRequest { EmployeeId = 1000054, EmailSubject = "UnitTest", ClientTag = "TestBody", BccSelf = true, SendCopy = true };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.EmailEmployee(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			MockAppSettings.UseOfficeAddressToSendMessages = false;
		}

		[Test]
		public void EmailEmployee_WithValidRequestOneEmailSendCopyAndUseOfficeAddressForSendMessagesFalse_ReturnsSuccess()
		{
			MockAppSettings.UseOfficeAddressToSendMessages = false; // False is the default setting

			// Arrange
			var request = new EmailEmployeeRequest { EmployeeId = 1000054, EmailSubject = "UnitTest", ClientTag = "TestBody", BccSelf = true, SendCopy = true };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.EmailEmployee(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			MockAppSettings.UseOfficeAddressToSendMessages = false;
		}

		[Test]
		public void EmailEmployee_WithValidRequestOneEmailDoNotSendCopyAndUseOfficeAddressForSendMessagesTrue_ReturnsSuccess()
		{
			MockAppSettings.UseOfficeAddressToSendMessages = true;

			// Arrange
			var request = new EmailEmployeeRequest { EmployeeId = 1000054, EmailSubject = "UnitTest", ClientTag = "TestBody", BccSelf = true, SendCopy = false };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.EmailEmployee(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			MockAppSettings.UseOfficeAddressToSendMessages = false;
		}

		[Test]
		public void EmailEmployee_WithValidRequestOneEmailDoNotSendCopyAndUseOfficeAddressForSendMessagesFalse_ReturnsSuccess()
		{
			MockAppSettings.UseOfficeAddressToSendMessages = false; // False is the default setting

			// Arrange
			var request = new EmailEmployeeRequest { EmployeeId = 1000054, EmailSubject = "UnitTest", ClientTag = "TestBody", BccSelf = true, SendCopy = false };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.EmailEmployee(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			MockAppSettings.UseOfficeAddressToSendMessages = false;
		}

		[Test]
		public void GetEmployerAccountReferralView_WhenPassedValidRequest_ReturnsSuccessAndData()
		{
			// Arrange
			var request = new EmployerAccountReferralViewRequest { Criteria = new EmployerAccountReferralCriteria { Id = 1157083 } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetEmployerAccountReferralView(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Referral.EmployeeId.ShouldEqual(1157079);
		}

		[Test]
		public void GetEmployerAccountReferralView_WhenPassedValidRequestWithCriteria_ReturnsSuccessAndData()
		{
			// Arrange
			var request = new EmployerAccountReferralViewRequest { Criteria = new EmployerAccountReferralCriteria { OfficeIds = new List<long?> { 2 }, EmployerType = "4755000", ApprovalType = ApprovalType.Employer, FetchOption = CriteriaBase.FetchOptions.PagedList } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetEmployerAccountReferralView(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.ReferralViewsPaged.ShouldNotBeNull();
			response.ReferralViewsPaged.Count.ShouldEqual(1);
			response.ReferralViewsPaged[0].EmployerId.ShouldEqual(1157083);

			var employerOfficeMappers = DataCoreRepository.EmployerOfficeMappers.Where(x => x.EmployerId == response.ReferralViewsPaged[0].EmployerId).ToList();
			employerOfficeMappers.ShouldNotBeNull();
			employerOfficeMappers.Count.ShouldEqual(2);
		}

		[Test]
		public void ApproveReferral_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new EmployeeRequest { EmployeeId = 1157079, BusinessUnitId = 1157080, EmployerApprovalType = ApprovalType.HiringManager };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ApproveReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void ApproveReferralForEmployer_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new EmployeeRequest { EmployeeId = 7212428, BusinessUnitId = 7212429, EmployerApprovalType = ApprovalType.Employer};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ApproveReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.EmployerApprovalStatus.ShouldEqual(ApprovalStatuses.Approved);
		}
		
		[Test]
		public void ApprovalReferralForHiringManager_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new EmployeeRequest { EmployeeId = 7212428, BusinessUnitId = 7212429, EmployerApprovalType = ApprovalType.HiringManager };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ApproveReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.ReferralResponseType = ApproveReferralResponseType.ParentCompanyLinkedToHiringManagerNotApproved;
		}


		[Test] public void ApprovalReferralForHiringManager_WhenPassedInValidRequest_ReturnsFailure()
		{
			// Arrange
			var request = new EmployeeRequest { EmployeeId = 0, BusinessUnitId = 7212429, EmployerApprovalType = ApprovalType.HiringManager };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ApproveReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}

		[Test]
		public void ApprovalReferral_WhenPassedInValidRequestWithNoApprovalType_ReturnsFailure()
		{
			// Arrange
			var request = new EmployeeRequest { EmployeeId = 7212428, BusinessUnitId = 7212429};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ApproveReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}

		[Test]
		public void ApprovalReferralForBusinessUnit_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new EmployeeRequest { EmployeeId = 7212428, BusinessUnitId = 7212429, EmployerApprovalType = ApprovalType.BusinessUnit };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ApproveReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.ReferralResponseType = ApproveReferralResponseType.ParentCompanyLinkedToBusinessUnitNotApproved;
		}

		[Test]
		public void ApprovalReferralForBusinessUnit_WhenPassedInValidRequest_ReturnsFailure()
		{
			// Arrange
			var request = new EmployeeRequest { EmployeeId = 7212428, BusinessUnitId = 0, EmployerApprovalType = ApprovalType.BusinessUnit };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ApproveReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}

		[Test]
		public void ApprovalReferralForEmployer_WhenPassedInValidRequest_ReturnsFailure()
		{
			// Arrange
			var request = new EmployeeRequest { EmployeeId = 0, BusinessUnitId = 7212429, EmployerApprovalType = ApprovalType.Employer };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ApproveReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}

		
		[Test]
		public void HoldReferral_WhenPassedInValidRequestWithoutReasons_ReturnsFailure()
		{
			// Arrange
      var request = new EmployeeRequest { EmployeeId = 1157079, BusinessUnitId = 1157080, EmployerApprovalType = ApprovalType.HiringManager };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.HoldReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}

    [Test]
    public void HoldReferral_WhenPassedInValidRequest_ReturnsFailure()
    {
      // Arrange
      var request = new EmployeeRequest { EmployeeId = 0, BusinessUnitId = 1157080, EmployerApprovalType = ApprovalType.HiringManager };
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.HoldReferral(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
    }

		[Test]
		public void HoldReferralBusinessUnit_WhenPassedInValidRequest_ReturnsFailure()
		{
			// Arrange
			var request = new EmployeeRequest
			{
				EmployeeId = 0,
				BusinessUnitId = 1157080,
				EmployerApprovalType = ApprovalType.BusinessUnit,
				ReferralStatusReasons = new List<KeyValuePair<long, string>>()
				{
					new KeyValuePair<long, string>(5531255, null),
					new KeyValuePair<long, string>(5531257, null),
					new KeyValuePair<long, string>(5531258, null),
					new KeyValuePair<long, string>(5531260, "other text")
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.HoldReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}
		
		[Test]
		public void HoldReferral_WhenPassedInValidRequestWithNoApprovalType_ReturnsFailure()
		{
			// Arrange
			// Arrange
			var request = new EmployeeRequest
			{
				EmployeeId = 0,
				BusinessUnitId = 1157080,
				ReferralStatusReasons = new List<KeyValuePair<long, string>>()
				{
					new KeyValuePair<long, string>(5531255, null),
					new KeyValuePair<long, string>(5531257, null),
					new KeyValuePair<long, string>(5531258, null),
					new KeyValuePair<long, string>(5531260, "other text")
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);


			// Act
			var response = _service.HoldReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}


		[Test]
		public void AssignActivity_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new AssignEmployeeActivityRequest { EmployeeId = 1000054, ActivityId = 1107787 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.AssignActivity(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void AssignActivityToMultipleEmployees_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new AssignEmployeeActivityRequest { EmployeeIds = new List<long> { 1000054, 1088856, 1091969 }, ActivityId = 1107787 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.AssignActivityToMultipleEmployees(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}
	
		[Test]
		public void BlockAccount_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new EmployeeRequest { Id = 1136497 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.BlockAccount(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}
		
		[Test]
		public void UnblockAccount_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new EmployeeRequest { Id = 1000054 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.UnblockAccount(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}
	
		[Test]
		public void GetEmployeeUserDetails_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new EmployeeUserDetailsRequest { EmployeeId = 1088856 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetEmployeeUserDetails(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.AddressDetails.PersonId.ShouldEqual(1088853);
		}
		
		[Test]
		public void GetEmployeeBusinessUnitView_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new EmployeeBusinessUnitViewRequest { Criteria = new EmployeeBusinessUnitCriteria { Id = 1088912 } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetEmployeeBusinessUnitView(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.EmployeeBusinessUnitView.ShouldNotBeNull();
		}
		
		[Test]
		public void AssignEmployeeToBusinessUnit_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new AssignEmployeeToBusinessUnitRequest { EmployeeId = 1088856, BusinessUnitId = 1192837 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.AssignEmployeeToBusinessUnit(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}
		
		[Test]
		public void SetBusinessUnitAsDefault_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new SetBusinessUnitAsDefaultRequest { EmployeeId = 1185024, BusinessUnitId = 1187320};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.SetBusinessUnitAsDefault(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}
		
		[Test]
		public void GetEmployeeBusinessUnitActivityUsers_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new EmployeeBusinessUnitActivityRequest { EmployeeBuId = 1185030 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetEmployeeBusinessUnitActivityUsers(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.EmployeeBusinessUnitActivityUsers.Count.ShouldEqual(2);
		}
		
		[Test]
		public void GetHiringManagerActivityView_WhenPassedValidRequest_ReturnsSuccessWithExpectedData()
		{
			// Arrange
			var request = new EmployeeBusinessUnitActivityRequest { Criteria = new EmployeeBusinessUnitCriteria { Id = 5526447, DaysBack = 10000 } };

			request.Criteria.OrderBy = Constants.SortOrders.ActivityDateDesc;
			request.Criteria.FetchOption = CriteriaBase.FetchOptions.PagedList;

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act	
			var response = _service.GetHiringManagerActivityView(request);

			// Assert 
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.EmployeeBusinessUnitActivityPaged.TotalCount.ShouldNotEqual(0);
		}
		
		[Test]
		public void GetHiringManagerProfile_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange 
			var request = new EmployeeBusinessUnitInfoRequest { Id = 5526447 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetHiringManagerProfile(request);

			// Assert
			response.ShouldNotBeNull();
      
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}
		
		#region Deny Referral Tests

		[Test]
		public void DenyReferral_WhenPassedInValidRequestWithoutApprovalType_ReturnsFailure()
		{
			// Arrange
			var request = new EmployeeRequest { EmployeeId = 1157079, BusinessUnitId = 1157080 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.DenyReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}


		#region Approval type - Hiring Manager

		[Test]
		public void DenyReferralForHiringManager_WhenPassedInValidRequest_ReturnsFailure()
		{
			// Arrange
			var request = new EmployeeRequest { EmployeeId = 0, BusinessUnitId = 0, EmployerApprovalType = ApprovalType.HiringManager };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.DenyReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);

			var employee = DataCoreRepository.FindById<Employee>(7212428);

			employee.ApprovalStatus.ShouldNotEqual(ApprovalStatuses.Rejected);
		}

		[Test]
		public void DenyReferral_HiringManagerWhenPassedMultipleDenialReasonsWithOtherReason_ReturnsSuccess()
		{
			// Arrange
			var request = new EmployeeRequest
			{
				EmployeeId = 1157079,
				BusinessUnitId = 1157080,
				EmployerApprovalType = ApprovalType.HiringManager,
				ReferralStatusReasons = new List<KeyValuePair<long, string>>()
				{
					new KeyValuePair<long, string>(5531261, null),
					new KeyValuePair<long, string>(5531263, null),
					new KeyValuePair<long, string>(5531264, null),
					new KeyValuePair<long, string>(5531266, "other text")
				},
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.DenyReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			var employee = DataCoreRepository.FindById<Employee>(1157079);
			var savedReason = employee.EmployeeReferralStatusReasons.ToList();
			employee.ApprovalStatus.ShouldEqual(ApprovalStatuses.Rejected);

			savedReason.Count.ShouldEqual(4);
			savedReason.Count(x => x.EmployeeId.Equals(employee.Id)).ShouldEqual(4);

			savedReason.Count(x => x.ReasonId.Equals(5531261)).ShouldEqual(1);
			savedReason.Count(x => x.ReasonId.Equals(5531263)).ShouldEqual(1);
			savedReason.Count(x => x.ReasonId.Equals(5531264)).ShouldEqual(1);
			savedReason.Count(x => x.OtherReasonText == null).ShouldEqual(3);
			savedReason.Count(x => x.ReasonId.Equals(5531266)).ShouldEqual(1);
			savedReason.Count(x => x.OtherReasonText == "other text" && x.ReasonId.Equals(5531266)).ShouldEqual(1);
			savedReason.Count(x => x.EntityType.Equals(EntityTypes.Staff)).ShouldEqual(4);
			savedReason.Count(x => x.ApprovalStatus.Equals(ApprovalStatuses.Rejected)).ShouldEqual(4);
		}

		[Test]
		public void DenyReferral_HiringManagerWhenPassedOtherReason_ReturnsSuccess()
		{
			// Arrange
			var request = new EmployeeRequest
			{
				EmployeeId = 1157079,
				BusinessUnitId = 1157080,
				EmployerApprovalType = ApprovalType.HiringManager,
				ReferralStatusReasons = new List<KeyValuePair<long, string>>()
				{
					new KeyValuePair<long, string>(5531266, "other text")
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.DenyReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			var employee = DataCoreRepository.FindById<Employee>(1157079);
			var savedReason = employee.EmployeeReferralStatusReasons.ToList();

			savedReason.Count.ShouldEqual(1);
			savedReason[0].EmployeeId.ShouldEqual(employee.Id);
			savedReason[0].ReasonId.ShouldEqual(5531266);
			savedReason[0].OtherReasonText.ShouldEqual("other text");
			savedReason[0].EntityType.ShouldEqual(EntityTypes.Staff);
			savedReason[0].ApprovalStatus.ShouldEqual(ApprovalStatuses.Rejected);
		}

		[Test]
		public void DenyReferral_HiringManagerWhenPassedMultipleDenialReasonsWithInvalidReasonIdAndTextCombo_ReturnsFailure()
		{
			// Arrange
			var request = new EmployeeRequest
			{
				EmployeeId = 1157079,
				BusinessUnitId = 1157080,
				EmployerApprovalType = ApprovalType.HiringManager,
				ReferralStatusReasons = new List<KeyValuePair<long, string>>()
				{
					new KeyValuePair<long, string>(5531262, null),
					new KeyValuePair<long, string>(5531263, null),
					new KeyValuePair<long, string>(5531264, "Text is not valid with this reason ID")
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.DenyReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);

			var employee = DataCoreRepository.FindById<Employee>(1157079);
			employee.ApprovalStatus.ShouldNotEqual(ApprovalStatuses.Rejected);

			var savedReason = employee.EmployeeReferralStatusReasons.ToList();

			savedReason.Count().ShouldEqual(0);
		}

		[Test]
		public void DenyReferral_HiringManagerWhenPassedMultipleDenialReasonsWithNoOtherTextForOtherReasonId_ReturnsFailure()
		{
			// Arrange
			var request = new EmployeeRequest
			{
				EmployeeId = 1157079,
				BusinessUnitId = 1157080,
				EmployerApprovalType = ApprovalType.HiringManager,
				ReferralStatusReasons = new List<KeyValuePair<long, string>>()
				{
					new KeyValuePair<long, string>(5531262, null),
					new KeyValuePair<long, string>(5531264, null),
					new KeyValuePair<long, string>(5531266, "")
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.DenyReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);

			var employee = DataCoreRepository.FindById<Employee>(1157079);
			employee.ApprovalStatus.ShouldNotEqual(ApprovalStatuses.Rejected);

			var savedReason = employee.EmployeeReferralStatusReasons.ToList();

			savedReason.Count().ShouldEqual(0);
		}

		[Test]
		public void DenyReferral_HiringManagerWhenPassedMultipleDenialReasons_ReturnsSuccess()
		{
			// Arrange
			var request = new EmployeeRequest
			{
				EmployeeId = 1157079,
				BusinessUnitId = 1157080,
				EmployerApprovalType = ApprovalType.HiringManager,
				ReferralStatusReasons = new List<KeyValuePair<long, string>>()
				{
					new KeyValuePair<long, string>(5531261, null),
					new KeyValuePair<long, string>(5531263, null),
					new KeyValuePair<long, string>(5531265, null)
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.DenyReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			var employee = DataCoreRepository.FindById<Employee>(1157079);
			employee.ApprovalStatus.ShouldEqual(ApprovalStatuses.Rejected);

			var savedReason = employee.EmployeeReferralStatusReasons.ToList();

			savedReason.Count.ShouldEqual(3);
			savedReason.Count(x => x.EmployeeId.Equals(employee.Id)).ShouldEqual(3);

			savedReason.Count(x => x.ReasonId.Equals(5531261)).ShouldEqual(1);
			savedReason.Count(x => x.ReasonId.Equals(5531263)).ShouldEqual(1);
			savedReason.Count(x => x.ReasonId.Equals(5531265)).ShouldEqual(1);
			savedReason.Count(x => x.OtherReasonText == null).ShouldEqual(3);
			savedReason.Count(x => x.EntityType.Equals(EntityTypes.Staff)).ShouldEqual(3);
			savedReason.Count(x => x.ApprovalStatus.Equals(ApprovalStatuses.Rejected)).ShouldEqual(3);
		}

		[Test]
		public void DenyReferral_HiringManagerWhenPassedNoOtherTextForOtherReasonId_ReturnsFailure()
		{
			var reason = new KeyValuePair<long, string>(5531266, "");
			// Arrange
			var request = new EmployeeRequest
			{
				EmployeeId = 1157079,
				BusinessUnitId = 1157080,
				EmployerApprovalType = ApprovalType.HiringManager,
				ReferralStatusReasons = new List<KeyValuePair<long, string>>() { reason }
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.DenyReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);

			var employee = DataCoreRepository.FindById<Employee>(1157079);
			employee.ApprovalStatus.ShouldNotEqual(ApprovalStatuses.Rejected);

			var savedReason = employee.EmployeeReferralStatusReasons.ToList();

			savedReason.Count().ShouldEqual(0);
		}

		[Test]
		public void DenyReferral_HiringManagerWhenPassedInvalidReasonIdAndTextCombo_ReturnsFailure()
		{
			var reason = new KeyValuePair<long, string>(5531264, "Text is not valid with this reason ID");
			// Arrange
			var request = new EmployeeRequest
			{
				EmployeeId = 1157079,
				BusinessUnitId = 1157080,
				EmployerApprovalType = ApprovalType.HiringManager,
				ReferralStatusReasons = new List<KeyValuePair<long, string>>() { reason }
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.DenyReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);

			var employee = DataCoreRepository.FindById<Employee>(1157079);
			employee.ApprovalStatus.ShouldNotEqual(ApprovalStatuses.Rejected);

			var savedReason = employee.EmployeeReferralStatusReasons.ToList();

			savedReason.Count().ShouldEqual(0);
		}

		[Test]
		public void DenyReferral_HiringManagerWhenPassedDenialReason_ReturnsSuccess()
		{
			// Arrange
			var request = new EmployeeRequest
			{
				EmployeeId = 1157079,
				BusinessUnitId = 1157080,
				EmployerApprovalType = ApprovalType.HiringManager,
				ReferralStatusReasons = new List<KeyValuePair<long, string>>()
        {
          new KeyValuePair<long, string>(5531264, null)
        }
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.DenyReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			var employee = DataCoreRepository.FindById<Employee>(1157079);
			var savedReason = employee.EmployeeReferralStatusReasons.ToList();

			savedReason.Count.ShouldEqual(1);
			savedReason[0].EmployeeId.ShouldEqual(employee.Id);
			savedReason[0].ReasonId.ShouldEqual(5531264);
			savedReason[0].OtherReasonText.ShouldBeNull();
			savedReason[0].EntityType.ShouldEqual(EntityTypes.Staff);
			savedReason[0].ApprovalStatus.ShouldEqual(ApprovalStatuses.Rejected);
		}



		#endregion

		#region Approval type - Employer

		[Test]
		public void DenyReferralForEmployer_WhenPassedInValidRequest_ReturnsFailure()
		{
			// Arrange
			var request = new EmployeeRequest { EmployeeId = 0, BusinessUnitId = 7212429, EmployerApprovalType = ApprovalType.Employer };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.DenyReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}

		[Test]
		public void DenyReferral_EmployerWhenPassedMultipleDenialReasonsWithOtherReason_ReturnsSuccess()
		{
			// Arrange
			var request = new EmployeeRequest
			{
				EmployeeId = 7212428,
				BusinessUnitId = 7212429,
				EmployerApprovalType = ApprovalType.Employer,
				ReferralStatusReasons = new List<KeyValuePair<long, string>>()
				{
					new KeyValuePair<long, string>(5531231, null),
					new KeyValuePair<long, string>(5531236, null),
					new KeyValuePair<long, string>(5531238, null),
					new KeyValuePair<long, string>(5531240, "other text")
				},
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.DenyReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			var employee = DataCoreRepository.FindById<Employee>(7212428);
			var savedReason = employee.Employer.EmployerReferralStatusReasons.ToList();

			savedReason.Count.ShouldEqual(4);
			savedReason.Count(x => x.EmployerId.Equals(employee.EmployerId)).ShouldEqual(4);

			savedReason.Count(x => x.ReasonId.Equals(5531231)).ShouldEqual(1);
			savedReason.Count(x => x.ReasonId.Equals(5531236)).ShouldEqual(1);
			savedReason.Count(x => x.ReasonId.Equals(5531238)).ShouldEqual(1);
			savedReason.Count(x => x.OtherReasonText == null).ShouldEqual(3);
			savedReason.Count(x => x.ReasonId.Equals(5531240)).ShouldEqual(1);
			savedReason.Count(x => x.OtherReasonText == "other text" && x.ReasonId.Equals(5531240)).ShouldEqual(1);
			savedReason.Count(x => x.EntityType.Equals(EntityTypes.Employer)).ShouldEqual(4);
			savedReason.Count(x => x.ApprovalStatus.Equals(ApprovalStatuses.Rejected)).ShouldEqual(4);
		}

		[Test]
		public void DenyReferral_EmployerWhenPassedOtherReason_ReturnsSuccess()
		{
			// Arrange
			var request = new EmployeeRequest
			{
				EmployeeId = 7212428,
				BusinessUnitId = 7212429,
				EmployerApprovalType = ApprovalType.Employer,
				ReferralStatusReasons = new List<KeyValuePair<long, string>>()
				{
					new KeyValuePair<long, string>(5531240, "other text")
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.DenyReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			var employee = DataCoreRepository.FindById<Employee>(7212428);
			var savedReason = employee.Employer.EmployerReferralStatusReasons.ToList();

			savedReason.Count.ShouldEqual(1);
			savedReason[0].EmployerId.ShouldEqual(employee.EmployerId);
			savedReason[0].ReasonId.ShouldEqual(5531240);
			savedReason[0].OtherReasonText.ShouldEqual("other text");
			savedReason[0].EntityType.ShouldEqual(EntityTypes.Employer);
			savedReason[0].ApprovalStatus.ShouldEqual(ApprovalStatuses.Rejected);
		}

		[Test]
		public void DenyReferral_EmployerWhenPassedMultipleDenialReasonsWithInvalidReasonIdAndTextCombo_ReturnsFailure()
		{
			// Arrange
			var request = new EmployeeRequest
			{
				EmployeeId = 7212428,
				BusinessUnitId = 7212429,
				EmployerApprovalType = ApprovalType.Employer,
				ReferralStatusReasons = new List<KeyValuePair<long, string>>()
				{
					new KeyValuePair<long, string>(5531231, null),
					new KeyValuePair<long, string>(5531236, null),
					new KeyValuePair<long, string>(5531238, "Text is not valid with this reason ID")
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.DenyReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);

			var employee = DataCoreRepository.FindById<Employee>(7212428);
			var savedReason = employee.Employer.EmployerReferralStatusReasons.ToList();

			savedReason.Count().ShouldEqual(0);
		}

		[Test]
		public void DenyReferral_EmployerWhenPassedMultipleDenialReasonsWithNoOtherTextForOtherReasonId_ReturnsFailure()
		{
			// Arrange
			var request = new EmployeeRequest
			{
				EmployeeId = 7212428,
				BusinessUnitId = 7212429,
				EmployerApprovalType = ApprovalType.Employer,
				ReferralStatusReasons = new List<KeyValuePair<long, string>>()
				{
					new KeyValuePair<long, string>(5531231, null),
					new KeyValuePair<long, string>(5531236, null),
					new KeyValuePair<long, string>(5531240, "")
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.DenyReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);

			var employee = DataCoreRepository.FindById<Employee>(7212428);
			var savedReason = employee.Employer.EmployerReferralStatusReasons.ToList();

			savedReason.Count().ShouldEqual(0);
		}

		[Test]
		public void DenyReferral_EmployerWhenPassedMultipleDenialReasons_ReturnsSuccess()
		{
			// Arrange
			var request = new EmployeeRequest
			{
				EmployeeId = 7212428,
				BusinessUnitId = 7212429,
				EmployerApprovalType = ApprovalType.Employer,
				ReferralStatusReasons = new List<KeyValuePair<long, string>>()
				{
					new KeyValuePair<long, string>(5531231, null),
					new KeyValuePair<long, string>(5531236, null),
					new KeyValuePair<long, string>(5531238, null)
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.DenyReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			var employee = DataCoreRepository.FindById<Employee>(7212428);
			var savedReason = employee.Employer.EmployerReferralStatusReasons.ToList();

			savedReason.Count.ShouldEqual(3);
			savedReason.Count(x => x.EmployerId.Equals(employee.EmployerId)).ShouldEqual(3);

			savedReason.Count(x => x.ReasonId.Equals(5531231)).ShouldEqual(1);
			savedReason.Count(x => x.ReasonId.Equals(5531236)).ShouldEqual(1);
			savedReason.Count(x => x.ReasonId.Equals(5531238)).ShouldEqual(1);
			savedReason.Count(x => x.OtherReasonText == null).ShouldEqual(3);
			savedReason.Count(x => x.EntityType.Equals(EntityTypes.Employer)).ShouldEqual(3);
			savedReason.Count(x => x.ApprovalStatus.Equals(ApprovalStatuses.Rejected)).ShouldEqual(3);
		}

		[Test]
		public void DenyReferral_EmployerWhenPassedNoOtherTextForOtherReasonId_ReturnsFailure()
		{
			var reason = new KeyValuePair<long, string>(5531240, "");
			// Arrange
			var request = new EmployeeRequest
			{
				EmployeeId = 7212428,
				BusinessUnitId = 7212429,
				EmployerApprovalType = ApprovalType.Employer,
				ReferralStatusReasons = new List<KeyValuePair<long, string>>() { reason }
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.DenyReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);

			var employee = DataCoreRepository.FindById<Employee>(7212428);
			var savedReason = employee.Employer.EmployerReferralStatusReasons.ToList();

			savedReason.Count().ShouldEqual(0);
		}

		[Test]
		public void DenyReferral_EmployerWhenPassedInvalidReasonIdAndTextCombo_ReturnsFailure()
		{
			var reason = new KeyValuePair<long, string>(5531231, "Text is not valid with this reason ID");
			// Arrange
			var request = new EmployeeRequest
			{
				EmployeeId = 7212428,
				BusinessUnitId = 7212429,
				EmployerApprovalType = ApprovalType.Employer,
				ReferralStatusReasons = new List<KeyValuePair<long, string>>() { reason }
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.DenyReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);

			var employee = DataCoreRepository.FindById<Employee>(7212428);
			var savedReason = employee.Employer.EmployerReferralStatusReasons.ToList();

			savedReason.Count().ShouldEqual(0);
		}

		[Test]
		public void DenyReferral_EmployerWhenPassedDenialReason_ReturnsSuccess()
		{
			// Arrange
			var request = new EmployeeRequest
			{
				EmployeeId = 7212428,
				BusinessUnitId = 7212429,
				EmployerApprovalType = ApprovalType.Employer,
				ReferralStatusReasons = new List<KeyValuePair<long, string>>()
        {
          new KeyValuePair<long, string>(5531231, null)
        }
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.DenyReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.EmployerApprovalStatus.ShouldEqual(ApprovalStatuses.Rejected);

			var employee = DataCoreRepository.FindById<Employee>(7212428);
			var savedReason = employee.Employer.EmployerReferralStatusReasons.ToList();

			savedReason.Count.ShouldEqual(1);
			savedReason[0].EmployerId.ShouldEqual(employee.EmployerId);
			savedReason[0].ReasonId.ShouldEqual(5531231);
			savedReason[0].OtherReasonText.ShouldBeNull();
			savedReason[0].EntityType.ShouldEqual(EntityTypes.Employer);
			savedReason[0].ApprovalStatus.ShouldEqual(ApprovalStatuses.Rejected);
		}

		#endregion

		#region Approval type - Business Unit

		[Test]
		public void DenyReferralForBusinessUnit_WhenPassedInValidRequest_ReturnsFailure()
		{
			// Arrange
			var request = new EmployeeRequest { EmployeeId = 7212428, BusinessUnitId = 0, EmployerApprovalType = ApprovalType.BusinessUnit };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.DenyReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}

    [Test]
		public void DenyReferral_BusinessUnitWhenPassedMultipleDenialReasonsWithOtherReason_ReturnsSuccess()
		{
			// Arrange
			var request = new EmployeeRequest
			{
		    EmployeeId = 1157079,
		    BusinessUnitId = 1157080,
				EmployerApprovalType = ApprovalType.BusinessUnit,
				ReferralStatusReasons = new List<KeyValuePair<long, string>>()
				{
					new KeyValuePair<long, string>(5531231, null),
					new KeyValuePair<long, string>(5531236, null),
					new KeyValuePair<long, string>(5531238, null),
					new KeyValuePair<long, string>(5531240, "other text")
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.DenyReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			var employee = DataCoreRepository.FindById<Employee>(1157079);
			var businessUnit = employee.Employer.BusinessUnits.FirstOrDefault();

			businessUnit.ShouldNotBeNull();

			if (businessUnit != null)
			{
				var savedReason = businessUnit.BusinessUnitReferralStatusReasons.ToList();

				savedReason.Count.ShouldEqual(4);
				savedReason.Count(x => x.BusinessUnitId.Equals(businessUnit.Id)).ShouldEqual(4);

				savedReason.Count(x => x.ReasonId.Equals(5531231)).ShouldEqual(1);
				savedReason.Count(x => x.ReasonId.Equals(5531236)).ShouldEqual(1);
				savedReason.Count(x => x.ReasonId.Equals(5531238)).ShouldEqual(1);
				savedReason.Count(x => x.OtherReasonText == null).ShouldEqual(3);
				savedReason.Count(x => x.ReasonId.Equals(5531240)).ShouldEqual(1);
				savedReason.Count(x => x.OtherReasonText == "other text" && x.ReasonId.Equals(5531240)).ShouldEqual(1);
				savedReason.Count(x => x.EntityType.Equals(EntityTypes.BusinessUnit)).ShouldEqual(4);
				savedReason.Count(x => x.ApprovalStatus.Equals(ApprovalStatuses.Rejected)).ShouldEqual(4);
			}
		}

		[Test]
		public void DenyReferral_BusinessUnitWhenPassedOtherReason_ReturnsSuccess()
		{
			// Arrange
			var request = new EmployeeRequest
			{
				EmployeeId = 1157079,
				BusinessUnitId = 1157080,
				EmployerApprovalType = ApprovalType.BusinessUnit,
				ReferralStatusReasons = new List<KeyValuePair<long, string>>()
				{
					new KeyValuePair<long, string>(5531240, "other text")
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.DenyReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			var employee = DataCoreRepository.FindById<Employee>(1157079);
			var businessUnit = employee.Employer.BusinessUnits.FirstOrDefault();

			businessUnit.ShouldNotBeNull();

			if (businessUnit != null)
			{
				var savedReason = businessUnit.BusinessUnitReferralStatusReasons.ToList();

				savedReason.Count.ShouldEqual(1);
				savedReason[0].BusinessUnitId.ShouldEqual(businessUnit.Id);
				savedReason[0].ReasonId.ShouldEqual(5531240);
				savedReason[0].OtherReasonText.ShouldEqual("other text");
				savedReason[0].EntityType.ShouldEqual(EntityTypes.BusinessUnit);
				savedReason[0].ApprovalStatus.ShouldEqual(ApprovalStatuses.Rejected);
			}
		}

		[Test]
		public void DenyReferral_BusinessUnitWhenPassedMultipleDenialReasonsWithInvalidReasonIdAndTextCombo_ReturnsFailure()
		{
			// Arrange
			var request = new EmployeeRequest
			{
				EmployeeId = 1157079,
				BusinessUnitId = 1157080,
				EmployerApprovalType = ApprovalType.BusinessUnit,
				ReferralStatusReasons = new List<KeyValuePair<long, string>>()
				{
					new KeyValuePair<long, string>(5531231, null),
					new KeyValuePair<long, string>(5531236, null),
					new KeyValuePair<long, string>(5531238, "Text is not valid with this reason ID")
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.DenyReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);

			var employee = DataCoreRepository.FindById<Employee>(1157079);
			var businessUnit = employee.Employer.BusinessUnits.FirstOrDefault();

			businessUnit.ShouldNotBeNull();

			if (businessUnit != null)
			{
				var savedReason = businessUnit.BusinessUnitReferralStatusReasons.ToList();

				savedReason.Count().ShouldEqual(0);
			}
		}

		[Test]
		public void DenyReferral_BusinessUnitWhenPassedMultipleDenialReasonsWithNoOtherTextForOtherReasonId_ReturnsFailure()
		{
			// Arrange
			var request = new EmployeeRequest
			{
				EmployeeId = 1157079,
				BusinessUnitId = 1157080,
				EmployerApprovalType = ApprovalType.BusinessUnit,
				ReferralStatusReasons = new List<KeyValuePair<long, string>>()
				{
					new KeyValuePair<long, string>(5531231, null),
					new KeyValuePair<long, string>(5531236, null),
					new KeyValuePair<long, string>(5531240, "")
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.DenyReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);

			var employee = DataCoreRepository.FindById<Employee>(1157079);
			var businessUnit = employee.Employer.BusinessUnits.FirstOrDefault();

			businessUnit.ShouldNotBeNull();

			if (businessUnit != null)
			{
				var savedReason = businessUnit.BusinessUnitReferralStatusReasons.ToList();

				savedReason.Count().ShouldEqual(0);
			}
		}

		[Test]
		public void DenyReferral_BusinessUnitWhenPassedMultipleDenialReasons_ReturnsSuccess()
		{
			// Arrange
			var request = new EmployeeRequest
			{
				EmployeeId = 1157079,
				BusinessUnitId = 1157080,
				EmployerApprovalType = ApprovalType.BusinessUnit,
				ReferralStatusReasons = new List<KeyValuePair<long, string>>()
				{
					new KeyValuePair<long, string>(5531231, null),
					new KeyValuePair<long, string>(5531236, null),
					new KeyValuePair<long, string>(5531238, null)
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.DenyReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			var employee = DataCoreRepository.FindById<Employee>(1157079);
			var businessUnit = employee.Employer.BusinessUnits.FirstOrDefault();

			businessUnit.ShouldNotBeNull();

			if (businessUnit != null)
			{
				var savedReason = businessUnit.BusinessUnitReferralStatusReasons.ToList();

				savedReason.Count.ShouldEqual(3);
				savedReason.Count(x => x.BusinessUnitId.Equals(businessUnit.Id)).ShouldEqual(3);

				savedReason.Count(x => x.ReasonId.Equals(5531231)).ShouldEqual(1);
				savedReason.Count(x => x.ReasonId.Equals(5531236)).ShouldEqual(1);
				savedReason.Count(x => x.ReasonId.Equals(5531238)).ShouldEqual(1);
				savedReason.Count(x => x.OtherReasonText == null).ShouldEqual(3);
				savedReason.Count(x => x.EntityType.Equals(EntityTypes.BusinessUnit)).ShouldEqual(3);
				savedReason.Count(x => x.ApprovalStatus.Equals(ApprovalStatuses.Rejected)).ShouldEqual(3);
			}
		}

		[Test]
		public void DenyReferral_BusinessUnitWhenPassedNoOtherTextForOtherReasonId_ReturnsFailure()
		{
			var reason = new KeyValuePair<long, string>(5531240, "");
			// Arrange
	    var request = new EmployeeRequest
	    {
		    EmployeeId = 1157079,
		    BusinessUnitId = 1157080,
		    EmployerApprovalType = ApprovalType.BusinessUnit,
		    ReferralStatusReasons = new List<KeyValuePair<long, string>>() {reason}
	    };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.DenyReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);

			var employee = DataCoreRepository.FindById<Employee>(1157079);
			var businessUnit = employee.Employer.BusinessUnits.FirstOrDefault();

			businessUnit.ShouldNotBeNull();

			if (businessUnit != null)
			{
				var savedReason = businessUnit.BusinessUnitReferralStatusReasons.ToList();

				savedReason.Count().ShouldEqual(0);
			}
		}

		[Test]
		public void DenyReferral_BusinessUnitWhenPassedInvalidReasonIdAndTextCombo_ReturnsFailure()
		{
			var reason = new KeyValuePair<long, string>(5531231, "Text is not valid with this reason ID");
			// Arrange
			var request = new EmployeeRequest
			{
				EmployeeId = 1157079,
				BusinessUnitId = 1157080,
				EmployerApprovalType = ApprovalType.BusinessUnit,
				ReferralStatusReasons = new List<KeyValuePair<long, string>>() { reason }
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.DenyReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);

			var employee = DataCoreRepository.FindById<Employee>(1157079);
			var businessUnit = employee.Employer.BusinessUnits.FirstOrDefault();

			businessUnit.ShouldNotBeNull();

			if (businessUnit != null)
			{
				var savedReason = businessUnit.BusinessUnitReferralStatusReasons.ToList();
				savedReason.Count().ShouldEqual(0);
			}
		}

		[Test]
		public void DenyReferral_BusinessUnitWhenPassedDenialReason_ReturnsSuccess()
		{
			var reason = new KeyValuePair<long, string>(5531231, null);
			// Arrange
			var request = new EmployeeRequest
			{
				EmployeeId = 1157079,
				BusinessUnitId = 1157080,
				EmployerApprovalType = ApprovalType.BusinessUnit,
				ReferralStatusReasons = new List<KeyValuePair<long, string>>() { reason }
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.DenyReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			var employee = DataCoreRepository.FindById<Employee>(1157079);
			var businessUnit = employee.Employer.BusinessUnits.FirstOrDefault();

			businessUnit.ShouldNotBeNull();

			if (businessUnit != null)
			{
				var savedReason = businessUnit.BusinessUnitReferralStatusReasons.ToList();

				savedReason.Count.ShouldEqual(1);
				savedReason[0].BusinessUnitId.ShouldEqual(businessUnit.Id);
				savedReason[0].ReasonId.ShouldEqual(5531231);
				savedReason[0].OtherReasonText.ShouldBeNull();
				savedReason[0].EntityType.ShouldEqual(EntityTypes.BusinessUnit);
				savedReason[0].ApprovalStatus.ShouldEqual(ApprovalStatuses.Rejected);
			}
		}

		#endregion

		#endregion

		#region HoldReferral Tests

		#region Approval type - Employer

		[Test]
		public void HoldReferralForEmployer_WhenPassedInValidRequest_ReturnsFailure()
		{
			// Arrange
			var request = new EmployeeRequest { EmployeeId = 0, BusinessUnitId = 7212429, EmployerApprovalType = ApprovalType.Employer };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.HoldReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}

		[Test]
		public void HoldReferral_EmployerWhenPassedMultipleDenialReasonsWithOtherReason_ReturnsSuccess()
		{
			// Arrange
			var request = new EmployeeRequest
			{
				EmployeeId = 7212428,
				BusinessUnitId = 7212429,
				EmployerApprovalType = ApprovalType.Employer,
				ReferralStatusReasons = new List<KeyValuePair<long, string>>()
				{
					new KeyValuePair<long, string>(5531255, null),
					new KeyValuePair<long, string>(5531257, null),
					new KeyValuePair<long, string>(5531258, null),
					new KeyValuePair<long, string>(5531260, "other text")
				},
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.HoldReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			var employee = DataCoreRepository.FindById<Employee>(7212428);
			var savedReason = employee.Employer.EmployerReferralStatusReasons.ToList();

			savedReason.Count.ShouldEqual(4);
			savedReason.Count(x => x.EmployerId.Equals(employee.EmployerId)).ShouldEqual(4);

			savedReason.Count(x => x.ReasonId.Equals(5531255)).ShouldEqual(1);
			savedReason.Count(x => x.ReasonId.Equals(5531257)).ShouldEqual(1);
			savedReason.Count(x => x.ReasonId.Equals(5531258)).ShouldEqual(1);
			savedReason.Count(x => x.OtherReasonText == null).ShouldEqual(3);
			savedReason.Count(x => x.ReasonId.Equals(5531260)).ShouldEqual(1);
			savedReason.Count(x => x.OtherReasonText == "other text" && x.ReasonId.Equals(5531260)).ShouldEqual(1);
			savedReason.Count(x => x.EntityType.Equals(EntityTypes.Employer)).ShouldEqual(4);
			savedReason.Count(x => x.ApprovalStatus.Equals(ApprovalStatuses.OnHold)).ShouldEqual(4);
		}

		[Test]
		public void HoldReferral_EmployerWhenPassedOtherReason_ReturnsSuccess()
		{
			// Arrange
			var request = new EmployeeRequest
			{
				EmployeeId = 7212428,
				BusinessUnitId = 7212429,
				EmployerApprovalType = ApprovalType.Employer,
				ReferralStatusReasons = new List<KeyValuePair<long, string>>()
				{
					new KeyValuePair<long, string>(5531260, "other text")
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.HoldReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			var employee = DataCoreRepository.FindById<Employee>(7212428);
			var savedReason = employee.Employer.EmployerReferralStatusReasons.ToList();

			savedReason.Count.ShouldEqual(1);
			savedReason[0].EmployerId.ShouldEqual(employee.EmployerId);
			savedReason[0].ReasonId.ShouldEqual(5531260);
			savedReason[0].OtherReasonText.ShouldEqual("other text");
			savedReason[0].EntityType.ShouldEqual(EntityTypes.Employer);
			savedReason[0].ApprovalStatus.ShouldEqual(ApprovalStatuses.OnHold);
		}

		[Test]
		public void HoldReferral_EmployerWhenPassedMultipleDenialReasonsWithInvalidReasonIdAndTextCombo_ReturnsFailure()
		{
			// Arrange
			var request = new EmployeeRequest
			{
				EmployeeId = 7212428,
				BusinessUnitId = 7212429,
				EmployerApprovalType = ApprovalType.Employer,
				ReferralStatusReasons = new List<KeyValuePair<long, string>>()
				{
					new KeyValuePair<long, string>(5531231, null),
					new KeyValuePair<long, string>(5531236, null),
					new KeyValuePair<long, string>(5531238, "Text is not valid with this reason ID")
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.HoldReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);

			var employee = DataCoreRepository.FindById<Employee>(7212428);
			var savedReason = employee.Employer.EmployerReferralStatusReasons.ToList();

			savedReason.Count().ShouldEqual(0);
		}

		[Test]
		public void HoldReferral_EmployerWhenPassedMultipleDenialReasonsWithNoOtherTextForOtherReasonId_ReturnsFailure()
		{
			// Arrange
			var request = new EmployeeRequest
			{
				EmployeeId = 7212428,
				BusinessUnitId = 7212429,
				EmployerApprovalType = ApprovalType.Employer,
				ReferralStatusReasons = new List<KeyValuePair<long, string>>()
				{
					new KeyValuePair<long, string>(5531257, null),
					new KeyValuePair<long, string>(5531258, null),
					new KeyValuePair<long, string>(5531260, "")
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.HoldReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);

			var employee = DataCoreRepository.FindById<Employee>(7212428);
			var savedReason = employee.Employer.EmployerReferralStatusReasons.ToList();

			savedReason.Count().ShouldEqual(0);
		}

		[Test]
		public void HoldReferral_EmployerWhenPassedMultipleDenialReasons_ReturnsSuccess()
		{
			// Arrange
			var request = new EmployeeRequest
			{
				EmployeeId = 7212428,
				BusinessUnitId = 7212429,
				EmployerApprovalType = ApprovalType.Employer,
				ReferralStatusReasons = new List<KeyValuePair<long, string>>()
				{
					new KeyValuePair<long, string>(5531231, null),
					new KeyValuePair<long, string>(5531236, null),
					new KeyValuePair<long, string>(5531238, null)
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.HoldReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			var employee = DataCoreRepository.FindById<Employee>(7212428);
			var savedReason = employee.Employer.EmployerReferralStatusReasons.ToList();

			savedReason.Count.ShouldEqual(3);
			savedReason.Count(x => x.EmployerId.Equals(employee.EmployerId)).ShouldEqual(3);

			savedReason.Count(x => x.ReasonId.Equals(5531231)).ShouldEqual(1);
			savedReason.Count(x => x.ReasonId.Equals(5531236)).ShouldEqual(1);
			savedReason.Count(x => x.ReasonId.Equals(5531238)).ShouldEqual(1);
			savedReason.Count(x => x.OtherReasonText == null).ShouldEqual(3);
			savedReason.Count(x => x.EntityType.Equals(EntityTypes.Employer)).ShouldEqual(3);
			savedReason.Count(x => x.ApprovalStatus.Equals(ApprovalStatuses.OnHold)).ShouldEqual(3);
		}

		[Test]
		public void HoldReferral_EmployerWhenPassedNoOtherTextForOtherReasonId_ReturnsFailure()
		{
			var reason = new KeyValuePair<long, string>(5531260, "");
			// Arrange
			var request = new EmployeeRequest
			{
				EmployeeId = 7212428,
				BusinessUnitId = 7212429,
				EmployerApprovalType = ApprovalType.Employer,
				ReferralStatusReasons = new List<KeyValuePair<long, string>>() { reason }
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.HoldReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);

			var employee = DataCoreRepository.FindById<Employee>(7212428);
			var savedReason = employee.Employer.EmployerReferralStatusReasons.ToList();

			savedReason.Count().ShouldEqual(0);
		}

		[Test]
		public void HoldReferral_EmployerWhenPassedInvalidReasonIdAndTextCombo_ReturnsFailure()
		{
			var reason = new KeyValuePair<long, string>(5531231, "Text is not valid with this reason ID");
			// Arrange
			var request = new EmployeeRequest
			{
				EmployeeId = 7212428,
				BusinessUnitId = 7212429,
				EmployerApprovalType = ApprovalType.Employer,
				ReferralStatusReasons = new List<KeyValuePair<long, string>>() { reason }
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.HoldReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);

			var employee = DataCoreRepository.FindById<Employee>(7212428);
			var savedReason = employee.Employer.EmployerReferralStatusReasons.ToList();

			savedReason.Count().ShouldEqual(0);
		}

		[Test]
		public void HoldReferral_EmployerWhenPassedDenialReason_ReturnsSuccess()
		{
			// Arrange
			var request = new EmployeeRequest
			{
				EmployeeId = 7212428,
				BusinessUnitId = 7212429,
				EmployerApprovalType = ApprovalType.Employer,
				ReferralStatusReasons = new List<KeyValuePair<long, string>>()
        {
          new KeyValuePair<long, string>(5531231, null)
        }
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.HoldReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.EmployerApprovalStatus.ShouldEqual(ApprovalStatuses.OnHold);

			var employee = DataCoreRepository.FindById<Employee>(7212428);
			var savedReason = employee.Employer.EmployerReferralStatusReasons.ToList();

			savedReason.Count.ShouldEqual(1);
			savedReason[0].EmployerId.ShouldEqual(employee.EmployerId);
			savedReason[0].ReasonId.ShouldEqual(5531231);
			savedReason[0].OtherReasonText.ShouldBeNull();
			savedReason[0].EntityType.ShouldEqual(EntityTypes.Employer);
			savedReason[0].ApprovalStatus.ShouldEqual(ApprovalStatuses.OnHold);
		}

		#endregion

		#region Approval type - Hiring Manager

		[Test]
		public void HoldReferralForHiringManager_WhenPassedInValidRequest_ReturnsFailure()
		{
			// Arrange
			var request = new EmployeeRequest { EmployeeId = 0, BusinessUnitId = 0, EmployerApprovalType = ApprovalType.HiringManager };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.HoldReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);

			var employee = DataCoreRepository.FindById<Employee>(7212428);

			employee.ApprovalStatus.ShouldNotEqual(ApprovalStatuses.Rejected);
		}

		[Test]
		public void HoldReferral_HiringManagerWhenPassedMultipleDenialReasonsWithOtherReason_ReturnsSuccess()
		{
			// Arrange
			var request = new EmployeeRequest
			{
				EmployeeId = 1157079,
				BusinessUnitId = 1157080,
				EmployerApprovalType = ApprovalType.HiringManager,
				ReferralStatusReasons = new List<KeyValuePair<long, string>>()
				{
					new KeyValuePair<long, string>(5531274, null),
					new KeyValuePair<long, string>(5531273, null),
					new KeyValuePair<long, string>(5531275, null),
					new KeyValuePair<long, string>(5531278, "other text")
				},
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.HoldReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			var employee = DataCoreRepository.FindById<Employee>(1157079);
			var savedReason = employee.EmployeeReferralStatusReasons.ToList();
			employee.ApprovalStatus.ShouldEqual(ApprovalStatuses.OnHold);

			savedReason.Count.ShouldEqual(4);
			savedReason.Count(x => x.EmployeeId.Equals(employee.Id)).ShouldEqual(4);

			savedReason.Count(x => x.ReasonId.Equals(5531274)).ShouldEqual(1);
			savedReason.Count(x => x.ReasonId.Equals(5531273)).ShouldEqual(1);
			savedReason.Count(x => x.ReasonId.Equals(5531275)).ShouldEqual(1);
			savedReason.Count(x => x.OtherReasonText == null).ShouldEqual(3);
			savedReason.Count(x => x.ReasonId.Equals(5531278)).ShouldEqual(1);
			savedReason.Count(x => x.OtherReasonText == "other text" && x.ReasonId.Equals(5531278)).ShouldEqual(1);
			savedReason.Count(x => x.EntityType.Equals(EntityTypes.Staff)).ShouldEqual(4);
			savedReason.Count(x => x.ApprovalStatus.Equals(ApprovalStatuses.OnHold)).ShouldEqual(4);
		}

		[Test]
		public void OnHoldReferral_HiringManagerWhenPassedOtherReason_ReturnsSuccess()
		{
			// Arrange
			var request = new EmployeeRequest
			{
				EmployeeId = 1157079,
				BusinessUnitId = 1157080,
				EmployerApprovalType = ApprovalType.HiringManager,
				ReferralStatusReasons = new List<KeyValuePair<long, string>>()
				{
					new KeyValuePair<long, string>(5531278, "other text")
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.HoldReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			var employee = DataCoreRepository.FindById<Employee>(1157079);
			var savedReason = employee.EmployeeReferralStatusReasons.ToList();

			savedReason.Count.ShouldEqual(1);
			savedReason[0].EmployeeId.ShouldEqual(employee.Id);
			savedReason[0].ReasonId.ShouldEqual(5531278);
			savedReason[0].OtherReasonText.ShouldEqual("other text");
			savedReason[0].EntityType.ShouldEqual(EntityTypes.Staff);
			savedReason[0].ApprovalStatus.ShouldEqual(ApprovalStatuses.OnHold);
		}

		[Test]
		public void HoldReferral_HiringManagerWhenPassedMultipleDenialReasonsWithInvalidReasonIdAndTextCombo_ReturnsFailure()
		{
			// Arrange
			var request = new EmployeeRequest
			{
				EmployeeId = 1157079,
				BusinessUnitId = 1157080,
				EmployerApprovalType = ApprovalType.HiringManager,
				ReferralStatusReasons = new List<KeyValuePair<long, string>>()
				{
					new KeyValuePair<long, string>(5531275, null),
					new KeyValuePair<long, string>(5531277, null),
					new KeyValuePair<long, string>(5531274, "Text is not valid with this reason ID")
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.HoldReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);

			var employee = DataCoreRepository.FindById<Employee>(1157079);
			employee.ApprovalStatus.ShouldNotEqual(ApprovalStatuses.OnHold);

			var savedReason = employee.EmployeeReferralStatusReasons.ToList();

			savedReason.Count().ShouldEqual(0);
		}

		[Test]
		public void HoldReferral_HiringManagerWhenPassedMultipleDenialReasonsWithNoOtherTextForOtherReasonId_ReturnsFailure()
		{
			// Arrange
			var request = new EmployeeRequest
			{
				EmployeeId = 1157079,
				BusinessUnitId = 1157080,
				EmployerApprovalType = ApprovalType.HiringManager,
				ReferralStatusReasons = new List<KeyValuePair<long, string>>()
				{
					new KeyValuePair<long, string>(5531273, null),
					new KeyValuePair<long, string>(5531276, null),
					new KeyValuePair<long, string>(5531278, "")
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.HoldReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);

			var employee = DataCoreRepository.FindById<Employee>(1157079);
			employee.ApprovalStatus.ShouldNotEqual(ApprovalStatuses.OnHold);

			var savedReason = employee.EmployeeReferralStatusReasons.ToList();

			savedReason.Count().ShouldEqual(0);
		}

		[Test]
		public void HoldReferral_HiringManagerWhenPassedMultipleDenialReasons_ReturnsSuccess()
		{
			// Arrange
			var request = new EmployeeRequest
			{
				EmployeeId = 1157079,
				BusinessUnitId = 1157080,
				EmployerApprovalType = ApprovalType.HiringManager,
				ReferralStatusReasons = new List<KeyValuePair<long, string>>()
				{
					new KeyValuePair<long, string>(5531273, null),
					new KeyValuePair<long, string>(5531276, null),
					new KeyValuePair<long, string>(5531274, null)
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.HoldReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			var employee = DataCoreRepository.FindById<Employee>(1157079);
			employee.ApprovalStatus.ShouldEqual(ApprovalStatuses.OnHold);

			var savedReason = employee.EmployeeReferralStatusReasons.ToList();

			savedReason.Count.ShouldEqual(3);
			savedReason.Count(x => x.EmployeeId.Equals(employee.Id)).ShouldEqual(3);

			savedReason.Count(x => x.ReasonId.Equals(5531273)).ShouldEqual(1);
			savedReason.Count(x => x.ReasonId.Equals(5531276)).ShouldEqual(1);
			savedReason.Count(x => x.ReasonId.Equals(5531274)).ShouldEqual(1);
			savedReason.Count(x => x.OtherReasonText == null).ShouldEqual(3);
			savedReason.Count(x => x.EntityType.Equals(EntityTypes.Staff)).ShouldEqual(3);
			savedReason.Count(x => x.ApprovalStatus.Equals(ApprovalStatuses.OnHold)).ShouldEqual(3);
		}

		[Test]
		public void HoldReferral_HiringManagerWhenPassedNoOtherTextForOtherReasonId_ReturnsFailure()
		{
			var reason = new KeyValuePair<long, string>(5531278, "");
			// Arrange
			var request = new EmployeeRequest
			{
				EmployeeId = 1157079,
				BusinessUnitId = 1157080,
				EmployerApprovalType = ApprovalType.HiringManager,
				ReferralStatusReasons = new List<KeyValuePair<long, string>>() { reason }
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.HoldReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);

			var employee = DataCoreRepository.FindById<Employee>(1157079);
			employee.ApprovalStatus.ShouldNotEqual(ApprovalStatuses.OnHold);

			var savedReason = employee.EmployeeReferralStatusReasons.ToList();

			savedReason.Count().ShouldEqual(0);
		}

		[Test]
		public void HoldReferral_HiringManagerWhenPassedInvalidReasonIdAndTextCombo_ReturnsFailure()
		{
			var reason = new KeyValuePair<long, string>(5531276, "Text is not valid with this reason ID");
			// Arrange
			var request = new EmployeeRequest
			{
				EmployeeId = 1157079,
				BusinessUnitId = 1157080,
				EmployerApprovalType = ApprovalType.HiringManager,
				ReferralStatusReasons = new List<KeyValuePair<long, string>>() { reason }
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.HoldReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);

			var employee = DataCoreRepository.FindById<Employee>(1157079);
			employee.ApprovalStatus.ShouldNotEqual(ApprovalStatuses.OnHold);

			var savedReason = employee.EmployeeReferralStatusReasons.ToList();

			savedReason.Count().ShouldEqual(0);
		}

		[Test]
		public void HoldReferral_HiringManagerWhenPassedDenialReason_ReturnsSuccess()
		{
			// Arrange
			var request = new EmployeeRequest
			{
				EmployeeId = 1157079,
				BusinessUnitId = 1157080,
				EmployerApprovalType = ApprovalType.HiringManager,
				ReferralStatusReasons = new List<KeyValuePair<long, string>>()
        {
          new KeyValuePair<long, string>(5531274, null)
        }
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.HoldReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			var employee = DataCoreRepository.FindById<Employee>(1157079);
			var savedReason = employee.EmployeeReferralStatusReasons.ToList();

			savedReason.Count.ShouldEqual(1);
			savedReason[0].EmployeeId.ShouldEqual(employee.Id);
			savedReason[0].ReasonId.ShouldEqual(5531274);
			savedReason[0].OtherReasonText.ShouldBeNull();
			savedReason[0].EntityType.ShouldEqual(EntityTypes.Staff);
			savedReason[0].ApprovalStatus.ShouldEqual(ApprovalStatuses.OnHold);
		}



		#endregion

		#region Approval type - Business Unit

		[Test]
		public void HoldReferralForBusinessUnit_WhenPassedInValidRequest_ReturnsFailure()
		{
			// Arrange
			var request = new EmployeeRequest { EmployeeId = 7212428, BusinessUnitId = 0, EmployerApprovalType = ApprovalType.BusinessUnit };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.DenyReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}

		[Test]
		public void HoldReferral_BusinessUnitWhenPassedMultipleDenialReasonsWithOtherReason_ReturnsSuccess()
		{
			// Arrange
			var request = new EmployeeRequest
			{
				EmployeeId = 1157079,
				BusinessUnitId = 1157080,
				EmployerApprovalType = ApprovalType.BusinessUnit,
				ReferralStatusReasons = new List<KeyValuePair<long, string>>()
				{
					new KeyValuePair<long, string>(5531255, null),
					new KeyValuePair<long, string>(5531257, null),
					new KeyValuePair<long, string>(5531259, null),
					new KeyValuePair<long, string>(5531260, "other text")
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.HoldReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			var employee = DataCoreRepository.FindById<Employee>(1157079);
			var businessUnit = employee.Employer.BusinessUnits.FirstOrDefault();

			businessUnit.ShouldNotBeNull();

			if (businessUnit != null)
			{
				var savedReason = businessUnit.BusinessUnitReferralStatusReasons.ToList();

				savedReason.Count.ShouldEqual(4);
				savedReason.Count(x => x.BusinessUnitId.Equals(businessUnit.Id)).ShouldEqual(4);

				savedReason.Count(x => x.ReasonId.Equals(5531255)).ShouldEqual(1);
				savedReason.Count(x => x.ReasonId.Equals(5531257)).ShouldEqual(1);
				savedReason.Count(x => x.ReasonId.Equals(5531259)).ShouldEqual(1);
				savedReason.Count(x => x.OtherReasonText == null).ShouldEqual(3);
				savedReason.Count(x => x.ReasonId.Equals(5531260)).ShouldEqual(1);
				savedReason.Count(x => x.OtherReasonText == "other text" && x.ReasonId.Equals(5531260)).ShouldEqual(1);
				savedReason.Count(x => x.EntityType.Equals(EntityTypes.BusinessUnit)).ShouldEqual(4);
				savedReason.Count(x => x.ApprovalStatus.Equals(ApprovalStatuses.OnHold)).ShouldEqual(4);
			}
		}

		[Test]
		public void HoldReferral_BusinessUnitWhenPassedOtherReason_ReturnsSuccess()
		{
			// Arrange
			var request = new EmployeeRequest
			{
				EmployeeId = 1157079,
				BusinessUnitId = 1157080,
				EmployerApprovalType = ApprovalType.BusinessUnit,
				ReferralStatusReasons = new List<KeyValuePair<long, string>>()
				{
					new KeyValuePair<long, string>(5531260, "other text")
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.HoldReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			var employee = DataCoreRepository.FindById<Employee>(1157079);
			var businessUnit = employee.Employer.BusinessUnits.FirstOrDefault();

			businessUnit.ShouldNotBeNull();

			if (businessUnit != null)
			{
				var savedReason = businessUnit.BusinessUnitReferralStatusReasons.ToList();

				savedReason.Count.ShouldEqual(1);
				savedReason[0].BusinessUnitId.ShouldEqual(businessUnit.Id);
				savedReason[0].ReasonId.ShouldEqual(5531260);
				savedReason[0].OtherReasonText.ShouldEqual("other text");
				savedReason[0].EntityType.ShouldEqual(EntityTypes.BusinessUnit);
				savedReason[0].ApprovalStatus.ShouldEqual(ApprovalStatuses.OnHold);
			}
		}

		[Test]
		public void HoldReferral_BusinessUnitWhenPassedMultipleReasonsWithInvalidReasonIdAndTextCombo_ReturnsFailure()
		{
			// Arrange
			var request = new EmployeeRequest
			{
				EmployeeId = 1157079,
				BusinessUnitId = 1157080,
				EmployerApprovalType = ApprovalType.BusinessUnit,
				ReferralStatusReasons = new List<KeyValuePair<long, string>>()
				{
					new KeyValuePair<long, string>(5531258, null),
					new KeyValuePair<long, string>(5531255, null),
					new KeyValuePair<long, string>(5531259, "Text is not valid with this reason ID")
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.HoldReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);

			var employee = DataCoreRepository.FindById<Employee>(1157079);
			var businessUnit = employee.Employer.BusinessUnits.FirstOrDefault();

			businessUnit.ShouldNotBeNull();

			if (businessUnit != null)
			{
				var savedReason = businessUnit.BusinessUnitReferralStatusReasons.ToList();

				savedReason.Count().ShouldEqual(0);
			}
		}

		[Test]
		public void HoldReferral_BusinessUnitWhenPassedMultipleDenialReasonsWithNoOtherTextForOtherReasonId_ReturnsFailure()
		{
			// Arrange
			var request = new EmployeeRequest
			{
				EmployeeId = 1157079,
				BusinessUnitId = 1157080,
				EmployerApprovalType = ApprovalType.BusinessUnit,
				ReferralStatusReasons = new List<KeyValuePair<long, string>>()
				{
					new KeyValuePair<long, string>(5531256, null),
					new KeyValuePair<long, string>(5531258, null),
					new KeyValuePair<long, string>(5531260, "")
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.HoldReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);

			var employee = DataCoreRepository.FindById<Employee>(1157079);
			var businessUnit = employee.Employer.BusinessUnits.FirstOrDefault();

			businessUnit.ShouldNotBeNull();

			if (businessUnit != null)
			{
				var savedReason = businessUnit.BusinessUnitReferralStatusReasons.ToList();

				savedReason.Count().ShouldEqual(0);
			}
		}

		[Test]
		public void HoldReferral_BusinessUnitWhenPassedMultipleDenialReasons_ReturnsSuccess()
		{
			// Arrange
			var request = new EmployeeRequest
			{
				EmployeeId = 1157079,
				BusinessUnitId = 1157080,
				EmployerApprovalType = ApprovalType.BusinessUnit,
				ReferralStatusReasons = new List<KeyValuePair<long, string>>()
				{
					new KeyValuePair<long, string>(5531255, null),
					new KeyValuePair<long, string>(5531257, null),
					new KeyValuePair<long, string>(5531259, null)
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.HoldReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			var employee = DataCoreRepository.FindById<Employee>(1157079);
			var businessUnit = employee.Employer.BusinessUnits.FirstOrDefault();

			businessUnit.ShouldNotBeNull();

			if (businessUnit != null)
			{
				var savedReason = businessUnit.BusinessUnitReferralStatusReasons.ToList();

				savedReason.Count.ShouldEqual(3);
				savedReason.Count(x => x.BusinessUnitId.Equals(businessUnit.Id)).ShouldEqual(3);

				savedReason.Count(x => x.ReasonId.Equals(5531255)).ShouldEqual(1);
				savedReason.Count(x => x.ReasonId.Equals(5531257)).ShouldEqual(1);
				savedReason.Count(x => x.ReasonId.Equals(5531259)).ShouldEqual(1);
				savedReason.Count(x => x.OtherReasonText == null).ShouldEqual(3);
				savedReason.Count(x => x.EntityType.Equals(EntityTypes.BusinessUnit)).ShouldEqual(3);
				savedReason.Count(x => x.ApprovalStatus.Equals(ApprovalStatuses.OnHold)).ShouldEqual(3);
			}
		}

		[Test]
		public void HoldReferral_BusinessUnitWhenPassedNoOtherTextForOtherReasonId_ReturnsFailure()
		{
			var reason = new KeyValuePair<long, string>(5531260, "");
			// Arrange
			var request = new EmployeeRequest
			{
				EmployeeId = 1157079,
				BusinessUnitId = 1157080,
				EmployerApprovalType = ApprovalType.BusinessUnit,
				ReferralStatusReasons = new List<KeyValuePair<long, string>>() { reason }
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.HoldReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);

			var employee = DataCoreRepository.FindById<Employee>(1157079);
			var businessUnit = employee.Employer.BusinessUnits.FirstOrDefault();

			businessUnit.ShouldNotBeNull();

			if (businessUnit != null)
			{
				var savedReason = businessUnit.BusinessUnitReferralStatusReasons.ToList();

				savedReason.Count().ShouldEqual(0);
			}
		}

		[Test]
		public void HoldReferral_BusinessUnitWhenPassedInvalidReasonIdAndTextCombo_ReturnsFailure()
		{
			var reason = new KeyValuePair<long, string>(5531257, "Text is not valid with this reason ID");
			// Arrange
			var request = new EmployeeRequest
			{
				EmployeeId = 1157079,
				BusinessUnitId = 1157080,
				EmployerApprovalType = ApprovalType.BusinessUnit,
				ReferralStatusReasons = new List<KeyValuePair<long, string>>() { reason }
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.HoldReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);

			var employee = DataCoreRepository.FindById<Employee>(1157079);
			var businessUnit = employee.Employer.BusinessUnits.FirstOrDefault();

			businessUnit.ShouldNotBeNull();

			if (businessUnit != null)
			{
				var savedReason = businessUnit.BusinessUnitReferralStatusReasons.ToList();
				savedReason.Count().ShouldEqual(0);
			}
		}

		[Test]
		public void HoldReferral_BusinessUnitWhenPassedReason_ReturnsSuccess()
		{
			var reason = new KeyValuePair<long, string>(5531256, null);
			// Arrange
			var request = new EmployeeRequest
			{
				EmployeeId = 1157079,
				BusinessUnitId = 1157080,
				EmployerApprovalType = ApprovalType.BusinessUnit,
				ReferralStatusReasons = new List<KeyValuePair<long, string>>() { reason }
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.HoldReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			var employee = DataCoreRepository.FindById<Employee>(1157079);
			var businessUnit = employee.Employer.BusinessUnits.FirstOrDefault();

			businessUnit.ShouldNotBeNull();

			if (businessUnit != null)
			{
				var savedReason = businessUnit.BusinessUnitReferralStatusReasons.ToList();

				savedReason.Count.ShouldEqual(1);
				savedReason[0].BusinessUnitId.ShouldEqual(businessUnit.Id);
				savedReason[0].ReasonId.ShouldEqual(5531256);
				savedReason[0].OtherReasonText.ShouldBeNull();
				savedReason[0].EntityType.ShouldEqual(EntityTypes.BusinessUnit);
				savedReason[0].ApprovalStatus.ShouldEqual(ApprovalStatuses.OnHold);
			}
		}

		#endregion
		#endregion

		[Test]
		public void CreateHiringManagerHomepageAlert_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new CreateHiringManagerHomepageAlertRequest
			{
				HomepageAlert =
					new HiringManagerHomepageAlertView
					{
						Message = "Unit test message",
						ExpiryDate = DateTime.Now.AddDays(7),
						IsSystemAlert = true
					}
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.CreateHiringManagerHomepageAlert(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}
	}
}
