﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using Focus.Core.Criteria.CandidateSearch;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Data.Core.Entities;
using Focus.UnitTests.Core;
using Framework.Core;
using Moq;
using NUnit.Framework;

using Focus.Core;
using Focus.Core.Criteria;
using Focus.Core.Criteria.Candidate;
using Focus.Core.Criteria.CandidateApplication;
using Focus.Core.Criteria.JobSeeker;
using Focus.Core.Messages.AccountService;
using Focus.Core.Messages.CandidateService;
using Focus.Core.Messages.ResumeService;
using Focus.Core.Views;
using Focus.Services.ServiceImplementations;
using Framework.Testing.NUnit;
using Focus.UnitTests.FakeHelpers;

using AcknowledgementType = Focus.Core.Messages.AcknowledgementType;
using AssignJobSeekerToAdminRequest = Focus.Core.Messages.CandidateService.AssignJobSeekerToAdminRequest;
using SchoolStatus = Focus.Core.SchoolStatus;

#endregion

namespace Focus.UnitTests.Services
{
	internal enum JobSeekerDenialReasons
	{
		DidNotMeetEducationRequirements = 5531242,
		DidNotMeetExperienceRequirements = 5531243,
		DidNotMeetWorkOrEducationRequirements = 5531244,
		DidNotMeetLicensingRequirements = 5531245,
		DidNotMeetAgeRequirements = 5531246,
		AlienRegistrationInfoNeedsUpdating = 5531247,
		DidNotMeetSkillRequirements = 5531248,
		Other = 5531249
	}

	[TestFixture]
	public class CandidateServiceTests : TestFixtureBase
	{
		private CandidateService _service;
		private TestEmailHelper _emailHelper;

		[SetUp]
		public override void SetUp()
		{
			base.SetUp();

			SetUpCacheProvider();
			SetUpMockLogProvider();
			_emailHelper = (TestEmailHelper)RuntimeContext.Helpers.Email;
			_emailHelper.SentEmails.Clear();

			_service = new CandidateService(RuntimeContext);
		}

		private void CheckSentEmails(ICollection<CandidateEmailView> emails)
		{
			var index = 0;
			_emailHelper.SentEmails.Count.ShouldEqual(emails.Count);
			foreach (var email in emails)
			{
				var sentEmail = _emailHelper.SentEmails[index++];
				sentEmail.To.ToString().ShouldEqual(email.To);
				if (email.BccRequestor)
				{
					email.SenderAddress.ShouldEqual(sentEmail.Bcc.ToString());
				}
				else
				{
					sentEmail.Bcc.Count.ShouldEqual(0);
				}
				sentEmail.From.ToString().ShouldEqual(email.SenderAddress);
				sentEmail.Body.ShouldEqual( email.Body );
				sentEmail.Subject.ShouldEqual( email.Subject );
			}
		}

		[Test]
		public void SendCandidateEmails_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new SendCandidateEmailsRequest
			{
				Emails = new List<CandidateEmailView>
				{
					new CandidateEmailView
					{
						PersonId = 1093748,
						CandidateName = "James Cameron",
                        To ="motionpics@burning-glass.com",
						//SendCandidateEmails call should set To = "motionpics@burning-glass.com",
						Subject = "Unit Test",
						Body = "Email body"
					}
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			request.Emails[0].SenderAddress = request.UserContext.EmailAddress;

			// Act
			var response = _service.SendCandidateEmails(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			request.Emails[0].To.ShouldEqual( "motionpics@burning-glass.com" );
			//CheckSentEmails(request.Emails);
		}

		[Test]
		public void SendDuplicateCandidateEmailsSendCopy_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new SendCandidateEmailsRequest
			{
				Emails = new List<CandidateEmailView>
				{
					new CandidateEmailView
					{
						PersonId = 1093748,
						CandidateName = "James Cameron",
                        To="motionpics@burning-glass.com",
						//SendCandidateEmails call should set To = "motionpics@burning-glass.com",
						Subject = "Unit Test",
						Body = "Email body"
					},
					new CandidateEmailView
					{
						PersonId = 1091966,
						CandidateName = "Steven Spielberg",
                        To="waltdisney@burning-glass.com",
						//SendCandidateEmails call should set To = "waltdisney@burning-glass.com",
						Subject = "Unit Test",
						Body = "Email body"
					}
				},
				SendCopy = true
			};
			request.Prepare( MockClientTag, MockSessionId, MockUserData, MockCulture );
			request.Emails[0].SenderAddress = request.UserContext.EmailAddress;
			request.Emails[1].SenderAddress = request.UserContext.EmailAddress;

			// Act
			var response = _service.SendCandidateEmails( request );

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual( AcknowledgementType.Success );
			request.Emails[0].To.ShouldEqual( "motionpics@burning-glass.com" );
			request.Emails[1].To.ShouldEqual( "waltdisney@burning-glass.com" );
			request.Emails.Add( new CandidateEmailView
			{
				To = request.UserContext.EmailAddress,
				Subject = "Unit Test",
				Body = "<p>Sent to: motionpics@burning-glass.com; waltdisney@burning-glass.com</p>Email body",
				SenderAddress = request.UserContext.EmailAddress
			} );
			//CheckSentEmails( request.Emails );
		}

		[Test]
		public void SendCandidateEmails_WithValidRequestOneEmailSendCopyAndUseOfficeAddressForSendMessagesTrue_ReturnsSuccess()
		{
			MockAppSettings.UseOfficeAddressToSendMessages = true;

			// Arrange
			var request = new SendCandidateEmailsRequest
			{
				Emails = new List<CandidateEmailView>
				{
					new CandidateEmailView
					{
						PersonId = 1093748,
						CandidateName = "James Cameron",
                        To="motionpics@burning-glass.com",
						//SendCandidateEmails call should set To = "motionpics@burning-glass.com",
						Subject = "Unit Test",
						Body = "Email body"
					}
				},
				SendCopy = true
			};

			MockUserData.PersonId = 1093026;
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			request.Emails[0].SenderAddress = RuntimeContext.Helpers.Office.GetHiringManagerOfficeEmail( Convert.ToInt64( request.UserContext.PersonId ) );

			// Act
			var response = _service.SendCandidateEmails(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			request.Emails[0].To.ShouldEqual( "motionpics@burning-glass.com" );

			request.Emails.Add( new CandidateEmailView
			{
				To = request.Emails[0].SenderAddress,
				Subject = "Unit Test",
				Body = "<p>Sent to: motionpics@burning-glass.com</p>Email body",
				SenderAddress = request.Emails[0].SenderAddress
			} );

			//CheckSentEmails( request.Emails );

			MockAppSettings.UseOfficeAddressToSendMessages = false;
		}

		[Test]
		public void SendCandidateEmails_WithValidRequestOneEmailSendCopyAndUseOfficeAddressForSendMessagesFalse_ReturnsSuccess()
		{
			MockAppSettings.UseOfficeAddressToSendMessages = false; // False is the default

			// Arrange
			var request = new SendCandidateEmailsRequest
			{
				Emails = new List<CandidateEmailView>
				{
					new CandidateEmailView
					{
						PersonId = 1093748,
						CandidateName = "James Cameron",
                        To="motionpics@burning-glass.com",
						//SendCandidateEmails call should set To = "motionpics@burning-glass.com",
						Subject = "Unit Test",
						Body = "Email body"
					}
				},
				SendCopy = true
			};

			MockUserData.PersonId = 1093026;
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			request.Emails[0].SenderAddress = request.UserContext.EmailAddress;

			// Act
			var response = _service.SendCandidateEmails(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			request.Emails[0].To.ShouldEqual( "motionpics@burning-glass.com" );

			request.Emails.Add( new CandidateEmailView
			{
				To = request.UserContext.EmailAddress,
				Subject = "Unit Test",
				Body = "<p>Sent to: motionpics@burning-glass.com</p>Email body",
				SenderAddress = request.UserContext.EmailAddress
			} );

			//CheckSentEmails( request.Emails );

			MockAppSettings.UseOfficeAddressToSendMessages = false;
		}

		[Test]
		public void SendCandidateEmails_WithValidRequestOneEmailDoNotSendCopyAndUseOfficeAddressForSendMessagesFalse_ReturnsSuccess()
		{
			MockAppSettings.UseOfficeAddressToSendMessages = false; // False is the default

			// Arrange
			var request = new SendCandidateEmailsRequest
			{
				Emails = new List<CandidateEmailView>
				{
					new CandidateEmailView
					{
						PersonId = 1093748,
						CandidateName = "James Cameron",
                        To="motionpics@burning-glass.com",
						//SendCandidateEmails call should set To = "motionpics@burning-glass.com",
						Subject = "Unit Test",
						Body = "Email body"
					}
				},
				SendCopy = false
			};

			MockUserData.PersonId = 1093026;
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			request.Emails[0].SenderAddress = request.UserContext.EmailAddress;

			// Act
			var response = _service.SendCandidateEmails(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			request.Emails[0].To.ShouldEqual( "motionpics@burning-glass.com" );
			//CheckSentEmails( request.Emails );

			MockAppSettings.UseOfficeAddressToSendMessages = false;
		}

		[Test]
		public void SendCandidateEmails_WithValidRequestOneEmailDoNotSendCopyAndUseOfficeAddressForSendMessagesTrue_ReturnsSuccess()
		{
			MockAppSettings.UseOfficeAddressToSendMessages = true;

			// Arrange
			var request = new SendCandidateEmailsRequest
			{
				Emails = new List<CandidateEmailView>
				{
					new CandidateEmailView
					{
						PersonId = 1093748,
						CandidateName = "James Cameron",
                        To="motionpics@burning-glass.com",
						//SendCandidateEmails call should set To = "motionpics@burning-glass.com",
						Subject = "Unit Test",
						Body = "Email body"
					}
				},
				SendCopy = false
			};

			MockUserData.PersonId = 1093026;
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			request.Emails[0].SenderAddress = request.UserContext.EmailAddress;

			// Act
			var response = _service.SendCandidateEmails(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			request.Emails[0].To.ShouldEqual( "motionpics@burning-glass.com" );
			//CheckSentEmails( request.Emails );

			MockAppSettings.UseOfficeAddressToSendMessages = false;
		}

		[Test]
		public void GetResume_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new GetResumeRequest {ResumeId = 1089005, PersonId = 1089001};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetResume(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void GetResumes_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new GetResumesRequest {PersonId = 1087041};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetResumes(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void GetJobSeekerReferralView_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new JobSeekerReferralViewRequest
			{
				Criteria =
						new JobSeekerReferralCriteria {BusinessUnitId = 1088857, FetchOption = CriteriaBase.FetchOptions.PagedList}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetJobSeekerReferralView(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void GetJobSeekerReferralView_Pending_ReturnsSuccess()
		{
			// Arrange
			var request = new JobSeekerReferralViewRequest
			{
				Criteria =
					new JobSeekerReferralCriteria { ApprovalStatus = ApprovalStatuses.WaitingApproval, FetchOption = CriteriaBase.FetchOptions.PagedList }
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetJobSeekerReferralView(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.ReferralViewsPaged.Count.ShouldBeGreater(0);
		}

		[Test]
		public void GetJobSeekerReferralView_Denied_ReturnsSuccess()
		{
			// Arrange
			var request = new JobSeekerReferralViewRequest
			{
				Criteria =
					new JobSeekerReferralCriteria { ApprovalStatus = ApprovalStatuses.Rejected, FetchOption = CriteriaBase.FetchOptions.PagedList }
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetJobSeekerReferralView(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.ReferralViewsPaged.Count.ShouldBeGreater(0);
		}

		[Test]
		public void GetJobSeekerReferralView_Veteran_ReturnsSuccess()
		{
			// Arrange
			var request = new JobSeekerReferralViewRequest
			{
				Criteria =
					new JobSeekerReferralCriteria { JobSeekerType = VeteranFilterTypes.Veteran, FetchOption = CriteriaBase.FetchOptions.PagedList }
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetJobSeekerReferralView(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.ReferralViewsPaged.Count.ShouldBeGreater(0);
		}

		[Test]
		public void GetJobSeekerReferralView_NonVeteran_ReturnsSuccess()
		{
			// Arrange
			var request = new JobSeekerReferralViewRequest
			{
				Criteria =
					new JobSeekerReferralCriteria { JobSeekerType = VeteranFilterTypes.NonVeteran, FetchOption = CriteriaBase.FetchOptions.PagedList }
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetJobSeekerReferralView(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.ReferralViewsPaged.Count.ShouldBeGreater(0);
		}

		[Test]
		public void GetJobSeekerReferralView_Office2_ReturnsSuccess()
		{
			// Arrange
			var request = new JobSeekerReferralViewRequest
			{
				Criteria =
					new JobSeekerReferralCriteria { OfficeIds = new List<long?>{2}, FetchOption = CriteriaBase.FetchOptions.PagedList }
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetJobSeekerReferralView(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.ReferralViewsPaged.Count.ShouldBeGreater(0);
		}

		[Test]
		public void GetJobSeekerReferralView_AssignedTo_ReturnsSuccess()
		{
			// Arrange
			var request = new JobSeekerReferralViewRequest
			{
				Criteria =
					new JobSeekerReferralCriteria { OfficeIds = new List<long?> { 2 }, StaffMemberId = 1093026, FetchOption = CriteriaBase.FetchOptions.PagedList }
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetJobSeekerReferralView(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.ReferralViewsPaged.Count.ShouldBeGreater(0);
		}

		[Test]
		public void GetJobSeekerReferralView_ByJobSeekerOffice_ReturnsSuccess()
		{
			// Arrange
			var request = new JobSeekerReferralViewRequest
			{
				Criteria = new JobSeekerReferralCriteria { OfficeIds = new List<long?> { 2 }, StaffMemberId = 1093026, FetchOption = CriteriaBase.FetchOptions.PagedList }
			};
			request.Prepare( MockClientTag, MockSessionId, MockUserData, MockCulture );

			// Act
			var response = _service.GetJobSeekerReferralView( request );

			// Assert
			response.ShouldNotBeNull();
			response.ReferralViewsPaged.Count().ShouldEqual( 7 );
			response.Acknowledgement.ShouldEqual( AcknowledgementType.Success );
			response.ReferralViewsPaged.Count.ShouldBeGreater( 0 );
		}

		[Test]
		public void GetJobSeekerReferralView_ByJobOffice_ReturnsSuccess()
		{
			// Arrange
			var request = new JobSeekerReferralViewRequest
			{
				Criteria = new JobSeekerReferralCriteria { OfficeIds = new List<long?> { 4 }, StaffMemberId = 1093026, FetchOption = CriteriaBase.FetchOptions.PagedList }
			};
			request.Prepare( MockClientTag, MockSessionId, MockUserData, MockCulture );

			((FakeAppSettings)RuntimeContext.AppSettings).SetReferralRequestApprovalQueueOfficeFilterType( true );
			// Act
			var response = _service.GetJobSeekerReferralView( request );
			((FakeAppSettings)RuntimeContext.AppSettings).SetReferralRequestApprovalQueueOfficeFilterType( false );

			// Assert
			response.ShouldNotBeNull();
			response.ReferralViewsPaged.Count().ShouldEqual( 2 );
			response.Acknowledgement.ShouldEqual( AcknowledgementType.Success );
			response.ReferralViewsPaged.Count.ShouldBeGreater( 0 );
		}

		[Test]
		public void ApproveReferral_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new CandidateApplicationRequest { Id = 1091911 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ApproveReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			var logEvent = (from actionEvent in DataCoreRepository.ActionEvents
											join actionType in DataCoreRepository.ActionTypes
												on actionEvent.ActionTypeId equals actionType.Id
											where actionType.Name == "ApproveCandidateReferral"
												&& actionEvent.EntityId == 1091911
											orderby actionEvent.ActionedOn descending
											select actionEvent).FirstOrDefault();
			logEvent.ShouldNotBeNull();
		}

		[Test]
		public void ApproveReferral_WhenPassedValidRequestForAutoApproval_ReturnsSuccess()
		{
			// Arrange
			var request = new CandidateApplicationRequest { Id = 1091911, AutoApproval = true};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ApproveReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			var logEvent = (from actionEvent in DataCoreRepository.ActionEvents
											join actionType in DataCoreRepository.ActionTypes
												on actionEvent.ActionTypeId equals actionType.Id
											where actionType.Name == "AutoApprovedReferralBypass"
												&& actionEvent.EntityId == 1091911
											orderby actionEvent.ActionedOn descending
											select actionEvent).FirstOrDefault();
			logEvent.ShouldNotBeNull();
		}

		#region DenyReferral Tests

		[Test]
		public void DenyReferral_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var referralDeniedReasons = new List<KeyValuePair<long, string>>();
			referralDeniedReasons.Add( new KeyValuePair<long, string>( (int)JobSeekerDenialReasons.DidNotMeetAgeRequirements, null ) );
			var request = new CandidateApplicationRequest {Id = 1091911, ReferralStatusReasons = referralDeniedReasons};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.DenyReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void DenyReferral_WithMultipleReasons_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var referralDeniedReasons = new List<KeyValuePair<long, string>>
			{
				new KeyValuePair<long, string>( (int)JobSeekerDenialReasons.DidNotMeetAgeRequirements, null ),
				new KeyValuePair<long, string>( (int)JobSeekerDenialReasons.DidNotMeetSkillRequirements, null )
			};
			var request = new CandidateApplicationRequest { Id = 1091911, ReferralStatusReasons = referralDeniedReasons };
			request.Prepare( MockClientTag, MockSessionId, MockUserData, MockCulture );

			// Act
			var response = _service.DenyReferral( request );

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual( AcknowledgementType.Success );
		}

		[Test]
		public void DenyReferral_WithOtherReason_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var referralDeniedReasons = new List<KeyValuePair<long, string>>
			{
				new KeyValuePair<long, string>( (int)JobSeekerDenialReasons.Other, "He smells funny!" )
			};
			var request = new CandidateApplicationRequest { Id = 1091911, ReferralStatusReasons = referralDeniedReasons };
			request.Prepare( MockClientTag, MockSessionId, MockUserData, MockCulture );

			// Act
			var response = _service.DenyReferral( request );

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual( AcknowledgementType.Success );
		}

		[Test]
		public void DenyReferral_WithMultipleReasonsPlusOtherReason_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var referralDeniedReasons = new List<KeyValuePair<long, string>>
			{
				new KeyValuePair<long, string>( (int)JobSeekerDenialReasons.DidNotMeetAgeRequirements, null ),
				new KeyValuePair<long, string>( (int)JobSeekerDenialReasons.DidNotMeetSkillRequirements, null ),
				new KeyValuePair<long, string>( (int)JobSeekerDenialReasons.Other, "He smells funny!" )
			};
			var request = new CandidateApplicationRequest { Id = 1091911, ReferralStatusReasons = referralDeniedReasons };
			request.Prepare( MockClientTag, MockSessionId, MockUserData, MockCulture );

			// Act
			var response = _service.DenyReferral( request );

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual( AcknowledgementType.Success );
		}

		[Test]
		public void DenyReferral_WhenPassedInvalidRequest_ReturnsFailure()
		{
			// Arrange
			var referralDeniedReasons = new List<KeyValuePair<long, string>>();
			var request = new CandidateApplicationRequest { Id = 1091911, ReferralStatusReasons = referralDeniedReasons };
			request.Prepare( MockClientTag, MockSessionId, MockUserData, MockCulture );

			// Act
			var response = _service.DenyReferral( request );

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual( AcknowledgementType.Failure );
		}

		[Test]
		public void DenyReferral_WhenPassedInvalidRequest2_ReturnsFailure()
		{
			// Arrange
			var request = new CandidateApplicationRequest { Id = 1091911, ReferralStatusReasons = null };
			request.Prepare( MockClientTag, MockSessionId, MockUserData, MockCulture );

			// Act
			var response = _service.DenyReferral( request );

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual( AcknowledgementType.Failure );
		}

		[Test]
		public void DenyReferral_WithNoOtherReason_WhenPassedInvalidRequest_ReturnsFailure()
		{
			// Arrange
			var referralDeniedReasons = new List<KeyValuePair<long, string>>
			{
				new KeyValuePair<long, string>( (int)JobSeekerDenialReasons.Other, null )
			};
			var request = new CandidateApplicationRequest { Id = 1091911, ReferralStatusReasons = referralDeniedReasons };
			request.Prepare( MockClientTag, MockSessionId, MockUserData, MockCulture );

			// Act
			var response = _service.DenyReferral( request );

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual( AcknowledgementType.Failure );
		}

		[Test]
		public void DenyReferral_WithIncorrectOtherReason_WhenPassedInvalidRequest_ReturnsFailure()
		{
			// Arrange
			var referralDeniedReasons = new List<KeyValuePair<long, string>>
			{
				new KeyValuePair<long, string>( (int)JobSeekerDenialReasons.DidNotMeetAgeRequirements, "He smells funny!" )
			};
			var request = new CandidateApplicationRequest { Id = 1091911, ReferralStatusReasons = referralDeniedReasons };
			request.Prepare( MockClientTag, MockSessionId, MockUserData, MockCulture );

			// Act
			var response = _service.DenyReferral( request );

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual( AcknowledgementType.Failure );
		}

		#endregion


		[Test]
		public void HoldReferral_WhenPassedValidRequest_ReturnsSuccess()
		{
			const int applicationId = 1091911;

			// Arrange
			var request = new CandidateApplicationRequest
			{
				Id = applicationId,
				OnHoldReason = ApplicationOnHoldReasons.PutOnHoldByStaff,
				LockVersion = 0
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.HoldReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			var application = RuntimeContext.Repositories.Core.Applications.First(a => a.Id == applicationId);
			application.ApprovalStatus.ShouldEqual(ApprovalStatuses.OnHold);
			application.AutomaticallyOnHold.ShouldEqual(false);
		}

		[Test]
		public void HoldReferral_WhenPassedInvalidLoclRequest_ReturnsFailure()
		{
			const int applicationId = 1091911;

			// Arrange
			var request = new CandidateApplicationRequest
			{
				Id = applicationId, LockVersion = -1,
				OnHoldReason = ApplicationOnHoldReasons.PutOnHoldByStaff
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.HoldReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);

			var application = RuntimeContext.Repositories.Core.Applications.First(a => a.Id == applicationId);
			application.ApprovalStatus.ShouldEqual(ApprovalStatuses.WaitingApproval);
			application.AutomaticallyOnHold.ShouldBeNull();
		}

		[Test]
		public void AssignActivity_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new AssignActivityOrActionRequest
			{
				AdminId = 2,
				PersonId = 1000049,
				ActivityId = 1107788
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.AssignActivity(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void AssignActivityToMultipleUsers_WhenPassedValidRequest_ReturnsSuccess()
		{
			var jobSeeker = new JobSeekerView {PersonId = 1000049};
			var jobseeker2 = new JobSeekerView { PersonId = 1195044 };
			var jobseeker3 = new JobSeekerView { PersonId = 1178730 };

			var jobSeekers = new List<JobSeekerView> {jobSeeker, jobseeker2, jobseeker3};


			// Arrange
			var request = new AssignActivityOrActionRequest
			{
				AdminId = 2,
				Jobseekers = jobSeekers,
				ActivityId = 1107788,
				IgnoreClient = true
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.AssignActivityToMultipleUsers(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void GetJobSeekerLists_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new GetJobSeekerListsRequest {ListType = ListTypes.FlaggedJobSeeker};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetJobSeekerLists(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void AddJobSeekersToList_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new AddJobSeekersToListRequest
			{
					JobSeekers = new[] {new JobSeekerView {PersonId = 1000049}},
				ListType = ListTypes.FlaggedJobSeeker,
				ListId = 1121201,
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.AddJobSeekersToList(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void GetJobSeekersForList_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new GetJobSeekersForListRequest
			{
				Criteria = new JobSeekerListCriteria
				{
					FetchOption = CriteriaBase.FetchOptions.List,
					ListId = 1121201
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetJobSeekersForList(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void RemoveJobSeekersFromList_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new RemoveJobSeekersFromListRequest
			{
					JobSeekers = new[] {new JobSeekerView {PersonId = 1105733}},
				ListId = 1121201,
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.RemoveJobSeekersFromList(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void DeleteJobSeekerList_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new DeleteJobSeekerListRequest {ListId = 1121201};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.DeleteJobSeekerList(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void AssignJobSeekerToAdmin_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new AssignJobSeekerToAdminRequest
			{
				PersonId = 1113453,
				AdminId = 2
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.AssignJobSeekerToAdmin(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void ReferCandidate_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new ReferCandidateRequest { JobId = 1109568, PersonId = 1116920, ApplicationScore = 800 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ReferCandidate(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}
		
		[Test]
		public void GetCandidateSystemDefaults_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new GetCandidateSystemDefaultsRequest();
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetCandidateSystemDefaults(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void UpdateCandidateSystemDefaults_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new UpdateCandidateSystemDefaultsRequest
			{
				SystemDefaults = new CandidateSystemDefaultsView
				{
					AlertDefaults = new AlertDefaultsView
					{
						JobAlertsOption = JobAlertsOptions.OnButCanUnsubscribe,
						AlertFrequency = Frequencies.Weekly,
						SendOtherJobsEmail = true
					},
					ApprovalDefaults = new ApprovalDefaultsView
					{
						NewResumeApprovalsOption = NewResumeApprovalsOptions.ApprovalSystemAvailable,
						ResumeReferralApprovalsOption = ResumeReferralApprovalsOptions.SelfReferralRequires3StarPlusMatch
					},
					ColourDefaults = new ColourDefaultsView
					{
						LightColour = "bfbf00",
						DarkColour = "ff0000"
					}
				}
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.UpdateCandidateSystemDefaults(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void CreateJobSeekerHomepageAlert_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new CreateJobSeekerHomepageAlertRequest
			{
				HomepageAlerts =
					new List<JobSeekerHomepageAlertView>
					{
						new JobSeekerHomepageAlertView
									{CandidateId = 1108084, Message = "Unit test message", ExpiryDate = DateTime.Now.AddDays(7)}
					}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.CreateJobSeekerHomepageAlert(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void SavePersonNote_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new SavePersonNoteRequest {PersonId = 1108084, EmployerId = 1088910, Note = "Unit test note"};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.SavePersonNote(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void GetPersonNote_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new GetPersonNoteRequest {EmployerId = 1088910, PersonId = 1131366};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetPersonNote(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.PersonNote.Note.ShouldEqual("test");
		}

		[Test]
		public void UpdateApplicationStatus_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new UpdateApplicationStatusRequest
				{ApplicationId = 1094143, NewStatus = ApplicationStatusTypes.Interviewed};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.UpdateApplicationStatus(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void GetFlaggedCandidates_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new GetFlaggedCandidatesRequest {Criteria = new FlaggedCandidateCriteria {JobId = 1098749}};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetFlaggedCandidates(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void GetApplication_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new GetApplicationRequest {PersonId = 1091954, JobId = 1094495};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetApplication(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void GetApplicationViews_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new GetApplicationViewRequest { PersonId = 1091954 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetApplicationViews(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void GetApplicantResumeView_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new ResumeViewRequest {Criteria = new ResumeViewCriteria {ApplicationId = 1094552}};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetApplicantResumeView(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void GetApplicationStatusLogView_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new ApplicationStatusLogViewRequest
			{
				Criteria =
						new ApplicationStatusLogViewCriteria {ApplicationId = 1094552, FetchOption = CriteriaBase.FetchOptions.PagedList}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetApplicationStatusLogView(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void GetJobSeekerActivityView_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new JobSeekerActivityViewRequest {Criteria = new JobSeekerCriteria {JobSeekerId = 1091954}};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetJobSeekerActivityView(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void GetCandidate_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new GetCandidateRequest {PersonId = 1091954};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetCandidate(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Candidate.ShouldNotBeNull();
		}

		[Test]
		public void GetCandidate_WhenPassedValidRequestForPersonOnly_ReturnsSuccess()
		{
			// Arrange
      var request = new GetCandidateRequest { PersonId = 1091954, GetPersonRecord = true};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetCandidate(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Person.ShouldNotBeNull();
		}

		[Test]
		public void ResolveCandidateIssues_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new ResolveCandidateIssuesRequest
				{PersonId = 1098781, IssuesResolved = new List<CandidateIssueType> {CandidateIssueType.NotLoggingIn}};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ResolveCandidateIssues(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void FlagCandidateForFollowUp_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new FlagCandidateForFollowUpRequest {PersonId = 1098781};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.FlagCandidateForFollowUp(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void GetJobSeekerActivities_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new JobSeekerActivityActionViewRequest {Criteria = new JobSeekerCriteria {JobSeekerId = 1098781}};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetJobSeekerActivities(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void GetJobSeekerActivityUsers_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new JobSeekerActivityActionViewRequest {Criteria = new JobSeekerCriteria {JobSeekerId = 1098781}};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetJobSeekerActivityUsers(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.JobSeekerActivityActionViewUserList.Count.ShouldBeGreater(0);
		}

		[Test]
		public void GetCandidatesAndIssues_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new GetCandidatesAndIssuesRequest
				{Criteria = new JobSeekerCriteria {JobSeekerId = 1098781, FetchOption = CriteriaBase.FetchOptions.PagedList}};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetCandidatesAndIssues(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void GetStudentAlumniIssues_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new GetStudentAlumniIssuesRequest
				{Criteria = new JobSeekerCriteria {JobSeekerId = 1098781, FetchOption = CriteriaBase.FetchOptions.List}};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetStudentAlumniIssues(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void GetStudentAlumniIssues_WhenPassedValidRequestForCampus_ReturnsSuccess()
		{
			// Arrange
			var request = new GetStudentAlumniIssuesRequest
			{
				Criteria = new JobSeekerCriteria
				{
					UserType = 12,
					CampusId = 5944975,
					FetchOption = CriteriaBase.FetchOptions.List
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetStudentAlumniIssues(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.StudentAlumniIssues.ShouldNotBeNull();
			response.StudentAlumniIssues.Count.ShouldNotEqual(0);
		}

		[Test]
		public void RegisterFindJobsForSeeker_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new RegisterFindJobsForSeekerRequest {CandidatePersonId = 1087331};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.RegisterFindJobsForSeeker(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void RegisterHowToApply_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new RegisterHowToApplyRequest
				{LensPostingId = "ABC123_BB70751AA6FB419E8D45D072E6B9F52F", CandidatePersonId = 1087331};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.RegisterHowToApply(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success, 
				response.HasRaisedException
					? string.Concat("Test failed with exception: ", response.Exception.Message)
					: string.Concat("Test failed with message: ", response.Message));
		}

		[Test]
		public void RegisterSelfReferral_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new RegisterSelfReferralRequest { PostingId = 1084190, CandidatePersonId = 1087331 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.RegisterSelfReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void RegisterSelfReferral_WhenPassedActionTypeValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new RegisterSelfReferralRequest { PostingId = 1084190, CandidatePersonId = 1087331, ReferralActionType = ActionTypes.ExternalStaffReferral};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.RegisterSelfReferral(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void ShowNotWhatYourLookingFor_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new LogUIActionRequest {LensPostingId = "ABC123_BB70751AA6FB419E8D45D072E6B9F52F", PersonId = 1087330};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ShowNotWhatYourLookingFor(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void ViewJobDetails_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new LogUIActionRequest {LensPostingId = "ABC123_BB70751AA6FB419E8D45D072E6B9F52F", PersonId = 1087330};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ViewJobDetails(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void ToggleCandidateFlag_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new ToggleCandidateFlagRequest {PersonId = 1103531};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act	
			var response = _service.ToggleCandidateFlag(request);

			// Assert 
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void ToggleCandidateFlag_WhenPassedInValidCandidate_ReturnsFailure()
		{
			// Arrange
			var request = new ToggleCandidateFlagRequest {PersonId = 0};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act	
			var response = _service.ToggleCandidateFlag(request);

			// Assert 
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}

		[Test]
		public void GetResumeAsHtml_WhenPassedValidRequest_ReturnsSuccessWithData()
		{
			// Arrange 
			var request = new GetResumeAsHtmlRequest {PersonId = 1000049};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			var service = new CandidateService(RuntimeContext);

			// Act
			var response = service.GetResumeAsHtml(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.ResumeHtml.ShouldNotBeNull();
		}

		[Test]
		public void GetResumeAsHtml_WhenPassedInValidRequest_ReturnsFailure()
		{
			// Arrange 
			var request = new GetResumeAsHtmlRequest();
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			var service = new CandidateService(RuntimeContext);

			// Act
			var response = service.GetResumeAsHtml(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.ResumeHtml.ShouldBeNull();
		}

		[Test]
		public void GetResumeById_WhenPassedValidRequest_ReturnsSuccessWithData()
		{
			// Arrange 
			var request = new GetResumeRequest {ResumeId = 1086379};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			var service = new CandidateService(RuntimeContext);

			// Act
			var response = service.GetResumeById(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.ResumeDetails.ShouldNotBeNull();
		}

		[Test]
		public void GetResumeById_WhenPassedInValidRequest_ReturnsFailure()
		{
			// Arrange 
			var request = new GetResumeRequest();
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			var service = new CandidateService(RuntimeContext);

			// Act
			var response = service.GetResumeById(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.ResumeDetails.ShouldBeNull();
		}

		[Test]
		public void GetJobSeekerProfile_WhenPassedValidRequest_ReturnsSuccessWithData()
		{
			// Arrange 
			var request = new JobSeekerProfileRequest {JobSeekerId = 1000049};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			var service = new CandidateService(RuntimeContext);

			// Act
			var response = service.GetJobSeekerProfile(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.JobSeekerProfileView.ShouldNotBeNull();
		}

		[Test]
		public void GetJobSeekerProfile_WhenPassedInValidRequest_ReturnsFailure()
		{
			// Arrange 
			var request = new JobSeekerProfileRequest();
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			var service = new CandidateService(RuntimeContext);

			// Act
			var response = service.GetJobSeekerProfile(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.JobSeekerProfileView.ShouldBeNull();
		}

		[Test]
		public void SearchJobSeekers_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange 
			var criteria = new JobSeekerCriteria {Lastname = "fashoni"};
			var request = new JobSeekerSearchRequest {Criteria = criteria};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			var service = new CandidateService(RuntimeContext);

			// Act
			var response = service.SearchJobSeekers(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.SearchResults.ShouldNotBeNull();
		}

		[Test]
		public void GetCandidateOpenPositions_WhenPassedValidRequest_ReturnsSuccessWithData()
		{
			// Arrange 
			var request = new CandidateOpenPositionMatchesRequest {CandidateId = 1085521, EmployerId = 29441};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetOpenPositionJobs(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.CandidateOpenPositionMatches.Count.ShouldNotEqual(0);
		}

		[Test]
		public void IgnorePersonPostingMatch_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new IgnorePersonPostingMatchRequest {PostingId = 1092299, PersonId = 1085521};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act	
			var response = _service.IgnorePersonPostingMatch(request);

			// Assert 
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void AllowPersonPostingMatch_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var requestIgnore = new IgnorePersonPostingMatchRequest {PostingId = 1092299, PersonId = 1102287};
			requestIgnore.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			var requestAllow = new AllowPersonPostingMatchRequest {PostingId = 1092299, PersonId = 1102287};
			requestAllow.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act	
			var responseIgnore = _service.IgnorePersonPostingMatch(requestIgnore);

			// Assert
			responseIgnore.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			//Act
			var responseAllow = _service.AllowPersonPostingMatch(requestAllow);

			// Assert 
			responseAllow.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void UpdateAssignedStaffMember_WhenPassedSingleJobSeeker_ReturnsSuccess()
		{
			// Arrange
			var request = new UpdateAssignedStaffMemberRequest {JobSeekerIds = new List<long> {1000049}, PersonId = 1};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act	
			var jobSeekerResponse = _service.UpdateAssignedStaffMember(request);

			// Assert 
			jobSeekerResponse.ShouldNotBeNull();
			jobSeekerResponse.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			var person = DataCoreRepository.Persons.First(x => x.Id == 1000049);
			person.ShouldNotBeNull();
			person.AssignedToId.ShouldNotBeNull();
			person.AssignedToId.ShouldEqual(1);
		}

		[Test]
		public void UpdateAssignedStaffMember_WhenPassedMultipleJobSeekers_ReturnsSuccess()
		{
			// Arrange
			var request = new UpdateAssignedStaffMemberRequest {JobSeekerIds = new List<long> {1000049, 1088251}, PersonId = 1};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act	
			var jobSeekerResponse = _service.UpdateAssignedStaffMember(request);

			// Assert 
			jobSeekerResponse.ShouldNotBeNull();
			jobSeekerResponse.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			var person = DataCoreRepository.Persons.First(x => x.Id == 1000049);
			person.ShouldNotBeNull();
			person.AssignedToId.ShouldNotBeNull();
			person.AssignedToId.ShouldEqual(1);

			var person2 = DataCoreRepository.Persons.First(x => x.Id == 1088251);
			person2.ShouldNotBeNull();
			person2.AssignedToId.ShouldNotBeNull();
			person2.AssignedToId.ShouldEqual(1);
		}

		[Test]
		public void UpdateAssignedStaffMember_WhenPassedInvalidSingleJobSeeker_ReturnsFailure()
		{
			// Arrange
			var request = new UpdateAssignedStaffMemberRequest {JobSeekerIds = new List<long> {-1}, PersonId = 1};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act	
			var jobSeekerResponse = _service.UpdateAssignedStaffMember(request);

			// Assert 
			jobSeekerResponse.ShouldNotBeNull();
			jobSeekerResponse.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);

			var person = DataCoreRepository.Persons.FirstOrDefault(x => x.Id == -1);
			person.ShouldBeNull();
		}

		[Test]
		public void UpdateAssignedStaffMember_WhenPassedInvalidJobSeeker_ReturnsFailure()
		{
			// Arrange
			var request = new UpdateAssignedStaffMemberRequest {JobSeekerIds = new List<long> {-1, 1000049, -3}, PersonId = 1};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act	
			var jobSeekerResponse = _service.UpdateAssignedStaffMember(request);

			// Assert 
			jobSeekerResponse.ShouldNotBeNull();
			jobSeekerResponse.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);

			var person = DataCoreRepository.Persons.FirstOrDefault(x => x.Id == -1);
			person.ShouldBeNull();

			var person2 = DataCoreRepository.Persons.FirstOrDefault(x => x.Id == 1000049);
			person2.ShouldNotBeNull();
			person2.AssignedToId.ShouldBeNull();

			var person3 = DataCoreRepository.Persons.FirstOrDefault(x => x.Id == -3);
			person3.ShouldBeNull();
		}

		[Test]
		public void UpdateAssignedStaffMember_WhenPassedInvalidStaffMember_ReturnsSuccess()
		{
			// Arrange
			var request = new UpdateAssignedStaffMemberRequest {JobSeekerIds = new List<long> {1000049}, PersonId = -1};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act	
			var jobSeekerResponse = _service.UpdateAssignedStaffMember(request);

			// Assert 
			jobSeekerResponse.ShouldNotBeNull();
			jobSeekerResponse.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);

			var person = DataCoreRepository.Persons.First(x => x.Id == 1000049);
			person.ShouldNotBeNull();
			person.AssignedToId.ShouldBeNull();
		}

		[Test]
		public void RegisterJobPostingOfInterestSent_WhenPassedTalentJob_ReturnsSuccess()
		{
			// Arrange
			var request = new RegisterJobPostingOfInterestSentRequest
			{
				CandidatePersonId = 1178472,
				LensPostingId = "ABC123_A840CB1051D44F6497BFAD611EC7D487",
				Score = 123
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			var startCount = DataCoreRepository.Query<JobPostingOfInterestSent>().Count();

			// Act
			var response = _service.RegisterJobPostingOfInterestSent(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			var endCount = DataCoreRepository.Query<JobPostingOfInterestSent>().Count();
			endCount.ShouldBeGreater(startCount);
		}

		[Test]
		public void RegisterJobPostingOfInterestSent_WhenPassedTalentJobAsRecommendation_ReturnsSuccess()
		{
			// Arrange
			var request = new RegisterJobPostingOfInterestSentRequest
			{
				CandidatePersonId = 1178472,
				LensPostingId = "ABC123_A840CB1051D44F6497BFAD611EC7D487",
				Score = 123,
				IsRecommendation = true
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			var startCount = DataCoreRepository.Query<JobPostingOfInterestSent>().Count();

			// Act
			var response = _service.RegisterJobPostingOfInterestSent(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			var endCount = DataCoreRepository.Query<JobPostingOfInterestSent>().Count();
			endCount.ShouldBeGreater(startCount);

			// Act again
			request.IsRecommendation = true;
			response = _service.RegisterJobPostingOfInterestSent(request);
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			
			// Assert
			var record = DataCoreRepository.Query<JobPostingOfInterestSentView>().FirstOrDefault(p => p.LensPostingId == "ABC123_A840CB1051D44F6497BFAD611EC7D487");
			record.ShouldNotBeNull();
			if (record.IsNotNull())
				record.IsRecommendation.ShouldEqual(true);
		}

		[Test]
		public void RegisterJobPostingOfInterestSent_WhenPassedSpideredJob_ReturnsSuccess()
		{
			// Arrange
			var request = new RegisterJobPostingOfInterestSentRequest
			{
				CandidatePersonId = 1178472,
				LensPostingId = "careerjobs_A3CBB9BCF1E14738866D5A0EE91A33C3",
				Score = 123
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			var startCount = DataCoreRepository.Query<JobPostingOfInterestSent>().Count();

			// Act
			var response = _service.RegisterJobPostingOfInterestSent(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			var endCount = DataCoreRepository.Query<JobPostingOfInterestSent>().Count();
			endCount.ShouldBeGreater(startCount);
		}

		[Test]
		public void GetPostingsOfInterestReceived_WhenPassedValidData_ReturnsSuccess()
		{
			// Arrange
			var request = new GetPostingsOfInterestReceivedRequest();
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetPostingsOfInterestReceived(request);

			// Assert 
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.PostingsSent.ShouldNotBeNull();
		}

		[Test]
		public void GetPostingsOfInterestReceived_WhenPassedLensId_ReturnsSuccess()
		{
			// Arrange
			var request = new GetPostingsOfInterestReceivedRequest { PersonId = 1197307, LensPostingId = "ABC123_66B75E9150974D858E474C1447521319" };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetPostingsOfInterestReceived(request);

			// Assert 
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.PostingSent.ShouldNotBeNull();
			response.PostingSent.LensPostingId.ShouldEqual("ABC123_66B75E9150974D858E474C1447521319");
		}

		/// <summary>
		/// Marks the application viewed_ when passed valid data_ returns success.
		/// </summary>
		[Test]
		public void MarkApplicationViewed_WhenPassedValidData_ReturnsSuccess()
		{
			//Arrange
			var request = new MarkApplicationViewedRequest {JobId = 1124874, CandidateId = 1105733};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.MarkApplicationViewed(request);

			// Assert 
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void GetNewApplicantReferralView_WhenPassedValidData_ReturnsSuccess()
		{
			//Arrange
			var request = new NewApplicantReferralViewRequest
			{
					Criteria = new NewApplicantReferralViewCriteria {PersonId = 1085521, FetchOption = CriteriaBase.FetchOptions.PagedList}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetNewApplicantReferralView(request);

			// Assert 
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.NewApplicantReferralViewPaged.ShouldNotBeNull();
			response.NewApplicantReferralViewPaged.Count.ShouldBeGreater(0);
		}

		[Test]
		public void GetLatestSurvey_WhenPassedValidData_ReturnsSuccess()
		{
			//Arrange
			var request = new JobSeekerSurveyRequest {PersonId = 1};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetLatestSurvey(request);

			// Assert 
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.JobSeekerSurvey.ShouldNotBeNull();
		}

		[Test]
		public void SaveJobSeekerSurvey_WhenPassedValidData_ReturnsSuccess()
		{
			//Arrange
			var request = new JobSeekerSurveyRequest
			{
				JobSeekerSurvey =
					new JobSeekerSurveyDto
					{
						PersonId = 1,
						SatisfactionLevel = SatisfactionLevels.VeryDissatisfied,
						UnanticipatedMatches = true,
						WasInvitedDidApply = false
					}
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.SaveJobSeekerSurvey(request);

			// Assert 
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.JobSeekerSurvey.ShouldNotBeNull();
			response.JobSeekerSurvey.Id.ShouldNotBeNull();
			response.JobSeekerSurvey.CreatedOn.ShouldNotBeNull();
			response.JobSeekerSurvey.CreatedOn.Date.ShouldEqual(DateTime.Now.Date);
		}

		[Test]
		public void GetCampuses_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange 
			var request = new GetCampusesRequest();
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetCampuses(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Campuses.Count.ShouldEqual(4);
		}

		[Test]
		public void GetCampuses_WhenPassedRequestForCampus_ReturnsSuccess()
		{
			// Arrange 
			var request = new GetCampusesRequest { CampusId = 5944974 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetCampuses(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Campus.ShouldNotBeNull();
			response.Campus.Id.ShouldEqual(5944974);
		}

		[Test]
		public void GetInvitees_WithNoJobId_ThrowsArgumentInvalidException()
		{
			// Arrange
			var request = new InviteesForJobRequest { InviteesCriteria = new InviteesResumeViewCriteria { JobId = null } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			//Act
			var response = _service.GetInviteesForJob(request);

			//Assert
			response.SetFailure(ErrorTypes.ArgumentNull);
		}

		[Test]
		public void GetInvitees_WithJobIdZero_ThrowsArgumentInvalidException()
		{
			// Arrange
			var request = new InviteesForJobRequest { InviteesCriteria = new InviteesResumeViewCriteria { JobId = 0 } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			//Act
			var response = _service.GetInviteesForJob(request);

			//Assert
			response.SetFailure(ErrorTypes.ArgumentNull);
		}

		[Test]
		public void GetInvitees_WithValidJobId_ReturnsSuccess()
		{
			// Arrange
			var request = new InviteesForJobRequest { InviteesCriteria = new InviteesResumeViewCriteria { JobId = 1003791 } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetInviteesForJob(request);

			//Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.InviteesResumeViews.ShouldNotBeNull();
		}

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void IsCandidateExisitingJobInviteeOrApplicant_WithNoJobId_ThrowsArgumentNullException()
		{
			// Arrange
			var request = new IsCandidateExistingInviteeOrApplicantRequest { IsCandidateInviteeCriteria = new IsCandidateExistingInviteeOrApplicantCriteria { JobId = null, CandidateId = 1004258 } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.IsCandidateExisitingJobInviteeOrApplicant(request);

			//Assert

		}

		[Test]
		[ExpectedException(typeof(ArgumentNullException))]
		public void IsCandidateExisitingJobInviteeOrApplicant_WithNoCandidateId_ThrowsArgumentNullException()
		{
			// Arrange
			var request = new IsCandidateExistingInviteeOrApplicantRequest { IsCandidateInviteeCriteria = new IsCandidateExistingInviteeOrApplicantCriteria { JobId = 1000220, CandidateId = null } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.IsCandidateExisitingJobInviteeOrApplicant(request);

			//Assert

		}

		[Test]
		[ExpectedException(typeof (ArgumentOutOfRangeException))]
		public void IsCandidateExisitingJobInviteeOrApplicant_JobIdOutOfRange_ThrowsArgumentOutOfRangeException()
		{
			// Arrange
			var request = new IsCandidateExistingInviteeOrApplicantRequest { IsCandidateInviteeCriteria = new IsCandidateExistingInviteeOrApplicantCriteria { JobId = 0, CandidateId = 1004258 } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			//Act
			var response = _service.IsCandidateExisitingJobInviteeOrApplicant(request);
			// Assert
		}

		[Test]
		[ExpectedException(typeof (ArgumentOutOfRangeException))]
		public void IsCandidateExisitingJobInviteeOrApplicant_CandidateIdOutOfRange_ThrowsArgumentOutOfRangeException()
		{
			// Arrange
			var request = new IsCandidateExistingInviteeOrApplicantRequest { IsCandidateInviteeCriteria = new IsCandidateExistingInviteeOrApplicantCriteria { JobId = 1000220, CandidateId = 0 } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.IsCandidateExisitingJobInviteeOrApplicant(request);

			// Assert
		}

		[Test]
		public void IsCandidateExisitingJobInviteeOrApplicant_WithValidJobAndCandidateId_NotAnInviteeOrApplicant_ReturnsSuccess()
		{
			// Arrange
			var request = new IsCandidateExistingInviteeOrApplicantRequest { IsCandidateInviteeCriteria = new IsCandidateExistingInviteeOrApplicantCriteria { JobId = 1116079, CandidateId = 1091954 } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.IsCandidateExisitingJobInviteeOrApplicant(request);

			//Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.IsCandidateExistingJobInviteeOrApplicant.ShouldEqual(false);
		}

		[Test]
		public void IsCandidateExisitingJobInviteeOrApplicant_WithValidJobAndCandidateId_IsAnInvitee_ReturnsSuccess()
		{
			// Arrange
			var request = new IsCandidateExistingInviteeOrApplicantRequest { IsCandidateInviteeCriteria = new IsCandidateExistingInviteeOrApplicantCriteria { JobId = 1094072, CandidateId = 1091954} };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.IsCandidateExisitingJobInviteeOrApplicant(request);

			//Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.IsCandidateExistingJobInviteeOrApplicant.ShouldEqual(true);
		}

		[Test]
		public void IsCandidateExisitingJobInviteeOrApplicant_WithValidJobAndCandidateId_IsAnApplicant_ReturnsSuccess()
		{
			// Arrange
			var request = new IsCandidateExistingInviteeOrApplicantRequest { IsCandidateInviteeCriteria = new IsCandidateExistingInviteeOrApplicantCriteria { JobId = 1197370, CandidateId = 1117616 } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.IsCandidateExisitingJobInviteeOrApplicant(request);

			//Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.IsCandidateExistingJobInviteeOrApplicant.ShouldEqual(true);
		}

		[Test]
		public void GetPushNotificationList_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new GetPushNotificationListRequest
			{
				DeviceToken = "TEST001"
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetPushNotificationList(request);
			// Assert
			response.PushNotifications.ShouldNotBeNull();
			Assert.True(response.PushNotifications.Count > 0);
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void GetPushNotificationList_WhenNotificationsNotExistForDevice_ReturnsSuccessAndEmptyList()
		{
			// Arrange
			var request = new GetPushNotificationListRequest
			{
				DeviceToken = "TEST002"
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetPushNotificationList(request);
			// Assert
			response.ShouldNotBeNull();
			Assert.That(response.PushNotifications, Is.Empty);
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void GetPushNotificationList_WhenDeviceNotExist_ReturnsSuccessAndEmptyList()
		{
			// Arrange
			var request = new GetPushNotificationListRequest
			{
				DeviceToken = "TEST003"
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetPushNotificationList(request);
			// Assert
			response.ShouldNotBeNull();
			Assert.That(response.PushNotifications, Is.Empty);
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void GetPushNotificationList_WithNoDeviceToken_ThrowsArgumentInvalidException()
		{
			// Arrange
			var request = new GetPushNotificationListRequest();
			
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetPushNotificationList(request);
			// Assert
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}

		[Test]
		public void ViewedPushNotification_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new ViewedPushNotificationRequest
			{
				Id = 99999991
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ViewedPushNotification(request);
			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void ViewedPushNotification_WhenPassedInvalidId_ReturnsFailure()
		{
			// Arrange
			var request = new ViewedPushNotificationRequest
			{
				Id = 0
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ViewedPushNotification(request);
			// Assert
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}

		[Test]
		public void GetPushNotification_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new GetPushNotificationRequest
			{
				Id = 99999991
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetPushNotification(request);
			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void GetPushNotification_WhenPassedInvalidId_ReturnsFailure()
		{
			// Arrange
			var request = new GetPushNotificationRequest
			{
				Id = 0
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetPushNotification(request);
			// Assert
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}
	}
}
