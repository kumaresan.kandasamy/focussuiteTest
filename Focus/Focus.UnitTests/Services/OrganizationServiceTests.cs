﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using Focus.UnitTests.Core;
using Framework.Core;
using Moq;
using NUnit.Framework;

using Focus.Core.Messages;
using Focus.Core.Messages.OrganizationService;
using Focus.Services.Repositories;
using Focus.Services.ServiceImplementations;
using Framework.Testing.NUnit;

#endregion

namespace Focus.UnitTests.Services
{
	[TestFixture]
	public class OrganizationServiceTests : TestFixtureBase
	{
    private OrganizationService _service;

    [SetUp]
    public override void SetUp()
    {
      base.SetUp();

      SetUpCacheProvider();
      SetUpMockLogProvider();
      
			_service = new OrganizationService(RuntimeContext);
    }

		[Test]
		public void DontShowJob_WithValidRequest_ReturnsSuccess()
		{
      // Arrange
			var request = new DontDisplayJobRequest { LensPostingId = "ABC123_C9369E8621B84B94A6C5D7180E6C517B" };
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.DontShowJob(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void ApplyForJob_WithValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new ApplyForJobRequest
			{
				ResumeId = 1194892,
				LensPostingId = "ABC123_A66A29459D1E4BC3A6B53058E1628FC6",
				ReviewApplication = false,
				MatchingScore = 600,
				Notes = string.Empty,
				IsEligible = true
			};

			request.Prepare(MockClientTag, MockSessionId, MockCareerUserData, MockCulture);

			// Act
			var response = _service.ApplyForJob(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			var logEvent = (from actionEvent in DataCoreRepository.ActionEvents
											join actionType in DataCoreRepository.ActionTypes
												on actionEvent.ActionTypeId equals actionType.Id
											where actionType.Name == "CreateCandidateApplication"
												&& actionEvent.EntityIdAdditional01 == 1098749
												&& actionEvent.EntityIdAdditional02 == MockCareerPersonId
											orderby actionEvent.ActionedOn descending
											select actionEvent).FirstOrDefault();
			logEvent.ShouldBeNull();

			logEvent = (from actionEvent in DataCoreRepository.ActionEvents
									join actionType in DataCoreRepository.ActionTypes
										on actionEvent.ActionTypeId equals actionType.Id
									where actionType.Name == "SelfReferral"
										&& actionEvent.EntityId == response.ApplicationId
									orderby actionEvent.ActionedOn descending
									select actionEvent).FirstOrDefault();
			logEvent.ShouldNotBeNull();
		}

		[Test]
		public void ApplyForJob_WithValidRequestWhenImpersonating_ReturnsSuccess()
		{
			// Arrange
			var request = new ApplyForJobRequest
			{
				ResumeId = 1194892,
				LensPostingId = "ABC123_A66A29459D1E4BC3A6B53058E1628FC6",
				ReviewApplication = false,
				MatchingScore = 600,
				Notes = string.Empty,
				IsEligible = true
			};

			request.Prepare(MockClientTag, MockSessionId, MockImpersonatedCareerUserData, MockCulture);

			// Act
			var response = _service.ApplyForJob(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			var logEvent = (from actionEvent in DataCoreRepository.ActionEvents
											join actionType in DataCoreRepository.ActionTypes
												on actionEvent.ActionTypeId equals actionType.Id
											where actionType.Name == "CreateCandidateApplication"
												&& actionEvent.EntityIdAdditional01 == 1098749
												&& actionEvent.EntityIdAdditional02 == MockCareerPersonId
											orderby actionEvent.ActionedOn descending
											select actionEvent).FirstOrDefault();
			logEvent.ShouldNotBeNull();

			logEvent = (from actionEvent in DataCoreRepository.ActionEvents
									join actionType in DataCoreRepository.ActionTypes
										on actionEvent.ActionTypeId equals actionType.Id
									where actionType.Name == "SelfReferral"
										&& actionEvent.EntityId == response.ApplicationId
									orderby actionEvent.ActionedOn descending
									select actionEvent).FirstOrDefault();
			logEvent.ShouldBeNull();
		}
	
		[Test]
		public void Match_WithValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new MatchRequest
			{
				ResumeId = 1097142,
				LensPostingId = "ABC123_A66A29459D1E4BC3A6B53058E1628FC6"
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var lensRepository = new Mock<ILensRepository>();
			lensRepository.Setup(x => x.Match(It.IsAny<string>(), It.IsAny<string>())).Returns(800);

			((TestRepositories)RuntimeContext.Repositories).Lens = lensRepository.Object;

			var service = new OrganizationService(RuntimeContext);

      var response = service.MatchResumeToPosting(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Score.ShouldNotBeNull();
		}

		[Test]
		public void GetApplicationList_WithValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new ApplicationListRequest {FromDate = DateTime.Now.AddDays(-10), ToDate = DateTime.Now};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetApplicationList(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.ReferralPostings.ShouldNotBeNull();
		}

		[Test]
		public void JobInformation_WithValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new JobInformationRequest { JobLensId = "Test Job Id" };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.JobInformation(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.JobInformation.ShouldNotBeNull();
		}
	}
}
