﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using Focus.Core;
using Focus.Core.Criteria.Report;
using Focus.Core.Messages;
using Focus.Core.Messages.ReportService;
using Focus.Core.Models;
using Focus.Core.Models.Career;
using Focus.Core.Settings.OAuth;
using Focus.Services.ServiceImplementations;
using Focus.UnitTests.Core;
using Framework.Testing.NUnit;

using NUnit.Framework;

#endregion

namespace Focus.UnitTests.Services
{
  [TestFixture]
  public class ReportServiceTests : TestFixtureBase
  {
    private ReportService _service;

    [SetUp]
    public override void SetUp()
    {
      DoSetup(false);

      SetUpCacheProvider();
      SetUpMockLogProvider();
      ((FakeAppSettings)RuntimeContext.AppSettings).OutgoingOAuthSettings = new List<OAuthSettings>
			{
				new OAuthSettings
				{
					Client = OAuthClient.LaborInsight,
					ConsumerKey = "BGT",
					ConsumerSecret = "086718ED10AC48B685F93535208AE903",
					Token = "Insight",
					TokenSecret = "32611633362D4A76A3C6038E71BFE90C",
					EndPoint = "http://sandbox.api.burning-glass.com/v202/"
				}
			};
      _service = new ReportService(RuntimeContext);
      
    }

		#region Job Seekers

		[Test]
    public void GetJobSeekers_WhenPassedValidRequestWithEmploymentStatus_ReturnsSuccess()
    {
      // Arrange
      var request = new JobSeekersReportRequest
      {
        ReportCriteria = new JobSeekersReportCriteria
        {
          JobSeekerStatusInfo = new JobSeekerStatusReportCriteria
          {
            EmploymentStatuses = new List<EmploymentStatus?> { EmploymentStatus.UnEmployed }
          },
          OrderInfo = new JobSeekersReportSortOrder
          {
            OrderBy = ReportJobSeekerOrderBy.FirstName,
            Direction = ReportOrder.Ascending
          }
        }
      }.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.GetJobSeekers(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.JobSeekers.ShouldNotBeNull();
      response.JobSeekers.Count.ShouldBeGreater(0);
    }

		[Test]
		public void GetJobSeekers_WhenPassedValidRequestWithCareerAccountTypeStatus_ReturnsSuccess()
		{
			// Arrange
			var request = new JobSeekersReportRequest
			{
				ReportCriteria = new JobSeekersReportCriteria
				{
					JobSeekerStatusInfo = new JobSeekerStatusReportCriteria
					{
						CareerAccountTypes = new List<CareerAccountType?> { CareerAccountType.JobSeeker }
					},
					OrderInfo = new JobSeekersReportSortOrder
					{
						OrderBy = ReportJobSeekerOrderBy.FirstName,
						Direction = ReportOrder.Ascending
					}
				}
			}.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetJobSeekers(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.JobSeekers.ShouldNotBeNull();
			response.JobSeekers.Count.ShouldBeGreater(0);
		}

		[Test]
		public void GetJobSeekers_WhenPassedValidRequestWithCareerAccountTypeStudentStatus_ReturnsSuccess()
		{
			// Arrange
			var request = new JobSeekersReportRequest
			{
				ReportCriteria = new JobSeekersReportCriteria
				{
					JobSeekerStatusInfo = new JobSeekerStatusReportCriteria
					{
						CareerAccountTypes = new List<CareerAccountType?> { CareerAccountType.Student }
					},
					OrderInfo = new JobSeekersReportSortOrder
					{
						OrderBy = ReportJobSeekerOrderBy.FirstName,
						Direction = ReportOrder.Ascending
					}
				}
			}.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetJobSeekers(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.JobSeekers.ShouldNotBeNull();
			response.JobSeekers.Count.ShouldBeGreater(0);
		}

    [Test]
    public void GetJobSeekers_WhenPassedValidRequestWithVeteranInfo_ReturnsSuccess()
    {
      // Arrange
      var request = new JobSeekersReportRequest
      {
        ReportCriteria = new JobSeekersReportCriteria
        {
          VeteranInfo = new VeteranReportCriteria
          {
            BranchOfServices = new List<ReportMilitaryBranchOfService?> {ReportMilitaryBranchOfService.AirForce }
          },
          OrderInfo = new JobSeekersReportSortOrder
          {
            OrderBy = ReportJobSeekerOrderBy.FirstName,
            Direction = ReportOrder.Ascending
          }
        }
      }.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.GetJobSeekers(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.JobSeekers.ShouldNotBeNull();
      response.JobSeekers.Count.ShouldEqual(0);
    }

    [Test]
    public void GetJobSeekers_WhenPassedValidRequestWithChararcterisicInfo_ReturnsSuccess()
    {
      // Arrange
      var request = new JobSeekersReportRequest
      {
        ReportCriteria = new JobSeekersReportCriteria
        {
          CharacteristicsInfo = new JobSeekerCharacteristicsReportCriteria
          {
            MinimumAge = 20,
            MaximumAge = 40
          }
        }
      }.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.GetJobSeekers(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.JobSeekers.ShouldNotBeNull();
      response.JobSeekers.Count.ShouldBeGreater(0);
    }

    [Test]
    public void GetJobSeekers_WhenPassedValidRequestWithQualificationsInfo_ReturnsSuccess()
    {
      // Arrange
      var request = new JobSeekersReportRequest
      {
        ReportCriteria = new JobSeekersReportCriteria
        {
          QualificationsInfo = new QualificationsReportCriteria
          {
            Skills = new List<string> { "commercial insurance sales" }
          }
        }
      }.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.GetJobSeekers(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.JobSeekers.ShouldNotBeNull();
      response.JobSeekers.Count.ShouldBeGreater(0);
    }

    [Test]
    public void GetJobSeekers_WhenPassedValidRequestWithSalaryInfo_ReturnsSuccess()
    {
      // Arrange
      var request = new JobSeekersReportRequest
      {
        ReportCriteria = new JobSeekersReportCriteria
        {
          SalaryInfo = new SalaryReportCriteria
          {
            MinimumSalary = 10000,
            MaximumSalary = 90000,
            SalaryFrequency = ReportSalaryFrequency.Yearly
          }
        }
      }.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.GetJobSeekers(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.JobSeekers.ShouldNotBeNull();
      response.JobSeekers.Count.ShouldBeGreater(0);
    }

    [Test]
    public void GetJobSeekers_WhenPassedValidRequestWithOccupationsInfo_ReturnsSuccess()
    {
      // Arrange
      var request = new JobSeekersReportRequest
      {
        ReportCriteria = new JobSeekersReportCriteria
        {
          OccupationInfo = new OccupationsReportCriteria
          {
            MilitaryOccupationTitles = new List<string> { "Radio" }
          }
        }
      }.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.GetJobSeekers(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.JobSeekers.ShouldNotBeNull();
      response.JobSeekers.Count.ShouldEqual(0);
    }

    [Test]
    public void GetJobSeekers_WhenPassedValidRequestWithResumeInfo_ReturnsSuccess()
    {
      // Arrange
      var request = new JobSeekersReportRequest
      {
        ReportCriteria = new JobSeekersReportCriteria
        {
          ResumeInfo = new ResumeReportCriteria
          {
            WillingToWorkOvertime = true
          }
        }
      }.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.GetJobSeekers(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.JobSeekers.ShouldNotBeNull();
      response.JobSeekers.Count.ShouldBeGreater(0);
    }

    [Test]
    public void GetJobSeekers_WhenPassedValidRequestWithActivityInfo_ReturnsSuccess()
    {
      // Arrange
      var request = new JobSeekersReportRequest
      {
        ReportCriteria = new JobSeekersReportCriteria
        {
          JobSeekerActivityLogInfo = new JobSeekerActivityLogCriteria
          {
            ActivityCategory = new Tuple<long?, string>(1107357, "Assessment"),
            Activity = new Tuple<long?, string>(1107787, "Career Assessment")
          },
          DateRangeInfo = new DateRangeReportCriteria
          {
            FromDate = new DateTime(2013, 08, 01),
            ToDate = new DateTime(2013, 08, 31)
          },
          OrderInfo = new JobSeekersReportSortOrder
          {
            OrderBy = ReportJobSeekerOrderBy.ActivityServices,
            Direction = ReportOrder.Descending
          }
        }
      }.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.GetJobSeekers(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.JobSeekers.ShouldNotBeNull();
      response.JobSeekers.Count.ShouldBeGreater(0);
      response.JobSeekers.Count(js => js.Totals.ActivityServices > 0).ShouldBeGreater(0);
    }

    [Test]
    public void GetJobSeekers_WhenPassedValidRequestWithLocationInfo_ReturnsSuccess()
    {
      // Arrange
      var request = new JobSeekersReportRequest
      {
        ReportCriteria = new JobSeekersReportCriteria
        {
          LocationInfo = new LocationReportCriteria
          {
            PostalCode = "10043",
            DistanceFromPostalCode = 100
          }
        }
      }.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.GetJobSeekers(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.JobSeekers.ShouldNotBeNull();
      response.JobSeekers.Count.ShouldBeGreater(0);
    }

    [Test]
    public void GetJobSeekers_WhenPassedValidRequestWithKeyWords_ReturnsSuccess()
    {
      // Arrange
      var request = new JobSeekersReportRequest
      {
        ReportCriteria = new JobSeekersReportCriteria
        {
          KeywordInfo = new KeywordReportCriteria
          {
            SearchType = KeywordSearchType.Any,
            KeyWordSearch = new List<KeyWord?> { KeyWord.Skills, KeyWord.JobTitle },
            KeyWords = new List<string> { "Java", "Tester" }
          },
          OrderInfo = new JobSeekersReportSortOrder
          {
            OrderBy = ReportJobSeekerOrderBy.LastName,
            Direction = ReportOrder.Descending
          }
        }
      }.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.GetJobSeekers(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.JobSeekers.ShouldNotBeNull();
      response.JobSeekers.Count.ShouldBeGreater(0);
    }

    [Test]
    public void GetJobSeekers_WhenPassedValidRequestWithDistance_ReturnsSuccess()
    {
      // Arrange
      var request = new JobSeekersReportRequest
      {
        ReportCriteria = new JobSeekersReportCriteria
        {
          LocationInfo = new LocationReportCriteria
          {
            PostalCode = "10043",
            DistanceFromPostalCode = 50
          }
        }
      }.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.GetJobSeekers(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.JobSeekers.ShouldNotBeNull();
      response.JobSeekers.Count.ShouldBeGreater(0);
    }

    [Test]
    public void GetJobSeekers_WhenPassedValidRequestWithDateRange_ReturnsSuccess()
    {
      // Arrange
      var request = new JobSeekersReportRequest
      {
        ReportCriteria = new JobSeekersReportCriteria
        {
          DateRangeInfo = new DateRangeReportCriteria
          {
            FromDate = new DateTime(2013, 06, 01),
            ToDate = new DateTime(2013, 08, 30)
          }
        }
      }.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.GetJobSeekers(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.JobSeekers.ShouldNotBeNull();
      response.JobSeekers.Count.ShouldBeGreater(0);
    }

    #endregion

    #region Job Orders

    [Test]
    public void GetJobOrders_WhenPassedValidRequestWithSalaryInfo_ReturnsSuccess()
    {
      // Arrange
      var request = new JobOrdersReportRequest
      {
        ReportCriteria = new JobOrdersReportCriteria
        {
          SalaryInfo = new SalaryReportCriteria
          {
            MinimumSalary = 10000,
            MaximumSalary = 90000,
            SalaryFrequency = ReportSalaryFrequency.Yearly
          }
        }
      }.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.GetJobOrders(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.JobOrders.ShouldNotBeNull();
      response.JobOrders.Count.ShouldBeGreater(0);
    }

    [Test]
    public void GetJobOrders_WhenPassedValidRequestWithLocationInfo_ReturnsSuccess()
    {
      // Arrange
      var request = new JobOrdersReportRequest
      {
        ReportCriteria = new JobOrdersReportCriteria
        {
          LocationInfo = new LocationReportCriteria
          {
            PostalCode = "10043",
            DistanceFromPostalCode = 100
          }
        }
      }.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.GetJobOrders(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.JobOrders.ShouldNotBeNull();
      response.JobOrders.Count.ShouldBeGreater(0);
    }

    [Test]
    public void GetJobOrders_WhenPassedValidRequestWithKeyWords_ReturnsSuccess()
    {
      // Arrange
      var request = new JobOrdersReportRequest
      {
        ReportCriteria = new JobOrdersReportCriteria
        {
          KeywordInfo = new KeywordReportCriteria
          {
            SearchType = KeywordSearchType.Any,
            KeyWordSearch = new List<KeyWord?> { KeyWord.JobTitle, KeyWord.JobDescription },
            KeyWords = new List<string> { "Java", "Tester" }
          },
          OrderInfo = new JobOrdersReportSortOrder
          {
            OrderBy = ReportJobOrderOrderBy.JobTitle,
            Direction = ReportOrder.Descending
          }
        }
      }.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.GetJobOrders(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.JobOrders.ShouldNotBeNull();
      response.JobOrders.Count.ShouldBeGreater(0);
    }

    [Test]
    public void GetJobOrders_WhenPassedValidRequestWithDistance_ReturnsSuccess()
    {
      // Arrange
      var request = new JobOrdersReportRequest
      {
        ReportCriteria = new JobOrdersReportCriteria
        {
          LocationInfo = new LocationReportCriteria
          {
            PostalCode = "10043",
            DistanceFromPostalCode = 50
          }
        }
      }.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.GetJobOrders(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.JobOrders.ShouldNotBeNull();
      response.JobOrders.Count.ShouldBeGreater(0);
    }

    [Test]
    public void GetJobOrders_WhenPassedValidRequestWithDateRange_ReturnsSuccess()
    {
      // Arrange
      var request = new JobOrdersReportRequest
      {
        ReportCriteria = new JobOrdersReportCriteria
        {
          DateRangeInfo = new DateRangeReportCriteria
          {
            FromDate = new DateTime(2013, 06, 01),
            ToDate = new DateTime(2013, 08, 30)
          }
        }
      }.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.GetJobOrders(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.JobOrders.ShouldNotBeNull();
      response.JobOrders.Count.ShouldBeGreater(0);
    }

    #endregion

    #region Employers

    [Test]
    public void GetEmployers_WhenPassedValidRequestWithLocationInfo_ReturnsSuccess()
    {
      // Arrange
      var request = new EmployersReportRequest
      {
        ReportCriteria = new EmployersReportCriteria
        {
          LocationInfo = new LocationReportCriteria
          {
            PostalCode = "12345",
            DistanceFromPostalCode = 100
          }
        }
      }.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.GetEmployers(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.Employers.ShouldNotBeNull();
      response.Employers.Count.ShouldBeGreater(0);
    }

    [Test]
    public void GetEmployers_WhenPassedValidRequestWithKeyWords_ReturnsSuccess()
    {
      // Arrange
      var request = new EmployersReportRequest
      {
        ReportCriteria = new EmployersReportCriteria
        {
          KeywordInfo = new KeywordReportCriteria
          {
            SearchType = KeywordSearchType.All,
            KeyWordSearch = new List<KeyWord?> { KeyWord.EmployerName, KeyWord.JobTitle },
            KeyWords = new List<string> { "Test", "Software" }
          },
          OrderInfo = new EmployersReportSortOrder
          {
            OrderBy = ReportEmployerOrderBy.Name,
            Direction = ReportOrder.Descending
          }
        }
      }.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.GetEmployers(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.Employers.ShouldNotBeNull();
      response.Employers.Count.ShouldBeGreater(0);
    }

    [Test]
    public void GetEmployers_WhenPassedValidRequestWithDistance_ReturnsSuccess()
    {
      // Arrange
      var request = new EmployersReportRequest
      {
        ReportCriteria = new EmployersReportCriteria
        {
          LocationInfo = new LocationReportCriteria
          {
            PostalCode = "10043",
            DistanceFromPostalCode = 50
          }
        }
      }.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.GetEmployers(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.Employers.ShouldNotBeNull();
      response.Employers.Count.ShouldBeGreater(0);
    }

    [Test]
    public void GetEmployers_WhenPassedValidRequestWithDateRange_ReturnsSuccess()
    {
      // Arrange
      var request = new EmployersReportRequest
      {
        ReportCriteria = new EmployersReportCriteria
        {
          DateRangeInfo = new DateRangeReportCriteria
          {
            FromDate = new DateTime(2013, 06, 01),
            ToDate = new DateTime(2013, 08, 30)
          }
        }
      }.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.GetEmployers(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.Employers.ShouldNotBeNull();
      response.Employers.Count.ShouldBeGreater(0);
    }

    #endregion

    #region Supply Demand

    [Test]
		[Ignore]
    public void GetSupplyDemand_WhenPassedValidRequestWithOccupationInfo_ReturnsSuccess()
    {
      // Arrange
      var request = new SupplyDemandReportRequest
      {
        ReportCriteria = new SupplyDemandReportCriteria
        {

          DateRangeInfo = new DateRangeReportCriteria { FromDate = new DateTime(2007, 01, 01), ToDate = new DateTime(2015, 08, 01) },
          OccupationInfo = new SupplyDemandOccupationsReportCriteria
                             {
                               OnetId = "115494",
                               //OnetCode = "113133",
                               OnetFamily = 107156
                             }
                             
        }
      }.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.GetSupplyDemand(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.SupplyDemand.ShouldNotBeNull();
      response.SupplyDemand.Count().ShouldBeGreater(0);
    }

    [Test]
		[Ignore]
		public void GetSupplyDemandDataTable_WhenPassedValidRequestWithOccupationInfo_ReturnsSuccess()
    {
      // Arrange
      var request = new SupplyDemandDataTableRequest
      {
        ReportCriteria = new SupplyDemandReportCriteria
          {
            DateRangeInfo = new DateRangeReportCriteria { FromDate = new DateTime(2007, 01, 01), ToDate = new DateTime(2015, 08, 01) },
            OccupationInfo = new SupplyDemandOccupationsReportCriteria
            {
              OnetId = "115494",
              //OnetCode = "113133",
              //OnetFamily = "107156",
            },
            LocationInfo = new SupplyDemandLocationReportCriteria { MSA = new Tuple<long, string>(0, "Unknown"), State = new Tuple<long, string>(12852, "Kentucky") }
          }
      }.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.GetSupplyDemandDataTable(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.ReportData.ShouldNotBeNull();
      response.ReportData.ShouldBe<ReportDataTableModel>();
    }

    #endregion

    #region Lookups

    [Test]
    public void LookUpReportData_WhenPassedValidRequestForSkills_ReturnsSuccess()
    {
      // Arrange
      var request = new LookupReportDataRequest
      {
        LookUpType = ReportLookUpType.Skills,
        SearchString = "A"
      }.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.LookUpReportData(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.LookupData.ShouldNotBeNull();
      response.LookupData.Length.ShouldBeGreater(0);
    }

    #endregion

		#region Statistics

		[Ignore]
		[Test]
		public void GetStatistics_WhenPassedValidRequestWithOneStatisticType_ReturnsSuccess()
		{
			// Arrange
			var request = new GetStatisticsRequest
			{
				StatisticTypes = new List<StatisticType>{StatisticType.TotalSearchableResumes},
				StatisticDate = DateTime.Now.AddDays(-2)
			}.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetStatistics(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Statistics.ShouldNotBeNull();
			response.Statistics.Count.ShouldEqual(1);
		}

		[Ignore]
		[Test]
		public void GetStatistics_WhenPassedValidRequestWithTwoStatisticType_ReturnsSuccess()
		{
			// Arrange
			var request = new GetStatisticsRequest
			{
				StatisticTypes = new List<StatisticType> { StatisticType.TotalSearchableResumes, StatisticType.TotalSearchableResumesAddedYesterday },
				StatisticDate = DateTime.Now.AddDays(-2)
			}.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetStatistics(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Statistics.ShouldNotBeNull();
			response.Statistics.Count.ShouldEqual(2);
		}

		[Ignore]
		[Test]
		public void GetStatistics_WhenPassedInValidRequestWithNoStatisticType_ReturnsFailure()
		{
			// Arrange
			var request = new GetStatisticsRequest
			{
				StatisticDate = DateTime.Now.AddDays(-2)
			}.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetStatistics(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Statistics.ShouldBeNull();
		}

		[Ignore]
		[Test]
		public void GetStatistics_WhenPassedInValidRequestWithNoStatisticDate_ReturnsFailure()
		{
			// Arrange
			var request = new GetStatisticsRequest
			{
				StatisticTypes = new List<StatisticType> { StatisticType.TotalSearchableResumes, StatisticType.TotalSearchableResumesAddedYesterday }
			}.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetStatistics(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Statistics.ShouldBeNull();
		}

		[Ignore]
		[Test]
		public void GetStatistics_WhenPassedInValidRequestWithEmptyStatisticType_ReturnsFailure()
		{
			// Arrange
			var request = new GetStatisticsRequest
			{
				StatisticTypes = new List<StatisticType> (),
				StatisticDate = DateTime.Now.AddDays(-2)
			}.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetStatistics(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Statistics.ShouldBeNull();
		}

		#endregion

		#region Generate Employer Activity Report

		[Test]
		public void GenerateActivityReport_WhenPassedValidRequestWithSingleEmail_ReturnsSuccess()
		{
			// Arrange
      var request = new ActivityReportRequest
			{
        ReportType = AsyncReportType.EmployerActivity,
				FromDate = DateTime.Now.AddDays(-7),
				ToDate = DateTime.Now,
				Recipients = "test@test.com"
			}.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GenerateActivityReport(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void GenerateActivityReport_WhenPassedValidRequestWithMultipleEmail_ReturnsSuccess()
		{
			// Arrange
			var request = new ActivityReportRequest
			{
        ReportType = AsyncReportType.EmployerActivity,
        FromDate = DateTime.Now.AddDays(-7),
				ToDate = DateTime.Now,
				Recipients = "test@test.com, test1@test.com, test@test2.com, test@test3.com"
			}.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GenerateActivityReport(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void GenerateActivityReport_WhenPassedInValidRequestWithNoEmail_ReturnsFailure()
		{
			// Arrange
			var request = new ActivityReportRequest
			{
        ReportType = AsyncReportType.EmployerActivity,
        FromDate = DateTime.Now.AddDays(-7),
				ToDate = DateTime.Now
			}.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GenerateActivityReport(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.Errors.ShouldNotBeNull();
			response.Errors.Count.ShouldEqual(1);
			response.Errors.Any(x => x.Equals(ErrorTypes.EmailsNotSupplied)).ShouldBeTrue();
		}

		[Test]
		public void GenerateActivityReport_WhenPassedInValidRequestWithNoFromDate_ReturnsFailure()
		{
			// Arrange
			var request = new ActivityReportRequest
			{
        ReportType = AsyncReportType.EmployerActivity,
        ToDate = DateTime.Now,
				Recipients = "test@test.com, test1@test.com, test@test2.com, test@test3.com"
			}.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GenerateActivityReport(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.Errors.ShouldNotBeNull();
			response.Errors.Count.ShouldEqual(1);
			response.Errors.Any(x => x.Equals(ErrorTypes.FromDateNotSupplied)).ShouldBeTrue();
		}

		[Test]
		public void GenerateActivityReport_WhenPassedInValidRequestWithNoToDate_ReturnsFailure()
		{
			// Arrange
			var request = new ActivityReportRequest
			{
        ReportType = AsyncReportType.EmployerActivity,
        FromDate = DateTime.Now.AddDays(-3),
				Recipients = "test@test.com, test1@test.com, test@test2.com, test@test3.com"
			}.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GenerateActivityReport(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.Errors.ShouldNotBeNull();
			response.Errors.Count.ShouldEqual(2);

			var errorTypes = new List<ErrorTypes>{ErrorTypes.ToDateNotSupplied, ErrorTypes.FromDateAfterToDate};

			response.Errors.Any(errorTypes.Contains).ShouldBeTrue();
		}

		[Test]
		public void GenerateActivityReport_WhenPassedEmptyRequest_ReturnsFailure()
		{
			// Arrange
			var request = new ActivityReportRequest
			{
        ReportType = AsyncReportType.EmployerActivity,
      }.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GenerateActivityReport(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.Errors.ShouldNotBeNull();
			response.Errors.Count.ShouldEqual(3);

			var errorTypes = new List<ErrorTypes> { ErrorTypes.ToDateNotSupplied, ErrorTypes.FromDateNotSupplied, ErrorTypes.EmailsNotSupplied };

			response.Errors.Any(errorTypes.Contains).ShouldBeTrue();
		}

		[Test]
		public void GenerateActivityReport_WhenPassedInvalidRequestWithInvalidEmail_ReturnsFailure()
		{
			// Arrange
			var request = new ActivityReportRequest
			{
        ReportType = AsyncReportType.EmployerActivity,
        FromDate = DateTime.Now.AddDays(-7),
				ToDate = DateTime.Now,
				Recipients = "test@test.com, test1@test.com, test@test2.com, xxxx"
			}.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GenerateActivityReport(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.Errors.ShouldNotBeNull();
			response.Errors.Count.ShouldEqual(1);

			response.Errors.Any(x => x.Equals(ErrorTypes.InvalidEmail)).ShouldBeTrue();
		}

		[Test]
		public void GenerateActivityReport_WhenPassedInvalidRequestWithMultipleInvalidEmail_ReturnsFailure()
		{
			// Arrange
			var request = new ActivityReportRequest
			{
        ReportType = AsyncReportType.EmployerActivity,
        FromDate = DateTime.Now.AddDays(-7),
				ToDate = DateTime.Now,
				Recipients = "test@test.com, yyyyy, test@test2.com, xxxx"
			}.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GenerateActivityReport(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.Errors.ShouldNotBeNull();
			response.Errors.Count.ShouldEqual(1);

			response.Errors.Any(x => x.Equals(ErrorTypes.InvalidEmail)).ShouldBeTrue();
		}

		#endregion
	}
}
