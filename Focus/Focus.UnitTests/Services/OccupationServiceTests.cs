﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Focus.Core;
using Focus.Core.Criteria.OnetTask;
using Focus.Core.Messages.CoreService;
using Focus.Core.Messages.OccupationService;
using Focus.UnitTests.Core;
using Framework.Core;
using Moq;
using NUnit.Framework;

using Focus.Services.Repositories;
using Focus.Services.ServiceImplementations;
using Framework.Testing.NUnit;
using AcknowledgementType = Focus.Core.Messages.AcknowledgementType;

#endregion

namespace Focus.UnitTests.Services
{
  class OccupationServiceTests: TestFixtureBase
  {
    private OccupationService _service;

    [SetUp]
    public override void SetUp()
    {
      base.SetUp();

      SetUpCacheProvider();
      SetUpMockLogProvider();
     
			_service = new OccupationService(RuntimeContext);
    }
		
		[Test]
		public void GetOnetSkills_WhenPassedValidOnetCode_ReturnsSuccess()
		{
			// Arrange
			var request = new OnetSkillsRequest { OnetCode = "11-1011.00"};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetOnetSkills(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			// Functionality not yet present
			//response.OnetSkills.Count.ShouldBeGreater(0);
		}
		
  	[Test]
  	public void GetResumeBuilderQuestions_WhenPassedValidOnetCode_ReturnsSuccessAndQuestions()
  	{
  		// Arrange
  		var request = new ResumeBuilderQuestionsRequest {OnetCode = "13-1023.00"};
  		request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

  		// Act
  		var response = _service.GetResumeBuilderQuestions(request);

  		// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Questions.ShouldNotBeNull();
  	}
		
		[Test]
		public void GetMocOccupationCodes_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new OccupationCodeRequest { OccupationCode = "8406", OccupationTitle = "Aerospace Medical Technician" };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetMocOccupationCodes(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.OnetCodes.ShouldNotBeNull();
		}

		[Test]
		public void GetMocOccupationCodes_WhenPassedValidRequestWithCommissionedOccupation_ReturnsSuccessAnd3StarMatches()
		{
			// Arrange
			var request = new OccupationCodeRequest { OccupationCode = "33S4Z", OccupationTitle = "Communications and Information, C2ISREW" };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetMocOccupationCodes(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.OnetCodes.ShouldNotBeNull();
			Trace.WriteLine(response.OnetCodes.SerializeJson());
			Trace.WriteLine(string.Format("Number of 3 Star Matches:{0}", response.OnetCodes.Count(x => x.StarRating == 3)));
			Trace.WriteLine(string.Format("Number of 2 Star Matches:{0}", response.OnetCodes.Count(x => x.StarRating == 2)));
			Trace.WriteLine(string.Format("Number of 1 Star Matches:{0}", response.OnetCodes.Count(x => x.StarRating == 1)));
			response.OnetCodes.Count(x => x.StarRating == 3).ShouldBeGreater(0);
		}

		[Test]
		public void GetOccupationCodes_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new OccupationCodeRequest { OccupationTitle = "Test", SOCCount = 5 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var lensRepository = new Mock<ILensRepository>();
			lensRepository.Setup(x => x.GetOnetCodes(request.OccupationTitle)).Returns(new List<string>());

			((TestRepositories)RuntimeContext.Repositories).Lens = lensRepository.Object;

			var service = new OccupationService(RuntimeContext);
			var response = service.GetOccupationCodes(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.OnetCodes.ShouldNotBeNull();
		}

    [Test]
		public void GetStatements_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new StatementsRequest { OnetCode = "11-1011.00" };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetStatements(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Statements.ShouldNotBeNull();
		}

		[Test]
		public void GetStatements_WhenPassedValidRequestWithTense_ReturnsSuccess()
		{
			// Arrange
			var request = new StatementsRequest { OnetCode = "11-1011.00", Tense = StatementTense.Past};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetStatements(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Statements.ShouldNotBeNull();
		}
		
		[Test]
		public void GetMilitaryOccupationJobTitles_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
      var request = new MilitaryOccupationJobTitleRequest { JobTitle = "25", FullTitleSearch = false, ListSize = 25 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetMilitaryOccupationJobTitles(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.JobTitles.ShouldNotBeNull();
		}

    [Test]
    public void GetMilitaryOccupationJobTitlesWithFullSearch_WhenPassedValidRequest_ReturnsSuccess()
    {
      // Arrange
      var request = new MilitaryOccupationJobTitleRequest { JobTitle = "Signal", FullTitleSearch = true, ListSize = 25 };
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.GetMilitaryOccupationJobTitles(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.JobTitles.ShouldNotBeNull();
    }

    [Test]
    public void ConvertOnetToROnet_WhenPassedValidONet_ReturnsNull()
    {
      var request = new OnetToROnetConversionRequest {Onet = "Invalid ROnet"};
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      var response = _service.ConvertOnetToROnet(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.BestFitROnet.ShouldBeNull();
    }

    [Test]
    public void ConvertOnetToROnet_WhenPassedValidONet_ReturnsValidRecord()
    {
      var request = new OnetToROnetConversionRequest { Onet = "11-1021.00" };
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      var response = _service.ConvertOnetToROnet(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.BestFitROnet.ShouldNotBeNull();
    }
  }
}
