﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Linq;
using System.Text;

using Focus.Core;
using Focus.Core.Criteria;
using Focus.Core.Criteria.Resume;
using Focus.Core.Criteria.ResumeJob;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Messages.ResumeService;
using Focus.Core.Models.Career;
using Focus.Data.Core.Entities;
using Focus.Services.Mappers;
using Focus.Services.Messages;
using Focus.Services.Repositories.Lens;
using Focus.Services.Repositories;
using Focus.Services.ServiceImplementations;
using Focus.UnitTests.Core;
using Focus.UnitTests.Core.Repositories;
using Framework.Testing.NUnit;
using AcknowledgementType = Focus.Core.Messages.AcknowledgementType;

using Moq;
using NUnit.Framework;

#endregion

namespace Focus.UnitTests.Services
{
	[TestFixture]
	public class ResumeServiceTests:TestFixtureBase
	{
    private ResumeService _service;
	
		[SetUp]
    public override void SetUp()
    {
      base.SetUp();

      SetUpCacheProvider();
      SetUpMockLogProvider();
    	_service = new ResumeService(RuntimeContext);
    }
		
		[Test]
		public void GetResume_WithValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new GetResumeRequest { ResumeId = 1086379 };
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
		  var response = _service.GetResume(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.SeekerResume.ShouldNotBeNull();
		}
		
		[Test]
		public void SaveResume_WithValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new SaveResumeRequest();
			
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			var resumeModel = MockResumeXml.ToResumeModel(RuntimeContext, MockCulture);

			request.SeekerResume = resumeModel;

			// Act
			var lensRepository = new LensRepository(RuntimeContext);

			((TestRepositories)RuntimeContext.Repositories).Lens = lensRepository;

			var service = new ResumeService(RuntimeContext);
			var response = service.SaveResume(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}


    [Test]
    public void SaveResumeDocument_WithMSFW_CreatesIssuesAndReturnsSuccess()
    {
      var settings = MockAppSettings as FakeAppSettings;
      if (settings != null)
        settings.JobIssueFlagPotentialMSFW = true;

      // Arrange
      var request = new SaveResumeRequest();

      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      var resumeModel = MockCompletedResumeXml.ToResumeModel(RuntimeContext, MockCulture);
      resumeModel.ResumeContent.Profile.IsMSFW = true;
      request.SeekerResume = resumeModel;

      // Act
      var lensRepository = new LensRepository(RuntimeContext);

      ((TestRepositories)RuntimeContext.Repositories).Lens = lensRepository;

      var service = new ResumeService(RuntimeContext);
      var response = service.SaveResume(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      RuntimeContext.Repositories.Core.Issues.First(i => i.PersonId == MockUserData.PersonId).MigrantSeasonalFarmWorkerTriggered.ShouldBeTrue();

      if (settings != null)
        settings.JobIssueFlagPotentialMSFW = false;
    }

    [Test]
    public void SaveResumeDocument_WithOutMSFW_CreatesNoIssuesAndReturnsSuccess()
    {
      var settings = MockAppSettings as FakeAppSettings;
      if (settings != null)
        settings.JobIssueFlagPotentialMSFW = true;

      // Arrange
      var request = new SaveResumeRequest();

      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      var resumeModel = MockCompletedResumeXml.ToResumeModel(RuntimeContext, MockCulture);
      resumeModel.ResumeContent.Profile.IsMSFW = false;
      request.SeekerResume = resumeModel;

      // Act
      var lensRepository = new LensRepository(RuntimeContext);

      ((TestRepositories)RuntimeContext.Repositories).Lens = lensRepository;

      var service = new ResumeService(RuntimeContext);
      var response = service.SaveResume(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      var issues = RuntimeContext.Repositories.Core.Issues.FirstOrDefault(i => i.PersonId == MockUserData.PersonId);
      if (issues != null)
        issues.MigrantSeasonalFarmWorkerTriggered.ShouldBeFalse();

      if (settings != null)
        settings.JobIssueFlagPotentialMSFW = false;
    }

    [Test]
    public void SaveResumeDocument_WithMSFWConfigOff_CreatesNoIssuesAndReturnsSuccess()
    {
      // Arrange
      var request = new SaveResumeRequest();

      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      var resumeModel = MockCompletedResumeXml.ToResumeModel(RuntimeContext, MockCulture);
      resumeModel.ResumeContent.Profile.IsMSFW = true;
      request.SeekerResume = resumeModel;

      // Act
      var lensRepository = new LensRepository(RuntimeContext);

      ((TestRepositories)RuntimeContext.Repositories).Lens = lensRepository;

      var service = new ResumeService(RuntimeContext);
      var response = service.SaveResume(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      var issues = RuntimeContext.Repositories.Core.Issues.FirstOrDefault(i => i.PersonId == MockUserData.PersonId);
      if (issues != null)
        issues.MigrantSeasonalFarmWorkerTriggered.ShouldBeFalse();
    }

		[Test]
		public void SaveResumeDocument_WithValidRequest_ReturnsSuccess()
		{
			// Arrange
			const string resume = "Resume for example candidate.";
			var ASCII = new ASCIIEncoding();
			var bytes = ASCII.GetBytes(resume);
			var resumeDocument = new ResumeDocumentDto
			                     	{
															ResumeId = 1192112,
															FileName = "TestFile.txt",
			                     		ContentType = "mimetype/test",
			                     		DocumentBytes = bytes,
			                     		Html = "html"
			                     	};

			var request = new SaveResumeDocumentRequest{ResumeDocument = resumeDocument};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.SaveResumeDocument(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.ResumeDocument.ShouldNotBeNull();
		}

    [Test]
    public void GetResumeInfo_WithValidRequest_ReturnsSuccessAndResumeInfo()
    {
			// Arrange
			var request = new ResumeMetaInfoRequest { Criteria = new ResumeCriteria { Id = 1086379, FetchOption = CriteriaBase.FetchOptions.Single } };
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.GetResumeInfo(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
    }

		[Test]
		public void GetResumeInfo_WithValidRequest_ReturnsSuccessAndResumeInfoDictionary()
		{
			// Arrange
			var request = new ResumeMetaInfoRequest { Criteria = new ResumeCriteria { PersonId = 1000049, FetchOption = CriteriaBase.FetchOptions.Lookup } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetResumeInfo(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}
		
    [Test]
		public void GetAutomatedSummary_WithValidRequest_ReturnsSuccess()
    {
      var resumeDto = new ResumeDto
		                    {
                          ResumeXml = MockResumeXml
                        };
      
			var resumeModel = resumeDto.ToResumeModel(RuntimeContext, MockCulture);
    	
      var request = new SummaryRequest { SeekerResume = resumeModel };
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
			var lensRepository = new LensRepository(RuntimeContext);

			((TestRepositories)RuntimeContext.Repositories).Lens = lensRepository;

			var service = new ResumeService(RuntimeContext);

      var response = service.GetAutomatedSummary(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Summary.ShouldNotBeNullOrEmpty();
    }

    [Test]
    public void DeleteResume_WithValidRequest_ReturnsSuccess()
    {
			// Arrange
    	var request = new DeleteResumeRequest {ResumeId = 1086379};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.DeleteResume(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
    }
		
    [Test]
    public void GetStructuredResume_WithValidTaggedResume_ReturnsSuccess()
    {
      // Arrange
			var resume = @"<ResDoc><special><automatedsummary custom=""-1"" /><snapshot>I have 3 years of experience, including as a Design Engineer.  Most recently, I have been working as a Design Engineer at Test Employer from January 2010 to March 2012.  My skills and experiences include: Materials Management.  I hold a Bachelor of Engineering (BEng or BE) degree in Mechanical Engineer from University.</snapshot></special><resume canonversion=""2"" dateversion=""2"" iso8601=""2012-03-22"" present=""734586""><contact><name><givenname>Test</givenname><givenname>I</givenname><surname>N</surname></name>  <address lat=""42.0707"" lon=""72.625"" majorcity=""AGAWAM"" state=""MA""><state abbrev=""MA"">MA</state>    <street>Address 1</street>      <city>Test City</city>      <state_county>25013</state_county>      <state_fullname>Massachusetts</state_fullname>      <country iso3=""USA"">US</country>      <country_fullname>United States</country_fullname>      <postalcode>01001</postalcode>  </address>  home:<phone type=""home"">7777777777</phone>   home:<phone type=""home"" />   home:<phone type=""home"" />   <email>test@demo.com</email> </contact><experience><employment_status_cd>1</employment_status_cd></experience><education><school id=""2""><completiondate days=""733530"" iso8601=""2009-05-01"">05/2009</completiondate><completion_juliandate>733530</completion_juliandate><degree level=""16"">Bachelor of Engineering (BEng or BE)</degree><institution>University</institution><major code=""1502"">Mechanical Engineer</major><address><state abbrev=""MA"">MA</state><state_fullname>Massachusetts</state_fullname><country iso3=""USA"">US</country><country_fullname>United States</country_fullname></address></school><norm_edu_level_cd>24</norm_edu_level_cd><school_status_cd>5</school_status_cd></education><statements><personal><dob days=""723798"" iso8601=""1982-09-08"">09/08/1982</dob><preferences>  <sal>5.00</sal>   <salary_unit_cd>1</salary_unit_cd> <resume_searchable>-1</resume_searchable><email_flag>-1</email_flag><postal_flag>0</postal_flag><phone_pri_flag>0</phone_pri_flag><phone_sec_flag>0</phone_sec_flag><fax_flag>0</fax_flag><shift><work_week>1</work_week><work_type>1</work_type><work_over_time>0</work_over_time><shift_first_flag>-1</shift_first_flag><shift_rotating_flag>0</shift_rotating_flag><shift_second_flag>0</shift_second_flag><shift_split_flag>0</shift_split_flag><shift_third_flag>0</shift_third_flag><shift_varies_flag>0</shift_varies_flag></shift><desired_states><desired_state><code>IA</code><value>Iowa</value></desired_state><desired_state><code>KY</code><value>Kentucky</value></desired_state><desired_state><code>MA</code><value>Massachusetts</value></desired_state></desired_states></preferences><userinfo><username>test@demo.com</username><ssn_not_provided_flag>-1</ssn_not_provided_flag><customerregistrationid /><seeker_status_cd>1</seeker_status_cd><job_seeker_flag>1</job_seeker_flag></userinfo><license><driver_flag>1</driver_flag><driver_state>MA</driver_state><state_fullname>Massachusetts</state_fullname><driver_class>5</driver_class><driver_class_text>Motorcycle</driver_class_text><drv_pass_flag>-1</drv_pass_flag><drv_hazard_flag>0</drv_hazard_flag><drv_tank_flag>0</drv_tank_flag><drv_tankhazard_flag>0</drv_tankhazard_flag><drv_double_flag>-1</drv_double_flag><drv_airbrake_flag>0</drv_airbrake_flag><drv_bus_flag>0</drv_bus_flag><drv_cycle_flag>-1</drv_cycle_flag></license><crc />  <sex>2</sex> <ethnic_heritages><ethnic_heritage><ethnic_id>3</ethnic_id><selection_flag>1</selection_flag></ethnic_heritage><ethnic_heritage><ethnic_id>5</ethnic_id><selection_flag>-1</selection_flag></ethnic_heritage></ethnic_heritages>  <citizen_flag>-1</citizen_flag> <migrant_flag>0</migrant_flag>  <disability_status>1</disability_status> <veteran><vet_flag>-1</vet_flag><vet_start_date>05/05/2001</vet_start_date><vet_end_date>12/12/2012</vet_end_date>					<campaign_vet_flag>-1</campaign_vet_flag>					<vet_disability_status>1</vet_disability_status>					<vet_tsm_type_cd>2</vet_tsm_type_cd><vet_era>4</vet_era>					<vet_discharge>Honorable</vet_discharge>					<branch_of_service>USARMY</branch_of_service><rank>Sergeant Major of the Army</rank><moc>1410</moc><unit_affiliation>Regiment 143 Southern Command.</unit_affiliation><past_five_years_veteran>0</past_five_years_veteran><mostext>AN/SQS-53D Sensor Subsystem Level II Technician/Operator</mostext><mosonetcrosswalk /></veteran></personal></statements><summary><summary>I have 3 years of experience, including as a Design Engineer.  Most recently, I have been working as a Design Engineer at Test Employer from January 2010 to March 2012.  My skills and experiences include: Materials Management.  I hold a Bachelor of Engineering (BEng or BE) degree in Mechanical Engineer from University.</summary></summary><skills><skills>Materials Management, Mechanical,english</skills><languages><languages_profiency>English</languages_profiency></languages><courses><certifications><certification><certificate>REGISTERED RECORDS ADMINISTRATOR</certificate><organization_name>BGt</organization_name><completion_date>05/2010</completion_date><completion_juliandate>733895</completion_juliandate><address><state abbrev=""MA"">MA</state><state_fullname>Massachusetts</state_fullname><country iso3=""USA"">US</country><country_fullname>United States</country_fullname></address></certification></certifications></courses></skills><experience /><experience /><experience end=""734565"" start=""733775""><job end=""734565"" id=""1"" onet=""17-2141.00"" onetrank=""1"" pos=""1"" start=""733775""><display_in_resume>true</display_in_resume><daterange><start days=""733775"" iso8601=""2010-01-01"">01-2010</start><end currentjob=""1"" days=""734565"" iso8601=""2012-03-01"">03-2012</end></daterange><employer>Test Employer</employer><title onet=""17-2141.00"" onetrank=""1"">Design Engineer</title><onet>17-2141.00</onet><description>* Planned and designed mechanical systems and components. * Implemented conceptual designs. * Collaborated to implement operating procedures. * Assisted drafters in design tasks. * Evaluated and tested equipment. * Documented evaluations. * Generated bill of materials. * Resolved malfunctions. * Recommended design modifications. * Created engineering change orders. * Implemented operating procedures. * Organized tasks.</description><address><city>Test City</city><state abbrev=""MA"">MA</state><state_fullname>Massachusetts</state_fullname><country iso3=""USA"">US</country><country_fullname>United States</country_fullname></address><salary_unit_cd>6</salary_unit_cd><sal>0.00</sal></job></experience></resume> </ResDoc>";
			var request = new ResumeConvertionRequest { TaggedResume = resume, Canonize = true };
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
			var lensRepository = new LensRepository(RuntimeContext);

			((TestRepositories)RuntimeContext.Repositories).Lens = lensRepository;

			var service = new ResumeService(RuntimeContext);
      var response = service.GetStructuredResume(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
    }

    [Test]
    public void GetTaggedResume_WithValidRequest_ReturnsSuccess()
    {
			//TODO: Martha (UnitTest) - Need a resume id with a html resume
			// Arrange
			var request = new ResumeConvertionRequest { ResumeId = 1086379 };
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.GetTaggedResume(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
    }
		
    [Test]
    public void GetWorkHistory_WithValidRequest_ReturnsSuccess()
    {
			//TODO: Martha (UnitTest) - Need a resume id that has jobs
			// Arrange
			var request = new ResumeJobRequest { Criteria = new ResumeJobCriteria { ResumeId = 1110184, FetchOption = CriteriaBase.FetchOptions.Lookup } };
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.GetResumeJobs(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
    }

    [Test]
    public void GetDefaultResume_WithValidRequest_ReturnsSuccessAndResumeId()
    {
      //TODO: Martha (UnitTest) - Need a person id with resumes
			// Arrange
      var request = new DefaultResumeRequest();
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.GetDefaultResume(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.ResumeId.ShouldNotBeNull();
    }

    [Test]
    public void GetDefaultResume_WithValidRequestWithPersonId_ReturnsSuccessAndResumeId()
    {
      // Arrange
      var request = new DefaultResumeRequest
      {
        PersonId = 1091954
      };
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.GetDefaultResume(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.ResumeId.ShouldNotBeNull();
    }

		[Test]
		public void GetDefaultResume_WithValidRequest_ReturnsSuccessAndResume()
		{
			//TODO: Martha (UnitTest) - Need a person id with resumes
			// Arrange
			var request = new DefaultResumeRequest{FetchDefaultResume = true};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetDefaultResume(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void SetDefaultResume_WithValidRequest_ReturnsSuccess()
		{
			// Arrange
      var request = new DefaultResumeRequest { ResumeId = 1110418 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.SetDefaultResume(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

    [Test]
    public void SetDefaultResume_WithValidRequestForIncompleteResume_ReturnsFailure()
    {
      // Arrange
      var request = new DefaultResumeRequest { ResumeId = 1086379 };
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.SetDefaultResume(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
    }
		[Test]
		public void GetHtmlResume_WithValidRequest_ReturnsSuccess()
		{
			//TODO: Martha (UnitTest) - Need a resume id with a html resume
			// Arrange
			var request = new HTMLResumeRequest { ResumeId = 1086379 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetHTMLResume(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void GetOriginalResume_WithValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new OriginalResumeRequest { ResumeId = 1086379, ResumeType = DocumentContentType.Binary };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetOriginalResume(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}
		
		[Test]
		public void GetResumeViewCount_WithValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new ResumeViewCountRequest { UserType = UserTypes.Talent, PersonId = 1085521 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetResumeViewCount(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

    [Test]
    public void RegisterDefaultResume_WithValidRequest_ReturnsSuccess()
    {
      // Arrange
      var request = new RegisterResumeRequest { PersonId = 1085521 };
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.RegisterDefaultResume(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
    }

		[Test]
		public void SaveResume_WithResumeCompletedForFirstTimeAndNonStandaloneIntegrationClient_LogsSelfServiceActivityCompleteResume()
		{
			// Arrange
			var request = new SaveResumeRequest();

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Create model ans set the competion status to complete
			var resumeModel = MockResumeXml.ToResumeModel(RuntimeContext, MockCulture);
			resumeModel.ResumeMetaInfo.CompletionStatus = ResumeCompletionStatuses.Completed;

			// Ensure that the completion status of the existing resume in the database is not complete
			var resume = RuntimeContext.Repositories.Core.FindById<Resume>(resumeModel.ResumeMetaInfo.ResumeId);
			resume.ResumeCompletionStatusId = ResumeCompletionStatuses.Options;

			request.SeekerResume = resumeModel;

			((FakeAppSettings)RuntimeContext.AppSettings).Module = FocusModules.CareerExplorer;
			
			//  Set the integration client to be something other than standalone so that an integration message will be published.
			((FakeAppSettings)RuntimeContext.AppSettings).IntegrationClient = IntegrationClient.Wisconsin;

			((TestRepositories)RuntimeContext.Repositories).Lens = new MockLensRepository();

			var service = new ResumeService(RuntimeContext);

			// Act
			var response = service.SaveResume(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success, response.Exception != null ? response.Exception.Message : response.Message);

			// Check that the self service message has been posted to the mesage bus
			((TestMessagingHelper)RuntimeContext.Helpers.Messaging).Messages
				.Any(m =>
				{
					var resumeProcessMessage = m.Message as ResumeProcessMessage;
					return resumeProcessMessage != null
									&& resumeProcessMessage.ActionTypesToPublish != null
									&& resumeProcessMessage.ActionTypesToPublish.Any(action => action == ActionTypes.CompleteResume);
				}).ShouldBeTrue();
		}

		[Test]
		public void SaveResume_WithDefaultResumeAlreadyCompletedAndNonStandaloneIntegrationClient_LogsSelfServiceActivityEditDefaultCompletedResume()
		{
			// Arrange
			var request = new SaveResumeRequest();

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Create model ans set the competion status to complete
			var resumeModel = MockResumeXml.ToResumeModel(RuntimeContext, MockCulture);
			resumeModel.ResumeMetaInfo.CompletionStatus = ResumeCompletionStatuses.Completed;

			// Ensure that the completion status of the existing resume in the database is complete
			var resume = RuntimeContext.Repositories.Core.FindById<Resume>(resumeModel.ResumeMetaInfo.ResumeId);
			resume.ResumeCompletionStatusId = ResumeCompletionStatuses.Completed;

			request.SeekerResume = resumeModel;

			((FakeAppSettings)RuntimeContext.AppSettings).Module = FocusModules.CareerExplorer;

			//  Set the integration client to be something other than standalone so that an integration message will be published.
			((FakeAppSettings)RuntimeContext.AppSettings).IntegrationClient = IntegrationClient.Wisconsin;

			((TestRepositories)RuntimeContext.Repositories).Lens = new MockLensRepository();

			var service = new ResumeService(RuntimeContext);

			// Act
			var response = service.SaveResume(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success, response.Exception != null ? response.Exception.Message : response.Message);

			// Check that the self service message has been posted to the mesage bus
			((TestMessagingHelper)RuntimeContext.Helpers.Messaging).Messages
				.Any(m =>
				{
					var resumeProcessMessage = m.Message as ResumeProcessMessage;
					return resumeProcessMessage != null
									&& resumeProcessMessage.ActionTypesToPublish != null
									&& resumeProcessMessage.ActionTypesToPublish.Any(action => action == ActionTypes.EditDefaultCompletedResume);
				}).ShouldBeTrue();
		}

    [Test]
    public void GetResumeTemplate_WithPersonWithTemplate_ReturnsResumeObject()
    {
      var request = new GetResumeTemplateRequest();
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
      request.PersonId = 1194834;

      var response = _service.GetResumeTemplate(request);

      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.Resume.ShouldNotBeNull();
    }

    [Test]
    public void GetResumeTemplate_WithPersonWithOutTemplate_ReturnsNullResumeObject()
    {
      var request = new GetResumeTemplateRequest();
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
      request.PersonId = 1195044;

      var response = _service.GetResumeTemplate(request);

      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.Resume.ShouldBeNull();
    }


		[Test]
		public void SaveResume_WithResumeCompletedAndLogActionForUnsubscribingVeteranPriorityEmails_ReturnsSuccess()
		{
			var request = new SaveResumeRequest();

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Create model ans set the competion status to complete
			var resumeModel = MockResumeXml.ToResumeModel(RuntimeContext, MockCulture);
			resumeModel.ResumeMetaInfo.CompletionStatus = ResumeCompletionStatuses.Completed;
			resumeModel.ResumeContent.Profile.Veteran.VeteranPriorityServiceAlertsSubscription = false;
			resumeModel.ResumeContent.Profile.Veteran.VeteranPriorityServiceAlertsSubscriptionChanged = true;

			// Ensure that the completion status of the existing resume in the database is complete
			var resume = RuntimeContext.Repositories.Core.FindById<Resume>(resumeModel.ResumeMetaInfo.ResumeId);
			resume.ResumeCompletionStatusId = ResumeCompletionStatuses.Completed;
			resume.VeteranPriorityServiceAlertsSubscription = false;
			resume.IsDefault = true;

			request.SeekerResume = resumeModel;

			((FakeAppSettings)RuntimeContext.AppSettings).Module = FocusModules.CareerExplorer;
			
			((TestRepositories)RuntimeContext.Repositories).Lens = new MockLensRepository();

			var service = new ResumeService(RuntimeContext);

			// Act
			var response = service.SaveResume(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			var actionTypeId = DataCoreRepository.ActionTypes.Where(x => x.Name.Equals(ActionTypes.UnsubscribeEmail.ToString())).Select(x => x.Id).FirstOrDefault();
	  	DataCoreRepository.ActionEvents.Count(x => x.ActionTypeId.Equals(actionTypeId)).ShouldEqual(1);
		}


		[Test]
		public void SaveResume_WithResumeCompletedAndLogActionForSubscribingVeteranPriorityEmails_ReturnsSuccess()
		{
			var request = new SaveResumeRequest();

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Create model ans set the competion status to complete
			var resumeModel = MockResumeXml.ToResumeModel(RuntimeContext, MockCulture);
			resumeModel.ResumeMetaInfo.CompletionStatus = ResumeCompletionStatuses.Completed;
			resumeModel.ResumeContent.Profile.Veteran.VeteranPriorityServiceAlertsSubscription = true;
			resumeModel.ResumeContent.Profile.Veteran.VeteranPriorityServiceAlertsSubscriptionChanged = false;

			// Ensure that the completion status of the existing resume in the database is complete
			var resume = RuntimeContext.Repositories.Core.FindById<Resume>(resumeModel.ResumeMetaInfo.ResumeId);
			resume.ResumeCompletionStatusId = ResumeCompletionStatuses.Completed;
			resume.VeteranPriorityServiceAlertsSubscription = true;
			resume.IsDefault = true;

			request.SeekerResume = resumeModel;

			((FakeAppSettings)RuntimeContext.AppSettings).Module = FocusModules.CareerExplorer;

			((TestRepositories)RuntimeContext.Repositories).Lens = new MockLensRepository();

			var service = new ResumeService(RuntimeContext);

			// Act
			var response = service.SaveResume(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			var actionTypeId = DataCoreRepository.ActionTypes.Where(x => x.Name.Equals(ActionTypes.UnsubscribeEmail.ToString())).Select(x => x.Id).FirstOrDefault();
			DataCoreRepository.ActionEvents.Count(x => x.ActionTypeId.Equals(actionTypeId)).ShouldEqual(0);
		}
	}
}
