﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Text;
using Focus.UnitTests.Core;
using NUnit.Framework;

using Focus.Core;
using Focus.Core.Messages;
using Focus.Core.Messages.UtilityService;
using Focus.Services.Repositories.Lens;
using Focus.Services.ServiceImplementations;
using Framework.Testing.NUnit;

#endregion

namespace Focus.UnitTests.Services
{
	[TestFixture]
	public class UtilityServiceTests : TestFixtureBase
	{
		[SetUp]
		public override void SetUp()
		{
			base.SetUp();

			SetUpCacheProvider();
			SetUpMockLogProvider();
		}
		
		[Test]
		public void TagBinaryDocument_WithValidRequest_ReturnsSuccess()
		{
			// Arrange
			const string resume = "Resume for example candidate.";
			var ASCII = new ASCIIEncoding();
			var bytes = ASCII.GetBytes(resume);
			var request = new BinaryDocumentRequest {BinaryContent = bytes, FileExtension = "txt", DocumentType = DocumentType.Resume};
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			LensRepository = new LensRepository(RuntimeContext);
			((TestRepositories) RuntimeContext.Repositories).Lens = LensRepository;

			var service = new UtilityService(RuntimeContext);

			// Act
			var response = service.TagBinaryDocument(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.TaggedDocument.ShouldNotBeNullOrEmpty();	
			response.Html.ShouldNotBeNull();
		}
		
		[Test]
		public void TagPlainDocument_WithValidRequest_ReturnsSuccess()
		{
			// Arrange
			const string plainText = "Name: Andrew Jones, Phone number: 01222 3455556";
			var request = new PlainDocumentRequest {PlainDocument = plainText, DocumentType = DocumentType.Resume};
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			LensRepository = new LensRepository(RuntimeContext);
			((TestRepositories)RuntimeContext.Repositories).Lens = LensRepository;

			var service = new UtilityService(RuntimeContext);

			// Act
			var response = service.TagPlainDocument(request);

			// Assert
			response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.TaggedDocument.ShouldNotBeNullOrEmpty();
		}

		[Test]
		public void TagPlainDocument_WithValidPostingRequest_ReturnsSuccess()
		{
			// Arrange
			const string plainText = "Name: Andrew Jones, Phone number: 01222 3455556";
			var request = new PlainDocumentRequest { PlainDocument = plainText, DocumentType = DocumentType.Posting };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			LensRepository = new LensRepository(RuntimeContext);
			((TestRepositories)RuntimeContext.Repositories).Lens = LensRepository;

			var service = new UtilityService(RuntimeContext);

			// Act
			var response = service.TagPlainDocument(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.TaggedDocument.ShouldNotBeNullOrEmpty();
		}
	}
}
