﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.IO;
using System.Xml.Linq;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Models.Career;
using Focus.Services.Repositories;
using Focus.UnitTests.Core;
using Framework.Testing.NUnit;
using Framework.Logging;
using Moq;
using NUnit.Framework;
using Focus.Services.Mappers;

#endregion

namespace Focus.UnitTests.Services.Mappers
{
  [TestFixture]
	public class ResumeMapperTests : TestFixtureBase
  {
   
    [SetUp]
    public override void SetUp()
    {
      base.SetUp();

      SetUpCacheProvider();
      SetUpMockLogProvider();
    
    }
		
		[Test]
		public void ToResumeModel_WhenPassedValidResumeDto_ReturnsModel()
		{
      // Arrange
      var resumeDto = new ResumeDto{ ResumeXml = MockResumeXml };
   
      // Act
      var resumeModel = resumeDto.ToResumeModel(RuntimeContext, MockCulture);

      // Assert
      resumeModel.ShouldNotBeNull();
    }

		[Test]
		public void ToResumeModel_WhenPassedValidResumeDto_ReturnsModel_With_Language_Proficiencies()
		{
			// Arrange
			var resumeDto = new ResumeDto { ResumeXml = MockResumeWithLanguageProficienciesXml };

			// Act
			var resumeModel = resumeDto.ToResumeModel(RuntimeContext, MockCulture);

			// Assert
			resumeModel.ShouldNotBeNull();
		}
		
		[Test]
		public void ToResumeDto_WhenPassedValidResumeModel_ReturnsDto()
		{
			// Arrange
			var resumeDto = new ResumeDto { ResumeXml = MockResumeXml };

			// Act
			var resumeModel = resumeDto.ToResumeModel(RuntimeContext, MockCulture);

			resumeDto = resumeModel.ToResumeDto(RuntimeContext, MockCulture, MockAppSettings.DefaultCountryKey);
			// Assert
			resumeDto.ShouldNotBeNull();
		}

		[Test]
		public void ToResumeDto_WhenPassedValidResumeModel_ReturnsDto_With_Language_Proficiencies()
		{
			// Arrange
			var resumeDto = new ResumeDto { ResumeXml = MockResumeWithLanguageProficienciesXml };

			// Act
			var resumeModel = resumeDto.ToResumeModel(RuntimeContext, MockCulture);

			resumeDto = resumeModel.ToResumeDto(RuntimeContext, MockCulture, MockAppSettings.DefaultCountryKey);
			// Assert
			resumeDto.ShouldNotBeNull();
		}

		// Added for FVN-306
		[Test]
		public void ToResumeDto_WhenPassedValidResumeModelWithSpecialButNoPreferences_ReturnsDto()
		{
			// Arrange
			var resumeDto = new ResumeDto { ResumeXml = MockResumeXml };

			// Act
			var resumeModel = resumeDto.ToResumeModel(RuntimeContext, MockCulture);

			resumeModel.Special = resumeModel.Special ?? new ResumeSpecialInfo();

			resumeModel.Special.Preferences = null;

			resumeDto = resumeModel.ToResumeDto(RuntimeContext, MockCulture, MockAppSettings.DefaultCountryKey);
			// Assert
			resumeDto.ShouldNotBeNull();
		}

		// Added for FVN-306
		[Test]
		public void ToResumeDto_WhenPassedValidResumeModelWithSpecialButNoPreferences_ReturnsDto_With_Language_Proficiencies()
		{
			// Arrange
			var resumeDto = new ResumeDto { ResumeXml = MockResumeWithLanguageProficienciesXml };

			// Act
			var resumeModel = resumeDto.ToResumeModel(RuntimeContext, MockCulture);

			resumeModel.Special = resumeModel.Special ?? new ResumeSpecialInfo();

			resumeModel.Special.Preferences = null;

			resumeDto = resumeModel.ToResumeDto(RuntimeContext, MockCulture, MockAppSettings.DefaultCountryKey);
			// Assert
			resumeDto.ShouldNotBeNull();
		}
	}
}
