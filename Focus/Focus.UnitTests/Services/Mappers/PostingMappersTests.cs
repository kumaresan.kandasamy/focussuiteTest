﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;

using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Data.Core.Entities;
using Focus.Services.DtoMappers;
using Focus.Services.Mappers;
using Focus.UnitTests.Core;
using Framework.Testing.NUnit;

using NUnit.Framework;

#endregion

namespace Focus.UnitTests.Services.Mappers
{
	[TestFixture]
	public class PostingMappersTests : TestFixtureBase
	{
		[Test]
		public void ToPostingEnvelope_WhenPassedValidPostingDto_ReturnsPostingEnvelope()
		{
			// Arrange

			#region Arrange

			var postingDto = new PostingDto
			                 	{
			                 		Id = 1,
													JobTitle = "Test job",
													PostingXml =
			                 			@"<?xml version='1.0' encoding='iso-8859-1'?><jobposting><postinghtml>&lt;html&gt;
  &lt;body&gt;
    &lt;p&gt;&lt;img src=""http://talent-testnextgen.burning-glass.com/logo/4779742""&gt;&lt;/p&gt;
    &lt;p&gt;&lt;b&gt;Senior Adults Director&lt;/b&gt;&lt;br&gt;Test Company 01 Ltd A&lt;br&gt;Testerville, ID (00000)&lt;br&gt;Number of openings: 21&lt;br&gt;Application closing date: 7/25/2013&lt;br&gt;&lt;br&gt;Salary range $123.00 - $3434.00 Weekly&lt;br&gt;Normal working days are Monday and Saturday&lt;br&gt;Healthcare employer and Advanced manufacturing employer&lt;/p&gt;
    &lt;p&gt;* Provide overall direction and leadership in  private sector organization.
&lt;br&gt;* Determine company policy
&lt;br&gt;* Report to the board of directors
&lt;br&gt;* Plan operational activities
&lt;br&gt;* Direct operational activities at the highest level of management
&lt;br&gt;&lt;/p&gt;
    &lt;p&gt;&lt;b&gt;Requirements&lt;/b&gt;&lt;br&gt;Applicants must have at least a High school diploma/GED or equivalent&lt;br&gt;Applicants must have at least 1 years 2 months experience&lt;br&gt;Applicants must be at least 34 (No reason)&lt;br&gt;Applicants must hold a Motorcycle driving license with Pass transport, Hazardous materials, Tank vehicle and Tank hazard endorsements&lt;br&gt;Applicants must hold the occupational license of No License&lt;br&gt;Applicants must hold No certificate&lt;br&gt;Applicant must Know driving&lt;br&gt;Applicant must Know how to play games&lt;br&gt;Applicant must Know how to fly kite&lt;/p&gt;
    &lt;p&gt;&lt;b&gt;Benefits&lt;/b&gt;&lt;br&gt;Leave benefits include Paid holidays and Sick&lt;br&gt;Retirement benefits include Pension plan&lt;br&gt;Insurance benefits include Dental and Disability&lt;/p&gt;
    &lt;p&gt;&lt;b&gt;About Test Company 01 Ltd A&lt;/b&gt;&lt;br&gt;Testing Company.....&lt;/p&gt;
    &lt;p&gt;&lt;b&gt;How to apply&lt;/b&gt;&lt;br&gt;&lt;a href=""http://careerexplorerdev.usdev.burninglass.com"" target=""_blank""&gt;Log in to OK Job Match and submit your resume&lt;/a&gt;&lt;/p&gt;
    &lt;p&gt;&lt;small&gt;REF/29441/4784121&lt;/small&gt;&lt;/p&gt;
  &lt;/body&gt;
&lt;/html&gt;</postinghtml>
<joburl></joburl><JobDoc>
  <special>
    <cf001>1</cf001>
    <cf002>734894</cf002>
    <cf003>735076</cf003>
    <cf004>0</cf004>
    <cf005>0</cf005>
    <cf006>1</cf006>
    <cf007>FT0004784121</cf007>
    <cf008>11101100</cf008>
    <cf009>0</cf009>
    <cf010>0</cf010>
    <cf011>1</cf011>
    <cf012>7</cf012>
    <cf013></cf013>
    <cf014>Test</cf014>
    <cf015>0</cf015>
    <cf016>0</cf016>
    <cf017>0</cf017>
    <cf018>0</cf018>
    <cf019>0</cf019>
    <cf020>0</cf020>
    <cf021>0</cf021>
    <cf022>0</cf022>
    <cf023>0</cf023>
    <cf024>211101100,30000Test</cf024>
    <cf025></cf025>
    <mode>update</mode>
    <originid>7</originid>
    <job_status_cd>1</job_status_cd>
    <office_id></office_id>
    <jobtitle>Senior Adults Director</jobtitle>
    <jobemployer>Test Company 01 Ltd A</jobemployer>
    <foreignpostingid>FT0004784121</foreignpostingid>
    <jobtype>4</jobtype>
    <JOBTYPE_CD>4</JOBTYPE_CD>
  <green>0</green><cf026>0</cf026></special>
  <posting>
    <contact>
      <company>Test Company 01 Ltd A</company>
      <person_title>Mr</person_title>
      <person>DevClientEmp ClientEmployee</person>
      <address>
        <street></street>
        <street></street>
        <city>Testerville</city>
        <state>ID</state>
        <county>Franklin</county>
        <postalcode>00000</postalcode>
        <country>US</country>
      </address>
      <email>client@Employee.com</email>
      <joburl></joburl>
      <phone></phone>
      <phone></phone>
      <phone_ext></phone_ext>
      <phone_ext></phone_ext>
      <fax></fax>
    </contact>
    <duties>
      <title>Senior Adults Director</title>
      <onettitle>Chief Executives</onettitle>* Provide overall direction and leadership in  private sector organization.
* Determine company policy
* Report to the board of directors
* Plan operational activities
* Direct operational activities at the highest level of management
</duties>
    <background>
      <experience>14</experience>
    </background>
    <benefits>
      <salary>123.00-3434.00 $ Weekly</salary>
    </benefits>
    <qualifications>
      <required>
        <certification>No License, No certificate</certification>
        <training></training>
      </required>
    </qualifications>
    <skills>
      <languages></languages>
    </skills>
    <jobinfo>
      <educationlevel>2</educationlevel>
      <positioncount>21</positioncount>
    </jobinfo>
  <qualifications><required><education><degree>High School Diploma</degree></education></required></qualifications></posting>
  <clientjobdata />
</JobDoc><clientjobdata><JOBDOC ORIGINATION_METHOD_CD=""7"" MODE=""update"" ID=""FT0004784121""><JOB_CREATE_DATE>2013-01-24</JOB_CREATE_DATE><JOB_TITLE>Senior Adults Director</JOB_TITLE><JOB_STATUS_CD>1</JOB_STATUS_CD><JOB_CATEGORY_CD>A</JOB_CATEGORY_CD><OFFICE_ID /><COMPANY>Test Company 01 Ltd A</COMPANY><COMPANY_NAICS>Testing</COMPANY_NAICS><EMPLOYER_CONTACT_TITLE>Mr</EMPLOYER_CONTACT_TITLE><EMPLOYER_CONTACT_FIRST_NAME>DevClientEmp</EMPLOYER_CONTACT_FIRST_NAME><EMPLOYER_CONTACT_LAST_NAME>ClientEmployee</EMPLOYER_CONTACT_LAST_NAME><JOB_CONTACT_ADDR_1 /><JOB_CONTACT_ADDR_2 /><JOB_CONTACT_CITY> </JOB_CONTACT_CITY><JOB_CONTACT_STATE>KY</JOB_CONTACT_STATE><JOB_CONTACT_ZIP>40601</JOB_CONTACT_ZIP><JOB_CONTACT_COUNTRY>US</JOB_CONTACT_COUNTRY><JOB_CONTACT_COUNTY>21073</JOB_CONTACT_COUNTY><JOB_CONTACT_PHONE_PRI /><JOB_CONTACT_PHONE_PRI_EXT /><JOB_CONTACT_PHONE_SEC /><JOB_CONTACT_PHONE_SEC_EXT /><JOB_CONTACT_URL_PRI /><JOB_CONTACT_FAX_PRI /><JOB_CONTACT_EMAIL>client@Employee.com</JOB_CONTACT_EMAIL><CONTACT_SEND_DIRECT_FLAG>0</CONTACT_SEND_DIRECT_FLAG><CONTACT_POSTAL_FLAG>0</CONTACT_POSTAL_FLAG><CONTACT_PHONE_FLAG>0</CONTACT_PHONE_FLAG><CONTACT_FAX_FLAG>0</CONTACT_FAX_FLAG><CONTACT_EMAIL_FLAG>0</CONTACT_EMAIL_FLAG><CONTACT_URL_FLAG>0</CONTACT_URL_FLAG><CONTACT_TALENT_FLAG>1</CONTACT_TALENT_FLAG><SAL_MIN>123.00</SAL_MIN><SAL_MAX>3434.00</SAL_MAX><SAL_UNIT_CD>3</SAL_UNIT_CD><EDUCATION_CD>2</EDUCATION_CD><JOB_REQUIRED_LIC_CERT>No License, No certificate</JOB_REQUIRED_LIC_CERT><JOB_REQUIRED_TRAINING /><EXPERIENCE_IN_MONTHS>14</EXPERIENCE_IN_MONTHS><DURATION>1</DURATION><JOB_DESCRIPTION>* Provide overall direction and leadership in  private sector organization.
* Determine company policy
* Report to the board of directors
* Plan operational activities
* Direct operational activities at the highest level of management
</JOB_DESCRIPTION><JOB_LAST_OPEN_DATE>2013-07-25</JOB_LAST_OPEN_DATE><WORK_VARY_FLAG>0</WORK_VARY_FLAG><WORK_MON_FLAG>1</WORK_MON_FLAG><WORK_TUE_FLAG>0</WORK_TUE_FLAG><WORK_WED_FLAG>0</WORK_WED_FLAG><WORK_THU_FLAG>0</WORK_THU_FLAG><WORK_FRI_FLAG>0</WORK_FRI_FLAG><WORK_SAT_FLAG>1</WORK_SAT_FLAG><WORK_SUN_FLAG>0</WORK_SUN_FLAG><DRIVER_CLASS_CD>5</DRIVER_CLASS_CD><SHORTHAND_SPEED>0</SHORTHAND_SPEED><TYPING_SPEED>0</TYPING_SPEED><AGE_MIN>34</AGE_MIN><ONET_CODE>11101100</ONET_CODE><ONET_TITLE>Chief Executives</ONET_TITLE><REFERRAL_DESIRED_CT>0</REFERRAL_DESIRED_CT><REFERRAL_PROVIDED_CT>0</REFERRAL_PROVIDED_CT><POSITION_CT>21</POSITION_CT><JOB_LOCATION_CITY>Testerville</JOB_LOCATION_CITY><JOB_LOCATION_STATE>ID</JOB_LOCATION_STATE><JOB_LOCATION_ZIP>00000</JOB_LOCATION_ZIP><BENEFITS><BENEFIT401K>0</BENEFIT401K><MEDICALLEAVE>0</MEDICALLEAVE><LEAVESHARING>0</LEAVESHARING><PROFITSHARING>0</PROFITSHARING><DISABILITYINSURANCE>1</DISABILITYINSURANCE><HEALTHSAVINGS>0</HEALTHSAVINGS><LIFEINSURANCE>0</LIFEINSURANCE><VISIONINSURANCE>0</VISIONINSURANCE><RELOCATION>0</RELOCATION><TUITIONASSISTANCE>0</TUITIONASSISTANCE><BENEFITSNEGOTIABLE>0</BENEFITSNEGOTIABLE><OTHER>1</OTHER></BENEFITS><LANGUAGES><ARABIC>0</ARABIC><CHINESE>0</CHINESE><ENGLISH>0</ENGLISH><FRENCH>0</FRENCH><GERMAN>0</GERMAN><ITALIAN>0</ITALIAN><JAPANESE>0</JAPANESE><RUSSIAN>0</RUSSIAN><SPANISH>0</SPANISH><OTHER>0</OTHER></LANGUAGES><JOB_REQUIREMENTS><BONDING>0</BONDING><CREDITCHECK>0</CREDITCHECK><CRIMINALCHECK>0</CRIMINALCHECK><DRUGSCREEN>0</DRUGSCREEN><EMPLOYERTESTING>0</EMPLOYERTESTING><HEAVYLIFTING>0</HEAVYLIFTING><JOINUNION>0</JOINUNION><OWNTOOLS>0</OWNTOOLS><PHYSICALEXAM>0</PHYSICALEXAM><OTHER>0</OTHER><OVERTIMEREQUIRED>0</OVERTIMEREQUIRED><HL_MINLBS>0</HL_MINLBS></JOB_REQUIREMENTS><REQUIREMENTS screening=""-1""><REQUIREMENT type=""AGE"" mandate=""1""><MIN>34</MIN><REASON>No reason</REASON></REQUIREMENT><REQUIREMENT type=""CERTIFICATIONS"" mandate=""1""><CERTIFICATE>No certificate</CERTIFICATE></REQUIREMENT><REQUIREMENT type=""DRIVERS_LICENSE"" mandate=""1""><CLASS>Motorcycle</CLASS><ENDORSEMENTS>Pass transport, Hazardous materials, Tank vehicle, Tank hazard</ENDORSEMENTS></REQUIREMENT><REQUIREMENT type=""LICENSES"" mandate=""1""><LICENSE>No License</LICENSE></REQUIREMENT><REQUIREMENT type=""MIN_EDU"" mandate=""1"">High school diploma or equivalent</REQUIREMENT><REQUIREMENT type=""SPECIAL"" mandate=""1""><SPECIAL>Know driving</SPECIAL><SPECIAL>Know how to play games</SPECIAL><SPECIAL>Know how to fly kite</SPECIAL></REQUIREMENT></REQUIREMENTS></JOBDOC></clientjobdata><REQUIREMENTS />
<jobstatus>ACTIVE</jobstatus><special><jobviewed count=""2"" /><match jobseekermatchcount=""0"" veteranmatchcount=""0"" /></special></jobposting>",
												  EmployerName = "Test Employer",
													JobId = 2,
													StatusId = PostingStatuses.Active,
													OriginId = 99,
													Url = "www.bbc.co.uk",
													ExternalId = "3",
													LensPostingId = "4"
			                 	};

			#endregion

			// Act
			var result = postingDto.ToPostingEnvelope();

			// Assert
			result.ShouldNotBeNull();
		}

		[Test]
		public void ToPostingDto_WhenPassedValidJobPostingXml_ReturnsPostingDto()
		{
			// Arrange

			#region Arrange

			var jobPostingXml = @"<?xml version='1.0' encoding='iso-8859-1'?><jobposting><postinghtml>&lt;html&gt;
  &lt;body&gt;
    &lt;p&gt;&lt;img src=""http://talent-testnextgen.burning-glass.com/logo/4779742""&gt;&lt;/p&gt;
    &lt;p&gt;&lt;b&gt;Senior Adults Director&lt;/b&gt;&lt;br&gt;Test Company 01 Ltd A&lt;br&gt;Testerville, ID (00000)&lt;br&gt;Number of openings: 21&lt;br&gt;Application closing date: 7/25/2013&lt;br&gt;&lt;br&gt;Salary range $123.00 - $3434.00 Weekly&lt;br&gt;Normal working days are Monday and Saturday&lt;br&gt;Healthcare employer and Advanced manufacturing employer&lt;/p&gt;
    &lt;p&gt;* Provide overall direction and leadership in  private sector organization.
&lt;br&gt;* Determine company policy
&lt;br&gt;* Report to the board of directors
&lt;br&gt;* Plan operational activities
&lt;br&gt;* Direct operational activities at the highest level of management
&lt;br&gt;&lt;/p&gt;
    &lt;p&gt;&lt;b&gt;Requirements&lt;/b&gt;&lt;br&gt;Applicants must have at least a High school diploma/GED or equivalent&lt;br&gt;Applicants must have at least 1 years 2 months experience&lt;br&gt;Applicants must be at least 34 (No reason)&lt;br&gt;Applicants must hold a Motorcycle driving license with Pass transport, Hazardous materials, Tank vehicle and Tank hazard endorsements&lt;br&gt;Applicants must hold the occupational license of No License&lt;br&gt;Applicants must hold No certificate&lt;br&gt;Applicant must Know driving&lt;br&gt;Applicant must Know how to play games&lt;br&gt;Applicant must Know how to fly kite&lt;/p&gt;
    &lt;p&gt;&lt;b&gt;Benefits&lt;/b&gt;&lt;br&gt;Leave benefits include Paid holidays and Sick&lt;br&gt;Retirement benefits include Pension plan&lt;br&gt;Insurance benefits include Dental and Disability&lt;/p&gt;
    &lt;p&gt;&lt;b&gt;About Test Company 01 Ltd A&lt;/b&gt;&lt;br&gt;Testing Company.....&lt;/p&gt;
    &lt;p&gt;&lt;b&gt;How to apply&lt;/b&gt;&lt;br&gt;&lt;a href=""http://careerexplorerdev.usdev.burninglass.com"" target=""_blank""&gt;Log in to OK Job Match and submit your resume&lt;/a&gt;&lt;/p&gt;
    &lt;p&gt;&lt;small&gt;REF/29441/4784121&lt;/small&gt;&lt;/p&gt;
  &lt;/body&gt;
&lt;/html&gt;</postinghtml>
<joburl></joburl><JobDoc>
  <special>
    <cf001>1</cf001>
    <cf002>734894</cf002>
    <cf003>735076</cf003>
    <cf004>0</cf004>
    <cf005>0</cf005>
    <cf006>1</cf006>
    <cf007>FT0004784121</cf007>
    <cf008>11101100</cf008>
    <cf009>0</cf009>
    <cf010>0</cf010>
    <cf011>1</cf011>
    <cf012>7</cf012>
    <cf013></cf013>
    <cf014>Test</cf014>
    <cf015>0</cf015>
    <cf016>0</cf016>
    <cf017>0</cf017>
    <cf018>0</cf018>
    <cf019>0</cf019>
    <cf020>0</cf020>
    <cf021>0</cf021>
    <cf022>0</cf022>
    <cf023>0</cf023>
    <cf024>211101100,30000Test</cf024>
    <cf025></cf025>
    <mode>update</mode>
    <originid>7</originid>
    <job_status_cd>1</job_status_cd>
    <office_id></office_id>
    <jobtitle>Senior Adults Director</jobtitle>
    <jobemployer>Test Company 01 Ltd A</jobemployer>
    <foreignpostingid>FT0004784121</foreignpostingid>
    <jobtype>4</jobtype>
    <JOBTYPE_CD>4</JOBTYPE_CD>
  <green>0</green><cf026>0</cf026></special>
  <posting>
    <contact>
      <company>Test Company 01 Ltd A</company>
      <person_title>Mr</person_title>
      <person>DevClientEmp ClientEmployee</person>
      <address>
        <street></street>
        <street></street>
        <city>Testerville</city>
        <state>ID</state>
        <county>Franklin</county>
        <postalcode>00000</postalcode>
        <country>US</country>
      </address>
      <email>client@Employee.com</email>
      <joburl></joburl>
      <phone></phone>
      <phone></phone>
      <phone_ext></phone_ext>
      <phone_ext></phone_ext>
      <fax></fax>
    </contact>
    <duties>
      <title>Senior Adults Director</title>
      <onettitle>Chief Executives</onettitle>* Provide overall direction and leadership in  private sector organization.
* Determine company policy
* Report to the board of directors
* Plan operational activities
* Direct operational activities at the highest level of management
</duties>
    <background>
      <experience>14</experience>
    </background>
    <benefits>
      <salary>123.00-3434.00 $ Weekly</salary>
    </benefits>
    <qualifications>
      <required>
        <certification>No License, No certificate</certification>
        <training></training>
      </required>
    </qualifications>
    <skills>
      <languages></languages>
    </skills>
    <jobinfo>
      <educationlevel>2</educationlevel>
      <positioncount>21</positioncount>
    </jobinfo>
  <qualifications><required><education><degree>High School Diploma</degree></education></required></qualifications></posting>
  <clientjobdata />
</JobDoc><clientjobdata><JOBDOC ORIGINATION_METHOD_CD=""7"" MODE=""update"" ID=""FT0004784121""><JOB_CREATE_DATE>2013-01-24</JOB_CREATE_DATE><JOB_TITLE>Senior Adults Director</JOB_TITLE><JOB_STATUS_CD>1</JOB_STATUS_CD><JOB_CATEGORY_CD>A</JOB_CATEGORY_CD><OFFICE_ID /><COMPANY>Test Company 01 Ltd A</COMPANY><COMPANY_NAICS>Testing</COMPANY_NAICS><EMPLOYER_CONTACT_TITLE>Mr</EMPLOYER_CONTACT_TITLE><EMPLOYER_CONTACT_FIRST_NAME>DevClientEmp</EMPLOYER_CONTACT_FIRST_NAME><EMPLOYER_CONTACT_LAST_NAME>ClientEmployee</EMPLOYER_CONTACT_LAST_NAME><JOB_CONTACT_ADDR_1 /><JOB_CONTACT_ADDR_2 /><JOB_CONTACT_CITY> </JOB_CONTACT_CITY><JOB_CONTACT_STATE>KY</JOB_CONTACT_STATE><JOB_CONTACT_ZIP>40601</JOB_CONTACT_ZIP><JOB_CONTACT_COUNTRY>US</JOB_CONTACT_COUNTRY><JOB_CONTACT_COUNTY>21073</JOB_CONTACT_COUNTY><JOB_CONTACT_PHONE_PRI /><JOB_CONTACT_PHONE_PRI_EXT /><JOB_CONTACT_PHONE_SEC /><JOB_CONTACT_PHONE_SEC_EXT /><JOB_CONTACT_URL_PRI /><JOB_CONTACT_FAX_PRI /><JOB_CONTACT_EMAIL>client@Employee.com</JOB_CONTACT_EMAIL><CONTACT_SEND_DIRECT_FLAG>0</CONTACT_SEND_DIRECT_FLAG><CONTACT_POSTAL_FLAG>0</CONTACT_POSTAL_FLAG><CONTACT_PHONE_FLAG>0</CONTACT_PHONE_FLAG><CONTACT_FAX_FLAG>0</CONTACT_FAX_FLAG><CONTACT_EMAIL_FLAG>0</CONTACT_EMAIL_FLAG><CONTACT_URL_FLAG>0</CONTACT_URL_FLAG><CONTACT_TALENT_FLAG>1</CONTACT_TALENT_FLAG><SAL_MIN>123.00</SAL_MIN><SAL_MAX>3434.00</SAL_MAX><SAL_UNIT_CD>3</SAL_UNIT_CD><EDUCATION_CD>2</EDUCATION_CD><JOB_REQUIRED_LIC_CERT>No License, No certificate</JOB_REQUIRED_LIC_CERT><JOB_REQUIRED_TRAINING /><EXPERIENCE_IN_MONTHS>14</EXPERIENCE_IN_MONTHS><DURATION>1</DURATION><JOB_DESCRIPTION>* Provide overall direction and leadership in  private sector organization.
* Determine company policy
* Report to the board of directors
* Plan operational activities
* Direct operational activities at the highest level of management
</JOB_DESCRIPTION><JOB_LAST_OPEN_DATE>2013-07-25</JOB_LAST_OPEN_DATE><WORK_VARY_FLAG>0</WORK_VARY_FLAG><WORK_MON_FLAG>1</WORK_MON_FLAG><WORK_TUE_FLAG>0</WORK_TUE_FLAG><WORK_WED_FLAG>0</WORK_WED_FLAG><WORK_THU_FLAG>0</WORK_THU_FLAG><WORK_FRI_FLAG>0</WORK_FRI_FLAG><WORK_SAT_FLAG>1</WORK_SAT_FLAG><WORK_SUN_FLAG>0</WORK_SUN_FLAG><DRIVER_CLASS_CD>5</DRIVER_CLASS_CD><SHORTHAND_SPEED>0</SHORTHAND_SPEED><TYPING_SPEED>0</TYPING_SPEED><AGE_MIN>34</AGE_MIN><ONET_CODE>11101100</ONET_CODE><ONET_TITLE>Chief Executives</ONET_TITLE><REFERRAL_DESIRED_CT>0</REFERRAL_DESIRED_CT><REFERRAL_PROVIDED_CT>0</REFERRAL_PROVIDED_CT><POSITION_CT>21</POSITION_CT><JOB_LOCATION_CITY>Testerville</JOB_LOCATION_CITY><JOB_LOCATION_STATE>ID</JOB_LOCATION_STATE><JOB_LOCATION_ZIP>00000</JOB_LOCATION_ZIP><BENEFITS><BENEFIT401K>0</BENEFIT401K><MEDICALLEAVE>0</MEDICALLEAVE><LEAVESHARING>0</LEAVESHARING><PROFITSHARING>0</PROFITSHARING><DISABILITYINSURANCE>1</DISABILITYINSURANCE><HEALTHSAVINGS>0</HEALTHSAVINGS><LIFEINSURANCE>0</LIFEINSURANCE><VISIONINSURANCE>0</VISIONINSURANCE><RELOCATION>0</RELOCATION><TUITIONASSISTANCE>0</TUITIONASSISTANCE><BENEFITSNEGOTIABLE>0</BENEFITSNEGOTIABLE><OTHER>1</OTHER></BENEFITS><LANGUAGES><ARABIC>0</ARABIC><CHINESE>0</CHINESE><ENGLISH>0</ENGLISH><FRENCH>0</FRENCH><GERMAN>0</GERMAN><ITALIAN>0</ITALIAN><JAPANESE>0</JAPANESE><RUSSIAN>0</RUSSIAN><SPANISH>0</SPANISH><OTHER>0</OTHER></LANGUAGES><JOB_REQUIREMENTS><BONDING>0</BONDING><CREDITCHECK>0</CREDITCHECK><CRIMINALCHECK>0</CRIMINALCHECK><DRUGSCREEN>0</DRUGSCREEN><EMPLOYERTESTING>0</EMPLOYERTESTING><HEAVYLIFTING>0</HEAVYLIFTING><JOINUNION>0</JOINUNION><OWNTOOLS>0</OWNTOOLS><PHYSICALEXAM>0</PHYSICALEXAM><OTHER>0</OTHER><OVERTIMEREQUIRED>0</OVERTIMEREQUIRED><HL_MINLBS>0</HL_MINLBS></JOB_REQUIREMENTS><REQUIREMENTS screening=""-1""><REQUIREMENT type=""AGE"" mandate=""1""><MIN>34</MIN><REASON>No reason</REASON></REQUIREMENT><REQUIREMENT type=""CERTIFICATIONS"" mandate=""1""><CERTIFICATE>No certificate</CERTIFICATE></REQUIREMENT><REQUIREMENT type=""DRIVERS_LICENSE"" mandate=""1""><CLASS>Motorcycle</CLASS><ENDORSEMENTS>Pass transport, Hazardous materials, Tank vehicle, Tank hazard</ENDORSEMENTS></REQUIREMENT><REQUIREMENT type=""LICENSES"" mandate=""1""><LICENSE>No License</LICENSE></REQUIREMENT><REQUIREMENT type=""MIN_EDU"" mandate=""1"">High school diploma or equivalent</REQUIREMENT><REQUIREMENT type=""SPECIAL"" mandate=""1""><SPECIAL>Know driving</SPECIAL><SPECIAL>Know how to play games</SPECIAL><SPECIAL>Know how to fly kite</SPECIAL></REQUIREMENT></REQUIREMENTS></JOBDOC></clientjobdata><REQUIREMENTS />
<jobstatus>ACTIVE</jobstatus><special><jobviewed count=""2"" /><match jobseekermatchcount=""0"" veteranmatchcount=""0"" /></special></jobposting>";

			#endregion

			// Act
			var result = jobPostingXml.ToPostingDto("TestJob", MockAppSettings.JobPostingStylePath, MockAppSettings.SetExternalIdOnSpideredPostings);

			// Assert
			result.ShouldNotBeNull();
		}

		[Test]
		public void ToPostingDto_WhenPassedValidJobPostingXmlWithNoHtml_ReturnsPostingDtoWithHtml()
		{
			// Arrange

			#region Arrange

			var jobPostingXml = @"<?xml version=""1.0"" encoding=""utf-8""?>
<jobposting>
  <postinghtml></postinghtml>
  <joburl>https://www.ghcjobs.apply2jobs.com/ProfExt/index.cfm?fuseaction=mExternal.showJob&amp;amp;RID=119973&amp;amp;CurrentPage=122</joburl>
  <JobDoc>
    <posting sic=""809907"" sic2=""80"" dateversion=""2"" iso8601=""2015-03-31"" present=""735690"" inferred-naics=""72"">
      <contact>
        <person_title>
        </person_title>
        <person>
        </person>
        <url>https://www.ghcjobs.apply2jobs.com/ProfExt/index.cfm?fuseaction=mExternal.showJob&amp;amp;RID=119973&amp;amp;CurrentPage=122</url>
        <phone area="""" type="""">
        </phone>
        <address majorcity=""FLORENCE"" lat=""38.9915"" state=""KY"" inferred-city=""FLORENCE"" lon=""-84.6429"" inferred-state=""KY"">
          <state abbrev=""KY"">KY</state>
          <city majorcity=""FLORENCE"">Florence</city>
          <postalcode>41022</postalcode>
          <country>US</country>
          <county>
          </county>
        </address>
        <fax>
        </fax>
        <email>
        </email>
        <joburl invalid=""true"">https://www.ghcjobs.apply2jobs.com/ProfExt/index.cfm?fuseaction=mExternal.showJob&amp;amp;RID=119973&amp;amp;CurrentPage=122</joburl>
        <company />
      </contact>
      <duties>
        <title bgonetmodel-topic-onet=""35-3041.00|25-1194.00"" blacklist=""false"" clean=""Dietary Aide"" internship-flag=""false"" laytitles=""dietary aide"" occupation=""Dietetic Technicians"" onet=""29-2051.00"" onetrank=""4"" plurality=""false"" root=""aide"" soc=""29-2051.00"" standardtitle=""Dietary Aide"" std=""Dietary Aide:12402"">Dietary Aide</title>
        <onettitle>(Hierarchy Root)</onettitle>POSITION SUMMARY: The Dietary Aide performs a variety of food service functions in maintaining clean and sanitary conditions of food service areas, facilities, and equipment. Assists in some aspects of food preparation.

	RESPONSIBILITIES/ACCOUNTABILITIES: 
1. Reports at assigned time, in uniform, ready for duty; 
2. Handles food and equipment according to sanitation policies and procedures; 
3. Operates equipment in a safe manner; 
4. Reports any discrepancies in his/her area to department supervisor; 
5. Delivers and retrieves food carts to and from customer area; 
6. Sets up trays for meals/works tray line position; 
7. Processes soiled trays, dishes and utensils from carts through dishwasher; 
8. Controls use of soap and cleaning agents in dishwasher;
9. Returns clean items to appropriate places; 
10. Assists in the serving of food on the tray line; 
11. Washes pots, pans and other equipment as necessary; 
12. Maintains cleanliness and sanitation through entire work areas; 
13. Performs daily or scheduled cleaning duties, in accordance with established policies and procedures; Scrubs and mops floors, cleans and sanitizes equipment; 
14. Empties trash contains regularly; 
15. Stocks food and paper goods in the storeroom area; 
16. Prepares snacks and beverages from between-meal service to customers; 
17. Prepares nutritional supplements and tube feeding products and delivers to customer areas; 
18. Reports malfunctioning equipment to the Food Service Director; 
19. Puts Customer Service First: Ensures that customers and families receive the highest quality of service in an attentive and responsive atmosphere which recognizes the individuals' needs and rights; 
20. Performs other tasks as assigned by the Food Service Director. 

	 

	AD01 

Qualifications:

	SPECIFIC EDUCATIONAL/VOCATIONAL REQUIREMENTS: 
1. Ability to read, write, and understand directions in the English language. 
2. High school diploma or equivalent is preferred.

	 

	Genesis HealthCare, LLC and all affiliated entities are proud to practice Equal Employment Opportunity and Affirmative Action.  Genesis provides equal employment opportunity (EEO) to all employees and applicants for employment without regard to race, color, religion, gender, sexual orientation, national origin, age, actual or perceived disability, marital status, genetic information, amnesty, or status as covered veterans in accordance with applicable federal, state, and local laws.  Genesis takes affirmative action to employ and advance in employment qualified minorities, women, individuals with disabilities and covered veterans.

AJE Ref Number: 568837173</duties>
      <benefits healthcarebenefits=""0"">
        <salary>
        </salary>
      </benefits>
      <qualifications>
        <required>
          <area>
          </area>
          <certification />
          <education>
            <major>
            </major>
            <degree level=""12"" name=""Higher Secondary Certificate"">
            </degree>
          </education>
          <skill>Cleaning|English|Product Sale and Delivery|SOAP|Tube Feeding</skill>
        </required>
        <preferred>
          <education>
            <major std-major="""" cipcode="""" code="""" />
            <degree level=""12"" name=""Higher Secondary Certificate"" />
          </education>
          <skill>Cleaning|English|Product Sale and Delivery|SOAP|Tube Feeding</skill>
          <area>
          </area>
        </preferred>
      </qualifications>
      <jobinfo>
        <educationlevel>2</educationlevel>
        <positioncount>1</positioncount>
      </jobinfo>
      <background>
        <experience>Experience not required</experience>
        <experience>Experience not required</experience>
      </background>
    </posting>
    <special>
      <MinExperienceLevel />
      <MaxExperienceLevel />
      <ConsolidatedDegree>Higher Secondary Certificate</ConsolidatedDegree>
      <ConsolidatedDegreeLevel>12</ConsolidatedDegreeLevel>
      <foreignpostingid>JE3903300</foreignpostingid>
      <green>0</green>
      <joburl>https://www.ghcjobs.apply2jobs.com/ProfExt/index.cfm?fuseaction=mExternal.showJob&amp;amp;RID=119973&amp;amp;CurrentPage=122</joburl>
      <jobemployer>Genesis Healthcare</jobemployer>
      <website>
      </website>
      <jobtitle>Dietary Aide</jobtitle>
      <jobdate>12/05/2014</jobdate>
      <closedate>12/05/2015</closedate>
      <SAL_MIN />
      <SAL_MAX />
      <SAL_UNIT_CD />
      <COMPUTED_ANNUAL_PAY />
      <educationlevel>2</educationlevel>
      <originid>2</originid>
      <intermediary />
      <mode>update</mode>
      <job_status_cd>1</job_status_cd>
      <office_id>KY0099</office_id>
      <onet12>00000000</onet12>
      <cf001>1</cf001>
      <cf002>735574</cf002>
      <cf003>735939</cf003>
      <cf004>0</cf004>
      <cf005>0</cf005>
      <cf006 />
      <cf007>76384</cf007>
      <cf008>00000000</cf008>
      <cf009>33</cf009>
      <cf010 />
      <cf011 />
      <cf012>2</cf012>
      <cf013>80</cf013>
      <cf014>6231,7211,7225,7223,7224,7213,7212</cf014>
      <cf015>0</cf015>
      <cf016>12</cf016>
      <cf017>0</cf017>
      <cf018>0</cf018>
      <cf019>0</cf019>
      <cf020>0</cf020>
      <cf021>0</cf021>
      <cf022>0</cf022>
      <cf023>0</cf023>
      <cf024>200000000,300006231,300007211,300007225,300007223,300007224,300007213,300007212</cf024>
      <cf025 />
      <cf026>0</cf026>
      <cf027>9</cf027>
      <cf028 />
      <cf029>0,0</cf029>
      <cf030>
      </cf030>
      <cf031>35304192</cf031>
      <cf032 />
      <cf033 />
      <cf034 />
      <cf035 />
      <cf036 />
      <cf037 />
      <cf038 />
      <cf039 />
      <cf040 />
      <cf041 />
      <cf042 />
      <cf043 />
      <cf044 />
      <cf045 />
      <cf046 />
      <cf047 />
      <cf048 />
      <cf049 />
      <cf050 />
    </special>
    <skillrollup version=""1"">
      <canonskill name=""Cleaning"" skill-cluster=""Specialized Skills"">
        <variant ruleid=""6664"">cleaning</variant>
      </canonskill>
      <canonskill name=""English"" skill-cluster=""Common Skills: Language"">
        <variant ruleid=""13537"">English</variant>
      </canonskill>
      <canonskill name=""Product Sale and Delivery"" skill-cluster=""Sales: General; Specialized Skills"">
        <variant ruleid=""32470"">food</variant>
        <variant ruleid=""32476"">products</variant>
      </canonskill>
      <canonskill name=""SOAP"" skill-cluster=""Specialized Skills"">
        <variant ruleid=""37227"">soap</variant>
      </canonskill>
      <canonskill name=""Tube Feeding"" skill-cluster=""Specialized Skills"">
        <variant ruleid=""41603"">tube feeding</variant>
      </canonskill>
    </skillrollup>
  </JobDoc>
  <clientjobdata>
    <JOBDOC MODE=""update"" ID=""JE3903300"" ORIGINATION_METHOD_CD=""2"">
      <JOB_CREATE_DATE>2014-12-05</JOB_CREATE_DATE>
      <JOB_TITLE>Dietary Aide</JOB_TITLE>
      <JOB_STATUS_CD>1</JOB_STATUS_CD>
      <OFFICE_ID>KY0099</OFFICE_ID>
      <COMPANY>Genesis Healthcare</COMPANY>
      <COMPANY_NAICS>623110</COMPANY_NAICS>
      <EMPLOYER_CONTACT_TITLE />
      <EMPLOYER_CONTACT_FIRST_NAME />
      <EMPLOYER_CONTACT_LAST_NAME />
      <JOB_CONTACT_ADDR_1>Address Not Provided</JOB_CONTACT_ADDR_1>
      <JOB_CONTACT_ADDR_2 />
      <JOB_CONTACT_CITY>Florence</JOB_CONTACT_CITY>
      <JOB_CONTACT_STATE>KY</JOB_CONTACT_STATE>
      <JOB_CONTACT_ZIP>41022</JOB_CONTACT_ZIP>
      <JOB_CONTACT_COUNTRY>US</JOB_CONTACT_COUNTRY>
      <JOB_CONTACT_COUNTY>21015</JOB_CONTACT_COUNTY>
      <JOB_CONTACT_PHONE_PRI />
      <JOB_CONTACT_PHONE_PRI_EXT />
      <JOB_CONTACT_PHONE_SEC />
      <JOB_CONTACT_PHONE_SEC_EXT />
      <JOB_CONTACT_URL_PRI>https://www.ghcjobs.apply2jobs.com/ProfExt/index.cfm?fuseaction=mExternal.showJob&amp;amp;RID=119973&amp;amp;CurrentPage=122</JOB_CONTACT_URL_PRI>
      <JOB_CONTACT_FAX_PRI />
      <JOB_CONTACT_EMAIL />
      <SAL_MIN />
      <SAL_MAX />
      <SAL_UNIT_CD />
      <EDUCATION_CD>2</EDUCATION_CD>
      <JOB_REQUIRED_LIC_CERT />
      <JOB_REQUIRED_TRAINING />
      <EXPERIENCE_IN_MONTHS>Experience not required</EXPERIENCE_IN_MONTHS>
      <DURATION>3</DURATION>
      <SHIFT_CD />
      <JOB_DESCRIPTION>POSITION SUMMARY: The Dietary Aide performs a variety of food service functions in maintaining clean and sanitary conditions of food service areas, facilities, and equipment. Assists in some aspects of food preparation.

	RESPONSIBILITIES/ACCOUNTABILITIES: 
1. Reports at assigned time, in uniform, ready for duty; 
2. Handles food and equipment according to sanitation policies and procedures; 
3. Operates equipment in a safe manner; 
4. Reports any discrepancies in his/her area to department supervisor; 
5. Delivers and retrieves food carts to and from customer area; 
6. Sets up trays for meals/works tray line position; 
7. Processes soiled trays, dishes and utensils from carts through dishwasher; 
8. Controls use of soap and cleaning agents in dishwasher;
9. Returns clean items to appropriate places; 
10. Assists in the serving of food on the tray line; 
11. Washes pots, pans and other equipment as necessary; 
12. Maintains cleanliness and sanitation through entire work areas; 
13. Performs daily or scheduled cleaning duties, in accordance with established policies and procedures; Scrubs and mops floors, cleans and sanitizes equipment; 
14. Empties trash contains regularly; 
15. Stocks food and paper goods in the storeroom area; 
16. Prepares snacks and beverages from between-meal service to customers; 
17. Prepares nutritional supplements and tube feeding products and delivers to customer areas; 
18. Reports malfunctioning equipment to the Food Service Director; 
19. Puts Customer Service First: Ensures that customers and families receive the highest quality of service in an attentive and responsive atmosphere which recognizes the individuals' needs and rights; 
20. Performs other tasks as assigned by the Food Service Director. 

	 

	AD01 

Qualifications:

	SPECIFIC EDUCATIONAL/VOCATIONAL REQUIREMENTS: 
1. Ability to read, write, and understand directions in the English language. 
2. High school diploma or equivalent is preferred.

	 

	Genesis HealthCare, LLC and all affiliated entities are proud to practice Equal Employment Opportunity and Affirmative Action.  Genesis provides equal employment opportunity (EEO) to all employees and applicants for employment without regard to race, color, religion, gender, sexual orientation, national origin, age, actual or perceived disability, marital status, genetic information, amnesty, or status as covered veterans in accordance with applicable federal, state, and local laws.  Genesis takes affirmative action to employ and advance in employment qualified minorities, women, individuals with disabilities and covered veterans.

AJE Ref Number: 568837173</JOB_DESCRIPTION>
      <JOB_LAST_OPEN_DATE>2015-12-05</JOB_LAST_OPEN_DATE>
      <JOB_CATEGORY_CD />
      <WORK_VARY_FLAG />
      <WORK_MON_FLAG />
      <WORK_TUE_FLAG />
      <WORK_WED_FLAG />
      <WORK_THU_FLAG />
      <WORK_FRI_FLAG />
      <WORK_SAT_FLAG />
      <WORK_SUN_FLAG />
      <DRIVER_CLASS_CD />
      <SHORTHAND_SPEED />
      <TYPING_SPEED />
      <AGE_MIN />
      <ONET_CODE>00000000</ONET_CODE>
      <ONET_TITLE>(Hierarchy Root)</ONET_TITLE>
      <CONTACT_SEND_DIRECT_FLAG />
      <CONTACT_POSTAL_FLAG />
      <CONTACT_PHONE_FLAG />
      <CONTACT_FAX_FLAG />
      <CONTACT_EMAIL_FLAG />
      <CONTACT_URL_FLAG>1</CONTACT_URL_FLAG>
      <REFERRAL_DESIRED_CT />
      <REFERRAL_PROVIDED_CT>3</REFERRAL_PROVIDED_CT>
      <POSITION_CT>1</POSITION_CT>
      <JOB_LOCATION_ADDR1 />
      <JOB_LOCATION_ADDR2 />
      <JOB_LOCATION_CITY>Florence</JOB_LOCATION_CITY>
      <JOB_LOCATION_STATE>KY</JOB_LOCATION_STATE>
      <JOB_LOCATION_ZIP>41022</JOB_LOCATION_ZIP>
      <BENEFITS>
        <BENEFIT401K>0</BENEFIT401K>
        <MEDICALLEAVE>0</MEDICALLEAVE>
        <LEAVESHARING>0</LEAVESHARING>
        <PROFITSHARING>0</PROFITSHARING>
        <DISABILITYINSURANCE>0</DISABILITYINSURANCE>
        <HEALTHSAVINGS>0</HEALTHSAVINGS>
        <LIFEINSURANCE>0</LIFEINSURANCE>
        <VISIONINSURANCE>0</VISIONINSURANCE>
        <RELOCATION>0</RELOCATION>
        <TUITIONASSISTANCE>0</TUITIONASSISTANCE>
        <BENEFITSNEGOTIABLE>0</BENEFITSNEGOTIABLE>
        <OTHER>0</OTHER>
      </BENEFITS>
      <LANGUAGES>
        <ARABIC>0</ARABIC>
        <CHINESE>0</CHINESE>
        <ENGLISH>0</ENGLISH>
        <FRENCH>0</FRENCH>
        <GERMAN>0</GERMAN>
        <ITALIAN>0</ITALIAN>
        <JAPANESE>0</JAPANESE>
        <RUSSIAN>0</RUSSIAN>
        <SPANISH>0</SPANISH>
        <OTHER>0</OTHER>
      </LANGUAGES>
      <JOB_REQUIREMENTS>
        <BONDING>0</BONDING>
        <CREDITCHECK>0</CREDITCHECK>
        <CRIMINALCHECK>0</CRIMINALCHECK>
        <DRUGSCREEN>0</DRUGSCREEN>
        <EMPLOYERTESTING>0</EMPLOYERTESTING>
        <HEAVYLIFTING>0</HEAVYLIFTING>
        <JOINUNION>0</JOINUNION>
        <OWNTOOLS>0</OWNTOOLS>
        <PHYSICALEXAM>0</PHYSICALEXAM>
        <OTHER>0</OTHER>
        <OVERTIMEREQUIRED>0</OVERTIMEREQUIRED>
        <HL_MINLBS>0</HL_MINLBS>
      </JOB_REQUIREMENTS>
    </JOBDOC>
  </clientjobdata>
</jobposting>";

			#endregion

			// Act
			var result = jobPostingXml.ToPostingDto("KY20150227_b7e7885f8fcf4893b06ac216c4db768a", MockAppSettings.JobPostingStylePath, MockAppSettings.SetExternalIdOnSpideredPostings);

			// Assert
			result.ShouldNotBeNull();
			result.Html.ShouldNotBeNullOrEmpty();
		}
		
		[Test]
		public void ToPostingDto_WhenPassedValidJob_ReturnsPostingDto()
		{
			// Arrange
			var job = DataCoreRepository.FindById<Job>(1003791);

			// Act
			var result = job.ToPostingDto(RuntimeContext, "OfficeA", MockCulture);

			// Assert
			result.ShouldNotBeNull();
		}

		[Test]
		public void GetContactDetails_WhenPassedValidDto_ReturnsModel()
		{
			// Arrange
			var posting = DataCoreRepository.FindById<Posting>(1177700).AsDto();

			// Act
			var result = posting.GetContactDetails();

			// Assert
			result.ShouldNotBeNull();
		}
	}
}
