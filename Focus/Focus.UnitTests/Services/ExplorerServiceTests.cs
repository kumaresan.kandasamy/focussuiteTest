﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Collections.Generic;
using System.Linq;

using Focus.Core;
using Focus.Core.Criteria;
using Focus.Core.Criteria.DegreeEducationLevel;
using Focus.Core.Criteria.DegreeEducationLevelReport;
using Focus.Core.Criteria.EmployerReport;
using Focus.Core.Criteria.Explorer;
using Focus.Core.Criteria.InternshipReport;
using Focus.Core.Criteria.Job;
using Focus.Core.Criteria.Skill;
using Focus.Core.Criteria.SkillReport;
using Focus.Core.Criteria.StudyPlace;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Models;
using Focus.Core.Messages;
using Focus.Core.Messages.ExplorerService;
using Focus.Core.Views;
using Focus.Services.Core;
using Focus.Services.Repositories.Lens;
using Focus.Services.ServiceImplementations;
using Focus.Services.Core.Onet;
using Focus.Services.Repositories;
using Focus.UnitTests.Core;
using Framework.Testing.NUnit;

using Moq;
using NUnit.Framework;

#endregion

namespace Focus.UnitTests.Services
{
  [TestFixture]
  public class ExplorerServiceTests : TestFixtureBase
  {
    private ExplorerService _service;
    private OnetToROnet _onetToROnet;
    private Mock<ILensRepository> _lensRepository;
		
    [SetUp]
    public override void SetUp()
    {
      DoSetup(true);

      SetUpCacheProvider();
      SetUpMockLogProvider();

			_lensRepository = new Mock<ILensRepository>();
			_lensRepository.Setup(x => x.IsLicensed()).Returns(true);
	    _lensRepository.Setup(x => x.ParseResume(It.IsAny<byte[]>(), It.IsAny<string>(), false)).Returns(MockResumeXml);

			((TestRepositories)RuntimeContext.Repositories).Lens = _lensRepository.Object;

			_service = new ExplorerService(RuntimeContext);

			_onetToROnet = new OnetToROnet(RuntimeContext);
    }
		
		[Test]
			public void Ping_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new PingRequest { Name = "TEST" };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.Ping(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Name.ShouldEqual("TEST");
		}
	
		[Ignore("To fix")]
		[Test]
		public void UploadResume_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new UploadResumeRequest {
																							ResumeModel = new ExplorerResumeModel
																							{
																								Resume = new ResumeDto { ResumeXml = MockResumeXml, PersonId = 1116920 },
																								ResumeDocument = new ResumeDocumentDto { ResumeId = 1192112 ,DocumentBytes = new byte[] { 32, 34, 65, 66 }, ContentType = "text", FileName = "res.text"}
																							}
																						};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.UploadResume(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.ResumeModel.ShouldNotBeNull();
		}
	
		[Test]
		public void GetInternshipJobs_WhenPassedValidRequest_ReturnsSuccessAndData()
		{
			// Arrange
			var request = new InternshipJobRequest { Criteria = new InternshipJobCriteria{ InternshipCategoryId = 1}};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetInternshipJobs(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.Jobs.Count.ShouldBeGreater(0);
		}

		[Test]
		public void GetInternshipCategories_WhenPassedValidRequest_ReturnsSuccessAndData()
		{
			// Arrange
			var request = new InternshipCategoryRequest { };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetInternshipCategories(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.InternshipCategories.Count.ShouldBeGreater(0);
		}

		[Test]
		public void GetInternshipCategories_WhenPassedValidRequestWithInternshipCategoryIds_ReturnsSuccessAndData()
		{
			// Arrange
			var request = new InternshipCategoryRequest { InternshipCategoryIds = new List<long>{1, 7}};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetInternshipCategories(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.InternshipCategories.Count.ShouldEqual(2);
		}
    
		[Test]
		public void GetInternshipsBySkill_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new SkillInternshipsRequest { Criteria = new ExplorerCriteria ()};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetInternshipsBySkill(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void GetEmployerInternships_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new EmployerInternshipRequest { Criteria = new ExplorerCriteria() };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetEmployerInternships(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}
    
		[Test]
		public void GetInternshipEmployers_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new InternshipEmployerRequest{ Criteria = new ExplorerCriteria{ InternshipCategoryId = 1}};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetInternshipEmployers(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}
    
		[Test]
		public void GetInternshipSkills_WhenPassedValidRequest_ReturnsSuccessAndData()
		{
			// Arrange
			var request = new InternshipSkillRequest {  Criteria = new InternshipSkillCriteria{ InternshipCategoryId = 1}};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetInternshipSkills(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.Skills.Count.ShouldBeGreater(0);
		}

		[Test]
		public void GetInternship_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new InternshipRequest { InternshipCategoryId = 1};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetInternship(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.Internship.ShouldNotBeNull();
		}
	
		[Test]
		public void GetInternshipReport_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
      var request = new InternshipRequest { Criteria = new InternshipReportCriteria { ReportCriteria = new ExplorerCriteria() }};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetInternshipReport(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}
    
		[Test]
		public void SendHelpEmail_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
		  var request = new SendEmailRequest { ReportCriteria = new ExplorerCriteria(), EmailAddresses = "test@test.com", EmailSubject = "UnitTest", EmailBody = "TestBody" };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.SendHelpEmail(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}
	
		[Test]
		public void SendEmail_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
      var request = new SendEmailRequest { BookmarkUrl = "http://burning-glass.com", ReportCriteria = new ExplorerCriteria { ReportItemId = 1 }, EmailAddresses = "test@test.com", EmailSubject = "UnitTest", EmailBody = "TestBody" };
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.SendEmail(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}
    
		[Test]
		public void GetSiteSearchResults_WhenPassedValidRequest_ReturnsSuccessAndData()
		{
			// Arrange
			var request = new SiteSearchRequest { SearchTerm = "software"};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetSiteSearchResults(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.SiteSearchResults.Count.ShouldBeGreater(0);
		}
    
		[Test]
		public void GetJobExperienceLevels_WhenPassedValidRequest_ReturnsSuccessAndData()
		{
			// Arrange
			var request = new JobExperienceLevelRequest { JobId = 4 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetJobExperienceLevels(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.JobExperienceLevels.Count.ShouldBeGreater(0);
		}
    
		[Test]
		public void GetJobEducationRequirements_WhenPassedValidRequest_ReturnsSuccessAndData()
		{
			// Arrange
			var request = new JobEducationRequirementRequest { JobId = 4};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetJobEducationRequirements(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.JobEducationRequirements.Count.ShouldBeGreater(0);
		}
	
		[Test]
		public void GetJobJobTitles_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new JobTitleRequest { JobId = 4};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetJobJobTitles(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.JobTitles.Count.ShouldBeGreater(0);
		}
    
		[Test]
		public void GetSkillEmployers_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
      var request = new SkillEmployerRequest { Criteria = new ExplorerCriteria { SkillId = 21701, StateAreaId = 976} };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetSkillEmployers(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.SkillEmployers.Count.ShouldBeGreater(0);
		}
    
		[Test]
		public void GetSkillJobReport_WhenPassedValidRequest_ReturnsSuccessAndData()
		{
			// Arrange
      var request = new JobReportRequest { JobDegreeTierLevel = 2, Criteria = new ExplorerCriteria { SkillId = 21701, StateAreaId = 976 } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetSkillJobReport(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.JobReports.Count.ShouldBeGreater(0);
		}
    
		[Test]
		public void GetSkillReport_WhenPassedValidRequest_ReturnsSuccessAndData()
		{
			// Arrange
      var request = new SkillRequest {  Criteria = new SkillReportCriteria { FetchOption = CriteriaBase.FetchOptions.List, ReportCriteria = new ExplorerCriteria { SkillId = 21701, StateAreaId = 976 } } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetSkillReport(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.SkillReports.Count.ShouldBeGreater(0);
		}
	
		[Test]
		public void GetDegreeEducationLevelExts_WhenPassedValidRequest_ReturnsSuccessAndData()
		{
			// Arrange
			var request = new DegreeEducationLevelRequest { SearchCriteria = new DegreeEducationLevelCriteria { DegreeEducationLevelId = 10409 } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetDegreeEducationLevelExts(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.DegreeEducationLevelExts.ShouldNotBeNull();
		}
	
		[Test]
		public void GetDegreeEducationLevelStudyPlaces_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
      var request = new DegreeEducationLevelStudyPlaceRequest { Criteria = new DegreeEducationLevelStudyPlaceCriteria { DegreeEducationLevelId = 10411 } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetDegreeEducationLevelStudyPlaces(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.DegreeEducationLevelStudyPlaces.Count.ShouldBeGreater(0);
		}
    
		[Test]
		public void GetDegreeEducationLevelEmployers_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
      var request = new DegreeEducationLevelEmployerRequest { Criteria = new ExplorerCriteria { DegreeEducationLevelId = 10411,StateAreaId = 976} };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetDegreeEducationLevelEmployers(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.Employers.Count.ShouldBeGreater(0);
		}
    
		[Test]
		public void GetDegreeEducationLevelSkills_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
      var request = new DegreeEducationLevelSkillRequest { Criteria = new DegreeEducationLevelSkillCriteria { DegreeEducationLevelId = 10411, StateAreaId = 976 } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetDegreeEducationLevelSkills(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.DegreeEducationLevelSkills.Count.ShouldBeGreater(0);
		}
    
		[Test]
		public void GetDegreeEducationLevelJobReport_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new JobReportRequest { Criteria = new ExplorerCriteria{ StateAreaId = 505 }};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetDegreeEducationLevelJobReport(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.JobReports.Count.ShouldBeGreater(0);
		}
    
		[Test]
		public void GetDegreeEducationLevel_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new DegreeEducationLevelRequest { SearchCriteria = new DegreeEducationLevelCriteria { DegreeEducationLevelId = 10411 } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetDegreeEducationLevel(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.DegreeEducationLevel.ShouldNotBeNull();
		}
    
		[Test]
		public void GetDegreeEducationLevelReport_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
      var request = new DegreeEducationLevelRequest { ReportCriteria = new DegreeEducationLevelReportCriteria{ FetchOption = CriteriaBase.FetchOptions.List, ReportCriteria = new ExplorerCriteria{ StateAreaId = 976 }}};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetDegreeEducationLevelReport(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.DegreeEducationLevelReports.Count.ShouldBeGreater(0);
		}
    
		[Test]
		public void GetEmployerSkills_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
      var request = new EmployerSkillRequest { Criteria = new ExplorerCriteria { EmployerId = 32349 } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetEmployerSkills(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.EmployerSkills.Count.ShouldBeGreater(0);
		}
    
		[Test]
		public void GetEmployerJobs_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
      var request = new EmployerJobRequest { Criteria = new ExplorerCriteria { EmployerId = 32352, StateAreaId = 505} };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetEmployerJobs(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.EmployerJobs.Count.ShouldBeGreater(0);
		}

	  [Test]
		public void GetEmployer_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new EmployerRequest { EmployerId = 32352 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetEmployer(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.Employer.ShouldNotBeNull();
		}
    
		[Test]
		public void GetEmployerReport_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new EmployerRequest { EmployerId = 32352, Criteria = new EmployerReportCriteria{FetchOption = CriteriaBase.FetchOptions.List, ReportCriteria = new ExplorerCriteria{ StateAreaId = 505}} };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetEmployerReport(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.EmployerReports.Count.ShouldBeGreater(0);
		}
    
		[Test]
		public void GetJobDegreeCertifications_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new JobDegreeCertificationRequest { Criteria = new ExplorerCriteria{ CareerAreaJobId = 116 }};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetJobDegreeCertifications(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.JobDegreeCertifications.JobCertifications.Count.ShouldBeGreater(0);
		}

		[Test]
		public void GetJobDegreeReportCertifications_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new JobDegreeCertificationRequest { Criteria = new ExplorerCriteria{StateAreaId = 505, CareerAreaJobId = 430}};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetJobDegreeReportCertifications(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.JobDegreeCertifications.JobCertifications.Count.ShouldBeGreater(0);
		}
    
		[Test]
		public void GetJobSkills_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new JobSkillRequest { JobId = 430 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetJobSkills(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.Skills.Count.ShouldBeGreater(0);
		}

		[Test]
		public void GetJobEmployers_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new JobEmployerRequest { Criteria = new ExplorerCriteria{ CareerAreaJobId = 430, StateAreaId = 505}};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetJobEmployers(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.Employers.Count.ShouldBeGreater(0);
		}
		
		[Test]
		public void GetJobReport_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new JobReportRequest { Criteria = new ExplorerCriteria{ CareerAreaJobId = 430, StateAreaId = 505 }};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetJobReport(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.JobReport.ShouldNotBeNull();
		}
    
		[Test]
		public void GetSimilarJobReport_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new JobReportRequest { Criteria = new ExplorerCriteria{ CareerAreaJobId = 430, StateAreaId = 505 }};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetSimilarJobReport(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.JobReports.Count.ShouldBeGreater(0);
		}
    
		[Test]
		public void GetEmployers_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new EmployerRequest { SearchTerm = "software" };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetEmployers(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.Employers.Count.ShouldBeGreater(0);
		}
    
		[Test]
		public void GetJobs_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new JobStateAreaRequest { SearchTerm = "software" };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetJobs(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.Jobs.Count.ShouldBeGreater(0);
		}
    
		[Test]
		public void GetStateAreas_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new StateAreaRequest();
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetStateAreas(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.StateAreas.Count.ShouldBeGreater(0);
		}
    
		[Test]
		public void GetCareerAreas_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new CareerAreaRequest();
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetCareerAreas(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.CareerAreas.Count.ShouldBeGreater(0);
		}

		[Test]
		public void GetJobCareerAreaJobs_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new CareerAreaJobRequest { CareerAreaId = 20 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetCareerAreaJobs(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.CareerAreaJobs.Count.ShouldBeGreater(0);
		}
		
		[Test]
		public void GetDegreeAliases_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new DegreeEducationLevelRequest
				{
					SearchCriteria = new DegreeEducationLevelCriteria
						{
							DetailedDegreeLevel = DetailedDegreeLevels.GraduateOrProfessional,
							SearchTerm = "account",
							StateAreaId = 505
						}
				};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetDegreeAliases(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.DegreeAliases.Count.ShouldBeGreater(0);
		}
		
		[Test]
		public void GetJobTitles_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new JobTitleRequest { SearchTerm = "executive"};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetJobTitles(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.JobTitles.Count.ShouldBeGreater(0);
		}
		
		[Test]
		public void GetMyAccountModel_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new MyAccountRequest();
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetMyAccountModel(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Model.ShouldNotBeNull();
		}
		
		[Test]
		public void GetMyResumeModel_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new MyResumeRequest();
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetMyResumeModel(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}
		
		[Test]
		public void GetRelatedJobs_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new RelatedJobsRequest { JobTitleId = 1523 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetRelatedJobs(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Jobs.Count.ShouldBeGreater(0);
		}
		
		[Test]
		public void GetJobIntroduction_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new JobIntroductionRequest { JobId = 1 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetJobIntroduction(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.JobDescription.ShouldNotBeNullOrEmpty();
		}
	
		[Test]
		public void GetROnetFromResume_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new ROnetFromResumeRequest { ResumeXml = MockResumeXml };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			((TestRepositories)RuntimeContext.Repositories).Lens = new LensRepository(RuntimeContext);
			var service = new ExplorerService(RuntimeContext);

			// Act
			var response = service.GetROnetFromResume(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.ROnet.ShouldNotBeNull();
		}
		
		[Test]
		public void GetROnetsFromJob_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new JobIdToROnetRequest { JobId = 1 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetROnetsFromJob(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}
		
		[Test]
    public void GetOnetFromROnet_WhenPassedValidRequest_ReturnsSuccess()
    {
      // Arrange
      var request = new ROnetToOnetRequest { ROnet = "11-1011.00" };
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.GetOnetFromROnet(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.OnetDetails.Count.ShouldBeGreater(0);
    }

    [Test]
    public void GetSOCForOnet_WhenPassedValidRequest_ReturnsSuccess()
    {
      // Arrange
      var request = new OnetToSOCRequest { OnetId = 113736 };
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.GetSOCForOnet(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.SOCs.Count.ShouldBeGreater(0);
    }

		[Test]
		public void GetDegrees_WhenPassedRequestForSpecificDegree_ReturnsSuccessWithData()
		{
			// Arrange
			var request = new DegreeRequest();
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			request.DegreeId = 4193;

			// Act
			var response = _service.GetDegrees(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Degree.ShouldNotBeNull();
			response.Degree.Id.ShouldEqual(request.DegreeId);
		}

		[Test]
		public void GetDegrees_WhenPassedRequestForInvalidDegree_ReturnsFailure()
		{
			// Arrange
			var request = new DegreeRequest();
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			request.DegreeId = -1;

			// Act
			var response = _service.GetDegrees(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.Degree.ShouldBeNull();
		}

    [Test]
    public void GetProgramAreas_WhenPassedRequestForAll_ReturnsSuccessWithData()
    {
			// Arrange
      var request = new ProgramAreaRequest();
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
      var response = _service.GetProgramAreas(request);

			// Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.ProgramAreas.ShouldNotBeNull();
    }

    [Test]
    public void GetProgramAreas_WhenPassedRequestForSpecificArea_ReturnsSuccessWithData()
    {
			// Arrange
      var request = new ProgramAreaRequest();
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
      request.ProgramAreaId = 13;

			// Act
      var response = _service.GetProgramAreas(request);

			// Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.ProgramArea.ShouldNotBeNull();
      response.ProgramArea.Id.ShouldEqual(request.ProgramAreaId);
    }

    [Test]
    public void GetProgramAreas_WhenPassedRequestForInvalidArea_ReturnsFailure()
    {
			// Arrange
      var request = new ProgramAreaRequest();
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
      request.ProgramAreaId = -1;

			// Act
      var response = _service.GetProgramAreas(request);

			// Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
      response.ProgramArea.ShouldBeNull();
    }

    [Test]
    public void GetProgramAreaDegrees_WhenPassedRequestForAll_ReturnsSuccessWithData()
    {
			// Arrange
      var request = new ProgramAreaRequest();
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
      var response = _service.GetProgramAreaDegrees(request);

			// Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.ProgramAreaDegrees.ShouldNotBeNull();
    }

    [Test]
    public void GetProgramAreaDegrees_WhenPassedRequestForSpecificArea_ReturnsSuccessWithData()
    {
			// Arrange
      var request = new ProgramAreaRequest();
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
      request.ProgramAreaId = 1;

			// Act
      var response = _service.GetProgramAreaDegrees(request);

			// Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.ProgramAreaDegrees.ShouldNotBeNull();
      response.ProgramAreaDegrees.ForEach(pad => pad.ProgramAreaId.ShouldEqual(request.ProgramAreaId));
    }

    [Test]
    public void GetProgramAreaDegrees_WhenPassedRequestForUnknownArea_ReturnsSuccessWithEmptyList()
    {
			// Arrange
      var request = new ProgramAreaRequest();
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
      request.ProgramAreaId = -1;

			// Act
      var response = _service.GetProgramAreaDegrees(request);

			// Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.ProgramAreaDegrees.Count.ShouldEqual(0);
    }

    [Test]
    public void GetProgramAreaDegreeEducationLevels_WhenPassedRequestForAll_ReturnsSuccessWithData()
    {
			// Arrange
      var request = new ProgramAreaRequest();
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
      request.GetEducationLevels = true;

			// Act
      var response = _service.GetProgramAreaDegrees(request);

			// Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.ProgramAreaDegreeEducationLevels.ShouldNotBeNull();
    }

    [Test]
    public void GetProgramAreaDegreeEducationLevels_WhenPassedRequestForSpecificArea_ReturnsSuccessWithData()
    {
			// Arrange
      var request = new ProgramAreaRequest();
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
      request.ProgramAreaId = 1;
      request.GetEducationLevels = true;

			// Act
      var response = _service.GetProgramAreaDegrees(request);

			// Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.ProgramAreaDegreeEducationLevels.ShouldNotBeNull();
      response.ProgramAreaDegreeEducationLevels.ForEach(pad => pad.ProgramAreaId.ShouldEqual(request.ProgramAreaId));
    }

    [Test]
    public void GetProgramAreaDegreeEducationLevels_WhenPassedRequestForUnknownArea_ReturnsSuccessWithEmptyList()
    {
			// Arrange
      var request = new ProgramAreaRequest();
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
      request.ProgramAreaId = -11;
      request.GetEducationLevels = true;

			// Act
      var response = _service.GetProgramAreaDegrees(request);

			// Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.ProgramAreaDegreeEducationLevels.Count.ShouldEqual(0);
    }

    [Test]
    public void GetJobFromROnet_WhenPassedSingleROnet_ReturnsSuccessWithData()
    {
			// Arrange
      var request = new JobFromROnetRequest();
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
      request.ROnet = "11-3031.00";

			// Act
      var response = _service.GetJobFromROnet(request);

			// Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.Job.ShouldNotBeNull();
      response.Job.ROnet.ShouldEqual(request.ROnet);
    }

    [Test]
    public void GetJobFromROnet_WhenPassedUnknownROnet_ReturnsSuccessWithEmptyJob()
    {
			// Arrange
      var request = new JobFromROnetRequest();
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
      request.ROnet = "XX-XXXX.XX";

			// Act
      var response = _service.GetJobFromROnet(request);

			// Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.Job.ShouldBeNull();
    }

    [Test]
    public void GetJobFromROnet_WhenPassedROnetList_ReturnsSuccessWithData()
    {
			// Arrange
      var request = new JobFromROnetRequest();
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
      request.ROnets = new List<string> { "11-1011.00", "11-1011.03", "11-1021.91" };

			// Act
      var response = _service.GetJobFromROnet(request);

			// Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.Jobs.ShouldNotBeNull();
      response.Jobs.Count.ShouldEqual(3);

      var list = response.Jobs.Select(j => j.ROnet).ToList();
      request.ROnets.ForEach(r => list.Contains(r).ShouldBeTrue());
    }

    [Test]
    public void GetJobFromROnet_WhenPassedUnknownROnetList_ReturnsSuccessWithEmptyList()
    {
			// Arrange
      var request = new JobFromROnetRequest();
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
      request.ROnets = new List<string> { "XX-XXXX.XX", "YY-YYYY.YY", "ZZ-ZZZZ.ZZ" };

			// Act
      var response = _service.GetJobFromROnet(request);

			// Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.Jobs.Count.ShouldEqual(0);
    }

    [Test]
    public void GetJobCareerPaths_WhenPassedJob_ReturnsSuccessWithData()
    {
			// Arrange
      var request = new JobCareerPathRequest();
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
      request.JobId = 1;

			// Act
      var response = _service.GetJobCareerPaths(request);

			// Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.CareerPaths.ShouldNotBeNull();
      response.CareerPaths.Job.Id.ShouldEqual(request.JobId);
    }

    [Test]
    public void GetJobCareerPaths_WhenPassedJobAndStateArea_ReturnsSuccessWithData()
    {
			// Arrange
      var request = new JobCareerPathRequest();
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
      request.JobId = 1;
      request.HiringDemandStateAreaId = 505;

			// Act
      var response = _service.GetJobCareerPaths(request);

			// Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.CareerPaths.ShouldNotBeNull();
      response.CareerPaths.Job.Id.ShouldEqual(request.JobId);
      response.CareerPaths.JobReportDetails.ReportData.StateAreaId.ShouldEqual(request.HiringDemandStateAreaId);
      response.CareerPaths.CareerPathTo.ForEach(cp => cp.JobReportDetails.ReportData.StateAreaId.ShouldEqual(request.HiringDemandStateAreaId));
    }

		[Test]
		public void GetSkill_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new SkillRequest { SkillId = 11010 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetSkill(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Skill.ShouldNotBeNull();
			response.Skill.Id.ShouldEqual(request.SkillId);
		}

		[Test]
		public void GetSkillCategories_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new SkillCategoryRequest();
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetSkillCategories(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.SkillCategories.ShouldNotBeNull();
			response.SkillCategories.Count().ShouldNotEqual(0);
		}

		[Test]
		public void GetSkills_WhenPassed_ReturnsSuccessWithData()
		{
			// Arrange
			var request = new SkillRequest();
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetSkills(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Skills.ShouldNotBeNull();
			response.Skills.Count().ShouldNotEqual(0);
		}
		
		[Test]
		public void GetSkillsForJobs_WhenPassed_ReturnsSuccessWithData()
		{
			// Arrange
			var request = new SkillRequest();
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetSkillsForJobs(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.SkillsForJobs.ShouldNotBeNull();
			response.SkillsForJobs.Count().ShouldNotEqual(0);
		}

    [Test]
    public void GetCareerAreasForJob_WhenPassedValidJobId_ReturnsSuccessWithData()
    {
      // Arrange
      var request = new JobCareerAreaRequest();
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
      request.JobId = 2;

      // Act
      var response = _service.GetJobCareerAreas(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.CareerAreas.Count().ShouldNotEqual(0);
    }

    [Test]
    public void GetCareerAreasForJob_WhenPassedInvalidJobId_ReturnsSuccessWithNoData()
    {
      // Arrange
      var request = new JobCareerAreaRequest();
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
      request.JobId = 222222222;

      // Act
      var response = _service.GetJobCareerAreas(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.CareerAreas.Count().ShouldEqual(0);
    }

    [Test]
    public void GetCareerArea_WhenPassedValidRequest_ReturnsSuccessWithData()
    {
      // Arrange
      var request = new CareerAreaRequest
      {
        CareerAreaId = 15
      };
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.GetCareerArea(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.CareerArea.ShouldNotBeNull();
    }

    [Test]
    public void GetCareerAreaTotalJobs_WhenPassedValidRequest_ReturnsSuccessWithData()
    {
      // Arrange
      var request = new CareerAreaJobTotalRequest
      {
        Criteria = new ExplorerCriteria { StateAreaId = 505 }
      };
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.GetCareerAreaJobTotals(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.CareerAreaJobTotals.Count().ShouldNotEqual(0);
    }

    [Test]
    public void GetCareerAreaTotalJobs_WhenPassedValidRequestWithDegree_ReturnsSuccessWithData()
    {
      // Arrange
      var request = new CareerAreaJobTotalRequest 
      {
        Criteria = new ExplorerCriteria { StateAreaId = 505, DegreeEducationLevelId = 15647 } 
      };
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.GetCareerAreaJobTotals(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.CareerAreaJobTotals.Count().ShouldNotEqual(0);
    }
  }
}
