﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;

using Focus.Core;
using Focus.Core.Criteria;
using Focus.Core.Criteria.Activity;
using Focus.Core.Criteria.CertificateLicence;
using Focus.Core.Criteria.Document;
using Focus.Core.Criteria.EducationInternship;
using Focus.Core.Criteria.IndustryClassification;
using Focus.Core.Criteria.Language;
using Focus.Core.Criteria.NoteReminder;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Messages;
using Focus.Core.Messages.AccountService;
using Focus.Core.Messages.CoreService;
using Focus.Core.Messages.JobService;
using Focus.Core.Views;
using Focus.Data.Core.Entities;
using Focus.Services.Core;
using Focus.Services.ServiceImplementations;
using Focus.UnitTests.Core;
using Framework.Core;
using Framework.Testing.NUnit;
using Mindscape.LightSpeed.Querying;
using Constants = Focus.Core.Constants;

using NUnit.Framework;

#endregion

namespace Focus.UnitTests.Services
{
	[TestFixture]
	public class CoreServiceTests : TestFixtureBase
	{
		private CoreService _service;

		[SetUp]
		public override void SetUp()
		{
			base.SetUp();

			SetUpCacheProvider();
			SetUpMockLogProvider();
			_service = new CoreService(RuntimeContext);
		}

		#region General Tests
		
		[Test]
		public void GetGenericJobTitles_WhenPassedValidRequest_ReturnsSuccessAndData()
		{
			// Arrange
			var request = new GetGenericJobTitlesRequest { JobTitle = "software"};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetGenericJobTitles(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.JobTitles.Count.ShouldBeGreater(0);
		}
		
		[Test]
		public void GetNaics_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new GetNaicsRequest { Prefix = "oil" };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetNaics(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Naics.Count.ShouldBeGreater(0);
		}

		[Test]
		public void GetStateCityAndCountyForPostalCode_WhenPassedValidRequest_ReturnsSuccessAndData()
		{
			// Arrange
			var request = new GetStateCityAndCountyForPostalCodeRequest { PostalCode = "40013"};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetStateCityAndCountyForPostalCode(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.StateLookupId.ShouldEqual(12834);
		}
	
		[Test]
		public void EstablishLocation_WhenPassedValidRequest_ReturnsSuccessAndData()
		{
			// Arrange
			var request = new EstablishLocationRequest { PostalCode = "40013" };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.EstablishLocation(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Location.StateKey.ShouldEqual("State.KY");
		}
		
		[Test]
		public void GetEmailTemplate_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new EmailTemplateRequest { EmailTemplateType = EmailTemplateTypes.ExpiringJob };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetEmailTemplate(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.EmailTemplate.Subject.ShouldEqual("Position about to expire");
		}
		
		[Test]
		[Explicit]
		public void SaveEmailTemplate_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new SaveEmailTemplateRequest { EmailTemplate = new EmailTemplateDto
				                                                             {
					                                                             EmailTemplateType = EmailTemplateTypes.ExpiringJob,
																																			 Recipient = "Unittest@test.com",
																																			 Subject = "Position expiring",
																																			 Salutation = "Dear User",
																																			 Body = "Job expiring",
																																			 SenderEmailType = SenderEmailTypes.ClientSpecific,
																																			 ClientSpecificEmailAddress = "someemail@site.com"
				                                                             }};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.SaveEmailTemplate(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.EmailTemplate.Subject.ShouldEqual("Position expiring");
		}
		
		[Test]
		public void GetApplicationImage_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new ApplicationImageRequest { Type = ApplicationImageTypes.FocusTalentHeader };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetApplicationImage(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.ApplicationImage.Type.ShouldEqual(ApplicationImageTypes.FocusTalentHeader);
		}
		
		[Test]
		public void SaveApplicationImage_WhenPassedValidRequest_ReturnsSuccessAndImage()
		{
			// Arrange
			var request = new SaveApplicationImageRequest { ApplicationImage = new ApplicationImageDto { Type = ApplicationImageTypes.FocusTalentHeader, Image = new byte[] {32,45,76,87}}};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.SaveApplicationImage(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.ApplicationImage.Image[0].ShouldEqual(32);
		}
		
		[Test]
		public void SendFeedback_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new FeedbackRequest { Subject = "Subject", Detail = "Detail"};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.SendFeedback(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

    [Test]
		public void GetVersionInfo_WhenUsingValidRequest_ReturnsVersionInfo()
		{
			// Arrange
			var request = new GetVersionInfoRequest().Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetVersionInfo(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.VersionInfo.ShouldNotBeNull();
		}

		#endregion

    #region Activity Categories

    [Test]
		public void GetActivityCategories_WhenPassedValidRequest_ReturnsSuccessAndData()
		{
			// Arrange
			var request = new GetActivityCategoriesRequest { ActivityType = ActivityType.JobSeeker };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetActivityCategories(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.ActivityCategories.Count.ShouldBeGreater(0);
		}

    [Test]
    public void SaveActivityCategory_WhenPassedNewRequest_ReturnsSuccess()
    {
      // Arrange
      var request = new SaveActivityCategoryRequest
      {
        Name = "New Category",
        ActivityType = ActivityType.JobSeeker
      };

      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.SaveActivityCategory(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.Saved.ShouldBeTrue();
      response.ActivityCategory.ShouldNotBeNull();
      response.ActivityCategory.Id.ShouldNotBeNull();
    }

    [Test]
    public void SaveActivityCategory_WhenPassedExistingRequest_ReturnsSuccess()
    {
      // Arrange
      var request = new SaveActivityCategoryRequest
      {
        Id = 1107370,
        Name = "Assistance New",
        ActivityType = ActivityType.JobSeeker
      };

      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.SaveActivityCategory(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.Saved.ShouldBeTrue();
      response.ActivityCategory.ShouldNotBeNull();
      response.ActivityCategory.Id.ShouldNotBeNull();
    }

    [Test]
    public void SaveActivityCategory_WhenPassedNewRequestWithExistingName_ReturnsFailure()
    {
      // Arrange
      var request = new SaveActivityCategoryRequest
      {
        Name = "Assessment",
        ActivityType = ActivityType.Employer
      };

      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.SaveActivityCategory(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.Saved.ShouldBeFalse();
    }

    [Test]
    public void SaveActivityCategory_WhenPassedExistingRequestWithExistingName_ReturnsFailure()
    {
      // Arrange
      var request = new SaveActivityCategoryRequest
      {
        Id = 1107370,
        Name = "Alien Labor",
        ActivityType = ActivityType.JobSeeker
      };

      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.SaveActivityCategory(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.Saved.ShouldBeFalse();
    }

    [Test]
    public void DeleteActivityCategory_WhenPassedValidRequest_ReturnsSuccess()
    {
      // Arrange
      var request = new DeleteActivityCategoryRequest {Id = 1107377};

      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.DeleteActivityCategory(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.Deleted.ShouldBeFalse();
    }

    [Test]
    public void DeleteActivityCategory_WhenPassedInvalidRequest_ReturnsFailure()
    {
      // Arrange
      var request = new DeleteActivityCategoryRequest { Id = 2107377 };

      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.DeleteActivityCategory(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
    }
    
    #endregion

    #region Activity

    [Test]
		public void GetActivities_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new GetActivitiesRequest { Criteria = new ActivityCriteria { ActivityType = ActivityType.JobSeeker, FetchOption = CriteriaBase.FetchOptions.PagedList}};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetActivities(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.ActivitiesPaged.Count.ShouldEqual(10);
		}

		[Test]
		public void SaveActivities_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new SaveActivitiesRequest { Activities = new List<ActivityViewDto>{new ActivityViewDto{ Id = 1107787, ActivityName = "name" }}};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.SaveActivities(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

    [Test]
    public void SaveActivity_WhenPassedNewRequest_ReturnsSuccess()
    {
      // Arrange
      var request = new SaveActivitiesRequest
      {
        Name = "New Activity",
        ActivityCategoryId = 1107357,
        ExternalId = 1107357,
        Visible = true
      };

      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.SaveActivity(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.Saved.ShouldBeTrue();
      response.Activity.ShouldNotBeNull();
      response.Activity.Id.ShouldNotBeNull();
    }

    [Test]
    public void SaveActivity_WhenPassedExistingRequest_ReturnsSuccess()
    {
      // Arrange
      var request = new SaveActivitiesRequest
      {
        Id = 1107928,
        Name = "New Human Resource Issues"
      };

      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.SaveActivity(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.Saved.ShouldBeTrue();
      response.Activity.ShouldNotBeNull();
      response.Activity.Id.ShouldNotBeNull();
    }

    [Test]
    public void SaveActivity_WhenPassedNewRequestWithExistingName_ReturnsFailure()
    {
      // Arrange
      var request = new SaveActivitiesRequest
      {
        Name = "Human Resource Issues",
        ActivityCategoryId = 1107370,
        ExternalId = 5553528,
        Visible = true
      };

      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.SaveActivity(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.Saved.ShouldBeFalse();
    }

    [Test]
    public void SaveActivity_WhenPassedExistingRequestWithExistingName_ReturnsFailure()
    {
      // Arrange
      var request = new SaveActivitiesRequest
      {
        Id = 1107929,
        Name = "Human Resource Issues"
      };

      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.SaveActivity(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.Saved.ShouldBeFalse();
    }

    [Test]
    public void DeleteActivity_WhenPassedValidRequest_ReturnsSuccess()
    {
      // Arrange
      var request = new DeleteActivityRequest { Id = 1107928 };

      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.DeleteActivity(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.Deleted.ShouldBeTrue();
    }

    [Test]
    public void DeleteActivity_WhenPassedInvalidRequest_ReturnsFailure()
    {
      // Arrange
      var request = new DeleteActivityRequest { Id = 11079280 };

      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.DeleteActivity(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
    }

    [Test]
    public void SaveSelfService_WhenPassedValidUser_ReturnsSuccess()
    {
      // Arrange
      var request = new SaveSelfServiceRequest { ActingOnBehalfOfUserId = 1197305 }.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
      // Act
      var response = _service.SaveSelfService(request);
      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
    }

    [Test]
    public void SaveSelfService_WhenPassedValidUser_ReturnsFailure()
    {
      // Arrange
      var request = new SaveSelfServiceRequest { ActingOnBehalfOfUserId = -1197305 }.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
      // Act
      var response = _service.SaveSelfService(request);
      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
    }

    #endregion

		#region Message Tests

		[Test]
		public void GetMessage_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new GetMessageRequest { Module = FocusModules.Assist };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetMessage(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void DismissMessage_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new DismissMessageRequest { MessageId = 1090130 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.DismissMessage(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void CreateHomepageAlert_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var messageTexts = new List<MessageTextDto> { new MessageTextDto { LocalisationId = 0, MessageId = 0, Text = "Test message" } };
			var message = new MessageDto { ExpiresOn = DateTime.Now, IsSystemAlert = true, Audience = MessageAudiences.User };

			var request = new CreateHomepageAlertRequest { HomepageAlerts = new List<HomepageAlertView> { new HomepageAlertView { Message = message, MessageTexts = messageTexts } } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.CreateHomepageAlert(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void SaveMessageTexts_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var messageTexts = new List<SavedMessageTextDto> { new SavedMessageTextDto { LocalisationId = 0, Text = "Test message" } };
			var message = new SavedMessageDto { Name = "Saved message", UserId = 2, Audience = MessageAudiences.User };

			var request = new SaveMessageTextsRequest { Message = message, MessageTexts = messageTexts };

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.SaveMessageTexts(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void GetSavedMessage_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new GetSavedMessageRequest { Audience = MessageAudiences.User };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetSavedMessage(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void GetSavedMessageTexts_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new GetSavedMessageTextsRequest { SavedMessageId = 1108278 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetSavedMessageTexts(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.SavedMessageTexts.Count.ShouldBeGreater(0);
		}

		[Test]
		public void DeleteSavedMessage_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new DeleteSavedMessageRequest { SavedMessageId = 1108278 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.DeleteSavedMessage(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void GetRecentMessage_WhenPassedValidRequest_ReturnsSuccess()
		{
			CreateRecentAnnouncement();

			// Arrange
			var request = new GetMessageRequest { OrderBy = "CreatedOn Desc"};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetRecentMessages(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Messages.Count().ShouldBeGreaterOrEqual(1);
		}

		[Test]
		public void GetRecentMessage_WhenNoMessages_ReturnsSuccess()
		{
			// Ensure all messages expired
			var changes = new Dictionary<string, object> { { "ExpiresOn", DateTime.Now } };
			var updateQuery = new Query(typeof(Message));
			DataCoreRepository.Update(updateQuery, changes);

			// Arrange
			var request = new GetMessageRequest { OrderBy = "CreatedOn Desc" };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetRecentMessages(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Messages.Count().ShouldBeGreaterOrEqual(0);
		}
		
		[Test]
		public void ExpireMessage_WhenPassedValidRequest_ReturnsSuccess()
		{
			var id = CreateRecentAnnouncement();

			// Get current number of messages
			var getRequest = new GetMessageRequest { OrderBy = "CreatedOn Desc" };
			getRequest.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			var count = _service.GetRecentMessages(getRequest).Messages.Count();

			// Arrange
			var request = new DismissMessageRequest { MessageId = id };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ExpireMessage(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			// Re-get current number of messages
			var newCount = _service.GetRecentMessages(getRequest).Messages.Count();
			newCount.ShouldEqual(count - 1);
		}

		[Test]
		public void ExpireMessage_WhenPassedInvalidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new DismissMessageRequest { MessageId = 1 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ExpireMessage(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		/// <summary>
		/// Creates a recent message
		/// </summary>
		/// <returns>The Id of the message</returns>
		private long CreateRecentAnnouncement()
		{
			var message = new Message
			{
				IsSystemAlert = true,
				ExpiresOn = DateTime.Now.AddDays(7),
				Audience = MessageAudiences.AllAssistUsers,
				MessageType = MessageTypes.General,
				CreatedBy = 2
			};

			var messageText = new MessageText
			{
				Text = "This is a test",
				LocalisationId = 12090
			};

			message.MessageTexts.Add(messageText);

			DataCoreRepository.Add(message);
			DataCoreRepository.SaveChanges();

			return message.Id;
		}

		#endregion

		#region Configuration Tests

		[Test]
		public void SaveConfigurationItems_WhenPassedValidRequestWithKey_ReturnsSuccess()
		{
			// Arrange
			var request = new SaveConfigurationItemsRequest { ConfigurationItems = new List<ConfigurationItemDto> { new ConfigurationItemDto { Key = "SchoolType", Value = "2" } } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.SaveConfigurationItems(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

    [Test]
    public void SaveConfigurationItems_WhenPassedValidRequestWithId_ReturnsSuccess()
    {
      // Arrange
      var request = new SaveConfigurationItemsRequest { ConfigurationItems = new List<ConfigurationItemDto> { new ConfigurationItemDto { Id = 5522426, Value = "State.TX" } } };
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.SaveConfigurationItems(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
    }

    [Test]
    public void SaveExternalConfigurationItems_WhenPassedKeyForInternalItem_ReturnsFailure()
    {
      // Arrange
      var request = new SaveConfigurationItemsRequest { ConfigurationItems = new List<ConfigurationItemDto> { new ConfigurationItemDto { Key = "SchoolType", Value = "2" } } };
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.SaveExternalConfigurationItems(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
    }

    [Test]
    public void SaveConfigurationItems_WhenPassedIdForInternalItem_ReturnsSuccess()
    {
      // Arrange
      var request = new SaveConfigurationItemsRequest { ConfigurationItems = new List<ConfigurationItemDto> { new ConfigurationItemDto { Id = 5522741, Value = "2" } } };
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.SaveExternalConfigurationItems(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
    }

		[Test]
		public void GetConfiguration_WhenUsingValidRequest_ReturnsConfigurationValue() // TEST
		{
			// Arrange
			var request = new GetConfigurationRequest().Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetConfigurationItems(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.ConfigurationItems.ShouldNotBeNull();

		  var appSettings = new AppSettings(response.ConfigurationItems);
      appSettings.SmtpServer.ShouldEqual("DEVSMTPServer");
		}

    [Test]
    public void GetExternalConfiguration_WhenUsingValidRequest_ReturnsConfigurationValue()
    {
      // Arrange
      var request = new GetConfigurationRequest().Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.GetExternalConfigurationItems(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.ConfigurationItems.ShouldNotBeNull();

      //response.ConfigurationItems.Count().ShouldEqual(2);
    }

		#endregion

		#region Localisation Tests

		[Test]
		public void GetLocalisation_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new GetLocalisationRequest();
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetLocalisation(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}
		
		[Test]
		public void GetLocalisationDictionary_WhenUsingDefaultCulture_ReturnsDefaultLocalisationDictionary()
		{
			// Arrange
			MockUserData.Culture = Constants.DefaultCulture;
			var request = new GetLocalisationDictionaryRequest();
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			
			// Act
			var response = _service.GetLocalisationDictionary(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.LocalisationDictionary.ShouldNotBeNull();
			response.LocalisationDictionary.Count.ShouldBeGreater(0);
		}
		
		[Test]
		public void GetLocalisationDictionary_WhenUsingUsersCulture_ReturnsUsersLocalisationDictionary()
		{
			// Arrange
			var request = new GetLocalisationDictionaryRequest();
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetLocalisationDictionary(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.LocalisationDictionary.ShouldNotBeNull();
			response.LocalisationDictionary.Count.ShouldBeGreater(0);
		}

    [Test]
    public void SaveLocalisationItems_WhenPassedValidRequest_ReturnsSuccess()
    {
      // Arrange
      var request = new SaveLocalisationItemsRequest
      {
        Items = new List<LocalisationItemDto>
        {
          new LocalisationItemDto
          {
            Key = "SaveLocalisationItems.Test",
            Value = "Save Localisation Items"
          }
        }
      }.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.SaveLocalisationItems(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.LocalisationDictionary.ShouldNotBeNull();
      response.LocalisationDictionary.Count.ShouldBeGreater(0);

      var localisedItem = response.LocalisationDictionary.GetValue("SaveLocalisationItems.Test");

      localisedItem.ShouldNotBeNull();
      if (localisedItem != null)
        localisedItem.LocalisedValue.ShouldEqual("Save Localisation Items");
      //response.LocalisationDictionary.ContainsKey("SaveLocalisationItems.Test").ShouldBeTrue();
    }

		#endregion

		#region Lookup Tests
		
		[Test]
		public void GetLookup_WhenUsingDefaultCultureAndGettingTitles_ReturnsDefaultLookup()
		{
			// Arrange
			//var request = new GetLookupRequest(MockUserData, LookupTypes.Titles);
			var request = new GetLookupRequest {LookupType = LookupTypes.Titles};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetLookup(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.LookupItems.ShouldNotBeNull();
			response.LookupItems.Count.ShouldBeGreater(0);
		}
		
		[Test] 
		public void GetLookup_WhenUsingUsersCultureAndGettingTitles_ReturnsUsersLookup()
		{
			// Arrange
			MockUserData.Culture = Constants.DefaultCulture;

			//var request = new GetLookupRequest(MockUserData, LookupTypes.Titles);
			var request = new GetLookupRequest { LookupType = LookupTypes.Titles };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetLookup(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.LookupItems.ShouldNotBeNull();
			response.LookupItems.Count.ShouldBeGreater(0);
		}

		#endregion

		#region ServiceBase Tests
		
		[Test]
		public void FormatErrorMessageFormatErrorMessage_WhenGivenALocalisationWithLocalisation_ReturnsSuccessWithLocalisedText()
		{
			// Arrange
			var request = new RegisterTalentUserRequest();
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			
			// Act
			var response = _service.FormatErrorMessage(request, ErrorTypes.Unknown);

			// Assert
			response.ShouldEqual("There has been a problem processing your request.<br/>Please check the details and try again.");
		}
		
		[Test]
		public void FormatErrorMessageFormatErrorMessage_WhenGivenALocalisationKeyWithoutLocalisation_ReturnsSuccessWithLocalisedKey()
		{
			// Arrange
			var request = new RegisterTalentUserRequest();
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			
			// Act
			var response = _service.FormatErrorMessage(request, ErrorTypes.Ok);

			// Assert
			response.ShouldEqual("[ErrorType.Ok]");
		}
		
		[Test]
		public void GetCertificateLicenses_WhenPassedValidRequest_ReturnsSuccessWithData()
		{
			// Arrange
			var request = new GetCertificateLicenseRequest { Criteria = new CertificateLicenseCriteria { LikeName = "WSO", ListSize = 3 } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetCertificateLicenses(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.CertificateLicenses.Count().ShouldBeGreater(0);
			response.CertificateLicenses.Count().ShouldBeLessOrEqual(3);
		}
		
		[Test]
		public void GetLanguages_WhenPassedValidRequest_ReturnsSuccessWithData()
		{
			// Arrange
			var request = new GetLanguageRequest { Criteria = new LanguageCriteria { LanguageLike = "En", ListSize = 3 } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetLanguage(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Languages.Count().ShouldBeGreater(0);
			response.Languages[0].Text.ShouldEqual("English");
		}

		#endregion

		#region Notes Reminders

		[Test]
		public void GetNoteReminder_WhenPassedValidRequestWithId_ReturnsSuccessAndReminder()
		{
			// Arrange
			var request = new NoteReminderViewRequest { Criteria = new NoteReminderCriteria { Id = 1090050 } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetNoteReminder(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.NoteReminder.Text.ShouldEqual("Test job seeker reminder");
		}

		[Test]
		public void SaveNoteReminder_WhenPassedValidReminder_ReturnsSuccess()
		{
			// Arrange
			var request = new SaveNoteReminderRequest{ NoteReminder  = new NoteReminderDto
			                                              							{
			                                              								Text = "Test reminder",
																																		EntityId = 12345,
																																		EntityType = EntityTypes.Job,
																																		CreatedBy = MockUserId,
																																		NoteReminderType = NoteReminderTypes.Reminder,
																																		ReminderVia = ReminderMedia.Email,
																																		ReminderDue = DateTime.Today.AddDays(1)
			                                              							} ,
                                                                  ReminderRecipients = new List<NoteReminderRecipientDto>()
																								};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.SaveNoteReminder(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}
		
		[Test]
		public void SendReminder_WhenPassedValidReminderId_ReturnsSuccess()
		{
			// Arrange
			var request = new SendReminderRequest { Id = 1090050 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.SendReminder(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

    [Test]
    public void SendHiringFromTaxCreditProgramNotification_WhenPassedValidReminderId_ReturnsSuccess()
    {
      // Arrange
      var workOpportunitiesTaxCreditHires = WorkOpportunitiesTaxCreditCategories.DesignatedCommunityResidents;
      workOpportunitiesTaxCreditHires = workOpportunitiesTaxCreditHires | WorkOpportunitiesTaxCreditCategories.SNAPRecipients;
      var request = new EmailHiringFromTaxCreditProgramNotificationRequest { WorkOpportunitiesTaxCreditHires = workOpportunitiesTaxCreditHires, EmailTo = "person@site.com"};
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.SendHiringFromTaxCreditProgramNotification(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
    }

		#endregion

		#region Industry Classification Tests

		[Test]
		public void GetIndustryClassifications_WhenFilteringByParentIdZero_ReturnsSuccess()
		{
			// Arrange
			var criteria = new IndustryClassificationCriteria {ParentId = 0, FetchOption = CriteriaBase.FetchOptions.Lookup};
			var request = new IndustryClassificationRequest {Criteria = criteria};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetIndustryClassification(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}
		
		[Test]
		public void GetIndustryClassifications_WhenFilteringByCode_ReturnsSuccess()
		{
			// Arrange
			var criteria = new IndustryClassificationCriteria { Code = "1111", FetchOption = CriteriaBase.FetchOptions.Single };
			var request = new IndustryClassificationRequest { Criteria = criteria };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetIndustryClassification(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}
		
		[Test]
		public void GetIndustryClassifications_WhenFilteringByCodeLength_ReturnsSuccess()
		{
			// Arrange
			var criteria = new IndustryClassificationCriteria { CodeLength = 4, FetchOption = CriteriaBase.FetchOptions.List };
			var request = new IndustryClassificationRequest { Criteria = criteria };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetIndustryClassification(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		#endregion

    #region Internship Statement / Category Tests
    [Test]
    public void GetInternshipCategories_WhenPassedValidRequest_ReturnsSuccess()
    {
      // Arrange
      var request = new GetEducationInternshipCategoriesRequest { IncludeSkills = true };
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
  
      var service = new CoreService(RuntimeContext);
      var response = service.GetEducationInternshipCategories(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.EducationInternshipCategories.ShouldNotBeNull();
      response.EducationInternshipCategories.Count.ShouldBeGreater(0);
    }

    [Test]
    public void GetInternshipStatements_WhenPassedValidRequest_ReturnsSuccess()
    {
      // Arrange
      var criteria = new EducationInternshipStatementCriteria
                       {
                         EducationInternshipSkillIds
                           =
                           new List<long> {1, 2, 3, 4}
                       };

      var request = new GetEducationInternshipStatementsRequest { Criteria = criteria };
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act

      var service = new CoreService(RuntimeContext);
      var response = service.GetEducationInternshipStatements(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.EducationInternshipStatements.ShouldNotBeNull();
      response.EducationInternshipStatements.Count.ShouldBeGreater(0);
    }
    #endregion

    [Test]
    public void SendEmail_ReturnsSuccess()
    {
      // Arrange
      var request =
        new SendEmailRequest
          {EmailBody = "Unit Test email", EmailSubject = "Unit Test Email", EmailToAddresses = "fakeemail@burning-glass.comx"}.
          Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.SendEmail(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
    }

		[Test]
		public void GetJobNameFromROnet_WhenFilteringByParentIdZero_ReturnsSuccess()
		{
			// Arrange
			var request = new JobNameRequest { RONet = "11-1021.91"};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetJobNameFromROnet(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void GetCareerAreaName_WhenFilteringByParentIdZero_ReturnsSuccess()
		{
			// Arrange
			var request = new CareerAreaNameRequest { CareerAreaId = 17 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetCareerAreaName(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
    }

    #region Upload Files

    [Test]
    public void GetValidationSchema_WhenJobUploadRequested_ReturnsSuccess()
    {
      var request = new ValidationSchemaRequest
      {
        SchemaType = ValidationSchema.JobUpload,
        GetTemplate = false
      }.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      var response = _service.GetValidationSchema(request);
      response.ShouldNotBeNull();
      response.ValidationSchema.ShouldNotBeNull();
    }

    [Test]
    public void GetCSVTemplateForSchema_WhenJobUploadRequested_ReturnsSuccess()
    {
      var request = new ValidationSchemaRequest
      {
        SchemaType = ValidationSchema.JobUpload,
        GetTemplate = true
      }.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      var response = _service.GetCSVTemplateForSchema(request);
      response.ShouldNotBeNull();
      response.Template.ShouldNotBeNull();
      response.Template.StartsWith("Job Title,").ShouldBeTrue();
    }

    [Test]
    public void ValidateUploadFile_WhenJobUploadUploaded_ReturnsSuccess()
    {
      const string fileText = @"Job Title,Description,Zip,Number of Openings,Closing Date
Technical Product Manager,""Manages and coordinates all the processes involved in creating a product.  Oversees work involved with product design, production, distribution, marketing and sales."",76352,2,01/01/2002
";
      var request = new UploadFileValidationRequest
      {
        FileName = "TestUpload.csv",
        SchemaType = ValidationSchema.JobUpload,
        FileBytes = System.Text.Encoding.UTF8.GetBytes(fileText),
        FileType = UploadFileType.CSV,
        CustomInformation = new Dictionary<string, string> { { "EmployeeId", "29452" }, { "BusinessUnitId", "1546755" } },
        IgnoreHeader = true
      }.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      var response = _service.ValidateUploadFile(request);
      response.ShouldNotBeNull();
      response.UploadFileId.ShouldBeGreater(0);
    }

    [Test]
    public void GetUploadFileProcessingState_WhenPassedFileRequiringValidating_ReturnsSuccess()
    {
      var request = new UploadFileDetailsRequest
      {
        UploadFileId = 7057745
      }.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      var response = _service.GetUploadFileProcessingState(request);

      response.ShouldNotBeNull();
      response.ProcessingState.ShouldEqual(UploadFileProcessingState.RequiresValidation);
    }

    [Test]
    public void GetUploadFileProcessingState_WhenPassedValidatedFile_ReturnsSuccess()
    {
      var request = new UploadFileDetailsRequest
      {
        UploadFileId = 7057746
      }.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      var response = _service.GetUploadFileProcessingState(request);

      response.ShouldNotBeNull();
      response.ProcessingState.ShouldEqual(UploadFileProcessingState.Validated);
    }

    [Test]
    public void GetUploadFileDetails_ForSummary_ReturnsSuccess()
    {
      var request = new UploadFileDetailsRequest
      {
        UploadFileId = 7057746,
        ExceptionsOnly = false,
        BasicDataOnly = true
      }.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      var response = _service.GetUploadFileDetails(request);

      response.ShouldNotBeNull();
      response.ValidationResults.ShouldNotBeNull();
      response.ValidationResults.Count.ShouldEqual(6);
      response.ValidationResults.All(result => result.FieldData.IsNull() && result.FieldValidation.IsNull()).ShouldBeTrue();
    }

    [Test]
    public void GetUploadFileDetails_ForExceptionLog_ReturnsSuccess()
    {
      var request = new UploadFileDetailsRequest
      {
        UploadFileId = 7057746,
        ExceptionsOnly = true,
        BasicDataOnly = false
      }.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      var response = _service.GetUploadFileDetails(request);

      response.ShouldNotBeNull();
      response.ValidationResults.ShouldNotBeNull();
      response.ValidationResults.Count.ShouldEqual(5);
      response.ValidationResults.All(result => result.FieldData.IsNotNull() && result.FieldValidation.IsNotNull()).ShouldBeTrue();
    }

    [Test]
    public void CommitUploadFile_ReturnsSuccess()
    {
      var request = new UploadFileCommitRequest
      {
        UploadFileId = 7057746
      }.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      var response = _service.CommitUploadFile(request);

      response.ShouldNotBeNull();

      var processingRequest = new UploadFileDetailsRequest
      {
        UploadFileId = 7057746
      }.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      var processingResponse = _service.GetUploadFileProcessingState(processingRequest);

      processingResponse.ProcessingState.ShouldEqual(UploadFileProcessingState.RequiresCommitting);
    }

    #endregion


		#region Document Tests

		#region GetDocuments Tests

		[Test]
		public void GetDocuments_WithCategory_ReturnsListSuccess()
		{
			// Arrange
			var request = new GetDocumentsRequest { Criteria = new DocumentCriteria { FetchOption = CriteriaBase.FetchOptions.List, Category = 5531284 } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetDocuments(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			response.Documents.ShouldNotBeNull();
			response.Documents.Count.ShouldEqual(6);
		}

		[Test]
		public void GetDocuments_WithInvalidCategory_ReturnsSuccess()
		{
			// Arrange
			var request = new GetDocumentsRequest { Criteria = new DocumentCriteria { FetchOption = CriteriaBase.FetchOptions.List, Category = 5555555 } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetDocuments(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			response.Documents.ShouldNotBeNull();
			response.Documents.Count.ShouldEqual(0);
		}

		[Test]
		public void GetDocuments_WithGroup_ReturnsListSuccess()
		{
			// Arrange
			var request = new GetDocumentsRequest { Criteria = new DocumentCriteria { FetchOption = CriteriaBase.FetchOptions.List, Group = 5531285 } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetDocuments(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			response.Documents.ShouldNotBeNull();
			response.Documents.Count.ShouldEqual(3);
		}

		[Test]
		public void GetDocuments_WithInvalidGroup_ReturnsSuccess()
		{
			// Arrange
			var request = new GetDocumentsRequest { Criteria = new DocumentCriteria { FetchOption = CriteriaBase.FetchOptions.List, Group = 6666666 } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetDocuments(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			response.Documents.ShouldNotBeNull();
			response.Documents.Count.ShouldEqual(0);
		}

		[Test]
		public void GetDocuments_WithList_ReturnsListSuccess()
		{
			// Arrange
			var request = new GetDocumentsRequest { Criteria = new DocumentCriteria { FetchOption = CriteriaBase.FetchOptions.List } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetDocuments(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			response.Documents.ShouldNotBeNull();
			response.Documents.Count.ShouldEqual(7);
		}

		[Test]
		public void GetDocuments_WithCategoryGroup_ReturnsListSuccess()
		{
			// Arrange
			var request = new GetDocumentsRequest { Criteria = new DocumentCriteria { FetchOption = CriteriaBase.FetchOptions.List, Category = 5531284, Group = 5531285 } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetDocuments(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			response.Documents.ShouldNotBeNull();
			response.Documents.Count.ShouldEqual(3);
		}

		[Test]
		public void GetDocuments_WithNullCriteria_ReturnsFailure()
		{
			// Arrange
			var request = new GetDocumentsRequest ();
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetDocuments(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}

		[Test]
		public void GetDocuments_WithDocumentId_ReturnsSingleSuccess()
		{
			// Arrange
			var request = new GetDocumentsRequest { Criteria = new DocumentCriteria { FetchOption = CriteriaBase.FetchOptions.Single, DocumentId = 11117060 } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetDocuments(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			response.Document.ShouldNotBeNull();
		}

		[Test]
		public void GetDocuments_WithInvalidDocumentId_ReturnsSuccess()
		{
			// Arrange
			var request = new GetDocumentsRequest { Criteria = new DocumentCriteria { FetchOption = CriteriaBase.FetchOptions.Single, DocumentId = 11111111 } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetDocuments(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			response.Document.ShouldBeNull();
		}

		#endregion

		#region DeleteDocument Tests

		[Test]
		public void DeleteDocuments_WithDocumentId_ReturnsSingleSuccess()
		{
			// Arrange
			var request = new GetDocumentsRequest { Criteria = new DocumentCriteria { DocumentId = 11117060 } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			var originalCount = DataCoreRepository.Documents.Count();

			// Act
			var response = _service.DeleteDocument(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			(originalCount - DataCoreRepository.Documents.Count()).ShouldEqual(1);
		}

		[Test]
		public void DeleteDocuments_WithInvalidDocumentId_ReturnsSingleSuccess()
		{
			// Arrange
			var request = new GetDocumentsRequest { Criteria = new DocumentCriteria { DocumentId = 11111111 } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			var originalCount = DataCoreRepository.Documents.Count();

			// Act
			var response = _service.DeleteDocument(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);

			(originalCount - DataCoreRepository.Documents.Count()).ShouldEqual(0);
		}

		[Test]
		public void DeleteDocuments_WithoutDocumentId_ReturnsSingleSuccess()
		{
			// Arrange
			var request = new GetDocumentsRequest { Criteria = new DocumentCriteria () };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			var originalCount = DataCoreRepository.Documents.Count();

			// Act
			var response = _service.DeleteDocument(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);

			(originalCount - DataCoreRepository.Documents.Count()).ShouldEqual(0);
		}

		#endregion

		#endregion
	}
}
