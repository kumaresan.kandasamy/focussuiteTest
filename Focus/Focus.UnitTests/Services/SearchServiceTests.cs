﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using Focus.Services.Core;
using Focus.Services.Helpers;
using Focus.Services.Messages;
using Focus.UnitTests.Core;
using Framework.Messaging;
using Moq;
using NUnit.Framework;

using Focus.Core;
using Focus.Core.Criteria;
using Focus.Core.Criteria.CandidateSearch;
using Focus.Core.Criteria.Onet;
using Focus.Core.Criteria.OnetTask;
using Focus.Core.Criteria.SavedSearch;
using Focus.Core.Messages;
using Focus.Core.Messages.SearchService;
using Focus.Core.Models.Career;
using Focus.Core.Views;
using Focus.Services.Repositories.Lens;
using Focus.Services.Repositories;
using Focus.Services.ServiceImplementations;
using Framework.Testing.NUnit;

using CandidateSearchRequest = Focus.Core.Messages.SearchService.CandidateSearchRequest;
using DistanceUnits = Focus.Core.DistanceUnits;

#endregion

namespace Focus.UnitTests.Services
{
	[TestFixture]
	class SearchServiceTests : TestFixtureBase
	{
		private SearchService _service;

		[SetUp]
		public override void SetUp()
		{
			base.SetUp();

			SetUpCacheProvider();
			SetUpMockLogProvider();

			_service = new SearchService(RuntimeContext);
		}

		[Test]
		public void DeleteSavedSearch_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new DeleteSavedSearchRequest { SavedSearchId = 1000173 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.DeleteSavedSearch(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		#region Search Candidates Tests

		[Test]
		public void SearchCandidates_WhenPassedValidRequest_ReturnsSuccessAndData()
		{
			var initialActionCount = DataCoreRepository.ActionEvents.Count();

			// Arrange
			var searchCriteria = new CandidateSearchCriteria
			{
				SearchType = CandidateSearchTypes.ByJobDescription,
				ScopeType = CandidateSearchScopeTypes.Open,
				JobDetailsCriteria = new CandidateSearchJobDetailsCriteria { JobId = 1197655 },
				MinimumDocumentCount = 5
			};

			var request = new CandidateSearchRequest
											{
												SearchId = Guid.Empty,
												Criteria = searchCriteria
											};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			var lensRepositoryMock = new Mock<ILensRepository>();

			var candidates = new List<ResumeView>
			                      {
			                        new ResumeView
			                          {
			                            Name = "CandidateName",
			                            EmploymentHistories =
			                              new List<ResumeView.ResumeJobView>
			                              	{
			                              		new ResumeView.ResumeJobView
			                              		  {
			                              		    Employer = "Employer",
			                              		    JobTitle = "JobTitle",
			                              		    YearEnd = "2011",
			                              		    YearStart = "2010"
			                              		  }
			                              	},
																			Score = 100,
																			Id = 1,
																			YearsExperience = 2,
																			StarRating = 1
			                          }
			                      };

			lensRepositoryMock.Setup(x => x.SearchResumes(It.IsAny<CandidateSearchCriteria>(), It.IsAny<string>())).Returns(candidates);
			lensRepositoryMock.Setup(x => x.IsLicensed()).Returns(true);

			((TestRepositories)RuntimeContext.Repositories).Lens = lensRepositoryMock.Object;

			var service = new SearchService(RuntimeContext);

			// Act
			var response = service.SearchCandidates(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Candidates.Count.ShouldBeGreater(0);
			response.HighestStarRating.ShouldEqual(1);

			var postSearchActionCount = DataCoreRepository.ActionEvents.Count();

			postSearchActionCount.ShouldBeGreater(initialActionCount);
			(postSearchActionCount - initialActionCount).ShouldEqual(1);

			var actionTypeId = DataCoreRepository.ActionTypes.Where(x => x.Name.Equals(ActionTypes.SearchCandidates.ToString())).Select(x => x.Id).FirstOrDefault();
			DataCoreRepository.ActionEvents.Count(x => x.ActionTypeId.Equals(actionTypeId)).ShouldEqual(1);
		}

		[Test]
		public void SearchCandidates_WhenPassedValidRequestWhichFindsNoData_ReturnsSuccessWithoutData()
		{
			var initialActionCount = DataCoreRepository.ActionEvents.Count();

			// Arrange
			var searchCriteria = new CandidateSearchCriteria
			{
				SearchType = CandidateSearchTypes.ByJobDescription,
				ScopeType = CandidateSearchScopeTypes.Open,
				JobDetailsCriteria = new CandidateSearchJobDetailsCriteria { JobId = 1194581 },
				MinimumDocumentCount = 5
			};

			var request = new CandidateSearchRequest
			{
				SearchId = Guid.Empty,
				Criteria = searchCriteria
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			var lensRepositoryMock = new Mock<ILensRepository>();
			var candidates = new List<ResumeView>();

			lensRepositoryMock.Setup(x => x.SearchResumes(It.IsAny<CandidateSearchCriteria>(), It.IsAny<string>())).Returns(candidates);
			lensRepositoryMock.Setup(x => x.IsLicensed()).Returns(true);

			((TestRepositories)RuntimeContext.Repositories).Lens = lensRepositoryMock.Object;

			var service = new SearchService(RuntimeContext);

			// Act
			var response = service.SearchCandidates(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.HighestStarRating.ShouldEqual(0);
			response.Candidates.Count.ShouldEqual(0);

			var postSearchActionCount = DataCoreRepository.ActionEvents.Count();

			postSearchActionCount.ShouldBeGreater(initialActionCount);
			(postSearchActionCount - initialActionCount).ShouldEqual(1);

			var actionTypeId = DataCoreRepository.ActionTypes.Where(x => x.Name.Equals(ActionTypes.SearchCandidates.ToString())).Select(x => x.Id).FirstOrDefault();
			DataCoreRepository.ActionEvents.Count(x => x.ActionTypeId.Equals(actionTypeId)).ShouldEqual(1);
		}

		[Test]
		public void SearchCandidates_WhenRecievesIntegrationFails_ReturnsFailure()
		{
			var initialActionCount = DataCoreRepository.ActionEvents.Count();

			// Arrange
			var searchCriteria = new CandidateSearchCriteria
			{
				SearchType = CandidateSearchTypes.ByJobDescription,
				ScopeType = CandidateSearchScopeTypes.Open,
				JobDetailsCriteria = new CandidateSearchJobDetailsCriteria { JobId = 106640 },
				MinimumDocumentCount = 5
			};

			var request = new CandidateSearchRequest
			{
				SearchId = Guid.Empty,
				Criteria = searchCriteria
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			var lensRepositoryMock = new Mock<ILensRepository>();
			lensRepositoryMock.Setup(x => x.SearchResumes(It.IsAny<CandidateSearchCriteria>(), It.IsAny<string>())).Throws(new Exception("Boom"));
			lensRepositoryMock.Setup(x => x.IsLicensed()).Returns(true);

			var service = new SearchService(RuntimeContext);

			// Act
			var response = service.SearchCandidates(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.Candidates.ShouldBeNull();

			var postSearchActionCount = DataCoreRepository.ActionEvents.Count();

			postSearchActionCount.ShouldEqual(initialActionCount);
			(postSearchActionCount - initialActionCount).ShouldEqual(0);

			var actionTypeId = DataCoreRepository.ActionTypes.Where(x => x.Name.Equals(ActionTypes.SearchCandidates.ToString())).Select(x => x.Id).FirstOrDefault();
			DataCoreRepository.ActionEvents.Count(x => x.ActionTypeId.Equals(actionTypeId)).ShouldEqual(0);
		}

		[Test]
		[Explicit]
		public void SearchCandidates_WhenPassedValidEducationCriteria_ReturnsSuccessAndData()
		{
			// Arrange
			var searchCriteria = new CandidateSearchCriteria
			{
				SearchType = CandidateSearchTypes.OpenSearch,
				AdditionalCriteria = new CandidateSearchAdditionalCriteria
															{
																EducationCriteria = new CandidateSearchEducationCriteria
																											{
																												MonthsUntilExpectedCompletion = 6,
																												IncludeAlumni = true,
																												EducationLevels = new List<EducationLevel> { EducationLevel.Bachelors_OR_Equivalent_24, EducationLevel.Doctorate_Degree_26 }
																												//MinimumGPA = 3.2
																											}
															},
				JobDetailsCriteria = new CandidateSearchJobDetailsCriteria { JobId = 106640 },
				MinimumDocumentCount = 5
			};

			var request = new CandidateSearchRequest
			{
				SearchId = Guid.Empty,
				Criteria = searchCriteria
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			var lensRepository = new LensRepository(RuntimeContext);

			((TestRepositories)RuntimeContext.Repositories).Lens = lensRepository;
			var service = new SearchService(RuntimeContext);

			// Act
			var response = service.SearchCandidates(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		[Explicit]
		public void SearchCandidates_WhenPassedValidDrivingLicenceCriteria_ReturnsSuccessAndData()
		{
			var initialActionCount = DataCoreRepository.ActionEvents.Count();

			// Arrange
			var searchCriteria = new CandidateSearchCriteria
			{
				SearchType = CandidateSearchTypes.OpenSearch,
				AdditionalCriteria = new CandidateSearchAdditionalCriteria
				{
					DrivingLicenceCriteria = new CandidateSearchDrivingLicenceCriteria
					{
						DrivingLicenceClassId = 1596586,
						DrivingLicenceEndorsementsIds = new List<long> { 1596672, 1596668 }
					}
				},
				JobDetailsCriteria = new CandidateSearchJobDetailsCriteria { JobId = 1199001 },
				MinimumDocumentCount = 5
			};

			var request = new CandidateSearchRequest
			{
				SearchId = Guid.Empty,
				Criteria = searchCriteria
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			var lensRepository = new LensRepository(RuntimeContext);

			((TestRepositories)RuntimeContext.Repositories).Lens = lensRepository;
			var service = new SearchService(RuntimeContext);

			// Act
			var response = service.SearchCandidates(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			var postSearchActionCount = DataCoreRepository.ActionEvents.Count();

			postSearchActionCount.ShouldBeGreater(initialActionCount);
			(postSearchActionCount - initialActionCount).ShouldEqual(1);

			var actionTypeId = DataCoreRepository.ActionTypes.Where(x => x.Name.Equals(ActionTypes.SearchCandidates.ToString())).Select(x => x.Id).FirstOrDefault();
			DataCoreRepository.ActionEvents.Count(x => x.ActionTypeId.Equals(actionTypeId)).ShouldEqual(1);
		}

		[Test]
		public void SaveCandidateSavedSearch_WhenPassedSaveRequestWithId_ReturnsSuccessAndSavesData()
		{
			// Arrange
			var request = new SaveCandidateSavedSearchRequest
							{
								Id = 1004562,
								AlertEmailAddress = "test@tes.com",
								AlertEmailFormat = EmailFormats.TextOnly,
								AlertEmailFrequency = EmailAlertFrequencies.Weekly,
								AlertEmailRequired = true,
								Name = "TestSavedSearch",
								Criteria = GetFullCandidateSearchCriteria(),
								SearchType = SavedSearchTypes.TalentCandidateSearch
							};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act	
			var response = _service.SaveCandidateSearch(request);
			var savedSearch = DataCoreRepository.SavedSearches.SingleOrDefault(x => x.Name == "TestSavedSearch");

			// Assert 
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			savedSearch.ShouldNotBeNull();
			savedSearch.AlertEmailFormat.ShouldEqual(EmailFormats.TextOnly);
			savedSearch.AlertEmailFrequency.ShouldEqual(EmailAlertFrequencies.Weekly);
			savedSearch.AlertEmailRequired.ShouldEqual(true);
			savedSearch.Name.ShouldEqual("TestSavedSearch");
		}

		[Test]
		[Explicit]
		public void SearchCandidates_WhenPassedValidRegisteredFromDate_ReturnsSuccessAndData()
		{
			// Arrange
			var searchCriteria = new CandidateSearchCriteria
			{
				SearchType = CandidateSearchTypes.ByJobDescription,
				ScopeType = CandidateSearchScopeTypes.Open,
				CandidateRegisteredFrom = DateTime.Now.Date.AddDays(-3660),
				JobDetailsCriteria = new CandidateSearchJobDetailsCriteria { JobId = 1199001 },
				MinimumDocumentCount = 5
			};

			var request = new CandidateSearchRequest
			{
				SearchId = Guid.Empty,
				Criteria = searchCriteria
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			var lensRepository = new LensRepository(RuntimeContext);

			((TestRepositories)RuntimeContext.Repositories).Lens = lensRepository;
			var service = new SearchService(RuntimeContext);

			// Act
			var response = service.SearchCandidates(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Candidates.Count.ShouldBeGreater(0);
		}

		[Test]
		[Explicit]
		public void SearchCandidates_WhenPassedValidUpdatedFromDate_ReturnsSuccessAndData()
		{
			// Arrange
			var searchCriteria = new CandidateSearchCriteria
			{
				SearchType = CandidateSearchTypes.ByJobDescription,
				ScopeType = CandidateSearchScopeTypes.Open,
				CandidateUpdatedFrom = DateTime.Now.Date.AddDays(-3660),
				JobDetailsCriteria = new CandidateSearchJobDetailsCriteria { JobId = 1199001 },
				MinimumDocumentCount = 5
			};

			var request = new CandidateSearchRequest
			{
				SearchId = Guid.Empty,
				Criteria = searchCriteria
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			var lensRepository = new LensRepository(RuntimeContext);

			((TestRepositories)RuntimeContext.Repositories).Lens = lensRepository;
			var service = new SearchService(RuntimeContext);

			// Act
			var response = service.SearchCandidates(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Candidates.Count.ShouldBeGreater(0);
		}

		[Test]
		public void SaveCandidateSavedSearch_WhenPassedSaveRequestWithouId_ReturnsSuccessAndSavesData()
		{
			// Arrange
			var request = new SaveCandidateSavedSearchRequest
										{
											Id = 0,
											AlertEmailAddress = "test@tes.com",
											AlertEmailFormat = EmailFormats.TextOnly,
											AlertEmailFrequency = EmailAlertFrequencies.Weekly,
											AlertEmailRequired = true,
											Name = "TestSavedSearch",
											Criteria = GetFullCandidateSearchCriteria(),
											SearchType = SavedSearchTypes.TalentCandidateSearch
										};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act	
			var response = _service.SaveCandidateSearch(request);
			var savedSearch = DataCoreRepository.SavedSearches.SingleOrDefault(x => x.Name == "TestSavedSearch");

			// Assert 
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			savedSearch.ShouldNotBeNull();
			savedSearch.AlertEmailFormat.ShouldEqual(EmailFormats.TextOnly);
			savedSearch.AlertEmailFrequency.ShouldEqual(EmailAlertFrequencies.Weekly);
			savedSearch.AlertEmailRequired.ShouldEqual(true);
			savedSearch.Name.ShouldEqual("TestSavedSearch");
		}

		[Test]
		public void SaveCandidateSavedSearch_WhenPassedSaveRequestWithInvalidUserId_ReturnsFailure()
		{
			// Arrange
			MockUserData.UserId = -1;

			var request = new SaveCandidateSavedSearchRequest
											{
												AlertEmailAddress = "test@tes.com",
												AlertEmailFormat = EmailFormats.TextOnly,
												AlertEmailFrequency = EmailAlertFrequencies.Weekly,
												AlertEmailRequired = true,
												Name = "TestSavedSearch",
												Criteria = GetFullCandidateSearchCriteria(),
												SearchType = SavedSearchTypes.TalentCandidateSearch
											};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act	
			var response = _service.SaveCandidateSearch(request);

			// Assert 
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}

		[Test]
		public void GetCandidateSavedSearch_WhenPassedValidId_ReturnsSuccessAndSavedSearch()
		{
			// Arrange
			var request = new GetCandidateSavedSearchRequest
			{
				UserContext = MockUserData,
				Criteria = new SavedSearchCriteria { SavedSearchId = 1004562 }
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act	
			var response = _service.GetCandidateSavedSearch(request);

			// Assert 
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.SavedSearch.Name.ShouldEqual("TestSearch");
		}

		[Test]
		public void GetCandidateSavedSearch_WhenPassedInValidId_ReturnsFailure()
		{
			var request = new GetCandidateSavedSearchRequest
			{
				UserContext = MockUserData,
				Criteria = new SavedSearchCriteria { SavedSearchId = 0 }
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act	
			var response = _service.GetCandidateSavedSearch(request);

			// Assert 
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}

		[Test]
		public void GetCandidateSavedSearchView_WhenPassedUserId_ReturnsSuccessAndSavedSearches()
		{
			// Arrange
			var request = new GetCandidateSavedSearchViewRequest
			{
				UserContext = MockUserData,
				Criteria = new SavedSearchCriteria { UserId = 2 }
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act	
			var response = _service.GetCandidateSavedSearchView(request);

			// Assert 
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.SavedSearches.Count().ShouldEqual(3);
			response.SavedSearches[0].Name.ShouldEqual("TestSearch");
			response.SavedSearches[0].Id.ShouldEqual(1004562);
			response.SavedSearches[1].Name.ShouldEqual("TestSearch (1)");
			response.SavedSearches[1].Id.ShouldEqual(1004572);
		}

		[Test]
		public void GetResumeSearchSession_WhenPassedValidSearchId_ReturnsSuccessAndResumes()
		{
			// Arrange
			var guid = new Guid();

			var request = new GetResumeSearchSessionRequest
			{
				SearchId = guid
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			var mockSessionHelper = new Mock<ISessionHelper>();
			mockSessionHelper.Setup(x => x.GetSessionState<List<ResumeView>>(It.IsAny<Guid>(), It.IsAny<string>(), It.IsAny<List<ResumeView>>(), It.IsAny<IEnumerable<Type>>())).Returns(MockResumeViews);

			((TestHelpers)RuntimeContext.Helpers).Session = mockSessionHelper.Object;
			var service = new SearchService(RuntimeContext);

			// Act
			var response = service.GetResumeSearchSession(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Resumes.ShouldNotBeNull();
			response.Resumes.Count.ShouldEqual(5);
		}

		[Test]
		public void GetResumeSearchSession_WhenPassedValidSearchIdAndStarRating_ReturnsSuccessAndResume()
		{
			// Arrange
			var guid = new Guid();

			var request = new GetResumeSearchSessionRequest
			{
				SearchId = guid,
				StarRating = 1
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			var mockSessionHelper = new Mock<ISessionHelper>();
			mockSessionHelper.Setup(x => x.GetSessionState<List<ResumeView>>(It.IsAny<Guid>(), It.IsAny<string>(), It.IsAny<List<ResumeView>>(), It.IsAny<IEnumerable<Type>>())).Returns(MockResumeViews);

			((TestHelpers)RuntimeContext.Helpers).Session = mockSessionHelper.Object;
			var service = new SearchService(RuntimeContext);

			// Act
			var response = service.GetResumeSearchSession(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Resumes.ShouldNotBeNull();
            response.Resumes.Count.ShouldBeGreaterOrEqual(1);
		}

		[Test]
		public void GetResumeSearchSession_WhenPassedInvalidSearchIdAndStarRating_ReturnsFailureAndNoResumes()
		{
			// Arrange
			var guid = new Guid();

			var request = new GetResumeSearchSessionRequest
			{
				SearchId = guid,
				StarRating = 1
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetResumeSearchSession(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.Resumes.ShouldBeNull();
		}

		#endregion

		#region Lens Search Tests

		[Test]
		public void LensSearch_WhenPassedZipCode_ReturnsSuccess()
		{
			// Arrange
			var searchCriteria = new SearchCriteria
			{
				JobLocationCriteria = new JobLocationCriteria { Radius = new RadiusCriteria { PostalCode = "41063", Distance = 5, DistanceUnits = global::Focus.Core.Models.Career.DistanceUnits.Miles } }
			};

			var request = new LensSearchRequest
			{
				Criteria = searchCriteria
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			var lensRepositoryMock = new Mock<ILensRepository>();
			lensRepositoryMock.Setup(x => x.IsLicensed()).Returns(true);
			lensRepositoryMock.Setup(x => x.SearchPostings(searchCriteria, MockCulture, false, 1)).Returns(new List<PostingSearchResultView> { new PostingSearchResultView() });

			((TestRepositories)RuntimeContext.Repositories).Lens = lensRepositoryMock.Object;
			var service = new SearchService(RuntimeContext);

			// Act
			var response = service.LensSearch(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Postings.ShouldNotBeNull();
			response.Postings.Count.ShouldBeGreater(0);
		}

		[Test]
		public void LensSearch_WhenPassedPostingAge_ReturnsSuccess()
		{
			// Arrange
			var searchCriteria = new SearchCriteria
			{
				PostingAgeCriteria = new PostingAgeCriteria { PostingAgeInDays = 0 }
			};

			var request = new LensSearchRequest
			{
				Criteria = searchCriteria
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			var lensRepositoryMock = new Mock<ILensRepository>();
			lensRepositoryMock.Setup(x => x.IsLicensed()).Returns(true);
			lensRepositoryMock.Setup(x => x.SearchPostings(searchCriteria, MockCulture, false, 1)).Returns(new List<PostingSearchResultView> { new PostingSearchResultView() });

			((TestRepositories)RuntimeContext.Repositories).Lens = lensRepositoryMock.Object;
			var service = new SearchService(RuntimeContext);

			// Act
			var response = service.LensSearch(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Postings.ShouldNotBeNull();
			response.Postings.Count.ShouldBeGreater(0);
		}

		[Test]
		public void LensSearch_WhenPassedSalary_ReturnsSuccess()
		{
			// Arrange
			var searchCriteria = new SearchCriteria
			{
				SalaryCriteria = new SalaryCriteria { IncludeJobsWithoutSalaryInformation = true, MinimumSalary = 1 }
			};

			var request = new LensSearchRequest
			{
				Criteria = searchCriteria
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			var lensRepositoryMock = new Mock<ILensRepository>();
			lensRepositoryMock.Setup(x => x.IsLicensed()).Returns(true);
			lensRepositoryMock.Setup(x => x.SearchPostings(searchCriteria, MockCulture, false, 1)).Returns(new List<PostingSearchResultView> { new PostingSearchResultView() });
			((TestRepositories)RuntimeContext.Repositories).Lens = lensRepositoryMock.Object;

			var service = new SearchService(RuntimeContext);

			// Act
			var response = service.LensSearch(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Postings.ShouldNotBeNull();
			response.Postings.Count.ShouldBeGreater(0);
		}

		[Test]
		public void LensSearch_WhenPassedInternship_ReturnsSuccess()
		{
			// Arrange
			var searchCriteria = new SearchCriteria
			{
				InternshipCriteria = new InternshipCriteria() { Internship = FilterTypes.ShowOnly }
			};

			var request = new LensSearchRequest
			{
				Criteria = searchCriteria
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			var lensRepository = new LensRepository(RuntimeContext);

			((TestRepositories)RuntimeContext.Repositories).Lens = lensRepository;
			var service = new SearchService(RuntimeContext);

			// Act
			var response = service.LensSearch(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Postings.ShouldNotBeNull();
			response.Postings.Count.ShouldBeGreater(0);
		}

		[Test]
		public void LensSearch_WhenPassedHomeBased_ReturnsSuccess()
		{
			// Arrange
			var searchCriteria = new SearchCriteria
			{
				HomeBasedJobsCriteria = new HomeBasedJobsCriteria() { HomeBasedJobs = FilterTypes.ShowOnly }
			};

			var request = new LensSearchRequest
			{
				Criteria = searchCriteria
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			var lensRepository = new LensRepository(RuntimeContext);

			((TestRepositories)RuntimeContext.Repositories).Lens = lensRepository;
			var service = new SearchService(RuntimeContext);

			// Act
			var response = service.LensSearch(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Postings.ShouldNotBeNull();
			response.Postings.Count.ShouldBeGreater(0);
		}

		[Test]
		public void LensSearch_WhenPassedCommissionBased_ReturnsSuccess()
		{
			// Arrange
			var searchCriteria = new SearchCriteria
			{
				CommissionBasedJobsCriteria = new CommissionBasedJobsCriteria() { CommissionBasedJobs = FilterTypes.ShowOnly }
			};

			var request = new LensSearchRequest
			{
				Criteria = searchCriteria
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			var lensRepository = new LensRepository(RuntimeContext);

			((TestRepositories)RuntimeContext.Repositories).Lens = lensRepository;
			var service = new SearchService(RuntimeContext);

			// Act
			var response = service.LensSearch(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Postings.ShouldNotBeNull();
			response.Postings.Count.ShouldBeGreater(0);
		}


		[Test]
		public void LensSearch_WhenPassedSalaryAndCommissionBased_ReturnsSuccess()
		{
			// Arrange
			var searchCriteria = new SearchCriteria
			{
				SalaryAndCommissionBasedJobsCriteria = new SalaryAndCommissionBasedJobsCriteria() { SalaryAndCommissionBasedJobs = FilterTypes.ShowOnly }
			};

			var request = new LensSearchRequest
			{
				Criteria = searchCriteria
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			var lensRepository = new LensRepository(RuntimeContext);

			((TestRepositories)RuntimeContext.Repositories).Lens = lensRepository;
			var service = new SearchService(RuntimeContext);

			// Act
			var response = service.LensSearch(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Postings.ShouldNotBeNull();
			response.Postings.Count.ShouldBeGreater(0);
		}

		[Test]
		public void LensSearch_WhenPassedIndustry_ReturnsSuccess()
		{
			// Arrange
			var searchCriteria = new SearchCriteria
			{
				IndustryCriteria = new IndustryCriteria { IndustryId = 1, IndustryDetailIds = new List<long> { 3 } }
			};

			var request = new LensSearchRequest
			{
				Criteria = searchCriteria
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			var lensRepository = new LensRepository(RuntimeContext);

			((TestRepositories)RuntimeContext.Repositories).Lens = lensRepository;
			var service = new SearchService(RuntimeContext);

			// Act
			var response = service.LensSearch(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Postings.ShouldNotBeNull();
			response.Postings.Count.ShouldBeGreater(0);
		}

		[Test]
		public void LensSearch_WhenPassedOccupation_ReturnsSuccess()
		{
			// Arrange
			var searchCriteria = new SearchCriteria
			{
				OccupationCriteria = new OccupationCriteria { JobFamilyId = 107184, OccupationIds = new List<long> { 112752 } }
			};

			var request = new LensSearchRequest
			{
				Criteria = searchCriteria
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			var lensRepository = new LensRepository(RuntimeContext);

			((TestRepositories)RuntimeContext.Repositories).Lens = lensRepository;
			var service = new SearchService(RuntimeContext);

			// Act
			var response = service.LensSearch(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Postings.ShouldNotBeNull();
			response.Postings.Count.ShouldBeGreater(0);
		}

		[Test]
		public void LensSearch_WhenPassedJobSector_ReturnsSuccess()
		{
			// Arrange
			var searchCriteria = new SearchCriteria
			{
				JobSectorCriteria = new JobSectorCriteria { RequiredJobSectorIds = new List<long> { 1001318, 1001311 } }
			};

			var request = new LensSearchRequest
			{
				Criteria = searchCriteria
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			var lensRepositoryMock = new Mock<ILensRepository>();
			lensRepositoryMock.Setup(x => x.SearchPostings(searchCriteria, MockCulture, false, 1)).Returns(new List<PostingSearchResultView> { new PostingSearchResultView() });
			lensRepositoryMock.Setup(x => x.IsLicensed()).Returns(true);
			((TestRepositories)RuntimeContext.Repositories).Lens = lensRepositoryMock.Object;

			// Call Lens
			//var lensRepository = new LensRepository(RuntimeContext);
			//((TestRepositories)RuntimeContext.Repositories).Lens = lensRepository;

			var service = new SearchService(RuntimeContext);

			// Act
			var response = service.LensSearch(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Postings.ShouldNotBeNull();
			response.Postings.Count.ShouldBeGreater(0);
		}

		[Test]
		public void LensSearch_WhenPassedEducationLevel_ReturnsSuccess()
		{
			// Arrange
			var searchCriteria = new SearchCriteria
			{
				EducationLevelCriteria = new EducationLevelCriteria { IncludeJobsWithoutEducationRequirement = true, RequiredEducationIds = new List<long> { 1001025 } }
			};

			var request = new LensSearchRequest
			{
				Criteria = searchCriteria
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			var lensRepository = new LensRepository(RuntimeContext);

			((TestRepositories)RuntimeContext.Repositories).Lens = lensRepository;
			var service = new SearchService(RuntimeContext);

			// Act
			var response = service.LensSearch(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Postings.ShouldNotBeNull();
			response.Postings.Count.ShouldBeGreater(0);
		}

		[Test]
		public void LensSearch_WhenPassedKeyword_ReturnsSuccess()
		{
			// Arrange
			var searchCriteria = new SearchCriteria
			{
				KeywordCriteria = new KeywordCriteria { KeywordText = "test", SearchLocation = PostingKeywordScopes.Anywhere }
			};

			var request = new LensSearchRequest
			{
				Criteria = searchCriteria
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			var lensRepository = new LensRepository(RuntimeContext);

			((TestRepositories)RuntimeContext.Repositories).Lens = lensRepository;
			var service = new SearchService(RuntimeContext);

			// Act
			var response = service.LensSearch(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Postings.ShouldNotBeNull();
			response.Postings.Count.ShouldBeGreater(0);
		}

		[Test]
		public void LensSearch_WhenPassedInternshipCategories_ReturnsSuccess()
		{
			// Arrange
			var searchCriteria = new SearchCriteria
			{
				InternshipCriteria = new InternshipCriteria { InternshipCategories = new List<long> { 1, 3 } }
			};

			var request = new LensSearchRequest
			{
				Criteria = searchCriteria
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			var lensRepository = new LensRepository(RuntimeContext);

			((TestRepositories)RuntimeContext.Repositories).Lens = lensRepository;
			var service = new SearchService(RuntimeContext);

			// Act
			var response = service.LensSearch(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Postings.ShouldNotBeNull();
			response.Postings.Count.ShouldBeGreater(0);
		}

		[Test]
		[Explicit]
		public void LensSearch_WhenPassedProgramOfStudy_ReturnsSuccess()
		{
			// Arrange
			var searchCriteria = new SearchCriteria
			{
				EducationProgramOfStudyCriteria = new EducationProgramOfStudyCriteria { DegreeEducationLevelId = 16152 }
			};

			var request = new LensSearchRequest
			{
				Criteria = searchCriteria
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			var lensRepository = new LensRepository(RuntimeContext);

			((TestRepositories)RuntimeContext.Repositories).Lens = lensRepository;
			var service = new SearchService(RuntimeContext);

			// Act
			var response = service.LensSearch(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Postings.ShouldNotBeNull();
			response.Postings.Count.ShouldBeGreater(0);
		}

		[Test]
		[Explicit]
		public void LensSearch_WhenPassedProgramArea_ReturnsSuccess()
		{
			// Arrange
			var searchCriteria = new SearchCriteria
			{
				EducationProgramOfStudyCriteria = new EducationProgramOfStudyCriteria { ProgramAreaId = 5 }
			};

			var request = new LensSearchRequest
			{
				Criteria = searchCriteria
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			var lensRepository = new LensRepository(RuntimeContext);

			((TestRepositories)RuntimeContext.Repositories).Lens = lensRepository;
			var service = new SearchService(RuntimeContext);

			// Act
			var response = service.LensSearch(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Postings.ShouldNotBeNull();
			response.Postings.Count.ShouldBeGreater(0);
		}

		[Test]
		public void LensSearch_WhenPassedROnets_ReturnsSuccess()
		{
			// Arrange
			var searchCriteria = new SearchCriteria { ROnetCriteria = new ROnetCriteria { ROnets = new List<string> { "11-1021.92", "11-3021.00" } } };

			var request = new LensSearchRequest { Criteria = searchCriteria };

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			var lensRepository = new LensRepository(RuntimeContext);

			((TestRepositories)RuntimeContext.Repositories).Lens = lensRepository;
			var service = new SearchService(RuntimeContext);

			// Act
			var response = service.LensSearch(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Postings.ShouldNotBeNull();
			response.Postings.Count.ShouldBeGreater(0);
		}

		[Test]
		public void LensSearch_WhenPassedMSAId_ReturnsSuccess()
		{
			// Arrange
			var searchCriteria = new SearchCriteria { JobLocationCriteria = new JobLocationCriteria { Area = new AreaCriteria { MsaId = 1000904 } } };

			var request = new LensSearchRequest { Criteria = searchCriteria };

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			var lensRepository = new LensRepository(RuntimeContext);

			((TestRepositories)RuntimeContext.Repositories).Lens = lensRepository;
			var service = new SearchService(RuntimeContext);

			// Act
			var response = service.LensSearch(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Postings.ShouldNotBeNull();
			response.Postings.Count.ShouldBeGreater(0);
		}

		[Test]
		public void LensSearch_WhenPassedResumeAndExcludedJobs_ReturnsSuccess()
		{
			// Arrange
			var searchCriteria = new SearchCriteria
			{
				ReferenceDocumentCriteria = new ReferenceDocumentCriteria
				{
					DocumentId = "1095111",
					DocumentType = DocumentType.Resume,
					ExcludedJobs = new List<Job> { new Job { JobId = new Guid("F64DAA36-5A13-4E99-904F-89B28629271B") } }
				}
			};

			var request = new LensSearchRequest { Criteria = searchCriteria };

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			var lensRepository = new LensRepository(RuntimeContext);

			((TestRepositories)RuntimeContext.Repositories).Lens = lensRepository;
			var service = new SearchService(RuntimeContext);

			// Act
			var response = service.LensSearch(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Postings.ShouldNotBeNull();
			response.Postings.Count.ShouldBeGreater(0);
		}

		[Test]
		public void PostingLikeThisSearch_WhenPassedInValidPostingId_ReturnsSuccess()
		{
			// Arrange
			var searchCriteria = new SearchCriteria
			{
				SearchType = PostingSearchTypes.PostingsLikeThis,
				ReferenceDocumentCriteria = new ReferenceDocumentCriteria
				{
					DocumentType = DocumentType.Posting,
					DocumentId = "xxxx"
				},
			};

			var request = new LensSearchRequest { Criteria = searchCriteria };

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			var lensRepository = new LensRepository(RuntimeContext);

			((TestRepositories)RuntimeContext.Repositories).Lens = lensRepository;

			// Override the mock document repository
			var documentRepositoryMock = new Mock<IDocumentRepository>();
			documentRepositoryMock.Setup(x => x.GetDocumentText(It.IsAny<string>())).Returns("");

			((TestRepositories)RuntimeContext.Repositories).Document = documentRepositoryMock.Object;

			var service = new SearchService(RuntimeContext);

			// Act
			var response = service.LensSearch(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Postings.ShouldNotBeNull();
			response.Postings.Count.ShouldEqual(0);
		}

		#endregion

		#region Onet tests

		[Test]
		public void GetOnets_WhenGivenRequestForOnesWithJobTasks_ReturnsSuccessWithPopulatedList()
		{
			// Arrange
			var request = new OnetRequest { Criteria = new OnetCriteria { OnetsWithJobTasks = true } };

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetOnets(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Onets.Count.ShouldBeGreater(0);
		}

		[Test]
		public void GetOnets_WhenGivenValidJobFamilyId_ReturnsSuccessWithPopulatedList()
		{
			// Arrange
			var request = new OnetRequest { Criteria = new OnetCriteria { JobFamilyId = 107140 } };

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetOnets(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Onets.Count.ShouldBeGreater(0);
		}

		[Test]
		public void GetOnets_WhenGivenInValidJobFamilyId_ReturnsSuccessWithEmpyist()
		{
			// Arrange
			var request = new OnetRequest { Criteria = new OnetCriteria { JobFamilyId = 0 } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetOnets(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Onets.Count.ShouldEqual(0);
		}

		[Test]
		public void GetOnets_WhenGivenValidOnetId_ReturnsSuccessWithData()
		{
			// Arrange
			var request = new OnetRequest { Criteria = new OnetCriteria { OnetId = 152922 } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetOnets(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Onets.Count.ShouldBeGreater(0);
		}

		[Test]
		public void GetOnets_WhenGivenValidOnetId_ReturnsSuccessWithoutData()
		{
			// Arrange
			var request = new OnetRequest { Criteria = new OnetCriteria { JobFamilyId = 107140 } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetOnets(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Onets.Count.ShouldBeGreater(0);
		}

		[Test]
		public void GetOnets_WhenGivenValidJobTitle_ReturnsSuccessWithoutData()
		{
			// Arrange
			var request = new OnetRequest { Criteria = new OnetCriteria { JobTitle = "Project Manager", ListSize = 5, FetchOption = CriteriaBase.FetchOptions.List } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			var lensRepository = new LensRepository(RuntimeContext);

			((TestRepositories)RuntimeContext.Repositories).Lens = lensRepository;
			var service = new SearchService(RuntimeContext);

			// Act
			var response = service.GetOnets(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Onets.Count.ShouldBeGreater(0);
		}

		[Test]
		[Ignore]
		public void GetROnets_WhenGivenValidJobTitle_ReturnsSuccessWithoutData()
		{
			// Arrange
			var request = new ROnetRequest
											{
												Criteria =
													new OnetCriteria
														{
															JobTitle =
																"Public Relations and Fundraising Managers",
															ListSize = 5,
															FetchOption = CriteriaBase.FetchOptions.List
														}
											};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			var lensRepository = new LensRepository(RuntimeContext);

			((TestRepositories)RuntimeContext.Repositories).Lens = lensRepository;
			var service = new SearchService(RuntimeContext);

			// Act
			var response = service.GetROnets(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.ROnets.Count.ShouldBeGreater(0);
		}

		#endregion

		#region Clarify Tests

		[Test]
		public void GetClarifiedWords_WhenPassedValidRequest_ReturnsSuccessWithData()
		{
			// Arrange
			var request = new ClarifyWordRequest { Text = "Words words words" };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			var clarifiedWords = new List<string> { "Word1", "Word2" };

			var lensRepositoryMock = new Mock<ILensRepository>();
			lensRepositoryMock.Setup(x => x.IsLicensed()).Returns(true);
			lensRepositoryMock.Setup(x => x.Clarify(It.IsAny<string>(), 0, 0, "")).Returns(clarifiedWords);
			((TestRepositories)RuntimeContext.Repositories).Lens = lensRepositoryMock.Object;

			var service = new SearchService(RuntimeContext);

			// Act
			var response = service.GetClarifiedWords(request, 0, 0, "");

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.ClarifiedWords.Count.ShouldBeGreater(0);
		}

		[Test]
		public void GetClarifiedWords_WhenPassedValidRequest_ReturnsFailureWithData()
		{
			// Arrange
			var request = new ClarifyWordRequest { Text = "Words words words" };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			var lensRepositoryMock = new Mock<ILensRepository>();
			lensRepositoryMock.Setup(x => x.IsLicensed()).Returns(true);
			lensRepositoryMock.Setup(x => x.Clarify(It.IsAny<string>(), 0, 0, "")).Throws(new Exception("Boom"));
			((TestRepositories)RuntimeContext.Repositories).Lens = lensRepositoryMock.Object;

			var service = new SearchService(RuntimeContext);

			// Act
			var response = service.GetClarifiedWords(request, 0, 0, "");

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.ClarifiedWords.ShouldBeNull();
		}

		#endregion

		#region Keyword Tests

		[Test]
		[Ignore("OnetCommodity table not populated.")]
		public void GetOnetKeywords_WhenGivenValidOnetCode_ReturnsSuccessWithData()
		{
			// TODO: Martha - populate OnetCommodity table
			// Arrange
			var request = new OnetKeywordsRequest { OnetCode = "11-9051.00" };

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetOnetKeywords(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Keywords.Count.ShouldBeGreater(0);
		}

		#endregion

		#region Task Tests

		[Test]
		public void GetOnetTaskKeywords_WhenGivenValidOnetId_ReturnsSuccessWithData()
		{
			// Arrange
			var request = new OnetTaskRequest { Criteria = new OnetTaskCriteria { OnetId = 112752 } };

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetOnetTasks(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.OnetTasks.Count.ShouldBeGreater(0);
		}

		#endregion

		#region ConvertBinaryData Tests

		[Test]
		public void ConvertBinaryData_WhenPassedValidRequest_ReturnsSuccessWithData()
		{
			// Arrange
			var request = new ConvertBinaryDataRequest { BinaryFileData = new byte[0], FileName = "FileName" };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			var lensRepositoryMock = new Mock<ILensRepository>();
			lensRepositoryMock.Setup(x => x.CovertBinaryData(It.IsAny<byte[]>(), It.IsAny<string>())).Returns("Word1 Word2");
			lensRepositoryMock.Setup(x => x.IsLicensed()).Returns(true);

			((TestRepositories)RuntimeContext.Repositories).Lens = lensRepositoryMock.Object;

			var service = new SearchService(RuntimeContext);

			// Act
			var response = service.ConvertBinaryDataToText(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Text.Length.ShouldBeGreater(0);
		}

		[Test]
		public void ConvertBinaryData_WhenPassedValidRequest_ReturnsFailureWithData()
		{
			// Arrange
			var request = new ConvertBinaryDataRequest { BinaryFileData = new byte[0], FileName = "FileName" };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			var lensRepositoryMock = new Mock<ILensRepository>();
			lensRepositoryMock.Setup(x => x.CovertBinaryData(It.IsAny<byte[]>(), It.IsAny<string>())).Throws(new Exception("Boom"));
			lensRepositoryMock.Setup(x => x.IsLicensed()).Returns(true);

			((TestRepositories)RuntimeContext.Repositories).Lens = lensRepositoryMock.Object;

			var service = new SearchService(RuntimeContext);

			// Act
			var response = service.ConvertBinaryDataToText(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.Text.ShouldBeNull();
		}

		#endregion

		#region Helper methods

		private static CandidateSearchCriteria GetFullCandidateSearchCriteria()
		{
			return new CandidateSearchCriteria
			{
				PageIndex = 0,
				PageSize = 10,
				FetchOption = CriteriaBase.FetchOptions.PagedList,
				SearchType = CandidateSearchTypes.ByJobDescription,
				MinimumScore = 100,
				ResumeCriteria = new CandidateSearchResumeCriteria { Resume = new byte[1], ResumeFileExtension = "test" },
				CandidatesLikeThisSearchCriteria = new CandidatesLikeThisCriteria { CandidateId = "999" },
				JobDetailsCriteria = new CandidateSearchJobDetailsCriteria { JobTitle = "Description", JobId = 888 },
				KeywordCriteria = new CandidateSearchKeywordCriteria { Context = KeywordContexts.FullResume, Keywords = "Keywords", Scope = KeywordScopes.LastTwoJobs },
				AdditionalCriteria = new CandidateSearchAdditionalCriteria
				{
					AvailabilityCriteria =
						new CandidateSearchAvailabilityCriteria
						{
							NormalWorkShiftId = 777
						},
					EducationCriteria =
						new CandidateSearchEducationCriteria { EducationLevel = EducationLevels.All },
					Licences = new List<string> { "Lang&Lic" },
					Languages = new List<string> { "Languages" },
					LocationCriteria =
						new CandidateSearchLocationCriteria
						{
							Distance = 12,
							DistanceUnits = DistanceUnits.Miles,
							PostalCode = "postcode"
						},
					StatusCriteria =
						new CandidateSearchStatusCriteria { USCitizen = false, Veteran = false }
				}
			};
		}

		private List<ResumeView> MockResumeViews
		{
			get
			{
				return new List<ResumeView>
				{
					new ResumeView {Name = "5 Star Candidate", StarRating = 5},
					new ResumeView {Name = "4 Star Candidate", StarRating = 4},
					new ResumeView {Name = "3 Star Candidate", StarRating = 3},
					new ResumeView {Name = "2 Star Candidate", StarRating = 2},
					new ResumeView {Name = "1 Star Candidate", StarRating = 1},
				};
			}
		}


		#endregion

		[Test]
		public void LensSearch_WithManualSearchRequestAndNonStandaloneIntegrationClient_LogsSelfServiceActivityRunManualJobSearch()
		{
			// Arrange
			var integrationMessages = new List<IntegrationRequestMessage>();

			var searchCriteria = new SearchCriteria
			{
				PostingAgeCriteria = new PostingAgeCriteria { PostingAgeInDays = 0 }
			};

			var request = new LensSearchRequest
			{
				Criteria = searchCriteria,
				ManualSearch = true
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			var lensRepositoryMock = new Mock<ILensRepository>();
			lensRepositoryMock.Setup(x => x.IsLicensed()).Returns(true);
			lensRepositoryMock.Setup(x => x.SearchPostings(searchCriteria, MockCulture, false, null)).Returns(new List<PostingSearchResultView> { new PostingSearchResultView() });

			((FakeAppSettings)RuntimeContext.AppSettings).Module = FocusModules.CareerExplorer;
			((FakeAppSettings)RuntimeContext.AppSettings).IntegrationClient = IntegrationClient.Wisconsin;
			((TestRepositories)RuntimeContext.Repositories).Lens = lensRepositoryMock.Object;

			var service = new SearchService(RuntimeContext);

			// Act
			var response = service.LensSearch(request);

			// Assert
			response.ShouldNotBeNull();
			((TestMessagingHelper)RuntimeContext.Helpers.Messaging).Messages
				.Any(m =>
					{
						var integrationRequestMessage = m.Message as IntegrationRequestMessage;
						return integrationRequestMessage != null && integrationRequestMessage.AssignJobSeekerActivityRequest.ActivityActionType == ActionTypes.RunManualJobSearch;
					}).ShouldBeTrue();
		}
	}
}
