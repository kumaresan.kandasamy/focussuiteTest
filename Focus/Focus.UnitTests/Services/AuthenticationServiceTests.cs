﻿#region Copyright © 2012 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using Focus.Core.Models;
using Focus.Services.Repositories;
using Focus.UnitTests.Core;
using Moq;
using NUnit.Framework;

using Focus.Core;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Messages;
using Focus.Core.Messages.AuthenticationService;
using Focus.Data.Core.Entities;
using Focus.Data.Repositories.Contracts;
using Focus.Services.ServiceImplementations;
using Focus.Web.Code;
using Framework.Testing.NUnit;

#endregion

namespace Focus.UnitTests.Services
{
	[TestFixture]
	public class AuthenticationServiceTests : TestFixtureBase
	{	
		private AuthenticationService _service;

		[SetUp]
		public override void SetUp()
		{
			base.SetUp();

			SetUpCacheProvider();
			SetUpMockLogProvider();
			_service = new AuthenticationService(RuntimeContext);
		}
		
		[Test]
		public void ValidatePasswordResetValidationKey_WhenPassed_ReturnsSuccessAnd()
		{
			// Arrange
			var request = new ValidatePasswordResetValidationKeyRequest { ValidationKey = "6Mhwdp0XWTvECl5aYDxnZ3pBo3iJ/e5+QcWdK2VI9wGWXAzWPrCc26+VpFayc4gTqfO/3Cjwh5GeXRRo5j+ZXA" };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ValidatePasswordResetValidationKey(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void ValidatePinRegistration_WhenPassedValidEmailAndPinCombination_ReturnsSuccess()
		{
			// Arrange
			var request = new ValidatePinRequest { EmailAddress = "test@test.com", Pin = "poiuytrewqasdfgh" };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ValidatePinRegistration(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void ValidatePinRegistration_WhenPassedValidEmailWithInvalidPin_ReturnsFailure()
		{
			// Arrange
			var request = new ValidatePinRequest { EmailAddress = "test@test.com", Pin = "1f7bb72ad96a39b1" };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ValidatePinRegistration(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}	

		#region LogIn Tests

		[Test]
		public void LogIn_WhenUsingValidCredentials_ReturnsUserDetails()
		{
			// Arrange
			var request = new LogInRequest { UserName = MockTalentUsername, Password = MockPassword, ModuleToLoginTo = FocusModules.Talent };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.LogIn(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			response.UserId.ShouldEqual(MockTalentUserId);
			response.FirstName.ShouldEqual(MockTalentFirstName);
			response.LastName.ShouldEqual(MockTalentLastName);
		}
		
		[Test]
		public void LogIn_WhenUsingInvalidCredentials_ReturnsFailure()
		{
			// Arrange
			var invalidUserName = MockUsername + DateTime.Now.Ticks;
			var request = new LogInRequest { UserName = invalidUserName, Password = MockPassword + DateTime.Now.Ticks};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.LogIn(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.Message.ShouldNotBeNull();
			response.Message.ShouldEqual(_service.FormatErrorMessage(request, ErrorTypes.InvalidUserNameOrPassword));
		}
		
		[Test]
		public void LogIn_WhenSentNewAssistUser_ReturnsFailure()
		{
			var invalidUserName = MockUsername + DateTime.Now.Ticks;
			var request = new LogInRequest { UserName = invalidUserName, Password = MockPassword + DateTime.Now.Ticks };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			request.ModuleToLoginTo = FocusModules.Assist;

			// Act
			var response = _service.LogIn(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.Message.ShouldNotBeNull();
			response.Message.ShouldEqual(_service.FormatErrorMessage(request, ErrorTypes.InvalidUserNameOrPassword));
		}

		[Test]
		[Explicit]
		public void LoginCareerSSOUser_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new LogInRequest
			{
				Type = LoginType.SSO,
				ExternalId = "SSOCareerUser",
				EmailAddress = "newssocareeruser@bg.com",
				FirstName = "SSO",
				LastName = "User",
				ScreenName = "SSO User",
				ModuleToLoginTo = FocusModules.CareerExplorer
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.LogIn(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.IsEnabled.ShouldBeTrue();

			var user = DataCoreRepository.Users.First(x => x.ExternalId == request.ExternalId);
			user.ShouldNotBeNull();
			user.Person.ShouldNotBeNull();
		}

		[Test]
		[Explicit]
		public void LoginCareerSSOUserForRightManagement_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new LogInRequest
			{
				Type = LoginType.SSO,
				ExternalId = "54F55921-C707-42CB-A73E-89F6DD5067A4",
				EmailAddress = "rightmanagement-sso-user@bg.com",
				FirstName = "Right",
				LastName = "Management",
				ScreenName = "RM SSO User",
				ModuleToLoginTo = FocusModules.CareerExplorer
			};

			// Pass in an empty user context as there will be none when the login method is called because the user will not be logged in before the login method is executed.
			request.Prepare(MockClientTag, MockSessionId, new UserContext(), MockCulture);

			var oldClientRepository = ((FakeAppSettings)RuntimeContext.AppSettings).IntegrationClient;
			var oldIntegrationRepository = ((TestRepositories)RuntimeContext.Repositories).Integration;

			((FakeAppSettings)RuntimeContext.AppSettings).IntegrationClient = IntegrationClient.RightManagement;
			((TestRepositories)RuntimeContext.Repositories).Integration = new IntegrationRepository(RuntimeContext, IntegrationClient.RightManagement, "{\"Url\": \"https://bb2dttest.manpower.com/\", \"SourceSystem\": \"BurningGlass\"}");

			// Act
			LogInResponse response;

			try
			{
				response = _service.LogIn(request);
			}
			finally
			{
				((FakeAppSettings)RuntimeContext.AppSettings).IntegrationClient = oldClientRepository;
				((TestRepositories)RuntimeContext.Repositories).Integration = oldIntegrationRepository;
			}			

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.IsEnabled.ShouldBeTrue();

			var user = DataCoreRepository.Users.First(x => x.ExternalId == request.ExternalId);
			user.ShouldNotBeNull();
			user.Person.ShouldNotBeNull();
		}

		[Test]
		[Explicit]
		public void LoginAssistSSOUser_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new LogInRequest
			{
				Type = LoginType.SSO,
				ExternalId = "OAuthAssistUser",
				EmailAddress = "newoauthassistuser@bg.com",
				FirstName = "OAuth",
				LastName = "User",
				ScreenName = "oAuth User",
				ModuleToLoginTo = FocusModules.Assist
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.LogIn(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.IsEnabled.ShouldBeTrue();

			var user = DataCoreRepository.Users.First(x => x.ExternalId == request.ExternalId);
			user.ShouldNotBeNull();
			user.Person.ShouldNotBeNull();
		}

		[Test]
		[Explicit]
		public void LoginTalentSSOUser_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new LogInRequest
			{
				Type = LoginType.SSO,
				ExternalId = "OAuthTalentUser",
				EmailAddress = "newoauthtalentuser@bg.com",
				FirstName = "OAuth",
				LastName = "User",
				ScreenName = "oAuth User",
				ModuleToLoginTo = FocusModules.Talent
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.LogIn(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.IsEnabled.ShouldBeTrue();

			var user = DataCoreRepository.Users.First(x => x.ExternalId == request.ExternalId);
			user.ShouldNotBeNull();
			user.Person.ShouldNotBeNull();
		}

		[Test]
		public void LogOut_WhenLoggedIn_ReturnsSuccess()
		{
			// Arrange
			var request = new LogOutRequest();
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.LogOut(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void LogOut_WhenNotLoggedIn_ReturnsSuccess()
		{
			// Arrange
			MockUserData.UserId = -1; // -1 to make it an invalid user
			
			var request = new LogOutRequest();
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.LogOut(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}
		
		#endregion

		#region Process Forgotten Password Tests
		
		[Test]
		public void ProcessForgottenPassword_WhenPassedValidUserNameAndValidSecurityAnswer_ReturnsSuccessAndEmailIsSentToUser()
		{
			MockAppSettings.SharedEncryptionKey = "88D8ACA4-1A3B-4C95-98C4-9801090D";

			var request = new ProcessForgottenPasswordRequest { UserName = MockTalentUsername, Module = FocusModules.Talent };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			var securityQuestions = new[]
			{
				new UserSecurityQuestionDto
				{
					QuestionIndex = 1,
					SecurityQuestionId = 4763960,
					SecurityQuestion = null,
					UserId = 17370,
					SecurityAnswerEncrypted = "red"
				},
				new UserSecurityQuestionDto
				{
					QuestionIndex = 1,
					SecurityQuestionId = 4763964,
					SecurityQuestion = null,
					UserId = 17370,
					SecurityAnswerEncrypted = "rover"
				}
			};
			// First set up the security questions for the user
			UserHelper.UpdateSecurityQuestions( 17370, securityQuestions, false );			

			// Now we can get the forgotten password service to check that we entered the security answers
			request.DayOfBirth = 6;
			request.MonthOfBirth = 9;
			request.SecurityQuestionId = 4763960;
			request.SecurityQuestion = null;
			request.SecurityAnswer = "red";

			// Act
			var response = _service.ProcessForgottenPassword(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.AttemptsFailed.ShouldEqual( false );
		}

		[Test]
		public void ProcessForgottenPassword_WhenPassedValidUserNameAndValidCustomSecurityAnswer_ReturnsSuccessAndEmailIsSentToUser()
		{
			MockAppSettings.SharedEncryptionKey = "88D8ACA4-1A3B-4C95-98C4-9801090D";

			var request = new ProcessForgottenPasswordRequest { UserName = MockTalentUsername, Module = FocusModules.Talent };
			request.Prepare( MockClientTag, MockSessionId, MockUserData, MockCulture );
			var securityQuestions = new[]
			{
				new UserSecurityQuestionDto
				{
					QuestionIndex = 1,
					SecurityQuestionId = null,
					SecurityQuestion = "Who is your favourite Star Trek character?",
					UserId = 17370,
					SecurityAnswerEncrypted = "Jean luc Picard"
				},
				new UserSecurityQuestionDto
				{
					QuestionIndex = 1,
					SecurityQuestionId = 4763964,
					SecurityQuestion = null,
					UserId = 17370,
					SecurityAnswerEncrypted = "rover"
				}
			};
			// First set up the security questions for the user
			UserHelper.UpdateSecurityQuestions( 17370, securityQuestions, false );

			// Now we can get the forgotten password service to check that we entered the security answers
			request.DayOfBirth = 6;
			request.MonthOfBirth = 9;
			request.SecurityQuestionId = 4763960;
			request.SecurityQuestion = "Who is your favourite Star Trek character?";
			request.SecurityAnswer = "Jean luc Picard";

			// Act
			var response = _service.ProcessForgottenPassword( request );

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual( AcknowledgementType.Success );
			response.AttemptsFailed.ShouldEqual( false );
		}

		[Test]
		public void ProcessForgottenPassword_WhenPassedValidUserNameAndInvalidSecurityAnswer_ReturnsFailure()
		{
			MockAppSettings.SharedEncryptionKey = "88D8ACA4-1A3B-4C95-98C4-9801090D";

			var request = new ProcessForgottenPasswordRequest { UserName = MockTalentUsername, Module = FocusModules.Talent };
			request.Prepare( MockClientTag, MockSessionId, MockUserData, MockCulture );
			var securityQuestions = new[]
			{
				new UserSecurityQuestionDto
				{
					QuestionIndex = 1,
					SecurityQuestionId = 4763960,
					SecurityQuestion = null,
					UserId = 17370,
					SecurityAnswerEncrypted = "red"
				},
				new UserSecurityQuestionDto
				{
					QuestionIndex = 1,
					SecurityQuestionId = 4763964,
					SecurityQuestion = null,
					UserId = 17370,
					SecurityAnswerEncrypted = "rover"
				}
			};
			// First set up the security questions for the user
			UserHelper.UpdateSecurityQuestions( 17370, securityQuestions, false );

			// Now we can get the forgotten password service to check that we entered the security answers
			request.DayOfBirth = 6;
			request.MonthOfBirth = 9;
			request.SecurityQuestionId = 4763960;
			request.SecurityQuestion = null;
			request.SecurityAnswer = "blue mist";

			// Act
			var response = _service.ProcessForgottenPassword( request );

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual( AcknowledgementType.Success );
			response.AttemptsFailed.ShouldEqual(true);
		}
		
		[Test]
		public void ProcessForgottenPassword_WhenPassedValidEmailAddress_ReturnsSuccessAndEmailIsSentToUser()
		{
			// Arrange
			var request = new ProcessForgottenPasswordRequest { EmailAddress = MockTalentEmailAddress, Module = FocusModules.Talent };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ProcessForgottenPassword(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}
		
		[Test]
		public void ProcessForgottenPassword_WhenPassedNullUserNameOrEmailAddress_ReturnsFailure()
		{
			// Arrange
			var request = new ProcessForgottenPasswordRequest ();
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ProcessForgottenPassword(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.Message.ShouldNotBeNullOrEmpty();
			response.Message.ShouldEqual(_service.FormatErrorMessage(request, ErrorTypes.MustSupplyUserNameOrEmailAddress));
		}
	
		[Test]
		public void ProcessForgottenPassword_WhenPassedInvalidUserName_ReturnsFailure()
		{
			// Arrange
			var request = new ProcessForgottenPasswordRequest { UserName = "InvalidUserName", Module = FocusModules.Talent };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ProcessForgottenPassword(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.Message.ShouldNotBeNullOrEmpty();
			response.Message.ShouldEqual(_service.FormatErrorMessage(request, ErrorTypes.UnableToProcessForgottenPassword));
		}
		
		[Test]
		public void ProcessForgottenPassword_WhenPassedInvalidEmailAddress_ReturnsFailure()
		{
			// Arrange
      var request = new ProcessForgottenPasswordRequest { EmailAddress = "InvalidEmailAddress", Module = FocusModules.Talent};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ProcessForgottenPassword(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.Message.ShouldNotBeNullOrEmpty();
			response.Message.ShouldEqual(_service.FormatErrorMessage(request, ErrorTypes.UnableToProcessForgottenPassword));
		}

		[Test]
		public void ProcessForgottenPassword_WhenUserDoesNotHaveEmailAddress_ReturnsFailure()
		{
			// Arrange
			var request = new ProcessForgottenPasswordRequest { UserName = MockUsername, Module = FocusModules.Talent};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			var user = new User { Enabled = true, UserName = MockUsername, UserType = UserTypes.Talent};
			var person = new Person { EmailAddress = "" };
			user.Person = person;
			var users = new List<User> {user};
			var session = new Session { Id = request.SessionId, LastRequestOn = DateTime.Now, UserId = MockUserData.UserId };
			var repository = new Mock<ICoreRepository>();
			repository.Setup(x => x.Users).Returns(users.AsQueryable());
			repository.Setup(x => x.FindById<User>(MockUserData.UserId)).Returns(user);
			repository.Setup(x => x.FindById<Session>(request.SessionId)).Returns(session);

			((TestRepositories)RuntimeContext.Repositories).Core = repository.Object;
			var service = new AuthenticationService(RuntimeContext);
			
			// Act
			var response = service.ProcessForgottenPassword(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.Message.ShouldEqual(_service.FormatErrorMessage(request, ErrorTypes.UserEmailAddressNotFound));
		}


		[Test]
		public void ProcessForgottenPassword_WhenCareerUserIsInActive_ReturnsSuccess()
		{
			// Arrange
			var request = new ProcessForgottenPasswordRequest { UserName = MockUsername, Module = FocusModules.Career };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			var user = new User { Enabled = false, UserName = MockUsername, UserType = UserTypes.Career, AccountDisabledReason = AccountDisabledReason.Inactivity};
			var person = new Person { EmailAddress = MockUsername };
			user.Person = person;
			var users = new List<User> { user };
			var session = new Session { Id = request.SessionId, LastRequestOn = DateTime.Now, UserId = MockUserData.UserId };
			var repository = new Mock<ICoreRepository>();
			repository.Setup(x => x.Users).Returns(users.AsQueryable());
			repository.Setup(x => x.FindById<User>(MockUserData.UserId)).Returns(user);
			repository.Setup(x => x.FindById<Session>(request.SessionId)).Returns(session);

			((TestRepositories)RuntimeContext.Repositories).Core = repository.Object;
			var service = new AuthenticationService(RuntimeContext);

			// Act
			var response = service.ProcessForgottenPassword(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
    
		}

		[Test]
		public void ProcessForgottenPassword_WhenCareerUserIsInActive_ReturnsFailure()
		{
			// Arrange
			var request = new ProcessForgottenPasswordRequest { UserName = MockUsername, Module = FocusModules.Career };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			var user = new User { Enabled = false, UserName = MockUsername, UserType = UserTypes.Career };
			var person = new Person { EmailAddress = MockUsername };
			user.Person = person;
			var users = new List<User> { user };
			var session = new Session { Id = request.SessionId, LastRequestOn = DateTime.Now, UserId = MockUserData.UserId };
			var repository = new Mock<ICoreRepository>();
			repository.Setup(x => x.Users).Returns(users.AsQueryable());
			repository.Setup(x => x.FindById<User>(MockUserData.UserId)).Returns(user);
			repository.Setup(x => x.FindById<Session>(request.SessionId)).Returns(session);

			((TestRepositories)RuntimeContext.Repositories).Core = repository.Object;
			var service = new AuthenticationService(RuntimeContext);

			// Act
			var response = service.ProcessForgottenPassword(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.Message.ShouldEqual(_service.FormatErrorMessage(request, ErrorTypes.UserNotEnabled));
		}
		#endregion

		#region Single Sign On Tests

		[Test]
		public void CreateSingleSignOn_WhenPassedValidUserId_ReturnsGuid()
		{
			// Arrange
			var user = new User {Id = 1, Enabled = true};
			var actioner = new User {Id = 2, Enabled = true};
			var request = new CreateSingleSignOnRequest { AccountUserId = user.Id };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			var session = new Session { Id = request.SessionId, LastRequestOn = DateTime.Now, UserId = MockUserData.UserId };
			var repository = new Mock<ICoreRepository>();
			repository.Setup(x => x.FindById<User>(user.Id)).Returns(user);
			repository.Setup(x => x.FindById<User>(MockUserData.UserId)).Returns(actioner);
			repository.Setup(x => x.FindById<Session>(request.SessionId)).Returns(session);

			((TestRepositories)RuntimeContext.Repositories).Core = repository.Object;
			var service = new AuthenticationService(RuntimeContext);
			
			// Act
			var response = service.CreateSingleSignOn(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.SingleSignOnKey.ShouldNotBeNull();
		}

		[Test]
		public void CreateSingleSignOn_WhenPassedValidEmployeeId_ReturnsGuid()
		{
			// Arrange
			var user = new User { Id = 1, Enabled = true };
			var employeePerson = new Person {Id = 4, User = user};
			var employee = new Employee {Id = 3, Person = employeePerson};
			var actioner = new User() { Id = 2, Enabled = true };
			var request = new CreateSingleSignOnRequest { AccountEmployeeId = employee.Id };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			var session = new Session { Id = request.SessionId, LastRequestOn = DateTime.Now, UserId = MockUserData.UserId };
			var repository = new Mock<ICoreRepository>();
			repository.Setup(x => x.FindById<User>(user.Id)).Returns(user);
			repository.Setup(x => x.FindById<User>(MockUserData.UserId)).Returns(actioner);
			repository.Setup(x => x.FindById<Session>(request.SessionId)).Returns(session);
			repository.Setup(x => x.FindById<Employee>(employee.Id)).Returns(employee);

			((TestRepositories)RuntimeContext.Repositories).Core = repository.Object;
			var service = new AuthenticationService(RuntimeContext);

			// Act
			var response = service.CreateSingleSignOn(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.SingleSignOnKey.ShouldNotBeNull();
		}

		[Test]
		public void CreateSingleSignOn_WhenPassedValidPersonId_ReturnsGuid()
		{
			// Arrange
			var request = new CreateSingleSignOnRequest { AccountPersonId = 1085521 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			// Act
			var response = _service.CreateSingleSignOn(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.SingleSignOnKey.ShouldNotBeNull();
		}
		
		[Test]
		public void CreateSingleSignOn_WhenNoUserIdOrEmployeeIdPassed_ReturnsFailure()
		{
			// Arrange
			var user = new User { Id = 1, Enabled = true };
			var actioner = new User() { Id = 2, Enabled = true };
			var request = new CreateSingleSignOnRequest { };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			var session = new Session { Id = request.SessionId, LastRequestOn = DateTime.Now, UserId = MockUserData.UserId };
			var repository = new Mock<ICoreRepository>();
			repository.Setup(x => x.FindById<User>(user.Id)).Returns(user);
			repository.Setup(x => x.FindById<User>(MockUserData.UserId)).Returns(actioner);
			repository.Setup(x => x.FindById<Session>(request.SessionId)).Returns(session);

			((TestRepositories)RuntimeContext.Repositories).Core = repository.Object;
			var service = new AuthenticationService(RuntimeContext);

			// Act
			var response = service.CreateSingleSignOn(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}

		[Test]
		[Ignore("There is an issue with getting the app settings - I have turned the test off as it fails")]
		public void CreateSingleSignOn_WhenInvalidUserContext_ReturnsFailure()
		{
			// Arrange
			var user = new User { Id = 1, Enabled = true };
			var actioner = new User { Id = 2, Enabled = false };

			var request = new CreateSingleSignOnRequest { AccountUserId = user.Id };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			var session = new Session { Id = request.SessionId, LastRequestOn = DateTime.Now, UserId = MockUserData.UserId };
			
			var repository = new Mock<ICoreRepository>();
			repository.Setup(x => x.FindById<User>(user.Id)).Returns(user);
			repository.Setup(x => x.FindById<User>(MockUserData.UserId)).Returns(actioner);
			repository.Setup(x => x.FindById<Session>(request.SessionId)).Returns(session);

			((TestRepositories)RuntimeContext.Repositories).Core = repository.Object;
			var service = new AuthenticationService(RuntimeContext);

			// Act
			var response = service.CreateSingleSignOn(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.Message.ShouldEqual("[ErrorType.UnknownClientTag]");
		}

		[Test]
		[Ignore("There is an issue with getting the app settings - I have turned the test off as it fails")]
		public void CreateSingleSignOn_WhenInvalidAccountToSignInto_ReturnsFailure()
		{
			// Arrange
			var user = new User { Id = 1, Enabled = false };
			var actioner = new User { Id = 2, Enabled = true };

			var request = new CreateSingleSignOnRequest { AccountUserId = user.Id };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			var session = new Session { Id = request.SessionId, LastRequestOn = DateTime.Now, UserId = MockUserData.UserId };

			var repository = new Mock<ICoreRepository>();
			repository.Setup(x => x.FindById<User>(user.Id)).Returns(user);
			repository.Setup(x => x.FindById<User>(MockUserData.UserId)).Returns(actioner);
			repository.Setup(x => x.FindById<Session>(request.SessionId)).Returns(session);

			((TestRepositories)RuntimeContext.Repositories).Core = repository.Object;
			var service = new AuthenticationService(RuntimeContext);

			// Act
			var response = service.CreateSingleSignOn(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.Message.ShouldEqual("[ErrorType.UnknownClientTag]");
		}

		[Test]
		public void ValidateSingleSignOn_WhenPassedValidGuid_ReturnsUserDetails()
		{
			// Arrange
			var user = new User { Id = 1, Enabled = true };
			var person = new Person { EmailAddress = "" };
			user.Person = person;
			var actioner = new User() { Id = 2, Enabled = true };
			var singleSignOnKey = Guid.NewGuid();
			var singleSignOn = new SingleSignOn {ActionerId = actioner.Id, UserId = user.Id, Id = singleSignOnKey};

			var request = new ValidateSingleSignOnRequest { SingleSignOnKey = singleSignOnKey };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			
			var session = new Session { Id = request.SessionId, LastRequestOn = DateTime.Now, UserId = MockUserData.UserId };
			var repository = new Mock<ICoreRepository>();
			repository.Setup(x => x.FindById<User>(user.Id)).Returns(user);
			repository.Setup(x => x.FindById<User>(actioner.Id)).Returns(actioner);
			repository.Setup(x => x.FindById<Session>(request.SessionId)).Returns(session);
			repository.Setup(x => x.FindById<SingleSignOn>(singleSignOnKey)).Returns(singleSignOn);

			((TestRepositories)RuntimeContext.Repositories).Core = repository.Object;
			var service = new AuthenticationService(RuntimeContext);

			// Act
			var response = service.ValidateSingleSignOn(request);
			
			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.UserId.ShouldNotBeNull();
		}
		
		[Test]
		public void ValidateSingleSignOn_WhenPassedInvalidSessionId_ReturnsFailure()
		{
			// Arrange
			var user = new User { Id = 1, Enabled = true };
			var actioner = new User() { Id = 2, Enabled = true };
			var singleSignOnKey = Guid.NewGuid();
			var singleSignOn = new SingleSignOn {ActionerId = actioner.Id, UserId = user.Id, Id = singleSignOnKey};

			var request = new ValidateSingleSignOnRequest { SingleSignOnKey = singleSignOnKey };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			
			var repository = new Mock<ICoreRepository>();
			repository.Setup(x => x.FindById<User>(user.Id)).Returns(user);
			repository.Setup(x => x.FindById<User>(actioner.Id)).Returns(actioner);
			repository.Setup(x => x.FindById<SingleSignOn>(singleSignOnKey)).Returns(singleSignOn);

			((TestRepositories)RuntimeContext.Repositories).Core = repository.Object;
			var service = new AuthenticationService(RuntimeContext);

			// Act
			var response = service.ValidateSingleSignOn(request);
			
			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}
		
		[Test]
		public void ValidateSingleSignOn_WhenPassedInvalidGuid_ReturnsFailure()
		{
			// Arrange
			var user = new User { Id = 1, Enabled = true };
			var actioner = new User() { Id = 2, Enabled = true };
			var singleSignOnKey = Guid.NewGuid();
			
			var request = new ValidateSingleSignOnRequest { SingleSignOnKey = singleSignOnKey };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			var session = new Session { Id = request.SessionId, LastRequestOn = DateTime.Now, UserId = MockUserData.UserId };
			var repository = new Mock<ICoreRepository>();
			repository.Setup(x => x.FindById<User>(user.Id)).Returns(user);
			repository.Setup(x => x.FindById<User>(actioner.Id)).Returns(actioner);
			repository.Setup(x => x.FindById<Session>(request.SessionId)).Returns(session);

			((TestRepositories)RuntimeContext.Repositories).Core = repository.Object;
			var service = new AuthenticationService(RuntimeContext);

			// Act
			var response = service.ValidateSingleSignOn(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}
		
		[Test]
		public void ValidateSingleSignOn_WhenRecordContainsInvalidUserId_ReturnsFailure()
		{
			// Arrange
			var actioner = new User() { Id = 2, Enabled = true };
			var singleSignOnKey = Guid.NewGuid();
			var singleSignOn = new SingleSignOn { ActionerId = actioner.Id, UserId = 1, Id = singleSignOnKey };

			var request = new ValidateSingleSignOnRequest { SingleSignOnKey = singleSignOnKey };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			var session = new Session { Id = request.SessionId, LastRequestOn = DateTime.Now, UserId = MockUserData.UserId };
			var repository = new Mock<ICoreRepository>();
			repository.Setup(x => x.FindById<User>(actioner.Id)).Returns(actioner);
			repository.Setup(x => x.FindById<Session>(request.SessionId)).Returns(session);
			repository.Setup(x => x.FindById<SingleSignOn>(singleSignOnKey)).Returns(singleSignOn);

			((TestRepositories)RuntimeContext.Repositories).Core = repository.Object;
			var service = new AuthenticationService(RuntimeContext);

			// Act
			var response = service.ValidateSingleSignOn(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}
		
		[Test]
		public void ValidateSingleSignOn_WhenRecordContainsInvalidActionerId_ReturnsFailure()
		{
			// Arrange
			var user = new User { Id = 1, Enabled = true };
			var singleSignOnKey = Guid.NewGuid();
			var singleSignOn = new SingleSignOn { ActionerId = 2, UserId = user.Id, Id = singleSignOnKey };

			var request = new ValidateSingleSignOnRequest { SingleSignOnKey = singleSignOnKey };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			var session = new Session { Id = request.SessionId, LastRequestOn = DateTime.Now, UserId = MockUserData.UserId };
			var repository = new Mock<ICoreRepository>();
			repository.Setup(x => x.FindById<User>(user.Id)).Returns(user);
			repository.Setup(x => x.FindById<Session>(request.SessionId)).Returns(session);
			repository.Setup(x => x.FindById<SingleSignOn>(singleSignOnKey)).Returns(singleSignOn);

			((TestRepositories)RuntimeContext.Repositories).Core = repository.Object;
			var service = new AuthenticationService(RuntimeContext);

			// Act
			var response = service.ValidateSingleSignOn(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}

		#endregion

    #region Career User Data tests

    [Test]
    public void GetCareerUserData_WhenUsingUserWithoutResume_ReturnsSuccess()
    {
      // Arrange
      var request = new CareerUserDataRequest();
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act
      var response = _service.GetCareerUserData(request);

      // Assert
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

      response.DefaultResumeId.ShouldBeNull();
      response.Profile.ShouldNotBeNull();
     }

    #endregion

		#region GetSecurityQuestions tests

		[Test]
		public void GetSecurityQuestions_With1Question_ReturnsSuccess()
		{
			// Arrange
			var request = new SecurityQuestionRequest { UserName = MockTalentUsername, Module = FocusModules.Talent };
			MockAppSettings.SharedEncryptionKey = "88D8ACA4-1A3B-4C95-98C4-9801090D";

			MockUserData.UserId = 17370;
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			var userSecurityQuestionRecord = new UserSecurityQuestion
			{
				QuestionIndex = 1,
				SecurityQuestion = "Who is your favourite Star Trek character?",
				SecurityQuestionId = null,
				SecurityAnswerHash = null,
				SecurityAnswerEncrypted = RuntimeContext.Helpers.Encryption.Encrypt( "Jean Luc Picard", TargetTypes.SecurityAnswer, EntityTypes.User, MockUserData.UserId ),
				UserId = MockUserData.UserId
			};
			DataCoreRepository.Add( userSecurityQuestionRecord );
			DataCoreRepository.SaveChanges();

			// Act
			var response = _service.GetSecurityQuestions(request);

			// Assert
			response.ShouldNotBeNull();
			response.SecurityQuestion1.ShouldEqual( "Who is your favourite Star Trek character?" );
			response.SecurityQuestionId1.ShouldEqual( null );
			response.SecurityAnswer1.ShouldEqual( "Jean Luc Picard" );
			response.SecurityQuestionIndex1.ShouldEqual(1);

			response.SecurityQuestion2.ShouldEqual( null );
			response.SecurityQuestionId2.ShouldEqual( null );
			response.SecurityAnswer2.ShouldEqual( null );
			response.SecurityQuestionIndex2.ShouldEqual( 0 );

			MockUserData.UserId = 2;
		}

		[Test]
		public void GetSecurityQuestions_With2Questions_ReturnsSuccess()
		{
			// Arrange
			var request = new SecurityQuestionRequest { UserName = MockTalentUsername, Module = FocusModules.Talent };
			MockAppSettings.SharedEncryptionKey = "88D8ACA4-1A3B-4C95-98C4-9801090D";

			MockUserData.UserId = 17370;
			request.Prepare( MockClientTag, MockSessionId, MockUserData, MockCulture );

			var userSecurityQuestionRecord = new UserSecurityQuestion
			{
				QuestionIndex = 1,
				SecurityQuestion = "Who is your favourite Star Trek character?",
				SecurityQuestionId = null,
				SecurityAnswerHash = null,
				SecurityAnswerEncrypted = RuntimeContext.Helpers.Encryption.Encrypt( "Jean Luc Picard", TargetTypes.SecurityAnswer, EntityTypes.User, MockUserData.UserId ),
				UserId = MockUserData.UserId
			};
			DataCoreRepository.Add( userSecurityQuestionRecord );

			userSecurityQuestionRecord = new UserSecurityQuestion
			{
				QuestionIndex = 2,
				SecurityQuestion = null,
				SecurityQuestionId = 4763960,
				SecurityAnswerHash = null,
				SecurityAnswerEncrypted = RuntimeContext.Helpers.Encryption.Encrypt( "Red", TargetTypes.SecurityAnswer, EntityTypes.User, MockUserData.UserId ),
				UserId = MockUserData.UserId
			};
			DataCoreRepository.Add( userSecurityQuestionRecord );
			DataCoreRepository.SaveChanges();

			// Act
			var response = _service.GetSecurityQuestions( request );

			// Assert
			response.ShouldNotBeNull();
			if( response.SecurityQuestionIndex1 == 1 )
			{
				response.SecurityQuestion1.ShouldEqual( "Who is your favourite Star Trek character?" );
				response.SecurityQuestionId1.ShouldEqual( null );
				response.SecurityAnswer1.ShouldEqual( "Jean Luc Picard" );

				response.SecurityQuestion2.ShouldEqual( null );
				response.SecurityQuestionId2.ShouldEqual( 4763960 );
				response.SecurityAnswer2.ShouldEqual( "Red" );
			}
			else
			{
				response.SecurityQuestion2.ShouldEqual( "Who is your favourite Star Trek character?" );
				response.SecurityQuestionId2.ShouldEqual( null );
				response.SecurityAnswer2.ShouldEqual( "Jean Luc Picard" );

				response.SecurityQuestion1.ShouldEqual( null );
				response.SecurityQuestionId1.ShouldEqual( 4763960 );
				response.SecurityAnswer1.ShouldEqual( "Red" );
			}
			MockUserData.UserId = 2;
		}

		[Test]
		public void GetSecurityQuestions_With3Questions_ReturnsSuccess()
		{
			// Arrange
			var request = new SecurityQuestionRequest { UserName = MockTalentUsername, Module = FocusModules.Talent };
			MockAppSettings.SharedEncryptionKey = "88D8ACA4-1A3B-4C95-98C4-9801090D";

			MockUserData.UserId = 17370;
			request.Prepare( MockClientTag, MockSessionId, MockUserData, MockCulture );

			var userSecurityQuestionRecord = new UserSecurityQuestion
			{
				QuestionIndex = 1,
				SecurityQuestion = "Who is your favourite Star Trek character?",
				SecurityQuestionId = null,
				SecurityAnswerHash = null,
				SecurityAnswerEncrypted = RuntimeContext.Helpers.Encryption.Encrypt( "Jean Luc Picard", TargetTypes.SecurityAnswer, EntityTypes.User, MockUserData.UserId ),
				UserId = MockUserData.UserId
			};
			DataCoreRepository.Add( userSecurityQuestionRecord );

			userSecurityQuestionRecord = new UserSecurityQuestion
			{
				QuestionIndex = 2,
				SecurityQuestion = null,
				SecurityQuestionId = 4763960,
				SecurityAnswerHash = null,
				SecurityAnswerEncrypted = RuntimeContext.Helpers.Encryption.Encrypt( "Red", TargetTypes.SecurityAnswer, EntityTypes.User, MockUserData.UserId ),
				UserId = MockUserData.UserId
			};
			DataCoreRepository.Add( userSecurityQuestionRecord );

			userSecurityQuestionRecord = new UserSecurityQuestion
			{
				QuestionIndex = 3,
				SecurityQuestion = null,
				SecurityQuestionId = 4763962,
				SecurityAnswerHash = null,
				SecurityAnswerEncrypted = RuntimeContext.Helpers.Encryption.Encrypt( "Farqueharson", TargetTypes.SecurityAnswer, EntityTypes.User, MockUserData.UserId ),
				UserId = MockUserData.UserId
			};
			DataCoreRepository.Add( userSecurityQuestionRecord );
			DataCoreRepository.SaveChanges();

			// As there is randomness involved with this request, run it 4 times.
			for (var i = 0; i < 4; i++)
			{
				// Act
				var response = _service.GetSecurityQuestions(request);

				// Assert
				response.ShouldNotBeNull();
				switch (response.SecurityQuestionIndex1)
				{
					case 1:
						response.SecurityQuestion1.ShouldEqual("Who is your favourite Star Trek character?");
						response.SecurityQuestionId1.ShouldEqual(null);
						response.SecurityAnswer1.ShouldEqual("Jean Luc Picard");
						break;
					case 2:
						response.SecurityQuestion1.ShouldEqual(null);
						response.SecurityQuestionId1.ShouldEqual(4763960);
						response.SecurityAnswer1.ShouldEqual("Red");
						break;
					case 3:
						response.SecurityQuestion1.ShouldEqual(null);
						response.SecurityQuestionId1.ShouldEqual(4763962);
						response.SecurityAnswer1.ShouldEqual("Farqueharson");
						break;
				}
				switch (response.SecurityQuestionIndex2)
				{
					case 1:
						response.SecurityQuestion2.ShouldEqual("Who is your favourite Star Trek character?");
						response.SecurityQuestionId2.ShouldEqual(null);
						response.SecurityAnswer2.ShouldEqual("Jean Luc Picard");
						break;
					case 2:
						response.SecurityQuestion2.ShouldEqual(null);
						response.SecurityQuestionId2.ShouldEqual(4763960);
						response.SecurityAnswer2.ShouldEqual("Red");
						break;
					case 3:
						response.SecurityQuestion2.ShouldEqual(null);
						response.SecurityQuestionId2.ShouldEqual(4763962);
						response.SecurityAnswer2.ShouldEqual("Farqueharson");
						break;
				}
			}
			MockUserData.UserId = 2;
		}

		#endregion

		#region ProcessSecurityQuestions tests

		[Test]
		public void ProcessSecurityQuestions_ReturnsSuccess()
		{
			// Arrange
			MockAppSettings.SharedEncryptionKey = "88D8ACA4-1A3B-4C95-98C4-9801090D";
			var userId = 17370;

			var userSecurityQuestionRecord = new UserSecurityQuestion
			{
				QuestionIndex = 1,
				SecurityQuestion = "Who is your favourite Star Trek character?",
				SecurityQuestionId = null,
				SecurityAnswerHash = null,
				SecurityAnswerEncrypted = RuntimeContext.Helpers.Encryption.Encrypt( "Jean Luc Picard", TargetTypes.SecurityAnswer, EntityTypes.User, userId ),
				UserId = userId
			};
			DataCoreRepository.Add( userSecurityQuestionRecord );

			userSecurityQuestionRecord = new UserSecurityQuestion
			{
				QuestionIndex = 2,
				SecurityQuestion = null,
				SecurityQuestionId = 4763960,
				SecurityAnswerHash = null,
				SecurityAnswerEncrypted = RuntimeContext.Helpers.Encryption.Encrypt( "Red", TargetTypes.SecurityAnswer, EntityTypes.User, userId ),
				UserId = userId
			};
			DataCoreRepository.Add( userSecurityQuestionRecord );
			DataCoreRepository.SaveChanges();

			var user = DataCoreRepository.Users.First(u => u.Id == userId);
			var resetPasswordUrl = string.Format("{0}{1}", ((FakeAppSettings)RuntimeContext.AppSettings).AssistApplicationPath, UrlBuilder.ResetPassword(false));

			var request = new ProcessSecurityQuestionsRequest().Prepare( MockClientTag, MockSessionId, MockUserData, MockCulture );

			request.SecurityQuestionIndex1 = 1;
			request.SecurityQuestionIndex2 = 2;
			request.SecurityAnswer1 = "Jean Luc Picard";
			request.SecurityAnswer2 = "Red";
			request.EmailAddress = user.UserName;
			request.UserName = user.UserName;
			request.Module = FocusModules.Talent;
			request.ResetPasswordUrl = resetPasswordUrl;

			// Act
			var response = _service.ProcessSecurityQuestions(request);

			// Assert
			response.ShouldNotBeNull();
			response.AttemptsFailed.ShouldEqual(false);
			response.AttemptsLeft.ShouldEqual( 0 );
		}

		[Test]
		public void ProcessSecurityQuestions_ReturnsFail()
		{
			// Arrange
			MockAppSettings.SharedEncryptionKey = "88D8ACA4-1A3B-4C95-98C4-9801090D";
			var userId = 17370;

			var userSecurityQuestionRecord = new UserSecurityQuestion
			{
				QuestionIndex = 1,
				SecurityQuestion = "Who is your favourite Star Trek character?",
				SecurityQuestionId = null,
				SecurityAnswerHash = null,
				SecurityAnswerEncrypted = RuntimeContext.Helpers.Encryption.Encrypt( "Jean Luc Picard", TargetTypes.SecurityAnswer, EntityTypes.User, userId ),
				UserId = userId
			};
			DataCoreRepository.Add( userSecurityQuestionRecord );

			userSecurityQuestionRecord = new UserSecurityQuestion
			{
				QuestionIndex = 2,
				SecurityQuestion = null,
				SecurityQuestionId = 4763960,
				SecurityAnswerHash = null,
				SecurityAnswerEncrypted = RuntimeContext.Helpers.Encryption.Encrypt( "Red", TargetTypes.SecurityAnswer, EntityTypes.User, userId ),
				UserId = userId
			};
			DataCoreRepository.Add( userSecurityQuestionRecord );
			DataCoreRepository.SaveChanges();

			var user = DataCoreRepository.Users.First( u => u.Id == userId );
			var resetPasswordUrl = string.Format( "{0}{1}", ((FakeAppSettings)RuntimeContext.AppSettings).AssistApplicationPath, UrlBuilder.ResetPassword( false ) );

			var request = new ProcessSecurityQuestionsRequest().Prepare( MockClientTag, MockSessionId, MockUserData, MockCulture );

			request.SecurityQuestionIndex1 = 1;
			request.SecurityQuestionIndex2 = 2;
			request.SecurityAnswer1 = "Will Riker";
			request.SecurityAnswer2 = "Blue";
			request.EmailAddress = user.UserName;
			request.UserName = user.UserName;
			request.Module = FocusModules.Talent;
			request.ResetPasswordUrl = resetPasswordUrl;

			// Act
			var response = _service.ProcessSecurityQuestions( request );

			// Assert
			response.ShouldNotBeNull();
			response.AttemptsFailed.ShouldEqual( true );
			response.AttemptsLeft.ShouldEqual( 2 );
		}

		[Test]
		public void ProcessSecurityQuestions_ReturnsFailTwice()
		{
			// Arrange
			MockAppSettings.SharedEncryptionKey = "88D8ACA4-1A3B-4C95-98C4-9801090D";
			var userId = 17370;

			var userSecurityQuestionRecord = new UserSecurityQuestion
			{
				QuestionIndex = 1,
				SecurityQuestion = "Who is your favourite Star Trek character?",
				SecurityQuestionId = null,
				SecurityAnswerHash = null,
				SecurityAnswerEncrypted = RuntimeContext.Helpers.Encryption.Encrypt( "Jean Luc Picard", TargetTypes.SecurityAnswer, EntityTypes.User, userId ),
				UserId = userId
			};
			DataCoreRepository.Add( userSecurityQuestionRecord );

			userSecurityQuestionRecord = new UserSecurityQuestion
			{
				QuestionIndex = 2,
				SecurityQuestion = null,
				SecurityQuestionId = 4763960,
				SecurityAnswerHash = null,
				SecurityAnswerEncrypted = RuntimeContext.Helpers.Encryption.Encrypt( "Red", TargetTypes.SecurityAnswer, EntityTypes.User, userId ),
				UserId = userId
			};
			DataCoreRepository.Add( userSecurityQuestionRecord );
			DataCoreRepository.SaveChanges();

			var user = DataCoreRepository.Users.First( u => u.Id == userId );
			var resetPasswordUrl = string.Format( "{0}{1}", ((FakeAppSettings)RuntimeContext.AppSettings).AssistApplicationPath, UrlBuilder.ResetPassword( false ) );

			var request = new ProcessSecurityQuestionsRequest().Prepare( MockClientTag, MockSessionId, MockUserData, MockCulture );

			request.SecurityQuestionIndex1 = 1;
			request.SecurityQuestionIndex2 = 2;
			request.SecurityAnswer1 = "Will Riker";
			request.SecurityAnswer2 = "Blue";
			request.EmailAddress = user.UserName;
			request.UserName = user.UserName;
			request.Module = FocusModules.Talent;
			request.ResetPasswordUrl = resetPasswordUrl;

			// Act
			var response = _service.ProcessSecurityQuestions( request );

			// Assert
			response.ShouldNotBeNull();
			response.AttemptsFailed.ShouldEqual( true );
			response.AttemptsLeft.ShouldEqual( 2 );

			// Arrange
			request.SecurityAnswer1 = "Jean Luc Picard";
			request.SecurityAnswer2 = "Blue";

			// Act
			response = _service.ProcessSecurityQuestions( request );

			// Assert
			response.ShouldNotBeNull();
			response.AttemptsFailed.ShouldEqual( true );
			response.AttemptsLeft.ShouldEqual( 1 );
		}

		#endregion
	}
}

