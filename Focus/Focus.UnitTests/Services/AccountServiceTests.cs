﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using Focus.Common.Models;
using Focus.Core;
using Focus.Core.Criteria.User;
using Focus.Core.Criteria;
using Focus.Core.Criteria.Role;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Messages.AccountService;
using Focus.Core.Models.Career;
using Focus.Core.Models.Integration;
using Focus.Data.Core.Entities;
using Focus.Data.Repositories.Contracts;
using Focus.Services.Integration;
using Focus.Services.ServiceImplementations;
using Focus.Services.DtoMappers;
using Focus.UnitTests.Core;
using Framework.Testing.NUnit;
using Constants = Focus.Core.Constants;
using IntegrationMessages = Focus.Core.IntegrationMessages;
using AcknowledgementType = Focus.Core.Messages.AcknowledgementType;
using Address = Focus.Core.Models.Career.Address;

using Framework.Core;

using Moq;

using NUnit.Framework;

#endregion

namespace Focus.UnitTests.Services
{
	[TestFixture]
	public class AccountServiceTests : TestFixtureBase
	{
		private AccountService _service;

		[SetUp]
		public override void SetUp()
		{
			base.SetUp();

			SetUpCacheProvider();
			SetUpMockLogProvider();

			_service = new AccountService(RuntimeContext);
		}

		#region Block/unblock user - this includes "deactivating" job seekers which is also done through this method

		[Test]
		public void BlockUnblockPerson_WhenPassedValidRequestToBlockTalentUser_ReturnsSuccess()
		{
			// Arrange
			var request = new BlockUnblockPersonRequest
			{
				PersonId = 17280,
				Block = true
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			// This service method never sets failure - just ignores request if no values
			var response = _service.BlockUnblockPerson(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Success.ShouldEqual(true);

			var person = DataCoreRepository.FindById<Person>(17280);

			// Blocked tests
			person.User.Blocked.ShouldBeTrue();
			DataCoreRepository.Offices.Any(x => x.DefaultAdministratorId.Equals(17280)).ShouldBeFalse();

			// Inactivated tests - for non-CE users the user shouldn't be inactivated (or at least not at this point since there's no way of re-activating them)
			person.User.Enabled.ShouldBeTrue();
			person.User.AccountDisabledReason.ShouldBeNull();
		}

		[Test]
		public void BlockUnblockPerson_WhenPassedValidRequestToBlockCareerUser_ReturnsSuccess()
		{
			// Arrange
			var request = new BlockUnblockPersonRequest()
			{
				PersonId = 1197307,
				Block = true
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			// This service method never sets failure - just ignores request if no values
			var response = _service.BlockUnblockPerson(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Success.ShouldEqual(true);

			var person = DataCoreRepository.FindById<Person>(1197307);

			// Blocked tests
			person.User.Blocked.ShouldBeTrue();
			DataCoreRepository.Offices.Any(x => x.DefaultAdministratorId.Equals(1197307)).ShouldBeFalse();

			// must not be disabled and be able to login
			person.User.Enabled.ShouldBeTrue();
			person.User.AccountDisabledReason.ShouldBeNull();
			person.Resumes.Any(x => x.IsDefault && x.IsSearchable).ShouldBeFalse();
			// Also unregisters resume from lens but can't test that
		}

		[Test]
		public void BlockUnblockPerson_WhenPassedInvalidRequestToUnblockTalentUser_ReturnsFailure()
		{
			// Arrange
			var request = new BlockUnblockPersonRequest()
			{
				PersonId = 17280,
				Block = false
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			// This service method never sets failure - just ignores request if no values
			var response = _service.BlockUnblockPerson(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Success.ShouldEqual(false);

			var person = DataCoreRepository.FindById<Person>(17280);

			// Blocked tests - nothing should have changed
			person.User.Blocked.ShouldBeFalse();

			// Inactivated tests - nothing should have changed
			person.User.Enabled.ShouldBeTrue();
			person.User.AccountDisabledReason.ShouldBeNull();
		}

		[Test]
		public void BlockUnblockPerson_WhenPassedInvalidRequestToUnblockCareerUser_ReturnsFailure()
		{
			// Arrange
			var request = new BlockUnblockPersonRequest()
			{
				PersonId = 1197307,
				Block = false
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			// This service method never sets failure - just ignores request if no values
			var response = _service.BlockUnblockPerson(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Success.ShouldEqual(false);

			var person = DataCoreRepository.FindById<Person>(1197307);

			// Blocked tests - nothing should have changed
			person.User.Blocked.ShouldBeFalse();

			// Inactivated tests - nothing should have changed
			person.User.Enabled.ShouldBeTrue();
			person.User.AccountDisabledReason.ShouldBeNull();
		}

		[Test]
		public void BlockUnblockPerson_WhenPassedValidRequestToDeactivateCareerUser_ReturnsSuccess()
		{
			// Arrange
			var request = new BlockUnblockPersonRequest
			{
				PersonId = 1116701,
				DeactivateJobSeeker = true,
				Block = true
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.BlockUnblockPerson(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Success.ShouldEqual(true);

			var person = DataCoreRepository.FindById<Person>(1116701);

			// Blocked tests
			person.User.Blocked.ShouldBeTrue();
			DataCoreRepository.Offices.Any(x => x.DefaultAdministratorId.Equals(1116701)).ShouldBeFalse();
			//Deactivated tests - does the same as block but with the following additions
			person.EmailAddress.Contains("DEACTIVATEDON").ShouldBeTrue();
			person.User.ExternalId.ShouldBeNull();

			// Inactivated tests
			person.User.Enabled.ShouldBeFalse();
			person.User.AccountDisabledReason.ShouldEqual(AccountDisabledReason.BlockedByStaffRequest);
			person.Resumes.Any(x => x.IsDefault && x.IsSearchable).ShouldBeFalse();
			// Also unregisters resume from lens but can't test that
		}

		[Test]
		public void BlockUnblockPerson_WhenPassedDeactivateFlagInValidUserTypeRequestToDecactivate_ReturnsFalse()
		{
			// Arrange
			var request = new BlockUnblockPersonRequest()
			{
				PersonId = 17370, // Talent user
				DeactivateJobSeeker = true,
				Block = true
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.BlockUnblockPerson(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.Success.ShouldEqual(false);

			var person = DataCoreRepository.FindById<Person>(1116701);
			// Blocked tests - nothing should have changed
			person.User.Blocked.ShouldBeFalse();
			//Deactivated tests - does the same as block but with the following additions
			person.EmailAddress.Contains("DEACTIVATEDON").ShouldBeFalse();
			person.User.ExternalId.ShouldNotBeNull();

			// Inactivated tests - nothing should have changed
			person.User.Enabled.ShouldBeTrue();
			person.User.AccountDisabledReason.ShouldBeNull();
		}

		[Test]
		public void BlockUnblockPerson_WhenPassedNonExistentUserRequestToDeactivate_ReturnsFalse()
		{
			// Arrange
			var request = new BlockUnblockPersonRequest()
			{
				PersonId = -9999999,
				DeactivateJobSeeker = true,
				Block = true
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.BlockUnblockPerson(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.Success.ShouldEqual(false);

			var person = DataCoreRepository.FindById<Person>(1116701);
			// Blocked tests - nothing should have changed
			person.User.Blocked.ShouldBeFalse();
			//Deactivated tests - does the same as block but with the following additions
			person.EmailAddress.Contains("DEACTIVATEDON").ShouldBeFalse();
			person.User.ExternalId.ShouldNotBeNull();

			// Inactivated tests - nothing should have changed
			person.User.Enabled.ShouldBeTrue();
			person.User.AccountDisabledReason.ShouldBeNull();
		}

		#endregion

		[Test]
		public void SendAccountActivationEmail_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new SendAccountActivationEmailRequest { UserName = "assisttest5@bg.com", Module = MockAppSettings.Module };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			// This service method never sets failure - just ignores request if no values
			var response = _service.SendAccountActivationEmail(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void ActivateUser_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new ActivateUserRequest { UserId = 1115456 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ActivateUser(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void DeactivateUser_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new DeactivateUserRequest { UserId = 1003967 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.DeactivateUser(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void GetRole_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new RoleRequest { Criteria = new RoleCriteria { UserId = 2, FetchOption = CriteriaBase.FetchOptions.List } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetRole(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void GetRolesLookup_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new RoleRequest { Criteria = new RoleCriteria { UserId = 2, FetchOption = CriteriaBase.FetchOptions.Lookup } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetRole(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void UpdateUsersRoles_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new UpdateUsersRolesRequest { UserId = 1103438, Roles = new List<RoleDto>() };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.UpdateUsersRoles(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void GetUserLookup_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new UserLookupRequest { Criteria = new UserCriteria { UserId = 1102165 } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetUserLookup(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void ResetPassword_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new ResetPasswordRequest
			{
				Module = FocusModules.Talent,
				UserId = 1104328,
				ValidationKey = "6Mhwdp0XWTvECl5aYDxnZ3pBo3iJ/e5+QcWdK2VI9wGWXAzWPrCc26+VpFayc4gTqfO/3Cjwh5GeXRRo5j+ZXA",
				NewPassword = MockValidPassword
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ResetPassword(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void RegisterExplorerUser_WithCustomSecurityQuestion_WhenPassedValidRequest_ReturnsSuccess()
		{
			MockAppSettings.SharedEncryptionKey = "88D8ACA4-1A3B-4C95-98C4-9801090D";

			// Arrange
			var request = new RegisterExplorerUserRequest
			{
				ScreenName = "explorertest@test.com",
				EmailAddress = "explorertest@test.com",
				Password = MockValidPassword,
				FirstName = "Forename",
				LastName = "Surname",
				DateOfBirth = new DateTime(1970, 1, 1),
				SecurityQuestion = "Q",
				SecurityQuestionId = null,
				SecurityAnswer = "A"
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.RegisterExplorerUser(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void RegisterExplorerUser_WithStandardSecurityQuestion_WhenPassedValidRequest_ReturnsSuccess()
		{
			MockAppSettings.SharedEncryptionKey = "88D8ACA4-1A3B-4C95-98C4-9801090D";

			// Arrange
			var request = new RegisterExplorerUserRequest
			{
				ScreenName = "explorertest@test.com",
				EmailAddress = "explorertest@test.com",
				Password = MockValidPassword,
				FirstName = "Forename",
				LastName = "Surname",
				DateOfBirth = new DateTime( 1970, 1, 1 ),
				SecurityQuestion = null,
				SecurityQuestionId = 4763960,
				SecurityAnswer = "A"
			};

			request.Prepare( MockClientTag, MockSessionId, MockUserData, MockCulture );

			// Act
			var response = _service.RegisterExplorerUser( request );

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual( AcknowledgementType.Success );
		}

		[Test]
		public void UpdateScreenNameAndEmailAddress_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new UpdateScreenNameAndEmailAddressRequest { ScreenName = "newemail@email.com", EmailAddress = "newemail@email.com" };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.UpdateScreenNameAndEmailAddress(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void Change_WithStandardSecurityQuestion_WhenPassedValidRequest_ReturnsSuccess()
		{
			MockAppSettings.SharedEncryptionKey = "88D8ACA4-1A3B-4C95-98C4-9801090D";

			// First set the first security question
			// Arrange
			var request = new ChangeSecurityQuestionRequest
			{
				ChangeSingleQuestion = true,
				Questions = new List<UserSecurityQuestionDto>
				{
					new UserSecurityQuestionDto { SecurityQuestion = null, SecurityQuestionId = 4763968, SecurityAnswerEncrypted = "NewAnswer", QuestionIndex = 1	}
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ChangeSecurityQuestion(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual( AcknowledgementType.Success );

			// Now set the second security question
			// Arrange
			request = new ChangeSecurityQuestionRequest
			{
				ChangeSingleQuestion = true,
				Questions = new List<UserSecurityQuestionDto>
				{
					new UserSecurityQuestionDto { SecurityQuestion = null, SecurityQuestionId = 4763962, SecurityAnswerEncrypted = "AnotherNewAnswer", QuestionIndex = 2	}
				}
			};
			request.Prepare( MockClientTag, MockSessionId, MockUserData, MockCulture );

			// Act
			response = _service.ChangeSecurityQuestion( request );

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			var securityQuestions = DataCoreRepository.UserSecurityQuestions.Where(usq => usq.UserId == 2).OrderBy(usq => usq.QuestionIndex).ToList();
			securityQuestions.Count().ShouldEqual( 2 );
			securityQuestions[0].QuestionIndex.ShouldEqual(1);
			securityQuestions[0].SecurityQuestion.ShouldBeNull();
			securityQuestions[0].SecurityQuestionId.ShouldEqual( 4763968 );
			securityQuestions[1].QuestionIndex.ShouldEqual( 2 );
			securityQuestions[1].SecurityQuestion.ShouldBeNull();
			securityQuestions[1].SecurityQuestionId.ShouldEqual( 4763962 );
		}

		[Test]
		public void Change_WithCustomSecurityQuestion_WhenPassedValidRequest_ReturnsSuccess()
		{
			MockAppSettings.SharedEncryptionKey = "88D8ACA4-1A3B-4C95-98C4-9801090D";

			// First set the first security question
			// Arrange
			var request = new ChangeSecurityQuestionRequest
			{
				ChangeSingleQuestion = true,
				Questions = new List<UserSecurityQuestionDto>
				{
					new UserSecurityQuestionDto { SecurityQuestion = "NewQuestion", SecurityAnswerEncrypted = "NewAnswer", QuestionIndex = 1	}
				}
			};
			request.Prepare( MockClientTag, MockSessionId, MockUserData, MockCulture );

			// Act
			var response = _service.ChangeSecurityQuestion( request );

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual( AcknowledgementType.Success );

			// Now set the second security question
			// Arrange
			request = new ChangeSecurityQuestionRequest
			{
				ChangeSingleQuestion = true,
				Questions = new List<UserSecurityQuestionDto>
				{
					new UserSecurityQuestionDto { SecurityQuestion = "AnotherNewQuestion", SecurityAnswerEncrypted = "AnotherNewAnswer", QuestionIndex = 2	}
				}
			};
			request.Prepare( MockClientTag, MockSessionId, MockUserData, MockCulture );

			// Act
			response = _service.ChangeSecurityQuestion( request );

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual( AcknowledgementType.Success );

			var securityQuestions = DataCoreRepository.UserSecurityQuestions.Where( usq => usq.UserId == 2 ).OrderBy( usq => usq.QuestionIndex ).ToList();
			securityQuestions.Count().ShouldEqual( 2 );
			securityQuestions[0].QuestionIndex.ShouldEqual( 1 );
			securityQuestions[0].SecurityQuestion.ShouldEqual( "NewQuestion" );
			securityQuestions[0].SecurityQuestionId.ShouldBeNull();
			securityQuestions[1].QuestionIndex.ShouldEqual( 2 );
			securityQuestions[1].SecurityQuestion.ShouldEqual( "AnotherNewQuestion" );
			securityQuestions[1].SecurityQuestionId.ShouldBeNull();
		}

		[Test]
		public void Change_WithCustomSecurityQuestion_CheckFieldLength_ReturnsSuccess()
		{
			const int userId = 2;
			MockAppSettings.SharedEncryptionKey = "88D8ACA4-1A3B-4C95-98C4-9801090D";

			const string longQuestion = "This is stupidly long question to elicit an even longer answer for no reason than to be an annoyance";
			const string longAnswer   = "I cannot recall why I set such a ridiculously long answer to this question or if I would remember it";

			// First set the first security question
			// Arrange
			var request = new ChangeSecurityQuestionRequest
			{
				ChangeSingleQuestion = true,
				Questions = new List<UserSecurityQuestionDto>
				{
					new UserSecurityQuestionDto { SecurityQuestion = longQuestion, SecurityAnswerEncrypted = longAnswer, QuestionIndex = 1 }
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			request.Prepare( MockClientTag, MockSessionId, MockUserData, MockCulture );

			// Act
			var response = _service.ChangeSecurityQuestion( request );

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual( AcknowledgementType.Success );

			var securityQuestions = DataCoreRepository.UserSecurityQuestions.Where( usq => usq.UserId == 2 ).OrderBy( usq => usq.QuestionIndex ).ToList();
			securityQuestions.Count().ShouldEqual( 1 );
			securityQuestions[0].QuestionIndex.ShouldEqual( 1 );
			securityQuestions[0].SecurityQuestion.ShouldEqual( longQuestion );
			securityQuestions[0].SecurityQuestionId.ShouldBeNull();
			RuntimeContext.Helpers.Encryption.Decrypt(securityQuestions[0].SecurityAnswerEncrypted, TargetTypes.SecurityAnswer, EntityTypes.User, userId).ShouldEqual(longAnswer);
		}

		[Test]
		public void Change_WithCustomSecurityQuestion_CheckQuestionFieldLength_ReturnsFailure()
		{
			MockAppSettings.SharedEncryptionKey = "88D8ACA4-1A3B-4C95-98C4-9801090D";

			const string longQuestion = "This is stupidly long question to elicit an even longer answer for no other reason than to be an annoyance";	// this is over 100 characters long
			const string longAnswer = "I cannot recall why I set such a ridiculously long answer to this question or if I would remember it";
			//"This is a very long answer to the question which was originally set by me a long long time ago, and I cannot remember for the life of me why I entered such a ridiculously long answer to this question or whether I would remember it";

			// First set the first security question
			// Arrange
			var request = new ChangeSecurityQuestionRequest
			{
				ChangeSingleQuestion = true,
				Questions = new List<UserSecurityQuestionDto>
				{
					new UserSecurityQuestionDto { SecurityQuestion = longQuestion, SecurityAnswerEncrypted = longAnswer, QuestionIndex = 1 }
				}
			};
			request.Prepare( MockClientTag, MockSessionId, MockUserData, MockCulture );

			// Act
			var response = _service.ChangeSecurityQuestion( request );

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual( AcknowledgementType.Failure );
			response.Exception.ShouldNotBeNull();
			response.Exception.Message.IndexOf( "Security Question length must be between 0 and 100", StringComparison.Ordinal ).ShouldBeGreater( 0 );
		}

		[Test]
		public void Change_WithCustomSecurityQuestion_CheckAnswerFieldLength_ReturnsFailure()
		{
			MockAppSettings.SharedEncryptionKey = "88D8ACA4-1A3B-4C95-98C4-9801090D";

			const string longQuestion = "This is stupidly long question to elicit an even longer answer for no reason than to be an annoyance";
            const string longAnswer = "I cannot remember why I entered such a ridiculously long answer to this question or whether I would remember it.I cannot remember why I entered such a ridiculously long answer to this question or whether I would remember it.I cannot remember why I entered such a ridiculously long answer to this question or whether I would remember it";	// this is over 250 characters long

			// First set the first security question
			// Arrange
			var request = new ChangeSecurityQuestionRequest
			{
				ChangeSingleQuestion = true,
				Questions = new List<UserSecurityQuestionDto>
				{
					new UserSecurityQuestionDto { SecurityQuestion = longQuestion, SecurityAnswerEncrypted = longAnswer, QuestionIndex = 1 }
				}
			};
			request.Prepare( MockClientTag, MockSessionId, MockUserData, MockCulture );

			// Act
			var response = _service.ChangeSecurityQuestion( request );

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual( AcknowledgementType.Failure );
			response.Exception.ShouldNotBeNull();
			response.Exception.Message.IndexOf( "Security Answer length must be between 0 and 250", StringComparison.Ordinal ).ShouldNotEqual(-1);
		}

		[Test]
		public void ChangeCareerAccountType_WhenPassedValidRequest_ReturnsSuccessAndUpdatesCareerAccountType()
		{
			// Arrange
			var request = new ChangeCareerAccountTypeRequest{ NewAccountType = CareerAccountType.Student };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ChangeCareerAccountType(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			var person = DataCoreRepository.Persons.First(x => x.Id == request.UserContext.PersonId);
			person.AccountType.ShouldEqual(request.NewAccountType);
		}

		[Test]
		public void RegisterCareerUser_WithStandardSecurityQuestion_WhenPassedValidRequest_ReturnsSuccess()
		{
			MockAppSettings.SharedEncryptionKey = "88D8ACA4-1A3B-4C95-98C4-9801090D";

			// Arrange
			var request = new RegisterCareerUserRequest
			{
				AccountUserName = "newcareeruser@bg.com",
				AccountPassword = MockValidPassword,
				DateOfBirth = new DateTime(1970, 1, 1),
				EmailAddress = "newcareeruser@bg.com",
				FirstName = "New",
				LastName = "User",
				SecurityQuestion = null,
				SecurityQuestionId = 4763962,
				SecurityAnswer = "A",
				PrimaryPhone = new Phone { PhoneType = PhoneType.Home, PhoneNumber = "000-000 0000" },
				PostalAddress = MockAddressForJobseeker
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.RegisterCareerUser(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			var user = DataCoreRepository.Users.First(x => x.UserName == request.AccountUserName);
			user.ShouldNotBeNull();
			user.Person.ShouldNotBeNull();

			var userSecurityQuestions = DataCoreRepository.UserSecurityQuestions.Where( x => x.UserId == user.Id );
			var userSecurityQuestion = userSecurityQuestions.First( usq => usq.QuestionIndex == 1 );
			userSecurityQuestion.ShouldNotBeNull();
			userSecurityQuestion.SecurityQuestion.ShouldBeNull();
			userSecurityQuestion.SecurityQuestionId.ShouldEqual(4763962);
			RuntimeContext.Helpers.Encryption.Decrypt(userSecurityQuestion.SecurityAnswerEncrypted, TargetTypes.SecurityAnswer, EntityTypes.User, user.Id).ShouldEqual("A");
		}

		[Test]
		public void RegisterCareerUser_WithCustomSecurityQuestion_WhenPassedValidRequest_ReturnsSuccess()
		{
			MockAppSettings.SharedEncryptionKey = "88D8ACA4-1A3B-4C95-98C4-9801090D";

			// Arrange
			var request = new RegisterCareerUserRequest
			{
				AccountUserName = "newcareeruser@bg.com",
				AccountPassword = MockValidPassword,
				DateOfBirth = new DateTime( 1970, 1, 1 ),
				EmailAddress = "newcareeruser@bg.com",
				FirstName = "New",
				LastName = "User",
				SecurityQuestion = "Who is your favourite character from Heroes?",
				SecurityQuestionId = null,
				SecurityAnswer = "Sylar",
				PrimaryPhone = new Phone { PhoneType = PhoneType.Home, PhoneNumber = "000-000 0000" },
				PostalAddress = MockAddressForJobseeker
			};

			request.Prepare( MockClientTag, MockSessionId, MockUserData, MockCulture );

			// Act
			var response = _service.RegisterCareerUser( request );

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual( AcknowledgementType.Success );

			var user = DataCoreRepository.Users.First( x => x.UserName == request.AccountUserName );
			user.ShouldNotBeNull();
			user.Person.ShouldNotBeNull();

			var userSecurityQuestions = DataCoreRepository.UserSecurityQuestions.Where( x => x.UserId == user.Id );
			var userSecurityQuestion = userSecurityQuestions.First( usq => usq.QuestionIndex == 1 );
			userSecurityQuestion.ShouldNotBeNull();
			userSecurityQuestion.SecurityQuestion.ShouldEqual( "Who is your favourite character from Heroes?" );
			userSecurityQuestion.SecurityQuestionId.ShouldBeNull();
			RuntimeContext.Helpers.Encryption.Decrypt( userSecurityQuestion.SecurityAnswerEncrypted, TargetTypes.SecurityAnswer, EntityTypes.User, user.Id ).ShouldEqual( "Sylar" );
		}

		[Test]
		public void RegisterCareerUser_WithStandardSecurityQuestion_WhenPassedValidRequestWithAssignedOffice_ReturnsSuccess()
		{
			MockAppSettings.SharedEncryptionKey = "88D8ACA4-1A3B-4C95-98C4-9801090D";

			// Arrange
			var request = new RegisterCareerUserRequest
			{
				AccountUserName = "newcareeruser@bg.com",
				AccountPassword = MockValidPassword,
				DateOfBirth = new DateTime(1970, 1, 1),
				EmailAddress = "newcareeruser@bg.com",
				FirstName = "New",
				LastName = "User",
				SecurityQuestion = null,
				SecurityQuestionId = 4763962,
				SecurityAnswer = "A",
				PrimaryPhone = new Phone { PhoneType = PhoneType.Home, PhoneNumber = "000-000 0000" },
				PostalAddress = MockAddressForJobseekerWithAssignedOffice
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.RegisterCareerUser(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			var user = DataCoreRepository.Users.First(x => x.UserName == request.AccountUserName);
			user.ShouldNotBeNull();
			user.Person.ShouldNotBeNull();
			var pom = DataCoreRepository.PersonOfficeMappers.First(x => x.PersonId == user.Person.Id);
			pom.ShouldNotBeNull();
			pom.OfficeId.ShouldNotBeNull();
			pom.OfficeId.ShouldEqual(1);
			pom.OfficeUnassigned.ShouldBeNull();

			var userSecurityQuestions = DataCoreRepository.UserSecurityQuestions.Where( x => x.UserId == user.Id );
			var userSecurityQuestion = userSecurityQuestions.First( usq => usq.QuestionIndex == 1 );
			userSecurityQuestion.ShouldNotBeNull();
			userSecurityQuestion.SecurityQuestion.ShouldBeNull();
			userSecurityQuestion.SecurityQuestionId.ShouldEqual( 4763962 );
			RuntimeContext.Helpers.Encryption.Decrypt( userSecurityQuestion.SecurityAnswerEncrypted, TargetTypes.SecurityAnswer, EntityTypes.User, user.Id ).ShouldEqual( "A" );
		}

		[Test]
		public void RegisterCareerUser_WithCustomSecurityQuestion_WhenPassedValidRequestWithAssignedOffice_ReturnsSuccess()
		{
			MockAppSettings.SharedEncryptionKey = "88D8ACA4-1A3B-4C95-98C4-9801090D";

			// Arrange
			var request = new RegisterCareerUserRequest
			{
				AccountUserName = "newcareeruser@bg.com",
				AccountPassword = MockValidPassword,
				DateOfBirth = new DateTime( 1970, 1, 1 ),
				EmailAddress = "newcareeruser@bg.com",
				FirstName = "New",
				LastName = "User",
				SecurityQuestion = "Who is your favourite character from Heroes?",
				SecurityQuestionId = null,
				SecurityAnswer = "Sylar",
				PrimaryPhone = new Phone { PhoneType = PhoneType.Home, PhoneNumber = "000-000 0000" },
				PostalAddress = MockAddressForJobseekerWithAssignedOffice
			};

			request.Prepare( MockClientTag, MockSessionId, MockUserData, MockCulture );

			// Act
			var response = _service.RegisterCareerUser( request );

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual( AcknowledgementType.Success );

			var user = DataCoreRepository.Users.First( x => x.UserName == request.AccountUserName );
			user.ShouldNotBeNull();
			user.Person.ShouldNotBeNull();
			var pom = DataCoreRepository.PersonOfficeMappers.First( x => x.PersonId == user.Person.Id );
			pom.ShouldNotBeNull();
			pom.OfficeId.ShouldNotBeNull();
			pom.OfficeId.ShouldEqual( 1 );
			pom.OfficeUnassigned.ShouldBeNull();

			var userSecurityQuestions = DataCoreRepository.UserSecurityQuestions.Where( x => x.UserId == user.Id );
			var userSecurityQuestion = userSecurityQuestions.First( usq => usq.QuestionIndex == 1 );
			userSecurityQuestion.ShouldNotBeNull();
			userSecurityQuestion.SecurityQuestion.ShouldEqual( "Who is your favourite character from Heroes?" );
			userSecurityQuestion.SecurityQuestionId.ShouldBeNull();
			RuntimeContext.Helpers.Encryption.Decrypt( userSecurityQuestion.SecurityAnswerEncrypted, TargetTypes.SecurityAnswer, EntityTypes.User, user.Id ).ShouldEqual( "Sylar" );
		}

		[Test]
		public void RegisterCareerUser_WithStandardSecurityQuestion_WhenPassedValidRequestWithUnAssignedOffice_ReturnsSuccess()
		{
			MockAppSettings.SharedEncryptionKey = "88D8ACA4-1A3B-4C95-98C4-9801090D";

			// Arrange
			var request = new RegisterCareerUserRequest
			{
				AccountUserName = "newcareeruser@bg.com",
				AccountPassword = MockValidPassword,
				DateOfBirth = new DateTime(1970, 1, 1),
				EmailAddress = "newcareeruser@bg.com",
				FirstName = "New",
				LastName = "User",
				SecurityQuestion = null,
				SecurityQuestionId = 4763960,
				SecurityAnswer = "A",
				PrimaryPhone = new Phone { PhoneType = PhoneType.Home, PhoneNumber = "000-000 0000" },
				PostalAddress = MockAddressForJobseekerWithUnAssignedOffice
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.RegisterCareerUser(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			var user = DataCoreRepository.Users.First(x => x.UserName == request.AccountUserName);
			user.ShouldNotBeNull();
			user.Person.ShouldNotBeNull();
			var pom = DataCoreRepository.PersonOfficeMappers.First(x => x.PersonId == user.Person.Id);
			pom.ShouldNotBeNull();
			pom.OfficeId.ShouldNotBeNull();
			pom.OfficeId.ShouldEqual(4);
			pom.OfficeUnassigned.ShouldNotBeNull();
			pom.OfficeUnassigned.ShouldEqual(true);

			var userSecurityQuestions = DataCoreRepository.UserSecurityQuestions.Where( x => x.UserId == user.Id );
			var userSecurityQuestion = userSecurityQuestions.First( usq => usq.QuestionIndex == 1 );
			userSecurityQuestion.ShouldNotBeNull();
			userSecurityQuestion.SecurityQuestion.ShouldBeNull();
			userSecurityQuestion.SecurityQuestionId.ShouldEqual( 4763960 );
			RuntimeContext.Helpers.Encryption.Decrypt( userSecurityQuestion.SecurityAnswerEncrypted, TargetTypes.SecurityAnswer, EntityTypes.User, user.Id ).ShouldEqual( "A" );
		}

		[Test]
		public void RegisterCareerUser_WithCustomSecurityQuestion_WhenPassedValidRequestWithUnAssignedOffice_ReturnsSuccess()
		{
			MockAppSettings.SharedEncryptionKey = "88D8ACA4-1A3B-4C95-98C4-9801090D";

			// Arrange
			var request = new RegisterCareerUserRequest
			{
				AccountUserName = "newcareeruser@bg.com",
				AccountPassword = MockValidPassword,
				DateOfBirth = new DateTime( 1970, 1, 1 ),
				EmailAddress = "newcareeruser@bg.com",
				FirstName = "New",
				LastName = "User",
				SecurityQuestion = "Who is your favourite character from Heroes?",
				SecurityQuestionId = null,
				SecurityAnswer = "Sylar",
				PrimaryPhone = new Phone { PhoneType = PhoneType.Home, PhoneNumber = "000-000 0000" },
				PostalAddress = MockAddressForJobseekerWithUnAssignedOffice
			};

			request.Prepare( MockClientTag, MockSessionId, MockUserData, MockCulture );

			// Act
			var response = _service.RegisterCareerUser( request );

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual( AcknowledgementType.Success );

			var user = DataCoreRepository.Users.First( x => x.UserName == request.AccountUserName );
			user.ShouldNotBeNull();
			user.Person.ShouldNotBeNull();
			var pom = DataCoreRepository.PersonOfficeMappers.First( x => x.PersonId == user.Person.Id );
			pom.ShouldNotBeNull();
			pom.OfficeId.ShouldNotBeNull();
			pom.OfficeId.ShouldEqual( 4 );
			pom.OfficeUnassigned.ShouldNotBeNull();
			pom.OfficeUnassigned.ShouldEqual( true );

			var userSecurityQuestions = DataCoreRepository.UserSecurityQuestions.Where( x => x.UserId == user.Id );
			var userSecurityQuestion = userSecurityQuestions.First( usq => usq.QuestionIndex == 1 );
			userSecurityQuestion.ShouldNotBeNull();
			userSecurityQuestion.SecurityQuestion.ShouldEqual( "Who is your favourite character from Heroes?" );
			userSecurityQuestion.SecurityQuestionId.ShouldBeNull();
			RuntimeContext.Helpers.Encryption.Decrypt( userSecurityQuestion.SecurityAnswerEncrypted, TargetTypes.SecurityAnswer, EntityTypes.User, user.Id ).ShouldEqual( "Sylar" );
		}

		[Test]
		[Ignore]
		public void RegisterCareerUser_WithStandardSecurityQuestion_WhenPassedValidRequestWithOverrideOffice_ReturnsSuccess()
		{
			MockAppSettings.SharedEncryptionKey = "88D8ACA4-1A3B-4C95-98C4-9801090D";

			var overrideOfficeId = DataCoreRepository.Offices
				.Where(office => !office.AssignedPostcodeZip.Contains("98765"))
				.OrderBy(office => office.Id)
				.Select(office => office.Id)
				.First();

			// Arrange
			var request = new RegisterCareerUserRequest
			{
				AccountUserName = "newcareeruser@bg.com",
				AccountPassword = MockValidPassword,
				DateOfBirth = new DateTime(1970, 1, 1),
				EmailAddress = "newcareeruser@bg.com",
				FirstName = "New",
				LastName = "User",
				SecurityQuestion = null,
				SecurityQuestionId = 4763968,
				SecurityAnswer = "A",
				PrimaryPhone = new Phone { PhoneType = PhoneType.Home, PhoneNumber = "000-000 0000" },
				PostalAddress = MockAddressForJobseekerWithUnAssignedOffice,
				OverrideOfficeId = overrideOfficeId
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.RegisterCareerUser(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			var user = DataCoreRepository.Users.First(x => x.UserName == request.AccountUserName);
			user.ShouldNotBeNull();
			user.Person.ShouldNotBeNull();

			var pom = DataCoreRepository.PersonOfficeMappers.First(x => x.PersonId == user.Person.Id);
			pom.ShouldNotBeNull();
			pom.OfficeId.ShouldNotBeNull();
			pom.OfficeId.ShouldEqual(overrideOfficeId);
			pom.OfficeUnassigned.ShouldBeNull();

			var checkOffice = DataCoreRepository.FindById<Office>(overrideOfficeId);
			checkOffice.AssignedPostcodeZip.Contains("11111").ShouldBeTrue();

			var userSecurityQuestions = DataCoreRepository.UserSecurityQuestions.Where( x => x.UserId == user.Id );
			var userSecurityQuestion = userSecurityQuestions.First( usq => usq.QuestionIndex == 1 );
			userSecurityQuestion.ShouldNotBeNull();
			userSecurityQuestion.SecurityQuestion.ShouldBeNull();
			userSecurityQuestion.SecurityQuestionId.ShouldEqual( 4763968 );
			RuntimeContext.Helpers.Encryption.Decrypt( userSecurityQuestion.SecurityAnswerEncrypted, TargetTypes.SecurityAnswer, EntityTypes.User, user.Id ).ShouldEqual( "A" );
		}

		[Test]
		[Ignore]
		// FVN-185 : Code temporarily commented out as logic may not be what was actually required by the business
		public void RegisterCareerUser_WithCustomSecurityQuestion_WhenPassedValidRequestWithOverrideOffice_ReturnsSuccess()
		{
			MockAppSettings.SharedEncryptionKey = "88D8ACA4-1A3B-4C95-98C4-9801090D";

			var overrideOfficeId = DataCoreRepository.Offices
				.Where( office => !office.AssignedPostcodeZip.Contains( "98765" ) )
				.OrderBy( office => office.Id )
				.Select( office => office.Id )
				.First();

			// Arrange
			var request = new RegisterCareerUserRequest
			{
				AccountUserName = "newcareeruser@bg.com",
				AccountPassword = MockValidPassword,
				DateOfBirth = new DateTime( 1970, 1, 1 ),
				EmailAddress = "newcareeruser@bg.com",
				FirstName = "New",
				LastName = "User",
				SecurityQuestion = "Who is your favourite character from Heroes?",
				SecurityQuestionId = null,
				SecurityAnswer = "Sylar",
				PrimaryPhone = new Phone { PhoneType = PhoneType.Home, PhoneNumber = "000-000 0000" },
				PostalAddress = MockAddressForJobseekerWithUnAssignedOffice,
				OverrideOfficeId = overrideOfficeId
			};

			request.Prepare( MockClientTag, MockSessionId, MockUserData, MockCulture );

			// Act
			var response = _service.RegisterCareerUser( request );

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual( AcknowledgementType.Success );

			var user = DataCoreRepository.Users.First( x => x.UserName == request.AccountUserName );
			user.ShouldNotBeNull();
			user.Person.ShouldNotBeNull();

			var pom = DataCoreRepository.PersonOfficeMappers.First( x => x.PersonId == user.Person.Id );
			pom.ShouldNotBeNull();
			pom.OfficeId.ShouldNotBeNull();
			pom.OfficeId.ShouldEqual( overrideOfficeId );
			pom.OfficeUnassigned.ShouldBeNull();

			var checkOffice = DataCoreRepository.FindById<Office>( overrideOfficeId );
			checkOffice.AssignedPostcodeZip.Contains( "11111" ).ShouldBeTrue();

			var userSecurityQuestions = DataCoreRepository.UserSecurityQuestions.Where( x => x.UserId == user.Id );
			var userSecurityQuestion = userSecurityQuestions.First( usq => usq.QuestionIndex == 1 );
			userSecurityQuestion.ShouldNotBeNull();
			userSecurityQuestion.SecurityQuestion.ShouldEqual( "Who is your favourite character from Heroes?" );
			userSecurityQuestion.SecurityQuestionId.ShouldBeNull();
			RuntimeContext.Helpers.Encryption.Decrypt( userSecurityQuestion.SecurityAnswerEncrypted, TargetTypes.SecurityAnswer, EntityTypes.User, user.Id ).ShouldEqual( "Sylar" );
		}

		[Test]
		public void ChangeMigratedStatus_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new ChangeMigratedStatusRequest { IsMigrated = true };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ChangeMigratedStatus(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void ChangeConsentStatus_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new ChangeConsentStatusRequest { IsConsentGiven = true };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ChangeConsentStatus(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		#region Change Password Tests

		[Test]
		public void ChangePassword_WhenPassedValidUserWithCorrectCurrentPassword_ReturnsSuccessAndPasswordChanged()
		{
			// Arrange
			var request = new ChangePasswordRequest { CurrentPassword = MockPassword, NewPassword = "NewPassword1" };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ChangePassword(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void ChangePassword_WhenPassedUnknownUser_ReturnsFailure()
		{
			// Arrange
			MockUserData.UserId = MockUserData.ActionerId = 0;
			var request = new ChangePasswordRequest { CurrentPassword = MockPassword, NewPassword = "NewPassword" };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ChangePassword(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.Message.ShouldNotBeNullOrEmpty();
			response.Message.ShouldEqual(_service.FormatErrorMessage(request, ErrorTypes.LogInRequired) + Environment.NewLine);
		}

		[Test]
		public void ChangePassword_WhenPassedValidUserWithIncorrectCurrectPassword_ReturnsFailure()
		{
			// Arrange
			var request = new ChangePasswordRequest { CurrentPassword = "WrongPassword", NewPassword = "NewPassword" };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ChangePassword(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.Message.ShouldNotBeNullOrEmpty();
			response.Message.ShouldEqual(_service.FormatErrorMessage(request, ErrorTypes.CurrentPasswordDoesNotMatch));
		}

		#endregion

		#region Validate Employer Tests

		[Test]
		public void ValidateEmployer_WhenPassedFEINOfCurrentEmployerForClient_ReturnsSuccessWithEmployerDetails()
		{
			// Arrange
			var request = new ValidateEmployerRequest { FederalEmployerIdentificationNumber = "01-000001" };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			var validateResult = new IntegrationMessages.GetEmployerResponse(null)
			{
				Employer = new EmployerModel
				{
					Name = "DevEmployer",
					FederalEmployerIdentificationNumber = "01-000001",
					IsValidFederalEmployerIdentificationNumber = true,
					Url = "http://" + "www.spoof-" + "01-000001" + ".com",
					Phone = "11-" + "01-000001",
					IsRegistrationComplete = false
				}
			};

			var employerRepositoryMock = new Mock<IIntegrationRepository>();
			employerRepositoryMock.Setup(x => x.GetEmployer(It.IsAny<IntegrationMessages.GetEmployerRequest>())).Returns(validateResult);

			((TestRepositories)RuntimeContext.Repositories).Integration = employerRepositoryMock.Object;
			var service = new AccountService(RuntimeContext);

			// Act
			var response = service.ValidateEmployer(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.ValidatedEmployer.ShouldNotBeNull();
			response.ValidatedEmployer.Name.ShouldEqual("DevEmployer");
		}

		[Test]
		public void ValidateEmployer_WhenPassedFEINOfNewEmployer_ReturnsSuccessWithEmployerDetails()
		{
			var request = new ValidateEmployerRequest { FederalEmployerIdentificationNumber = "00-000000" };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			var validateResult = new IntegrationMessages.GetEmployerResponse(null)
			{
				Employer = new EmployerModel
				{
					Name = "Spoof 00-000000 Ltd",
					FederalEmployerIdentificationNumber = "00-000000",
					IsValidFederalEmployerIdentificationNumber = true,
					Url = "http://" + "www.spoof-" + "00-000000" + ".com",
					Phone = "11-" + "00-000000",
					IsRegistrationComplete = false
				}
			};

			var employerRepositoryMock = new Mock<IIntegrationRepository>();
			employerRepositoryMock.Setup(x => x.GetEmployer(It.IsAny<IntegrationMessages.GetEmployerRequest>())).Returns(validateResult);

			((TestRepositories)RuntimeContext.Repositories).Integration = employerRepositoryMock.Object;
			var service = new AccountService(RuntimeContext);

			// Act
			var response = service.ValidateEmployer(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.ValidatedEmployer.ShouldNotBeNull();
		}

		[Test]
		public void ValidateEmployer_WhenPassedInvalidFEIN_ReturnsSuccessWithNoEmployerDetails()
		{
			var request = new ValidateEmployerRequest { FederalEmployerIdentificationNumber = "01-010101" };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			var validateResult = new IntegrationMessages.GetEmployerResponse(null)
			{
				Employer = null
			};

			var employerRepositoryMock = new Mock<IIntegrationRepository>();
			employerRepositoryMock.Setup(x => x.GetEmployer(It.IsAny<IntegrationMessages.GetEmployerRequest>())).Returns(validateResult);

			((TestRepositories)RuntimeContext.Repositories).Integration = employerRepositoryMock.Object;
			var service = new AccountService(RuntimeContext);

			// Act
			var response = service.ValidateEmployer(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.ValidatedEmployer.ShouldBeNull();
		}

		#endregion

		#region Register User Tests

		[Test]
		public void RegisterTalentUser_WhenPassedExistingUserDetails_ReturnsFailure()
		{
			// Arrange
			var request = new RegisterTalentUserRequest { Module = MockAppSettings.Module, AccountUserName = MockUsername };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.RegisterTalentUser(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.Message.ShouldNotBeNull();
			response.Message.ShouldEqual(_service.FormatErrorMessage(request, ErrorTypes.UserNameAlreadyExists));
		}

		[Test]
		public void RegisterTalentUser_WhenPassedNewUserDataAndNewEmployer_ReturnsSuccessAndEmployeeUserCreated()
		{
			// Arrange
			MockUserData.FirstName = "NewDev";
			MockUserData.LastName = "NewEmployee";

			var request = GetRegisterAccountNewRequest();
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.RegisterTalentUser(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			var user = DataCoreRepository.Users.First(x => x.UserName == MockNewUserName);
			user.Person.ShouldNotBeNull();
			user.Person.FirstName.ShouldEqual(MockNewEmployeePerson.FirstName);
			user.Person.LastName.ShouldEqual(MockNewEmployeePerson.LastName);
			user.UserRoles.Count.ShouldEqual(1);
			user.Person.Employee.ShouldNotBeNull();
			user.Person.Employee.Employer.Name.ShouldEqual(MockNewEmployer.Name);
			user.Person.PhoneNumbers.Count.ShouldEqual(3);
			user.Person.Employee.Employer.ShouldNotBeNull();
			user.Person.Employee.Employer.BusinessUnits.ShouldNotBeNull();
			user.Person.Employee.Employer.BusinessUnits.Count.ShouldEqual(1);
			user.Person.Employee.Employer.BusinessUnits[0].BusinessUnitDescriptions.ShouldNotBeNull();
			user.Person.Employee.Employer.BusinessUnits[0].BusinessUnitDescriptions.Count.ShouldEqual(1);
		}

		[Test]
		public void RegisterTalentUser_WithStandardSecurityQuestions_WhenPassedNewUserDataAndNewEmployerAndSecurityQuestions_ReturnsSuccessAndEmployeeUserCreated()
		{
			// Arrange
			MockAppSettings.SharedEncryptionKey = "88D8ACA4-1A3B-4C95-98C4-9801090D";

			MockUserData.FirstName = "NewDev";
			MockUserData.LastName = "NewEmployee";

			var request = GetRegisterAccountNewRequest();
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			request.SecurityQuestions = new[]
			{
				new UserSecurityQuestionDto
				{
					QuestionIndex = 1,
					SecurityQuestion = null,
					SecurityQuestionId = 4763960,
					SecurityAnswerEncrypted = @"red"
				},
				new UserSecurityQuestionDto
				{
					QuestionIndex = 2,
					SecurityQuestion = null,
					SecurityQuestionId = 4763962,
					SecurityAnswerEncrypted = @"jones"
				},
				new UserSecurityQuestionDto
				{
					QuestionIndex = 3,
					SecurityQuestion = null,
					SecurityQuestionId = 4763964,
					SecurityAnswerEncrypted = @"rover"
				}
			};

			// Act
			var response = _service.RegisterTalentUser(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			var user = DataCoreRepository.Users.First(x => x.UserName == MockNewUserName);
			var userSecurityQuestions = DataCoreRepository.UserSecurityQuestions.Where(x => x.UserId == user.Id);
			user.Person.ShouldNotBeNull();
			user.Person.FirstName.ShouldEqual(MockNewEmployeePerson.FirstName);
			user.Person.LastName.ShouldEqual(MockNewEmployeePerson.LastName);
			user.UserRoles.Count.ShouldEqual(1);
			user.Person.Employee.ShouldNotBeNull();
			user.Person.Employee.Employer.Name.ShouldEqual(MockNewEmployer.Name);
			user.Person.PhoneNumbers.Count.ShouldEqual(3);
			user.Person.Employee.Employer.ShouldNotBeNull();
			user.Person.Employee.Employer.BusinessUnits.ShouldNotBeNull();
			user.Person.Employee.Employer.BusinessUnits.Count.ShouldEqual(1);
			user.Person.Employee.Employer.BusinessUnits[0].BusinessUnitDescriptions.ShouldNotBeNull();
			user.Person.Employee.Employer.BusinessUnits[0].BusinessUnitDescriptions.Count.ShouldEqual(1);

			userSecurityQuestions.First(usq => usq.QuestionIndex == 1).ShouldNotBeNull();
			userSecurityQuestions.First(usq => usq.QuestionIndex == 2).ShouldNotBeNull();
			userSecurityQuestions.First(usq => usq.QuestionIndex == 3).ShouldNotBeNull();

			userSecurityQuestions.First(usq => usq.QuestionIndex == 1).SecurityQuestion.ShouldBeNull();
			userSecurityQuestions.First(usq => usq.QuestionIndex == 2).SecurityQuestion.ShouldBeNull();
			userSecurityQuestions.First(usq => usq.QuestionIndex == 3).SecurityQuestion.ShouldBeNull();

			userSecurityQuestions.First(usq => usq.QuestionIndex == 1).SecurityQuestionId.ShouldEqual(4763960);
			userSecurityQuestions.First(usq => usq.QuestionIndex == 2).SecurityQuestionId.ShouldEqual(4763962);
			userSecurityQuestions.First(usq => usq.QuestionIndex == 3).SecurityQuestionId.ShouldEqual(4763964);

			userSecurityQuestions.First(usq => usq.QuestionIndex == 1).SecurityAnswerEncrypted.ShouldNotBeNull();
			userSecurityQuestions.First(usq => usq.QuestionIndex == 2).SecurityAnswerEncrypted.ShouldNotBeNull();
			userSecurityQuestions.First(usq => usq.QuestionIndex == 3).SecurityAnswerEncrypted.ShouldNotBeNull();

			MockAppSettings.SharedEncryptionKey = string.Empty;
		}

		[Test]
		public void RegisterTalentUser_WithCustomSecurityQuestions_WhenPassedNewUserDataAndNewEmployerAndSecurityQuestions_ReturnsSuccessAndEmployeeUserCreated()
		{
			// Arrange
			MockAppSettings.SharedEncryptionKey = "88D8ACA4-1A3B-4C95-98C4-9801090D";

			MockUserData.FirstName = "NewDev";
			MockUserData.LastName = "NewEmployee";
			const string question1 = "Who is your favorite Star Trek character?";
			const string question2 = "What was the name of your first girlfriend?";
			const string question3 = "In which country is your favourite holiday resort?";

			var request = GetRegisterAccountNewRequest();
			request.Prepare( MockClientTag, MockSessionId, MockUserData, MockCulture );

			request.SecurityQuestions = new[]
			{
				new UserSecurityQuestionDto
				{
					QuestionIndex = 1,
					SecurityQuestion = question1,
					SecurityQuestionId = null,
					SecurityAnswerEncrypted = @"Jean Luc Picard"
				},
				new UserSecurityQuestionDto
				{
					QuestionIndex = 2,
					SecurityQuestion = question2,
					SecurityQuestionId = null,
					SecurityAnswerEncrypted = @"Grizelda"
				},
				new UserSecurityQuestionDto
				{
					QuestionIndex = 3,
					SecurityQuestion = question3,
					SecurityQuestionId = null,
					SecurityAnswerEncrypted = @"Afghanistan"
				}
			};

			// Act
			var response = _service.RegisterTalentUser( request );

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual( AcknowledgementType.Success );

			var user = DataCoreRepository.Users.First( x => x.UserName == MockNewUserName );
			var userSecurityQuestions = DataCoreRepository.UserSecurityQuestions.Where( x => x.UserId == user.Id );
			user.Person.ShouldNotBeNull();
			user.Person.FirstName.ShouldEqual( MockNewEmployeePerson.FirstName );
			user.Person.LastName.ShouldEqual( MockNewEmployeePerson.LastName );
			user.UserRoles.Count.ShouldEqual( 1 );
			user.Person.Employee.ShouldNotBeNull();
			user.Person.Employee.Employer.Name.ShouldEqual( MockNewEmployer.Name );
			user.Person.PhoneNumbers.Count.ShouldEqual( 3 );
			user.Person.Employee.Employer.ShouldNotBeNull();
			user.Person.Employee.Employer.BusinessUnits.ShouldNotBeNull();
			user.Person.Employee.Employer.BusinessUnits.Count.ShouldEqual( 1 );
			user.Person.Employee.Employer.BusinessUnits[0].BusinessUnitDescriptions.ShouldNotBeNull();
			user.Person.Employee.Employer.BusinessUnits[0].BusinessUnitDescriptions.Count.ShouldEqual( 1 );

			userSecurityQuestions.First( usq => usq.QuestionIndex == 1 ).ShouldNotBeNull();
			userSecurityQuestions.First( usq => usq.QuestionIndex == 2 ).ShouldNotBeNull();
			userSecurityQuestions.First( usq => usq.QuestionIndex == 3 ).ShouldNotBeNull();

			userSecurityQuestions.First(usq => usq.QuestionIndex == 1).SecurityQuestion.ShouldEqual(question1);
			userSecurityQuestions.First(usq => usq.QuestionIndex == 2).SecurityQuestion.ShouldEqual(question2);
			userSecurityQuestions.First(usq => usq.QuestionIndex == 3).SecurityQuestion.ShouldEqual(question3);

			userSecurityQuestions.First( usq => usq.QuestionIndex == 1 ).SecurityQuestionId.ShouldBeNull();
			userSecurityQuestions.First( usq => usq.QuestionIndex == 2 ).SecurityQuestionId.ShouldBeNull();
			userSecurityQuestions.First( usq => usq.QuestionIndex == 3 ).SecurityQuestionId.ShouldBeNull();

			userSecurityQuestions.First( usq => usq.QuestionIndex == 1 ).SecurityAnswerEncrypted.ShouldNotBeNull();
			userSecurityQuestions.First( usq => usq.QuestionIndex == 2 ).SecurityAnswerEncrypted.ShouldNotBeNull();
			userSecurityQuestions.First( usq => usq.QuestionIndex == 3 ).SecurityAnswerEncrypted.ShouldNotBeNull();

			MockAppSettings.SharedEncryptionKey = string.Empty;
		}

		[Test]
		public void RegisterTalentUser_WhenPassedNewMinimumUserDataAndNewEmployer_ReturnsSuccessAndEmployeeUserCreated()
		{
			// Arrange
			MockUserData.FirstName = "NewDev";
			MockUserData.LastName = "NewEmployee";

			var request = GetRegisterAccountNewRequest();
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Remove all employer non-required items
			request.BusinessUnit = null;
			request.Employer.StateEmployerIdentificationNumber = null;
			request.EmployerAddress.Line2 = request.EmployerAddress.Line3 = null;
			request.BusinessUnitDescription = null;
			request.Employer.Url = null;
			request.Employer.AlternatePhone1 = request.Employer.AlternatePhone1 = null;

			// Remove all employee non-required items
			request.EmployeePerson.JobTitle = null;
			request.EmployeePerson.MiddleInitial = null;
			request.EmployeeAlternatePhone1 = request.EmployeeAlternatePhone2 = null;

			// Act
			var response = _service.RegisterTalentUser(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			var user = DataCoreRepository.Users.First(x => x.UserName == MockNewUserName);
			user.Person.ShouldNotBeNull();
			user.Person.FirstName.ShouldEqual(MockNewEmployeePerson.FirstName);
			user.Person.LastName.ShouldEqual(MockNewEmployeePerson.LastName);
			user.UserRoles.Count.ShouldEqual(1);
			user.Person.Employee.ShouldNotBeNull();
			user.Person.Employee.Employer.Name.ShouldEqual(MockNewEmployer.Name);
			user.Person.PhoneNumbers.Count.ShouldEqual(1);
			user.Person.Employee.Employer.ShouldNotBeNull();
			user.Person.Employee.Employer.BusinessUnits.ShouldNotBeNull();
			user.Person.Employee.Employer.BusinessUnits.Count.ShouldEqual(0);
		}

		[Test]
		public void RegisterTalentUser_WhenPassedNewUserDataAndExistingEmployer_ReturnsSuccessAndEmployeeUserCreated()
		{
			// Arrange
			var employer = DataCoreRepository.Employers.FirstOrDefault(x => x.Name == "DevEmployer");
			MockUserData.FirstName = "NewDev";
			MockUserData.LastName = "NewEmployee";
			var businessUnit = employer.BusinessUnits.Where(x => x.IsPrimary).Select(x => x.AsDto()).FirstOrDefault();

			var request = GetRegisterAccountSpecifiedRequest(employer.AsDto(), businessUnit);
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.RegisterTalentUser(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			var user = DataCoreRepository.Users.First(x => x.UserName == MockNewUserName);
			user.Person.ShouldNotBeNull();
			user.Person.FirstName.ShouldEqual(MockNewEmployeePerson.FirstName);
			user.Person.LastName.ShouldEqual(MockNewEmployeePerson.LastName);
			user.UserRoles.Count.ShouldEqual(1);
			user.Person.Employee.ShouldNotBeNull();
			user.Person.Employee.Employer.Name.ShouldEqual(MockNewEmployer.Name);
			user.Person.PhoneNumbers.Count.ShouldEqual(3);
			user.Person.Employee.Employer.ShouldNotBeNull();
			user.Person.Employee.Employer.BusinessUnits.ShouldNotBeNull();
			user.Person.Employee.Employer.BusinessUnits.Count.ShouldEqual(2);
			user.Person.Employee.Employer.EmployerAddresses.ShouldNotBeNull();
			user.Person.Employee.Employer.EmployerAddresses.Count.ShouldEqual(1);
		}

		[Test]
		public void RegisterTalentUser_WhenPassedNewUserDataAndExistingEmployerByIdNotFound_ReturnsFailure()
		{
			// Arrange
			var employer = DataCoreRepository.Employers.FirstOrDefault(x => x.Name == "DevEmployer");
			var employerDto = employer.AsDto();
			employerDto.Id = 9999;

			var request = new RegisterTalentUserRequest
			{
				Module = MockAppSettings.Module,
				Employer = employerDto,
				EmployeePerson = MockNewEmployeePerson,
				AccountUserName = MockNewUserName,
				AccountPassword = MockPassword
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.RegisterTalentUser(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.Message.ShouldEqual(_service.FormatErrorMessage(request, ErrorTypes.EmployerNotFound));
		}

		[Test]
		public void RegisterTalentUser_WhenPassedNewUserDataAndExistingEmployee_ReturnsFailure()
		{
			// Arrange
			var employer = DataCoreRepository.Employers.FirstOrDefault(x => x.Name == "DevEmployer");

			var request = new RegisterTalentUserRequest { Module = MockAppSettings.Module, Employer = employer.AsDto(), EmployeePerson = MockNewEmployeePerson, AccountUserName = MockUsername, AccountPassword = MockPassword };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.RegisterTalentUser(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}

		[Test]
		public void RegisterTalentUser_CreateAnException_ReturnsFailure()
		{
			// Arrange
			MockUserData.FirstName = null;

			var request = new RegisterTalentUserRequest { Module = MockAppSettings.Module, Employer = null, EmployeePerson = MockNewEmployeePerson, AccountUserName = MockUsername, AccountPassword = MockPassword };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.RegisterTalentUser(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}

		[Test]
		public void RegisterTalentUser_WhenNotPassedPhoneNumber_ReturnsFailure()
		{
			MockUserData.FirstName = "NewDev";
			MockUserData.LastName = "NewEmployee";

			var request = GetRegisterAccountNewRequest();
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			request.EmployeePhone = null;

			// Act
			var response = _service.RegisterTalentUser(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.Message.ShouldEqual(_service.FormatErrorMessage(request, ErrorTypes.PhoneNumberMissing));
		}

		[Test]
		public void RegisterTalentUser_WhenNotPassedFaxNumber_ReturnsSuccess()
		{
			MockUserData.FirstName = "NewDev";
			MockUserData.LastName = "NewEmployee";

			var request = GetRegisterAccountNewRequest();
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			request.EmployeeAlternatePhone2 = null;

			// Act
			var response = _service.RegisterTalentUser(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void RegisterTalentUser_WhenNotPassedEmailAddress_ReturnsFailure()
		{
			// Arrange
			MockUserData.FirstName = "NewDev";
			MockUserData.LastName = "NewEmployee";

			var request = GetRegisterAccountNewRequest();
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			request.EmployeeEmailAddress = null;

			// Act
			var response = _service.RegisterTalentUser(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.Message.ShouldEqual(_service.FormatErrorMessage(request, ErrorTypes.PersonEmailAddressMissing));
		}

		[Test]
		public void RegisterTalentUser_WhenNotPassedAlternatePhoneNumber_ReturnsSuccess()
		{
			MockUserData.FirstName = "NewDev";
			MockUserData.LastName = "NewEmployee";

			var request = GetRegisterAccountNewRequest();
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.RegisterTalentUser(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void RegisterTalentUser_WhenNotPassedEmployeeAdddressDetails_ReturnsSuccess()
		{
			// Arrange
			MockUserData.FirstName = "NewDev";
			MockUserData.LastName = "NewEmployee";

			var request = GetRegisterAccountNewRequest();
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			request.EmployeeAddress = null;

			// Act
			var response = _service.RegisterTalentUser(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void RegisterTalentUser_WhenNotPassedEmployerAdddressDetails_ReturnsFailure()
		{
			// Arrange
			MockUserData.FirstName = "NewDev";
			MockUserData.LastName = "NewEmployee";

			var request = GetRegisterAccountNewRequest();
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			request.EmployerAddress = null;

			// Act
			var response = _service.RegisterTalentUser(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}

		[Test]
		public void RegisterTalentUser_WhenNotPassedEmployerBusinessUnit_ReturnsSuccess()
		{
			// Arrange
			var employer = DataCoreRepository.Employers.FirstOrDefault(x => x.Name == "DevEmployer");
			MockUserData.FirstName = "NewDev";
			MockUserData.LastName = "NewEmployee";
			var businessUnit = employer.BusinessUnits.Where(x => x.IsPrimary).Select(x => x.AsDto()).FirstOrDefault();

			var request = GetRegisterAccountSpecifiedRequest(employer.AsDto(), businessUnit);
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			request.BusinessUnit = null;

			// Act
			var response = _service.RegisterTalentUser(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void RegisterTalentUser_WhenEmployerBusinessUnitFindByID_ReturnsSuccess()
		{
			// Arrange
			var employer = DataCoreRepository.Employers.FirstOrDefault(x => x.Name == "DevEmployer");
			MockUserData.FirstName = "NewDev";
			MockUserData.LastName = "NewEmployee";
			var businessUnit = employer.BusinessUnits.Where(x => x.IsPrimary).Select(x => x.AsDto()).FirstOrDefault();
			var request = GetRegisterAccountSpecifiedRequest(employer.AsDto(), businessUnit);
			request.BusinessUnit = businessUnit;
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.RegisterTalentUser(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			employer.BusinessUnits.ShouldNotBeNull();
			employer.BusinessUnits.Count.ShouldEqual(2);
			employer.BusinessUnits.FirstOrDefault(x => x.IsPrimary).Name.ShouldEqual("PrimaryBusinessUnit");
		}

		[Test]
		public void RegisterTalentUser_WhenNotPassedEmployerDescription_ReturnsSuccess()
		{
			// Arrange
			var employer = DataCoreRepository.Employers.FirstOrDefault(x => x.Name == "DevEmployer");
			MockUserData.FirstName = "NewDev";
			MockUserData.LastName = "NewEmployee";
			var businessUnit = employer.BusinessUnits.Where(x => x.IsPrimary).Select(x => x.AsDto()).FirstOrDefault();

			var request = GetRegisterAccountSpecifiedRequest(employer.AsDto(), businessUnit);
			request.BusinessUnitDescription = null;
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.RegisterTalentUser(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void RegisterTalentUser_WhenNotPassedEmployerAddress_ReturnsFailure()
		{
			// Arrange
			var employer = DataCoreRepository.Employers.FirstOrDefault(x => x.Name == "DevEmployer");
			MockUserData.FirstName = "NewDev";
			MockUserData.LastName = "NewEmployee";
			var businessUnit = employer.BusinessUnits.Where(x => x.IsPrimary).Select(x => x.AsDto()).FirstOrDefault();

			var request = GetRegisterAccountSpecifiedRequest(employer.AsDto(), businessUnit);
			request.EmployerAddress = null;
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.RegisterTalentUser(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.Message.ShouldEqual(_service.FormatErrorMessage(request, ErrorTypes.EmployerAddressMissing));
		}

		[Test]
		public void RegisterTalentUser_WhenEmployerAddressFindByID_ReturnsSuccess()
		{
			// Arrange
			var employer = DataCoreRepository.Employers.FirstOrDefault(x => x.Name == "DevEmployer");
			MockUserData.FirstName = "NewDev";
			MockUserData.LastName = "NewEmployee";
			var employerAddress = employer.EmployerAddresses.FirstOrDefault(x => x.IsPrimary);
			var businessUnit = employer.BusinessUnits.Where(x => x.IsPrimary).Select(x => x.AsDto()).FirstOrDefault();

			var request = GetRegisterAccountSpecifiedRequest(employer.AsDto(), businessUnit);
			request.EmployerAddress = new EmployerAddressDto
			{
				Id = employerAddress.Id,
				IsPrimary = false,
				Line1 = employerAddress.Line1 + "xxx",
				Line2 = employerAddress.Line2 + "xxx",
				Line3 = employerAddress.Line3 + "xxx",
				TownCity = employerAddress.TownCity + "xxx",
				CountyId = employerAddress.CountyId + 10,
				StateId = employerAddress.StateId + 10,
				CountryId = employerAddress.CountryId + 10,
				PostcodeZip = employerAddress.PostcodeZip + "xxx"
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.RegisterTalentUser(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			employer.EmployerAddresses.ShouldNotBeNull();
			employer.EmployerAddresses.Count.ShouldEqual(1);
			employer.EmployerAddresses.FirstOrDefault(x => x.IsPrimary).Line1.ShouldEqual(employerAddress.Line1);
			employer.EmployerAddresses.FirstOrDefault(x => x.IsPrimary).Line2.ShouldEqual(employerAddress.Line2);
			employer.EmployerAddresses.FirstOrDefault(x => x.IsPrimary).Line3.ShouldEqual(employerAddress.Line3);
			employer.EmployerAddresses.FirstOrDefault(x => x.IsPrimary).TownCity.ShouldEqual(employerAddress.TownCity);
			employer.EmployerAddresses.FirstOrDefault(x => x.IsPrimary).PostcodeZip.ShouldEqual(employerAddress.PostcodeZip);
			employer.EmployerAddresses.FirstOrDefault(x => x.IsPrimary).CountyId.ShouldEqual(employerAddress.CountyId);
			employer.EmployerAddresses.FirstOrDefault(x => x.IsPrimary).StateId.ShouldEqual(employerAddress.StateId);
			employer.EmployerAddresses.FirstOrDefault(x => x.IsPrimary).CountryId.ShouldEqual(employerAddress.CountryId);
		}

		[Test]
		public void RegisterTalentUser_WhenEmployerAddressFindByIsPrimary_ReturnsSuccess()
		{
			// Arrange
			var employer = DataCoreRepository.Employers.FirstOrDefault(x => x.Name == "DevEmployer");
			MockUserData.FirstName = "NewDev";
			MockUserData.LastName = "NewEmployee";
			var employerAddress = employer.EmployerAddresses.FirstOrDefault(x => x.IsPrimary);
			var businessUnit = employer.BusinessUnits.Where(x => x.IsPrimary).Select(x => x.AsDto()).FirstOrDefault();

			var request = GetRegisterAccountSpecifiedRequest(employer.AsDto(), businessUnit);
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			request.EmployerAddress = new EmployerAddressDto
			{
				Id = employerAddress.Id,
				IsPrimary = false,
				Line1 = employerAddress.Line1 + "xxx",
				Line2 = employerAddress.Line2 + "xxx",
				Line3 = employerAddress.Line3 + "xxx",
				TownCity = employerAddress.TownCity + "xxx",
				CountyId = employerAddress.CountyId + 10,
				StateId = employerAddress.StateId + 10,
				CountryId = employerAddress.CountryId + 10,
				PostcodeZip = employerAddress.PostcodeZip + "xxx"
			};

			// Act
			var response = _service.RegisterTalentUser(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			employer.EmployerAddresses.ShouldNotBeNull();
			employer.EmployerAddresses.Count.ShouldEqual(1);
			employer.EmployerAddresses.FirstOrDefault(x => x.IsPrimary).Line1.ShouldEqual(employerAddress.Line1);
			employer.EmployerAddresses.FirstOrDefault(x => x.IsPrimary).Line2.ShouldEqual(employerAddress.Line2);
			employer.EmployerAddresses.FirstOrDefault(x => x.IsPrimary).Line3.ShouldEqual(employerAddress.Line3);
			employer.EmployerAddresses.FirstOrDefault(x => x.IsPrimary).TownCity.ShouldEqual(employerAddress.TownCity);
			employer.EmployerAddresses.FirstOrDefault(x => x.IsPrimary).PostcodeZip.ShouldEqual(employerAddress.PostcodeZip);
			employer.EmployerAddresses.FirstOrDefault(x => x.IsPrimary).CountyId.ShouldEqual(employerAddress.CountyId);
			employer.EmployerAddresses.FirstOrDefault(x => x.IsPrimary).StateId.ShouldEqual(employerAddress.StateId);
			employer.EmployerAddresses.FirstOrDefault(x => x.IsPrimary).CountryId.ShouldEqual(employerAddress.CountryId);
		}

		[Test]
		public void RegisterTalentUser_OAtuh_ReturnsSuccess()
		{
			// Arrange
			var employer = DataCoreRepository.Employers.FirstOrDefault(x => x.Name == "DevEmployer");
			MockUserData.FirstName = "NewDev";
			MockUserData.LastName = "NewEmployee";
			var employerAddress = employer.EmployerAddresses.FirstOrDefault(x => x.IsPrimary);
			var businessUnit = employer.BusinessUnits.Where(x => x.IsPrimary).Select(x => x.AsDto()).FirstOrDefault();

			var request = GetRegisterAccountSpecifiedRequest(employer.AsDto(), businessUnit);

			request.AccountUserName = "";
			request.AccountPassword = "";

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			request.User = DataCoreRepository.FindById<User>(1197306).AsDto();
			request.EmployeePerson = DataCoreRepository.FindById<Person>(request.User.PersonId).AsDto();
			request.EmployeePerson.TitleId = 12780;
			request.EmployeePerson.DateOfBirth = DateTime.Now.AddYears(-21);
			request.EmployeePerson.JobTitle = "TestJob";
			request.EmployeePerson.SocialSecurityNumber = "078-05-1120";

			request.EmployerAddress = new EmployerAddressDto
			{
				Id = employerAddress.Id,
				IsPrimary = false,
				Line1 = employerAddress.Line1 + "xxx",
				Line2 = employerAddress.Line2 + "xxx",
				Line3 = employerAddress.Line3 + "xxx",
				TownCity = employerAddress.TownCity + "xxx",
				CountyId = employerAddress.CountyId + 10,
				StateId = employerAddress.StateId + 10,
				CountryId = employerAddress.CountryId + 10,
				PostcodeZip = employerAddress.PostcodeZip + "xxx"
			};

			// Act
			var response = _service.RegisterTalentUser(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			employer.EmployerAddresses.ShouldNotBeNull();
			employer.EmployerAddresses.Count.ShouldEqual(1);
			employer.EmployerAddresses.FirstOrDefault(x => x.IsPrimary).Line1.ShouldEqual(employerAddress.Line1);
			employer.EmployerAddresses.FirstOrDefault(x => x.IsPrimary).Line2.ShouldEqual(employerAddress.Line2);
			employer.EmployerAddresses.FirstOrDefault(x => x.IsPrimary).Line3.ShouldEqual(employerAddress.Line3);
			employer.EmployerAddresses.FirstOrDefault(x => x.IsPrimary).TownCity.ShouldEqual(employerAddress.TownCity);
			employer.EmployerAddresses.FirstOrDefault(x => x.IsPrimary).PostcodeZip.ShouldEqual(employerAddress.PostcodeZip);
			employer.EmployerAddresses.FirstOrDefault(x => x.IsPrimary).CountyId.ShouldEqual(employerAddress.CountyId);
			employer.EmployerAddresses.FirstOrDefault(x => x.IsPrimary).StateId.ShouldEqual(employerAddress.StateId);
			employer.EmployerAddresses.FirstOrDefault(x => x.IsPrimary).CountryId.ShouldEqual(employerAddress.CountryId);
		}

		[Test]
		public void RegisterTalentUser_WhenEmployerAddressFindByValue_ReturnsSuccess()
		{
			// Arrange
			var employer = DataCoreRepository.Employers.FirstOrDefault(x => x.Name == "DevEmployer");
			MockUserData.FirstName = "NewDev";
			MockUserData.LastName = "NewEmployee";
			var employerAddress = employer.EmployerAddresses.FirstOrDefault(x => x.IsPrimary);
			var businessUnit = employer.BusinessUnits.Where(x => x.IsPrimary).Select(x => x.AsDto()).FirstOrDefault();

			var request = GetRegisterAccountSpecifiedRequest(employer.AsDto(), businessUnit);
			request.EmployerAddress = new EmployerAddressDto
			{
				Id = employerAddress.Id,
				IsPrimary = false,
				Line1 = employerAddress.Line1 + "xxx",
				Line2 = employerAddress.Line2 + "xxx",
				Line3 = employerAddress.Line3 + "xxx",
				TownCity = employerAddress.TownCity + "xxx",
				CountyId = employerAddress.CountyId + 10,
				StateId = employerAddress.StateId + 10,
				CountryId = employerAddress.CountryId + 10,
				PostcodeZip = employerAddress.PostcodeZip + "xxx"
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.RegisterTalentUser(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			employer.EmployerAddresses.ShouldNotBeNull();
			employer.EmployerAddresses.Count.ShouldEqual(1);
			employer.EmployerAddresses.FirstOrDefault(x => x.IsPrimary).Line1.ShouldEqual(employerAddress.Line1);
			employer.EmployerAddresses.FirstOrDefault(x => x.IsPrimary).Line2.ShouldEqual(employerAddress.Line2);
			employer.EmployerAddresses.FirstOrDefault(x => x.IsPrimary).Line3.ShouldEqual(employerAddress.Line3);
			employer.EmployerAddresses.FirstOrDefault(x => x.IsPrimary).TownCity.ShouldEqual(employerAddress.TownCity);
			employer.EmployerAddresses.FirstOrDefault(x => x.IsPrimary).PostcodeZip.ShouldEqual(employerAddress.PostcodeZip);
			employer.EmployerAddresses.FirstOrDefault(x => x.IsPrimary).CountyId.ShouldEqual(employerAddress.CountyId);
			employer.EmployerAddresses.FirstOrDefault(x => x.IsPrimary).StateId.ShouldEqual(employerAddress.StateId);
			employer.EmployerAddresses.FirstOrDefault(x => x.IsPrimary).CountryId.ShouldEqual(employerAddress.CountryId);
		}

		[Test]
		public void RegisterTalentUser_WhenPassedAddressWithAssignedOffice_ReturnsSuccess()
		{
			// Arrange
			MockUserData.FirstName = "NewDev";
			MockUserData.LastName = "NewEmployee";

			var request = GetRegisterAccountNewRequestWithAssignedOffice();
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.RegisterTalentUser(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			var user = DataCoreRepository.Users.First(x => x.UserName == MockNewUserName);
			user.ShouldNotBeNull();
			user.Person.ShouldNotBeNull();
			user.Person.Employee.ShouldNotBeNull();
			user.Person.Employee.Employer.ShouldNotBeNull();

			var employerOfficeMappers = DataCoreRepository.EmployerOfficeMappers.Where(x => x.EmployerId == user.Person.Employee.Employer.Id).ToList();
			employerOfficeMappers.ShouldNotBeNull();
			employerOfficeMappers.Count.ShouldEqual(1);
		}

		[Test]
		public void RegisterTalentUser_WhenPassedAddressWithMultiAssignedOffice_ReturnsSuccess()
		{
			// Arrange
			MockUserData.FirstName = "NewDev";
			MockUserData.LastName = "NewEmployee";

			var request = GetRegisterAccountNewRequestWithMultiAssignedOffice();
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.RegisterTalentUser(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			var user = DataCoreRepository.Users.First(x => x.UserName == MockNewUserName);
			user.ShouldNotBeNull();
			user.Person.ShouldNotBeNull();
			user.Person.Employee.ShouldNotBeNull();
			user.Person.Employee.Employer.ShouldNotBeNull();

			var employerOfficeMappers = DataCoreRepository.EmployerOfficeMappers.Where(x => x.EmployerId == user.Person.Employee.Employer.Id).ToList();
			employerOfficeMappers.ShouldNotBeNull();
			employerOfficeMappers.Count.ShouldEqual(2);
		}

		[Test]
		public void RegisterTalentUser_WhenPassedAddressWithUnAssignedOffice_ReturnsSuccess()
		{
			// Arrange
			MockUserData.FirstName = "NewDev";
			MockUserData.LastName = "NewEmployee";

			var request = GetRegisterAccountNewRequest();
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.RegisterTalentUser(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			var user = DataCoreRepository.Users.First(x => x.UserName == MockNewUserName);
			user.ShouldNotBeNull();
			user.Person.ShouldNotBeNull();
			user.Person.Employee.ShouldNotBeNull();
			user.Person.Employee.Employer.ShouldNotBeNull();

			var employerOfficeMappers = DataCoreRepository.EmployerOfficeMappers.Where(x => x.EmployerId == user.Person.Employee.Employer.Id).ToList();
			employerOfficeMappers.ShouldNotBeNull();
			employerOfficeMappers.Count.ShouldEqual(1);
			employerOfficeMappers[0].OfficeId.ShouldEqual(4);
			employerOfficeMappers[0].OfficeUnassigned.ShouldEqual(true);
		}

		#endregion

		#region Check User Exists Tests

		[Test]
		public void CheckUserExists_WhenProvideAnExistingUserName_ReturnsSuccessAndExistsIsTrue()
		{
			// Arrange
			var request = new CheckUserExistsRequest { UserName = MockUsername, ModuleToCheck = MockAppSettings.Module };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.CheckUserExists(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Exists.ShouldBeTrue();
		}

		[Test]
		public void CheckUserExists_WhenProvideAnNonExistingUserName_ReturnsSuccessAndExistsIsFalse()
		{
			// Arrange
			var userName = "DevClientEmp-" + Guid.NewGuid();
			var request = new CheckUserExistsRequest { UserName = userName, ModuleToCheck = MockAppSettings.Module };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.CheckUserExists(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Exists.ShouldBeFalse();
		}

		[Test]
		public void CheckUserExists_WhenProvideAnNullUserName_ReturnsFailure()
		{
			// Arrange
			var request = new CheckUserExistsRequest { UserName = null, ModuleToCheck = MockAppSettings.Module };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.CheckUserExists(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.Message.ShouldEqual(_service.FormatErrorMessage(request, ErrorTypes.UserNameIsNull));
		}

		#endregion

		#region Create Assist User

		[Test]
		public void CreateAssistUser_WhenProvideMandatoryData_ReturnsSuccessAndAccountCreated()
		{
			// Arrange
			var request = new CreateAssistUserRequest
			{
				UserContext = MockUserData,
				Models = new List<CreateAssistUserModel>
				{
					new CreateAssistUserModel
					{
						UserPerson = MockNewEmployeePerson,
						AccountUserName = MockNewUserName,
						AccountPassword = MockValidPassword,
						UserPhone = "123456789",
						UserFax = "123456789",
						UserEmailAddress = "client@Employee.com",
						UserExternalOffice = "UserOffice",
						UserAddress = MockAddressForEmployee.AsDto(),
						IsEnabled = true
					}
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.CreateAssistUser(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void CreateAssistUser_WhenNotPassedPhoneNumberData_ReturnsFailure()
		{
			// Arrange 
			var request = new CreateAssistUserRequest
			{
				Models = new List<CreateAssistUserModel>
				{
					new CreateAssistUserModel
					{
						UserPerson = MockNewEmployeePerson,
						AccountUserName = MockNewUserName,
						AccountPassword = MockValidPassword,
						UserFax = "123456789",
						UserEmailAddress = "client@Employee.com",
						UserExternalOffice = "UserOffice",
						UserAddress = MockAddressForEmployee.AsDto(),
						IsEnabled = true
					}
				}
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.CreateAssistUser(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.Message.ShouldEqual(_service.FormatErrorMessage(request, ErrorTypes.PhoneNumberMissing));
		}

		[Test]
		public void CreateAssistUser_WhenNotPassedFaxNumberData_ReturnsSuccess()
		{
			// Arrange
			var request = new CreateAssistUserRequest
			{
				Models = new List<CreateAssistUserModel>
				{
					new CreateAssistUserModel
					{
						UserPerson = MockNewEmployeePerson,
						AccountUserName = MockNewUserName,
						AccountPassword = MockValidPassword,
						UserPhone = "123456789",
						UserEmailAddress = "client@Employee.com",
						UserExternalOffice = "UserOffice",
						UserAddress = MockAddressForEmployee.AsDto(),
						IsEnabled = true
					}
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.CreateAssistUser(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void CreateAssistUser_WhenNotPassedAlternatePhoneNumberData_ReturnsSuccess()
		{
			// Arrange
			var request = new CreateAssistUserRequest
			{
				Models = new List<CreateAssistUserModel>
				{
					new CreateAssistUserModel
					{
						UserPerson = MockNewEmployeePerson,
						AccountUserName = MockNewUserName,
						AccountPassword = MockValidPassword,
						UserPhone = "123456789",
						UserFax = "123456789",
						UserEmailAddress = "client@Employee.com",
						UserExternalOffice = "UserOffice",
						UserAddress = MockAddressForEmployee.AsDto(),
						IsEnabled = true
					}
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.CreateAssistUser(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void CreateAssistUser_WhenNotPassedEmailAddressData_ReturnsFailure()
		{
			// Arrange
			var request = new CreateAssistUserRequest
			{
				Models = new List<CreateAssistUserModel>
				{
					new CreateAssistUserModel
					{
						UserPerson = MockNewEmployeePerson,
						AccountUserName = MockNewUserName,
						AccountPassword = MockValidPassword,
						UserPhone = "123456789",
						UserFax = "123456789",
						UserExternalOffice = "UserOffice",
						UserAddress = MockAddressForEmployee.AsDto(),
						IsEnabled = true
					}
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.CreateAssistUser(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.Message.ShouldEqual(_service.FormatErrorMessage(request, ErrorTypes.PersonEmailAddressMissing));
		}

		[Test]
		public void CreateAssistUser_WhenNotPassedOfficeData_ReturnsFailure()
		{
			// Arrange
			var request = new CreateAssistUserRequest
			{
				Models = new List<CreateAssistUserModel>
				{
					new CreateAssistUserModel
					{
						UserPerson = MockNewEmployeePerson,
						AccountUserName = MockNewUserName,
						AccountPassword = MockValidPassword,
						UserPhone = "123456789",
						UserFax = "123456789",
						UserEmailAddress = "client@Employee.com",
						UserAddress = MockAddressForEmployee.AsDto(),
						IsEnabled = true
					}
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.CreateAssistUser(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.Message.ShouldEqual(_service.FormatErrorMessage(request, ErrorTypes.PersonOfficeMissing));
		}

		[Test]
		public void CreateAssistUser_WhenPassedAnExistingUser_ResturnsFailure()
		{
			// Arrange
			var person = new PersonDto
			{
				FirstName = "DevClientEmp",
				MiddleInitial = "L",
				LastName = "ClientEmployee",
				DateOfBirth = DateTime.Now.AddYears(-25),
				JobTitle = "Client Employee",
				SocialSecurityNumber = "000-00-0001",
				TitleId = 0
			};

			var request = new CreateAssistUserRequest
			{
				Models = new List<CreateAssistUserModel>
				{
					new CreateAssistUserModel
					{
						UserPerson = person,
						AccountUserName = "DevClientEmp",
						AccountPassword = MockPassword,
						UserPhone = "123456789",
						UserFax = "123456789",
						UserEmailAddress = "client@Employee.com",
						UserExternalOffice = "UserOffice",
						UserAddress = MockAddressForEmployee.AsDto(),
						IsEnabled = true
					}
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.CreateAssistUser(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.Message.ShouldEqual(_service.FormatErrorMessage(request, ErrorTypes.UserAlreadyExists));
		}

		#endregion

		#region Create Assist users (multiple)

		[Test]
		public void CreateAssistUser_WhenProvideMultipleMandatoryData_ReturnsSuccessAndAccountsCreated()
		{
			// Arrange
			var models = new List<CreateAssistUserModel>
			{
				new CreateAssistUserModel
				{
					UserPerson = MockNewEmployeePerson,
					AccountUserName = MockNewUserName,
					AccountPassword = MockValidPassword,
					UserPhone = "123456789",
					UserFax = "123456789",
					UserEmailAddress = "client@Employee.com",
					UserExternalOffice = "UserOffice",
					UserAddress = MockAddressForEmployee.AsDto(),
					IsEnabled = true
				}
			};

			models.Add(new CreateAssistUserModel
			{
				UserPerson = MockNewEmployeePerson2,
				AccountUserName = MockNewUserName2,
				AccountPassword = MockValidPassword2,
				UserPhone = "123456789",
				UserFax = "123456789",
				UserEmailAddress = "client@Employee.com",
				UserExternalOffice = "UserOffice",
				UserAddress = MockAddressForEmployee.AsDto(),
				IsEnabled = true
			});

			var request = new CreateAssistUserRequest
			{
				UserContext = MockUserData,
				Models = models,
				BulkUpload = true
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.CreateAssistUser(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Models.Count.ShouldEqual(2);
			response.Models.Any(x => !x.IsEnabled).ShouldBeFalse();
		}

		[Test]
		public void CreateAssistUser_MultipleUsersWhenNotPassedPhoneNumberData_ReturnsFailure()
		{
			// Arrange 
			var models = new List<CreateAssistUserModel>
			{
				new CreateAssistUserModel
				{
					UserPerson = MockNewEmployeePerson,
					AccountUserName = MockNewUserName,
					AccountPassword = MockValidPassword,
					UserPhone = "123456789",
					UserFax = "123456789",
					UserEmailAddress = "client@Employee.com",
					UserExternalOffice = "UserOffice",
					UserAddress = MockAddressForEmployee.AsDto(),
					IsEnabled = true
				}
			};

			models.Add(new CreateAssistUserModel
			{
				UserPerson = MockNewEmployeePerson2,
				AccountUserName = MockNewUserName2,
				AccountPassword = MockValidPassword2,
				UserFax = "123456789",
				UserEmailAddress = "client@Employee.com",
				UserExternalOffice = "UserOffice",
				UserAddress = MockAddressForEmployee.AsDto(),
				IsEnabled = true
			});

			var request = new CreateAssistUserRequest
			{
				UserContext = MockUserData,
				Models = models,
				BulkUpload = true
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.CreateAssistUser(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.Message.ShouldEqual(_service.FormatErrorMessage(request, ErrorTypes.BulkRegistrationValidationFailed));
		}

		[Test]
		public void CreateAssistUser_WhenMultipleAndNotPassedFaxNumberData_ReturnsSuccess()
		{
			// Arrange
			var models = new List<CreateAssistUserModel>
			{
				new CreateAssistUserModel
				{
					UserPerson = MockNewEmployeePerson,
					AccountUserName = MockNewUserName,
					AccountPassword = MockValidPassword,
					UserPhone = "123456789",
					UserEmailAddress = "client@Employee.com",
					UserExternalOffice = "UserOffice",
					UserAddress = MockAddressForEmployee.AsDto(),
					IsEnabled = true
				}
			};

			models.Add(new CreateAssistUserModel
			{
				UserPerson = MockNewEmployeePerson2,
				AccountUserName = MockNewUserName2,
				AccountPassword = MockValidPassword2,
				UserPhone = "123456789",
				UserFax = "123456789",
				UserEmailAddress = "client@Employee.com",
				UserExternalOffice = "UserOffice",
				UserAddress = MockAddressForEmployee.AsDto(),
				IsEnabled = true
			});

			var request = new CreateAssistUserRequest
			{
				UserContext = MockUserData,
				Models = models,
				BulkUpload = true
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.CreateAssistUser(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void CreateAssistUser_MultipleWhenNotPassedEmailAddressData_ReturnsFailure()
		{
			// Arrange
			var models = new List<CreateAssistUserModel>
			{
				new CreateAssistUserModel
				{
					UserPerson = MockNewEmployeePerson,
					AccountUserName = MockNewUserName,
					AccountPassword = MockValidPassword,
					UserPhone = "123456789",
					UserFax = "123456789",
					UserExternalOffice = "UserOffice",
					UserAddress = MockAddressForEmployee.AsDto(),
					IsEnabled = true
				}
			};

			models.Add(new CreateAssistUserModel
			{
				UserPerson = MockNewEmployeePerson2,
				AccountUserName = MockNewUserName2,
				AccountPassword = MockValidPassword2,
				UserPhone = "123456789",
				UserFax = "123456789",
				UserEmailAddress = "client@Employee.com",
				UserExternalOffice = "UserOffice",
				UserAddress = MockAddressForEmployee.AsDto(),
				IsEnabled = true
			});

			var request = new CreateAssistUserRequest
			{
				UserContext = MockUserData,
				Models = models,
				BulkUpload = true
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.CreateAssistUser(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.Message.ShouldEqual(_service.FormatErrorMessage(request, ErrorTypes.BulkRegistrationValidationFailed));
		}

		[Test]
		public void CreateAssistUser_MultipleWhenNotPassedOfficeData_ReturnsFailure()
		{
			// Arrange
			var models = new List<CreateAssistUserModel>
			{
				new CreateAssistUserModel
				{
					UserPerson = MockNewEmployeePerson,
					AccountUserName = MockNewUserName,
					AccountPassword = MockValidPassword,
					UserPhone = "123456789",
					UserFax = "123456789",
					UserEmailAddress = "client@Employee.com",
					UserExternalOffice = "UserOffice",
					UserAddress = MockAddressForEmployee.AsDto(),
					IsEnabled = true
				}
			};

			models.Add(new CreateAssistUserModel
			{
				UserPerson = MockNewEmployeePerson2,
				AccountUserName = MockNewUserName2,
				AccountPassword = MockValidPassword2,
				UserPhone = "123456789",
				UserFax = "123456789",
				UserEmailAddress = "client@Employee.com",
				UserAddress = MockAddressForEmployee.AsDto(),
				IsEnabled = true
			});

			var request = new CreateAssistUserRequest
			{
				UserContext = MockUserData,
				Models = models,
				BulkUpload = true
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.CreateAssistUser(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.Message.ShouldEqual(_service.FormatErrorMessage(request, ErrorTypes.BulkRegistrationValidationFailed));
		}

		[Test]
		public void CreateAssistUser_MultipleWhenPassedAnExistingUser_ResturnsFailure()
		{
			// Arrange
			var models = new List<CreateAssistUserModel>
			{
				new CreateAssistUserModel
				{
					UserPerson = MockNewEmployeePerson,
					AccountUserName = MockNewUserName,
					AccountPassword = MockValidPassword,
					UserPhone = "123456789",
					UserFax = "123456789",
					UserEmailAddress = "client@Employee.com",
					UserExternalOffice = "UserOffice",
					UserAddress = MockAddressForEmployee.AsDto(),
					IsEnabled = true
				}
			};

			var person = new PersonDto
			{
				FirstName = "DevClientEmp",
				MiddleInitial = "L",
				LastName = "ClientEmployee",
				DateOfBirth = DateTime.Now.AddYears(-25),
				JobTitle = "Client Employee",
				SocialSecurityNumber = "000-00-0001",
				TitleId = 0
			};

			models.Add(new CreateAssistUserModel
			{
				UserPerson = person,
				AccountUserName = MockNewUserName2,
				AccountPassword = MockValidPassword2,
				UserPhone = "123456789",
				UserFax = "123456789",
				UserEmailAddress = "client@Employee.com",
				UserExternalOffice = "UserOffice",
				UserAddress = MockAddressForEmployee.AsDto(),
				IsEnabled = true
			});

			var request = new CreateAssistUserRequest
			{
				UserContext = MockUserData,
				Models = models,
				BulkUpload = true
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.CreateAssistUser(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.Message.ShouldEqual(_service.FormatErrorMessage(request, ErrorTypes.BulkRegistrationValidationFailed));
		}

		#endregion

		#region Find User

		[Test]
		public void FindUser_WhenPassedClientUserFirstName_ReturnsSuccessWithMatchingData()
		{
			// Arrange
			MockUserData.FirstName = "DevClientEmp";
			MockUserData.LastName = "";

			var request = new FindUserRequest
			{
				FindBy = FindUserRequest.UserFindBy.Name,
				SearchFirstName = "Dev",
				SearchLastName = "",
				SearchRole = Constants.RoleKeys.AssistUser,
				PageIndex = 0,
				PageSize = 10
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.FindUser(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Users.Count.ShouldBeGreater(0);
		}

		[Test]
		public void FindUser_WhenPassedClientUserLastName_ReturnsSuccessWithMatchingData()
		{
			// Arrange
			MockUserData.FirstName = "";
			MockUserData.LastName = "ClientEmployee";

			var request = new FindUserRequest
			{
				FindBy = FindUserRequest.UserFindBy.Name,
				SearchFirstName = "",
				SearchLastName = "Client",
				SearchRole = Constants.RoleKeys.AssistUser,
				PageIndex = 0,
				PageSize = 10
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.FindUser(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Users.Count.ShouldBeGreater(0);
		}

		[Test]
		public void FindUser_WhenPassedClientUserBothNames_ReturnsSuccessWithMatchingData()
		{
			// Arrange
			var request = new FindUserRequest
			{
				FindBy = FindUserRequest.UserFindBy.Name,
				SearchRole = Constants.RoleKeys.AssistUser,
				SearchFirstName = "Dev",
				SearchLastName = "Client",
				PageIndex = 0,
				PageSize = 10
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.FindUser(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Users.Count.ShouldBeGreater(0);
		}

		[Test]
		public void FindUser_WhenPassedClientUserEmailAddress_ReturnsSuccessWithMatchingData()
		{
			// Arrange
			var request = new FindUserRequest
			{
				FindBy = FindUserRequest.UserFindBy.Email,
				SearchEmailAddress = "client@Employee.com",
				SearchRole = Constants.RoleKeys.AssistUser,
				PageIndex = 0,
				PageSize = 10
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.FindUser(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Users.Count.ShouldBeGreater(0);
		}

		[Test]
		public void FindUser_WhenPassedClientUserOffice_ReturnsSuccessWithMatchingData()
		{
			// Arrange
			var request = new FindUserRequest
			{
				FindBy = FindUserRequest.UserFindBy.ExternalOffice,
				SearchExternalOffice = "ClientEmployeeOffice",
				SearchRole = Constants.RoleKeys.AssistUser,
				PageIndex = 0,
				PageSize = 10
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.FindUser(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Users.Count.ShouldBeGreater(0);
		}

		[Test]
		public void FindUser_WhenSpecificResultsPageReqested_ReturnsSuccessWithCorrectPageOfData()
		{
			// Arrange
			var request = new FindUserRequest
			{
				FindBy = FindUserRequest.UserFindBy.ExternalOffice,
				SearchRole = Constants.RoleKeys.TalentUser,
				PageIndex = 2,
				PageSize = 2
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.FindUser(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Users.Count.ShouldEqual(2);
			response.Users.PageIndex.ShouldEqual(2);
		}

		[Test]
		public void FindUser_WhenSpecificPageSizeReqested_ReturnsSuccessWithCorrectNumberOfItemsPerPage()
		{
			// Arrange
			var request = new FindUserRequest
			{
				FindBy = FindUserRequest.UserFindBy.Name,
				SearchRole = Constants.RoleKeys.TalentUser,
				PageIndex = 2,
				PageSize = 2
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.FindUser(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Users.Count.ShouldEqual(2);
			response.Users.PageSize.ShouldEqual(2);
		}

		#endregion

		#region Delete User

		[Test]
		public void DeleteUser_WhenGivenAnExistingUser_ReturnsSuccessAndDeletesUser()
		{
			// Arrange
			var request = new DeleteUserRequest
			{
				DeleteUserFirstName = MockUserData.FirstName,
				DeleteUserLastName = MockUserData.LastName,
				DeleteUserId = MockUserData.UserId
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.DeleteUser(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			DataCoreRepository.Users.FirstOrDefault(x => x.Id == request.DeleteUserId).ShouldBeNull();
		}

		[Test]
		public void DeleteUser_WhenGivenUseWhoDoseNotexist_ReturnsFailure()
		{
			// Arrange
			var request = new DeleteUserRequest
			{
				DeleteUserFirstName = MockUserData.FirstName,
				DeleteUserLastName = MockUserData.LastName,
				DeleteUserId = 1
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.DeleteUser(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			DataCoreRepository.Users.FirstOrDefault(x => x.Id == request.DeleteUserId).ShouldBeNull();
			response.Message.ShouldEqual(_service.FormatErrorMessage(request, ErrorTypes.UserNotFound));
		}

		#endregion

		#region Request Password Reset

		[Test]
		public void RequestPasswordReset_WhenGivenAnExistingUser_ReturnsSuccess()
		{
			// Arrange
			var request = new ResetUsersPasswordRequest { ResetUserId = 2 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			var user = new User { Enabled = true };
			var person = new Person { EmailAddress = "TestEmail" };
			user.Person = person;

			var session = new Session { Id = request.SessionId, LastRequestOn = DateTime.Now, UserId = MockUserData.UserId };
			var repository = new Mock<ICoreRepository>();
			repository.Setup(x => x.FindById<User>(request.ResetUserId)).Returns(user);
			repository.Setup(x => x.FindById<User>(MockUserData.UserId)).Returns(user);
			repository.Setup(x => x.FindById<Session>(request.SessionId)).Returns(session);

			((TestRepositories)RuntimeContext.Repositories).Core = repository.Object;
			var service = new AccountService(RuntimeContext);

			// Act
			var response = service.ResetUsersPassword(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void RequestPasswordReset_WhenGivenUserWhoDoesNotexist_ReturnsFailure()
		{
			// Arrange
			//var request = new ResetPasswordRequest(MockUserData) { ResetUserId = 1 };
			var request = new ResetUsersPasswordRequest { ResetUserId = 1 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			User user = null;
			var actionUser = new User { Enabled = true };
			var session = new Session { Id = request.SessionId, LastRequestOn = DateTime.Now, UserId = MockUserData.UserId };
			var repository = new Mock<ICoreRepository>();
			repository.Setup(x => x.FindById<User>(request.ResetUserId)).Returns(user);
			repository.Setup(x => x.FindById<User>(MockUserData.UserId)).Returns(actionUser);
			repository.Setup(x => x.FindById<Session>(request.SessionId)).Returns(session);

			((TestRepositories)RuntimeContext.Repositories).Core = repository.Object;
			var service = new AccountService(RuntimeContext);

			// Act
			var response = service.ResetUsersPassword(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.Message.ShouldEqual(_service.FormatErrorMessage(request, ErrorTypes.UserNotFound));
		}

		[Test]
		public void RequestPasswordReset_WhenGivenUserWhoDoesNotHaveAnEmailAddress_ReturnsFailure()
		{
			// Arrange
			var request = new ResetUsersPasswordRequest { ResetUserId = 2 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			var user = new User { Enabled = true };
			var person = new Person { EmailAddress = "" };
			user.Person = person;
			var session = new Session { Id = request.SessionId, LastRequestOn = DateTime.Now, UserId = MockUserData.UserId };
			var repository = new Mock<ICoreRepository>();
			repository.Setup(x => x.FindById<User>(request.ResetUserId)).Returns(user);
			repository.Setup(x => x.FindById<User>(MockUserData.UserId)).Returns(user);
			repository.Setup(x => x.FindById<Session>(request.SessionId)).Returns(session);

			((TestRepositories)RuntimeContext.Repositories).Core = repository.Object;
			var service = new AccountService(RuntimeContext);

			// Act
			var response = service.ResetUsersPassword(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.Message.ShouldEqual(_service.FormatErrorMessage(request, ErrorTypes.UserEmailAddressNotFound));
		}

		[Test]
		public void GetUserDetails_WithStandardSecurityQuestion_WhenPassedValidRequest_ReturnsSuccessWithData()
		{
			MockAppSettings.SharedEncryptionKey = "88D8ACA4-1A3B-4C95-98C4-9801090D";


			// Set up User account so that Security question and answer is set up
			var userRequest = GetUpdateUserRequest();
			_service.UpdateUser(userRequest);

			// Arrange
			var request = new GetUserDetailsRequest { Criteria = new UserCriteria { UserId = userRequest.UpdateUserId, GetSecurityAnswer = true } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetUserDetails(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.PersonDetails.ShouldNotBeNull();
			response.OfficeRoles.Count.ShouldEqual(2);
			response.SecurityAnswer.ShouldEqual( userRequest.SecurityAnswer );
			response.SecurityQuestion.ShouldEqual( userRequest.SecurityQuestion );
			response.SecurityQuestionId.ShouldEqual( userRequest.SecurityQuestionId );
		}

		[Test]
		public void GetUserType_WhenPassedValidRequest_ReturnsSuccessWithData()
		{
			// Arrange
			var request = new GetUserTypeRequest() { UserId = 2 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetUserType(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.UserType.ShouldEqual(UserTypes.Assist);
		}

		[Test]
		public void GetUserType_WhenPassedInvalidRequest_ReturnsFailure()
		{
			// Arrange
			var request = new GetUserTypeRequest() { UserId = 1 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetUserType(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}

		[Test]
		public void GetUserDetails_WithCustomSecurityQuestion_WhenPassedValidRequest_ReturnsSuccessWithData()
		{
			MockAppSettings.SharedEncryptionKey = "88D8ACA4-1A3B-4C95-98C4-9801090D";


			// Set up User account so that Security question and answer is set up
			var userRequest = GetUpdateUserRequest(true);
			_service.UpdateUser( userRequest );

			// Arrange
			var request = new GetUserDetailsRequest { Criteria = new UserCriteria { UserId = userRequest.UpdateUserId, GetSecurityAnswer = true } };
			request.Prepare( MockClientTag, MockSessionId, MockUserData, MockCulture );

			// Act
			var response = _service.GetUserDetails( request );

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual( AcknowledgementType.Success );
			response.PersonDetails.ShouldNotBeNull();
			response.OfficeRoles.Count.ShouldEqual( 2 );
			response.SecurityAnswer.ShouldEqual( userRequest.SecurityAnswer );
			response.SecurityQuestion.ShouldEqual( userRequest.SecurityQuestion );
			response.SecurityQuestionId.ShouldEqual( userRequest.SecurityQuestionId );
		}

		[Test]
		public void UpdateContactDetails_WhenPassedValidRequest_ReturnsSuccessAndSavesData()
		{
			// Arrange
			var request = CreateUpdateContactDetailsRequest();
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.UpdateContactDetails(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.PrimaryPhoneNumber.ShouldNotBeNull();
			response.PrimaryPhoneNumber.Number.ShouldEqual("999999999");
			response.AlternatePhoneNumber1.ShouldNotBeNull();
			response.AlternatePhoneNumber1.Number.ShouldEqual("1999999999");
			response.AlternatePhoneNumber2.ShouldNotBeNull();
			response.AlternatePhoneNumber2.Number.ShouldEqual("2999999999");
		}

		[Test]
		public void UpdateContactDetails_WhenPassedValidRequestWithContactDetailForDelete_ReturnsSuccessAndSavesData()
		{
			// Arrange
			var request = CreateUpdateContactDetailsRequest();
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			request.PrimaryContactDetails.Number = "";

			// Act
			var response = _service.UpdateContactDetails(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.PrimaryPhoneNumber.IsNull();
			response.AlternatePhoneNumber1.IsNotNull();
			response.AlternatePhoneNumber1.Number.ShouldEqual("1999999999");
			response.AlternatePhoneNumber2.IsNotNull();
			response.AlternatePhoneNumber2.Number.ShouldEqual("2999999999");
		}

		[Test]
		public void UpdateContactDetails_WhenPassedValidRequestWithAddress_ReturnsSuccessAndSavesData()
		{
			// Arrange
			var request = CreateUpdateContactDetailsRequest();
			request.Criteria.UserId = 1091965;
			request.AddressDetails = new PersonAddressDto
			{
				Line1 = "Line1",
				Line2 = "Line2",
				TownCity = "City3",
				PostcodeZip = "Zip",
				StateId = 1,
				CountyId = 2,
				CountryId = 3

			};
			request.PrimaryContactDetails.Id = 1091967;

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.UpdateContactDetails(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.AddressDetails.Line1.ShouldEqual("Line1");
			response.AddressDetails.Line2.ShouldEqual("Line2");
			response.AddressDetails.TownCity.ShouldEqual("City3");
			response.AddressDetails.PostcodeZip.ShouldEqual("Zip");
			response.AddressDetails.StateId.ShouldEqual(1);
			response.AddressDetails.CountyId.ShouldEqual(2);
			response.AddressDetails.CountryId.ShouldEqual(3);
		}

		[Test]
		public void UpdateContactDetails_WhenPassedValidRequestWithUser_ReturnsSuccessAndSavesData()
		{
			// Arrange
			var request = CreateUpdateContactDetailsRequest();
			request.Criteria.UserId = 1091965;
			request.PersonDetails.FirstName = "NewName";
			request.PersonDetails.LastName = "NewLastName";
			request.PrimaryContactDetails.Id = 1091967;

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.UpdateContactDetails(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Person.FirstName = "NewName";
			response.Person.LastName = "NewLastName";
		}


		#endregion

		#region Get SSN

		[Test]
		public void GetSSN_WhenPassedValidRequest_ReturnsSuccessWithData()
		{
			// Arrange
			var request = new GetSSNRequest { PersonId = 1195062 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetSSN(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.SSN.ShouldEqual("654789321");
		}

		#endregion

		#region Update SSN

		[Test]
		public void UpdateSsn_WhenPassedValidRequest_ReturnsSuccessWithData()
		{
			// Arrange
			var request = new UpdateSsnRequest { Id = 1195062, CurrentSsn = "654789321", NewSsn = "753369874" };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.UpdateSsn(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Person.SocialSecurityNumber.ShouldEqual("753369874");
		}

		[Test]
		public void UpdateSsn_WhenPassedExistingNewSsn_ReturnFailure()
		{
			// Arrange
			var request = new UpdateSsnRequest { Id = 1195062, CurrentSsn = "654789321", NewSsn = "130412222" };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.UpdateSsn(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}

		[Test]
		public void UpdateSsn_WhenPassedWrongCurrentSsn_ReturnFailure()
		{
			// Arrange
			var request = new UpdateSsnRequest { Id = 1195062, CurrentSsn = "130412222", NewSsn = "298741321" };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.UpdateSsn(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}

		#endregion

		#region Change User Name

		[Test]
		public void ChangeUsername_WhenPassedValidRequest_ReturnsSuccessWithData()
		{
			// Arrange
			var request = new ChangeUsernameRequest { CurrentUsername = MockUsername, NewUsername = "NewName" };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ChangeUsername(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void ChangeUsername_WhenPassedValidRequest_ReturnsSuccessAndUpdatesUserName()
		{
			// Arrange 
			var request = new ChangeUsernameRequest { CurrentUsername = "dev@client.com", NewUsername = "newdev@Client.com" };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ChangeUsername(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.User.UserName.ShouldEqual("newdev@Client.com");
		}

		#endregion

		#region "Email Alerts"

		[Test]
		public void GetEmailAlerts_WhenNoneAlreadyExist_ReturnsSuccess()
		{
			var criteria = new UserAlertCriteria() { UserId = MockUserId };
			var request = new GetUserAlertRequest { Criteria = criteria };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			var response = _service.GetUserAlert(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.UserAlert.ShouldNotBeNull();
			response.UserAlert.Id.ShouldBeNull();
		}

		[Test]
		public void SaveEmailAlerts_WhenSavingAlertsForFirstTime_ReturnsSuccess()
		{
			// Data to save
			var userAlert = new UserAlertDto
			{
				UserId = MockUserId,
				NewReferralRequests = true,
				EmployerAccountRequests = false,
				NewJobPostings = true,
				Frequency = 'D'
			};

			var request = new UpdateUserAlertRequest { UserAlert = userAlert };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			var response = _service.UpdateUserAlert(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.UserAlert.ShouldNotBeNull();
			response.UserAlert.Id.ShouldNotBeNull();
		}

		[Test]
		public void GetEmailAlerts_WhenOneAlreadyExist_ReturnsSuccess()
		{
			// Add a record to start with
			var userAlert = new UserAlertDto
			{
				UserId = MockUserId,
				NewReferralRequests = true,
				EmployerAccountRequests = false,
				NewJobPostings = true,
				Frequency = 'D'
			};

			var updaterequest = new UpdateUserAlertRequest { UserAlert = userAlert };
			updaterequest.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			_service.UpdateUserAlert(updaterequest);

			// Check record can be read from the database
			var criteria = new UserAlertCriteria() { UserId = MockUserId };
			var request = new GetUserAlertRequest { Criteria = criteria };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			var response = _service.GetUserAlert(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.UserAlert.ShouldNotBeNull();
			response.UserAlert.Id.ShouldNotBeNull();
		}

		[Test]
		public void SaveEmailAlerts_WhenOneAlreadyExist_ReturnsSuccess()
		{
			// Add a record to start with
			var userAlert = new UserAlertDto
			{
				UserId = MockUserId,
				NewReferralRequests = true,
				EmployerAccountRequests = false,
				NewJobPostings = true,
				Frequency = 'D'
			};

			var updaterequest = new UpdateUserAlertRequest { UserAlert = userAlert };
			updaterequest.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			_service.UpdateUserAlert(updaterequest);

			// Update the record in the database
			userAlert = new UserAlertDto
			{
				UserId = MockUserId,
				NewReferralRequests = false,
				EmployerAccountRequests = true,
				NewJobPostings = true,
				Frequency = 'W'
			};

			updaterequest = new UpdateUserAlertRequest { UserAlert = userAlert };
			updaterequest.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			_service.UpdateUserAlert(updaterequest);

			var response = _service.UpdateUserAlert(updaterequest);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.UserAlert.ShouldNotBeNull();
			response.UserAlert.Id.ShouldNotBeNull();
		}

		#endregion

		#region Change Email Address

		[Test]
		public void ChangeEmailAddress_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange 
			var request = new ChangeEmailAddressRequest { CurrentEmailAddress = "client@Employee.com", NewEmailAddress = "newdev@client.com" };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ChangeEmailAddress(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void ChangeEmailAddress_WhenPassedInvalidRequest_ReturnsFailure()
		{
			// Arrange 
			var request = new ChangeEmailAddressRequest { CurrentEmailAddress = "Not Existing Address", NewEmailAddress = "newdev@client.com" };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ChangeEmailAddress(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}


		#endregion

		#region Change Campus

		[Test]
		public void ChangeCampus_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange 
			var request = new ChangeCampusRequest { CampusId = 5944974 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ChangeCampus(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void ChangeCampus_WhenPassedInvalidRequest_ReturnsFailure()
		{
			// Arrange 
			var request = new ChangeCampusRequest { CampusId = 0 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ChangeCampus(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}

		#endregion

		#region Update user details

		[Test]
		public void UpdateUserDetails_WhenPassedValidRequest_ReturnsSuccessAndSavesData()
		{
			MockAppSettings.SharedEncryptionKey = "88D8ACA4-1A3B-4C95-98C4-9801090D";

			// Arrange
			var request = GetUpdateUserRequest();

			// Act
			var response = _service.UpdateUser(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.UserId.ShouldBeGreater(0);
		}

		[Test]
		public void UpdateUserDetails_WhenPassedInvalidValidRequest_ReturnsFailure()
		{
			MockAppSettings.SharedEncryptionKey = "88D8ACA4-1A3B-4C95-98C4-9801090D";

			// Arrange
			var request = GetUpdateUserRequest();
			request.UpdatePhoneNumber.Id = 99999999999999;

			// Act
			var response = _service.UpdateUser(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}

		[Test]
		public void UpdateUserDetails_WhenPassedInvalidCampusId_ReturnsFailure()
		{
			MockAppSettings.SharedEncryptionKey = "88D8ACA4-1A3B-4C95-98C4-9801090D";

			// Arrange
			var request = GetUpdateUserRequest();
			request.CampusId = 0;

			// Act
			var response = _service.UpdateUser(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}

		#endregion

		#region Update last surveyed date

		[Test]
		public void UpdateLastSurveyedDate_WhenPassedRequest_ReturnsSuccessAndSavesData()
		{
			// Arrange
			var request = new UpdateLastSurveyedDateRequest { PersonId = 1 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.UpdateLastSurveyedDate(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			var person = DataCoreRepository.Persons.First(x => x.Id == request.PersonId);
			person.ShouldNotBeNull();
			person.LastSurveyedOn.ShouldNotBeNull();
			person.LastSurveyedOn.GetValueOrDefault().Date.ShouldEqual(DateTime.Now.Date);
		}

		#endregion

		#region Export assist users

		#region Xls files

		[Test]
		public void ExportAssistUsers_WithValidXlsRequest_ReturnsSuccessAndDataTable()
		{
			var fileName = @"App_Data\\AssistUserUploadFiles\\ValidUsers.xls";
			var fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
			var binaryReader = new BinaryReader(fileStream);
			var byteLength = new FileInfo(fileName).Length;
			var fileBytes = binaryReader.ReadBytes((Int32)byteLength);
			binaryReader.Close();
			fileStream.Close();

			// Arrange
			var request = new ExportAssistUsersRequest { File = fileBytes, FileName = Path.GetFileName(fileName) };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ExportAssistUsers(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.ValidationErrors.ShouldBeFalse();
			response.ModelList.Count.ShouldEqual(2);
		}

		[Test]
		public void ExportAssistUsers_WithXlsWithFirstNameMissingRequest_ReturnsFailure()
		{
			var fileName = @"App_Data\\AssistUserUploadFiles\\InValidUsersFirstNameMissing.xls";
			var fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
			var binaryReader = new BinaryReader(fileStream);
			var byteLength = new FileInfo(fileName).Length;
			var fileBytes = binaryReader.ReadBytes((Int32)byteLength);
			binaryReader.Close();
			fileStream.Close();

			// Arrange
			var request = new ExportAssistUsersRequest { File = fileBytes, FileName = Path.GetFileName(fileName) };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ExportAssistUsers(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.ValidationErrors.ShouldBeTrue();
			response.ModelList[0].RegistrationError.Contains(ErrorTypes.RequiredDataNotFound).ShouldBeTrue();
		}

		[Test]
		public void ExportAssistUsers_WithXlsWithLastNameMissingRequest_ReturnsFailure()
		{
			var fileName = @"App_Data\\AssistUserUploadFiles\\InValidUsersLastNameMissing.xls";
			var fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
			var binaryReader = new BinaryReader(fileStream);
			var byteLength = new FileInfo(fileName).Length;
			var fileBytes = binaryReader.ReadBytes((Int32)byteLength);
			binaryReader.Close();
			fileStream.Close();

			// Arrange
			var request = new ExportAssistUsersRequest { File = fileBytes, FileName = Path.GetFileName(fileName) };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ExportAssistUsers(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.ValidationErrors.ShouldBeTrue();
			response.ModelList[1].RegistrationError.Contains(ErrorTypes.RequiredDataNotFound).ShouldBeTrue();
		}

		[Test]
		public void ExportAssistUsers_WithXlsWithTitleMissingRequest_ReturnsFailure()
		{
			var fileName = @"App_Data\\AssistUserUploadFiles\\InValidUsersTitleMissing.xls";
			var fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
			var binaryReader = new BinaryReader(fileStream);
			var byteLength = new FileInfo(fileName).Length;
			var fileBytes = binaryReader.ReadBytes((Int32)byteLength);
			binaryReader.Close();
			fileStream.Close();

			// Arrange
			var request = new ExportAssistUsersRequest { File = fileBytes, FileName = Path.GetFileName(fileName) };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ExportAssistUsers(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.ValidationErrors.ShouldBeTrue();
			response.ModelList[0].RegistrationError.Contains(ErrorTypes.RequiredDataNotFound).ShouldBeTrue();
		}

		[Test]
		public void ExportAssistUsers_WithXlsWithInvalidTitleRequest_ReturnsFailure()
		{
			var fileName = @"App_Data\\AssistUserUploadFiles\\InValidUsersInvalidTitle.xls";
			var fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
			var binaryReader = new BinaryReader(fileStream);
			var byteLength = new FileInfo(fileName).Length;
			var fileBytes = binaryReader.ReadBytes((Int32)byteLength);
			binaryReader.Close();
			fileStream.Close();

			// Arrange
			var request = new ExportAssistUsersRequest { File = fileBytes, FileName = Path.GetFileName(fileName) };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ExportAssistUsers(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.ValidationErrors.ShouldBeTrue();
			response.ModelList[0].RegistrationError.Contains(ErrorTypes.InvalidTitle).ShouldBeTrue();
		}

		[Test]
		public void ExportAssistUsers_WithXlsWithEmailMissingRequest_ReturnsFailure()
		{
			var fileName = @"App_Data\\AssistUserUploadFiles\\InValidUsersEmailMissing.xls";
			var fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
			var binaryReader = new BinaryReader(fileStream);
			var byteLength = new FileInfo(fileName).Length;
			var fileBytes = binaryReader.ReadBytes((Int32)byteLength);
			binaryReader.Close();
			fileStream.Close();

			// Arrange
			var request = new ExportAssistUsersRequest { File = fileBytes, FileName = Path.GetFileName(fileName) };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ExportAssistUsers(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.ValidationErrors.ShouldBeTrue();
			response.ModelList[0].RegistrationError.Contains(ErrorTypes.RequiredDataNotFound).ShouldBeTrue();
		}

		[Test]
		public void ExportAssistUsers_WithXlsWithInvalidEmailRequest_ReturnsFailure()
		{
			var fileName = @"App_Data\\AssistUserUploadFiles\\InValidUsersInvalidEmail.xls";
			var fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
			var binaryReader = new BinaryReader(fileStream);
			var byteLength = new FileInfo(fileName).Length;
			var fileBytes = binaryReader.ReadBytes((Int32)byteLength);
			binaryReader.Close();
			fileStream.Close();

			// Arrange
			var request = new ExportAssistUsersRequest { File = fileBytes, FileName = Path.GetFileName(fileName) };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ExportAssistUsers(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.ValidationErrors.ShouldBeTrue();
			response.ModelList[0].RegistrationError.Contains(ErrorTypes.InvalidEmail).ShouldBeTrue();
		}

		[Test]
		public void ExportAssistUsers_WithXlsWithInvalidPhoneRequest_ReturnsFailure()
		{
			var fileName = @"App_Data\\AssistUserUploadFiles\\InValidUsersInvalidPhone.xls";
			var fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
			var binaryReader = new BinaryReader(fileStream);
			var byteLength = new FileInfo(fileName).Length;
			var fileBytes = binaryReader.ReadBytes((Int32)byteLength);
			binaryReader.Close();
			fileStream.Close();

			// Arrange
			var request = new ExportAssistUsersRequest { File = fileBytes, FileName = Path.GetFileName(fileName) };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ExportAssistUsers(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.ValidationErrors.ShouldBeTrue();
			response.ModelList[0].RegistrationError.Contains(ErrorTypes.InvalidPhoneNumber).ShouldBeTrue();
		}

		[Test]
		public void ExportAssistUsers_WithXlsWithMultipleErrorTypes_ReturnsFailure()
		{
			var fileName = @"App_Data\\AssistUserUploadFiles\\InValidUsersMultipleErrorTypes.xls";
			var fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
			var binaryReader = new BinaryReader(fileStream);
			var byteLength = new FileInfo(fileName).Length;
			var fileBytes = binaryReader.ReadBytes((Int32)byteLength);
			binaryReader.Close();
			fileStream.Close();

			// Arrange
			var request = new ExportAssistUsersRequest { File = fileBytes, FileName = Path.GetFileName(fileName) };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ExportAssistUsers(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.ValidationErrors.ShouldBeTrue();
			response.ModelList[0].RegistrationError.Contains(ErrorTypes.InvalidPhoneNumber).ShouldBeTrue();
			response.ModelList[0].RegistrationError.Contains(ErrorTypes.InvalidTitle).ShouldBeTrue();
			response.ModelList[0].RegistrationError.Contains(ErrorTypes.RequiredDataNotFound).ShouldBeTrue();
		}

		[Test]
		public void ExportAssistUsers_WithXlsWithMultipleErrorTypesOnMultipleRows_ReturnsFailure()
		{
			var fileName = @"App_Data\\AssistUserUploadFiles\\InValidUsersMultipleErrorTypesOnMultipleRows.xls";
			var fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
			var binaryReader = new BinaryReader(fileStream);
			var byteLength = new FileInfo(fileName).Length;
			var fileBytes = binaryReader.ReadBytes((Int32)byteLength);
			binaryReader.Close();
			fileStream.Close();

			// Arrange
			var request = new ExportAssistUsersRequest { File = fileBytes, FileName = Path.GetFileName(fileName) };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ExportAssistUsers(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.ValidationErrors.ShouldBeTrue();
			response.ModelList[0].RegistrationError.Contains(ErrorTypes.InvalidPhoneNumber).ShouldBeTrue();
			response.ModelList[0].RegistrationError.Contains(ErrorTypes.InvalidTitle).ShouldBeTrue();
			response.ModelList[0].RegistrationError.Contains(ErrorTypes.RequiredDataNotFound).ShouldBeTrue();
			response.ModelList[1].RegistrationError.Contains(ErrorTypes.InvalidEmail).ShouldBeTrue();
			response.ModelList[1].RegistrationError.Contains(ErrorTypes.InvalidPhoneNumber).ShouldBeTrue();
		}

		#endregion

		#region Xlsx files

		[Test]
		public void ExportAssistUsers_WithValidXlsxRequest_ReturnsSuccessAndDataTable()
		{
			var fileName = @"App_Data\\AssistUserUploadFiles\\ValidUsers.xlsx";
			var fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
			var binaryReader = new BinaryReader(fileStream);
			var byteLength = new FileInfo(fileName).Length;
			var fileBytes = binaryReader.ReadBytes((Int32)byteLength);
			binaryReader.Close();
			fileStream.Close();

			// Arrange
			var request = new ExportAssistUsersRequest { File = fileBytes, FileName = Path.GetFileName(fileName) };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ExportAssistUsers(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.ValidationErrors.ShouldBeFalse();
			response.ModelList.Count.ShouldEqual(2);
		}

		[Test]
		public void ExportAssistUsers_WithXlsxWithFirstNameMissingRequest_ReturnsFailure()
		{
			var fileName = @"App_Data\\AssistUserUploadFiles\\InValidUsersFirstNameMissing.xlsx";
			var fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
			var binaryReader = new BinaryReader(fileStream);
			var byteLength = new FileInfo(fileName).Length;
			var fileBytes = binaryReader.ReadBytes((Int32)byteLength);
			binaryReader.Close();
			fileStream.Close();

			// Arrange
			var request = new ExportAssistUsersRequest { File = fileBytes, FileName = Path.GetFileName(fileName) };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ExportAssistUsers(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.ValidationErrors.ShouldBeTrue();
			response.ModelList[0].RegistrationError.Contains(ErrorTypes.RequiredDataNotFound).ShouldBeTrue();
		}

		[Test]
		public void ExportAssistUsers_WithXlsxWithLastNameMissingRequest_ReturnsFailure()
		{
			var fileName = @"App_Data\\AssistUserUploadFiles\\InValidUsersLastNameMissing.xlsx";
			var fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
			var binaryReader = new BinaryReader(fileStream);
			var byteLength = new FileInfo(fileName).Length;
			var fileBytes = binaryReader.ReadBytes((Int32)byteLength);
			binaryReader.Close();
			fileStream.Close();

			// Arrange
			var request = new ExportAssistUsersRequest { File = fileBytes, FileName = Path.GetFileName(fileName) };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ExportAssistUsers(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.ValidationErrors.ShouldBeTrue();
			response.ModelList[1].RegistrationError.Contains(ErrorTypes.RequiredDataNotFound).ShouldBeTrue();
		}

		[Test]
		public void ExportAssistUsers_WithXlsxWithTitleMissingRequest_ReturnsFailure()
		{
			var fileName = @"App_Data\\AssistUserUploadFiles\\InValidUsersTitleMissing.xlsx";
			var fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
			var binaryReader = new BinaryReader(fileStream);
			var byteLength = new FileInfo(fileName).Length;
			var fileBytes = binaryReader.ReadBytes((Int32)byteLength);
			binaryReader.Close();
			fileStream.Close();

			// Arrange
			var request = new ExportAssistUsersRequest { File = fileBytes, FileName = Path.GetFileName(fileName) };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ExportAssistUsers(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.ValidationErrors.ShouldBeTrue();
			response.ModelList[0].RegistrationError.Contains(ErrorTypes.RequiredDataNotFound).ShouldBeTrue();
		}

		[Test]
		public void ExportAssistUsers_WithXlsxWithInvalidTitleRequest_ReturnsFailure()
		{
			var fileName = @"App_Data\\AssistUserUploadFiles\\InValidUsersInvalidTitle.xlsx";
			var fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
			var binaryReader = new BinaryReader(fileStream);
			var byteLength = new FileInfo(fileName).Length;
			var fileBytes = binaryReader.ReadBytes((Int32)byteLength);
			binaryReader.Close();
			fileStream.Close();

			// Arrange
			var request = new ExportAssistUsersRequest { File = fileBytes, FileName = Path.GetFileName(fileName) };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ExportAssistUsers(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.ValidationErrors.ShouldBeTrue();
			response.ModelList[0].RegistrationError.Contains(ErrorTypes.InvalidTitle).ShouldBeTrue();
		}

		[Test]
		public void ExportAssistUsers_WithXlsxWithEmailMissingRequest_ReturnsFailure()
		{
			var fileName = @"App_Data\\AssistUserUploadFiles\\InValidUsersEmailMissing.xlsx";
			var fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
			var binaryReader = new BinaryReader(fileStream);
			var byteLength = new FileInfo(fileName).Length;
			var fileBytes = binaryReader.ReadBytes((Int32)byteLength);
			binaryReader.Close();
			fileStream.Close();

			// Arrange
			var request = new ExportAssistUsersRequest { File = fileBytes, FileName = Path.GetFileName(fileName) };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ExportAssistUsers(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.ValidationErrors.ShouldBeTrue();
			response.ModelList[0].RegistrationError.Contains(ErrorTypes.RequiredDataNotFound).ShouldBeTrue();
		}

		[Test]
		public void ExportAssistUsers_WithXlsxWithInvalidEmailRequest_ReturnsFailure()
		{
			var fileName = @"App_Data\\AssistUserUploadFiles\\InValidUsersInvalidEmail.xlsx";
			var fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
			var binaryReader = new BinaryReader(fileStream);
			var byteLength = new FileInfo(fileName).Length;
			var fileBytes = binaryReader.ReadBytes((Int32)byteLength);
			binaryReader.Close();
			fileStream.Close();

			// Arrange
			var request = new ExportAssistUsersRequest { File = fileBytes, FileName = Path.GetFileName(fileName) };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ExportAssistUsers(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.ValidationErrors.ShouldBeTrue();
			response.ModelList[0].RegistrationError.Contains(ErrorTypes.InvalidEmail).ShouldBeTrue();
		}

		[Test]
		public void ExportAssistUsers_WithXlsxWithInvalidPhoneRequest_ReturnsFailure()
		{
			var fileName = @"App_Data\\AssistUserUploadFiles\\InValidUsersInvalidPhone.xlsx";
			var fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
			var binaryReader = new BinaryReader(fileStream);
			var byteLength = new FileInfo(fileName).Length;
			var fileBytes = binaryReader.ReadBytes((Int32)byteLength);
			binaryReader.Close();
			fileStream.Close();

			// Arrange
			var request = new ExportAssistUsersRequest { File = fileBytes, FileName = Path.GetFileName(fileName) };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ExportAssistUsers(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.ValidationErrors.ShouldBeTrue();
			response.ModelList[0].RegistrationError.Contains(ErrorTypes.InvalidPhoneNumber).ShouldBeTrue();
		}

		[Test]
		public void ExportAssistUsers_WithXlsxWithMultipleErrorTypes_ReturnsFailure()
		{
			var fileName = @"App_Data\\AssistUserUploadFiles\\InValidUsersMultipleErrorTypes.xlsx";
			var fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
			var binaryReader = new BinaryReader(fileStream);
			var byteLength = new FileInfo(fileName).Length;
			var fileBytes = binaryReader.ReadBytes((Int32)byteLength);
			binaryReader.Close();
			fileStream.Close();

			// Arrange
			var request = new ExportAssistUsersRequest { File = fileBytes, FileName = Path.GetFileName(fileName) };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ExportAssistUsers(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.ValidationErrors.ShouldBeTrue();
			response.ModelList[0].RegistrationError.Contains(ErrorTypes.InvalidPhoneNumber).ShouldBeTrue();
			response.ModelList[0].RegistrationError.Contains(ErrorTypes.InvalidTitle).ShouldBeTrue();
			response.ModelList[0].RegistrationError.Contains(ErrorTypes.RequiredDataNotFound).ShouldBeTrue();
		}

		[Test]
		public void ExportAssistUsers_WithXlsxWithMultipleErrorTypesOnMultipleRows_ReturnsFailure()
		{
			var fileName = @"App_Data\\AssistUserUploadFiles\\InValidUsersMultipleErrorTypesOnMultipleRows.xlsx";
			var fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
			var binaryReader = new BinaryReader(fileStream);
			var byteLength = new FileInfo(fileName).Length;
			var fileBytes = binaryReader.ReadBytes((Int32)byteLength);
			binaryReader.Close();
			fileStream.Close();

			// Arrange
			var request = new ExportAssistUsersRequest { File = fileBytes, FileName = Path.GetFileName(fileName) };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ExportAssistUsers(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.ValidationErrors.ShouldBeTrue();
			response.ModelList[0].RegistrationError.Contains(ErrorTypes.InvalidPhoneNumber).ShouldBeTrue();
			response.ModelList[0].RegistrationError.Contains(ErrorTypes.InvalidTitle).ShouldBeTrue();
			response.ModelList[0].RegistrationError.Contains(ErrorTypes.RequiredDataNotFound).ShouldBeTrue();
			response.ModelList[1].RegistrationError.Contains(ErrorTypes.InvalidEmail).ShouldBeTrue();
			response.ModelList[1].RegistrationError.Contains(ErrorTypes.InvalidPhoneNumber).ShouldBeTrue();
		}

		#endregion

		#region CSV files

		[Test]
		public void ExportAssistUsers_WithValidCsvRequest_ReturnsSuccessAndDataTable()
		{
			var fileName = @"App_Data\\AssistUserUploadFiles\\ValidUsers.csv";
			var fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
			var binaryReader = new BinaryReader(fileStream);
			var byteLength = new FileInfo(fileName).Length;
			var fileBytes = binaryReader.ReadBytes((Int32)byteLength);
			binaryReader.Close();
			fileStream.Close();

			// Arrange
			var request = new ExportAssistUsersRequest { File = fileBytes, FileName = Path.GetFileName(fileName) };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ExportAssistUsers(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.ValidationErrors.ShouldBeFalse();
			response.ModelList.Count.ShouldEqual(2);
		}

		[Test]
		public void ExportAssistUsers_WithCsvWithFirstNameMissingRequest_ReturnsFailure()
		{
			var fileName = @"App_Data\\AssistUserUploadFiles\\InValidUsersFirstNameMissing.csv";
			var fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
			var binaryReader = new BinaryReader(fileStream);
			var byteLength = new FileInfo(fileName).Length;
			var fileBytes = binaryReader.ReadBytes((Int32)byteLength);
			binaryReader.Close();
			fileStream.Close();

			// Arrange
			var request = new ExportAssistUsersRequest { File = fileBytes, FileName = Path.GetFileName(fileName) };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ExportAssistUsers(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.ValidationErrors.ShouldBeTrue();
			response.ModelList[0].RegistrationError.Contains(ErrorTypes.RequiredDataNotFound).ShouldBeTrue();
		}

		[Test]
		public void ExportAssistUsers_WithCsvWithLastNameMissingRequest_ReturnsFailure()
		{
			var fileName = @"App_Data\\AssistUserUploadFiles\\InValidUsersLastNameMissing.csv";
			var fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
			var binaryReader = new BinaryReader(fileStream);
			var byteLength = new FileInfo(fileName).Length;
			var fileBytes = binaryReader.ReadBytes((Int32)byteLength);
			binaryReader.Close();
			fileStream.Close();

			// Arrange
			var request = new ExportAssistUsersRequest { File = fileBytes, FileName = Path.GetFileName(fileName) };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ExportAssistUsers(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.ValidationErrors.ShouldBeTrue();
			response.ModelList[1].RegistrationError.Contains(ErrorTypes.RequiredDataNotFound).ShouldBeTrue();
		}

		[Test]
		public void ExportAssistUsers_WithCsvWithTitleMissingRequest_ReturnsFailure()
		{
			var fileName = @"App_Data\\AssistUserUploadFiles\\InValidUsersTitleMissing.csv";
			var fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
			var binaryReader = new BinaryReader(fileStream);
			var byteLength = new FileInfo(fileName).Length;
			var fileBytes = binaryReader.ReadBytes((Int32)byteLength);
			binaryReader.Close();
			fileStream.Close();

			// Arrange
			var request = new ExportAssistUsersRequest { File = fileBytes, FileName = Path.GetFileName(fileName) };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ExportAssistUsers(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.ValidationErrors.ShouldBeTrue();
			response.ModelList[0].RegistrationError.Contains(ErrorTypes.RequiredDataNotFound).ShouldBeTrue();
		}

		[Test]
		public void ExportAssistUsers_WithCsvWithInvalidTitleRequest_ReturnsFailure()
		{
			var fileName = @"App_Data\\AssistUserUploadFiles\\InValidUsersInvalidTitle.csv";
			var fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
			var binaryReader = new BinaryReader(fileStream);
			var byteLength = new FileInfo(fileName).Length;
			var fileBytes = binaryReader.ReadBytes((Int32)byteLength);
			binaryReader.Close();
			fileStream.Close();

			// Arrange
			var request = new ExportAssistUsersRequest { File = fileBytes, FileName = Path.GetFileName(fileName) };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ExportAssistUsers(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.ValidationErrors.ShouldBeTrue();
			response.ModelList[0].RegistrationError.Contains(ErrorTypes.InvalidTitle).ShouldBeTrue();
		}

		[Test]
		public void ExportAssistUsers_WithCsvWithEmailMissingRequest_ReturnsFailure()
		{
			var fileName = @"App_Data\\AssistUserUploadFiles\\InValidUsersEmailMissing.csv";
			var fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
			var binaryReader = new BinaryReader(fileStream);
			var byteLength = new FileInfo(fileName).Length;
			var fileBytes = binaryReader.ReadBytes((Int32)byteLength);
			binaryReader.Close();
			fileStream.Close();

			// Arrange
			var request = new ExportAssistUsersRequest { File = fileBytes, FileName = Path.GetFileName(fileName) };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ExportAssistUsers(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.ValidationErrors.ShouldBeTrue();
			response.ModelList[0].RegistrationError.Contains(ErrorTypes.RequiredDataNotFound).ShouldBeTrue();
		}

		[Test]
		public void ExportAssistUsers_WithCsvWithInvalidEmailRequest_ReturnsFailure()
		{
			var fileName = @"App_Data\\AssistUserUploadFiles\\InValidUsersInvalidEmail.csv";
			var fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
			var binaryReader = new BinaryReader(fileStream);
			var byteLength = new FileInfo(fileName).Length;
			var fileBytes = binaryReader.ReadBytes((Int32)byteLength);
			binaryReader.Close();
			fileStream.Close();

			// Arrange
			var request = new ExportAssistUsersRequest { File = fileBytes, FileName = Path.GetFileName(fileName) };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ExportAssistUsers(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.ValidationErrors.ShouldBeTrue();
			response.ModelList[0].RegistrationError.Contains(ErrorTypes.InvalidEmail).ShouldBeTrue();
		}

		[Test]
		public void ExportAssistUsers_WithCsvWithInvalidPhoneRequest_ReturnsFailure()
		{
			var fileName = @"App_Data\\AssistUserUploadFiles\\InValidUsersInvalidPhone.csv";
			var fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
			var binaryReader = new BinaryReader(fileStream);
			var byteLength = new FileInfo(fileName).Length;
			var fileBytes = binaryReader.ReadBytes((Int32)byteLength);
			binaryReader.Close();
			fileStream.Close();

			// Arrange
			var request = new ExportAssistUsersRequest { File = fileBytes, FileName = Path.GetFileName(fileName) };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ExportAssistUsers(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.ValidationErrors.ShouldBeTrue();
			response.ModelList[0].RegistrationError.Contains(ErrorTypes.InvalidPhoneNumber).ShouldBeTrue();
		}

		[Test]
		public void ExportAssistUsers_WithCsvWithMultipleErrorTypes_ReturnsFailure()
		{
			var fileName = @"App_Data\\AssistUserUploadFiles\\InValidUsersMultipleErrorTypes.csv";
			var fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
			var binaryReader = new BinaryReader(fileStream);
			var byteLength = new FileInfo(fileName).Length;
			var fileBytes = binaryReader.ReadBytes((Int32)byteLength);
			binaryReader.Close();
			fileStream.Close();

			// Arrange
			var request = new ExportAssistUsersRequest { File = fileBytes, FileName = Path.GetFileName(fileName) };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ExportAssistUsers(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.ValidationErrors.ShouldBeTrue();
			response.ModelList[0].RegistrationError.Contains(ErrorTypes.InvalidPhoneNumber).ShouldBeTrue();
			response.ModelList[0].RegistrationError.Contains(ErrorTypes.InvalidTitle).ShouldBeTrue();
			response.ModelList[0].RegistrationError.Contains(ErrorTypes.RequiredDataNotFound).ShouldBeTrue();
		}

		[Test]
		public void ExportAssistUsers_WithCsvWithMultipleErrorTypesOnMultipleRows_ReturnsFailure()
		{
			var fileName = @"App_Data\\AssistUserUploadFiles\\InValidUsersMultipleErrorTypesOnMultipleRows.csv";
			var fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
			var binaryReader = new BinaryReader(fileStream);
			var byteLength = new FileInfo(fileName).Length;
			var fileBytes = binaryReader.ReadBytes((Int32)byteLength);
			binaryReader.Close();
			fileStream.Close();

			// Arrange
			var request = new ExportAssistUsersRequest { File = fileBytes, FileName = Path.GetFileName(fileName) };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ExportAssistUsers(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.ValidationErrors.ShouldBeTrue();
			response.ModelList[0].RegistrationError.Contains(ErrorTypes.InvalidPhoneNumber).ShouldBeTrue();
			response.ModelList[0].RegistrationError.Contains(ErrorTypes.InvalidTitle).ShouldBeTrue();
			response.ModelList[0].RegistrationError.Contains(ErrorTypes.RequiredDataNotFound).ShouldBeTrue();
			response.ModelList[1].RegistrationError.Contains(ErrorTypes.InvalidEmail).ShouldBeTrue();
			response.ModelList[1].RegistrationError.Contains(ErrorTypes.InvalidPhoneNumber).ShouldBeTrue();
		}

		#endregion

		#region Txt files

		[Test]
		public void ExportAssistUsers_WithTxtRequest_ReturnsFailure()
		{
			var fileName = @"App_Data\\AssistUserUploadFiles\\ValidUsers.txt";
			var fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
			var binaryReader = new BinaryReader(fileStream);
			var byteLength = new FileInfo(fileName).Length;
			var fileBytes = binaryReader.ReadBytes((Int32)byteLength);
			binaryReader.Close();
			fileStream.Close();

			// Arrange
			var request = new ExportAssistUsersRequest { File = fileBytes, FileName = Path.GetFileName(fileName) };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ExportAssistUsers(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.ValidationErrors.ShouldBeTrue();
			response.ModelList.Count.ShouldEqual(1);
			response.ModelList[0].ErrorText.Count.ShouldEqual(1);
		}

		#endregion

		#endregion

		#region Unsubscribe User From Emails

		[Test]
		public void UnsubscribeUserFromEmails_WhenPassedValidRequest_ReturnsSuccessAndUpdatesPersonRecord()
		{
			// Arrange
			MockAppSettings.SharedEncryptionKey = "88D8ACA4-1A3B-4C95-98C4-9801090D";


			var request = new UnsubscribeUserFromEmailsRequest
			{
				EncryptedPersonId = "iu813VmAa0M9E4jS%2b8TJHg%3d%3d", TargetType = TargetTypes.JobSeekerUnsubscribe, UserType = EntityTypes.JobSeeker, EncryptionId = 1, UrlEncoded = true
			}; // Equates to 1 - this is encrypted and URLEncoded

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.UnsubscribeUserFromEmails(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			var person = DataCoreRepository.Persons.First(x => x.Id == 1);
			person.Unsubscribed.ShouldEqual(true);

			MockAppSettings.SharedEncryptionKey = string.Empty;
		}

		[Test]
		public void UnsubscribeUserFromEmails_WhenPassedWrongEncryptedPersonId_ReturnsFailure()
		{
			// Arrange
			MockAppSettings.SharedEncryptionKey = "88D8ACA4-1A3B-4C95-98C4-9801090D";

			var request = new UnsubscribeUserFromEmailsRequest
			{
				EncryptedPersonId = "4bkRaZKG3XuftFB%2fQmwdPg%3d%3d", TargetType = TargetTypes.JobSeekerUnsubscribe, UserType = EntityTypes.JobSeeker, EncryptionId = 1, UrlEncoded = true
			}; // Equates to 9999999 - this is encrypted and URLEncoded

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.UnsubscribeUserFromEmails(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.Error.ShouldEqual(ErrorTypes.PersonNotFound);

			MockAppSettings.SharedEncryptionKey = string.Empty;
		}

		[Test]
		public void UnsubscribeUserFromEmails_WhenPassedInValidRequest_ReturnsFailure()
		{
			// Arrange
			MockAppSettings.SharedEncryptionKey = "88D8ACA4-1A3B-4C95-98C4-9801090D";

			var request = new UnsubscribeUserFromEmailsRequest
			{
				EncryptedPersonId = "xxxxxx",
				TargetType = TargetTypes.JobSeekerUnsubscribe,
				UserType = EntityTypes.JobSeeker,
				EncryptionId = 1
			}; // Equates to 9999999 - this is encrypted and URLEncoded

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.UnsubscribeUserFromEmails(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);

			MockAppSettings.SharedEncryptionKey = string.Empty;
		}

		[Test]
		public void UnsubscribeUserFromEmails_WhenPassedEmptyRequest_ReturnsFailure()
		{
			// Arrange
			MockAppSettings.SharedEncryptionKey = "88D8ACA4-1A3B-4C95-98C4-9801090D";

			var request = new UnsubscribeUserFromEmailsRequest();
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.UnsubscribeUserFromEmails(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.Error.ShouldEqual(ErrorTypes.EncryptedValueNotSupplied);

			MockAppSettings.SharedEncryptionKey = string.Empty;
			MockAppSettings.SharedEncryptionVector = string.Empty;
		}

		[Test]
		public void UnsubscribeUserFromEmails_WhenPassedKeysNotSet_ReturnsFailure()
		{
			// Arrange
			MockAppSettings.SharedEncryptionKey = string.Empty;
			MockAppSettings.SharedEncryptionVector = string.Empty;

			var request = new UnsubscribeUserFromEmailsRequest { EncryptedPersonId = "tcV6KC%2bJGGGWW27a0TMKEA%3d%3d" }; // Equates to 1 - this is encrypted and URLEncoded
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.UnsubscribeUserFromEmails(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.Error.ShouldEqual(ErrorTypes.Unknown);

			MockAppSettings.SharedEncryptionKey = string.Empty;
			MockAppSettings.SharedEncryptionVector = string.Empty;
		}



		[Test]
		public void UnsubscribeUserFromEmails_LogsAction_ReturnsSuccess()
		{
			// Arrange
			MockAppSettings.SharedEncryptionKey = "88D8ACA4-1A3B-4C95-98C4-9801090D";
			
			var request = new UnsubscribeUserFromEmailsRequest
			{
				EncryptedPersonId = "iu813VmAa0M9E4jS%2b8TJHg%3d%3d",
				TargetType = TargetTypes.JobSeekerUnsubscribe,
				UserType = EntityTypes.JobSeeker,
				EncryptionId = 1,
				UrlEncoded = true
			}; // Equates to 1 - this is encrypted and URLEncoded

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.UnsubscribeUserFromEmails(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			var actionTypeId = DataCoreRepository.ActionTypes.Where(x => x.Name.Equals(ActionTypes.UnsubscribeEmail.ToString())).Select(x => x.Id).FirstOrDefault();
			DataCoreRepository.ActionEvents.Count(x => x.ActionTypeId.Equals(actionTypeId)).ShouldEqual(1);
		}

		#endregion

		#region Inactivate job seeker

		[Test]
		public void InactivateJobSeeker_WhenPassedValidRequestToInactivate_ReturnsSuccess()
		{
			// Arrange
			var request = new InactivateJobSeekerRequest()
			{
				PersonId = 1116701,
				AccountDisabledReason = AccountDisabledReason.InactivatedByStaffRequest
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.InactivateJobSeeker(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			var person = DataCoreRepository.FindById<Person>(1116701);

			person.User.Enabled.ShouldBeFalse();
			var firstOrDefault = person.Resumes.FirstOrDefault(x => x.IsDefault);
			(firstOrDefault != null && firstOrDefault.IsSearchable).ShouldBeFalse();
		}

		[Test]
		public void InactivateJobSeeker_WhenPassedInValidUserTypeRequestToInactivate_ReturnsFalse()
		{
			// Arrange
			var request = new InactivateJobSeekerRequest()
			{
				PersonId = 1113825, // Talent user
				AccountDisabledReason = AccountDisabledReason.InactivatedByStaffRequest
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.InactivateJobSeeker(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}

		[Test]
		public void InactivateJobSeeker_WhenPassedNonExistentUserRequestToInactivate_ReturnsFalse()
		{
			// Arrange
			var request = new InactivateJobSeekerRequest()
			{
				PersonId = -9999999,
				AccountDisabledReason = AccountDisabledReason.InactivatedByStaffRequest
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.InactivateJobSeeker(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}

		[Test]
		public void InactivateJobSeeker_WhenPassedValidInvalidDisableReasonRequestToInactivate_ReturnsSuccess()
		{
			// Arrange
			var request = new InactivateJobSeekerRequest()
			{
				PersonId = 1116701,
				AccountDisabledReason = AccountDisabledReason.Unknown
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.InactivateJobSeeker(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}

		[Test]
		public void InactivateJobSeeker_WhenPassedEmptyUserRequestToInactivate_ReturnsFalse()
		{
			// Arrange
			var request = new InactivateJobSeekerRequest()
			{
				AccountDisabledReason = AccountDisabledReason.InactivatedByStaffRequest
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.InactivateJobSeeker(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}

		#endregion

		#region Update assist user

		[Test]
		public void UpdateAssistUser_WhenPassedValidRequest_ReturnsSuccessAndSavesData()
		{
			// Arrange
			var request = new UpdateAssistUserRequest
			{
				UserId = 2,
				LocalVeteranEmploymentRepresentative = true,
				DisabledVeteransOutreachProgramSpecialist = true,
				Manager = true
			}.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.UpdateAssistUser(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.User.ShouldNotBeNull();
			response.User.Id.ShouldEqual(2);

			var user = DataCoreRepository.FindById<User>(2);

			user.Person.DisabledVeteransOutreachProgramSpecialist.ShouldNotBeNull();
			user.Person.DisabledVeteransOutreachProgramSpecialist.ShouldEqual(true);

			user.Person.LocalVeteranEmploymentRepresentative.ShouldNotBeNull();
			user.Person.LocalVeteranEmploymentRepresentative.ShouldEqual(true);
		}

		[Test]
		public void UpdateAssistUser_WhenPassedInValidRequest_ReturnsSuccessAndSavesData()
		{
			// Arrange
			var request = new UpdateAssistUserRequest
			{
				LocalVeteranEmploymentRepresentative = true,
				DisabledVeteransOutreachProgramSpecialist = true
			}.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.UpdateAssistUser(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.User.ShouldBeNull();

			var user = DataCoreRepository.FindById<User>(2);

			user.Person.DisabledVeteransOutreachProgramSpecialist.ShouldBeNull();

			user.Person.LocalVeteranEmploymentRepresentative.ShouldBeNull();
		}

		#endregion

		#region Helpers

		private static UpdateContactDetailsRequest CreateUpdateContactDetailsRequest()
		{
			var request = new UpdateContactDetailsRequest
			{
				Criteria = new UserCriteria { UserId = 2 },
				PersonDetails = new PersonDto
				{
					FirstName = "First Name Updated",
					LastName = "Last Name Updated",
					MiddleInitial = "U",
					JobTitle = "Job Title Updated",
					TitleId = 12780,
					EmailAddress = "EMAIL@ADDRESS.COM",
					ExternalOffice = "Office"
				},
				PrimaryContactDetails = new PhoneNumberDto
				{
					PhoneType = PhoneTypes.Phone,
					IsPrimary = true,
					Id = 47170,
					Number = "PRIM999999999"
				},
				Alternate1ContactDetails = new PhoneNumberDto
				{
					PhoneType = PhoneTypes.Phone,
					IsPrimary = false,
					Id = 0,
					Number = "Alt1999999999"
				},
				Alternate2ContactDetails = new PhoneNumberDto
				{
					PhoneType = PhoneTypes.Mobile,
					IsPrimary = false,
					Id = 0,
					Number = "Alt2999999999"
				},
				LockVersion = 0

			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			return request;
		}

		private static RegisterTalentUserRequest GetRegisterAccountSpecifiedRequest(EmployerDto employerDto, BusinessUnitDto businessUnitDto)
		{
			return (new RegisterTalentUserRequest
			{
				Module = MockAppSettings.Module,
				Employer = employerDto,
				EmployeePerson = MockNewEmployeePerson,
				AccountUserName = MockNewUserName,
				AccountPassword = MockValidPassword,
				EmployeePhone = "123456789",
				EmployeePhoneType = "Phone",
				EmployeeAlternatePhone1 = "123456789",
				EmployeeAlternatePhone1Type = "Phone",
				EmployeeAlternatePhone2 = "123456789",
				EmployeeAlternatePhone2Type = "Fax",
				EmployeeEmailAddress = "test@test.com",
				EmployeeAddress = MockAddressForEmployee.AsDto(),
				EmployerAddress = MockEmployerAddress.AsDto(),
				BusinessUnit = businessUnitDto,
				BusinessUnitDescription = new BusinessUnitDescriptionDto { Id = 99, Description = "EmployerDescription", Title = "DescriptionTitle", IsPrimary = true },
				BusinessUnitAddress = MockBusinessUnitAddress.AsDto()
			}).Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
		}

		private static RegisterTalentUserRequest GetRegisterAccountNewRequest()
		{
			return GetRegisterAccountSpecifiedRequest(MockNewEmployer, MockNewBusinessUnit).Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
		}

		private static RegisterTalentUserRequest GetRegisterAccountSpecifiedRequestWithAssignedOffice(EmployerDto employerDto, BusinessUnitDto businessUnitDto)
		{
			return (new RegisterTalentUserRequest
			{
				Module = MockAppSettings.Module,
				Employer = employerDto,
				EmployeePerson = MockNewEmployeePerson,
				AccountUserName = MockNewUserName,
				AccountPassword = MockValidPassword,
				EmployeePhone = "123456789",
				EmployeePhoneType = "Phone",
				EmployeeAlternatePhone1 = "123456789",
				EmployeeAlternatePhone1Type = "Phone",
				EmployeeAlternatePhone2 = "123456789",
				EmployeeAlternatePhone2Type = "Fax",
				EmployeeEmailAddress = "test@test.com",
				EmployeeAddress = MockAddressForEmployee.AsDto(),
				EmployerAddress = MockEmployerAddressWithAssignedOffice.AsDto(),
				BusinessUnit = businessUnitDto,
				BusinessUnitDescription = new BusinessUnitDescriptionDto { Id = 99, Description = "EmployerDescription", Title = "DescriptionTitle", IsPrimary = true },
				BusinessUnitAddress = MockBusinessUnitAddress.AsDto()
			}).Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
		}

		private static RegisterTalentUserRequest GetRegisterAccountNewRequestWithAssignedOffice()
		{
			return GetRegisterAccountSpecifiedRequestWithAssignedOffice(MockNewEmployer, MockNewBusinessUnit).Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
		}

		private static RegisterTalentUserRequest GetRegisterAccountSpecifiedRequestWithMultiAssignedOffice(EmployerDto employerDto, BusinessUnitDto businessUnitDto)
		{
			return (new RegisterTalentUserRequest
			{
				Module = MockAppSettings.Module,
				Employer = employerDto,
				EmployeePerson = MockNewEmployeePerson,
				AccountUserName = MockNewUserName,
				AccountPassword = MockValidPassword,
				EmployeePhone = "123456789",
				EmployeePhoneType = "Phone",
				EmployeeAlternatePhone1 = "123456789",
				EmployeeAlternatePhone1Type = "Phone",
				EmployeeAlternatePhone2 = "123456789",
				EmployeeAlternatePhone2Type = "Fax",
				EmployeeEmailAddress = "test@test.com",
				EmployeeAddress = MockAddressForEmployee.AsDto(),
				EmployerAddress = MockEmployerAddressWithMultiAssignedOffice.AsDto(),
				BusinessUnit = businessUnitDto,
				BusinessUnitDescription = new BusinessUnitDescriptionDto { Id = 99, Description = "EmployerDescription", Title = "DescriptionTitle", IsPrimary = true },
				BusinessUnitAddress = MockBusinessUnitAddress.AsDto()
			}).Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
		}

		private static RegisterTalentUserRequest GetRegisterAccountNewRequestWithMultiAssignedOffice()
		{
			return GetRegisterAccountSpecifiedRequestWithMultiAssignedOffice(MockNewEmployer, MockNewBusinessUnit).Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
		}

		private static RegisterCareerUserRequest GetUpdateUserRequest( bool customSecurityQuestion = false )
		{
			var request = new RegisterCareerUserRequest
			{
				UpdateUserId = 2,
				UpdatePhoneNumber = new PhoneNumberDto
				{
					Number = "PRIM999999999",
					PhoneType = PhoneTypes.Phone,
					ProviderId = 5530602
				},
				DegreeId = 16132,
				ProgramOfStudyId = 1,
				EmailAddress = "EMAIL@ADDRESS.COM",
				EnrollmentStatus = global::Focus.Core.SchoolStatus.Sophomore,
				CampusId = 5944975,
				FirstName = "FirstName",
				LastName = "LastName",
				MiddleInitial = "L",
				PostalAddress = new Address { Street1 = "Street 1", City = "City Test", StateId = 12866, Zip = "11111" },
				SecurityQuestionId = customSecurityQuestion ? (long?)null : 4763960,
				SecurityQuestion = customSecurityQuestion ? "In which country is your favourite holiday resort?" : null,
				SecurityAnswer = customSecurityQuestion ? "Afghanistan" : "red",
				AccountPassword = "Passw0rd",
				AccountUserName = "username"
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			return request;
		}

		private void CreateSecurityQuestions(long userId)
		{
			MockAppSettings.SharedEncryptionKey = "88D8ACA4-1A3B-4C95-98C4-9801090D";

			var userSecurityQuestionRecord = new UserSecurityQuestion
			{
				QuestionIndex = 1,
				SecurityQuestion = "Who is your favourite Star Trek character?",
				SecurityQuestionId = null,
				SecurityAnswerHash = null,
				SecurityAnswerEncrypted = RuntimeContext.Helpers.Encryption.Encrypt("Jean Luc Picard", TargetTypes.SecurityAnswer, EntityTypes.User, userId),
				UserId = userId
			};
			DataCoreRepository.Add(userSecurityQuestionRecord);

			userSecurityQuestionRecord = new UserSecurityQuestion
			{
				QuestionIndex = 2,
				SecurityQuestion = null,
				SecurityQuestionId = 4763960,
				SecurityAnswerHash = null,
				SecurityAnswerEncrypted = RuntimeContext.Helpers.Encryption.Encrypt("Red", TargetTypes.SecurityAnswer, EntityTypes.User, userId),
				UserId = userId
			};
			DataCoreRepository.Add(userSecurityQuestionRecord);

			userSecurityQuestionRecord = new UserSecurityQuestion
			{
				QuestionIndex = 3,
				SecurityQuestion = null,
				SecurityQuestionId = 4763962,
				SecurityAnswerHash = null,
				SecurityAnswerEncrypted = RuntimeContext.Helpers.Encryption.Encrypt("Farqueharson", TargetTypes.SecurityAnswer, EntityTypes.User, userId),
				UserId = userId
			};
			DataCoreRepository.Add(userSecurityQuestionRecord);
			DataCoreRepository.SaveChanges();
		}

		#endregion

		#region Security Answers tests

		[Test]
		public void GetUserSecurityQuestion_ReturnsSuccess()
		{
			const int userId = 17370;
			CreateSecurityQuestions(userId);

			var request = new SecurityQuestionDetailsRequest().Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			request.UserId = userId;

			for (var questionIndex = 0; questionIndex <= 3; questionIndex++)
			{
				//Arrange
				request.QuestionIndex = questionIndex;

				// Act
				var response = _service.GetUserSecurityQuestion(request);

				// Assert
				response.ShouldNotBeNull();
				response.UserSecurityQuestions.ShouldNotBeNull();

				UserSecurityQuestionDto securityQuestionObj;
				switch (questionIndex)
				{
					case 0:
						response.UserSecurityQuestions.Count().ShouldEqual(3);
						securityQuestionObj = response.UserSecurityQuestions.First(usq => usq.QuestionIndex == 1);
						securityQuestionObj.ShouldNotBeNull();
						securityQuestionObj.SecurityQuestion.ShouldEqual("Who is your favourite Star Trek character?");
						securityQuestionObj.SecurityQuestionId.ShouldBeNull();
						securityQuestionObj.SecurityAnswerEncrypted.ShouldEqual("Jean Luc Picard");

						securityQuestionObj = response.UserSecurityQuestions.First(usq => usq.QuestionIndex == 2);
						securityQuestionObj.ShouldNotBeNull();
						securityQuestionObj.SecurityQuestion.ShouldBeNull();
						securityQuestionObj.SecurityQuestionId.ShouldEqual(4763960);
						securityQuestionObj.SecurityAnswerEncrypted.ShouldEqual("Red");

						securityQuestionObj = response.UserSecurityQuestions.First(usq => usq.QuestionIndex == 3);
						securityQuestionObj.ShouldNotBeNull();
						securityQuestionObj.SecurityQuestion.ShouldBeNull();
						securityQuestionObj.SecurityQuestionId.ShouldEqual(4763962);
						securityQuestionObj.SecurityAnswerEncrypted.ShouldEqual( "Farqueharson" );
						break;
					case 1:
						response.UserSecurityQuestions.Count().ShouldEqual(1);
						securityQuestionObj = response.UserSecurityQuestions.First(usq => usq.QuestionIndex == 1);
						securityQuestionObj.ShouldNotBeNull();
						securityQuestionObj.SecurityQuestion.ShouldEqual("Who is your favourite Star Trek character?");
						securityQuestionObj.SecurityQuestionId.ShouldBeNull();
						securityQuestionObj.SecurityAnswerEncrypted.ShouldEqual("Jean Luc Picard");
						break;
					case 2:
						response.UserSecurityQuestions.Count().ShouldEqual(1);
						securityQuestionObj = response.UserSecurityQuestions.First(usq => usq.QuestionIndex == 2);
						securityQuestionObj.ShouldNotBeNull();
						securityQuestionObj.SecurityQuestion.ShouldBeNull();
						securityQuestionObj.SecurityQuestionId.ShouldEqual(4763960);
						securityQuestionObj.SecurityAnswerEncrypted.ShouldEqual("Red");
						break;
					case 3:
						response.UserSecurityQuestions.Count().ShouldEqual(1);
						securityQuestionObj = response.UserSecurityQuestions.First(usq => usq.QuestionIndex == 3);
						securityQuestionObj.ShouldNotBeNull();
						securityQuestionObj.SecurityQuestion.ShouldBeNull();
						securityQuestionObj.SecurityQuestionId.ShouldEqual(4763962);
						securityQuestionObj.SecurityAnswerEncrypted.ShouldEqual( "Farqueharson" );
						break;
				}
			}
		}

		[Test]
		public void HasUserSecurityQuestion_WhenNone_ReturnsSuccess()
		{
			// Arrange
			var request = new SecurityQuestionDetailsRequest
			{
				UserId = 2,
			}.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.HasUserSecurityQuestions(request);

			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.HasQuestions.ShouldBeFalse();
		}

		[Test]
		public void HasUserSecurityQuestion_WhenPresent_ReturnsSuccess()
		{
			const int userId = 17370;
			CreateSecurityQuestions(userId);

			// Arrange
			var request = new SecurityQuestionDetailsRequest
			{
				UserId = userId,
			}.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.HasUserSecurityQuestions(request);

			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.HasQuestions.ShouldBeTrue();
		}

		#endregion
	}
}
