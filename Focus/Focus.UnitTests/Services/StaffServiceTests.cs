﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;
using Focus.UnitTests.Core;
using NUnit.Framework;

using Focus.Core.Criteria.User;
using Focus.Core.Messages;
using Focus.Core.Messages.StaffService;
using Focus.Core.Views;
using Focus.Services.ServiceImplementations;
using Framework.Testing.NUnit;

#endregion

namespace Focus.UnitTests.Services
{
	[TestFixture]
	public class StaffServiceTests : TestFixtureBase
	{
		private StaffService _service;
		
		[SetUp]
		public override void SetUp()
		{
			base.SetUp();

			SetUpCacheProvider();
			SetUpMockLogProvider();
			_service = new StaffService(RuntimeContext);
		}
		
		[Test]
		public void SearchStaff_WhenPassedValidRequest_ReturnsSuccessAndData()
		{
			// Arrange
			var request = new StaffSearchRequest { Criteria = new StaffCriteria { UserId = 2 } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.SearchStaff(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.StaffSearchViewsPaged.Count().ShouldEqual(1);
		}
		
		[Test]
		public void GetAssistUserActivityView_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new AssistUserActivityRequest { Criteria = new StaffCriteria { UserId = 2 } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetAssistUserActivityView(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}
		
		[Test]
		public void GetAssistUserEmployersAssistedView_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new AssistUserEmployersAssistedRequest { Criteria = new StaffCriteria { UserId = 2 } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetAssistUserEmployersAssistedView(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}
	
		[Test]
		public void GetAssistUserJobSeekersAssistedView_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new AssistUserJobSeekersAssistedRequest { Criteria = new StaffCriteria { UserId = 2 } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetAssistUserJobSeekersAssistedView(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}
		
		[Test]
		public void GetStaffProfile_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new StaffInfoRequest { Id = 2 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetStaffProfile(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void CreateStaffHomepageAlert_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new CreateStaffHomepageAlertRequest
			{
				HomepageAlert =
					new StaffHomepageAlertView
					{
						Message = "Unit test message",
						ExpiryDate = DateTime.Now.AddDays(7),
						IsSystemAlert = true
					}
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.CreateStaffHomepageAlert(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}
	}
}
