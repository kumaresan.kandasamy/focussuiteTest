﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;

using Focus.Core.Criteria.SpideredEmployer;
using Focus.Core.Models.Assist;
using Focus.UnitTests.Core;
using Moq;
using NUnit.Framework;

using Focus.Core;
using Focus.Core.Criteria;
using Focus.Core.Criteria.Employer;
using Focus.Core.DataTransferObjects.FocusCore;
using Focus.Core.Messages.EmployerService;
using Focus.Core.Models;
using Focus.Data.Core.Entities;
using Focus.Core.Criteria.BusinessUnit;
using Focus.Core.Criteria.EmployerAddress;
using Focus.Core.Criteria.BusinessUnitDescription;
using Focus.Core.Criteria.Job;
using Focus.Services.DtoMappers;
using Focus.Services.Repositories;
using Focus.Services.ServiceImplementations;
using Framework.Testing.NUnit;
using Focus.Core.Criteria.BusinessUnitAddress;
using Focus.Core.Criteria.BusinessUnitLogo;
using Focus.Core.Views;
using AcknowledgementType = Focus.Core.Messages.AcknowledgementType;

#endregion

namespace Focus.UnitTests.Services
{
	[TestFixture]
	public class EmployerServiceTests : TestFixtureBase
	{
		private EmployerService _service;

		[SetUp]
		public override void SetUp()
		{
			base.SetUp();

			SetUpCacheProvider();
			SetUpMockLogProvider();

			_service = new EmployerService(RuntimeContext);
		}

		[Test]
		public void GetBusinessUnitProfile_WhenPassedValidRequest_ReturnsSuccessAndData()
		{
			// Arrange
			var request = new BusinessUnitProfileRequest {BusinessUnitId = 1185025};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetBusinessUnitProfile(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.BusinessUnit.ShouldNotBeNull();
		}

		[Test]
		public void GetEmployerDetails_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new GetEmployerDetailsRequest {Criteria = new EmployerCriteria {EmployerId = 1091993}};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetEmployerDetails(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Name.ShouldEqual("Walt Disney");
		}

		[Test]
		public void GetEmployerLegalName_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new GetEmployerDetailsRequest { Criteria = new EmployerCriteria { EmployerId = 1091993 } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetEmployerLegalName(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.LegalName.ShouldEqual("Walt Disney", "Legal Name");
		}

		[Test]
		public void GetBusinessUnitAddress_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new BusinessUnitAddressRequest {Criteria = new BusinessUnitAddressCriteria {BusinessUnitId = 1185025}};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetBusinessUnitAddress(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.BusinessUnitAddress.ShouldNotBeNull();
		}

		[Test]
		public void GetBusinessUnitLogos_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new BusinessUnitLogoRequest {Criteria = new BusinessUnitLogoCriteria {Id = 1003614}};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetBusinessUnitLogos(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.BusinessUnitLogo.Name.ShouldEqual("test1");
		}

		[Test]
		public void AssignEmployerToAdmin_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new AssignEmployerToAdminRequest {EmployeeId = 1097127, AdminId = 2};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.AssignEmployerToAdmin(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void CheckNaicsExists_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new CheckNaicsExistsRequest {Naics = "11114"};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.CheckNaicsExists(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Exists.ShouldEqual(true);
		}

		[Test]
		public void BlockEmployeesAccounts_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new BlockEmployeesAccountsRequest { BusinessUnitId = 1157080 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.BlockEmployeesAccounts(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void UnblockEmployeesAccounts_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new UnblockEmployeesAccountsRequest { BusinessUnitId = 1594639 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.UnblockEmployeesAccounts(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void GetEmployerRecentlyPlacedMatches_WhenPassedValidRequest_ReturnsSuccessAndData()
		{
			// Arrange
			var request = new EmployerRecentlyPlacedMatchRequest
			{
				Criteria = new BusinessUnitCriteria {BusinessUnitId = 1088857, OrderBy = Constants.SortOrders.BusinessUnitNameAsc}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetEmployerRecentlyPlacedMatches(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.RecentlyPlacedPaged.Count.ShouldEqual(1);
		}

		[Test]
		public void GetBusinessUnits_WhenPassedEmployerId_ReturnsSuccessWithExpectedData()
		{
			// Arrange
			var request = new BusinessUnitRequest
			{
				Criteria = new BusinessUnitCriteria {EmployerId = 13930, FetchOption = CriteriaBase.FetchOptions.List}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act	
			var response = _service.GetBusinessUnits(request);

			// Assert 
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.BusinessUnits.Count.ShouldEqual(2);
			response.BusinessUnitsPaged.ShouldBeNull();
      response.EmployeeIds.ShouldBeNull();
			response.BusinessUnit.ShouldBeNull();
		}

		[Test]
		public void GetBusinessUnits_WhenPassedBusinessUnitName_ReturnsSuccessWithExpectedData()
		{
			// Arrange
			var request = new BusinessUnitRequest
			{
				Criteria = new BusinessUnitCriteria { BusinessUnitName = "PrimaryBusinessUnit", FetchOption = CriteriaBase.FetchOptions.Single }
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act	
			var response = _service.GetBusinessUnits(request);

			// Assert 
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.BusinessUnits.ShouldBeNull();
			response.BusinessUnitsPaged.ShouldBeNull();
      response.EmployeeIds.ShouldBeNull();
			response.BusinessUnit.ShouldNotBeNull();
		}

    [Test]
    public void GetBusinessUnits_WhenPassedRequestForEmployeeId_ReturnsSuccessWithExpectedData()
    {
      // Arrange
      var request = new BusinessUnitRequest
      {
        IncludeEmployeeIds = true,
        Criteria = new BusinessUnitCriteria
        {
          EmployerId = 1103832,
          FetchOption = CriteriaBase.FetchOptions.List
        }
      };
      request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

      // Act	
      var response = _service.GetBusinessUnits(request);

      // Assert 
      response.ShouldNotBeNull();
      response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      response.BusinessUnits.Count.ShouldEqual(2);
      response.BusinessUnitsPaged.ShouldBeNull();
      response.BusinessUnit.ShouldBeNull();
      response.EmployeeIds.ShouldNotBeNull();
      response.EmployeeIds.Count.ShouldNotEqual(0);
    }

		[Test]
		public void SaveBusinessUnitAddress_WhenPassedRequestWithBusinessUnitAddressInDb_ReturnsSuccessAndSavesData()
		{
			// Arrange
			var addresses =
				DataCoreUnitOfWorkScope.Current.BusinessUnitAddresses.Where(x => x.BusinessUnitId == 1088857 && x.IsPrimary)
					.ToList();
			var address = new BusinessUnitAddress();
			if (addresses.Count() == 1)
				address = addresses[0];

			var request = new SaveBusinessUnitAddressRequest
			{
				BusinessUnitAddress =
					new BusinessUnitAddressDto
					{
						Id = address.Id,
						Line1 = "Test",
						TownCity = address.TownCity,
						PostcodeZip = address.PostcodeZip,
						StateId = address.StateId,
						CountryId = address.CountryId,
						IsPrimary = address.IsPrimary,
						PublicTransitAccessible = address.PublicTransitAccessible,
						BusinessUnitId = address.BusinessUnitId
					}
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.SaveBusinessUnitAddress(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.BusinessUnitAddress.Id.ShouldNotBeNull();
			response.BusinessUnitAddress.TownCity.ShouldNotBeNull();
			response.BusinessUnitAddress.PostcodeZip.ShouldNotBeNull();
			response.BusinessUnitAddress.StateId.ShouldNotBeNull();
			response.BusinessUnitAddress.CountryId.ShouldNotBeNull();
			response.BusinessUnitAddress.IsPrimary.ShouldNotBeNull();
			response.BusinessUnitAddress.PublicTransitAccessible.ShouldNotBeNull();
			response.BusinessUnitAddress.BusinessUnitId.ShouldNotBeNull();
		}

		[Test]
		public void SaveBusinessUnitAddress_WhenPassedRequestWithBusinessUnitAddressNotInDb_ReturnsSuccessAndSavesData()
		{
			// Arrange
			var addresses =
				DataCoreUnitOfWorkScope.Current.BusinessUnitAddresses.Where(x => x.BusinessUnitId == 1088857 && x.IsPrimary)
					.ToList();
			var address = new BusinessUnitAddress();
			if (addresses.Count() == 1)
				address = addresses[0];

			var request = new SaveBusinessUnitAddressRequest
			{
				BusinessUnitAddress =
					new BusinessUnitAddressDto
					{
						Id = 0,
						Line1 = "Test",
						TownCity = address.TownCity,
						PostcodeZip = address.PostcodeZip,
						StateId = address.StateId,
						CountryId = address.CountryId,
						IsPrimary = false,
						PublicTransitAccessible = false,
						BusinessUnitId = address.BusinessUnitId
					}
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.SaveBusinessUnitAddress(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.BusinessUnitAddress.Id.ShouldNotBeNull();
			response.BusinessUnitAddress.TownCity.ShouldNotBeNull();
			response.BusinessUnitAddress.PostcodeZip.ShouldNotBeNull();
			response.BusinessUnitAddress.StateId.ShouldNotBeNull();
			response.BusinessUnitAddress.CountryId.ShouldNotBeNull();
			response.BusinessUnitAddress.IsPrimary.ShouldNotBeNull();
			response.BusinessUnitAddress.PublicTransitAccessible.ShouldNotBeNull();
			response.BusinessUnitAddress.BusinessUnitId.ShouldNotBeNull();
		}

		[Test]
		public void SaveBusinessUnitAddress_WhenPassedRequestWithBusinessUnitAddressNotInDbWithInvalidData_ReturnsFailure()
		{
			// Arrange
			var request = new SaveBusinessUnitAddressRequest
			{
				BusinessUnitAddress = new BusinessUnitAddressDto {Id = 0}
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.SaveBusinessUnitAddress(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}

		[Test]
		public void SaveBusinessUnit_WhenPassedRequestWithBusinessUnitInDb_ReturnsSuccessAndSavesData()
		{
			// Arrange
			var businessUnits =
				DataCoreUnitOfWorkScope.Current.BusinessUnits.Where(x => x.EmployerId == 13930 && x.IsPrimary == true).ToList();
			var businessUnit = new BusinessUnit();
			if (businessUnits.Count() == 1)
				businessUnit = businessUnits[0];

			var request = new SaveBusinessUnitRequest
			{
				BusinessUnit =
					new BusinessUnitDto
					{
						Id = 25181,
						Name = businessUnit.Name,
						IsPrimary = businessUnit.IsPrimary,
						EmployerId = businessUnit.EmployerId,
						Url = businessUnit.Url,
						OwnershipTypeId = businessUnit.OwnershipTypeId,
						IndustrialClassification = businessUnit.IndustrialClassification,
						PrimaryPhone = businessUnit.PrimaryPhone,
						PrimaryPhoneType = businessUnit.PrimaryPhoneType,
						PrimaryPhoneExtension = businessUnit.PrimaryPhoneExtension,
						AlternatePhone1 = businessUnit.AlternatePhone1,
						AlternatePhone1Type = businessUnit.AlternatePhone1Type,
						AlternatePhone2 = businessUnit.AlternatePhone2,
						AlternatePhone2Type = businessUnit.AlternatePhone2Type
					}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.SaveBusinessUnit(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.BusinessUnit.Id.ShouldNotBeNull();
			response.BusinessUnit.Name.ShouldNotBeNull();
			response.BusinessUnit.IsPrimary.ShouldNotBeNull();
			response.BusinessUnit.EmployerId.ShouldNotBeNull();
			response.BusinessUnit.OwnershipTypeId.ShouldNotBeNull();
			response.BusinessUnit.IndustrialClassification.ShouldNotBeNull();
			response.BusinessUnit.PrimaryPhone.ShouldNotBeNull();
		}

		[Test]
		public void SaveBusinessUnit_WhenPassedRequestWithBusinessUnitNotInDb_ReturnsSuccessAndSavesDataAndRemovesExistingData
			()
		{

			// Arrange
			var businessUnits =
				DataCoreUnitOfWorkScope.Current.BusinessUnits.Where(x => x.EmployerId == 13930 && x.IsPrimary == true).ToList();
			var businessUnit = new BusinessUnit();
			if (businessUnits.Count() == 1)
				businessUnit = businessUnits[0];

			var request = new SaveBusinessUnitRequest
			{
				BusinessUnit =
					new BusinessUnitDto
					{
						Id = 0,
						Name = businessUnit.Name,
						IsPrimary = businessUnit.IsPrimary,
						EmployerId = businessUnit.EmployerId,
						Url = businessUnit.Url,
						OwnershipTypeId = businessUnit.OwnershipTypeId,
						IndustrialClassification = businessUnit.IndustrialClassification,
						PrimaryPhone = businessUnit.PrimaryPhone,
						PrimaryPhoneType = businessUnit.PrimaryPhoneType,
						PrimaryPhoneExtension = businessUnit.PrimaryPhoneExtension,
						AlternatePhone1 = businessUnit.AlternatePhone1,
						AlternatePhone1Type = businessUnit.AlternatePhone1Type,
						AlternatePhone2 = businessUnit.AlternatePhone2,
						AlternatePhone2Type = businessUnit.AlternatePhone2Type
					}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.SaveBusinessUnit(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.BusinessUnit.Id.ShouldNotBeNull();
			response.BusinessUnit.Name.ShouldNotBeNull();
			response.BusinessUnit.IsPrimary.ShouldNotBeNull();
			response.BusinessUnit.EmployerId.ShouldNotBeNull();
			response.BusinessUnit.OwnershipTypeId.ShouldNotBeNull();
			response.BusinessUnit.IndustrialClassification.ShouldNotBeNull();
			response.BusinessUnit.PrimaryPhone.ShouldNotBeNull();
		}

		[Test]
		public void SaveBusinessUnit_WhenPassedRequestWithBusinessUnitNotInDbWithInvalidData_ReturnsFailure()
		{

			// Arrange
			var request = new SaveBusinessUnitRequest
			{
				BusinessUnit = new BusinessUnitDto {Id = 0}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.SaveBusinessUnit(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}

		[Test]
		public void SaveBusinessUnitDescription_WhenPassedValidRequest_ReturnsSuccessAndSavesData()
		{
			// Arrange
			var businessUnit = DataCoreRepository.BusinessUnits.FirstOrDefault(x => x.Name == "PrimaryBusinessUnit");

			var request = new SaveBusinessUnitDescriptionRequest
			{
				BusinessUnitDescription =
					new BusinessUnitDescriptionDto
					{
						BusinessUnitId = businessUnit.Id,
						Title = "Test Employer Description",
						Description = "This is a test employer description"
					}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.SaveBusinessUnitDescription(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.BusinessUnitDescription.ShouldNotBeNull();
			response.BusinessUnitDescription.Id.ShouldNotBeNull();
		}

		[Test]
		public void GetBusinessUnitDescriptions_WhenPassedValidEmployerIdRequest_ReturnsSuccessWithExpectedList()
		{
			// Arrange 
			var request = new BusinessUnitDescriptionRequest
			{
				Criteria =
					new BusinessUnitDescriptionCriteria {BusinessUnitId = 1091970, FetchOption = CriteriaBase.FetchOptions.List}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetBusinessUnitDescriptions(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.BusinessUnitDescriptionsPaged.ShouldBeNull();
			response.BusinessUnitDescriptions.Count.ShouldBeGreater(0);
			response.BusinessUnitDescription.ShouldBeNull();
		}

		[Test]
		public void GetBusinessUnitDescriptions_WhenPassedValidEmployerIdRequest_ReturnsSuccessWithExpectedPagedList()
		{
			// Arrange 
			var request = new BusinessUnitDescriptionRequest
			{
				Criteria =
					new BusinessUnitDescriptionCriteria {BusinessUnitId = 1091970, FetchOption = CriteriaBase.FetchOptions.PagedList}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetBusinessUnitDescriptions(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.BusinessUnitDescriptionsPaged.Count.ShouldBeGreater(0);
			response.BusinessUnitDescriptions.ShouldBeNull();
			response.BusinessUnitDescription.ShouldBeNull();
		}

		[Test]
		public void GetBusinessUnitDescriptions_WhenPassedValidEmployerDescriptionIdRequest_ReturnsSuccessWithExpectedData()
		{
			// Arrange 
			var request = new BusinessUnitDescriptionRequest
			{
				Criteria =
					new BusinessUnitDescriptionCriteria
					{
						BusinessUnitDescriptionId = 1091992,
						FetchOption = CriteriaBase.FetchOptions.Single
					}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetBusinessUnitDescriptions(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void GetBusinessUnitDescriptions_WhenPassedInvalidValidEmployerIdListRequest_ReturnsSuccessWithEmptyList()
		{
			// Arrange 
			var request = new BusinessUnitDescriptionRequest
			{
				Criteria = new BusinessUnitDescriptionCriteria {BusinessUnitId = 0, FetchOption = CriteriaBase.FetchOptions.List}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetBusinessUnitDescriptions(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.BusinessUnitDescriptionsPaged.ShouldBeNull();
			response.BusinessUnitDescriptions.Count.ShouldEqual(0);
			response.BusinessUnitDescription.ShouldBeNull();
		}

		[Test]
		public void
			GetBusinessUnitDescriptions_WhenPassedInvalidValidEmployerIdPagedListRequest_ReturnsSuccessWithEmptyPagedList()
		{
			// Arrange 
			var request = new BusinessUnitDescriptionRequest
			{
				Criteria =
					new BusinessUnitDescriptionCriteria {BusinessUnitId = 0, FetchOption = CriteriaBase.FetchOptions.PagedList}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetBusinessUnitDescriptions(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.BusinessUnitDescriptionsPaged.Count.ShouldEqual(0);
			response.BusinessUnitDescriptions.ShouldBeNull();
			response.BusinessUnitDescription.ShouldBeNull();
		}

		[Test]
		public void GetBusinessUnitDescriptions_WhenPassedInvalidValidEmployerDescriptionIdRequest_ReturnsFailure()
		{
			// Arrange 
			var request = new BusinessUnitDescriptionRequest
			{
				Criteria =
					new BusinessUnitDescriptionCriteria {BusinessUnitDescriptionId = 0, FetchOption = CriteriaBase.FetchOptions.Single}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetBusinessUnitDescriptions(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.BusinessUnitDescriptionsPaged.ShouldBeNull();
			response.BusinessUnitDescriptions.ShouldBeNull();
			response.BusinessUnitDescription.ShouldBeNull();
		}

		[Test]
		public void SaveBusinessUnitLogo_WhenPassedValidRequest_ReturnsSuccessAndSavesData()
		{
			// Arrange
			var businessUnit = DataCoreRepository.BusinessUnits.FirstOrDefault(x => x.Name == "PrimaryBusinessUnit");

			var logo = new byte[] {0x32, 0x32, 0x32, 0x32};

			var request = new SaveBusinessUnitLogoRequest
			{
				BusinessUnitLogo =
					new BusinessUnitLogoDto {BusinessUnitId = businessUnit.Id, Name = "Test Employer Logo", Logo = logo}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.SaveBusinessUnitLogo(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.BusinessUnitLogoId.ShouldBeGreater(0);
		}

		[Test]
		public void GetEmployerJobTitles_WhenPassedValidEmployerId_ReturnsSuccessWithList()
		{
			// Arrange
			var request = new EmployerJobTitleRequest {Criteria = new JobCriteria {EmployerId = 1088910}};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetEmployerJobTitles(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.JobTitles.Count.ShouldBeGreater(0);
		}

		[Test]
		public void GetEmployerJobTitles_WhenPassedInValidEmployerId_ReturnsSuccessWithEmptyList()
		{
			// Arrange
			var request = new EmployerJobTitleRequest {Criteria = new JobCriteria {EmployerId = 0}};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetEmployerJobTitles(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.JobTitles.Count.ShouldEqual(0);
		}

		[Test]
		public void GetEmployerAddress_WhenPassedRequestWithValidEmployerIdIsPrimary_ReturnSuccessWithPrimaryAddress()
		{
			// Arrange
			var request = new EmployerAddressRequest
			{
				Criteria = new EmployerAddressCriteria {EmployerId = 1091993, IsPrimary = true}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetEmployerAddress(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.PrimaryEmployerAddress.ShouldNotBeNull();
			response.OtherEmployerAddresses.Count.ShouldEqual(0);
			response.AllEmployerAddresses.Count.ShouldEqual(0);
		}

		[Test]
		public void GetEmployerAddress_WhenPassedRequestWithValidEmployerIdNotPrimary_ReturnSuccessWithNonPrimaryAddresses()
		{
			// Arrange
			var request = new EmployerAddressRequest
			{
				Criteria = new EmployerAddressCriteria {EmployerId = 1193171, IsPrimary = false}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetEmployerAddress(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.PrimaryEmployerAddress.ShouldBeNull();
			response.OtherEmployerAddresses.Count.ShouldEqual(1);
			response.AllEmployerAddresses.Count.ShouldEqual(0);
		}

		[Test]
		public void GetEmployerAddress_WhenPassedRequestWithValidEmployerIdOnly_ReturnSuccessWithAllAddresses()
		{
			// Arrange
			var request = new EmployerAddressRequest {Criteria = new EmployerAddressCriteria {EmployerId = 1193171}};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetEmployerAddress(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.PrimaryEmployerAddress.ShouldNotBeNull();
			response.OtherEmployerAddresses.Count.ShouldEqual(1);
			response.AllEmployerAddresses.Count.ShouldEqual(2);
		}

		[Test]
		public void GetEmployerJobs_WhenPassedRequestWithValidEmployerIdOnly_ReturnSuccessWithAllAddresses()
		{
			// Arrange
			var request = new EmployerJobRequest
			{
				Criteria = new JobCriteria {EmployerId = 1088910, FetchOption = CriteriaBase.FetchOptions.List, ListSize = 10}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetEmployerJobs(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Jobs.ShouldNotBeNull();
			response.Jobs.Count.ShouldBeGreater(0);
			response.Jobs.Count.ShouldBeLessOrEqual(10);
		}

		[Test]
		public void GetEmployerJobs_WhenPassedRequestWithValidEmployerIdAndInternshipType_ReturnSuccessWithAllAddresses()
		{
			// Arrange
			var request = new EmployerJobRequest
			{
				Criteria = new JobCriteria
				{
					EmployerId = 1185028,
					JobTypes = new List<JobTypes> {JobTypes.InternshipPaid, JobTypes.InternshipUnpaid},
					FetchOption = CriteriaBase.FetchOptions.List,
					ListSize = 10
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetEmployerJobs(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Jobs.ShouldNotBeNull();
			response.Jobs.Count.ShouldBeGreater(0);
			response.Jobs.Count.ShouldBeLessOrEqual(10);
		}

		[Test]
		public void GetEmployerJobs_WhenPassedRequestWithNoEmployerId_ReturnSuccessAndNoData()
		{
			// Arrange
			var request = new EmployerJobRequest
			{
				Criteria = new JobCriteria {FetchOption = CriteriaBase.FetchOptions.List, ListSize = 10}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetEmployerJobs(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Jobs.ShouldBeNull();
		}

		[Test]
		public void SaveBusinessUnitDescriptions_WhenPassedRequestWithBusinessUnits_ReturnsSuccessAndSavesData()
		{
			// Arrange
			var currentBusinessUnitDescriptions =
				DataCoreUnitOfWorkScope.Current.BusinessUnitDescriptions.Where(x => x.BusinessUnitId == 5533272).ToList();
			var edsToSave = new List<BusinessUnitDescriptionDto>();

			currentBusinessUnitDescriptions.ForEach(x => edsToSave.Add(x.AsDto()));
			edsToSave.Add(new BusinessUnitDescriptionDto
			{
				BusinessUnitId = 5533272,
				Id = 0,
				IsPrimary = false,
				Description = "Test",
				Title = "Test Title"
			});

			var request = new SaveBusinessUnitDescriptionsRequest {Descriptions = edsToSave, BusinessUnitId = 5533272};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.SaveBusinessUnitDescriptions(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.DescriptionsAssociatedWithJob.ShouldBeNull();
			response.Descriptions.Count.ShouldEqual(3);
		}

		[Test]
		public void
			SaveBusinessUnitDescriptions_WhenPassedRequestWithBusinessUnitsNotInDb_ReturnsSuccessAndSavesDataAndRemovesExistingData
			()
		{
			// Arrange
			var edsToSave = new List<BusinessUnitDescriptionDto>();

			// make sure we have the items associated with jobs so we dont try to delete them
			var descriptionsAssociatedWithJob = (from bud in DataCoreUnitOfWorkScope.Current.BusinessUnitDescriptions
				join j in DataCoreUnitOfWorkScope.Current.Jobs on bud.Id equals j.BusinessUnitDescriptionId
				where bud.BusinessUnitId == 1103829
				select bud).Distinct().ToList();

			edsToSave.AddRange(descriptionsAssociatedWithJob.Select(x => x.AsDto()));

			// add a new ed
			edsToSave.Add(new BusinessUnitDescriptionDto
			{
				BusinessUnitId = 1103829,
				Id = 0,
				IsPrimary = false,
				Description = "Test",
				Title = "Test Title"
			});

			var request = new SaveBusinessUnitDescriptionsRequest {Descriptions = edsToSave, BusinessUnitId = 1103829};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.SaveBusinessUnitDescriptions(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.DescriptionsAssociatedWithJob.ShouldBeNull();
			response.Descriptions.Count.ShouldEqual(4);
		}

		[Test]
		public void
			SaveBusinessUnitDescriptions_WhenPassedRequestWithBusinessUnitsNotInDbWhereAssociatedWithJob_ReturnsFailure()
		{
			// Arrange
			var currentDescriptions =
				DataCoreUnitOfWorkScope.Current.BusinessUnitDescriptions.Where(x => x.BusinessUnitId == 1103829).ToList();
			var edsToSave = new List<BusinessUnitDescriptionDto>();

			// make sure we do not have the items associated with jobs so we try to delete them
			edsToSave.AddRange(currentDescriptions.Where(x => !x.IsPrimary).Select(x => x.AsDto()));

			// add a new ed
			edsToSave.Add(new BusinessUnitDescriptionDto
			{
				BusinessUnitId = 1103829,
				Id = 0,
				IsPrimary = false,
				Description = "Test",
				Title = "Test Title"
			});

			var request = new SaveBusinessUnitDescriptionsRequest {Descriptions = edsToSave, BusinessUnitId = 1103829};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.SaveBusinessUnitDescriptions(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.DescriptionsAssociatedWithJob.Count.ShouldEqual(1);
			response.Descriptions.ShouldBeNull();
		}

		[Test]
		public void SaveBusinessUnitLogos_WhenPassedRequestWithBusinessUnits_ReturnsSuccessAndSavesData()
		{
			// Arrange
			var currentLogos = DataCoreUnitOfWorkScope.Current.BusinessUnitLogos.Where(x => x.BusinessUnitId == 1104440).ToList();
			var logosToSave = new List<BusinessUnitLogoDto>();

			currentLogos.ForEach(x => logosToSave.Add(x.AsDto()));

			logosToSave.Add(new BusinessUnitLogoDto
			{
				BusinessUnitId = 1104440,
				Id = 0,
				Name = "Name",
				Logo = new byte[] {0x32, 0x32, 0x32, 0x32}
			});

			var request = new SaveBusinessUnitLogosRequest {BusinessUnitLogos = logosToSave, BusinessUnitId = 1104440};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.SaveBusinessUnitLogos(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.BusinessUnitLogosAssociatedWithJob.ShouldBeNull();
			response.BusinessUnitLogos.Count.ShouldEqual(2);
		}

		[Test]
		public void
			SaveBusinessUnitLogos_WhenPassedRequestWithBusinessUnitsNotInDb_ReturnsSuccessAndSavesDataAndRemovesExistingData()
		{
			// Arrange
			var currentLogos = DataCoreUnitOfWorkScope.Current.BusinessUnitLogos.Where(x => x.BusinessUnitId == 1104440).ToList();
			var logosToSave = new List<BusinessUnitLogoDto>();

			// make sure we have the items associated with jobs so we dont try to delete them
			logosToSave.AddRange(currentLogos.Where(x => x.Id == 1108641).Select(x => x.AsDto()));

			// add a new ed
			logosToSave.Add(new BusinessUnitLogoDto
			{
				BusinessUnitId = 1104440,
				Id = 0,
				Name = "Name",
				Logo = new byte[] {0x32, 0x32, 0x32, 0x32}
			});

			var request = new SaveBusinessUnitLogosRequest {BusinessUnitLogos = logosToSave, BusinessUnitId = 1104440};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.SaveBusinessUnitLogos(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.BusinessUnitLogosAssociatedWithJob.ShouldBeNull();
			response.BusinessUnitLogos.Count.ShouldEqual(2);
		}

		[Test]
		public void SaveBusinessUnitLogos_WhenPassedRequestWithBusinessUnitsNotInDbWhereAssociatedWithJob_ReturnsFailure()
		{
			// Arrange
			var logosToSave = new List<BusinessUnitLogoDto>
			{
				new BusinessUnitLogoDto
				{BusinessUnitId = 1104440, Id = 0, Name = "Name", Logo = new byte[] {0x32, 0x32, 0x32, 0x32}}
			};

			// add a new ed

			var request = new SaveBusinessUnitLogosRequest {BusinessUnitId = 1104440, BusinessUnitLogos = logosToSave};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.SaveBusinessUnitLogos(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.BusinessUnitLogosAssociatedWithJob.Count.ShouldEqual(1);
			response.BusinessUnitLogos.ShouldBeNull();
		}

		[Test]
		public void SaveBusinessUnitModel_WhenPassedValidModel_ReturnsSuccess()
		{
			// Arrange
			var businessUnitDescription = new BusinessUnitDescriptionDto
			{
				Title = "Mock business unit description",
				Description = "Business unit description for Mock new business unit",
				IsPrimary = true
			};

			var businessUnit = MockNewBusinessUnit;
			businessUnit.EmployerId = MockEmployerId;

			var model = new BusinessUnitModel
			{
				BusinessUnit = businessUnit,
				BusinessUnitAddress = MockBusinessUnitAddress.AsDto(),
				BusinessUnitDescription = businessUnitDescription,
				LockVersion = 0
			};

			var request = new SaveBusinessUnitModelRequest().Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			request.Model = model;

			// Act
			var response = _service.SaveBusinessUnitModel(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Model.ShouldNotBeNull();
			response.Model.BusinessUnit.ShouldNotBeNull();
			response.Model.BusinessUnit.Id.Value.ShouldBeGreater(0);
		}

		[Test]
		public void SaveEmployerDetailsModel_WhenPassedValidRequest_ReturnsSuccessAndSavesData()
		{
			// Arrange
			var request = new SaveEmployerDetailsRequest
			{
				Criteria = new EmployerCriteria {EmployerId = 13930, FetchOption = CriteriaBase.FetchOptions.Single},
				FederalEmployerIdentificationNumber = "00-000000",
				Name = "Mock Test Name",
				StateEmployerIdentificationNumber = "01-1111111",
				LockVersion = 0

			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.SaveEmployerDetails(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}


		[Test]
		public void GetBusinessUnitActivityLog_WhenPassedValidRequest_ReturnsSuccessWithExpectedData()
		{
			// Arrange
			var request = new BusinessUnitActivityRequest
			{
				Criteria = new BusinessUnitCriteria {BusinessUnitId = 1546755, FetchOption = CriteriaBase.FetchOptions.PagedList}
			};
			request.Criteria.OrderBy = Constants.SortOrders.ActivityDateAsc;
			request.Criteria.FetchOption = CriteriaBase.FetchOptions.PagedList;

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act	
			var response = _service.GetBusinessUnitActivities(request);

			// Assert 
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.ShouldNotBeNull();
			response.BusinessUnitActivitiesPaged.TotalCount.ShouldBeGreater(0);
			response.BusinessUnitActivitiesPaged.HasNextPage.ShouldBeTrue();
		}

		[Test]
		public void GetBusinessUnitPlacements_WhenPassedValidRequest_ReturnsSuccessWithExpectedData()
		{
			// Arrange
			var request = new BusinessUnitPlacementsRequest
			{
				Criteria = new BusinessUnitCriteria {BusinessUnitId = 5533272, FetchOption = CriteriaBase.FetchOptions.PagedList}
			};
			request.Criteria.OrderBy = Constants.SortOrders.PlacementNameDesc;
			request.Criteria.FetchOption = CriteriaBase.FetchOptions.PagedList;

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act	
			var response = _service.GetBusinessUnitPlacements(request);

			// Assert 
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.BusinessUnitPlacementsViewsPaged.TotalCount.ShouldEqual(1);
		}

		[Test]
		public void GetBusinessUnitPlacement_WhenPassedValidRequestWithNoBusinessUnitId_ReturnSuccessWithAllData()
		{
			// Arrange
			var request = new BusinessUnitPlacementsRequest
			{
				Criteria = new BusinessUnitCriteria {FetchOption = CriteriaBase.FetchOptions.PagedList}
			};
			request.Criteria.OrderBy = Constants.SortOrders.PlacementNameDesc;
			request.Criteria.FetchOption = CriteriaBase.FetchOptions.PagedList;

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act	
			var response = _service.GetBusinessUnitPlacements(request);

			// Assert 
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.BusinessUnitPlacementsViewsPaged.TotalCount.ShouldNotEqual(0);
		}

		[Test]
		public void GetBusinessUnitPlacements_WhenPassedUnknownRequest_ReturnSuccessWithNoData()
		{
			// Arrange
			var request = new BusinessUnitPlacementsRequest
			{
				Criteria = new BusinessUnitCriteria {BusinessUnitId = 1001191, FetchOption = CriteriaBase.FetchOptions.PagedList}
			};
			request.Criteria.OrderBy = Constants.SortOrders.PlacementNameDesc;
			request.Criteria.FetchOption = CriteriaBase.FetchOptions.PagedList;

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act	
			var response = _service.GetBusinessUnitPlacements(request);

			// Assert 
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.BusinessUnitPlacementsViewsPaged.TotalCount.ShouldEqual(0);
		}

		[Test]
		public void IgnoreRecentlyPlacedMatch_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new IgnoreRecentlyPlacedMatchRequest {RecentlyPlacedMatchId = 1};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act	
			var response = _service.IgnoreRecentlyPlacedMatch(request);

			// Assert 
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void AllowRecentlyPlacedMatch_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request1 = new IgnoreRecentlyPlacedMatchRequest {RecentlyPlacedMatchId = 1};

			request1.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			_service.IgnoreRecentlyPlacedMatch(request1);

			var request2 = new AllowRecentlyPlacedMatchRequest {RecentlyPlacedMatchId = 1};
			request2.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act	
			var response2 = _service.AllowRecentlyPlacedMatch(request2);

			// Assert 
			response2.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void SearchEmployer_WhenPassedExistingCompanyZipCode_ReturnsRecordsAndSuccess()
		{
			var request = new SearchBusinessUnitsRequest() {SearchCriteria = new BusinessUnitCriteria() {ZipCode = "12345"}};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			var response = _service.SearchBusinessUnits(request);

			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.BusinessUnits.Count.ShouldNotEqual(0);
		}

		[Test]
		public void SearchEmployer_WhenPassedNewC0mpanyZipCode_ReturnsNoRecordsAndSuccess()
		{
			var request = new SearchBusinessUnitsRequest()
			{
				SearchCriteria = new BusinessUnitCriteria() {ZipCode = "zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz"}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			var response = _service.SearchBusinessUnits(request);

			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.BusinessUnits.Count.ShouldEqual(0);
		}

		#region Office tests

		[Test]
		public void GetOffices_WhenPassedValidOfficeId_ReturnsOneRecordAndSuccess()
		{
			var request = new OfficeRequest() {Criteria = new OfficeCriteria() {OfficeId = 1}};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			var response = _service.GetOffices(request);

			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Office.ShouldNotBeNull();
		}

		[Test]
		public void GetOffices_WhenPassedInvalidOfficeId_ReturnsNoRecordAndSuccess()
		{
			var request = new OfficeRequest() { Criteria = new OfficeCriteria() { OfficeId = -1 } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			var response = _service.GetOffices(request);

			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Office.ShouldBeNull();
		}

		[Test]
		public void GetOffices_WhenPassedDefaultOfficeFlag_ReturnsOneRecordAndSuccess()
		{
			// Arrange
			var request = new OfficeRequest
			{
			  Criteria = new OfficeCriteria
			  {
			    DefaultOffice = true,
          DefaultType = OfficeDefaultType.Employer
			  }
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetOffices(request);

			// Assert
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
      (response.Office.DefaultType & OfficeDefaultType.Employer).ShouldEqual(OfficeDefaultType.Employer);
		}

		[Test]
		public void GetOffices_WhenPassedValidPersonId_ReturnsListAndSuccess()
		{
			var request = new OfficeRequest()
			{
				Criteria = new OfficeCriteria() {PersonId = 1088251, FetchOption = CriteriaBase.FetchOptions.List}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			var response = _service.GetOffices(request);

			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Offices.Count.ShouldBeGreater(0);
		}

		[Test]
		public void GetOffices_WhenPassedValidPersonIdManagesState_ReturnsListAndSuccess()
		{
			var request = new OfficeRequest()
			{
				Criteria = new OfficeCriteria() { PersonId = 1, FetchOption = CriteriaBase.FetchOptions.List }
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			var response = _service.GetOffices(request);

			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Offices.Count.ShouldBeGreater(0);
		}

		[Test]
		public void GetOffices_WhenPassedValidPersonId_ReturnsPagedListAndSuccess()
		{
			// Arrange
			var request = new OfficeRequest()
			{
				Criteria = new OfficeCriteria() {PersonId = 1088251, FetchOption = CriteriaBase.FetchOptions.PagedList}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetOffices(request);

			// Assert 
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.ShouldNotBeNull();
			response.OfficesPaged.TotalCount.ShouldBeGreater(0);
		}

		[Test]
		public void GetOffices_WhenPassedInvalidPersonId_ReturnsEmptyListAndSuccess()
		{
			// Arrange
			var request = new OfficeRequest{Criteria = new OfficeCriteria() {PersonId = -1, FetchOption = CriteriaBase.FetchOptions.List}};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetOffices(request);

			// Assert
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.Offices.ShouldBeNull();
			response.OfficesPaged.ShouldBeNull();
			response.Office.ShouldBeNull();
		}

		[Test]
		public void GetOffices_WhenPassedInactiveFalse_ReturnsSuccess()
		{
			// Arrange
			var request = new OfficeRequest { Criteria = new OfficeCriteria() { InActive = false, FetchOption = CriteriaBase.FetchOptions.List } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetOffices(request);

			// Assert
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Offices.ShouldNotBeNull();
			response.Offices.Count.ShouldBeGreater(0);
		}

		[Test]
		public void GetOffices_WhenPassedInactiveTrue_ReturnsSuccess()
		{
			// Arrange
			var request = new OfficeRequest { Criteria = new OfficeCriteria() { InActive = true, FetchOption = CriteriaBase.FetchOptions.List } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetOffices(request);

			// Assert
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Offices.ShouldNotBeNull();
			response.Offices.Count.ShouldBeGreater(0);
		}

		[Test]
		public void GetOffices_WhenPassedInactiveNull_ReturnsSuccess()
		{
			// Arrange
			var request = new OfficeRequest { Criteria = new OfficeCriteria() { FetchOption = CriteriaBase.FetchOptions.List } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetOffices(request);

			// Assert
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Offices.ShouldNotBeNull();
			response.Offices.Count.ShouldBeGreater(0);
		}

		[Test]
		public void SaveOffice_WhenPassedValidNewOfficeWithoutDefaultAdminsitrator_ReturnsOfficeDtoWithIdAndSuccess()
		{
			var office = new OfficeDto
			{

				OfficeName = "Test Office Name",
				Line1 = "Test Line 1",
				Line2 = "Test Line 2",
				TownCity = "Test City",
				CountyId = 1583098,
				StateId = 12834,
				CountryId = 13370,
				PostcodeZip = "40013",
				AssignedPostcodeZip = "11111, 22222, 33333"
			};

			var request = new SaveOfficeRequest() {Office = office};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			var response = _service.SaveOffice(request);

			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Office.ShouldNotBeNull();
			response.Office.Id.ShouldNotBeNull();
			response.Office.DefaultAdministratorId.ShouldEqual(null);
		}

		[Test]
		public void SaveOffice_WhenPassedInValidNewOffice_ReturnsFailure()
		{
			// Arrange
			var office = new OfficeDto
			{
				OfficeName = "Office Name with more than 40 characters in the text that should break the sav office request",
				Line1 = "Test Line 1",
				Line2 = "Test Line 2",
				TownCity = "Test City",
				CountyId = 1583098,
				StateId = 12834,
				CountryId = 13370,
				PostcodeZip = "40013",
				AssignedPostcodeZip = "11111, 22222, 33333"
			};

			// Act
			var request = new SaveOfficeRequest() {Office = office};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			var response = _service.SaveOffice(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}

		[Test]
		public void SaveOffice_WhenPassedValidExistingOffice_ReturnsOfficeDtoWithIdAndSuccess()
		{
			// Similar to the saving a new office but there will be an existing ID in the dto
			// Arrange
			var office = DataCoreRepository.FindById<Office>(1).AsDto();
			office.OfficeName = "New Test Save";

			// Act
			var request = new SaveOfficeRequest() {Office = office};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			var response = _service.SaveOffice(request);

			// Assert
			response.Office.Id.ShouldNotBeNull();
			response.Office.OfficeName = "New Test Save";
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void SaveOffice_WhenPassedInValidExistingOffice_ReturnsFailure()
		{
			// Similar to above but there will be an ID in the dto that doesn't exist in the DB
			// Arrange
			var office = DataCoreRepository.FindById<Office>(1).AsDto();
			// Update to Invalid Office
			office.Id = -1;
			office.OfficeName = "Office Name with more than 20 characters";

			// Act
			var request = new SaveOfficeRequest() {Office = office};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			var response = _service.SaveOffice(request);

			// Assert
			response.Office.ShouldBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}

		[Test]
		public void SaveOffice_WhenPassedValidNewOfficeWithDefaultAdministrator_ReturnsOfficeDtoWithIdAndSuccess()
		{
			var office = new OfficeDto
			{

				OfficeName = "Test Office Name",
				Line1 = "Test Line 1",
				Line2 = "Test Line 2",
				TownCity = "Test City",
				CountyId = 1583098,
				StateId = 12834,
				CountryId = 13370,
				PostcodeZip = "40013",
				AssignedPostcodeZip = "11111, 22222, 33333",
				DefaultAdministratorId = 1097124
			};

			var request = new SaveOfficeRequest() { Office = office };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);
			var response = _service.SaveOffice(request);

			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Office.ShouldNotBeNull();
			response.Office.Id.ShouldNotBeNull();
			response.Office.DefaultAdministratorId.ShouldEqual(1097124);
		}

		[Test]
		public void GetOfficeStaffMembers_WhenPassedValidOfficeId_ReturnsPagedListAndSuccess()
		{
			// Arrange
			var request = new StaffMemberRequest()
			{
				Criteria = new StaffMemberCriteria() {OfficeIds = new List<long?>{1}, FetchOption = CriteriaBase.FetchOptions.PagedList}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetOfficeStaffMembers(request);

			// Assert 
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.ShouldNotBeNull();
			response.StaffMembersPaged.TotalCount.ShouldBeGreater(0);
		}

		[Test]
		public void GetOfficeStaffMembers_WhenPassedValidOfficeId_ReturnsListAndSuccess()
		{
			// Arrange
			var request = new StaffMemberRequest()
			{
				Criteria = new StaffMemberCriteria() {OfficeIds = new List<long?>{1}, FetchOption = CriteriaBase.FetchOptions.List}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetOfficeStaffMembers(request);

			// Assert
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.StaffMembers.Count.ShouldBeGreater(0);
		}

		[Test]
		public void GetOfficeStaffMembers_WhenPassedInValidOfficeId_ReturnsEmptyListAndSuccess()
		{
			// Arrange
			var request = new StaffMemberRequest()
			{
				Criteria = new StaffMemberCriteria() {OfficeIds = new List<long?>{-1}, FetchOption = CriteriaBase.FetchOptions.List}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetOfficeStaffMembers(request);

			// Assert
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.StaffMembers.Count.ShouldEqual(0);
		}

		[Test]
		public void GetOfficeStaffMembers_WhenPassedValidOfficeId_ReturnsNameListAndSuccess()
		{
			// Arrange
			var request = new StaffMemberRequest()
			{
				Criteria = new StaffMemberCriteria() { CheckNames = true, Name = "dev", ListSize = 15, FetchOption = CriteriaBase.FetchOptions.List}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetOfficeStaffMembers(request);

			// Assert 
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.ShouldNotBeNull();
			response.StaffMembers.ShouldNotBeNull();
			response.StaffMembers.Count.ShouldBeGreaterOrEqual(1);
		}

		[Test]
		public void GetOfficeStaffMembers_WhenPassedStatewideFalse_ReturnsListAndSuccess()
		{
			// Arrange
			var request = new StaffMemberRequest()
			{
				Criteria = new StaffMemberCriteria() { Statewide = false, FetchOption = CriteriaBase.FetchOptions.List }
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetOfficeStaffMembers(request);

			// Assert 
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.ShouldNotBeNull();
			response.StaffMembers.ShouldNotBeNull();
			response.StaffMembers.Count.ShouldBeGreaterOrEqual(1);
		}

		[Test]
		public void GetOfficeStaffMembers_WhenPassedStatewideTrue_ReturnsNameListAndSuccess()
		{
			// Arrange
			var request = new StaffMemberRequest()
			{
				Criteria = new StaffMemberCriteria() { Statewide = true, FetchOption = CriteriaBase.FetchOptions.List }
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetOfficeStaffMembers(request);

			// Assert 
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.ShouldNotBeNull();
			response.StaffMembers.ShouldNotBeNull();
			response.StaffMembers.Count.ShouldBeGreaterOrEqual(1);
		}

		[Test]
		public void GetOfficeManagers_WhenPassedValidOfficeId_ReturnsListAndSuccess()
		{
			// Arrange
			var request = new StaffMemberRequest() {Criteria = new StaffMemberCriteria() {OfficeIds = new List<long?>{1}}};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetOfficeManagers(request);

			// Assert
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.StaffMembers.Count.ShouldBeGreater(0);
		}

		[Test]
		public void GetOfficeManagers_WhenPassedInValidOfficeId_ReturnsEmptyListAndSuccess()
		{
			// Arrange
			var request = new StaffMemberRequest() { Criteria = new StaffMemberCriteria() { OfficeIds = new List<long?>{-1} } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetOfficeManagers(request);

			// Assert
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.StaffMembers.Count.ShouldEqual(0);
		}

		[Test]
		public void GetStaffMemberCount_WhenPassedValidOfficeId_ReturnsListAndSuccess()
		{
			// Arrange
			var request = new StaffMemberRequest() { Criteria = new StaffMemberCriteria() { OfficeIds = new List<long?>{1} } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetStaffMemberCount(request);

			// Assert
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.StaffMemberCount.ShouldBeGreater(0);
		}

		[Test]
		public void GetStaffMemberCount_WhenPassedInValidOfficeId_ReturnsEmptyListAndSuccess()
		{
			// Arrange
			var request = new StaffMemberRequest() { Criteria = new StaffMemberCriteria() { OfficeIds = new List<long?>{-1} } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetStaffMemberCount(request);

			// Assert
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.StaffMemberCount.ShouldEqual(0);
		}

		[Test]
		public void HasAssignedPostcodes_WhenPassedValidOffice_ReturnsTrueAndSuccess()
		{
			// Arrange
			var request = new OfficeRequest(){Criteria = new OfficeCriteria () { OfficeId = 1}} ;
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.HasAssignedPostcodes(request);

			// Assert
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.HasAssignedPostcodes.ShouldEqual(true);
		}

		[Test]
		public void HasAssignedPostcodes_WhenPassedInValidOffice_ReturnsFalseAndSuccess()
		{
			// Arrange
			var request = new OfficeRequest() { Criteria = new OfficeCriteria() { OfficeId = 4 } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.HasAssignedPostcodes(request);

			// Assert
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.HasAssignedPostcodes.ShouldEqual(false);
		}

		[Test]
		public void SavePersonOffices_WhenPassedValidPerson_ReturnsSuccess()
		{
			// Arrange
			var edsToSave = new List<PersonOfficeMapperDto>();
						edsToSave.Add(new PersonOfficeMapperDto
			{
				OfficeId = 4,
				PersonId = 1093026
			});

			var request = new PersonOfficeMapperRequest() { PersonId = 1093026, PersonOffices = edsToSave };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.SavePersonOffices(request);

			// Assert
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			DataCoreRepository.PersonOfficeMappers.Any(x => x.OfficeId.Equals(4) && x.PersonId.Equals(1093026)).ShouldBeTrue();
		}

		[Test]
		public void SavePersonOffices_WhenPassedInValidPerson_ReturnsFailure()
		{
			
			// Arrange
			var edsToSave = new List<PersonOfficeMapperDto>();
			edsToSave.Add(new PersonOfficeMapperDto
			{
				Id = 6,
				OfficeId = 4,
				PersonId = -1
			});

			var request = new PersonOfficeMapperRequest() { PersonId = -1, PersonOffices = edsToSave };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.SavePersonOffices(request);

			// Assert
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			DataCoreRepository.PersonOfficeMappers.Any(x => x.OfficeId.Equals(4) && x.PersonId.Equals(-1)).ShouldBeFalse();
		}

		[Test]
		public void SavePersonOffices_WhenPassedValidPersonAndInvalidOffice_ReturnsFailure()
		{
		
			// Arrange
			var edsToSave = new List<PersonOfficeMapperDto>();
			edsToSave.Add(new PersonOfficeMapperDto
			{
				Id = 0,
				OfficeId = -1,
				PersonId = 1093026
			});

			var request = new PersonOfficeMapperRequest() { PersonId = 1093026, PersonOffices = edsToSave };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.SavePersonOffices(request);

			// Assert
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			DataCoreRepository.PersonOfficeMappers.Any(x => x.OfficeId.Equals(4) && x.PersonId.Equals(1093026)).ShouldBeFalse();
		}

		[Test]
		public void SavePersonOffices_WhenPassedValidPersonWhoIsAssignedToAJobAssigningToNewOffice_ReturnsSuccess()
		{
			var originalCount = DataCoreRepository.Jobs.Count(x => x.AssignedToId.Equals(1093026));

			// Arrange
			var edsToSave = new List<PersonOfficeMapperDto>
				                {
													new PersonOfficeMapperDto
						                {
							                OfficeId = 1,
							                PersonId = 1093026
						                },
													new PersonOfficeMapperDto
						                {
							                OfficeId = 2,
							                PersonId = 1093026
						                },
													new PersonOfficeMapperDto
						                {
							                OfficeId = 3,
							                PersonId = 1093026
						                },
					                new PersonOfficeMapperDto
						                {
							                OfficeId = 9,
							                PersonId = 1093026
						                }
				                };

			var request = new PersonOfficeMapperRequest() { PersonId = 1093026, PersonOffices = edsToSave };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.SavePersonOffices(request);

			// Assert
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			DataCoreRepository.PersonOfficeMappers.Count(x => x.PersonId.Equals(1093026)).ShouldEqual(4);

			DataCoreRepository.Jobs.Count(x => x.AssignedToId.Equals(1093026)).ShouldEqual(originalCount);
		}

		[Test]
		public void SavePersonOffices_WhenPassedValidPersonWhoIsAssignedToAJobRemovingFromAnOffice_ReturnsSuccess()
		{
			var originalCount = DataCoreRepository.Jobs.Count(x => x.AssignedToId.Equals(1093026));

			// Arrange
			var edsToSave = new List<PersonOfficeMapperDto>
				                {
													new PersonOfficeMapperDto
						                {
							                OfficeId = 1,
							                PersonId = 1093026
						                },
													new PersonOfficeMapperDto
						                {
							                OfficeId = 2,
							                PersonId = 1093026
						                }
				                };

			var request = new PersonOfficeMapperRequest() { PersonId = 1093026, PersonOffices = edsToSave };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.SavePersonOffices(request);

			// Assert
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			DataCoreRepository.PersonOfficeMappers.Count(x => x.PersonId.Equals(1093026)).ShouldEqual(2);

			DataCoreRepository.Jobs.Count(x => x.AssignedToId.Equals(1093026)).ShouldNotBeTheSameAs(originalCount);
			DataCoreRepository.Jobs.Count(x => x.AssignedToId.Equals(1093026)).ShouldEqual(0);
		}

		[Test]
		public void SavePersonOffices_WhenValidPersonChangedToStatewideWhoIsDefaultAdministrator_ReturnsSuccess()
		{
			MockAppSettings.IntegrationClient = IntegrationClient.EKOS;

			// Arrange
			var edsToSave = new List<PersonOfficeMapperDto>
				                {
													new PersonOfficeMapperDto
						                {
							                StateId = 12834,
							                PersonId = 1088251
						                }
				                };

			var request = new PersonOfficeMapperRequest() { PersonId = 1088251, PersonOffices = edsToSave };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.SavePersonOffices(request);

			// Assert
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			var pom = DataCoreRepository.PersonOfficeMappers.Where(x => x.PersonId.Equals(1088251));
			pom.Count(x => x.PersonId.Equals(1088251)).ShouldEqual(1);
			pom.Select(x => x.OfficeId).FirstOrDefault().ShouldBeNull();
			pom.Select(x => x.StateId).FirstOrDefault().ShouldEqual(12834);

			DataCoreRepository.Offices.Where(x => x.Id.Equals(9)).Select(x => x.DefaultAdministratorId).FirstOrDefault().ToString().ShouldBeNullOrEmpty();

			// Set back to the default setting - IMPORTANT!
			MockAppSettings.IntegrationClient = IntegrationClient.Standalone;
		}


		[Test]
		public void AssignOfficeStaff_WhenPassedOneOfficeWithMultipleStaffMembersToAdd_ReturnsSuccess()
		{
			// Arrange
			var officeStaff = new List<PersonOfficeMapperDto>
				                  {
					                  new PersonOfficeMapperDto
						                  {
							                  OfficeId = 4,
							                  PersonId = 1132303
						                  }, 
															new PersonOfficeMapperDto
							                {
								                OfficeId = 4,
								                PersonId = 1128583
							                }, 
				                  };

			var request = new PersonOfficeMapperRequest() { PersonOffices = officeStaff };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.AssignOfficeStaff(request);

			// Assert
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			DataCoreRepository.PersonOfficeMappers.Where(pom => pom.Person.User.UserType.Equals(UserTypes.Assist)).Count(x => x.OfficeId.Equals(4)).ShouldEqual(2);
		}

		[Test]
		public void AssignOfficeStaff_WhenPassedOneOfficeWithMultipleStaffMembersToAddAndRemove_ReturnsSuccess()
		{
			// Arrange
			var officeStaff = new List<PersonOfficeMapperDto>
				                  {
					                  new PersonOfficeMapperDto
						                  {
							                  OfficeId = 2,
							                  PersonId = 1132303
						                  }, 
															new PersonOfficeMapperDto
							                {
								                OfficeId = 2,
								                PersonId = 1128583
							                }
				                  };

			var request = new PersonOfficeMapperRequest() { PersonOffices = officeStaff };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.AssignOfficeStaff(request);

			// Assert
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);

			DataCoreRepository.PersonOfficeMappers.Count(x => x.OfficeId.Equals(2) && x.Person.User.UserType.Equals(UserTypes.Assist)).ShouldEqual(2);
		}

		[Test]
		public void AssignOfficeStaff_WhenPassedMultiplePersonsOneOfficePersonAssignedToJobJobAssignedToOffice_ReturnsSuccess()
		{
			var originalCount = DataCoreRepository.Jobs.Count(x => x.AssignedToId.Equals(1093026));

			// Arrange
			var edsToSave = new List<PersonOfficeMapperDto>
				                {
													new PersonOfficeMapperDto
						                {
							                OfficeId = 3,
							                PersonId = 1
						                },
													new PersonOfficeMapperDto
						                {
							                OfficeId = 3,
							                PersonId = 1103437
						                }
				                };

			var request = new PersonOfficeMapperRequest() { PersonOffices = edsToSave };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.AssignOfficeStaff(request);

			// Assert
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			DataCoreRepository.PersonOfficeMappers.Count(x => x.OfficeId.Equals(3) && x.Person.User.UserType.Equals(UserTypes.Assist)).ShouldEqual(2);

			DataCoreRepository.Jobs.Count(x => x.AssignedToId.Equals(1093026)).ShouldEqual(originalCount);
		}

		[Test]
		public void AssignOfficeStaff_WhenPassedMultiplePersonsOneOfficePersonAssignedToJobJobNotAssignedToOffice_ReturnsSuccess()
		{
			var originalCount = DataCoreRepository.Jobs.Count(x => x.AssignedToId.Equals(1));

			// Arrange
			var edsToSave = new List<PersonOfficeMapperDto>
				                {
													new PersonOfficeMapperDto
													  {
													    OfficeId = 2,
													    PersonId = 1093026
													  }
													  ,
													new PersonOfficeMapperDto
													  {
													    OfficeId = 2,
													    PersonId = 1103437
													  }
				                };

			var request = new PersonOfficeMapperRequest() { PersonOffices = edsToSave };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.AssignOfficeStaff(request);

			// Assert
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			DataCoreRepository.PersonOfficeMappers.Count(x => x.OfficeId.Equals(2) && x.Person.User.UserType.Equals(UserTypes.Assist)).ShouldEqual(2);

			DataCoreRepository.Jobs.Count(x => x.AssignedToId.Equals(1)).ShouldEqual(originalCount - 1);
		}

		[Test]
		public void GetOfficeEmployers_WhenPassedValidOffice_ReturnsListAndSuccess()
		{
			// Arrange
			var request = new OfficeEmployersRequest() { OfficeId = 1 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetOfficeEmployers(request);

			// Assert
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.OfficeEmployers.ShouldNotBeNull();
		}

		[Test]
		public void GetOfficeEmployers_WhenPassedInValidOffice_ReturnsEmptyListAndSuccess()
		{
			// Arrange
			var request = new OfficeEmployersRequest() { OfficeId = -1 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetOfficeEmployers(request);

			// Assert
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.OfficeEmployers.Count.ShouldEqual(0);
		}

		[Test]
		public void UpdateEmployerAssignedOffice_WhenPassedValidOfficeAndValidEmployer_ReturnSuccess()
		{
			// Arrange
			var eomToSave = new List<EmployerOfficeMapperDto>();
			eomToSave.Add(new EmployerOfficeMapperDto
			{
				OfficeId = 4,
				EmployerId = 1157083
			});

			var request = new UpdateEmployerAssignedOfficeRequest() { EmployerOffices = eomToSave, EmployerId = 1157083 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.UpdateEmployerAssignedOffice(request);

			// Assert
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void UpdateEmployerAssignedOffice_WhenPassedValidMultiOfficesAndValidEmployer_ReturnSuccess()
		{
			// Arrange
			var eomToSave = new List<EmployerOfficeMapperDto>();
			eomToSave.Add(new EmployerOfficeMapperDto
			{
				OfficeId = 4,
				EmployerId = 1157083
			});

			eomToSave.Add(new EmployerOfficeMapperDto
			{
				OfficeId = 6,
				EmployerId = 1157083
			});

			var request = new UpdateEmployerAssignedOfficeRequest() { EmployerOffices = eomToSave, EmployerId = 1157083 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.UpdateEmployerAssignedOffice(request);

			// Assert
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void UpdateEmployerAssignedOffice_WhenPassedInValidOfficeAndInValidEmployer_ReturnFailure()
		{
			// Arrange
			var eomToSave = new List<EmployerOfficeMapperDto>();
			eomToSave.Add(new EmployerOfficeMapperDto
			{
				OfficeId = 4,
				EmployerId = -1
			});

			var request = new UpdateEmployerAssignedOfficeRequest() { EmployerOffices = eomToSave, EmployerId = -1 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.UpdateEmployerAssignedOffice(request);

			// Assert
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);

		}

		[Test]
		public void UpdateDefaultOffice_WhenPassedValidOffice_ReturnSuccess()
		{
			// Arrange
			var request = new UpdateDefaultOfficeRequest() { DefaultOfficeId = 1 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.UpdateDefaultOffice(request);

			// Assert
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void UpdateDefaultOffice_WhenPassedInValidOffice_ReturnFailure()
		{
			// Arrange
			var request = new UpdateDefaultOfficeRequest() { DefaultOfficeId = -1 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.UpdateDefaultOffice(request);

			// Assert
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}

		[Test]
		public void GetOfficeJobs_WhenPassedValidOffice_ReturnsListAndSuccess()
		{
			// Arrange
			var request = new OfficeJobsRequest() { OfficeId = 4 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetOfficeJobs(request);

			// Assert
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.OfficeJobs.ShouldNotBeNull();
			response.OfficeJobs.Count.ShouldBeGreater(0);
		}

		[Test]
		public void GetOfficeJobs_WhenPassedValidOffice_ReturnsEmptyListAndSuccess()
		{
			// Arrange
			var request = new OfficeJobsRequest() { OfficeId = 1 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetOfficeJobs(request);

			// Assert
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.OfficeJobs.ShouldNotBeNull();
			response.OfficeJobs.Count.ShouldEqual(0);
		}

		[Test]
		public void UpdateOfficeAssignedZips_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new UpdateOfficeAssignedZipsRequest() { OfficeIds = new List<long> { 1 }, Zips = new List<string> { "12345" } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.UpdateOfficeAssignedZips(request);

			// Assert
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Offices.ShouldNotBeNull();
			response.Offices.Count.ShouldEqual(1);
			response.Offices[0].AssignedPostcodeZip.Contains("12345").ShouldBeTrue();

		}

		[Test]
		public void UpdateOfficeAssignedZips_WhenPassedNullOfficeRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new UpdateOfficeAssignedZipsRequest() { OfficeIds = new List<long> { 1 }, Zips = new List<string> { null } };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.UpdateOfficeAssignedZips(request);

			// Assert
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Offices.ShouldNotBeNull();
			response.Offices.Count.ShouldEqual(1);

		}

		[Test]
		public void IsOfficeAdministrator_WhenPassedValidPersonAndOffice_ReturnsSuccess()
		{
			var request = new OfficeRequest { Criteria = new OfficeCriteria { PersonId = 1088251, OfficeId = 9 } }.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.IsOfficeAdministrator(request);

			// Assert
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.IsDefaultAdministrator.ShouldBeTrue();
		}

		[Test]
		public void IsOfficeAdministrator_WhenPassedValidPersonAndNullOffice_ReturnsSuccess()
		{
			var request = new OfficeRequest { Criteria = new OfficeCriteria { PersonId = 1088251 } }.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.IsOfficeAdministrator(request);

			// Assert
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.IsDefaultAdministrator.ShouldBeTrue();
			response.Offices.ShouldNotBeNull();
			response.Offices.Count.ShouldEqual(1);
			response.Offices[0].Id.ShouldEqual(9);
		}

		[Test]
		public void IsOfficeAdministrator_WhenPassedInvalidOfficeAndValidPerson_ReturnsFailure()
		{
			var request = new OfficeRequest { Criteria = new OfficeCriteria { PersonId = 1088251, OfficeId = 9999 } }.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.IsOfficeAdministrator(request);

			// Assert
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}

		[Test]
		public void IsOfficeAdministrator_WhenPassedInvalidPersonAndNullOffice_ReturnsFailure()
		{
			var request = new OfficeRequest { Criteria = new OfficeCriteria { PersonId = 99999999 } }.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.IsOfficeAdministrator(request);

			// Assert
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.IsDefaultAdministrator.ShouldBeFalse();
		}


		#endregion

		[Test]
		public void GetManagerState_WhenPassedValidManager_ReturnsSuccess()
		{
			
			// Arrange
			var request = new GetManagerStateRequest() { PersonId = 1 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetManagerState(request);

			//Assert
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		
		}

		[Test]
		public void GetManagerState_WhenPassedValidPersonAndInvalidState_ReturnsSuccess()
		{
			
			// Arrange
			var request = new GetManagerStateRequest() { PersonId = 1093026 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetManagerState(request);

			// Assert
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.StateId.ShouldBeNull();
		
		}

		

		#region GetEmployerPlacements tests
		
		[Test]
		public void GetEmployerPlacements_WhenNoCriteriaPassed_ReturnsFailure()
		{
			// Arrange
			var request = new EmployerPlacementsViewRequest();
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetEmployerPlacements(request);

			// Assert	
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}

		[Test]
		public void GetEmployerPlacements_WhenStateWideOfficesCriteriaPassed_ReturnsSuccess()
		{
			// Arrange
			var request = new EmployerPlacementsViewRequest
			              	{
			              		Criteria = new PlacementsCriteria
			              		           	{
			              		           		FetchOption = CriteriaBase.FetchOptions.PagedList,
																			PageIndex = 0,
																			PageSize = 10,
																			OfficeGroup = OfficeGroup.StatewideOffices,
																			NumberOfDays = 1000
			              		           	}
			              	};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetEmployerPlacements(request);

			// Assert	
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void GetEmployerPlacements_WhenMyOfficesCriteriaPassed_ReturnsSuccess()
		{
			// Arrange
			var request = new EmployerPlacementsViewRequest
			{
				Criteria = new PlacementsCriteria
				{
					FetchOption = CriteriaBase.FetchOptions.PagedList,
					PageIndex = 0,
					PageSize = 10,
					OfficeGroup = OfficeGroup.MyOffices,
					NumberOfDays = 1000
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetEmployerPlacements(request);

			// Assert	
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void GetEmployerPlacements_WhenOfficeIdCriteriaPassed_ReturnsSuccess()
		{
			// Arrange
			var request = new EmployerPlacementsViewRequest
			{
				Criteria = new PlacementsCriteria
				{
					FetchOption = CriteriaBase.FetchOptions.PagedList,
					PageIndex = 0,
					PageSize = 10,
					OfficeId = 2,
					NumberOfDays = 1000
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetEmployerPlacements(request);

			// Assert	
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void GetEmployerPlacements_WhenInvalidOfficeIdCriteriaPassed_ReturnsSuccessButNoPlacements()
		{
			// Arrange
			var request = new EmployerPlacementsViewRequest
			{
				Criteria = new PlacementsCriteria
				{
					FetchOption = CriteriaBase.FetchOptions.PagedList,
					PageIndex = 0,
					PageSize = 10,
					OfficeId = 6050,
					NumberOfDays = 1000
				}
			};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetEmployerPlacements(request);

			// Assert	
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.EmployerPlacementsViewsPaged.ShouldNotBeNull();
			response.EmployerPlacementsViewsPaged.Count.ShouldEqual(0);
		}

		#endregion

		[Test]
		public void GetSpideredEmployersWithGoodMatches_WithValidRequest_ReturnsSuccessAndEmployers()
		{
			// Arrange
			var request = new SpideredEmployerRequest
			              	{
			              		Criteria = new SpideredEmployerCriteria
			              		           	{
			              		           		FetchOption = CriteriaBase.FetchOptions.PagedList,
			              		           		PageIndex = 0,
			              		           		PageSize = 10,
			              		           		PostingAge = 1000
			              		           	}
			              	};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetSpideredEmployersWithGoodMatches(request);

			// Assert	
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void IsOfficeAssigned_WithAssignedOffice_ReturnsFailureWithErrors()
		{
			// Arrange
			var request = new IsOfficeAssignedRequest
			{
				OfficeId = 1
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.IsOfficeAssigned(request);

			// Assert	
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.IsAssigned.ShouldBeTrue();
			response.Errors.Count.ShouldBeGreater(0);
		}

		[Test]
		public void IsOfficeAssigned_WithUnAssignedOffice_ReturnsSuccess()
		{
			// Arrange
			var request = new IsOfficeAssignedRequest
			{
				OfficeId = 6
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.IsOfficeAssigned(request);

			// Assert	
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.IsAssigned.ShouldBeFalse();
			response.Errors.Count.ShouldEqual(0);
		}

		[Test]
		public void DeactivateOffice_WithValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new ActivateDeactivateOfficeRequest
			{
				OfficeId = 9,
				Deactivate = true
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ActivateDeactivateOffice(request);

			// Assert	
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Office.InActive.ShouldBeTrue();
		}

		[Test]
		public void ActivateOffice_WithValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new ActivateDeactivateOfficeRequest
			{
				OfficeId = 9,
				Deactivate = false
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ActivateDeactivateOffice(request);

			// Assert	
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Office.InActive.ShouldBeFalse();
		}

		[Test]
		public void GetEmployerAccountTypes_WithLensPostingIdsWithAccountTypes_ReturnsSuccess()
		{
			// Arrange
			var request = new GetEmployerAccountTypesRequest
			{
				LensPostingIds = new List<string> { "ABC123_66B75E9150974D858E474C1447521319", "ABC123_A840CB1051D44F6497BFAD611EC7D487" }
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetEmployerAccountTypes(request);

			// Assert	
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.EmployerAccountTypes.Count.ShouldEqual(2);
			response.EmployerAccountTypes.Count(x => x.Value != null).ShouldEqual(2);
		}

		[Test]
		public void GetEmployerAccountTypes_WithLensPostingIdsWithAndWithoutAccountTypes_ReturnsSuccess()
		{
			// Arrange
			var request = new GetEmployerAccountTypesRequest
			{
				LensPostingIds = new List<string> { "ABC123_66B75E9150974D858E474C1447521319", "ABC123_A840CB1051D44F6497BFAD611EC7D487", "ABC123_4F12B8E46D7A4E25B4D3860D7E2D388E" }
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetEmployerAccountTypes(request);

			// Assert	
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.EmployerAccountTypes.Count.ShouldEqual(3);
			response.EmployerAccountTypes.Count(x => x.Value.Equals(null)).ShouldEqual(1);
		}

		[Test]
		public void GetEmployerAccountTypes_WithLensPostingIdsWithoutAccountTypes_ReturnsSuccess()
		{
			// Arrange
			var request = new GetEmployerAccountTypesRequest
			{
				LensPostingIds = new List<string> { "ABC123_FCC3D4BAABC44911AE9EC4044A8C9831", "ABC123_F100D7354DF94AF6955DE863B02AA56E", "ABC123_2D2E5EEF256148E6A4F4ECFACD0E4743" }
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetEmployerAccountTypes(request);

			// Assert	
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.EmployerAccountTypes.Count.ShouldEqual(3);
			response.EmployerAccountTypes.Count(x => x.Value.Equals(null)).ShouldEqual(3);
		}

		[Test]
		public void GetEmployerAccountTypes_WithInvalidLensPostingIds_ReturnsSuccess()
		{
			// Arrange
			var request = new GetEmployerAccountTypesRequest
			{
				LensPostingIds = new List<string> { "Invalid lens Id" }
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetEmployerAccountTypes(request);

			// Assert	
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.EmployerAccountTypes.Count.ShouldEqual(0);
		}

		[Test]
		public void GetEmployerAccountTypes_WithNullLensPostingIds_ReturnsSuccess()
		{
			// Arrange
			var request = new GetEmployerAccountTypesRequest();

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetEmployerAccountTypes(request);

			// Assert	
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.EmployerAccountTypes.ShouldBeNull();
		}

		[Test]
		public void GetEmployerAccountTypes_WithEmptyLensPostingIds_ReturnsSuccess()
		{
			// Arrange
			var request = new GetEmployerAccountTypesRequest
			{
				LensPostingIds = new List<string> { "" }
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetEmployerAccountTypes(request);

			// Assert	
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.EmployerAccountTypes.Count.ShouldEqual(0);
		}

		[Test]
		public void GetEmployerAccountTypes_WithDuplicateLensPostingIds_ReturnsSuccess()
		{
			// Arrange
			var request = new GetEmployerAccountTypesRequest
			{
				LensPostingIds = new List<string> { "ABC123_66B75E9150974D858E474C1447521319", "ABC123_66B75E9150974D858E474C1447521319" }
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetEmployerAccountTypes(request);

			// Assert	
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.EmployerAccountTypes.Count.ShouldEqual(1);
		}
			
		#region Get FEIN tests

		[Test]
		public void GetFEIN_WithNoBusinessUnitId_ReturnsFailure()
		{
			// Arrange
			var request = new GetFEINRequest();
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetFEIN(request);

			// Assert
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.FEIN.ShouldBeNullOrEmpty();
		}

		[Test]
		public void GetFEIN_WithInvalidBusinessUnitId_ReturnsFailure()
		{
			// Arrange
			var request = new GetFEINRequest{ BusinessUnitId = 1 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetFEIN(request);

			// Assert
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.FEIN.ShouldBeNullOrEmpty();
		}

		[Test]
		public void GetFEIN_WithInValidBusinessUnitId_ReturnsSuccessAndFEIN()
		{
			// Arrange
			var request = new GetFEINRequest { BusinessUnitId = 5533272 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetFEIN(request);

			// Assert
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.FEIN.ShouldNotBeNullOrEmpty();
			response.FEIN.ShouldEqual("01-0000001");
		}



		#endregion

		#region Validate FEIN Change tests

		[Test]
		public void ValidateFEINChange_WithNoBusinessUnitId_ReturnsFailure()
		{
			// Arrange
			var request = new ValidateFEINChangeRequest { NewFEIN = "11-7777777"};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ValidateFEINChange(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.ValidationResult.ShouldBeNull();
		}

		[Test]
		public void ValidateFEINChange_WithNoNewFEIN_ReturnsFailure()
		{
			// Arrange
			var request = new ValidateFEINChangeRequest { BusinessUnitId = 5533272 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ValidateFEINChange(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.ValidationResult.ShouldBeNull();
		}

		[Test]
		public void ValidateFEINChange_WithInvalidBusinessUnitId_ReturnsFailure()
		{
			// Arrange
			var request = new ValidateFEINChangeRequest { BusinessUnitId = 1, NewFEIN = "11-7777777" };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ValidateFEINChange(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.ValidationResult.ShouldBeNull();
		}

		[Test]
		public void ValidateFEINChange_WithCurrentFEIN_ReturnsSuccessWithValidationError()
		{
			// Arrange
			var request = new ValidateFEINChangeRequest { BusinessUnitId = 5533272, NewFEIN = "01-0000001" };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ValidateFEINChange(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.ValidationResult.ShouldNotBeNull();
			response.ValidationResult.IsValid.ShouldBeFalse();
			response.ValidationResult.ValidationMessage.ShouldEqual(FEINChangeValidationMessages.FEINNotChanged);
			response.ValidationResult.ActionToBeCarriedOut.ShouldEqual(FEINChangeActions.None);
		}

		[Test]
		public void ValidateFEINChange_WithPrimaryBusinessUnitExistingFEINAndNoSiblings_ReturnsSuccessWithActionToBeCarriedOutAndExistingEmployerName()
		{
			// Arrange
			var request = new ValidateFEINChangeRequest { BusinessUnitId = 5536589, NewFEIN = "11-7777777" };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ValidateFEINChange(request);

			// Assert
response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.ValidationResult.ShouldNotBeNull();
			response.ValidationResult.IsValid.ShouldBeTrue();
			response.ValidationResult.ActionToBeCarriedOut.ShouldEqual(FEINChangeActions.PrimaryBusinessUnitExistingFEINNoSiblings);
			response.ValidationResult.NewEmployerName.ShouldNotBeNullOrEmpty();
		}

		[Test]
		public void ValidateFEINChange_WithPrimaryBusinessUnitExistingFEINAndSiblings_ReturnsSuccessWithActionToBeCarriedOutAndExistingEmployerName()
		{
			// Arrange
			var request = new ValidateFEINChangeRequest { BusinessUnitId = 5531278, NewFEIN = "11-7777777" };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ValidateFEINChange(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.ValidationResult.ShouldNotBeNull();
			response.ValidationResult.IsValid.ShouldBeTrue();
			response.ValidationResult.ActionToBeCarriedOut.ShouldEqual(FEINChangeActions.PrimaryBusinessUnitExistingFEINSiblings);
			response.ValidationResult.NewEmployerName.ShouldNotBeNullOrEmpty();
		}

		[Test]
		public void ValidateFEINChange_WithPrimaryBusinessUnitNewFEINAndNoSiblings_ReturnsSuccessWithActionToBeCarriedOut()
		{
			// Arrange
			var request = new ValidateFEINChangeRequest { BusinessUnitId = 5536589, NewFEIN = "11-8777777" };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ValidateFEINChange(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.ValidationResult.ShouldNotBeNull();
			response.ValidationResult.IsValid.ShouldBeTrue();
			response.ValidationResult.ActionToBeCarriedOut.ShouldEqual(FEINChangeActions.PrimaryBusinessUnitNewFEINNoSiblings);
		}

		[Test]
		public void ValidateFEINChange_WithPrimaryBusinessUnitNewFEINAndSiblings_ReturnsSuccessWithActionToBeCarriedOut()
		{
			// Arrange
			var request = new ValidateFEINChangeRequest { BusinessUnitId = 5531278, NewFEIN = "11-8777777" };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ValidateFEINChange(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.ValidationResult.ShouldNotBeNull();
			response.ValidationResult.IsValid.ShouldBeTrue();
			response.ValidationResult.ActionToBeCarriedOut.ShouldEqual(FEINChangeActions.PrimaryBusinessUnitNewFEINSiblings);
		}

		[Test]
		public void ValidateFEINChange_WithNonPrimaryBusinessWithSharedEmployee_ReturnsSuccessWithValidationErrorAndBusinessUnitName()
		{
			// Arrange
			var request = new ValidateFEINChangeRequest { BusinessUnitId = 1104440, NewFEIN = "11-7777777" };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ValidateFEINChange(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.ValidationResult.ShouldNotBeNull();
			response.ValidationResult.IsValid.ShouldBeFalse();
			response.ValidationResult.ValidationMessage.ShouldEqual(FEINChangeValidationMessages.EmployeeAssignedToSiblingBusinessUnit);
			response.ValidationResult.ActionToBeCarriedOut.ShouldEqual(FEINChangeActions.None);
			response.ValidationResult.BusinessUnitName.ShouldNotBeNullOrEmpty();
		}

		[Test]
		public void ValidateFEINChange_WithNonPrimaryBusinessUnitExistingFEIN_ReturnsSuccessWithActionToBeCarriedOutAndExistingEmployerName()
		{
			// Arrange
			var request = new ValidateFEINChangeRequest { BusinessUnitId = 1192846, NewFEIN = "11-7777777" };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ValidateFEINChange(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.ValidationResult.ShouldNotBeNull();
			response.ValidationResult.IsValid.ShouldBeTrue();
			response.ValidationResult.ActionToBeCarriedOut.ShouldEqual(FEINChangeActions.NonPrimaryBusinessUnitExistingFEIN);
			response.ValidationResult.NewEmployerName.ShouldNotBeNullOrEmpty();
		}

		[Test]
		public void ValidateFEINChange_WithNonPrimaryBusinessUnitNewFEIN_ReturnsSuccessWithActionToBeCarriedOut()
		{
			// Arrange
			var request = new ValidateFEINChangeRequest { BusinessUnitId = 1192846, NewFEIN = "11-8777777" };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.ValidateFEINChange(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.ValidationResult.ShouldNotBeNull();
			response.ValidationResult.IsValid.ShouldBeTrue();
			response.ValidationResult.ActionToBeCarriedOut.ShouldEqual(FEINChangeActions.NonPrimaryBusinessUnitNewFEIN);
		}

		#endregion

		#region Save FEIN Change tests

		[Test]
		public void SaveFEINChange_WithNoBusinessUnitId_ReturnsFailure()
		{
			// Arrange
			var request = new SaveFEINChangeRequest { NewFEIN = "11-7777777" };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.SaveFEINChange(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.ActionCarriedOut.ShouldEqual(FEINChangeActions.None);
		}

		[Test]
		public void SaveFEINChange_WithNoNewFEIN_ReturnsFailure()
		{
			// Arrange
			var request = new SaveFEINChangeRequest { BusinessUnitId = 5533272 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.SaveFEINChange(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.ActionCarriedOut.ShouldEqual(FEINChangeActions.None);
		}

		[Test]
		public void SaveFEINChange_WithInvalidBusinessUnitId_ReturnsFailure()
		{
			// Arrange
			var request = new SaveFEINChangeRequest { BusinessUnitId = 1, NewFEIN = "11-7777777" };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.SaveFEINChange(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
			response.ActionCarriedOut.ShouldEqual(FEINChangeActions.None);
		}

		[Test]
		public void SaveFEINChange_WithCurrentFEIN_ReturnsSuccessWithValidationError()
		{
			// Arrange
			var request = new SaveFEINChangeRequest { BusinessUnitId = 5533272, NewFEIN = "01-0000001" };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.SaveFEINChange(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.ActionCarriedOut.ShouldEqual(FEINChangeActions.None);
		}

		[Test]
		public void SaveFEINChange_WithPrimaryBusinessUnitExistingFEINAndNoSiblings_ReturnsSuccessWithActionCarriedOut()
		{
			// Arrange
			var request = new SaveFEINChangeRequest { BusinessUnitId = 5536589, NewFEIN = "11-7777777" };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.SaveFEINChange(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.ActionCarriedOut.ShouldEqual(FEINChangeActions.PrimaryBusinessUnitExistingFEINNoSiblings);
		}

		[Test]
		public void SaveFEINChange_WithPrimaryBusinessUnitExistingFEINAndSiblings_ReturnsSuccessWithActionCarriedOut()
		{
			// Arrange
			var request = new SaveFEINChangeRequest { BusinessUnitId = 5531278, NewFEIN = "11-7777777" };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.SaveFEINChange(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.ActionCarriedOut.ShouldEqual(FEINChangeActions.PrimaryBusinessUnitExistingFEINSiblings);
		}

		[Test]
		public void SaveFEINChange_WithPrimaryBusinessUnitNewFEINAndNoSiblings_ReturnsSuccessWithActionCarriedOut()
		{
			// Arrange
			var request = new SaveFEINChangeRequest { BusinessUnitId = 5536589, NewFEIN = "11-8777777" };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.SaveFEINChange(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.ActionCarriedOut.ShouldEqual(FEINChangeActions.PrimaryBusinessUnitNewFEINNoSiblings);
		}

		[Test]
		public void SaveFEINChange_WithPrimaryBusinessUnitNewFEINAndSiblings_ReturnsSuccessWithActionCarriedOut()
		{
			// Arrange
			var request = new SaveFEINChangeRequest { BusinessUnitId = 5531278, NewFEIN = "11-8777777" };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.SaveFEINChange(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.ActionCarriedOut.ShouldEqual(FEINChangeActions.PrimaryBusinessUnitNewFEINSiblings);
		}

		[Test]
		public void SaveFEINChange_WithNonPrimaryBusinessWithSharedEmployee_ReturnsSuccessWithActionCarriedOut()
		{
			// Arrange
			var request = new SaveFEINChangeRequest { BusinessUnitId = 1104440, NewFEIN = "11-7777777" };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.SaveFEINChange(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.ActionCarriedOut.ShouldEqual(FEINChangeActions.None);
		}

		[Test]
		public void SaveFEINChange_WithNonPrimaryBusinessUnitExistingFEIN_ReturnsSuccessWithActionCarriedOut()
		{
			// Arrange
			var request = new SaveFEINChangeRequest { BusinessUnitId = 1192846, NewFEIN = "11-7777777" };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.SaveFEINChange(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.ActionCarriedOut.ShouldEqual(FEINChangeActions.NonPrimaryBusinessUnitExistingFEIN);
		}

		[Test]
		public void SaveFEINChange_WithNonPrimaryBusinessUnitNewFEIN_ReturnsSuccessWithActionToBeCarriedOut()
		{
			// Arrange
			var request = new SaveFEINChangeRequest { BusinessUnitId = 1192846, NewFEIN = "11-8777777" };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.SaveFEINChange(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.ActionCarriedOut.ShouldEqual(FEINChangeActions.NonPrimaryBusinessUnitNewFEIN);
		}

		#endregion

		#region Update primary business unit tests

		[Test]
		public void UpdatePrimaryBusinessUnit_WithNoCurrentBusinessUnitId_ReturnsFailure()
		{
			// Arrange
			var request = new UpdatePrimaryBusinessUnitRequest() { CurrentPrimaryBusinessUnitId = null, NewPrimaryBusinessUnitId = 5533272 };
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.UpdatePrimaryBusinessUnit(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}

		[Test]
		public void UpdatePrimaryBusinessUnit_WithNoNewBusinessUnitId_ReturnsFailure()
		{
			// Arrange
			var request = new UpdatePrimaryBusinessUnitRequest() { CurrentPrimaryBusinessUnitId = 1546755, NewPrimaryBusinessUnitId = null};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.UpdatePrimaryBusinessUnit(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}

		[Test]
		public void UpdatePrimaryBusinessUnit_CurrentBusinessUnitNotPrimary_ReturnsFailure()
		{
			// Arrange
			var request = new UpdatePrimaryBusinessUnitRequest() { CurrentPrimaryBusinessUnitId = 5533272, NewPrimaryBusinessUnitId = 5533402};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.UpdatePrimaryBusinessUnit(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}

		[Test]
		public void UpdatePrimaryBusinessUnit_ValidBusinessUnitsProvidedAndUpdated_ReturnsSuccess()
		{
			// Arrange
			var request = new UpdatePrimaryBusinessUnitRequest() { CurrentPrimaryBusinessUnitId = 1546755, NewPrimaryBusinessUnitId = 5533272};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.UpdatePrimaryBusinessUnit(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}

		[Test]
		public void UpdatePrimaryBusinessUnits_ValidBusinessUnitsProvided_CheckEmployerIDsDoNotMatch_ReturnsFailure()
		{
			// Arrange
			var request = new UpdatePrimaryBusinessUnitRequest() { CurrentPrimaryBusinessUnitId = 1546755, NewPrimaryBusinessUnitId = 43630};
			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.UpdatePrimaryBusinessUnit(request);
			
			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Failure);
		}

		#endregion

		#region Get Hiring Manager Office Email tests

		[Test]
		public void GetHiringManagerOfficeEmail_WithValidPersonId_ReturnsSuccessAndEmail()
		{
			// Arrange
			var request = new GetHiringManagerOfficeEmailRequest {PersonId = 1093026};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetHiringManagerOfficeEmail(request);

			// Assert	
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Email.ShouldEqual("test@bgt.com");
		}

		[Test]
		public void GetHiringManagerOfficeEmail_WithInvalidPersonId_ReturnsSuccessAndNullEmail()
		{
			// Arrange
			var request = new GetHiringManagerOfficeEmailRequest { PersonId = 964514684164 };

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetHiringManagerOfficeEmail(request);

			// Assert	
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Email.ShouldBeNull();
		}

		[Test]
		public void GetHiringManagerOfficeEmail_WithNullPersonId_ReturnsSuccessAndNullEmail()
		{
			// Arrange
			var request = new GetHiringManagerOfficeEmailRequest {};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.GetHiringManagerOfficeEmail(request);

			// Assert	
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
			response.Email.ShouldBeNull();
		}

		#endregion

		[Test]
		public void CreateBusinessUnitHomepageAlert_WhenPassedValidRequest_ReturnsSuccess()
		{
			// Arrange
			var request = new CreateBusinessUnitHomepageAlertRequest
			{
				HomepageAlerts = new List<BusinessUnitHomepageAlertView>
				{
					new BusinessUnitHomepageAlertView
					{
						Message = "Unit test message",
						ExpiryDate = DateTime.Now.AddDays(7),
						IsSystemAlert = true
					}
				}
					
			};

			request.Prepare(MockClientTag, MockSessionId, MockUserData, MockCulture);

			// Act
			var response = _service.CreateBusinessUnitHomepageAlert(request);

			// Assert
			response.ShouldNotBeNull();
			response.Acknowledgement.ShouldEqual(AcknowledgementType.Success);
		}
	}
}
