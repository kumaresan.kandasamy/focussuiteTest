﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using Focus.Data.Repositories.Contracts;
using Focus.MigrationServices.ServiceContracts;
using Focus.Services.ServiceContracts;
using Focus.Services.ServiceImplementations;

#endregion

namespace Focus.MigrationServices.ServiceImplementations
{
  public class ImportCoreService : ServiceBase, IImportCoreService
  {
    protected IDataImportService DataImportService { get { return new DataImportService(); } }

    /// <summary>
    /// Initializes a new instance of the <see cref="CoreService"/> class.
    /// </summary>
    public ImportCoreService()
    { }

  	/// <summary>
  	/// Initializes a new instance of the <see cref="CoreService"/> class.
  	/// </summary>
    /// <param name="coreRepository"></param>
  	/// <param name="configRepository"></param>
    /// <param name="libraryRepository"></param>
  	/// <param name="migrationRepository">The repository.</param>
    public ImportCoreService(ICoreRepository coreRepository, IConfigurationRepository configRepository, ILibraryRepository libraryRepository, IMigrationRepository migrationRepository)
      : base(coreRepository, configRepository, libraryRepository, migrationRepository)
    { }
  }
}
