﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Linq;

using Focus.Core;
using Focus.Data.Migration.Entities;
using Focus.Data.Repositories.Contracts;
using Focus.MigrationServices.Messages;
using Focus.MigrationServices.Providers.Common;
using Focus.MigrationServices.ServiceContracts;
using Focus.Services;

using Framework.Core;
using Framework.Logging;

using Mindscape.LightSpeed;
using Mindscape.LightSpeed.Querying;

#endregion

namespace Focus.MigrationServices.ServiceImplementations
{
  public class BatchService : BatchCoreService, IBatchService
  {
    public BatchService()
    {

    }

    public BatchService(IRuntimeContext runtimeContext)
    {
      RuntimeContext = runtimeContext;
    }

    public BatchService(IRuntimeContext runtimeContext, ICoreRepository coreRepository, IMigrationRepository migrationRepository, ILibraryRepository libraryRepository, IConfigurationRepository configRepository)
      : base(coreRepository, configRepository, libraryRepository, migrationRepository)
    {
      RuntimeContext = runtimeContext;
    }

    /// <summary>
    /// Sets the batch number
    /// </summary>
    /// <param name="request">The object requesting the migration.</param>
    /// <returns>A response from the migration.</returns>
    public BatchResponse Batch(BatchRequest request)
    {
      var response = new BatchResponse(request);
			
				// Validate request
			if (!ValidateRequest(request, response, Validate.All))
				return response;

      try
      {
        IQueryable<long> idQuery = null;

        switch (request.RecordType)
        {
          case RecordType.Employer:
            idQuery = GetEmployerMigrationIds(request.RecordsToImport, MigrationEmployerRecordType.Employee);
            ClearBatch<EmployerRequest>((int)MigrationEmployerRecordType.Employee, MigrationEntityType.Employer);
            break;

					case RecordType.EmployerTradeName:
						idQuery = GetEmployerMigrationIds(request.RecordsToImport, MigrationEmployerRecordType.TradeName);
            ClearBatch<EmployerRequest>((int)MigrationEmployerRecordType.TradeName, MigrationEntityType.Employer);
						break;

          case RecordType.Job:
            idQuery = GetJobIds(request.RecordsToImport);
            ClearBatch<JobOrderRequest>((int)MigrationJobOrderRecordType.Job, MigrationEntityType.JobOrder);
            break;

					case RecordType.SpideredJobs:
						idQuery = GetSpideredJobIds(request.RecordsToImport);
						ClearBatch<JobOrderRequest>((int)MigrationJobOrderRecordType.SpideredJob, MigrationEntityType.JobOrder);
						break;
						
					case RecordType.SpideredJobReferrals:
						idQuery = GetSpideredJobReferralIds(request.RecordsToImport);
						ClearBatch<JobOrderRequest>((int)MigrationJobOrderRecordType.SpideredJobReferral, MigrationEntityType.JobOrder);
						break;

					case RecordType.SavedJob:
						idQuery = GetSavedJobIds(request.RecordsToImport);
						ClearBatch<JobOrderRequest>((int)MigrationJobOrderRecordType.SavedJob, MigrationEntityType.JobOrder);
						break;

          case RecordType.JobSeeker:
            idQuery = GetJobSeekerIds(request.RecordsToImport);
            ClearBatch<JobSeekerRequest>((int)MigrationJobSeekerRecordType.JobSeeker, MigrationEntityType.JobSeeker);
            break;

          case RecordType.Resume:
            idQuery = GetResumeIds(request.RecordsToImport);
            ClearBatch<JobSeekerRequest>((int)MigrationJobSeekerRecordType.Resume, MigrationEntityType.JobSeeker);
            break;

          case RecordType.SavedSearch:
            idQuery = GetJobSeekerMigrationIds(request.RecordsToImport, MigrationJobSeekerRecordType.SavedSearch);
            ClearBatch<JobSeekerRequest>((int)MigrationJobSeekerRecordType.SavedSearch, MigrationEntityType.JobSeeker);
            break;

          case RecordType.SearchAlert:
            idQuery = GetJobSeekerMigrationIds(request.RecordsToImport, MigrationJobSeekerRecordType.SearchAlert);
            ClearBatch<JobSeekerRequest>((int)MigrationJobSeekerRecordType.SearchAlert, MigrationEntityType.JobSeeker);
            break;

          case RecordType.ResumeSearch:
            idQuery = GetEmployerMigrationIds(request.RecordsToImport, MigrationEmployerRecordType.ResumeSearch);
            ClearBatch<EmployerRequest>((int)MigrationEmployerRecordType.ResumeSearch, MigrationEntityType.Employer);
            break;

          case RecordType.ResumeAlert:
            idQuery = GetEmployerMigrationIds(request.RecordsToImport, MigrationEmployerRecordType.ResumeAlert);
            ClearBatch<EmployerRequest>((int)MigrationEmployerRecordType.ResumeAlert, MigrationEntityType.Employer);
            break;

          case RecordType.Referral:
            idQuery = GetJobSeekerMigrationIds(request.RecordsToImport, MigrationJobSeekerRecordType.Referral);
            ClearBatch<JobSeekerRequest>((int)MigrationJobSeekerRecordType.Referral, MigrationEntityType.JobSeeker);
            break;

          case RecordType.Activity:
            idQuery = GetJobSeekerMigrationIds(request.RecordsToImport, MigrationJobSeekerRecordType.Activity);
            ClearBatch<JobSeekerRequest>((int)MigrationJobSeekerRecordType.Activity, MigrationEntityType.JobSeeker);
            break;

					case RecordType.Notes:
						idQuery = GetAssistUserMigrationIds(request.RecordsToImport, MigrationAssistUserRecordType.NotesAndReminders);
						ClearBatch<UserRequest>((int)MigrationAssistUserRecordType.NotesAndReminders, MigrationEntityType.User);
						break;

					case RecordType.ViewedPosting:
						idQuery = GetJobSeekerMigrationIds(request.RecordsToImport, MigrationJobSeekerRecordType.ViewedPosting);
						ClearBatch<JobSeekerRequest>((int)MigrationJobSeekerRecordType.ViewedPosting, MigrationEntityType.JobSeeker);
						break;
        }

        if (idQuery.IsNull())
        {
          response.SetFailure("Unsupported record type");
          return response;
        }

        var listToSplit = idQuery.ToList();
        
        var listSize = listToSplit.Count();
        if (listSize == 0)
        {
          response.SetFailure("No records left to batch");
          return response;
        }

        var batchSize = request.NumberOfBatches;
        if (batchSize < 1)
          batchSize = 1;
        
        // Integer division rounds down so some batches need more
        var recordsPerBatch = new int[batchSize]; //+ ((listSize % batchSize != 0) ? 1 : 0);
        var basicBatchCount = listSize / batchSize;
        var batchesToAdjust = listSize % batchSize;
        for (var index = 1; index <= batchSize; index++)
        {
          recordsPerBatch[index - 1] = basicBatchCount + ((index <= batchesToAdjust) ? 1 : 0);
        }
        
        var batchNumber = 1;
        var batchIndex = 0;
        while (batchIndex < listSize)
        {
          var lastIndex = batchIndex + recordsPerBatch[batchNumber - 1] - 1;
          if (lastIndex >= listSize)
            lastIndex = listSize - 1;

          var firstId = listToSplit[batchIndex];
          var lastId = listToSplit[lastIndex];

          switch (request.RecordType)
          {
            case RecordType.Employer:
              UpdateBatch<EmployerRequest>(firstId, lastId, (int)MigrationEmployerRecordType.Employee, batchNumber, MigrationEntityType.Employer);
							break;
						case RecordType.EmployerTradeName:
              UpdateBatch<EmployerRequest>(firstId, lastId, (int)MigrationEmployerRecordType.TradeName, batchNumber, MigrationEntityType.Employer);
							break;
            case RecordType.Job:
              UpdateBatch<JobOrderRequest>(firstId, lastId, (int)MigrationJobOrderRecordType.Job, batchNumber, MigrationEntityType.JobOrder);
							break;
						case RecordType.SpideredJobs:
							UpdateBatch<JobOrderRequest>(firstId, lastId, (int)MigrationJobOrderRecordType.SpideredJob, batchNumber, MigrationEntityType.JobOrder);
							break;
						case RecordType.SpideredJobReferrals:
							UpdateBatch<JobOrderRequest>(firstId, lastId, (int)MigrationJobOrderRecordType.SpideredJobReferral, batchNumber, MigrationEntityType.JobOrder);
							break;
						case RecordType.SavedJob:
							UpdateBatch<JobOrderRequest>(firstId, lastId, (int)MigrationJobOrderRecordType.SavedJob, batchNumber, MigrationEntityType.JobOrder);
							break;
            case RecordType.JobSeeker:
              UpdateBatch<JobSeekerRequest>(firstId, lastId, (int)MigrationJobSeekerRecordType.JobSeeker, batchNumber, MigrationEntityType.JobSeeker);
              break;
            case RecordType.Resume:
              UpdateBatch<JobSeekerRequest>(firstId, lastId, (int)MigrationJobSeekerRecordType.Resume, batchNumber, MigrationEntityType.JobSeeker);
              break;
            case RecordType.SavedSearch:
              UpdateBatch<JobSeekerRequest>(firstId, lastId, (int)MigrationJobSeekerRecordType.SavedSearch, batchNumber, MigrationEntityType.JobSeeker);
              break;
            case RecordType.SearchAlert:
              UpdateBatch<JobSeekerRequest>(firstId, lastId, (int)MigrationJobSeekerRecordType.SearchAlert, batchNumber, MigrationEntityType.JobSeeker);
              break;
            case RecordType.ResumeSearch:
              UpdateBatch<EmployerRequest>(firstId, lastId, (int)MigrationEmployerRecordType.ResumeSearch, batchNumber, MigrationEntityType.Employer);
              break;
            case RecordType.ResumeAlert:
              UpdateBatch<EmployerRequest>(firstId, lastId, (int)MigrationEmployerRecordType.ResumeAlert, batchNumber, MigrationEntityType.Employer);
              break;
            case RecordType.Referral:
              UpdateBatch<JobSeekerRequest>(firstId, lastId, (int)MigrationJobSeekerRecordType.Referral, batchNumber, MigrationEntityType.JobSeeker);
              break;
            case RecordType.Activity:
              UpdateBatch<JobSeekerRequest>(firstId, lastId, (int)MigrationJobSeekerRecordType.Activity, batchNumber, MigrationEntityType.JobSeeker);
							break;
						case RecordType.Notes:
							UpdateBatch<UserRequest>(firstId, lastId, (int)MigrationAssistUserRecordType.NotesAndReminders, batchNumber, MigrationEntityType.User);
							break;
          }

          batchIndex += recordsPerBatch[batchNumber - 1];
          batchNumber++;
        }

        response.RecordsPerBatch = recordsPerBatch;
      }
      catch (Exception ex)
      {
        response.SetFailure(string.Format("{0} - {1}", FormatErrorMessage(request, ErrorTypes.Unknown), ex.Message), ex);
        Logger.Error(request.SessionId, request.RequestId, request.UserContext.UserId, "Error in Batch", ex);
      }

      return response;
    }

    /// <summary>
    /// Gets the Ids of all unprocessed employer records
    /// </summary>
    /// <param name="recordsToImport">The number of records to import</param>
    /// <param name="recordType">The type of migration records to import</param>
    /// <returns>A query to get the Ids for all unprocessed employer records</returns>
    private IQueryable<long> GetEmployerMigrationIds(int recordsToImport, MigrationEmployerRecordType recordType)
    {
      return MigrationRepository.EmployerRequests
                                .Where(er => er.RecordType == recordType && er.Status == MigrationStatus.ToBeProcessed)
                                .OrderBy(er => er.Id)
                                .Take(recordsToImport)
                                .Select(er => er.Id);
    }

    /// <summary>
    /// Gets the Ids of all unprocessed job order records
    /// </summary>
    /// <param name="recordsToImport">The number of records to import</param>
    /// <returns>A query to get the Ids for all unprocessed job order records</returns>
    private IQueryable<long> GetJobIds(int recordsToImport)
    {
      return MigrationRepository.JobOrderRequests
                                .Where(j => j.RecordType == MigrationJobOrderRecordType.Job && j.Status == MigrationStatus.ToBeProcessed)
                                .OrderBy(j => j.Id)
                                .Take(recordsToImport)
                                .Select(j => j.Id);
    }

		/// <summary>
		/// Gets the Ids of all unprocessed job order records
		/// </summary>
		/// <param name="recordsToImport">The number of records to import</param>
		/// <returns>A query to get the Ids for all unprocessed job order records</returns>
		private IQueryable<long> GetSpideredJobIds(int recordsToImport)
		{
			return MigrationRepository.JobOrderRequests
																.Where(j => j.RecordType == MigrationJobOrderRecordType.SpideredJob && j.Status == MigrationStatus.ToBeProcessed)
																.OrderBy(j => j.Id)
																.Take(recordsToImport)
																.Select(j => j.Id);
		}

		/// <summary>
		/// Gets the Ids of all unprocessed job order records
		/// </summary>
		/// <param name="recordsToImport">The number of records to import</param>
		/// <returns>A query to get the Ids for all unprocessed job order records</returns>
		private IQueryable<long> GetSpideredJobReferralIds(int recordsToImport)
		{
			return MigrationRepository.JobOrderRequests
																.Where(j => j.RecordType == MigrationJobOrderRecordType.SpideredJobReferral && j.Status == MigrationStatus.ToBeProcessed)
																.OrderBy(j => j.Id)
																.Take(recordsToImport)
																.Select(j => j.Id);
		}

		/// <summary>
		/// Gets the Ids of all unprocessed job order records
		/// </summary>
		/// <param name="recordsToImport">The number of records to import</param>
		/// <returns>A query to get the Ids for all unprocessed job order records</returns>
		private IQueryable<long> GetSavedJobIds(int recordsToImport)
		{
			return MigrationRepository.JobOrderRequests
																.Where(j => j.RecordType == MigrationJobOrderRecordType.SavedJob && j.Status == MigrationStatus.ToBeProcessed)
																.OrderBy(j => j.Id)
																.Take(recordsToImport)
																.Select(j => j.Id);
		}

    /// <summary>
    /// Gets the Ids of all unprocessed job seeker records
    /// </summary>
    /// <param name="recordsToImport">The number of records to import</param>
    /// <returns>A query to get the Ids for all unprocessed job seeker records</returns>
    private IQueryable<long> GetJobSeekerIds(int recordsToImport)
    {
      return MigrationRepository.JobSeekerRequests
                                .Where(j => j.RecordType == MigrationJobSeekerRecordType.JobSeeker && j.Status == MigrationStatus.ToBeProcessed)
                                .OrderBy(j => j.Id)
                                .Take(recordsToImport)
                                .Select(j => j.Id);
    }

    /// <summary>
    /// Gets the Ids of all unprocessed resume records
    /// </summary>
    /// <param name="recordsToImport">The number of records to import</param>
    /// <returns>A query to get the Ids for all unprocessed resume records</returns>
    private IQueryable<long> GetResumeIds(int recordsToImport)
    {
      return MigrationRepository.JobSeekerRequests
                                .Where(j => j.RecordType == MigrationJobSeekerRecordType.Resume && j.Status == MigrationStatus.ToBeProcessed)
                                .OrderBy(j => j.Id)
                                .Take(recordsToImport)
                                .Select(j => j.Id);
    }

    /// <summary>
    /// Gets the Ids of all unprocessed migration records
    /// </summary>
    /// <param name="recordsToImport">The number of records to import</param>
    /// <param name="recordType">The type of migration records to import</param>
    /// <returns>A query to get the Ids for all unprocessed migration records</returns>
    private IQueryable<long> GetJobSeekerMigrationIds(int recordsToImport, MigrationJobSeekerRecordType recordType)
    {
      return MigrationRepository.JobSeekerRequests
                                .Where(j => j.RecordType == recordType && j.Status == MigrationStatus.ToBeProcessed)
                                .OrderBy(j => j.Id)
                                .Take(recordsToImport)
                                .Select(j => j.Id);
    }

		/// <summary>
		/// Gets the Ids of all unprocessed migration records
		/// </summary>
		/// <param name="recordsToImport">The number of records to import</param>
		/// <param name="recordType">The type of migration records to import</param>
		/// <returns>A query to get the Ids for all unprocessed migration records</returns>
		private IQueryable<long> GetAssistUserMigrationIds(int recordsToImport, MigrationAssistUserRecordType recordType)
		{
			return MigrationRepository.UserRequests
																.Where(u => u.RecordType == recordType && u.Status == MigrationStatus.ToBeProcessed)
																.OrderBy(u => u.Id)
																.Take(recordsToImport)
																.Select(u => u.Id);
		}

    /// <summary>
    /// Clears down all batch numbers.
    /// </summary>
    /// <typeparam name="T">The type of record to update</typeparam>
    /// <param name="recordType">The type of record to update.</param>
    /// <param name="entityType">The type of entity to update.</param>
    private void ClearBatch<T>(int recordType, MigrationEntityType entityType)
    {
      var dbEntityType = (int) entityType;
      var resetQuery = new Query(typeof(BatchRecord), Entity.Attribute("RecordType") == recordType && Entity.Attribute("EntityType") == dbEntityType);
      MigrationRepository.Remove(resetQuery);
      MigrationRepository.SaveChanges(true);

      /*
      const string sqlTemplate = @"
UPDATE TOP (5000)
  [{0}]
SET
  [BatchNumber] = NULL
WHERE
  [RecordType] = {1} AND [Status] = {2} AND [BatchNumber] IS NOT NULL
  
SELECT @@ROWCOUNT As [RowCount]";

      var tableName = string.Format("Data.Migration.{0}", typeof(T).Name);

      var rowCount = -1;
      while (rowCount != 0)
      {
        var sql = string.Format(sqlTemplate, tableName, recordType, (int)MigrationStatus.ToBeProcessed);
        rowCount = MigrationRepository.ExecuteScalar<int>(sql);
        MigrationRepository.SaveChanges(true);
      }
      */

      /*
      var resetQuery = new Query(typeof(T), Entity.Attribute("RecordType") == recordType && Entity.Attribute("BatchNumber") != null);
      MigrationRepository.Update(resetQuery, new { BatchNumber = (long?)null });
      MigrationRepository.SaveChanges(true);
      */
    }

    /// <summary>
    /// Updates all the records in a batch to have the same batch number
    /// </summary>
    /// <typeparam name="T">The type of record to update</typeparam>
    /// <param name="firstId">The id of the first record to update</param>
    /// <param name="lastId">The id of the last record to update</param>
    /// <param name="recordType">The type of record to update.</param>
    /// <param name="batchNumber">The batch number.</param>
    /// <param name="entityType">The type of entity to update.</param>
    private void UpdateBatch<T>(long firstId, long lastId, int recordType, int batchNumber, MigrationEntityType entityType)
    {
      var batch = new BatchRecord
      {
        EntityType = entityType,
        RecordType = recordType,
        BatchNumber = batchNumber,
        FirstId = firstId,
        LastId = lastId
      };

      MigrationRepository.Add(batch);
      MigrationRepository.SaveChanges(true);

      /*
      var batchQuery = new Query(typeof(T),
                                 Entity.Attribute("Id") >= firstId
                                 && Entity.Attribute("Id") <= lastId
                                 && Entity.Attribute("RecordType") == recordType
                                 && Entity.Attribute("Status") == (int)MigrationStatus.ToBeProcessed
                                 && Entity.Attribute("BatchNumber") == null
                                 );
      MigrationRepository.Update(batchQuery, new { BatchNumber = batchNumber });
      MigrationRepository.SaveChanges(true);
      */
    }
  }
}