﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;

using Focus.Core;
using Focus.Core.Views;
using Focus.Data.Repositories.Contracts;
using Focus.MigrationServices.Messages;
using Focus.Services;
using Focus.Services.Core;

using Framework.Caching;
using Framework.ServiceLocation;

#endregion 

namespace Focus.MigrationServices.ServiceImplementations
{

	public class ServiceBase 
	{
    protected IMigrationRepository MigrationRepository;
    protected IConfigurationRepository ConfigRepository;
    protected ILibraryRepository LibraryRepository;
    protected ICoreRepository CoreRepository;
	  protected IRuntimeContext RuntimeContext;

    protected static readonly object SyncObject = new object();

		/// <summary>
		/// Initializes a new instance of the <see cref="ServiceBase"/> class.
		/// </summary>
    public ServiceBase()
		{
      CoreRepository = ServiceLocator.Current.Resolve<ICoreRepository>();
      ConfigRepository = ServiceLocator.Current.Resolve<IConfigurationRepository>();
      MigrationRepository = ServiceLocator.Current.Resolve<IMigrationRepository>();
		  LibraryRepository = ServiceLocator.Current.Resolve<ILibraryRepository>();
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ServiceBase"/> class.
		/// </summary>
    /// <param name="coreRepository"></param>
    /// <param name="configRepository"></param>
    /// <param name="libraryRepository"></param>
    /// <param name="migrationRepository">The repository.</param>
    public ServiceBase(ICoreRepository coreRepository, IConfigurationRepository configRepository, ILibraryRepository libraryRepository, IMigrationRepository migrationRepository)
		{
		  CoreRepository = coreRepository;
      ConfigRepository = configRepository;
		  LibraryRepository = libraryRepository;
			MigrationRepository = migrationRepository;
		}

		/// <summary>
		/// Formats an error message.
		/// </summary>
		/// <param name="request">The request.</param>
		/// <param name="error">The error.</param>
		/// <param name="args">The args.</param>
		/// <returns></returns>
		public string FormatErrorMessage(ServiceRequest request, ErrorTypes error, params object[] args)
		{
			var s = "";
			if (args != null)
				foreach (var arg in args)
				{
					if (s.Length > 0) s += ", ";
					s += arg;
				}

			return "ErrorType." + error + (s.Length > 0 ? " - Arguments : " + s : "");
		}

		/// <summary>
		/// Validate 2 security levels for a request: ClientTag
		/// </summary>
		/// <param name="request">The request message.</param>
		/// <param name="response">The response message.</param>
		/// <param name="validate">The validation that needs to take place.</param>
		/// <returns></returns>
		protected bool ValidateRequest(ServiceRequest request, ServiceResponse response, Validate validate)
		{
      RuntimeContext.SetRequest(request);

			// Validate client tag. 
			if ((Validate.ClientTag & validate) == Validate.ClientTag)
			{
				if (!AppSettings.ValidClientTags.Contains(request.ClientTag))
				{
					response.Acknowledgement = AcknowledgementType.Failure;
					response.Message = FormatErrorMessage(request, ErrorTypes.UnknownClientTag);
					return false;
				}
			}

			return true;
		}

		#region Application Settings

		/// <summary>
		/// Gets the application settings.
		/// </summary>
		/// <returns></returns>
		internal AppSettings AppSettings
		{
			get
			{
				var configurationItems = Cacher.Get<List<ConfigurationItemView>>(Constants.CacheKeys.Configuration);

				if (configurationItems == null)
				{
					lock (SyncObject)
					{
						var keySet = (from ci in ConfigRepository.ConfigurationItems
													orderby ci.Key
													select ci).ToList();

						configurationItems = (from ci in keySet
																	select new ConfigurationItemView { Key = ci.Key, Value = ci.Value }
																 ).ToList();

						Cacher.Set(Constants.CacheKeys.Configuration, configurationItems);
					}
				}

				return new AppSettings(configurationItems);
			}
		}

		/// <summary>
		/// Refreshes the app settings.
		/// </summary>
		internal void RefreshAppSettings()
		{
			lock (SyncObject)
			{
        var keySet = (from ci in ConfigRepository.ConfigurationItems
											orderby ci.Key
											select ci).ToList();

				var configurationItems = (from ci in keySet
																	select new ConfigurationItemView { Key = ci.Key, Value = ci.Value }
														 ).ToList();
				
				Cacher.Set(Constants.CacheKeys.Configuration, configurationItems);
			}
		}

		#endregion


		#region Enums

		/// <summary>
		/// Validation options enum. Used in validation of messages.
		/// </summary>
		[Flags]
		protected enum Validate
		{
			License = 0x0001,
			ClientTag = 0x0002,
			SessionId = 0x0004,
			UserCredentials = 0x0008,
			Core = ClientTag | SessionId | UserCredentials,
			Explorer = License | ClientTag | SessionId,
			All = License | ClientTag | SessionId | UserCredentials
		}

		#endregion
	}
}
