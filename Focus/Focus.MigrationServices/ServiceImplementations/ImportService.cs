﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

using Focus.Core;
using Focus.Core.Messages.DataImportService;
using Focus.Data.Repositories.Contracts;
using Focus.MigrationServices.Messages;
using Focus.MigrationServices.Providers.Common;
using Focus.MigrationServices.ServiceContracts;
using Focus.Services;
using JetBrains.Annotations;
using AcknowledgementType = Focus.Core.Messages.AcknowledgementType;
using FocusRequest = Focus.Core.Messages.ServiceRequest;
using FocusResponse = Focus.Core.Messages.ServiceResponse;

using Framework.Core;
using Framework.Logging;

#endregion

namespace Focus.MigrationServices.ServiceImplementations 
{
  public class ImportService : ImportCoreService, IImportService
  {
    private int _batchsize;
    private int _recordsToImport;
    private int _totalRecordsRead;

    public ImportService()
  	{
			_batchsize = AppSettings.ImportBatchSize;
  	}

    public ImportService(IRuntimeContext runtimeContext)
			: base(runtimeContext.Repositories.Core, runtimeContext.Repositories.Configuration, runtimeContext.Repositories.Library, runtimeContext.Repositories.Migration)
		{
      RuntimeContext = runtimeContext;

      _batchsize = AppSettings.ImportBatchSize;
    }

    public ImportService(IRuntimeContext runtimeContext, ICoreRepository coreRepository, IMigrationRepository migrationRepository, ILibraryRepository libraryRepository, IConfigurationRepository configRepository)
      : base(coreRepository, configRepository, libraryRepository, migrationRepository)
    {
      RuntimeContext = runtimeContext;

      _batchsize = AppSettings.ImportBatchSize;
  	}

    /// <summary>
    /// Imports all the requests from the migration database into the main focus database.
    /// </summary>
    /// <param name="request">The import request object.</param>
    /// <returns>An import response object</returns>
    public ImportResponse Import(ImportRequest request)
    {
      var response = new ImportResponse(request);

      // Validate request
      if (!ValidateRequest(request, response, Validate.All))
        return response;

      try
      {
        var jobIds = new List<long>();

				if (request.BatchSize.HasValue && request.BatchSize > 0)
          _batchsize = request.BatchSize.Value;

        if (request.RecordsToImport.HasValue && request.RecordsToImport > 0)
          _recordsToImport = request.RecordsToImport.Value;
        else
          _recordsToImport = _batchsize;

        switch (request.RecordType)
        {
          case RecordType.Employer:
            ImportEmployerRequests(MigrationEmployerRecordType.Employee, request, response);
            ImportEmployerRequests(MigrationEmployerRecordType.BusinessUnitDescription, request, response);
            ImportEmployerRequests(MigrationEmployerRecordType.BusinessUnitLogo, request, response);
						break;
					case RecordType.EmployerTradeName:
						ImportEmployerTradeNameRequests(MigrationEmployerRecordType.TradeName, request, response);
						break;
          case RecordType.ResumeSearch:
            ImportEmployerRequests(MigrationEmployerRecordType.ResumeSearch, request, response);
            break;
          case RecordType.ResumeAlert:
            ImportEmployerRequests(MigrationEmployerRecordType.ResumeAlert, request, response);
            break;
          case RecordType.Job:
            ImportJobOrderRequests(MigrationJobOrderRecordType.Job, request, response, null);
            ImportJobOrderRequests(MigrationJobOrderRecordType.Location, request, response, jobIds);
            ImportJobOrderRequests(MigrationJobOrderRecordType.Address, request, response, null);
            ImportJobOrderRequests(MigrationJobOrderRecordType.Certificate, request, response, jobIds);
            ImportJobOrderRequests(MigrationJobOrderRecordType.Licence, request, response, jobIds);
            ImportJobOrderRequests(MigrationJobOrderRecordType.Language, request, response, jobIds);
            ImportJobOrderRequests(MigrationJobOrderRecordType.SpecialRequirement, request, response, jobIds);
            ImportJobOrderRequests(MigrationJobOrderRecordType.ProgramOfStudy, request, response, jobIds);
            break;
        }

      	// For all jobs that were added, or had locations added, the job needs to be updated to generate the posting
        if (jobIds.Any())
          GenerateJobPosting(request, jobIds);

        switch (request.RecordType)
        {
          case RecordType.JobSeeker:
            ImportJobSeekerRequests(MigrationJobSeekerRecordType.JobSeeker, request, response);
            break;
          case RecordType.Resume:
            ImportJobSeekerRequests(MigrationJobSeekerRecordType.Resume, request, response);
            ImportJobSeekerRequests(MigrationJobSeekerRecordType.ResumeDocument, request, response);
            break;
          case RecordType.SavedSearch:
            ImportJobSeekerRequests(MigrationJobSeekerRecordType.SavedSearch, request, response);
            break;
          case RecordType.SearchAlert:
            ImportJobSeekerRequests(MigrationJobSeekerRecordType.SearchAlert, request, response);
            break;
          case RecordType.ViewedPosting:
            ImportJobSeekerRequests(MigrationJobSeekerRecordType.ViewedPosting, request, response);
            break;
          case RecordType.Referral:
            ImportJobSeekerRequests(MigrationJobSeekerRecordType.Referral, request, response);
            break;
          case RecordType.Activity:
            ImportJobSeekerRequests(MigrationJobSeekerRecordType.Activity, request, response);
            break;
          case RecordType.Flagged:
            ImportJobSeekerRequests(MigrationJobSeekerRecordType.Flagged, request, response);
            break;
          case RecordType.AssistUser:
            ImportAssistUserRequests(MigrationAssistUserRecordType.AssistUser, request, response);
            break;
          case RecordType.Notes:
            ImportAssistUserRequests(MigrationAssistUserRecordType.NotesAndReminders, request, response);
            break;
          case RecordType.Office:
            ImportAssistUserRequests(MigrationAssistUserRecordType.Office, request, response);
            break;
          case RecordType.OfficeMapping:
            ImportAssistUserRequests(MigrationAssistUserRecordType.OfficeMapping, request, response);
            break;
					case RecordType.SpideredJobs:
						 ImportJobOrderRequests(MigrationJobOrderRecordType.SpideredJob, request, response, null);
		        break;
					case RecordType.SpideredJobReferrals:
						ImportJobOrderRequests(MigrationJobOrderRecordType.SpideredJobReferral, request, response, null);
		        break;
					case RecordType.SavedJob:
						ImportJobOrderRequests(MigrationJobOrderRecordType.SavedJob, request, response, null);
		        break;
        }

        response.SetSuccess("Import Requests Created");

      }
      catch (Exception ex)
      {
        response.SetFailure(FormatErrorMessage(request, ErrorTypes.Unknown), ex);
        Logger.Error(request.SessionId, request.RequestId, request.UserContext.UserId, "Error in Import", ex);
      }

      return response;
    }


		public void ImportEmployerTradeNameRequests(MigrationEmployerRecordType type, [NotNull] ImportRequest importRequest,
			[NotNull] ImportResponse importResponse)
		{
			if (importRequest == null) throw new ArgumentNullException("importRequest");
			if (importResponse == null) throw new ArgumentNullException("importResponse");
			if (type != MigrationEmployerRecordType.TradeName) throw new ArgumentException("type");

			var doProcessing = true;

			var ignoreClient = ConfigurationManager.AppSettings.Get("Focus-ImportService-IgnoreClientService")
																						 .ToBool()
																						 .GetValueOrDefault(true);

      long? firstId = null;
      long? lastId = null;

      if (importRequest.BatchNumber.IsNotNull())
      {
        var batchNumbers = MigrationRepository.BatchRecords.FirstOrDefault(r => r.EntityType == MigrationEntityType.Employer && r.RecordType == (int)type && r.BatchNumber == importRequest.BatchNumber);
        if (batchNumbers.IsNotNull())
        {
          firstId = batchNumbers.FirstId;
          lastId = batchNumbers.LastId;
        }
      }

      while (_totalRecordsRead < _recordsToImport && doProcessing)
			{
				var recordsToRead = _recordsToImport - _totalRecordsRead;
				if (recordsToRead > _batchsize)
					recordsToRead = _batchsize;

			  var requests = MigrationRepository.EmployerRequests
                                          .Where(r => r.RecordType == type && r.Status == MigrationStatus.ToBeProcessed && (firstId == null || r.Id >= firstId) && (lastId == null || r.Id <= lastId))
																					.OrderBy(e => e.Id)
																					.Select(e => e.Id)
																					.Take(recordsToRead)
																					.ToList();

				if (requests.IsNotNullOrEmpty())
				{
					requests.ForEach(requestId =>
					{
						var employerTradeNamesImportRequest = new EmployerImportRequest
						{
							EmployerRequestId = requestId,
							IgnoreClient = ignoreClient,
						};
						SetServiceRequestProperties(employerTradeNamesImportRequest, importRequest);

						Core.Messages.ServiceResponse serviceResponse = null;

						var service = DataImportService;

						serviceResponse = service.ProcessEmployerTradeNames(employerTradeNamesImportRequest);

						_totalRecordsRead++;

						if (serviceResponse != null && serviceResponse.Acknowledgement == AcknowledgementType.Success)
						{
							lock (SyncObject)
							{
								importResponse.NumberOfEmployerTradeNamesCreated++;
							}
						}
					});

					Logger.Reset();
				}
				else
				{
					doProcessing = false;
				}

				MigrationRepository.SaveChanges(true);
			}
		}

  	#region Employers

    /// <summary>
    /// Imports the requests for employers and associated records.
    /// </summary>
    /// <param name="type">The type of employer request (employee, business unit description or logo).</param>
    /// <param name="importRequest">The import request object.</param>
    /// <param name="importResponse">A response object to update with records processed.</param>
    private void ImportEmployerRequests(MigrationEmployerRecordType type, ImportRequest importRequest, ImportResponse importResponse)
    {
      var doProcessing = true;

      var ignoreClient = ConfigurationManager.AppSettings.Get("Focus-ImportService-IgnoreClientService")
                                             .ToBool()
                                             .GetValueOrDefault(true);

      long? firstId = null;
      long? lastId = null;

      if (importRequest.BatchNumber.IsNotNull())
      {
        var batchNumbers = MigrationRepository.BatchRecords.FirstOrDefault(r => r.EntityType == MigrationEntityType.Employer && r.RecordType == (int)type && r.BatchNumber == importRequest.BatchNumber);
        if (batchNumbers.IsNotNull())
        {
          firstId = batchNumbers.FirstId;
          lastId = batchNumbers.LastId;
        }
      }

      while (_totalRecordsRead < _recordsToImport && doProcessing)
      {
        var recordsToRead = _recordsToImport - _totalRecordsRead;
        if (recordsToRead > _batchsize)
          recordsToRead = _batchsize;

        var requests = MigrationRepository.EmployerRequests
                                          .Where(r => r.RecordType == type && r.Status == MigrationStatus.ToBeProcessed && (firstId == null || r.Id >= firstId) && (lastId == null || r.Id <= lastId))
                                          .OrderBy(e => e.Id)
                                          .Select(e => e.Id)
                                          .Take(recordsToRead)
                                          .ToList();

        if (requests.IsNotNullOrEmpty())
        {
          requests.ForEach(requestId =>
          {
            var employerImportRequest = new EmployerImportRequest
            {
              EmployerRequestId = requestId,
              IgnoreClient = ignoreClient
            };
            SetServiceRequestProperties(employerImportRequest, importRequest);

            Core.Messages.ServiceResponse serviceResponse = null;

            var service = DataImportService;

            switch (type)
            {
              case MigrationEmployerRecordType.Employee:
                serviceResponse = service.ProcessEmployer(employerImportRequest);
                if (serviceResponse.Acknowledgement == AcknowledgementType.Success)
                {
                  lock (SyncObject)
                  {
                    importResponse.NumberOfEmployersCreated++;
                  }
                }
                break;
              case MigrationEmployerRecordType.BusinessUnitDescription:
                serviceResponse = service.ProcessBusinessUnitDescription(employerImportRequest);
                break;
              case MigrationEmployerRecordType.BusinessUnitLogo:
                serviceResponse = service.ProcessBusinessUnitLogo(employerImportRequest);
                break;
              case MigrationEmployerRecordType.ResumeSearch:
                serviceResponse = service.ProcessResumeSearch(employerImportRequest, MigrationEmployerRecordType.ResumeSearch);
                if (serviceResponse.Acknowledgement == AcknowledgementType.Success)
                {
                  lock (SyncObject)
                  {
                    importResponse.NumberOfResumeSearchesCreated++;
                  }
                }
                break;
              case MigrationEmployerRecordType.ResumeAlert:
                serviceResponse = service.ProcessResumeSearch(employerImportRequest, MigrationEmployerRecordType.ResumeAlert);
                if (serviceResponse.Acknowledgement == AcknowledgementType.Success)
                {
                  lock (SyncObject)
                  {
                    importResponse.NumberOfResumeAlertsCreated++;
                  }
                }
                break;
            }

            _totalRecordsRead++;
          });

          Logger.Reset();
        }
        else
        {
          doProcessing = false;
        }

        MigrationRepository.SaveChanges(true);
      }
    }

    /*
    /// <summary>
    /// Sends an employer request from the migration database to the focus database.
    /// </summary>
    /// <param name="importRequest">The import request object.</param>
    /// <param name="migrationRequest">The employer request from the migration database.</param>
    private void ProcessEmployer(ImportRequest importRequest, EmployerRequest migrationRequest)
    {
      var talentRequest = migrationRequest.Request.DataContractDeserialize<RegisterTalentUserRequest>();

      // A negative business unit indicates this needs to be looked up rather than add a new one
      if (talentRequest.BusinessUnit.Id.HasValue && talentRequest.BusinessUnit.Id < 0)
      {
				var employer = CoreRepository.Employers.FirstOrDefault(e => e.FederalEmployerIdentificationNumber == talentRequest.Employer.FederalEmployerIdentificationNumber);
        if (employer != null)
        {
          var businessUnit = employer.BusinessUnits.FirstOrDefault(bu => bu.IsPrimary);
          talentRequest.BusinessUnit.Id = (businessUnit == null) ? 0 : businessUnit.Id;
        }
        else
        {
          talentRequest.BusinessUnit.Id = 0;
        }
      }

      SetServiceRequestProperties(talentRequest, importRequest);

      var response = AccountService.RegisterTalentUser(talentRequest);

      CheckEmployerServiceResponse(response, migrationRequest, response.EmployeeId);
    }

    /// <summary>
    /// Sends an business unit description request from the migration database to the focus database.
    /// </summary>
    /// <param name="importRequest">The import request object.</param>
    /// <param name="migrationRequest">The business unit description request from the migration database.</param>
    private void ProcessBusinessUnitDescription(ImportRequest importRequest, EmployerRequest migrationRequest)
    {
      var descriptionRequest = migrationRequest.Request.DataContractDeserialize<SaveBusinessUnitDescriptionRequest>();
      
      var employerRequest = LookupEmployerRequest(descriptionRequest.EmployeeMigrationId);
      if (employerRequest != null)
      {
        SetServiceRequestProperties(descriptionRequest, importRequest);

        var employeeBusinessUnit = LookupDefaultBusinessUnitForEmployee(employerRequest.FocusId);
        descriptionRequest.BusinessUnitDescription.BusinessUnitId = employeeBusinessUnit.BusinessUnitId;

        var response = EmployerService.SaveBusinessUnitDescription(descriptionRequest);

        var focusId = (response.BusinessUnitDescription != null && response.BusinessUnitDescription.Id.HasValue) ? response.BusinessUnitDescription.Id.Value : 0;
        CheckEmployerServiceResponse(response, migrationRequest, focusId);
      }
      else
      {
        migrationRequest.Status = MigrationStatus.Failed;
        migrationRequest.Message = "EmployerRequest record not found";
      }
    }

    /// <summary>
    /// Sends an business unit logo request from the migration database to the focus database.
    /// </summary>
    /// <param name="importRequest">The import request object.</param>
    /// <param name="migrationRequest">The business unit logo request from the migration database.</param>
    private void ProcessBusinessUnitLogo(ImportRequest importRequest, EmployerRequest migrationRequest)
    {
      var logoRequest = migrationRequest.Request.DataContractDeserialize<SaveBusinessUnitLogoRequest>();

      var employerRequest = LookupEmployerRequest(logoRequest.EmployeeMigrationId);
      if (employerRequest != null)
      {
        SetServiceRequestProperties(logoRequest, importRequest);

        var employeeBusinessUnit = LookupDefaultBusinessUnitForEmployee(employerRequest.FocusId);
        logoRequest.BusinessUnitLogo.BusinessUnitId = employeeBusinessUnit.BusinessUnitId;

        var response = EmployerService.SaveBusinessUnitLogo(logoRequest);

        CheckEmployerServiceResponse(response, migrationRequest, response.BusinessUnitLogoId);
      }
      else
      {
        migrationRequest.Status = MigrationStatus.Failed;
        migrationRequest.Message = "EmployerRequest record not found";
      }
    }

    /// <summary>
    /// Looks up the original migration request for an employer.
    /// </summary>
    /// <param name="externalId">The ID of the employer from the source system.</param>
    /// <returns>The original migration request.</returns>
    private EmployerRequest LookupEmployerRequest(string externalId)
    {
			// We need to refactor this. We would only have one or the other, so it makes sense that we have an OR in the query
			// Time is against us so I am making the TalentUser the dominate value to search by as this is what we are workign with
			//var employerRequest = LookupEmployerRequest(externalId, MigrationEmployerRecordType.TalentUser);

			var employerRequest= LookupEmployerRequest(externalId, MigrationEmployerRecordType.Employee);
    	return employerRequest;
    }

    /// <summary>
    /// Looks up the original migration request for an employer.
    /// </summary>
    /// <param name="type">The type of migration request (employer, business unit description or logo)</param>
    /// <param name="externalId">The ID of the employer from the source system.</param>
    /// <returns>The original migration request.</returns>
    private EmployerRequest LookupEmployerRequest(string externalId, MigrationEmployerRecordType type)
    {
      return MigrationRepository.EmployerRequests.FirstOrDefault(er => er.RecordType == type && er.ExternalId == externalId && er.FocusId != 0);
    }

    /// <summary>
    /// Looks up the default business unit for an employee.
    /// </summary>
    /// <param name="employeeId">The id of the employee to look up.</param>
    /// <returns>The employee's default business unit.</returns>
    private EmployeeBusinessUnit LookupDefaultBusinessUnitForEmployee(long employeeId)
    {
			var employee = CoreRepository.Employees.First(e => e.Id == employeeId);
      var employeeBusinessUnit = CoreRepository.EmployeeBusinessUnits.First(ebu => ebu.EmployeeId == employee.Id && ebu.Default);

      return employeeBusinessUnit;
    }
    */

    #endregion

    #region Job Orders

    /// <summary>
    /// Imports the requests for job orders.
    /// </summary>
    /// <param name="type">The type of children element, such as Locations or Address.</param>
    /// <param name="importRequest">The import request object.</param>
    /// <param name="importResponse">A response object to update with records processed.</param>
    /// <param name="jobIds">A list to be populated with the Ids of job referenced the focus database</param>
    private void ImportJobOrderRequests(MigrationJobOrderRecordType type, ImportRequest importRequest, ImportResponse importResponse, List<long> jobIds)
    {
      var doProcessing = true;

      var ignoreLens = ConfigurationManager.AppSettings.Get("Focus-ImportService-IgnoreLens")
                                     .ToBool()
                                     .GetValueOrDefault(false);

      var ignoreClient = ConfigurationManager.AppSettings.Get("Focus-ImportService-IgnoreClientService")
                                             .ToBool()
                                             .GetValueOrDefault(true);

      long? firstId = null;
      long? lastId = null;

      if (importRequest.BatchNumber.IsNotNull())
      {
        var batchNumbers = MigrationRepository.BatchRecords.FirstOrDefault(r => r.EntityType == MigrationEntityType.JobOrder && r.RecordType == (int)type && r.BatchNumber == importRequest.BatchNumber);
        if (batchNumbers.IsNotNull())
        {
          firstId = batchNumbers.FirstId;
          lastId = batchNumbers.LastId;
        }
      }

      while (_totalRecordsRead < _recordsToImport && doProcessing)
      {
        var recordsToRead = _recordsToImport - _totalRecordsRead;
        if (recordsToRead > _batchsize)
          recordsToRead = _batchsize;

        var requests = MigrationRepository.JobOrderRequests
                                          .Where(r => r.RecordType == type && r.Status == MigrationStatus.ToBeProcessed && (firstId == null || r.Id >= firstId) && (lastId == null || r.Id <= lastId))
                                          .OrderBy(e => e.Id)
                                          .Select(e => e.Id)
                                          .Take(recordsToRead)
                                          .ToList();

        if (requests.IsNotNullOrEmpty())
        {
          requests.ForEach(requestId =>
          {
            var jobOrderImportRequest = new JobOrderImportRequest
            {
              JobOrderRequestId = requestId,
              IgnoreLens = ignoreLens,
              IgnoreClient = ignoreClient
            };
            SetServiceRequestProperties(jobOrderImportRequest, importRequest);

            Core.Messages.ServiceResponse serviceResponse;

            var service = DataImportService;

            switch (type)
            {
              case MigrationJobOrderRecordType.Job:
                serviceResponse = service.ProcessJobOrder(jobOrderImportRequest);
                if (serviceResponse.Acknowledgement == AcknowledgementType.Success)
                {
                  lock (SyncObject)
                  {
                    importResponse.NumberOfJobOrdersCreated++;
                  }
                }
                break;
              case MigrationJobOrderRecordType.Location:
                serviceResponse = service.ProcessJobLocation(jobOrderImportRequest);
                if (serviceResponse.Acknowledgement == AcknowledgementType.Success)
                {
                  lock (SyncObject)
                  {
                    importResponse.NumberOfJobLocationsCreated++;
                  }
                }
                break;
              case MigrationJobOrderRecordType.Address:
                serviceResponse = service.ProcessJobAddress(jobOrderImportRequest);
                if (serviceResponse.Acknowledgement == AcknowledgementType.Success)
                {
                  lock (SyncObject)
                  {
                    importResponse.NumberOfJobAddressesCreated++;
                  }
                }
                break;
              case MigrationJobOrderRecordType.Certificate:
                serviceResponse = service.ProcessJobCertificate(jobOrderImportRequest);
                if (serviceResponse.Acknowledgement == AcknowledgementType.Success)
                {
                  lock (SyncObject)
                  {
                    importResponse.NumberOfJobCertificatesCreated++;
                  }
                }
                break;
              case MigrationJobOrderRecordType.Licence:
                serviceResponse = service.ProcessJobLicence(jobOrderImportRequest);
                if (serviceResponse.Acknowledgement == AcknowledgementType.Success)
                {
                  lock (SyncObject)
                  {
                    importResponse.NumberOfJobLicencesCreated++;
                  }
                }
                break;
              case MigrationJobOrderRecordType.Language:
                serviceResponse = service.ProcessJobLanguage(jobOrderImportRequest);
                if (serviceResponse.Acknowledgement == AcknowledgementType.Success)
                {
                  lock (SyncObject)
                  {
                    importResponse.NumberOfJobLanguagesCreated++;
                  }
                }
                break;
              case MigrationJobOrderRecordType.SpecialRequirement:
                serviceResponse = service.ProcessJobSpecialRequirement(jobOrderImportRequest);
                if (serviceResponse.Acknowledgement == AcknowledgementType.Success)
                {
                  lock (SyncObject)
                  {
                    importResponse.NumberOfJobSpecialRequirementsCreated++;
                  }
                }
                break;
              case MigrationJobOrderRecordType.ProgramOfStudy:
                serviceResponse = service.ProcessJobProgramOfStudy(jobOrderImportRequest);
                if (serviceResponse.Acknowledgement == AcknowledgementType.Success)
                {
                  lock (SyncObject)
                  {
                    importResponse.NumberOfJobProgramOfStudysCreated++;
                  }
                }
                break;
							case MigrationJobOrderRecordType.SpideredJob:
		            serviceResponse = service.ProcessSpideredJob(jobOrderImportRequest);
		            if (serviceResponse.Acknowledgement == AcknowledgementType.Success)
		            {
			            lock (SyncObject)
			            {
				            importResponse.NumberOfSpideredJobsCreated++;
			            }
								}
		            break;
							case MigrationJobOrderRecordType.SpideredJobReferral:
								serviceResponse = service.ProcessSpideredJobReferral(jobOrderImportRequest);
								if (serviceResponse.Acknowledgement == AcknowledgementType.Success)
								{
									lock (SyncObject)
									{
										importResponse.NumberOfSpideredJobsCreated++;
									}
								}
		            break;
							case MigrationJobOrderRecordType.SavedJob:
								serviceResponse = service.ProcessSavedJob(jobOrderImportRequest);
								if (serviceResponse.Acknowledgement == AcknowledgementType.Success)
								{
									lock (SyncObject)
									{
										importResponse.NumberOfSpideredJobsCreated++;
									}
								}
		            break;
            }

            _totalRecordsRead++;
          });

          if (jobIds != null)
          {
            var processedRequests = MigrationRepository.JobOrderRequests.Where(jo => requests.Contains(jo.Id)).ToList();
            processedRequests.ForEach(request =>
            {
              if (request.Status == MigrationStatus.Processed && !jobIds.Contains(request.FocusId))
                jobIds.Add(request.FocusId);
            });
          }

          Logger.Reset();
        }
        else
        {
          doProcessing = false;
        }

        MigrationRepository.SaveChanges(true);
      }
    }

    /// <summary>
    /// Updates a list of jobs so that the Posting field is generated.
    /// </summary>
    /// <param name="importRequest">The request object for the import service.</param>
    /// <param name="jobIds">The list of job ids to update.</param>
    private void GenerateJobPosting(ImportRequest importRequest, List<long> jobIds)
    {
      var ignoreLens = ConfigurationManager.AppSettings.Get("Focus-ImportService-IgnoreLens")
                                     .ToBool()
                                     .GetValueOrDefault(false);

      jobIds.ForEach(jobId =>
      {
        var jobOrderImportRequest = new JobOrderImportRequest
        {
          JobId = jobId,
          IgnoreLens = ignoreLens
        };
        SetServiceRequestProperties(jobOrderImportRequest, importRequest);

        var service = DataImportService;
        service.GenerateJobPosting(jobOrderImportRequest);
      });
    }

    /*
    /// <summary>
    /// Sends an job order request from the migration database to the focus database.
    /// </summary>
    /// <param name="importRequest">The import request object.</param>
    /// <param name="migrationRequest">The job order request from the migration database.</param>
    private void ProcessJobOrder(ImportRequest importRequest, JobOrderRequest migrationRequest)
    {
      var saveJobRequest = migrationRequest.Request.DataContractDeserialize<SaveJobRequest>();

      var employerRequest = LookupEmployerRequest(saveJobRequest.EmployeeMigrationId);// determine what we need here

      if (employerRequest.IsNotNull())
      {
        SetServiceRequestProperties(saveJobRequest, importRequest);

        var employee = CoreRepository.Employees.FirstOrDefault(e => e.Id == employerRequest.FocusId);
				if (employee == null)
				{
					migrationRequest.Status = MigrationStatus.Failed;
					migrationRequest.Message = "Employee record not found";
					return;
				}

        saveJobRequest.Job.EmployerId = employee.EmployerId;
        saveJobRequest.Job.EmployeeId = employee.Id;

        var employeeBusinessUnit = CoreRepository.EmployeeBusinessUnits.First(ebu => ebu.EmployeeId == employee.Id && ebu.Default);
        saveJobRequest.Job.BusinessUnitId = employeeBusinessUnit.BusinessUnitId;

        var employeeBusinessUnitDescription = employeeBusinessUnit.BusinessUnit.BusinessUnitDescriptions.FirstOrDefault(bud => bud.IsPrimary);
        saveJobRequest.Job.BusinessUnitDescriptionId = (employeeBusinessUnitDescription == null ? (long?) null : employeeBusinessUnitDescription.Id);

        if (!string.IsNullOrEmpty(saveJobRequest.EmployerMigrationLogoId))
        {
          var employerLogoRequest = LookupEmployerRequest(saveJobRequest.EmployerMigrationLogoId, MigrationEmployerRecordType.BusinessUnitLogo);
          if (employerLogoRequest != null)
            saveJobRequest.Job.BusinessUnitLogoId = employerLogoRequest.FocusId;
        }

        var response = JobService.SaveJob(saveJobRequest);

        var focusId = (response.Job != null && response.Job.Id.HasValue) ? response.Job.Id.Value : 0;
        CheckJobOrderServiceResponse(response, migrationRequest, focusId);

        if (response.Acknowledgement == AcknowledgementType.Success)
        {
          var jobLocationRequest = LookupJobOrderRequest(saveJobRequest.JobMigrationId, MigrationJobOrderRecordType.Location, false);
          if (jobLocationRequest != null)
            ProcessJobLocation(importRequest, jobLocationRequest, focusId);

          var jobAddressRequest = LookupJobOrderRequest(saveJobRequest.JobMigrationId, MigrationJobOrderRecordType.Address, false);
          if (jobAddressRequest != null)
            ProcessJobAddress(importRequest, jobAddressRequest, focusId);

          var jobCertificateRequest = LookupJobOrderRequest(saveJobRequest.JobMigrationId, MigrationJobOrderRecordType.Certificate, false);
          if (jobCertificateRequest != null)
            ProcessJobCertificate(importRequest, jobCertificateRequest, focusId);

          var jobLicenceRequest = LookupJobOrderRequest(saveJobRequest.JobMigrationId, MigrationJobOrderRecordType.Licence, false);
          if (jobLicenceRequest != null)
            ProcessJobLicence(importRequest, jobLicenceRequest, focusId);

          var jobLanguageRequest = LookupJobOrderRequest(saveJobRequest.JobMigrationId, MigrationJobOrderRecordType.Language, false);
          if (jobLanguageRequest != null)
            ProcessJobLanguage(importRequest, jobLanguageRequest, focusId);

          var jobSpecialRequirementRequest = LookupJobOrderRequest(saveJobRequest.JobMigrationId, MigrationJobOrderRecordType.SpecialRequirement, false);
          if (jobSpecialRequirementRequest != null)
            ProcessJobSpecialRequirement(importRequest, jobSpecialRequirementRequest, focusId);

          var jobProgramOfStudyRequest = LookupJobOrderRequest(saveJobRequest.JobMigrationId, MigrationJobOrderRecordType.ProgramOfStudy, false);
          if (jobProgramOfStudyRequest != null)
            ProcessJobProgramOfStudy(importRequest, jobProgramOfStudyRequest, focusId);

          GenerateJobPosting(importRequest, focusId);
        }
      }
      else
      {
        migrationRequest.Status = MigrationStatus.Failed;
        migrationRequest.Message = "EmployerRequest record not found";
      }
    }

    /// <summary>
    /// Sends requests for job location from the migration database to the focus database.
    /// </summary>
    /// <param name="importRequest">The import request object.</param>
    /// <param name="migrationRequest">The job location request from the migration database.</param>
    private void ProcessJobLocation(ImportRequest importRequest, JobOrderRequest migrationRequest)
    {
      ProcessJobLocation(importRequest, migrationRequest, null);
    }

    /// <summary>
    /// Sends requests for job location from the migration database to the focus database.
    /// </summary>
    /// <param name="importRequest">The import request object.</param>
    /// <param name="migrationRequest">The job location request from the migration database.</param>
    /// <param name="jobId">Optional job id if known</param>
    private void ProcessJobLocation(ImportRequest importRequest, JobOrderRequest migrationRequest, long? jobId)
    {
      var locationRequest = migrationRequest.Request.DataContractDeserialize<SaveJobLocationsRequest>();

      if (jobId == null)
      {
        var jobRequest = LookupJobOrderRequest(locationRequest.JobMigrationId);
        if (jobRequest != null)
          jobId = jobRequest.FocusId;
      }

      if (jobId.HasValue)
      {
        SetServiceRequestProperties(locationRequest, importRequest);

        locationRequest.JobLocations.ForEach(jobLocation =>
        {
          jobLocation.JobId = jobId.Value;

          // If the address id is -1, then this indicates the job address should be the business unit address
          if (jobLocation.Id == -1)
          {
            jobLocation.Id = null;
            //var address = CoreRepository.BusinessUnitAddresses.First(bua => bua.BusinessUnitId == job.BusinessUnitId);

            var address = (from j in CoreRepository.Jobs
                           join a in CoreRepository.BusinessUnitAddresses
                             on j.BusinessUnitId equals a.BusinessUnitId
                           where j.Id == jobId
                           select a.AsDto()).First();

            var state = RuntimeContext.Helpers.Lookup.GetLookup(LookupTypes.States).FirstOrDefault(l => l.Id == address.StateId);
            jobLocation.Location = string.Format("{0}, {1} ({2})", address.TownCity, state == null ? "" : state.ExternalId, address.PostcodeZip);
          }
        });
        locationRequest.Criteria = new JobLocationCriteria
        {
          JobId = jobId
        };

        var saveResponse = JobService.SaveJobLocations(locationRequest);

        CheckJobOrderServiceResponse(saveResponse, migrationRequest, jobId.Value);
      }
      else
      {
        migrationRequest.Status = MigrationStatus.Failed;
        migrationRequest.Message = "JobOrderRequest record not found";
      }
    }

    /// <summary>
    /// Sends requests for job addresss from the migration database to the focus database.
    /// </summary>
    /// <param name="importRequest">The import request object.</param>
    /// <param name="migrationRequest">The job child element request from the migration database.</param>
    private void ProcessJobAddress(ImportRequest importRequest, JobOrderRequest migrationRequest)
    {
      ProcessJobAddress(importRequest, migrationRequest, null);
    }

    /// <summary>
    /// Sends requests for job addresss from the migration database to the focus database.
    /// </summary>
    /// <param name="importRequest">The import request object.</param>
    /// <param name="migrationRequest">The job child element request from the migration database.</param>
    /// <param name="jobId">Optional job id if known</param>
    private void ProcessJobAddress(ImportRequest importRequest, JobOrderRequest migrationRequest, long? jobId)
    {
      var addressRequest = migrationRequest.Request.DataContractDeserialize<SaveJobAddressRequest>();

      if (jobId == null)
      {
        var jobRequest = LookupJobOrderRequest(addressRequest.JobMigrationId);
        if (jobRequest != null)
          jobId = jobRequest.FocusId;
      }

      if (jobId.HasValue)
      {
        SetServiceRequestProperties(addressRequest, importRequest);

        // If the address id is -1, then this indicates the job address should be the business unit address
        if (addressRequest.JobAddress.Id == -1)
        {
          addressRequest.JobAddress.Id = null;

          var address = (from j in CoreRepository.Jobs
                         join a in CoreRepository.BusinessUnitAddresses
                           on j.BusinessUnitId equals a.BusinessUnitId
                         where j.Id == jobId
                         select a.AsDto()).First();

          addressRequest.JobAddress = new JobAddressDto
          {
            Line1 = address.Line1,
            Line2 = address.Line2,
            Line3 = address.Line3,
            TownCity = address.TownCity,
            CountyId = address.CountyId,
            CountryId = address.CountryId,
            StateId = address.StateId,
            PostcodeZip = address.PostcodeZip,
            IsPrimary = true
          };
        }

        addressRequest.JobAddress.JobId = jobId.Value;

        var saveResponse = JobService.SaveJobAddress(addressRequest);

        CheckJobOrderServiceResponse(saveResponse, migrationRequest, jobId.Value);
      }
      else
      {
        migrationRequest.Status = MigrationStatus.Failed;
        migrationRequest.Message = "JobOrderRequest record not found";
      }
    }

    /// <summary>
    /// Sends requests for job certificate from the migration database to the focus database.
    /// </summary>
    /// <param name="importRequest">The import request object.</param>
    /// <param name="migrationRequest">The job certificate request from the migration database.</param>
    private void ProcessJobCertificate(ImportRequest importRequest, JobOrderRequest migrationRequest)
    {
      ProcessJobCertificate(importRequest, migrationRequest, null);
    }

    /// <summary>
    /// Sends requests for job certificate from the migration database to the focus database.
    /// </summary>
    /// <param name="importRequest">The import request object.</param>
    /// <param name="migrationRequest">The job certificate request from the migration database.</param>
    /// <param name="jobId">Optional job id if known</param>
    private void ProcessJobCertificate(ImportRequest importRequest, JobOrderRequest migrationRequest, long? jobId)
    {
      var certificateRequest = migrationRequest.Request.DataContractDeserialize<SaveJobCertificatesRequest>();

      if (jobId == null)
      {
        var jobRequest = LookupJobOrderRequest(certificateRequest.JobMigrationId);
        if (jobRequest != null)
          jobId = jobRequest.FocusId;
      }

      if (jobId.HasValue)
      {
        SetServiceRequestProperties(certificateRequest, importRequest);

        certificateRequest.JobCertificates.ForEach(jc => jc.JobId = jobId.Value);
        certificateRequest.Criteria = new JobCertificateCriteria
        {
          JobId = jobId
        };

        var saveResponse = JobService.SaveJobCertificates(certificateRequest);

        CheckJobOrderServiceResponse(saveResponse, migrationRequest, jobId.Value);
      }
      else
      {
        migrationRequest.Status = MigrationStatus.Failed;
        migrationRequest.Message = "JobOrderRequest record not found";
      }
    }

    /// <summary>
    /// Sends requests for job licence from the migration database to the focus database.
    /// </summary>
    /// <param name="importRequest">The import request object.</param>
    /// <param name="migrationRequest">The job licence request from the migration database.</param>
    private void ProcessJobLicence(ImportRequest importRequest, JobOrderRequest migrationRequest)
    {
      ProcessJobLicence(importRequest, migrationRequest, null);
    }

    /// <summary>
    /// Sends requests for job licence from the migration database to the focus database.
    /// </summary>
    /// <param name="importRequest">The import request object.</param>
    /// <param name="migrationRequest">The job licence request from the migration database.</param>
    /// <param name="jobId">Optional job id if known</param>
    private void ProcessJobLicence(ImportRequest importRequest, JobOrderRequest migrationRequest, long? jobId)
    {
      var licenceRequest = migrationRequest.Request.DataContractDeserialize<SaveJobLicencesRequest>();

      if (jobId == null)
      {
        var jobRequest = LookupJobOrderRequest(licenceRequest.JobMigrationId);
        if (jobRequest != null)
          jobId = jobRequest.FocusId;
      }

      if (jobId.HasValue)
      {
        SetServiceRequestProperties(licenceRequest, importRequest);

        licenceRequest.JobLicences.ForEach(jl => jl.JobId = jobId.Value);
        licenceRequest.Criteria = new JobLicenceCriteria
        {
          JobId = jobId
        };

        var saveResponse = JobService.SaveJobLicences(licenceRequest);

        CheckJobOrderServiceResponse(saveResponse, migrationRequest, jobId.Value);
      }
      else
      {
        migrationRequest.Status = MigrationStatus.Failed;
        migrationRequest.Message = "JobOrderRequest record not found";
      }
    }

    /// <summary>
    /// Sends requests for job language from the migration database to the focus database.
    /// </summary>
    /// <param name="importRequest">The import request object.</param>
    /// <param name="migrationRequest">The job language request from the migration database.</param>
    private void ProcessJobLanguage(ImportRequest importRequest, JobOrderRequest migrationRequest)
    {
      ProcessJobLanguage(importRequest, migrationRequest, null);
    }

    /// <summary>
    /// Sends requests for job language from the migration database to the focus database.
    /// </summary>
    /// <param name="importRequest">The import request object.</param>
    /// <param name="migrationRequest">The job language request from the migration database.</param>
    /// <param name="jobId">Optional job id if known</param>
    private void ProcessJobLanguage(ImportRequest importRequest, JobOrderRequest migrationRequest, long? jobId)
    {
      var languageRequest = migrationRequest.Request.DataContractDeserialize<SaveJobLanguagesRequest>();

      if (jobId == null)
      {
        var jobRequest = LookupJobOrderRequest(languageRequest.JobMigrationId);
        if (jobRequest != null)
          jobId = jobRequest.FocusId;
      }

      if (jobId.HasValue)
      {
        SetServiceRequestProperties(languageRequest, importRequest);

        languageRequest.JobLanguages.ForEach(jl => jl.JobId = jobId.Value);
        languageRequest.Criteria = new JobLanguageCriteria
        {
          JobId = jobId
        };

        var saveResponse = JobService.SaveJobLanguages(languageRequest);

        CheckJobOrderServiceResponse(saveResponse, migrationRequest, jobId.Value);
      }
      else
      {
        migrationRequest.Status = MigrationStatus.Failed;
        migrationRequest.Message = "JobOrderRequest record not found";
      }
    }

    /// <summary>
    /// Sends requests for job special requirement from the migration database to the focus database.
    /// </summary>
    /// <param name="importRequest">The import request object.</param>
    /// <param name="migrationRequest">The job special requirement request from the migration database.</param>
    private void ProcessJobSpecialRequirement(ImportRequest importRequest, JobOrderRequest migrationRequest)
    {
      ProcessJobSpecialRequirement(importRequest, migrationRequest, null);
    }

    /// <summary>
    /// Sends requests for job special requirement from the migration database to the focus database.
    /// </summary>
    /// <param name="importRequest">The import request object.</param>
    /// <param name="migrationRequest">The job special requirement request from the migration database.</param>
    /// <param name="jobId">Optional job id if known</param>
    private void ProcessJobSpecialRequirement(ImportRequest importRequest, JobOrderRequest migrationRequest, long? jobId)
    {
      var requirementRequest = migrationRequest.Request.DataContractDeserialize<SaveJobSpecialRequirementsRequest>();

      if (jobId == null)
      {
        var jobRequest = LookupJobOrderRequest(requirementRequest.JobMigrationId);
        if (jobRequest != null)
          jobId = jobRequest.FocusId;
      }

      if (jobId.HasValue)
      {
        SetServiceRequestProperties(requirementRequest, importRequest);

        requirementRequest.JobSpecialRequirements.ForEach(jsr => jsr.JobId = jobId.Value);
        requirementRequest.Criteria = new JobSpecialRequirementCriteria
        {
          JobId = jobId
        };

        var saveResponse = JobService.SaveJobSpecialRequirements(requirementRequest);

        CheckJobOrderServiceResponse(saveResponse, migrationRequest, jobId.Value);
      }
      else
      {
        migrationRequest.Status = MigrationStatus.Failed;
        migrationRequest.Message = "JobOrderRequest record not found";
      }
    }

    /// <summary>
    /// Sends requests for job certificate from the migration database to the focus database.
    /// </summary>
    /// <param name="importRequest">The import request object.</param>
    /// <param name="migrationRequest">The job certificate request from the migration database.</param>
    private void ProcessJobProgramOfStudy(ImportRequest importRequest, JobOrderRequest migrationRequest)
    {
      ProcessJobProgramOfStudy(importRequest, migrationRequest, null);
    }

    /// <summary>
    /// Sends requests for job certificate from the migration database to the focus database.
    /// </summary>
    /// <param name="importRequest">The import request object.</param>
    /// <param name="migrationRequest">The job certificate request from the migration database.</param>
    /// <param name="jobId">Optional job id if known</param>
    private void ProcessJobProgramOfStudy(ImportRequest importRequest, JobOrderRequest migrationRequest, long? jobId)
    {
      var programOfStudyRequest = migrationRequest.Request.DataContractDeserialize<SaveJobProgramsOfStudyRequest>();

      if (jobId == null)
      {
        var jobRequest = LookupJobOrderRequest(programOfStudyRequest.JobMigrationId);
        if (jobRequest != null)
          jobId = jobRequest.FocusId;
      }

      if (jobId.HasValue)
      {
        SetServiceRequestProperties(programOfStudyRequest, importRequest);

        programOfStudyRequest.JobProgramsOfStudy.ForEach(jp => jp.JobId = jobId.Value);
        programOfStudyRequest.Criteria = new JobProgramsOfStudyCriteria
        {
          JobId = jobId
        };

        var saveResponse = JobService.SaveJobProgramsOfStudy(programOfStudyRequest);

        CheckJobOrderServiceResponse(saveResponse, migrationRequest, jobId.Value);
      }
      else
      {
        migrationRequest.Status = MigrationStatus.Failed;
        migrationRequest.Message = "JobOrderRequest record not found";
      }
    }

    /// <summary>
    /// Looks up the original migration request for a job.
    /// </summary>
    /// <param name="externalId">The ID of the job order from the source system.</param>
    /// <param name="type">The type of request</param>
    /// <param name="checkFocusId">Only return records where the focus ID is set</param>
    /// <returns>The original migration request.</returns>
    private JobOrderRequest LookupJobOrderRequest(string externalId, MigrationJobOrderRecordType type = MigrationJobOrderRecordType.Job, bool checkFocusId = true)
    {
      return checkFocusId
        ? MigrationRepository.JobOrderRequests.FirstOrDefault(jo => jo.RecordType == type && jo.ExternalId == externalId && jo.FocusId != 0)
        : MigrationRepository.JobOrderRequests.FirstOrDefault(jo => jo.RecordType == type && jo.ExternalId == externalId);
    }

    /// <summary>
    /// Updates a job so that the Posting field is generated.
    /// </summary>
    /// <param name="importRequest">The request object for the import service.</param>
    /// <param name="jobId">The Id of the job to update.</param>
    private void GenerateJobPosting(ImportRequest importRequest, long jobId)
    {
      // Job must be saved, as posting Html is only generated when saving an existing job
      var saveRequest = new SaveJobRequest
      {
        ClientTag = importRequest.ClientTag,
        SessionId = importRequest.SessionId,
        RequestId = importRequest.RequestId,
        UserContext = importRequest.UserContext,
        Job = CoreRepository.Jobs.First(j => j.Id == jobId).AsDto()
      };

      var saveResponse = JobService.SaveJob(saveRequest);

      if (saveResponse.Acknowledgement == AcknowledgementType.Success)
      {
        var postRequest = new PostJobRequest
          {
            ClientTag = importRequest.ClientTag,
            SessionId = importRequest.SessionId,
            RequestId = importRequest.RequestId,
            UserContext = importRequest.UserContext,
            Job = saveResponse.Job
          };

        JobService.PostJob(postRequest);
      }
    }
    */
    #endregion

    #region Job Seekers

    /// <summary>
    /// Imports the requests for job seekers.
    /// </summary>
    /// <param name="type">The type of job seeker record (seeker, resume, resume document)</param>
    /// <param name="importRequest">The import request object.</param>
    /// <param name="importResponse">A response object to update with records processed.</param>
    private void ImportJobSeekerRequests(MigrationJobSeekerRecordType type, ImportRequest importRequest, ImportResponse importResponse)
    {
      var doProcessing = true;

      var ignoreLens = ConfigurationManager.AppSettings.Get("Focus-ImportService-IgnoreLens")
                                                       .ToBool()
                                                       .GetValueOrDefault(false);

      var ignoreClient = ConfigurationManager.AppSettings.Get("Focus-ImportService-IgnoreClientService")
                                             .ToBool()
                                             .GetValueOrDefault(true);

      long? firstId = null;
      long? lastId = null;

      if (importRequest.BatchNumber.IsNotNull())
      {
        var batchNumbers = MigrationRepository.BatchRecords.FirstOrDefault(r => r.EntityType == MigrationEntityType.JobSeeker && r.RecordType == (int)type && r.BatchNumber == importRequest.BatchNumber);
        if (batchNumbers.IsNotNull())
        {
          firstId = batchNumbers.FirstId;
          lastId = batchNumbers.LastId;
        }
      }

      while (_totalRecordsRead < _recordsToImport && doProcessing)
      {
        var recordsToRead = _recordsToImport - _totalRecordsRead;
        if (recordsToRead > _batchsize)
          recordsToRead = _batchsize;

        var requests = MigrationRepository.JobSeekerRequests
                                          .Where(r => r.RecordType == type && r.Status == MigrationStatus.ToBeProcessed && (firstId == null || r.Id >= firstId) && (lastId == null || r.Id <= lastId))
                                          .OrderBy(e => e.Id)
                                          .Select(e => e.Id)
                                          .Take(recordsToRead)
                                          .ToList();

        if (requests.IsNotNullOrEmpty())
        {
          requests.ForEach(requestId =>
          {
            var jobSeekerImportRequest = new JobSeekerImportRequest
            {
              JobSeekerRequestId = requestId,
              IgnoreLens = ignoreLens,
              IgnoreClient = ignoreClient
            };
            SetServiceRequestProperties(jobSeekerImportRequest, importRequest);

            Core.Messages.ServiceResponse serviceResponse;

            var service = DataImportService;

            switch (type)
            {
              case MigrationJobSeekerRecordType.JobSeeker:
                serviceResponse = service.ProcessJobSeeker(jobSeekerImportRequest);
                if (serviceResponse.Acknowledgement == AcknowledgementType.Success)
                {
                  lock (SyncObject)
                  {
                    importResponse.NumberOfJobSeekersCreated++;
                  }
                }
                break;
              case MigrationJobSeekerRecordType.Resume:
                serviceResponse = service.ProcessJobSeekerResume(jobSeekerImportRequest);
                if (serviceResponse.Acknowledgement == AcknowledgementType.Success)
                {
                  lock (SyncObject)
                  {
                    importResponse.NumberOfResumesCreated++;
                  }
                }
                break;
              case MigrationJobSeekerRecordType.ResumeDocument:
                serviceResponse = service.ProcessJobSeekerResumeDocument(jobSeekerImportRequest);
                if (serviceResponse.Acknowledgement == AcknowledgementType.Success)
                {
                  lock (SyncObject)
                  {
                    importResponse.NumberOfResumesCreated++;
                  }
                }
                break;
              case MigrationJobSeekerRecordType.ViewedPosting:
                serviceResponse = service.ProcessJobSeekerViewedPosting(jobSeekerImportRequest);
                if (serviceResponse.Acknowledgement == AcknowledgementType.Success)
                {
                  lock (SyncObject)
                  {
                    importResponse.NumberOfViewedPostingsCreated++;
                  }
                }
                break;
              case MigrationJobSeekerRecordType.SavedSearch:
                serviceResponse = service.ProcessJobSeekerSavedSearch(jobSeekerImportRequest, MigrationJobSeekerRecordType.SavedSearch);
                if (serviceResponse.Acknowledgement == AcknowledgementType.Success)
                {
                  lock (SyncObject)
                  {
                    importResponse.NumberOfSavedSearchesCreated++;
                  }
                }
                break;
              case MigrationJobSeekerRecordType.SearchAlert:
                serviceResponse = service.ProcessJobSeekerSavedSearch(jobSeekerImportRequest, MigrationJobSeekerRecordType.SearchAlert);
                if (serviceResponse.Acknowledgement == AcknowledgementType.Success)
                {
                  lock (SyncObject)
                  {
                    importResponse.NumberOfSearchAlertsCreated++;
                  }
                }
                break;
              case MigrationJobSeekerRecordType.Referral:
                serviceResponse = service.ProcessReferral(jobSeekerImportRequest, MigrationJobSeekerRecordType.Referral);
                if (serviceResponse.Acknowledgement == AcknowledgementType.Success)
                {
                  lock (SyncObject)
                  {
                    importResponse.NumberOfReferralsCreated++;
                  }
                }
                break;
              case MigrationJobSeekerRecordType.Activity:
                serviceResponse = service.ProcessJobSeekerActivity(jobSeekerImportRequest);
                if (serviceResponse.Acknowledgement == AcknowledgementType.Success)
                {
                  lock (SyncObject)
                  {
                    importResponse.NumberOfActivitiesCreated++;
                  }
                }
                break;
              case MigrationJobSeekerRecordType.Flagged:
                serviceResponse = service.ProcessFlagJobSeeker(jobSeekerImportRequest);
                if (serviceResponse.Acknowledgement == AcknowledgementType.Success)
                {
                  lock (SyncObject)
                  {
                    importResponse.NumberOfFlaggedJobSeekerCreated++;
                  }
                }
                break;
            }

            _totalRecordsRead++;
          });

          Logger.Reset();
        }
        else
        {
          doProcessing = false;
        }

        MigrationRepository.SaveChanges(true);
      }
    }

    /*
    /// <summary>
    /// Sends a job seeker request from the migration database to the focus database.
    /// </summary>
    /// <param name="importRequest">The import request object.</param>
    /// <param name="migrationRequest">The employer request from the migration database.</param>
    private void ProcessJobSeeker(ImportRequest importRequest, JobSeekerRequest migrationRequest)
    {
      var careerRequest = migrationRequest.Request.DataContractDeserialize<RegisterCareerUserRequest>();

      SetServiceRequestProperties(careerRequest, importRequest);

      var response = AccountService.RegisterCareerUser(careerRequest);

      if (response.Acknowledgement == AcknowledgementType.Success)
      {
        migrationRequest.FocusId = response.PersonId;
        migrationRequest.Status = MigrationStatus.Processed;
      }
      else
      {
        migrationRequest.Status = MigrationStatus.Failed;
        migrationRequest.Message = FormatResponseMessage(response);
      }
    }

    /// <summary>
    /// Sends a job seeker resume request from the migration database to the focus database.
    /// </summary>
    /// <param name="importRequest">The import request object.</param>
    /// <param name="migrationRequest">The employer request from the migration database.</param>
    private void ProcessJobSeekerResume(ImportRequest importRequest, JobSeekerRequest migrationRequest)
    {
      var careerRequest = migrationRequest.Request.DataContractDeserialize<SaveResumeRequest>();
      
      var jobSeekerRequest = LookupJobSeekerRequest(careerRequest.JobSeekerMigrationId, MigrationJobSeekerRecordType.JobSeeker);
      if (jobSeekerRequest != null)
      {
        SetServiceRequestProperties(careerRequest, importRequest);

        careerRequest.PersonId = jobSeekerRequest.FocusId;

        var registerCareerUserRequest = jobSeekerRequest.Request.DataContractDeserialize<RegisterCareerUserRequest>();
        var emailAddress = registerCareerUserRequest.EmailAddress;
        if (jobSeekerRequest.ExternalId.EndsWith(".tag") && emailAddress.StartsWith("Test") && emailAddress.EndsWith("@burning-glass.com"))
        {
          if (careerRequest.SeekerResume.ResumeContent.SeekerContactDetails != null)
            careerRequest.SeekerResume.ResumeContent.SeekerContactDetails.EmailAddress = emailAddress;

          if (careerRequest.SeekerResume.ResumeContent.Profile != null)
            careerRequest.SeekerResume.ResumeContent.Profile.EmailAddress = emailAddress;
        }

        var response = ResumeService.SaveResume(careerRequest);

        if (response.Acknowledgement == AcknowledgementType.Success)
        {
          migrationRequest.FocusId = response.SeekerResume.ResumeMetaInfo.ResumeId.GetValueOrDefault(0);
          migrationRequest.Status = MigrationStatus.Processed;

          var resumeDocumentRequest = LookupJobSeekerRequest(careerRequest.ResumeMigrationId, MigrationJobSeekerRecordType.ResumeDocument, false);
          if (resumeDocumentRequest != null)
            ProcessJobSeekerResumeDocument(importRequest, resumeDocumentRequest, migrationRequest.FocusId);
        }
        else
        {
          migrationRequest.Status = MigrationStatus.Failed;
          migrationRequest.Message = FormatResponseMessage(response);
        }
      }
      else
      {
        migrationRequest.Status = MigrationStatus.Failed;
        migrationRequest.Message = "JobSeekerRequest record not found";
      }
    }

    /// <summary>
    /// Sends a job seeker resume document request from the migration database to the focus database.
    /// </summary>
    /// <param name="importRequest">The import request object.</param>
    /// <param name="migrationRequest">The employer request from the migration database.</param>
    private void ProcessJobSeekerResumeDocument(ImportRequest importRequest, JobSeekerRequest migrationRequest)
    {
      ProcessJobSeekerResumeDocument(importRequest, migrationRequest, null);
    }

    /// <summary>
    /// Sends a job seeker resume document request from the migration database to the focus database.
    /// </summary>
    /// <param name="importRequest">The import request object.</param>
    /// <param name="migrationRequest">The employer request from the migration database.</param>
    /// <param name="resumeId">The original resume id, if known</param>
    private void ProcessJobSeekerResumeDocument(ImportRequest importRequest, JobSeekerRequest migrationRequest, long? resumeId)
    {
      var careerRequest = migrationRequest.Request.DataContractDeserialize<SaveResumeDocumentRequest>();

      if (resumeId == null)
      {
        var resumeRequest = LookupJobSeekerRequest(careerRequest.ResumeMigrationId, MigrationJobSeekerRecordType.Resume);
        if (resumeRequest != null)
          resumeId = resumeRequest.FocusId;
      }

      if (resumeId.HasValue)
      {
        SetServiceRequestProperties(careerRequest, importRequest);

        careerRequest.ResumeDocument.ResumeId = resumeId.Value;
        var response = ResumeService.SaveResumeDocument(careerRequest);

        if (response.Acknowledgement == AcknowledgementType.Success)
        {
          migrationRequest.FocusId = response.ResumeDocument.Id.GetValueOrDefault(0);
          migrationRequest.Status = MigrationStatus.Processed;
        }
        else
        {
          migrationRequest.Status = MigrationStatus.Failed;
          migrationRequest.Message = FormatResponseMessage(response);
        }
      }
      else
      {
        migrationRequest.Status = MigrationStatus.Failed;
        migrationRequest.Message = "JobSeekerRequest (resume) record not found";
      }
    }

    /// <summary>
    /// Sends a saved search request from the migration database to the focus database.
    /// </summary>
    /// <param name="importRequest">The import request object.</param>
    /// <param name="migrationRequest">The Job Seeker Saved Search request from the migration database.</param>
    private void ProcessJobSeekerSavedSearch(ImportRequest importRequest, JobSeekerRequest migrationRequest)
    {
      var careerRequest = migrationRequest.Request.DataContractDeserialize<SaveSearchRequest>();

      var jobSeekerRequest = LookupJobSeekerRequest(careerRequest.JobSeekerMigrationId, MigrationJobSeekerRecordType.JobSeeker);
      if (jobSeekerRequest != null)
      {
        SetServiceRequestProperties(careerRequest, importRequest);

        var user = CoreRepository.Users.FirstOrDefault(u => u.PersonId == jobSeekerRequest.FocusId);

        if (user != null)
        {
          careerRequest.SaveForUserId = user.Id;
          if (careerRequest.AlertEmailRequired && careerRequest.AlertEmailAddress.IsNullOrEmpty())
            careerRequest.AlertEmailAddress = user.Person.EmailAddress;

          var response = AnnotationService.SaveSearch(careerRequest);

          if (response.Acknowledgement == AcknowledgementType.Success)
          {
            migrationRequest.FocusId = response.SavedSearchId;
            migrationRequest.Status = MigrationStatus.Processed;
          }
          else
          {
            migrationRequest.Status = MigrationStatus.Failed;
            migrationRequest.Message = FormatResponseMessage(response);
          }
        }
        else
        {
          migrationRequest.Status = MigrationStatus.Failed;
          migrationRequest.Message = "User record not found for SavedSearch";
        }
      }
      else
      {
        migrationRequest.Status = MigrationStatus.Failed;
        migrationRequest.Message = "JobSeekerRequest (SavedSearch) record not found";
      }
    }

    /// <summary>
    /// Sends a viewed posting request from the migration database to the focus database.
    /// </summary>
    /// <param name="importRequest">The import request object.</param>
    /// <param name="migrationRequest">The viewed posting request from the migration database.</param>
    private void ProcessJobSeekerViewedPosting(ImportRequest importRequest, JobSeekerRequest migrationRequest)
    {
      var careerRequest = migrationRequest.Request.DataContractDeserialize<AddViewedPostingRequest>();

      var jobSeekerRequest = LookupJobSeekerRequest(careerRequest.JobSeekerMigrationId, MigrationJobSeekerRecordType.JobSeeker);
      if (jobSeekerRequest != null)
      {
        SetServiceRequestProperties(careerRequest, importRequest);

        var user = CoreRepository.Users.FirstOrDefault(u => u.PersonId == jobSeekerRequest.FocusId);

        if (user != null)
        {
          careerRequest.ViewedPosting.UserId = user.Id;

          var response = PostingService.AddViewedPostings(careerRequest);

          if (response.Acknowledgement == AcknowledgementType.Success)
          {
            migrationRequest.FocusId = response.ViewedPosting.Id.GetValueOrDefault(0);
            migrationRequest.Status = MigrationStatus.Processed;
          }
          else
          {
            migrationRequest.Status = MigrationStatus.Failed;
            migrationRequest.Message = FormatResponseMessage(response);
          }
        }
        else
        {
          migrationRequest.Status = MigrationStatus.Failed;
          migrationRequest.Message = "User record not found for ViewedPosting";
        }
      }
      else
      {
        migrationRequest.Status = MigrationStatus.Failed;
        migrationRequest.Message = "JobSeekerRequest (ViewedPosting) record not found";
      }
    }

    /// <summary>
    /// Looks up the original migration request for a job seeker.
    /// </summary>
    /// <param name="externalId">The ID of the job seeker from the source system.</param>
    /// <param name="type">The type of request</param>
    /// <param name="checkFocusId">Only return records where the focus ID is set</param>
    /// <returns>The original migration request.</returns>
    private JobSeekerRequest LookupJobSeekerRequest(string externalId, MigrationJobSeekerRecordType type, bool checkFocusId = true)
    {
      return checkFocusId
        ? MigrationRepository.JobSeekerRequests.FirstOrDefault(js => js.RecordType == type && js.ExternalId == externalId && js.FocusId != 0)
        : MigrationRepository.JobSeekerRequests.FirstOrDefault(js => js.RecordType == type && js.ExternalId == externalId);
    }
    */

    #endregion

    #region Users

    private void ImportAssistUserRequests(MigrationAssistUserRecordType type, ImportRequest importRequest, ImportResponse importResponse)
		{
      var doProcessing = true;

      long? firstId = null;
      long? lastId = null;

      if (importRequest.BatchNumber.IsNotNull())
      {
        var batchNumbers = MigrationRepository.BatchRecords.FirstOrDefault(r => r.EntityType == MigrationEntityType.User && r.RecordType == (int)type && r.BatchNumber == importRequest.BatchNumber);
        if (batchNumbers.IsNotNull())
        {
          firstId = batchNumbers.FirstId;
          lastId = batchNumbers.LastId;
        }
      }

      while (_totalRecordsRead < _recordsToImport && doProcessing)
      {
        var recordsToRead = _recordsToImport - _totalRecordsRead;
        if (recordsToRead > _batchsize)
          recordsToRead = _batchsize;

        var requests = MigrationRepository.UserRequests
                                          .Where(r => r.RecordType == type && r.Status == MigrationStatus.ToBeProcessed && (firstId == null || r.Id >= firstId) && (lastId == null || r.Id <= lastId))
                                          .OrderBy(e => e.Id)
                                          .Select(e => e.Id)
                                          .Take(recordsToRead)
                                          .ToList();

        if (requests.IsNotNullOrEmpty())
        {
          requests.ForEach(requestId =>
          {
            var assistImportRequest = new AssistImportRequest { AssistRequestId = requestId };
            SetServiceRequestProperties(assistImportRequest, importRequest);

            Core.Messages.ServiceResponse serviceResponse;

            var service = DataImportService;

            switch (type)
            {
              case MigrationAssistUserRecordType.AssistUser:
                serviceResponse = service.ProcessCreateAssistUser(assistImportRequest);
                if (serviceResponse.Acknowledgement == AcknowledgementType.Success)
                {
                  lock (SyncObject)
                  {
                    importResponse.NumberOfAssistUsersCreated++;
                  }
                }
                break;

              case MigrationAssistUserRecordType.NotesAndReminders:
                serviceResponse = service.ProcessNoteReminder(assistImportRequest);
                if (serviceResponse.Acknowledgement == AcknowledgementType.Success)
                {
                  lock (SyncObject)
                  {
                    importResponse.NumberOfNotesAndRemindersCreated++;
                  }
                }
                break;

              case MigrationAssistUserRecordType.Office:
                serviceResponse = service.ProcessSaveOffice(assistImportRequest);
                if (serviceResponse.Acknowledgement == AcknowledgementType.Success)
                {
                  lock (SyncObject)
                  {
                    importResponse.NumberOfOfficesCreated++;
                  }
                }
                break;

              case MigrationAssistUserRecordType.OfficeMapping:
                serviceResponse = service.ProcessOfficeMappings(assistImportRequest);
                if (serviceResponse.Acknowledgement == AcknowledgementType.Success)
                {
                  lock (SyncObject)
                  {
                    importResponse.NumberOfOfficeMappingsCreated++;
                  }
                }
                break;
            }

            _totalRecordsRead++;
          });

          Logger.Reset();
        }
        else
        {
          doProcessing = false;
        }

        MigrationRepository.SaveChanges(true);
      }
		}

    /*
    /// <summary>
    /// Sends an assist user request from the migration database to the focus database.
    /// </summary>
    /// <param name="importRequest">The import request object.</param>
    /// <param name="migrationRequest">The assist user request from the migration database.</param>
    private void ProcessCreateAssistUser(ImportRequest importRequest, UserRequest migrationRequest)
    {
      var assistRequest = migrationRequest.Request.DataContractDeserialize<CreateAssistUserRequest>();

      SetServiceRequestProperties(assistRequest, importRequest);

      var response = AccountService.CreateAssistUser(assistRequest);

      if (response.Acknowledgement == AcknowledgementType.Success)
      {
        migrationRequest.FocusId = response.UserId;
        migrationRequest.Status = MigrationStatus.Processed;
      }
      else
      {
        migrationRequest.Status = MigrationStatus.Failed;
        migrationRequest.Message = FormatResponseMessage(response);
      }
    }

    /// <summary>
    /// Sends an assist user request from the migration database to the focus database.
    /// </summary>
    /// <param name="importRequest">The import request object.</param>
    /// <param name="migrationRequest">The assist user request from the migration database.</param>
    private void ProcessNoteReminder(ImportRequest importRequest, UserRequest migrationRequest)
    {
      var assistRequest = migrationRequest.Request.DataContractDeserialize<SaveNoteReminderRequest>();

      var jobSeekerRequest = LookupJobSeekerRequest(assistRequest.JobSeekerMigrationId, MigrationJobSeekerRecordType.JobSeeker);
      if (jobSeekerRequest != null)
      {
        var userRequest = LookupUserRequest(assistRequest.StaffMigrationId, MigrationAssistUserRecordType.AssistUser);

        if (userRequest != null)
        {
          SetServiceRequestProperties(assistRequest, importRequest);

          assistRequest.NoteReminder.EntityId = jobSeekerRequest.FocusId;
          assistRequest.NoteReminder.CreatedBy = userRequest.FocusId;

          if (assistRequest.ReminderRecipients.IsNotNullOrEmpty())
          {
            assistRequest.ReminderRecipients.ForEach(
              r => r.RecipientEntityId = (r.RecipientEntityType == EntityTypes.JobSeeker ? jobSeekerRequest.FocusId : userRequest.FocusId));
          }

          var response = CoreService.SaveNoteReminder(assistRequest);

          if (response.Acknowledgement == AcknowledgementType.Success)
          {
            migrationRequest.FocusId = response.NoteReminder.Id.GetValueOrDefault(0);
            migrationRequest.Status = MigrationStatus.Processed;
          }
          else
          {
            migrationRequest.Status = MigrationStatus.Failed;
            migrationRequest.Message = FormatResponseMessage(response);
          }
        }
        else
        {
          migrationRequest.Status = MigrationStatus.Failed;
          migrationRequest.Message = "UserRequest record not found";
        }
      }
      else
      {
        migrationRequest.Status = MigrationStatus.Failed;
        migrationRequest.Message = "JobSeekerRequest record not found";
      }
    }

    /// <summary>
    /// Looks up the original migration request for a user.
    /// </summary>
    /// <param name="externalId">The ID of the user from the source system.</param>
    /// <param name="type">The type of request</param>
    /// <param name="checkFocusId">Only return records where the focus ID is set</param>
    /// <returns>The original migration request.</returns>
    private UserRequest LookupUserRequest(string externalId, MigrationAssistUserRecordType type, bool checkFocusId = true)
    {
      return checkFocusId
        ? MigrationRepository.UserRequests.FirstOrDefault(u => u.RecordType == type && u.ExternalId == externalId && u.FocusId != 0)
        : MigrationRepository.UserRequests.FirstOrDefault(u => u.RecordType == type && u.ExternalId == externalId);
    }
    */

    #endregion

		#region Helpers

		/// <summary>
    /// Sets the standard properties of a service request.
    /// </summary>
    /// <param name="serviceRequest">The service request.</param>
    /// <param name="importRequest">The import request from which the properties can be copied.</param>
    private void SetServiceRequestProperties(FocusRequest serviceRequest, ImportRequest importRequest)
    {
      serviceRequest.ClientTag = importRequest.ClientTag;
      serviceRequest.SessionId = importRequest.SessionId;
      serviceRequest.RequestId = importRequest.RequestId;
      serviceRequest.UserContext = importRequest.UserContext;
		  serviceRequest.VersionNumber = importRequest.VersionNumber;
    }

    /*
    /// <summary>
    /// Checks the response from a call to an employer service.
    /// </summary>
    /// <param name="saveResponse">The save response from the employer service.</param>
    /// <param name="migrationRequest">The employer migration request record.</param>
    /// <param name="focusId">The id of any record saved in Focus Assist/Talent.</param>
    private void CheckEmployerServiceResponse(FocusResponse saveResponse, EmployerRequest migrationRequest, long focusId)
    {
      if (saveResponse.Acknowledgement == AcknowledgementType.Success)
      {
        migrationRequest.FocusId = focusId;
        migrationRequest.Status = MigrationStatus.Processed;
      }
      else
      {
        migrationRequest.Status = MigrationStatus.Failed;
        migrationRequest.Message = FormatResponseMessage(saveResponse);
      }
    }

    /// <summary>
    /// Checks the response from a call to an job service.
    /// </summary>
    /// <param name="saveResponse">The save response from the job service.</param>
    /// <param name="migrationRequest">The job migration request record.</param>
    /// <param name="focusId">The id of any record saved in Focus Assist/Talent.</param>
    private void CheckJobOrderServiceResponse(FocusResponse saveResponse, JobOrderRequest migrationRequest, long focusId)
    {
      if (saveResponse.Acknowledgement == AcknowledgementType.Success)
      {
        migrationRequest.FocusId = focusId;
        migrationRequest.Status = MigrationStatus.Processed;
      }
      else
      {
        migrationRequest.Status = MigrationStatus.Failed;
        migrationRequest.Message = FormatResponseMessage(saveResponse);
      }
    }

    /// <summary>
    /// Formats the message from a service 'save' response.
    /// </summary>
    /// <param name="saveResponse">The response from the service.</param>
    /// <returns>The formatted message.</returns>
    private string FormatResponseMessage(FocusResponse saveResponse)
    {
      return (saveResponse.HasRaisedException)
               ? string.Format("{0}. EXCEPTION: {1}", saveResponse.Message, saveResponse.Exception.Message)
               : saveResponse.Message;
    }
    */

    #endregion
  }
}
