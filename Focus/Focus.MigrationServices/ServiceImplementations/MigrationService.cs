﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Focus.Core;
using Focus.Core.Messages.AccountService;
using Focus.Core.Messages.EmployerService;
using Focus.Core.Messages.ResumeService;
using Focus.Core.Messages.SearchService;
using Focus.Data.Migration.Entities;
using Focus.Data.Repositories.Contracts;
using Focus.MigrationServices.Providers;
using Focus.MigrationServices.Providers.Common;
using Focus.MigrationServices.Providers.Interfaces;
using Focus.MigrationServices.Messages;
using Focus.MigrationServices.ServiceContracts;
using Focus.Services;
using Framework.Core;
using Framework.Logging;

#endregion

namespace Focus.MigrationServices.ServiceImplementations
{
  public class MigrationService : MigrationCoreService, IMigrationService
  {
  	private IMigrationProvider _migrationProvider;
    private string _lastMigrationProviderName;

    private MigrationProviderArgs _providerArgs;
    private bool _updateExistingRecords;
    private int? _recordsToMigrate;

  	public MigrationService()
  	{
  	  _providerArgs = new MigrationProviderArgs
  	  {
	      BatchSize = AppSettings.MigrationBatchSize,
        CustomParm = ""
  	  };
  	}

    public MigrationService(IRuntimeContext runtimeContext)
			: base(runtimeContext.Repositories.Core, runtimeContext.Repositories.Configuration, runtimeContext.Repositories.Library, runtimeContext.Repositories.Migration)
    {
      RuntimeContext = runtimeContext;

      _providerArgs = new MigrationProviderArgs
      {
        BatchSize = AppSettings.MigrationBatchSize,
        CustomParm = ""
      };
    }

    public MigrationService(IRuntimeContext runtimeContext, ICoreRepository coreRepository, IMigrationRepository migrationRepository, ILibraryRepository libraryRepository, IConfigurationRepository configRepository)
      : base(coreRepository, configRepository, libraryRepository, migrationRepository)
    {
      RuntimeContext = runtimeContext;

      _providerArgs = new MigrationProviderArgs
      {
        BatchSize = AppSettings.MigrationBatchSize,
        CustomParm = ""
      };
    }

    /// <summary>
    /// Performs the migration of all of the source data into the migration database.
    /// </summary>
    /// <param name="request">The object requesting the migration.</param>
    /// <returns>A response from the migration.</returns>
		public MigrationResponse Migrate(MigrationRequest request)
    {
      InitialiseMigrationProvider(request);

			var response = new MigrationResponse(request);
			
				// Validate request
			if (!ValidateRequest(request, response, Validate.All))
				return response;

      _recordsToMigrate = request.RecordsToMigrate.HasValue && request.RecordsToMigrate > 0 
        ? request.RecordsToMigrate.Value 
        : int.MaxValue;

      try
			{
        if (request.BatchSize.HasValue && request.BatchSize > 0)
          _providerArgs.BatchSize = request.BatchSize.Value;

			  if (request.CustomParm.IsNotNullOrEmpty())
          _providerArgs.CustomParm = request.CustomParm;

			  if (request.CutOffDate.IsNotNull())
			    _providerArgs.CutOffDate = request.CutOffDate;
				
			  _updateExistingRecords = request.UpdateExistingRecords;

        switch (request.RecordType)
        {
          case RecordType.Employer:
            {
              var registerTalentUserProvider = _migrationProvider as IEmployer;
              if (registerTalentUserProvider.IsNotNull()) MigrateEmployers(registerTalentUserProvider, response);
            }
            break;
					case RecordType.EmployerTradeName:
						{
							var employerTradeNameProvider = _migrationProvider as IEmployer;
							if (employerTradeNameProvider.IsNotNull()) MigrateEmployerTradeNames(employerTradeNameProvider, response);
						}
		        break;
          case RecordType.ResumeSearch:
            {
              var resumeSearchProvider = _migrationProvider as IEmployer;
              if (resumeSearchProvider.IsNotNull()) MigrateResumeSearches(resumeSearchProvider, response);
            }
            break;
          case RecordType.ResumeAlert:
            {
              var resumeSearchProvider = _migrationProvider as IEmployer;
              if (resumeSearchProvider.IsNotNull()) MigrateResumeAlerts(resumeSearchProvider, response);
            }
            break;
          case RecordType.Job:
            {
              var jobOrderProvider = _migrationProvider as IJobOrder;
              if (jobOrderProvider.IsNotNull()) MigrateJobOrders(jobOrderProvider, response);
            }
            break;
          case RecordType.JobSeeker:
            {
              var jobSeekerProvider = _migrationProvider as IJobSeeker;
              if (jobSeekerProvider.IsNotNull()) MigrateJobSeekers(jobSeekerProvider, response);
            }
            break;
          case RecordType.Resume:
            {
              var resumeProvider = _migrationProvider as IResume;
              if (resumeProvider.IsNotNull()) MigrateResumes(resumeProvider, response);
            }
            break;
          case RecordType.SavedSearch:
            {
              var savedSearchProvider = _migrationProvider as IJobSeeker;
              if (savedSearchProvider.IsNotNull()) MigrateJobSeekerSavedSearches(savedSearchProvider, response);
            }
            break;
          case RecordType.SearchAlert:
            {
              var savedSearchProvider = _migrationProvider as IJobSeeker;
              if (savedSearchProvider.IsNotNull()) MigrateJobSeekerSearchAlerts(savedSearchProvider, response);
            }
            break;
          case RecordType.ViewedPosting:
            {
              var viewedPostingProvider = _migrationProvider as IJobSeeker;
              if (viewedPostingProvider.IsNotNull()) MigrateViewedPostings(viewedPostingProvider, response);
            }
            break;
          case RecordType.Referral:
            {
              var referralProvider = _migrationProvider as IJobSeeker;
              if (referralProvider.IsNotNull()) MigrateReferrals(referralProvider, response);
            }
            break;
          case RecordType.Activity:
            {
              var activitiesProvider = _migrationProvider as IJobSeeker;
              if (activitiesProvider.IsNotNull()) MigrateJobSeekerActivities(activitiesProvider, response);
            }
            break;
          case RecordType.Flagged:
            {
              var flagsProvider = _migrationProvider as IJobSeeker;
              if (flagsProvider.IsNotNull()) MigrateFlaggedJobSeeker(flagsProvider, response);
            }
            break;
          case RecordType.AssistUser:
            {
              var userProvider = _migrationProvider as IUser;
              if (userProvider.IsNotNull()) MigrateUsers(userProvider, response);
            }
            break;
          case RecordType.Office:
            {
              var userProvider = _migrationProvider as IUser;
              if (userProvider.IsNotNull()) MigrateOffices(userProvider, response);
            }
            break;
          case RecordType.OfficeMapping:
            {
              var userProvider = _migrationProvider as IUser;
              if (userProvider.IsNotNull()) MigrateOfficeMappings(userProvider, response);
            }
            break;
          case RecordType.Notes:
            {
              var noteProvider = _migrationProvider as IUser;
              if (noteProvider.IsNotNull()) MigrateNoteAndReminders(noteProvider, response);
            }
            break;
					case RecordType.SpideredJobs:
						{
							var spideredJobsProvider = _migrationProvider as IJobOrder;
							if (spideredJobsProvider.IsNotNull()) MigrateSpideredJobs(spideredJobsProvider, response);
						}
						break;
					case RecordType.SpideredJobReferrals:
						{
							var spideredJobsProvider = _migrationProvider as IJobOrder;
							if (spideredJobsProvider.IsNotNull()) MigrateSpideredJobReferrals(spideredJobsProvider, response);
						}
						break;
					case RecordType.SavedJob:
						{
							var spideredJobsProvider = _migrationProvider as IJobOrder;
							if (spideredJobsProvider.IsNotNull()) MigrateSavedJobs(spideredJobsProvider, response);
						}
		        break;
        }
				response.SetSuccess("Migration Requests Created");
			}
			catch (Exception ex)
			{
				response.SetFailure(string.Format("{0} - {1}", FormatErrorMessage(request, ErrorTypes.Unknown), ex.Message), ex);
				Logger.Error(request.SessionId, request.RequestId, request.UserContext.UserId, "Error in Migrate", ex);
			}

			return response;
		}

		private int MigrateSpideredJobReferrals(IJobOrder provider, MigrationResponse response)
		{
			// process the spidered job referrals
			var selectIds = MigrationRepository.Query<JobOrderRequest>().Where(x => x.RecordType == MigrationJobOrderRecordType.SpideredJobReferral).Select(x => x.ExternalId);

			var existingExternalIds = new HashSet<string>(selectIds);

			// get a batch of spidered job referrals
			var spideredJobReferrals = provider.MigrateSpideredJobReferrals(_providerArgs);
			var totalRecordsCreated = 0;
			var failedRecords = 0;

			while (spideredJobReferrals.Count > 0 && totalRecordsCreated < _recordsToMigrate)
			{
				var spideredJobReferralsAdded = 0;

				spideredJobReferrals.ForEach(jl =>
				{
					if (totalRecordsCreated < _recordsToMigrate)
					{
						var isExisting = existingExternalIds.Contains(jl.ReferralMigrationId); // what migration id do we use to map spidered jobs to spidered job referrals

						if (!isExisting || _updateExistingRecords)
						{
							spideredJobReferralsAdded++;
							totalRecordsCreated++;

							if (
								!ProcessJobOrderRequest(MigrationJobOrderRecordType.SpideredJobReferral, isExisting, jl.DataContractSerialize(),
									jl.ReferralMigrationId, jl.LensPostingId, jl.MigrationValidationMessage))
							{
								failedRecords++;
							}
						}

						if (!isExisting)
              existingExternalIds.Add(jl.ReferralMigrationId);
					}
				});

				if (spideredJobReferralsAdded > 0)
				{
					response.NumberOfSpideredReferralsCreated += spideredJobReferralsAdded;
					MigrationRepository.SaveChanges();
				}

				// get the next batch of spidered job referrals
				spideredJobReferrals = provider.MigrateSpideredJobReferrals(_providerArgs);
			}
			response.NumberOfFailedRecords = failedRecords;

			return failedRecords;
		}

		private int MigrateSavedJobs(IJobOrder provider, MigrationResponse response)
		{
			// process the saved jobs
			var selectIds = MigrationRepository.Query<JobOrderRequest>().Where(x => x.RecordType == MigrationJobOrderRecordType.SavedJob).Select(x => x.ExternalId);

			var existingExternalIds = new HashSet<string>(selectIds);

			// get a batch of saved jobs
			var savedJobs = provider.MigrateSavedJobs(_providerArgs);
			var totalRecordsCreated = 0;
								int failedRecords = 0;
			while (savedJobs.Count > 0 && totalRecordsCreated < _recordsToMigrate)
			{
				var savedJobsAdded = 0;

				savedJobs.ForEach(jl =>
				{
					if (totalRecordsCreated < _recordsToMigrate)
					{
						var isExisting = existingExternalIds.Contains(jl.SavedJobMigrationId);
						if (!isExisting || _updateExistingRecords)
						{
							savedJobsAdded++;
							totalRecordsCreated++;

							if (!ProcessJobOrderRequest(MigrationJobOrderRecordType.SavedJob, isExisting, jl.DataContractSerialize(), jl.SavedJobMigrationId, jl.LensPostingMigrationId, jl.MigrationValidationMessage))
							{
								failedRecords++;
							}
						}

						if (!isExisting)
							existingExternalIds.Add(jl.SavedJobMigrationId);
					}
				});

				if (savedJobsAdded > 0)
				{
					response.NumberOfSpideredJobsCreated += savedJobsAdded;
					MigrationRepository.SaveChanges();
				}

				// get the next batch of spidered job referrals
				savedJobs = provider.MigrateSavedJobs(_providerArgs);
			}
			response.NumberOfFailedRecords = failedRecords;

			return failedRecords;
		}
		
    /// <summary>
    /// Initialises the migration provider
    /// </summary>
    private void InitialiseMigrationProvider(MigrationRequest request)
    {
      if (_migrationProvider.IsNull() || request.MigrationProviderName != _lastMigrationProviderName)
      {
        _lastMigrationProviderName = request.MigrationProviderName ?? AppSettings.MigrationProvider;

        _migrationProvider = MigrationProviderFactory.LoadMigrationProvider(_lastMigrationProviderName);
        _migrationProvider.ConfigurationRepository = ConfigRepository;
        _migrationProvider.LibraryRepository = LibraryRepository;
        _migrationProvider.RuntimeContext = RuntimeContext;
        _migrationProvider.AppSettings = AppSettings;
        _migrationProvider.SpoofData = request.SpoofData;

        if (request.SourceConnectionString.IsNotNullOrEmpty())
          _migrationProvider.MigrationConnectionString = request.SourceConnectionString;

        if (request.SourceFolderName.IsNotNullOrEmpty())
          _migrationProvider.MigrationFolderName = request.SourceFolderName;

        if (request.SourceFileName.IsNotNullOrEmpty())
          _migrationProvider.MigrationFileName = request.SourceFileName;

        _migrationProvider.Initialise(request.RecordType, request.LastId);
      }
    }

  	#region Employers / Employees

    /// <summary>
    /// Performs the migration of employers, to create 'talent user' requests in the migration database.
    /// </summary>
    /// <param name="provider">The provider used to get the employer migrations.</param>
    /// <param name="response">A response object to indicate the status of the migration.</param>
    private void MigrateEmployers(IEmployer provider, MigrationResponse response)
		{
      // process the employers
      var selectIds = MigrationRepository.Query<EmployerRequest>()
                                         .Where(x => x.RecordType == MigrationEmployerRecordType.Employee)
                                         .Select(x => x.ExternalId);

      var existingExternalIds = new HashSet<string>(selectIds);

      var totalRecordsCreated = 0;
      var failedRecords = 0;

      // get a batch of employers
      var employers = provider.MigrateEmployers(_providerArgs);
      while (employers.Count > 0 && totalRecordsCreated < _recordsToMigrate)
			{
        var employersCreated = 0;

        employers.ForEach(e =>
        {
          if (totalRecordsCreated < _recordsToMigrate)
          {
            var isExisting = existingExternalIds.Contains(e.EmployeeMigrationId);
            if (!isExisting || _updateExistingRecords)
            {
              employersCreated++;
              totalRecordsCreated++;

              if (!ProcessEmployer(e, isExisting))
                failedRecords++;
            }

            if (!isExisting)
              existingExternalIds.Add(e.EmployeeMigrationId);
          }
        });

        if (employersCreated > 0)
        {
          response.NumberOfEmployersCreated += employersCreated;
          MigrationRepository.SaveChanges(true);
        }

				// Get the next batch of employers
				employers = provider.MigrateEmployers(_providerArgs);
			}

      selectIds = MigrationRepository.Query<EmployerRequest>()
                                     .Where(x => x.RecordType == MigrationEmployerRecordType.BusinessUnitDescription)
                                     .Select(x => x.ExternalId);

      existingExternalIds = new HashSet<string>(selectIds);

      // get a batch of business unit descriptions
      var desriptions = provider.MigrateBusinessUnitDescriptions(_providerArgs);
      while (desriptions.Count > 0 && totalRecordsCreated < _recordsToMigrate)
      {
        var descriptionsCreated = 0;

        // process the business unit descriptions
        desriptions.Where(bu => !existingExternalIds.Contains(bu.BusinessUnitDescriptionMigrationId))
                   .ToList()
                   .ForEach(bu =>
                    {
                      if (totalRecordsCreated < _recordsToMigrate)
                      {
                        descriptionsCreated++;
                        totalRecordsCreated++;

                        if (!ProcessBusinessUnitDescription(bu))
                          failedRecords++;

                        existingExternalIds.Add(bu.BusinessUnitDescriptionMigrationId);
                      }
                    });

        if (descriptionsCreated > 0)
        {
          response.NumberOfBusinessUnitDescriptionCreated += descriptionsCreated;
          MigrationRepository.SaveChanges(true);
        }

        // Get the next batch of business unit descriptions
        desriptions = provider.MigrateBusinessUnitDescriptions(_providerArgs);
      }

      selectIds = MigrationRepository.Query<EmployerRequest>()
                                   .Where(x => x.RecordType == MigrationEmployerRecordType.BusinessUnitLogo)
                                   .Select(x => x.ExternalId);

      existingExternalIds = new HashSet<string>(selectIds);

      // get a batch of business unit descriptions
      var logos = provider.MigrateBusinessUnitLogos(_providerArgs);
      while (logos.Count > 0 && totalRecordsCreated < _recordsToMigrate)
      {
        var logosCreated = 0;

        // process the business unit descriptions
        logos.Where(bu => !existingExternalIds.Contains(bu.BusinessUnitLogoMigrationId))
             .ToList()
             .ForEach(bu =>
              {
                if (totalRecordsCreated < _recordsToMigrate)
                {
                  logosCreated++;
                  totalRecordsCreated++;

                  if (!ProcessBusinessUnitLogo(bu))
                    failedRecords++;
                  
                  existingExternalIds.Add(bu.BusinessUnitLogoMigrationId);
                }
              });

        if (logosCreated > 0)
        {
          response.NumberOfEmployerLogosCreated += logosCreated;
          MigrationRepository.SaveChanges(true);
        }

        // Get the next batch of business unit descriptions
        logos = provider.MigrateBusinessUnitLogos(_providerArgs);
      }

			#region process the employer trade names

			MigrateEmployerTradeNames(provider, response);

			#endregion

      response.NumberOfFailedRecords = failedRecords;
    }

    /// <summary>
    /// Saves the talent user request in the migration database.
    /// </summary>
    /// <param name="request">The talent user request.</param>
    /// <param name="isUpdate">Whether this is an update to an existing record.</param>
    /// <returns>A boolean indicating if the job order request was created</returns>
    private bool ProcessEmployer(RegisterTalentUserRequest request, bool isUpdate)
		{
			// This is a new record, create them 
      request.Module = AppSettings.Module;

			var employerRequest = new EmployerRequest
			{
				SourceId = 0,
        Request = request.DataContractSerialize(),
				Status = request.MigrationValidationMessage.IsNullOrEmpty() ? MigrationStatus.ToBeProcessed : MigrationStatus.Failed,
        ExternalId = request.EmployeeMigrationId,
        Message = request.MigrationValidationMessage ?? string.Empty,
				RecordType = MigrationEmployerRecordType.Employee,
				ProcessedBy = DateTime.UtcNow,
        IsUpdate = isUpdate
			};

			// Serialise & Save to MigrationDb
			MigrationRepository.Add(employerRequest);

      return employerRequest.Status == MigrationStatus.ToBeProcessed;
		}

    /// <summary>
    /// Saves the business unit description request in the migration database.
    /// </summary>
    /// <param name="request">The business unit description request.</param>
    /// <returns>A boolean indicating if the business unit description request was created</returns>
    private bool ProcessBusinessUnitDescription(SaveBusinessUnitDescriptionRequest request)
    {
      // This is a new record, create them 
      var descriptionRequest = new EmployerRequest
      {
        SourceId = 0,
        Request = request.DataContractSerialize(),
        Status = request.MigrationValidationMessage.IsNullOrEmpty() ? MigrationStatus.ToBeProcessed : MigrationStatus.Failed,
        ExternalId = request.BusinessUnitDescriptionMigrationId,
        ParentExternalId = request.EmployeeMigrationId,
        Message = request.MigrationValidationMessage ?? string.Empty,
				RecordType = MigrationEmployerRecordType.BusinessUnitDescription,
				ProcessedBy = DateTime.UtcNow
      };

      // Serialise & Save to MigrationDb
      MigrationRepository.Add(descriptionRequest);

      return descriptionRequest.Status == MigrationStatus.ToBeProcessed;
    }

    /// <summary>
    /// Saves the business unit logo request in the migration database.
    /// </summary>
    /// <param name="request">The business unit logo request.</param>
    /// <returns>A boolean indicating if the business unit logo request was created</returns>
    private bool ProcessBusinessUnitLogo(SaveBusinessUnitLogoRequest request)
    {
      // This is a new record, create them 
      var logoRequest = new EmployerRequest
      {
        SourceId = 0,
        Request = request.DataContractSerialize(),
        Status = request.MigrationValidationMessage.IsNullOrEmpty() ? MigrationStatus.ToBeProcessed : MigrationStatus.Failed,
        ExternalId = request.BusinessUnitLogoMigrationId,
        ParentExternalId = request.EmployeeMigrationId,
        Message = request.MigrationValidationMessage ?? string.Empty,
				RecordType = MigrationEmployerRecordType.BusinessUnitLogo,
				ProcessedBy = DateTime.UtcNow
      };

      // Serialise & Save to MigrationDb
      MigrationRepository.Add(logoRequest);

      return logoRequest.Status == MigrationStatus.ToBeProcessed;
    }
		
		private void MigrateEmployerTradeNames(IEmployer provider, MigrationResponse response)
		{
			// process the employer trade names
			var selectIds = MigrationRepository.Query<Focus.Data.Migration.Entities.EmployerRequest>()
																				 .Where(x => x.RecordType == MigrationEmployerRecordType.TradeName)
																				 .Select(x => x.ExternalId);

			var existingExternalIds = new HashSet<string>(selectIds);

			var totalRecordsCreated = 0;
			var failedRecords = 0;

			// get a batch of employer trade names
			bool preProcess = true;
			var employerTradeNames = provider.MigrateEmployerTradeNames(_providerArgs, preProcess);
			while (employerTradeNames.Count > 0 && totalRecordsCreated < _recordsToMigrate)
			{
				var employerTradeNamesCreated = 0;

				employerTradeNames.ForEach(e =>
				{
					if (totalRecordsCreated < _recordsToMigrate)
					{
						var isExisting = existingExternalIds.Contains(e.TradeNameId);
						if (!isExisting || _updateExistingRecords)
						{
							employerTradeNamesCreated++;
							totalRecordsCreated++;

							if (!ProcessEmployerTradeName(e, _updateExistingRecords))
								failedRecords++;
						}

						if (!isExisting)
							existingExternalIds.Add(e.TradeNameId);

						preProcess = false;
					}
				});

				if (employerTradeNamesCreated > 0)
				{
					response.NumberofEmployerTradenamesCreated += employerTradeNamesCreated;
					MigrationRepository.SaveChanges(true);
				}

				// Get the next batch of employer tradenames
				employerTradeNames = provider.MigrateEmployerTradeNames(_providerArgs, preProcess);
			}

			//provider.PostprocessEmployerTradeNameData();

			selectIds = MigrationRepository.Query<Focus.Data.Migration.Entities.EmployerRequest>()
																		 .Where(x => x.RecordType == MigrationEmployerRecordType.TradeName)
																		 .Select(x => x.ExternalId);

			existingExternalIds = new HashSet<string>(selectIds);
		}

		private bool ProcessEmployerTradeName(Focus.Core.Messages.EmployerService.EmployerTradeNamesRequest request, bool isUpdate)
		{
			// This is a new record, create them 
			request.Module = AppSettings.Module;

			var employerTradeNamesRequest = new Focus.Data.Migration.Entities.EmployerRequest()
			{
				SourceId = request.Id,
				FocusId = request.EmployerId,
				Request = request.DataContractSerialize(),
				Status = request.MigrationValidationMessage.IsNullOrEmpty() ? MigrationStatus.ToBeProcessed : MigrationStatus.Failed,
				ExternalId = request.ExternalId ?? string.Empty,
				Message = request.MigrationValidationMessage ?? string.Empty,
				RecordType = MigrationEmployerRecordType.TradeName,
				ProcessedBy = DateTime.UtcNow,
				IsUpdate = isUpdate
			};

			// Serialise & Save to MigrationDb
			MigrationRepository.Add(employerTradeNamesRequest);

			return employerTradeNamesRequest.Status == MigrationStatus.ToBeProcessed;
		}


    /// <summary>
    /// Performs the migration of resume searches
    /// </summary>
    /// <param name="provider">The provider used to get the employer migrations.</param>
    /// <param name="response">A response object to indicate the status of the migration.</param>
    private void MigrateResumeSearches(IEmployer provider, MigrationResponse response)
    {
      // Get any existing requests
      var selectIds = MigrationRepository.Query<EmployerRequest>()
                                         .Where(x => x.RecordType == MigrationEmployerRecordType.ResumeSearch)
                                         .Select(x => x.ExternalId);

      var existingExternalIds = new HashSet<string>(selectIds);

      var totalResumeSearchesAdded = 0;
      var failedRecords = 0;
      
      // get a batch of saved searches
      var resumeSearches = provider.MigrateResumeSearches(_providerArgs);

      while (resumeSearches.Count > 0 && totalResumeSearchesAdded < _recordsToMigrate)
      {
        var resumeSearchesAdded = 0;

        resumeSearches.ForEach(s =>
        {
          if (totalResumeSearchesAdded < _recordsToMigrate)
          {
            var isExisting = existingExternalIds.Contains(s.SavedSearchMigrationId);
            if (!isExisting || _updateExistingRecords)
            {
              resumeSearchesAdded++;
              totalResumeSearchesAdded++;

              if (!ProcessResumeSearchRequest(s, MigrationEmployerRecordType.ResumeSearch, isExisting))
                failedRecords++;
            }

            if (!isExisting)
              existingExternalIds.Add(s.SavedSearchMigrationId);
          }
        });

        if (resumeSearchesAdded > 0)
        {
          response.NumberOfResumeSearchesCreated += resumeSearchesAdded;
          MigrationRepository.SaveChanges(true);
        }

        // Get the next batch of saved searches
        resumeSearches = provider.MigrateResumeSearches(_providerArgs);
      }

      response.NumberOfFailedRecords = failedRecords;
    }

    /// <summary>
    /// Performs the migration of resume alerts
    /// </summary>
    /// <param name="provider">The provider used to get the employer migrations.</param>
    /// <param name="response">A response object to indicate the status of the migration.</param>
    private void MigrateResumeAlerts(IEmployer provider, MigrationResponse response)
    {
      // Get any existing requests
      var selectIds = MigrationRepository.Query<EmployerRequest>()
                                         .Where(x => x.RecordType == MigrationEmployerRecordType.ResumeAlert)
                                         .Select(x => x.ExternalId);

      var existingExternalIds = new HashSet<string>(selectIds);

      var totalResumeAlertsAdded = 0;
      var failedRecords = 0;

      // get a batch of resume alerts
      var resumeAlerts = provider.MigrateResumeAlerts(_providerArgs);
      while (resumeAlerts.Count > 0 && totalResumeAlertsAdded < _recordsToMigrate)
      {
        var resumeAlertsAdded = 0;

        resumeAlerts.ForEach(s =>
        {
          if (totalResumeAlertsAdded < _recordsToMigrate)
          {
            var isExisting = existingExternalIds.Contains(s.SavedSearchMigrationId);
            if (!isExisting || _updateExistingRecords)
            {
              resumeAlertsAdded += 1;
              totalResumeAlertsAdded += 1;

              if (!ProcessResumeSearchRequest(s, MigrationEmployerRecordType.ResumeAlert, isExisting))
                failedRecords++;
            }

            if (!isExisting)
              existingExternalIds.Add(s.SavedSearchMigrationId);
          }
        });

        if (resumeAlertsAdded > 0)
        {
          response.NumberOfResumeAlertsCreated += resumeAlertsAdded;
          MigrationRepository.SaveChanges(true);
        }

        // Get the next batch of alerts
        resumeAlerts = provider.MigrateResumeAlerts(_providerArgs);
      }

      response.NumberOfFailedRecords = failedRecords;
    }

    /// <summary>
    /// Saves the resume search request in the migration database.
    /// </summary>
    /// <param name="request">The resume search request.</param>
    /// <param name="recordType">Type of the record.</param>
    /// <param name="isUpdate">Whether this is an update to an existing record.</param>
    /// <returns>A boolean indicating if the resume search request was created</returns>
    private bool ProcessResumeSearchRequest(SaveCandidateSavedSearchRequest request, MigrationEmployerRecordType recordType, bool isUpdate)
    {
      var employerRequest = new EmployerRequest
      {
        SourceId = 0,
        Request = request.DataContractSerialize(),
        Status = request.MigrationValidationMessage.IsNullOrEmpty() ? MigrationStatus.ToBeProcessed : MigrationStatus.Failed,
        ExternalId = request.SavedSearchMigrationId,
        Message = request.MigrationValidationMessage ?? string.Empty,
        RecordType = recordType,
        ProcessedBy = DateTime.UtcNow,
        IsUpdate = isUpdate
      };

      MigrationRepository.Add(employerRequest);

      return employerRequest.Status == MigrationStatus.ToBeProcessed;
    }

    #endregion

    #region JobOrders

    /// <summary>
    /// Performs the migration of job orders, to create job requests in the migration database.
    /// </summary>
    /// <param name="provider">The provider used to get the job order migrations.</param>
    /// <param name="response">A response object to indicate the status of the migration.</param>
    private void MigrateJobOrders(IJobOrder provider, MigrationResponse response)
		{
      // Get any existing ids
      var selectIds = MigrationRepository.Query<JobOrderRequest>()
                                         .Where(x => x.RecordType == MigrationJobOrderRecordType.Job && x.Status != MigrationStatus.Failed)
                                         .Select(x => x.ExternalId);

      var existingExternalIds = new HashSet<string>(selectIds);

      var totalRecordsCreated = 0;
      var failedRecords = 0;

      // get a batch of job orders
			var jobOrders = provider.MigrateJobOrders(_providerArgs);

      while (jobOrders.Count > 0 && totalRecordsCreated < _recordsToMigrate)
			{
			  var jobsCreated = 0;

			  // process the job orders
        jobOrders.ForEach(jo =>
        {
          if (totalRecordsCreated < _recordsToMigrate)
          {
            var isExisting = existingExternalIds.Contains(jo.JobMigrationId);
            if (!isExisting || _updateExistingRecords)
            {
              jobsCreated++;
              totalRecordsCreated++;

              if (!ProcessJobOrderRequest(MigrationJobOrderRecordType.Job, isExisting, jo.DataContractSerialize(), jo.JobMigrationId, failureMessage: jo.MigrationValidationMessage))
                failedRecords++;
            }

            if (!isExisting)
              existingExternalIds.Add(jo.JobMigrationId);
          }
        });

			  if (jobsCreated > 0)
			  {
			    response.NumberOfJobsCreated += jobsCreated;
			    MigrationRepository.SaveChanges();
			  }

			  // Get the next batch of employers
			  jobOrders = provider.MigrateJobOrders(_providerArgs);
			}

      // process the job locations
      selectIds = MigrationRepository.Query<JobOrderRequest>()
                                     .Where(x => x.RecordType == MigrationJobOrderRecordType.Location)
                                     .Select(x => x.ExternalId);

      existingExternalIds = new HashSet<string>(selectIds);

			 //get a batch of job locations
			var jobLocations = provider.MigrateJobLocations(_providerArgs);
      while (jobLocations.Count > 0 && totalRecordsCreated < _recordsToMigrate)
			{
			  var locationsAdded = 0;

        jobLocations.ForEach(jl =>
        {
          if (totalRecordsCreated < _recordsToMigrate)
          {
            var isExisting = existingExternalIds.Contains(jl.JobLocationMigrationId);
            if (!isExisting || _updateExistingRecords)
            {
              locationsAdded++;
              totalRecordsCreated++;

              if (!ProcessJobOrderRequest(MigrationJobOrderRecordType.Location, isExisting, jl.DataContractSerialize(), jl.JobLocationMigrationId, jl.JobMigrationId, jl.MigrationValidationMessage))
                failedRecords++;
            }

            if (!isExisting)
              existingExternalIds.Add(jl.JobLocationMigrationId);
          }
        });

			  if (locationsAdded > 0)
        {
          response.NumberOfJobLocationsCreated += locationsAdded;
          MigrationRepository.SaveChanges();
        }

			  // Get the next batch of job locations
			  jobLocations = provider.MigrateJobLocations(_providerArgs);
			}

      selectIds = MigrationRepository.Query<JobOrderRequest>()
                                     .Where(x => x.RecordType == MigrationJobOrderRecordType.Address)
                                     .Select(x => x.ExternalId);

      existingExternalIds = new HashSet<string>(selectIds);

      // get a batch of job addresses
      var jobAddresses = provider.MigrateJobAddresses(_providerArgs);
      while (jobAddresses.Count > 0 && totalRecordsCreated < _recordsToMigrate)
      {
        var addressesAdded = 0;

        // process the job addresses
        jobAddresses.ForEach(ja =>
        {
          if (totalRecordsCreated < _recordsToMigrate)
          {
            var isExisting = existingExternalIds.Contains(ja.JobAddressMigrationId);
            if (!isExisting || _updateExistingRecords)
            {
              addressesAdded++;
              totalRecordsCreated++;

              if (!ProcessJobOrderRequest(MigrationJobOrderRecordType.Address, isExisting, ja.DataContractSerialize(), ja.JobAddressMigrationId, ja.JobMigrationId, ja.MigrationValidationMessage))
                failedRecords++;
            }

            if (!isExisting)
              existingExternalIds.Add(ja.JobAddressMigrationId);
          }
        });

        if (addressesAdded > 0)
        {
          response.NumberOfJobAddressesCreated += addressesAdded;
          MigrationRepository.SaveChanges();
        }

        // Get the next batch of job addresses
        jobAddresses = provider.MigrateJobAddresses(_providerArgs);
      }

      selectIds = MigrationRepository.Query<JobOrderRequest>()
                                     .Where(x => x.RecordType == MigrationJobOrderRecordType.Certificate)
                                     .Select(x => x.ExternalId);

      existingExternalIds = new HashSet<string>(selectIds);

      // get a batch of job certificates
      var jobCertificates = provider.MigrateJobCertificates(_providerArgs);
      while (jobCertificates.Count > 0 && totalRecordsCreated < _recordsToMigrate)
      {
        var certificatesAdded = 0;

        // process the job certificates
        jobCertificates.ForEach(jc =>
        {
          if (totalRecordsCreated < _recordsToMigrate)
          {
            var isExisting = existingExternalIds.Contains(jc.JobCertificationMigrationId);
            if (!isExisting || _updateExistingRecords)
            {
              certificatesAdded++;
              totalRecordsCreated++;

              if (!ProcessJobOrderRequest(MigrationJobOrderRecordType.Certificate, isExisting, jc.DataContractSerialize(), jc.JobCertificationMigrationId, jc.JobMigrationId, jc.MigrationValidationMessage))
                failedRecords++;
            }

            if (!isExisting)
              existingExternalIds.Add(jc.JobCertificationMigrationId);
          }
        });

        if (certificatesAdded > 0)
        {
          response.NumberOfJobCertificatesCreated += certificatesAdded;
          MigrationRepository.SaveChanges();
        }

        // Get the next batch of job certificates
        jobCertificates = provider.MigrateJobCertificates(_providerArgs);
      }

      selectIds = MigrationRepository.Query<JobOrderRequest>()
                                     .Where(x => x.RecordType == MigrationJobOrderRecordType.Licence)
                                     .Select(x => x.ExternalId);

      existingExternalIds = new HashSet<string>(selectIds);

      // get a batch of job licences
      var jobLicences = provider.MigrateJobLicences(_providerArgs);
      while (jobLicences.Count > 0 && totalRecordsCreated < _recordsToMigrate)
      {
        var licencesAdded = 0;

        // process the job licences
        jobLicences.ForEach(jl =>
        {
          if (totalRecordsCreated < _recordsToMigrate)
          {
            var isExisting = existingExternalIds.Contains(jl.JobLicenceMigrationId);
            if (!isExisting || _updateExistingRecords)
            {
              licencesAdded++;
              totalRecordsCreated++;

              if (!ProcessJobOrderRequest(MigrationJobOrderRecordType.Licence, isExisting, jl.DataContractSerialize(), jl.JobLicenceMigrationId, jl.JobMigrationId, jl.MigrationValidationMessage))
                failedRecords++;
            }

            if (!isExisting)
              existingExternalIds.Add(jl.JobLicenceMigrationId);
          }
        });

        if (licencesAdded > 0)
        {
          response.NumberOfJobLicencesCreated += licencesAdded;
          MigrationRepository.SaveChanges();
        }

        // Get the next batch of job licences
        jobLicences = provider.MigrateJobLicences(_providerArgs);
      }

      selectIds = MigrationRepository.Query<JobOrderRequest>()
                                     .Where(x => x.RecordType == MigrationJobOrderRecordType.Language)
                                     .Select(x => x.ExternalId);

      existingExternalIds = new HashSet<string>(selectIds);

      // get a batch of job languages
      var jobLanguages = provider.MigrateJobLanguages(_providerArgs);
      while (jobLanguages.Count > 0 && totalRecordsCreated < _recordsToMigrate)
      {
        var languagesAdded = 0;

        // process the job languages
        jobLanguages.ForEach(jl =>
        {
          if (totalRecordsCreated < _recordsToMigrate)
          {
            var isExisting = existingExternalIds.Contains(jl.JobLanguageMigrationId);
            if (!isExisting || _updateExistingRecords)
            {
              languagesAdded++;
              totalRecordsCreated++;

              if (!ProcessJobOrderRequest(MigrationJobOrderRecordType.Language, isExisting, jl.DataContractSerialize(), jl.JobLanguageMigrationId, jl.JobMigrationId, jl.MigrationValidationMessage))
                failedRecords++;
            }

            if (!isExisting)
              existingExternalIds.Add(jl.JobLanguageMigrationId);
          }
        });

        if (languagesAdded > 0)
        {
          response.NumberOfJobLanguagesCreated += languagesAdded;
          MigrationRepository.SaveChanges();
        }

        // Get the next batch of job languages
        jobLanguages = provider.MigrateJobLanguages(_providerArgs);
      }

      selectIds = MigrationRepository.Query<JobOrderRequest>()
                                     .Where(x => x.RecordType == MigrationJobOrderRecordType.SpecialRequirement)
                                     .Select(x => x.ExternalId);

      existingExternalIds = new HashSet<string>(selectIds);

      // get a batch of job special requirements
      var jobSpecialRequirements = provider.MigrateJobSpecialRequirements(_providerArgs);
      while (jobSpecialRequirements.Count > 0 && totalRecordsCreated < _recordsToMigrate)
      {
        var specialRequirementsAdded = 0;

        // process the job specialRequirements
        jobSpecialRequirements.ForEach(jsr =>
        {
          if (totalRecordsCreated < _recordsToMigrate)
          {
            var isExisting = existingExternalIds.Contains(jsr.JobSpecialRequirementMigrationId);
            if (!isExisting || _updateExistingRecords)
            {
              specialRequirementsAdded++;
              totalRecordsCreated++;

              if (!ProcessJobOrderRequest(MigrationJobOrderRecordType.SpecialRequirement, isExisting, jsr.DataContractSerialize(), jsr.JobSpecialRequirementMigrationId, jsr.JobMigrationId, jsr.MigrationValidationMessage))
                failedRecords++;
            }

            if (!isExisting)
              existingExternalIds.Add(jsr.JobSpecialRequirementMigrationId);
          }
        });

        if (specialRequirementsAdded > 0)
        {
          response.NumberOfJobSpecialRequirementsCreated += specialRequirementsAdded;
          MigrationRepository.SaveChanges();
        }

        // Get the next batch of job specialRequirements
        jobSpecialRequirements = provider.MigrateJobSpecialRequirements(_providerArgs);
      }

      selectIds = MigrationRepository.Query<JobOrderRequest>()
                                     .Where(x => x.RecordType == MigrationJobOrderRecordType.ProgramOfStudy)
                                     .Select(x => x.ExternalId);

      existingExternalIds = new HashSet<string>(selectIds);

      // get a batch of job programs of study
      var jobProgramsOfStudy = provider.MigrateJobProgramsOfStudy(_providerArgs);
      while (jobProgramsOfStudy.Count > 0 && totalRecordsCreated < _recordsToMigrate)
      {
        var programsOfStudyAdded = 0;

        // process the job programs of study
        jobProgramsOfStudy.ForEach(jps =>
        {
          if (totalRecordsCreated < _recordsToMigrate)
          {
            var isExisting = existingExternalIds.Contains(jps.JobProgramOfStudyMigrationId);
            if (!isExisting || _updateExistingRecords)
            {
              programsOfStudyAdded++;
              totalRecordsCreated++;

              if (!ProcessJobOrderRequest(MigrationJobOrderRecordType.ProgramOfStudy, isExisting, jps.DataContractSerialize(), jps.JobProgramOfStudyMigrationId, jps.JobMigrationId, jps.MigrationValidationMessage))
                failedRecords++;
            }

            if (!isExisting)
              existingExternalIds.Add(jps.JobProgramOfStudyMigrationId);
          }
        });

        if (programsOfStudyAdded > 0)
        {
          response.NumberOfJobProgramsOfStudyCreated += programsOfStudyAdded;
          MigrationRepository.SaveChanges();
        }

        // Get the next batch of job programs of study
        jobProgramsOfStudy = provider.MigrateJobProgramsOfStudy(_providerArgs);
      }

      response.NumberOfFailedRecords = failedRecords;
		}

		/// <summary>
		/// Saves an assist user request in the migration database.
		/// </summary>
		/// <param name="type">The type assist user request.</param>
		/// <param name="serialisedRequest">The serialised request object.</param>
		/// <param name="migrationId">The unique id to identify the migration</param>
		/// <param name="secondaryMigrationId">An additional id to identify the migration</param>
		/// <param name="failedMessage">A message indicating if there was a problem with the migration</param>
		/// <returns>
		/// A boolean indicating if the assist request was created
		/// </returns>
		private bool ProcessSpideredJobOrderRequest(MigrationJobOrderRecordType type, bool isUpdate, string serialisedRequest, string migrationId, string parentMigrationId = null, string failureMessage = "")
		{
			// This is a new record, create them 
			var jobOrderRequest = new JobOrderRequest
			{
				SourceId = 0,
				Request = serialisedRequest,
				Status = failureMessage.IsNullOrEmpty() ? MigrationStatus.ToBeProcessed : MigrationStatus.Failed,
				ExternalId = migrationId,
				ParentExternalId = parentMigrationId,
				RecordType = type,
				Message = failureMessage ?? string.Empty,
				IsUpdate = isUpdate,
				ProcessedBy = DateTime.UtcNow
			};

			// Serialise & Save to MigrationDb
			MigrationRepository.Add(jobOrderRequest);

			return jobOrderRequest.Status == MigrationStatus.ToBeProcessed;
		}

    /// <summary>
    /// Saves the job order request in the migration database.
    /// </summary>
    /// <param name="type">The type of job order request (e.g, Job, Location, Address).</param>
    /// <param name="isUpdate">Whether this is an update to an existing record.</param>
    /// <param name="serialisedRequest">The serialised request.</param>
    /// <param name="migrationId">The id of the migration.</param>
    /// <param name="parentMigrationId">The id of the parent migration.</param>
    /// <param name="failureMessage">Optional message if request is known to be invalid</param>
    /// <returns>A boolean indicating if the job order request was created</returns>
    private bool ProcessJobOrderRequest(MigrationJobOrderRecordType type, bool isUpdate, string serialisedRequest, string migrationId, string parentMigrationId = null, string failureMessage = "")
    {
      // This is a new record, create them 
      var jobOrderRequest = new JobOrderRequest
      {
        SourceId = 0,
        Request = serialisedRequest,
        Status = failureMessage.IsNullOrEmpty() ? MigrationStatus.ToBeProcessed : MigrationStatus.Failed,
        ExternalId = migrationId,
        ParentExternalId =  parentMigrationId,
        RecordType = type,
        Message = failureMessage ?? string.Empty,
        IsUpdate = isUpdate,
        ProcessedBy = DateTime.UtcNow
      };

      // Serialise & Save to MigrationDb
      MigrationRepository.Add(jobOrderRequest);

      return jobOrderRequest.Status == MigrationStatus.ToBeProcessed;
    }

    #endregion

    #region JobSeekers

    /// <summary>
    /// Performs the migration of job seekers, to create job seeker requests in the migration database.
    /// </summary>
    /// <param name="provider">The provider used to get the job seeker migrations.</param>
    /// <param name="response">A response object to indicate the status of the migration.</param>
    private void MigrateJobSeekers(IJobSeeker provider, MigrationResponse response)
    {
      // Get existing job seeker ids
      var selectIds = MigrationRepository.Query<JobSeekerRequest>()
                                         .Where(x => x.RecordType == MigrationJobSeekerRecordType.JobSeeker)
                                         .Select(x => x.ExternalId);

			var existingExternalIds = new HashSet<string>(selectIds);
      
      var totalRecordsCreated = 0;
      var failedRecords = 0;
      
      // get a batch of job seekers
      var jobSeekers = provider.MigrateJobSeekers(_providerArgs);
      
      while (jobSeekers.Count > 0 && totalRecordsCreated < _recordsToMigrate)
      {
        var jobSeekersAdded = 0;

        jobSeekers.ForEach(js =>
        {
					if (totalRecordsCreated < _recordsToMigrate)
          {
						var isExisting = existingExternalIds.Contains(js.JobSeekerMigrationId);
						if (!isExisting || _updateExistingRecords)
            {
							jobSeekersAdded++;
							totalRecordsCreated++;

							UseAccountUserNameAsEmailAddressIfEmailAddressIsInvalid(js);

							if (!ProcessJobSeekerRequest(MigrationJobSeekerRecordType.JobSeeker, isExisting, js.DataContractSerialize(), js.JobSeekerMigrationId, ValidateJobSeekerRequest(js)))
                failedRecords++;
            }

            if (!isExisting)
              existingExternalIds.Add(js.JobSeekerMigrationId);
          }
        });

        if (jobSeekersAdded > 0)
        {
					response.NumberOfJobSeekersCreated += jobSeekersAdded;
          MigrationRepository.SaveChanges();
        }

        // Get the next batch of job seekers
				jobSeekers = provider.MigrateJobSeekers(_providerArgs);
      }

      response.NumberOfFailedRecords = failedRecords;
    }

	  public void UseAccountUserNameAsEmailAddressIfEmailAddressIsInvalid(RegisterCareerUserRequest js)
	  {
		  if ((js.EmailAddress.IsNullOrEmpty() ||
		       !Regex.IsMatch(js.EmailAddress, AppSettings.EmailAddressRegExPattern)) &&
		      Regex.IsMatch(js.AccountUserName, AppSettings.EmailAddressRegExPattern))
		  {
			  js.EmailAddress = js.AccountUserName;
		  }
	  }

	  /// <summary>
    /// Validates the register career user request
    /// </summary>
    /// <param name="request">The request to validate</param>
    /// <returns>A message if the request is invalid</returns>
    private string ValidateJobSeekerRequest(RegisterCareerUserRequest request)
    {
      var message = string.Empty;
			
      if (request.MigrationValidationMessage.IsNotNullOrEmpty())
        message = request.MigrationValidationMessage;
      else if (request.EmailAddress.IsNullOrEmpty() || !Regex.IsMatch(request.EmailAddress, AppSettings.EmailAddressRegExPattern))
        message = string.Format("Invalid email address:{0} RegexPattern:{1}", request.EmailAddress, AppSettings.EmailAddressRegExPattern);
      else if (request.AccountUserName.IsNullOrEmpty())
        message = "User name not specified";

      return message;
    }

    /// <summary>
    /// Performs the migration of job seeker's resumes, to create resume requests in the migration database.
    /// </summary>
    /// <param name="provider">The provider used to get the resume migrations.</param>
    /// <param name="response">A response object to indicate the status of the migration.</param>
    private void MigrateResumes(IResume provider, MigrationResponse response)
    {
      var selectIds = MigrationRepository.Query<JobSeekerRequest>()
                                   .Where(x => x.RecordType == MigrationJobSeekerRecordType.Resume)
                                   .Select(x => x.ExternalId);

      var existingExternalIds = new HashSet<string>(selectIds);

      var totalRecordsCreated = 0;
      var failedRecords = 0;
      
      //get a batch of resumes
      var resumes = provider.MigrateResumes(_providerArgs);

      while (resumes.Count > 0 && totalRecordsCreated < _recordsToMigrate)
      {
        var resumesAdded = 0;

        // process the resumes
        resumes.ForEach(r =>
        {
          if (totalRecordsCreated < _recordsToMigrate)
          {
            var isExisting = existingExternalIds.Contains(r.ResumeMigrationId);
            if (!isExisting || _updateExistingRecords)
            {
              resumesAdded += 1;
              totalRecordsCreated += 1;
              
              if (!ProcessJobSeekerRequest(MigrationJobSeekerRecordType.Resume, isExisting, r.DataContractSerialize(), r.ResumeMigrationId, ValidateResumeRequest(r)))
                failedRecords++;
            }

            if (!isExisting)
              existingExternalIds.Add(r.ResumeMigrationId);
          }
        });

        if (resumesAdded > 0)
        {
          response.NumberOfResumesCreated += resumesAdded;
          MigrationRepository.SaveChanges(true);
        }

        // Get the next batch of resumes
        resumes = provider.MigrateResumes(_providerArgs);
      }

      selectIds = MigrationRepository.Query<JobSeekerRequest>()
                                     .Where(x => x.RecordType == MigrationJobSeekerRecordType.ResumeDocument)
                                     .Select(x => x.ExternalId);

      existingExternalIds = new HashSet<string>(selectIds);

      //get a batch of resumes documents
      var resumeDocuments = provider.MigrateResumeDocuments(_providerArgs);
      while (resumeDocuments.Count > 0 && totalRecordsCreated < _recordsToMigrate)
      {
        var resumesAdded = 0;

        // process the resume documents
        resumeDocuments.ForEach(rd =>
        {
          if (totalRecordsCreated < _recordsToMigrate)
          {
            var isExisting = existingExternalIds.Contains(rd.ResumeMigrationId);
            if (!isExisting || _updateExistingRecords)
            {
              resumesAdded += 1;
              totalRecordsCreated += 1;

              if (!ProcessJobSeekerRequest(MigrationJobSeekerRecordType.ResumeDocument, isExisting, rd.DataContractSerialize(), rd.ResumeMigrationId, rd.MigrationValidationMessage))
                failedRecords++;
            }

            if (!isExisting)
              existingExternalIds.Add(rd.ResumeMigrationId);
          }
        });

        if (resumesAdded > 0)
        {
          response.NumberOfResumeDocumentsCreated += resumesAdded;
          MigrationRepository.SaveChanges(true);
        }

        // Get the next batch of resumes documents
        resumeDocuments = provider.MigrateResumeDocuments(_providerArgs);
      }

      response.NumberOfFailedRecords = failedRecords;
		}

    /// <summary>
    /// Validates the register career user request
    /// </summary>
    /// <param name="request">The request to validate</param>
    /// <returns>A message if the request is invalid</returns>
    private string ValidateResumeRequest(SaveResumeRequest request)
    {
      var message = string.Empty;

      if (request.MigrationValidationMessage.IsNotNullOrEmpty())
        message = request.MigrationValidationMessage;
      else if (request.SeekerResume.IsNull())
        message = "Unable to migrate resume";

      return message;
    }


    /// <summary>
    /// Performs the migration of saved searches job seekers, to create save search requests in the migration database.
    /// </summary>
    /// <param name="provider">The provider used to get the job seeker save search migrations.</param>
    /// <param name="response">A response object to indicate the status of the migration.</param>
    private void MigrateJobSeekerSavedSearches(IJobSeeker provider, MigrationResponse response)
    {
      // Get any existing requests
      var selectIds = MigrationRepository.Query<JobSeekerRequest>()
                                         .Where(x => x.RecordType == MigrationJobSeekerRecordType.SavedSearch)
                                         .Select(x => x.ExternalId);

      var existingExternalIds = new HashSet<string>(selectIds);

      var totalRecordsCreated = 0;
      var failedRecords = 0;
      
      // get a batch of saved searches
      var savedSearches = provider.MigrateJobSeekerSavedSearches(_providerArgs);
      while (savedSearches.Count > 0 && totalRecordsCreated < _recordsToMigrate)
      {
        var savedSearchesAdded = 0;

        savedSearches.ForEach(s =>
        {
          if (totalRecordsCreated < _recordsToMigrate)
          {
            var isExisting = existingExternalIds.Contains(s.SavedSearchMigrationId);
            if (!isExisting || _updateExistingRecords)
            {
              savedSearchesAdded += 1;
              totalRecordsCreated += 1;

              if (!ProcessJobSeekerRequest(MigrationJobSeekerRecordType.SavedSearch, isExisting, s.DataContractSerialize(), s.SavedSearchMigrationId, s.MigrationValidationMessage))
                failedRecords++;
            }

            if (!isExisting)
              existingExternalIds.Add(s.SavedSearchMigrationId);
          }
        });

        if (savedSearchesAdded > 0)
        {
          response.NumberOfSavedSearchesCreated += savedSearchesAdded;
          MigrationRepository.SaveChanges(true);
        }

        // Get the next batch of saved searches
        savedSearches = provider.MigrateJobSeekerSavedSearches(_providerArgs);
      }

      response.NumberOfFailedRecords = failedRecords;
    }

    /// <summary>
    /// Performs the migration of search alerts job seekers, to create save search requests in the migration database.
    /// </summary>
    /// <param name="provider">The provider used to get the job seeker search alert migrations.</param>
    /// <param name="response">A response object to indicate the status of the migration.</param>
    private void MigrateJobSeekerSearchAlerts(IJobSeeker provider, MigrationResponse response)
    {
      var selectIds = MigrationRepository.Query<JobSeekerRequest>()
                                   .Where(x => x.RecordType == MigrationJobSeekerRecordType.SearchAlert)
                                   .Select(x => x.ExternalId);

      var existingExternalIds = new HashSet<string>(selectIds);

      var totalRecordsCreated = 0;
      var failedRecords = 0;

      // get a batch of search alerts
      var searchAlerts = provider.MigrateJobSeekerSearchAlerts(_providerArgs);
      while (searchAlerts.Count > 0 && totalRecordsCreated < _recordsToMigrate)
      {
        var searchAlertsAdded = 0;

        // Get any existing requests
        searchAlerts.ForEach(s =>
        {
          if (totalRecordsCreated < _recordsToMigrate)
          {
            var isExisting = existingExternalIds.Contains(s.SavedSearchMigrationId);
            if (!isExisting || _updateExistingRecords)
            {
              searchAlertsAdded += 1;
              totalRecordsCreated += 1;

              if (!ProcessJobSeekerRequest(MigrationJobSeekerRecordType.SearchAlert, isExisting, s.DataContractSerialize(), s.SavedSearchMigrationId, s.MigrationValidationMessage))
                failedRecords++;
            }

            if (!isExisting)
              existingExternalIds.Add(s.SavedSearchMigrationId);
          }
        });

        if (searchAlertsAdded > 0)
        {
          response.NumberOfSearchAlertsCreated += searchAlertsAdded;
          MigrationRepository.SaveChanges(true);
        }

        // Get the next batch of search alerts
        searchAlerts = provider.MigrateJobSeekerSearchAlerts(_providerArgs);
      }

      response.NumberOfFailedRecords = failedRecords;
    }

    /// <summary>
    /// Performs the migration of saved searches job seekers, to create save search requests in the migration database.
    /// </summary>
    /// <param name="provider">The provider used to get the job seeker save search migrations.</param>
    /// <param name="response">A response object to indicate the status of the migration.</param>
    private void MigrateViewedPostings(IJobSeeker provider, MigrationResponse response)
    {
      // Get any existing requests
      var selectIds = MigrationRepository.Query<JobSeekerRequest>()
                                         .Where(x => x.RecordType == MigrationJobSeekerRecordType.ViewedPosting)
                                         .Select(x => x.ExternalId);

      var existingExternalIds = new HashSet<string>(selectIds);

      var totalRecordsCreated = 0;
      var failedRecords = 0;

      // get a batch of viewed postings
      var viewedPostings = provider.MigrateViewedPostings(_providerArgs);
      while (viewedPostings.Count > 0 && totalRecordsCreated < _recordsToMigrate)
      {
        var viewedPostingsAdded = 0;

        viewedPostings.ForEach(vp =>
        {
          if (totalRecordsCreated < _recordsToMigrate)
          {
            // Viewed postings are only ever added, not updated
            var isExisting = existingExternalIds.Contains(vp.ViewedPostingMigrationId);
            if (!isExisting)
            {
              viewedPostingsAdded += 1;
              totalRecordsCreated += 1;

              if (!ProcessJobSeekerRequest(MigrationJobSeekerRecordType.ViewedPosting, false, vp.DataContractSerialize(), vp.ViewedPostingMigrationId, vp.MigrationValidationMessage))
                failedRecords++;
            }

            if (!isExisting)
              existingExternalIds.Add(vp.ViewedPostingMigrationId);
          }
        });

        if (viewedPostingsAdded > 0)
        {
          response.NumberOfViewedPostingsCreated += viewedPostingsAdded;
          MigrationRepository.SaveChanges(true);
        }

        // Get the next batch of viewed postings
        viewedPostings = provider.MigrateViewedPostings(_providerArgs);
      }

      response.NumberOfFailedRecords = failedRecords;
    }

    /// <summary>
    /// Performs the migration of referral, to create referral requests in the migration database.
    /// </summary>
    /// <param name="provider">The provider used to get the referral migrations.</param>
    /// <param name="response">A response object to indicate the status of the migration.</param>
    private void MigrateReferrals(IJobSeeker provider, MigrationResponse response)
    {
      // Get any existing requests
      var selectIds = MigrationRepository.Query<JobSeekerRequest>()
                                         .Where(x => x.RecordType == MigrationJobSeekerRecordType.Referral)
                                         .Select(x => x.ExternalId);

      var existingExternalIds = new HashSet<string>(selectIds);

      var totalRecordsCreated = 0;
      var failedRecords = 0;

      // get a batch of referrals
      var referrals = provider.MigrateReferrals(_providerArgs);
      while (referrals.Count > 0 && totalRecordsCreated < _recordsToMigrate)
      {
        var referralsAdded = 0;

        referrals.ForEach(r =>
        {
          if (totalRecordsCreated < _recordsToMigrate)
          {
            var isExisting = existingExternalIds.Contains(r.ReferralMigrationId);
            if (!isExisting || _updateExistingRecords)
            {
              referralsAdded += 1;
              totalRecordsCreated += 1;

              if (!ProcessJobSeekerRequest(MigrationJobSeekerRecordType.Referral, isExisting, r.DataContractSerialize(), r.ReferralMigrationId, r.MigrationValidationMessage))
                failedRecords++;
            }

            if (!isExisting)
              existingExternalIds.Add(r.ReferralMigrationId);
          }
        });

        if (referralsAdded > 0)
        {
          response.NumberOfReferralsCreated += referralsAdded;
          MigrationRepository.SaveChanges(true);
        }

        // Get the next batch of viewed postings
        referrals = provider.MigrateReferrals(_providerArgs);
      }

      response.NumberOfFailedRecords = failedRecords;
    }

    /// <summary>
    /// Performs the migration of activities, to create activity requests in the migration database.
    /// </summary>
    /// <param name="provider">The provider used to get the activities migrations.</param>
    /// <param name="response">A response object to indicate the status of the migration.</param>
    private void MigrateJobSeekerActivities(IJobSeeker provider, MigrationResponse response)
    {
      // Get any existing requests
      var selectIds = MigrationRepository.Query<JobSeekerRequest>()
                                         .Where(x => x.RecordType == MigrationJobSeekerRecordType.Activity);

      //var existingExternalIds = new HashSet<string>(selectIds);

      var totalRecordsCreated = 0;
      var failedRecords = 0;

      // get a batch of activites
	    //var preProcess = true;
      var activities = provider.MigrateJobSeekerActivities(_providerArgs);
      while (activities.Count > 0 && totalRecordsCreated < _recordsToMigrate)
      {
        var activitiesAdded = 0;

        var migratedIds = activities.Select(a => a.ActivityMigrationId).ToList();

        var existingExternalIds = new HashSet<string>(selectIds.Where(x => migratedIds.Contains(x.ExternalId)).Select(x => x.ExternalId));

        activities.ForEach(a =>
        {
          if (totalRecordsCreated < _recordsToMigrate)
          {
            // Activities are only ever added, not updated
            var isExisting = existingExternalIds.Contains(a.ActivityMigrationId);
            if (!isExisting)
            {
              activitiesAdded += 1;
              totalRecordsCreated += 1;

              if (!ProcessJobSeekerRequest(MigrationJobSeekerRecordType.Activity, false, a.DataContractSerialize(), a.ActivityMigrationId, a.MigrationValidationMessage))
                failedRecords++;
            }

            if (!isExisting)
              existingExternalIds.Add(a.ActivityMigrationId);
          }
        });

        if (activitiesAdded > 0)
        {
          response.NumberOfActivitiesCreated += activitiesAdded;
          MigrationRepository.SaveChanges(true);
        }
				
        // Get the next batch of job seeker activities
        activities = provider.MigrateJobSeekerActivities(_providerArgs);
      }

      response.NumberOfFailedRecords = failedRecords;
    }

    /// <summary>
    /// Performs the migration of job seeker flags to create toggle flag requests in the migration database.
    /// </summary>
    /// <param name="provider">The provider used to get the flags migrations.</param>
    /// <param name="response">A response object to indicate the status of the migration.</param>
    private void MigrateFlaggedJobSeeker(IJobSeeker provider, MigrationResponse response)
    {
      // Get any existing requests
      var selectIds = MigrationRepository.Query<JobSeekerRequest>()
                                         .Where(x => x.RecordType == MigrationJobSeekerRecordType.Flagged)
                                         .Select(x => x.ExternalId);

      var existingExternalIds = new HashSet<string>(selectIds);

      var totalRecordsCreated = 0;
      var failedRecords = 0;

      // get a batch of activites
      var flags = provider.MigrateFlaggedJobSeekers(_providerArgs);
      while (flags.Count > 0 && totalRecordsCreated < _recordsToMigrate)
      {
        var flagsAdded = 0;

        flags.ForEach(f =>
        {
          if (totalRecordsCreated < _recordsToMigrate)
          {
            // Viewed postings are only ever added, not updated
            var isExisting = existingExternalIds.Contains(f.ToggleFlagMigrationId);
            if (!isExisting)
            {
              flagsAdded += 1;
              totalRecordsCreated += 1;

              if (!ProcessJobSeekerRequest(MigrationJobSeekerRecordType.Flagged, false, f.DataContractSerialize(), f.ToggleFlagMigrationId, f.MigrationValidationMessage))
                failedRecords++;
            }

            if (!isExisting)
              existingExternalIds.Add(f.ToggleFlagMigrationId);
          }
        });

        if (flagsAdded > 0)
        {
          response.NumberOfFlaggedJobSeekerCreated += flagsAdded;
          MigrationRepository.SaveChanges(true);
        }

        // Get the next batch of viewed postings
        flags = provider.MigrateFlaggedJobSeekers(_providerArgs);
      }

      response.NumberOfFailedRecords = failedRecords;
    }


    /// <summary>
    /// Saves the job seeker request in the migration database.
    /// </summary>
    /// <param name="type">The type of request.</param>
    /// <param name="isUpdate">Whether this is an update to an existing record.</param>
    /// <param name="serialisedRequest">The serialised request object.</param>
    /// <param name="migrationId">The unique id to identify the migration</param>
    /// <param name="failureMessage">Optional message if request is known to be invalid</param>
    /// <returns>
    /// A boolean indicating if the job seeker request was created
    /// </returns>
    private bool ProcessJobSeekerRequest(MigrationJobSeekerRecordType type, bool isUpdate, string serialisedRequest, string migrationId, string failureMessage = "")
		{
			// This is a new record, create them 
      var jobSeekerRequest = new JobSeekerRequest
			{
				SourceId = 0,
        Request = serialisedRequest, 
				Status = failureMessage.IsNullOrEmpty() ? MigrationStatus.ToBeProcessed : MigrationStatus.Failed,
        ExternalId = migrationId,
        ParentExternalId = type == MigrationJobSeekerRecordType.ResumeDocument ? migrationId : null,
        Message = failureMessage ?? string.Empty,
        RecordType = type,
        IsUpdate = isUpdate,
				ProcessedBy = DateTime.UtcNow
			};

			// Save to MigrationDb
			MigrationRepository.Add(jobSeekerRequest);

      return jobSeekerRequest.Status == MigrationStatus.ToBeProcessed;
		}

		#endregion

		#region Assist

    /// <summary>
    /// Performs the migration of users, to create Assist user requests in the migration database.
    /// </summary>
    /// <param name="provider">The provider used to get the user migrations.</param>
    /// <param name="response">A response object to indicate the status of the migration.</param>
    private void MigrateUsers(IUser provider, MigrationResponse response)
		{
      // process the users
      var selectIds = MigrationRepository.Query<UserRequest>()
                                         .Where(x => x.RecordType == MigrationAssistUserRecordType.AssistUser && x.Status != MigrationStatus.Failed)
                                         .Select(x => x.ExternalId);

      var existingExternalIds = new HashSet<string>(selectIds);

      var totalRecordsCreated = 0;
      var failedRecords = 0;

      // get a batch of users
      var users = provider.MigrateUsers(_providerArgs);

      while (users.Count > 0 && totalRecordsCreated < _recordsToMigrate)
      {
        var usersCreated = 0;

        users.Where(u => !existingExternalIds.Contains(u.UserMigrationId))
                  .ToList()
                  .ForEach(u =>
                  {
                    if (totalRecordsCreated < _recordsToMigrate)
                    {
                      usersCreated++;
                      totalRecordsCreated++;

                      if (!ProcessAssistUserRequest(MigrationAssistUserRecordType.AssistUser, false, u.DataContractSerialize(), u.UserMigrationId, u.UserSecondaryMigrationId, u.MigrationValidationMessage))
                        failedRecords++;

                      existingExternalIds.Add(u.UserMigrationId);
                    }
                  });

        if (usersCreated > 0)
        {
          response.NumberOfUsersCreated += usersCreated;
          MigrationRepository.SaveChanges(true);
        }

        // Get the next batch of users
        users = provider.MigrateUsers(_providerArgs);
      }

      response.NumberOfFailedRecords = failedRecords;
		}

    /// <summary>
    /// Performs the migration of Offices, to create Assist Office requests in the migration database.
    /// </summary>
    /// <param name="provider">The provider used to get the Office migrations.</param>
    /// <param name="response">A response object to indicate the status of the migration.</param>
    private void MigrateOffices(IUser provider, MigrationResponse response)
    {
      var selectIds = MigrationRepository.Query<UserRequest>()
                                   .Where(x => x.RecordType == MigrationAssistUserRecordType.Office)
                                   .Select(x => x.ExternalId);

      var existingExternalIds = new HashSet<string>(selectIds);

      var totalRecordsCreated = 0;
      var failedRecords = 0;

      // get a batch of Offices
      var offices = provider.MigrateOffices(_providerArgs);

      while (offices.Count > 0 && totalRecordsCreated < _recordsToMigrate)
      {
        var officesCreated = 0;

        // process the officers
        offices.ForEach(o =>
        {
          if (totalRecordsCreated < _recordsToMigrate)
          {
            var isExisting = existingExternalIds.Contains(o.OfficeMigrationId, StringComparer.OrdinalIgnoreCase);
            if (!isExisting || _updateExistingRecords)
            {
              officesCreated += 1;
              totalRecordsCreated += 1;

              if (!ProcessAssistUserRequest(MigrationAssistUserRecordType.Office, isExisting, o.DataContractSerialize(), o.OfficeMigrationId, null, o.MigrationValidationMessage))
                failedRecords++;
            }

            if (!isExisting)
              existingExternalIds.Add(o.OfficeMigrationId);
          }
        });

        if (officesCreated > 0)
        {
          response.NumberOfOfficesCreated += officesCreated;
          MigrationRepository.SaveChanges(true);
        }

        // Get the next batch of Offices
        offices = provider.MigrateOffices(_providerArgs);
      }

      response.NumberOfFailedRecords = failedRecords;
    }

    /// <summary>
    /// Performs the migration of Offices Mappings, to create Person Office Mapping requests in the migration database.
    /// </summary>
    /// <param name="provider">The provider used to get the Office migrations.</param>
    /// <param name="response">A response object to indicate the status of the migration.</param>
    private void MigrateOfficeMappings(IUser provider, MigrationResponse response)
    {
      var selectIds = MigrationRepository.Query<UserRequest>()
                                   .Where(x => x.RecordType == MigrationAssistUserRecordType.OfficeMapping)
                                   .Select(x => x.ExternalId);

      var existingExternalIds = new HashSet<string>(selectIds);

      var totalRecordsCreated = 0;
      var failedRecords = 0;

      // get a batch of Offices
      var offices = provider.MigrateOfficeMappings(_providerArgs);

      while (offices.Count > 0 && totalRecordsCreated < _recordsToMigrate)
      {
        var mappingsCreated = 0;

        // process the office mappings
        offices.Where(o => !existingExternalIds.Contains(o.UserMigrationId))
                  .ToList()
                  .ForEach(o =>
                  {
                    if (totalRecordsCreated < _recordsToMigrate)
                    {
                      mappingsCreated++;
                      totalRecordsCreated++;

                      if (!ProcessAssistUserRequest(MigrationAssistUserRecordType.OfficeMapping, false, o.DataContractSerialize(), o.UserMigrationId, null, o.MigrationValidationMessage))
                        failedRecords++;

                      existingExternalIds.Add(o.UserMigrationId);
                    }
                  });

        if (mappingsCreated > 0)
        {
          response.NumberOfOfficeMappingsCreated += mappingsCreated;
          MigrationRepository.SaveChanges(true);
        }

        // Get the next batch of Offices
        offices = provider.MigrateOfficeMappings(_providerArgs);
      }

      response.NumberOfFailedRecords = failedRecords;
    }

	  private void MigrateSpideredJobs(IJobOrder provider, MigrationResponse response)
	  {
		  #region spidered jobs

		  // Get any existing ids
		  var selectIds = MigrationRepository.Query<JobOrderRequest>()
			  .Where(x => x.RecordType == MigrationJobOrderRecordType.SpideredJob)
			  .Select(x => x.ExternalId);

		  var existingExternalIds = new HashSet<string>(selectIds);

		  var totalRecordsCreated = 0;
		  var failedRecords = 0;

		  // get a batch of job orders
		  var spideredJobOrders = provider.MigrateSpideredJobs(_providerArgs);

		  while (spideredJobOrders.Count > 0 && totalRecordsCreated < _recordsToMigrate)
		  {
			  var spideredJobsCreated = 0;

			  // process the job orders
			  spideredJobOrders.ForEach(jo =>
			  {
				  if (totalRecordsCreated < _recordsToMigrate)
				  {
					  var isExisting = existingExternalIds.Contains(jo.JobMigrationId);
					  if (!isExisting || _updateExistingRecords)
					  {
						  spideredJobsCreated++;
						  totalRecordsCreated++;

						  if (
							  !ProcessSpideredJobOrderRequest(MigrationJobOrderRecordType.SpideredJob, isExisting, jo.DataContractSerialize(),
								  jo.JobMigrationId, jo.LensPostingMigrationId, failureMessage: jo.MigrationValidationMessage))
							  failedRecords++;
					  }

					  if (!isExisting)
						  existingExternalIds.Add(jo.JobMigrationId);
				  }
			  });

			  if (spideredJobsCreated > 0)
			  {
				  response.NumberOfSpideredJobsCreated += spideredJobsCreated;
				  MigrationRepository.SaveChanges();
			  }

			  // Get the next batch of employers
			  spideredJobOrders = provider.MigrateSpideredJobs(_providerArgs);
		  }
	  

			#endregion

			#region spidered job referrals

			failedRecords += MigrateSpideredJobReferrals(provider, response);

			// process the spidered job referrals
			selectIds = MigrationRepository.Query<JobOrderRequest>()
															 .Where(x => x.RecordType == MigrationJobOrderRecordType.SpideredJobReferral)
															 .Select(x => x.ExternalId);

			existingExternalIds = new HashSet<string>(selectIds);

			// get a batch of spidered job referrals
			var spideredJobReferrals = provider.MigrateSpideredJobReferrals(_providerArgs);
			while (spideredJobReferrals.Count > 0 && totalRecordsCreated < _recordsToMigrate)
			{
				var spideredJobReferralsAdded = 0;

				spideredJobReferrals.ForEach(jl =>
				{
					if (totalRecordsCreated < _recordsToMigrate)
					{
						var isExisting = existingExternalIds.Contains(jl.ReferralMigrationId); // what migration id do we use to map spidered jobs to spidered job referrals

						if (!isExisting || _updateExistingRecords)
						{
							spideredJobReferralsAdded++;
							totalRecordsCreated++;

							if (
								!ProcessJobOrderRequest(MigrationJobOrderRecordType.SpideredJobReferral, isExisting, jl.DataContractSerialize(),
									jl.ReferralMigrationId, jl.LensPostingId, jl.MigrationValidationMessage))
								failedRecords++;
						}

						if (!isExisting)
							existingExternalIds.Add(jl.JobMigrationId);
					}
				});

				if (spideredJobReferralsAdded > 0)
				{
					response.NumberOfJobLocationsCreated += spideredJobReferralsAdded;
					MigrationRepository.SaveChanges();
				}

				// get the next batch of spidered job referrals
				spideredJobReferrals = provider.MigrateSpideredJobReferrals(_providerArgs);
			}

			#endregion
			
			response.NumberOfFailedRecords = failedRecords;
		}
		
    /// <summary>
    /// Performs the migration of notes and reminders
    /// </summary>
    /// <param name="provider">The provider used to get the migrations.</param>
    /// <param name="response">A response object to indicate the status of the migration.</param>
    private void MigrateNoteAndReminders(IUser provider, MigrationResponse response)
    {
      var selectIds = MigrationRepository.Query<UserRequest>()
                                         .Where(x => x.RecordType == MigrationAssistUserRecordType.NotesAndReminders);

      var totalRecordsCreated = 0;
      var failedRecords = 0;

      // get a batch of users
      var notes = provider.MigrateNotesAndReminders(_providerArgs);

      while (notes.Count > 0 && totalRecordsCreated < _recordsToMigrate)
      {
        var notesCreated = 0;

        var migratedIds = notes.Select(a => a.NoteReminderMigrationId).ToList();

        var existingExternalIds = new HashSet<string>(selectIds.Where(x => migratedIds.Contains(x.ExternalId)).Select(x => x.ExternalId));
        
        // process the notes
        notes.ForEach(u =>
        {
          if (totalRecordsCreated < _recordsToMigrate)
          {
            var isExisting = existingExternalIds.Contains(u.NoteReminderMigrationId);
            if (!isExisting || _updateExistingRecords)
            {
              notesCreated += 1;
              totalRecordsCreated += 1;

              if (!ProcessAssistUserRequest(MigrationAssistUserRecordType.NotesAndReminders, isExisting, u.DataContractSerialize(), u.NoteReminderMigrationId, null, u.MigrationValidationMessage))
                failedRecords++;
            }

            if (!isExisting)
              existingExternalIds.Add(u.NoteReminderMigrationId);
          }
        });

        if (notesCreated > 0)
        {
          response.NumberOfNotesAndRemindersCreated += notesCreated;
          MigrationRepository.SaveChanges(true);
        }

        // Get the next batch of users
        notes = provider.MigrateNotesAndReminders(_providerArgs);
      }

      response.NumberOfFailedRecords = failedRecords;
    }

    /// <summary>
    /// Saves an assist user request in the migration database.
    /// </summary>
    /// <param name="type">The type assist user request.</param>
    /// <param name="isUpdate">If this record is updating an existing record in Focus.</param>
    /// <param name="serialisedRequest">The serialised request object.</param>
    /// <param name="migrationId">The unique id to identify the migration</param>
    /// <param name="secondaryMigrationId">An additional id to identify the migration</param>
    /// <param name="failedMessage">A message indicating if there was a problem with the migration</param>
    /// <returns>
    /// A boolean indicating if the assist request was created
    /// </returns>
    private bool ProcessAssistUserRequest(MigrationAssistUserRecordType type, bool isUpdate, string serialisedRequest, string migrationId, string secondaryMigrationId, string failedMessage = "")
    {
      // This is a new record, create them 
      var userRequest = new UserRequest
      {
        SourceId = 0,
        Request = serialisedRequest,
        Status = failedMessage.IsNullOrEmpty() ? MigrationStatus.ToBeProcessed : MigrationStatus.Failed,
        ExternalId = migrationId,
        SecondaryExternalId = secondaryMigrationId,
        Message = failedMessage ?? string.Empty,
				RecordType = type,
        IsUpdate = isUpdate,
				ProcessedBy = DateTime.UtcNow
      };

      // Serialise & Save to MigrationDb
      MigrationRepository.Add(userRequest);

      return userRequest.Status == MigrationStatus.ToBeProcessed;
    }

		#endregion

		public bool isUpdate { get; set; }
  }
}
