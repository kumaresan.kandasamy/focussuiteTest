﻿#region Copyright © 2000-2014 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using Focus.Core;
using Focus.Core.Messages.DataImportService;
using Focus.Services.ServiceContracts;

#endregion

namespace Focus.MigrationServices.ServiceClients
{
  [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
  public interface IDataImportServiceChannel : IDataImportService, System.ServiceModel.IClientChannel
  {
  }

  [System.Diagnostics.DebuggerStepThroughAttribute]
  [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
  public class DataImportServiceClient : System.ServiceModel.ClientBase<IDataImportService>, IDataImportService
  {

    public DataImportServiceClient()
    {
    }

    public DataImportServiceClient(string endpointConfigurationName) :
      base(endpointConfigurationName)
    {
    }

    public DataImportServiceClient(string endpointConfigurationName, string remoteAddress) :
      base(endpointConfigurationName, remoteAddress)
    {
    }

    public DataImportServiceClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) :
      base(endpointConfigurationName, remoteAddress)
    {
    }

    public DataImportServiceClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) :
      base(binding, remoteAddress)
    {
    }

    public EmployerImportResponse ProcessEmployer(EmployerImportRequest request)
    {
      return Channel.ProcessEmployer(request);
    }

    public EmployerImportResponse ProcessBusinessUnitDescription(EmployerImportRequest request, bool resetRepository, long employeeId)
    {
      return Channel.ProcessBusinessUnitDescription(request, resetRepository, employeeId);
    }

    public EmployerImportResponse ProcessBusinessUnitLogo(EmployerImportRequest request, bool resetRepository, long employeeId)
    {
      return Channel.ProcessBusinessUnitLogo(request, resetRepository, employeeId);
    }

    public EmployerImportResponse ProcessResumeSearch(EmployerImportRequest request, MigrationEmployerRecordType recordType, bool resetRepository)
    {
      return Channel.ProcessResumeSearch(request, recordType, resetRepository);
    }

    public EmployerImportResponse ProcessEmployerTradeNames(EmployerImportRequest request, bool resetRepository, long employeeId)
    {
      return Channel.ProcessEmployerTradeNames(request, resetRepository, employeeId);
    }

    public JobSeekerImportResponse ProcessJobSeeker(JobSeekerImportRequest request)
    {
      return Channel.ProcessJobSeeker(request);
    }

    public JobSeekerImportResponse ProcessJobSeekerResume(JobSeekerImportRequest request)
    {
      return Channel.ProcessJobSeekerResume(request);
    }

    public JobSeekerImportResponse ProcessJobSeekerResumeDocument(JobSeekerImportRequest request, bool resetRepository)
    {
      return Channel.ProcessJobSeekerResumeDocument(request, resetRepository);
    }

    public JobSeekerImportResponse ProcessJobSeekerViewedPosting(JobSeekerImportRequest request)
    {
      return Channel.ProcessJobSeekerViewedPosting(request);
    }

    public JobSeekerImportResponse ProcessJobSeekerSavedSearch(JobSeekerImportRequest request, MigrationJobSeekerRecordType recordType)
    {
      return Channel.ProcessJobSeekerSavedSearch(request, recordType);
    }

    public JobSeekerImportResponse ProcessReferral(JobSeekerImportRequest request, MigrationJobSeekerRecordType recordType)
    {
      return Channel.ProcessReferral(request, recordType);
    }

    public JobSeekerImportResponse ProcessJobSeekerActivity(JobSeekerImportRequest request)
    {
      return Channel.ProcessJobSeekerActivity(request);
    }

    public JobSeekerImportResponse ProcessFlagJobSeeker(JobSeekerImportRequest request)
    {
      return Channel.ProcessFlagJobSeeker(request);
    }

    public JobOrderImportResponse ProcessJobOrder(JobOrderImportRequest request)
    {
      return Channel.ProcessJobOrder(request);
    }

    public JobOrderImportResponse ProcessJobLocation(JobOrderImportRequest request, bool resetRepository)
    {
      return Channel.ProcessJobLocation(request, resetRepository);
    }

    public JobOrderImportResponse ProcessJobAddress(JobOrderImportRequest request, bool resetRepository)
    {
      return Channel.ProcessJobAddress(request, resetRepository);
    }

    public JobOrderImportResponse ProcessJobCertificate(JobOrderImportRequest request, bool resetRepository)
    {
      return Channel.ProcessJobCertificate(request, resetRepository);
    }

    public JobOrderImportResponse ProcessJobLicence(JobOrderImportRequest request, bool resetRepository)
    {
      return Channel.ProcessJobLicence(request, resetRepository);
    }

    public JobOrderImportResponse ProcessJobLanguage(JobOrderImportRequest request, bool resetRepository)
    {
      return Channel.ProcessJobLanguage(request, resetRepository);
    }

    public JobOrderImportResponse ProcessJobSpecialRequirement(JobOrderImportRequest request, bool resetRepository)
    {
      return Channel.ProcessJobSpecialRequirement(request, resetRepository);
    }

    public JobOrderImportResponse ProcessJobProgramOfStudy(JobOrderImportRequest request, bool resetRepository)
    {
      return Channel.ProcessJobProgramOfStudy(request, resetRepository);
    }

    public JobOrderImportResponse GenerateJobPosting(JobOrderImportRequest request)
    {
      return Channel.GenerateJobPosting(request);
    }

    public AssistImportResponse ProcessCreateAssistUser(AssistImportRequest request)
    {
      return Channel.ProcessCreateAssistUser(request);
    }

    public AssistImportResponse ProcessNoteReminder(AssistImportRequest request)
    {
      return Channel.ProcessNoteReminder(request);
    }

    public AssistImportResponse ProcessSaveOffice(AssistImportRequest request)
    {
      return Channel.ProcessSaveOffice(request);
    }

    public AssistImportResponse ProcessOfficeMappings(AssistImportRequest request)
    {
      return Channel.ProcessOfficeMappings(request);
    }
  }
}
