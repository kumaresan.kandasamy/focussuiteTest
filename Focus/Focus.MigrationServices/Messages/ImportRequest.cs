﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

using Focus.Core;
using Focus.MigrationServices.Providers.Common;

#endregion

namespace Focus.MigrationServices.Messages
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ImportRequest : ServiceRequest
  {
    [DataMember]
    public bool Debug { get; set; }

		[DataMember]
		public int? BatchSize { get; set; }

    [DataMember]
    public int? RecordsToImport { get; set; }

		[DataMember]
		public RecordType RecordType { get; set; }

    [DataMember]
    public int? BatchNumber { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class ImportResponse : ServiceResponse
  {
    public ImportResponse()
    {

    }

    public ImportResponse(ImportRequest request): base(request)
    {

    }

    [DataMember]
    public int NumberOfEmployersCreated { get; set; }

		[DataMember]
		public int NumberOfEmployerTradeNamesCreated { get; set; }

		[DataMember]
		public int NumberOfTalentUsersCreated { get; set; }

    [DataMember]
    public int NumberOfJobOrdersCreated { get; set; }

    [DataMember]
    public int NumberOfJobSeekersCreated { get; set; }

		[DataMember]
		public int NumberOfAssistUsersCreated { get; set; }

    [DataMember]
    public int NumberOfOfficesCreated { get; set; }

    [DataMember]
    public int NumberOfOfficeMappingsCreated { get; set; }

		[DataMember]
		public int NumberOfJobLocationsCreated { get; set; }

		[DataMember]
		public int NumberOfJobAddressesCreated { get; set; }

    [DataMember]
    public int NumberOfJobCertificatesCreated { get; set; }

    [DataMember]
    public int NumberOfJobLicencesCreated { get; set; }

    [DataMember]
    public int NumberOfJobLanguagesCreated { get; set; }

    [DataMember]
    public int NumberOfJobSpecialRequirementsCreated { get; set; }

    [DataMember]
    public int NumberOfJobProgramOfStudysCreated { get; set; }

		[DataMember]
		public int NumberOfSpideredJobsCreated { get; set; }

		[DataMember]
		public int NumberOfSpideredJobReferralsCreated { get; set; }

		[DataMember]
		public int NumberOfSavedJobsCreated { get; set; }

		[DataMember]
		public int NumberOfJobsViewedCreated { get; set; }

    [DataMember]
    public int NumberOfResumesCreated { get; set; }

    [DataMember]
    public int NumberOfViewedPostingsCreated { get; set; }

    [DataMember]
    public int NumberOfReferralsCreated { get; set; }

    [DataMember]
    public int NumberOfActivitiesCreated { get; set; }

    [DataMember]
    public int NumberOfFlaggedJobSeekerCreated { get; set; }

    [DataMember]
    public int NumberOfSavedSearchesCreated { get; set; }

    [DataMember]
    public int NumberOfSearchAlertsCreated { get; set; }

    [DataMember]
    public int NumberOfNotesAndRemindersCreated { get; set; }

    [DataMember]
    public int NumberOfResumeSearchesCreated { get; set; }

    [DataMember]
    public int NumberOfResumeAlertsCreated { get; set; }

  	[DataMember]
  	public int TotalNumberRecordsCreated
  	{
  		get
  		{
  		  return NumberOfEmployersCreated
  		         + NumberOfTalentUsersCreated
  		         + NumberOfJobOrdersCreated
  		         + NumberOfAssistUsersCreated
  		         + NumberOfOfficesCreated
  		         + NumberOfOfficeMappingsCreated
  		         + NumberOfJobLocationsCreated
  		         + NumberOfJobAddressesCreated
  		         + NumberOfJobCertificatesCreated
  		         + NumberOfJobLicencesCreated
  		         + NumberOfJobLanguagesCreated
  		         + NumberOfJobSpecialRequirementsCreated
  		         + NumberOfJobProgramOfStudysCreated
  		         + NumberOfJobSeekersCreated
  		         + NumberOfResumesCreated
  		         + NumberOfViewedPostingsCreated
  		         + NumberOfSavedSearchesCreated
  		         + NumberOfSearchAlertsCreated
               + NumberOfReferralsCreated
  		         + NumberOfActivitiesCreated
  		         + NumberOfFlaggedJobSeekerCreated
  		         + NumberOfNotesAndRemindersCreated
  		         + NumberOfResumeSearchesCreated
							 + NumberOfResumeAlertsCreated
							 + NumberOfEmployerTradeNamesCreated
							 + NumberOfSpideredJobsCreated
							 + NumberOfSpideredJobReferralsCreated
							 + NumberOfSavedJobsCreated
							 + NumberOfJobsViewedCreated;
  		}
  	}
  }
}
