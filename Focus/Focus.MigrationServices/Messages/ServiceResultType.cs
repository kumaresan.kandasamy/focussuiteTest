﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

#endregion

namespace Focus.MigrationServices.Messages
{
	/// <summary>
	/// Enumeration of message response . 
	/// This is a simple enumerated values indicating success of failure. 
	/// </summary>
	public enum AcknowledgementType
	{
		/// <summary>
		/// Respresents an failure.
		/// </summary>
		Failure = 0,

		/// <summary>
		/// Represents a success.
		/// </summary>
		Success = 1
	}
}
