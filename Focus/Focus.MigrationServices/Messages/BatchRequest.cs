﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System.Runtime.Serialization;

using Focus.Core;
using Focus.MigrationServices.Providers.Common;

#endregion

namespace Focus.MigrationServices.Messages
{
  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class BatchRequest : ServiceRequest
  {
    [DataMember]
    public int NumberOfBatches { get; set; }

    [DataMember]
    public int RecordsToImport { get; set; }

    [DataMember]
    public RecordType RecordType { get; set; }
  }

  [DataContract(Namespace = Constants.DataContractNamespace)]
  public class BatchResponse : ServiceResponse
  {
    public BatchResponse()
    {

    }

    public BatchResponse(BatchRequest request) : base(request)
    {

    }

    [DataMember]
    public int[] RecordsPerBatch { get; set; }
  }
}
