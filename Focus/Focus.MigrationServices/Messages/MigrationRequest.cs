﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Runtime.Serialization;

using Focus.Core;
using Focus.MigrationServices.Providers.Common;

#endregion

namespace Focus.MigrationServices.Messages
{
	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class MigrationRequest : ServiceRequest
	{
		[DataMember]
		public bool Debug { get; set; }

		[DataMember]
		public int? BatchSize { get; set; }

    [DataMember]
    public int? RecordsToMigrate { get; set; }

    [DataMember]
    public string CustomParm { get; set; }

    [DataMember]
    public long LastId { get; set; }

    [DataMember]
		public RecordType RecordType { get; set; }

    [DataMember]
    public string MigrationProviderName { get; set; }

    [DataMember]
    public string SourceConnectionString { get; set; }

    [DataMember]
    public string SourceFolderName { get; set; }

    [DataMember]
    public string SourceFileName { get; set; }

    [DataMember]
    public bool SpoofData { get; set; }

    [DataMember]
    public bool UpdateExistingRecords { get; set; }

    [DataMember]
    public DateTime? CutOffDate { get; set; }
  }

	[DataContract(Namespace = Constants.DataContractNamespace)]
	public class MigrationResponse : ServiceResponse
	{
		public MigrationResponse()
		{
			
		}

		public MigrationResponse(MigrationRequest request) : base(request) 
		{
			
		}

		[DataMember]
		public int NumberOfEmployersCreated {get; set; }

    [DataMember]
    public int NumberOfEmployerLogosCreated { get; set; }

		[DataMember]
		public int NumberofEmployerTradenamesCreated { get; set; }
    
    [DataMember]
    public int NumberOfBusinessUnitDescriptionCreated { get; set; }

		[DataMember]
		public int NumberOfJobsCreated {get; set; }

    [DataMember]
    public int NumberOfJobLocationsCreated { get; set; }

    [DataMember]
    public int NumberOfJobAddressesCreated { get; set; }

    [DataMember]
    public int NumberOfJobCertificatesCreated { get; set; }

    [DataMember]
    public int NumberOfJobLicencesCreated { get; set; }

    [DataMember]
    public int NumberOfJobLanguagesCreated { get; set; }

    [DataMember]
    public int NumberOfJobSpecialRequirementsCreated { get; set; }

    [DataMember]
    public int NumberOfJobProgramsOfStudyCreated { get; set; }

		[DataMember]
		public int NumberOfJobSeekersCreated {get; set; }

    [DataMember]
    public int NumberOfResumesCreated { get; set; }

    [DataMember]
    public int NumberOfResumeDocumentsCreated { get; set; }

    [DataMember]
    public int NumberOfSavedSearchesCreated { get; set; }

    [DataMember]
    public int NumberOfSearchAlertsCreated { get; set; }

    [DataMember]
    public int NumberOfResumeSearchesCreated { get; set; }

    [DataMember]
    public int NumberOfResumeAlertsCreated { get; set; }

    [DataMember]
    public int NumberOfViewedPostingsCreated { get; set; }

    [DataMember]
    public int NumberOfSpideredJobsCreated { get; set; }

    [DataMember]
    public int NumberOfSpideredReferralsCreated { get; set; }

    [DataMember]
    public int NumberOfReferralsCreated { get; set; }

    [DataMember]
    public int NumberOfActivitiesCreated { get; set; }

    [DataMember]
    public int NumberOfFlaggedJobSeekerCreated { get; set; }

		[DataMember]
		public int NumberOfUsersCreated {get; set; }

    [DataMember]
    public int NumberOfOfficesCreated { get; set; }

    [DataMember]
    public int NumberOfOfficeMappingsCreated { get; set; }

    [DataMember]
    public int NumberOfNotesAndRemindersCreated { get; set; }

    [DataMember]
    public int NumberOfFailedRecords { get; set; }

		[DataMember]
		public int TotalNumberRecordsCreated
		{
			get
			{
			  return NumberOfEmployersCreated
               + NumberOfBusinessUnitDescriptionCreated
               + NumberOfEmployerLogosCreated
							 + NumberofEmployerTradenamesCreated
			         + NumberOfJobsCreated
               + NumberOfJobLocationsCreated
               + NumberOfJobAddressesCreated
               + NumberOfJobCertificatesCreated
               + NumberOfJobLicencesCreated
               + NumberOfJobLanguagesCreated
               + NumberOfJobSpecialRequirementsCreated
               + NumberOfJobProgramsOfStudyCreated
			         + NumberOfJobSeekersCreated
			         + NumberOfResumesCreated
			         + NumberOfResumeDocumentsCreated
			         + NumberOfSavedSearchesCreated
               + NumberOfSearchAlertsCreated
               + NumberOfResumeSearchesCreated
               + NumberOfResumeAlertsCreated
			         + NumberOfViewedPostingsCreated
               + NumberOfSpideredJobsCreated
               + NumberOfSpideredReferralsCreated
               + NumberOfReferralsCreated
               + NumberOfActivitiesCreated
               + NumberOfFlaggedJobSeekerCreated
			         + NumberOfUsersCreated
               + NumberOfOfficesCreated
               + NumberOfOfficeMappingsCreated
			         + NumberOfNotesAndRemindersCreated;
			}
		}
	}
}
