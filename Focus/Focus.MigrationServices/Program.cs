﻿#region Copyright © 2000-2013 Burning Glass International Inc.

// Proprietary and Confidential
//
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of Burning Glass International Inc.

#endregion

#region Using Directives

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;

using Focus.Core;
using Focus.Core.Messages.AuthenticationService;
using Focus.Core.Models;
using Focus.MigrationServices.Messages;
using Focus.MigrationServices.Providers.Common;
using Focus.MigrationServices.ServiceImplementations;
using Focus.Services;
using Focus.Services.ServiceImplementations;
using AcknowledgementType = Focus.Core.Messages.AcknowledgementType;

using Framework.Core;
using Framework.Logging;
using Framework.ServiceLocation;

#endregion

namespace Focus.MigrationServices
{
  public static class Program
  {
    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    private static void Main(string[] args)
    {
      var arguments = ParseArguments(args);
      if (arguments.RecordType == null)
        return;

      if (arguments.RecordType == RecordType.None)
      {
        Console.WriteLine(@"Unknown record type");
        return;
      }

      var servicesRuntime = new ServicesRuntime<StructureMapServiceLocator>(ServiceRuntimeEnvironment.DllOrExe, new StructureMapServiceLocator());
      Logger.FlushCount = 0;
      servicesRuntime.Start();

      var cultureName = ConfigurationManager.AppSettings["Focus-MigrationDefaultCulture"];
      if (cultureName.IsNotNullOrEmpty())
      {
        var culture = new CultureInfo(cultureName);
        Thread.CurrentThread.CurrentCulture = culture;
        Thread.CurrentThread.CurrentUICulture = culture;
      }

      try
      {
        var runtimeContext = new RuntimeContext(ServiceRuntimeEnvironment.DllOrExe);
        var appSettings = runtimeContext.AppSettings;

        var authService = new AuthenticationService(runtimeContext);

        var sessionRequest = new SessionRequest
        {
          ClientTag = appSettings.ClientTag,
          SessionId = Guid.Empty,
          RequestId = Guid.NewGuid(),
          UserContext = new UserContext
          {
            Culture = "**-**"
          }
        };
        var sessionId = authService.StartSession(sessionRequest).SessionId;

        var loginRequest = new LogInRequest
        {
          ClientTag = appSettings.ClientTag,
          SessionId = sessionId,
          RequestId = sessionRequest.RequestId,
          UserContext = sessionRequest.UserContext,
          UserName = ConfigurationManager.AppSettings.Get("Focus-MigrationSource-User"),
          Password = ConfigurationManager.AppSettings.Get("Focus-MigrationSource-Password"),
          ModuleToLoginTo = FocusModules.Assist
        };
        var loginResponse = authService.LogIn(loginRequest);
        if (loginResponse.Acknowledgement == AcknowledgementType.Failure)
        {
          Console.WriteLine(loginResponse.Message);
          return;
        }

        var userContext = new UserContext
        {
          UserId = loginResponse.UserId,
          ActionerId = loginResponse.UserId,
          FirstName = loginResponse.FirstName,
          LastName = loginResponse.LastName,
          EmailAddress = loginResponse.EmailAddress,
          PersonId = loginResponse.PersonId,
          EmployeeId = loginResponse.EmployeeId,
          EmployerId = loginResponse.EmployerId,
          ExternalUserId = loginResponse.ExternalUserId,
          ExternalUserName = loginResponse.ExternalUserName,
          ExternalPassword = loginResponse.ExternalPassword,
          ExternalOfficeId = loginResponse.ExternalOfficeId,
          ScreenName = string.IsNullOrEmpty(loginResponse.ScreenName) ? loginResponse.FirstName : loginResponse.ScreenName,
          UserName = loginResponse.UserName,
          LastLoggedInOn = loginResponse.LastLoggedInOn,
          IsMigrated = loginResponse.IsMigrated,
          RegulationsConsent = loginResponse.RegulationsConsent,
          Culture = sessionRequest.UserContext.Culture
        };

        var startTime = DateTime.Now;

        switch (arguments.ServiceType)
        {
          case "migrate":
            var migrationService = new MigrationService(runtimeContext);

            var isDir = arguments.FolderOrFileName.IsNotNullOrEmpty()
              && (File.GetAttributes(arguments.FolderOrFileName) & FileAttributes.Directory) == FileAttributes.Directory;

            var migrationRequest = new MigrationRequest
            {
              ClientTag = appSettings.ClientTag,
              SessionId = sessionId,
              RequestId = sessionRequest.RequestId,
              UserContext = userContext,
              VersionNumber = Constants.SystemVersion,
              BatchSize = arguments.BatchSize,
              CustomParm = arguments.CustomParm,
              LastId = arguments.LastId,
              RecordType = arguments.RecordType.Value,
              MigrationProviderName = arguments.ProviderName,
              SourceConnectionString = arguments.DatabaseConnection,
              SourceFolderName = isDir ? arguments.FolderOrFileName : string.Empty,
              SourceFileName = isDir ? string.Empty : arguments.FolderOrFileName,
              SpoofData = arguments.SpoofData,
              UpdateExistingRecords = arguments.UpdateExistingRecords,
              CutOffDate = arguments.CutOffDate,
              RecordsToMigrate = arguments.RecordsToImport.GetValueOrDefault(int.MaxValue),
            };

            var migrationResponse = migrationService.Migrate(migrationRequest);
            Console.WriteLine(migrationResponse.Acknowledgement == Messages.AcknowledgementType.Success
                                ? string.Concat("Migration complete. Records migrated: ", migrationResponse.TotalNumberRecordsCreated)
                                : string.Concat("Migration failed with message:", migrationResponse.Message));

            if (migrationResponse.NumberOfFailedRecords > 0)
              Console.WriteLine(string.Concat("Records failed: ", migrationResponse.NumberOfFailedRecords));

            break;

          case "batch":
            var batchService = new BatchService(runtimeContext);
            var batchRequest = new BatchRequest
            {
              ClientTag = appSettings.ClientTag,
              SessionId = sessionId,
              RequestId = sessionRequest.RequestId,
              UserContext = userContext,
              VersionNumber = Constants.SystemVersion,
              NumberOfBatches = arguments.NumberOfBatches.GetValueOrDefault(0),
              RecordsToImport = arguments.RecordsToImport.GetValueOrDefault(int.MaxValue),
              RecordType = arguments.RecordType.Value
            };

            var batchResponse = batchService.Batch(batchRequest);
            Console.WriteLine(batchResponse.Acknowledgement == Messages.AcknowledgementType.Success
                                ? string.Concat("Batching complete. Max records per batch: ", batchResponse.RecordsPerBatch[0])
                                : string.Concat("Batching failed with message:", batchResponse.Message));
            break;

          case "import":
            if (arguments.NumberOfBatches.IsNotNull())
            {
              if (arguments.RecordType.Value.IsIn(RecordType.Employer))
              {
                Console.WriteLine("Batching not supported for {0} record type", RecordType.Employer.ToString());
              }
              else
              {
                var importBatchService = new BatchService(runtimeContext);
                var importBatchRequest = new BatchRequest
                  {
                    ClientTag = appSettings.ClientTag,
                    SessionId = sessionId,
                    RequestId = sessionRequest.RequestId,
                    UserContext = userContext,
                    VersionNumber = Constants.SystemVersion,
                    NumberOfBatches = arguments.NumberOfBatches.GetValueOrDefault(0),
                    RecordsToImport = arguments.RecordsToImport.GetValueOrDefault(int.MaxValue),
                    RecordType = arguments.RecordType.Value
                  };

                var importBatchResponse = importBatchService.Batch(importBatchRequest);
                if (importBatchResponse.Acknowledgement == Messages.AcknowledgementType.Success)
                {
                  var currentExe = System.Reflection.Assembly.GetEntryAssembly().Location;
                  var basicArgs = string.Join(" ", args.Skip(2).Where(a => !Regex.IsMatch(a, @"^\d+$") && !a.ToLower().StartsWith("/n:")));

                  var processes = new List<Process>();
                  for (var batchNumber = 1; batchNumber <= arguments.NumberOfBatches; batchNumber++)
                  {
                    var proc = new Process
                      {
                        StartInfo =
                          {
                            FileName = currentExe,
                            Arguments = string.Format("import {0} {1} /bn:{2} {3}", arguments.RecordType, importBatchResponse.RecordsPerBatch[batchNumber - 1], batchNumber, basicArgs),
                            RedirectStandardOutput = true,
                            UseShellExecute = false
                          }
                      };

                    Console.WriteLine(string.Concat("Running command: ", proc.StartInfo.Arguments));

                    processes.Add(proc);
                    proc.Start();
                  }

                  var processesFinished = new List<int>();
                  var finishedCount = 0;
                  var processCount = processes.Count;

                  while (finishedCount < processCount)
                  {
                    for (var index = 0; index < processCount; index++)
                    {
                      var proc = processes[index];
                      if (!processesFinished.Contains(index) && proc.HasExited)
                      {
                        finishedCount++;
                        processesFinished.Add(index);

                        Console.WriteLine(proc.StandardOutput.ReadToEnd());
                      }
                    }
                    Thread.Sleep(1000);
                  }
                }
                else
                {
                  Console.WriteLine(string.Concat("Batching failed with message:", importBatchResponse.Message));
                }
              }
            }
            else
            {
              var importService = new ImportService(runtimeContext);

              var importRequest = new ImportRequest
              {
                ClientTag = appSettings.ClientTag,
                SessionId = sessionId,
                RequestId = sessionRequest.RequestId,
                UserContext = userContext,
                VersionNumber = Constants.SystemVersion,
                BatchSize = arguments.BatchSize,
                BatchNumber = arguments.BatchNumber,
                RecordsToImport = arguments.RecordsToImport.GetValueOrDefault(int.MaxValue),
                RecordType = arguments.RecordType.Value
              };

              var importResponse = importService.Import(importRequest);

              var batchText = arguments.BatchNumber.HasValue ? string.Concat("Batch ", arguments.BatchNumber, " - ") : string.Empty;
              Console.WriteLine(importResponse.Acknowledgement == Messages.AcknowledgementType.Success
                                  ? string.Concat(batchText, "Import complete. Records imported: ", importResponse.TotalNumberRecordsCreated)
                                  : string.Concat(batchText, "Import failed with message:", importResponse.Message));              
            }

            break;
        }

        var endTime = DateTime.Now;

        Console.WriteLine(@"Time taken (seconds):" + endTime.Subtract(startTime).TotalSeconds.ToString(CultureInfo.InvariantCulture));
        Console.WriteLine();
      }
      catch (Exception ex)
      {
        Logger.Error(ex.Message, ex);
        Console.WriteLine(string.Concat("Unexpected error:", ex.Message));
      }
      finally
      {
        servicesRuntime.Shutdown();
      }
      Console.WriteLine(string.Empty);
    }

    public static ServiceArguments ParseArguments(string[] args)
    {
      if (args.Length < 2 || args[0].ToLowerInvariant().IsNotIn("/?", "migrate", "import", "batch"))
      {
        Console.WriteLine(@"Usage: ");
        Console.WriteLine(@"   MIGRATIONSERVICES.EXE MIGRATE recordtype [recordstomigrate] [/B:batchsize] [/P:providername] [/F:foldername] [/D:databaseconnection] [/C:customparameter] [/L:lastid] [/E[:cutoffdate]] [/SPOOF]");
        Console.WriteLine(@"   MIGRATIONSERVICES.EXE BATCH recordtype recordstoimport /N:numberofbatches");
        Console.WriteLine(@"   MIGRATIONSERVICES.EXE IMPORT recordtype [recordstoimport [/N:numberofbatches|/BN:batchnumber]]");
        return new ServiceArguments { RecordType = RecordType.None };
      }

      var arguments = new ServiceArguments { ServiceType = args[0].ToLowerInvariant() };

      switch (args[1].ToLower())
      {
				case "job":
				case "jobs":
          arguments.RecordType = RecordType.Job;
          break;
				case "jobseeker":
				case "jobseekers":
          arguments.RecordType = RecordType.JobSeeker;
          break;
				case "resume":
				case "resumes":
          arguments.RecordType = RecordType.Resume;
          break;
				case "savedsearch":
				case "savedsearches":
          arguments.RecordType = RecordType.SavedSearch;
          break;
				case "jobalert":
				case "jobalerts":
				case "searchalert":
				case "searchalerts":
          arguments.RecordType = RecordType.SearchAlert;
          break;
				case "resumesearch":
				case "resumesearches":
          arguments.RecordType = RecordType.ResumeSearch;
          break;
				case "resumealert":
				case "resumealerts":
          arguments.RecordType = RecordType.ResumeAlert;
          break;
				case "viewedposting":
				case "viewedpostings":
          arguments.RecordType = RecordType.ViewedPosting;
          break;
				case "employer":
				case "employers":
          arguments.RecordType = RecordType.Employer;
          break;
				case "employertradename":
				case "employertradenames":
					arguments.RecordType = RecordType.EmployerTradeName;
		      break;
				case "assistuser":
				case "assistusers":
          arguments.RecordType = RecordType.AssistUser;
          break;
				case "office":
				case "offices":
          arguments.RecordType = RecordType.Office;
          break;
				case "officemapping":
				case "officemappings":
          arguments.RecordType = RecordType.OfficeMapping;
          break;
        case "note":
				case "notes":
          arguments.RecordType = RecordType.Notes;
          break;
        case "referral":
        case "referrals":
          arguments.RecordType = RecordType.Referral;
          break;
				case "flag":
				case "flags":
          arguments.RecordType = RecordType.Flagged;
          break;
        case "activity":
        case "activities":
          arguments.RecordType = RecordType.Activity;
					break;
				case "spideredjob":
				case "spideredjobs":
					arguments.RecordType = RecordType.SpideredJobs;
					break;
				case "spideredjobreferral":
				case "spideredjobreferrals":
					arguments.RecordType = RecordType.SpideredJobReferrals;
					break;
				case "savedjob":
				case "savedjobs":
					arguments.RecordType = RecordType.SavedJob;
					break;
        default:
          arguments.RecordType = RecordType.None;
          break;
      }

      var argCount = args.Length;
      for (var argIndex = 2; argIndex < argCount; argIndex++)
      {
        var argData = args[argIndex];

        if (Regex.IsMatch(argData, @"^\d+$"))
        {
          if (!arguments.RecordsToImport.HasValue)
            arguments.RecordsToImport = int.Parse(argData);
          else
            throw new Exception(string.Format("Unknown paramater: {0}", argData)); 
        }
        else if (argData.Length > 3)
        {
          var argSwitch = argData.Contains(":") ? argData.Substring(0, argData.IndexOf(":", StringComparison.Ordinal)).ToLower() : argData.ToLower();
          var argValue = argData.Contains(":") ? argData.Substring(argData.IndexOf(":", StringComparison.Ordinal) + 1).ToLower().Trim() : string.Empty;

          switch (argSwitch)
          {
            case "/p":
              arguments.ProviderName = argValue;
              break;

            case "/f":
              arguments.FolderOrFileName = argValue;
              break;

            case "/d":
              arguments.DatabaseConnection = argValue;
              break;

            case "/b":
              arguments.BatchSize = int.Parse(argValue);
              break;

            case "/bn":
              arguments.BatchNumber = int.Parse(argValue);
              break;

            case "/n":
              arguments.NumberOfBatches = int.Parse(argValue);
              if (arguments.NumberOfBatches > 20)
                throw new Exception(@"Maximum number of batches is 20");
              break;

            case "/spoof":
              arguments.SpoofData = true;
              break;

            case "/c":
              arguments.CustomParm = argValue;
              break;

            case "/e":
              arguments.UpdateExistingRecords = true;
              if (argValue.IsNotNullOrEmpty())
                arguments.CutOffDate = DateTime.Parse(argValue);
              break;

            case "/l":
              arguments.LastId = long.Parse(argValue);
              break;

            default:
              throw new Exception(string.Format("Unknown paramater: {0}", argData));
          }
        }
        else
        {
          throw new Exception(string.Format("Unknown paramater: {0}", argData));
        }
      }

      return arguments;
    }

    public struct ServiceArguments
    {
      public string ServiceType;
      public RecordType? RecordType;
      public int? BatchSize;
      public string ProviderName;
      public string FolderOrFileName;
      public string DatabaseConnection;
      public int? RecordsToImport;
      public int? NumberOfBatches;
      public int? BatchNumber;
      public bool SpoofData;
      public string CustomParm;
      public DateTime? CutOffDate;
      public bool UpdateExistingRecords;
      public long LastId;
    }
  }
}
